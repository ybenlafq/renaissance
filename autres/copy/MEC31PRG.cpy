      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:03 >
      ******************************************************************
      * CETTE COPY EST EN FAIT UN PROGRAMME ENTIER; ELLE EST UTILISEE
      * PAR TOUS LES PROGRAMMES MEC31X, OU X EST LE SUFFIXE FILIALE
      * CE PROGRAMME AJOUTE DES PSE POST ACHAT
      *    V1 : AJOUT DE PSE
      ********************************************GV11-N****************
      *  modification du lieu paiement et Seqnens
      *  faire 'PSE' en position 1
      ******************************************************************
      * 13-03-2012 - DE02074 - AJOUT CODE-ACTION DANS MEC00M
      ******************************************************************
      * 27-03-2012 - DE02074 - AJOUT gestion mode paiement
      *                        appel au mbs60 pour faire les dupli
      *                        suite probleme avec les filialles
      ******************************************************************
      *   LES FLAGS
      ******************************************************************
      * 07/11/2012  J. BICHY : mettre  en phase le nseqnq de GV10 et
      *                        GV11
      ******************************************************************
      * 01/08/2013  Mantis 23862 De02009
      *             PSE assurentielle
      ******************************************************************
      * 20/12/2014  demande pole client ajout appel mbs42 pour
      *             desarchivage  quand pse demandee sur vente sav
      ******************************************************************
      * 30/04/2015  Permettre l'ajout de la PSE m�me si la vente n'est
      *             pas totalement encaiss�e
      ******************************************************************
       01  TOP-QTE           PIC X.
       88  QTE-SUP-3-OUI              VALUE '1'.
       88  QTE-SUP-3-NON              VALUE '0'.
       01  ETAT-PAIEMENT      PIC X.
       88  PAIEMENT-OK                VALUE '1'.
       88  PAIEMENT-KO                VALUE '0'.
       01  ETAT-GV11-TROUVEE  PIC X.
       88  WS-GV11-TROUVEE            VALUE '1'.
       88  WS-GV11-NON-TROUVEE        VALUE '0'.
       01  ETAT-GV31-TROUVEE  PIC X.
       88  WS-GV31-TROUVEE            VALUE '1'.
       88  WS-GV31-NON-TROUVEE        VALUE '0'.
       01  ETAT-CONTROLES     PIC X.
       88  WS-CONTROLES-OK            VALUE '1'.
       88  WS-CONTROLES-KO            VALUE '0'.
       01  ETAT-MAJ           PIC X.
       88  WS-MAJ-OK                  VALUE '1'.
       88  WS-MAJ-KO                  VALUE '0'.
      ******************************************************************
      *   LES ZONES
      ******************************************************************
       01  RESTE-PVTOTAL
               PIC S9(7)V9(0002) COMP-3.
       01  RVGV1106-B.
           02  GV11B-NSOCIETE
               PIC X(0003).
           02  GV11B-NLIEU
               PIC X(0003).
           02  GV11B-NVENTE
               PIC X(0007).
           02  GV11B-CTYPENREG
               PIC X(0001).
           02  GV11B-NCODICGRP
               PIC X(0007).
           02  GV11B-NCODIC
               PIC X(0007).
           02  GV11B-NSEQ
               PIC X(0002).
           02  GV11B-CMODDEL
               PIC X(0003).
           02  GV11B-DDELIV
               PIC X(0008).
           02  GV11B-NORDRE
               PIC X(0005).
           02  GV11B-CENREG
               PIC X(0005).
           02  GV11B-QVENDUE
               PIC S9(5) COMP-3.
           02  GV11B-PVUNIT
               PIC S9(7)V9(0002) COMP-3.
           02  GV11B-PVUNITF
               PIC S9(7)V9(0002) COMP-3.
           02  GV11B-PVTOTAL
               PIC S9(7)V9(0002) COMP-3.
           02  GV11B-CEQUIPE
               PIC X(0005).
           02  GV11B-NLIGNE
               PIC X(0002).
           02  GV11B-TAUXTVA
               PIC S9(3)V9(0002) COMP-3.
           02  GV11B-QCONDT
               PIC S9(5) COMP-3.
           02  GV11B-PRMP
               PIC S9(7)V9(0002) COMP-3.
           02  GV11B-WEMPORTE
               PIC X(0001).
           02  GV11B-CPLAGE
               PIC X(0002).
           02  GV11B-CPROTOUR
               PIC X(0005).
           02  GV11B-CADRTOUR
               PIC X(0001).
           02  GV11B-CPOSTAL
               PIC X(0005).
           02  GV11B-LCOMMENT
               PIC X(0035).
           02  GV11B-CVENDEUR
               PIC X(0006).
           02  GV11B-NSOCLIVR
               PIC X(0003).
           02  GV11B-NDEPOT
               PIC X(0003).
           02  GV11B-NAUTORM
               PIC X(0005).
           02  GV11B-WARTINEX
               PIC X(0001).
           02  GV11B-WEDITBL
               PIC X(0001).
           02  GV11B-WACOMMUTER
               PIC X(0001).
           02  GV11B-WCQERESF
               PIC X(0001).
           02  GV11B-NMUTATION
               PIC X(0007).
           02  GV11B-CTOURNEE
               PIC X(0008).
           02  GV11B-WTOPELIVRE
               PIC X(0001).
           02  GV11B-DCOMPTA
               PIC X(0008).
           02  GV11B-DCREATION
               PIC X(0008).
           02  GV11B-HCREATION
               PIC X(0004).
           02  GV11B-DANNULATION
               PIC X(0008).
           02  GV11B-NLIEUORIG
               PIC X(0003).
           02  GV11B-WINTMAJ
               PIC X(0001).
           02  GV11B-DSYST
               PIC S9(13) COMP-3.
           02  GV11B-DSTAT
               PIC X(0004).
           02  GV11B-CPRESTGRP
               PIC X(0005).
           02  GV11B-NSOCMODIF
               PIC X(03).
           02  GV11B-NLIEUMODIF
               PIC X(03).
           02  GV11B-DATENC
               PIC X(08).
           02  GV11B-CDEV
               PIC X(03).
           02  GV11B-WUNITR
               PIC X(20).
           02  GV11B-LRCMMT
               PIC X(10).
           02  GV11B-NSOCP
               PIC X(03).
           02  GV11B-NLIEUP
               PIC X(03).
           02  GV11B-NTRANS
               PIC S9(8) COMP-3.
           02  GV11B-NSEQFV
               PIC X(02).
           02  GV11B-NSEQNQ
               PIC S9(5) COMP-3.
           02  GV11B-NSEQREF
               PIC S9(5) COMP-3.
           02  GV11B-NMODIF
               PIC S9(5) COMP-3.
           02  GV11B-NSOCORIG
               PIC X(03).
           02  GV11B-DTOPE
               PIC X(08).
           02  GV11B-NSOCLIVR1
               PIC X(03).
           02  GV11B-NDEPOT1
               PIC X(03).
           02  GV11B-NSOCGEST
               PIC X(03).
           02  GV11B-NLIEUGEST
               PIC X(03).
           02  GV11B-NSOCDEPLIV
               PIC X(03).
           02  GV11B-NLIEUDEPLIV
               PIC X(03).
           02  GV11B-TYPVTE
               PIC X(01).
           02  GV11B-CTYPENT
               PIC X(02).
           02  GV11B-NLIEN
               PIC S9(5) COMP-3.
           02  GV11B-NACTVTE
               PIC S9(5) COMP-3.
           02  GV11B-PVCODIG
               PIC S9(7)V9(0002) COMP-3.
           02  GV11B-QTCODIG
               PIC S9(5) COMP-3.
           02  GV11B-DPRLG
               PIC X(08).
           02  GV11B-CALERTE
               PIC X(05).
           02  GV11B-CREPRISE
               PIC X(05).
           02  GV11B-NSEQENS
               PIC S9(5) COMP-3.
           02  GV11B-MPRIMECLI
               PIC S9(7)V9(0002) COMP-3.
      *
      ******************************************************************
       77 WS-DATE-DU-JOUR  PIC X(08).
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *77  LONGUEUR-COMMAREA        PIC S9(4) COMP  VALUE +200.
      *--
       77  LONGUEUR-COMMAREA        PIC S9(4) COMP-5  VALUE +200.
      *}
       01  WS-GV11-NLIGNE-SAUV    PIC X(2).
       01  EDIT-NSEQ              PIC ZZ9.
       01  EDIT-SQL               PIC -ZZ999.
       01  EDIT-GV14              PIC -ZZZ999,99.
       01  EDIT-GV11              PIC -ZZZ999,99.
       01  WS-GV11-NLIGNE-PICX    PIC X(2).
       01  WS-GV11-NLIGNE-PIC9
                      REDEFINES WS-GV11-NLIGNE-PICX PIC 9(2).
       01  WS-GV11-NLI    PIC X(2).
       01  WS-GV11-NLI9
                      REDEFINES WS-GV11-NLI PIC 9(2).
       01  WS-MAX-NSEQ2             PIC X(2)        VALUE '00'.
       01  WS-MAX-NSEQ29  REDEFINES WS-MAX-NSEQ2 PIC 9(2).
       01  WS-MAX-NSEQ              PIC X(2)        VALUE '00'.
       01  WS-MAX-NSEQ9   REDEFINES WS-MAX-NSEQ  PIC 9(2).
       01  NSEQNQ-save            pic   s9(5).
       01  NSEQ-prod               PIC X(0002)  value '00' .
       01  NSEQNQ-prod             PIC S9(5)    COMP-3 value 0.
       01  NSEQENS-prod            PIC S9(5)    COMP-3 value 0.
       01  NLIGNE-prod             PIC X(0002)  value  '00'.
       01  W-MAX-NSEQ               PIC X(0002)  value '00' .
       01  WS-MAX-NSEQNQ            PIC S9(5)    COMP-3 value  0 .
       01  W-MAX-NSEQENS            PIC S9(5)    comp-3 value 0.
       01  W-MAX-NLIGNE             PIC X(0002)  value  '00'.
       01  LIGNE-PROD               PIC S9(5) COMP-3 VALUE 0.
       01  LIGNE-PROD2              PIC S9(5) COMP-3 VALUE 0.
       01  WS-QVENDUE               PIC S9(5) COMP-3 VALUE 0.
       01  WS-QVENDUE-DEBUT         PIC S9(5) COMP-3 VALUE 0.
       01  CPT-INS                  PIC S9(5) COMP-3 VALUE 0.
       01  CPT-QTE                  PIC S9(5) COMP-3 VALUE 0.
       01  WS-GV11-NSEQ             PIC x(2)        value '00'.
       01  WS-GV11-NSEQENS          PIC S9(5) COMP-3 value 0.
       01  WS-GV11-QVENDUE          PIC S9(5) COMP-3 value 0.
       01  W-CC-NSOC-ECOM           PIC X(003).
       01  W-CC-NLIEU-ECOM          PIC X(003).
       01  WS-DATE-MAX              PIC X(8)   VALUE SPACES.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  WS-DATE-MAX-F            PIC S9(04) COMP.
      *--
       01  WS-DATE-MAX-F            PIC S9(04) COMP-5.
      *}
       01  WS-NLIGNE             PIC X(2)        VALUE '00'.
       01  WS-NLIGNE9 REDEFINES  WS-NLIGNE    PIC 9(2).
       01  I-NSEQENS                 PIC 9(2).
       01 W-MNT-TOTAL               PIC S9(5)V99 PACKED-DECIMAL.
       01 W-MNT-GV11PLUS            PIC S9(5)V99 PACKED-DECIMAL.
       01 W-MNT-GV11MOINS           PIC S9(5)V99 PACKED-DECIMAL.
       01 WS-GV10-NVENTE-SAUVE     PIC X(7).
       01 WS-GV10-NSOCIETE-SAUVE   PIC X(3).
       01 WS-GV10-NLIEU-SAUVE      PIC X(3).
       01 CPT-TAB                  PIC S9(7) VALUE 0.
      *
      ******************************************************************
      * ZONES POUR GENERATION DES LIGNES DE VENTES...
      ******************************************************************
      ******************************************************************
       01  W-GV11-NSEQNQ         PIC S9(5) COMP-3.
       01  W-GV11-NCODIC         PIC X(7).
       01  CODIC-SAUV            PIC X(7).
      ******************************************************************
      * ...ZONES POUR MESSAGE DEBEDEUX...
      ******************************************************************
       01  MSG-SQL.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MSG-SQL-LENGTH PIC S9(4) COMP VALUE +240.
      *--
           02 MSG-SQL-LENGTH PIC S9(4) COMP-5 VALUE +240.
      *}
           02 MSG-SQL-MSG    PIC X(80) OCCURS 3.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  MSG-SQL-LRECL     PIC S9(8) COMP VALUE +80.
      *--
       01  MSG-SQL-LRECL     PIC S9(8) COMP-5 VALUE +80.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-SQL             PIC S9(4) BINARY.
      *--
       01  I-SQL             PIC S9(4) COMP-5.
      *}
       01  DSNTIAR           PIC X(8)  VALUE 'DSNTIAR'.
      ******************************************************************
      * ...ZONES DONT ON A BESOIN PARCE QU'IL LE FAUT BIEN...
      ******************************************************************
       01  W-MESSAGE-S.
           02  FILLER      PIC X(5) VALUE 'EC31'.
           02  W-MESSAGE-H PIC X(2).
           02  FILLER      PIC X(1) VALUE ':'.
           02  W-MESSAGE-M PIC X(2).
           02  FILLER      PIC X(1) VALUE ' '.
           02  W-MESSAGEs  PIC X(69).
      * INDICE POUR... INDICE, � UTILISER N'IMPORTE O�
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-I  PIC S9(4) COMP.
      *--
       01  I-I  PIC S9(4) COMP-5.
      *}
      * INDICE POUR NPRIORITE
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-P  PIC S9(4) COMP.
      *--
       01  I-P  PIC S9(4) COMP-5.
      *}
      * INDICES POUR DECALER LES LIGNES DE VENTE
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-DEC-DEBUT  PIC S9(4) COMP.
      *--
       01  I-DEC-DEBUT  PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-DEC-FIN    PIC S9(4) COMP.
      *--
       01  I-DEC-FIN    PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-DEC-NB     PIC S9(4) COMP.
      *--
       01  I-DEC-NB     PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-DEC-I      PIC S9(4) COMP.
      *--
       01  I-DEC-I      PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-DEC-SAVE   PIC S9(4) COMP.
      *--
       01  I-DEC-SAVE   PIC S9(4) COMP-5.
      *}
      * INDICES POUR DECALER LES LIGNES DE VENTE
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-MUL-DEBUT  PIC S9(4) COMP.
      *--
       01  I-MUL-DEBUT  PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-MUL-FIN    PIC S9(4) COMP.
      *--
       01  I-MUL-FIN    PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-MUL-NB     PIC S9(4) COMP.
      *--
       01  I-MUL-NB     PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-A          PIC S9(4) COMP.
      *--
       01  I-A          PIC S9(4) COMP-5.
      *}
      * POUR METTRE EN FORME LE NUM�RO DE COMMANDE DARTY.COM
       01  PIC9-NCDEWC   PIC Z(14)9.
       01  PICX-NCDEWC   REDEFINES PIC9-NCDEWC PIC X(15).
      * POUR GARDER DE LA NCGFC MECCC SI ON TRACE LES OPERATIONS
       01  FILLER           PIC X   VALUE 'N'.
       88  FAIS-DES-DISPLAYS        VALUE 'O'.
       01  W-NCDEWC         PIC  9(08)           VALUE ZEROES.
      ******************************************************************
      * ...DATE ET HEURE...
      ******************************************************************
           COPY COMMDATC.
       01  W-EIBTIME        PIC 9(7).
       01  FILLER REDEFINES W-EIBTIME.
           02  FILLER    PIC X.
           02  W-HEURE   PIC XX.
           02  W-MINUTE  PIC XX.
           02  W-SECONDE PIC XX.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  W-SPDATDB2-RC                 PIC S9(4)  COMP.
      *--
       01  W-SPDATDB2-RC                 PIC S9(4) COMP-5.
      *}
       01  W-SPDATDB2-DSYST              PIC S9(13) COMP-3 VALUE ZERO.
       01  w-nomprog     pic x(06).
           copy COMMBS42.
      ******************************************************************
      * ...POUR DECODER L'ADRESSE...
      ******************************************************************
       01  MEC09C.
           COPY MEC09C.
      ******************************************************************
      * ...POUR MEC15 (LOG DES COMMANDES)...
      ******************************************************************
       01  MEC15C-COMMAREA.
           COPY MEC15C.
           COPY MEC15E.
      ******************************************************************
      * ...POUR MEC00 (ENVOI A DARTY.COM)...
      ******************************************************************
           COPY MEC0MC.
      *    COPY MEC00C.
      ******************************************************************
      * ...POUR MGV65 (EXPEDITIONS)...
      ******************************************************************
      *01  MGV65 PIC X(8) VALUE 'MGV65'.
      *    COPY COMMGV65.
      ******************************************************************
      * ...POUR MBS43 (DUPLI DE VENTE)...
      ******************************************************************
           COPY COMMBS43.
           COPY COMMGV51.
      * ...ON ARRIVE FINALEMENT A LA COMMAREA.
      * (TAILLE DES TABLEAUX DE LA COMMAREA, TOUJOURS EN WORKING)
      ******************************************************************
      *    COPY COMMECCL.
      *  => Zones de travail utiles pour la gestion de TSTENVOI
           COPY WKTENVOI.
      ******************************************************************
      * POUR ECRIRE LA QUEUE POUR LA DUPLI DE VENTE (MBS70) - DEA
      ******************************************************************
           COPY COMMMQ21.
           COPY COMMMQ20.
      
      *   COPY MESSAGES MQ INTERROGATION STATUT VENTE LOCAL
      *
      *****************************************************************
      *
           10  WS-MESSAGE REDEFINES COMM-MQ20-MESSAGE.
      *---
           15  MES-ENTETE.
               20   MES-TYPE      PIC    X(3).
               20   MES-NSOCMSG   PIC    X(3).
               20   MES-NLIEUMSG  PIC    X(3).
               20   MES-NSOCDST   PIC    X(3).
               20   MES-NLIEUDST  PIC    X(3).
               20   MES-NORD      PIC    9(8).
               20   MES-LPROG     PIC    X(10).
               20   MES-DJOUR     PIC    X(8).
               20   MES-WSID      PIC    X(10).
               20   MES-USER      PIC    X(10).
               20   MES-CHRONO    PIC    9(7).
               20   MES-NBRMSG    PIC    9(7).
               20   MES-FILLER    PIC    X(30).
      *  DATA ENTETE : 1 OCCURENCE = 22 CAR
         15 MESSAGE45-DATA.
      *  SOCIETE ET LIEU DE DEMANDE DE LA VENTE
           25  MES60-NSOCD         PIC    X(3).
           25  MES60-NLIEUD        PIC    X(3).
      *  SOCIETE ET LIEU DE LA VENTE
           25  MES60-NSOCV         PIC    X(3).
           25  MES60-NLIEUV        PIC    X(3).
           25  MES60-NVENTE        PIC    X(7).
           25  MES60-TYPVTE        PIC    X(1).
      *  SOCIETE ET LIEU DE PAIEMENT DE LA VENTE
           25  MES60-NSOCP         PIC    X(3).
           25  MES60-NLIEUP        PIC    X(3).
      *  TOP ACCUSE RECEPTION O/N
           25  MES60-ACCUSE        PIC    X(1).
      *  ZONE CODE RETOUR = OK OU KO
           25  MES60-CRET          PIC    X(2).
      *    COPY COMMMQ12.
      
       77  WS-LONG-MESSAGE   PIC S9(03) VALUE +134.
      * --> DESCRIPTION DU MESSAGE DEA ENVOYE VERS LE 400 DE PAIEMENT.
           COPY MESBS70.
       LINKAGE SECTION.
       01  DFHCOMMAREA.
           COPY COMMEC31.
      *=================================================================
       PROCEDURE DIVISION.
      *=================================================================
      *{ normalize-exec-xx 1.5
      *    EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL) END-EXEC.
      *--
           EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL)
           END-EXEC.
      *}
           PERFORM MODULE-ENTREE.
      * recherche de la vente dans le courant
           MOVE W-MEC31-NSOC   TO gv11-nsociete
           MOVE W-MEC31-NLIEU  TO gv11-nlieu
           MOVE W-MEC31-NVENTE TO gv11-nvente
           exec sql select nvente
                      into :gv11-nvente
                      from rvgv1005
                     where nsociete = :gv11-nsociete
                       and nlieu    = :gv11-nlieu
                       and nvente   = :gv11-nvente
           end-exec.
           if sqlcode = +100
      *{ normalize-exec-xx 1.5
      *       exec cics assign program (w-nomprog) end-exec
      *--
              exec cics assign program (w-nomprog)
              end-exec
      *}
             move 'MBS42' to w-nomprog(1:5)
             MOVE GV11-NSOCIETE  TO COMM-BS42-NSOCIETE
             MOVE GV11-NLIEU     TO COMM-BS42-NLIEU
             MOVE GV11-NVENTE    TO COMM-BS42-NVENTE
             MOVE 'G'            TO COMM-BS42-TYPVTE
             MOVE 'MEC31'        TO COMM-BS42-PGAPPEL
             EXEC CICS LINK   PROGRAM   (w-nomprog)
                            COMMAREA  (COMM-BS42-APPLI)
                            LENGTH    (length of COMM-BS42-APPLI)
                            NOHANDLE
             END-EXEC
             if COMM-BS42-CODRET-ERREUR
      *{ normalize-exec-xx 1.5
      *         EXEC CICS ASKTIME NOHANDLE END-EXEC
      *--
                EXEC CICS ASKTIME NOHANDLE
                END-EXEC
      *}
                MOVE EIBTIME             TO W-EIBTIME
                MOVE W-HEURE             TO W-MESSAGE-H
                MOVE W-MINUTE            TO W-MESSAGE-M
                string
                GV11-NSOCIETE
                GV11-NLIEU
                GV11-NVENTE
                COMM-BS42-LIBERR
                delimited by size into w-message
                perform display-log
                move COMM-BS42-LIBERR to w-message
                perform display-log
                PERFORM MODULE-SORTIE
             end-if
           else
             if sqlcode not = 0
                string
                GV11-NSOCIETE
                GV11-NLIEU
                GV11-NVENTE
                COMM-BS42-LIBERR
                delimited by size into w-message
                perform display-log
                MOVE SQLCODE   TO WZ-XCTRL-EXPDEL-SQLCODE
                string
                'erreur db2 : '
                WZ-XCTRL-EXPDEL-SQLCODE
                delimited by size into w-message
                perform display-log
                PERFORM MODULE-SORTIE
             end-if
           end-if.
           PERFORM MODULE-TRAITEMENT.
           PERFORM MODULE-SORTIE.
      *=================================================================
      * INITIALISATION  DES DONNEES (DATES, ...)
      *
      ******************************************************************
       display-log section.
                EXEC CICS WRITEQ TD QUEUE ('CESO')
                          FROM (W-MESSAGE-S)
                          LENGTH (80) NOHANDLE
                END-EXEC.
       f-display-log. exit.
       MODULE-ENTREE SECTION.
           PERFORM CALL-SPDATDB2.
           MOVE '4'          TO GFDATA.
           PERFORM FAIS-UN-LINK-A-TETDATC.
           MOVE GFSAMJ-0 TO WS-DATE-DU-JOUR.
           MOVE ZERO TO W-MNT-TOTAL.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.
      *--
           EXEC CICS ASKTIME NOHANDLE
           END-EXEC.
      *}
           MOVE EIBTIME             TO W-EIBTIME.
           MOVE W-HEURE             TO W-MESSAGE-H.
           MOVE W-MINUTE            TO W-MESSAGE-M.
           PERFORM CALL-SPDATDB2.
           PERFORM RECH-MAG-INTERNET.
           MOVE '0' TO W-MEC31-CODE-RET
           MOVE SPACES TO TXTVA-CTABLEG2.
           MOVE 'MEC31' TO NCGFC-CTABLEG2.
           PERFORM SELECT-NCGFC.
           IF NCGFC-COMMENT(1:7) = 'DISPLAY'
              SET FAIS-DES-DISPLAYS TO TRUE
           END-IF.
           INITIALIZE MESSAGE70.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-MODULE-ENTREE. EXIT.
      *
      *-----------------------------------------------------------------
      *
      *=================================================================
      *==>  Traitement: Creation de la vente
       MODULE-TRAITEMENT SECTION.
      ****************************
      * W-PSE-R-U CONTIENT LE NOMBRE DE PSE A AJOUTER
           PERFORM VARYING W-PSE-R-I FROM 1 BY 1
           UNTIL W-PSE-R-I > W-PSE-R-U
              SET QTE-SUP-3-NON   TO TRUE
      *   Rechercher vente avec qte > 1 avec codic et nseqens
              PERFORM  SELECT-VTE-CODIC
              IF  WS-GV11-TROUVEE
      *  sauvegarde ligne produit
      *  ecriture produit avec PVTOTAL = PVUNIT,
      *qte = 1 , meme NSEQ , meme NSEQENS, nseqref = NSEQNQ, meme NLINE
                  MOVE    RVGV1106       TO   RVGV1106-B
                  IF      W-PSE-R-I      =    1
                          MOVE  GV11B-QVENDUE  TO   WS-QVENDUE-DEBUT
                  END-IF
                  COMPUTE GV11-QVENDUE   =    1
                  COMPUTE GV11-PVTOTAL   =    GV11-PVUNIT
                  MOVE    GV11-NSEQNQ    TO   GV11-NSEQREF
                  MOVE    GV11B-NSEQ     TO   NSEQ-PROD
                  MOVE    GV11B-NSEQNQ   TO   NSEQNQ-PROD
                  MOVE    GV11B-NSEQENS  TO   NSEQENS-PROD
      *           ADD     1              TO   NSEQENS-PROD
                  MOVE    GV11B-NLIGNE   TO   NLIGNE-PROD
                  PERFORM UPDATE-RTGV11-QTE
                  PERFORM DECALER-NLIGNE
      *  preparation ecriture PSE avec
      *  QTE = 1 , NSEQ  = NSEQ   DU PRODUIT
      *  NSEQREF         = NSEQNQ DU PRODUIT
                  MOVE    WS-GV11-NSEQ   TO WS-MAX-NSEQ
      *  Calcul du pvtotal des autres prod pour equilibre gv14 / gv11
                  COMPUTE RESTE-PVTOTAL = (GV11B-PVUNIT  *
                                       (WS-QVENDUE-DEBUT  - W-PSE-R-I))
                  SET QTE-SUP-3-OUI  TO TRUE
      * CONTROLE PAIEMENT  SUM-GV14   /   SUM-GV11
      *  SI LA LIGNE A MODIFIER EXISTE
      *  SI TOUTE LA VENTE EST TOPE
      *  SI LA LIGNE 'PSE' N'A PAS DEJA ETE CREE
      *  SI LA LIGNE 'PSE' EXISTE DANS GA52
                  PERFORM CONTROLES
      * CREATION de la PSE du produit si controles ok
                  IF W-MEC31-RET(W-PSE-R-I) = '00'
                     PERFORM SELECT-MAX-NSEQNQ
                     ADD     1  TO   WS-MAX-NSEQNQ
                     GIVING          GV11-NSEQNQ
                     PERFORM MAJ-VENTE
                     PERFORM SELECT-GV10-NSEQNQ
                     IF GV11-NSEQNQ > GV10-NSEQNQ
                        MOVE GV11-NSEQNQ     TO GV10-NSEQNQ
                        PERFORM MAJ-GV10-NSEQNQ
                     END-IF
                  END-IF
      * Ecriture de la ligne produit avec QTE   >  1
                  MOVE    RVGV1106-B   TO RVGV1106
                  COMPUTE GV11-QVENDUE =  GV11-QVENDUE - 1
                  COMPUTE GV11-PVTOTAL =  GV11-PVTOTAL -  GV11-PVUNIT
                  IF      GV11-QVENDUE =  1
                          COMPUTE GV11-PVTOTAL    =  GV11-PVUNIT
                  END-IF
                  PERFORM  SELECT-MAX-NSEQ
                  PERFORM  SELECT-MAX-NSEQENS
                  PERFORM  SELECT-MAX-NSEQNQ
                  PERFORM  SELECT-MAX-NLIGNE
                  MOVE W-MAX-NSEQ      TO  WS-MAX-NSEQ2
                  ADD  1               TO  WS-MAX-NSEQ29
                  MOVE W-MAX-NLIGNE    TO  WS-GV11-NLI
                  ADD  1               TO  WS-GV11-NLI9
                  MOVE WS-MAX-NSEQ2    TO  W-MAX-NSEQ
                  MOVE WS-MAX-NSEQNQ   TO  GV11-NSEQNQ
                  MOVE WS-MAX-NSEQNQ   TO  NSEQNQ-PROD
                  ADD  1               TO  NSEQNQ-PROD
                  ADD  1               TO  GV11-NSEQNQ
                  MOVE GV11-NSEQNQ     TO  GV11-NSEQREF
                  ADD  1               TO  W-MAX-NSEQENS
                  MOVE WS-GV11-NLI     TO  W-MAX-NLIGNE
                  MOVE W-MAX-NSEQ      TO  GV11-NSEQ      NSEQ-PROD
                  MOVE W-MAX-NSEQENS   TO  GV11-NSEQENS   NSEQENS-PROD
                  MOVE W-MAX-NLIGNE    TO  GV11-NLIGNE    NLIGNE-PROD
                  PERFORM INSERT-RTGV11
                  PERFORM SELECT-GV10-NSEQNQ
                  IF GV11-NSEQNQ > GV10-NSEQNQ
                        MOVE GV11-NSEQNQ     TO GV10-NSEQNQ
                        PERFORM MAJ-GV10-NSEQNQ
                  END-IF
                  PERFORM SELECT-RTEC02
                  if sqlcode = 0
                     INITIALIZE MEC15C-COMMAREA
                     MOVE EC02-NCDEWC       TO MEC15C-NCDEWC
                     MOVE W-MEC31-NSOC      TO MEC15C-NSOCIETE
                     MOVE W-MEC31-NLIEU     TO MEC15C-NLIEU
                     MOVE W-MEC31-NVENTE    TO MEC15C-NVENTE
                     MOVE GV11-NSEQNQ       TO MEC15C-NSEQNQ
                     MOVE 'I'               TO MEC15C-STAT
                     MOVE SPACE          TO MEC15C-TEXTE
                     EXEC CICS START TRANSID ('EC15')
                     FROM (MEC15C-COMMAREA)
                     END-EXEC
                  end-if
                  MOVE RVGV1106        TO RVGV1106-B
                  MOVE W-MEC31-NCODIC(W-PSE-R-I)   TO CODIC-SAUV
              ELSE
      * Ecriture ligne PSE du Prod  QTE  =  1  en fin de decomposition
                  IF  W-MEC31-NCODIC(W-PSE-R-I)  =  CODIC-SAUV
                       ADD  2             TO  WS-MAX-NSEQNQ
      *                ADD  2             TO  W-MAX-NSEQENS
                       MOVE GV11-NSEQ     to  NSEQ-PROD
                       MOVE WS-MAX-NSEQNQ TO  GV11-NSEQNQ
      *                MOVE W-MAX-NSEQENS TO  GV11-NSEQENS NSEQENS-PROD
                       MOVE GV11B-NSEQENS TO  NSEQENS-PROD
                       MOVE W-MAX-NLIGNE  TO  WS-GV11-NLI9
                       ADD  1             TO  WS-GV11-NLI9
                       MOVE W-MAX-NLIGNE  TO  GV11-NLIGNE  NLIGNE-PROD
                       PERFORM MAJ-VENTE
                       PERFORM SELECT-GV10-NSEQNQ
                       IF GV11-NSEQNQ > GV10-NSEQNQ
                        MOVE GV11-NSEQNQ     TO GV10-NSEQNQ
                        PERFORM MAJ-GV10-NSEQNQ
                       END-IF
                  ELSE
                     PERFORM CONTROLES
                     IF W-MEC31-RET(W-PSE-R-I) = '00'
                         move gv11-nseqnq     to nseqnq-prod
                         move gv11-nseq       to nseq-prod
                         move gv11-nligne     to nligne-prod
                         PERFORM SELECT-MAX-NSEQNQ
                         ADD 1                TO WS-MAX-NSEQNQ
                         giving gv11-nseqnq
      *                  PERFORM  SELECT-MAX-NSEQENS
      *                  ADD  1               TO  W-MAX-NSEQENS
      *                  MOVE W-MAX-NSEQENS   TO  NSEQENS-prod
                         MOVE GV11-NSEQENS    TO  NSEQENS-prod
                         PERFORM DECALER-NLIGNE
                         PERFORM MAJ-VENTE
                         PERFORM SELECT-GV10-NSEQNQ
                         IF GV11-NSEQNQ > GV10-NSEQNQ
                            MOVE GV11-NSEQNQ     TO GV10-NSEQNQ
                            PERFORM MAJ-GV10-NSEQNQ
                         END-IF
                     END-IF
                  END-IF
              END-IF
           END-PERFORM.
           IF W-MEC31-CODE-RET = '0'
              PERFORM CREATION-RTEC32
              PERFORM ENVOI-MESSAGES
           END-IF.
       FIN-MODULE-TRAITEMENT. EXIT.
      *
      *=================================================================
       MODULE-SORTIE SECTION.
      * ON A EUT UN PROBLEME SUR AU MOINS 1 DES PSE, DONC ON REJETTE
      * TOUTE LA DEMANDE, CEPANDANT, POUR LES PSE QUI AVAIENT ETE
      * BIEN TRAITES (CODE '00') ON MET LE CODE '90' POUR INDIQUER
      * A DARTY.COM QUE QUE POUR CE PSE LA DEMANDE ETAIT VALIDE MAIS
      * QU'ON LA PLANTE A CAUSE D'UN AUTRE PROBLEME
           IF W-MEC31-CODE-RET NOT = '0'
              PERFORM VARYING W-PSE-R-I FROM 1 BY 1
              UNTIL W-PSE-R-I > W-PSE-R-U
                 IF W-MEC31-RET(W-PSE-R-I) = '00'
                    MOVE '90' TO W-MEC31-RET(W-PSE-R-I)
                 END-IF
              END-PERFORM
           END-IF.
              STRING 'sortie ec31 ' GV11-NVENTE
              ' ' W-MEC31-CODE-RET
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
      * ENCAISSEMENT reponse darty.com
           PERFORM ENVOI-REPONSE.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS RETURN END-EXEC.
      *--
           EXEC CICS RETURN
           END-EXEC.
      *}
       FIN-MODULE-SORTIE. EXIT.
       CONTROLES SECTION.
      ********************
           MOVE W-MEC31-NSOC                 TO     GV11-NSOCIETE
           MOVE W-MEC31-NLIEU                TO     GV11-NLIEU
           MOVE W-MEC31-NVENTE               TO     GV11-NVENTE
           MOVE W-MEC31-NCODIC(W-PSE-R-I)    TO     GV11-NCODIC
           MOVE W-MEC31-NSEQNQ(W-PSE-R-I)    TO     GV11-NSEQNQ
           MOVE W-MEC31-NSEQNQ(W-PSE-R-I)    TO     GV11-NSEQ
           MOVE '00' TO W-MEC31-RET(W-PSE-R-I)
      *    IF W-MEC31-CODE-RET = '0' AND W-PSE-R-I = 1
      *       PERFORM CONTROL-PAIEMENT
      *       IF PAIEMENT-KO
      *          MOVE '04' TO W-MEC31-RET(W-PSE-R-I)
      *          MOVE '9' TO W-MEC31-CODE-RET
      *          MOVE   'CONTROLE PAIEMENT KO' TO W-MESSAGES
      *          PERFORM DISPLAY-W-MESSAGE
      *          GO TO FIN-CONTROLES
      *       END-IF
      *    END-IF
      * ON CHERCHE SI LA LIGNE A MODIFIER EXISTE
           PERFORM SELECT-RTGV11
           IF W-MEC31-CODE-RET NOT = '0'
              GO TO FIN-CONTROLES
           END-IF
           IF W-MEC31-CODE-RET = '0'
              IF GV11-DANNULATION > '        '
                 MOVE '02' TO W-MEC31-RET(W-PSE-R-I)
                 MOVE '9' TO W-MEC31-CODE-RET
      *          MOVE   'VENTE ANNUL�E' TO W-MESSAGES
                 STRING 'VENTE ANNUL�E ' W-MEC31-NSOC
                                       ' ' W-MEC31-NLIEU
                                       ' ' W-MEC31-NVENTE
                 DELIMITED BY SIZE INTO W-MESSAGEs
                 PERFORM DISPLAY-W-MESSAGE
                 GO TO FIN-CONTROLES
              END-IF
           END-IF
      * ON CHERCHE SI TOUTE LA VENTE EST TOPE
      *    PERFORM SELECT-RTGV11-TOPE
      *    IF W-MEC31-CODE-RET NOT = '0'
      *       GO TO FIN-CONTROLES
      *    END-IF
      * ON CHERCHE SI LA LIGNE 'PSE' N'A PAS DEJA ETE CREE
           PERFORM SELECT-PSE
           IF WS-GV11-TROUVEE
              MOVE '03' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
              MOVE   'il y a deja une PSE' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              GO TO FIN-CONTROLES
           END-IF
           IF W-MEC31-CODE-RET NOT = '0'
              MOVE '94' TO W-MEC31-RET(W-PSE-R-I)
              GO TO FIN-CONTROLES
           END-IF
      * ON CHERCHE SI LA LIGNE 'PSE' existe dans ga52
           PERFORM SELECT-GA52
           IF SQLCODE = +100
              MOVE '05' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
              MOVE   'ga52 non trouve    ' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              GO TO FIN-CONTROLES
           END-IF
      * ON CHERCHE SI LA LIGNE 'PSE' EXISTE DANS GA40
           PERFORM SELECT-GA40
           IF SQLCODE = +100
              MOVE '06' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
              MOVE   'ga40 non trouve    ' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              GO TO FIN-CONTROLES
           END-IF
      * ON REFAIT L'ACCES A GV11 POUR PR�PARER LA CR�ATION DE LA 'PSE'
           PERFORM SELECT-RTGV11
           IF WS-GV11-TROUVEE
              MOVE    GV11-QVENDUE  TO WS-GV11-QVENDUE
              PERFORM SELECT-CVENDEUR
              IF W-MEC31-CODE-RET NOT = '0'
                 MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              ELSE
                 IF WS-GV31-NON-TROUVEE
                    MOVE '009440' TO GV11-CVENDEUR
                 ELSE
                    MOVE GV31-CVENDEUR TO GV11-CVENDEUR
                 END-IF
              END-IF
           END-IF.
       FIN-CONTROLES. EXIT.
      ******************************************************************
      * CREATION DE LA LIGNE DE PSE ; MAJ DE LA VENTE + TABLES EC
       MAJ-VENTE SECTION.
             PERFORM CREATION-LIGNE-PSE
             IF W-MEC31-RET(W-PSE-R-I) = '00'
                PERFORM MAJ-RTGV10
             END-IF.
             IF W-MEC31-RET(W-PSE-R-I) = '00'
                PERFORM INSERT-RTEC10
                PERFORM CREE-MOUCHARD-LIGNE
             END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-MAJ-VENTE. EXIT.
      * ON VERIFIE QU'IL N'Y A PAS D'�CART ENTRE GV11 ET GV14
       CONTROL-PAIEMENT SECTION.
             SET PAIEMENT-OK TO TRUE
             PERFORM SELECT-SUM-GV14
             PERFORM SELECT-SUM-GV11
             IF GV14-PREGLTVTE NOT = GV11-PVTOTAL
                SET PAIEMENT-KO TO TRUE
                MOVE GV11-PVTOTAL TO EDIT-GV11
                MOVE GV14-PREGLTVTE TO EDIT-GV14
              STRING 'ECART PAIEMENT POUR VENTE ' GV11-NVENTE
              ' ' EDIT-GV11 ' ' EDIT-GV14
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
             END-IF.
      *{ Tr-Empty-Sentence 1.6
      *      .
      *}
       FIN-CONTROL-PAIEMENT. EXIT.
      * CREATION DE LA LIGNE DE PSE
       CREATION-LIGNE-PSE SECTION.
      * ON A LU LA LIGNE DE RTGV11 QUI S'Y RAPPORTE ; ON VA SIMPLEMENT
      * MODIFIER LES ZONES NECESSAIRES POUR PREPARER L'INSERT
      *
      *      MOVE GV11-NSEQNQ      TO GV11-NSEQREF
             MOVE NSEQNQ-PROD      TO GV11-NSEQREF
             MOVE '3'              TO GV11-CTYPENREG
             MOVE W-MEC31-NCDEWC   TO W-NCDEWC
      *      MOVE W-NCDEWC         TO GV11-LCOMMENT(27:9)
             MOVE W-NCDEWC         TO GV11-CTOURNEE
             IF W-MEC31-NUM-PS(W-PSE-R-I)  > '  '
                MOVE W-MEC31-NUM-PS(W-PSE-R-I) TO GV11-LCOMMENT(1:8)
             ELSE
                MOVE GV11-NVENTE   TO GV11-LCOMMENT(1:7)
                MOVE '1'           TO GV11-LCOMMENT(8:1)
             END-IF
      *      PERFORM SELECT-MAX-NSEQNQ
      *      ADD 1                 TO GV11-NSEQNQ
      *      PERFORM DECALER-NLIGNE
      *      MOVE    ????          TO GV11-NSEQ
             MOVE NSEQ-PROD        TO GV11-NSEQ
             MOVE WS-DATE-DU-JOUR  TO GV11-DCREATION
             STRING W-HEURE W-MINUTE DELIMITED BY SIZE
               INTO GV11-HCREATION
             MOVE W-SPDATDB2-DSYST TO GV11-DSYST
             MOVE    SPACES        TO GV11-DATENC
             MOVE    SPACES        TO GV11-WEMPORTE
             MOVE    SPACES        TO GV11-CEQUIPE
             MOVE    SPACES        TO GV11-CPOSTAL
             MOVE    SPACES        TO GV11-DTOPE
             MOVE    SPACES        TO GV11-Dcompta
             MOVE    SPACES        TO GV11-dstat
             MOVE    SPACES        TO GV11-NSOCLIVR1
             MOVE    SPACES        TO GV11-NDEPOT1
             MOVE    0             TO GV11-QCONDT
             MOVE  W-CC-NSOC-ECOM  TO GV11-NSOCP
             MOVE  W-CC-NLIEU-ECOM TO GV11-NLIEUP    GV11-NLIEUMODIF
             MOVE    SPACE         TO GV11-CDEV
             MOVE    0             TO GV11-NTRANS
             MOVE    'PA'          TO GV11-CTYPENT
             MOVE    'N'           TO GV11-WTOPELIVRE
             MOVE  W-CC-NLIEU-ECOM TO GV11-NDEPOT
      *      MOVE    W-MEC31-NLIEU TO GV11-NDEPOT
             MOVE    W-MEC31-C-PSE(W-PSE-R-I)    TO GV11-CENREG
             MOVE    W-MEC31-PMONTANT(W-PSE-R-I) TO GV11-PVUNIT
                       GV11-PVUNITF GV11-PVTOTAL
      *      MOVE    W-MEC31-C-VEND(W-PSE-R-I) TO GV11-CVENDEUR
                 MOVE NLIGNE-PROD      TO WS-GV11-NLI
                 ADD            1      TO WS-GV11-NLI9
                 MOVE WS-GV11-NLI9     TO GV11-NLIGNE
                 MOVE NSEQENS-prod     TO GV11-NSEQENS
                 MOVE NSEQ-prod        TO GV11-NSEQ
                PERFORM INSERT-RTGV11
                PERFORM SELECT-GV10-NSEQNQ
                IF GV11-NSEQNQ > GV10-NSEQNQ
                        MOVE GV11-NSEQNQ     TO GV10-NSEQNQ
                        PERFORM MAJ-GV10-NSEQNQ
                END-IF
                IF W-MEC31-CODE-RET = '0'
                   PERFORM CRE-PSE-RTPS00
                END-IF
                IF W-MEC31-CODE-RET = '0'
                   COMPUTE W-MNT-TOTAL = W-MNT-TOTAL +
                           W-MEC31-PMONTANT(W-PSE-R-I)
                END-IF
      *         PERFORM INSERT-RTGV11
      *         IF W-MEC31-CODE-RET = '0' and  WS-GV11-NLIGNE-SAUV = 1
      *            PERFORM CRE-PSE-RTPS00
      *         END-IF
      *         IF W-MEC31-CODE-RET = '0'
      *            COMPUTE W-MNT-TOTAL = W-MNT-TOTAL +
      *                    W-MEC31-PMONTANT(W-PSE-R-I)
      *         END-IF
      *      END-IF.
           .
       FIN-CREATION-LIGNE-PSE. EXIT.
       CRE-PSE-RTPS00                   SECTION.
      *----------------------------------------
      *
            MOVE GV11-NSOCIETE       TO PS00-NSOC
            MOVE GV11-CENREG         TO PS00-CGCPLT
            MOVE GV11-NLIEU          TO PS00-NLIEU
            MOVE GV11-NVENTE         TO PS00-NVENTE
            MOVE WS-DATE-DU-JOUR     TO PS00-DDELIV
            MOVE GV11-DCOMPTA        TO PS00-DCOMPTA
            IF PS00-NGCPLT NOT > SPACES
               MOVE GV11-LCOMMENT(1:8) TO PS00-NGCPLT
            END-IF.
            MOVE GV11-NCODIC         TO PS00-NCODIC
            MOVE GV11-PVTOTAL        TO PS00-MTGCPLT
            MOVE SPACES              TO PS00-DANNUL
            MOVE SPACES              TO PS00-DRACHAT
            MOVE 0                   TO PS00-MTREMB
            MOVE SPACES              TO PS00-DREMB
            MOVE 0                   TO PS00-MTRACHAT
            MOVE GV11-DCREATION      TO PS00-DTRAIT
            MOVE SPACES              TO PS00-CINSEE
            MOVE SPACES              TO PS00-NOMCLI
            MOVE W-SPDATDB2-DSYST    TO PS00-DSYST
            MOVE '99999999'          TO PS00-DTRAITR
            MOVE GV11-NSOCIETE       TO GV02-NSOCIETE
            MOVE GV11-NLIEU          TO GV02-NLIEU
            MOVE GV11-NVENTE         TO GV02-NVENTE
            PERFORM SELECT-RTGV02
            IF W-MEC31-CODE-RET = '0'
               MOVE GV02-CINSEE TO PS00-CINSEE
               MOVE GV02-LNOM    TO PS00-NOMCLI
               PERFORM INSERT-RTPS00
            END-IF.
      *
       FIN-CRE-PSE-RTPS00. EXIT.
      * ATTRIBUER LE NLIGNE : NLIGNE(PRODUIT) + 1
      * ET DECALER LES NLIGNES >
       DECALER-NLIGNE SECTION.
      * SAUVEGARDER LE NLIGNE DU PRODUIT
             MOVE NLIGNE-PROD  TO WS-GV11-NLIGNE-SAUV
      * ON DECALE LES NLIGNE >
             PERFORM DECLARE-OPEN-RTGV11
             PERFORM FETCH-RTGV11
             PERFORM UNTIL SQLCODE = 100
               IF GV11-NLIGNE >  WS-GV11-NLIGNE-SAUV
                     MOVE GV11-NLIGNE  TO WS-GV11-NLIGNE-PICX
                     ADD 1 TO WS-GV11-NLIGNE-PIC9
                     MOVE WS-GV11-NLIGNE-PICX TO GV11-NLIGNE
                     PERFORM UPDATE-RTGV11
               END-IF
               PERFORM FETCH-RTGV11
             END-PERFORM
             PERFORM CLOSE-RTGV11
      * MAINTENANT ON CALCULE LE NLIGNE DE LA PSE
      *      MOVE WS-GV11-NLIGNE-SAUV TO WS-GV11-NLIGNE-PICX
      *      ADD 1 TO WS-GV11-NLIGNE-PIC9
      *      MOVE WS-GV11-NLIGNE-PICX TO GV11-NLIGNE
           .
       FIN-DECALER-NLIGNE. EXIT.
      * MISE A JOUR DE L'ENTETE
       MAJ-RTGV10 SECTION.
      * ON AJOUTE LE MONTANT DE LA PSE AU TOTAL DE LA VENTE
             MOVE W-SPDATDB2-DSYST         TO GV10-DSYST
             PERFORM UPDATE-RTGV10
           .
       FIN-MAJ-RTGV10. EXIT.
      * ENVOI DES MESSAGES
      *   DUPLI MBS43 - MGV51
      *   DEMANDE ENCAISSEMENT
      *   RETOUR A DARTY.COM
       ENVOI-MESSAGES SECTION.
           PERFORM SELECT-GV10
           PERFORM APPEL-MBS43.
      
      *    INITIALIZE COMM-MBS43-APPLI
      *    MOVE GV10-NSOCIETE       TO COMM-MBS43-NSOCIETE
      *    MOVE GV10-NSOCP          TO COMM-MBS43-NSOCP
      *    MOVE W-CC-NSOC-ECOM      TO COMM-MBS43-NSOCP
      *                                COMM-MBS43-NSOCMODIF
      *    MOVE GV10-NLIEU          TO COMM-MBS43-NLIEU
      *    MOVE GV10-NLIEUP         TO COMM-MBS43-NLIEUP
      *    MOVE W-CC-NLIEU-ECOM     TO COMM-MBS43-NLIEUP
      *                                COMM-MBS43-NLIEUMODIF
      *    MOVE GV10-NVENTE         TO COMM-MBS43-NVENTE
      *    MOVE ' '                 TO COMM-MBS43-MODIF-CTYPLIEU
      *    MOVE GV10-TYPVTE         TO COMM-MBS43-TYPVTE
      *    MOVE 'EXPEDITIO'         TO COMM-MBS43-CIMPRIM
      *    MOVE LENGTH OF COMM-MBS43-APPLI TO LONGUEUR-COMMAREA
      *    EXEC CICS LINK   PROGRAM   ('MBS43')
      *                     COMMAREA  (COMM-MBS43-APPLI)
      *                     LENGTH    (LONGUEUR-COMMAREA)
      *                     NOHANDLE
      *    END-EXEC.
      *
      *    MOVE EIBRCODE TO EIB-RCODE.
      *    IF NOT EIB-NORMAL
      *       STRING 'LINK MODULE MBS43 MAL TERMINE '
      *       DELIMITED BY SIZE INTO W-MESSAGEs
      *       PERFORM DISPLAY-W-MESSAGE
      *       MOVE '98' TO W-MEC31-RET(W-PSE-R-I)
      *       MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM MODULE-SORTIE
      *    END-IF.
      *
           IF W-MEC31-CODE-RET = '0'
             MOVE GV10-NSOCIETE       TO COMM-MGV51-NSOCIETE
             MOVE GV10-NLIEU          TO COMM-MGV51-NLIEU
             MOVE GV10-NVENTE         TO COMM-MGV51-NVENTE
             MOVE WS-DATE-DU-JOUR     TO COMM-MGV51-DJOUR
             MOVE SPACES              TO COMM-MGV51-CCEVENT
             MOVE LENGTH OF COMM-MGV51-APPLI TO LONGUEUR-COMMAREA
             EXEC CICS LINK   PROGRAM   ('MGV51')
                            COMMAREA  (COMM-MGV51-APPLI)
                            LENGTH    (LONGUEUR-COMMAREA)
                            NOHANDLE
             END-EXEC
             MOVE EIBRCODE TO EIB-RCODE
             IF NOT EIB-NORMAL
                STRING 'LINK MODULE MGV51 MAL TERMINE '
                DELIMITED BY SIZE INTO W-MESSAGEs
                PERFORM DISPLAY-W-MESSAGE
      *         MOVE '97' TO W-MEC31-RET(W-PSE-R-I)
                MOVE '9' TO W-MEC31-CODE-RET
      *         PERFORM ABANDON-ERREUR
                PERFORM MODULE-SORTIE
             END-IF
           END-IF.
      * ENCAISSEMENT AUTOMATIQUE
           PERFORM ENVOI-MESSAGE-DEA.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-ENVOI-MESSAGES. EXIT.
       APPEL-MBS43  SECTION.
      ***********************
      *
      * POUR TRAITER LES DEMANDES DES FILIALES, ON NE PEUX PAS APPELER
      * DIRECTEMENT LE MBS43, ON PASSE DONC PAR UN MASSAGE MQ ENVOY�
      * AU MBS60 QUI CE CHARGERA D'APPELER LE MBS43 EN LOCAL
      *
      
           INITIALIZE WS-MESSAGE
                      COMM-MQ20-MESSAGE.
           MOVE 'IV1'               TO       MES-TYPE.
           MOVE W-CC-NSOC-ECOM      TO       MES-NSOCMSG
           MOVE W-CC-NLIEU-ECOM     TO       MES-NLIEUMSG
           MOVE GV10-NSOCIETE       TO       MES-NSOCDST
           MOVE '000'               TO       MES-NLIEUDST
           MOVE SPACES              TO       MES-WSID
                                             MES-USER
           MOVE 'TEC02'             TO       MES-LPROG
           MOVE ZEROES              TO       MES-NORD
           MOVE 1                   TO       MES-CHRONO
                                             MES-NBRMSG
           MOVE SPACES              TO       MES-FILLER
      
      *    partie data
           MOVE W-CC-NSOC-ECOM      TO      MES60-NSOCD.
           MOVE W-CC-NLIEU-ECOM     TO      MES60-NLIEUD
           MOVE GV10-NSOCIETE       TO      MES60-NSOCV.
           MOVE GV10-NLIEU          TO      MES60-NLIEUV
      *    MOVE W-CC-NSOC-ECOM      TO      MES60-NSOCV.
      *    MOVE W-CC-NLIEU-ECOM     TO      MES60-NLIEUV
           MOVE GV10-NVENTE         TO      MES60-NVENTE
           MOVE '4'                 TO      MES60-TYPVTE
           MOVE W-CC-NSOC-ECOM      TO      MES60-NSOCP.
           MOVE W-CC-NLIEU-ECOM     TO      MES60-NLIEUP
           MOVE 'N'                 TO      MES60-ACCUSE
           MOVE 'OK'                TO      MES60-CRET
           PERFORM ENVOI-DUPLI.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-APPEL-MBS43. EXIT.
       ENVOI-MESSAGE-DEA SECTION.
      * -> ZONES MESSAGE.
           MOVE 'DEA'              TO MES70-TYPE
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCMSG
           MOVE '000'              TO MES70-NLIEUMSG
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCDST
           MOVE W-CC-NLIEU-ECOM    TO MES70-NLIEUDST
           MOVE ZERO               TO MES70-NORD
           MOVE 'MEC31'            TO MES70-LPROG
           MOVE WS-DATE-DU-JOUR    TO MES70-DJOUR
           MOVE SPACES             TO MES70-WSID
           MOVE SPACES             TO MES70-USER
           MOVE 1                  TO MES70-CHRONO
           MOVE 1                  TO MES70-NBRMSG
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCD
           MOVE W-CC-NLIEU-ECOM    TO MES70-NLIEUD
           MOVE 1                  TO MES70-NBVTES
           MOVE 1                  TO MES70-NBMODP
      *    MOVE W-MEC31-PMONTANT(W-PSE-R-I) TO MES70-PCOMPT
      *    MOVE W-MEC31-PMONTANT(W-PSE-R-I) TO GV11-PVTOTAL
      *    MOVE GV10-PTTVENTE      TO MES70-PCOMPT
      *    MOVE W-MEC31-PMONTANT(W-PSE-R-I) TO MES70-PCOMPT
           MOVE W-MNT-TOTAL        TO MES70-PCOMPT
           MOVE 'EUR'              TO MES70-CDEVISE
      *    MOVE 0                  TO MES70-PCOMPT
      *    PERFORM VARYING W-PSE-R-I FROM 1 BY 1
      *    UNTIL W-PSE-R-I > W-PSE-R-U
      *       COMPUTE MES70-PCOMPT = MES70-PCOMPT
      *                 + W-MEC31-PMONTANT(W-PSE-R-I)
      *    END-PERFORM
           MOVE GV10-NSOCIETE      TO MES70-NSOCV  (1)
           MOVE GV10-NLIEU         TO MES70-NLIEUV (1)
           MOVE GV10-NVENTE        TO MES70-NVENTE (1)
           MOVE GV10-TYPVTE        TO MES70-TYPVTE (1)
           MOVE GV10-DVENTE        TO MES70-DVENTE (1)
      *    MOVE GV10-NSOCP         TO MES70-NSOCP
      *    MOVE GV10-NLIEUP        TO MES70-NLIEUP
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCP
           MOVE W-CC-NLIEU-ECOM    TO MES70-NLIEUP
           MOVE W-MEC31-MD-PMENT   TO EC008-CTABLEG2
           PERFORM SELECT-EC008
           MOVE EC008-MOPAISI      TO MES70-CMOPAI(1)
      *    MOVE '3'                TO MES70-CMOPAI (1)
           MOVE MES70-PCOMPT       TO MES70-PMOPAI (1)
      *    MOVE W-MEC31-PMONTANT(W-PSE-R-I) TO MES70-PMOPAI (1)
           MOVE 'EUR'              TO MES70-CDEV    (1)
           MOVE SPACES             TO MES70-DESC    (1)
           MOVE 'O'                TO MES70-DUP-OK
      * POUR INDIQUER ENCAISST P.A. ET DONC LAISSER PASSER
           MOVE 'P'                TO MES70-ENC-OK
      * -> ZONES MQ.
           MOVE SPACES             TO COMM-MQ21-MSGID.
           MOVE SPACES             TO COMM-MQ21-CORRELID.
           MOVE SPACES             TO COMM-MQ21-CODRET.
           MOVE SPACES             TO COMM-MQ21-ERREUR.
           MOVE 'DEA'              TO COMM-MQ21-FONCTION.
           MOVE 'MEC31'            TO COMM-MQ21-NOMPROG.
           MOVE 'N'                TO COMM-MQ21-SYNCPOINT.
           MOVE 7                  TO COMM-MQ21-PRIORITE.
      *    MOVE GV10-NSOCP         TO COMM-MQ21-NSOC.
      *    MOVE GV10-NLIEUP        TO COMM-MQ21-NLIEU.
           MOVE W-CC-NSOC-ECOM     TO COMM-MQ21-NSOC.
           MOVE W-CC-NLIEU-ECOM    TO COMM-MQ21-NLIEU.
           MOVE 650                TO COMM-MQ21-LONGMES.
           MOVE MESSAGE70          TO COMM-MQ21-MESSAGE.
           COMPUTE LONGUEUR-COMMAREA  = COMM-MQ21-LONGMES + 124.
      *    MOVE COMM-MQ21-APPLI    TO Z-COMMAREA-LINK.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS
      *         LINK PROGRAM ('MMQ21') COMMAREA (COMM-MQ21-APPLI)
      *         LENGTH(LONGUEUR-COMMAREA)
      *         NOHANDLE
      *    END-EXEC.
      *--
           EXEC CICS LINK PROGRAM ('MMQ21') COMMAREA (COMM-MQ21-APPLI)
                LENGTH(LONGUEUR-COMMAREA)
                NOHANDLE
           END-EXEC.
      *}
           MOVE EIBRCODE TO EIB-RCODE
           IF NOT EIB-NORMAL
               STRING 'LINK MODULE MMQ21 MAL TERMINE '
               DELIMITED BY SIZE INTO W-MESSAGEs
               PERFORM DISPLAY-W-MESSAGE
      *        MOVE '96' TO W-MEC31-RET(W-PSE-R-I)
               MOVE '9' TO W-MEC31-CODE-RET
      *        PERFORM ABANDON-ERREUR
               PERFORM MODULE-SORTIE
           ELSE
               STRING 'DEA ENVOYE POUR VENTE ' GV10-NVENTE
               DELIMITED BY SIZE INTO W-MESSAGEs
               PERFORM DISPLAY-W-MESSAGE
           END-IF.
       F-ENVOI-MESSAGE-DEA. EXIT.
       ENVOI-REPONSE SECTION.
           INITIALIZE              MEC00C-COMMAREA
           MOVE 'CMD-MOD1'      TO MEC00C-FICHIER-SI
           MOVE 'D'             TO MEC00C-CODE-ACTION
      *    MOVE 'D'             TO MEC00C-CODE-MAJ
           MOVE W-MEC31-ENTETE  TO MEC00C-DATA-SI
           MOVE LENGTH OF W-MEC31-ENTETE TO MEC00C-DATA-SI-L
           EXEC CICS LINK PROGRAM ('MEC00P') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
           PERFORM VARYING W-PSE-R-I FROM 1 BY 1
           UNTIL W-PSE-R-I > W-PSE-R-U
           OR W-MEC31-NCODIC(W-PSE-R-I) = SPACE
              MOVE 'CMD-MOD2'      TO MEC00C-FICHIER-SI
              MOVE 'T'             TO MEC00C-CODE-ACTION
      *       MOVE 'T'             TO MEC00C-CODE-MAJ
              MOVE W-MEC31-PSE(W-PSE-R-I) TO MEC00C-DATA-SI
              MOVE LENGTH OF W-MEC31-PSE(W-PSE-R-I)
                                          TO MEC00C-DATA-SI-L
              EXEC CICS LINK PROGRAM ('MEC00P')
                COMMAREA (MEC00C-COMMAREA)
              END-EXEC
           END-PERFORM
           IF MEC00C-CODE-RETOUR NOT = '0'
              MOVE SPACES           TO W-MESSAGE
              STRING 'MEC31-REP-T:'
                     ' MEC00 CDRET='  MEC00C-CODE-RETOUR
                     ' SQL='          MEC00C-CODE-DB2-DISPLAY
                     ' MESS='         MEC00C-MESSAGE
                DELIMITED BY SIZE
                INTO W-MESSAGE
              END-STRING
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
           MOVE 'CMD-MOD1'      TO MEC00C-FICHIER-SI
           MOVE 'F'             TO MEC00C-CODE-ACTION
      *    MOVE 'F'             TO MEC00C-CODE-MAJ
           MOVE SPACE           TO MEC00C-DATA-SI
           MOVE LENGTH OF W-MEC31-ENTETE TO MEC00C-DATA-SI-L
           EXEC CICS LINK PROGRAM ('MEC00P') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
           IF MEC00C-CODE-RETOUR NOT = '0'
              MOVE SPACES           TO W-MESSAGE
              STRING 'MEC31-REP-F:'
                     ' MEC00 CDRET='  MEC00C-CODE-RETOUR
                     ' SQL='          MEC00C-CODE-DB2-DISPLAY
                     ' MESS='         MEC00C-MESSAGE
                DELIMITED BY SIZE
                INTO W-MESSAGE
              END-STRING
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
           SET MEC00C-FIN-TRAITEMENT TO TRUE
           EXEC CICS LINK PROGRAM ('MEC00P') COMMAREA (MEC00C-COMMAREA)
           END-EXEC.
           IF MEC00C-CODE-RETOUR NOT = '0'
              MOVE SPACES           TO W-MESSAGE
              STRING 'MEC31-REP-FF:'
                     ' MEC00 CDRET='  MEC00C-CODE-RETOUR
                     ' SQL='          MEC00C-CODE-DB2-DISPLAY
                     ' MESS='         MEC00C-MESSAGE
                DELIMITED BY SIZE
                INTO W-MESSAGE
              END-STRING
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
           IF W-MEC31-CODE-RET NOT = '0'
      *{ normalize-exec-xx 1.5
      *       EXEC CICS SYNCPOINT ROLLBACK END-EXEC
      *--
              EXEC CICS SYNCPOINT ROLLBACK
              END-EXEC
      *}
           END-IF.
           PERFORM LOG-LE-MESSAGE.
      *
       F-ENVOI-REPONSE. EXIT.
      * CREATION D'UN MOUCHARD MESSAGE EN SORTIE.
       LOG-LE-MESSAGE SECTION.
           IF MEC00C-DATA-XML-L > 4034
              MOVE 4034 TO EC05-XMLDATA-LEN
           ELSE
              MOVE MEC00C-DATA-XML-L TO EC05-XMLDATA-LEN
           END-IF.
           MOVE MEC00C-DATA-XML TO EC05-XMLDATA-TEXT.
           EXEC SQL INSERT INTO RVEC0500 (TIMESTP, WRECENV, XMLDATA)
                         VALUES (CURRENT TIMESTAMP, 'E', :EC05-XMLDATA)
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'INSERT RTEC05:' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
      *
      ******************************************************************
      *
      * A partir d'ici, uniquement modules techniques sans int�r�t pour
      * la compr�hension du programme.
      *
      ******************************************************************
      ******************************************************************
      *
      ******************************************************************
       SELECT-NCGFC SECTION.
      *---------------------
           MOVE SPACES TO NCGFC-WTABLEG.
           EXEC SQL SELECT WTABLEG INTO :NCGFC-WTABLEG
                      FROM RVGA0100
                     WHERE CTABLEG1 = 'NCGFC'
                       AND CTABLEG2 = :NCGFC-CTABLEG2
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE ' ' TO W-MESSAGE
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT NCGFC: ' NCGFC-CTABLEG2
                      ' '     EDIT-SQL
                            DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       SELECT-RTGV02 SECTION.
           EXEC SQL
                SELECT CINSEE,
                       LNOM
                INTO  :GV02-CINSEE,
                      :GV02-LNOM
                FROM   RVGV0202
                WHERE  NVENTE     = :GV02-NVENTE
                AND    NSOCIETE   = :GV02-NSOCIETE
                AND    NLIEU      = :GV02-NLIEU
                AND    WTYPEADR   = 'A'
                WITH UR
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE ' ' TO W-MESSAGE
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT GV02: ' NCGFC-CTABLEG2
                      ' '     EDIT-SQL
                            DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
      ******************************************************************
      * MAJ DE L'ENTETE DE VENTE
      ******************************************************************
       UPDATE-RTGV10 SECTION.
      *----------------------
           MOVE GV11-NSOCIETE        TO GV10-NSOCIETE
           MOVE GV11-NLIEU           TO GV10-NLIEU
           MOVE GV11-NVENTE          TO GV10-NVENTE
           MOVE WS-DATE-DU-JOUR      TO GV10-DMODIFVTE
      *    COMPUTE MES70-PCOMPT = MES70-PCOMPT + GV11-PVTOTAL
           EXEC SQL UPDATE RVGV1005
                       SET PTTVENTE   = PTTVENTE  + :GV11-PVTOTAL,
                           PCOMPT     = PCOMPT + :GV11-PVTOTAL ,
                           DMODIFVTE  = :GV10-DMODIFVTE ,
                           DSYST      = :GV10-DSYST
                     WHERE NSOCIETE   = :GV10-NSOCIETE
                       AND NLIEU      = :GV10-NLIEU
                       AND NVENTE     = :GV10-NVENTE
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'UPDATE RTGV10 NSOC ' GV10-NSOCIETE
                                     ' NLIEU ' GV10-NLIEU
                                     ' NVENTE ' GV10-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
       FIN-UPDATE-RTGV10. EXIT.
       SELECT-MAX-NSEQNQ SECTION.
      *-------------------------------
           MOVE 0                 TO GV11-NSEQNQ
           MOVE W-MEC31-NSOC      TO GV11-NSOCIETE
           MOVE W-MEC31-NLIEU     TO GV11-NLIEU
           MOVE W-MEC31-NVENTE    TO GV11-NVENTE
           EXEC SQL SELECT MAX(NSEQNQ)
                      INTO :WS-MAX-NSEQNQ
                      FROM RVGV1106
                     WHERE NSOCIETE = :GV11-NSOCIETE
                       AND NLIEU    = :GV11-NLIEU
                       AND NVENTE   = :GV11-NVENTE
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT MAX NSEQNQ GV11'
                                   ' NSOCIETE ' GV11-NSOCIETE
                                      ' NLIEU ' GV11-NLIEU
                                     ' DVENTE ' GV11-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-SELECT-MAX-NSEQNQ. EXIT.
       SELECT-MAX-NSEQ SECTION.
      *-------------------------------
           MOVE 0     TO W-MAX-NSEQ
           MOVE W-MEC31-NSOC      TO GV11-NSOCIETE
           MOVE W-MEC31-NLIEU     TO GV11-NLIEU
           MOVE W-MEC31-NVENTE    TO GV11-NVENTE
           EXEC SQL SELECT MAX(NSEQ)
                      INTO :W-MAX-NSEQ
                      FROM RVGV1106
                     WHERE NSOCIETE = :GV11-NSOCIETE
                       AND NLIEU    = :GV11-NLIEU
                       AND NVENTE   = :GV11-NVENTE
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT MAX NSEQNQ GV11'
                                   ' NSOCIETE ' GV11-NSOCIETE
                                      ' NLIEU ' GV11-NLIEU
                                     ' DVENTE ' GV11-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-SELECT-MAX-NSEQ. EXIT.
       SELECT-MAX-NSEQENS     SECTION.
      *-------------------------------
           MOVE 0     TO W-MAX-NSEQENS
           MOVE W-MEC31-NSOC      TO GV11-NSOCIETE
           MOVE W-MEC31-NLIEU     TO GV11-NLIEU
           MOVE W-MEC31-NVENTE    TO GV11-NVENTE
           EXEC SQL SELECT MAX(NSEQENS)
                      INTO :W-MAX-NSEQENS
                      FROM RVGV1106
                     WHERE NSOCIETE = :GV11-NSOCIETE
                       AND NLIEU    = :GV11-NLIEU
                       AND NVENTE   = :GV11-NVENTE
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT MAX NSEQENS gV11'
                                   ' NSOCIETE ' GV11-NSOCIETE
                                      ' NLIEU ' GV11-NLIEU
                                     ' DVENTE ' GV11-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-SELECT-MAX-NSEQENS. EXIT.
       SELECT-MAX-Nligne       SECTION.
      *-------------------------------
           MOVE 0     TO W-MAX-Nligne
           MOVE W-MEC31-NSOC      TO GV11-NSOCIETE
           MOVE W-MEC31-NLIEU     TO GV11-NLIEU
           MOVE W-MEC31-NVENTE    TO GV11-NVENTE
           EXEC SQL SELECT MAX(Nligne)
                      INTO :W-MAX-Nligne
                      FROM RVGV1106
                     WHERE NSOCIETE = :GV11-NSOCIETE
                       AND NLIEU    = :GV11-NLIEU
                       AND NVENTE   = :GV11-NVENTE
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT MAX Nligne   gV11'
                                   ' NSOCIETE ' GV11-NSOCIETE
                                      ' NLIEU ' GV11-NLIEU
                                     ' DVENTE ' GV11-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-SELECT-MAX-Nligne. EXIT.
       SELECT-SUM-GV14 SECTION.
      *-------------------------
           MOVE W-MEC31-NSOC      TO GV14-NSOCIETE
           MOVE W-MEC31-NLIEU     TO GV14-NLIEU
           MOVE W-MEC31-NVENTE    TO GV14-NVENTE
           EXEC SQL SELECT SUM(PREGLTVTE)
                      INTO :GV14-PREGLTVTE
                      FROM RVGV1402
                     WHERE NSOCIETE = :GV14-NSOCIETE
                       AND NLIEU    = :GV14-NLIEU
                       AND NVENTE   = :GV14-NVENTE
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100 and -305
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT SUM GV14'
                                   ' NSOCIETE ' GV14-NSOCIETE
                                      ' NLIEU ' GV14-NLIEU
                                     ' NVENTE ' GV14-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
           IF SQLCODE = -305
              MOVE ZERO   TO GV14-PREGLTVTE
           END-IF.
       FIN-SELECT-SUM-GV14. EXIT.
       SELECT-SUM-GV11 SECTION.
      *-------------------------
           MOVE ZERO TO W-MNT-GV11PLUS
           MOVE ZERO TO W-MNT-GV11MOINS
           MOVE W-MEC31-NSOC      TO GV11-NSOCIETE
           MOVE W-MEC31-NLIEU     TO GV11-NLIEU
           MOVE W-MEC31-NVENTE    TO GV11-NVENTE
           EXEC SQL SELECT SUM(PVTOTAL)
                      INTO :W-MNT-GV11PLUS
                      FROM RVGV1106
                     WHERE NSOCIETE = :GV11-NSOCIETE
                       AND NLIEU    = :GV11-NLIEU
                       AND NVENTE   = :GV11-NVENTE
                       AND ((CTYPENREG = '1' AND CENREG = '     ')
                       OR CTYPENREG = '3'
                       OR CTYPENREG = '4')
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT SUM GV11'
                                   ' NSOCIETE ' GV11-NSOCIETE
                                      ' NLIEU ' GV11-NLIEU
                                     ' NVENTE ' GV11-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
           IF SQLCODE     = 0  and  QTE-SUP-3-OUI
              ADD RESTE-PVTOTAL TO  W-MNT-GV11PLUS
           END-IF.
           EXEC SQL SELECT SUM(PVTOTAL)
                      INTO :W-MNT-GV11MOINS
                      FROM RVGV1106
                     WHERE NSOCIETE = :GV11-NSOCIETE
                       AND NLIEU    = :GV11-NLIEU
                       AND NVENTE   = :GV11-NVENTE
                       AND ((CTYPENREG = '1' AND CENREG > '     ')
                       OR CTYPENREG = '2')
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100 and -305
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT SUM GV11'
                                   ' NSOCIETE ' GV11-NSOCIETE
                                      ' NLIEU ' GV11-NLIEU
                                     ' NVENTE ' GV11-NVENTE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
           IF SQLCODE = -305
              MOVE ZERO   TO W-MNT-GV11MOINS
           END-IF.
           COMPUTE GV11-PVTOTAL = W-MNT-GV11PLUS - W-MNT-GV11MOINS.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-SELECT-SUM-GV11. EXIT.
      ******************************************************************
      *INSERT-RTEC02      SECTION.
      *---------------------------
      *
      *    PERFORM SELECT-RTEC02-NCDEWC.
      *    IF SQLCODE = +100
      *       INITIALIZE RVEC0203
      *
      *       MOVE W-MEC31-NSOC      TO EC02-NSOCIETE
      *       MOVE W-MEC31-NLIEU     TO EC02-NLIEU
      *       MOVE W-MEC31-NVENTE    TO EC02-NVENTE
      *       MOVE WS-DATE-DU-JOUR   TO EC02-DVENTE
      *       MOVE W-MEC31-NCDEWC    TO EC02-NCDEWC
      *       MOVE W-CC-E-TP-CMD     TO EC02-WTPCMD.
      *
      *       IF W-CC-R-U > 0
      *          MOVE W-CC-R-MD-PMENT(1) TO EC008-CTABLEG2
      *          PERFORM SELECT-EC008
      *          IF EC008-WENLIGNE = 'O'
      *             MOVE W-CC-R-MD-PMENT(1) TO EC02-CPAYM
      *             MOVE 'VISA'             TO EC02-CPAYM
      *             IF W-CC-R-DT-ESSEMT(1) > ' '
      *                MOVE 'P' TO EC02-WPAYM
      *             END-IF
      *             MOVE GV11-PVTOTAL       TO EC02-MTPAYM
      *          END-IF
      *       END-IF.
      *
      *       MOVE W-SPDATDB2-DSYST  TO EC02-DSYST
      *
      *       EXEC SQL INSERT INTO RVEC0203
      *                     (NSOCIETE, NLIEU, NVENTE, DVENTE,
      *                      NCDEWC, WTPCMD, CPAYM, WPAYM,
      *                      MTPAYM, LCADEAU, DSYST,
      *                      WCC, CCPSWD, CCEVENT)
      *             VALUES (:EC02-NSOCIETE, :EC02-NLIEU,
      *                     :EC02-NVENTE, :EC02-DVENTE,
      *                     :EC02-NCDEWC, :EC02-WTPCMD,
      *                     :EC02-CPAYM, :EC02-WPAYM,
      *                     :EC02-MTPAYM, :EC02-LCADEAU,
      *                     :EC02-DSYST, :EC02-WCC,
      *                     :EC02-CCPSWD, :EC02-CCEVENT)
      *       END-EXEC
      *
      *       IF SQLCODE NOT = 0
      *          MOVE SPACES TO W-MESSAGEs
      *          MOVE SQLCODE TO EDIT-SQL
      *          STRING 'INSERT RTEC02 NSOCIETE: ' W-MEC31-NSOC
      *                                 ' NLIEU: ' W-MEC31-NLIEU
      *                                ' NVENTE: ' W-MEC31-NVENTE
      *               ' '     EDIT-SQL
      *          DELIMITED BY SIZE INTO W-MESSAGEs
      *          PERFORM DISPLAY-W-MESSAGE
      *          PERFORM ERREUR-SQL
      *          MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
      *          MOVE '9' TO W-MEC31-CODE-RET
      *       END-IF
      *    ELSE
      *       MOVE W-SPDATDB2-DSYST TO EC02-DSYST
      *       EXEC SQL UPDATE RVEC0203
      *                SET MTPAYM     = MTPAYM    + :GV11-PVTOTAL,
      *                    DSYST      = :EC02-DSYST
      *              WHERE NCDEWC     = :EC02-NCDEWC
      *              WHERE NSOCIETE   = :EC02-NSOCIETE
      *                AND NLIEU      = :EC02-NLIEU
      *                AND NVENTE     = :EC02-NVENTE
      *       END-EXEC
      *       IF SQLCODE NOT = 0
      *          MOVE SPACES TO W-MESSAGEs
      *          MOVE SQLCODE TO EDIT-SQL
      *          STRING 'UPDATE RTEC02 NSOCIETE: ' W-MEC31-NSOC
      *                                 ' NLIEU: ' W-MEC31-NLIEU
      *                                ' NVENTE: ' W-MEC31-NVENTE
      *               ' '     EDIT-SQL
      *          DELIMITED BY SIZE INTO W-MESSAGEs
      *       PERFORM DISPLAY-W-MESSAGE
      *          PERFORM ERREUR-SQL
      *          MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
      *          MOVE '9' TO W-MEC31-CODE-RET
      *       END-IF
      *    END-IF.
      *
      *FIN-INSERT-RTEC02.
       INSERT-RTEC10      SECTION.
      *---------------------------
           PERFORM SELECT-RTEC10-NCDEWC.
           IF SQLCODE = +100
              INITIALIZE RVEC1000
              MOVE W-MEC31-NCDEWC   TO EC10-NCDEWC
              MOVE W-MEC31-NSOC     TO EC10-NSOCIETE
              MOVE W-MEC31-NLIEU    TO EC10-NLIEU
              MOVE W-MEC31-NVENTE   TO EC10-NVENTE
              MOVE 'I'              TO EC10-STAT
              MOVE 'EC00'           TO EC10-OPER
      *       MOVE MEC15E-NOM       TO EC10-NOM
              MOVE WS-DATE-DU-JOUR  TO EC10-DCRE
      *       MOVE EIBTIME          TO W-EIBTIME
      *       MOVE W-HHMMSS         TO EC10-HCRE
              MOVE W-SPDATDB2-DSYST TO EC10-DSYST
              EXEC SQL INSERT INTO RVEC1000 (NCDEWC, NSOCIETE, NLIEU,
                             NVENTE, STAT, OPER, NOM,
                             DCRE, HCRE, DSYST)
                    VALUES (:EC10-NCDEWC, :EC10-NSOCIETE, :EC10-NLIEU,
                            :EC10-NVENTE, :EC10-STAT, :EC10-OPER,
                            :EC10-NOM, :EC10-DCRE, :EC10-HCRE,
                            :EC10-DSYST)
              END-EXEC
              IF SQLCODE NOT = 0
                 MOVE SPACES TO W-MESSAGEs
                 MOVE SQLCODE TO EDIT-SQL
                 STRING 'INSERT RTEC02 NSOCIETE: ' W-MEC31-NSOC
                                        ' NLIEU: ' W-MEC31-NLIEU
                                       ' NVENTE: ' W-MEC31-NVENTE
                      ' '     EDIT-SQL
                 DELIMITED BY SIZE INTO W-MESSAGEs
                 PERFORM DISPLAY-W-MESSAGE
      *          PERFORM ERREUR-SQL
                 MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
                 MOVE '9' TO W-MEC31-CODE-RET
              END-IF
           END-IF.
       FIN-INSERT-RTEC10.
      ******************************************************************
      * CREATION LIEN ENTRE COMMANDE MERE ET COMMANDE FILLE
      ******************************************************************
       CREATION-RTEC32      SECTION.
      *---------------------------
           INITIALIZE RVEC3200.
           PERFORM SELECT-RTEC02
           IF SQLCODE = +100
              MOVE W-MEC31-NCDEWC    TO EC32-NCDEWC
           ELSE
              MOVE EC02-NCDEWC       TO EC32-NCDEWC
           END-IF
           MOVE W-MEC31-NCDEWC    TO EC32-NCDEWC2
           MOVE WS-DATE-DU-JOUR   TO EC32-DVENTE.
           MOVE W-MNT-TOTAL       TO EC32-MTPAYM
           MOVE W-MEC31-MD-PMENT  TO EC32-CPAYM
           MOVE W-SPDATDB2-DSYST  TO EC32-DSYST
           MOVE 'MEC31'           TO EC32-CNOMPROG
           EXEC SQL INSERT INTO RVEC3200
                            (NCDEWC  , NCDEWC2, DVENTE,
                             DVALP, WVALP, CPAYM, MTPAYM,
                             CNOMPROG, DSYST)
                    VALUES (:EC32-NCDEWC, :EC32-NCDEWC2,
                            :EC32-DVENTE, :EC32-DVALP, :EC32-WVALP,
                            :EC32-CPAYM, :EC32-MTPAYM,
                            :EC32-CNOMPROG,
                            :EC32-DSYST )
           END-EXEC.
      *    EXEC SQL INSERT INTO RVEC3200
      *                     (NCDEWC  , NCDEWC2, DVENTE,
      *                      CNOMPROG, DSYST)
      *             VALUES (:EC32-NCDEWC, :EC32-NCDEWC2,
      *                     :EC32-DVENTE, :EC32-CNOMPROG,
      *                     :EC32-DSYST )
      *    END-EXEC.
           IF SQLCODE NOT = 0 AND NOT = -803
              MOVE SPACES TO W-MESSAGEs
              MOVE EC32-NCDEWC2 TO PIC9-NCDEWC
              MOVE SQLCODE TO EDIT-SQL
              STRING 'INSERT RTEC32 NCDEWC 2 ' PICX-NCDEWC
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-CREATION-RTEC32.
      ******************************************************************
      * Insertion de la ligne de vente
      ******************************************************************
       INSERT-RTGV11 SECTION.
      *----------------------
           EXEC SQL INSERT INTO RVGV1106
            VALUES (
              :GV11-NSOCIETE
             ,:GV11-NLIEU
             ,:GV11-NVENTE
             ,:GV11-CTYPENREG
             ,:GV11-NCODICGRP
             ,:GV11-NCODIC
             ,:GV11-NSEQ
             ,:GV11-CMODDEL
             ,:GV11-DDELIV
             ,:GV11-NORDRE
             ,:GV11-CENREG
             ,:GV11-QVENDUE
             ,:GV11-PVUNIT
             ,:GV11-PVUNITF
             ,:GV11-PVTOTAL
             ,:GV11-CEQUIPE
             ,:GV11-NLIGNE
             ,:GV11-TAUXTVA
             ,:GV11-QCONDT
             ,:GV11-PRMP
             ,:GV11-WEMPORTE
             ,:GV11-CPLAGE
             ,:GV11-CPROTOUR
             ,:GV11-CADRTOUR
             ,:GV11-CPOSTAL
             ,:GV11-LCOMMENT
             ,:GV11-CVENDEUR
             ,:GV11-NSOCLIVR
             ,:GV11-NDEPOT
             ,:GV11-NAUTORM
             ,:GV11-WARTINEX
             ,:GV11-WEDITBL
             ,:GV11-WACOMMUTER
             ,:GV11-WCQERESF
             ,:GV11-NMUTATION
             ,:GV11-CTOURNEE
             ,:GV11-WTOPELIVRE
             ,:GV11-DCOMPTA
             ,:GV11-DCREATION
             ,:GV11-HCREATION
             ,:GV11-DANNULATION
             ,:GV11-NLIEUORIG
             ,:GV11-WINTMAJ
             ,:GV11-DSYST
             ,:GV11-DSTAT
             ,:GV11-CPRESTGRP
             ,:GV11-NSOCMODIF
             ,:GV11-NLIEUMODIF
             ,:GV11-DATENC
             ,:GV11-CDEV
             ,:GV11-WUNITR
             ,:GV11-LRCMMT
             ,:GV11-NSOCP
             ,:GV11-NLIEUP
             ,:GV11-NTRANS
             ,:GV11-NSEQFV
             ,:GV11-NSEQNQ
             ,:GV11-NSEQREF
             ,:GV11-NMODIF
             ,:GV11-NSOCORIG
             ,:GV11-DTOPE
             ,:GV11-NSOCLIVR1
             ,:GV11-NDEPOT1
             ,:GV11-NSOCGEST
             ,:GV11-NLIEUGEST
             ,:GV11-NSOCDEPLIV
             ,:GV11-NLIEUDEPLIV
             ,:GV11-TYPVTE
             ,:GV11-CTYPENT
             ,:GV11-NLIEN
             ,:GV11-NACTVTE
             ,:GV11-PVCODIG
             ,:GV11-QTCODIG
             ,:GV11-DPRLG
             ,:GV11-CALERTE
             ,:GV11-CREPRISE
             ,:GV11-NSEQENS
             ,:GV11-MPRIMECLI
                   )
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'INSERT RTGV11 NSOC: ' GV11-NSOCIETE
                                 ' NLIEU: '  GV11-NLIEU
                                 ' NORDRE: ' GV11-NORDRE
                                 ' NLIGNE: ' GV11-NLIGNE
                      ' '     EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
       FIN-INSERT-RTGV11. EXIT.
       SELECT-RTGV11 SECTION.
      *-------------------------------
           MOVE W-MEC31-NSOC              TO   GV11-NSOCIETE
           MOVE W-MEC31-NLIEU             TO   GV11-NLIEU
           MOVE W-MEC31-NVENTE            TO   GV11-NVENTE
           MOVE W-MEC31-NCODIC(W-PSE-R-I) TO   GV11-NCODIC
           MOVE W-MEC31-NSEQNQ(W-PSE-R-I) TO   GV11-NSEQNQ
           EXEC SQL SELECT *
                    INTO :RVGV1106
                      FROM RVGV1106
                     WHERE NSOCIETE  =  :GV11-NSOCIETE
                       AND NLIEU     =  :GV11-NLIEU
                       AND NVENTE    =  :GV11-NVENTE
                       AND NCODIC    =  :GV11-NCODIC
                       AND NSEQNQ    =  :GV11-NSEQNQ
           END-EXEC.
           IF SQLCODE = 0
             SET WS-GV11-TROUVEE TO TRUE
             MOVE GV11-NSEQENS TO WS-GV11-NSEQENS
           ELSE
             IF SQLCODE = 100
               MOVE GV11-NSEQNQ   TO EDIT-NSEQ
               MOVE '01' TO W-MEC31-RET(W-PSE-R-I)
               MOVE '9' TO W-MEC31-CODE-RET
               STRING 'GV11 '
                         GV11-NSOCIETE
                    ' '  GV11-NLIEU
                    ' '  GV11-NVENTE
                    ' '  GV11-NCODIC
                    ' '  EDIT-NSEQ
                      ' NON TROUVE'
                DELIMITED BY SIZE INTO W-MESSAGES
      *       MOVE   'GV11 NON TROUVE    ' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
             ELSE
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT GV11 SOC ' GV11-NSOCIETE
                               ' LIEU ' GV11-NLIEU
                               ' VENTE ' GV11-NVENTE
                               ' CODIC ' GV11-NCODIC
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '95' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
             END-IF
           END-IF.
       FIN-SELECT-RTGV11. EXIT.
       SELECT-VTE-CODIC SECTION.
      *-------------------------------
           MOVE W-MEC31-NSOC              TO   GV11-NSOCIETE
           MOVE W-MEC31-NLIEU             TO   GV11-NLIEU
           MOVE W-MEC31-NVENTE            TO   GV11-NVENTE
           MOVE W-MEC31-NCODIC(W-PSE-R-I) TO   GV11-NCODIC
           if   W-MEC31-NCODIC(W-PSE-R-I)      =    GV11B-NCODIC
                MOVE GV11B-NSEQNQ              TO   GV11-NSEQNQ
           else
                MOVE W-MEC31-NSEQNQ(W-PSE-R-I) TO   GV11-NSEQNQ
           end-if
           EXEC SQL SELECT *
                    INTO :RVGV1106
                      FROM RVGV1106
                     WHERE NSOCIETE  =  :GV11-NSOCIETE
                       AND NLIEU     =  :GV11-NLIEU
                       AND NVENTE    =  :GV11-NVENTE
                       AND NCODIC    =  :GV11-NCODIC
                       AND NSEQNQ    =  :GV11-NSEQNQ
                       AND QVENDUE   >  1
                       AND CTYPENREG =  '1'
           END-EXEC.
      
           IF SQLCODE = 0
             SET WS-GV11-TROUVEE TO TRUE
           ELSE
             IF SQLCODE = 100
                SET WS-GV11-NON-TROUVEE  TO TRUE
             ELSE
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT GV11 SOC ' GV11-NSOCIETE
                               ' LIEU ' GV11-NLIEU
                               ' VENTE ' GV11-NVENTE
                               ' CODIC ' GV11-NCODIC
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '99' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
             END-IF
           END-IF.
       FIN-SELECT-VTE-CODIC . EXIT.
       SELECT-RTGV11-TOPE SECTION.
      *-------------------------------
           EXEC SQL SELECT *
                    INTO :RVGV1106
                      FROM RVGV1106
                     WHERE NSOCIETE  =  :GV11-NSOCIETE
                       AND NLIEU     =  :GV11-NLIEU
                       AND NVENTE    =  :GV11-NVENTE
                       AND WTOPELIVRE = 'N'
           END-EXEC.
           IF SQLCODE NOT = +100
               MOVE '07' TO W-MEC31-RET(W-PSE-R-I)
               MOVE '9' TO W-MEC31-CODE-RET
              MOVE   'PRESENCE NON TOPE  ' TO W-MESSAGES
               STRING 'PRESENCE NON TOPE SUR VENTE '
                         W-MEC31-NSOC
                    ' '  W-MEC31-NLIEU
                    ' '  W-MEC31-NVENTE
                DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
       FIN-SELECT-RTGV11-TOPE. EXIT.
       SELECT-PSE    SECTION.
      *-------------------------------
      *  A FAIRE (EVENTUELLEMENT)
           MOVE W-MEC31-NCODIC(W-PSE-R-I) TO GV11-NCODIC
           MOVE W-MEC31-C-PSE(W-PSE-R-I) TO GV11-CENREG
      *
           EXEC SQL SELECT RVGV1106.*
                    INTO :RVGV1106
                      FROM RVGV1106
                     WHERE NSOCIETE  =  :GV11-NSOCIETE
                       AND NLIEU     =  :GV11-NLIEU
                       AND NVENTE    =  :GV11-NVENTE
                       AND NCODIC    =  :GV11-NCODIC
                       AND NSEQ      =  :GV11-NSEQ
                       AND DANNULATION > '        '
                       AND CENREG    >  '     '
      *         ctypenreg = '3'
           END-EXEC.
           IF SQLCODE = 0
             SET WS-GV11-TROUVEE TO TRUE
           ELSE
             IF SQLCODE = 100
               SET WS-GV11-NON-TROUVEE TO TRUE
             ELSE
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT PSE SOC ' GV11-NSOCIETE
                               ' LIEU ' GV11-NLIEU
                               ' VENTE ' GV11-NVENTE
                               ' CODIC ' GV11-NCODIC
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
             END-IF
           END-IF.
       FIN-SELECT-PSE. EXIT.
       SELECT-GA52   SECTION.
      *-------------------------------
      *  A FAIRE (EVENTUELLEMENT)
           MOVE W-MEC31-NCODIC(W-PSE-R-I) TO GA52-NCODIC
           MOVE W-MEC31-C-PSE(W-PSE-R-I) TO GA52-CGARANTCPLT
      *
           EXEC SQL SELECT
                          CVGARANCPLT
                    INTO
                         :GA52-CVGARANCPLT
                    FROM     RVGA5200
                    WHERE
                          NCODIC      = :GA52-NCODIC
                    AND   CGARANTCPLT = :GA52-CGARANTCPLT
           END-EXEC.
           IF SQLCODE NOT = 0 AND +100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT GA52 PSE '  W-MEC31-C-PSE(W-PSE-R-I)
                               ' CODIC ' W-MEC31-NCODIC(W-PSE-R-I)
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
       FIN-SELECT-GA52. EXIT.
       SELECT-GA40   SECTION.
      *-------------------------------
      *  A FAIRE (EVENTUELLEMENT)
           MOVE W-MEC31-NCODIC(W-PSE-R-I) TO GA00-NCODIC
      *
           EXEC SQL SELECT
                          CFAM
                    INTO
                         :GA00-CFAM
                    FROM     RVGA0000
                    WHERE
                          NCODIC      = :GA00-NCODIC
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT GA00 '
                                'CODIC ' W-MEC31-NCODIC(W-PSE-R-I)
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
           MOVE W-MEC31-C-PSE(W-PSE-R-I)  TO GA40-CGARANTIE
           MOVE GA00-CFAM                 TO GA40-CFAM
           MOVE GA52-CVGARANCPLT          TO GA40-CTARIF
           MOVE '00000000'             TO WS-DATE-MAX
           MOVE WS-DATE-DU-JOUR        TO GA40-DEFFET
      *
           EXEC SQL SELECT
                          MAX (DEFFET)
                    INTO
                         :WS-DATE-MAX    :WS-DATE-MAX-F
                    FROM     RVGA4000
                    WHERE
                          CGARANTIE = :GA40-CGARANTIE
                    AND   CFAM      = :GA40-CFAM
                    AND   CTARIF    = :GA40-CTARIF
                    AND   DEFFET   <= :GA40-DEFFET
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT GA40 '
                                'CFAM ' GA00-CFAM
                                'PSE  ' W-MEC31-C-PSE(W-PSE-R-I)
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
           IF WS-DATE-MAX NOT = '00000000'
              EXEC SQL SELECT
                             PGARANTIE
                       INTO
                            :GA40-PGARANTIE
                       FROM     RVGA4000
                       WHERE
                             CGARANTIE = :GA40-CGARANTIE
                       AND   CFAM      = :GA40-CFAM
                       AND   CTARIF    = :GA40-CTARIF
                       AND   DEFFET    = :WS-DATE-MAX
                       END-EXEC
           ELSE
              MOVE '05' TO W-MEC31-RET(W-PSE-R-I)
              MOVE '9' TO W-MEC31-CODE-RET
           END-IF.
       FIN-SELECT-GA40. EXIT.
      * RECHERCHE DE LA COMMANDE MERE
       SELECT-RTEC02 SECTION.
      *----------------------
           MOVE W-MEC31-NSOC   TO EC02-NSOCIETE
           MOVE W-MEC31-NLIEU  TO EC02-NLIEU
           MOVE W-MEC31-NVENTE TO EC02-NVENTE
      *    MOVE W-MEC31-NCDEWC TO EC02-NCDEWC
           EXEC SQL SELECT   NCDEWC  INTO :EC02-NCDEWC
                      FROM RVEC0200
                     WHERE   NSOCIETE  =  :EC02-NSOCIETE
                       AND   NLIEU     =  :EC02-NLIEU
                       AND   NVENTE    =  :EC02-NVENTE
      *                AND   NCDEWC   ^=  :EC02-NCDEWC
      *                AND   NCDEWC    =  B.NCDEWC
           END-EXEC.
      *    IF SQLCODE = -811
      *       EXEC SQL SELECT A.NCDEWC  INTO :EC02-NCDEWC
      *             FROM     RVEC0203 A
      *             INNER JOIN RVEC3200  B
      *             ON A.NCDEWC = B.NCDEWC
      *             WHERE   A.NSOCIETE  = :EC02-NSOCIETE
      *             AND     A.NLIEU     = :EC02-NLIEU
      *             AND     A.NVENTE    = :EC02-NVENTE
      *       END-EXEC
      *    END-IF
      *    IF SQLCODE NOT = 0 AND 100
           IF SQLCODE NOT = 0 and +100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT EC02'
                           ' SOC '  W-MEC31-NSOC
                          ' LIEU '  W-MEC31-NLIEU
                          ' VENTE ' W-MEC31-NVENTE
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-SELECT-RTEC02. EXIT.
       SELECT-RTEC02-NCDEWC SECTION.
      *----------------------
           MOVE W-MEC31-NSOC   TO EC02-NSOCIETE
           MOVE W-MEC31-NLIEU  TO EC02-NLIEU
           MOVE W-MEC31-NVENTE TO EC02-NVENTE
           MOVE W-MEC31-NCDEWC TO EC02-NCDEWC
           EXEC SQL SELECT DVENTE  INTO :EC02-DVENTE
                      FROM RVEC0203
                     WHERE NSOCIETE  =  :EC02-NSOCIETE
                       AND NLIEU     =  :EC02-NLIEU
                       AND NVENTE    =  :EC02-NVENTE
                       AND NCDEWC    =  :EC02-NCDEWC
           END-EXEC.
           IF SQLCODE = -811
              EXEC SQL SELECT a.DVENTE  INTO :EC02-DVENTE
                    FROM     RVEC0203 A
                    INNER JOIN RVEC3200  B
                    ON A.NCDEWC = B.NCDEWC
                    WHERE   A.NSOCIETE  = :EC02-NSOCIETE
                    AND     A.NLIEU     = :EC02-NLIEU
                    AND     A.NVENTE    = :EC02-NVENTE
              END-EXEC
           END-IF
           IF SQLCODE NOT = 0 AND 100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT EC02N'
                           ' SOC '  W-MEC31-NSOC
                          ' LIEU '  W-MEC31-NLIEU
                          ' VENTE ' W-MEC31-NVENTE
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-SELECT-RTEC02-NCDEWC. EXIT.
       SELECT-RTEC10-NCDEWC SECTION.
      *----------------------
      *    MOVE W-MEC31-NSOC   TO EC10-NSOCIETE
      *    MOVE W-MEC31-NLIEU  TO EC10-NLIEU
      *    MOVE W-MEC31-NVENTE TO EC10-NVENTE
           MOVE W-MEC31-NCDEWC TO EC10-NCDEWC
           EXEC SQL SELECT NVENTE  INTO :EC10-NVENTE
                      FROM RVEC1000
      *              WHERE NSOCIETE  =  :EC10-NSOCIETE
      *                AND NLIEU     =  :EC10-NLIEU
      *                AND NVENTE    =  :EC10-NVENTE
                     WHERE NCDEWC    =  :EC10-NCDEWC
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE SPACES TO W-MESSAGEs
              MOVE SQLCODE TO EDIT-SQL
              STRING 'SELECT EC10N'
      *                    ' SOC '  W-MEC31-NSOC
      *                   ' LIEU '  W-MEC31-NLIEU
      *                   ' VENTE ' W-MEC31-NVENTE
      *                   ' VENTE ' W-MEC31-NVENTE
                       ' ' EDIT-SQL
              DELIMITED BY SIZE INTO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
              MOVE '9' TO W-MEC31-CODE-RET
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-SELECT-RTEC10-NCDEWC. EXIT.
      ******************************************************************
      * CURSEUR GV11 POUR DECALER LES LIGNES
      ******************************************************************
       DECLARE-OPEN-RTGV11 SECTION.
      *----------------------------
           EXEC SQL DECLARE CRSGV11  CURSOR FOR
                    SELECT NLIGNE
                      FROM RVGV1106
                     WHERE NSOCIETE = :GV11-NSOCIETE
                       AND NLIEU    = :GV11-NLIEU
                       AND NVENTE   = :GV11-NVENTE
                       FOR UPDATE OF NLIGNE
           END-EXEC.
           EXEC SQL OPEN CRSGV11    END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'OPEN RTGV11:' TO W-MESSAGEs
              MOVE GV11-NVENTE   TO W-MESSAGEs(14:)
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-DECLARE-OPEN-RTGV11. EXIT.
       FETCH-RTGV11 SECTION.
      *---------------------
           EXEC SQL FETCH CRSGV11  INTO :GV11-NLIGNE
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE 'FETCH RTGV11:' TO W-MESSAGEs
              MOVE GV11-NVENTE   TO W-MESSAGEs(15:)
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-FETCH-RTGV11. EXIT.
       CLOSE-RTGV11 SECTION.
      *---------------------
           EXEC SQL CLOSE CRSGV11   END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'CLOSE RTGV11:' TO W-MESSAGEs
              MOVE GV11-NVENTE   TO W-MESSAGEs(15:)
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-CLOSE-RTGV11. EXIT.
       UPDATE-RTGV11 SECTION.
      *----------------------
           EXEC SQL
                UPDATE RVGV1106
                SET NLIGNE  = :GV11-NLIGNE
                WHERE CURRENT OF CRSGV11
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'UPDATE RTGV11:' TO W-MESSAGEs
              MOVE GV11-NVENTE   TO W-MESSAGEs(15:)
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
        FIN-UPDATE-RTGV11. EXIT.
       UPDATE-RTGV11-qte    SECTION.
      *----------------------
           MOVE W-MEC31-NSOC                 TO     GV11-NSOCIETE
           MOVE W-MEC31-NLIEU                TO     GV11-NLIEU
           MOVE W-MEC31-NVENTE               TO     GV11-NVENTE
           MOVE W-MEC31-NCODIC(W-PSE-R-I)    TO     GV11-NCODIC
      
           EXEC SQL
                UPDATE RVGV1106
                SET PVTOTAL = :GV11-PVUNIT
                   ,QVENDUE = :GV11-QVENDUE
                WHERE
                       NSOCIETE      =  :GV11-NSOCIETE
                       AND NLIEU     =  :GV11-NLIEU
                       AND NVENTE    =  :GV11-NVENTE
                       AND NCODIC    =  :GV11-NCODIC
                       AND CTYPENREG =  '1'
                       AND QVENDUE   >   1
      
           END-EXEC.
      
           IF SQLCODE NOT = 0
              MOVE 'UPDATE RTGV11-QTE:' TO W-MESSAGEs
              MOVE GV11-NVENTE   TO W-MESSAGEs(15:)
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
      
      
        FIN-UPDATE-RTGV11-qte. EXIT.
       SELECT-GV10 SECTION.
      *--------------------
           MOVE ' '  TO GV10-NSOCP GV10-NLIEUP GV10-TYPVTE.
           MOVE W-MEC31-NSOC      TO     GV10-NSOCIETE
           MOVE W-MEC31-NLIEU     TO     GV10-NLIEU
           MOVE W-MEC31-NVENTE    TO     GV10-NVENTE
           EXEC SQL SELECT PTTVENTE, PCOMPT, NSOCP, NLIEUP, TYPVTE
                           , DVENTE
                      INTO :GV10-PTTVENTE, :GV10-PCOMPT,
                           :GV10-NSOCP, :GV10-NLIEUP, :GV10-TYPVTE
                         , :GV10-DVENTE
                      FROM RVGV1005
                     WHERE NSOCIETE = :GV10-NSOCIETE
                       AND NLIEU    = :GV10-NLIEU
                       AND NVENTE   = :GV10-NVENTE
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'SELECT RTGV10:' TO W-MESSAGEs
              MOVE GV11-NVENTE   TO W-MESSAGEs(15:)
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
      *    PERFORM TEST-CODE-RETOUR-SQL.
       FIN-SELECT-GV10. EXIT.
       SELECT-CVENDEUR SECTION.
           MOVE W-MEC31-C-VEND(W-PSE-R-I)   TO GV31-CVENDEUR
           EXEC SQL
                SELECT LVENDEUR INTO :GV31-LVENDEUR
                 FROM RVGV3101
                WHERE CVENDEUR = :GV31-CVENDEUR
           END-EXEC.
           IF SQLCODE = 0
             SET WS-GV31-TROUVEE TO TRUE
           ELSE
             IF SQLCODE = 100
               SET WS-GV31-NON-TROUVEE TO TRUE
             ELSE
               MOVE SPACES TO W-MESSAGEs
               MOVE SQLCODE TO EDIT-SQL
               STRING 'SELECT RTGV31 CVENDEUR: ' GV31-CVENDEUR
                       ' ' EDIT-SQL
               DELIMITED BY SIZE INTO W-MESSAGEs
               PERFORM DISPLAY-W-MESSAGE
               MOVE '9' TO W-MEC31-CODE-RET
      *        PERFORM ERREUR-SQL
             END-IF
           END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-SELECT-CVENDEUR. EXIT.
      ******************************************************************
      * Link � MEC09: d�codage Adresse
      ******************************************************************
       FAIS-UN-LINK-A-MEC09 SECTION.
      *-----------------------------
           EXEC CICS LINK PROGRAM ('MEC09')
                     COMMAREA (MEC09C)
           END-EXEC.
      ******************************************************************
      * Erreur Programme
      ******************************************************************
       ERREUR-PRG SECTION.
      *-------------------
           PERFORM DISPLAY-W-MESSAGE.
           MOVE '-1' TO W-MEC31-RET(W-PSE-R-I).
      *    EXEC CICS RETURN END-EXEC.
      ******************************************************************
      * Erreur SQL
      ******************************************************************
       ERREUR-SQL SECTION.
      *-------------------
           PERFORM DISPLAY-W-MESSAGE.
           CALL DSNTIAR USING SQLCA, MSG-SQL, MSG-SQL-LRECL.
           PERFORM VARYING I-SQL FROM 1 BY 1 UNTIL I-SQL > 3
              MOVE MSG-SQL-MSG (I-SQL) TO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
           END-PERFORM.
           MOVE '-1' TO W-MEC31-RET(W-PSE-R-I).
      *    EXEC CICS RETURN END-EXEC.
      ******************************************************************
      * Display d'un Message
      ******************************************************************
       DISPLAY-W-MESSAGE SECTION.
      *--------------------------
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.
      *--
           EXEC CICS ASKTIME NOHANDLE
           END-EXEC.
      *}
           MOVE EIBTIME             TO W-EIBTIME.
           MOVE W-HEURE             TO W-MESSAGE-H.
           MOVE W-MINUTE            TO W-MESSAGE-M.
           EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
                     LENGTH (80) NOHANDLE
           END-EXEC.
              MOVE SPACES TO W-MESSAGEs.
      ******************************************************************
      * Link � Tetdatc.
      ******************************************************************
       FAIS-UN-LINK-A-TETDATC SECTION.
      *-------------------------------
           EXEC CICS LINK PROGRAM ('TETDATC')
                     COMMAREA (Z-COMMAREA-TETDATC)
           END-EXEC.
           IF GFVDAT NOT = '1'
              MOVE 'Code retour TETDATC: ' TO W-MESSAGEs
              MOVE GFVDAT                  TO W-MESSAGEs(24:)
              PERFORM DISPLAY-W-MESSAGE
              MOVE '-1' TO W-MEC31-RET(W-PSE-R-I)
      *       EXEC CICS RETURN END-EXEC
           END-IF.
      ******************************************************************
      * SPDatDB2
      ******************************************************************
       CALL-SPDATDB2 SECTION.
           CALL 'SPDATDB2' USING W-SPDATDB2-RC W-SPDATDB2-DSYST.
           IF W-SPDATDB2-RC NOT = ZERO
              MOVE 'Pbm Date Syst�me SPDATDB2' TO W-MESSAGEs
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
      *
      *-----------------------------------------------------------------
      * Abend Label: on s'est plant�; on flingue le code retour et
      *              on revient l� o� on �tait: on ne cr�e pas de vente.
       ABEND-LABEL SECTION.
           MOVE 'Handle Abend effectu�' TO W-MESSAGEs.
           PERFORM DISPLAY-W-MESSAGE.
           MOVE '-1' TO W-MEC31-RET(W-PSE-R-I).
      *    EXEC CICS RETURN END-EXEC.
      *
       RECH-MAG-INTERNET              SECTION.
      ******************************************************************
      * LECTURE DU "MAGASIN INTERNET" PAR DEFAUT.
      ******************************************************************
      * V�RIFICATION EXISTENCE DE LA TS ET CR�ATION LE CAS �CH�ANT
           MOVE 'MEC31     '   TO WS-TSTENVOI-NOMPGM
           PERFORM GESTION-TSTENVOI
      * RECHERCHE DES TYPES D'ENVOI � TRAITER
           MOVE  LOW-VALUES TO   WS-TYPENVOI
           MOVE  '0' TO WF-TSTENVOI
           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1
             UNTIL  WC-TSTENVOI-FIN
              PERFORM LECTURE-TSTENVOI
              IF  WC-TSTENVOI-SUITE
              AND TSTENVOI-NSOCZP    = '00000'
              AND TSTENVOI-CTRL       = 'ECOM'
                 MOVE TSTENVOI-WPARAM(1:3) TO W-CC-NSOC-ECOM
                 MOVE TSTENVOI-WPARAM(4:3) TO W-CC-NLIEU-ECOM
              END-IF
           END-PERFORM.
           display 'W-CC-NSOC-ECOM   = '  W-CC-NSOC-ECOM
           display 'W-CC-NLIEU-ECOM  = '  W-CC-NLIEU-ECOM .
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-RECH-MAG-INTERNET. EXIT.
      *-----------------------------------------------------------------
      *  => Lecture de la TS TSTENVOI
      * mecccprg n'accepte pas "include cstenvoi" .
      *LECTURE-TSTENVOI SECTION.
      *    EXEC CICS
      *         READQ TS
      *         QUEUE  (WC-ID-TSTENVOI)
      *         INTO   (TS-TSTENVOI-DONNEES)
      *         LENTGH (LENGTH OF TS-TSTENVOI-DONNEES)
      *         ITEM   (WP-RANG-TSTENVOI)
      *         NOHANDLE
      *    END-EXEC.
      *    IF EIBRESP = 0
      *       SET WC-TSTENVOI-SUITE TO TRUE
      *    ELSE
      *       SET WC-TSTENVOI-FIN   TO TRUE
      *    END-IF.
      *FIN-LECT-TSTENVOI. EXIT.
      * -------------------------------------------------------------- *
      * CETTE COPIE GERE TOUT CE QUI TOUCHE LA GESTION DE LA TS        *
      * TSTENVOI: ** CREATION SI CETTE DERNERE N'EXISTE PAS            *
      *           ** LECTURE DE CETTE TS                               *
      * LES ZONES DE TRAVAIL SONT DECLAREES DANS WKTENVOI              *
      *                                                                *
      * CREE LE 01/03/2011 PAR DE02003 (CL)                            *
      *                                                                *
      * MODIF LE 16/08/2011 PAR DE02074 (EG)                           *
      *          DANS GESTION TSTENVOI, ON AJOUTE UN ENREG DE FIN, SI  *
      *          ON NE LE TROUVE PAS LORS DE LA LECTURE DE LA TS, ON   *
      *          DELETE LA TS POUR LA RE CREER                         *
      * -------------------------------------------------------------- *
       GESTION-TSTENVOI  SECTION.
           MOVE 1                            TO WP-RANG-TSTENVOI
           PERFORM LECTURE-TSTENVOI
           IF WC-TSTENVOI-FIN
              PERFORM CREATION-TSTENVOI
           ELSE
      * ON CHERCHE LE DERNIER ENREG DE LA TSTENVOI
              PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1
              UNTIL WC-TSTENVOI-FIN
                 PERFORM LECTURE-TSTENVOI
              END-PERFORM
              IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '
                 PERFORM DELETE-TSTENVOI
                 PERFORM CREATION-TSTENVOI
              END-IF
           END-IF.
       FIN-GEST-TSTENVOI.  EXIT.
      *
      *-----------------------------------------------------------------
       CREATION-TSTENVOI SECTION.
              MOVE '00000'                   TO TSTENVOI-NSOCZP
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM
              MOVE 'NOM PROGRAMME'           TO TSTENVOI-LIBELLE
              PERFORM ECRITURE-TSTENVOI
              SET WC-XCTRL-EXPDEL-NON-TROUVE TO TRUE
              INITIALIZE WZ-XCTRL-EXPDEL-SQLCODE
              PERFORM DECLARE-OPEN-CXCTRL
              IF WC-XCTRL-EXPDEL-PB-DB2
                 SET WC-TSTENVOI-ERREUR TO TRUE
                 GO TO FIN-CREATION-TSTENVOI
              END-IF
              PERFORM FETCH-CXCTRL
              IF WC-XCTRL-EXPDEL-PB-DB2
                 SET WC-TSTENVOI-ERREUR TO TRUE
                 GO TO FIN-CREATION-TSTENVOI
              END-IF
              PERFORM UNTIL WC-XCTRL-EXPDEL-FIN
                 IF  XCTRL-WPARAM   > SPACES
                 AND XCTRL-LIBELLE  > SPACES
                    MOVE XCTRL-SOCZP         TO TSTENVOI-NSOCZP
                    MOVE XCTRL-CTRL          TO TSTENVOI-CTRL
                    MOVE XCTRL-WPARAM        TO TSTENVOI-WPARAM
                    MOVE XCTRL-LIBELLE       TO TSTENVOI-LIBELLE
                    PERFORM ECRITURE-TSTENVOI
                 END-IF
                 PERFORM FETCH-CXCTRL
                 IF WC-XCTRL-EXPDEL-PB-DB2
                    SET WC-TSTENVOI-ERREUR TO TRUE
                    GO TO FIN-CREATION-TSTENVOI
                 END-IF
              END-PERFORM
              PERFORM CLOSE-CXCTRL
      * AJOUT D'UN ENREG DE FIN
              MOVE '99999'                   TO TSTENVOI-NSOCZP
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM
              MOVE 'FIN TSTENVOI '           TO TSTENVOI-LIBELLE
              PERFORM ECRITURE-TSTENVOI.
       FIN-CREATION-TSTENVOI.  EXIT.
      *
      *-----------------------------------------------------------------
       LECTURE-TSTENVOI SECTION.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS
      *         READQ TS
      *         QUEUE  (WC-ID-TSTENVOI)
      *         INTO   (TS-TSTENVOI-DONNEES)
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
      *         ITEM   (WP-RANG-TSTENVOI)
      *         NOHANDLE
      *    END-EXEC.
      *--
           EXEC CICS READQ TS
                QUEUE  (WC-ID-TSTENVOI)
                INTO   (TS-TSTENVOI-DONNEES)
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
                ITEM   (WP-RANG-TSTENVOI)
                NOHANDLE
           END-EXEC.
      *}
           EVALUATE EIBRESP
           WHEN 0
              SET WC-TSTENVOI-SUITE      TO TRUE
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE
           WHEN 26
           WHEN 44
              SET WC-TSTENVOI-FIN        TO TRUE
           WHEN OTHER
              SET WC-TSTENVOI-ERREUR     TO TRUE
           END-EVALUATE.
       FIN-LECT-TSTENVOI. EXIT.
      *
       ENVOI-DUPLI                    SECTION.
      *---------------------------------------
      *   ENVOI DANS LE MAG OU ON REPREND LA VENTE
           MOVE  GV10-NSOCIETE       TO      COMM-MQ20-NSOC
           MOVE  '000'               TO      COMM-MQ20-NLIEU
           MOVE  'IV1'               TO      COMM-MQ20-FONCTION
           MOVE  'TEC02'             TO      COMM-MQ20-NOMPROG
           MOVE  WS-LONG-MESSAGE     TO      COMM-MQ20-LONGMES
           MOVE  '00'                TO      COMM-MQ20-CODRET.
           MOVE  SPACES              TO      COMM-MQ20-CORRELID
           EXEC CICS LINK  PROGRAM  ('MMQ20')
                           COMMAREA (COMM-MQ20-APPLI)
                           LENGTH   (LENGTH OF COMM-MQ20-APPLI)
                           NOHANDLE
           END-EXEC.
      
           MOVE EIBRCODE TO EIB-RCODE.
           IF   NOT EIB-NORMAL
               STRING 'LINK MODULE MMQ20 MAL TERMINE '
               DELIMITED BY SIZE INTO W-MESSAGEs
               PERFORM DISPLAY-W-MESSAGE
               MOVE '9' TO W-MEC31-CODE-RET
               PERFORM MODULE-SORTIE
           END-IF.
      
      *
       FIN-ENVOI-DUPLI. EXIT.
      *
      *-----------------------------------------------------------------
       ECRITURE-TSTENVOI SECTION.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS
      *         WRITEQ TS
      *         QUEUE  (WC-ID-TSTENVOI)
      *         FROM   (TS-TSTENVOI-DONNEES)
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
      *         NOHANDLE
      *    END-EXEC.
      *--
           EXEC CICS WRITEQ TS
                QUEUE  (WC-ID-TSTENVOI)
                FROM   (TS-TSTENVOI-DONNEES)
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
                NOHANDLE
           END-EXEC.
      *}
           INITIALIZE TS-TSTENVOI-DONNEES.
       FIN-ECRIT-TSTENVOI. EXIT.
      *
      *-----------------------------------------------------------------
       DECLARE-OPEN-CXCTRL SECTION.
           MOVE FUNC-DECLARE             TO TRACE-SQL-FUNCTION.
           MOVE 'RVGA01ZZ'               TO TABLE-NAME.
           MOVE 'EXPDEL'                 TO MODEL-NAME.
           EXEC SQL
                DECLARE CXCTRL CURSOR FOR
                SELECT SOCZP, CTRL, WPARAM, LIBELLE
                FROM RVGA01ZZ
                WHERE TRANS = 'EXPDEL'
                AND   WFLAG = 'O'
                ORDER BY SOCZP, CTRL
           END-EXEC.
           PERFORM TEST-SQLCODE.
           MOVE FUNC-OPEN                TO TRACE-SQL-FUNCTION.
           EXEC SQL OPEN CXCTRL END-EXEC.
           PERFORM TEST-SQLCODE.
      *
       FETCH-CXCTRL SECTION.
           MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.
           EXEC SQL
                FETCH CXCTRL
                INTO :XCTRL-SOCZP
                   , :XCTRL-CTRL
                   , :XCTRL-WPARAM
                   , :XCTRL-LIBELLE
           END-EXEC.
           PERFORM TEST-SQLCODE.
           IF WC-XCTRL-EXPDEL-SUITE
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE
           END-IF.
      *
       CLOSE-CXCTRL SECTION.
           MOVE FUNC-CLOSE               TO TRACE-SQL-FUNCTION.
           EXEC SQL CLOSE CXCTRL  END-EXEC.
           PERFORM TEST-SQLCODE.
      *
       INSERT-RTPS00                SECTION.
      *-------------------------------------
      *
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION
           EXEC SQL INSERT
                    INTO    RVPS0001
                          (
                              NSOC       ,
                              CGCPLT     ,
                              NGCPLT     ,
                              NLIEU      ,
                              NVENTE     ,
                              DDELIV     ,
                              DCOMPTA    ,
                              CINSEE     ,
                              NCODIC     ,
                              MTGCPLT    ,
                              DANNUL     ,
                              MTREMB     ,
                              DREMB      ,
                              MTRACHAT   ,
                              DRACHAT    ,
                              NOMCLI     ,
                              DTRAIT     ,
                              DSYST      ,
                              DTRAITR
                                          )
                    VALUES (
                        :PS00-NSOC       ,
                        :PS00-CGCPLT     ,
                        :PS00-NGCPLT     ,
                        :PS00-NLIEU      ,
                        :PS00-NVENTE     ,
                        :PS00-DDELIV     ,
                        :PS00-DCOMPTA    ,
                        :PS00-CINSEE     ,
                        :PS00-NCODIC     ,
                        :PS00-MTGCPLT    ,
                        :PS00-DANNUL     ,
                        :PS00-MTREMB     ,
                        :PS00-DREMB      ,
                        :PS00-MTRACHAT   ,
                        :PS00-DRACHAT    ,
                        :PS00-NOMCLI     ,
                        :PS00-DTRAIT     ,
                        :PS00-DSYST      ,
                        :PS00-DTRAITR      )
           END-EXEC
           IF SQLCODE = -803
              MOVE 0 TO SQL-CODE
           END-IF.
      *
       FIN-INSERT-RTPS00. EXIT.
      *
       SELECT-GV10-NSEQNQ SECTION.
      *-------------------------------
           MOVE GV11-NSOCIETE      TO GV10-NSOCIETE
           MOVE GV11-NLIEU         TO GV10-NLIEU
           MOVE GV11-NVENTE        TO GV10-NVENTE
           EXEC SQL SELECT NSEQNQ
                      INTO :GV10-NSEQNQ :GV10-NSEQNQ-f
                      FROM RVGV1005
                     WHERE NSOCIETE = :GV10-NSOCIETE
                       AND NLIEU    = :GV10-NLIEU
                       AND NVENTE   = :GV10-NVENTE
           END-EXEC.
           IF SQLCODE NOT   = 0 AND 100
              MOVE SPACES TO W-MESSAGE
              STRING 'select GV10'
                                   ' NSOCIETE: ' GV10-NSOCIETE
                                      ' NLIEU: ' GV10-NLIEU
                                     ' NVENTE: ' GV10-NVENTE
              DELIMITED BY SIZE INTO W-MESSAGE
              PERFORM DISPLAY-W-MESSAGE
           END-IF
           PERFORM TEST-SQLCODE.
       FIN-SELECT-GV10-NSEQNQ. EXIT.
       MAJ-GV10-NSEQNQ    SECTION.
      *-------------------------------
           MOVE GV11-NSOCIETE      TO GV10-NSOCIETE
           MOVE GV11-NLIEU         TO GV10-NLIEU
           MOVE GV11-NVENTE        TO GV10-NVENTE
           EXEC SQL UPDATE
                     RVGV1005
                     SET  NSEQNQ    = :GV10-NSEQNQ
                     WHERE NSOCIETE = :GV10-NSOCIETE
                       AND NLIEU    = :GV10-NLIEU
                       AND NVENTE   = :GV10-NVENTE
           END-EXEC.
           IF SQLCODE NOT   = 0 AND 100
              MOVE SPACES TO W-MESSAGE
              STRING 'UPDATE GV10'
                                   ' NSOCIETE: ' GV10-NSOCIETE
                                      ' NLIEU: ' GV10-NLIEU
                                     ' NVENTE: ' GV10-NVENTE
              DELIMITED BY SIZE INTO W-MESSAGE
              PERFORM DISPLAY-W-MESSAGE
           END-IF
           PERFORM TEST-SQLCODE.
       FIN-MAJ-GV10-NSEQNQ. EXIT.
      * MODE DE PAIEMENT DARTY.COM: EN LIGNE O/N
       SELECT-EC008 SECTION.
           MOVE SPACES TO EC008-WTABLEG.
           EXEC SQL SELECT WTABLEG INTO :EC008-WTABLEG
                      FROM RVGA0100
                     WHERE CTABLEG1 = 'EC008'
                       AND CTABLEG2 = :EC008-CTABLEG2
           END-EXEC.
           IF SQLCODE =  +100
              STRING 'MODE PAIEMENT INCONNU : '
                 W-MEC31-MD-PMENT
                 DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
           END-IF
           PERFORM TEST-SQLCODE.
       DELETE-TSTENVOI SECTION.
      ******************************
           EXEC CICS DELETEQ TS
                QUEUE    (WC-ID-TSTENVOI)
                NOHANDLE
           END-EXEC.
       FIN-DELETE-TSTENVOI. EXIT.
      ********************************
      * Erreur MQ
      *ERREUR-MQ SECTION.
      *    MOVE 'EC31: REASON: rrrrrr CODE: ccc' TO W-MESSAGEs
      *    MOVE W-MQ-REASON             TO W-MQ-REASON-DISPLAY
      *    MOVE W-MQ-REASON-DISPLAY     TO W-MESSAGEs(15:6)
      *    MOVE W-MQ-BINARYCODE         TO W-MQ-BINARYCODE-DISPLAY
      *    MOVE W-MQ-BINARYCODE-DISPLAY TO W-MESSAGEs(28:3)
      *    PERFORM DISPLAY-W-MESSAGE.
      *    EXEC CICS ABEND ABCODE ('MQ  ') END-EXEC.
      *
       CREE-MOUCHARD-LIGNE SECTION.
           INITIALIZE MEC15C-COMMAREA.
           MOVE W-MEC31-NCDEWC    TO MEC15C-NCDEWC.
           MOVE W-MEC31-NSOC      TO MEC15C-NSOCIETE.
           MOVE W-MEC31-NLIEU     TO MEC15C-NLIEU.
           MOVE W-MEC31-NVENTE    TO MEC15C-NVENTE.
           MOVE GV11-NSEQNQ       TO MEC15C-NSEQNQ.
      *    MOVE 0                 TO MEC15C-NSEQNQ.
           MOVE 'I'               TO MEC15C-STAT.
      *    MOVE '0025'            TO MEC15C-NSEQERR.
      *    MOVE W-MEC31-PSE(W-PSE-R-I) TO MEC15C-ALP(1).
      *    MOVE W-MEC31-NCODIC(W-PSE-R-I)  TO MEC15C-NUM(1).
           STRING 'AJOUT ' W-MEC31-C-PSE(W-PSE-R-I) ' SUR LE CODIC '
           W-MEC31-NCODIC(W-PSE-R-I)
           DELIMITED BY SIZE INTO MEC15C-TEXTE
      *    MOVE MEC10C-NRESERVATION(5:) TO PX-NORDRE.
      *    MOVE PD-NORDRE               TO P9-NORDRE.
      *    MOVE P9-NORDRE               TO MEC15C-ALP(1).
           EXEC CICS START TRANSID ('EC15') FROM (MEC15C-COMMAREA)
           END-EXEC.
       TEST-SQLCODE SECTION.
            EVALUATE TRUE
            WHEN SQLCODE = 0
                 SET WC-XCTRL-EXPDEL-SUITE    TO TRUE
            WHEN SQLCODE = +100
                 SET WC-XCTRL-EXPDEL-FIN      TO TRUE
            WHEN OTHER
                 SET WC-XCTRL-EXPDEL-PB-DB2   TO TRUE
                 SET WC-XCTRL-EXPDEL-FIN      TO TRUE
                 MOVE SQLCODE   TO WZ-XCTRL-EXPDEL-SQLCODE
           END-EVALUATE.
      
