      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA MODULES D'EDITION DES PLANS DE COMPTES / SECTIONS /  *         
      * RUBRIQUES                                                     *         
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-FT99X-LONG-COMMAREA PIC S9(3)  COMP VALUE +176.                 
      *--                                                                       
       01  COMM-FT99X-LONG-COMMAREA PIC S9(3) COMP-5 VALUE +176.                
      *}                                                                        
       01  Z-COMMAREA-MFT99X.                                                   
           05  FT99X-ENTITE        PIC X(5).                                    
           05  FT99X-LENTITE       PIC X(30).                                   
           05  FT99X-NSOC          PIC X(5).                                    
           05  FT99X-LSOC          PIC X(30).                                   
           05  FT99X-NETAB         PIC X(3).                                    
           05  FT99X-LETAB         PIC X(30).                                   
           05  FT99X-CPX           PIC X(4).                                    
           05  FT99X-CPLAN         PIC X.                                       
           05  FT99X-SEL           PIC X(6).                                    
           05  FT99X-ETENDUE       PIC X.                                       
           05  FT99X-CODE-RETOUR   PIC X.                                       
           05  FT99X-MESSAGE       PIC X(60).                                   
                                                                                
