      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : LOGISTIQUE GROUPE                                *        
      *  PROGRAMME  : MFL005                                           *        
      *  TITRE      : COMMAREA DU MODULE BATCH MFL005                  *        
      *               DETERMINATION DES ENTREPOTS DE STOCKAGE          *        
      *               D'UN CODIC OU D'UNE FAMILLE                      *        
      *  LONGUEUR   : 4096 C                                           *        
      *                                                                *        
      *  CONTENU    : CETTE COMMAREA CONTIENT LES DEMANDES             *        
      *  REMARQUE   : LA ZONE COMM-FL05-CFAM EST OBLIGATOIRE           *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MFL05-APPLI.                                                    
      *--- DONNEES LIEU DEMANDEUR/TYPE TRAIT                 90X44 = 100        
           05 COMM-MFL05-DONNES-PARAMETRES.                                     
              10 COMM-MFL05-ZONES-ENTREE.                                       
                 15 COMM-MFL05-NSOCIETE       PIC X(03).                        
                 15 COMM-MFL05-NLIEU          PIC X(03).                        
                 15 COMM-MFL05-CTYPTRAIT      PIC X(05).                        
                 15 COMM-MFL05-DEFFET         PIC X(08).                        
              10 COMM-MFL05-ZONES-SORTIE.                                       
                 15 COMM-MFL05-MESSAGE.                                         
                    20 COMM-MFL05-CODRET         PIC X(01).                     
                       88 COMM-MFL05-CODRET-OK                VALUE ' '.        
                       88 COMM-MFL05-CODRET-ERR-LIEU          VALUE '1'.        
                       88 COMM-MFL05-CODRET-ERR-DEMANDE       VALUE '2'.        
                       88 COMM-MFL05-CODRET-ERR-SQL           VALUE '3'.        
                    20 COMM-MFL05-LIBERR.                                       
                       25 COMM-MFL05-NSEQERR        PIC X(04).                  
                       25 COMM-MFL05-ERRFIL         PIC X(01).                  
                       25 COMM-MFL05-LMESSAGE       PIC X(53).                  
                 15 COMM-MFL05-NPRIORITE         PIC X(05).                     
                 15 COMM-MFL05-CPROAFF           PIC X(05).                     
              10 COMM-MFL05-FILLER1        PIC X(12).                           
      *--- TABLEAU DES DEMANDES (CODIC/FAM)                 90X44 = 3960        
           05 COMM-MFL05-IPOSTE-MAX        PIC S9(03) COMP-3  VALUE 90.         
           05 COMM-MFL05-DEMANDE-POSTE  OCCURS 90.                              
              10 COMM-MFL05-TYPE-DEMANDE   PIC X(01).                           
                 88 COMM-MFL05-PAS-DEMANDE                 VALUE ' '.           
                 88 COMM-MFL05-DEMANDE-CODIC               VALUE 'C'.           
                 88 COMM-MFL05-DEMANDE-FAMILLE             VALUE 'F'.           
              10 COMM-MFL05-NCODIC         PIC X(07).                           
              10 COMM-MFL05-CFAM           PIC X(05).                           
              10 COMM-MFL05-KONTROL        PIC X(01).                           
                 88 COMM-MFL05-OK                          VALUE ' '.           
                 88 COMM-MFL05-ERREUR                      VALUE '1'.           
              10 COMM-MFL05-DEPOT          OCCURS 5.                            
                 20 COMM-MFL05-NSOC           PIC X(03).                        
                 20 COMM-MFL05-NDEPOT         PIC X(03).                        
           05 COMM-MFL05-FILLER2        PIC X(34).                              
                                                                                
