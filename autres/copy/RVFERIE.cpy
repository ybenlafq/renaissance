      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE FERIE TABLE DES JOURS F�RI�S           *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFERIE .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFERIE .                                                            
      *}                                                                        
           05  FERIE-CTABLEG2    PIC X(15).                                     
           05  FERIE-CTABLEG2-REDEF REDEFINES FERIE-CTABLEG2.                   
               10  FERIE-CODE            PIC X(05).                             
               10  FERIE-NSEQ            PIC X(02).                             
               10  FERIE-DATEMMJJ        PIC X(04).                             
           05  FERIE-WTABLEG     PIC X(80).                                     
           05  FERIE-WTABLEG-REDEF  REDEFINES FERIE-WTABLEG.                    
               10  FERIE-TOP             PIC X(01).                             
               10  FERIE-LIB             PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVFERIE-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVFERIE-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FERIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  FERIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FERIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  FERIE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
