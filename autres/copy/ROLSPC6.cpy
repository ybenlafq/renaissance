      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION OP LIGNE STANDARD (MUTATION)          * 00030000
      * NOM FICHIER.: ROLSPC6                                         * 00031000
      *---------------------------------------------------------------* 00032000
      * CR   .......: 27/08/2013                                      * 00033000
      * MODIFIE.....:   /  /                                          * 00034000
      * VERSION N�..: 001                                             * 00035000
      * LONGUEUR....: 168                                             * 00036000
      ***************************************************************** 00037000
      *                                                                 00037100
       01  ROLSPC6.                                                     00037200
      * TYPE ENREGISTREMENT                                             00037300
           05      ROLSPC6-TYP-ENREG      PIC  X(0006).                 00037400
      * CODE SOCI�T�                                                    00037500
           05      ROLSPC6-CSOCIETE       PIC  X(0005).                 00037600
      * NUM�RO D OP                                                     00037700
           05      ROLSPC6-NOP            PIC  X(0015).                 00037800
      * NUM�RO DE LIGNE                                                 00037900
           05      ROLSPC6-NLIGNE         PIC  9(0005).                 00038000
      * NUM�RO DE SOUS-LIGNE                                            00038100
           05      ROLSPC6-NSSLIGNE       PIC  9(0005).                 00038200
      * CODE FA�ONNAGE                                                  00038300
           05      ROLSPC6-CODE-FACONNAGE PIC  9(0002).                 00038400
      * N� DE LIGNE DE COMMANDE                                         00038500
           05      ROLSPC6-NLIGNE-CDE     PIC  9(0003).                 00038600
      * DATE DE COMMANDE CLIENT                                         00038700
           05      ROLSPC6-DCDE           PIC  X(0008).                 00038800
      * COMMENTAIRE COMMANDE CLIENT                                     00038900
           05      ROLSPC6-COMMENTAIRE-CDE PIC X(0035).                 00039000
      * FAMILLE PRODUIT                                                 00040000
           05      ROLSPC6-CFAM           PIC  X(0006).                 00050000
      * LIBELL� FAMILLE PRODUIT                                         00060000
           05      ROLSPC6-LFAM           PIC  X(0035).                 00070000
      * TYPE DE FLUX                                                    00080000
           05      ROLSPC6-TYPE-FLUX      PIC  X(0002).                 00090000
      * PRIX DE VENTE AU CLIENT FINAL                                   00100000
           05      ROLSPC6-PRIX-VENTE     PIC  9(0010).                 00110000
      * FILLER 30                                                       00120000
           05      ROLSPC6-FILLER         PIC  X(0030).                 00130000
      * FIN                                                             00140000
           05      ROLSPC6-FIN            PIC  X(0001).                 00150000
                                                                                
