      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVDBS050                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVDBS050                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVDBS050.                                                            
           02  VCV-CORP                                                         
               PIC X(0005).                                                     
           02  VCV-ENAME                                                        
               PIC X(0008).                                                     
           02  VCV-EVAL                                                         
               PIC X(0020).                                                     
           02  VCV-MSK                                                          
               PIC X(0020).                                                     
           02  VCV-IE                                                           
               PIC X(0001).                                                     
           02  VCV-DESC                                                         
               PIC X(0035).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVDBS050                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVDBS050-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VCV-CORP-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VCV-CORP-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VCV-ENAME-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VCV-ENAME-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VCV-EVAL-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VCV-EVAL-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VCV-MSK-F                                                        
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VCV-MSK-F                                                        
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VCV-IE-F                                                         
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VCV-IE-F                                                         
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VCV-DESC-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VCV-DESC-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
