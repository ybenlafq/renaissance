      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * - MESMMU00 COPY GENERALISES  49974 OCTETS                      *      13
      *   = 105 DE DEFINITION D ENTETE                                 *        
      *   + 34  DE DEFINITION DE CODE RETOUR                           *        
      *   + 49835 DE MESSAGE REEL                                      *        
      * COPY POUR LA SERIE DE TRANSACTION MTL45 <-> MMU00              *      13
      ******************************************************************        
              05 MESMMU00-ENTETE.                                               
                 10 MESMMU00-TYPE        PIC X(03).                             
                 10 MESMMU00-NSOCMSG     PIC X(03).                             
                 10 MESMMU00-NLIEUMSG    PIC X(03).                             
                 10 MESMMU00-NSOCDST     PIC X(03).                             
                 10 MESMMU00-NLIEUDST    PIC X(03).                             
                 10 MESMMU00-NORD        PIC 9(08).                             
                 10 MESMMU00-LPROG       PIC X(10).                             
                 10 MESMMU00-DJOUR       PIC X(08).                             
                 10 MESMMU00-WSID        PIC X(10).                             
                 10 MESMMU00-USER        PIC X(10).                             
                 10 MESMMU00-CHRONO      PIC 9(07).                             
                 10 MESMMU00-NBRMSG      PIC 9(07).                             
                 10 MESMMU00-NBRENR      PIC 9(5).                              
                 10 MESMMU00-TAILLE      PIC 9(5).                              
                 10 MESMMU00-VERSION     PIC X(2).                              
                 10 MESMMU00-FILLER      PIC X(18).                             
              05 MESMMU00-RETOUR.                                               
                 10 MESMMU00-CODE-RETOUR.                                       
                    15  MESMMU00-CODRET  PIC X(1).                              
                         88  MESMMU00-CODRET-OK        VALUE ' '.               
                         88  MESMMU00-CODRET-ERREUR    VALUE '1'.               
                    15  MESMMU00-LIBERR  PIC X(33).                             
              05 MESMMU00-DATA           PIC X(49835).                          
              05 MESMMU00C-DATA REDEFINES MESMMU00-DATA.                        
                 10 MESMMU00C-DATA-X.                                           
                    15 MESMMU00-NBL             PIC  X(7).                      
                    15 MESMMU00-NBMUT           PIC  S9(5) COMP-3.              
                    15 MESMMU00-WMAJGS10-EMET   PIC  X(1).                      
                    15 MESMMU00-WDACEM          PIC  X(1).                      
                    15 MESMMU00-DEXPEDITION     PIC  X(8).                      
                    15 MESMMU00-DETAIL          OCCURS 1300.                    
                       20 MESMMU00-NMUTATION    PIC X(7).                       
                       20 MESMMU00-MUT-OK       PIC X.                          
                       20 MESMMU00-GESTION-GV21 PIC  X(1).                      
                       20 MESMMU00-FILLER       PIC X(14).                      
                    15 MESMMU00-FILLER          PIC  X(20).                     
                 10 MESMMU00-DATA-INUTILE       PIC X(19895).                   
                                                                                
