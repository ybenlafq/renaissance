      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CPFIL CODE POSTAUX PAR FILIALE.        *        
      *----------------------------------------------------------------*        
       01  RVCPFIL.                                                             
           05  CPFIL-CTABLEG2    PIC X(15).                                     
           05  CPFIL-CTABLEG2-REDEF REDEFINES CPFIL-CTABLEG2.                   
               10  CPFIL-CPOSTAL         PIC X(05).                             
           05  CPFIL-WTABLEG     PIC X(80).                                     
           05  CPFIL-WTABLEG-REDEF  REDEFINES CPFIL-WTABLEG.                    
               10  CPFIL-LFIL            PIC X(04).                             
               10  CPFIL-NSOC            PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCPFIL-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CPFIL-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CPFIL-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CPFIL-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CPFIL-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
