      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * CETTE COPY EST EN FAIT UN PROGRAMME ENTIER; ELLE EST UTILISEE           
      * PAR TOUS LES PROGRAMMES MECCCX, OU X EST LE SUFFIXE FILIALE             
      * CE PROGRAMME CR�E LES VENTES POUR DARTY.COM                             
      ******************************************************************        
      * ----------------------------------------------------------------        
      * MAINTENANCE   : 10/08/2009                                              
      * MARQUE AL1008 : LORSQUE QUE L'ON RECOIT UNE PSE, ON LA PLACE            
      *                 SUR CHAQUE PRODUIT IDENTIQUE                            
      * ----------------------------------------------------------------        
      ******************************************************************        
AL1008* CE TABLEAU VA SERVIR A EXTRAIRE LES LIGNES 'SERVICES'                   
      * IL SERA UTILISE PAR LA SUITE POUR CREER LA STRUCTURE DE                 
      * DEPENDANCE ENTRE LES PRODUITS ET CES 'SERVICES'.                        
      ******************************************************************        
NV2209* TO EXTEND THE SYSTEM OF RESERVATION ON SUPPLIER ORDERS                  
NV2209* 22 SEP 2009    DSA196                                                   
NV2209* MODLOG  NV2209     NOBLE K V  BANGALORE                                 
      ******************************************************************        
LD1109* 05/11/2009  DSA029 MODIFICATION PVUNITF DANS CAS DES BUNDDLES *         
      ******************************************************************        
LD1209* 05/11/2009  DSA029 MODIFICATION PVUNITF DANS CAS DES BUNDDLES *         
LD1209* 05/11/2009  DSA029 POUR LES EXPEDIES                          *         
      ******************************************************************        
LD0310* 19/04/2010  DSA029 MODIFICATION POUR COMMANDE FOURNISSEUR     *         
LD0310* 19/04/2010  DSA029 EXPEDIES                                   *         
      ******************************************************************        
LD0410* 19/04/2010  DSA029 MODIFICATION POUR ALIMENTER LA TVA DANS    *         
LD0410* 19/04/2010  DSA029 LA COMMECCC                                *         
      ******************************************************************        
L20410* 19/04/2010  DSA029 MODIFICATION POUR CHRONOPOST               *         
      ******************************************************************        
L30410* 30/04/2010  DSA029 MODIFICATION COMMANDE FOURNISSEURS         *         
      ******************************************************************        
LD0610* 08/06/2010  DSA029 MODIFICATION POUR CHRONOPOST               *         
      ******************************************************************        
V33   * 09/07/2010  FT CHOIX DE LA PRESTA                             *         
      ******************************************************************        
569549* 22/09/2010  LD CORRECTION LD2 MISE EN Z                       *         
      ******************************************************************        
V34CEN* 02/11/2010  FT CORRECTION ANO PRESTA EXPED/LD2                *         
      ******************************************************************        
V3.5.2* 02/11/2010  AJOUT DES MESSAGE POUR EC15                       *         
V40R0 * 02/11/2010  ALIMENTATION DE LA TABLE RTGV99 POUR PRECOMMANDE  *         
      ******************************************************************        
V34R4 * 01/02/2011  MODIFICATION DE L'APPEL AU MGV65                  *         
      ******************************************************************        
V41R0 * 09/03/2011  DE02003 (C.LAVAURE)  KIALA + MONOEQUIPAGE                   
      ******************************************************************        
V41R1 * 25/05/2011  DE02074 correction erreur sur frais d'envoi                 
      ******************************************************************        
      ******************************************************************        
j0j1  * 16/06/2011  DE01002 evolution suite J0                                  
j0j1  *            prendre les mutations relatives au mode de delivrance        
      ******************************************************************        
V41R1 * 24/06/2011  FT CORRECTION ERREUR SUR EMAIL SUITE KIALA                  
      ******************************************************************        
V43R0 * 08/07/2011  EG AJOUT GESTION CODE VENDEUR                               
V43R0 *                AJOUT nordre lors de l'appel au mgv65                    
      ******************************************************************        
V43R1 * 26/09/2011  EG AJOUT GESTION dispo des cf                               
      ******************************************************************        
      ******************************************************************        
M16733* 29/09/2011  APPEL MEC03 POUR ENVOI POSITION STOCK A DARTY.COM           
M16733* 29/09/2011  APPEL MEC24 POUR ENVOI POSITION QUOTA A DARTY.COM           
V50R1 * 21/02/2012  modifICATION appel MEC24 +                                  
      ******************************************************************        
      ******************************************************************        
V5.1P * 13/03/2012 !DCOM LIVRAISON SOIREE                                       
      *            !ON CHERCHE DANS LES SERVICES UN SERVICE DIT DE              
      *            !LIVRAISON, S'IL EST CADRE ET DANS CE CAS ACC�S              
      *            !� LA TABLE RTGQ20 pour nouvelle zone elementaire            
      ******************************************************************        
PORDRE* 22/05/2012 Initialiser � z�ro GV10-NORDRE et                            
      *     l'ajouter dans les criteres de selection du MAX(NORDRE)             
      * J.BICHY                                                                 
      ******************************************************************        
LA1008* 10/08/2012  DE02001 EVOLUTION : IMPLEMENTATION DES COMMANDES            
      *                                 CONTREMARQUE                            
      ******************************************************************        
M19892* 16/08/2012  J. BICHY : Pb GV10 des ventes 040 livr�es en                
      *                        particulier le NSEQNQ � 0                        
      ******************************************************************        
q6107 * 02/10/2012  J. BICHY : performance DB2                                  
      ******************************************************************        
LA2203* 22/03/2013  DE02001 CORRECTIF CEN NOEL 2012                             
      *             REMPLACEMENT DU MODULE MGV65 PAR LE MEC64                   
      ******************************************************************        
promo *01/07/2013 : evolution code promo                               *        
      ******************************************************************        
M23862*25/07/2013   DE02009 Mantis 23862                               *        
      *             Projet PSE ASSURANTIELLE                           *        
      ******************************************************************        
V6.5  * 21/08/2013 ! DE02013! Ajout RTEC04 pour C&C consignes                   
      ******************************************************************        
M25592* 02/12/2013 ! DE02009! C&C avec paiement au magasin                      
      ******************************************************************        
NSKEP * 22/05/2014 - DE02001                                           *        
      * PRISE EN COMPTE DES COMMANDES NSKEP                            *        
      ******************************************************************        
LA1806* 18/06/2014 - DE02001                                           *        
      * CORRECTION ALIMENTATION NSEQ DE LA RTGF00                      *        
      ******************************************************************        
stkpf * 06/08/2014 - DE01002                                           *        
      * livraison stock plateforme  interfacer mgv48                   *        
      ******************************************************************        
MP839 * 01/10/2014 - DE02009                                           *        
      * Suite JIRA MP-839 la vente ne s'int�gre pas                    *        
      * Ajouter le codic groupe dans les �l�ments sauvgard�s           *        
      ******************************************************************        
151014* 15/10/2014 - DE02009                                           *        
      * correction bug sur la r�servation sur commande fournisseur     *        
      ******************************************************************        
041114* 15/10/2014 - DE02009                                           *        
      * correction sur prise de quotas mutations                       *        
      * on ne prenait pas la mutation si on atteignait exactement les  *        
      * quotas volume ou pieces                                        *        
      ******************************************************************        
co0611* 06/11/2014 - DE01002                                           *        
      * correction pour bundle ne contenant que des element en quantit�*        
      * multiple. solution ajouter un service d'ajustement pour        *        
      * retrouver le total de vente                                    *        
      ******************************************************************        
MGD15 *04/12/2014  de02009 PROJET MGD : int�gration des ventes MGD              
      ******************************************************************        
1000M *26/03/2015  de02009 1000 Mercis:Ajout des zones date et heure msg        
      ******************************************************************        
xdock *17/06/2015  cross dock                                          *        
      ******************************************************************        
ddest *petit correctif date de destockage pour s�curiser la prise de   *        
      * mutation                                                       *        
      ******************************************************************        
CCPRM *25/08/2015  carte cadeau par moteur de CCPRM                    *        
      ******************************************************************        
      ******************************************************************        
cr1711*17/11/2015  pb remise a blanc numero de mutation si crossdock z *        
      ******************************************************************        
comliv*11/12/2015  Si commentaire de liv > 78 car, alors il n'est pas  *        
      *            ajout� dans EC02                                    *        
      ******************************************************************        
0116  *04/01/2016  Initializer la zone CEQUIPE                         *        
      ******************************************************************        
       01 calcul-xdock            pic 9.                                        
          88 cherche-xdock        value 0.                                      
          88 cherche-pas-xdock    value 1.                                      
ccprm  01 W-PRESTA-KDO            pic 9.                                        
 "        88 PRESTA-KDO-ko        value 0.                                      
 "        88 PRESTA-KDO-OK        value 1.                                      
       01 w-nseqens               PIC S9(5) COMP-3 value 0.                     
sykav  01 eib-rcode.                                                            
          05 eibr   pic x(01) value low-value.                                  
          05 filler pic x(05).                                                  
V41R0  77  WS-WEMPORTE            PIC X.                                        
V43R0  77  W-DATE-ACTIVATION-GF55     PIC X(8).                                 
V41R0  77  WS-MASQZ2              PIC ZZ.                                       
       77  W-TVA                  PIC ZZV99.                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-CC-S-MAX             PIC S9(4) BINARY VALUE 50.                    
      *--                                                                       
       77  W-CC-S-MAX             PIC S9(4) COMP-5 VALUE 50.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-CC-S-I               PIC S9(4) BINARY VALUE 0.                     
      *--                                                                       
       77  W-CC-S-I               PIC S9(4) COMP-5 VALUE 0.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-CC-S-U               PIC S9(4) BINARY VALUE 0.                     
      *--                                                                       
       77  W-CC-S-U               PIC S9(4) COMP-5 VALUE 0.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  W-V-U                  PIC S9(4) BINARY VALUE 0.                     
      *--                                                                       
       77  W-V-U                  PIC S9(4) COMP-5 VALUE 0.                     
      *}                                                                        
       01  W-TAB-SERVICES.                                                      
         02  W-TABL-SERVICES occurs 50.                                         
           03  W-CC-S-CTYPENREG   PIC X(1).                                     
           03  W-CC-S-CENREG      PIC X(5).                                     
           03  W-CC-S-codic       PIC X(7).                                     
           03  W-CC-S-codicgrp    PIC X(7).                                     
           03  W-CC-S-PX-VT       PIC S9(7)V99 PACKED-DECIMAL.                  
M23862     03  W-CC-S-NUM-PS      PIC X(8).                                     
M23862     03  W-CC-S-FAIT        PIC X(1).                                     
M16733 01      WS-GV10-NVENTE-SAUVE     PIC X(7).                               
       01      WS-GV10-NSOCIETE-SAUVE   PIC X(3).                               
       01      WS-GV10-NLIEU-SAUVE      PIC X(3).                               
Q6107  01  WS-MOD-PLAG-EQUIP.                                                   
"          03  WS-CMODDEL         PIC X(03).                                    
"          03  WS-CPLAGE          PIC X(02).                                    
"          03  WS-CEQUIP          PIC X(05).                                    
       01  WS-MOD-PLAG-EQUIP-r   REDEFINES WS-MOD-PLAG-EQUIP PIC x(10).         
M25592 01  WS-PREPARATION.                                                      
"          03  WS-DATE-PREPA      PIC X(08).                                    
M25592     03  WS-HEURE-PREPA     PIC X(04).                                    
CCPRM  01  WS-CENREG              PIC X(5).                                     
      ******************************************************************        
      * ZONES POUR GENERATION DES LIGNES DE VENTES...                           
      ******************************************************************        
      ******************************************************************        
       01  W-GV10-NVENTE-PICX7   PIC X(7).                                      
       01  W-GV10-NVENTE-PICS913 REDEFINES W-GV10-NVENTE-PICX7                  
                                 PIC S9(13) PACKED-DECIMAL.                     
       01  W-GV10-NVENTE-PICX13  PIC X(13).                                     
       01  W-GV10-NVENTE-PIC913  REDEFINES W-GV10-NVENTE-PICX13                 
                                 PIC 9(13).                                     
       01  W-GV10-NORDRE-PICX    PIC X(5).                                      
       01  W-GV10-NORDRE-PIC9    REDEFINES W-GV10-NORDRE-PICX PIC 9(5).         
       01  W-GV11-NLIGNE-PICXX   PIC XX.                                        
       01  W-GV11-NLIGNE-PIC99   REDEFINES W-GV11-NLIGNE-PICXX                  
                                 PIC 99.                                        
       01  W-GV11-NSEQNQ         PIC S9(5) COMP-3.                              
V34CEN 01  W-GV11-NCODIC         PIC X(7).                                      
      ******************************************************************        
      *...LES FLAGS QUI COMME D'HABITUDE SONT BIEN PRATIQUES...                 
      ******************************************************************        
       01  W-CMODEL-EMD             PIC 9(01).                                  
           88 W-CMODEL-EMD-OUI      VALUE 1.                                    
           88 W-CMODEL-EMD-NON      VALUE 0.                                    
       01  FILLER             PIC X.                                            
       88  PARTIE-LIVREE-A-CREER      VALUE '1'.                                
       88  NE-PAS-CREER-LIVRAISON     VALUE '0'.                                
       01  FILLER             PIC X.                                            
       88  PARTIE-EXPEDIEE-A-CREER    VALUE '1'.                                
       88  NE-PAS-CREER-EXPEDITION    VALUE '0'.                                
       01  FILLER             PIC X.                                            
       88  IL-Y-A-UN-ENTREPOT        VALUE '1'.                                 
       88  YA-PAS-D-ENTREPOT         VALUE '0'.                                 
       01  GV10-WCQERESF-FLAG PIC X.                                            
       88  GV10-WCQERESF-NORMAL      VALUE SPACE.                               
       88  GV10-WCQERESF-EN-Z        VALUE 'Z'.                                 
       01  colle-pse                 pic 9  value 0.                            
           88 pse-pas-collee                value 0.                            
           88 pse-collee                    value 1.                            
M25592 01  W-PAIEMENT-MAG          PIC 9.                                       
  "        88  PAIEMENT-NON-MAGASIN        VALUE 0.                             
  "        88  PAIEMENT-MAGASIN            VALUE 1.                             
LA1008*01  GV11-WCQERESF-FLAG PIC X.                                            
LA1008*88  GV11-WCQERESF-NORMAL      VALUE ' '.                                 
LA1008 01  GV11-WCQERESF-FLAG PIC X  VALUE ' '.                                 
      *{ remove-comma-in-dde 1.5                                                
LA1008*88  GV11-WCQERESF-NORMAL      VALUE ' ' , 'Q' ,'F','K'.                  
      *--                                                                       
       88  GV11-WCQERESF-NORMAL      VALUE ' '   'Q'  'F' 'K'.                  
      *}                                                                        
       88  GV11-WCQERESF-EN-Z        VALUE 'Z'.                                 
LA1008 88  GV11-WCQERESF-EN-Q        VALUE 'Q'.                                 
LA1008 88  GV11-WCQERESF-XDOCK       VALUE 'K'.                                 
V3.5.2 01  GV11-MUTATION-FLAG PIC X.                                            
V3.5.2 88  GV11-MUTATION-OK          VALUE '0'.                                 
V3.5.2 88  GV11-MUTATION-KO          VALUE '1'.                                 
       01  WMAGIQ-FLAG        PIC X.                                            
       88  WMAGIQ-KO                 VALUE '0'.                                 
       88  WMAGIQ-OK                 VALUE '1'.                                 
       01  WS-RESER-FLAG      PIC X.                                            
       88  WS-RESER-FOURN-KO         VALUE '0'.                                 
       88  WS-RESER-FOURN-OK         VALUE '1'.                                 
       01  WS-RES-CQE-FLAG    PIC X.                                            
       88  WS-RES-CQE-TRT-KO         VALUE '0'.                                 
       88  WS-RES-CQE-TRT-OK         VALUE '1'.                                 
       01  WS-RES-xdk-FLAG    PIC X.                                            
       88  WS-RES-xdk-TRT-KO         VALUE '0'.                                 
       88  WS-RES-xdk-TRT-OK         VALUE '1'.                                 
       01  WS-GF00-FLAG       PIC X.                                            
       88  GF00-NON-TROUVE           VALUE '0'.                                 
       88  GF00-TROUVE               VALUE '1'.                                 
       01  WS-TOP-DATE-VALIDE PIC X.                                            
       88  WS-DATE-INVALIDE          VALUE '0'.                                 
       88  WS-DATE-VALIDE            VALUE '1'.                                 
      *01  WS-TOP-TYPE-EXPED  PIC X.                                            
      *88  WS-COLISSIMO              VALUE '0'.                                 
      *88  WS-CHRONOPOST             VALUE '1'.                                 
       01  WS-TOP-PRESTA-FRENV PIC X.                                           
       88  PRESTA-FRENV-NON          VALUE '0'.                                 
       88  PRESTA-FRENV-A-CREER      VALUE '1'.                                 
       88  PRESTA-FRENV-CREEE        VALUE '2'.                                 
       01  W-NLIEU-CC                PIC XXX.                                   
       01  W-TYPE-MUTATION           PIC X(10) VALUE SPACES.                    
      * -> jour de la semaine commande gf55.                                    
       01  w-j-sem                 PIC S9(09) comp-3   VALUE 0.                 
       01  w-indic-var.                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  05  w-f-sem-f               PIC S9(4) COMP.                            
      *--                                                                       
         05  w-f-sem-f               PIC S9(4) COMP-5.                          
      *}                                                                        
       01 W-CADRE                    PIC X(05) VALUE SPACES.                    
          88 W-CADRE-NON                       VALUE SPACES.                    
       01 W-SERVICE-livraison        PIC X(05) VALUE SPACES.                    
       77 W-FRAIS-WEMPORTE           PIC X(01) VALUE SPACE.                     
       77 W-FRAIS-CMODDEL            PIC X(03) VALUE SPACES.                    
       77 W-FRAIS-DDELIV             PIC X(08) VALUE SPACES.                    
      * > TABLEAU CONTENANT LE D�TAIL DE LA XCTRL (TSTENVOI)                    
      *   CONCERNANT LE SERVICE DE LIVRAISON ENVOY�                             
      *   PAR DCOM.                                                             
       01 TAB1-CPT                      PIC 9(02) value 0.                      
       01 TAB1-CPT-MAX                  PIC 9(02) VALUE 50.                     
       01 w-tab1.                                                               
          05 filler occurs 50 indexed by tab1-i.                                
             10 W-tab1-service          PIC X(05) VALUE SPACES.                 
             10 W-tab1-ddeliv           PIC X(08) VALUE SPACES.                 
             10 W-tab1-cmoddel          PIC X(03) VALUE SPACES.                 
             10 W-tab1-cplage           PIC X(02) VALUE SPACES.                 
       01 W-CMODDEL-CADRE               PIC X(03) VALUE SPACES.                 
      ******************************************************************        
      * ...ZONES POUR RECHERCHER LES EQUIPES DE LIVRAISON                       
      * TABLEAUX: - TOUTES LES FAMILLES LIVREES DANS LA VENTE                   
      *           - TOUTES LES EQUIPES DE LIVRAISON ET LEUR POIDS               
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-F-I               PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-F-I               PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-F-I-B             PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-F-I-B             PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-F-U               PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-F-U               PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-F-MAX             PIC S9(4) COMP VALUE 50.                         
      *--                                                                       
       01  T-F-MAX             PIC S9(4) COMP-5 VALUE 50.                       
      *}                                                                        
       01  TAB-FAMILLES.                                                        
           02  T-F-FILLER      OCCURS 50.                                       
               03  T-F-CFAM    PIC X(5).                                        
               03  T-F-EQ-CADR PIC X(5).                                        
V5.1.P         03  T-F-CEQUIP  PIC X(5) OCCURS 5.                               
V5.1.P 01 W-EQUIPE-CADRE       PIC X(05) VALUE SPACES.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-E-I               PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-E-I               PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-E-I-B             PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-E-I-B             PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-E-U               PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-E-U               PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-E-MAX             PIC S9(4) COMP VALUE 5.                          
      *--                                                                       
       01  T-E-MAX             PIC S9(4) COMP-5 VALUE 5.                        
      *}                                                                        
       01  TAB-CEQUIP.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  T-E-POIDS-MAX  PIC S9(4) COMP.                                   
      *--                                                                       
           02  T-E-POIDS-MAX  PIC S9(4) COMP-5.                                 
      *}                                                                        
           02  T-E-FILLER      OCCURS 5.                                        
               03  T-E-CEQUIP  PIC X(5).                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        03  T-E-POIDS   PIC S9(4) COMP.                                  
      *--                                                                       
               03  T-E-POIDS   PIC S9(4) COMP-5.                                
      *}                                                                        
NV2209 01  IND-TABC            PIC 99      VALUE ZEROES.                        
NV2209 01  IND-CDE             PIC 99      VALUE 2.                             
  !    01  WS-TABLE-CDE.                                                        
  !        05 WS-POSTE-CDE     OCCURS 2.                                        
  !           10 WS-DCDE-FNR   PIC X(06).                                       
  !           10 WS-FIL1-FNR   PIC X.                                           
  !           10 WS-QCDE-FNR   PIC Z(05).                                       
  !           10 WS-FIL2-FNR   PIC X.                                           
NV2209     05 FILLER           PIC X(04)   VALUE SPACES.                        
LA1008 01 WS-QDELAIAPPRO.                                                       
LA1008    05 WS-QDELAIAPPRO-9  PIC 9(03).                                       
      * ---------------------------------------------------------- *            
      * tableau des selections articles contient pour une relation *            
      * orig / dest, une famille produit un mode de d�livrance     *            
      * pour le ld0 (par ex.) les selections articles a chercher   *            
      * dans la table des mutations                                *            
      * ---------------------------------------------------------- *            
V5.1.P 01 W1-SEL-ARTICLES.                                                      
          05 W1-CPT              PIC 9(03) VALUE 0.                             
          05 W1-CPT-MAX          PIC 9(03) VALUE 50.                            
          05 w1-zone-table.                                                     
          06 W1-TABLE OCCURS 50 INDEXED BY W1-I.                                
             10 W1-SELART        PIC X(05).                                     
             10 W1-WACTIF        PIC X(01).                                     
      * -> FILTRAGE DES SELECTIONS ARTICLES PAR MODE DE D�LIVRANCE              
V5.1.P 01 W2-SEL-ARTICLES.                                                      
          05 W2-CPT              PIC 9(03) VALUE 0.                             
          05 W2-CPT-MAX          PIC 9(03) VALUE 50.                            
          05 W2-TABLE OCCURS 50 INDEXED BY W2-I.                                
             15 W2-CMODDEL       PIC X(03).                                     
             15 W2-SELART        PIC X(05).                                     
       01 filler.                                                               
         05 W-CSELA-MIN            PIC X(15) VALUE SPACES.                      
       01 filler.                                                               
         05 W-CSELA-MAX            PIC X(15) VALUE SPACES.                      
       01 crs-csela              pic x(01) value '1'.                           
          88 csela-fin                     value '0'.                           
          88 csela-ok                      value '1'.                           
       01 indic-lect-gb05        pic x(01) value '0'.                           
          88 mutation-non-trouvee          value '0'.                           
          88 mutation-trouvee              value '1'.                           
      ******************************************************************        
      * ...TABLEAU POUR GARDER LES DONNEES NECESSAIRES AU CALCUL DES            
      * GROUPES DE PRODUITS...                                                  
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-G-I               PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-G-I               PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-G-U               PIC S9(4) COMP.                                  
      *--                                                                       
       01  T-G-U               PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  T-G-MAX             PIC S9(4) COMP VALUE 50.                         
      *--                                                                       
       01  T-G-MAX             PIC S9(4) COMP-5 VALUE 50.                       
      *}                                                                        
       01  T-G-GROUPE.                                                          
           02  T-G-FILLER      OCCURS 50.                                       
               03  T-G-NCODICLIE   PIC X(7).                                    
               03  T-G-QTYPLIEN    PIC S9(3)      PACKED-DECIMAL.               
               03  T-G-SRP         PIC S9(7)V9(6) PACKED-DECIMAL.               
Cl0411         03  T-G-PX-VT       PIC S9(7)V999  PACKED-DECIMAL.               
Cl0411*        03  T-G-PX-VT       PIC S9(7)V99   PACKED-DECIMAL.               
v41r0  01  W-G-PX-VT           PIC S9(7)V99   PACKED-DECIMAL.                   
       01  W-G-SRP-TOT         PIC S9(7)V9(6) PACKED-DECIMAL.                   
       01  W-G-PX-VT-TOT       PIC S9(7)V99   PACKED-DECIMAL.                   
      ******************************************************************        
      * ...ZONES POUR MESSAGE DEBEDEUX...                                       
      ******************************************************************        
       01  MSG-SQL.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSG-SQL-LENGTH PIC S9(4) COMP VALUE +240.                         
      *--                                                                       
           02 MSG-SQL-LENGTH PIC S9(4) COMP-5 VALUE +240.                       
      *}                                                                        
           02 MSG-SQL-MSG    PIC X(80) OCCURS 3.                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  MSG-SQL-LRECL     PIC S9(8) COMP VALUE +80.                          
      *--                                                                       
       01  MSG-SQL-LRECL     PIC S9(8) COMP-5 VALUE +80.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-SQL             PIC S9(4) BINARY.                                  
      *--                                                                       
       01  I-SQL             PIC S9(4) COMP-5.                                  
      *}                                                                        
       01  DSNTIAR           PIC X(8)  VALUE 'DSNTIAR'.                         
      ******************************************************************        
      * ...ZONES DONT ON A BESOIN PARCE QU'IL LE FAUT BIEN...                   
      ******************************************************************        
       01  W-MESSAGE-S.                                                         
           02  FILLER      PIC X(5) VALUE 'ECCC'.                               
           02  W-MESSAGE-H PIC X(2).                                            
           02  FILLER      PIC X(1) VALUE ':'.                                  
           02  W-MESSAGE-M PIC X(2).                                            
           02  FILLER      PIC X(1) VALUE ' '.                                  
           02  W-MESSAGE   PIC X(69).                                           
      * INDICE POUR... INDICE, � UTILISER N'IMPORTE O�                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-I  PIC S9(4) COMP.                                                 
      *--                                                                       
       01  I-I  PIC S9(4) COMP-5.                                               
      *}                                                                        
      * INDICE POUR NPRIORITE                                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-P  PIC S9(4) COMP.                                                 
      *--                                                                       
       01  I-P  PIC S9(4) COMP-5.                                               
      *}                                                                        
      * INDICES POUR DECALER LES LIGNES DE VENTE                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEC-DEBUT  PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-DEC-DEBUT  PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEC-FIN    PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-DEC-FIN    PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEC-NB     PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-DEC-NB     PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEC-I      PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-DEC-I      PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-DEC-SAVE   PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-DEC-SAVE   PIC S9(4) COMP-5.                                       
      *}                                                                        
      * INDICES POUR DECALER LES LIGNES DE VENTE                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MUL-DEBUT  PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-MUL-DEBUT  PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MUL-FIN    PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-MUL-FIN    PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-MUL-NB     PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-MUL-NB     PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-A          PIC S9(4) COMP.                                         
      *--                                                                       
       01  I-A          PIC S9(4) COMP-5.                                       
      *}                                                                        
      * POUR METTRE EN FORME LES COMMENTAIRES                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-CMENT      PIC S9(4) BINARY.                                       
      *--                                                                       
       01  I-CMENT      PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-LCOMVTE    PIC S9(4) BINARY.                                       
      *--                                                                       
       01  I-LCOMVTE    PIC S9(4) COMP-5.                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-LCOMLIV    PIC S9(4) BINARY.                                       
      *--                                                                       
       01  I-LCOMLIV    PIC S9(4) COMP-5.                                       
      *}                                                                        
       01  W-CMENT      PIC X(255).                                             
      * POUR METTRE EN FORME LE NUM�RO DE COMMANDE DARTY.COM                    
       01  PIC9-NCDEWC   PIC Z(14)9.                                            
       01  PICX-NCDEWC   REDEFINES PIC9-NCDEWC PIC X(15).                       
      * POUR GARDER LE 1ER ENTREPOT D'UN CODIC                                  
       01  W-GS10-NSOCDEPOT PIC X(3).                                           
       01  W-GS10-NDEPOT    PIC X(3).                                           
      * POUR SAVOIR (TG CILOT) S'IL Y A DU LOGITRANS DANS L'AIR...              
       01  FILLER           PIC X   VALUE 'N'.                                  
       88  IL-Y-A-DU-LOGITRANS      VALUE 'O'.                                  
      * POUR GARDER DE LA NCGFC MECCC SI ON TRACE LES OPERATIONS                
       01  FILLER           PIC X   VALUE 'N'.                                  
       88  FAIS-DES-DISPLAYS        VALUE 'O'.                                  
NV2008 01  WQTE             PIC S9(05)    COMP-3 VALUE ZEROES.                  
NV0809 01  WQTE-TEMP        PIC S9(05)    COMP-3 VALUE ZEROES.                  
NV0809 01  WQTE-TEMP1       PIC S9(05)    COMP-3 VALUE ZEROES.                  
NV0809 01  WQNBLIGNES       PIC S9(05)    COMP-3 VALUE ZEROES.                  
NV0809 01  WQVOLUME         PIC S9(11)    COMP-3 VALUE ZEROES.                  
NV2209 01  WS-DCDE7         PIC  X(08)           VALUE SPACES.                  
NV2209 01  WS-DJOUR7        PIC  X(08)           VALUE SPACES.                  
NV2209 01  WS-DRECEPT       PIC  X(08)           VALUE SPACES.                  
NV2209 01  WS-DRECEPT6      PIC  X(06)           VALUE SPACES.                  
NV2209 01  WS-QUANT-DIS     PIC S9(06)           VALUE ZEROES.                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WN-I-CRE         PIC S9(02) COMP      VALUE ZEROES.                  
      *--                                                                       
       01  WN-I-CRE         PIC S9(02) COMP-5      VALUE ZEROES.                
      *}                                                                        
       01 W-NOM-PROG        PIC X(06).                                          
       01 W-NOM-PROG-appel  PIC X(05).                                          
      * POUR TRANSFORMER LES METRES CUBES                                       
       01  W-GB05-QVOLUME   PIC S9(3)V9(0002) PACKED-DECIMAL.                   
      ******************************************************************        
      * ...DATE ET HEURE...                                                     
      ******************************************************************        
           COPY COMMDATC.                                                       
       01  W-EIBTIME        PIC 9(7).                                           
       01  FILLER REDEFINES W-EIBTIME.                                          
           02  FILLER    PIC X.                                                 
           02  W-HEURE   PIC XX.                                                
           02  W-MINUTE  PIC XX.                                                
           02  W-SECONDE PIC XX.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-SPDATDB2-RC                 PIC S9(4)  COMP.                       
      *--                                                                       
       01  W-SPDATDB2-RC                 PIC S9(4) COMP-5.                      
      *}                                                                        
       01  W-SPDATDB2-DSYST              PIC S9(13) COMP-3 VALUE ZERO.          
      ******************************************************************        
      * ...POUR DECODER L'ADRESSE...                                            
      ******************************************************************        
       01  MEC09C.                                                              
           COPY MEC09C.                                                         
      ******************************************************************        
      * ...POUR MEC15 (LOG DES COMMANDES)...                                    
      ******************************************************************        
       01  MEC15C-COMMAREA.                                                     
           COPY MEC15C.                                                         
           COPY MEC15E.                                                         
M16733     COPY MEC03C.                                                         
           COPY MEC24C.                                                         
      ******************************************************************        
      * ...POUR MGV65 (EXPEDITIONS)...                                          
      ******************************************************************        
LA2203*01  MGV65 PIC X(8) VALUE 'MGV65'.                                        
LA2203*    COPY COMMGV65.                                                       
LA2203 01  MEC64 PIC X(8) VALUE 'MEC64'.                                        
LA2203     COPY COMMEC64.                                                       
stkpf  01  MGV48 PIC X(8) VALUE 'MGV48'.                                        
      ******************************************************************        
      * ...ON ARRIVE FINALEMENT A LA COMMAREA.                                  
      * (TAILLE DES TABLEAUX DE LA COMMAREA, TOUJOURS EN WORKING)               
      ******************************************************************        
           COPY COMMECCL.                                                       
V41R0 *  => Zones de travail utiles pour la gestion de TSTENVOI                 
    |      COPY WKTENVOI.                                                       
stkpf      copy COMMGV48.                                                       
CCPRM      copy COMMNV45.                                                       
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           COPY COMMECCC.                                                       
      *=================================================================        
       PROCEDURE DIVISION.                                                      
      *=================================================================        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL) END-EXEC.                 
      *--                                                                       
           EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL)                           
           END-EXEC.                                                            
      *}                                                                        
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *=================================================================        
      * ON POSITIONNE JUSTE DES FLAGS POUR LES TRAITEMENTS QUI SUIVENT.         
      * COMME LES FLAGS PARLENT D'EUX-MEMES, IL N'Y A RIEN � DIRE.              
      ******************************************************************        
       MODULE-ENTREE SECTION.                                                   
           PERFORM CALL-SPDATDB2.                                               
           IF W-CC-E-C-RET <= ' '                                               
              MOVE '0' TO W-CC-E-C-RET                                          
           END-IF.                                                              
           SET NE-PAS-CREER-LIVRAISON TO TRUE.                                  
           SET NE-PAS-CREER-EXPEDITION TO TRUE.                                 
           IF W-CC-VENTE-CLICK-COLLECT                                          
              PERFORM RECHERCHE-MAG-CLICK-COLLECT                               
           ELSE                                                                 
              IF (W-CC-NSOC-LIVR = W-CC-NSOC-EN-TRT)                            
                AND W-CC-IL-Y-A-DES-LIVRAISONS                                  
                 SET PARTIE-LIVREE-A-CREER TO TRUE                              
              END-IF                                                            
              IF (W-CC-NSOC-ECOM = W-CC-NSOC-EN-TRT)                            
                OR (W-CC-NSOC-EN-TRT = W-CC-R-C-SOC(1))                         
                 SET PARTIE-EXPEDIEE-A-CREER TO TRUE                            
              END-IF                                                            
           END-IF.                                                              
           MOVE SPACES TO TXTVA-CTABLEG2.                                       
           MOVE 'MECCC' TO NCGFC-CTABLEG2.                                      
           PERFORM SELECT-NCGFC.                                                
           IF NCGFC-COMMENT(1:7) = 'DISPLAY'                                    
              SET FAIS-DES-DISPLAYS TO TRUE                                     
           END-IF.                                                              
V43R0      PERFORM RECHERCHE-PARAMETRES.                                        
M16733     MOVE 0 TO MEC03C-NBP.                                                
V5.1.P     IF PARTIE-LIVREE-A-CREER                                             
             PERFORM ASSOC-MDELIV-SEL-ART                                       
           END-IF.                                                              
      * -> pour display et libell� de livraison                                 
v5.1.p     MOVE W-CC-E-NUM-CMD-DC  TO PIC9-NCDEWC.                              
      *    MOVE SPACES TO NCGFC-WTABLEG.                                        
      *    MOVE 'MGV64' TO NCGFC-CTABLEG2                                       
      *    PERFORM SELECT-NCGFC.                                                
      *    IF SQLCODE NOT = 0                                                   
V34R4 *       MOVE 'N' TO W-TOP-NEW                                             
V34R4 *    ELSE                                                                 
V34R4 *       MOVE NCGFC-WTABLEG(18:1) TO W-TOP-NEW                             
V34R4 *    END-IF.                                                              
      *-----------------------------------------------------------------        
      * Recherche de la premi�re ligne click and collect libre                  
       RECHERCHE-MAG-CLICK-COLLECT SECTION.                                     
           PERFORM VARYING W-CC-V-I FROM 1 BY 1                                 
             UNTIL W-CC-V-I > W-CC-V-U                                          
                OR (W-CC-V-NSOC-CC(W-CC-V-I) = W-CC-NSOC-EN-TRT)                
           END-PERFORM.                                                         
           MOVE W-CC-V-NLIEU-CC(W-CC-V-I) TO W-NLIEU-CC.                        
      *                                                                         
      *=================================================================        
      *==>  Traitement: Creation de la vente                                    
       MODULE-TRAITEMENT SECTION.                                               
           PERFORM CREATION-VENTE.                                              
      *                                                                         
      *=================================================================        
       MODULE-SORTIE SECTION.                                                   
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      *=================================================================        
      *=================================================================        
      ******************************************************************        
      * CREATION DE VENTE.                                                      
      * ON FAIT TOUT UN TAS DE CHOSES; C'EST EXPLIQUE A CHAQUE SECTION          
      * CE QU'ON FAIT (SAUF SI CA NE L'EST PAS); CEPENDANT POUR QUE TU          
      * AIES UNE IDEE GENERALE DE TOUT CA:                                      
      * - ON MET D'ABORD LA VENTE INTERNET CONFORME AU FORMAT GV                
      *   (TRANSFO DES GROUPES, PSE APR�S LA LIGNE PRODUIT...).                 
      * - PUIS, S'IL Y A LIVRAISON, RECHERCHE DE DONNEES DONT ON AURA           
      *   BESOIN PAR LA SUITE.                                                  
      * - ET FINALEMENT CREATION DE LA VENTE.                                   
      ******************************************************************        
       CREATION-VENTE SECTION.                                                  
v5.1p * > on profite de la boucle existante pour chercher                       
      *   les services cadre                                                    
           MOVE SPACES TO W-CADRE                                               
           PERFORM VARYING W-CC-V-I FROM 1 BY 1                                 
           UNTIL W-CC-V-I > W-CC-V-U                                            
              IF (W-CC-V-CTYPENREG(W-CC-V-I) = '1')                             
              AND W-CC-V-DT-LIV(W-CC-V-I) <= ' '                                
                 MOVE W-CC-DATE-DU-JOUR TO W-CC-V-DT-LIV(W-CC-V-I)              
              END-IF                                                            
              IF (W-CC-V-CTYPENREG(W-CC-V-I) = '4')                             
                 PERFORM CHERCHE-CADRE                                          
              END-IF                                                            
           END-PERFORM.                                                         
V5.1P      IF NOT W-CADRE-NON                                                   
              PERFORM SELECT-RTGQ20                                             
               IF   FAIS-DES-DISPLAYS                                           
                MOVE SPACES TO W-MESSAGE                                        
                STRING                                                          
                'Cadre trouv� : ' w-cadre '/' W-CC-CELEMEN                      
                delimited by size into w-message                                
                PERFORM DISPLAY-W-MESSAGE                                       
               end-if                                                           
           END-IF                                                               
           PERFORM TRANSFO-GROUPE-EN-ELEMENTS.                                  
           PERFORM METS-EN-FORME-LES-SERVICES.                                  
           IF PARTIE-LIVREE-A-CREER                                             
      * EQUIPE POUBELLE � BLANC (SERT DE FLAG)                                  
              MOVE SPACES TO EQUIG-CTABLEG2                                     
              PERFORM RECHERCHE-PLATEFORME                                      
              MOVE 0 TO  T-F-I                                                  
              INITIALIZE TAB-FAMILLES                                           
              MOVE ' ' TO GQ04-DEFFET                                           
              PERFORM VARYING W-CC-V-I FROM 1 BY 1                              
              UNTIL W-CC-V-I > W-CC-V-U                                         
      ********* ECRASEMENT MODE DE DELIVRANCE DES EXPEDIES FILIALE              
                 IF W-CC-V-CTYPENREG(W-CC-V-I) = '1'                            
                 AND W-CC-V-MODE-DELIV(W-CC-V-I) = 'EMR'                        
                 AND W-CC-V-MODE-DELIV-L(W-CC-V-I) = 'LD2'                      
                    MOVE W-CC-V-CODIC(W-CC-V-I) TO GA64-NCODIC                  
                    PERFORM SELECT-RTGA64                                       
                    MOVE GA64-CMODDEL TO W-CC-V-MODE-DELIV-L(W-CC-V-I)          
                 END-IF                                                         
                 IF (W-CC-V-CTYPENREG(W-CC-V-I) = '1')                          
v5.1.p*           AND (   (W-CC-V-MODE-DELIV(W-CC-V-I) = 'LD2')                 
      *                OR (W-CC-V-MODE-DELIV-L(W-CC-V-I) = 'LD2'))              
                  AND (   (W-CC-V-MODE-DELIV(W-CC-V-I) (1:2) = 'LD')            
                       OR (W-CC-V-MODE-DELIV-L(W-CC-V-I) (1:2) = 'LD'))         
V5.1.P              PERFORM CHERCHE-MODE-DELIVRANCE                             
                    PERFORM AJOUTE-AU-TABLEAU-DES-FAMILLES                      
                    IF GQ04-DEFFET = ' '                                        
                       MOVE W-CC-V-DT-LIV(W-CC-V-I)   TO GQ04-DEFFET            
                       MOVE W-CC-V-CREN-LIV(W-CC-V-I) TO GQ03-CPLAGE            
                    END-IF                                                      
                 END-IF                                                         
              END-PERFORM                                                       
              IF GV10-WCQERESF-NORMAL                                           
                 PERFORM RECHERCHE-EQUIPE                                       
                 IF FAIS-DES-DISPLAYS                                           
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING                                                      
                           'EQUIPES: GQ03: ' GQ03-CEQUIP                        
                           ' GQ04: ' GQ04-CEQUIP1                               
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-W-MESSAGE                                   
                 END-IF                                                         
                 IF GQ04-CEQUIP1 = ' '                                          
                    PERFORM SELECT-EQUIG                                        
                 ELSE                                                           
                    PERFORM MISE-A-JOUR-QUOTA                                   
                 END-IF                                                         
              ELSE                                                              
                 PERFORM SELECT-EQUIG                                           
              END-IF                                                            
           END-IF.                                                              
           PERFORM CREATION-ENTETE.                                             
           PERFORM CREATION-ADRESSE.                                            
           MOVE '00' TO W-GV11-NLIGNE-PICXX.                                    
           SET PRESTA-FRENV-NON TO TRUE.                                        
           PERFORM VARYING W-CC-V-I FROM 1 BY 1                                 
           UNTIL W-CC-V-I > W-CC-V-U                                            
              IF W-CC-V-C-RET(W-CC-V-I) <= ' '                                  
                 EVALUATE W-CC-V-CTYPENREG(W-CC-V-I)                            
                 WHEN ('1')                                                     
      *         SI SUR LE PREC CODIC ON AVAIT UNE PRESTA CHRONO A CREER         
      *         ALORS IL FAUT LE FAIRE MAINTENANT ...                           
                    IF PRESTA-FRENV-A-CREER                                     
V34CEN                 PERFORM CREATION-PRESTA-FRENV                            
                       SET PRESTA-FRENV-CREEE TO TRUE                           
                    END-IF                                                      
                    IF W-CC-VENTE-CLICK-COLLECT                                 
                       IF (W-CC-V-NSOC-CC(W-CC-V-I)                             
                                           = W-CC-NSOC-EN-TRT)                  
                        AND (W-CC-V-NLIEU-CC(W-CC-V-I)                          
                                           = W-NLIEU-CC)                        
                          PERFORM CREATION-LIGNE-EXPEDIEE                       
                       END-IF                                                   
                    ELSE                                                        
v5.1.p*                IF (W-CC-V-MODE-DELIV(W-CC-V-I) = 'LD2')                 
      *                 OR (W-CC-V-MODE-DELIV-L(W-CC-V-I) = 'LD2')              
      *                IF (W-CC-V-MODE-DELIV(W-CC-V-I) (1:2) = 'LD')            
      *                 OR (W-CC-V-MODE-DELIV-L(W-CC-V-I) (1:2) = 'LD')         
                       IF (W-CC-V-MODE-DELIV(W-CC-V-I) (1:1) = 'L')             
                        OR (W-CC-V-MODE-DELIV-L(W-CC-V-I) (1:1) = 'L')          
                          IF PARTIE-LIVREE-A-CREER                              
                             PERFORM CREATION-LIGNE-LIVREE                      
                          ELSE                                                  
                             IF (W-CC-V-MODE-DELIV(W-CC-V-I) = 'EMR')           
                               AND (PARTIE-EXPEDIEE-A-CREER)                    
                                PERFORM CREATION-LIGNE-EXPEDIEE                 
                             END-IF                                             
                          END-IF                                                
                       ELSE                                                     
                          IF PARTIE-EXPEDIEE-A-CREER                            
                             PERFORM CREATION-LIGNE-EXPEDIEE                    
                          END-IF                                                
                       END-IF                                                   
                    END-IF                                                      
                 WHEN ('3')                                                     
                    IF W-CC-V-CODIC(W-CC-V-I) = GV11-NCODIC                     
                       MOVE '3' TO GV11-CTYPENREG                               
                       PERFORM CREATION-LIGNE-SERVICE                           
                    END-IF                                                      
promo            WHEN ('2')                                                     
                    IF W-CC-V-CODIC(W-CC-V-I) = GV11-NCODIC                     
                       AND W-CC-V-NCODICGRP (W-CC-V-I)  <= SPACES               
                       MOVE '2' TO GV11-CTYPENREG                               
                       PERFORM CREATION-LIGNE-SERVICE                           
                    END-IF                                                      
v5.1.p           WHEN ('4')                                                     
                     MOVE '4' TO GV11-CTYPENREG                                 
                     PERFORM CREATION-LIGNE-SERVICE-SEUL                        
                     PERFORM CREE-MOUCHARD-ENTETE-LIGNE                         
                 END-EVALUATE                                                   
              END-IF                                                            
           END-PERFORM.                                                         
LA2203     IF WN-I-CRE > 0                                                      
LA2203        PERFORM LINK-MEC64                                                
LA2203        ADD 1 TO MEC03C-NBP                                               
LA2203        PERFORM VARYING I-A FROM 1 BY 1                                   
LA2203        UNTIL (I-A > WN-I-CRE) OR (MEC03C-NBP  > MEC03C-NBP-MAX)          
LA2203*          IF (COMM-MEC64-CDEDACEM(I-A)                                   
LA2203*          AND EC64-AUCUN-STOCK-DISPO(I-A))                               
LA2203*          OR (COMM-MEC64-CDEDARTY(I-A))                                  
LA2203           IF  COMM-MEC64-CDEDARTY(I-A)                                   
LA2203               MOVE COMM-MEC64-TAB-NSOCLIVR(I-A)                          
LA2203                 TO MEC03C-SOCIETE-ENT(MEC03C-NBP)                        
LA2203               MOVE COMM-MEC64-TAB-NDEPOT(I-A)                            
LA2203                 TO MEC03C-LIEU-ENT(MEC03C-NBP)                           
LA2203               MOVE COMM-MEC64-TAB-NCODIC(I-A)                            
LA2203                 TO MEC03C-CODIC-ENT(MEC03C-NBP)                          
LA2203               IF COMM-MEC64-CDEDARTY(I-A)                                
LA2203                  MOVE 0 TO MEC03C-WDACEM(MEC03C-NBP)                     
LA2203               ELSE                                                       
LA2203                  MOVE 1 TO MEC03C-WDACEM(MEC03C-NBP)                     
LA2203               END-IF                                                     
LA2203               MOVE COMM-MEC64-TAB-ETAT-RESA(I-A)                         
LA2203                 TO MEC03C-ETAT-RESERVATION (MEC03C-NBP)                  
LA2203               ADD 1 TO MEC03C-NBP                                        
LA2203           END-IF                                                         
LA2203        END-PERFORM                                                       
              IF MEC03C-NBP  > MEC03C-NBP-MAX                                   
                 MOVE 'TABLEAU MEC03C MAXI ATTEINT' TO W-MESSAGE                
                 PERFORM ERREUR-PRG                                             
              END-IF                                                            
LA2203     END-IF                                                               
           .                                                                    
      ******************************************************************        
      * PRESTAS (RAJUSTEMENT ET EXPEDITION) CREEES SUR 1ERE VENTE               
      * -> EST-CE QUE FRAIS D'EXPEDITION � CREER SI PARTIE EXPEDIEE?            
      ******************************************************************        
           IF W-CC-E-STAT NOT = 'P'                                             
              IF PRESTA-FRENV-A-CREER                                           
V34CEN           PERFORM CREATION-PRESTA-FRENV                                  
                 SET PRESTA-FRENV-CREEE TO TRUE                                 
              END-IF                                                            
V34CEN        PERFORM CREATION-PRESTA-AJUST                                     
              MOVE 'P' TO W-CC-E-STAT                                           
           END-IF.                                                              
      ******************************************************************        
      * ON CALCULE LE MONTANT "COMPTANT" ET "A LA LIVRAISON"                    
      * REMARQUE: S'IL Y A AU MOINS UN PRODUIT EXPEDIE, IL FAUT TOUT            
      *           PAYER � LA COMMANDE; DONC, SI LES REGLEMENTS NE               
      *           REGLENT PAS TOUT, LE MONTANT RESTANT EST FORCEMMENT �         
      *           LA LIVRAISON.                                                 
      ******************************************************************        
      *??? 02  GV10-PTTVENTE incr�ment� par GV11 + prestas                      
      *??? 02  GV10-PVERSE                                                      
      *??? 02  GV10-PCOMPT                                                      
      *??? 02  GV10-PLIVR                                                       
M25592     SET PAIEMENT-NON-MAGASIN      TO TRUE                                
           PERFORM VARYING W-CC-R-I FROM 1 BY 1                                 
              UNTIL (W-CC-R-I > W-CC-R-U)                                       
                 OR (GV10-PCOMPT >= GV10-PTTVENTE)                              
              MOVE W-CC-R-MD-PMENT(W-CC-R-I) TO EC008-CTABLEG2                  
              PERFORM SELECT-EC008                                              
M25592           IF EC008-WENLIGNE = 'M'                                        
M25592              SET PAIEMENT-MAGASIN      TO TRUE                           
M25592           END-IF                                                         
                 IF W-CC-R-MT(W-CC-R-I) > W-CC-R-MT-PRIS(W-CC-R-I)              
                    COMPUTE GV10-PCOMPT = GV10-PCOMPT                           
                      + (W-CC-R-MT(W-CC-R-I) - W-CC-R-MT-PRIS(W-CC-R-I))        
                    COMPUTE W-CC-R-MT-PRIS(W-CC-R-I)                            
                           = W-CC-R-MT(W-CC-R-I)                                
                    IF GV10-PCOMPT > GV10-PTTVENTE                              
                       COMPUTE W-CC-R-MT-PRIS(W-CC-R-I)                         
                             = W-CC-R-MT-PRIS(W-CC-R-I)                         
                             - GV10-PCOMPT + GV10-PTTVENTE                      
                       COMPUTE GV10-PCOMPT = GV10-PTTVENTE                      
                    END-IF                                                      
                 END-IF                                                         
           END-PERFORM.                                                         
      ** si paiement magasin mettre le total dans prix comptant                 
M25592     IF PAIEMENT-MAGASIN                                                  
M25592        MOVE GV10-PTTVENTE  TO GV10-PCOMPT                                
M25592     END-IF                                                               
      ** PRIX A LA LIVRAISON                                                    
           IF GV10-PTTVENTE > GV10-PCOMPT                                       
              COMPUTE GV10-PLIVR = GV10-PTTVENTE - GV10-PCOMPT                  
           END-IF.                                                              
MGD15      MOVE  W-CC-TYPVTE      TO GV10-TYPVTE                                
      ******************************************************************        
      * ET HOP! CREATION ENTETE!                                                
      ******************************************************************        
kdo        move w-nseqens to gv10-nseqens                                       
           PERFORM INSERT-RTGV10-EC02.                                          
           IF (PARTIE-LIVREE-A-CREER)                                           
              AND (EQUIG-CTABLEG2 = SPACES)                                     
              PERFORM INSERT-RTGV41                                             
           END-IF.                                                              
           COMPUTE W-CC-V-I = W-CC-V-MAX + 1.                                   
           MOVE GV10-NSOCIETE TO W-CC-V-C-SOC(W-CC-V-I).                        
           MOVE GV10-NLIEU    TO W-CC-V-C-LIEU(W-CC-V-I).                       
           MOVE GV10-NVENTE   TO W-CC-V-NUM-CMD-SI(W-CC-V-I).                   
           IF GV10-WCQERESF-EN-Z                                                
              MOVE '1' TO W-CC-E-C-RET                                          
           END-IF.                                                              
M16733     IF  W-CMODEL-EMD-NON                                                 
           AND MEC03C-CODIC-ENT(1) > ' '                                        
"              EXEC CICS LINK PROGRAM  ('MEC03')                                
"                             COMMAREA (MEC03C-COMMAREA)                        
"              END-EXEC                                                         
"          END-if.                                                              
      ******************************************************************        
       APPEL-MEC24                SECTION.                                      
           MOVE GQ03-CZONLIV      TO     MEC24C-CZONLIV-ENT                     
           MOVE GQ03-CPLAGE       TO     MEC24C-CPLAGE-ENT                      
           MOVE GQ04-DEFFET       TO     MEC24C-DATE-ENT                        
V5.1.P*    MOVE 'LD2'             TO     MEC24C-CMODDEL-ENT                     
           MOVE GQ03-CMODDEL      TO     MEC24C-CMODDEL-ENT                     
           MOVE GQ03-CEQUIP       TO     MEC24C-CEQUIP-ENT                      
           MOVE LIVRG-LDEPLIV     TO     MEC24C-SOCIETE-ENT                     
           MOVE LIVRG-LDEPLIV(4:3) TO    MEC24C-LIEU-ENT                        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         ASSIGN PROGRAM (W-NOM-PROG)                                     
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS ASSIGN PROGRAM (W-NOM-PROG)                                
           END-EXEC.                                                            
      *}                                                                        
           MOVE 'MEC24'                                                         
           TO W-NOM-PROG(1:5)                                                   
           EXEC CICS LINK     PROGRAM  (w-nom-prog)                             
                              COMMAREA (MEC24C-COMMAREA)                        
                              NOHANDLE                                          
           END-EXEC .                                                           
      ******************************************************************        
      * TRANSFORMATION DES GROUPES EN ELEMENTS:                                 
      * ==> ATTENTION: NE TIENT PAS COMPTE DU PBM DES PSE                       
      ******************************************************************        
       TRANSFO-GROUPE-EN-ELEMENTS SECTION.                                      
      *-----------------------------------                                      
      * POINT1: ON RECHERCHE LES PRODUITS MULTI FAMILLE                         
           PERFORM VARYING W-CC-V-I FROM 1 BY 1                                 
             UNTIL W-CC-V-I > W-CC-V-U                                          
              IF W-CC-V-CTYPENREG(W-CC-V-I) = '1'                               
                 MOVE W-CC-V-CODIC(W-CC-V-I) TO GA00-NCODIC                     
                 PERFORM SELECT-RTGA14-WMULTIFAM                                
                 IF GA14-WMULTIFAM = 'O'                                        
                    PERFORM REMPLACE-GROUPE-PAR-ELEMENTS                        
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
       REMPLACE-GROUPE-PAR-ELEMENTS SECTION.                                    
      *-------------------------------------                                    
           PERFORM CHARGE-TABLEAU-ELEMENTS.                                     
           COMPUTE I-DEC-DEBUT = W-CC-V-I + 1.                                  
           COMPUTE I-DEC-FIN   = W-CC-V-U.                                      
           COMPUTE I-DEC-NB    = T-G-U - 1.                                     
           COMPUTE I-DEC-SAVE = W-CC-V-MAX + 1.                                 
           MOVE W-CC-VENTE(W-CC-V-I) TO W-CC-VENTE(I-DEC-SAVE).                 
           PERFORM DECALAGE-DES-LIGNES.                                         
           MOVE 0 TO W-G-PX-VT-TOT.                                             
           PERFORM VARYING T-G-I FROM 1 BY 1 UNTIL T-G-I > T-G-U                
              MOVE W-CC-VENTE(I-DEC-SAVE) TO W-CC-VENTE(W-CC-V-I)               
              MOVE W-CC-V-CODIC(I-DEC-SAVE)                                     
                                          TO W-CC-V-NCODICGRP(W-CC-V-I)         
              MOVE T-G-NCODICLIE(T-G-I)   TO W-CC-V-CODIC(W-CC-V-I)             
              COMPUTE W-CC-V-QTY(W-CC-V-I)   =                                  
                      W-CC-V-QTY(I-DEC-SAVE) * T-G-QTYPLIEN(T-G-I)              
              COMPUTE W-CC-V-PX-VT(W-CC-V-I) = T-G-PX-VT(T-G-I)                 
              ADD 1 TO W-CC-V-I                                                 
           END-PERFORM.                                                         
           SUBTRACT 1 FROM W-CC-V-I.                                            
      ******************************************************************        
      * MISE EN FORME DES SERVICES - EXPLICATION:                               
      *                                                                         
      * LES PRODUITS ET SERVICES NOUS ARRIVENT DANS LE DESORDRE; OR POUR        
      * GV00, LES SERVICES SUIVENT FORCEMMENT LA LIGNE PRODUIT.                 
      *                                                                         
      * EN PLUS, QUAND UNE LIGNE PRODUIT QUI A DES SERVICES A UNE               
      * QUANTITE > � 1, IL FAUT FAIRE DES ENSEMBLES. PAR EXEMPLE, VENTE         
      * RECUE DE DARTY.COM:                                                     
      *                                                                         
      *      CODIC/SERVICE     R�F�RENCE                   QUANTITE             
      *      -------------     ------------------------    --------             
      *      0000001           PC                              2                
      *      0000002           MACHINE � LAVER                 1                
      *      PSE               EXT. GARANTIE SUR PC            -                
      *      AT2               ASS. TELEPHONIQUE SUR PC        -                
      *      PSE               EXT. GARANTIE SUR MAL           -                
      *                                                                         
      * CETTE VENTE DOIT ETRE TRANSFORMEE POUR GV00 EN:                         
      *                                                                         
      *      CODIC/SERVICE     REFERENCE                   QUANTITE             
      *      -------------     ------------------------    --------             
      *      0000001           PC                              1                
      *      PSE               EXT. GARANTIE SUR PC            -                
      *      AT2               ASS. TELEPHONIQUE SUR PC        -                
      *      0000001           PC                              1                
      *      PSE               EXT. GARANTIE SUR PC            -                
      *      AT2               ASS. TELEPHONIQUE SUR PC        -                
      *      0000002           MACHINE A LAVER                 1                
      *      PSE               EXT. GARANTIE SUR MAL           -                
      *                                                                         
      * LES LIGNES SUR DARTY.COM SONT STOCKEES DANS L'ORDRE DE SAISIE:          
      * ON NE POURRA DONC JAMAIS TROUVER UN SERVICE AVANT UN PRODUIT,           
      * CE QUI NOUS FACILITE UN PEU LE PROBLEME.                                
      *                                                                         
      * ON VA PROCEDER COMME SUIT:                                              
      *                                                                         
POINT1* - D'ABORD, ON VA TRIER LES LIGNES, DE FACON A AVOIR TOUJOURS            
      *   LES SERVICES APRES LES PRODUITS. DANS L'EXEMPLE, ON AURAIT            
      *   APRES CE TRI (LE TABLEAU CONTIENT TOUJOURS LE MEME NOMBRE DE          
      *   POSTES UTILISES):                                                     
      *                                                                         
      *      CODIC/SERVICE     REFERENCE                   QUANTITE             
      *      -------------     ------------------------    --------             
      *      0000001           PC                              2                
      *      PSE               EXT. GARANTIE SUR PC            -                
      *      AT2               ASS. TELEPHONIQUE SUR PC        -                
      *      0000002           MACHINE A LAVER                 1                
      *      PSE               EXT. GARANTIE SUR MAL           -                
      *                                                                         
POINT2* - ENSUITE, POUR LES LIGNES AVEC UNE QUANTITE > A 1, IL FAUT             
      *   INSERER LES LIGNES POUR FAIRE DES ENSEMBLES (ON AUGMENTE LE           
      *   NOMBRE DE LIGNES UTILISEES):                                          
      *                                                                         
      *      CODIC/SERVICE     REFERENCE                   QUANTITE             
      *      -------------     ------------------------    --------             
      *      0000001           PC                              2                
      *      PSE               EXT. GARANTIE SUR PC            -                
      *      AT2               ASS. TELEPHONIQUE SUR PC        -                
      *      '''''''           ''''''''                       '''               
      *      '''''''           ''''''''                       '''               
      *      '''''''           ''''''''                       '''               
      *      0000002           MACHINE A LAVER                 1                
      *      PSE               EXT. GARANTIE SUR MAL           -                
      *                                                                         
      *      REMARQUE: LE NOMBRE DE LIGNES INSEREES EST:                        
      *                (QTE CODIC - 1) * (NBRE DE SERVICES + 1)                 
      *                                                                         
POINT3* -EET FINALEMENT MULTIPLIER L'ENSEMBLE PRODUIT + SERVICES, AVEC          
      *   UNE QUANTITE FORCEE A 1; ON LE MULTIPLIE "QTE CODIC - 1" FOIS:        
      *                                                                         
      *      CODIC/SERVICE     REFERENCE                   QUANTITE             
      *      -------------     ------------------------    --------             
      *      0000001           PC                              1                
      *      PSE               EXT. GARANTIE SUR PC            -                
      *      AT2               ASS. TELEPHONIQUE SUR PC        -                
      *      0000001           PC                              1                
      *      PSE               EXT. GARANTIE SUR PC            -                
      *      AT2               ASS. TELEPHONIQUE SUR PC        -                
      *      0000002           MACHINE A LAVER                 1                
      *      PSE               EXT. GARANTIE SUR MAL           -                
      *                                                                         
      * CE QUI EST BIEN LE RESULTAT QU'ON VOULAIT. APRES CETTE                  
      * BIDOUILLE, L'INSERTION DES LIGNES GV11 EST BIEN PLUS SIMPLE.            
      ******************************************************************        
       METS-EN-FORME-LES-SERVICES SECTION.                                      
      *-----------------------------------                                      
AL1008* POINT0: EXTRACTION DES 'SERVICES'                                       
+          MOVE 1        TO W-CC-S-I                                            
V5.1.P     MOVE 0        TO I-DEC-DEBUT                                         
V5.1.P     MOVE W-CC-V-U TO I-DEC-FIN                                           
|          PERFORM VARYING W-CC-V-I FROM W-CC-V-U BY -1                         
|          UNTIL W-CC-V-I < 1                                                   
      * les remises peuvent se trouver ins�r�e entre les produits               
      *  suite aux mex moteur de promo...                                       
|     *    OR    W-CC-V-CTYPENREG(W-CC-V-I) = '1'                               
promo         IF W-CC-V-CTYPENREG(W-CC-V-I) = '3' or '2'                        
|                MOVE W-CC-V-CTYPENREG(W-CC-V-I)                                
|                                          TO W-CC-S-CTYPENREG(W-CC-S-I)        
|                MOVE W-CC-V-CENREG(W-CC-V-I) TO W-CC-S-CENREG(W-CC-S-I)        
|                MOVE W-CC-V-CODIC(W-CC-V-I)  TO W-CC-S-CODIC(W-CC-S-I)         
|                MOVE W-CC-V-PX-VT(W-CC-V-I)  TO W-CC-S-PX-VT(W-CC-S-I)         
MP839            MOVE W-CC-V-NCODICGRP(W-CC-V-I)                                
MP839                                     TO W-CC-S-CODICGRP(W-CC-S-I)          
M23862           MOVE W-CC-V-NUM-PS(W-CC-V-I) TO W-CC-S-NUM-PS(W-CC-S-I)        
M23862           MOVE 'N'                     TO W-CC-S-FAIT  (W-CC-S-I)        
|                MOVE SPACES TO W-CC-VENTE (W-CC-V-I)                           
V5.1.P           MOVE W-CC-V-I  TO I-DEC-DEBUT                                  
V5.1.P           MOVE W-CC-V-U  TO I-DEC-FIN                                    
V5.1.P           PERFORM COMPRESSE-LIGNES                                       
V5.1.P           MOVE I-DEC-DEBUT  TO W-CC-V-U                                  
|                ADD 1 TO W-CC-S-I                                              
|                IF W-CC-S-I > W-CC-S-MAX                                       
|                   MOVE 'TAB SERVICES MAX' TO W-MESSAGE                        
|                   PERFORM ERREUR-PRG                                          
|                END-IF                                                         
|             END-IF                                                            
|          END-PERFORM.                                                         
|          MOVE W-CC-S-I  TO W-CC-S-U.                                          
           PERFORM VARYING W-CC-V-I FROM 1 BY 1                                 
             UNTIL W-CC-V-I > W-CC-V-U                                          
              IF W-CC-V-CTYPENREG(W-CC-V-I) = '1'                               
                 PERFORM RAPPROCHE-LES-SERVICES                                 
              END-IF                                                            
           END-PERFORM.                                                         
      * POINTS 2 ET 3: ON INSERE LES LIGNES ET MULTIPLIE LES ENSEMBLES          
           PERFORM VARYING W-CC-V-I FROM 1 BY 1                                 
             UNTIL W-CC-V-I > W-CC-V-U                                          
              IF (W-CC-V-CTYPENREG(W-CC-V-I) = '1')                             
               AND (W-CC-V-QTY(W-CC-V-I) > 1)                                   
               AND (W-CC-V-NB-SERVICES(W-CC-V-I) > 0)                           
      * POINT2: ON INSERE LES LIGNES                                            
                 COMPUTE I-DEC-DEBUT = W-CC-V-I                                 
                                    + W-CC-V-NB-SERVICES(W-CC-V-I) + 1          
                 COMPUTE I-DEC-FIN   = W-CC-V-U                                 
                 COMPUTE I-DEC-NB    = (W-CC-V-QTY(W-CC-V-I) - 1)               
                                    * (W-CC-V-NB-SERVICES(W-CC-V-I) + 1)        
                 PERFORM DECALAGE-DES-LIGNES                                    
      * POINT3: ON MULTIPLIE LES ENSEMBLES                                      
                 COMPUTE I-MUL-DEBUT = W-CC-V-I                                 
                 COMPUTE I-MUL-FIN   = W-CC-V-I                                 
                                     + W-CC-V-NB-SERVICES(W-CC-V-I)             
                 COMPUTE I-MUL-NB    = W-CC-V-QTY(W-CC-V-I) - 1                 
                 MOVE 1 TO W-CC-V-QTY(W-CC-V-I)                                 
                 PERFORM MULTIPLIE-LES-ENSEMBLES                                
              END-IF                                                            
           END-PERFORM.                                                         
           PERFORM VARYING W-CC-S-I FROM  W-CC-S-U BY -1                        
             UNTIL W-CC-S-I < 1                                                 
              IF W-CC-S-CTYPENREG(W-CC-S-I) = '3'                               
                 AND W-CC-S-NUM-PS(W-CC-S-I) > SPACES                           
                 set pse-pas-collee to true                                     
                 PERFORM VARYING W-CC-V-I FROM 1 BY 1                           
                           UNTIL W-CC-V-I > W-CC-V-U                            
                              or pse-collee                                     
                    IF W-CC-S-CENREG (W-CC-S-I)                                 
                     = W-CC-V-CENREG (W-CC-V-I)                                 
                    AND W-CC-S-CODIC (W-CC-S-I)                                 
                     = W-CC-V-CODIC (W-CC-V-I)                                  
                    AND W-CC-V-NUM-PS(W-CC-V-I) <= SPACES                       
                     MOVE W-CC-S-NUM-PS(W-CC-S-I)                               
                                       TO W-CC-V-NUM-PS(W-CC-V-I)               
                     SET PSE-COLLEE TO TRUE                                     
                    END-IF                                                      
                 END-PERFORM                                                    
              END-IF                                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * POINT1: RAPPROCHEMENT DES LIGNES SERVICE                                
      ******************************************************************        
       RAPPROCHE-LES-SERVICES SECTION.                                          
      *-------------------------------                                          
+          MOVE 1 TO  W-CC-S-I                                                  
|          PERFORM VARYING W-CC-S-I FROM 1 BY 1                                 
|          UNTIL W-CC-S-I > W-CC-S-U                                            
|             IF W-CC-S-CODIC(W-CC-S-I) = W-CC-V-CODIC(W-CC-V-I)                
|             AND W-CC-V-CTYPENREG(W-CC-V-I) = '1'                              
                  AND ((W-CC-S-CTYPENREG (W-CC-S-I) = '2'                       
                       AND W-CC-V-NCODICGRP (W-CC-V-I) <= SPACES)               
                       OR (W-CC-S-CTYPENREG (W-CC-S-I) = '3'                    
                           AND (W-CC-S-NUM-PS (W-CC-S-I) <= SPACES              
                                OR (W-CC-S-NUM-PS (W-CC-S-I) > SPACES           
                                    and W-CC-S-FAIT (W-CC-S-I) not = 'O'        
                                    and W-CC-S-FAIT (W-CC-V-I) not = 'O'        
                     ))))                                                       
|                COMPUTE I-DEC-DEBUT = W-CC-V-I                                 
|                                    + W-CC-V-NB-SERVICES(W-CC-V-I) + 1         
|                COMPUTE I-DEC-FIN   = W-CC-V-U                                 
|                COMPUTE I-DEC-NB    = 1                                        
|                PERFORM DECALAGE-DES-LIGNES                                    
|                COMPUTE I-DEC-DEBUT = W-CC-V-I                                 
|                                    + W-CC-V-NB-SERVICES(W-CC-V-I) + 1         
|                MOVE W-CC-S-CTYPENREG(W-CC-S-I)                                
|                  TO W-CC-V-CTYPENREG(I-DEC-DEBUT)                             
|                MOVE W-CC-S-CENREG(W-CC-S-I)                                   
|                  TO W-CC-V-CENREG(I-DEC-DEBUT)                                
|                MOVE W-CC-S-CODIC(W-CC-S-I)                                    
|                  TO W-CC-V-CODIC(I-DEC-DEBUT)                                 
|                MOVE W-CC-S-PX-VT(W-CC-S-I)                                    
|                  TO W-CC-V-PX-VT(I-DEC-DEBUT)                                 
MP839            MOVE W-CC-S-CODICGRP(W-CC-S-I)                                 
MP839              TO W-CC-V-NCODICGRP(I-DEC-DEBUT)                             
M23862           IF W-CC-S-NUM-PS (W-CC-S-I) > SPACES                           
                 AND W-CC-S-CTYPENREG (W-CC-S-I)  = '3'                         
      * ->  on ne veut rapprocher qu'une fois la pse                            
                     MOVE 'O'   TO W-CC-S-FAIT (W-CC-V-I)                       
                     MOVE 'O'   TO W-CC-S-FAIT (W-CC-S-I)                       
                 END-IF                                                         
                 ADD 1 TO W-CC-V-NB-SERVICES (W-CC-V-I)                         
|             END-IF                                                            
|          END-PERFORM.                                                         
      ******************************************************************        
      * DECALAGE DES LIGNES DU TABLEAU DES VENTES. PARAMETRES:                  
      * I-DEC-DEBUT - PREMIERE LIGNE � DECALER                                  
      * I-DEC-FIN   - DERNIERE LIGNE � DECALER                                  
      * I-DEC-NB    - NOMBRE DE LIGNES � DECALER                                
      * ATTENTION: CETTE PROCEDURE ECRASE LES LIGNES DE DESTINATION.            
      ******************************************************************        
       DECALAGE-DES-LIGNES SECTION.                                             
      *----------------------------                                             
      * EST-CE QUE LE DECALAGE EST UTILE...                                     
           IF I-DEC-NB < 1                                                      
              GO FIN-DECALAGE-DES-LIGNES                                        
           END-IF.                                                              
      * ...EST-CE QUE LE DECALAGE EST POSSIBLE...                               
           COMPUTE I-DEC-FIN = I-DEC-FIN + I-DEC-NB.                            
           IF I-DEC-FIN > W-CC-V-MAX                                            
              MOVE 'TAB LIGNES VENTE MAXI ATTEINT' TO W-MESSAGE                 
              PERFORM ERREUR-PRG                                                
           END-IF.                                                              
      * ...EST-CE QU'IL FAUT AGRANDIR LA PARTIE UTILISEE...                     
           IF I-DEC-FIN > W-CC-V-U                                              
              MOVE I-DEC-FIN TO W-CC-V-U                                        
           END-IF.                                                              
      * ...ICI, LA PARTIE A AGRANDIR DEPASSAIT LE TABLEAU UTILISE...            
           IF I-DEC-DEBUT > I-DEC-FIN                                           
              GO FIN-DECALAGE-DES-LIGNES                                        
           END-IF.                                                              
      * ...ET FINALEMENT DECALAGE DES LIGNES.                                   
           COMPUTE I-DEC-DEBUT = I-DEC-DEBUT + I-DEC-NB.                        
           PERFORM VARYING I-DEC-FIN FROM I-DEC-FIN BY -1                       
             UNTIL I-DEC-FIN < I-DEC-DEBUT                                      
              COMPUTE I-DEC-I = I-DEC-FIN - I-DEC-NB                            
              MOVE W-CC-VENTE(I-DEC-I) TO W-CC-VENTE(I-DEC-FIN)                 
           END-PERFORM.                                                         
       FIN-DECALAGE-DES-LIGNES. EXIT.                                           
       COMPRESSE-LIGNES SECTION.                                                
           if I-DEC-DEBUT < I-DEC-FIN                                           
           PERFORM VARYING I-DEC-DEBUT FROM I-DEC-DEBUT BY 1                    
             UNTIL I-DEC-DEBUT > I-DEC-FIN - 1                                  
              COMPUTE I-DEC-I = I-DEC-DEBUT + 1                                 
              MOVE W-CC-VENTE(I-DEC-I) TO W-CC-VENTE(I-DEC-DEBUT)               
              move spaces              to W-CC-VENTE(I-DEC-I)                   
           END-PERFORM                                                          
           END-IF.                                                              
           compute I-DEC-DEBUT = I-DEC-DEBUT - 1.                               
       F-COMPRESSE-LIGNES. EXIT.                                                
      ******************************************************************        
      * Multiplication des ensembles. Param�tres:                               
      * I-MUL-DEBUT - premi�re ligne � multiplier                               
      * I-MUL-FIN   - derni�re ligne � multiplier                               
      * I-MUL-NB    - nombre de fois que l'on doit multiplier                   
      * Attention: cette proc�dure �crase les lignes de destination.            
      ******************************************************************        
       MULTIPLIE-LES-ENSEMBLES SECTION.                                         
      *--------------------------------                                         
           IF I-MUL-NB < 1                                                      
              GO FIN-MULTIPLIE-LES-ENSEMBLES                                    
           END-IF.                                                              
           PERFORM VARYING I-MUL-NB FROM I-MUL-NB BY -1                         
             UNTIL I-MUL-NB < 1                                                 
              COMPUTE I-A = I-MUL-FIN - I-MUL-DEBUT + 1                         
              PERFORM VARYING I-A FROM I-A BY -1 UNTIL I-A < 1                  
                 COMPUTE I-DEC-DEBUT = I-MUL-DEBUT + I-A - 1                    
                 COMPUTE I-DEC-FIN   = I-DEC-DEBUT +                            
                          ((I-MUL-FIN - I-MUL-DEBUT + 1) * I-MUL-NB)            
                 MOVE W-CC-VENTE(I-DEC-DEBUT) TO W-CC-VENTE(I-DEC-FIN)          
              END-PERFORM                                                       
           END-PERFORM.                                                         
       FIN-MULTIPLIE-LES-ENSEMBLES. EXIT.                                       
      ******************************************************************        
      * Recherche de la plateforme de Livraison pour toute la vente,            
      * puis p�rim�tre (on en aura besoin pour les quotas).                     
      ******************************************************************        
       RECHERCHE-PLATEFORME SECTION.                                            
      *-----------------------------                                            
           SET GV10-WCQERESF-NORMAL TO TRUE.                                    
      * PLATEFORME                                                              
           MOVE W-CC-CELEMEN TO LIVRD-CTABLEG2.                                 
           EXEC SQL SELECT LIVRG.CTABLEG2, LIVRG.WTABLEG                        
                      INTO :LIVRG-CTABLEG2, :LIVRG-WTABLEG                      
                      FROM RVGA0100 LIVRD, RVGA0100 LIVRG                       
                     WHERE LIVRD.CTABLEG1 = 'LIVRD'                             
                       AND LIVRG.CTABLEG1 = 'LIVRG'                             
                       AND SUBSTR(LIVRD.CTABLEG2, 6) = :LIVRD-CTABLEG2          
                       AND SUBSTR(LIVRD.CTABLEG2, 1, 5) = LIVRG.CTABLEG2        
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'SELECT LIVRD/G:' TO W-MESSAGE                               
              MOVE LIVRD-CTABLEG2(1:10) TO W-MESSAGE(17:)                       
              MOVE LIVRG-CTABLEG2(1:5)  TO W-MESSAGE(29:)                       
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      * RECHERCHE DE MAGASIN DE REMPLACEMENT (SOC040 VERS "MGI")                
      * (CE N'EST PAS DES MGI, C'EST DES MGC MAIS C'EST PRESQUE DES MGI)        
           MOVE SPACES TO EC007-CTABLEG2.                                       
           MOVE LIVRG-LDEPLIV       TO EC007-NSOCPLT.                           
           MOVE LIVRG-LDEPLIV(4:)   TO EC007-NLIEUPLT.                          
           MOVE W-CC-NSOC-EN-TRT    TO EC007-NSOCMAG.                           
           MOVE W-CC-NLIEU-ECOM     TO EC007-NLIEUMAG.                          
           EXEC SQL SELECT WTABLEG INTO :EC007-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'EC007'                                   
                       AND CTABLEG2 = :EC007-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE 'SELECT EC007:' TO W-MESSAGE                                 
              MOVE EC007-CTABLEG2  TO W-MESSAGE(15:)                            
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF (SQLCODE = 0) AND (FAIS-DES-DISPLAYS)                             
              MOVE SPACES TO W-MESSAGE                                          
              STRING                                                            
                     'Magasin: ' W-CC-NSOC-EN-TRT ' ' W-CC-NLIEU-ECOM           
                     ' transform� via EC007 en: '                               
                     EC007-NSOCMAG ' ' EC007-NLIEUMAG                           
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM DISPLAY-W-MESSAGE                                         
           END-IF.                                                              
       FIN-RECHERCHE-PLATEFORME. EXIT.                                          
      ******************************************************************        
      * CHARGEMENT D'UN TABLEAU AVEC TOUTES LES FAMILLES POUR LES LIGNES        
      * A LIVRER: ON S'EN SERVIRA POUR SAVOIR L'EQUIPE DE LIVRAISON.            
      ******************************************************************        
       AJOUTE-AU-TABLEAU-DES-FAMILLES SECTION.                                  
      *---------------------------------------                                  
           MOVE W-CC-V-CODIC(W-CC-V-I) TO GA00-NCODIC.                          
           PERFORM SELECT-RTGA00.                                               
      * jusqu'� ce qu'on d�passe ou poste vide ou poste trouv�                  
           PERFORM VARYING T-F-I FROM 1 BY 1                                    
             UNTIL (T-F-I > T-F-MAX)                                            
                OR (T-F-CFAM(T-F-I) = ' ')                                      
                OR (T-F-CFAM(T-F-I) = GA00-CFAM)                                
           END-PERFORM.                                                         
      * impossible en principe (voir commarea, n'a que 50 postes)               
           IF T-F-I > T-F-MAX                                                   
              MOVE 'TAB FAMILLES MAXI ATTEINT' TO W-MESSAGE                     
              PERFORM ERREUR-PRG                                                
           END-IF.                                                              
      * save dans le poste qu'on a eu tant de mal (!!) � trouver                
           MOVE GA00-CFAM TO T-F-CFAM(T-F-I).                                   
      * -> RECUPERATION DE L'�QUIPE ASSOCI� AU CADRE � CE NIVEAU                
      *    CAR IL EST D�FINI SUR L'ASSOCIATION CADRE/FAMILLE PRODUIT.           
      *    POUR L'INSTANT JE NE TESTE PAS LA COH�RENCE SUR LES �QUIPES          
      *    EN CAS DE VENTE MULTI FAMILLE.                                       
           IF NOT W-CADRE-NON                                                   
              PERFORM SELECT-CADRE                                              
              MOVE CADRE-EQUIPE TO  T-F-EQ-CADR(T-F-I)                          
                                    W-EQUIPE-CADRE                              
           END-IF.                                                              
      * set T-F-U au dernier poste utilis�                                      
           PERFORM VARYING T-F-U FROM T-F-MAX BY -1                             
             UNTIL T-F-CFAM(T-F-U) > ' '                                        
           END-PERFORM.                                                         
      ******************************************************************        
      * Recherche de l'�quipe de livraison: comme tu peux le voir:              
      * - si on trouve dans la LIVRP, on prend celle-l�.                        
      * - sinon, on recherche par famille, MAIS � la fin, petit verrou          
      *   en dur: si on est sur Paris et qu'on n'est pas sur Reims, on          
      *   est sur Mitry.                                                        
      * Quand on a l'�quipe, on a le profil de tourn�e par TPROD                
      ******************************************************************        
       RECHERCHE-EQUIPE SECTION.                                                
      *-------------------------                                                
           MOVE SPACES TO GQ04-CEQUIP1 GQ03-CEQUIP.                             
           MOVE LIVRG-CTABLEG2 TO GQ04-CPERIMETRE.                              
           PERFORM SELECT-LIVRP.                                                
V5.1.P     IF NOT W-CADRE-NON                                                   
             if W-EQUIPE-CADRE NOT = '     ' and 'XXXXX' AND LOW-VALUE          
              MOVE W-EQUIPE-CADRE TO LIVRP-CEQUIP                               
            end-if                                                              
           END-IF                                                               
           IF LIVRP-CEQUIP NOT = '     ' AND 'XXXXX' AND LOW-VALUE              
              MOVE LIVRP-CEQUIP TO GQ04-CEQUIP1 GQ03-CEQUIP                     
           ELSE                                                                 
              PERFORM RECHERCHE-EQUIPE-PAR-FAMILLE                              
              IF    (W-CC-NSOC-EN-TRT = '907')                                  
                AND (GQ04-CEQUIP1 NOT = 'REIMS')                                
                 MOVE 'MITRY' TO GQ03-CEQUIP                                    
              END-IF                                                            
           END-IF.                                                              
           PERFORM SELECT-TPROD.                                                
      ******************************************************************        
      * Il faut choisir l'�quipe qui:                                           
      * - sert TOUTES les familles                                              
      * - a le poids le plus fort... ceci demande des explications...           
      *                                                                         
      * on a un tableau de la forme suivante, qui regroupe TOUTES les           
      * familles A LIVRER d'une vente:                                          
      *                                                                         
      *       Famille Equipe1 Equipe2 Equipe3 Equipe4 Equipe5                   
      *       ------- ------- ------- ------- ------- -------                   
      *       TVC     EQ1     EQ2     EQ3     EQ4                               
      *       TLPRO   EQ3     EQ1                                               
      *       TVPLA   EQ1     EQ2             EQ4     EQ3                       
      *       UNCEN   EQ4     EQ2     EQ3     EQ1                               
      *                                                                         
      * En sachant qu'il y a 5 �quipes maxi et que plus l'�quipe est �          
      * gauche plus elle a de poids: Equipe1 a un poids de 5, Equipe2 de        
      * 4, etc... jusqu'� Equipe5 qui a un poids 1.                             
      *                                                                         
      * Dans l'exemple, seules les �quipes EQ1 et EQ3 nous int�ressent:         
      * ce sont celles qui sont pr�sentes sur TOUTES les lignes.                
      *                                                                         
      * Puis, on calcule le poids des �quipes:                                  
      *   EQ1 est pr�sente 2 fois en 1�re position, 1 fois en 2�me et 1         
      *   fois en 4�me: (2x5) + (1x4) + (1x2) = 16                              
      *   EQ3 est pr�sente 1 fois en 1�re position, 2 fois en 2�me et 1         
      *   fois en 5�me: (1x5) + (2x3) + (1x1) = 12                              
      *                                                                         
      * On prend donc EQ1, qui a le plus fort poids.                            
      *                                                                         
      * Maintenant, parlons Cobol: ce sera mieux de le d�tailler �              
      * chaque boucle...                                                        
      ******************************************************************        
       RECHERCHE-EQUIPE-PAR-FAMILLE SECTION.                                    
      *-------------------------------------                                    
      * chargement du tableau des equipes dans le tableau des familles          
      * (on est en tableau � deux dimensions: pour une famille, cinq            
      *  �quipes possibles).                                                    
           PERFORM VARYING T-F-I FROM 1 BY 1 UNTIL T-F-I > T-F-U                
              PERFORM SELECT-RTGQ04                                             
           END-PERFORM.                                                         
      * On va d'abord (au cas o�) supprimer une possible incoh�rence:           
      * la m�me �quipe ne doit pas se trouver plus d'une fois sur la            
      * m�me ligne (penses-y, c'est d�bile). De plus, la m�canique ne           
      * fonctionnerait pas si c'�tait le cas (le poids serait faux).            
           PERFORM VARYING T-F-I FROM 1 BY 1 UNTIL T-F-I > T-F-U                
              PERFORM VARYING T-E-I FROM 1 BY 1 UNTIL T-E-I > T-E-MAX           
                 IF T-F-CEQUIP(T-F-I, T-E-I) > ' '                              
                    COMPUTE T-E-I-B = T-E-I + 1                                 
                    PERFORM VARYING T-E-I-B FROM 1 BY 1                         
                      UNTIL (T-E-I-B > T-E-MAX)                                 
                       IF T-F-CEQUIP(T-F-I, T-E-I)                              
                            = T-F-CEQUIP(T-F-I, T-E-I-B)                        
                          MOVE SPACES TO T-F-CEQUIP(T-F-I, T-E-I-B)             
                       END-IF                                                   
                    END-PERFORM                                                 
                 END-IF                                                         
              END-PERFORM                                                       
           END-PERFORM.                                                         
      * Puis... on parcourt ledit tableau pour virer les �quipes qui ne         
      * sont pas pr�sentes dans TOUTES les lignes:                              
      * pour chaque famille....                                                 
           PERFORM VARYING T-F-I FROM 1 BY 1 UNTIL T-F-I > T-F-U                
      * pour chaque �quipe de cette famille renseign�e....                      
              PERFORM VARYING T-E-I FROM 1 BY 1 UNTIL T-E-I > T-E-MAX           
                 IF T-F-CEQUIP(T-F-I, T-E-I) > ' '                              
      * on parcourt le tableau � partir de la prochaine famille...              
                    COMPUTE T-F-I-B = T-F-I + 1                                 
                    PERFORM VARYING T-F-I-B FROM T-F-I-B BY 1                   
                      UNTIL T-F-I-B > T-F-U                                     
      * pour voir si l'�quipe de d�part se trouve sur chaque famille...         
                       PERFORM VARYING T-E-I-B FROM 1 BY 1                      
                         UNTIL (T-E-I-B > T-E-MAX)                              
                            OR (T-F-CEQUIP(T-F-I, T-E-I)                        
                                = T-F-CEQUIP(T-F-I-B, T-E-I-B))                 
                       END-PERFORM                                              
      * et si ce n'est pas le cas, on met � blanc cette �quipe                  
                       IF T-E-I-B > T-E-MAX                                     
                          COMPUTE T-F-I-B = T-F-I + 1                           
                          PERFORM VARYING T-F-I-B FROM T-F-I-B BY 1             
                            UNTIL T-F-I-B > T-F-U                               
                             PERFORM VARYING T-E-I-B FROM 1 BY 1                
                               UNTIL T-E-I-B > T-E-MAX                          
                                IF T-F-CEQUIP(T-F-I, T-E-I)                     
                                     = T-F-CEQUIP(T-F-I-B, T-E-I-B)             
                                   MOVE SPACES                                  
                                     TO T-F-CEQUIP(T-F-I-B, T-E-I-B)            
                                END-IF                                          
                             END-PERFORM                                        
                          END-PERFORM                                           
                          MOVE SPACES TO T-F-CEQUIP(T-F-I, T-E-I)               
                       END-IF                                                   
                    END-PERFORM                                                 
                 END-IF                                                         
              END-PERFORM                                                       
           END-PERFORM.                                                         
      * Maintenant, il ne reste dans le tableau des familles que les            
      * �quipes qui sont pr�sentes dans TOUTES les lignes.                      
      * On peut donc constituer un tableau des �quipes (il y en a au            
      * maximum 5) et calculer leur poids.                                      
           INITIALIZE TAB-CEQUIP.                                               
      * pour chaque poste du tableau des �quipes...                             
           PERFORM VARYING T-E-I-B FROM 1 BY 1 UNTIL T-E-I-B > T-E-MAX          
      * ... on parcourt le tableau des familles...                              
              PERFORM VARYING T-F-I FROM 1 BY 1 UNTIL T-F-I > T-F-U             
      * ... et pour chaque nouvelle �quipe trouv�e on cumule le poids.          
                 PERFORM VARYING T-E-I FROM 1 BY 1 UNTIL T-E-I > T-E-MAX        
                    IF (T-E-CEQUIP(T-E-I-B) = ' ')                              
                      AND (T-F-CEQUIP(T-F-I, T-E-I) > ' ')                      
                       MOVE T-F-CEQUIP(T-F-I, T-E-I)                            
                         TO T-E-CEQUIP(T-E-I-B)                                 
                    END-IF                                                      
                    IF T-F-CEQUIP(T-F-I, T-E-I) = T-E-CEQUIP(T-E-I-B)           
                       COMPUTE  T-E-POIDS(T-E-I-B)                              
                             =  T-E-POIDS(T-E-I-B) + (6 - T-E-I)                
                       MOVE SPACES TO T-F-CEQUIP(T-F-I, T-E-I)                  
                    END-IF                                                      
                 END-PERFORM                                                    
              END-PERFORM                                                       
           END-PERFORM.                                                         
      * finalement, on prend celle qui a le poids le plus fort.                 
      * Ouf!                                                                    
           PERFORM VARYING T-E-I FROM 1 BY 1 UNTIL T-E-I > T-E-MAX              
              IF (T-E-CEQUIP(T-E-I) > ' ')                                      
                AND (T-E-POIDS(T-E-I) > T-E-POIDS-MAX)                          
                 MOVE T-E-CEQUIP(T-E-I) TO GQ04-CEQUIP1 GQ03-CEQUIP             
              END-IF                                                            
           END-PERFORM.                                                         
      ******************************************************************        
      * Recherche des Quotas de Livraison                                       
      * on regarde le quota de GQ03                                             
      *   - s'il y a du quota, c'est bon                                        
      *   - si non, on ne peut pas livrer -> ZZZZZ                              
      ******************************************************************        
       MISE-A-JOUR-QUOTA SECTION.                                               
      *--------------------------                                               
           PERFORM SELECT-RTGQ03.                                               
           IF SQLCODE = 0 OR -811                                               
              PERFORM UPDATE-RTGQ03                                             
              PERFORM APPEL-MEC24                                               
           ELSE                                                                 
              PERFORM SELECT-EQUIG                                              
           END-IF.                                                              
      ******************************************************************        
      * Insertion d'Ent�te de Vente - GV10                                      
      ******************************************************************        
       CREATION-ENTETE SECTION.                                                 
MGD15 *    INITIALIZE RVGV1004.                                                 
MGD15      INITIALIZE RVGV1005.                                                 
           IF W-CC-VENTE-CLICK-COLLECT                                          
              MOVE W-CC-NSOC-EN-TRT    TO GV10-NSOCIETE                         
              MOVE W-NLIEU-CC          TO GV10-NLIEU                            
           ELSE                                                                 
LA2203        INITIALIZE COMM-MEC64-APPLI                                       
LA2203                   WN-I-CRE                                               
              IF GV10-WCQERESF-NORMAL                                           
                 MOVE EC007-NSOCMAG    TO GV10-NSOCIETE                         
                 MOVE EC007-NLIEUMAG   TO GV10-NLIEU                            
              ELSE                                                              
                 MOVE W-CC-NSOC-EN-TRT TO GV10-NSOCIETE                         
                 MOVE W-CC-NLIEU-ECOM  TO GV10-NLIEU                            
              END-IF                                                            
           END-IF.                                                              
           PERFORM SELECT-RTGA10-NZONPRIX.                                      
           PERFORM SELECT-MAX-GV10-NORDRE.                                      
           MOVE W-GV10-NORDRE-PICX     TO GV10-NORDRE.                          
           PERFORM METS-EN-FORME-GV10-NVENTE.                                   
           MOVE W-GV10-NVENTE-PICX7    TO GV10-NVENTE.                          
           MOVE W-CC-DATE-DU-JOUR      TO GV10-DVENTE.                          
      * CICS, t'as l'heure, s'il te pla�t?                                      
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME                TO W-EIBTIME.                            
           MOVE W-HEURE                TO GV10-DHVENTE.                         
           MOVE W-MINUTE               TO GV10-DMVENTE.                         
???        MOVE '1'                    TO GV10-CREMVTE.                         
           PERFORM MISE-EN-FORME-LCOMVTE.                                       
           MOVE 'N'                    TO GV10-WFACTURE.                        
           MOVE 'N'                    TO GV10-WEXPORT                          
           MOVE 'N'                    TO GV10-WDETAXEC                         
           MOVE 'N'                    TO GV10-WDETAXEHC                        
      *    GV10-CORGORED                                                        
      *    GV10-CMODPAIMTI                                                      
      *    GV10-LDESCRIPTIF1 commentaires                                       
      *    GV10-LDESCRIPTIF2                                                    
      *    GV10-DLIVRBL calcul� lors des lignes GV11                            
      *    GV10-NFOLIOBL reste � blanc                                          
      *    GV10-LAUTORM reste � blanc                                           
      *    GV10-NAUTORD  reste � blanc                                          
           MOVE W-SPDATDB2-DSYST       TO GV10-DSYST.                           
      *    GV10-DSTAT reste � blanc                                             
      *    GV10-DFACTURE reste � blanc                                          
      *    GV10-CACID reste � blanc                                             
      *    GV10-NSOCMODIF reste � blanc                                         
      *    GV10-NLIEUMODIF reste � blanc                                        
           MOVE W-CC-NSOC-ECOM         TO GV10-NSOCP.                           
           MOVE W-CC-NLIEU-ECOM        TO GV10-NLIEUP.                          
           MOVE W-CC-CDEVISE           TO GV10-CDEV.                            
      *    GV10-CFCRED reste � blanc                                            
      *    GV10-NCREDI reste � blanc                                            
      *    GV10-NSEQNQ  calcul� lors des lignes GV11                            
      *    l'insert GV10 est fait plus tard, car il y a des zones qui           
      *    d�pendent des lignes: DLIVRBL, NSEQNQ, montants, etc...              
      *    PERFORM INSERT-RTGV10-EC02. � ne pas faire, pas touche!              
      * par contre, on peut ins�rer l'ent�te du mouchard sans probl�me          
           PERFORM CREE-MOUCHARD-ENTETE-VENTE.                                  
      ******************************************************************        
      * Commentaire de vente en proc parce que c'est un peu compliqu�           
      ******************************************************************        
       MISE-EN-FORME-LCOMVTE SECTION.                                           
      *------------------------------                                           
           MOVE W-CC-E-CMENT-VT TO W-CMENT.                                     
           IF W-CC-E-REPR-MAT = '1'                                             
              MOVE 'REPRISE ANCIEN MATERIEL' TO GV10-LCOMVTE1                   
              MOVE 2 TO I-LCOMVTE                                               
           ELSE                                                                 
              MOVE 1 TO I-LCOMVTE                                               
           END-IF.                                                              
           PERFORM VARYING I-LCOMVTE FROM I-LCOMVTE BY 1                        
             UNTIL I-LCOMVTE > 4                                                
      *{ Tr-Hexa-Map 1.5                                                        
      *       PERFORM UNTIL W-CMENT(1:1) NOT = X'25' AND ' '                    
      *--                                                                       
              PERFORM UNTIL W-CMENT(1:1) NOT = X'85' AND ' '                    
      *}                                                                        
                 MOVE W-CMENT(2:) TO W-CMENT                                    
                 IF W-CMENT = ' '                                               
                    GO FIN-MISE-EN-FORME-LCOMVTE                                
                 END-IF                                                         
              END-PERFORM                                                       
              PERFORM VARYING I-CMENT FROM 1 BY 1                               
      *{ Tr-Hexa-Map 1.5                                                        
      *          UNTIL (I-CMENT > 30) OR (W-CMENT(I-CMENT:1) = X'25')           
      *--                                                                       
                 UNTIL (I-CMENT > 30) OR (W-CMENT(I-CMENT:1) = X'85')           
      *}                                                                        
              END-PERFORM                                                       
      *{ Tr-Hexa-Map 1.5                                                        
      *       IF (I-CMENT <= 30) AND (W-CMENT(I-CMENT:1) = X'25')               
      *--                                                                       
              IF (I-CMENT <= 30) AND (W-CMENT(I-CMENT:1) = X'85')               
      *}                                                                        
                 SUBTRACT 1 FROM I-CMENT                                        
                 EVALUATE I-LCOMVTE                                             
                    WHEN 1 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE1             
                    WHEN 2 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE2             
                    WHEN 3 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE3             
                    WHEN 4 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE4             
                 END-EVALUATE                                                   
                 ADD 2 TO I-CMENT                                               
                 MOVE W-CMENT(I-CMENT:) TO W-CMENT                              
              ELSE                                                              
                 PERFORM VARYING I-CMENT FROM 30 BY -1                          
                    UNTIL (I-CMENT = 2) OR (W-CMENT(I-CMENT:1) = ' ')           
                 END-PERFORM                                                    
                 SUBTRACT 1 FROM I-CMENT                                        
                 EVALUATE I-LCOMVTE                                             
                    WHEN 1 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE1             
                    WHEN 2 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE2             
                    WHEN 3 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE3             
                    WHEN 4 MOVE W-CMENT(1:I-CMENT) TO GV10-LCOMVTE4             
                 END-EVALUATE                                                   
                 ADD 2 TO I-CMENT                                               
                 MOVE W-CMENT(I-CMENT:) TO W-CMENT                              
              END-IF                                                            
           END-PERFORM.                                                         
       FIN-MISE-EN-FORME-LCOMVTE. EXIT.                                         
      ******************************************************************        
      * Insertion de l'Adresse Client                                           
      ******************************************************************        
       CREATION-ADRESSE SECTION.                                                
V3.5  *    INITIALIZE RVGV0202 RVGV0300.                                        
V3.5       INITIALIZE RVGV0202 rvgv0303 RVGV0401.                               
           MOVE GV10-NSOCIETE          TO GV02-NSOCIETE GV03-NSOCIETE.          
           MOVE GV10-NLIEU             TO GV02-NLIEU    GV03-NLIEU.             
           MOVE GV10-NORDRE            TO GV02-NORDRE   GV03-NORDRE.            
           MOVE GV10-NVENTE            TO GV02-NVENTE   GV03-NVENTE.            
           IF W-CC-E-NOM-FACT > ' '                                             
V41R1         MOVE 'B'                 TO GV02-WTYPEADR GV03-WTYPEADR           
V3.5          MOVE W-CC-E-IDENT-CLIENT TO GV02-IDCLIENT                         
           ELSE                                                                 
              MOVE 'A'                 TO GV02-WTYPEADR GV03-WTYPEADR           
              MOVE W-CC-E-EMAIL        TO GV03-EMAIL                            
              MOVE W-CC-E-IDENT-CLIENT TO GV02-IDCLIENT                         
           END-IF.                                                              
           MOVE W-CC-E-CIVIL           TO GV02-CTITRENOM.                       
           IF W-CC-VENTE-CLICK-COLLECT                                          
              MOVE FUNCTION UPPER-CASE (W-CC-E-NOM-LIV)                         
                                       TO GV02-LNOM                             
           ELSE                                                                 
              MOVE W-CC-E-NOM-LIV      TO GV02-LNOM                             
           END-IF.                                                              
           MOVE W-CC-E-PNOM-LIV        TO GV02-LPRENOM.                         
           MOVE W-CC-E-BMENT           TO GV02-LBATIMENT.                       
           MOVE W-CC-E-ESCAL           TO GV02-LESCALIER.                       
           MOVE W-CC-E-ETAGE           TO GV02-LETAGE.                          
           MOVE W-CC-E-PTE             TO GV02-LPORTE.                          
           MOVE W-CC-E-ADR1-LIV        TO MEC09C-ADRESSE-TEXTE                  
           PERFORM FAIS-UN-LINK-A-MEC09                                         
           IF MEC09C-PARFAIT OR MEC09C-OUI-MAIS                                 
              MOVE MEC09C-CVOIE        TO GV02-CVOIE                            
              MOVE MEC09C-CTVOIE       TO GV02-CTVOIE                           
              MOVE MEC09C-LNOMVOIE     TO GV02-LNOMVOIE                         
           ELSE                                                                 
              MOVE W-CC-E-ADR1-LIV     TO GV02-LNOMVOIE                         
           END-IF.                                                              
           IF W-CC-E-ADR2-LIV > ' '                                             
              MOVE W-CC-E-ADR2-LIV     TO GV02-LCMPAD1                          
              MOVE W-CC-E-CPT-ADR-LIV  TO GV02-LCMPAD2                          
           ELSE                                                                 
              MOVE W-CC-E-CPT-ADR-LIV  TO GV02-LCMPAD1                          
           END-IF.                                                              
           MOVE W-CC-E-CMUNE-LIV       TO GV02-LCOMMUNE.                        
           MOVE W-CC-E-C-POST-LIV      TO GV02-CPOSTAL.                         
           MOVE W-CC-E-CMUNE-LIV       TO GV02-LBUREAU.                         
           MOVE W-CC-E-TEL-FIXE        TO GV02-TELDOM.                          
           MOVE W-CC-E-TEL-PORT        TO GV02-TELBUR.                          
      *??? 02  GV02-LPOSTEBUR                                                   
           PERFORM MISE-EN-FORME-LCOMLIV.                                       
           IF (MEC09C-J-AI-RIEN-PU-FAIRE OR MEC09C-C-EST-GRAVE)                 
             AND (GV02-LCOMLIV2 <= ' ')                                         
              MOVE W-CC-E-ADR1-LIV     TO GV02-LCOMLIV2                         
           END-IF.                                                              
      *??? 02  GV02-WETRANGER                                                   
           MOVE GQ03-CZONLIV           TO GV02-CZONLIV.                         
           MOVE W-CC-E-C-INSEE-LIV     TO GV02-CINSEE.                          
      *??? 02  GV02-WCONTRA                                                     
           MOVE W-SPDATDB2-DSYST       TO GV02-DSYST GV03-DSYST.                
           PERFORM SELECT-CILOT.                                                
           IF IL-Y-A-DU-LOGITRANS                                               
              PERFORM RECHERCHE-CODE-ILOT                                       
           END-IF.                                                              
      *    02  GV02-IDCLIENT � blanc                                            
           MOVE W-CC-E-C-PAYS-LIV      TO GV02-CPAYS.                           
           MOVE W-CC-E-TEL-PORT        TO GV02-NGSM.                            
           PERFORM INSERT-RTGV02.                                               
           IF W-CC-E-NOM-FACT > ' '                                             
              IF W-CC-VENTE-CLICK-COLLECT                                       
                 MOVE FUNCTION UPPER-CASE (W-CC-E-NOM-FACT)                     
                   TO GV02-LNOM                                                 
              ELSE                                                              
                 MOVE W-CC-E-NOM-FACT     TO GV02-LNOM                          
              END-IF                                                            
              MOVE W-CC-E-PNOM-FACT       TO GV02-LPRENOM                       
              MOVE W-CC-E-ADR1-FACT       TO MEC09C-ADRESSE-TEXTE               
              PERFORM FAIS-UN-LINK-A-MEC09                                      
              IF MEC09C-PARFAIT OR MEC09C-OUI-MAIS                              
                 MOVE MEC09C-CVOIE        TO GV02-CVOIE                         
                 MOVE MEC09C-CTVOIE       TO GV02-CTVOIE                        
                 MOVE MEC09C-LNOMVOIE     TO GV02-LNOMVOIE                      
                 MOVE W-CC-E-ADR2-FACT    TO GV02-LCMPAD1                       
                 MOVE W-CC-E-CPT-ADR-FACT TO GV02-LCMPAD2                       
              ELSE                                                              
                 MOVE W-CC-E-ADR1-FACT    TO GV02-LNOMVOIE                      
                 MOVE W-CC-E-ADR2-FACT    TO GV02-LCMPAD1                       
                 MOVE W-CC-E-CPT-ADR-FACT TO GV02-LCMPAD2                       
              END-IF                                                            
              MOVE SPACES    TO GV02-LBATIMENT GV02-LESCALIER                   
                                GV02-LETAGE GV02-LPORTE GV02-CILOT              
              MOVE W-CC-E-C-POST-FACT  TO GV02-CPOSTAL                          
              MOVE W-CC-E-C-PAYS-FACT  TO GV02-CPAYS                            
              MOVE W-CC-E-C-INSEE-FACT TO GV02-CINSEE                           
              MOVE W-CC-E-CMUNE-FACT   TO GV02-LCOMMUNE                         
???           MOVE W-CC-E-CMUNE-FACT   TO GV02-LBUREAU                          
              MOVE 'A'                 TO GV02-WTYPEADR                         
V3.5          MOVE W-CC-E-EMAIL        TO GV03-EMAIL                            
V3.5          MOVE W-CC-E-IDENT-CLIENT TO GV02-IDCLIENT                         
              PERFORM INSERT-RTGV02                                             
           END-IF.                                                              
V3.5       MOVE W-CC-E-CODE            TO GV03-CPORTE                           
V3.5       MOVE W-CC-u-asc           TO GV03-wasc                               
V3.5       MOVE GV10-NSOCIETE          TO GV04-NSOCIETE                         
V3.5       MOVE GV10-NLIEU             TO GV04-NLIEU                            
V3.5       MOVE GV10-NVENTE            TO GV04-NVENTE                           
V3.5       MOVE GV10-DVENTE            TO GV04-DCREATION                        
V3.5       MOVE W-CC-E-DATE-NAISS      TO GV04-DATENAISS                        
V3.5       MOVE W-CC-E-CODE-NAISS      TO GV04-CPNAISS                          
V3.5       MOVE W-CC-E-VILLE-NAISS     TO GV04-LIEUNAISS                        
V3.5       MOVE W-SPDATDB2-DSYST       TO GV04-DSYST                            
           move W-CC-u-asc-u           to gv04-WASCUTIL                         
           move W-CC-u-marches         to gv04-wmarches                         
           move W-CC-u-haut-col        to gv04-wcol.                            
V41R0      IF W-CC-E-NB-REPRISE > SPACE                                         
    |         MOVE W-CC-E-NB-REPRISE   TO WS-MASQZ2                             
    |         MOVE WS-MASQZ2           TO GV04-NBPRDREP                         
    |      ELSE                                                                 
    |         MOVE 0                   TO GV04-NBPRDREP                         
    |      END-IF                                                               
    |      MOVE W-CC-E-ACCES-LOG       TO GV04-WACCES                           
    |      MOVE W-CC-E-PDT-VOLUMIQ     TO GV04-WREPRISE                         
              PERFORM INSERT-RTGV03.                                            
V3.5          PERFORM INSERT-RTGV04.                                            
      ******************************************************************        
      * Commentaire de livraison en proc aussi (c'est compliqu�, eh oui)        
      ******************************************************************        
       MISE-EN-FORME-LCOMLIV SECTION.                                           
      *------------------------------                                           
      * on met le num�ro Darty.Com dans ledit commentaire, parce que            
      * comme �a il sera imprim� et donc il l'aura sur le BC                    
v5.1.p*    MOVE W-CC-E-NUM-CMD-DC TO PIC9-NCDEWC.                               
           PERFORM UNTIL PICX-NCDEWC(1:1) > ' '                                 
              MOVE PICX-NCDEWC(2:) TO PICX-NCDEWC                               
           END-PERFORM.                                                         
           MOVE PICX-NCDEWC TO W-CMENT.                                         
           PERFORM VARYING I-CMENT FROM 15 BY -1                                
             UNTIL W-CMENT(I-CMENT:1) > ' '                                     
           END-PERFORM.                                                         
           ADD 2 TO I-CMENT.                                                    
      * y a-t-il un code (pour rentrer dans la r�sidence, par exemple)          
           IF W-CC-E-CODE > ' '                                                 
              MOVE 'CODE' TO W-CMENT(I-CMENT:)                                  
              ADD 5 TO I-CMENT                                                  
              MOVE W-CC-E-CODE TO W-CMENT(I-CMENT:)                             
              PERFORM VARYING I-CMENT FROM 32 BY -1                             
                UNTIL W-CMENT(I-CMENT:1) > ' '                                  
              END-PERFORM                                                       
              ADD 2 TO I-CMENT                                                  
           END-IF.                                                              
      * on cadre l'�ventuel commentaire � gauche...                             
      *{ Tr-Hexa-Map 1.5                                                        
      *    PERFORM UNTIL (W-CC-E-CMENT-LIV(1:1) NOT = X'25' AND ' ')            
      *--                                                                       
           PERFORM UNTIL (W-CC-E-CMENT-LIV(1:1) NOT = X'85' AND ' ')            
      *}                                                                        
                      OR (W-CC-E-CMENT-LIV = ' ')                               
              MOVE W-CC-E-CMENT-LIV(2:) TO W-CC-E-CMENT-LIV                     
           END-PERFORM.                                                         
           MOVE W-CC-E-CMENT-LIV TO W-CMENT(I-CMENT:).                          
      * et on le charge dans les 2 zones de commentaire de livraison            
           PERFORM VARYING I-LCOMLIV FROM 1 BY 1                                
             UNTIL I-LCOMLIV > 2                                                
      *{ Tr-Hexa-Map 1.5                                                        
      *       PERFORM UNTIL W-CMENT(1:1) NOT = X'25' AND ' '                    
      *--                                                                       
              PERFORM UNTIL W-CMENT(1:1) NOT = X'85' AND ' '                    
      *}                                                                        
                 MOVE W-CMENT(2:) TO W-CMENT                                    
                 IF W-CMENT = ' '                                               
                    GO FIN-MISE-EN-FORME-LCOMLIV                                
                 END-IF                                                         
              END-PERFORM                                                       
�             PERFORM VARYING I-CMENT FROM 1 BY 1                               
comliv*          UNTIL (I-CMENT > 78) OR (W-CMENT(I-CMENT:1) = X'25')           
      *{ Tr-Hexa-Map 1.5                                                        
comliv*          UNTIL (I-CMENT >= 78) OR (W-CMENT(I-CMENT:1) = X'25')          
      *--                                                                       
                 UNTIL (I-CMENT >= 78) OR (W-CMENT(I-CMENT:1) = X'85')          
      *}                                                                        
              END-PERFORM                                                       
comliv*       IF (I-CMENT <= 78) AND (W-CMENT(I-CMENT:1) = X'25')               
      *{ Tr-Hexa-Map 1.5                                                        
comliv*       IF ((I-CMENT <  78) AND (W-CMENT(I-CMENT:1) = X'25'))             
      *--                                                                       
              IF ((I-CMENT <  78) AND (W-CMENT(I-CMENT:1) = X'85'))             
      *}                                                                        
comliv        OR (I-CMENT =  78)                                                
                 SUBTRACT 1 FROM I-CMENT                                        
                 EVALUATE I-LCOMLIV                                             
                    WHEN 1 MOVE W-CMENT(1:I-CMENT) TO GV02-LCOMLIV1             
                    WHEN 2 MOVE W-CMENT(1:I-CMENT) TO GV02-LCOMLIV2             
                 END-EVALUATE                                                   
                 ADD 2 TO I-CMENT                                               
                 MOVE W-CMENT(I-CMENT:) TO W-CMENT                              
              ELSE                                                              
                 PERFORM VARYING I-CMENT FROM 78 BY -1                          
                    UNTIL (I-CMENT = 2) OR (W-CMENT(I-CMENT:1) = ' ')           
                 END-PERFORM                                                    
                 SUBTRACT 1 FROM I-CMENT                                        
                 EVALUATE I-LCOMLIV                                             
                    WHEN 1 MOVE W-CMENT(1:I-CMENT) TO GV02-LCOMLIV1             
                    WHEN 2 MOVE W-CMENT(1:I-CMENT) TO GV02-LCOMLIV2             
                 END-EVALUATE                                                   
                 ADD 2 TO I-CMENT                                               
                 MOVE W-CMENT(I-CMENT:) TO W-CMENT                              
              END-IF                                                            
           END-PERFORM.                                                         
       FIN-MISE-EN-FORME-LCOMLIV. EXIT.                                         
      ******************************************************************        
      * Cr�ation d'une ligne livr�e:                                            
      * - le stock entrep�t a d�j� �t� contr�l�; on va mettre � jour le         
      *   stock GS10 et la ligne de mut concern�e (ou la cr�er).                
      ******************************************************************        
       CREATION-LIGNE-LIVREE SECTION.                                           
      *------------------------------                                           
MGD15 *    INITIALIZE RVGV1105.                                                 
MGD15      INITIALIZE RVGV1106.                                                 
           add 1 to w-nseqens                                                   
           move w-nseqens           to gv11-nseqens.                            
           MOVE GV10-NSOCIETE       TO GV11-NSOCIETE.                           
           MOVE GV10-NLIEU          TO GV11-NLIEU.                              
           MOVE GV10-NVENTE         TO GV11-NVENTE.                             
           MOVE GV10-NSOCIETE       TO W-CC-V-C-SOC(W-CC-V-I).                  
           MOVE GV10-NLIEU          TO W-CC-V-C-LIEU(W-CC-V-I).                 
           MOVE GV10-NVENTE         TO W-CC-V-NUM-CMD-SI(W-CC-V-I).             
           MOVE '1'                 TO GV11-CTYPENREG.                          
           MOVE W-CC-V-NCODICGRP(W-CC-V-I)  TO GV11-NCODICGRP.                  
           MOVE W-CC-V-CODIC(W-CC-V-I)      TO GV11-NCODIC.                     
           ADD 1 TO W-GV11-NLIGNE-PIC99.                                        
           MOVE W-GV11-NLIGNE-PIC99 TO W-CC-V-L-CMD-SI(W-CC-V-I).               
           MOVE W-GV11-NLIGNE-PICXX         TO GV11-NSEQ.                       
v5.1.p     MOVE W-GV11-NLIGNE-PIC99         TO GV11-NSEQNQ.                     
M19892     IF GV11-NSEQNQ > GV10-NSEQNQ                                         
"             MOVE GV11-NSEQNQ             TO GV10-NSEQNQ                       
m19892     END-IF.                                                              
v5.1.p*    MOVE 'LD2'                       TO GV11-CMODDEL                     
           MOVE W-CC-V-MODE-DELIV(W-CC-V-I) TO GV11-CMODDEL                     
           MOVE W-CC-V-DT-LIV(W-CC-V-I)     TO GV11-DDELIV.                     
      *    GV10-DLIVRBL = plus petite date de livraison de GV11-DDELIV          
           IF GV10-DLIVRBL <= ' '                                               
              MOVE GV11-DDELIV TO GV10-DLIVRBL                                  
           END-IF.                                                              
           IF GV10-DLIVRBL > GV11-DDELIV                                        
              MOVE GV11-DDELIV TO GV10-DLIVRBL                                  
           END-IF.                                                              
           MOVE GV10-NORDRE         TO GV11-NORDRE.                             
      *    GV11-CENREG reste � blanc (pour les lignes de produit)               
           MOVE W-CC-V-QTY(W-CC-V-I) TO GV11-QVENDUE.                           
           PERFORM SELECT-PRIX-GG20-OU-GA69.                                    
LD1109     IF W-CC-V-NCODICGRP(W-CC-V-I) NOT = SPACE                            
              MOVE W-CC-V-PX-VT(W-CC-V-I) TO GV11-PVUNITF                       
      *       MOVE W-CC-V-PX-VT-UNIT(W-CC-V-I) TO GV11-PVUNITF                  
      *                                           GV11-PVUNIT                   
LD1109     ELSE                                                                 
              MOVE GG20-PEXPTTC                TO GV11-PVUNITF                  
LD1109     END-IF.                                                              
LD1109*    MOVE GG20-PEXPTTC           TO GV11-PVUNITF.                         
           MOVE W-CC-V-PX-VT(W-CC-V-I) TO GV11-PVUNIT.                          
           COMPUTE GV11-PVTOTAL = GV11-QVENDUE * GV11-PVUNIT.                   
           ADD GV11-PVTOTAL            TO GV10-PTTVENTE.                        
           MOVE GQ04-CEQUIP1           TO GV11-CEQUIPE.                         
           MOVE W-GV11-NLIGNE-PICXX    TO GV11-NLIGNE.                          
           MOVE GV11-NCODIC            TO GA00-NCODIC.                          
           PERFORM SELECT-RTGA00.                                               
           IF GV11-DDELIV >= TXTVA-DATE                                         
              MOVE TXTVA-TVA TO GV11-TAUXTVA                                    
LD0410        MOVE TXTVA-TVA TO W-TVA                                           
LD0410        MOVE W-TVA     TO W-CC-V-T-TVA(W-CC-V-I)                          
           ELSE                                                                 
              MOVE TXTVA-ANCTVA TO GV11-TAUXTVA                                 
LD0410        MOVE TXTVA-ANCTVA TO W-TVA                                        
LD0410        MOVE W-TVA        TO W-CC-V-T-TVA(W-CC-V-I)                       
           END-IF.                                                              
           MOVE GA00-QCONDT               TO GV11-QCONDT.                       
stkpf      if gv11-cmoddel (2:1) = 'P'                                          
              perform cherche-stock-plateforme                                  
           else                                                                 
              PERFORM RECHERCHE-STOCK-QUOTA-MUT                                 
           end-if.                                                              
      *    PERFORM RECHERCHE-STOCK-QUOTA-MUT.                                   
      *    GV11-PRMP reste � z�ro                                               
      *    GV11-WEMPORTE 'E' soit blanc, donc liv reste � blanc                 
           MOVE W-CC-V-CREN-LIV(W-CC-V-I) TO GV11-CPLAGE.                       
           MOVE TPROD-CPROF               TO GV11-CPROTOUR                      
           IF W-CC-E-NOM-FACT > ' '                                             
              MOVE 'B' TO GV11-CADRTOUR                                         
           ELSE                                                                 
              MOVE 'A' TO GV11-CADRTOUR                                         
           END-IF.                                                              
           MOVE W-CC-E-C-POST-LIV         TO GV11-CPOSTAL.                      
      *    GV11-LCOMMENT reste � blanc                                          
      *    MOVE '009440'                  TO GV11-CVENDEUR                      
           IF W-CC-E-C-VEND NOT = SPACE                                         
              MOVE W-CC-E-C-VEND          TO GV11-CVENDEUR                      
           ELSE                                                                 
              MOVE '009440'               TO GV11-CVENDEUR                      
           END-IF                                                               
stkpf *    MOVE GS10-NSOCDEPOT            TO GV11-NSOCLIVR.                     
      *    MOVE GS10-NDEPOT               TO GV11-NDEPOT.                       
      *    GV11-NAUTORM reste � blanc                                           
      *    GV11-WARTINEX r�b, 'N' si article inexistant pour rachat PSE         
      *    GV11-WEDITBL reste � blanc                                           
      *    GV11-WCQERESF dispo=' ' cde four='F' cmq='Q' si pas r�s='Z'          
           MOVE GV11-WCQERESF-FLAG TO GV11-WCQERESF.                            
      *    GV11-CTOURNEE reste � blanc                                          
           MOVE 'N'                 TO GV11-WTOPELIVRE.                         
      *    GV11-DCOMPTA reste � blanc                                           
           MOVE GV10-DVENTE         TO GV11-DCREATION.                          
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE GV10-DHVENTE        TO GV11-HCREATION.                          
           MOVE GV10-DMVENTE        TO GV11-HCREATION(3:).                      
      *    GV11-DANNULATION reste � blanc                                       
           MOVE GV10-NLIEU          TO GV11-NLIEUORIG.                          
           MOVE 'N'                 TO GV11-WINTMAJ.                            
           MOVE W-SPDATDB2-DSYST    TO GV11-DSYST.                              
      *    GV11-DSTAT reste � blanc                                             
      *    GV11-CPRESTGRP reste � blanc                                         
           MOVE GV10-NSOCIETE       TO GV11-NSOCMODIF.                          
           MOVE GV10-NLIEU          TO GV11-NLIEUMODIF.                         
      *    GV11-DATENC reste � blanc                                            
           MOVE W-CC-CDEVISE               TO GV11-CDEV.                        
      *    GV11-WUNITR reste � blanc                                            
      *    GV11-LRCMMT reste � blanc                                            
           MOVE W-CC-NSOC-ECOM             TO GV11-NSOCP.                       
           MOVE W-CC-NLIEU-ECOM            TO GV11-NLIEUP.                      
      *    GV11-NTRANS reste � blanc                                            
      *    GV11-NSEQFV reste � blanc                                            
v5.1.p* deplacement de la ligne ci-dessous plus haut.                           
      *    MOVE W-GV11-NLIGNE-PIC99        TO GV11-NSEQNQ.                      
      *                                                                         
      *    IF GV11-NSEQNQ > GV10-NSEQNQ                                         
      *       MOVE GV11-NSEQNQ             TO GV10-NSEQNQ                       
      *    END-IF.                                                              
           MOVE W-GV11-NLIGNE-PIC99        TO GV11-NSEQREF.                     
      *    GV11-NMODIF reste � z�ro                                             
           MOVE GV10-NSOCIETE              TO GV11-NSOCORIG.                    
      *    GV11-DTOPE reste � blanc                                             
stkpf      if gv11-cmoddel (2:1) = 'P'                                          
              MOVE spaces         TO GV11-NMUTATION                             
              MOVE LIVRG-LDEPLIV           TO GV11-NSOCLIVR                     
              MOVE LIVRG-LDEPLIV           TO GV11-NSOCLIVR1                    
                                              GV11-NSOCGEST                     
                                              GV11-NSOCDEPLIV                   
              MOVE LIVRG-LDEPLIV(4:)       TO GV11-NDEPOT                       
              MOVE LIVRG-LDEPLIV(4:)       TO GV11-NDEPOT1                      
                                              GV11-NLIEUGEST                    
                                              GV11-NLIEUDEPLIV                  
           else                                                                 
              MOVE GB05-NMUTATION TO GV11-NMUTATION                             
              MOVE GS10-NSOCDEPOT          TO GV11-NSOCLIVR                     
              MOVE GS10-NDEPOT             TO GV11-NDEPOT                       
              MOVE GS10-NSOCDEPOT          TO GV11-NSOCLIVR1                    
              MOVE GS10-NDEPOT             TO GV11-NDEPOT1                      
              MOVE LIVRG-LIEUGEST          TO GV11-NSOCGEST                     
              MOVE LIVRG-LIEUGEST(4:)      TO GV11-NLIEUGEST                    
              MOVE LIVRG-LDEPLIV           TO GV11-NSOCDEPLIV                   
              MOVE LIVRG-LDEPLIV(4:)       TO GV11-NLIEUDEPLIV                  
           end-if.                                                              
           IF GV11-WCQERESF-NORMAL                                              
              MOVE '0' TO W-CC-V-C-RET(W-CC-V-I)                                
           ELSE                                                                 
              MOVE '1' TO W-CC-V-C-RET(W-CC-V-I)                                
              MOVE '1' TO W-CC-E-C-RET                                          
           END-IF.                                                              
           IF W-CC-V-CARSPEC(W-CC-V-I) > ' '                                    
              PERFORM INSERT-RTGV15                                             
              PERFORM SELECT-CARSP                                              
              IF (CARSP-WCARACTS = 'C')                                         
                OR (    (CARSP-WCARACTS = ' ')                                  
                    AND (CARSP-CVDCARAC NOT = GV15-CVCARACTSPE1))               
                 MOVE 'C'             TO GV11-WACOMMUTER                        
              END-IF                                                            
           END-IF.                                                              
           MOVE  GV11-NSEQNQ TO W-GV11-NSEQNQ.                                  
L30410     IF WS-RESER-FOURN-OK                                                 
L30410        MOVE 'F' TO GV11-WCQERESF                                         
LD0810     ELSE                                                                 
LD0810*       MOVE SPACE TO GV11-WCQERESF                                       
              MOVE GV11-WCQERESF-FLAG TO GV11-WCQERESF                          
L30410     END-IF                                                               
           PERFORM INSERT-RTGV11.                                               
V40R0      IF  W-CC-V-IS-PRECO(W-CC-V-I) > '0'                                  
               PERFORM INSERT-RTGV99                                            
           END-IF                                                               
stkpf      if gv11-cmoddel (2:1) = 'P'                                          
           PERFORM INSERT-RTGV22                                                
           else                                                                 
           PERFORM INSERT-RTGV21                                                
           end-if                                                               
           PERFORM INSERT-RTEC03.                                               
           PERFORM CREE-MOUCHARD-ENTETE-LIGNE.                                  
v5.1.p*    IF GV11-WCQERESF-EN-Z AND NOT GV11-MUTATION-OK                       
           IF GV11-WCQERESF-EN-Z AND NOT GV11-MUTATION-KO                       
              INITIALIZE MEC15C-COMMAREA                                        
              MOVE 'E' TO MEC15C-STAT                                           
              MOVE '0001' TO MEC15C-NSEQERR                                     
              MOVE GV11-NCODIC TO MEC15C-ALP(1)                                 
              PERFORM CREE-MOUCHARD-LIGNE                                       
           END-IF.                                                              
M16733     MOVE GV11-NSOCLIVR  TO MEC03C-SOCIETE-ENT(MEC03C-NBP-MAX + 1)        
           MOVE GV11-NDEPOT    TO MEC03C-LIEU-ENT   (MEC03C-NBP-MAX + 1)        
           MOVE GV11-NCODIC    TO MEC03C-CODIC-ENT  (MEC03C-NBP-MAX + 1)        
           SET MEC03C-WDACEM-NON (MEC03C-NBP-MAX)        TO TRUE.               
           SET  W-CMODEL-EMD-NON  TO TRUE                                       
           PERFORM ALIMENTE-MEC03C.                                             
      ******************************************************************        
      * Cr�ation d'une ligne exp�di�e:                                          
      ******************************************************************        
       CREATION-LIGNE-EXPEDIEE SECTION.                                         
MGD15 *    INITIALIZE RVGV1105.                                                 
MGD15      INITIALIZE RVGV1106.                                                 
           add 1 to w-nseqens                                                   
           move w-nseqens           to gv11-nseqens.                            
           MOVE GV10-NSOCIETE            TO GV11-NSOCIETE.                      
           MOVE GV10-NLIEU               TO GV11-NLIEU.                         
           MOVE GV10-NVENTE              TO GV11-NVENTE.                        
           MOVE GV10-NSOCIETE            TO W-CC-V-C-SOC(W-CC-V-I).             
           MOVE GV10-NLIEU               TO W-CC-V-C-LIEU(W-CC-V-I).            
           MOVE GV10-NVENTE              TO W-CC-V-NUM-CMD-SI(W-CC-V-I).        
           MOVE '1'                      TO GV11-CTYPENREG.                     
           MOVE W-CC-V-NCODICGRP(W-CC-V-I) TO GV11-NCODICGRP.                   
           MOVE W-CC-V-CODIC(W-CC-V-I)   TO GV11-NCODIC.                        
           ADD 1                         TO W-GV11-NLIGNE-PIC99.                
           MOVE W-GV11-NLIGNE-PIC99      TO W-CC-V-L-CMD-SI(W-CC-V-I).          
           MOVE W-GV11-NLIGNE-PICXX      TO GV11-NSEQ.                          
AL1908     MOVE W-CC-V-MODE-DELIV(W-CC-V-I)                                     
     |                                   TO GV11-CMODDEL                        
      *    ddeliv promise au client au lieu de date du jour                     
           MOVE W-CC-V-DT-LIV(W-CC-V-I)  TO GV11-DDELIV.                        
           MOVE GV10-NORDRE              TO GV11-NORDRE.                        
      *    GV11-CENREG reste � blanc (pour les lignes de produit)               
           MOVE W-CC-V-QTY(W-CC-V-I)     TO GV11-QVENDUE.                       
           PERFORM SELECT-PRIX-GG20-OU-GA69.                                    
LD1209     IF W-CC-V-NCODICGRP(W-CC-V-I) NOT = SPACE                            
              MOVE W-CC-V-PX-VT(W-CC-V-I) TO GV11-PVUNITF                       
LD1209     ELSE                                                                 
              MOVE GG20-PEXPTTC           TO GV11-PVUNITF                       
LD1209     END-IF.                                                              
LD1209*    MOVE GG20-PEXPTTC              TO GV11-PVUNITF.                      
           MOVE W-CC-V-PX-VT(W-CC-V-I)    TO GV11-PVUNIT.                       
           COMPUTE GV11-PVTOTAL = GV11-QVENDUE * GV11-PVUNIT.                   
           ADD GV11-PVTOTAL               TO GV10-PTTVENTE.                     
           MOVE GQ04-CEQUIP1              TO GV11-CEQUIPE.                      
 0116      IF   GV11-CEQUIPE < SPACES                                           
 0116           MOVE SPACES            TO GV11-CEQUIPE                          
 0116      END-IF                                                               
           MOVE W-GV11-NLIGNE-PICXX       TO GV11-NLIGNE.                       
           MOVE GV11-NCODIC               TO GA00-NCODIC.                       
           PERFORM SELECT-RTGA00.                                               
           IF W-CC-V-DT-LIV(W-CC-V-I) >= TXTVA-DATE                             
              MOVE TXTVA-TVA             TO GV11-TAUXTVA                        
           ELSE                                                                 
              MOVE TXTVA-ANCTVA          TO GV11-TAUXTVA                        
           END-IF.                                                              
           MOVE GA00-QCONDT              TO GV11-QCONDT.                        
      *    GV11-PRMP reste � z�ro                                               
           IF W-CC-VENTE-CLICK-COLLECT                                          
c&c           MOVE 'I'                   TO GV11-WEMPORTE                       
           ELSE                                                                 
L20410*       MOVE 'X' TO GV11-WEMPORTE                                         
L20410        MOVE W-CC-V-WEMPORTE(W-CC-V-I)  TO GV11-WEMPORTE                  
V41R1 * sauvegarde du wemporte pour la cr�ation de la prestation                
V41R1 * de frais d'envoi                                                        
    !         MOVE GV11-WEMPORTE TO WS-WEMPORTE                                 
           END-IF                                                               
v5.1.p     move gv11-wemporte to w-frais-wemporte                               
           move gv11-cmoddel  to w-frais-cmoddel                                
           move gv11-ddeliv   to w-frais-ddeliv                                 
           MOVE W-CC-E-C-POST-LIV        TO GV11-CPOSTAL.                       
           IF W-CC-VENTE-CLICK-COLLECT                                          
c&c           MOVE 'COLLECT'             TO GV11-LCOMMENT                       
              MOVE W-CC-V-DELIVERY(W-CC-V-I) TO GV11-LCOMMENT(8:)               
M25592        MOVE W-CC-V-DELIVERY(W-CC-V-I) TO WS-PREPARATION                  
           END-IF                                                               
      *    GV11-LCOMMENT reste � blanc                                          
v43r0 *    MOVE '009440'                 TO GV11-CVENDEUR                       
V43R0      IF W-CC-E-C-VEND NOT = SPACE                                         
V43R0         MOVE W-CC-E-C-VEND          TO GV11-CVENDEUR                      
V43R0      ELSE                                                                 
V43R0         MOVE '009440'               TO GV11-CVENDEUR                      
V43R0      END-IF                                                               
           MOVE GV10-NSOCIETE            TO GV11-NSOCLIVR.                      
           MOVE GV10-NLIEU               TO GV11-NDEPOT.                        
      *    GV11-NAUTORM reste � blanc                                           
      *    GV11-WARTINEX r�b, 'N' si article inexistant pour rachat PSE         
      *    GV11-WEDITBL reste � blanc                                           
      *    GV11-WCQERESF dispo=' ' cde four='F' cmq='Q' si pas r�s='Z'          
           MOVE '0'                      TO W-CC-V-C-RET(W-CC-V-I).             
      *    MOVE GB05-NMUTATION TO GV11-NMUTATION.                               
      *    GV11-CTOURNEE reste � blanc                                          
           MOVE 'N'                      TO GV11-WTOPELIVRE.                    
      *    GV11-DCOMPTA reste � blanc                                           
           MOVE GV10-DVENTE              TO GV11-DCREATION.                     
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE GV10-DHVENTE             TO GV11-HCREATION.                     
           MOVE GV10-DMVENTE             TO GV11-HCREATION(3:).                 
      *    GV11-DANNULATION reste � blanc                                       
           MOVE GV10-NLIEU               TO GV11-NLIEUORIG.                     
           MOVE 'N'                      TO GV11-WINTMAJ.                       
           MOVE W-SPDATDB2-DSYST         TO GV11-DSYST.                         
      *    GV11-DSTAT reste � blanc                                             
      *    GV11-CPRESTGRP reste � blanc                                         
           MOVE GV10-NSOCIETE            TO GV11-NSOCMODIF.                     
           MOVE GV10-NLIEU               TO GV11-NLIEUMODIF.                    
      *    GV11-DATENC reste � blanc                                            
           MOVE W-CC-CDEVISE             TO GV11-CDEV.                          
      *    GV11-WUNITR reste � blanc                                            
      *    GV11-LRCMMT reste � blanc                                            
           MOVE W-CC-NSOC-ECOM           TO GV11-NSOCP.                         
           MOVE W-CC-NLIEU-ECOM          TO GV11-NLIEUP.                        
      *    GV11-NTRANS reste � blanc                                            
      *    GV11-NSEQFV reste � blanc                                            
           MOVE W-GV11-NLIGNE-PIC99      TO GV11-NSEQNQ.                        
           IF GV11-NSEQNQ > GV10-NSEQNQ                                         
              MOVE GV11-NSEQNQ           TO GV10-NSEQNQ                         
           END-IF.                                                              
           MOVE W-GV11-NLIGNE-PIC99      TO GV11-NSEQREF.                       
      *    GV11-NMODIF reste � z�ro                                             
           MOVE GV10-NSOCIETE            TO GV11-NSOCORIG.                      
      *    GV11-DTOPE reste � blanc                                             
      *    MOVE GS10-NSOCDEPOT           TO GV11-NSOCLIVR1.                     
      *    MOVE GS10-NDEPOT              TO GV11-NDEPOT1.                       
emrx  *    MOVE GV10-NSOCIETE            TO GV11-NSOCGEST.                      
emrx  *    MOVE GV10-NLIEU               TO GV11-NLIEUGEST.                     
emrx  *    MOVE GV10-NSOCIETE            TO GV11-NSOCDEPLIV.                    
emrx  *    MOVE GV10-NLIEU               TO GV11-NLIEUDEPLIV.                   
c&c        IF W-CC-VENTE-CLICK-COLLECT                                          
c&c           MOVE GV10-NSOCIETE         TO GV11-NSOCDEPLIV                     
c&c           MOVE GV10-NLIEU            TO GV11-NLIEUDEPLIV                    
c&c        END-IF.                                                              
           IF W-CC-V-CARSPEC(W-CC-V-I) > ' '                                    
              PERFORM INSERT-RTGV15                                             
              PERFORM SELECT-CARSP                                              
              IF (CARSP-WCARACTS = 'C')                                         
                OR (    (CARSP-WCARACTS = ' ')                                  
                    AND (CARSP-CVDCARAC NOT = GV15-CVCARACTSPE1))               
                 MOVE 'C'                TO GV11-WACOMMUTER                     
              END-IF                                                            
           END-IF.                                                              
           MOVE  GV11-NSEQNQ             TO W-GV11-NSEQNQ.                      
LD0810     MOVE SPACE TO GV11-WCQERESF                                          
           PERFORM INSERT-RTGV11.                                               
V40R0      IF  W-CC-V-IS-PRECO(W-CC-V-I) > '0'                                  
               PERFORM INSERT-RTGV99                                            
           END-IF.                                                              
emrx  *    PERFORM INSERT-RTGV22.                                               
           SET  W-CMODEL-EMD-NON  TO TRUE                                       
           IF NOT W-CC-VENTE-CLICK-COLLECT                                      
EMRX  *       PERFORM LINK-MGV65                                                
LA2203        PERFORM ALIM-TAB-MEC64                                            
      *       Presta Frais d'Envoi                                              
              IF (W-CC-E-MT-SHIP NOT = 0)                                       
              AND (NOT PRESTA-FRENV-CREEE)                                      
                 SET PRESTA-FRENV-A-CREER TO TRUE                               
              END-IF                                                            
      * > RECUPERATION POUR ENVOI STOCK                                         
LA2203*          IF (COMM-GV65-DACEM-OUI                                        
LA2203*              AND COMM-GV65-CODRET = '6506')                             
LA2203*           OR COMM-GV65-DACEM-NON                                        
LA2203*          MOVE COMM-GV65-NSOCDEPOT                                       
LA2203*            TO MEC03C-SOCIETE-ENT      (MEC03C-NBP-MAX + 1)              
LA2203*          MOVE COMM-GV65-NDEPOT                                          
LA2203*            TO MEC03C-LIEU-ENT         (MEC03C-NBP-MAX + 1)              
LA2203*          MOVE GV11-NCODIC                                               
LA2203*            TO MEC03C-CODIC-ENT        (MEC03C-NBP-MAX + 1)              
LA2203*          MOVE COMM-GV25-WDACEM                                          
LA2203*            TO MEC03C-WDACEM           (MEC03C-NBP-MAX + 1)              
LA2203*          MOVE COMM-GV65-ETAT-RESERVATION                                
LA2203*            TO MEC03C-ETAT-RESERVATION (MEC03C-NBP-MAX + 1)              
LA2203*          PERFORM ALIMENTE-MEC03C                                        
LA2203*          END-IF                                                         
           ELSE                                                                 
      * -> SUR LES VENTES CC ON NE PEUT D�TERMINER LE LIEU DE STOCK             
      *    DANS LE CAS DES EMDI � VOIR DANS LE MBS70.                           
M16733           IF GV11-CMODDEL = 'EMR'                                        
                     MOVE GV11-NSOCIETE                                         
                     TO MEC03C-SOCIETE-ENT      (MEC03C-NBP-MAX + 1)            
                     MOVE GV11-NLIEU                                            
                     TO MEC03C-LIEU-ENT         (MEC03C-NBP-MAX + 1)            
                     MOVE GV11-NCODIC                                           
                     TO MEC03C-CODIC-ENT        (MEC03C-NBP-MAX + 1)            
                     SET MEC03C-WDACEM-NON (MEC03C-NBP-MAX + 1) TO TRUE         
                     PERFORM ALIMENTE-MEC03C                                    
                 ELSE                                                           
                     SET  W-CMODEL-EMD-OUI  TO TRUE                             
                 END-IF                                                         
           END-IF.                                                              
           PERFORM INSERT-RTEC03.                                               
           PERFORM CREE-MOUCHARD-ENTETE-LIGNE.                                  
      ******************************************************************        
      * Recherche des infos:                                                    
      * - entrepot de stockage                                                  
      * - quota de livraison                                                    
      * - mut � affecter                                                        
      ******************************************************************        
       RECHERCHE-STOCK-QUOTA-MUT SECTION.                                       
      *----------------------------------                                       
LA1008*    SET GV11-WCQERESF-NORMAL TO TRUE.                                    
LA1008     INITIALIZE GV11-WCQERESF-FLAG                                        
           SET GV11-MUTATION-OK     TO TRUE.                                    
      * R�Z diff�rentes zones                                                   
           MOVE SPACES TO GS10-NSOCDEPOT GS10-NDEPOT GB05-NMUTATION.            
      * Mode de d�livrance ---> Type de traitement                              
           EXEC SQL SELECT WTABLEG INTO :MDLIV-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'MDLIV'                                   
      *                AND CTABLEG2 = 'LD2'                                     
v5.1.p                 AND CTABLEG2 = :gv11-cmoddel                             
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'SELECT MDLIV: LD2' TO W-MESSAGE                             
              MOVE LIVRG-CTABLEG2(1:10) TO W-MESSAGE(15:)                       
              PERFORM ERREUR-SQL                                                
           else                                                                 
              display 'mdliv : ' ga01-wtableg                                   
           END-IF.                                                              
      * Type de traitement ---> Priorit�                                        
           MOVE MDLIV-WDONNEES(2:5) TO FLTRT-CTABLEG2.                          
           EXEC SQL SELECT WTABLEG INTO :FLTRT-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'FLTRT'                                   
                       AND CTABLEG2 = :FLTRT-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'SELECT FLTRT:' TO W-MESSAGE                                 
              MOVE MDLIV-CTABLEG2  TO W-MESSAGE(15:)                            
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      * Soc/Mag/Type de traitement/Date ---> Profil                             
           MOVE EC007-NSOCPLT   TO FL05-NSOCIETE                                
           MOVE EC007-NLIEUPLT  TO FL05-NLIEU.                                  
           MOVE MDLIV-WDONNEES(2:5) TO FL05-CTYPTRAIT.                          
           MOVE GQ04-DEFFET     TO FL05-DEFFET.                                 
           EXEC SQL SELECT CPROAFF INTO :FL05-CPROAFF                           
                      FROM RVFL0500 MSTR                                        
                     WHERE NSOCIETE  = :FL05-NSOCIETE                           
                       AND NLIEU     = :FL05-NLIEU                              
                       AND CTYPTRAIT = :FL05-CTYPTRAIT                          
                       AND DEFFET    =                                          
                          (SELECT MAX(DEFFET) FROM RVFL0500                     
                            WHERE NSOCIETE  = MSTR.NSOCIETE                     
                              AND NLIEU     = MSTR.NLIEU                        
                              AND CTYPTRAIT = MSTR.CTYPTRAIT                    
                              AND DEFFET   <= :FL05-DEFFET)                     
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SEL FL05 NSOC: ' FL05-NSOCIETE                            
                                 ' NLIEU: ' FL05-NLIEU                          
                             ' CTYPTRAIT: ' FL05-CTYPTRAIT                      
                                ' DEFFET: ' FL05-DEFFET                         
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      * par priorite, entrepot du codic (c'est une exception)                   
           MOVE SPACES TO W-GS10-NSOCDEPOT W-GS10-NDEPOT.                       
           MOVE FL05-CPROAFF TO FL06-CPROAFF.                                   
           MOVE W-CC-V-CODIC(W-CC-V-I) TO FL06-NCODIC.                          
           SET YA-PAS-D-ENTREPOT TO TRUE.                                       
NV2209     SET WMAGIQ-KO         TO TRUE.                                       
NV2209     SET WS-RESER-FOURN-KO TO TRUE.                                       
LA1008     SET WS-RES-CQE-TRT-KO TO TRUE.                                       
xdock      set WS-RES-xdk-TRT-KO to true.                                       
           PERFORM VARYING I-P FROM 1 BY 1                                      
             UNTIL (I-P > 5) OR (IL-Y-A-UN-ENTREPOT)                            
NV2008*NV2209 OR WMAGIQ-OK                                                      
NV2209        OR WMAGIQ-OK  OR WS-RESER-FOURN-OK                                
              MOVE FLTRT-NPRIORIT(I-P:1) TO FL06-NPRIORITE                      
              IF FL06-NPRIORITE > '0'                                           
                 EXEC SQL SELECT NSOCDEPOT, NDEPOT                              
                            INTO :FL06-NSOCDEPOT, :FL06-NDEPOT                  
                            FROM RVFL0600                                       
                           WHERE CPROAFF   = :FL06-CPROAFF                      
                             AND NCODIC    = :FL06-NCODIC                       
                             AND NPRIORITE = :FL06-NPRIORITE                    
                 END-EXEC                                                       
                 IF SQLCODE NOT = 0 AND 100                                     
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING 'SEL FL06 CPROAFF: ' FL06-CPROAFF                    
                                         ' NCODIC: ' FL06-NCODIC                
                                      ' NPRIORITE: ' FL06-NPRIORITE             
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ERREUR-SQL                                          
                 END-IF                                                         
                 IF SQLCODE = 0                                                 
                    MOVE FL06-NSOCDEPOT TO GS10-NSOCDEPOT                       
                    MOVE FL06-NDEPOT    TO GS10-NDEPOT                          
                    IF W-GS10-NSOCDEPOT <= ' '                                  
                       MOVE GS10-NSOCDEPOT TO W-GS10-NSOCDEPOT                  
                       MOVE GS10-NDEPOT    TO W-GS10-NDEPOT                     
                    END-IF                                                      
                    MOVE W-CC-V-CODIC(W-CC-V-I) TO GS10-NCODIC                  
                    PERFORM CHERCHE-MUTATION-MAGIQUE                            
                    IF WMAGIQ-KO                                                
                       PERFORM SELECT-RTGS10                                    
                       IF GS10-QSTOCK >= W-CC-V-QTY(W-CC-V-I)                   
                          SET IL-Y-A-UN-ENTREPOT TO TRUE                        
                       ELSE                                                     
xdock * -> controle commande cross-dock                                         
                          IF GA00-CROSSDOCKFOUR = 'O'                           
                             perform select-rtfl90                              
                             if FL90-CROSSDOCKFOUR = 'O'                        
                                perform CTRL-MUTATION-CROSSDOCK                 
                                if cherche-xdock                                
                                   perform controle-cross-dock                  
                                end-if                                          
                             end-if                                             
                          end-if                                                
xdock                     if  WS-RES-xdk-TRT-KO                                 
                              PERFORM RESERVER-CDE-FOURNISSEUR                  
                          end-if                                                
                          IF FL50-CAPPRO = 'C'                                  
                          AND (WS-RESER-FOURN-KO)                               
                          AND (WS-RES-CQE-TRT-KO)                               
xdock                     and  WS-RES-xdk-TRT-KO                                
                             PERFORM RESERVER-CONTREMARQUE                      
                          END-IF                                                
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *    Si on n'a pas trouve d'entrep�t par article, on voit celui           
      *    de la famille.                                                       
           IF YA-PAS-D-ENTREPOT                                                 
           and WS-RES-xdk-TRT-KO                                                
           and WS-RES-CQE-TRT-KO                                                
           and WS-RESER-FOURN-KO                                                
              MOVE FL05-CPROAFF TO GQ05-CPROAFF                                 
              MOVE GA00-CFAM    TO GQ05-CFAM                                    
LA1008        SET WS-RES-CQE-TRT-KO TO TRUE                                     
              PERFORM VARYING I-P FROM 1 BY 1                                   
                UNTIL (I-P > 5) OR (IL-Y-A-UN-ENTREPOT)                         
NV2209          OR WMAGIQ-OK  OR WS-RESER-FOURN-OK                              
                 MOVE FLTRT-NPRIORIT(I-P:1) TO GQ05-NPRIORITE                   
                 IF GQ05-NPRIORITE > '0'                                        
                    EXEC SQL SELECT NSOC, NDEPOT                                
                               INTO :GQ05-NSOC, :GQ05-NDEPOT                    
                               FROM RVGQ0500                                    
                              WHERE CPROAFF   = :GQ05-CPROAFF                   
                                AND CFAM      = :GQ05-CFAM                      
                                AND NPRIORITE = :GQ05-NPRIORITE                 
                    END-EXEC                                                    
                    IF SQLCODE NOT = 0 AND 100                                  
                       MOVE SPACES TO W-MESSAGE                                 
                       STRING 'SEL GQ05 CPROAFF: ' GQ05-CPROAFF                 
                                              ' CFAM: ' GQ05-CFAM               
                                         ' NPRIORITE: ' GQ05-NPRIORITE          
                       DELIMITED BY SIZE INTO W-MESSAGE                         
                       PERFORM ERREUR-SQL                                       
                    END-IF                                                      
                    IF SQLCODE = 0                                              
                       MOVE GQ05-NSOC      TO GS10-NSOCDEPOT                    
                       MOVE GQ05-NDEPOT    TO GS10-NDEPOT                       
                       IF W-GS10-NSOCDEPOT <= ' '                               
                          MOVE GS10-NSOCDEPOT TO W-GS10-NSOCDEPOT               
                          MOVE GS10-NDEPOT    TO W-GS10-NDEPOT                  
                       END-IF                                                   
                       MOVE W-CC-V-CODIC(W-CC-V-I) TO GS10-NCODIC               
NV2008                 PERFORM CHERCHE-MUTATION-MAGIQUE                         
NV2008                 IF WMAGIQ-KO                                             
                          PERFORM SELECT-RTGS10                                 
                          IF GS10-QSTOCK >= W-CC-V-QTY(W-CC-V-I)                
                             SET IL-Y-A-UN-ENTREPOT TO TRUE                     
NV2209                    ELSE                                                  
xdock * -> controle commande cross-dock                                         
                            IF GA00-CROSSDOCKFOUR = 'O'                         
                               perform select-rtfl90                            
                               if FL90-CROSSDOCKFOUR = 'O'                      
                                  perform CTRL-MUTATION-CROSSDOCK               
                                  if cherche-xdock                              
                                     perform controle-cross-dock                
                                  end-if                                        
                               end-if                                           
                            end-if                                              
xdock                       if  WS-RES-xdk-TRT-KO                               
                                PERFORM RESERVER-CDE-FOURNISSEUR                
                            end-if                                              
                             IF FL50-CAPPRO = 'C'                               
                             AND (WS-RESER-FOURN-KO)                            
                             AND (WS-RES-CQE-TRT-KO)                            
                             and WS-RES-xdk-TRT-KO                              
                                PERFORM RESERVER-CONTREMARQUE                   
                             END-IF                                             
                          END-IF                                                
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
           IF (IL-Y-A-UN-ENTREPOT)                                              
              OR WS-RESER-FOURN-OK                                              
xdock         or WS-RES-xdk-TRT-OK                                              
              OR WMAGIQ-OK                                                      
              IF  (LIVRG-LIEUGEST(1:3) NOT = GS10-NSOCDEPOT)                    
                OR (LIVRG-LIEUGEST(4:3) NOT = GS10-NDEPOT)                      
xdock            if WS-RES-xdk-TRT-OK                                           
                    PERFORM UPDATE-RTGB05                                       
                 else                                                           
                    PERFORM SELECT-RTGB05                                       
                    IF mutation-non-trouvee                                     
                       IF FAIS-DES-DISPLAYS                                     
                          MOVE SPACES TO W-MESSAGE                              
                          STRING                                                
                                'AUCUNE MUTATION POUR CODIC: '                  
                                 W-CC-V-CODIC(W-CC-V-I)                         
                          DELIMITED BY SIZE INTO W-MESSAGE                      
                          PERFORM DISPLAY-W-MESSAGE                             
                          MOVE SPACES TO W-MESSAGE                              
                          STRING GS10-NSOCDEPOT                                 
                            '!'  GS10-NDEPOT                                    
                            '!'  GB05-NSOCIETE                                  
                            '!'  GB05-NLIEU                                     
                          DELIMITED BY SIZE INTO W-MESSAGE                      
                          PERFORM DISPLAY-W-MESSAGE                             
                       END-IF                                                   
                       SET GV11-WCQERESF-EN-Z  TO TRUE                          
                       MOVE '0023'             TO MEC15C-NSEQERR                
                       MOVE 'E'                TO MEC15C-STAT                   
                       SET  MEC15E-KO          TO TRUE                          
                       SET  GV11-MUTATION-KO   TO TRUE                          
                       PERFORM CREE-MOUCHARD-LIGNE                              
                    ELSE                                                        
      ******************************************************************        
      * Attention: �tude � faire, simplement virer la mut n'est pas             
      *            suffisant.                                                   
      ******************************************************************        
                       PERFORM UPDATE-RTGB05                                    
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
              IF GV11-WCQERESF-NORMAL                                           
                 IF WMAGIQ-OK                                                   
                    IF GB15-QDEMANDEE  = WQTE                                   
                       PERFORM DELETE-RTGB15                                    
                       MOVE -1   TO WQNBLIGNES                                  
                    ELSE                                                        
                       PERFORM UPDATE-RTGB15                                    
                       MOVE  0   TO WQNBLIGNES                                  
                    END-IF                                                      
                    IF SQLCODE = 0                                              
                       PERFORM UPDATE-RTGB05-NBLIG                              
                    END-IF                                                      
                    MOVE GV11-NSEQNQ        TO MEC15C-NSEQNQ                    
                    MOVE '0009'             TO MEC15C-NSEQERR                   
                    MOVE 'I'                TO MEC15C-STAT                      
                    MOVE  GB05-NMUTATION   TO MEC15C-ALP(1)                     
                    PERFORM CREE-MOUCHARD-LIGNE                                 
                 ELSE                                                           
                    IF WS-RESER-FOURN-OK                                        
                       PERFORM UPDATE-RTGF55                                    
                       MOVE GV11-NSEQNQ        TO MEC15C-NSEQNQ                 
                       MOVE '0022'             TO MEC15C-NSEQERR                
                       MOVE 'I'                TO MEC15C-STAT                   
                       MOVE  GB05-NMUTATION   TO MEC15C-ALP(1)                  
                       PERFORM CREE-MOUCHARD-LIGNE                              
                       SET IL-Y-A-UN-ENTREPOT TO TRUE                           
                    ELSE                                                        
                       IF GV11-WCQERESF-EN-Q or GV11-WCQERESF-XDOCK             
                          SET  MEC15E-KO          TO TRUE                       
                          if GV11-WCQERESF-XDOCK                                
                             move '0028' to mec15c-nseqerr                      
                             move 'I'    to mec15c-stat                         
                          else                                                  
                             MOVE '0027' TO MEC15C-NSEQERR                      
                             MOVE 'I'    TO MEC15C-STAT                         
                          end-if                                                
                          PERFORM CREE-MOUCHARD-LIGNE                           
                       ELSE                                                     
                          PERFORM UPDATE-RTGS10                                 
                          MOVE GV11-NSEQNQ        TO MEC15C-NSEQNQ              
                          MOVE '0010'             TO MEC15C-NSEQERR             
                          MOVE 'I'                TO MEC15C-STAT                
                          MOVE  GB05-NMUTATION   TO MEC15C-ALP(1)               
                          PERFORM CREE-MOUCHARD-LIGNE                           
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              SET GV11-WCQERESF-EN-Z TO TRUE                                    
              MOVE W-GS10-NSOCDEPOT TO GS10-NSOCDEPOT                           
              MOVE W-GS10-NDEPOT    TO GS10-NDEPOT                              
              IF FAIS-DES-DISPLAYS                                              
                 MOVE SPACES TO W-MESSAGE                                       
                 STRING                                                         
                        'AUCUN ENTREP�T AVEC STOCK POUR CODIC: '                
                        W-CC-V-CODIC(W-CC-V-I)                                  
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM DISPLAY-W-MESSAGE                                      
              END-IF                                                            
           END-IF.                                                              
NV2008 CHERCHE-MUTATION-MAGIQUE SECTION.                                        
      *---------------------------------                                        
  !        MOVE GS10-NSOCDEPOT             TO GB05-NSOCENTR                     
  !        MOVE GS10-NDEPOT                TO GB05-NDEPOT                       
  !        MOVE GV11-QVENDUE               TO WQTE                              
  !        MOVE MDLIV-WDONNEES  (25:3)     TO GB05-NSOCIETE                     
  !        MOVE MDLIV-WDONNEES  (28:3)     TO GB05-NLIEU                        
V41R0 *    MOVE  'ECOM%ECOM%'              TO GB05-LHEURLIMIT                   
  !                                                                             
V41R0 *    PERFORM CHERCHE-MUTATION-MAGIQUES                                    
V41R0      MOVE  '0' TO WF-TSTENVOI                                             
  !        PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
  !        UNTIL  WC-TSTENVOI-FIN                                               
  !           PERFORM LECTURE-TSTENVOI                                          
  !           IF  WC-TSTENVOI-SUITE                                             
  !           AND TSTENVOI-NSOCZP    = '00000'                                  
  !           AND TSTENVOI-CTRL(1:2) = 'BA'                                     
  !           AND TSTENVOI-WPARAM(1:3) = GB05-NSOCENTR                          
  !           AND TSTENVOI-WPARAM(4:3) = GB05-NDEPOT                            
  !              MOVE TSTENVOI-LIBELLE(1:7)  TO GB15-NMUTATION                  
  !              PERFORM SELECT-RTGB15-TEMPORAIRE                               
  !              IF SQLCODE = 0 AND GB15-QDEMANDEE >= WQTE                      
LA2203*             MOVE GB05-NSOCENTR TO COMM-GV65-NSOCDEPOT                   
LA2203*             MOVE GB05-NDEPOT   TO COMM-GV65-NDEPOT                      
  !                 SET WMAGIQ-OK            TO TRUE                            
NV2008              MOVE SPACES TO W-MESSAGE                                    
  !                 STRING                                                      
                       'Magic Mutation Ok   ' GB15-NMUTATION                    
  !                    DELIMITED BY SIZE INTO W-MESSAGE                         
  !                    PERFORM DISPLAY-W-MESSAGE                                
  !              END-IF                                                         
  !           END-IF                                                            
V41R0      END-PERFORM.                                                         
NV2008 FIN-SCH-MUT-MAGIQUE. EXIT.                                               
V41R0 *CHERCHE-MUTATION-MAGIQUES SECTION.                                       
  !   *----------------------------------                                       
  !   *                                                                         
  !   *    PERFORM SELECT-RTGB05-TEMPORAIRE.                                    
  !   *                                                                         
  !   *    IF SQLCODE = 0                                                       
  !   *       MOVE GB05-NMUTATION TO GB15-NMUTATION                             
  !   *       PERFORM SELECT-RTGB15-TEMPORAIRE                                  
  !   *                                                                         
  !   *       IF SQLCODE = 0  AND GB15-QDEMANDEE >= WQTE                        
  !   *          MOVE GB05-NSOCENTR       TO COMM-GV65-NSOCDEPOT                
  !   *          MOVE GB05-NDEPOT         TO COMM-GV65-NDEPOT                   
  !   *          SET WMAGIQ-OK            TO TRUE                               
NV2008*          MOVE SPACES TO W-MESSAGE                                       
  !   *          STRING 'MAGIC MUTATION OK   ' GB15-NMUTATION                   
  !   *          DELIMITED BY SIZE INTO W-MESSAGE                               
  !   *          PERFORM DISPLAY-W-MESSAGE                                      
  !   *       END-IF                                                            
  !   *                                                                         
  !   *    END-IF.                                                              
  !   *                                                                         
V41R0 *FIN-SCH-MUT-MAGIQUES. EXIT.                                              
NV2008 SELECT-RTGB05-TEMPORAIRE       SECTION.                                  
  !   *---------------------------------------                                  
  ! *                                                                           
  !        EXEC SQL SELECT   NSOCENTR     , NDEPOT      , DMUTATION   ,         
  !                          QNBM3QUOTA   , QNBPQUOTA   , QVOLUME     ,         
  !                          QNBPIECES,     QNBLIGNES   , QNBLLANCE   ,         
  !                          WVAL         , NMUTATION                           
  !                 INTO    :GB05-NSOCENTR       , :GB05-NDEPOT       ,         
  !                         :GB05-DMUTATION      , :GB05-QNBM3QUOTA   ,         
  !                         :GB05-QNBPQUOTA      , :GB05-QVOLUME      ,         
  !                         :GB05-QNBPIECES      , :GB05-QNBLIGNES    ,         
  !                         :GB05-QNBLLANCE      , :GB05-WVAL         ,         
  !                         :GB05-NMUTATION                                     
  !                 FROM     RVGB0501                                           
  !                 WHERE NSOCENTR   = :GB05-NSOCENTR                           
  !                   AND NDEPOT     = :GB05-NDEPOT                             
  !                   AND LHEURLIMIT = :GB05-LHEURLIMIT                         
  !                   AND NSOCIETE   = :GB05-NSOCIETE                           
  !                   AND NLIEU      = :GB05-NLIEU                              
  !        END-EXEC.                                                            
  ! *                                                                           
  !        IF SQLCODE NOT = 0 AND 100                                           
  !           MOVE SPACES TO W-MESSAGE                                          
  !           STRING 'SELECT RTGB05 NMUTATION  ' GB05-NMUTATION                 
  !                           'POUR  NSOCENTR  ' GB05-NSOCENTR                  
  !                                 'NDEPOT    ' GB05-NDEPOT                    
  !                                 'LHEURLIMIT' GB05-LHEURLIMIT                
  !                                 'NSOCIETE  ' GB05-NSOCIETE                  
  !                                 'NLIEU     ' GB05-NLIEU                     
  !               DELIMITED BY SIZE INTO W-MESSAGE                              
  !           PERFORM ERREUR-SQL                                                
  !        END-IF .                                                             
  !                                                                             
NV2008 FIN-SELECT-RTGB05-TEMPORAIRE. EXIT.                                      
NV2008 SELECT-RTGB15-TEMPORAIRE SECTION.                                        
      *---------------------------------                                        
  !                                                                             
  !        MOVE GV11-NCODIC        TO GB15-NCODIC                               
  !                                                                             
  !        EXEC SQL SELECT     QDEMANDEE                                        
  !                          , QVOLUME                                          
  !                          , QCC                                              
  !                          , QDEMINIT                                         
  !                          , NSOCDEPOT                                        
  !                          , NDEPOT                                           
  !                 INTO       :GB15-QDEMANDEE                                  
  !                          , :GB15-QVOLUME                                    
  !                          , :GB15-QCC                                        
  !                          , :GB15-QDEMINIT                                   
  !                          , :GB15-NSOCDEPOT                                  
  !                          , :GB15-NDEPOT                                     
  !                 FROM     RVGB1502                                           
  !                 WHERE    NCODIC     = :GB15-NCODIC                          
  !                   AND    NMUTATION  = :GB15-NMUTATION                       
  !        END-EXEC.                                                            
  !                                                                             
  !   * -> TOUT CE QU'ON VEUT SAVOIR C'EST SI ON A DES CODES RETOURS            
  !   *    TYPE -805 (BLOQUANTS) ET LE NON-TROUVE (NON BLOQUANT)                
  !        IF SQLCODE NOT = 0 AND 100                                           
  !           MOVE SPACES TO W-MESSAGE                                          
  !           STRING 'SELECT RTGB15 TEMPORAIRE '                                
  !                               ' NSOCDEPOT  ' GB15-NSOCDEPOT                 
  !                               ' NDEPOT     ' GB15-NDEPOT                    
  !                          'POUR  NCODIC     ' GB15-NCODIC                    
  !                                'NMUTATION  ' GB15-NMUTATION                 
  !               DELIMITED BY SIZE INTO W-MESSAGE                              
  !           PERFORM ERREUR-SQL                                                
  !        END-IF .                                                             
  !                                                                             
NV2008 FIN-SELECT-GB15-TEMPORAIRE. EXIT.                                        
stkpf  CHERCHE-STOCK-PLATEFORME SECTION.                                        
           set COMM-MGV48-RESERVATION to true                                   
           MOVE SPACES TO W-MESSAGE                                             
           STRING 'Recherche stock plateforme sur : '                           
                  LIVRG-LDEPLIV(1:3)                                            
                  LIVRG-LDEPLIV(4:3)                                            
                  'pour CODIC: '                                                
                  W-CC-V-CODIC(W-CC-V-I)                                        
           DELIMITED BY SIZE INTO W-MESSAGE                                     
           PERFORM DISPLAY-W-MESSAGE                                            
           MOVE LIVRG-LDEPLIV(1:3) TO COMM-MGV48-NSOCDEPOT  (1)                 
           MOVE LIVRG-LDEPLIV(4:3) TO COMM-MGV48-NLIEUDEPOT (1)                 
           MOVE gv11-ncodic        to COMM-MGV48-NCODIC     (1)                 
           MOVE gv11-qvendue       TO COMM-MGV48-QSTOCK     (1)                 
           MOVE gv11-nligne        to COMM-MGV48-NLIGNE     (1)                 
           MOVE gv11-NSEQNQ        to COMM-MGV48-NSEQNQ     (1)                 
           MOVE SPACE              TO COMM-MGV48-CRET       (1)                 
           MOVE gv11-nordre         TO COMM-MGV48-NUMRES                        
           MOVE GV11-NSOCIETE       TO COMM-MGV48-NSOCIETE                      
           MOVE GV11-NLIEU          TO COMM-MGV48-NLIEU                         
           MOVE GV11-NVENTE         TO COMM-MGV48-NVENTE                        
           MOVE GV10-NORDRE         TO COMM-MGV48-NORDRE                        
           MOVE GV10-DVENTE         TO COMM-MGV48-DVENTE                        
           MOVE W-CC-DATE-DU-JOUR   TO COMM-MGV48-SSAAMMJJ                      
           MOVE GV11-NSOCIETE TO SUFIX-CTABLEG2.                                
           EXEC SQL SELECT WTABLEG INTO :SUFIX-WTABLEG                          
                      FROM RVSUFIX                                              
                     WHERE CTABLEG2 = :SUFIX-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SLT SUFIX NSOCIETE: ' GV11-NSOCIETE                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           MOVE 'MGV48'             TO MGV48                                    
           move SUFIX-SUFIX         to MGV48(6:)                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         LINK PROGRAM (MGV48) COMMAREA (COMM-MGV48-APPLI)                
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS LINK PROGRAM (MGV48) COMMAREA (COMM-MGV48-APPLI)           
           END-EXEC.                                                            
      *}                                                                        
           move eibrcode to eib-rcode                                           
           IF COMM-MGV48-ERR-BLQ OR COMM-MGV48-ERR-STO                          
           or eibr not = low-value                                              
              SET GV11-WCQERESF-EN-Z TO TRUE                                    
              IF FAIS-DES-DISPLAYS                                              
                 MOVE SPACES TO W-MESSAGE                                       
                 STRING                                                         
                        'Aucun stock plateforme sur : '                         
                        GV11-NSOCLIVR                                           
                        GV11-NDEPOT                                             
                        'pour CODIC: '                                          
                        W-CC-V-CODIC(W-CC-V-I)                                  
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM DISPLAY-W-MESSAGE                                      
           end-if.                                                              
       F-CHERCHE-STOCK-PLATEFORME. EXIT.                                        
      ******************************************************************        
      * Cr�ation des services: On r�cup�re toutes les Services rattach�s        
      * � la ligne de vente article.                                            
      ******************************************************************        
       CREATION-LIGNE-SERVICE SECTION.                                          
      *-------------------------------                                          
           MOVE GV10-NSOCIETE       TO W-CC-V-C-SOC(W-CC-V-I).                  
           MOVE GV10-NLIEU          TO W-CC-V-C-LIEU(W-CC-V-I).                 
           MOVE GV10-NVENTE         TO W-CC-V-NUM-CMD-SI(W-CC-V-I).             
           MOVE 1                   TO GV11-QVENDUE.                            
           MOVE W-GV11-NSEQNQ       TO GV11-NSEQREF.                            
           ADD 1 TO W-GV11-NLIGNE-PIC99.                                        
           MOVE W-GV11-NLIGNE-PIC99 TO W-CC-V-L-CMD-SI(W-CC-V-I).               
           MOVE W-GV11-NLIGNE-PICXX TO GV11-NSEQ.                               
           MOVE W-GV11-NLIGNE-PICXX TO GV11-NLIGNE.                             
           MOVE W-GV11-NLIGNE-PIC99 TO GV11-NSEQNQ                              
           IF GV11-NSEQNQ > GV10-NSEQNQ                                         
              MOVE GV11-NSEQNQ             TO GV10-NSEQNQ                       
           END-IF.                                                              
           MOVE ' '                     TO GV11-LCOMMENT                        
M23862*    Si PSE, alors on recup�re le n� dans le LCOMMENT                     
  "        IF GV11-CTYPENREG = '3'                                              
  "           MOVE W-CC-V-NUM-PS(W-CC-V-I) TO GV11-LCOMMENT                     
M23862     END-IF                                                               
      *                                                                         
 0116      IF   GV11-CEQUIPE < SPACES                                           
 0116           MOVE SPACES            TO GV11-CEQUIPE                          
 0116      END-IF                                                               
           MOVE W-CC-V-CENREG(W-CC-V-I) TO GV11-CENREG.                         
           MOVE W-CC-V-PX-VT(W-CC-V-I)  TO GV11-PVUNIT GV11-PVUNITF             
                                           GV11-PVTOTAL.                        
           IF GV11-CTYPENREG = '2'                                              
              SUBTRACT GV11-PVTOTAL FROM  GV10-PTTVENTE                         
           ELSE                                                                 
              ADD GV11-PVTOTAL TO GV10-PTTVENTE                                 
           END-IF.                                                              
V5.1.P*    MOVE '3' TO GV11-CTYPENREG.                                          
           MOVE '0' TO W-CC-V-C-RET(W-CC-V-I).                                  
           PERFORM INSERT-RTGV11.                                               
           IF PRESTA-FRENV-A-CREER                                              
V34CEN        PERFORM CREATION-PRESTA-FRENV                                     
              SET PRESTA-FRENV-CREEE TO TRUE                                    
           END-IF                                                               
           .                                                                    
       CREATION-LIGNE-SERVICE-SEUL SECTION.                                     
      *-------------------------------                                          
           add 1 to w-nseqens                                                   
           move w-nseqens           to gv11-nseqens.                            
           MOVE GV10-NSOCIETE       TO W-CC-V-C-SOC(W-CC-V-I).                  
           MOVE GV10-NLIEU          TO W-CC-V-C-LIEU(W-CC-V-I).                 
           MOVE GV10-NVENTE         TO W-CC-V-NUM-CMD-SI(W-CC-V-I).             
           MOVE 1                   TO GV11-QVENDUE.                            
           ADD 1 TO W-GV11-NLIGNE-PIC99.                                        
           MOVE SPACES        TO GV11-CEQUIPE                                   
                                 GV11-CPROTOUR                                  
                                 GV11-WCQERESF                                  
                                 GV11-NCODIC                                    
                                 GV11-NCODICGRP                                 
                                 GV11-NMUTATION                                 
           MOVE 'N'           TO GV11-WINTMAJ                                   
           MOVE GV10-NSOCIETE TO GV11-NSOCLIVR                                  
                                 GV11-NSOCLIVR1                                 
                                 GV11-NSOCGEST                                  
                                 GV11-NSOCDEPLIV                                
           MOVE GV10-NLIEU    TO GV11-NDEPOT                                    
                                 GV11-NDEPOT1                                   
                                 GV11-NLIEUGEST                                 
                                 GV11-NLIEUDEPLIV                               
           MOVE W-GV11-NLIGNE-PIC99     TO W-CC-V-L-CMD-SI(W-CC-V-I).           
           MOVE W-GV11-NLIGNE-PICXX     TO GV11-NSEQ.                           
           MOVE W-GV11-NLIGNE-PICXX     TO GV11-NLIGNE.                         
           MOVE W-GV11-NLIGNE-PIC99     TO GV11-NSEQNQ                          
                                       GV11-NSEQREF.                            
           IF GV11-NSEQNQ > GV10-NSEQNQ                                         
              MOVE GV11-NSEQNQ          TO GV10-NSEQNQ                          
           END-IF.                                                              
           MOVE W-CC-V-DT-LIV(W-CC-V-I) TO GV11-DDELIV                          
           MOVE ' '                     TO GV11-LCOMMENT                        
           MOVE W-CC-V-CENREG(W-CC-V-I) TO GV11-CENREG.                         
           MOVE W-CC-V-PX-VT(W-CC-V-I)  TO GV11-PVUNIT GV11-PVUNITF             
                                           GV11-PVTOTAL.                        
           IF GV11-CENREG = W-SERVICE-LIVRAISON                                 
              MOVE W-CMODDEL-CADRE TO GV11-CMODDEL                              
              MOVE SPACE           TO GV11-WEMPORTE                             
           END-IF                                                               
           ADD GV11-PVTOTAL TO GV10-PTTVENTE.                                   
           MOVE '0' TO W-CC-V-C-RET(W-CC-V-I).                                  
      * Chercher si la presation est de type carte kdo                          
ccprm      set PRESTA-KDO-ko        TO TRUE                                     
           move 0 to w-cc-v-p1                                                  
           PERFORM UNTIL W-CC-V-P1  > W-CC-V-P1-MAX                             
                OR PRESTA-KDO-OK                                                
               add 1 to w-cc-v-p1                                               
               PERFORM VARYING W-CC-V-P2 FROM 1 BY 1                            
                   UNTIL W-CC-V-P2  > W-CC-V-P2-MAX                             
                      OR PRESTA-KDO-OK                                          
                   IF W-CC-V-VALEUR(W-CC-V-P1, W-CC-V-P2) = GV11-CENREG         
                      SET PRESTA-KDO-OK  TO TRUE                                
                   END-IF                                                       
              END-PERFORM                                                       
           END-PERFORM                                                          
           IF PRESTA-KDO-OK                                                     
              MOVE 'EMR'           TO GV11-CMODDEL                              
              MOVE SPACE             TO GV11-WEMPORTE                           
              move W-CC-DATE-DU-JOUR TO gv11-ddeliv                             
           END-IF                                                               
      *                                                                         
           PERFORM INSERT-RTGV11.                                               
      *                                                                         
      * En cas de pr�sence des donn�es CC, on d�clenche la cr�ation             
      * de dossier                                                              
               IF PRESTA-KDO-OK                                                 
                  MOVE GV11-CENREG TO WS-CENREG                                 
                 PERFORM APPEL-DOSSIERS-ADMIN                                   
               END-IF.                                                          
      *    END-PERFORM.                                                         
      *-----------------------------------------------------------------        
      * CREATION PRESTA FRAIS ENVOIS                                            
V34CEN* CORRECTION : PRESTA FRENV CREEE AVEC EXPEDIES                           
V41R0 * RECUPERATION DU CODE PRESTA (FRAIS D'ENVOI) DANS LA TS TSTENVOI         
       CREATION-PRESTA-FRENV     SECTION.                                       
           add 1 to w-nseqens                                                   
           move w-nseqens           to gv11-nseqens.                            
           MOVE W-FRAIS-WEMPORTE        TO GV11-WEMPORTE                        
           MOVE W-FRAIS-CMODDEL         TO GV11-CMODDEL                         
           MOVE W-FRAIS-DDELIV          TO GV11-DDELIV                          
      *    MOVE W-CC-DATE-DU-JOUR       TO GV11-DDELIV                          
 0116      IF   GV11-CEQUIPE < SPACES                                           
 0116           MOVE SPACES            TO GV11-CEQUIPE                          
 0116      END-IF                                                               
           MOVE '4'                     TO GV11-CTYPENREG.                      
           MOVE GV11-NCODIC             TO W-GV11-NCODIC.                       
           MOVE SPACES                  TO GV11-NCODIC GV11-NCODICGRP.          
           MOVE 1                       TO GV11-QVENDUE.                        
V34CEN*    MOVE W-GV11-NSEQNQ           tO GV11-NSEQREF.                        
      *=>  Presta Frais d'Envoi                                                 
V34CEN*    IF W-CC-E-MT-SHIP NOT = 0                                            
V41R0 *    MOVE 'FRENV'                 TO NCGFC-CTABLEG2                       
    | *    PERFORM SELECT-NCGFC                                                 
    | *    IF SQLCODE NOT = 0                                                   
    | *       MOVE ' '                  TO W-MESSAGE                            
    | *       STRING 'SELECT NCGFC: ' NCGFC-CTABLEG2                            
    | *                     DELIMITED BY SIZE INTO W-MESSAGE                    
    | *       PERFORM ERREUR-SQL                                                
    | *    END-IF                                                               
           ADD 1                        TO W-GV11-NLIGNE-PIC99                  
           MOVE W-GV11-NLIGNE-PICXX     TO GV11-NSEQ                            
           MOVE W-GV11-NLIGNE-PICXX     TO GV11-NLIGNE                          
           MOVE W-GV11-NLIGNE-PIC99     TO GV11-NSEQNQ GV11-NSEQREF             
           IF GV11-NSEQNQ > GV10-NSEQNQ                                         
              MOVE GV11-NSEQNQ          TO GV10-NSEQNQ                          
           END-IF.                                                              
V41R1 *    IF W-CC-V-I < W-CC-V-U                                               
    | *       MOVE W-CC-V-WEMPORTE(W-CC-V-I) TO WS-WEMPORTE                     
    | *    ELSE                                                                 
    | *       MOVE GV11-WEMPORTE         TO WS-WEMPORTE                         
    | *    END-IF                                                               
    |      SET WC-TSTENVOI-SUITE         TO TRUE                                
    |      PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                         
    |      UNTIL WC-TSTENVOI-FIN                                                
    |         PERFORM LECTURE-TSTENVOI                                          
    |         IF  WC-TSTENVOI-SUITE                                             
    |         AND TSTENVOI-NSOCZP      = '00000'                                
    |         AND TSTENVOI-CTRL(1:1)   = 'A'                                    
    |         AND TSTENVOI-WPARAM(4:1) > SPACE                                  
    |         AND TSTENVOI-WPARAM(4:1) = WS-WEMPORTE                            
      * ->  on annule la modif.                                                 
1202  *    D.COM envoie les frais.                                              
      *    MGD envoie les services...                                           
mgd15 *       and tstenvoi-wparam(6:4) = w-cc-canal (1:4)                       
    |            MOVE TSTENVOI-LIBELLE(6:5) TO GV11-CENREG                      
    |            SET WC-TSTENVOI-FIN    TO TRUE                                 
    |         END-IF                                                            
    |      END-PERFORM.                                                         
    | *    IF WS-CHRONOPOST                                                     
    | *      MOVE NCGFC-COMMENT(6:5)    TO GV11-CENREG                          
    | *    ELSE                                                                 
    | *       MOVE NCGFC-COMMENT(1:5)   TO GV11-CENREG                          
    | *    END-IF                                                               
           MOVE W-CC-E-MT-SHIP          TO GV11-PVUNIT GV11-PVUNITF             
                                           GV11-PVTOTAL                         
            ADD GV11-PVTOTAL            TO GV10-PTTVENTE                        
           PERFORM INSERT-RTGV11                                                
      *    END-IF.                                                              
           MOVE W-GV11-NCODIC           TO GV11-NCODIC.                         
       FIN-CREATION-PRESTA-FRENV. EXIT.                                         
      *                                                                         
      *-----------------------------------------------------------------        
      * Cr�ation Presta ajustement                                              
       CREATION-PRESTA-AJUST     SECTION.                                       
           add 1 to w-nseqens                                                   
           move w-nseqens           to gv11-nseqens.                            
           MOVE '4'    TO GV11-CTYPENREG.                                       
           MOVE SPACES TO GV11-NCODIC GV11-NCODICGRP.                           
           MOVE 1      TO GV11-QVENDUE.                                         
      *    MOVE W-GV11-NSEQNQ        TO GV11-NSEQREF.                           
 0116      IF   GV11-CEQUIPE < SPACES                                           
 0116           MOVE SPACES            TO GV11-CEQUIPE                          
 0116      END-IF                                                               
      * Presta Ajustement                                                       
           IF W-CC-E-MT-AJUST NOT = 0                                           
              MOVE 'AJUST' TO NCGFC-CTABLEG2                                    
              PERFORM SELECT-NCGFC                                              
              IF SQLCODE NOT = 0                                                
                 MOVE ' ' TO W-MESSAGE                                          
                 STRING 'SELECT NCGFC: ' NCGFC-CTABLEG2                         
                               DELIMITED BY SIZE INTO W-MESSAGE                 
                 PERFORM ERREUR-SQL                                             
              END-IF                                                            
              ADD 1 TO W-GV11-NLIGNE-PIC99                                      
              MOVE W-GV11-NLIGNE-PICXX TO GV11-NSEQ                             
              MOVE W-GV11-NLIGNE-PICXX TO GV11-NLIGNE                           
              MOVE W-GV11-NLIGNE-PIC99 TO GV11-NSEQNQ GV11-NSEQREF              
              IF GV11-NSEQNQ > GV10-NSEQNQ                                      
                 MOVE GV11-NSEQNQ             TO GV10-NSEQNQ                    
              END-IF                                                            
              MOVE NCGFC-COMMENT       TO GV11-CENREG                           
              MOVE W-CC-E-MT-AJUST TO GV11-PVUNIT GV11-PVUNITF                  
                                     GV11-PVTOTAL                               
              ADD GV11-PVTOTAL TO GV10-PTTVENTE                                 
              PERFORM INSERT-RTGV11                                             
              MOVE '1' TO W-CC-E-C-RET                                          
           END-IF.                                                              
       FIN-CREATION-PRESTA-AJUST. EXIT.                                         
      ******************************************************************        
      *                                                                         
      * A partir d'ici, uniquement modules techniques sans int�r�t pour         
      * la compr�hension du programme.                                          
      *                                                                         
      ******************************************************************        
      ******************************************************************        
      * Recherche du Code Ilot (sert � Paris pour Logitrans), C�D:              
      * sur Paris il y a un soft (sur PC) qui g�re l'optimisation des           
      * tourn�es de livraison.                                                  
      * On cherche dans RTLG09: si trouv� avec une parit� <> ' ', c'est         
      * OK, on a trouv�: toute la rue fait partie du m�me CILOT.                
      * Si parit� <= ' ', on va voir le CILOT du tron�on (RTLG01).              
      ******************************************************************        
       RECHERCHE-CODE-ILOT SECTION.                                             
      *----------------------------                                             
           PERFORM SELECT-RTLG09.                                               
           IF SQLCODE = 0                                                       
              IF LG09-CPARITE > ' '                                             
                 MOVE LG09-CILOT     TO GV02-CILOT                              
              ELSE                                                              
                 PERFORM SELECT-RTLG01                                          
                 IF SQLCODE = 0 OR -811                                         
                    MOVE LG01-CILOT  TO GV02-CILOT                              
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF (SQLCODE = 100) AND (FAIS-DES-DISPLAYS)                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING                                                            
                     'Ilot Non trouv�: CINSEE: ' LG09-CINSEE                    
                                     ' CTVOIE: ' LG09-CTVOIE                    
                                   ' LNOMVOIE: ' LG09-LNOMVOIE                  
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM DISPLAY-W-MESSAGE                                         
           END-IF.                                                              
       SELECT-RTLG09 SECTION.                                                   
      *----------------------                                                   
           MOVE GV02-CINSEE   TO LG09-CINSEE.                                   
           MOVE GV02-CTVOIE   TO LG09-CTVOIE.                                   
           MOVE GV02-LNOMVOIE TO LG09-LNOMVOIE.                                 
           EXEC SQL SELECT NVOIE, CILOT, CPARITE                                
                      INTO :LG09-NVOIE, :LG09-CILOT, :LG09-CPARITE              
                      FROM RVLG0900                                             
                     WHERE CINSEE   = :LG09-CINSEE                              
                       AND CTVOIE   = :LG09-CTVOIE                              
                       AND LNOMVOIE = :LG09-LNOMVOIE                            
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE ' ' TO W-MESSAGE                                             
              STRING 'SELECT RTGL09 CINSEE: ' LG09-CINSEE                       
                                  ' CTVOIE: ' LG09-CTVOIE                       
                                ' LNOMVOIE: ' LG09-LNOMVOIE                     
                     DELIMITED BY SIZE INTO W-MESSAGE                           
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       SELECT-RTLG01 SECTION.                                                   
      *----------------------                                                   
      * pas de num�ro de voie...                                                
           IF GV02-CVOIE <= ' '                                                 
              GO FIN-SELECT-RTLG01                                              
           END-IF.                                                              
           MOVE GV02-CVOIE TO LG01-NDEBTR.                                      
      * ...on �limine tout ce qui n'est pas num�rique...                        
           PERFORM VARYING I-I FROM 1 BY 1 UNTIL I-I > 4                        
              IF LG01-NDEBTR(I-I:1) < '0' OR > '9'                              
                 MOVE SPACES TO LG01-NDEBTR(I-I:1)                              
              END-IF                                                            
           END-PERFORM.                                                         
      * ...il n'y avait pas de num�ro...                                        
           IF LG01-NDEBTR <= ' '                                                
              GO FIN-SELECT-RTLG01                                              
           END-IF.                                                              
      * ...on cadre � droite...                                                 
           PERFORM VARYING I-I FROM 1 BY 1 UNTIL I-I > 4                        
              IF LG01-NDEBTR(4:1) = ' '                                         
                 MOVE LG01-NDEBTR TO LG01-NDEBTR(2:)                            
                 MOVE ' '         TO LG01-NDEBTR(1:1)                           
              END-IF                                                            
           END-PERFORM.                                                         
      * ...et on remplit par des z�ros.                                         
           INSPECT LG01-NDEBTR REPLACING ALL ' ' BY '0'                         
      * ...et on continue....  t'es pair ou impair?                             
           IF LG01-NDEBTR(4:1) = '1' OR '3' OR '5' OR '7' OR '9'                
              MOVE '1' TO LG01-CPARITE                                          
           ELSE                                                                 
              MOVE '2' TO LG01-CPARITE                                          
           END-IF.                                                              
           EXEC SQL SELECT CILOT                                                
                      INTO :LG01-CILOT                                          
                      FROM RVLG0100                                             
                     WHERE CINSEE   = :LG09-CINSEE                              
                       AND NVOIE    = :LG09-NVOIE                               
                       AND CPARITE  = :LG01-CPARITE                             
                       AND :LG01-NDEBTR BETWEEN NDEBTR AND NFINTR               
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100 AND -811                                  
              MOVE ' ' TO W-MESSAGE                                             
              STRING 'SELECT RTGL01 CINSEE: ' LG09-CINSEE                       
                                   ' NVOIE: ' LG09-NVOIE                        
                                 ' CPARITE: ' LG01-CPARITE                      
                                  ' NDEBTR: ' LG01-NDEBTR                       
                     DELIMITED BY SIZE INTO W-MESSAGE                           
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       FIN-SELECT-RTLG01. EXIT.                                                 
      ******************************************************************        
      * On va voir si cette livraison est soumise � Logitrans ou pas.           
      ******************************************************************        
       SELECT-CILOT SECTION.                                                    
      *---------------------                                                    
           MOVE W-CC-E-C-INSEE-LIV  TO CILOT-CTABLEG2.                          
           EXEC SQL SELECT WTABLEG INTO :CILOT-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'CILOT'                                   
                       AND CTABLEG2 = :CILOT-CTABLEG2                           
                       AND SUBSTR(WTABLEG, 1 , 1) = 'O'                         
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE ' ' TO W-MESSAGE                                             
              STRING 'SELECT CILOT: ' CILOT-CTABLEG2                            
                            DELIMITED BY SIZE INTO W-MESSAGE                    
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 0                                                       
              SET IL-Y-A-DU-LOGITRANS TO TRUE                                   
           END-IF.                                                              
      ******************************************************************        
      * Codes Presta � r�cup�rer de NCFFC                                       
      ******************************************************************        
       SELECT-NCGFC SECTION.                                                    
      *---------------------                                                    
           MOVE SPACES TO NCGFC-WTABLEG.                                        
           EXEC SQL SELECT WTABLEG INTO :NCGFC-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'NCGFC'                                   
                       AND CTABLEG2 = :NCGFC-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE ' ' TO W-MESSAGE                                             
              STRING 'SELECT NCGFC: ' NCGFC-CTABLEG2                            
                            DELIMITED BY SIZE INTO W-MESSAGE                    
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * S'il y a une �quipe de livraison forc�e                                 
      ******************************************************************        
       SELECT-LIVRP SECTION.                                                    
      *---------------------                                                    
           MOVE SPACES TO LIVRP-CTABLEG2.                                       
           MOVE GQ04-CPERIMETRE TO LIVRP-CPERIM.                                
           MOVE 'L'             TO LIVRP-CTQUOTA.                               
           EXEC SQL SELECT WTABLEG INTO :LIVRP-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'LIVRP'                                   
                       AND CTABLEG2 = :LIVRP-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE ' ' TO W-MESSAGE                                             
              STRING 'SELECT LIVRP (code �quipe) ' LIVRP-CTABLEG2               
                            DELIMITED BY SIZE INTO W-MESSAGE                    
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Profil de tourn�e                                                       
      ******************************************************************        
       SELECT-TPROD SECTION.                                                    
      *---------------------                                                    
           MOVE SPACES             TO LIVRP-CTABLEG2.                           
           MOVE LIVRP-CMODDEL(1:3) TO TPROD-CMODDEL.                            
           MOVE SPACES             TO TPROD-HEURE.                              
Q6107      MOVE TPROD-CMODDEL      TO WS-CMODDEL.                               
"          MOVE GQ03-CPLAGE        TO WS-CPLAGE .                               
"          MOVE GQ04-CEQUIP1       TO WS-CEQUIP .                               
Q6107      EXEC SQL SELECT CTABLEG2 INTO :TPROD-CTABLEG2                        
             FROM  RVGA0100                                                     
            WHERE                                                               
             CTABLEG1                      =  'TPROD'                           
             AND SUBSTR(CTABLEG2, 6, 10)   =  :WS-MOD-PLAG-EQUIP-R              
             AND SUBSTR(WTABLEG, 1, 1)    <>  'N'                               
             ORDER BY SUBSTR(WTABLEG, 2, 4)   ASC                               
Q6107        FETCH FIRST 1 ROWS ONLY                                            
Q6107      END-EXEC.                                                            
Q6107 * *************************************************                       
Q6107 * query num 6107 consommateur  cpu                                        
Q6107 * ordre decoup� en 2 select                                               
Q6107 * *************************************************                       
      *    EXEC SQL SELECT CTABLEG2 INTO :TPROD-CTABLEG2                        
      *               FROM RVGA01HN MSTR                                        
V5.1.P*              WHERE CMODDEL =  :TPROD-CMODDEL                            
      *                AND CPLAGE  =  :GQ03-CPLAGE                              
      *                AND CEQUIP  =  :GQ04-CEQUIP1                             
      *                AND WACTIF  <> 'N'                                       
v5.1p *                AND HEURE   = (SELECT MIN(HEURE)                         
      *                                 FROM RVGA01HN                           
      *                                WHERE CMODDEL =  MSTR.CMODDEL            
      *                                  AND CPLAGE  =  MSTR.CPLAGE             
      *                                  AND CEQUIP  =  MSTR.CEQUIP             
      *                                  AND WACTIF <> 'N')                     
Q6107 *    END-EXEC.                                                            
Q6107|*    EXEC SQL                                                             
v5.1p|*              SELECT MIN(HEURE)                                          
     |*               into  TPROD-HEURE                                         
     |*              FROM RVGA01HN                                              
     |*              WHERE CMODDEL =  :TPROD-CMODDEL                            
     |*                AND CPLAGE  =  :GQ03-CPLAGE                              
     |*                AND CEQUIP  =  :GQ04-CEQUIP1                             
     |*                AND WACTIF <> 'N'                                        
Q6107|*    END-EXEC.                                                            
      *    IF SQLCODE NOT = 0 AND 100                                           
      *       MOVE ' ' TO W-MESSAGE                                             
      *       STRING 'SELECT TPROD (PROFIL TOURN�E) '                           
      *                    'plage: ' GQ03-CPLAGE                                
      *                    ' �quipe: ' GQ04-CEQUIP1                             
      *                    ' MIN(HEURE)'                                        
      *                     DELIMITED BY SIZE INTO W-MESSAGE                    
      *       PERFORM ERREUR-SQL                                                
      *    END-IF.                                                              
      *                                                                         
Q6107|*    EXEC SQL SELECT CTABLEG2 INTO :TPROD-CTABLEG2                        
     |*               FROM RVGA01HN                                             
V5.1.|*              WHERE CMODDEL =  :TPROD-CMODDEL                            
     |*                AND CPLAGE  =  :GQ03-CPLAGE                              
     |*                AND CEQUIP  =  :GQ04-CEQUIP1                             
     |*                AND WACTIF  <> 'N'                                       
     |*                and HEURE   =  :TPROD-HEURE                              
Q6106|*    END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE ' ' TO W-MESSAGE                                             
              STRING 'SELECT TPROD (PROFIL TOURN�E) '                           
                           'plage: ' GQ03-CPLAGE                                
                           ' �quipe: ' GQ04-CEQUIP1                             
                            DELIMITED BY SIZE INTO W-MESSAGE                    
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           if sqlcode = +100                                                    
              MOVE ' ' TO W-MESSAGE                                             
              STRING   ' TPROD non trouv�e'                                     
                                      ' plage: ' GQ03-CPLAGE                    
                                      ' �quipe: ' GQ04-CEQUIP1                  
                            DELIMITED BY SIZE INTO W-MESSAGE                    
              PERFORM DISPLAY-W-MESSAGE                                         
           end-if.                                                              
      ******************************************************************        
      * Recherche de l'�quipe "poubelle" de livraison.                          
      ******************************************************************        
       SELECT-EQUIG SECTION.                                                    
      *---------------------                                                    
           EXEC SQL SELECT CTABLEG2 INTO :EQUIG-CTABLEG2                        
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'EQUIG'                                   
                       AND SUBSTR(WTABLEG, 21, 1) = 'O'                         
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'SELECT Equipe Poubelle' TO W-MESSAGE                        
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           MOVE EQUIG-CEQUIQ TO GQ04-CEQUIP1 GQ03-CEQUIP.                       
           SET GV10-WCQERESF-EN-Z TO TRUE.                                      
      ******************************************************************        
      * Param�tres Caract�ristiques Sp�cifiques                                 
      ******************************************************************        
       SELECT-CARSP SECTION.                                                    
      *---------------------                                                    
           MOVE GV15-CARACTSPE1 TO CARSP-CTABLEG2.                              
           EXEC SQL SELECT WTABLEG INTO :CARSP-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'CARSP'                                   
                       AND CTABLEG2 = :CARSP-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'SELECT TG-CarSp:' TO W-MESSAGE                              
              MOVE CARSP-CTABLEG2     TO W-MESSAGE(18:)                         
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       SELECT-CADRE SECTION.                                                    
      *---------------------                                                    
           MOVE  SPACES               TO CADRE-CTABLEG2                         
           MOVE  W-SERVICE-LIVRAISON  TO CADRE-PRESTA                           
           MOVE  GA00-CFAM            TO CADRE-FAMILLE                          
           EXEC SQL SELECT WTABLEG                                              
                      INTO :CADRE-WTABLEG                                       
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'CADRE'                                   
                       AND CTABLEG2 = :CADRE-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT CADRE : ' CADRE-CTABLEG2                           
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Recherche des Quotas de Livraison                                       
      ******************************************************************        
       SELECT-RTGQ03 SECTION.                                                   
      *----------------------                                                   
           MOVE SPACES         TO LIVRE-CTABLEG2.                               
           MOVE W-CC-CELEMEN   TO LIVRE-CTABLEG2(6:).                           
           MOVE LIVRG-CTABLEG2      TO LIVRE-CTABLEG2(11:).                     
           MOVE LIVRP-CMODDEL(1:3)  TO GQ03-CMODDEL                             
           EXEC SQL SELECT QQUOTA, QPRIS, CZONLIV                               
                      INTO :GQ03-QQUOTA, :GQ03-QPRIS, :GQ03-CZONLIV             
                      FROM RVGQ0300, RVGA0100                                   
                     WHERE CMODDEL = :GQ03-CMODDEL                              
                       AND CEQUIP  = :GQ03-CEQUIP                               
                       AND DJOUR   = :GQ04-DEFFET                               
                       AND CZONLIV = SUBSTR(CTABLEG2, 1 , 5)                    
                       AND CTABLEG1 = 'LIVRE'                                   
                       AND SUBSTR(CTABLEG2, 6 , 10)                             
                         = SUBSTR(:LIVRE-CTABLEG2, 6 , 10)                      
                       AND QQUOTA > QPRIS                                       
                       AND CPLAGE  = :GQ03-CPLAGE                               
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100 AND -811                                  
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT RTGQ03     CEQUIP : ' GQ03-CEQUIP                  
                                      ' DEFFET : ' GQ04-DEFFET                  
                                      ' cmoddel: ' GQ03-CMODDEL                 
                                      ' CPLAGE : ' GQ03-CPLAGE                  
                                       ' LIVRE: ' LIVRE-CTABLEG2                
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 100                                                     
              EXEC SQL SELECT QQUOTA, QPRIS, CZONLIV                            
                         INTO :GQ03-QQUOTA, :GQ03-QPRIS, :GQ03-CZONLIV          
                         FROM RVGQ0300, RVGA0100                                
                        WHERE CMODDEL = :GQ03-CMODDEL                           
                          AND CEQUIP  = :GQ03-CEQUIP                            
                          AND DJOUR   = :GQ04-DEFFET                            
                          AND CZONLIV = SUBSTR(CTABLEG2, 1 , 5)                 
                          AND CTABLEG1 = 'LIVRE'                                
                          AND SUBSTR(CTABLEG2, 6 , 10)                          
                            = SUBSTR(:LIVRE-CTABLEG2, 6 , 10)                   
                          AND CPLAGE  = :GQ03-CPLAGE                            
              END-EXEC                                                          
              IF SQLCODE NOT = 0 AND 100 AND -811                               
                 MOVE SPACES TO W-MESSAGE                                       
                 STRING 'SELECT RTGQ03 LD2 CEQUIP: ' GQ03-CEQUIP                
                                         ' DEFFET: ' GQ04-DEFFET                
                                         ' CPLAGE: ' GQ03-CPLAGE                
                                          ' LIVRE: ' LIVRE-CTABLEG2             
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ERREUR-SQL                                             
              END-IF                                                            
              IF SQLCODE = 100                                                  
                 SET GV10-WCQERESF-EN-Z TO TRUE                                 
                 STRING 'Quota non trouv� : '                                   
                        GQ03-CEQUIP                                             
                        GQ04-DEFFET                                             
                        GQ03-CPLAGE                                             
                        LIVRE-CTABLEG2                                          
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM DISPLAY-W-MESSAGE                                      
              END-IF                                                            
           END-IF.                                                              
      ******************************************************************        
      * Update GQ03: si le quota maxi est atteint, on le gonfle.                
      * On met � jour de toute fa�on la quantit� prise.                         
      ******************************************************************        
       UPDATE-RTGQ03 SECTION.                                                   
      *----------------------                                                   
           MOVE W-SPDATDB2-DSYST     TO GQ03-DSYST.                             
           IF GQ03-QPRIS >= GQ03-QQUOTA                                         
              EXEC SQL UPDATE RVGQ0300                                          
                          SET QQUOTA = QQUOTA + 1,                              
                              QPRIS  = QPRIS  + 1,                              
                              DSYST  = :GQ03-DSYST                              
                        WHERE CMODDEL = :GQ03-CMODDEL                           
                          AND CEQUIP  = :GQ03-CEQUIP                            
                          AND DJOUR   = :GQ04-DEFFET                            
                          AND CZONLIV = :GQ03-CZONLIV                           
                          AND CPLAGE  = :GQ03-CPLAGE                            
              END-EXEC                                                          
           ELSE                                                                 
              EXEC SQL UPDATE RVGQ0300                                          
                          SET QPRIS  = QPRIS  + 1,                              
                              DSYST  = :GQ03-DSYST                              
                        WHERE CMODDEL = :GQ03-CMODDEL                           
                          AND CEQUIP  = :GQ03-CEQUIP                            
                          AND DJOUR   = :GQ04-DEFFET                            
                          AND CZONLIV = :GQ03-CZONLIV                           
                          AND CPLAGE  = :GQ03-CPLAGE                            
              END-EXEC                                                          
           END-IF.                                                              
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'UPDATE RTGQ03 LD2 CEQUIP: ' GQ03-CEQUIP                   
                                      ' DEFFET: ' GQ04-DEFFET                   
                                     ' CZONLIV: ' GQ03-CZONLIV                  
                                      ' CPLAGE: ' GQ03-CPLAGE                   
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Quelle sont les �quipes de livraison pour cette famille?                
      ******************************************************************        
       SELECT-RTGQ04 SECTION.                                                   
      *----------------------                                                   
           MOVE SPACES TO GQ04-CEQUIP1 GQ04-CEQUIP2 GQ04-CEQUIP3                
                          GQ04-CEQUIP4 GQ04-CEQUIP5.                            
           MOVE T-F-CFAM(T-F-I) TO GQ04-CFAM.                                   
           EXEC SQL SELECT CEQUIP1, CEQUIP2, CEQUIP3, CEQUIP4, CEQUIP5          
                      INTO :GQ04-CEQUIP1, :GQ04-CEQUIP2, :GQ04-CEQUIP3,         
                           :GQ04-CEQUIP4, :GQ04-CEQUIP5                         
                      FROM RVGQ0400 MSTR                                        
                     WHERE CMODDEL    = 'LD2'                                   
                       AND CPERIMETRE = :GQ04-CPERIMETRE                        
                       AND CFAM       = :GQ04-CFAM                              
                       AND DEFFET = (SELECT MAX(DEFFET) FROM RVGQ0400           
                                      WHERE CMODDEL    = MSTR.CMODDEL           
                                        AND CPERIMETRE = MSTR.CPERIMETRE        
                                        AND CFAM       = MSTR.CFAM              
                                        AND DEFFET    <= :GQ04-DEFFET)          
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT RTGQ04 LD2 CPERIMETRE: ' GQ04-CPERIMETRE           
                                         ' FAMILLE: ' GQ04-CFAM                 
                                          ' DEFFET: ' GQ04-DEFFET               
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 0                                                       
              MOVE GQ04-CEQUIP1 TO T-F-CEQUIP(T-F-I, 1)                         
              MOVE GQ04-CEQUIP2 TO T-F-CEQUIP(T-F-I, 2)                         
              MOVE GQ04-CEQUIP3 TO T-F-CEQUIP(T-F-I, 3)                         
              MOVE GQ04-CEQUIP4 TO T-F-CEQUIP(T-F-I, 4)                         
              MOVE GQ04-CEQUIP5 TO T-F-CEQUIP(T-F-I, 5)                         
           END-IF.                                                              
      ******************************************************************        
      * Coke en Stoke                                                           
      ******************************************************************        
       SELECT-RTGS10 SECTION.                                                   
      *----------------------                                                   
           MOVE 0 TO GS10-QSTOCK.                                               
           EXEC SQL SELECT QSTOCK - QSTOCKRES INTO :GS10-QSTOCK                 
                      FROM RVGS1000                                             
                     WHERE NSOCDEPOT  = :GS10-NSOCDEPOT                         
                       AND NDEPOT     = :GS10-NDEPOT                            
                       AND NCODIC     = :GS10-NCODIC                            
                       AND WSTOCKMASQ = 'N'                                     
                       AND NSSLIEU    = 'V'                                     
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT RTGS10 NSOCDEPOT: ' GS10-NSOCDEPOT                 
                                     ' NDEPOT: ' GS10-NDEPOT                    
                                     ' NCODIC: ' GS10-NCODIC                    
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      * LECTURE DE LA TABLE DES RESERVATIONS SUR COMMANDES                      
      * FOURNISSEUR ORDONNEE PAR DATE CROISSANTE ENTRE '01011900'               
      * ET '31122099'                                                           
      *                                                                         
       RESERVER-CDE-FOURNISSEUR       SECTION.                                  
      *---------------------------------------                                  
           PERFORM SELECT-RTFL50                                                
           IF FL50-WRESFOURN NOT = 'N'                                          
              PERFORM SELECT-RTFL90                                             
              INITIALIZE GFQNT0                                                 
              MOVE W-CC-DATE-DU-JOUR     TO GFSAMJ-0                            
              MOVE '5'                   TO GFDATA                              
              PERFORM FAIS-UN-LINK-A-TETDATC                                    
              COMPUTE GFQNT0 = GFQNT0 + FL90-QDELAIDEP                          
              MOVE '3'                   TO GFDATA                              
              PERFORM FAIS-UN-LINK-A-TETDATC                                    
              MOVE GFSAMJ-0              TO WS-DJOUR7                           
              MOVE GS10-NSOCDEPOT        TO GF55-NSOCDEPOT                      
              MOVE GS10-NDEPOT           TO GF55-NDEPOT                         
              MOVE GS10-NCODIC           TO GF55-NCODIC                         
              SET  WS-DATE-VALIDE        TO TRUE                                
              INITIALIZE WS-TABLE-CDE IND-TABC                                  
              PERFORM DECLARE-CRESERV                                           
              PERFORM FETCH-CRESERV                                             
              PERFORM UNTIL SQLCODE = 100 OR WS-RESER-FOURN-OK                  
                OR WS-DATE-INVALIDE                                             
                 IF GF55-QCDE > GF55-QCDERES                                    
                    PERFORM IMPACTER-CDE-FOURNISSEUR                            
                 END-IF                                                         
151014           IF NOT WS-RESER-FOURN-OK                                       
                 PERFORM FETCH-CRESERV                                          
151014           END-IF                                                         
              END-PERFORM                                                       
              IF WS-DCDE-FNR(1) NOT = SPACES OR                                 
                (WS-QCDE-FNR(1) NOT = SPACES AND ZEROES)                        
                 MOVE SPACES          TO W-MESSAGE                              
                 STRING                                                         
                 'AUTRE CDE FNR POSSIBLE ' WS-TABLE-CDE                         
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM DISPLAY-W-MESSAGE                                      
              END-IF                                                            
              PERFORM CLOSE-CRESERV                                             
           END-IF                                                               
              .                                                                 
       FIN-RESERVER-CDE-FOURNISSEUR.   EXIT.                                    
       SELECT-RTFL50                  SECTION.                                  
      *---------------------------------------                                  
           MOVE GS10-NSOCDEPOT     TO FL50-NSOCDEPOT                            
           MOVE GS10-NDEPOT        TO FL50-NDEPOT                               
           MOVE GS10-NCODIC        TO FL50-NCODIC                               
           EXEC SQL SELECT   CAPPRO, QDELAIAPPRO, CMODSTOCK1,                   
                      WMODSTOCK1, CMODSTOCK2, WMODSTOCK2,                       
                      CMODSTOCK3, WMODSTOCK3, WRESFOURN                         
                    INTO    :FL50-CAPPRO         , :FL50-QDELAIAPPRO  ,         
                            :FL50-CMODSTOCK1     , :FL50-WMODSTOCK1   ,         
                            :FL50-CMODSTOCK2     , :FL50-WMODSTOCK2   ,         
                            :FL50-CMODSTOCK3     , :FL50-WMODSTOCK3   ,         
                            :FL50-WRESFOURN                                     
                     FROM     RVFL5000                                          
                     WHERE    NSOCDEPOT = :FL50-NSOCDEPOT                       
                     AND      NDEPOT    = :FL50-NDEPOT                          
                     AND      NCODIC    = :FL50-NCODIC                          
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES          TO W-MESSAGE                                 
              STRING 'SELECT  WRESFOURN FL50'                                   
                                ' NSOCIETE: ' FL50-NSOCDEPOT                    
                                '    NLIEU: ' FL50-NDEPOT                       
                                '   NCODIC: ' FL50-NCODIC                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 100                                                     
              INITIALIZE  FL50-CAPPRO  FL50-QDELAIAPPRO  FL50-WRESFOURN         
               FL50-CMODSTOCK1 FL50-WMODSTOCK1 FL50-CMODSTOCK2                  
               FL50-WMODSTOCK2 FL50-CMODSTOCK3 FL50-WMODSTOCK3                  
               MOVE   'N'          TO FL50-WRESFOURN                            
           END-IF.                                                              
       FIN-SELECT-RTFL50.   EXIT.                                               
       SELECT-RTFL90                  SECTION.                                  
      *---------------------------------------                                  
           MOVE GS10-NSOCDEPOT     TO FL90-NSOCDEPOT                            
           MOVE GS10-NDEPOT        TO FL90-NDEPOT                               
           EXEC SQL SELECT  QDELAIDEP                                           
                           ,crossdockfour                                       
                    INTO    :FL90-QDELAIDEP                                     
                           ,:FL90-crossdockfour                                 
                    FROM     RVFL9005                                           
                    WHERE    NSOCDEPOT = :FL90-NSOCDEPOT                        
                    AND      NDEPOT    = :FL90-NDEPOT                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT  QDELAIDEP FL90'                                   
                                ' NSOCIETE: ' FL90-NSOCDEPOT                    
                                   ' NLIEU: ' FL90-NDEPOT                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 100                                                     
              MOVE 7               TO FL90-QDELAIDEP                            
           END-IF.                                                              
       FIN-SELECT-RTFL90.   EXIT.                                               
       DECLARE-CRESERV                SECTION.                                  
      *----------------------------------------                                 
           EXEC SQL DECLARE  RESERV CURSOR FOR                                  
                    SELECT   DCDE                                               
                            ,QCDE                                               
                            ,QCDERES                                            
                            ,DCALCUL                                            
                            ,dayofweek (                                        
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' !!                    
      *                      substr( dcde , 7 , 2) !! '/' !!                    
      *                      substr( dcde , 1 , 4))                             
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' !!                    
      *                      substr( dcde , 7 , 2) !! '/' ||                    
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' !!                    
      *                      substr( dcde , 7 , 2) || '/' ||                    
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' ||                    
      *--                                                                       
                             substr( dcde , 5 , 2) || '/' ||                    
      *}                                                                        
                             substr( dcde , 7 , 2) || '/' ||                    
      *}                                                                        
      *}                                                                        
                             substr( dcde , 1 , 4))                             
      *}                                                                        
                    FROM     RVGF5501                                           
                    WHERE    NSOCDEPOT = :GF55-NSOCDEPOT                        
                    AND      NDEPOT = :GF55-NDEPOT                              
                    AND      NCODIC = :GF55-NCODIC                              
                    AND      DCDE   BETWEEN '19000101' AND '20993112'           
                    ORDER BY DCALCUL                                            
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES          TO W-MESSAGE                                 
              STRING 'DECLARE  RESERV   GF55'                                   
                                 '      DCDE'                                   
                                 '      QCDE'                                   
                                 '   QCDERES'                                   
                                 ' NSOCDEPOT' GF55-NSOCDEPOT                    
                                 '    NDEPOT' GF55-NDEPOT                       
                                 '    NCODIC' GF55-NCODIC                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           EXEC SQL OPEN   RESERV END-EXEC.                                     
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES          TO W-MESSAGE                                 
              STRING 'OPEN RESERV   GF55 FOR'                                   
                                 ' NSOCDEPOT' GF55-NSOCDEPOT                    
                                 '    NDEPOT' GF55-NDEPOT                       
                                 '    NCODIC' GF55-NCODIC                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       FIN-DECLARE-CRESERV. EXIT.                                               
       FETCH-CRESERV                  SECTION.                                  
      *---------------------------------------                                  
           EXEC SQL FETCH    RESERV                                             
                    INTO    :GF55-DCDE                                          
                           ,:GF55-QCDE                                          
                           ,:GF55-QCDERES                                       
                           ,:GF55-DCALCUL                                       
                           ,:w-j-sem :w-f-sem-f                                 
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES          TO W-MESSAGE                                 
              STRING 'FETCH    RESERV   GF55'                                   
                                 '      DCDE'                                   
                                 '      QCDE'                                   
                                 '   QCDERES'                                   
                                 ' NSOCDEPOT' GF55-NSOCDEPOT                    
                                 '    NDEPOT' GF55-NDEPOT                       
                                 '    NCODIC' GF55-NCODIC                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       FIN-FETCH-CRESERV. EXIT.                                                 
       CLOSE-CRESERV                  SECTION.                                  
      *---------------------------------------                                  
           EXEC SQL CLOSE    RESERV                                             
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES          TO W-MESSAGE                                 
              STRING 'OPEN RESERV   GF55 FOR'                                   
                                 ' NSOCDEPOT' GF55-NSOCDEPOT                    
                                 '    NDEPOT' GF55-NDEPOT                       
                                 '    NCODIC' GF55-NCODIC                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       FIN-CLOSE-CRESERV. EXIT.                                                 
       IMPACTER-CDE-FOURNISSEUR       SECTION.                                  
      *---------------------------------------                                  
           IF W-CC-DATE-DU-JOUR  <  W-DATE-ACTIVATION-GF55                      
              MOVE GF55-DCDE          TO GFSAMJ-0                               
              MOVE '5'                TO GFDATA                                 
              PERFORM FAIS-UN-LINK-A-TETDATC                                    
              COMPUTE GFQNT0 = GFQNT0 + FL90-QDELAIDEP                          
              MOVE '3'                TO GFDATA                                 
              PERFORM FAIS-UN-LINK-A-TETDATC                                    
              MOVE GFSAMJ-0           TO WS-DCDE7                               
              IF WS-DCDE7 > WS-DJOUR7                                           
                 MOVE WS-DCDE7      TO GFSAMJ-0                                 
              ELSE                                                              
                 MOVE WS-DJOUR7     TO GFSAMJ-0                                 
              END-IF                                                            
           ELSE                                                                 
              MOVE GF55-DCALCUL       TO GFSAMJ-0                               
           END-IF.                                                              
           MOVE '5'                TO GFDATA                                    
           PERFORM FAIS-UN-LINK-A-TETDATC                                       
           MOVE GFSAMJ-0           TO WS-DRECEPT                                
           MOVE GFJMA-3            TO WS-DRECEPT6                               
           IF WQTE  > (GF55-QCDE - GF55-QCDERES)                                
             COMPUTE WS-QUANT-DIS   = GF55-QCDE - GF55-QCDERES                  
             ADD  1                 TO IND-TABC                                 
             IF IND-TABC NOT > IND-CDE                                          
                MOVE WS-DRECEPT6    TO WS-DCDE-FNR(IND-TABC)                    
                MOVE '/'            TO WS-FIL1-FNR(IND-TABC)                    
                MOVE WS-QUANT-DIS   TO WS-QCDE-FNR(IND-TABC)                    
                MOVE SPACE          TO WS-FIL2-FNR(IND-TABC)                    
             END-IF                                                             
           ELSE                                                                 
      *       IF GV11-DDELIV < WS-DRECEPT                                       
      *          MOVE WS-DRECEPT            TO GV11-DDELIV                      
      *       END-IF                                                            
              IF W-CC-DATE-DU-JOUR  <  W-DATE-ACTIVATION-GF55                   
                 IF (WS-DCDE7 = SPACES OR                                       
                    GV11-DDELIV NOT < WS-DCDE7)  AND                            
                     GV11-DDELIV NOT < WS-DJOUR7                                
                        SET WS-RESER-FOURN-OK TO TRUE                           
                 ELSE                                                           
                    SET  WS-DATE-INVALIDE      TO TRUE                          
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING                                                      
                    'DATE INVALIDE PROCHAINE'                                   
                    ' CDE FNR LE '   WS-DRECEPT6                                
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-W-MESSAGE                                   
                 END-IF                                                         
              ELSE                                                              
                 IF GV11-DDELIV NOT < WS-DRECEPT                                
                    SET  WS-RESER-FOURN-OK         TO TRUE                      
v5.1.p              PERFORM DATE-CF-CALEE                                       
                 ELSE                                                           
                    SET  WS-DATE-INVALIDE      TO TRUE                          
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING                                                      
                    'DATE INVALIDE PROCHAINE'                                   
                    ' CDE FNR LE '   WS-DRECEPT6                                
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-W-MESSAGE                                   
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
       FIN-IMPACTER-CDE-FOURNISSEUR .   EXIT.                                   
      * -> ON RECHERCHE SI LA DATE DE COMMANDE TROUV�E EST                      
      *    CAL�E OU NON.                                                        
      * -> R�GLE LE JOUR DE LA SEMAINE CORRESPONDANT � GF55-DCDE                
      *    EST UN SAMEDI OU UN DIMANCHE OU LA DATE GF55-DCDE SE                 
      *    TROUVE R�F�RENC�E COMME UNE DATE PHARE DANS RTGF49.                  
       DATE-CF-CALEE SECTION.                                                   
           IF w-j-sem not = 1 and 7                                             
               EXEC SQL                                                         
                    SELECT 1                                                    
                       INTO :w-j-sem                                            
                      FROM RVGF4900                                             
                     WHERE WACTIF = 'O'                                         
                       AND dphare = :GF55-DCDE                                  
                       AND CPHARE NOT LIKE 'NS%'                                
               END-EXEC                                                         
           END-IF.                                                              
           if w-j-sem  = 1 or 7                                                 
              MOVE '0026'             TO MEC15C-NSEQERR                         
              MOVE 'E'                TO MEC15C-STAT                            
              SET  MEC15E-KO          TO TRUE                                   
              PERFORM CREE-MOUCHARD-LIGNE                                       
           end-if.                                                              
       F-DATE-CF-CALEE. EXIT.                                                   
LA1008 RESERVER-CONTREMARQUE          SECTION.                                  
      *---------------------------------------                                  
           IF FL50-WRESFOURN NOT = 'N'                                          
              SET WS-RES-CQE-TRT-OK TO TRUE                                     
              INITIALIZE GFQNT0                                                 
              MOVE W-CC-DATE-DU-JOUR     TO GFSAMJ-0                            
              MOVE '5'                   TO GFDATA                              
              PERFORM FAIS-UN-LINK-A-TETDATC                                    
              MOVE FL50-QDELAIAPPRO  TO WS-QDELAIAPPRO                          
              COMPUTE GFQNT0= GFQNT0 + FL90-QDELAIDEP + WS-QDELAIAPPRO-9        
              MOVE '3'                   TO GFDATA                              
              PERFORM FAIS-UN-LINK-A-TETDATC                                    
              MOVE GFSAMJ-0              TO WS-DJOUR7                           
              IF GV11-DDELIV >= WS-DJOUR7                                       
                 SET IL-Y-A-UN-ENTREPOT  TO TRUE                                
                 MOVE GV10-NSOCIETE      TO GF00-NSOCIETE                       
                 MOVE GV10-NLIEU         TO GF00-NLIEU                          
                 MOVE W-CC-V-CODIC(W-CC-V-I) TO GF00-NCODIC                     
                 MOVE GV11-DDELIV        TO GF00-DSOUHAITEE                     
                 MOVE GS10-NSOCDEPOT     TO GF00-NSOCLIVR                       
                 MOVE GS10-NDEPOT        TO GF00-NDEPOT                         
                 MOVE GV10-NVENTE        TO GF00-NCDECLIENT                     
                 PERFORM DECLARE-RTGF00                                         
                 PERFORM FETCH-RTGF00                                           
                 PERFORM UNTIL GF00-NON-TROUVE                                  
                 OR (GF00-CSTATUT = 'A' OR 'I')                                 
                    PERFORM FETCH-RTGF00                                        
                 END-PERFORM                                                    
                 PERFORM CLOSE-RTGF00                                           
                 IF GF00-NON-TROUVE                                             
                    PERFORM INSERT-RTGF00                                       
                 ELSE                                                           
                    IF (GF00-CSTATUT = 'I')                                     
                       PERFORM SELECT-RTGF05                                    
                       IF SQLCODE = 0                                           
                          IF GF05-QSOUHAITEE > GF00-QSOUHAITEE                  
                             COMPUTE GF05-QSOUHAITEE =                          
                             GF05-QSOUHAITEE - GF00-QSOUHAITEE                  
                             MOVE W-SPDATDB2-DSYST     TO GF05-DSYST            
                             PERFORM UPDATE-RTGF05                              
                          ELSE                                                  
                             PERFORM DELETE-RTGF05                              
                          END-IF                                                
                       END-IF                                                   
                    END-IF                                                      
                    PERFORM UPDATE-RTGF00                                       
                 END-IF                                                         
                 SET GV11-WCQERESF-EN-Q  TO TRUE                                
              END-IF                                                            
           END-IF                                                               
           .                                                                    
LA1008 FIN-RESERVER-CONTREMARQUE.      EXIT.                                    
LA1008 DECLARE-RTGF00                 SECTION.                                  
      *                                                                         
           EXEC SQL DECLARE  CGF00 CURSOR FOR                                   
                    SELECT   QSOUHAITEE,                                        
                             CSTATUT,                                           
                             NSEQ                                               
                    FROM     RVGF0000                                           
                    WHERE    NSOCIETE   = :GF00-NSOCIETE                        
                    AND      NLIEU      = :GF00-NLIEU                           
                    AND      NCODIC     = :GF00-NCODIC                          
                    AND      DSOUHAITEE = :GF00-DSOUHAITEE                      
                    AND      NSOCLIVR   = :GF00-NSOCLIVR                        
                    AND      NDEPOT     = :GF00-NDEPOT                          
                    AND      NCDECLIENT = :GF00-NCDECLIENT                      
           END-EXEC                                                             
           EXEC SQL OPEN   CGF00                                                
           END-EXEC                                                             
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'OPEN RTGF00: SOCIETE ' GF00-NSOCIETE                      
                                     ' NDEPOT: ' GF00-NDEPOT                    
                                     ' NCODIC: ' GF00-NCODIC                    
                               ' DSOUHAITEE:  ' GF00-DSOUHAITEE                 
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF                                                               
           .                                                                    
      *                                                                         
LA1008 FIN-DECLARE-RTGF00.  EXIT.                                               
LA1008 FETCH-RTGF00                  SECTION.                                   
      *                                                                         
           EXEC SQL FETCH    CGF00                                              
                    INTO    :GF00-QSOUHAITEE,                                   
                            :GF00-CSTATUT,                                      
                            :GF00-NSEQ                                          
           END-EXEC                                                             
           EVALUATE SQLCODE                                                     
           WHEN 0                                                               
              SET GF00-TROUVE  TO TRUE                                          
           WHEN 100                                                             
              SET GF00-NON-TROUVE  TO TRUE                                      
           WHEN OTHER                                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'FETCH RTGF00: SOCIETE ' GF00-NSOCIETE                     
                                     ' NDEPOT: ' GF00-NDEPOT                    
                                     ' NCODIC: ' GF00-NCODIC                    
                               ' DSOUHAITEE:  ' GF00-DSOUHAITEE                 
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-EVALUATE                                                         
      *                                                                         
           .                                                                    
LA1008 FIN-FETCH-RTGF00.  EXIT.                                                 
LA1008 CLOSE-RTGF00                  SECTION.                                   
      *                                                                         
           EXEC SQL CLOSE    CGF00                                              
           END-EXEC                                                             
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'CLOSE RTGF00: SOCIETE ' GF00-NSOCIETE                     
                                     ' NDEPOT: ' GF00-NDEPOT                    
                                     ' NCODIC: ' GF00-NCODIC                    
                               ' DSOUHAITEE:  ' GF00-DSOUHAITEE                 
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      *                                                                         
LA1008 FIN-CLOSE-RTGF00.  EXIT.                                                 
LA1008 UPDATE-RTGF00                  SECTION.                                  
           MOVE W-SPDATDB2-DSYST     TO GF00-DSYST                              
           EXEC SQL UPDATE   RVGF0000                                           
                    SET      QSOUHAITEE = QSOUHAITEE + :WQTE,                   
                             DSYST      = :GF00-DSYST                           
                    WHERE                                                       
                             NSOCIETE   = :GF00-NSOCIETE                        
                    AND      NLIEU      = :GF00-NLIEU                           
                    AND      NCODIC     = :GF00-NCODIC                          
                    AND      DSOUHAITEE = :GF00-DSOUHAITEE                      
                    AND      NSOCLIVR   = :GF00-NSOCLIVR                        
                    AND      NDEPOT     = :GF00-NDEPOT                          
                    AND      NCDECLIENT = :GF00-NCDECLIENT                      
                    AND      NSEQ       = :GF00-NSEQ                            
                    AND      CSTATUT    = 'A'                                   
           END-EXEC                                                             
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'UPD RTGF00: SOCIETE ' GF00-NSOCIETE                       
                                     ' NDEPOT: ' GF00-NDEPOT                    
                                     ' NCODIC: ' GF00-NCODIC                    
                               ' DSOUHAITEE:  ' GF00-DSOUHAITEE                 
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
LA1008 FIN-UPDATE-RTGF00.   EXIT.                                               
LA1008 INSERT-RTGF00                  SECTION.                                  
           MOVE W-SPDATDB2-DSYST     TO GF00-DSYST                              
           MOVE W-CC-DATE-DU-JOUR    TO GF00-DCREATION                          
LA1806     MOVE W-GV11-NLIGNE-PICXX  TO GF00-NSEQ                               
           EXEC SQL INSERT                                                      
                    INTO     RVGF0000                                           
                            (NSOCIETE     , NLIEU       , DCREATION   ,         
                             NCODIC       , QSOUHAITEE  , DSOUHAITEE  ,         
                             NSOCLIVR     , NDEPOT      , NCDECLIENT  ,         
                             CSTATUT      , NSEQ        , DSYST)                
                    VALUES (                                                    
                             :GF00-NSOCIETE                                     
                            ,:GF00-NLIEU                                        
                            ,:GF00-DCREATION                                    
                            ,:GF00-NCODIC                                       
                            ,:WQTE                                              
                            ,:GF00-DSOUHAITEE                                   
                            ,:GF00-NSOCLIVR                                     
                            ,:GF00-NDEPOT                                       
                            ,:GF00-NCDECLIENT                                   
                            ,'A'                                                
LA1806*                     ,'01'                                               
LA1806                      ,:GF00-NSEQ                                         
                            ,:GF00-DSYST                                        
                    )                                                           
           END-EXEC                                                             
           .                                                                    
LA1008 FIN-INSERT-RTGF00.   EXIT.                                               
LA1008 SELECT-RTGF05                  SECTION.                                  
      *---------------------------------------                                  
           EXEC SQL SELECT   QSOUHAITEE,                                        
                             CSTATUT                                            
NSKEP                       ,CFEXT                                              
                    INTO    :GF05-QSOUHAITEE,                                   
                            :GF05-CSTATUT                                       
NSKEP                      ,:GF05-CFEXT                                         
NSKEP *             FROM     RVGF0500                                           
NSKEP               FROM     RVGF0501                                           
                    WHERE    NSOCIETE   = :GF00-NSOCIETE                        
                    AND      NLIEU      = :GF00-NLIEU                           
                    AND      NCODIC     = :GF00-NCODIC                          
                    AND      DSOUHAITEE = :GF00-DSOUHAITEE                      
                    AND      NSOCLIVR   = :GF00-NSOCLIVR                        
                    AND      NDEPOT     = :GF00-NDEPOT                          
                    AND      NSEQ       = :GF00-NSEQ                            
NSKEP               ORDER BY CFEXT                                              
NSKEP               FETCH FIRST 1 ROW ONLY                                      
           END-EXEC                                                             
           .                                                                    
      *                                                                         
LA1008 FIN-SELECT-RTGF05.   EXIT.                                               
LA1008 UPDATE-RTGF05                  SECTION.                                  
      *                                                                         
           MOVE W-SPDATDB2-DSYST     TO GF05-DSYST                              
NSKEP *    EXEC SQL UPDATE   RVGF0500                                           
NSKEP      EXEC SQL UPDATE   RVGF0501                                           
                    SET      QSOUHAITEE = :GF05-QSOUHAITEE,                     
                             DSYST      = :GF05-DSYST                           
                    WHERE    NSOCIETE   = :GF00-NSOCIETE                        
                    AND      NLIEU      = :GF00-NLIEU                           
                    AND      NCODIC     = :GF00-NCODIC                          
                    AND      DSOUHAITEE = :GF00-DSOUHAITEE                      
                    AND      NSOCLIVR   = :GF00-NSOCLIVR                        
                    AND      NDEPOT     = :GF00-NDEPOT                          
                    AND      NSEQ       = :GF00-NSEQ                            
NSKEP               AND      CFEXT      = :GF05-CFEXT                           
           END-EXEC                                                             
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'UPD GF05 NSOCIETE: ' GF00-NSOCIETE                        
                                     ' NDEPOT: ' GF00-NDEPOT                    
                                     ' NCODIC: ' GF00-NCODIC                    
                               ' DSOUHAITEE:  ' GF00-DSOUHAITEE                 
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF                                                               
           .                                                                    
LA1008 FIN-UPDATE-RTGF05.   EXIT.                                               
LA1008 DELETE-RTGF05                  SECTION.                                  
      *                                                                         
           EXEC SQL DELETE                                                      
NSKEP *             FROM     RVGF0500                                           
NSKEP               FROM     RVGF0501                                           
                    WHERE    NSOCIETE   = :GF00-NSOCIETE                        
                    AND      NLIEU      = :GF00-NLIEU                           
                    AND      NCODIC     = :GF00-NCODIC                          
                    AND      DSOUHAITEE = :GF00-DSOUHAITEE                      
                    AND      NSOCLIVR   = :GF00-NSOCLIVR                        
                    AND      NDEPOT     = :GF00-NDEPOT                          
                    AND      NSEQ       = :GF00-NSEQ                            
NSKEP               AND      CFEXT      = :GF05-CFEXT                           
           END-EXEC                                                             
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'DEL GF05 NSOCIETE: ' GF00-NSOCIETE                        
                                     ' NDEPOT: ' GF00-NDEPOT                    
                                     ' NCODIC: ' GF00-NCODIC                    
                               ' DSOUHAITEE:  ' GF00-DSOUHAITEE                 
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF                                                               
           .                                                                    
      *                                                                         
LA1008 FIN-DELETE-RTGF05.   EXIT.                                               
      *---------------------------------------                                  
      ******************************************************************        
      * M�J Quantit� r�serv�e Entrep�t.                                         
      ******************************************************************        
       UPDATE-RTGS10 SECTION.                                                   
      *----------------------                                                   
           MOVE W-CC-V-QTY(W-CC-V-I) TO GS10-QSTOCKRES.                         
           MOVE W-CC-DATE-DU-JOUR    TO GS10-DMAJSTOCK.                         
           MOVE W-SPDATDB2-DSYST     TO GS10-DSYST.                             
           EXEC SQL UPDATE RVGS1000                                             
                       SET QSTOCKRES  = QSTOCKRES + :GS10-QSTOCKRES,            
                           DMAJSTOCK  = :GS10-DMAJSTOCK,                        
                           DSYST      = :GS10-DSYST                             
                     WHERE NSOCDEPOT  = :GS10-NSOCDEPOT                         
                       AND NDEPOT     = :GS10-NDEPOT                            
                       AND NCODIC     = :GS10-NCODIC                            
                       AND WSTOCKMASQ = 'N'                                     
                       AND NSSLIEU    = 'V'                                     
                       AND QSTOCK - QSTOCKRES >= :GS10-QSTOCKRES                
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'UPD GS10 NSOCDEPOT: ' GS10-NSOCDEPOT                      
                                     ' NDEPOT: ' GS10-NDEPOT                    
                                     ' NCODIC: ' GS10-NCODIC                    
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
NV2209 UPDATE-RTGF55 SECTION.                                                   
      *----------------------                                                   
  !                                                                             
  !        MOVE W-SPDATDB2-DSYST   TO GF55-DSYST.                               
  !                                                                             
v43r1      EXEC SQL UPDATE   RVGF5501                                           
  !            SET      QCDERES   = QCDERES + :WQTE,                            
  !                     DSYST     = :GF55-DSYST                                 
  !            WHERE    NSOCDEPOT = :GF55-NSOCDEPOT                             
  !            AND      NDEPOT    = :GF55-NDEPOT                                
  !            AND      NCODIC    = :GF55-NCODIC                                
  !            AND      DCDE      = :GF55-DCDE                                  
  !        END-EXEC.                                                            
  !                                                                             
  !        IF SQLCODE NOT = 0                                                   
  !           MOVE SPACES          TO W-MESSAGE                                 
  !           STRING 'UPD GF55 NSOCDEPOT: ' GF55-NSOCDEPOT                      
  !                                  ' NDEPOT: ' GF55-NDEPOT                    
  !                                  ' NCODIC: ' GF55-NCODIC                    
  !           DELIMITED BY SIZE INTO W-MESSAGE                                  
  !           PERFORM ERREUR-SQL                                                
  !        END-IF.                                                              
  !                                                                             
NV2209 FIN-UPDATE-RTGF55.    EXIT.                                              
      ******************************************************************        
      * Comme le num�ro d'ordre de la vente est attribu� par semaine, il        
      * faut r�cup�rer le + grand num�ro d'ordre de la semaine en cours,        
      * puis y ajouter un (par Soci�t�/Lieu, bien s�r).                         
      ******************************************************************        
       SELECT-MAX-GV10-NORDRE SECTION.                                          
      *-------------------------------                                          
           MOVE '00000'                TO GV10-NORDRE                           
           MOVE W-CC-DATE-DU-LUNDI TO GV10-DVENTE                               
           EXEC SQL SELECT MAX(NORDRE)                                          
                      INTO :GV10-NORDRE :GV10-NORDRE-F                          
MGD15 *               FROM RVGV1004                                             
MGD15                 FROM RVGV1005                                             
                     WHERE NSOCIETE = :GV10-NSOCIETE                            
                       AND NLIEU    = :GV10-NLIEU                               
                       AND DVENTE  >= :GV10-DVENTE                              
PORDRE                 AND NORDRE  >  '00000'                                   
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'MAX NORDRE GV10'                                          
                                   ' NSOCIETE: ' GV10-NSOCIETE                  
                                      ' NLIEU: ' GV10-NLIEU                     
                                     ' DVENTE: ' GV10-DVENTE                    
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           MOVE GV10-NORDRE TO W-GV10-NORDRE-PICX.                              
           ADD  1           TO W-GV10-NORDRE-PIC9.                              
      ******************************************************************        
      * Le Num�ro de Vente est constitu�, en pack� s'il vous pla�t              
      * et sur 13 positions de: JJSSSLLLOOOOO                                   
      * JJ    - jour de cr�ation                                                
      * SSS   - soci�t�                                                         
      * LLL   - lieu                                                            
      * OOOOO - num�ro d'ordre                                                  
      ******************************************************************        
       METS-EN-FORME-GV10-NVENTE SECTION.                                       
      *----------------------------------                                       
           MOVE W-CC-DATE-DU-JOUR(7:2)                                          
                                 TO W-GV10-NVENTE-PICX13.                       
           MOVE GV10-NSOCIETE    TO W-GV10-NVENTE-PICX13(3:).                   
           MOVE GV10-NLIEU       TO W-GV10-NVENTE-PICX13(6:).                   
           MOVE GV10-NORDRE      TO W-GV10-NVENTE-PICX13(9:).                   
           MOVE W-GV10-NVENTE-PIC913                                            
                                 TO W-GV10-NVENTE-PICS913.                      
      ******************************************************************        
      * Insertion ent�te de vente                                               
      ******************************************************************        
       INSERT-RTGV10-EC02 SECTION.                                              
      *---------------------------                                              
MGD15 *    EXEC SQL INSERT INTO RVGV1004                                        
MGD15      EXEC SQL INSERT INTO RVGV1005                                        
                   (NSOCIETE, NLIEU, NVENTE, NORDRE, NCLIENT,                   
                    DVENTE, DHVENTE, DMVENTE, PTTVENTE, PVERSE,                 
                    PCOMPT, PLIVR, PDIFFERE, PRFACT, CREMVTE,                   
                    PREMVTE, LCOMVTE1, LCOMVTE2, LCOMVTE3,                      
                    LCOMVTE4, DMODIFVTE, WFACTURE, WEXPORT,                     
                    WDETAXEC, WDETAXEHC, CORGORED, CMODPAIMTI,                  
                    LDESCRIPTIF1, LDESCRIPTIF2, DLIVRBL, NFOLIOBL,              
                    LAUTORM, NAUTORD, DSYST, DSTAT, DFACTURE,                   
                    CACID, NSOCMODIF, NLIEUMODIF, NSOCP, NLIEUP,                
MGD15 *             CDEV, CFCRED, NCREDI, NSEQNQ)                               
MGD15               CDEV, CFCRED, NCREDI, NSEQNQ, TYPVTE                        
kdo                 , nseqens)                                                  
            VALUES (:GV10-NSOCIETE, :GV10-NLIEU, :GV10-NVENTE,                  
                    :GV10-NORDRE, :GV10-NCLIENT, :GV10-DVENTE,                  
                    :GV10-DHVENTE, :GV10-DMVENTE, :GV10-PTTVENTE,               
                    :GV10-PVERSE, :GV10-PCOMPT, :GV10-PLIVR,                    
                    :GV10-PDIFFERE, :GV10-PRFACT, :GV10-CREMVTE,                
                    :GV10-PREMVTE, :GV10-LCOMVTE1, :GV10-LCOMVTE2,              
                    :GV10-LCOMVTE3, :GV10-LCOMVTE4,                             
                    :GV10-DMODIFVTE, :GV10-WFACTURE,                            
                    :GV10-WEXPORT, :GV10-WDETAXEC,                              
                    :GV10-WDETAXEHC, :GV10-CORGORED,                            
                    :GV10-CMODPAIMTI, :GV10-LDESCRIPTIF1,                       
                    :GV10-LDESCRIPTIF2, :GV10-DLIVRBL,                          
                    :GV10-NFOLIOBL, :GV10-LAUTORM, :GV10-NAUTORD,               
                    :GV10-DSYST, :GV10-DSTAT, :GV10-DFACTURE,                   
                    :GV10-CACID, :GV10-NSOCMODIF,                               
                    :GV10-NLIEUMODIF, :GV10-NSOCP, :GV10-NLIEUP,                
                    :GV10-CDEV, :GV10-CFCRED, :GV10-NCREDI,                     
MGD15 *             :GV10-NSEQNQ)                                               
MGD15               :GV10-NSEQNQ, :GV10-TYPVTE, :gv10-nseqens)                  
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS GV10 NSOCIETE: ' GV10-NSOCIETE                        
                                     ' NLIEU: ' GV10-NLIEU                      
                                    ' NORDRE: ' GV10-NORDRE                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
V6.5  *mz12INITIALIZE RVEC0203 RVEC0400.                                        
M25592     INITIALIZE RVEC0206 RVEC0400.                                        
           MOVE GV10-NSOCIETE     TO EC02-NSOCIETE.                             
           MOVE GV10-NLIEU        TO EC02-NLIEU.                                
           MOVE GV10-NVENTE       TO EC02-NVENTE.                               
           MOVE GV10-DVENTE       TO EC02-DVENTE.                               
           MOVE W-CC-E-NUM-CMD-DC TO EC02-NCDEWC                                
           MOVE W-CC-E-TP-CMD     TO EC02-WTPCMD.                               
M25592     MOVE WS-DATE-PREPA     TO EC02-DATPREPA.                             
M25592     MOVE WS-HEURE-PREPA    TO EC02-HEURPREPA.                            
           IF W-CC-R-U > 0                                                      
              MOVE W-CC-R-MD-PMENT(1) TO EC008-CTABLEG2                         
              PERFORM SELECT-EC008                                              
              IF EC008-WENLIGNE = 'O'                                           
                 MOVE W-CC-R-MD-PMENT(1) TO EC02-CPAYM                          
                 IF W-CC-R-DT-ESSEMT(1) > ' '                                   
                    MOVE 'P' TO EC02-WPAYM                                      
                 END-IF                                                         
                 MOVE W-CC-R-MT(1)       TO EC02-MTPAYM                         
              END-IF                                                            
M25592        IF EC008-WENLIGNE = 'M'                                           
   "             MOVE W-CC-R-MD-PMENT(1) TO EC02-CPAYM                          
   "             MOVE 'M'                TO EC02-WPAYM                          
   "             MOVE 'M'                TO EC02-WVALP                          
M25592        END-IF                                                            
           END-IF.                                                              
           MOVE LENGTH OF W-CC-E-CMENT-ACC TO EC02-LCADEAU-LEN.                 
           MOVE W-CC-E-CMENT-ACC TO EC02-LCADEAU-TEXT.                          
           PERFORM VARYING EC02-LCADEAU-LEN FROM EC02-LCADEAU-LEN BY -1         
             UNTIL (EC02-LCADEAU-TEXT(EC02-LCADEAU-LEN:) > ' ')                 
                OR (EC02-LCADEAU-LEN < 1)                                       
           END-PERFORM.                                                         
           MOVE W-SPDATDB2-DSYST  TO EC02-DSYST.                                
           IF W-CC-VENTE-CLICK-COLLECT                                          
              MOVE 'O' TO  EC02-WCC                                             
              MOVE W-CC-E-CC-PASSWORD TO EC02-CCPSWD                            
              MOVE W-CC-E-EVENT       TO EC02-CCEVENT                           
           END-IF.                                                              
           MOVE W-CC-CANAL            TO EC02-CANAL                             
1000M      MOVE w-cc-v-date-creat     TO EC02-DATECREAT                         
1000M      MOVE w-cc-v-heure-creat    TO EC02-HEURECREAT                        
M25592*    EXEC SQL INSERT INTO RVEC0205                                        
1000M      EXEC SQL INSERT INTO RVEC0206                                        
                            (NSOCIETE, NLIEU, NVENTE, DVENTE,                   
                             NCDEWC, WTPCMD, CPAYM, WPAYM,                      
                             MTPAYM, LCADEAU, DSYST,                            
M25592                       WVALP,                                             
M25592                       WCC, CCPSWD, CCEVENT,                              
M25592                       DATPREPA, HEURPREPA                                
                            ,canal                                              
1000M                       ,DATECREAT , HEURECREAT)                            
                    VALUES (:EC02-NSOCIETE, :EC02-NLIEU,                        
                            :EC02-NVENTE, :EC02-DVENTE,                         
                            :EC02-NCDEWC, :EC02-WTPCMD,                         
                            :EC02-CPAYM, :EC02-WPAYM,                           
                            :EC02-MTPAYM, :EC02-LCADEAU,                        
                            :EC02-DSYST,                                        
M25592                      :EC02-WVALP,                                        
M25592                      :EC02-WCC, :EC02-CCPSWD, :EC02-CCEVENT,             
M25592                      :EC02-DATPREPA, :EC02-HEURPREPA                     
                           ,:ec02-canal                                         
1000M                      ,:EC02-DATECREAT , :EC02-HEURECREAT)                 
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS EC02 NSOCIETE: ' GV10-NSOCIETE                        
                                     ' NLIEU: ' GV10-NLIEU                      
                                    ' NORDRE: ' GV10-NORDRE                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
V6.5       IF W-CC-VENTE-CLICK-COLLECT                                          
  "           MOVE SPACES TO EC04-CCCHOIX                                       
  "                          EC04-CEVENTC                                       
  "                          EC04-CCASIER                                       
  "                          EC04-CPSWDC                                        
  "                          EC04-CEMPLOYE                                      
  "           EXEC SQL INSERT INTO RVEC0400                                     
  "                         (NSOCIETE, NLIEU, NVENTE, DVENTE,                   
  "                          NCDEWC, CCCHOIX, CCPSWD, CCEVENT,                  
  "                          CEVENTC, CCASIER, CPSWDC,                          
  "                          CEMPLOYE, DSYST )                                  
  "                 VALUES (:EC02-NSOCIETE, :EC02-NLIEU,                        
  "                         :EC02-NVENTE,   :EC02-DVENTE,                       
  "                         :EC02-NCDEWC,   :EC04-CCCHOIX,                      
  "                         :EC02-CCPSWD,   :EC02-CCEVENT,                      
  "                         :EC04-CEVENTC,  :EC04-CCASIER,                      
  "                         :EC04-CPSWDC,   :EC04-CEMPLOYE,                     
  "                         :EC02-DSYST )                                       
  "           END-EXEC                                                          
  "                                                                             
  "           IF SQLCODE NOT = 0                                                
  "              MOVE SPACES TO W-MESSAGE                                       
  "              STRING 'INS EC04 NSOCIETE: ' GV10-NSOCIETE                     
  "                                  ' NLIEU: ' GV10-NLIEU                      
  "                                 ' NORDRE: ' GV10-NORDRE                     
  "              DELIMITED BY SIZE INTO W-MESSAGE                               
  "              PERFORM ERREUR-SQL                                             
  "           END-IF                                                            
V6.5       END-IF.                                                              
      ******************************************************************        
      * Insertion GV41: Mouchard Quotas                                         
      ******************************************************************        
       INSERT-RTGV41 SECTION.                                                   
      *----------------------                                                   
           INITIALIZE RVGV4100.                                                 
           MOVE GV10-NSOCIETE   TO GV41-NSOCIETE                                
           MOVE GV10-NLIEU      TO GV41-NLIEU                                   
           MOVE GV10-DVENTE     TO GV41-DVENTE                                  
           MOVE GV10-NORDRE     TO GV41-NORDRE                                  
           MOVE GQ03-CMODDEL    TO GV41-CMODDEL                                 
           MOVE GV10-DLIVRBL    TO GV41-DDELIV                                  
           MOVE GQ03-CZONLIV    TO GV41-CZONLIV                                 
           MOVE GQ03-CPLAGE     TO GV41-CPLAGE                                  
           MOVE GQ04-CEQUIP1    TO GV41-CEQUIP                                  
           IF W-CC-E-NOM-FACT > ' '                                             
              MOVE '2'          TO GV41-CTYPADR                                 
           ELSE                                                                 
              MOVE '1'          TO GV41-CTYPADR                                 
           END-IF.                                                              
      *    02  GV41-NSOCLIVR reste � blanc.                                     
      *    02  GV41-NDEPOT reste � blanc.                                       
           MOVE W-SPDATDB2-DSYST TO GV41-DSYST.                                 
           EXEC SQL INSERT INTO RVGV4100                                        
                   (NSOCIETE, NLIEU, DVENTE, NORDRE, CMODDEL,                   
                    DDELIV, CZONLIV, CPLAGE, CEQUIP, CTYPADR,                   
                    NSOCLIVR, NDEPOT, DSYST)                                    
            VALUES (:GV41-NSOCIETE, :GV41-NLIEU, :GV41-DVENTE,                  
                    :GV41-NORDRE, :GV41-CMODDEL, :GV41-DDELIV,                  
                    :GV41-CZONLIV, :GV41-CPLAGE, :GV41-CEQUIP,                  
                    :GV41-CTYPADR, :GV41-NSOCLIVR, :GV41-NDEPOT,                
                    :GV41-DSYST)                                                
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS GV41 NSOCIETE: ' GV41-NSOCIETE                        
                                     ' NLIEU: ' GV41-NLIEU                      
                                    ' DVENTE: ' GV41-DVENTE                     
                                    ' NORDRE: ' GV41-NORDRE                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Insertion de l'adresse client                                           
      ******************************************************************        
       INSERT-RTGV02 SECTION.                                                   
      *----------------------                                                   
           EXEC SQL INSERT INTO RVGV0202                                        
                   (NSOCIETE, NLIEU, NORDRE, NVENTE, WTYPEADR,                  
                    CTITRENOM, LNOM, LPRENOM, LBATIMENT,                        
                    LESCALIER, LETAGE, LPORTE, LCMPAD1, LCMPAD2,                
                    CVOIE, CTVOIE, LNOMVOIE, LCOMMUNE, CPOSTAL,                 
                    LBUREAU, TELDOM, TELBUR, LPOSTEBUR, LCOMLIV1,               
                    LCOMLIV2, WETRANGER, CZONLIV, CINSEE, WCONTRA,              
                    DSYST, CILOT, IDCLIENT, CPAYS, NGSM)                        
            VALUES (:GV02-NSOCIETE, :GV02-NLIEU, :GV02-NORDRE,                  
                    :GV02-NVENTE, :GV02-WTYPEADR, :GV02-CTITRENOM,              
                    :GV02-LNOM, :GV02-LPRENOM, :GV02-LBATIMENT,                 
                    :GV02-LESCALIER, :GV02-LETAGE, :GV02-LPORTE,                
                    :GV02-LCMPAD1, :GV02-LCMPAD2, :GV02-CVOIE,                  
                    :GV02-CTVOIE, :GV02-LNOMVOIE, :GV02-LCOMMUNE,               
                    :GV02-CPOSTAL, :GV02-LBUREAU, :GV02-TELDOM,                 
                    :GV02-TELBUR, :GV02-LPOSTEBUR, :GV02-LCOMLIV1,              
                    :GV02-LCOMLIV2, :GV02-WETRANGER,                            
                    :GV02-CZONLIV, :GV02-CINSEE, :GV02-WCONTRA,                 
                    :GV02-DSYST, :GV02-CILOT, :GV02-IDCLIENT,                   
                    :GV02-CPAYS, :GV02-NGSM)                                    
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS GV02 NSOCIETE: ' GV10-NSOCIETE                        
                                     ' NLIEU: ' GV10-NLIEU                      
                                    ' NORDRE: ' GV10-NORDRE                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Insertion de l'adresse (email)                                          
      ******************************************************************        
       INSERT-RTGV03 SECTION.                                                   
      *----------------------                                                   
      *    EXEC SQL INSERT INTO rvgv0303                                        
      *            (NSOCIETE, NLIEU, NORDRE, NVENTE, WTYPEADR,                  
      *             EMAIL, DSYST, CPORTE)                                       
      *     VALUES (:GV03-NSOCIETE, :GV03-NLIEU, :GV03-NORDRE,                  
      *             :GV03-NVENTE, :GV03-WTYPEADR, :GV03-EMAIL,                  
      *             :GV03-DSYST, :GV03-CPORTE)                                  
      *    END-EXEC.                                                            
           EXEC SQL INSERT INTO rvgv0303                                        
                   (NSOCIETE                                                    
                   , NLIEU                                                      
                   , NORDRE                                                     
                   , NVENTE                                                     
                   , WTYPEADR                                                   
                   , EMAIL                                                      
                   , DSYST                                                      
                   , CPORTE                                                     
                   , wasc)                                                      
            VALUES (:GV03-NSOCIETE                                              
                  , :GV03-NLIEU                                                 
                  , :GV03-NORDRE                                                
                  , :GV03-NVENTE                                                
                  , :GV03-WTYPEADR                                              
                  , :GV03-EMAIL                                                 
                  , :GV03-DSYST                                                 
                  , :GV03-CPORTE                                                
                  , :gv03-wasc)                                                 
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS GV03 NSOCIETE: ' GV03-NSOCIETE                        
                                     ' NLIEU: ' GV03-NLIEU                      
                                    ' NORDRE: ' GV03-NORDRE                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
V3.5  ******************************************************************        
V3.5  * INSERTION DES DONNEES REDEVANCE                                         
V3.5  ******************************************************************        
V3.5   INSERT-RTGV04 SECTION.                                                   
      *----------------------                                                   
      *    EXEC SQL INSERT INTO RVGV0401                                        
      *            (NSOCIETE, NLIEU, NVENTE, DCREATION,                         
      *             DATENAISS, CPNAISS, LIEUNAISS, DSYST                        
      *           , WACCES, WREPRISE, NBPRDREP)                                 
      *     VALUES (:GV04-NSOCIETE, :GV04-NLIEU, :GV04-NVENTE,                  
      *             :GV04-DCREATION, :GV04-DATENAISS, :GV04-CPNAISS,            
      *             :GV04-LIEUNAISS, :GV04-DSYST                                
      *           , :GV04-WACCES, :GV04-WREPRISE, :GV04-NBPRDREP)               
      *    END-EXEC.                                                            
           EXEC SQL INSERT INTO RVGV0401                                        
                   (NSOCIETE                                                    
                   , NLIEU                                                      
                   , NVENTE                                                     
                   , DCREATION                                                  
                   , DATENAISS                                                  
                   , CPNAISS                                                    
                   , LIEUNAISS                                                  
                   , DSYST                                                      
                   , WACCES                                                     
                   , WREPRISE                                                   
                   , NBPRDREP                                                   
                   , WASCUTIL                                                   
                   , wmarches                                                   
                   , wcol)                                                      
            VALUES (:GV04-NSOCIETE                                              
                  , :GV04-NLIEU                                                 
                  , :GV04-NVENTE                                                
                  , :GV04-DCREATION                                             
                  , :GV04-DATENAISS                                             
                  , :GV04-CPNAISS                                               
                  , :GV04-LIEUNAISS                                             
                  , :GV04-DSYST                                                 
                  , :GV04-WACCES                                                
                  , :GV04-WREPRISE                                              
                  , :GV04-NBPRDREP                                              
                  , :gv04-WASCUTIL                                              
                  , :gv04-wmarches                                              
                  , :gv04-wcol)                                                 
            END-EXEC.                                                           
V3.5                                                                            
V3.5       IF SQLCODE NOT = 0                                                   
V3.5          MOVE SPACES TO W-MESSAGE                                          
V3.5          STRING 'INS GV04 NSOCIETE: ' GV04-NSOCIETE                        
V3.5                                 ' NLIEU: ' GV04-NLIEU                      
V3.5                                ' NVENTE: ' GV04-NVENTE                     
V3.5          DELIMITED BY SIZE INTO W-MESSAGE                                  
V3.5          PERFORM ERREUR-SQL                                                
V3.5       END-IF.                                                              
V3.5                                                                            
      ******************************************************************        
      * Insertion de la ligne de vente                                          
      ******************************************************************        
       INSERT-RTGV11 SECTION.                                                   
      *----------------------                                                   
MGD15      MOVE  W-CC-TYPVTE      TO GV11-TYPVTE                                
MGD15 *    EXEC SQL INSERT INTO RVGV1105                                        
MGD15      EXEC SQL INSERT INTO RVGV1106                                        
                   (NSOCIETE, NLIEU, NVENTE, CTYPENREG, NCODICGRP,              
                    NCODIC, NSEQ, CMODDEL, DDELIV, NORDRE, CENREG,              
                    QVENDUE, PVUNIT, PVUNITF, PVTOTAL, CEQUIPE, NLIGNE,         
                    TAUXTVA, QCONDT, PRMP, WEMPORTE, CPLAGE, CPROTOUR,          
                    CADRTOUR, CPOSTAL, LCOMMENT, CVENDEUR, NSOCLIVR,            
                    NDEPOT, NAUTORM, WARTINEX, WEDITBL, WACOMMUTER,             
                    WCQERESF, NMUTATION, CTOURNEE, WTOPELIVRE, DCOMPTA,         
                    DCREATION, HCREATION, DANNULATION, NLIEUORIG,               
                    WINTMAJ, DSYST, DSTAT, CPRESTGRP, NSOCMODIF,                
                    NLIEUMODIF, DATENC, CDEV, WUNITR, LRCMMT, NSOCP,            
                    NLIEUP, NTRANS, NSEQFV, NSEQNQ, NSEQREF, NMODIF,            
                    NSOCORIG, DTOPE, NSOCLIVR1, NDEPOT1, NSOCGEST,              
MGD15 *             NLIEUGEST, NSOCDEPLIV, NLIEUDEPLIV)                         
MGD15               NLIEUGEST, NSOCDEPLIV, NLIEUDEPLIV, TYPVTE                  
                    , nseqens)                                                  
            VALUES (:GV11-NSOCIETE, :GV11-NLIEU, :GV11-NVENTE,                  
                    :GV11-CTYPENREG, :GV11-NCODICGRP, :GV11-NCODIC,             
                    :GV11-NSEQ, :GV11-CMODDEL, :GV11-DDELIV,                    
                    :GV11-NORDRE, :GV11-CENREG, :GV11-QVENDUE,                  
                    :GV11-PVUNIT, :GV11-PVUNITF, :GV11-PVTOTAL,                 
                    :GV11-CEQUIPE, :GV11-NLIGNE, :GV11-TAUXTVA,                 
                    :GV11-QCONDT, :GV11-PRMP, :GV11-WEMPORTE,                   
                    :GV11-CPLAGE, :GV11-CPROTOUR, :GV11-CADRTOUR,               
                    :GV11-CPOSTAL, :GV11-LCOMMENT, :GV11-CVENDEUR,              
                    :GV11-NSOCLIVR, :GV11-NDEPOT, :GV11-NAUTORM,                
                    :GV11-WARTINEX, :GV11-WEDITBL, :GV11-WACOMMUTER,            
                    :GV11-WCQERESF, :GV11-NMUTATION, :GV11-CTOURNEE,            
                    :GV11-WTOPELIVRE, :GV11-DCOMPTA, :GV11-DCREATION,           
                    :GV11-HCREATION, :GV11-DANNULATION, :GV11-NLIEUORIG,        
                    :GV11-WINTMAJ, :GV11-DSYST, :GV11-DSTAT,                    
                    :GV11-CPRESTGRP, :GV11-NSOCMODIF, :GV11-NLIEUMODIF,         
                    :GV11-DATENC, :GV11-CDEV, :GV11-WUNITR,                     
                    :GV11-LRCMMT, :GV11-NSOCP, :GV11-NLIEUP,                    
                    :GV11-NTRANS, :GV11-NSEQFV, :GV11-NSEQNQ,                   
                    :GV11-NSEQREF, :GV11-NMODIF, :GV11-NSOCORIG,                
                    :GV11-DTOPE, :GV11-NSOCLIVR1, :GV11-NDEPOT1,                
                    :GV11-NSOCGEST, :GV11-NLIEUGEST, :GV11-NSOCDEPLIV,          
MGD15 *             :GV11-NLIEUDEPLIV)                                          
MGD15               :GV11-NLIEUDEPLIV, :GV11-TYPVTE, :GV11-nseqens)             
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS GV11 NSOCIETE: ' GV11-NSOCIETE                        
                                     ' NLIEU: ' GV11-NLIEU                      
                                    ' NORDRE: ' GV11-NORDRE                     
                                    ' NLIGNE: ' GV11-NLIGNE                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Carect�ristiques Sp�cifiques                                            
      ******************************************************************        
       INSERT-RTGV15 SECTION.                                                   
      *----------------------                                                   
           INITIALIZE RVGV1500.                                                 
           MOVE GV11-NSOCIETE  TO GV15-NSOCIETE.                                
           MOVE GV11-NLIEU     TO GV15-NLIEU.                                   
           MOVE GV11-NORDRE    TO GV15-NORDRE.                                  
           MOVE GV11-NVENTE    TO GV15-NVENTE.                                  
           MOVE GV11-NCODIC    TO GV15-NCODIC.                                  
           MOVE GV11-NCODICGRP TO GV15-NCODICGRP.                               
           MOVE GV11-NSEQ      TO GV15-NSEQ.                                    
           PERFORM VARYING GV15-CVCARACTSPE1-F FROM 1 BY 1                      
             UNTIL (W-CC-V-CARSPEC(W-CC-V-I)(GV15-CVCARACTSPE1-F:1)             
                    = '=')                                                      
                OR (GV15-CVCARACTSPE1-F > 15)                                   
           END-PERFORM.                                                         
           IF GV15-CVCARACTSPE1-F <= 15                                         
              SUBTRACT 1 FROM GV15-CVCARACTSPE1-F                               
              MOVE W-CC-V-CARSPEC(W-CC-V-I)(1:GV15-CVCARACTSPE1-F)              
                TO GV15-CARACTSPE1                                              
              ADD 2 TO GV15-CVCARACTSPE1-F                                      
              IF GV15-CVCARACTSPE1-F <= 15                                      
                 MOVE W-CC-V-CARSPEC(W-CC-V-I)(GV15-CVCARACTSPE1-F:)            
                   TO GV15-CVCARACTSPE1                                         
                 MOVE GV11-DSYST     TO GV15-DSYST                              
                 EXEC SQL INSERT INTO RVGV1500                                  
                         (NSOCIETE, NLIEU, NORDRE, NVENTE, NCODIC,              
                          NCODICGRP, NSEQ, CARACTSPE1,                          
                          CVCARACTSPE1, CARACTSPE2, CVCARACTSPE2,               
                          CARACTSPE3, CVCARACTSPE3, CARACTSPE4,                 
                          CVCARACTSPE4, CARACTSPE5, CVCARACTSPE5,               
                          WANNULCAR, DSYST)                                     
                  VALUES (:GV15-NSOCIETE, :GV15-NLIEU,                          
                          :GV15-NORDRE, :GV15-NVENTE,                           
                          :GV15-NCODIC, :GV15-NCODICGRP,                        
                          :GV15-NSEQ, :GV15-CARACTSPE1,                         
                          :GV15-CVCARACTSPE1, :GV15-CARACTSPE2,                 
                          :GV15-CVCARACTSPE2, :GV15-CARACTSPE3,                 
                          :GV15-CVCARACTSPE3, :GV15-CARACTSPE4,                 
                          :GV15-CVCARACTSPE4, :GV15-CARACTSPE5,                 
                          :GV15-CVCARACTSPE5, :GV15-WANNULCAR,                  
                          :GV15-DSYST)                                          
                 END-EXEC                                                       
                 IF SQLCODE NOT = 0                                             
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING 'INS GV15 NSOCIETE: ' GV15-NSOCIETE                  
                                           ' NLIEU: ' GV15-NLIEU                
                                          ' NORDRE: ' GV15-NORDRE               
                                          ' NCODIC: ' GV15-NCODIC               
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ERREUR-SQL                                          
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      ******************************************************************        
      * Insertion GV21                                                          
      ******************************************************************        
       INSERT-RTGV21 SECTION.                                                   
      *-----------------------                                                  
           INITIALIZE RVGV2102.                                                 
           MOVE GV11-NSOCLIVR    TO GV21-NSOCLIVR.                              
           MOVE GV11-NDEPOT      TO GV21-NDEPOT.                                
           MOVE GV11-NSOCIETE    TO GV21-NSOCIETE.                              
           MOVE GV11-NLIEU       TO GV21-NLIEU.                                 
           MOVE GV11-NVENTE      TO GV21-NVENTE.                                
           MOVE GV11-NCODICGRP   TO GV21-NCODICGRP.                             
           MOVE GV11-NCODIC      TO GV21-NCODIC.                                
           MOVE GV11-NSEQ        TO GV21-NSEQ.                                  
           MOVE GV11-NMUTATION   TO GV21-NMUTATION.                             
           MOVE GV11-QVENDUE     TO GV21-QVENDUE.                               
           MOVE GV11-QVENDUE     TO GV21-QRESERV.                               
           MOVE GV11-WCQERESF    TO GV21-WCQERESF.                              
           MOVE GV11-DSYST       TO GV21-DSYST.                                 
           MOVE GV11-DDELIV      TO GV21-DDELIV.                                
           MOVE GV11-CMODDEL     TO GV21-CMODDEL.                               
           MOVE GV11-DCREATION   TO GV21-DCREATION.                             
           MOVE GV11-WACOMMUTER  TO GV21-WACOMMUTER.                            
           MOVE GV11-LCOMMENT    TO GV21-LCOMMENT.                              
           MOVE GV11-NSOCGEST    TO GV21-NSOCGEST.                              
           MOVE GV11-NLIEUGEST   TO GV21-NLIEUGEST.                             
           MOVE GV11-NSOCDEPLIV  TO GV21-NSOCDEPLIV.                            
           MOVE GV11-NLIEUDEPLIV TO GV21-NLIEUDEPLIV.                           
           EXEC SQL INSERT INTO RVGV2102                                        
                   (NSOCLIVR, NDEPOT, NSOCIETE, NLIEU, NVENTE,                  
                    NCODICGRP, NCODIC, NSEQ, NMUTATION, QVENDUE,                
                    QRESERV, WCQERESF, DSYST, DDELIV, CMODDEL,                  
                    DCREATION, WACOMMUTER, LCOMMENT, NSOCGEST,                  
                    NLIEUGEST, NSOCDEPLIV, NLIEUDEPLIV)                         
            VALUES (:GV21-NSOCLIVR, :GV21-NDEPOT, :GV21-NSOCIETE,               
                    :GV21-NLIEU, :GV21-NVENTE, :GV21-NCODICGRP,                 
                    :GV21-NCODIC, :GV21-NSEQ, :GV21-NMUTATION,                  
                    :GV21-QVENDUE, :GV21-QRESERV, :GV21-WCQERESF,               
                    :GV21-DSYST, :GV21-DDELIV, :GV21-CMODDEL,                   
                    :GV21-DCREATION, :GV21-WACOMMUTER, :GV21-LCOMMENT,          
                    :GV21-NSOCGEST, :GV21-NLIEUGEST, :GV21-NSOCDEPLIV,          
                    :GV21-NLIEUDEPLIV)                                          
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS GV21 NSOCIETE: ' GV21-NSOCIETE                        
                                     ' NLIEU: ' GV21-NLIEU                      
                                    ' NVENTE: ' GV21-NVENTE                     
                                    ' NCODIC: ' GV21-NCODIC                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Insertion GV22                                                          
      ******************************************************************        
       INSERT-RTGV22 SECTION.                                                   
      *----------------------                                                   
           MOVE GV11-NSOCIETE    TO GV22-NSOCIETE.                              
           MOVE GV11-NLIEU       TO GV22-NLIEU.                                 
           MOVE GV11-NORDRE      TO GV22-NORDRE.                                
           MOVE GV11-NVENTE      TO GV22-NVENTE.                                
           MOVE GV11-NCODICGRP   TO GV22-NCODICGRP.                             
           MOVE GV11-NCODIC      TO GV22-NCODIC.                                
           MOVE GV11-NSEQ        TO GV22-NSEQ.                                  
           MOVE GV11-NMUTATION   TO GV22-NMUTATION.                             
           MOVE GV11-QVENDUE     TO GV22-QVENDUE.                               
???        MOVE GV11-QVENDUE     TO GV22-QRESERV.                               
           MOVE GV11-NAUTORM     TO GV22-NAUTORM.                               
           MOVE GV11-WEMPORTE    TO GV22-WEMPORTE.                              
           MOVE GV11-DSYST       TO GV22-DSYST.                                 
           MOVE GV11-NSOCLIVR    TO GV22-NSOCLIVR.                              
           MOVE GV11-NDEPOT      TO GV22-NDEPOT.                                
           MOVE GV11-DDELIV      TO GV22-DDELIV.                                
           MOVE GV11-CMODDEL     TO GV22-CMODDEL.                               
LD0310     MOVE GV11-CEQUIPE     TO GV22-CEQUIPE.                               
LD0310     MOVE GV11-WCQERESF    TO GV22-WCQERESF.                              
LD0310     MOVE GV11-DCREATION   TO GV22-DCREATION.                             
LD0310*    EXEC SQL INSERT INTO RVGV2202                                        
LD0310     EXEC SQL INSERT INTO RVGV2203                                        
                            (NSOCIETE, NLIEU, NORDRE, NVENTE,                   
                             NCODICGRP, NCODIC, NSEQ, NMUTATION,                
                             QVENDUE, QRESERV, NAUTORM, WEMPORTE,               
                             DSYST, NSOCLIVR, NDEPOT, DDELIV,                   
LD0310*                      CMODDEL)                                           
LD0310                       CMODDEL, CEQUIPE, WCQERESF, DCREATION)             
                    VALUES (:GV22-NSOCIETE, :GV22-NLIEU,                        
                            :GV22-NORDRE, :GV22-NVENTE,                         
                            :GV22-NCODICGRP, :GV22-NCODIC,                      
                            :GV22-NSEQ, :GV22-NMUTATION,                        
                            :GV22-QVENDUE, :GV22-QRESERV,                       
                            :GV22-NAUTORM, :GV22-WEMPORTE,                      
                            :GV22-DSYST, :GV22-NSOCLIVR,                        
                            :GV22-NDEPOT, :GV22-DDELIV,                         
LD0310*                     :GV22-CMODDEL)                                      
LD0310                      :GV22-CMODDEL, :GV22-CEQUIPE,                       
LD0310                      :GV22-WCQERESF, :GV22-DCREATION)                    
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS GV22 NSOCIETE: ' GV22-NSOCIETE                        
                                     ' NLIEU: ' GV22-NLIEU                      
                                    ' NVENTE: ' GV22-NVENTE                     
                                    ' NCODIC: ' GV22-NCODIC                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Appel du module pour les exp�ditions                                    
      ******************************************************************        
LA2203*LINK-MGV65 SECTION.                                                      
      *-------------------                                                      
      *                                                                         
      *    MOVE GV11-NSOCIETE TO SUFIX-CTABLEG2.                                
      *                                                                         
      *    EXEC SQL SELECT WTABLEG INTO :SUFIX-WTABLEG                          
      *               FROM RVSUFIX                                              
      *              WHERE CTABLEG2 = :SUFIX-CTABLEG2                           
      *    END-EXEC.                                                            
      *                                                                         
      *    IF SQLCODE NOT = 0                                                   
      *       MOVE SPACES TO W-MESSAGE                                          
      *       STRING 'SLT SUFIX NSOCIETE: ' GV11-NSOCIETE                       
      *       DELIMITED BY SIZE INTO W-MESSAGE                                  
      *       PERFORM ERREUR-SQL                                                
      *    END-IF.                                                              
      *                                                                         
      *    MOVE SUFIX-SUFIX TO MGV65(6:).                                       
      *                                                                         
      *    INITIALIZE COMM-GV65-APPLI.                                          
V34R4 *    MOVE ' NNNNNNN' TO COMM-GV65-NEW-MODE                                
V34R4 *    SET COMM-GV65-NEW-APPEL TO TRUE                                      
V34R4 *    MOVE 'O' TO COMM-GV65-INTERNET                                       
V34R4 *    MOVE '+' TO COMM-GV65-MAJ-STOCK                                      
V34R4 *    MOVE 'O' TO COMM-GV65-CRE-GB05-TEMPO                                 
      *                                                                         
      *    MOVE GV11-NSOCIETE     TO COMM-GV65-NSOCIETE                         
      *    MOVE GV11-NLIEU        TO COMM-GV65-NLIEU.                           
V43R0 *    MOVE GV10-NORDRE       TO COMM-GV65-NORDRE                           
      *    MOVE GV11-NVENTE       TO COMM-GV65-NVENTE.                          
      *    MOVE GV11-NCODIC       TO COMM-GV65-NCODIC.                          
      *    MOVE GV11-NSEQNQ       TO COMM-GV65-NSEQNQ.                          
      *    MOVE GV11-DDELIV       TO COMM-GV65-DDELIV.                          
      *    MOVE W-CC-DATE-DU-JOUR TO COMM-GV65-SSAAMMJJ.                        
      *    MOVE GV11-QVENDUE      TO COMM-GV65-QTE.                             
LD0610*    MOVE GV11-WEMPORTE     TO COMM-GV65-WEMPORTE.                        
LD0810*    MOVE GV11-WCQERESF     TO COMM-GV65-WCQERESF.                        
V5.1.P*    MOVE W-CC-E-NUM-CMD-DC TO COMM-GV65-NCDEWC.                          
V5.1.P*    MOVE SPACE             TO COMM-GV65-FILIERE-IN.                      
      *    EXEC CICS LINK PROGRAM (MGV65)                                       
      *              COMMAREA (COMM-GV65-APPLI)                                 
LA2203*    END-EXEC.                                                            
LA2203 LINK-MEC64 SECTION.                                                      
      *-------------------                                                      
           INITIALIZE W-NOM-PROG                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         ASSIGN PROGRAM (W-NOM-PROG)                                     
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS ASSIGN PROGRAM (W-NOM-PROG)                                
           END-EXEC.                                                            
      *}                                                                        
           MOVE 'MEC64' TO W-NOM-PROG(1:5)                                      
           SET COMM-MEC64-ACTION-RESERVATION   TO TRUE                          
           MOVE 'O' TO COMM-MEC64-INTERNET                                      
           MOVE '+' TO COMM-MEC64-MAJ-STOCK                                     
           EXEC CICS LINK PROGRAM (W-NOM-PROG)                                  
                     COMMAREA (COMM-MEC64-APPLI)                                
LA2203     END-EXEC                                                             
           .                                                                    
LA2203 ALIM-TAB-MEC64         SECTION.                                          
      *-------------------                                                      
           ADD  1                 TO WN-I-CRE                                   
           MOVE W-CC-DATE-DU-JOUR TO COMM-MEC64-DJOUR                           
           MOVE GV11-NSOCIETE     TO COMM-MEC64-NSOCIETE                        
           MOVE GV11-NLIEU        TO COMM-MEC64-NLIEU                           
           MOVE GV10-NORDRE       TO COMM-MEC64-NORDRE                          
           MOVE GV11-NVENTE       TO COMM-MEC64-NVENTE                          
           MOVE W-CC-E-NUM-CMD-DC TO COMM-MEC64-NCDEWC                          
           MOVE SPACE             TO COMM-MEC64-FILIERE                         
           MOVE GV11-NCODIC       TO COMM-MEC64-TAB-NCODIC(WN-I-CRE)            
           MOVE GV11-NCODICGRP    TO COMM-MEC64-TAB-NCODICGRP(WN-I-CRE)         
           MOVE GV11-NSEQNQ       TO COMM-MEC64-TAB-NSEQNQ(WN-I-CRE)            
           MOVE GV11-NSEQ         TO COMM-MEC64-TAB-NSEQ(WN-I-CRE)              
           MOVE GV11-CTYPENREG    TO COMM-MEC64-TAB-CTYPENREG(WN-I-CRE)         
           MOVE GV11-DDELIV       TO COMM-MEC64-TAB-DDELIVO(WN-I-CRE)           
           MOVE GV11-QVENDUE      TO COMM-MEC64-TAB-QVENDUE(WN-I-CRE)           
           MOVE GV11-WEMPORTE     TO COMM-MEC64-TAB-WEMPORTE(WN-I-CRE)          
           MOVE GV11-WCQERESF     TO COMM-MEC64-TAB-WCQERESF(WN-I-CRE)          
LA2203     .                                                                    
      ******************************************************************        
      * Insertion de la ligne de vente Darty.Com                                
      ******************************************************************        
       INSERT-RTEC03 SECTION.                                                   
      *----------------------                                                   
LD0610*    INITIALIZE RVEC0301.                                                 
V41R0 *    INITIALIZE RVEC0302.                                                 
    |      INITIALIZE RVEC0304.                                                 
           MOVE GV10-NSOCIETE                    TO EC03-NSOCIETE.              
           MOVE GV10-NLIEU                       TO EC03-NLIEU.                 
           MOVE GV10-NVENTE                      TO EC03-NVENTE.                
           MOVE GV10-DVENTE                      TO EC03-DVENTE.                
           MOVE GV11-NSEQNQ                      TO EC03-NSEQNQ.                
LD0810     MOVE GV11-WEMPORTE                    TO EC03-WEMPORTE.              
LD0810     MOVE GV11-DDELIV                      TO EC03-DDELIV.                
           MOVE W-CC-V-NUM-COLIS(W-CC-V-I)       TO EC03-CCOLIS.                
           IF W-CC-V-DELIVERY(W-CC-V-I) > ' '                                   
              MOVE W-CC-V-DELIVERY(W-CC-V-I)     TO EC03-CCDDEL                 
              MOVE W-CC-V-DELIVERY(W-CC-V-I)(9:) TO EC03-CCHDEL                 
           END-IF.                                                              
           MOVE W-SPDATDB2-DSYST                 TO EC03-DSYST.                 
V41R0 *    IF W-CC-V-WEMPORTE(W-CC-V-I) = 'K'                                   
    | *       MOVE W-CC-V-RELAIS-ID (W-CC-V-I)   TO EC03-NRELAISID              
    | *    END-IF.                                                              
V6.5       IF  W-CC-V-RELAIS-ID (W-CC-V-I) > SPACES OR LOW-VALUE                
              SET WC-TSTENVOI-SUITE         TO TRUE                             
              PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                      
              UNTIL WC-TSTENVOI-FIN                                             
                 PERFORM LECTURE-TSTENVOI                                       
                 IF  WC-TSTENVOI-SUITE                                          
                 AND TSTENVOI-NSOCZP      = '00000'                             
                 AND TSTENVOI-CTRL(1:1)   = 'A'                                 
                 AND TSTENVOI-WPARAM(4:1) > SPACE                               
                 AND TSTENVOI-WPARAM(4:1) = WS-WEMPORTE                         
                 AND TSTENVOI-LIBELLE(5:1)= 'O'                                 
                    MOVE W-CC-V-RELAIS-ID (W-CC-V-I) TO EC03-NRELAISID          
                 END-IF                                                         
              END-PERFORM                                                       
V6.5       END-IF                                                               
           MOVE W-CC-V-DT-LIV-MAX (W-CC-V-I)                                    
                                     TO EC03-DDELIVMAX.                         
           EXEC SQL                                                             
                INSERT INTO RVEC0304                                            
                            (NSOCIETE, NLIEU, NVENTE, DVENTE,                   
                             NSEQNQ, CCOLIS, DEXP, DSYST, CCDDEL,               
V41R0 *                      CCHDEL, WEMPORTE, DDELIV)                          
    |                        CCHDEL, WEMPORTE, DDELIV, NRELAISID                
                             , DDELIVMAX)                                       
                    VALUES (:EC03-NSOCIETE, :EC03-NLIEU,                        
                            :EC03-NVENTE, :EC03-DVENTE,                         
                            :EC03-NSEQNQ, :EC03-CCOLIS,                         
                            :EC03-DEXP, :EC03-DSYST, :EC03-CCDDEL,              
                          :EC03-CCHDEL, :EC03-WEMPORTE, :EC03-DDELIV            
V41R0                     , :EC03-NRELAISID , :ec03-DDELIVMAX)                  
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'INS EC03 NSOCIETE: ' GV10-NSOCIETE                        
                                     ' NLIEU: ' GV10-NLIEU                      
                                    ' NORDRE: ' GV10-NORDRE                     
                                    ' NSEQNQ: ' GV11-NLIGNE                     
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Quelle est la Zone de Prix de ce Magasin?                               
      ******************************************************************        
       SELECT-RTGA10-NZONPRIX SECTION.                                          
      *-------------------------------                                          
           EXEC SQL SELECT NZONPRIX INTO :GA10-NZONPRIX                         
                      FROM RVGA1000                                             
                     WHERE NSOCIETE  =  :GV10-NSOCIETE                          
                       AND NLIEU     =  :GV10-NLIEU                             
           END-EXEC.                                                            
           IF SQLCODE NOT = 0  and 100                                          
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SEL GA10 SOC: ' GV10-NSOCIETE                             
                               ' LIEU: ' GV10-NLIEU                             
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Quel est le prix de ce produit pour cette zone de prix?                 
      ******************************************************************        
       SELECT-PRIX-GG20-OU-GA69 SECTION.                                        
           if W-CC-TYPVTE  not = 'O'                                            
              PERFORM SELECT-RTGG20                                             
           end-if                                                               
           IF SQLCODE = 100 or W-CC-TYPVTE  = 'O'                               
              PERFORM SELECT-RTGA69                                             
              IF SQLCODE = 100                                                  
                 MOVE W-CC-V-PX-VT(W-CC-V-I) TO GG20-PEXPTTC                    
              END-IF                                                            
           END-IF.                                                              
      ** prix exceptionnel....                                                  
       SELECT-RTGG20 SECTION.                                                   
      *----------------------                                                   
           EXEC SQL SELECT PEXPTTC INTO :GG20-PEXPTTC                           
                      FROM RVGG2000                                             
                     WHERE NCODIC    =  :GV11-NCODIC                            
                       AND NSOCIETE  =  :GV10-NSOCIETE                          
                       AND NLIEU     =  :GV10-NLIEU                             
                       AND DEFFET    >= :GV10-DVENTE                            
                       AND DFINEFFET <= :GV10-DVENTE                            
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SEL GG20'                                                 
                         ' CODIC: ' GV11-NCODIC                                 
                           ' SOC: ' GV10-NSOCIETE                               
                          ' LIEU: ' GV10-NLIEU                                  
                          ' DATE: ' GV10-DVENTE                                 
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ** prix standard pour cette zone de prix..................                
       SELECT-RTGA69 SECTION.                                                   
      *----------------------                                                   
           IF W-CC-TYPVTE  = 'O'                                                
              move '907'         to  GA69-NSOCIETE                              
              move '90'          to  GA69-NZONPRIX                              
           else                                                                 
              move GV10-NSOCIETE to  GA69-NSOCIETE                              
              move GA10-NZONPRIX to  GA69-NZONPRIX                              
           END-IF                                                               
           EXEC SQL SELECT PSTDTTC INTO :GG20-PEXPTTC                           
                      FROM RVGA6900                                             
                     WHERE NCODIC    =  :GV11-NCODIC                            
                       AND NSOCIETE  =  :GA69-NSOCIETE                          
                       AND NZONPRIX  =  :GA69-NZONPRIX                          
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT GA69 CODIC: ' GV11-NCODIC                          
                                  ' SOC: ' GV10-NSOCIETE                        
                                 ' ZONE: ' GA10-NZONPRIX                        
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Qques infos de la table articles n�cessaires � GV11                     
      ******************************************************************        
       SELECT-RTGA00 SECTION.                                                   
      *----------------------                                                   
           EXEC SQL SELECT CFAM, CMARQ, CTAUXTVA, QCONDT,                       
                           QHAUTEUR, QPROFONDEUR, QLARGEUR                      
                          , CROSSDOCKFOUR                                       
                      INTO :GA00-CFAM, :GA00-CMARQ,                             
                           :GA00-CTAUXTVA, :GA00-QCONDT,                        
                           :GA00-QHAUTEUR, :GA00-QPROFONDEUR,                   
                           :GA00-QLARGEUR                                       
                          ,:GA00-CROSSDOCKFOUR                                  
                      FROM RVGA0015                                             
                     WHERE NCODIC = :GA00-NCODIC                                
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'SELECT GA00:' TO W-MESSAGE                                  
              MOVE GA00-NCODIC    TO W-MESSAGE(14:)                             
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 0                                                       
              IF GA00-CTAUXTVA NOT = TXTVA-CTABLEG2                             
                 MOVE GA00-CTAUXTVA TO TXTVA-CTABLEG2                           
                 EXEC SQL SELECT WTABLEG INTO :TXTVA-WTABLEG                    
                            FROM RVGA0100                                       
                           WHERE CTABLEG1 = 'TXTVA'                             
                             AND CTABLEG2 = :TXTVA-CTABLEG2                     
                 END-EXEC                                                       
                 IF SQLCODE NOT = 0                                             
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING 'SELECT TXTVA: ' GA00-CTAUXTVA                       
                            ' POUR CODIC: ' GA00-NCODIC                         
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ERREUR-SQL                                          
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      ******************************************************************        
      * Premi�re mut non d�stock�e pour la s�lection article...                 
      ******************************************************************        
       SELECT-RTGB05 SECTION.                                                   
      *----------------------                                                   
           MOVE LIVRG-LDEPLIV      TO GB05-NSOCIETE.                            
           MOVE LIVRG-LDEPLIV(4:3) TO GB05-NLIEU.                               
           MOVE W-CC-DATE-DU-JOUR  TO GB05-DDESTOCK.                            
           MOVE GV10-DLIVRBL       TO GFSAMJ-0.                                 
           MOVE '5' TO GFDATA.                                                  
           PERFORM FAIS-UN-LINK-A-TETDATC.                                      
           PERFORM SELECT-GVDEL.                                                
           IF GVDEL-QDELAI-N NOT = 0                                            
              COMPUTE GFQNT0 = GFQNT0 - GVDEL-QDELAI-N                          
              MOVE '3' TO GFDATA                                                
              PERFORM FAIS-UN-LINK-A-TETDATC                                    
           END-IF.                                                              
           MOVE GFSAMJ-0           TO GB05-DMUTATION.                           
V5.1.P* > CHARGEMENT DES SELECTIONS ARTICLES SELON                              
      *   FAMILLE /RELATION LIEUX / MODE DE D�LIVRANCE                          
           PERFORM CHARGEMENT-SEL-ARTICLE.                                      
           set mutation-non-trouvee to true                                     
           PERFORM VARYING W1-I FROM 1 BY 1                                     
             UNTIL W1-I > W1-CPT                                                
                OR mutation-trouvee                                             
               IF W1-WACTIF (W1-I) = 'O'                                        
                  MOVE W1-SELART (w1-i) TO GB05-CSELART                         
                  PERFORM SELECT-RTGB05-AVEC-QUOTA                              
               END-IF                                                           
           END-PERFORM.                                                         
           IF mutation-non-trouvee                                              
               PERFORM VARYING W1-I FROM 1 BY 1                                 
                 UNTIL W1-I > W1-CPT                                            
                    OR mutation-trouvee                                         
                   IF W1-WACTIF (W1-I) = 'O'                                    
                      MOVE W1-SELART (w1-i) TO GB05-CSELART                     
                      PERFORM SELECT-RTGB05-SANS-QUOTA                          
                   END-IF                                                       
               END-PERFORM                                                      
           END-IF.                                                              
      ******************************************************************        
      * On r�cup�re la premi�re Mut avec quota...                               
      ******************************************************************        
       SELECT-RTGB05-AVEC-QUOTA SECTION.                                        
      *---------------------------------                                        
           EXEC SQL SELECT NMUTATION, DMUTATION, QNBM3QUOTA, QNBPQUOTA,         
                           QVOLUME, QNBPIECES                                   
                           , ddestock                                           
                      INTO :GB05-NMUTATION, :GB05-DMUTATION,                    
                           :GB05-QNBM3QUOTA, :GB05-QNBPQUOTA,                   
                           :GB05-QVOLUME, :GB05-QNBPIECES                       
                           , :gb05-ddestock                                     
                      FROM RVGB0500 GB05                                        
                     WHERE GB05.QNBLLANCE  = 0                                  
      *{Post-translation Correct-Sign-not
      *                 AND GB05.WVAL      ^=  'O'                               
                       AND GB05.WVAL      <> 'O'
      *}
                       AND GB05.NSOCENTR   = :GS10-NSOCDEPOT                    
                       AND GB05.NDEPOT     = :GS10-NDEPOT                       
                       AND GB05.NSOCIETE   = :GB05-NSOCIETE                     
                       AND GB05.NLIEU      = :GB05-NLIEU                        
                       AND GB05.CSELART    = :GB05-CSELART                      
                       AND DDESTOCK       >= :GB05-DDESTOCK                     
                       AND DMUTATION =                                          
                          (SELECT MAX (DMUTATION) FROM RVGB0500                 
                            WHERE NSOCENTR  = GB05.NSOCENTR                     
                              AND NDEPOT    = GB05.NDEPOT                       
                              AND NSOCIETE  = GB05.NSOCIETE                     
                              AND NLIEU     = GB05.NLIEU                        
                              AND CSELART   = GB05.CSELART                      
                              AND QNBLLANCE = 0                                 
      *{Post-translation Correct-Sign-not
      *                        AND WVAL     ^= 'O'                               
                              AND WVAL     <> 'O'
      *}
                              AND DMUTATION <= :GB05-DMUTATION                  
                              AND (   (QNBM3QUOTA = 0)                          
                                  OR (QNBM3QUOTA > 0                            
                                   AND (QVOLUME/1000000) <= QNBM3QUOTA))        
                              AND (   (QNBPQUOTA = 0)                           
                                  OR (QNBPQUOTA > 0                             
                                    AND QNBPIECES <= QNBPQUOTA) )               
v5.1.p                              )                                           
                       AND WPROPERMIS =                                         
                          (SELECT MIN (WPROPERMIS) FROM RVGB0500                
                            WHERE NSOCENTR  = GB05.NSOCENTR                     
                              AND NDEPOT    = GB05.NDEPOT                       
                              AND NSOCIETE  = GB05.NSOCIETE                     
                              AND NLIEU     = GB05.NLIEU                        
                              AND CSELART   = GB05.CSELART                      
                              AND QNBLLANCE = 0                                 
      *{Post-translation Correct-Sign-not
      *                        AND WVAL     ^= 'O'                               
                              AND WVAL     <> 'O'
      *}
                              AND DMUTATION <= :GB05-DMUTATION                  
                              AND (   (QNBM3QUOTA = 0)                          
                                  OR (QNBM3QUOTA > 0                            
                                   AND (QVOLUME/1000000) <= QNBM3QUOTA))        
                              AND (   (QNBPQUOTA = 0)                           
                                  OR (QNBPQUOTA > 0                             
                                    AND QNBPIECES <= QNBPQUOTA) ))              
                       AND (   (QNBM3QUOTA = 0)                                 
                            OR (QNBM3QUOTA > 0                                  
                                AND (QVOLUME/1000000) <= QNBM3QUOTA) )          
                       AND (   (QNBPQUOTA = 0)                                  
                            OR (QNBPQUOTA > 0                                   
                                AND QNBPIECES <= QNBPQUOTA)                     
v5.1p                      )                                                    
                       order by gb05.ddestock, gb05.nmutation                   
                       fetch first 1 row  only                                  
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100 AND -811                                  
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT GB05Q NSOCDEPOT: ' GS10-NSOCDEPOT                  
                                   ' NDEPOT: ' GS10-NDEPOT                      
                                 ' NSOCIETE: ' GB05-NSOCIETE                    
                                    ' NLIEU: ' GB05-NLIEU                       
                               ' POUR CODIC: ' GA00-NCODIC                      
                                     ' CFAM: ' GA00-CFAM                        
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           else                                                                 
              if sqlcode = 0  or -811                                           
                 set mutation-trouvee to true                                   
              end-if                                                            
           END-IF.                                                              
      ******************************************************************        
      * Sinon on r�cup�re la premi�re Mut...                                    
      ******************************************************************        
V5.1.P SELECT-RTGB05-SANS-QUOTA SECTION.                                        
      *---------------------------------                                        
           EXEC SQL SELECT NMUTATION, DMUTATION, QNBM3QUOTA, QNBPQUOTA,         
                           QVOLUME, QNBPIECES                                   
                           , ddestock                                           
                      INTO :GB05-NMUTATION, :GB05-DMUTATION,                    
                           :GB05-QNBM3QUOTA, :GB05-QNBPQUOTA,                   
                           :GB05-QVOLUME, :GB05-QNBPIECES                       
                          ,:gb05-ddestock                                       
                      FROM RVGB0500 GB05                                        
                     WHERE GB05.QNBLLANCE  = 0                                  
      *{Post-translation Correct-Sign-not
      *                 AND GB05.WVAL      ^=  'O'                               
                       AND GB05.WVAL      <> 'O'
      *}
                       AND GB05.NSOCENTR   = :GS10-NSOCDEPOT                    
                       AND GB05.NDEPOT     = :GS10-NDEPOT                       
                       AND GB05.NSOCIETE   = :GB05-NSOCIETE                     
                       AND GB05.NLIEU      = :GB05-NLIEU                        
                       AND DDESTOCK       >= :GB05-DDESTOCK                     
                       AND GB05.CSELART    = :GB05-CSELART                      
                       AND DMUTATION =                                          
                          (SELECT MAX (DMUTATION) FROM RVGB0500                 
                            WHERE NSOCENTR  = GB05.NSOCENTR                     
                              AND NDEPOT    = GB05.NDEPOT                       
                              AND NSOCIETE  = GB05.NSOCIETE                     
                              AND NLIEU     = GB05.NLIEU                        
                              AND CSELART   = GB05.CSELART                      
                              AND QNBLLANCE = 0                                 
      *{Post-translation Correct-Sign-not
      *                        AND WVAL     ^= 'O'                               
                              AND WVAL     <> 'O'
      *}
                              AND DMUTATION <= :GB05-DMUTATION                  
                          )                                                     
                       AND WPROPERMIS =                                         
                          (SELECT MIN (WPROPERMIS) FROM RVGB0500                
                            WHERE NSOCENTR  = GB05.NSOCENTR                     
                              AND NDEPOT    = GB05.NDEPOT                       
                              AND NSOCIETE  = GB05.NSOCIETE                     
                              AND NLIEU     = GB05.NLIEU                        
                              AND CSELART   = GB05.CSELART                      
                              AND QNBLLANCE = 0                                 
      *{Post-translation Correct-Sign-not
      *                        AND WVAL     ^= 'O'                               
                              AND WVAL     <> 'O'
      *}
                              AND DMUTATION <= :GB05-DMUTATION                  
                          )                                                     
                       order by gb05.nmutation                                  
                       fetch first 1 row  only                                  
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100 AND -811                                  
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT GB05S NSOCDEPOT: ' GS10-NSOCDEPOT                  
                                   ' NDEPOT: ' GS10-NDEPOT                      
                                 ' NSOCIETE: ' GB05-NSOCIETE                    
                                    ' NLIEU: ' GB05-NLIEU                       
                               ' POUR CODIC: ' GA00-NCODIC                      
                                     ' CFAM: ' GA00-CFAM                        
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           else                                                                 
              if sqlcode = 0 or -811                                            
                 set mutation-trouvee to true                                   
              end-if                                                            
           END-IF.                                                              
V5.1.P*SELECT-RTGB05-AVEC-QUOTA SECTION.                                        
      *---------------------------------                                        
      *                                                                         
      *    EXEC SQL SELECT NMUTATION, DMUTATION, QNBM3QUOTA, QNBPQUOTA,         
      *                    QVOLUME, QNBPIECES                                   
      *               INTO :GB05-NMUTATION, :GB05-DMUTATION,                    
      *                    :GB05-QNBM3QUOTA, :GB05-QNBPQUOTA,                   
      *                    :GB05-QVOLUME, :GB05-QNBPIECES                       
      *               FROM RVGB0500 GB05, RVGA0100 LSELA                        
      *              WHERE LSELA.CTABLEG1  = 'LSELA'                            
      *                AND GB05.QNBLLANCE  = 0                                  
      *                AND GB05.WVAL       <> 'O'                               
      *                AND GB05.NSOCENTR   = :GS10-NSOCDEPOT                    
      *                AND GB05.NDEPOT     = :GS10-NDEPOT                       
      *                AND GB05.NSOCIETE   = :GB05-NSOCIETE                     
      *                AND GB05.NLIEU      = :GB05-NLIEU                        
      *                AND SUBSTR(LSELA.CTABLEG2, 1 , 3)                        
      *                        = :GS10-NSOCDEPOT                                
      *                AND SUBSTR(LSELA.CTABLEG2, 4 , 5)                        
      *                        = GB05.CSELART                                   
      *                AND SUBSTR(LSELA.CTABLEG2, 9 , 5)                        
      *                        = :GA00-CFAM                                     
      *                AND DDESTOCK      >= :GB05-DDESTOCK                      
J0J1  *                AND ((LHEURLIMIT = :W-TYPE-MUTATION                      
J0J1  *                AND (:W-TYPE-MUTATION = 'LD0'                            
J0J1  *                      OR :W-TYPE-MUTATION = 'LD1'))                      
J0J1  *                      OR (LHEURLIMIT <>    'LD0'                         
      *                       AND LHEURLIMIT <>    'LD1'                        
J0J1  *                      AND :W-TYPE-MUTATION = 'LD2'))                     
      *                AND DMUTATION =                                          
      *                   (SELECT MAX (DMUTATION) FROM RVGB0500                 
      *                     WHERE NSOCENTR  = GB05.NSOCENTR                     
      *                       AND NDEPOT    = GB05.NDEPOT                       
      *                       AND NSOCIETE  = GB05.NSOCIETE                     
      *                       AND NLIEU     = GB05.NLIEU                        
      *                       AND CSELART   = GB05.CSELART                      
      *                       AND QNBLLANCE = 0                                 
      *                       AND WVAL     ^= 'O'                               
      *                       AND DMUTATION <= :GB05-DMUTATION                  
      *                       AND (   (QNBM3QUOTA = 0)                          
      *                           OR (QNBM3QUOTA > 0                            
      *                            AND (QVOLUME/1000000) < QNBM3QUOTA) )        
      *                       AND (   (QNBPQUOTA = 0)                           
      *                           OR (QNBPQUOTA > 0                             
      *                             AND QNBPIECES < QNBPQUOTA) )                
V5.1.P*                             )                                           
J0J1  *               AND ((LHEURLIMIT = :W-TYPE-MUTATION                       
J0J1  *                         AND (:W-TYPE-MUTATION = 'LD0'                   
J0J1  *                              OR :W-TYPE-MUTATION = 'LD1'))              
J0J1  *                      OR (LHEURLIMIT <>    'LD0'                         
J0J1  *                      AND LHEURLIMIT <>    'LD1'                         
J0J1  *                      AND :W-TYPE-MUTATION = 'LD2')))                    
      *                AND WPROPERMIS =                                         
      *                   (SELECT MIN (WPROPERMIS) FROM RVGB0500                
      *                     WHERE NSOCENTR  = GB05.NSOCENTR                     
      *                       AND NDEPOT    = GB05.NDEPOT                       
      *                       AND NSOCIETE  = GB05.NSOCIETE                     
      *                       AND NLIEU     = GB05.NLIEU                        
      *                       AND CSELART   = GB05.CSELART                      
      *                       AND QNBLLANCE = 0                                 
      *                       AND WVAL     ^= 'O'                               
      *                       AND DMUTATION <= :GB05-DMUTATION                  
      *                       AND (   (QNBM3QUOTA = 0)                          
      *                           OR (QNBM3QUOTA > 0                            
      *                            AND (QVOLUME/1000000) < QNBM3QUOTA) )        
      *                       AND (   (QNBPQUOTA = 0)                           
      *                           OR (QNBPQUOTA > 0                             
      *                             AND QNBPIECES < QNBPQUOTA) ))               
      *                AND (   (QNBM3QUOTA = 0)                                 
      *                     OR (QNBM3QUOTA > 0                                  
      *                         AND (QVOLUME/1000000) < QNBM3QUOTA) )           
      *                AND (   (QNBPQUOTA = 0)                                  
      *                     OR (QNBPQUOTA > 0                                   
      *                         AND QNBPIECES < QNBPQUOTA)                      
V5.1P *                    )                                                    
J0J1  *                AND ((LHEURLIMIT = :W-TYPE-MUTATION                      
J0J1  *                       AND (:W-TYPE-MUTATION = 'LD0'                     
J0J1  *                             OR :W-TYPE-MUTATION = 'LD1'))               
J0J1  *                    OR (LHEURLIMIT <>    'LD0'                           
J0J1  *                    AND LHEURLIMIT <>    'LD1'                           
J0J1  *                    AND :W-TYPE-MUTATION = 'LD2')))                      
      *    END-EXEC.                                                            
      *                                                                         
      *    IF SQLCODE NOT = 0 AND 100 AND -811                                  
      *       MOVE SPACES TO W-MESSAGE                                          
      *       STRING 'SELECT GB05Q NSOCDEPOT: ' GS10-NSOCDEPOT                  
      *                            ' NDEPOT: ' GS10-NDEPOT                      
      *                          ' NSOCIETE: ' GB05-NSOCIETE                    
      *                             ' NLIEU: ' GB05-NLIEU                       
      *                        ' POUR CODIC: ' GA00-NCODIC                      
      *                              ' CFAM: ' GA00-CFAM                        
      *       DELIMITED BY SIZE INTO W-MESSAGE                                  
      *       PERFORM ERREUR-SQL                                                
      *    END-IF.                                                              
      *                                                                         
      *                                                                         
      ******************************************************************        
      * Sinon on r�cup�re la premi�re Mut...                                    
      ******************************************************************        
      *SELECT-RTGB05-SANS-QUOTA SECTION.                                        
      *---------------------------------                                        
      *                                                                         
      *    EXEC SQL SELECT NMUTATION, DMUTATION, QNBM3QUOTA, QNBPQUOTA,         
      *                    QVOLUME, QNBPIECES                                   
      *               INTO :GB05-NMUTATION, :GB05-DMUTATION,                    
      *                    :GB05-QNBM3QUOTA, :GB05-QNBPQUOTA,                   
      *                    :GB05-QVOLUME, :GB05-QNBPIECES                       
      *               FROM RVGB0500 GB05, RVGA0100 LSELA                        
      *              WHERE LSELA.CTABLEG1  = 'LSELA'                            
      *                AND GB05.QNBLLANCE  = 0                                  
      *                AND GB05.WVAL       <> 'O'                               
      *                AND GB05.NSOCENTR   = :GS10-NSOCDEPOT                    
      *                AND GB05.NDEPOT     = :GS10-NDEPOT                       
      *                AND GB05.NSOCIETE   = :GB05-NSOCIETE                     
      *                AND GB05.NLIEU      = :GB05-NLIEU                        
      *                AND SUBSTR(LSELA.CTABLEG2, 1 , 3)                        
      *                        = :GS10-NSOCDEPOT                                
      *                AND SUBSTR(LSELA.CTABLEG2, 4 , 5)                        
      *                        = GB05.CSELART                                   
      *                AND SUBSTR(LSELA.CTABLEG2, 9 , 5)                        
      *                        = :GA00-CFAM                                     
      *                AND DDESTOCK      >= :GB05-DDESTOCK                      
J0J1  *                AND ((LHEURLIMIT = :W-TYPE-MUTATION                      
J0J1  *                     AND (:W-TYPE-MUTATION = 'LD0'                       
J0J1  *                         OR :W-TYPE-MUTATION = 'LD1'))                   
J0J1  *                    OR (LHEURLIMIT <>    'LD0'                           
J0J1  *                     AND LHEURLIMIT <>    'LD1'                          
J0J1  *                    AND :W-TYPE-MUTATION = 'LD2'))                       
      *                AND DMUTATION =                                          
      *                   (SELECT MAX (DMUTATION) FROM RVGB0500                 
      *                     WHERE NSOCENTR  = GB05.NSOCENTR                     
      *                       AND NDEPOT    = GB05.NDEPOT                       
      *                       AND NSOCIETE  = GB05.NSOCIETE                     
      *                       AND NLIEU     = GB05.NLIEU                        
      *                       AND CSELART   = GB05.CSELART                      
      *                       AND QNBLLANCE = 0                                 
      *                       AND WVAL     ^= 'O'                               
      *                       AND DMUTATION <= :GB05-DMUTATION                  
      *                   )                                                     
J0J1  *                       AND ((LHEURLIMIT = :W-TYPE-MUTATION               
J0J1  *                            AND (:W-TYPE-MUTATION = 'LD0'                
J0J1  *                             OR :W-TYPE-MUTATION = 'LD1'))               
J0J1  *                             OR (LHEURLIMIT <>    'LD0'                  
J0J1  *                             AND LHEURLIMIT <>    'LD1'                  
J0J1  *                             AND :W-TYPE-MUTATION = 'LD2')))             
      *                AND WPROPERMIS =                                         
      *                   (SELECT MIN (WPROPERMIS) FROM RVGB0500                
      *                     WHERE NSOCENTR  = GB05.NSOCENTR                     
      *                       AND NDEPOT    = GB05.NDEPOT                       
      *                       AND NSOCIETE  = GB05.NSOCIETE                     
      *                       AND NLIEU     = GB05.NLIEU                        
      *                       AND CSELART   = GB05.CSELART                      
      *                       AND QNBLLANCE = 0                                 
      *                       AND WVAL     ^= 'O'                               
      *                       AND DMUTATION <= :GB05-DMUTATION                  
      *                   )                                                     
J0J1  *                       AND ((LHEURLIMIT = :W-TYPE-MUTATION               
J0J1  *                           AND (:W-TYPE-MUTATION = 'LD0'                 
J0J1  *                             OR :W-TYPE-MUTATION = 'LD1'))               
J0J1  *                             OR (LHEURLIMIT <>    'LD0'                  
J0J1  *                             AND LHEURLIMIT <>    'LD1'                  
J0J1  *                             AND :W-TYPE-MUTATION = 'LD2')))             
      *    END-EXEC.                                                            
      *                                                                         
      *    IF SQLCODE NOT = 0 AND 100 AND -811                                  
      *       MOVE SPACES TO W-MESSAGE                                          
      *       STRING 'SELECT GB05S NSOCDEPOT: ' GS10-NSOCDEPOT                  
      *                            ' NDEPOT: ' GS10-NDEPOT                      
      *                          ' NSOCIETE: ' GB05-NSOCIETE                    
      *                             ' NLIEU: ' GB05-NLIEU                       
      *                        ' POUR CODIC: ' GA00-NCODIC                      
      *                              ' CFAM: ' GA00-CFAM                        
      *       DELIMITED BY SIZE INTO W-MESSAGE                                  
      *       PERFORM ERREUR-SQL                                                
      *    END-IF.                                                              
      ******************************************************************        
      * Update Nombre de Pi�ces et Volume de la Mut...                          
      ******************************************************************        
       UPDATE-RTGB05 SECTION.                                                   
      *----------------------                                                   
           MOVE W-CC-V-QTY(W-CC-V-I) TO GB05-QNBPIECES.                         
           COMPUTE GB05-QVOLUME ROUNDED =                                       
                           (GA00-QHAUTEUR * GA00-QPROFONDEUR *                  
                            GA00-QLARGEUR * GB05-QNBPIECES).                    
           MOVE W-SPDATDB2-DSYST    TO GB05-DSYST.                              
           EXEC SQL UPDATE RVGB0500                                             
                       SET QNBPIECES = QNBPIECES + :GB05-QNBPIECES,             
                           QVOLUME   = QVOLUME + :GB05-QVOLUME,                 
                           DSYST     = :GB05-DSYST                              
                     WHERE NMUTATION = :GB05-NMUTATION                          
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'UPDATE GB05:' TO W-MESSAGE                                  
              MOVE GB05-NMUTATION TO W-MESSAGE(14:)                             
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
NV0809*--------------------------------------                                   
  !    DELETE-RTGB15                  SECTION.                                  
  !   *---------------------------------------                                  
  !                                                                             
  !        EXEC SQL DELETE                                                      
  !                 FROM     RVGB1502                                           
  !                 WHERE    NMUTATION = :GB15-NMUTATION                        
  !                 AND      NCODIC    = :GB15-NCODIC                           
  !        END-EXEC.                                                            
  !                                                                             
  !        IF SQLCODE NOT = 0 AND 100                                           
  !           MOVE SPACES TO W-MESSAGE                                          
  !           STRING 'DELETE GB15  QDEMANDEE '                                  
  !                         'POUR NMUTATION: ' GB15-NMUTATION                   
  !                                 ' CODIC: ' GB15-NCODIC                      
  !           DELIMITED BY SIZE INTO W-MESSAGE                                  
  !           PERFORM ERREUR-SQL                                                
  !        END-IF.                                                              
  !                                                                             
NV0809 FIN-DELETE-RTGB15.    EXIT.                                              
NV0809 UPDATE-RTGB05-NBLIG       SECTION.                                       
  !   *-------------------------------                                          
  !                                                                             
  !        MOVE W-SPDATDB2-DSYST    TO GB05-DSYST.                              
  !        COMPUTE WQTE-TEMP     =  WQTE * -1                                   
  !        COMPUTE WQVOLUME      =  GB05-QVOLUME * -1                           
  !                                                                             
  !        EXEC SQL UPDATE RVGB0500                                             
  !                    SET QNBLIGNES = QNBLIGNES + :WQNBLIGNES,                 
  !                        QVOLUME   = QVOLUME   + :WQVOLUME  ,                 
  !                        QNBPIECES = QNBPIECES + :WQTE-TEMP ,                 
  !                        DSYST     = :GB05-DSYST                              
  !                  WHERE NMUTATION = :GB15-NMUTATION                          
  !        END-EXEC.                                                            
  !                                                                             
  !        IF SQLCODE NOT = 0 AND 100                                           
  !           MOVE SPACES TO W-MESSAGE                                          
  !           STRING 'UPDATE GB05  QNBLIGNES '                                  
  !                         'POUR NMUTATION: ' GB15-NMUTATION                   
  !           DELIMITED BY SIZE INTO W-MESSAGE                                  
  !           PERFORM ERREUR-SQL                                                
  !        END-IF.                                                              
  !                                                                             
NV0809 FIN-UPDATE-RTGB05-NBLIG.  EXIT.                                          
NV2008 UPDATE-RTGB15                  SECTION.                                  
  !   *---------------------------------------                                  
  !                                                                             
  !        MOVE W-SPDATDB2-DSYST     TO GB15-DSYST                              
  !        COMPUTE WQTE-TEMP1 = WQTE * -1                                       
  !                                                                             
  !        EXEC SQL UPDATE   RVGB1502                                           
  !                 SET      QDEMANDEE = QDEMANDEE + :WQTE-TEMP1,               
  !                          DSYST     = :GB15-DSYST,                           
  !                          QDEMINIT  = QDEMINIT + :WQTE-TEMP1                 
  !                 WHERE    NMUTATION = :GB15-NMUTATION                        
  !                 AND      NCODIC    = :GB15-NCODIC                           
  !        END-EXEC.                                                            
  !                                                                             
  !        IF SQLCODE NOT = 0 AND 100 AND -811                                  
  !           MOVE SPACES TO W-MESSAGE                                          
  !           STRING 'UPDATE GB15  QDEMANDEE '                                  
  !                              ' QDEMINIT: '                                  
  !                         'POUR NMUTATION: ' GB15-NMUTATION                   
  !                                 ' CODIC: ' GA00-NCODIC                      
  !           DELIMITED BY SIZE INTO W-MESSAGE                                  
  !           PERFORM ERREUR-SQL                                                
  !        END-IF.                                                              
  !                                                                             
NV2008  FIN-UPDATE-RTGB15. EXIT.                                                
      ******************************************************************        
      * On r�cup�re le WSEQFAM de RTGA14                                        
      ******************************************************************        
       SELECT-RTGA14 SECTION.                                                   
      *----------------------                                                   
           IF GA00-CFAM NOT = GA14-CFAM                                         
              MOVE GA00-CFAM TO GA14-CFAM                                       
              EXEC SQL SELECT WSEQFAM INTO :GA14-WSEQFAM                        
                         FROM RVGA1400                                          
                     WHERE CFAM = :GA14-CFAM                                    
              END-EXEC                                                          
              IF SQLCODE NOT = 0                                                
                 MOVE 'SELECT GA14:' TO W-MESSAGE                               
                 MOVE GA14-CFAM      TO W-MESSAGE(14:)                          
                 PERFORM ERREUR-SQL                                             
              END-IF                                                            
           END-IF.                                                              
      ******************************************************************        
      * On r�cup�re le WMULTIFAM de RTGA00/14                                   
      ******************************************************************        
       SELECT-RTGA14-WMULTIFAM SECTION.                                         
      *--------------------------------                                         
           EXEC SQL SELECT GA14.WMULTIFAM INTO :GA14-WMULTIFAM                  
                      FROM RVGA0015 GA00, RVGA1400 GA14                         
                     WHERE GA14.CFAM   = GA00.CFAM                              
                       AND GA00.NCODIC = :GA00-NCODIC                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              STRING 'SELECT GA00/14: ' GA00-NCODIC                             
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * Donn�es pour les �l�ments d'un groupe:                                  
      * - recherche des �l�ments le composant: codics, qt� et SRP               
      * - calcul du prix des �l�ments                                           
      ******************************************************************        
       CHARGE-TABLEAU-ELEMENTS SECTION.                                         
      *--------------------------------                                         
           INITIALIZE T-G-GROUPE.                                               
           MOVE 0 TO T-G-U W-G-SRP-TOT.                                         
           PERFORM DECLARE-OPEN-RTGA58.                                         
           PERFORM FETCH-RTGA58.                                                
           PERFORM UNTIL SQLCODE = 100                                          
              ADD 1 TO T-G-U                                                    
              IF T-G-U > T-G-MAX                                                
                 MOVE 'Tableau Groupe Poduits Maxi atteint' TO W-MESSAGE        
                 PERFORM ERREUR-PRG                                             
              END-IF                                                            
              MOVE GA58-NCODICLIE TO T-G-NCODICLIE(T-G-U)                       
              MOVE GA58-QTYPLIEN  TO T-G-QTYPLIEN(T-G-U)                        
              PERFORM SELECT-RTGA67                                             
              MOVE GA67-SRP       TO T-G-SRP(T-G-U)                             
              COMPUTE W-G-SRP-TOT = W-G-SRP-TOT                                 
                                + (T-G-SRP(T-G-U) * T-G-QTYPLIEN(T-G-U))        
              PERFORM FETCH-RTGA58                                              
           END-PERFORM.                                                         
           PERFORM CLOSE-RTGA58.                                                
      ******************************************************************        
      * Calcul du prix de chaque �l�ment, selon la formule:                     
      *                                                                         
      *                  Prix_du_Groupe * SRP_de_l'�l�ment_i                    
      * Prix_El�ment_i = -----------------------------------                    
      *                  Somme_des_SRP_de_tous_les_�l�ments                     
      * Mais:                                                                   
      * -----                                                                   
      * La somme des prix de tous les �l�ments, ainsi calcul�e, peut            
      * �tre diff�rente du prix du Groupe (arrondi des calculs).                
      * Il faudra donc aller reporter sur un des �l�ments cette                 
      * diff�rence. Lequel choisir? Le premier qui aura une quantit� de         
      * conditionnement � 1, ce qui �vite de diviser un centime en 2.           
      ******************************************************************        
           MOVE 0 TO W-G-PX-VT-TOT.                                             
           PERFORM VARYING T-G-I FROM 1 BY 1 UNTIL T-G-I > T-G-U                
              COMPUTE T-G-PX-VT(T-G-I) ROUNDED =                                
                      (W-CC-V-PX-VT(W-CC-V-I) * T-G-SRP(T-G-I))                 
              COMPUTE T-G-PX-VT(T-G-I) ROUNDED =                                
                      T-G-PX-VT(T-G-I) / W-G-SRP-TOT                            
v41r0         COMPUTE W-G-PX-VT  ROUNDED = T-G-PX-VT(T-G-I) * 1                 
v41r0         MOVE W-G-PX-VT  TO T-G-PX-VT(T-G-I)                               
              COMPUTE W-G-PX-VT-TOT = W-G-PX-VT-TOT                             
                    + (T-G-PX-VT(T-G-I) * T-G-QTYPLIEN(T-G-I))                  
           END-PERFORM.                                                         
           IF W-G-PX-VT-TOT NOT = W-CC-V-PX-VT(W-CC-V-I)                        
              PERFORM VARYING T-G-I FROM 1 BY 1 UNTIL T-G-I > T-G-U             
                 IF T-G-QTYPLIEN(T-G-I) = 1                                     
                    COMPUTE T-G-PX-VT(T-G-I) = T-G-PX-VT(T-G-I)                 
                           + (W-CC-V-PX-VT(W-CC-V-I) - W-G-PX-VT-TOT)           
                    MOVE 999 TO T-G-I                                           
                 END-IF                                                         
              END-PERFORM                                                       
co0611                                                                          
              if t-g-i < 999                                                    
                 COMPUTE W-CC-E-MT-AJUST = W-CC-E-MT-AJUST                      
                        + (W-CC-V-PX-VT(W-CC-V-I) - W-G-PX-VT-TOT)              
              end-if                                                            
           END-IF.                                                              
      ******************************************************************        
      * recherche des diff�rents �l�ments                                       
      ******************************************************************        
       DECLARE-OPEN-RTGA58 SECTION.                                             
      *----------------------------                                             
           EXEC SQL DECLARE CGA58 CURSOR FOR                                    
                    SELECT NCODICLIE, QTYPLIEN                                  
                      FROM RVGA5800                                             
                     WHERE NCODIC   = :GA00-NCODIC                              
                       AND CTYPLIEN = 'GRP M'                                   
           END-EXEC.                                                            
           EXEC SQL OPEN CGA58 END-EXEC.                                        
           IF SQLCODE NOT = 0                                                   
              MOVE 'OPEN RTGA58:' TO W-MESSAGE                                  
              MOVE GA00-NCODIC   TO W-MESSAGE(14:)                              
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       FETCH-RTGA58 SECTION.                                                    
      *---------------------                                                    
           EXEC SQL FETCH CGA58 INTO :GA58-NCODICLIE, :GA58-QTYPLIEN            
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE 'FETCH RTGA58:' TO W-MESSAGE                                 
              MOVE GA00-NCODIC   TO W-MESSAGE(15:)                              
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       CLOSE-RTGA58 SECTION.                                                    
      *---------------------                                                    
           EXEC SQL CLOSE CGA58 END-EXEC.                                       
           IF SQLCODE NOT = 0                                                   
              MOVE 'CLOSE RTGA58:' TO W-MESSAGE                                 
              MOVE GA00-NCODIC   TO W-MESSAGE(15:)                              
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * R�cup�ration du SRP                                                     
      * jg modif: si on ne trouve pas dans la filiale, on prend celui           
      *           de la societe m�re.                                           
      ******************************************************************        
       SELECT-RTGA67 SECTION.                                                   
      *----------------------                                                   
           MOVE 0 TO GA67-SRP.                                                  
           MOVE W-CC-NSOC-EN-TRT TO GA67-NSOCIETE.                              
           EXEC SQL SELECT SRP INTO :GA67-SRP                                   
                      FROM RVGA6700                                             
                     WHERE NSOCIETE = :GA67-NSOCIETE                            
                       AND NCODIC   = :GA58-NCODICLIE                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE SPACES TO W-MESSAGE                                          
              STRING 'SELECT GA67 NSOCIETE: ' GA67-NSOCIETE                     
                                   ' CODIC: ' GA58-NCODICLIE                    
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 100 OR GA67-SRP = 0                                     
              MOVE W-CC-NSOC-ECOM   TO GA67-NSOCIETE                            
              EXEC SQL SELECT SRP INTO :GA67-SRP                                
                         FROM RVGA6700                                          
                        WHERE NSOCIETE = :GA67-NSOCIETE                         
                          AND NCODIC   = :GA58-NCODICLIE                        
              END-EXEC                                                          
              IF SQLCODE NOT = 0 AND 100                                        
                 MOVE SPACES TO W-MESSAGE                                       
                 STRING 'SELECT GA67 NSOCIETE: ' GA67-NSOCIETE                  
                                      ' CODIC: ' GA58-NCODICLIE                 
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ERREUR-SQL                                             
              END-IF                                                            
              IF SQLCODE = 100 OR GA67-SRP = 0                                  
                 MOVE GA58-NCODICLIE TO GV11-NCODIC                             
                 PERFORM SELECT-PRIX-GG20-OU-GA69                               
                 MOVE GG20-PEXPTTC TO GA67-SRP                                  
              END-IF                                                            
           END-IF.                                                              
      ******************************************************************        
      * Mode de paiement Darty.Com: en ligne O/N                                
      ******************************************************************        
       SELECT-EC008 SECTION.                                                    
      *---------------------                                                    
           MOVE SPACES TO EC008-WTABLEG.                                        
           EXEC SQL SELECT WTABLEG INTO :EC008-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'EC008'                                   
                       AND CTABLEG2 = :EC008-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'SELECT EC008:' TO W-MESSAGE                                 
              MOVE EC008-CTABLEG2  TO W-MESSAGE(15:)                            
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
      ******************************************************************        
      * GVDEL: d�lai d'arriv�e de la mut avant livraison.                       
      ******************************************************************        
       SELECT-GVDEL SECTION.                                                    
      *---------------------                                                    
           MOVE SPACES TO GVDEL-CTABLEG2 GVDEL-WTABLEG.                         
           MOVE GS10-NSOCDEPOT TO GVDEL-NSOCENTR.                               
           MOVE GS10-NDEPOT    TO GVDEL-NSOCENTR(4:).                           
           MOVE GB05-NSOCIETE  TO GVDEL-LDEPLIV                                 
           MOVE GB05-NLIEU     TO GVDEL-LDEPLIV(4:).                            
v5.1.p*    MOVE 'LD2'                                                           
v5.1.p     MOVE W-CC-V-MODE-DELIV (W-CC-V-I) TO GVDEL-CMODDEL.                  
           EXEC SQL SELECT WTABLEG INTO :GVDEL-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'GVDEL'                                   
                       AND CTABLEG2 = :GVDEL-CTABLEG2                           
                       AND SUBSTR(WTABLEG , 1 , 1) = 'O'                        
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE 'SELECT GVDEL:' TO W-MESSAGE                                 
              MOVE GVDEL-CTABLEG2  TO W-MESSAGE(15:)                            
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
           IF SQLCODE = 100                                                     
              MOVE ZEROES TO GVDEL-QDELAI-N                                     
           END-IF.                                                              
      ******************************************************************        
      * Select RTGA64: Mode de D�livrance pour un codic                         
      ******************************************************************        
       SELECT-RTGA64 SECTION.                                                   
      *----------------------                                                   
           MOVE SPACES TO GA64-CMODDEL.                                         
           EXEC SQL SELECT CMODDEL INTO :GA64-CMODDEL                           
                      FROM RVGA6400                                             
                     WHERE NCODIC = :GA64-NCODIC                                
                       AND CMODDEL = 'LD2'                                      
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              MOVE 'SELECT GA64' TO W-MESSAGE                                   
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       SELECT-RTGQ20 SECTION.                                                   
           move W-CC-E-C-INSEE-LIV to gq01-cinsee                               
           EXEC SQL                                                             
                SELECT CELEMEN                                                  
                INTO  :GQ01-CELEMEN                                             
                FROM RVGQ2000                                                   
                WHERE CINSEE = :GQ01-CINSEE                                     
                  AND CCADRE  = :W-CADRE                                        
           END-EXEC.                                                            
           IF SQLCODE NOT = 0  and 100                                          
              move  'pb db2 RTGQ20 : ' to W-MESSAGE                             
              PERFORM ERREUR-SQL                                                
           else                                                                 
              if sqlcode not = 0                                                
                 MOVE spaces TO W-MESSAGE                                       
                 string ' cinsee / cadre : ' gq01-cinsee '/' w-cadre            
                 'non trouve dans RTGQ20' delimited by size                     
                 into w-message                                                 
                 perform ERREUR-PRG                                             
              end-if                                                            
           END-IF.                                                              
           move gq01-celemen to  W-CC-CELEMEN.                                  
V40R0  INSERT-RTGV99 SECTION.                                                   
V40R0                                                                           
V40R0      MOVE W-CC-V-C-SOC(W-CC-V-I)         TO GV99-NSOCIETE.                
V40R0      MOVE W-CC-V-C-LIEU(W-CC-V-I)        TO GV99-NLIEU.                   
V40R0      MOVE W-CC-V-NUM-CMD-SI(W-CC-V-I)    TO GV99-NVENTE.                  
V40R0      MOVE W-CC-V-CTYPENREG(W-CC-V-I)     TO GV99-CTYPENREG.               
V40R0      MOVE W-CC-V-NCODICGRP(W-CC-V-I)     TO GV99-NCODICGRP.               
V40R0      MOVE W-CC-V-CODIC(W-CC-V-I)         TO GV99-NCODIC.                  
V40R0      MOVE GV11-NSEQ                      TO GV99-NSEQ.                    
V40R0      MOVE W-CC-V-DT-LIV(W-CC-V-I)        TO GV99-DDELIV.                  
V40R0      MOVE W-SPDATDB2-DSYST               TO GV99-DSYST.                   
V40R0      MOVE W-CC-V-IS-PRECO(W-CC-V-I)      TO GV99-WPRECO.                  
V40R0                                                                           
V40R0      EXEC SQL                                                             
V40R0           INSERT INTO RVGV9901                                            
V40R0           (NSOCIETE, NLIEU, NVENTE, CTYPENREG, NCODICGRP,                 
V40R0            NCODIC, NSEQ, DDELIV, DSYST, WPRECO)                           
V40R0           VALUES (:GV99-NSOCIETE, :GV99-NLIEU, :GV99-NVENTE,              
V40R0                   :GV99-CTYPENREG,                                        
V40R0             :GV99-NCODICGRP, :GV99-NCODIC, :GV99-NSEQ,                    
V40R0             :GV99-DDELIV, :GV99-DSYST, :GV99-WPRECO)                      
V40R0      END-EXEC.                                                            
V40R0                                                                           
V40R0                                                                           
V40R0      IF SQLCODE NOT = 0                                                   
V40R0         MOVE SPACES TO W-MESSAGE                                          
V40R0         STRING 'INSERT RTGV99 NSOCIETE: ' GV99-NSOCIETE                   
V40R0                                ' NLIEU: ' GV99-NLIEU                      
V40R0                               ' DVENTE: ' GV99-NVENTE                     
V40R0         DELIMITED BY SIZE INTO W-MESSAGE                                  
V40R0         PERFORM ERREUR-SQL                                                
V40R0      END-IF.                                                              
V40R0                                                                           
V40R0  F-INSERT-RTGV99. EXIT.                                                   
V43R0  ACCES-VENDEUR SECTION.                                                   
V43R0                                                                           
V43R0      MOVE W-CC-E-C-VEND     TO GV31-CVENDEUR                              
V43R0                                                                           
V43R0      EXEC SQL                                                             
V43R0           SELECT LVENDEUR INTO :GV31-LVENDEUR                             
V43R0            FROM RVGV3101                                                  
V43R0           WHERE CVENDEUR = :GV31-CVENDEUR                                 
V43R0      END-EXEC.                                                            
V43R0                                                                           
V43R0      IF SQLCODE NOT = 0 AND NOT = +100                                    
V43R0         MOVE SPACES TO W-MESSAGE                                          
V43R0         STRING 'SELECT RTGV31 CVENDEUR: ' GV31-CVENDEUR                   
V43R0         DELIMITED BY SIZE INTO W-MESSAGE                                  
V43R0         PERFORM ERREUR-SQL                                                
V43R0      END-IF.                                                              
V43R0                                                                           
V43R0  F-ACCES-VENDEUR. EXIT.                                                   
      ******************************************************************        
      * Link � MEC09: d�codage Adresse                                          
      ******************************************************************        
       FAIS-UN-LINK-A-MEC09 SECTION.                                            
      *-----------------------------                                            
           EXEC CICS LINK PROGRAM ('MEC09')                                     
                     COMMAREA (MEC09C)                                          
           END-EXEC.                                                            
      ******************************************************************        
      * Cr�e mouchard ent�te de vente.                                          
      ******************************************************************        
       CREE-MOUCHARD-ENTETE-VENTE SECTION.                                      
      *-----------------------------------                                      
           INITIALIZE MEC15E-ENTETE.                                            
           MOVE W-CC-E-NUM-CMD-DC TO MEC15E-NCDEWC.                             
           MOVE GV10-NSOCIETE     TO MEC15E-NSOCIETE.                           
           MOVE GV10-NLIEU        TO MEC15E-NLIEU.                              
           MOVE GV10-NVENTE       TO MEC15E-NVENTE.                             
           MOVE 'I'               TO MEC15E-STAT.                               
           SET MEC15E-FLAG-ENT    TO TRUE.                                      
           MOVE 'EC00'            TO MEC15E-OPER.                               
           MOVE W-CC-E-NOM-LIV    TO MEC15E-NOM.                                
           PERFORM START-TRANSID-EC15.                                          
      ******************************************************************        
      * Cr�e mouchard ent�te de ligne.                                          
      ******************************************************************        
       CREE-MOUCHARD-ENTETE-LIGNE SECTION.                                      
      *-----------------------------------                                      
           INITIALIZE MEC15C-COMMAREA.                                          
           MOVE W-CC-E-NUM-CMD-DC TO MEC15C-NCDEWC.                             
           MOVE GV10-NSOCIETE     TO MEC15C-NSOCIETE.                           
           MOVE GV10-NLIEU        TO MEC15C-NLIEU.                              
           MOVE GV10-NVENTE       TO MEC15C-NVENTE.                             
           MOVE GV11-NSEQNQ       TO MEC15C-NSEQNQ.                             
           MOVE 'I'               TO MEC15C-STAT.                               
           SET MEC15E-FLAG-LIGNE  TO TRUE.                                      
           PERFORM START-TRANSID-EC15.                                          
      ******************************************************************        
      * Cr�e mouchard ligne.                                                    
      ******************************************************************        
       CREE-MOUCHARD-LIGNE SECTION.                                             
      *----------------------------                                             
           MOVE W-CC-E-NUM-CMD-DC TO MEC15C-NCDEWC.                             
           MOVE GV10-NSOCIETE     TO MEC15C-NSOCIETE.                           
           MOVE GV10-NLIEU        TO MEC15C-NLIEU.                              
           MOVE GV10-NVENTE       TO MEC15C-NVENTE.                             
           MOVE GV11-NSEQNQ       TO MEC15C-NSEQNQ.                             
           PERFORM START-TRANSID-EC15.                                          
V43R0  RECHERCHE-PARAMETRES  SECTION.                                           
   !  **********************                                                    
   !                                                                            
      * recherche date d'activation de gf55-dcalcul                             
   !       INITIALIZE RVGA01ZZ                                                  
   !                                                                            
   !       EXEC SQL                                                             
   !            SELECT WPARAM                                                   
   !            INTO  :XCTRL-WPARAM                                             
   !            FROM   RVGA01ZZ                                                 
   !            WHERE  SOCZP = '00000'                                          
   !            AND    TRANS = 'GF55  '                                         
   !            WITH UR                                                         
   !       END-EXEC.                                                            
   !                                                                            
   !       IF SQLCODE = 0                                                       
   !          MOVE  XCTRL-WPARAM(1:8)  TO  W-DATE-ACTIVATION-GF55               
   !       ELSE                                                                 
   !          MOVE  '29991231'         TO  W-DATE-ACTIVATION-GF55               
   !       END-IF.                                                              
   !                                                                            
V43R0  FIN-REC-PARAMETRES. EXIT.                                                
      ***************************                                               
      ******************************************************************        
      * Start EC15 (cr�e mouchard).                                             
      ******************************************************************        
       START-TRANSID-EC15 SECTION.                                              
      *---------------------------                                              
           EXEC CICS START TRANSID ('EC15') FROM (MEC15E-ENTETE)                
           END-EXEC.                                                            
      ******************************************************************        
      * Erreur Programme                                                        
      ******************************************************************        
       ERREUR-PRG SECTION.                                                      
      *-------------------                                                      
           PERFORM DISPLAY-W-MESSAGE.                                           
           MOVE 1 TO W-CC-RETOUR.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      ******************************************************************        
      * Erreur SQL                                                              
      ******************************************************************        
       ERREUR-SQL SECTION.                                                      
      *-------------------                                                      
           PERFORM DISPLAY-W-MESSAGE.                                           
           CALL DSNTIAR USING SQLCA, MSG-SQL, MSG-SQL-LRECL.                    
           PERFORM VARYING I-SQL FROM 1 BY 1 UNTIL I-SQL > 3                    
              MOVE MSG-SQL-MSG (I-SQL) TO W-MESSAGE                             
              PERFORM DISPLAY-W-MESSAGE                                         
           END-PERFORM.                                                         
           MOVE 1 TO W-CC-RETOUR.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      ******************************************************************        
      * Display d'un Message                                                    
      ******************************************************************        
       DISPLAY-W-MESSAGE SECTION.                                               
      *--------------------------                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-MESSAGE-H.                             
           MOVE W-MINUTE            TO W-MESSAGE-M.                             
           EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)                
                     LENGTH (80) NOHANDLE                                       
           END-EXEC.                                                            
      ******************************************************************        
      * Link � Tetdatc.                                                         
      ******************************************************************        
       FAIS-UN-LINK-A-TETDATC SECTION.                                          
      *-------------------------------                                          
           EXEC CICS LINK PROGRAM ('TETDATC')                                   
                     COMMAREA (Z-COMMAREA-TETDATC)                              
           END-EXEC.                                                            
           IF GFVDAT NOT = '1'                                                  
              MOVE 'Code retour TETDATC: ' TO W-MESSAGE                         
              MOVE GFVDAT                  TO W-MESSAGE(24:)                    
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE 1 TO W-CC-RETOUR                                             
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS RETURN END-EXEC                                         
      *--                                                                       
              EXEC CICS RETURN                                                  
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      ******************************************************************        
      * SPDatDB2                                                                
      ******************************************************************        
       CALL-SPDATDB2 SECTION.                                                   
           CALL 'SPDATDB2' USING W-SPDATDB2-RC W-SPDATDB2-DSYST.                
           IF W-SPDATDB2-RC NOT = ZERO                                          
              MOVE 'Pbm Date Syst�me SPDATDB2' TO W-MESSAGE                     
              PERFORM DISPLAY-W-MESSAGE                                         
           END-IF.                                                              
      *                                                                         
      *-----------------------------------------------------------------        
      * Abend Label: on s'est plant�; on flingue le code retour et              
      *              on revient l� o� on �tait: on ne cr�e pas de vente.        
       ABEND-LABEL SECTION.                                                     
           MOVE 'Handle Abend effectu�' TO W-MESSAGE.                           
           PERFORM DISPLAY-W-MESSAGE.                                           
           MOVE 1 TO W-CC-RETOUR.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
      *                                                                         
      *-----------------------------------------------------------------        
V41R0 *  => Lecture de la TS TSTENVOI                                           
    | * mecccprg n'accepte pas "include cstenvoi" .                             
    |  LECTURE-TSTENVOI SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
    | *    EXEC CICS                                                            
    | *         READQ TS                                                        
    | *         QUEUE  (WC-ID-TSTENVOI)                                         
    | *         INTO   (TS-TSTENVOI-DONNEES)                                    
    | *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
    | *         ITEM   (WP-RANG-TSTENVOI)                                       
    | *         NOHANDLE                                                        
    | *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
    |      IF EIBRESP = 0                                                       
    |         SET WC-TSTENVOI-SUITE TO TRUE                                     
    |      ELSE                                                                 
    |         SET WC-TSTENVOI-FIN   TO TRUE                                     
    |      END-IF.                                                              
    |  FIN-LECT-TSTENVOI. EXIT.                                                 
M16733 ALIMENTE-MEC03C SECTION.                                                 
           ADD 1 TO MEC03C-NBP                                                  
           IF MEC03C-NBP  > MEC03C-NBP-MAX                                      
              MOVE 'TABLEAU MEC03C MAXI ATTEINT' TO W-MESSAGE                   
              PERFORM ERREUR-PRG                                                
           END-IF                                                               
           MOVE MEC03C-SOCIETE-ENT (MEC03C-NBP-MAX + 1)                         
             TO MEC03C-SOCIETE-ENT (MEC03C-NBP)                                 
           MOVE MEC03C-LIEU-ENT (MEC03C-NBP-MAX + 1)                            
             TO MEC03C-LIEU-ENT (MEC03C-NBP)                                    
           MOVE MEC03C-CODIC-ENT (MEC03C-NBP-MAX + 1)                           
             TO MEC03C-CODIC-ENT (MEC03C-NBP)                                   
           MOVE MEC03C-WDACEM (MEC03C-NBP-MAX + 1)                              
             TO MEC03C-WDACEM (MEC03C-NBP)                                      
           MOVE MEC03C-ETAT-RESERVATION (MEC03C-NBP-MAX + 1)                    
             TO MEC03C-ETAT-RESERVATION (MEC03C-NBP).                           
       F-ALIMENTE-MEC03C. EXIT.                                                 
       CHERCHE-CADRE SECTION.                                                   
      * > RECHERCHE DANS LA TSTENVOI DES LIGNES DE TYPE EXPDELES%               
      *  L'INDICATION DU CADRE SE TROUVERA DIRECTEMENT SUR LA LIGNE             
      *  C'EST UNE REDONDANCE DE LA PR�SENCE DANS LA SOUS-TABLE CADRE.          
      *  MAIS �A PERMET D'OPTIMISER LES ACC�S facilement.                       
      *  EN CAS DE CR�ATION D'UN NOUVEAU CADRE IL FAUDRA ABSOLUMENT             
      *  SE POSER LA QUESTION DE REPORTER CE CADRE DANS LE PARAM�TRAGE          
      *  SI DCOM VENAIT � L'UTILISER.                                           
      *  POUR CHAQUE LIGNE D�CRITE DANS CE PARAM�TRAGE, ON                      
      *  CONSERVE LES INFORMATIONS CMODDEL, CPLAGE ... NOUS PERMETTANT          
      *  DE R�CUP�RER LE MODE DE D�LIVRANCE                                     
      *  ON INT�GRE LA DATE DE D�LIVRANCE DANS LE TABLEAU                       
      *  SI UN JOUR ON FAIT DU MULTICADRE CHEZ DCOM...                          
           MOVE  '0' TO WF-TSTENVOI                                             
           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
           UNTIL  WC-TSTENVOI-FIN                                               
              PERFORM LECTURE-TSTENVOI                                          
              IF  WC-TSTENVOI-SUITE                                             
                IF  (TSTENVOI-CTRL(1:2) = 'ES'                                  
mgd15               or TSTENVOI-CTRL(1:2) = 'MG')                               
                AND TSTENVOI-LIBELLE (7:5) = W-CC-V-CENREG (W-CC-V-I)           
                AND (TSTENVOI-NSOCZP (1:3)    =  '000'                          
                     OR TSTENVOI-NSOCZP (1:3) = W-CC-NSOC-EN-TRT)               
                   ADD 1      TO TAB1-CPT                                       
                   IF TAB1-CPT > TAB1-CPT-MAX                                   
                      MOVE 'TROP TABLEAU TAB1' TO W-MESSAGE                     
                      PERFORM ERREUR-PRG                                        
                   END-IF                                                       
                   SET TAB1-I TO TAB1-CPT                                       
                   MOVE W-CC-V-CENREG (W-CC-V-I)                                
                     TO W-TAB1-SERVICE (TAB1-I)                                 
                   MOVE W-CC-V-DT-LIV(W-CC-V-I)                                 
                     TO W-TAB1-DDELIV  (TAB1-I)                                 
                   MOVE TSTENVOI-WPARAM(4:2)                                    
                     TO W-TAB1-CPLAGE  (TAB1-I)                                 
                   MOVE TSTENVOI-WPARAM(1:3)                                    
                     TO W-TAB1-CMODDEL (TAB1-I)                                 
                   MOVE  W-CC-V-CENREG (W-CC-V-I)                               
                     TO W-SERVICE-livraison                                     
      * -> information c'est un cadre de vente.                                 
                   IF  TSTENVOI-LIBELLE (2:5) > SPACES                          
                       MOVE TSTENVOI-LIBELLE (2:5)    TO W-CADRE                
                   END-IF                                                       
              END-IF                                                            
              END-IF                                                            
           END-PERFORM.                                                         
       F-CHERCHE-CADRE. EXIT.                                                   
       CHERCHE-MODE-DELIVRANCE SECTION.                                         
           PERFORM VARYING TAB1-cpt FROM 1 BY 1                                 
             UNTIL TAB1-cpt > TAB1-CPT-MAX                                      
               OR (W-TAB1-CPLAGE (TAB1-cpt)  = W-CC-V-CREN-LIV(W-CC-V-I)        
                and w-tab1-ddeliv (tab1-cpt) = W-CC-V-DT-LIV(W-CC-V-I)          
                   )                                                            
           END-PERFORM.                                                         
           IF TAB1-cpt <= TAB1-CPT-max                                          
           and TAB1-CPT-max > 0                                                 
              MOVE W-TAB1-CMODDEL (tab1-cpt)                                    
                TO W-CC-V-MODE-DELIV  (W-CC-V-I)                                
                   W-CMODDEL-CADRE                                              
      *    ELSE                                                                 
      *       IF TAB1-CPT-max > 0                                               
      *          STRING                                                         
      *          'CMODDEL NON TROUVE POUR '                                     
      *          'PLAGE :' W-CC-V-CREN-LIV(W-CC-V-I)                            
      *          DELIMITED BY SIZE                                              
      *          INTO W-MESSAGE                                                 
      *          PERFORM ERREUR-PRG                                             
      *       END-IF                                                            
           END-IF.                                                              
       F-CHERCHE-MODE-DELIVRANCE. EXIT.                                         
V5.1P  CHARGEMENT-SEL-ARTICLE SECTION.                                          
      * -> TENTATIVE D'OPTIMISATION DE L'ACC�S AUX MUTATIONS                    
      * -> lecture une seule fois sur relation lieux et sel. art.               
           INITIALIZE W1-ZONE-TABLE                                             
           MOVE 0 TO W1-CPT                                                     
           move spaces         to LSELA-CTABLEG2                                
           MOVE GS10-NSOCDEPOT TO LSELA-NSOCDEPO                                
           MOVE GA00-CFAM      TO LSELA-CFAM                                    
           PERFORM DECLARE-OPEN-CSELA                                           
           PERFORM FETCH-CSELA                                                  
           PERFORM UNTIL CSELA-FIN                                              
                   ADD 1    TO W1-CPT                                           
                   IF W1-CPT > W1-CPT-MAX                                       
                      MOVE 'PB TABLEAU SEL. ART. W1' TO W-MESSAGE               
                      PERFORM ERREUR-PRG                                        
                   END-IF                                                       
                   SET W1-I TO W1-CPT                                           
                   MOVE CSELA-WTABLEG(1:5) TO W1-SELART (W1-I)                  
                                              LSELA-CSELART                     
                   PERFORM SELECT-LSELA                                         
                   IF SQLCODE = 0                                               
                      MOVE 'O'                TO W1-WACTIF (W1-I)               
                   ELSE                                                         
                      MOVE 'N'                TO W1-WACTIF (W1-I)               
                   END-IF                                                       
                   PERFORM FETCH-CSELA                                          
           END-PERFORM                                                          
           PERFORM CLOSE-CSELA                                                  
      * > LIEN SELECTION ARTICLE FAMILLE                                        
           PERFORM VARYING W2-I FROM 1 BY 1                                     
                   UNTIL W2-I > W2-CPT                                          
                      OR W2-CMODDEL (W2-I)                                      
                       = W-CC-V-MODE-DELIV (W-CC-V-I)                           
           END-PERFORM                                                          
           IF W2-I > W2-CPT                                                     
      * -> AUCUNE REGLE CONCERNANT LE MODE DE D�LIVRANCE                        
      * -> EXCLUSION STRICTE                                                    
              PERFORM VARYING W1-I FROM 1 BY 1                                  
                UNTIL W1-I > W1-CPT                                             
                   IF W1-WACTIF (W1-I) = 'O'                                    
                      PERFORM VARYING W2-I FROM 1 BY 1                          
                         UNTIL W2-I > W2-CPT                                    
                            OR W2-SELART (W2-I) = W1-SELART (W1-I)              
                      END-PERFORM                                               
                      IF W2-I <= W2-CPT                                         
                         MOVE 'N' TO W1-WACTIF (W1-I)                           
                      END-IF                                                    
                    END-IF                                                      
              END-PERFORM                                                       
           ELSE                                                                 
      * -> INCLUSION STRICTE                                                    
              PERFORM VARYING W1-I FROM 1 BY 1                                  
                UNTIL W1-I > W1-CPT                                             
                   IF W1-WACTIF (W1-I) = 'O'                                    
                      PERFORM VARYING W2-I FROM 1 BY 1                          
                        UNTIL W2-I > W2-CPT                                     
                           OR (W2-CMODDEL (W2-I)                                
                            = W-CC-V-MODE-DELIV   (W-CC-V-I)                    
                           AND W2-SELART (W2-I) = W1-SELART (W1-I))             
                      END-PERFORM                                               
                      IF W2-I > W2-CPT                                          
                         MOVE 'N' TO W1-WACTIF (W1-I)                           
                      END-IF                                                    
                   END-IF                                                       
              END-PERFORM                                                       
           END-IF.                                                              
       F-CHARGEMENT-SEL-ARTICLE. EXIT.                                          
       DECLARE-OPEN-CSELA SECTION.                                              
           EXEC SQL                                                             
                DECLARE CS-CSELA CURSOR FOR                                     
                 SELECT WTABLEG                                                 
                 FROM  RVGA0100                                                 
                 WHERE CTABLEG1 = 'CSELA'                                       
                 AND   CTABLEG2 >= :W-CSELA-MIN                                 
                 AND   CTABLEG2 <= :W-CSELA-MAX                                 
           END-EXEC.                                                            
           STRING GS10-NSOCDEPOT                                                
                  GS10-NDEPOT                                                   
                  GB05-NSOCIETE                                                 
                  GB05-NLIEU                                                    
                  '000'                                                         
           DELIMITED BY SIZE INTO W-CSELA-MIN                                   
           STRING GS10-NSOCDEPOT                                                
                  GS10-NDEPOT                                                   
                  GB05-NSOCIETE                                                 
                  GB05-NLIEU                                                    
                  '999'                                                         
           DELIMITED BY SIZE INTO W-CSELA-MAX                                   
           EXEC SQL                                                             
                OPEN  CS-CSELA                                                  
           END-EXEC.                                                            
           set csela-ok to true.                                                
       F-DECLARE-OPEN-CSELA. EXIT.                                              
       FETCH-CSELA SECTION.                                                     
           EXEC SQL                                                             
                FETCH CS-CSELA                                                  
                INTO :CSELA-WTABLEG                                             
           END-EXEC.                                                            
           if sqlcode = 100                                                     
              set csela-fin to true                                             
           else                                                                 
              if sqlcode not = 0                                                
                 move 'fetch LSELA :' to W-MESSAGE                              
                 PERFORM ERREUR-SQL                                             
             end-if                                                             
           end-if.                                                              
       F-FETCH-CSELA. EXIT.                                                     
       CLOSE-CSELA SECTION.                                                     
           EXEC SQL                                                             
                CLOSE CS-CSELA                                                  
           END-EXEC.                                                            
       F-CLOSE-CSELA. EXIT.                                                     
       SELECT-LSELA SECTION.                                                    
           EXEC SQL                                                             
                SELECT CTABLEG1                                                 
                  INTO :GA01-CTABLEG1                                           
                  FROM RVGA0100                                                 
                 WHERE CTABLEG1 = 'LSELA'                                       
                   AND CTABLEG2 = :LSELA-CTABLEG2                               
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND 100                                           
              STRING 'SELECT LSELA : ' LSELA-CTABLEG2                           
              delimited by size into w-message                                  
              PERFORM ERREUR-SQL                                                
           END-IF.                                                              
       F-SELECT-LSELA. EXIT.                                                    
       ASSOC-MDELIV-SEL-ART SECTION.                                            
      * -> POUR L'INSTANT RECHERCHE LES SELECTIONS ARTICLES ASSOCI�ES           
      *    AU MODE DE D�LIVRANCE DANS LA VENTE.                                 
      *    LE PRINCIPE EST QU'IL EXISTE DES SELECTIONS ARTCILES                 
      *    A ASSOCIER AU MODE DE D�LIVRANCE ET D'AUTRES � EXCLURE               
      *    TOUJOURS PAR RAPPORT � CE DERNIER.                                   
           MOVE  '0' TO WF-TSTENVOI                                             
           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
           UNTIL  WC-TSTENVOI-FIN                                               
              PERFORM LECTURE-TSTENVOI                                          
              IF  WC-TSTENVOI-SUITE                                             
              AND TSTENVOI-CTRL(1:2) = 'MU'                                     
              AND (TSTENVOI-NSOCZP (1:3) =  '000'                               
                           OR TSTENVOI-NSOCZP (1:3) = W-CC-NSOC-ECOM)           
               ADD 1 TO W2-CPT                                                  
               IF W2-CPT > W2-CPT-MAX                                           
                  MOVE 'PB TABLEAU SEL. ART. W2' TO W-MESSAGE                   
                  PERFORM ERREUR-PRG                                            
               END-IF                                                           
               SET W2-I TO W2-CPT                                               
               MOVE tstenvoi-wparam  (1:3)       TO W2-CMODDEL (W2-I)           
               MOVE TSTENVOI-LIBELLE (1:5)       TO W2-SELART  (W2-I)           
              END-IF                                                            
           END-PERFORM.                                                         
       F-ASSOC-MDELIV-SEL-ART. EXIT.                                            
       ctrl-mutation-crossdock section.                                         
           set cherche-pas-xdock to true                                        
           IF  (LIVRG-LIEUGEST(1:3) NOT = GS10-NSOCDEPOT)                       
             OR (LIVRG-LIEUGEST(4:3) NOT = GS10-NDEPOT)                         
              PERFORM SELECT-RTGB05                                             
              IF mutation-non-trouvee                                           
                 IF FAIS-DES-DISPLAYS                                           
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING                                                      
                          'AUCUNE MUTATION POUR CODIC XDOCK: '                  
                           W-CC-V-CODIC(W-CC-V-I)                               
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-W-MESSAGE                                   
                    MOVE SPACES TO W-MESSAGE                                    
                    STRING GS10-NSOCDEPOT                                       
                      '!'  GS10-NDEPOT                                          
                      '!'  GB05-NSOCIETE                                        
                      '!'  GB05-NLIEU                                           
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-W-MESSAGE                                   
                 END-IF                                                         
              else                                                              
                 set cherche-xdock to true                                      
              end-if                                                            
           else                                                                 
              set cherche-xdock to true                                         
           end-if.                                                              
       f-CTRL-MUTATION-CROSSDOCK. exit.                                         
       controle-cross-dock section.                                             
           move 1 TO CO-GF50-IDX                                                
           SET CO-GF50-CREATION (CO-GF50-IDX) to true                           
           MOVE gv11-ncodic    TO CO-GF50-NCODIC    (CO-GF50-IDX)               
           move gv11-nseq      TO CO-GF50-NSEQ      (CO-GF50-IDX)               
           move gv11-qvendue   TO CO-GF50-QVENDUE   (CO-GF50-IDX)               
           move gv11-ddeliv    TO CO-GF50-DDELIV    (CO-GF50-IDX)               
           move 0              TO CO-GF50-QVENDUE-I (CO-GF50-IDX)               
           move GS10-NSOCDEPOT TO CO-GF50-NSOCDEP   (CO-GF50-IDX)               
           move GS10-NDEPOT    TO CO-GF50-NLIEUDEP  (CO-GF50-IDX)               
           move EC007-NSOCPLT  TO CO-GF50-NSOCD     (CO-GF50-IDX)               
           move EC007-NLIEUPLT TO CO-GF50-NLIEUD    (CO-GF50-IDX)               
           move gb05-ddestock  to CO-GF50-DDESTOCK  (CO-GF50-IDX)               
           move '0000'  to CO-GF50-CRET                                         
           SET CO-GF50-INTERRO TO TRUE                                          
           SET CO-GF50-VENTE   TO TRUE                                          
           move GV10-NSOCIETE      TO CO-GF50-NSOCIETE                          
           move GV10-NLIEU         TO CO-GF50-NLIEU                             
           move GV10-NVENTE        TO CO-GF50-NVENTE                            
           MOVE W-CC-DATE-DU-JOUR  TO CO-GF50-SSAAMMJJ                          
           MOVE CO-GF50-IDX        TO CO-GF50-IDXMAX                            
           move 'MGF50' to w-nom-prog                                           
           EXEC CICS LINK PROGRAM (w-NOM-PROG)                                  
                COMMAREA (COMMAREA-GF50)                                        
                LENGTH (LENGTH OF COMMAREA-GF50)                                
                NOHANDLE                                                        
           END-EXEC                                                             
           MOVE SPACES TO W-MESSAGE                                             
           string 'recherche xdock, codic ' gv11-ncodic                         
           DELIMITED BY SIZE INTO W-MESSAGE                                     
           PERFORM DISPLAY-W-MESSAGE                                            
           IF CO-GF50-CRET = '0000'                                             
              move 'K' to gv11-wcqeresf                                         
              set WS-RES-xdk-TRT-ok   to true                                   
              set GV11-WCQERESF-XDOCK to true                                   
              move 'xdock ok ' TO W-MESSAGE                                     
              PERFORM DISPLAY-W-MESSAGE                                         
           else                                                                 
              MOVE SPACES TO W-MESSAGE                                          
              move  'recherche xdock, ko ' TO W-MESSAGE                         
              PERFORM DISPLAY-W-MESSAGE                                         
              move co-gf50-cret to w-message                                    
              PERFORM DISPLAY-W-MESSAGE                                         
              move co-gf50-lretd(1)  to w-message                               
              PERFORM DISPLAY-W-MESSAGE                                         
cr1711        move spaces to gb05-nmutation                                     
           end-if.                                                              
       f-controle-cross-dock. exit.                                             
      * Creation de dossier                                                     
CCPRM  APPEL-DOSSIERS-ADMIN SECTION.                                            
           INITIALIZE  NV45-DONNEES                                             
           MOVE '00'                   TO NV45-CODRET                           
           MOVE 1                      TO IND-NV45                              
           MOVE 'C'                    TO NV45-CODACT (IND-NV45)                
           MOVE W-CC-V-NCODICGRP (W-CC-V-I)                                     
                                       TO NV45-NCODICGRP (IND-NV45)             
           MOVE W-CC-V-CODIC    (W-CC-V-I)                                      
                                       TO NV45-NCODIC    (IND-NV45)             
           MOVE WS-CENREG              TO NV45-CENREG    (IND-NV45)             
           MOVE 'N'                    TO NV45-WTOPLIVRE (IND-NV45)             
           MOVE 'SE'                   TO NV45-CTYPENT   (IND-NV45)             
           move gv11-nseqens           to nv45-nseqens (ind-nv45)               
      * chargement des �lements de la balise service                            
           PERFORM VARYING W-CC-V-P2 FROM 1 BY 1                                
                   UNTIL W-CC-V-P2 > W-CC-V-P2-MAX                              
                   OR W-CC-V-NOM (W-CC-V-P1, W-CC-V-P2) <= spaces               
               MOVE W-CC-V-NOM (W-CC-V-P1, W-CC-V-P2)                           
                                       TO NV45-NOM(W-CC-V-P2)                   
               MOVE W-CC-V-VALEUR(W-CC-V-P1, W-CC-V-P2)                         
                                       TO NV45-VALEUR(W-CC-V-P2)                
      * -> pour ne plus repasser dessus.                                        
               move spaces to W-CC-V-NOM (W-CC-V-P1, W-CC-V-P2)                 
                              W-CC-V-VALEUR(W-CC-V-P1, W-CC-V-P2)               
           END-PERFORM                                                          
           MOVE GV10-NSOCIETE          TO NV45-NSOCIETE                         
           MOVE GV10-NLIEU             TO NV45-NLIEU                            
           MOVE GV10-NVENTE            TO NV45-NVENTE                           
           MOVE GV10-CACID (1:2)       TO NV45-CAGENT                           
           MOVE IND-NV45               TO TAB-NV45-MAX                          
           EXEC CICS LINK PROGRAM  ('MNV45')                                    
                           COMMAREA (Z-COMMAREA-MNV45)                          
           END-EXEC                                                             
           IF NV45-CODRET NOT = '00'                                            
              MOVE NV45-LIBRET         TO W-MESSAGE                             
              PERFORM ERREUR-PRG                                                
           END-IF.                                                              
CCPRM  F-APPEL-DOSSIERS-ADMIN. EXIT.                                            
                                                                                
