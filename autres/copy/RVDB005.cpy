      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DB005 DB005 LISTE FAMILLES REDBOX      *        
      *----------------------------------------------------------------*        
       01  RVDB005.                                                             
           05  DB005-CTABLEG2    PIC X(15).                                     
           05  DB005-CTABLEG2-REDEF REDEFINES DB005-CTABLEG2.                   
               10  DB005-CFAM            PIC X(05).                             
               10  DB005-NCODIC          PIC X(07).                             
           05  DB005-WTABLEG     PIC X(80).                                     
           05  DB005-WTABLEG-REDEF  REDEFINES DB005-WTABLEG.                    
               10  DB005-TYPENREG        PIC X(01).                             
               10  DB005-RELANCE         PIC X(01).                             
               10  DB005-TEQUIP          PIC X(05).                             
               10  DB005-EXTRACT         PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVDB005-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DB005-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DB005-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DB005-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DB005-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
