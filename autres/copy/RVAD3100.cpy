      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE INT3.RTAD31                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD3100.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD3100.                                                            
      *}                                                                        
           10 AD31-NCODIC          PIC X(7).                                    
           10 AD31-EAN             PIC X(13).                                   
           10 AD31-OPCO            PIC X(3).                                    
           10 AD31-STATUT          PIC X(1).                                    
           10 AD31-LIBELLE         PIC X(20).                                   
           10 AD31-DSYST           PIC S9(13) COMP-3.                           
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD3100-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD3100-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD31-NCODIC-F         PIC S9(4) COMP.                             
      *--                                                                       
           10 AD31-NCODIC-F         PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD31-EAN-F            PIC S9(4) COMP.                             
      *--                                                                       
           10 AD31-EAN-F            PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD31-OPCO-F           PIC S9(4) COMP.                             
      *--                                                                       
           10 AD31-OPCO-F           PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD31-STATUT-F         PIC S9(4) COMP.                             
      *--                                                                       
           10 AD31-STATUT-F         PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD31-LIBELLE-F        PIC S9(4) COMP.                             
      *--                                                                       
           10 AD31-LIBELLE-F        PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD31-DSYST-F          PIC S9(4) COMP.                             
      *                                                                         
      *--                                                                       
           10 AD31-DSYST-F          PIC S9(4) COMP-5.                           
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
