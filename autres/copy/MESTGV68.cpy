      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:18 >
      
      ******************************************************************
      *                            D'ABORD LA COPY DE L"ENTETE MESSMQ  *
      *   MESTGV68 COPY                                                *
      * - ENVOI ENCAISSEMENT      POUR CONTREMARQUE DACEM              *
      * COPY POUR LES PROGRAMMES MGV68                                 *
      ******************************************************************
              05 MESTGV68-DATA.
                 10 MESTGV68-PGM         PIC X(5).
                 10 MESTGV68-CICS        PIC X(5).
                 10 MESTGV68-VALID       PIC X(1).
                 10 MESTGV68-NBL         PIC 99.
      * DONNE CLIENT LONGUEUR 792
                 10 MESTGV68-DONNES-GV10.
      *            SOCIETE DE LA VENTE
                   15 MESTGV68-NSOCIETE       PIC X(03).
      *            LIEU    DE LA VENTE
                   15 MESTGV68-NLIEU          PIC X(03).
      *            NUMERO  DE LA VENTE NCG
                   15 MESTGV68-NVENTE         PIC X(07).
      *            DATE DE L ENCAISSEMENT
                   15 MESTGV68-DREGLTVTE      PIC X(08).
      *            MONTANT   ENCAISSEMENT
                   15 MESTGV68-PREGLTVTE PIC S9(7)V9(2) COMP-3.
                   15 MESTGV68-PRFACT    PIC S9(7)V9(2) COMP-3.
                   15 MESTGV68-PTTVENTE  PIC S9(7)V9(2) COMP-3.
                   15 MESTGV68-FILLER         PIC X(10).
              05 MESTGV68-RETOUR.
                 10 MESTGV68-CODE-RETOUR.
                    15  MESTGV68-CODRET  PIC X(1).
                         88  MESTGV68-CODRET-OK        VALUE ' '.
                         88  MESTGV68-CODRET-ERREUR    VALUE '1'.
                    15  MESTGV68-LIBERR  PIC X(60).
      *
      *****************************************************************
      
