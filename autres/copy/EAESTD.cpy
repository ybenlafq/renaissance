      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE ENTETE STANDARD                *
      * NOM FICHIER.: EAESTD                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 301                                             *
      *****************************************************************
      *
       01  EAESTD.
      * TYPE ENREGISTREMENT
           05      EAESTD-TYP-ENREG       PIC  X(0006).
      * CODE ETAT
           05      EAESTD-CETAT           PIC  X(0001).
      * CODE SOCIETE
           05      EAESTD-CSOCIETE        PIC  X(0005).
      * CODE ARTICLE
           05      EAESTD-CARTICLE        PIC  X(0018).
      * LIBELLE ARTICLE
           05      EAESTD-LARTICLE        PIC  X(0035).
      * LIBELLE COMPLEMENTAIRE
           05      EAESTD-LCOMPL          PIC  X(0035).
      * CODE RACINE
           05      EAESTD-CRACINE         PIC  X(0018).
      * TYPE D ARTICLE
           05      EAESTD-TYPE-ARTICLE    PIC  9(0002).
      * DISPONIBLE
           05      EAESTD-DISPONIBLE      PIC  9(0001).
      * VERTICAL
           05      EAESTD-VERTICAL        PIC  9(0001).
      * DANGEREUX
           05      EAESTD-DANGEREUX       PIC  9(0001).
      * AEROSOL
           05      EAESTD-AEROSOL         PIC  9(0001).
      * FRAGILE
           05      EAESTD-FRAGILE         PIC  9(0001).
      * RELEVE LOT
           05      EAESTD-REL-LOT         PIC  9(0001).
      * RELEVE NUMERO DE SERIE
           05      EAESTD-REL-NSERIE      PIC  9(0001).
      * RELEVE CARTON
           05      EAESTD-REL-CARTON      PIC  9(0001).
      * RELEVE DATE LIMITE DE VENTE
           05      EAESTD-REL-DLV         PIC  9(0001).
      * RELEVE DATE LIMITE DE CONSOMMATION
           05      EAESTD-REL-DLC         PIC  9(0001).
      * RELEVE INFORMATION 1
           05      EAESTD-REL-INFO1       PIC  9(0001).
      * RELEVE INFORMATION 2
           05      EAESTD-REL-INFO2       PIC  9(0001).
      * RELEVE INFORMATION 3
           05      EAESTD-REL-INFO3       PIC  9(0001).
      * ROTATION
           05      EAESTD-ROTATION        PIC  X(0001).
      * PRIX UNITAIRE MOYEN PONDERE (PUMP)
           05      EAESTD-PUMP            PIC  9(0010)V9(5).
      * TYPE DE SORTIE
           05      EAESTD-TYPE-SORTIE     PIC  9(0003).
      * FENETRE TYPE DE SORTIE
           05      EAESTD-FEN-TYPE-SORTIE PIC  9(0005).
      * POURCENTAGE D ALCOOL
           05      EAESTD-PER-ALCOOL      PIC  9(0003)V9(2).
      * VOLUME LIQUIDE
           05      EAESTD-VOL-LIQUIDE     PIC  9(0010).
      * CODE-A-BARRES
           05      EAESTD-CODE-BARRES     PIC  X(0020).
      * IDENTIFIANT FAMILLE DE COLISAGE
           05      EAESTD-ID-FAM-COLISAGE PIC  X(0003).
      * IDENTIFIANT FAMILLE DE STOCKAGE
           05      EAESTD-ID-FAM-STOCKAGE PIC  X(0005).
      * PREMIERE RECEPTION
           05      EAESTD-1ERE-RECEPTION  PIC  9(0001).
      * CODE UNITE LOGISTIQUE DEFAUT
           05      EAESTD-CODE-UL         PIC  X(0005).
      * QTE TOLERANCE INVENTAIRE
           05      EAESTD-QTE-TOL-INVENT  PIC  9(0003).
      * DELAI INTER-INVENTAIRE
           05      EAESTD-DELAI-INTER-INV PIC  9(0003).
      * TAUX DE SERVICE
           05      EAESTD-TX-SERVICE      PIC  9(0003).
      * CARACTERISTIQUE 1
           05      EAESTD-CARACT1         PIC  X(0020).
      * CARACTERISTIQUE 2
           05      EAESTD-CARACT2         PIC  X(0020).
      * CARACTERISTIQUE 3
           05      EAESTD-CARACT3         PIC  X(0020).
      * COEFFICIENT UNITE
           05      EAESTD-COEF-UNITE      PIC  9(0006)V9(9).
      * LIBELLE UNITE
           05      EAESTD-LUNITE          PIC  X(0010).
      * FILLER
           05      EAESTD-FILLER          PIC  X(0001).
      
