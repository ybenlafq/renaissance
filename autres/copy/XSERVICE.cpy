      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EXEC SQL                                                             
       declare SE cursor for                                                    
       --codif_service                                                          
       with                                                                     
       --Prix National en cours                                                 
       PR10 as (                                                                
       select                                                                   
       PR10.CPRESTATION                                                         
       , PR10.MTPREST                                                           
       , PR10.DEFFET                                                            
       from RVPR1000 as PR10                                                    
       where                                                                    
       PR10.NZONPRIX = '01'                                                     
       and PR10.DEFFET <= :W-DJOUR-SUIV                                         
       )                                                                        
       --PRMP National en cours                                                 
       , PR11 as (                                                              
       select                                                                   
       PR11.CPRESTATION                                                         
       , PR11.MTPRMP                                                            
       , PR11.DEFFET                                                            
       from RVPR1100 as PR11                                                    
       where                                                                    
       PR11.NZONPRIX = '01'                                                     
       and PR11.DEFFET <= :W-DJOUR-SUIV                                         
       )                                                                        
       --Prime Nationale en cours                                               
       , PR12 as (                                                              
       select                                                                   
       PR12.CPRESTATION                                                         
       , PR12.MTPRIME                                                           
       , PR12.DEFFET                                                            
       from RVPR1200 as PR12                                                    
       where                                                                    
       PR12.NZONPRIX = '01'                                                     
       and PR12.DEFFET <= :W-DJOUR-SUIV                                         
       )                                                                        
       --Assortiment futur (un seul le prochain)                                
       , assor as(                                                              
       select                                                                   
       PR06.CPRESTATION                                                         
       , PR06.LSTATUT                                                           
       , PR06.DEFFET                                                            
       from RVPR0600 PR06                                                       
       where                                                                    
       PR06.CSTATUT = 'ASSOR'                                                   
       and PR06.DEFFET > :W-DJOUR-SUIV                                          
       )                                                                        
       --Attributs selon type et valeurs par d�faut                             
       , INOTR as (                                                             
       select                                                                   
       INOTR.CGESTION                                                           
       , replace(substr(INOTR.CTRTY, 5, 3), 'BIL', 'BIC') as NATUR              
       , row_number()                                                           
       over(partition by INOTR.CGESTION order by INOTR.CTRTY desc)              
       as SEQ                                                                   
       , INOTR.VALDFAUT                                                         
       , PRGES.TYPE                                                             
       , PRGES.TABLE                                                            
       from RVINOTR INOTR                                                       
       inner join RVGA01SF as PRGES on                                          
       PRGES.CGESTION = INOTR.CGESTION                                          
       --Hors scope                                                             
       and PRGES.CGESTION not in ('NAISS', 'CLIRQ', 'NATUR')                    
       where                                                                    
       INOTR.CTRTY like 'SE50___'                                               
       )                                                                        
       --Select                                                                 
       select                                                                   
       PR00.CPRESTATION as CPRESTATION                                          
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' concat                                               
      *  lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' ||                                                   
      *--                                                                       
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         || '_BXM101_' ||                                                       
      *}                                                                        
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *}                                                                        
       --Ent�te codif                                                           
       , xmlserialize (                                                         
       xmldocument(                                                             
       xmlelement(                                                              
       name "codif"                                                             
       , xmlnamespaces(default 'http://codif.darty.fr',                         
       'http://www.w3.org/2001/XMLSchema-instance' as "xsi")                    
       , xmlattributes(                                                         
       'http://codif.darty.fr http://codif.darty.fr/schemas/codif.xsd'          
       as "xsi:schemaLocation"                                                  
       , 'NCG' as "source"                                                      
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         concat '_BXM101_' concat                                               
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
         as "reference"                                                         
       , current timestamp as "issued"                                          
       )                                                                        
       --                                                                       
       , xmlelement(name "product"                                              
       , xmlnamespaces(default 'http://codif.darty.fr/service'                  
       , 'http://www.w3.org/2001/XMLSchema-instance' as "xsi"                   
       )                                                                        
       , xmlattributes('907' as "context")                                      
       , xmlelement(name "codicInt", rtrim(GA03.NCODICK))                       
       , xmlelement(name "lnkOpc"                                               
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "codicOpc", PR00.CPRESTATION)                          
       , xmlelement(name "creatd",                                              
       timestamp_format(PR00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "modifd",                                              
       timestamp_format(PR00.DMAJ, 'YYYYMMDD'))                                 
       --Specifique NCG                                                         
       , xmlelement(name "specOpc"                                              
       , xmlelement(name "type_NCG", rtrim(GA14.CTYPENT))                       
       , xmlelement(name "ref_NCG", rtrim(PR00.LPRESTATION))                    
       , xmlelement(name "categ_NCG", rtrim(PR00.CFAM))                         
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "attr_NCG"                                               
       , xmlelement(name "code", rtrim(PR53.CDESCRIPTIF))                       
       , xmlelement(name "value", rtrim(PR53.CVDESCRIPT))                       
       )                                                                        
       ) as xattr_NCG                                                           
       from RVPR5300 as PR53                                                    
       where                                                                    
       PR53.CPRESTATION = PR00.CPRESTATION                                      
       )                                                                        
       , xmlelement(name "mngr_NCG", rtrim(PR00.CHEFPROD))                      
       , xmlelement(name "gstSe_NCG", rtrim(PR00.CTYPPREST))                    
       , xmlelement(name "typeSe_NCG",                                          
       case when PR00.CNATPREST = 'SE'                                          
       then case when PR00.WPRODMAJ = 'M' then '1' else '2' end                 
       else '3' end                                                             
       concat                                                                   
       case NATUR.LPARAM                                                        
       when 'SRV' then '1'                                                      
       when 'BIC' then '2'                                                      
       when 'DEN' then '3'                                                      
       end                                                                      
       )                                                                        
       --Service attributes (RTPR03 sans INOTR pour default-MAJ)                
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "attrSe_NCG"                                             
       , xmlelement(name "code", rtrim(INOTR.CGESTION))                         
       , xmlelement(name "value",                                               
       case                                                                     
       when PR03.CGESTION is not null then '1'                                  
       else                                                                     
       case INOTR.TYPE                                                          
       when 'B' then '0'                                                        
       when 'A' then ''                                                         
       when 'N' then '0'                                                        
       end                                                                      
       end                                                                      
       )                                                                        
       )                                                                        
       )                                                                        
       from INOTR                                                               
       left join (                                                              
       select                                                                   
       CGESTION                                                                 
       from RVPR0300                                                            
       where                                                                    
       CPRESTATION = PR00.CPRESTATION                                           
       ) PR03 on                                                                
       PR03.CGESTION = INOTR.CGESTION                                           
       where                                                                    
       INOTR.TABLE = 'RTPR03'                                                   
       and (                                                                    
       (INOTR.NATUR = '***' and INOTR.SEQ = 1)                                  
       or (INOTR.NATUR = NATUR.LPARAM)                                          
       )                                                                        
       )                                                                        
       --Service attributes (RTNV14 sans INOTR pour default-MAJ)                
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "attrSe_NCG"                                             
       , xmlelement(name "code", rtrim(INOTR.CGESTION))                         
       , xmlelement(name "value",                                               
       case                                                                     
       when NV14.CPARAM is not null then                                        
       case INOTR.TYPE                                                          
       when 'B' then                                                            
       case NV14.WPARAM                                                         
       when 'O' then '1'                                                        
       else '0'                                                                 
       end                                                                      
       when 'A' then rtrim(NV14.LPARAM)                                         
       when 'N' then ltrim(rtrim(NV14.LPARAM))                                  
       end                                                                      
       else                                                                     
       case INOTR.TYPE                                                          
       when 'B' then '0'                                                        
       when 'A' then ''                                                         
       when 'N' then '0'                                                        
       end                                                                      
       end                                                                      
       )                                                                        
       )                                                                        
       )                                                                        
       from INOTR                                                               
       left join (                                                              
       select                                                                   
       CPARAM                                                                   
       , LPARAM                                                                 
       , WPARAM                                                                 
       from RVNV1400                                                            
       where                                                                    
       NENTITE = PR00.CPRESTATION                                               
       ) NV14 on                                                                
       NV14.CPARAM = INOTR.CGESTION                                             
       where                                                                    
       INOTR.TABLE = 'RTNV14'                                                   
       and (                                                                    
       (INOTR.NATUR = '***' and INOTR.SEQ = 1)                                  
       or (INOTR.NATUR = NATUR.LPARAM)                                          
       )                                                                        
       )                                                                        
       --Service attributes (PRALP)                                             
       , (                                                                      
       select                                                                   
       xmlelement(name "attrSe_NCG"                                             
       , xmlelement(name "code", 'PRALP')                                       
       , xmlelement(name "value", substr(PRALP.WTABLEG, 1, 13))                 
       )                                                                        
       from RVGA0100 PRALP                                                      
       where                                                                    
       PRALP.CTABLEG1 = 'PRALP'                                                 
       and PRALP.CTABLEG2 = PR00.CPRESTATION                                    
       )                                                                        
       --file_NCG                                                               
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "file_NCG"                                               
       , xmlelement(name "code", rtrim(BS25.CDOSSIER))                          
       , xmlelement(name "seq", int(BS25.NSEQDOS))                              
       , xmlelement(name "from",                                                
       timestamp_format(BS25.DEFFET, 'YYYYMMDD'))                               
       , case when BS25.DFINEFFET = '99999999' then null                        
       else xmlelement(name "to",                                               
       timestamp_format(BS25.DFINEFFET, 'YYYYMMDD'))                            
       end                                                                      
       )                                                                        
       ) as xfile_NCG                                                           
       from RVBS2500 as BS25                                                    
       where                                                                    
       BS25.CTYPENT = PR00.CNATPREST                                            
       and BS25.NENTITE = PR00.CPRESTATION                                      
       )                                                                        
       --supplr_NCG                                                             
       , (                                                                      
       select                                                                   
       xmlagg(                                                                  
       xmlelement(name "supplr_NCG"                                             
       , xmlelement(name "code", rtrim(PR55.NENTCDE))                           
       , xmlelement(name "main"                                                 
       , case when PR55.WENTCDE in ('R', 'C') then 1 else 0 end)                
       --INFO: non g�r�                                                         
       , xmlelement(name "lead", 0)                                             
       )                                                                        
       ) as xsupplr_NCG                                                         
       from RVPR5500 as PR55                                                    
       where                                                                    
       PR55.CPRESTATION = PR00.CPRESTATION                                      
       )                                                                        
       , xmlelement(name "assor_NCG", rtrim(PR00.CASSORT))                      
       , (                                                                      
       select                                                                   
       xmlconcat(                                                               
       xmlelement(name "assorNxt_NCG", rtrim(assor.LSTATUT))                    
       , xmlelement(name "assorNDt_NCG"                                         
       , timestamp_format(assor.DEFFET, 'YYYYMMDD'))                            
       )                                                                        
       from assor                                                               
       where                                                                    
       assor.CPRESTATION = PR00.CPRESTATION                                     
       order by                                                                 
       assor.DEFFET asc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , xmlelement(name "vat_NCG", rtrim(PR00.CTAUXTVA))                       
       , xmlelement(name "priceInv_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(PR11.MTPRMP as char(8))),',','.')                     
       from PR11                                                                
       where                                                                    
       PR11.CPRESTATION = PR00.CPRESTATION                                      
       order by                                                                 
       PR11.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "priceSgn_NCG", rtrim(PR00.CSIGNE))                    
       , xmlelement(name "priceNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(PR10.MTPREST as char(8))),',','.')                    
       from PR10                                                                
       where                                                                    
       PR10.CPRESTATION = PR00.CPRESTATION                                      
       order by                                                                 
       PR10.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "bonusNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(PR12.MTPRIME as char(8))),',','.')                    
       from PR12                                                                
       where                                                                    
       PR12.CPRESTATION = PR00.CPRESTATION                                      
       order by                                                                 
       PR12.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "valord_NCG", int(1))                                  
       , xmlelement(name "comment_account", '')                                 
       )                                                                        
       )                                                                        
       , xmlelement(name "creatd",                                              
       timestamp_format(PR00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "modifd",                                              
       timestamp_format(PR00.DMAJ, 'YYYYMMDD'))                                 
       --INFO:non g�r�                                                          
       , xmlelement(name "userMod",                                             
       xmlattributes('true' as "xsi:nil"), '')                                  
       --INFO:non g�r�                                                          
       , xmlelement(name "nature", 'service')                                   
       , xmlelement(name "brand_NCG", rtrim(PR00.CMARQ))                        
       --TODO: v�rifier alimentation refMnf                                     
       , xmlelement(name "refMnf", rtrim(PR00.LPRESTATION))                     
       , xmlelement(name "lblMrkt"                                              
       , xmlattributes('fr' as "locale")                                        
       , rtrim(PR00.LPRESTATION)                                                
       )                                                                        
       , xmlelement(name "refSale"                                              
       , xmlattributes('fr' as "locale")                                        
       , rtrim(PR00.LPRESTATION)                                                
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "categInt",                                            
       xmlattributes('true' as "xsi:nil"), '')                                  
       , xmlelement(name "tax"                                                  
       , xmlelement(name "country", 'FR')                                       
       , xmlelement(name "code", 'TVA')                                         
       )                                                                        
       --INFO:launch occurs: inconnu = 0                                        
       -- ) as XPRODUCT                                                         
       )                                                                        
       )) as clob(50K) including xmldeclaration) as XCODIF                      
       from RVPR0001 as PR00                                                    
       --Famille NCG                                                            
       inner join RVGA1401 GA14 on                                              
       GA14.CFAM = PR00.CFAM                                                    
       inner join RVGA0300 GA03 on                                              
       GA03.NCODIC = PR00.CPRESTATION                                           
       and GA03.CSOC = 'DAR'                                                    
       inner join RVNV1400 as NATUR on                                          
       NATUR.NENTITE = PR00.CPRESTATION                                         
       and NATUR.CPARAM = 'NATUR'                                               
       --                                                                       
       inner join rvad0500 ad05 on                                              
        pr00.cprestation = ad05.ncodic                                          
       and ad05.copco = 'DAR'                                                   
       and ad05.cdata = 'BXM101-SE'                                             
       and ad05.ddata = :W-DJOUR                                                
           END-EXEC                                                             
                                                                                
