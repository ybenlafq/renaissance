      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                *        
      *  PROJET     : ECOMMERCE                                        *        
      *  PGM        : MEC002 (RENVOI RAYON POUR ARTICLE/CETAT)         *        
      *  LONGUEUR   : 4096                                             *        
      *                                                                *        
      *                                                                *        
      ******************************************************************        
      *  DATE       : 12/11/2014                                                
      *  AUTEUR     : Y SCOPIN (DSA004)                                         
      *  REPAIRE    : YS12K4                                                    
      *  OBJET      : AGRANDISSEMENT TABLEAU DES FAMILLES A 1000                
      ******************************************************************        
      *                                                                         
       01  COMM-EC002.                                                          
      *                                                                         
      *    CHAMPS PASSES POUR EFFECTUER LA DEMANDE                              
      *-----------------------------------------------------------------        
      *                                                                         
           02 COMM-EC002-CETAT          PIC  X(010).                            
           02 COMM-EC002-NCODIC         PIC  X(007).                            
           02 COMM-EC002-CFAM           PIC  X(005).                            
      *                                                                         
      *    Zones de réponse                                                     
      *-----------------------------------------------------------------        
           02 COMM-EC002-CODE-RETOUR    PIC  9(01).                             
            88 COMM-EC002-OK            VALUE   0.                              
            88 COMM-EC002-KO            VALUE   1.                              
           02 COMM-EC002-CAGR           PIC  X(05).                             
           02 COMM-EC002-WSEQPRO        PIC  9(07).                             
           02 COMM-EC002-LIBERR         PIC  X(30).                             
      *                                                                         
      *    Zones de travail                                                     
           02 COMM-EC002-CETAT-P        PIC  X(010).                            
      *-----------------------------------------------------------------        
      *                                                                         
      *    Familles avec décomposition marketing                                
           02 COMM-EC002-CMK-ZONE.                                              
              05 COMM-EC002-CMK-NBP        PIC 9(5).                            
      *       05 COMM-EC002-CMK-POSTE      OCCURS 750.                          
              05 COMM-EC002-CMK-POSTE      OCCURS 1000.                         
                 07 COMM-EC002-CMK-CFAM    PIC X(05).                           
                 07 COMM-EC002-CMK-CMK     PIC X(05) OCCURS 3.                  
                 07 COMM-EC002-CMK-WSEQPRO PIC  9(07).                          
                 07 COMM-EC002-CMK-CAGR    PIC X(05).                           
      *                                                                         
      *                                                                         
      *    Rayon/Seqpro par composition marketing                               
      *    Zones de travail                                                     
      *-----------------------------------------------------------------        
           02 COMM-EC002-CFAM-ZONE.                                             
              05 COMM-EC002-CFAM-NBP    PIC 9(5).                               
      *       05 COMM-EC002-CFAM-POSTE OCCURS    500.                           
              05 COMM-EC002-CFAM-POSTE OCCURS   1000.                           
                 07 COMM-EC002-CFAM-CFAM         PIC X(5).                      
                 07 COMM-EC002-CFAM-CVMK         PIC X(05) OCCURS 3.            
                 07 COMM-EC002-CFAM-CAGR         PIC X(5).                      
                 07 COMM-EC002-CFAM-WSEQPRO      PIC 9(7).                      
      *                                                                         
      *                                                                         
                                                                                
