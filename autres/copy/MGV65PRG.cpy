      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      * -------------------------------------------------------------- *        
      * CE PROGRAMME A ETE TRANSFORME EN COPY.                         *        
      * LE MODE DE FONCTIONNEMENT EST LE MEME QUE LES MECCC (COMMANDES *        
      * INTERNET).                                                     *        
      * CETTE COPY EST UTILISEE PAR LE MGV65  ET PAR LES MGV65X        *        
      * QUI TOURNENT SUR L'ENVIRONNEMENT DE PARIS MAIS BINDE SUR LES   *        
      * DIFFERENTES FILIALES.                                          *        
      * -------------------------------------------------------------- *        
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : MGV65                                            *        
      *  FONCTION   : EN MODE CONTROLE (APPELER PAR LE MGV21).         *        
      *                   - RECHERCHE D'UN ENTREPOT.                   *        
      *                   - RECHERCHE D'UNE MUTATION.                  *        
      *                   - DEMANDE DE CONTROLE SUR STOCK DACEM.       *        
      *                                                                *        
      *               EN MODE RESERVATION (APPELER PAR MGV22).         *        
      *                   - RESERVATION SUR STOCK ENTREPOT.            *        
      *                   - CREATION D'UNE MUTATION TEMPORAIRE.        *        
      *                   - RIEN POUR DACEM.                           *        
      *                                                                *        
      *               EN MODE                                                   
      *                   - PLACE LA VENTE SUR UNE MUTATION (VERITABLE)*        
      *                   - LANCEMENT DE LA COMMANDE DACEM.            *        
      *                                                                *        
      * nouvelle version, on garde le mode controle, mais on supprime  *        
      * les autres modes.                                              *        
      * desormais, le programme fait ce qu'on lui demande suivant la   *        
      * maniere dont sont renseign�es les zones suivantes :            *        
      * COMM-GV65-ACT-DACEM :                                          *        
      *      si = 'N', ne fait rien                                    *        
      *      si = 'C', envoie une reservation chez Dacem               *        
      *      si = 'S', envoie d'une annulation de reservation chez     *        
      *                Dacem                                           *        
      *      si = 'M', envoie d'un changement de date de reservation   *        
      *                chez Dacem                                      *        
      * COMM-GV65-MAJ-STOCK :                                          *        
      *      si = 'N', ne fait rien                                    *        
      *      si = '+', on cherche le lieu de stockage                  *        
      *              -si article en stock, on fait + QTE sur           *        
      *               GS10-QSTOCKRES                                   *        
      *              -si article en CF, on fait + QTE sur GF55-QCDERES *        
      *              -si article sur mut magique ou mut B2B, on met �  *        
      *               jour la mut magique ou la mut B2B en d�cr�mentant*        
      *               la quantit� de GB15-QDEMANDEE (ou on supprime si *        
      *               GB15-QDEMANDEE = QTE)                            *        
      *      si = '-',                                                 *        
      *              -si article sur GS10, on fait - QTE sur           *        
      *               GS10-QSTOCKRES                                   *        
      *              -si article en CF, on dereserve sur GF55          *        
      *                                                                *        
      * COMM-GV65-CRE-GB05-TEMPO                                       *        
      *      si = 'N', ne fait rien                                    *        
      *      si = 'O', on cherche le lieu de stockage. si l'article    *        
      *                est dispo, en CF ou sur mut magique, on cr�� la *        
      *                mutation temporaire (GB05 + GB15)               *        
      *                                                                *        
      * COMM-GV65-SUP-MUT-GB15                                         *        
      *      si = 'N', ne fait rien                                    *        
      *      si = 'O', on acc�de � la mut gb15, si on la trouve, si    *        
      *                elle n'est pas valid� et si c'est une mut tempo *        
      *                ou si la vente est complete, on met � jour ou   *        
      *                on supprime GB15 puis on met � jour GB05. on    *        
      *                force COMM-GV65-SUP-MUT-GB05 � 'N' pour ne pas  *        
      *                la trait� une deuxieme fois.                    *        
      *                                                                *        
      * COMM-GV65-SUP-MUT-GB05                                         *        
      *      si = 'N', ne fait rien                                    *        
      *      si = 'O', on acc�de � la mut, on sauvegarde nsoclivr et   *        
      *                ndepot et on supprime la mut                    *        
      *                                                                *        
      * COMM-GV65-CRE-GB05-FINAL                                       *        
      *      si = 'N', ne fait rien                                    *        
      *      si = 'O', on recherche la mutation finale, si on la       *        
      *                trouve, on regarde si on doit aussi cr�er la    *        
      *                GB15, si oui, on l� cr��, puis on met � jour    *        
      *                GB05                                            *        
      *                                                                *        
      * COMM-GV65-INTERNET                                             *        
      *      si = 'N', ne fait rien                                    *        
      *      si = 'O', on met � jour GV11 et GV22 avec le lieu de      *        
      *                livraison.                                      *        
      *                                                                *        
      *                                                                *        
      *                                                                *        
      * -------------------------------------------------------------- *        
      *                                                                *        
      ******************************************************************        
LD0709* 07/07/09*LIOD    * CORRECTION BUG DANS LE CAS DE STOCK SUR DEUX*        
LD7009*         *        * ENTREPOT POUR UNE MEME FAMILLE              *        
LD0709*         *        * RAJOUT DU CONTROLE DE DATE POUR DACEM       *        
LD0709*         *        * PASSAGE DE J A J+1 OBLIGATOIRE EN SAISIE    *        
LD0709*         *        * RAJOUT DU LOG DU MESSAGE ENVOYER VERS DACEM *        
LD0709*         *        * SI PARAMETRE NCGFC EN 12 SUR 1 = OUI        *        
      ******************************************************************        
      ******************************************************************        
NV0910*  14102009 / DSA196 / NV0910                                             
NV0910*  IMPUTATION DES VENTES SUR LES COMMANDES FOURNISSEURS EXPEDIES          
      ******************************************************************        
LD0210*  05022010 / DSA044 /                                                    
LD0210*  MODIFICATION DU CLIENT DACEM POUR LE B2B                               
      ******************************************************************00600001
LD0310*  MODIFICATION                                                  *        
LD0310*  DATE       : 26/03/2010                                       *        
LD0310*  AUTEUR     : DSA029                                           *        
LD0310*  DESCRIPTIF : MODIFICATION POUR COMMANDE FOURNISSEURS SUR EMRX *        
LD0310*               PASSAGE RVGV2202 A RVGV2203 ADD ALIM NVELLES ZONE*        
      ******************************************************************        
LD0410*  DATE       : 26/03/2010                                       *        
LD0410*  AUTEUR     : DSA029                                           *        
LD0410*  DESCRIPTIF : MODIFICATION CHRONOPOST                          *        
      ******************************************************************        
L20410*  DATE       : 26/03/2010                                       *        
L20410*  AUTEUR     : DSA029                                           *        
L20410*  DESCRIPTIF : CORECTION COMMANDE FOURNISSEUR                   *        
      ******************************************************************        
V34CEN*  DATE       : 20/08/2010                                       *        
      *  AUTEUR     : DE01004                                          *        
      *  DESCRIPTIF : COMMANDES INCOMPLETES : MODE CONTROLE, MODE      *        
      *             : MAJ-DACEM, MODE SUPPRESSION                      *        
      ******************************************************************        
V34R4 *  DATE       : 10/01/2011                                       *        
      *  AUTEUR     : DE02074                                          *        
      *  DESCRIPTIF : nouvelle gestion                                 *        
      ******************************************************************        
V41R0 *  DATE       : 14/03/2011                                       *        
      *  AUTEUR     : DE02074                                          *        
      *  DESCRIPTIF :-activation du code qui etait en commentaire a    *        
      *             : tort si codic absent dans GA64                   *        
      *             :-correction du ndepot qui etait mal aliment� pour *        
      *             : les ventes en 'z'                                *        
      *             :-ajout date dans requete de consultation dacem    *        
      *             :-initialisation a space de la zone REFCRITERE-21  *        
      *             : qui etait mal initialis�e dans le cas d'une      *        
      *             : consultation gv00                                *        
      ******************************************************************        
V41R1 *  DATE       : 08/06/2011                                       *        
v41r1 *  AUTEUR     : DE02074                                          *        
v41r1 *  DESCRIPTIF :-on renseigne le lieu entrepot meme si on a pas   *        
v41r1 *             : trouver l'article pour que la vente soit bien    *        
v41r1 *             : renseign�e et puisse �tre trait�e lors des       *        
v41r1 *             : receptions fournisseur                           *        
v41r1 *             :-ajout de la date dans l'envoie de creation de    *        
v41r1 *             : lignes dacem                                     *        
v41r1 *             :-initialisation de la zone refcritere-21          *        
v41r1 *             :-mise � jour de la zone comment-21 avec le num�ro *        
v41r1 *             : de commande dacem pour les requetes de mise �    *        
v41r1 *             : jour et de sppression dacem                      *        
      ******************************************************************        
V41R2 *  DATE       : 28/06/2011                                       *        
      *  AUTEUR     : DE02074                                          *        
      *  DESCRIPTIF :si comm-gv65-resa-a-j = 'o', on envoi la resa     *        
      *             : dacem � la date du jour, si 'n' on envoi la resa *        
      *             : � la date ddeliv plus correction mut b2b         *        
      ******************************************************************        
V43R0 *  DATE       : 09/08/2011                                       *        
      *  AUTEUR     : DE02074                                          *        
      *  DESCRIPTIF :gestion contremarque dacem                        *        
      *             :MODIFICATION DE MESSAGES DANS LA LOG              *        
      *             :MODIF GESTION DATE DISPO POUR CF GF55-DCALCUL     *        
      *             :BLOCAGE VENTE KIALA SI PAS DE N� DE RELAIS        *        
      *             :CREATION DES MUT FINALE A J OU J+1 POUR VENTE     *        
      *             :COMPLETE                                          *        
      ******************************************************************        
V43R1 *  DATE       : 02/11/2011                                       *        
      *  AUTEUR     : DE02074                                          *        
      *  DESCRIPTIF :suppression du blocage des ventes kiala sans      *        
      *             :relais id pour les ventes internet                *        
      ******************************************************************        
V43R2 *  DATE       : 04/11/2011                                       *        
      *  AUTEUR     : DE02074                                          *        
      *  DESCRIPTIF :mise a kour de la zone COMM-GV65-DMUTATION pour   *        
      *             :les chronoposte                                   *        
      ******************************************************************        
V50R0 *  DATE       : 27/01/2012                                       *        
      *  AUTEUR     : DE02074                                          *        
      *  DESCRIPTIF :MISE EN PLACE PARAM�TRAGE POUR FORCAGE DDELIV     *        
      *             :modification de la gestion des mutations pour     *        
      *             :le rattrapage des ventes suite mise en place      *        
      *             :generix                                           *        
      ******************************************************************        
V50R1 *  DATE       : 09/03/2012                                       *        
      *  AUTEUR     : J BICHY                                          *        
      *  DESCRIPTIF :autoriser B2B a saisir dans la mut magique        *        
      ******************************************************************        
eg2003*  DATE       : 20/03/2012                                       *        
      *  AUTEUR     : e gabetty                                        *        
      *  DESCRIPTIF :gestion contremarque                              *        
      ******************************************************************        
V52R0 * 06/04/2012 ! DE02074 nouvelle gestion des ventes incompletes   *        
      ******************************************************************        
V52R1 * 02/08/2012 ! J BICHY correctif pour alimenter gs10 stockres    *        
      ******************************************************************        
       01  FILLER                    PIC X(16) VALUE '*** WORKING  ***'.        
       01 P-EIBTASKN PIC S9(7) PACKED-DECIMAL.                                  
       01 X-EIBTASKN REDEFINES P-EIBTASKN PIC X(4).                             
V43R0  01  W-NSEQNQ                     PIC  9(2).                              
V43R0  01  W-EDIT-QTE                   PIC -9(6).                              
V50R0  01  W-EDIT-QTERES                PIC -9(6).                              
V41r0  01  W-NMUTTEMP                   PIC X(9).                               
V34CEN 01  W-GV65-DDELIV                PIC X(8).                               
       01  WS-DATE-JOUR                 PIC X(8).                               
v50r0  01  WS-DATE-JOUR-dacem           PIC X(8).                               
       01  W-DDELIV-INITIALE            PIC X(8).                               
v50r1  01  W-MUT-B2B-MAG                PIC X(7).                               
       01  W-EC05                       PIC X(1).                               
V52R0  01  EDIT-SQL                     PIC -999999999.                         
      * -> jour de la semaine commande gf55.                                    
       01  w-j-sem                 PIC S9(09) comp-3   VALUE 0.                 
       01  w-indic-var.                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *  05  w-f-sem-f               PIC S9(4) COMP.                            
      *--                                                                       
         05  w-f-sem-f               PIC S9(4) COMP-5.                          
      *}                                                                        
      *    FORMAT SSAAMMJJ                                                      
V41R0  01  W-COMPTEUR                   PIC 9(3).                               
  !    01  FILLER.                                                              
  !      04  WCTABLEG1 PIC X(005).                                              
  !      04  WCTABLEG2 PIC X(015).                                              
V41R0    04  WWTABLEG  PIC X(080).                                              
       01  w-CICS-APPLID     PIC X(8).                                          
       01  w-CICS-NETNAM     PIC X(8).                                          
V50R0  01  WS-DATE-JOUR-O            PIC X(8).                                  
V50R0  01  WS-DATE-JOUR-J            PIC X(8).                                  
V52R0  01  W-NUM-RESA                PIC X(7).                                  
v52r0  01  W-FILIERE                 PIC X(2).                                  
v52r0  01  W-DELAI                   PIC 9(2).                                  
V50R0  01  W-NB-TENTATIVES           PIC 9(2).                                  
V50R0 *01  I-T                       PIC 9(2).                                  
V50R0 *01  W-MUT-GENERIX             PIC X(01) VALUE 'N'.                       
V50R0  01  W-MODIF-GENERIX           PIC X(01) VALUE 'N'.                       
V50R0 *01  W-SAVE-NMUTATION          PIC X(07).                                 
eg2003 01  WS-DATE-JOUR-PLUS15       PIC X(8).                                  
LD0709 01  WS-DATE-JOUR-PLUS1        PIC X(8).                                  
LD0709 01  WNMODULE                  PIC X(8).                          00000470
V50R0  01  W-STOCKAGE                 PIC  X.                                   
V50R0      88 LIEU-STOCK-CONNU            VALUE 'O'.                            
V50R0      88 LIEU-STOCK-INCONNU          VALUE 'N'.                            
V52R0  01  W-OLD-NEW                 PIC X(01) VALUE 'O'.                       
V52R0      88 ANCIENNE-VERSION                 VALUE 'O'.                       
V52R0      88 NOUVELLE-VERSION                 VALUE 'N'.                       
V50R0  01  W-APPEL-DACEM              PIC  X.                                   
V50R0      88 APPEL-DACEM-OUI              VALUE 'O'.                           
V50R0      88 PAS-APPEL-DACEM              VALUE 'N'.                           
v50r1  01  W-GB15-QMUTEE           PIC  9(05)     VALUE ZEROES.                 
V50R1  01  W-GB15-QCDEPREL         PIC  9(05)     VALUE ZEROES.                 
LD0709                                                                          
LD0709 01  WS-B2B-MUT-MAG            PIC x(01) VALUE '0'.                       
LD0709     88 WS-B2B-MUT-MAG-NON               VALUE '0'.                       
LD0709     88 WS-B2B-MUT-MAG-OUI               VALUE '1'.                       
LD0709                                                                          
LD0709 01  WS-B2B-DACEM              PIC x(01) VALUE '0'.                       
LD0709     88 WS-B2B-DACEM-NON                 VALUE '0'.                       
LD0709     88 WS-B2B-DACEM-OUI                 VALUE '1'.                       
LD0709                                                                          
LD0709 01  WS-B2B-NUM-DACEM          PIC X(06) VALUE SPACE.                     
LD0709                                                                          
LD0709 01  WS-LOG-UP                 PIC x(01) VALUE '0'.                       
LD0709     88 WS-LOG-UP-NON                    VALUE '0'.                       
LD0709     88 WS-LOG-UP-OUI                    VALUE '1'.                       
V50R0  01  WS-FORCAGE-DDELIV         PIC X(01) VALUE 'N'.                       
V50R0      88 DDELIV-FORCE                     VALUE 'O'.                       
V50R0      88 DDELIV-PAS-FORCE                 VALUE 'N'.                       
V50R0      88 DDELIV-DACEM                     VALUE 'D'.                       
V50R0      88 DDELIV-FORCE-DACEM               VALUE 'F'.                       
       01  TOP-DISPLAY               PIC X(01) VALUE 'N'.                       
           88 AUTO-DISPLAY                     VALUE 'O'.                       
           88 PAS-DE-DISPLAY                   VALUE 'N'.                       
       01  TOP-MDLIV                 PIC 9(01) VALUE 0.                         
           88 MDLIV-NON-TROUVE                 VALUE 0.                         
           88 MDLIV-TROUVE                     VALUE 1.                         
       01  TOP-RTGA00                PIC 9(01) VALUE 0.                         
           88 RTGA00-NON-TROUVE                VALUE 0.                         
           88 RTGA00-TROUVE                    VALUE 1.                         
V41R0  01  TOP-MUTATION            PIC X(01).                                   
V41R0      88 MUTATION-FINAL                   VALUE '1'.                       
V41R0      88 MUTATION-TEMPO                   VALUE '0'.                       
V41R0  01  TOP-PARAM               PIC X(01).                                   
V41R0      88 PARAM-TROUVE                     VALUE '1'.                       
V41R0      88 PARAM-NON-TROUVE                 VALUE '0'.                       
      *{Post-Translation Transform-Var-Varchar
LD0709* 01  WS-MESS-MGV65            PIC X(4000).                                
       01 WS-MESS-MGV65.
         49 WS-MESS-MGV65-LEN PIC S9(4) COMP-5 VALUE 4000.
         49 WS-MESS-MGV65-TEXT PIC X(4000).
      *} Post-Translation
      *
       01  W-EIBTIME        PIC 9(7).                                           
       01  FILLER REDEFINES W-EIBTIME.                                          
           02  FILLER    PIC X.                                                 
           02  W-HEURE   PIC XX.                                                
           02  W-MINUTE  PIC XX.                                                
           02  W-SECONDE PIC XX.                                                
       01  W-HHMM.                                                              
           05  W-HH      PIC 9(2).                                              
           05  W-MM      PIC 9(2).                                              
       01  W-HHMM9 REDEFINES W-HHMM PIC 9(04).                                  
       01  W-H-POST-GENERIX.                                                    
           05  W-POST-HH  PIC 9(2).                                             
           05  W-POST-MM  PIC 9(2).                                             
           05  W-POST-SS  PIC 9(2).                                             
       01  W-H-PRE-GENERIX.                                                     
           05  W-PRE-HH  PIC 9(2).                                              
           05  W-PRE-MM  PIC 9(2).                                              
           05  W-PRE-SS  PIC 9(2).                                              
       01  Z-HHMM.                                                              
           05  Z-HH      PIC 9(2).                                              
           05  Z-MM      PIC 9(2).                                              
       01  Z-HHMM9 REDEFINES Z-HHMM PIC 9(04).                                  
       01  W-MESSAGE-S.                                                         
           02  FILLER      PIC X(5) VALUE 'MGV65'.                              
           02  W-MESSAGE-H PIC X(2).                                            
           02  FILLER      PIC X(1) VALUE ':'.                                  
           02  W-MESSAGE-M PIC X(2).                                            
           02  FILLER      PIC X(1) VALUE ' '.                                  
           02  W-MESSAGE   PIC X(69).                                           
      *                     N. COMMANDE WCPE                                    
       01  WEC02-NCDEWC          PIC S9(15)V USAGE COMP-3.                      
V34CEN 01  W-COMMENT-21              PIC X(9).                                  
V50R0  01  W-NB-JOUR-O               PIC 9(1).                                  
V50R0  01  W-NB-JOUR-J               PIC 9(1).                                  
V34CEN 01  W-VENTE-COMPLETE                  PIC 9(01).                         
           88 VENTE-INCOMPLETE                     VALUE 0.                     
           88 VENTE-COMPLETE                       VALUE 1.                     
V34CEN 01  W-SUPPR-GB15                      PIC 9(01).                         
           88 SUPPR-GB15-OK                        VALUE 0.                     
           88 SUPPR-GB15-NT                        VALUE 1.                     
           88 SUPPR-GB15-VALIDEE                   VALUE 3.                     
       01  WS-DEPOT.                                                            
           05 WS-NSOCDEPOT           PIC X(03).                                 
           05 WS-NDEPOT              PIC X(03).                                 
       01  WRCH-STOCK                PIC 9(01) VALUE 0.                         
           88 WSTOCK-KO                        VALUE 0.                         
           88 WSTOCK-OK                        VALUE 1.                         
       01  WRCH-MAGIE                PIC 9(01) VALUE 0.                         
           88 WMAGIE-KO                        VALUE 0.                         
           88 WMAGIE-OK                        VALUE 1.                         
V50R1  01  WS-TOP-MUT-MAGIC           PIC 9                 VALUE 0.            
V50R1      88 MAGIE-OK                            VALUE 1.                      
V50R1      88 MAGIE-KO                            VALUE 0.                      
V50R1  01  WS-TOP-MUT-SUR-B2B         PIC 9                 VALUE 0.            
V50R1      88 MUT-SUR-B2B-OK                      VALUE 1.                      
V50R1      88 MUT-SUR-B2B-KO                      VALUE 0.                      
V50R1  01  WS-TOP-VTE-B2B             PIC 9                 VALUE 0.            
V50R1      88 VENTE-B2B-OK                        VALUE 1.                      
V50R1      88 VENTE-B2B-KO                        VALUE 0.                      
NV0910 01  WS-RESER-FLAG             PIC 9(01) VALUE 0.                         
NV0910     88 WS-RESER-FOURN-KO                VALUE 0.                         
NV0910     88 WS-RESER-FOURN-OK                VALUE 1.                         
NV0910 01  WS-TOP-DATE-VALIDE        PIC 9(01) VALUE 0.                         
NV0910     88 WS-DATE-VALIDE                   VALUE 0.                         
NV0910     88 WS-DATE-INVALIDE                 VALUE 1.                         
       01  WVTE-INTERNET             PIC 9(01) VALUE 0.                         
           88 WVTEI-KO                         VALUE 0.                         
           88 WVTEI-OK                         VALUE 1.                         
V41R0  01  W-TYPE-VENTE              PIC 9(01).                                 
V41R0  01  W-TYPE-VENTEX REDEFINES W-TYPE-VENTE PIC X(01).                      
V41R0  01  WTYPE-VENTE               PIC 9(01) VALUE 0.                         
  !        88 VENTE-INDETERMINE                VALUE 0.                         
  !        88 VENTE-B2B                        VALUE 1.                         
  !        88 VENTE-INTERNET                   VALUE 2.                         
V41R0      88 VENTE-MAGASIN                    VALUE 3.                         
       01  WS-QUANTITE-DISPONIBLE     PIC S9(06)           VALUE ZEROES.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  IDEP                       PIC S9(04) COMP      VALUE ZEROES.        
      *--                                                                       
       01  IDEP                       PIC S9(04) COMP-5      VALUE              
                                                                 ZEROES.        
      *}                                                                        
V34R4  01  W-SQLCODE                  PIC  9(04).                               
V43R0  01  W-DATE-ACTIVATION-CQE      PIC X(8).                                 
V43R0  01  W-DATE-ACTIVATION-GF55     PIC X(8).                                 
V43R0  01  W-TOP-CONTROLE-EC03        PIC X(1) VALUE 'N'.                       
       01  WS-HV-DMUTATION-1          PIC X(8).                                 
       01  WS-HV-DMUTATION-2          PIC X(8).                                 
       01  WS-CMODSTOCK               PIC X(05)            VALUE SPACES.        
V34CEN 01  WS-DELAI               PIC 9(02)        VALUE ZEROES.                
V52R0  01  WS-DELTA-DDELIV        PIC 9(02)        VALUE ZEROES.                
       01  WS-TOP-MUTATION            PIC 9(01)            VALUE 0.             
           88 WS-MUTATION-NON-TROUVEE                      VALUE 0.             
           88 WS-MUTATION-AFFECTEE                         VALUE 1.             
       01  WS-INDIC-MUT-TEMP          PIC 9(01).                                
           88 MUT-TEMPORAIRE-KO                            VALUE 0.             
           88 MUT-TEMPORAIRE-OK                            VALUE 1.             
HV     01  WS-NB-PRODUIT             PIC S9(05) COMP-3     VALUE ZEROES.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
090306*01  WS-NB-PRODUIT-F           PIC S9(04) COMP-4     VALUE ZEROES.        
      *--                                                                       
       01  WS-NB-PRODUIT-F           PIC S9(04) COMP-5     VALUE ZEROES.        
      *}                                                                        
       01  EDT-QTE                    PIC  9(05).                               
       01  EDT-sql                    PIC -9(10).                               
V43R0  01  WSIGNE                     PIC  X(01).                               
       01  WQTE                       PIC S9(05)    COMP-3 VALUE ZEROES.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WQTE-F                     PIC S9(04)    COMP   VALUE ZEROES.        
      *--                                                                       
       01  WQTE-F                     PIC S9(04) COMP-5   VALUE ZEROES.         
      *}                                                                        
       01  WQVOLUME                   PIC S9(11)    COMP-3 VALUE ZEROES.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WQVOLUME-F                 PIC S9(04)    COMP   VALUE ZEROES.        
      *--                                                                       
       01  WQVOLUME-F                 PIC S9(04) COMP-5   VALUE ZEROES.         
      *}                                                                        
       01  WQNBPIECES                 PIC S9(05)    COMP-3 VALUE ZEROES.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WQNBPIECES-F               PIC S9(04)    COMP   VALUE ZEROES.        
      *--                                                                       
       01  WQNBPIECES-F               PIC S9(04) COMP-5   VALUE ZEROES.         
      *}                                                                        
       01  WQNBLIGNES                 PIC S9(05)    COMP-3 VALUE ZEROES.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WQNBLIGNES-F               PIC S9(04)    COMP   VALUE ZEROES.        
      *--                                                                       
       01  WQNBLIGNES-F               PIC S9(04) COMP-5   VALUE ZEROES.         
      *}                                                                        
       01  WQVOLUME1                 PIC S9(9)V9(6) COMP-3 VALUE ZEROES.        
       01  WQVOLUMEM3                 PIC S9(5)V99  COMP-3 VALUE ZEROES.        
NV0910 01  IND-TABC                   PIC 99               VALUE ZEROES.        
  !    01  IND-CDE                    PIC 99               VALUE 2.             
  !                                                                             
  !    01  WS-TABLE-CDE.                                                        
  !        05 WS-POSTE-CDE     OCCURS 2.                                        
  !           10 WS-DCDE-FNR          PIC X(06).                                
  !           10 WS-FIL1-FNR          PIC X.                                    
  !           10 WS-QCDE-FNR          PIC Z(04).                                
  !           10 WS-FIL2-FNR          PIC X.                                    
NV0910     05 FILLER                  PIC X(04)            VALUE SPACES.        
NV0910 01  WS-DCDE7                   PIC  X(08)           VALUE SPACES.        
NV0910 01  WS-DJOUR7                  PIC  X(08)           VALUE SPACES.        
NV0910 01  WS-DRECEPT                 PIC  X(08)           VALUE SPACES.        
NV0910 01  WS-DRECEPT6                PIC  X(06)           VALUE SPACES.        
NV0910 01  WS-QUANT-DIS               PIC S9(06)           VALUE ZEROES.        
L20410 01  WS-QUANTITE-A-DERESERVER   PIC  9(03)           VALUE ZEROES.        
      *                                                                         
       01  GA01-CTABLEG2-MAX          PIC X(15)            VALUE SPACES.        
       01  WI                         PIC S9(05) COMP-3 VALUE 0.                
       01  W-APPLID                   PIC X(08).                                
      *01  W-USER                     PIC X(08).                                
       01  WS-WMULTI-MIN              PIC X(1).                                 
       01  WS-WMULTI-MAX              PIC X(1).                                 
       01  WS-WMULTI-VAL              PIC X(1).                                 
LD610  01  WS-MUT                     PIC X(4).                                 
       01  WGB05-NMUTATION            PIC X(7).                                 
       01  9-NMUTATION                PIC 9(7).                                 
       01  CHERCHE-MUT                PIC 9(01).                                
           88 MUT-PAS-OK                        VALUE 0.                        
           88 MUT-OK                            VALUE 1.                        
       01  FILLER                    PIC X(16) VALUE '*** AIDA DB2 ***'.        
           COPY SYKWSQ10.                                                       
           COPY ZLIBERRG.                                                       
           COPY SYKWZCMD.                                                       
           COPY COMMGV65.                                                       
      * -> APPEL CD20                                                           
           COPY SYLU62C.                                                        
      * -> APPEL MQ                                                             
           COPY COMMMQ21.                                                       
           COPY COMMMQ13.                                                       
           COPY COMMPTMG.                                                       
      ******************************************************************        
      * COMM POUR MEC15 (MOUCHARD).                                             
      ******************************************************************        
       01  MEC15C-COMMAREA.                                                     
           COPY MEC15C.                                                         
v5.1.p     COPY MEC15E.                                                         
      ******************************************************************        
      * -> MODULES GENERALISES APPELES PAR LES VENTES                           
           COPY WGV00.                                                          
       01 WS-WDONNEES.                                                          
          05 FILLER                  PIC X(01).                                 
          05 MDLIV-CTYPTRAIT         PIC X(05).                                 
          05 MDLIV-CORIGLIEU         PIC X(05).                                 
          05 MDLIV-GEST              PIC X(06).                                 
          05 MDLIV-DEPLIV            PIC X(06).                                 
          05 MDLIV-WEXP              PIC X(01).                                 
          05 MDLIV-LDEPLIVR-EXP      PIC X(06).                                 
          05 MDLIV-FILLER            PIC X(03).                                 
       01  FILLER                    PIC X(16) VALUE 'Z-COMMAREA-LINK'.         
       01  Z-COMMAREA-LINK           PIC X(50124).                              
           COPY COMMDATC.                                                       
       01  FILLER                    PIC X(16) VALUE '** AIDA ZONES **'.        
           COPY SYKWDIV0.                                                       
           COPY SYKWEIB0.                                                       
           COPY SYKWECRA.                                                       
           COPY SYKWZINO.                                                       
           COPY SYKWERRO.                                                       
           COPY SYKWCWA0.                                                       
       01  FILLER                    PIC X(16) VALUE '* ZONES MODULE *'.        
           COPY SYKWDATE.                                                       
           COPY SYKWMONT.                                                       
           COPY SYKWDATH.                                                       
           COPY SYKWTCTU.                                                       
      * ZONES DE TRAVAIL UTILISEES POUR LA GESTION DE LA TS TSTENVOI            
       77 WC-ID-TSTENVOI          PIC  X(08)      VALUE 'TSTENVOI'.             
       77 WZ-XCTRL-EXPDEL-SQLCODE PIC -Z(7)9      VALUE ZEROS .                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WP-RANG-TSTENVOI        PIC S9(04) COMP VALUE +0.                     
      *--                                                                       
       77 WP-RANG-TSTENVOI        PIC S9(04) COMP-5 VALUE +0.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 IND-RANG-TSTENVOI        PIC S9(04) COMP VALUE +0.                    
      *--                                                                       
       77 IND-RANG-TSTENVOI        PIC S9(04) COMP-5 VALUE +0.                  
      *}                                                                        
       01 WS-TYPENVOI.                                                          
          05 WS-TYPENVOI1         PIC X.                                        
          05 WS-TYPENVOI2         PIC X.                                        
          05 WS-TYPENVOI3         PIC X.                                        
          05 WS-TYPENVOI4         PIC X.                                        
          05 WS-TYPENVOI5         PIC X.                                        
          05 WS-TYPENVOI6         PIC X.                                        
          05 WS-TYPENVOI7         PIC X.                                        
          05 WS-TYPENVOI8         PIC X.                                        
          05 WS-TYPENVOI9         PIC X.                                        
          05 WS-TYPENVOI10        PIC X.                                        
       01 TAB-TYPENVOI REDEFINES WS-TYPENVOI.                                   
          05 WS-TAB-TENVOI        PIC X    OCCURS 10.                           
       01 WF-XCTRL-EXPDEL         PIC X.                                        
          88 WC-XCTRL-EXPDEL-TROUVE      VALUE '0'.                             
          88 WC-XCTRL-EXPDEL-NON-TROUVE  VALUE '1'.                             
          88 WC-XCTRL-EXPDEL-PB-DB2      VALUE '2'.                             
       01 WF-CURS-XCTRL-EXPDEL    PIC X.                                        
          88 WC-XCTRL-EXPDEL-SUITE       VALUE '0'.                             
          88 WC-XCTRL-EXPDEL-FIN         VALUE '1'.                             
       01 WF-TSTENVOI             PIC X.                                        
          88 WC-TSTENVOI-SUITE           VALUE '0'.                             
          88 WC-TSTENVOI-FIN             VALUE '1'.                             
          88 WC-TSTENVOI-ERREUR          VALUE '2'.                             
      * DESCRIPTION DE LA TS.                                                   
       01 TS-TSTENVOI-DONNEES.                                                  
          05 TSTENVOI-NSOCZP.                                                   
             10 TSTENVOI-NSOC                      PIC X(03).                   
             10 FILLER                             PIC X(02).                   
          05 TSTENVOI-CTRL.                                                     
             10 TSTENVOI-CTRL12.                                                
                15 TSTENVOI-CTRL1                  PIC X(01).                   
                15 TSTENVOI-CTRL2                  PIC X(01).                   
             10 FILLER                             PIC X(02).                   
          05 TSTENVOI-WPARAM                       PIC X(10).                   
          05 TSTENVOI-WPARAM-A REDEFINES TSTENVOI-WPARAM.                       
             10 TSTENVOI-CMODDEL                   PIC X(03).                   
             10 TSTENVOI-CTENVOI                   PIC X(01).                   
             10 FILLER                             PIC X(06).                   
          05 TSTENVOI-WPARAM-G REDEFINES TSTENVOI-WPARAM.                       
             10 TSTENVOI-NDEPOT                    PIC X(03).                   
             10 TSTENVOI-WEMPORTE                  PIC X(01).                   
             10 FILLER                             PIC X(06).                   
          05 TSTENVOI-WPARAM-B REDEFINES TSTENVOI-WPARAM.                       
             10 TSTENVOI-SOCIETE                   PIC X(03).                   
             10 TSTENVOI-LIEU                      PIC X(03).                   
             10 FILLER                             PIC X(04).                   
          05 TSTENVOI-LIBELLE                      PIC X(30).                   
          05 TSTENVOI-LIBELLE-A REDEFINES TSTENVOI-LIBELLE.                     
             10 TSTENVOI-FLAG.                                                  
                15 TSTENVOI-WDCOM                  PIC X(01).                   
                15 TSTENVOI-BTOB                   PIC X(01).                   
                15 TSTENVOI-MAG                    PIC X(01).                   
                15 FILLER                          PIC X(02).                   
             10 TSTENVOI-FLAG2 REDEFINES TSTENVOI-FLAG.                         
                15 TSTENVOI-AUTORISATION OCCURS 5.                              
                   20 TSTENVOI-AUTO                PIC X(01).                   
             10 TSTENVOI-CPRESTA                   PIC X(05).                   
             10 FILLER                             PIC X(02).                   
             10 TSTENVOI-WMUTAUTO.                                              
                15 TSTENVOI-WJOURMUTAUTOR OCCURS 7 PIC X.                       
             10 FILLER                             PIC X(02).                   
             10 TSTENVOI-LIB                       PIC X(09).                   
          05 TSTENVOI-LIBELLE-B REDEFINES TSTENVOI-LIBELLE.                     
             10 TSTENVOI-NMUTATION                 PIC X(07).                   
             10 FILLER                             PIC X(23).                   
          05 TSTENVOI-LIBELLE-c REDEFINES TSTENVOI-LIBELLE.                     
             10 TSTENVOI-TYPE                      PIC X(01).                   
             10 TSTENVOI-NUMCLI-DACEM              PIC X(06).                   
             10 TSTENVOI-B2B-MUT-MAG               PIC X(01).                   
             10 TSTENVOI-LOG-DACEM-B2B             PIC X(01).                   
             10 TSTENVOI-B2B-DACEM                 PIC X(01).                   
             10 FILLER                             PIC X(20).                   
       01  FILLER                    PIC X(16) VALUE '* TS MFL05     *'.        
           COPY TSSL05.                                                         
       01  FILLER                    PIC X(16) VALUE '* COMM MFL05   *'.        
           COPY COMMML05.                                                       
       LINKAGE                         SECTION.                                 
       01  DFHCOMMAREA.                                                         
           05 FILLER                  PIC X(0338).                              
           COPY SYKLINKB.                                                       
       PROCEDURE DIVISION.                                                      
       MODULE-MGV65 SECTION.                                                    
           PERFORM MODULE-ENTREE                                                
           IF OK                                                                
               PERFORM MODULE-TRAITEMENT                                        
           END-IF                                                               
           PERFORM MODULE-SORTIE.                                               
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-MODULE-MGV65. EXIT.                                                  
       MODULE-ENTREE SECTION.                                                   
           MOVE '0'                        TO KONTROL.                          
           MOVE 'MGV65'                    TO NOM-PROG.                         
           MOVE DFHCOMMAREA                TO COMM-GV65-APPLI.                  
           MOVE '0000'                     TO COMM-GV65-CODRET                  
           MOVE SPACES                     TO COMM-GV65-LMESSAGE.               
v43r0 *    MOVE 'N'   TO COMM-GV65-ARTICLE-CQE                                  
           SET COMM-GV65-STOCK-FOURN-NON   TO TRUE.                             
V50R0      SET LIEU-STOCK-INCONNU TO TRUE                                       
           MOVE COMM-GV65-DDELIV           TO W-DDELIV-INITIALE                 
      * -> affichage log                                                        
           INITIALIZE RVGA01ZZ                                                  
           MOVE FUNC-SELECT     TO TRACE-SQL-FUNCTION.                          
           MOVE 'N' TO TOP-DISPLAY                                              
           EXEC SQL                                                             
                SELECT WTABLEG                                                  
                INTO  :XCTRL-WTABLEG                                            
                FROM   RVGA01ZZ                                                 
                WHERE  SOCZP = 'DISPL'                                          
                AND    TRANS = 'MGV65 '                                         
                WITH UR                                                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                03644008
           IF TROUVE                                                            
              MOVE  XCTRL-WFLAG        TO  TOP-DISPLAY                          
           END-IF.                                                              
           PERFORM RECHERCHE-PARAMETRES.                                        
           MOVE COMM-GV65-SSAAMMJJ TO GFSAMJ-0                                  
           MOVE '5' TO GFDATA.                                                  
           PERFORM LINK-TETDATC                                                 
           COMPUTE GFQNT0 = GFQNT0 + 1.                                         
           MOVE '3' TO GFDATA                                                   
           PERFORM LINK-TETDATC                                                 
           MOVE GFSAMJ-0           TO WS-DATE-JOUR-PLUS1                        
eg2003*    MOVE COMM-GV65-SSAAMMJJ TO GFSAMJ-0                                  
  !   *    MOVE '5' TO GFDATA.                                                  
  ! *                                                                           
  !   *    PERFORM LINK-TETDATC                                                 
  ! *                                                                           
  !   *    COMPUTE GFQNT0 = GFQNT0 + WS-DELTA-DDELIV.                           
  ! *                                                                           
  !   *    MOVE '3' TO GFDATA                                                   
  ! *                                                                           
  !   *    PERFORM LINK-TETDATC                                                 
  ! *                                                                           
eg2003*    MOVE GFSAMJ-0           TO WS-DATE-JOUR-PLUS15.                      
      * pour l'instant le param�trage est � 1                                   
V50R0      IF DDELIV-FORCE OR DDELIV-FORCE-DACEM                                
  !           MOVE COMM-GV65-SSAAMMJJ TO GFSAMJ-0                               
  !           MOVE '5' TO GFDATA                                                
  !           PERFORM LINK-TETDATC                                              
  !           COMPUTE GFQNT0 = GFQNT0 + W-NB-JOUR-O                             
  !           MOVE '3' TO GFDATA                                                
  !           PERFORM LINK-TETDATC                                              
  !           MOVE GFSAMJ-0           TO WS-DATE-JOUR-O                         
  !                                                                             
      * pour l'instant le param�trage est � 0                                   
  !           MOVE COMM-GV65-SSAAMMJJ TO GFSAMJ-0                               
  !           MOVE '5' TO GFDATA                                                
  !           PERFORM LINK-TETDATC                                              
  !           COMPUTE GFQNT0 = GFQNT0 + W-NB-JOUR-J                             
  !           MOVE '3' TO GFDATA                                                
  !           PERFORM LINK-TETDATC                                              
  !           MOVE GFSAMJ-0           TO WS-DATE-JOUR-J                         
V50R0      END-IF.                                                              
V50R0 *    IF DDELIV-DACEM OR DDELIV-FORCE-DACEM                                
  !   *       MOVE COMM-GV65-SSAAMMJJ TO WS-DATE-JOUR-DACEM                     
V50R0 *    END-IF.                                                              
V41R0      PERFORM ACCES-TSTENVOI.                                              
      * AFIN DE POUVOIR ACTIVER LE CONTROLE DU FLAG WMULTI DANS                 
      * GV INDEPENDEMMENT DE LA GESTION DES MUTATIONS                           
      * ON AJOUTE UN PARAMETRE NCGFC : MULTI                                    
      * SI IL EST ABSENT OU SI WFALG = 'N' ON NE GERE PAS LA                    
      * FONCTIONNALITE MUTATION MONO MUTATION MULTI PRODUITS                    
           IF OK                                                                
              PERFORM CHERCHE-WMULTI                                            
           END-IF.                                                              
           IF NOT OK                                                            
              GO TO FIN-MODULE-ENTREE                                           
           END-IF.                                                              
           IF NOT COMM-GV65-MUT-UNIQUE                                          
              AND NOT COMM-GV65-MUT-MULTI                                       
              PERFORM SELECT-NB-RTGV11                                          
           END-IF                                                               
V43R0 * recherche si un des articles est en precommande                         
  !   * S'IL N'Y A PAS D'ARTICLE EN PRECOMMANDE, ON FORCE LA DDELIV             
  !   * A LA DATE DU JOUR POUR MUTER LE PLUS VITE POSSIBLE, SI NON,             
  !   * ON LAISSE LA DATE DE LIVRAISON INITIALEMENT PREVUE                      
  !        SET GV65-PAS-COMMANDE-PRECO TO TRUE                                  
V50R0      IF DDELIV-FORCE OR DDELIV-FORCE-DACEM                                
  !          IF COMM-GV65-RESA-A-J = 'O' OR  'J'                                
  !             PERFORM DECLARE-RTGV11                                          
  !             PERFORM FETCH-RTGV11                                            
  !             PERFORM UNTIL NON-TROUVE                                        
  !               IF GV99-WPRECO = '1'                                          
  !                  SET GV65-COMMANDE-PRECO TO TRUE                            
  !               END-IF                                                        
  !               PERFORM FETCH-RTGV11                                          
  !             END-PERFORM                                                     
  !             PERFORM CLOSE-RTGV11                                            
  !             IF GV65-PAS-COMMANDE-PRECO                                      
  !               IF COMM-GV65-RESA-A-J = 'O'                                   
  !                 MOVE WS-DATE-JOUR-O     TO COMM-GV65-DDELIV                 
  !   *             MOVE WS-DATE-JOUR-PLUS1 TO COMM-GV65-DDELIV                 
  !                 IF AUTO-DISPLAY                                             
  !                    STRING 'FORCAGE DDELIV A DATE DU JOUR+'                  
V50R0                  W-NB-JOUR-O                                              
  !                    ' SUR VENTE : ' COMM-GV65-NVENTE                         
  !                    DELIMITED BY SIZE INTO W-MESSAGE                         
  !                    PERFORM ECRITURE-DISPLAY                                 
  !                 END-IF                                                      
  !               END-IF                                                        
  !               IF COMM-GV65-RESA-A-J = 'J'                                   
  !                 MOVE WS-DATE-JOUR-J     TO COMM-GV65-DDELIV                 
  !                 IF AUTO-DISPLAY                                             
  !                   STRING 'FORCAGE DDELIV A DATE DU JOUR+'                   
V50R0                  W-NB-JOUR-J                                              
  !                   ' SUR VENTE : ' COMM-GV65-NVENTE                          
  !                   DELIMITED BY SIZE INTO W-MESSAGE                          
  !                   PERFORM ECRITURE-DISPLAY                                  
  !                 END-IF                                                      
  !               END-IF                                                        
  !             END-IF                                                          
V43R0        END-IF                                                             
V50R0      END-IF.                                                              
           SET COMM-GV65-DACEM-NON         TO TRUE.                             
           SET COMM-GV65-AUCUN-STOCK-DISPO TO TRUE.                             
           EXEC CICS ASSIGN APPLID  (W-APPLID)                                  
                            NOHANDLE                                            
           END-EXEC.                                                            
           MOVE EIBRCODE TO EIB-RCODE.                                          
           IF NOT EIB-NORMAL                                                    
              MOVE '1' TO KONTROL                                               
              MOVE   '0019'               TO PTMG-NSEQERR                       
              PERFORM MLIBERRGEN                                                
           END-IF.                                                              
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-MESSAGE-H.                             
           MOVE W-MINUTE            TO W-MESSAGE-M.                             
           PERFORM TEST-VTE-INTERNET.                                           
LD0709*    RECHERCHE SI C EST UN B2B                                            
LD0709*    PERFORM CHERCHE-B2B-LOG                                              
           EXEC CICS ASSIGN APPLID  (w-CICS-APPLID)                             
                            NETNAME (w-CICS-NETNAM)                             
                            NOHANDLE                                            
           END-EXEC.                                                            
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-MODULE-ENTREE. EXIT.                                                 
      **************************                                                
       MODULE-TRAITEMENT SECTION.                                               
      ****************************                                              
V34CEN     PERFORM CHERCHE-MDLIV-EMX.                                           
           PERFORM SELECT-RTGA00                                                
           PERFORM CHARGEMENT-DSYST                                             
           EVALUATE TRUE                                                        
      * -> CONTROLE (ENTREE SUR GV00) DARTY ET DACEM                            
              WHEN  COMM-GV65-CONTROLE                                          
                    PERFORM ACCES-AUTORISATION                                  
                    IF OK                                                       
                       PERFORM MODULE-CONTROLE                                  
                    END-IF                                                      
V34R4 * -> MODIFICATION RESERVATION DACEM                                       
V34R4         WHEN  COMM-GV65-NEW-APPEL                                         
V34R4               PERFORM MODULE-NEW-APPEL                                    
           END-EVALUATE.                                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *****************************                                             
       MODULE-SORTIE SECTION.                                                   
      ************************                                                  
           MOVE COMM-GV65-APPLI       TO DFHCOMMAREA.                           
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
           GOBACK.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-MODULE-SORTIE. EXIT.                                                 
      **************************                                                
V34R4  MODULE-NEW-APPEL SECTION.                                                
V34R4 *****************************                                             
V34R4                                                                           
V34R4      SET OK TO TRUE.                                                      
V34R4      IF AUTO-DISPLAY                                                      
V34R4         STRING 'appel sur vente: '                                        
V34R4         COMM-GV65-NVENTE ' '                                              
              'N� ordre '                                                       
              COMM-GV65-NORDRE ' '                                              
  !           COMM-GV65-NSOCIETE ' '                                            
  !           COMM-GV65-NLIEU ' '                                               
V34R4         ' CODIC: ' COMM-GV65-NCODIC                                       
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
V34R4                                                                           
              PERFORM ECRITURE-DISPLAY                                          
V34R4         STRING                                                            
V34R4             ' DDELIV : ' COMM-GV65-DDELIV                                 
V34R4             ' N� mut : ' COMM-GV65-NMUTTEMP                               
  !               ' AVEC !'                                                     
  !               COMM-GV65-ACT-DACEM                                           
  !               COMM-GV65-MAJ-STOCK                                           
  !               COMM-GV65-SUP-MUT-GB05                                        
  !               COMM-GV65-SUP-MUT-GB15                                        
  !               COMM-GV65-CRE-GB05-TEMPO                                      
  !               COMM-GV65-CRE-GB05-FINAL                                      
  !               COMM-GV65-CRE-GB15-FINAL                                      
  !               COMM-GV65-INTERNET                                            
  !               COMM-GV65-FILIERE-IN                                          
  !               ' cappro:'                                                    
  !               GA00-CAPPRO                                                   
  !           DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF.                                                              
V34R4      IF COMM-GV65-ACT-DACEM = 'C'                                         
V34R4         PERFORM RESERVATION-DACEM                                         
V34R4      END-IF                                                               
V34R4                                                                           
V34R4      IF COMM-GV65-ACT-DACEM = 'S'                                         
              IF W-MODIF-GENERIX = 'O'                                          
V34R4            PERFORM MODULE-SUPPRESSION-DACEM                               
              ELSE                                                              
                 MOVE '0000'    TO COMM-GV65-CODRET                             
V52R0            PERFORM ENLEVER-DE-GB07-DACEM                                  
V34R4         END-IF                                                            
V34R4      END-IF                                                               
V34R4                                                                           
V52R0 * on supprime les modification dacem car on ne peut pas modifier          
V52R0 * la filiere, on passe donc pas suppression + ajout                       
V52R0 *    IF COMM-GV65-ACT-DACEM = 'M'                                         
V52R0 *       IF W-MODIF-GENERIX = 'O'                                          
V52R0 *          PERFORM MODULE-MAJ-DACEM                                       
V52R0 *       ELSE                                                              
V52R0 *          MOVE 'C' TO COMM-GV65-ACT-DACEM                                
V52R0 *          PERFORM RESERVATION-DACEM                                      
V52R0 *       END-IF                                                            
V52R0 *    END-IF                                                               
V34R4                                                                           
V34R4      IF COMM-GV65-CRE-GB05-TEMPO = 'O'                                    
v41r0         PERFORM ACCES-AUTORISATION                                        
v41r0         IF OK                                                             
                 IF RTGA00-TROUVE                                               
V34R4               PERFORM MODULE-RESERVATION                                  
v41r0            END-IF                                                         
v41r0         END-IF                                                            
V34R4      END-IF                                                               
V34R4                                                                           
V34R4      IF COMM-GV65-SUP-MUT-GB15 = 'O'                                      
V34R4         IF COMM-GV65-NMUTTEMP NOT = SPACE                                 
V34R4            MOVE COMM-GV65-NMUTTEMP TO GB15-NMUTATION                      
V34R4            PERFORM SUPPRESSION-MUT-GB15-GB05                              
V34R4         END-IF                                                            
V34R4      END-IF                                                               
V34R4                                                                           
V34R4      IF COMM-GV65-SUP-MUT-GB05 = 'O'                                      
V34R4         PERFORM SELECT-GV11                                               
V34R4         IF COMM-GV65-NMUTTEMP NOT = SPACE                                 
V34R4            MOVE COMM-GV65-NMUTTEMP TO GB05-NMUTATION                      
V34R4            PERFORM SELECT-RTGB05                                          
V34R4            IF TROUVE                                                      
V34R4               MOVE GB05-NSOCENTR  TO WS-NSOCDEPOT                         
V34R4               MOVE GB05-NDEPOT    TO WS-NDEPOT                            
V34R4               MOVE GB05-NSOCENTR  TO COMM-GV65-NSOCDEPOT                  
V34R4               MOVE GB05-NDEPOT    TO COMM-GV65-NDEPOT                     
      * dans le cas ou on repousse une r�servation darty, on sauvegarde         
      * les donn�es necessaire � la recherche de la nouvelle mutation           
      * sans avoir a rechercher le lieu de stockage (ce qui pose problem        
      * e si le produit n'est plus dispo)                                       
V34R4               MOVE GB05-NSOCIETE  TO MDLIV-LDEPLIVR-EXP (1:3)             
V34R4               MOVE GB05-NLIEU     TO MDLIV-LDEPLIVR-EXP (4:3)             
                    SET LIEU-STOCK-CONNU TO TRUE                                
V34R4               PERFORM SUPPRESSION-MUT-GB05                                
V34R4            ELSE                                                           
V34R4               MOVE GV11-NSOCLIVR  TO WS-NSOCDEPOT                         
V34R4               MOVE GV11-NDEPOT    TO WS-NDEPOT                            
V34R4               MOVE GV11-NSOCLIVR  TO COMM-GV65-NSOCDEPOT                  
V34R4               MOVE GV11-NDEPOT    TO COMM-GV65-NDEPOT                     
V34R4            END-IF                                                         
V34R4         ELSE                                                              
V43R0            MOVE GV11-NSOCLIVR  TO WS-NSOCDEPOT                            
V43R0            MOVE GV11-NDEPOT    TO WS-NDEPOT                               
V34R4            MOVE GV11-NSOCLIVR  TO COMM-GV65-NSOCDEPOT                     
V34R4            MOVE GV11-NDEPOT    TO COMM-GV65-NDEPOT                        
V34R4         END-IF                                                            
V34R4      END-IF                                                               
V34R4                                                                           
      * quand COMM-GV65-MAJ-STOCK = '=' on est dans le cas d'un produit         
      * sur commande fournisseur pour qui on a trouv� du stock sur gs10         
      * avant la reception fournisseur, on doit donc "dereserver" la            
      * commande fournisseur puis s'imputer sur GS10                            
V34R4      IF COMM-GV65-MAJ-STOCK = '-' OR '='                                  
V34R4         PERFORM IMPUTATION-STOCK-MOINS                                    
V34R4      END-IF                                                               
V34R4                                                                           
V34R4      IF COMM-GV65-MAJ-STOCK = '+' OR '='                                  
V52R1 * debut  corectif 02/08/2012 pour alimentation stockres                   
v50r0 *        IF LIEU-STOCK-INCONNU                                            
v50r0 *           PERFORM CHERCHE-LIEU-STOCKAGE                                 
v50r0 *        END-IF                                                           
V52R1 * Fin    corectif 02/08/2012 pour alimentation stockres                   
V52R1          PERFORM CHERCHE-LIEU-STOCKAGE                                    
V34R4          IF OK AND WMAGIE-KO                                              
V34R4 * SI ARTICLE DISPO ==> IMPUTATION SUR GS10                                
V34R4 * SI ARTICLE EN CF ==> IMPUTATION SUR GF55                                
V34R4            PERFORM IMPUTATION-STOCK                                       
V34R4          END-IF                                                           
V34R4 * SI ARTICLE SUR MUT MAGIQUE ==> IMPUTATION SUR LA MUT MAGIQUE            
V34R4          IF WMAGIE-OK and   MUT-SUR-B2B-KO                                
test             IF AUTO-DISPLAY                                                
                    MOVE GB15-QDEMANDEE TO W-EDIT-QTE                           
v43r0               STRING 'imput sur mut magique '                             
v43r0               WGB05-NMUTATION ' CODIC ' COMM-GV65-NCODIC                  
v43r0               ' STOCK ' W-EDIT-QTE                                        
test                DELIMITED BY SIZE INTO W-MESSAGE                            
test                                                                            
                    PERFORM ECRITURE-DISPLAY                                    
                 END-IF                                                         
V34R4            MOVE WGB05-NMUTATION TO GB15-NMUTATION                         
V34R4            PERFORM SUPPRESSION-MUT-MAGIQUE                                
V34R4            MOVE COMM-GV65-NSEQNQ   TO MEC15C-NSEQNQ                       
V34R4            MOVE '0009'             TO MEC15C-NSEQERR                      
V34R4            MOVE 'I'                TO MEC15C-STAT                         
V34R4            MOVE  WGB05-NMUTATION   TO MEC15C-ALP(1)                       
V34R4            PERFORM CREER-EC15                                             
V34R4            MOVE 'R' TO GV22-CSTATUT                                       
V34R4          END-IF                                                           
V50R1         IF MUT-SUR-B2B-OK and  WMAGIE-OK                                  
      ** imputer sur B2B                                                        
V50R1            PERFORM MAJ-MUT-MAGIQUE                                        
                 COMPUTE W-GB15-QMUTEE = W-GB15-QMUTEE  - WQTE                  
                 STRING 'Imput sur mut mag B2B '                                
                 GB15-NMUTATION  ' CODIC ' COMM-GV65-NCODIC                     
                 ' STOCK QMUTEE ' W-GB15-QMUTEE                                 
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
V50R1         END-IF                                                            
V34R4      END-IF                                                               
V34R4                                                                           
V34R4      IF COMM-GV65-CRE-GB05-FINAL = 'O'                                    
              IF COMM-GV65-CRE-GB15-FINAL = 'O'                                 
                 PERFORM SELECT-GV11                                            
                 IF GV11-LCOMMENT(6:7) NOT = SPACE                              
      *             MOVE GV11-NSOCLIVR  TO WS-NSOCDEPOT                         
      *             MOVE GV11-NDEPOT    TO WS-NDEPOT                            
                    MOVE GV11-NSOCLIVR  TO COMM-GV65-NSOCDEPOT                  
                    MOVE GV11-NDEPOT    TO COMM-GV65-NDEPOT                     
                    SET LIEU-STOCK-CONNU TO TRUE                                
                 END-IF                                                         
              END-IF                                                            
v52r0 * si on a passe un N� de mut, on s'impute directement dessus              
v52r0 * si non, on fait la recherche comme d'habitude                           
v52r0         IF COMM-GV65-NMUT = SPACE                                         
  !              IF AUTO-DISPLAY                                                
  !                 STRING 'cre gb05 '                                          
V50R0               'recherche mut finale'                                      
  !                 ' SUR VENTE : ' COMM-GV65-NVENTE                            
  !                 DELIMITED BY SIZE INTO W-MESSAGE                            
  !                 PERFORM ECRITURE-DISPLAY                                    
  !              END-IF                                                         
                 IF LIEU-STOCK-INCONNU                                          
                    PERFORM CHERCHE-LIEU-STOCKAGE                               
                 END-IF                                                         
V52R0 *       IF W-MUT-GENERIX = 'N' OR COMM-GV65-NMUTATION = SPACE             
V34R4            PERFORM RECHERCHE-MUT-FINALE                                   
V50R0         ELSE                                                              
  !              IF AUTO-DISPLAY                                                
  !                 STRING 'cre gb05 '                                          
V50R0               'mut renseign�e ' COMM-GV65-NMUT                            
  !                 ' SUR VENTE : ' COMM-GV65-NVENTE                            
                    '!' W-STOCKAGE '!'                                          
  !                 DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ECRITURE-DISPLAY                                    
  !              END-IF                                                         
V52R0            MOVE COMM-GV65-NMUT      TO GB05-NMUTATION                     
V50R0            PERFORM SELECT-RTGB05                                          
                 IF SQLCODE = 0                                                 
V50R0               SET WS-MUTATION-AFFECTEE TO TRUE                            
                 ELSE                                                           
  !                IF AUTO-DISPLAY                                              
                     MOVE SQLCODE TO EDT-SQL                                    
  !                  STRING 'cre gb05 '                                         
V50R0                'la mut etait foireuse, on en tient pas compte'            
                       ' '  EDT-SQL                                             
  !                  DELIMITED BY SIZE INTO W-MESSAGE                           
                     PERFORM ECRITURE-DISPLAY                                   
  !                END-IF                                                       
                    IF LIEU-STOCK-INCONNU                                       
                       PERFORM CHERCHE-LIEU-STOCKAGE                            
                    END-IF                                                      
V52R0 *       IF W-MUT-GENERIX = 'N' OR COMM-GV65-NMUTATION = SPACE             
V34R4               PERFORM RECHERCHE-MUT-FINALE                                
V50R0            END-IF                                                         
V50R0         END-IF                                                            
V34R4         IF WS-MUTATION-AFFECTEE                                           
V34R4            MOVE COMM-GV65-NSEQNQ   TO MEC15C-NSEQNQ                       
V34R4            MOVE '0010'          TO MEC15C-NSEQERR                         
V34R4            MOVE 'I'             TO MEC15C-STAT                            
V34R4            MOVE  GB05-NMUTATION TO MEC15C-ALP(1)                          
V34R4            PERFORM CREER-EC15                                             
V34R4            PERFORM AJOUTER-SUR-GB05-GB15-FINAL                            
V34R4         ELSE                                                              
                 IF AUTO-DISPLAY                                                
V43R0               IF COMM-GV65-NVENTE > '0000000'                             
                       STRING 'aucune mutation trouv�e'                         
                       ' pour CODIC ' COMM-GV65-NCODIC                          
                       ' VENTE : '                                              
                       COMM-GV65-NVENTE                                         
                       DELIMITED BY SIZE INTO W-MESSAGE                         
V43R0               ELSE                                                        
  !                    STRING 'AUCUNE MUTATION TROUV�E'                         
  !                    ' POUR CODIC ' COMM-GV65-NCODIC                          
  !                    ' ORDRE: '                                               
  !                    COMM-GV65-NORDRE ' '                                     
  !                    COMM-GV65-NSOCIETE                                       
  !                    COMM-GV65-NLIEU                                          
  !                    DELIMITED BY SIZE INTO W-MESSAGE                         
V43R0               END-IF                                                      
                    PERFORM ECRITURE-DISPLAY                                    
                 END-IF                                                         
V34R4         END-IF                                                            
V34R4      END-IF                                                               
V34R4                                                                           
V34R4      IF COMM-GV65-INTERNET = 'O'                                          
V34R4         PERFORM MODULE-INTERNET                                           
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-MODULE-NEW-APPEL. EXIT.                                              
V34R4 ********************************                                          
V50R1  RECHERCHE-MUT-MAGIQUE SECTION.                                           
  !   *************************************                                     
  !                                                                             
  !        COMPUTE  W-GB15-QMUTEE          =   0                                
  !        COMPUTE  W-GB15-QCDEPREL        =   0                                
  !        MOVE     COMM-GV65-QTE          TO  WQTE                             
  !   * SI VENTE B2B ON CHERCHE S'IL Y A DU STOCK SUR LA MUT MAGIQUE            
  !        SET MAGIE-KO          TO TRUE                                        
  !        SET MUT-SUR-B2B-KO    TO TRUE                                        
  !        IF  VENTE-B2B                                                        
  !          MOVE  '0' TO WF-TSTENVOI                                           
  !          PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                      
  !          UNTIL WC-TSTENVOI-FIN                                              
  !             PERFORM LECTURE-TSTENVOI                                        
  !             IF  WC-TSTENVOI-SUITE                                           
  !             AND TSTENVOI-NSOCZP    = '00000'                                
  !             AND TSTENVOI-CTRL(1:2) = 'BA'                                   
  !             AND TSTENVOI-WPARAM(1:3) = WS-NSOCDEPOT                         
  !             AND TSTENVOI-WPARAM(4:3) = WS-NDEPOT                            
  !                MOVE TSTENVOI-LIBELLE(1:7) TO GB15-NMUTATION                 
  !                MOVE COMM-GV65-NCODIC      TO GB15-NCODIC                    
  !                                                                             
  !                PERFORM SELECT-RTGB15-TEMPORAIRE                             
  !                                                                             
  !   * QMUTEE = QUANTITE DISPO POUR LE B2B                                     
  !   * QLIEE = NOMBRE MAXI DE PRODUIT POUR UNE VENTE B2B                       
  !                IF  TROUVE                                                   
  !                AND GB15-QDEMANDEE         >= WQTE                           
  !                AND GB15-QMUTEE            >= WQTE                           
  !                AND GB15-QCDEPREL          >= WQTE                           
  !                    SET WMAGIE-OK          TO TRUE                           
  !                    SET MUT-SUR-B2B-OK     TO TRUE                           
  !                    MOVE GB15-QMUTEE       TO W-GB15-QMUTEE                  
  !                    MOVE GB15-NMUTATION    TO W-MUT-B2B-MAG                  
  !                    MOVE GB15-QCDEPREL     TO W-GB15-QCDEPREL                
  !                END-IF                                                       
  !             END-IF                                                          
  !          END-PERFORM                                                        
  !        END-IF.                                                              
  !                                                                             
  !    FIN-RECHERCHE-MUT-MAGIQUE. EXIT.                                         
V50R1 **********************************                                        
V50R1  MAJ-MUT-MAGIQUE SECTION.                                                 
  !   ***************************                                               
  !                                                                             
  !        MOVE FUNC-UPDATE          TO TRACE-SQL-FUNCTION.                     
  !        MOVE DATHEUR              TO GB15-DSYST                              
  !        MOVE W-MUT-B2B-MAG        TO GB15-NMUTATION                          
  !        PERFORM CLEF-GB1500.                                                 
  !        EXEC SQL UPDATE   RVGB1502                                           
  !           SET      QDEMANDEE = QDEMANDEE -  :WQTE,                          
  !                    QCC       = QCC       -  :WQTE,                          
  !                    QMUTEE    = QMUTEE    -  :WQTE,                          
  !                    DSYST     = :GB15-DSYST                                  
  !           WHERE    NMUTATION = :GB15-NMUTATION                              
  !           AND      NCODIC    = :GB15-NCODIC                                 
  !        END-EXEC.                                                            
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !        IF NON-TROUVE                                                        
  !             STRING 'UPDATE GB15  SUR MUT '  GB15-NMUTATION                  
  !                    'NCODIC             = '  GB15-NCODIC                     
  !             DELIMITED BY SIZE INTO W-MESSAGE                                
                PERFORM ECRITURE-DISPLAY                                        
  !        END-IF.                                                              
  !                                                                             
  !    FIN-MAJ-MUT-MAGIQUE. EXIT.                                               
V50R1 **********************************                                        
      * --------------------------------------------------- *                   
      * RECHERCHE MUTATION DARTY.COM PAS IMPLEMENTEE        *                   
      * RECHERCHE D'UN LIEU DE STOCKAGE                     *                   
      * RECHERCHE D'UNE MUTATION                            *                   
      * SI NON TROUVE RECHERCHE DACEM SI CODIC GERE PAR     *                   
      * DACEM.                                              *                   
      *                                                     *                   
      * --------------------------------------------------- *                   
       MODULE-CONTROLE   SECTION.                                               
      ***************************                                               
           SET WS-MUTATION-NON-TROUVEE TO TRUE.                                 
           IF RTGA00-TROUVE                                                     
              PERFORM CHERCHE-LIEU-STOCKAGE                                     
           END-IF                                                               
      * -> SI LIEU DE STOCK TROUVE, ET STOCK EST > 0                            
L20410     IF WSTOCK-OK OR WMAGIE-OK OR WS-RESER-FOURN-OK                       
              PERFORM RECHERCHE-MUT-FINALE                                      
           ELSE                                                                 
              IF NOT COMM-GV65-DACEM-OUI                                        
                 IF AUTO-DISPLAY                                                
V43R0               IF COMM-GV65-NVENTE > '0000000'                             
test                   STRING 'aucun lieu de stockage trouv�'                   
test                   ' pour CODIC ' COMM-GV65-NCODIC                          
                       ' VENTE : '                                              
                       COMM-GV65-NVENTE                                         
                       DELIMITED BY SIZE INTO W-MESSAGE                         
V43R0               ELSE                                                        
  !                    STRING 'AUCUN LIEU DE STOCKAGE TROUV�'                   
  !                    ': CODIC ' COMM-GV65-NCODIC                              
  !                    ' ORDRE: '                                               
  !                    COMM-GV65-NORDRE ' '                                     
  !                    COMM-GV65-NSOCIETE                                       
  !                    COMM-GV65-NLIEU                                          
  !                    DELIMITED BY SIZE INTO W-MESSAGE                         
V43R0               END-IF                                                      
                    PERFORM ECRITURE-DISPLAY                                    
                 END-IF                                                         
              END-IF                                                            
           END-IF                                                               
           IF OK AND COMM-GV65-DACEM-OUI                                        
                 AND  WS-MUTATION-NON-TROUVEE   AND WSTOCK-KO                   
      * -> SI ON TROUVE DU STOCK CHEZ DARTY ON NE VA PAS CHEZ DACEM             
      * -> COMMANDE DACEM APPEL DACEM EN MODE CONTROLE                          
              PERFORM CONSULTATION-DACEM                                        
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-MODULE-CONTROLE. EXIT.                                               
      ***************************                                               
V34R4  MODULE-RESERVATION SECTION.                                              
V34R4 ****************************                                              
V34R4                                                                           
V34R4      PERFORM CHERCHE-LIEU-STOCKAGE                                        
V34R4                                                                           
V34R4      IF WMAGIE-OK OR WSTOCK-OK OR WS-RESER-FOURN-OK                       
V34R4         SET COMM-GV65-STOCK-DISPONIBLE TO TRUE                            
V34R4         PERFORM AJOUT-SUR-MUT-TEMPORAIRE                                  
V34R4         MOVE GB05-NMUTATION TO COMM-GV65-NMUTTEMP                         
V34R4      ELSE                                                                 
              IF NOT COMM-GV65-DACEM-OUI                                        
test             IF AUTO-DISPLAY                                                
test                STRING 'aucun lieu de stockage trouv�'                      
test                 ' pour CODIC ' COMM-GV65-NCODIC                            
test                DELIMITED BY SIZE INTO W-MESSAGE                            
test                                                                            
                    PERFORM ECRITURE-DISPLAY                                    
test             END-IF                                                         
              END-IF                                                            
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-MODULE-RESERVATION. EXIT.                                            
V34R4 *******************************                                           
       AJOUT-SUR-MUT-TEMPORAIRE SECTION.                                        
      ************************************                                      
      * -> RECHERCHE DE LA MUT TEMPORAIRE                                       
           MOVE COMM-GV65-NSOCDEPOT        TO GB05-NSOCENTR                     
           MOVE COMM-GV65-NDEPOT           TO GB05-NDEPOT                       
           MOVE COMM-GV65-QTE              TO WQTE                              
           MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO GB05-NSOCIETE                     
           MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO GB05-NLIEU                        
V41R0      MOVE SPACE                      TO GB05-NMUTATION                    
  !                                                                             
  !        MOVE  '0' TO WF-TSTENVOI                                             
  !        PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
  !        UNTIL WC-TSTENVOI-FIN                                                
  !                                                                             
  !           PERFORM LECTURE-TSTENVOI                                          
  !           IF  WC-TSTENVOI-SUITE                                             
  !              AND TSTENVOI-NSOCZP    = '00000'                               
  !              AND TSTENVOI-CTRL1     = 'C'                                   
  !              AND TSTENVOI-SOCIETE     = COMM-GV65-NSOCDEPOT                 
  !              AND TSTENVOI-LIEU        = COMM-GV65-NDEPOT                    
  !                 MOVE TSTENVOI-NMUTATION    TO GB05-NMUTATION                
  !           END-IF                                                            
  !        END-PERFORM                                                          
  !                                                                             
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
      * LE CAS OU AUCUN NUMERO DE MUTATION TEMPO � �T� TROUV� DE DOIT           
      * PAS ARRIVER, IL FAUDRAIT POUR CELA QUE QUELQU'UN EST DETRUIT            
      * LE PARAMETRAGE XCTRL, CEPENDANT, SI JAMAIS �A ARRIVE, ON VA             
      * CREE UN NOUVEAU NUMERO POUR EVITER DE TOUT PLANTER, ON VA               
      * ENSUITE AJOUTER LA LIGNE DANS LE PARAMETRAGE XCTL DETRUIRE LA           
      * TSTENVOI ET LA RECREER POUR QU'ELLE PRENNE EN COMPTE LA                 
      * NOUVEAU NUMERO DE MUTATION                                              
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
      ***************************************************************           
V41R0      IF GB05-NMUTATION = SPACE                                            
              PERFORM CREATION-RTGB05-TEMPORAIRE                                
           END-IF.                                                              
           IF OK                                                                
               MOVE GB05-NMUTATION             TO GB15-NMUTATION                
               MOVE COMM-GV65-NCODIC           TO GB15-NCODIC                   
               COMPUTE WQVOLUME =   GA00-QHAUTEUR                               
                                  * GA00-QPROFONDEUR                            
                                  * GA00-QLARGEUR                               
               COMPUTE WQNBPIECES = COMM-GV65-QTE                               
               MOVE 0 TO WQNBLIGNES                                             
               PERFORM SELECT-RTGA14                                            
               IF OK                                                            
                  PERFORM SELECT-RTFL50                                         
               END-IF                                                           
               IF OK                                                            
                  PERFORM UPDATE-RTGB15                                         
               END-IF                                                           
               IF NON-TROUVE AND OK                                             
                  MOVE 1 TO WQNBLIGNES                                          
                  PERFORM INSERT-RTGB15                                         
               END-IF                                                           
               IF OK                                                            
                  PERFORM UPDATE-RTGB05                                         
                  MOVE GB05-NMUTATION TO COMM-GV65-NMUTTEMP                     
               END-IF                                                           
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-AJOUT-SUR-MUT-TEMPORAIRE. EXIT.                                      
      ************************************                                      
V34R4  SUPPRESSION-MUT-GB15-GB05 SECTION.                                       
V34R4 *********************************                                         
V34R4                                                                           
V34R4      PERFORM SELECT-RTGB15-TEMPORAIRE                                     
V34R4                                                                           
V34R4      IF TROUVE                                                            
V34R4 *       GB15 TROUVEE MAIS VALIDEE : TROP TARD, NE RIEN FAIRE              
V34R4        IF GB05-QNBLLANCE  =  0  AND                                       
V34R4           GB05-WVAL  NOT  = 'O'                                           
V34R4 * LA GB15 EXISTE POUR LA VENTE UNIQUEMENT SI C'EST UNE MUTATION           
V34R4 * TEMPO OU SI LA VENTE EST COMPLETE ET QU'ON A DEJA CREE LA MUT           
V34R4 * FINALE                                                                  
V41r0           SET MUTATION-FINAL TO TRUE                                      
  !             MOVE  '0' TO WF-TSTENVOI                                        
  !             PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                   
  !             UNTIL  WC-TSTENVOI-FIN                                          
  !                                                                             
  !               PERFORM LECTURE-TSTENVOI                                      
  !               IF  WC-TSTENVOI-SUITE                                         
  !               AND TSTENVOI-NSOCZP    = '00000'                              
  !               AND TSTENVOI-CTRL1     = 'C'                                  
  !                 IF TSTENVOI-NMUTATION = GB15-NMUTATION                      
  !                    SET MUTATION-TEMPO TO TRUE                               
  !                    SET WC-TSTENVOI-FIN TO TRUE                              
  !                 END-IF                                                      
  !               END-IF                                                        
  !             END-PERFORM                                                     
V41R0           IF MUTATION-TEMPO OR                                            
V41R0 *          IF GB05-LHEURLIMIT = 'EXP%EXP%' OR                             
V41r0 *            (GB05-LHEURLIMIT NOT = 'EXP%EXP%' AND                        
V41R0              ( NOT MUTATION-TEMPO               AND                       
V34R4               COMM-GV65-ETAT-PREC-VTE = 'C')                              
V34R4               PERFORM MAJ-RTGB15-RTGB05                                   
V34R4           ELSE                                                            
V34R4              IF AUTO-DISPLAY                                              
V34R4                  STRING 'PAS DE SUPP GB15 '                               
V34R4                  GB15-NMUTATION ' CDE INCOMPLETE'                         
V34R4                  ' VTE ' COMM-GV65-NVENTE                                 
V34R4                  ' CODIC ' COMM-GV65-NCODIC                               
V34R4                  DELIMITED BY SIZE INTO W-MESSAGE                         
V34R4                                                                           
                       PERFORM ECRITURE-DISPLAY                                 
V34R4              END-IF                                                       
FT                 MOVE 'O' TO COMM-GV65-SUP-MUT-GB05                           
V34R4           END-IF                                                          
FT           ELSE                                                               
FT              MOVE 'O' TO COMM-GV65-SUP-MUT-GB05                              
V34R4        END-IF                                                             
V34R4      ELSE                                                                 
V34R4         IF AUTO-DISPLAY                                                   
V34R4            STRING 'PAS DE SUPP GB15 '                                     
V34R4            GB15-NMUTATION ' mut non trouv�e'                              
V34R4            ' VTE ' COMM-GV65-NVENTE                                       
V34R4            ' CODIC ' COMM-GV65-NCODIC                                     
V34R4            DELIMITED BY SIZE INTO W-MESSAGE                               
V34R4                                                                           
                 PERFORM ECRITURE-DISPLAY                                       
V34R4         END-IF                                                            
FT            MOVE 'O' TO COMM-GV65-SUP-MUT-GB05                                
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-SUPPRESSION-MUT-GB15-GB05. EXIT.                                     
V34R4 ***************************************                                   
V34R4                                                                           
V34R4  SUPPRESSION-MUT-MAGIQUE SECTION.                                         
V34R4 *********************************                                         
V34R4                                                                           
V34R4      PERFORM SELECT-RTGB15-TEMPORAIRE                                     
V34R4                                                                           
V34R4      IF TROUVE                                                            
V34R4         PERFORM MAJ-RTGB15-RTGB05                                         
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-SUPPRESSION-MUT-MAGIQUE. EXIT.                                       
V34R4 ***************************************                                   
V34R4  SUPPRESSION-MUT-GB05 SECTION.                                            
V34R4 ******************************                                            
V34R4                                                                           
V34R4      IF AUTO-DISPLAY                                                      
V34R4         STRING 'SUPPRESSION GB05 '                                        
V34R4         GB05-NMUTATION                                                    
V34R4         ' VTE ' COMM-GV65-NVENTE                                          
V34R4         ' CODIC ' COMM-GV65-NCODIC                                        
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
V34R4                                                                           
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF                                                               
V34R4      COMPUTE WQTE       =  COMM-GV65-QTE * -1                             
V34R4                                                                           
V34R4      MOVE    WQTE       TO WQNBPIECES                                     
V34R4                                                                           
V34R4      COMPUTE WQVOLUME = GA00-QHAUTEUR    *                                
V34R4                         GA00-QPROFONDEUR *                                
V34R4                         GA00-QLARGEUR                                     
V34R4                                                                           
V34R4      PERFORM UPDATE-RTGB05.                                               
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-SUPPRESSION-MUT-GB05. EXIT.                                          
V34R4 *********************************                                         
L20410 DERESERVER-CDE-FOURNISSEUR     SECTION.                                  
L20410****************************************                                  
L20410                                                                          
L20410**** DERESERVATION CDE FNR SUR DATE DECROISSANTE                          
L20410**** IMMEDIATEMENT INFERIEURE OU EGALE A LA DATE DE DELIVRANCE            
           MOVE GB15-NSOCDEPOT             TO GF55-NSOCDEPOT                    
           MOVE GB15-NDEPOT                TO GF55-NDEPOT                       
           MOVE COMM-GV65-NCODIC           TO GF55-NCODIC                       
           MOVE COMM-GV65-DDELIV           TO GF55-DCDE                         
L20410     PERFORM DECLARE-CGF55D                                               
L20410     PERFORM FETCH-CGF55D                                                 
L20410     PERFORM UNTIL NON-TROUVE OR WQTE = ZEROES                            
L20410        PERFORM RENDRE-CDE-FOURNISSEUR                                    
L20410        IF WQTE NOT = ZEROES                                              
L20410           PERFORM FETCH-CGF55D                                           
L20410        END-IF                                                            
L20410     END-PERFORM.                                                         
L20410     PERFORM CLOSE-CGF55D.                                                
L20410*                                                                         
L20410**** DERESERVATION CDE FNR SUR DATE CROISSANTE                            
L20410**** IMMEDIATEMENT SUPERIEURE OU EGALE A LA DATE DE DELIVRANCE            
L20410     IF WQTE NOT = ZEROES                                                 
L20410        MOVE GV11-NSOCLIVR              TO GF55-NSOCDEPOT                 
L20410        MOVE GV11-NDEPOT                TO GF55-NDEPOT                    
L20410        MOVE GV11-NCODIC                TO GF55-NCODIC                    
L20410        MOVE GV11-DDELIV                TO GF55-DCDE                      
L20410        PERFORM DECLARE-CGF55C                                            
L20410        PERFORM FETCH-CGF55C                                              
L20410        PERFORM UNTIL NON-TROUVE OR WQTE = ZEROES                         
L20410           PERFORM RENDRE-CDE-FOURNISSEUR                                 
L20410           IF WQTE NOT = ZEROES                                           
L20410              PERFORM FETCH-CGF55C                                        
L20410           END-IF                                                         
L20410        END-PERFORM                                                       
L20410        PERFORM CLOSE-CGF55C                                              
L20410     END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
L20410 RENDRE-CDE-FOURNISSEUR         SECTION.                                  
L20410****************************************                                  
L20410                                                                          
L20410     IF GF55-QCDERES NOT = ZEROES AND                                     
L20410        WS-QUANTITE-A-DERESERVER NOT > GF55-QCDERES                       
L20410*       PERFORM CHARGEMENT-DSYST                                          
L20410        MOVE DATHEUR               TO GF55-DSYST                          
L20410        PERFORM UPDATE-RTGF55                                             
V43R0         MOVE SPACE TO W-MESSAGE                                           
L20410        MOVE ZEROES                TO WQTE                                
L20410     END-IF.                                                              
V34R4                                                                           
V34R4  IMPUTATION-STOCK   SECTION.                                              
V34R4 *********************************                                         
V34R4                                                                           
V34R4      IF  WSTOCK-OK AND WS-RESER-FOURN-KO                                  
V34R4         MOVE WS-NSOCDEPOT TO GS10-NSOCDEPOT                               
V34R4         MOVE WS-NDEPOT    TO GS10-NDEPOT                                  
V34R4         COMPUTE WQTE = COMM-GV65-QTE                                      
V34R4         PERFORM UPDATE-RTGS10                                             
V34R4         MOVE 'R' TO GV22-CSTATUT                                          
V34R4         IF AUTO-DISPLAY                                                   
V34R4           MOVE WQTE TO EDT-QTE                                            
V43R0           MOVE WS-QUANTITE-DISPONIBLE TO W-EDIT-QTE                       
V43R0           IF COMM-GV65-NVENTE > '0000000'                                 
V34R4              STRING 'IMPUTATION SUR GS10 + ' EDT-QTE ' '                  
V34R4              COMM-GV65-NVENTE ' CODIC ' COMM-GV65-NCODIC                  
V43R0              ' STOCK ' W-EDIT-QTE                                         
V34R4              DELIMITED BY SIZE INTO W-MESSAGE                             
V43R0           ELSE                                                            
  !                STRING 'IMPUTATION SUR GS10 + ' EDT-QTE ' '                  
  !                COMM-GV65-NORDRE ' CODIC ' COMM-GV65-NCODIC                  
  !                ' STOCK ' W-EDIT-QTE                                         
  !                DELIMITED BY SIZE INTO W-MESSAGE                             
V43R0           END-IF                                                          
                PERFORM ECRITURE-DISPLAY                                        
V34R4         END-IF                                                            
V34R4      ELSE                                                                 
V34R4         IF WS-RESER-FOURN-OK                                              
V34R4            COMPUTE WQTE = COMM-GV65-QTE                                   
V34R4            PERFORM UPDATE-RTGF55                                          
V34R4            MOVE 'F' TO GV22-CSTATUT                                       
v5.1.p*--> log dans EC10 pour CF non calee                                      
                 if COMM-GV65-INTERNET  = 'O'                                   
                    PERFORM DATE-CF-CALEE                                       
                 end-if                                                         
FT               MOVE 'N' TO COMM-GV65-CRE-GB15-FINAL                           
V34R4            IF AUTO-DISPLAY                                                
V34R4              MOVE WQTE TO EDT-QTE                                         
V43R0              IF COMM-GV65-NVENTE > '0000000'                              
V34R4               STRING 'IMPUTATION SUR GF55 +' EDT-QTE ' '                  
V34R4                COMM-GV65-NVENTE ' CODIC ' COMM-GV65-NCODIC                
                     ' ' GF55-NSOCDEPOT                                         
                     ' ' GF55-NDEPOT                                            
                     ' ' GF55-DCDE                                              
V34R4                DELIMITED BY SIZE INTO W-MESSAGE                           
V43R0              ELSE                                                         
  !                 STRING 'IMPUTATION SUR GF55 +' EDT-QTE ' '                  
  !                  COMM-GV65-NORDRE ' CODIC ' COMM-GV65-NCODIC                
  !                  ' ' GF55-NSOCDEPOT                                         
  !                  ' ' GF55-NDEPOT                                            
  !                  ' ' GF55-DCDE                                              
  !                  DELIMITED BY SIZE INTO W-MESSAGE                           
V43R0              END-IF                                                       
                   PERFORM ECRITURE-DISPLAY                                     
V34R4            END-IF                                                         
V34R4         END-IF                                                            
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-IMPUTATION-STOCK. EXIT.                                              
V34R4 ******************************                                            
V34R4                                                                           
V34R4  IMPUTATION-STOCK-MOINS  SECTION.                                         
V34R4 *********************************                                         
V34R4                                                                           
V34R4 *    IF  WSTOCK-OK AND WS-RESER-FOURN-KO                                  
V34R4      IF COMM-GV65-WCQERESF = ' '                                          
V34R4         MOVE WS-NSOCDEPOT TO GS10-NSOCDEPOT                               
V34R4         MOVE WS-NDEPOT    TO GS10-NDEPOT                                  
V34R4         COMPUTE WQTE = COMM-GV65-QTE * -1                                 
V34R4         PERFORM UPDATE-RTGS10                                             
V34R4         IF AUTO-DISPLAY                                                   
V34R4            MOVE WQTE TO EDT-QTE                                           
V34R4            STRING 'IMPUTATION SUR GS10 -' EDT-QTE  ' '                    
V34R4             COMM-GV65-NVENTE ' CODIC ' COMM-GV65-NCODIC                   
V34R4             DELIMITED BY SIZE INTO W-MESSAGE                              
V34R4                                                                           
                 PERFORM ECRITURE-DISPLAY                                       
V34R4         END-IF                                                            
V34R4      ELSE                                                                 
V34R4         IF COMM-GV65-WCQERESF = 'F'                                       
V34R4            MOVE WS-NSOCDEPOT TO GB15-NSOCDEPOT                            
V34R4            MOVE WS-NDEPOT    TO GB15-NDEPOT                               
V34R4            MOVE WQTE TO EDT-QTE                                           
V34R4            PERFORM DERESERVER-CDE-FOURNISSEUR                             
V34R4            IF AUTO-DISPLAY                                                
V34R4               STRING 'IMPUTATION SUR GF55 -' EDT-QTE ' '                  
V34R4                COMM-GV65-NVENTE ' CODIC ' COMM-GV65-NCODIC                
                     ' ' GF55-NSOCDEPOT                                         
                     ' ' GF55-NDEPOT                                            
                     ' ' GF55-DCDE                                              
V34R4                DELIMITED BY SIZE INTO W-MESSAGE                           
V34R4                                                                           
                    PERFORM ECRITURE-DISPLAY                                    
V34R4            END-IF                                                         
V34R4         END-IF                                                            
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-IMPUTATION-STOCK-MOINS. EXIT.                                        
      ******************************                                            
       RECHERCHE-MUT-FINALE   SECTION.                                          
      *********************************                                         
           SET WS-MUTATION-NON-TROUVEE TO TRUE                                  
V41R0      IF COMM-GV65-WEMPORTE     = 'P'                                      
              and COMM-GV65-MUT-SPE NOT = 'O'                                   
              PERFORM CHERCHE-MUTATION-SPE                                      
              IF WS-MUTATION-NON-TROUVEE                                        
                 PERFORM CHERCHE-MUTATION                                       
V34R4            IF AUTO-DISPLAY                                                
V34R4               STRING 'mut spe non trouve: '                               
V34R4                 COMM-GV65-NVENTE                                          
V34R4               DELIMITED BY SIZE INTO W-MESSAGE                            
V34R4                                                                           
                    PERFORM ECRITURE-DISPLAY                                    
V34R4            END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              PERFORM CHERCHE-MUTATION                                          
           END-IF.                                                              
           IF WS-MUTATION-AFFECTEE                                              
              SET COMM-GV65-STOCK-DISPONIBLE TO TRUE                            
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-RECHERCHE-MUT-FINALE.  EXIT.                                         
      *********************************                                         
V34CEN* MODULE DE SUPPRESSION DE RESERVATION CHEZ DACEM                         
V34CEN MODULE-SUPPRESSION-DACEM SECTION.                                        
V34CEN*****************************                                             
V34CEN                                                                          
V34CEN     INITIALIZE BUFFER-AS400                                              
V34CEN     MOVE 'S'      TO TOUCHE-21                                           
V43R0 *    IF COMM-GV65-DDELIV < COMM-GV65-SSAAMMJJ                             
V43R0 *          MOVE COMM-GV65-SSAAMMJJ TO COMM-GV65-DDELIV                    
V43R0 *    END-IF                                                               
v41r1 *    MOVE COMM-GV65-DDELIV   TO W-GV65-DDELIV.                            
V41R1      MOVE COMM-GV65-DMUT     TO W-GV65-DDELIV.                            
V34CEN     PERFORM APPEL-DACEM                                                  
V34CEN     IF OK                                                                
      * on a supprim� un codic dacem, on met a jour le quotas de la             
      * mut fictive gb07                                                        
V52R0         PERFORM ENLEVER-DE-GB07-DACEM                                     
V34CEN*       MOVE NCDE-21        TO COMM-GV65-NMUTATION                        
V34CEN        MOVE W-NMUTTEMP     TO COMM-GV65-NMUTATION                        
V34R4         IF AUTO-DISPLAY                                                   
V34R4         STRING 'SUPPRESSION DACEM sur vente: '                            
V34R4             COMM-GV65-NVENTE                                              
V34R4             ' CODIC: ' COMM-GV65-NCODIC                                   
V34R4             ' n� mut: ' COMM-GV65-NMUTATION                               
                  'date ' W-GV65-DDELIV                                         
V34R4          DELIMITED BY SIZE INTO W-MESSAGE                                 
               PERFORM ECRITURE-DISPLAY                                         
V34R4         END-IF                                                            
V34CEN     END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34CEN*    .                                                                    
      *}                                                                        
V34CEN FIN-MODULE-SUPPRESSION-DACEM. EXIT.                                      
      ********************************                                          
       CONSULTATION-DACEM SECTION.                                              
      *****************************                                             
           INITIALIZE BUFFER-AS400                                              
           MOVE 'C'      TO TOUCHE-21                                           
           MOVE COMM-GV65-SSAAMMJJ  TO W-GV65-DDELIV                            
v41r1 *    PERFORM CALCUL-DELAI                                                 
           PERFORM APPEL-DACEM                                                  
           IF OK                                                                
               MOVE '0000'   TO COMM-GV65-CODRET                                
               MOVE INFO-21 (1)  TO COMM-GV65-LMESSAGE                          
      *        MOVE NCDE-21      TO COMM-GV65-NMUTTEMP                          
      *        MOVE W-NUM-RESA   TO COMM-GV65-NMUTTEMP                          
               MOVE SPACE        TO COMM-GV65-NMUTTEMP                          
               SET COMM-GV65-STOCK-DACEM      TO TRUE                           
           END-IF.                                                              
           IF COMM-GV65-CODRET = '0000'                                         
V43R0 *      IF COMM-GV65-ARTICLE-CQE = 'O'                                     
  !   *        IF  COMM-GV65-DATE-DISPO       >  COMM-GV65-DDELIV               
  !   *        AND COMM-GV65-CONTROLE                                           
V50R0 *          IF COMM-GV65-CTRL-MGV64                                        
V50R0 *          IF COMM-GV65-CONTROLE                                          
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
  !   *            MOVE '6524'             TO COMM-GV65-CODRET                  
  !   *            MOVE COMM-GV65-DATE-DISPO TO COMM-GV65-LMESSAGE              
  !   *          ELSE                                                           
  !   *            MOVE '0024'          TO PTMG-NSEQERR                         
  !   *            MOVE COMM-GV65-DATE-DISPO (7:2)                              
  !   *                              TO PTMG-ALP (1) (1:2)                      
  !   *            MOVE '/'             TO PTMG-ALP (1) (3:1)                   
  !   *            MOVE COMM-GV65-DATE-DISPO (5:2)                              
  !   *                              TO PTMG-ALP (1) (4:2)                      
  !   *            MOVE '/'             TO PTMG-ALP (1) (6:1)                   
  !   *            MOVE COMM-GV65-DATE-DISPO (3:2)                              
  !   *                                    TO PTMG-ALP (1) (7:2)                
  !   *            PERFORM MLIBERRGEN                                           
  !   *          END-IF                                                         
  !   *        END-IF                                                           
  !   *      ELSE                                                               
V43R0          IF  WS-DATE-JOUR-PLUS1         >  COMM-GV65-DDELIV               
V34CEN         AND COMM-GV65-CONTROLE                                           
V50R0            IF COMM-GV65-CTRL-MGV64                                        
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *          IF COMM-GV65-CONTROLE                                          
V34CEN             MOVE '6524'             TO COMM-GV65-CODRET                  
V34CEN             MOVE WS-DATE-JOUR-PLUS1 TO COMM-GV65-LMESSAGE                
V34CEN           ELSE                                                           
                   MOVE '0024'          TO PTMG-NSEQERR                         
LD0709             MOVE WS-DATE-JOUR-PLUS1 (7:2)                                
LD0709                               TO PTMG-ALP (1) (1:2)                      
LD0709             MOVE '/'             TO PTMG-ALP (1) (3:1)                   
LD0709             MOVE WS-DATE-JOUR-PLUS1 (5:2)                                
LD0709                               TO PTMG-ALP (1) (4:2)                      
LD0709             MOVE '/'             TO PTMG-ALP (1) (6:1)                   
LD0709             MOVE WS-DATE-JOUR-PLUS1 (3:2)                                
LD0709                                     TO PTMG-ALP (1) (7:2)                
LD0709             PERFORM MLIBERRGEN                                           
V34CEN           END-IF                                                         
V34CEN         END-IF                                                           
LD0709*      END-IF                                                             
LD0709     END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-CONSULTATION-DACEM. EXIT.                                            
      ********************************                                          
       RESERVATION-DACEM SECTION.                                               
      *****************************                                             
           INITIALIZE BUFFER-AS400                                              
           MOVE 'V'      TO TOUCHE-21                                           
           PERFORM CALCUL-DELAI                                                 
           PERFORM APPEL-DACEM                                                  
           IF OK                                                                
V50R0         PERFORM AJOUTER-SUR-GB07-DACEM                                    
              MOVE W-NUM-RESA     TO COMM-GV65-NMUTATION                        
V34R4         IF AUTO-DISPLAY                                                   
V34R4            STRING 'creat DACEM vente:'                                    
V34R4                COMM-GV65-NVENTE                                           
V34R4                ' CODIC:' COMM-GV65-NCODIC                                 
V34R4                ' mut: ' COMM-GV65-NMUTATION                               
V34R4                ' dte: ' W-GV65-DDELIV                                     
V34R4            DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
V34R4         END-IF                                                            
           ELSE                                                                 
              MOVE '0017'          TO MEC15C-NSEQERR                            
              MOVE 'E'             TO MEC15C-STAT                               
              MOVE COMM-GV65-NSEQNQ TO MEC15C-NSEQNQ                            
              PERFORM CREER-EC15                                                
           END-IF                                                               
           .                                                                    
       FIN-RESERVATION-DACEM. EXIT.                                             
      *******************************                                           
V34CEN* MODULE DE MISE A JOUR DE LA DATE DE RESERVATION CHEZ DACEM              
V34CEN MODULE-MAJ-DACEM SECTION.                                                
V34CEN***************************                                               
V34CEN                                                                          
V34CEN     INITIALIZE BUFFER-AS400                                              
V34CEN     MOVE 'M'      TO TOUCHE-21                                           
V34CEN     PERFORM CALCUL-DELAI                                                 
V34CEN     PERFORM APPEL-DACEM                                                  
V34CEN     IF OK                                                                
V52R0 *       PERFORM ENLEVER-DE-GB07-DACEM                                     
V52R0 *       PERFORM AJOUTER-SUR-GB07-DACEM                                    
V34CEN        MOVE COMM-GV65-NMUTTEMP  TO COMM-GV65-NMUTATION                   
V34R4         IF AUTO-DISPLAY                                                   
V34R4         STRING 'modif DACEM vte: '                                        
V34R4             COMM-GV65-NVENTE                                              
V34R4             ' CODIC: ' COMM-GV65-NCODIC                                   
V34R4             ' mut: ' COMM-GV65-NMUTATION                                  
V34R4             ' dte: ' W-GV65-DDELIV                                        
V34R4          DELIMITED BY SIZE INTO W-MESSAGE                                 
               PERFORM ECRITURE-DISPLAY                                         
V34R4         END-IF                                                            
           ELSE                                                                 
              MOVE '0017'          TO MEC15C-NSEQERR                            
              MOVE 'E'             TO MEC15C-STAT                               
              MOVE COMM-GV65-NSEQNQ TO MEC15C-NSEQNQ                            
              PERFORM CREER-EC15                                                
V34CEN     END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34CEN*    .                                                                    
      *}                                                                        
V34CEN FIN-MODULE-MAJ-DACEM. EXIT.                                              
      *****************************                                             
V34R4  AJOUTER-SUR-GB05-GB15-FINAL  SECTION.                                    
V34R4 *******************************                                           
V34R4                                                                           
V34R4 *    MOVE COMM-GV65-NMUTATION TO GB15-NMUTATION                           
V34R4      MOVE GB05-NMUTATION TO GB15-NMUTATION                                
V34R4            IF AUTO-DISPLAY                                                
V34R4               STRING 'ajout 05-15 '                                       
V34R4               GB15-NMUTATION                                              
V34R4               DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ECRITURE-DISPLAY                                    
V34R4            END-IF                                                         
V34R4      MOVE COMM-GV65-NCODIC    TO GB15-NCODIC                              
V34R4      MOVE COMM-GV65-QTE       TO WQTE                                     
V34R4                                                                           
V34R4      COMPUTE WQVOLUME = GA00-QHAUTEUR * GA00-QPROFONDEUR *                
V34R4                         GA00-QLARGEUR                                     
V34R4                                                                           
V34R4      MOVE 0 TO WQNBLIGNES                                                 
V34R4                                                                           
V34R4      PERFORM SELECT-RTGB05                                                
V34R4      IF OK                                                                
V34R4         PERFORM SELECT-RTGA14                                             
V34R4         IF OK                                                             
V34R4            PERFORM SELECT-RTFL50                                          
V34R4            IF OK                                                          
V34R4               IF COMM-GV65-CRE-GB15-FINAL = 'O'                           
V34R4                  PERFORM AJOUTER-SUR-GB15-FINAL                           
V34R4               END-IF                                                      
V34R4               PERFORM UPDATE-RTGB05                                       
V34R4               MOVE GB05-NMUTATION TO COMM-GV65-NMUTATION                  
V34R4            END-IF                                                         
V34R4            IF AUTO-DISPLAY                                                
V34R4               STRING 'CR�ATION    GB05 '                                  
V34R4               GB15-NMUTATION                                              
V34R4               ' VTE 'COMM-GV65-NVENTE                                     
V34R4               ' CODIC ' COMM-GV65-NCODIC                                  
V34R4               DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ECRITURE-DISPLAY                                    
V34R4            END-IF                                                         
V34R4         END-IF                                                            
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-AJOUTER-SUR-GB05-FINAL. EXIT.                                        
V34R4 *********************************                                         
V52R0  AJOUTER-SUR-GB07-DACEM  SECTION.                                         
  !   *******************************                                           
  !                                                                             
  !        MOVE COMM-GV65-QTE       TO WQTE                                     
  !                                                                             
  !        PERFORM UPDATE-RTGB07-PLUS.                                          
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-AJOUTER-SUR-GB07-DACEM. EXIT.                                        
V52R0 *********************************                                         
V52R0  ENLEVER-DE-GB07-DACEM  SECTION.                                          
  !   *******************************                                           
  !                                                                             
  !        MOVE COMM-GV65-QTE       TO WQTE                                     
  !                                                                             
  !        IF COMM-GV65-FILIERE-IN NOT = SPACE                                  
  !         AND COMM-GV65-DMUT NOT = SPACE                                      
  !            PERFORM UPDATE-RTGB07-MOINS                                      
  !        END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-ENLEVER-DE-GB07-DACEM. EXIT.                                         
V52R0 *********************************                                         
V34R4  AJOUTER-SUR-GB15-FINAL  SECTION.                                         
V34R4 *******************************                                           
V34R4                                                                           
V34R4      PERFORM UPDATE-RTGB15                                                
V34R4      IF NON-TROUVE AND OK                                                 
V34R4         MOVE 1 TO WQNBLIGNES                                              
V34R4         PERFORM INSERT-RTGB15                                             
V34R4      END-IF.                                                              
V34R4      IF AUTO-DISPLAY                                                      
V34R4         MOVE SQLCODE TO EDIT-SQL                                          
V34R4         STRING 'CR�ATION GB15 '                                           
V34R4         GB15-NMUTATION                                                    
V34R4         ' VTE 'COMM-GV65-NVENTE                                           
V34R4         ' CODIC ' COMM-GV65-NCODIC ' SQLCODE ' EDIT-SQL                   
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
V34R4                                                                           
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF                                                               
V34R4      MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO                                   
V34R4                               COMM-GV65-NSOCDEPOT                         
V34R4      MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO                                   
V34R4                               COMM-GV65-NDEPOT.                           
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-AJOUTER-SUR-GB15-FINAL. EXIT.                                        
V34R4 *********************************                                         
V34R4                                                                           
      * --------------------------------------------------- *                   
      * RECHERCHE DU LIEU D'EXPEDITION POUR L'EMX           *                   
      * ON POURRAIT PRENDRE L'EMR PUISQUE LES LIGNES SONT   *                   
      * IDENTIQUES POUR CETTE DONNEE.                       *                   
      * --------------------------------------------------- *                   
       CHERCHE-MDLIV-EMX SECTION.                                               
      *****************************                                             
           MOVE FUNC-SELECT TO TRACE-SQL-FUNCTION.                              
           PERFORM CLEF-GA0100.                                                 
           MOVE SPACES      TO RVGA0100.                                        
           MOVE 'MDLIV'     TO GA01-CTABLEG1                                    
           MOVE 'EMX'       TO GA01-CTABLEG2                                    
           PERFORM SELECT-RTGA01                                                
           IF OK                                                                
              IF TROUVE                                                         
                 SET MDLIV-TROUVE    TO TRUE                                    
                 MOVE GA01-WTABLEG   TO MDLIV-WTABLEG                           
                 MOVE MDLIV-WDONNEES TO WS-WDONNEES                             
              ELSE                                                              
                 IF AUTO-DISPLAY                                                
V43R0               IF COMM-GV65-NVENTE > '0000000'                             
                      STRING 'MDLIV NON TROUVE POUR VENTE '                     
                       COMM-GV65-NVENTE                                         
                       DELIMITED BY SIZE INTO W-MESSAGE                         
V43R0              ELSE                                                         
  !                   STRING 'MDLIV NON TROUVE POUR ORDRE '                     
  !                    COMM-GV65-NORDRE                                         
  !                    DELIMITED BY SIZE INTO W-MESSAGE                         
V43R0              END-IF                                                       
                   PERFORM ECRITURE-DISPLAY                                     
                 END-IF                                                         
V50R0 *         IF COMM-GV65-CONTROLE                                           
V50R0           IF COMM-GV65-CTRL-MGV64                                         
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V34CEN             MOVE 'PB ACCES GA01 MDLIV EMX '                              
V34CEN                             TO COMM-GV65-LMESSAGE                        
V34CEN             MOVE '6502'             TO COMM-GV65-CODRET                  
V34CEN          ELSE                                                            
                   MOVE '2'            TO KONTROL                               
                   MOVE '0002'         TO PTMG-NSEQERR                          
                   PERFORM MLIBERRGEN                                           
V34CEN          END-IF                                                          
              END-IF                                                            
           END-IF.                                                              
       FIN-CHERCHE-MDLIV-EMX. EXIT.                                             
      ******************************                                            
       CHERCHE-LIEU-STOCKAGE SECTION.                                           
      *******************************                                           
           SET WSTOCK-KO TO TRUE                                                
           IF MDLIV-NON-TROUVE                                                  
              GO TO FIN-CHERCHE-LIEU-STOCKAGE                                   
           END-IF                                                               
      * TS                                                                      
           INITIALIZE TS-MFL05-DONNEES.                                         
           SET TS-MFL05-DEMANDE-CODIC   TO TRUE.                                
           MOVE COMM-GV65-NCODIC        TO TS-MFL05-NCODIC                      
           MOVE GA00-CFAM               TO TS-MFL05-CFAM                        
           IF EIBTRMID = LOW-VALUES                                             
              MOVE EIBTASKN              TO P-EIBTASKN                          
              MOVE X-EIBTASKN TO EIBTRMID                                       
           END-IF                                                               
           STRING 'FL05' EIBTRMID DELIMITED BY SIZE INTO IDENT-TS               
           END-STRING                                                           
           PERFORM WRITE-TS-MFL05                                               
      * COMM                                                                    
           MOVE MDLIV-CTYPTRAIT             TO COMM-MFL05-CTYPTRAIT             
           MOVE MDLIV-LDEPLIVR-EXP          TO COMM-MFL05-LDEPLIV               
           MOVE COMM-GV65-DDELIV            TO COMM-MFL05-DEFFET                
           MOVE LENGTH OF COMM-MFL05-APPLI  TO LONG-COMMAREA-LINK               
           MOVE COMM-MFL05-APPLI            TO Z-COMMAREA-LINK                  
           MOVE 'MFL05'                     TO NOM-PROG-LINK                    
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK             TO COMM-MFL05-APPLI                 
           IF COMM-MFL05-CODRET-ERREUR                                          
              MOVE '1'                 TO KONTROL                               
V50R0         IF COMM-GV65-CTRL-MGV64                                           
V50R0 *       IF COMM-GV65-CONTROLE                                             
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V34CEN           MOVE 'FL05'             TO COMM-GV65-CODRET                    
V34CEN           MOVE COMM-MFL05-NSEQERR TO COMM-GV65-LMESSAGE                  
V34CEN        ELSE                                                              
                 MOVE COMM-MFL05-NSEQERR  TO COMM-GV65-CODRET                   
                 MOVE COMM-MFL05-LMESSAGE TO COMM-GV65-LMESSAGE                 
V34CEN        END-IF                                                            
           ELSE                                                                 
      * -> LECTURE TS DE RETOUR DU MFL05 ET CONTROLE DES STOCKS.                
              MOVE LENGTH OF TS-MFL05-DONNEES  TO LONG-TS                       
              MOVE 1                     TO RANG-TS                             
              PERFORM READ-TS                                                   
              MOVE Z-INOUT               TO TS-MFL05-DONNEES                    
              MOVE TS-MFL05-DEPOT (1)    TO WS-DEPOT                            
              PERFORM CHERCHE-MUTATION-MAGIQUE                                  
              IF WMAGIE-KO                                                      
                 PERFORM VARYING IDEP FROM 1 BY 1                               
      *{ reorder-array-condition 1.1                                            
      *          UNTIL TS-MFL05-DEPOT (IDEP) = SPACES                           
      *          OR IDEP > 5 OR WSTOCK-OK                                       
      *--                                                                       
                 UNTIL IDEP > 5                                                 
                 OR TS-MFL05-DEPOT (IDEP) = SPACES OR WSTOCK-OK                 
      *}                                                                        
NV0910           OR WS-RESER-FOURN-OK                                           
LD0709*          OR COMM-GV65-CODRET NOT = '0000'                               
                    MOVE TS-MFL05-DEPOT (IDEP) TO WS-DEPOT                      
                    PERFORM CHERCHE-QTE-STOCK                                   
NV0910              SET WS-RESER-FOURN-KO      TO TRUE                          
NV0910              IF  WSTOCK-KO                                               
V50R1                  IF  WS-B2B-MUT-MAG-OUI                                   
V50R1 *Rechercher la mut sur les vente   B2B                                    
V50R1                      SET MUT-SUR-B2B-KO         TO TRUE                   
"                          MOVE COMM-GV65-QTE         TO WQTE                   
V50R1                      PERFORM RECHERCHE-MUT-MAGIQUE                        
V50R1                  END-IF                                                   
                       IF WMAGIE-KO                                             
NV0910                     PERFORM RESERVER-CDE-FOURNISSEUR                     
NV0910                 END-IF                                                   
NV0910              END-IF                                                      
                 END-PERFORM                                                    
              END-IF                                                            
NV0910*       IF WSTOCK-OK OR WMAGIE-OK                                         
NV0910        IF WSTOCK-OK OR WMAGIE-OK OR WS-RESER-FOURN-OK                    
                 MOVE WS-NSOCDEPOT        TO COMM-GV65-NSOCDEPOT                
                 MOVE WS-NDEPOT           TO COMM-GV65-NDEPOT                   
              ELSE                                                              
V34R4            MOVE GV11-NSOCLIVR  TO COMM-GV65-NSOCDEPOT                     
V34R4            MOVE GV11-NDEPOT    TO COMM-GV65-NDEPOT                        
                 IF NOT COMM-GV65-DACEM-OUI                                     
v41r1 * on renseigne le lieu de stockage meme si on a pas trouve pour           
v41r1 * que les ventes en 66vgm soient bien renseign�es                         
v41r1               MOVE WS-NSOCDEPOT        TO COMM-GV65-NSOCDEPOT             
v41r1               MOVE WS-NDEPOT           TO COMM-GV65-NDEPOT                
NV0910              IF WS-DATE-INVALIDE                                         
V50R0                 IF COMM-GV65-CTRL-MGV64                                   
V50R0 *                OR COMM-GV65-CTRL-SPE                                    
V50R0 *               IF COMM-GV65-CONTROLE                                     
V34CEN                  MOVE '6531'             TO COMM-GV65-CODRET             
V34CEN                  MOVE WS-DRECEPT6        TO COMM-GV65-LMESSAGE           
V34CEN                ELSE                                                      
NV0910                  MOVE WS-DRECEPT6         TO PTMG-ALP (1)                
  !                     MOVE   '0031'            TO PTMG-NSEQERR                
  !                     PERFORM MLIBERRGEN                                      
V34CEN                END-IF                                                    
  !                   MOVE '1'                 TO  KONTROL                      
NV0910              ELSE                                                        
NV0910                 IF WS-DCDE-FNR(1) NOT = SPACES OR                        
  !                      (WS-QCDE-FNR(1) NOT = SPACES AND ZEROES)               
V50R0                    IF COMM-GV65-CTRL-MGV64                                
V50R0 *                    OR COMM-GV65-CTRL-SPE                                
V50R0 *                  IF COMM-GV65-CONTROLE                                  
V34CEN                      MOVE '6532'           TO COMM-GV65-CODRET           
V34CEN                      STRING WS-POSTE-CDE (1) WS-POSTE-CDE (2)            
V34CEN                      DELIMITED BY SIZE INTO COMM-GV65-LMESSAGE           
V34CEN                   ELSE                                                   
  !                         MOVE WS-POSTE-CDE (1)   TO PTMG-ALP (1)             
  !                         MOVE WS-POSTE-CDE (2)   TO PTMG-ALP (2)             
  !                         MOVE   '0032'           TO PTMG-NSEQERR             
  !                         PERFORM MLIBERRGEN                                  
V34CEN                   END-IF                                                 
  !                      MOVE '1'                 TO  KONTROL                   
NV0910                 ELSE                                                     
V50R0                    IF COMM-GV65-CTRL-MGV64                                
V50R0 *                   OR COMM-GV65-CTRL-SPE                                 
V50R0 *                  IF COMM-GV65-CONTROLE                                  
V34CEN                     MOVE '6530'           TO COMM-GV65-CODRET            
V34CEN                   ELSE                                                   
                           MOVE COMM-GV65-NCODIC       TO PTMG-ALP (1)          
                           MOVE GA00-CFAM              TO PTMG-ALP (2)          
                           MOVE   '0030'               TO PTMG-NSEQERR          
                           MOVE '1'                    TO  KONTROL              
                           PERFORM MLIBERRGEN                                   
V34CEN                   END-IF                                                 
  !                      MOVE '1'                 TO  KONTROL                   
                       END-IF                                                   
NV0910              END-IF                                                      
                END-IF                                                          
              END-IF                                                            
           END-IF                                                               
LD0210     IF WSTOCK-KO AND WMAGIE-KO AND WS-RESER-FOURN-KO                     
LD0210     AND COMM-GV65-DACEM-NON                                              
  !           MOVE '1'                 TO  KONTROL                              
V50R0         IF COMM-GV65-CTRL-MGV64                                           
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *       IF COMM-GV65-CONTROLE                                             
V34CEN           MOVE '6530'           TO COMM-GV65-CODRET                      
V34CEN        ELSE                                                              
LD0210           MOVE COMM-GV65-NCODIC       TO PTMG-ALP (1)                    
LD0210           MOVE GA00-CFAM              TO PTMG-ALP (2)                    
LD0210           MOVE   '0030'               TO PTMG-NSEQERR                    
LD0210           PERFORM MLIBERRGEN                                             
V34CEN        END-IF                                                            
LD0210     END-IF                                                               
V50R0      SET LIEU-STOCK-CONNU TO TRUE                                         
           PERFORM DELETE-TS.                                                   
       FIN-CHERCHE-LIEU-STOCKAGE. EXIT.                                         
      **********************************                                        
       CHERCHE-WMULTI SECTION.                                                  
      *************************                                                 
           MOVE FUNC-SELECT TO TRACE-SQL-FUNCTION.                              
           PERFORM CLEF-GA0100.                                                 
           MOVE SPACES      TO RVGA0100.                                        
           MOVE 'NCGFC'     TO GA01-CTABLEG1                                    
           MOVE 'MULTI'     TO GA01-CTABLEG2                                    
           PERFORM SELECT-RTGA01                                                
           IF NOT NON-TROUVE AND                                                
              NOT NORMAL                                                        
                GO TO FIN-CHERCHE-WMULTI                                        
           END-IF                                                               
           IF NON-TROUVE                                                        
              MOVE SPACES         TO GA01-WTABLEG                               
           END-IF.                                                              
           IF GA01-WTABLEG(1:1) = ' ' OR 'N'                                    
              SET COMM-GV65-MUT-MULTI TO TRUE                                   
           END-IF.                                                              
       FIN-CHERCHE-WMULTI. EXIT.                                                
      ***************************                                               
       CHERCHE-QTE-STOCK SECTION.                                               
      ****************************                                              
           MOVE WS-NSOCDEPOT          TO GS10-NSOCDEPOT                         
           MOVE WS-NDEPOT             TO GS10-NDEPOT                            
           PERFORM SELECT-RTGS10                                                
           MOVE ZEROES TO WS-QUANTITE-DISPONIBLE                                
           IF TROUVE AND GS10-QSTOCK > GS10-QSTOCKRES                           
              IF GS10-QSTOCKRES NOT < ZEROES                                    
                 COMPUTE WS-QUANTITE-DISPONIBLE =                               
                            GS10-QSTOCK - GS10-QSTOCKRES                        
                 IF WS-QUANTITE-DISPONIBLE >= COMM-GV65-QTE                     
LD0709              SET  WSTOCK-OK           TO TRUE                            
      *             MOVE WS-NSOCDEPOT        TO COMM-GV65-NSOCDEPOT             
      *             MOVE WS-NDEPOT           TO COMM-GV65-NDEPOT                
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
       FIN-CHERCHE-QTE-STOCK. EXIT.                                             
      *****************************                                             
NV0910 RESERVER-CDE-FOURNISSEUR SECTION.                                        
      ***********************************                                       
  !                                                                             
  !        SET WS-DATE-VALIDE             TO TRUE                               
  !        PERFORM SELECT-RTFL50                                                
  !                                                                             
  !        IF FL50-WRESFOURN NOT = 'N'                                          
V43R0         PERFORM SELECT-RTFL90                                             
  !                                                                             
V43R0         INITIALIZE GFQNT0                                                 
V43R0         MOVE COMM-GV65-SSAAMMJJ    TO GFSAMJ-0                            
V43R0         MOVE '5'                   TO GFDATA                              
V43R0         PERFORM LINK-TETDATC                                              
  !                                                                             
V43R0         COMPUTE GFQNT0 = GFQNT0 + FL90-QDELAIDEP                          
V43R0         MOVE '3'                   TO GFDATA                              
V43R0         PERFORM LINK-TETDATC                                              
V43R0         MOVE GFSAMJ-0              TO WS-DJOUR7                           
  !                                                                             
  !           INITIALIZE WS-TABLE-CDE    IND-TABC                               
              MOVE COMM-GV65-SSAAMMJJ    TO WS-DRECEPT                          
  !                                                                             
  !           PERFORM DECLARE-CRESERV                                           
  !           PERFORM FETCH-CRESERV                                             
  !           PERFORM UNTIL NON-TROUVE                                          
  !                OR WS-RESER-FOURN-OK                                         
  !                OR WS-DATE-INVALIDE                                          
  !                                                                             
v50r0        IF AUTO-DISPLAY                                                    
                 MOVE GF55-QCDE TO W-EDIT-QTE                                   
                 MOVE GF55-QCDERES TO W-EDIT-QTERES                             
  !              STRING 'TRT GF55 CMD ' GF55-DCDE ' DCALCUL '                   
  !              GF55-DCALCUL ' QCDE ' W-EDIT-QTE                               
  !                ' QRES ' W-EDIT-QTERES                                       
  !              DELIMITED BY SIZE INTO W-MESSAGE                               
  !                                                                             
                 PERFORM ECRITURE-DISPLAY                                       
V50R0         END-IF                                                            
  !              IF GF55-QCDE > GF55-QCDERES                                    
  !                 PERFORM IMPACTER-CDE-FOURNISSEUR                            
  !              END-IF                                                         
  !                                                                             
v34cen           IF WS-RESER-FOURN-KO                                           
  !                 PERFORM FETCH-CRESERV                                       
v34cen           END-IF                                                         
  !           END-PERFORM                                                       
  !                                                                             
  !           PERFORM CLOSE-CRESERV                                             
  !                                                                             
  !        END-IF.                                                              
  !                                                                             
NV0910 FIN-RESERVER-CDE-FOURNISSEUR.   EXIT.                                    
      ***************************************                                   
V43R0  SELECT-RTEC03 SECTION.                                                   
  !   ************************                                                  
  !                                                                             
  !        MOVE FUNC-SELECT        TO TRACE-SQL-FUNCTION                        
  !                                                                             
  !        MOVE COMM-GV65-NSOCIETE    TO EC03-NSOCIETE                          
  !        MOVE COMM-GV65-NLIEU       TO EC03-NLIEU                             
  !        MOVE COMM-GV65-NVENTE      TO EC03-NVENTE                            
  !                                                                             
  !        EXEC SQL SELECT DISTINCT   NRELAISID                                 
  !                 INTO    :EC03-NRELAISID                                     
  !                 FROM     RVEC0303                                           
  !                 WHERE    NSOCIETE  = :EC03-NSOCIETE                         
  !                 AND      NLIEU     = :EC03-NLIEU                            
  !                 AND      NVENTE    = :EC03-NVENTE                           
  !                 AND      NRELAISID > '       '                              
                    WITH UR                                                     
  !        END-EXEC.                                                            
  !                                                                             
V43R0  FIN-SELECT-RTEC03.   EXIT.                                               
      ***************************                                               
V43R0  SELECT-RTFL90 SECTION.                                                   
  !   ************************                                                  
V43R0      MOVE FUNC-SELECT        TO TRACE-SQL-FUNCTION                        
V43R0      PERFORM CLEF-FL9000 .                                                
V43R0      MOVE WS-NSOCDEPOT       TO FL90-NSOCDEPOT                            
V43R0      MOVE WS-NDEPOT          TO FL90-NDEPOT                               
  !                                                                             
V43R0      EXEC SQL SELECT   QDELAIDEP                                          
V43R0               INTO    :FL90-QDELAIDEP                                     
V43R0               FROM     RVFL9001                                           
V43R0               WHERE    NSOCDEPOT = :FL90-NSOCDEPOT                        
V43R0               AND      NDEPOT    = :FL90-NDEPOT                           
                    WITH UR                                                     
V43R0      END-EXEC.                                                            
  !                                                                             
V43R0      PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
V43R0      IF NON-TROUVE                                                        
V43R0         MOVE 7               TO FL90-QDELAIDEP                            
V43R0      END-IF.                                                              
  !                                                                             
V43R0  FIN-SELECT-RTFL90.   EXIT.                                               
      ***************************                                               
V41R0  SELECT-RTGA64 SECTION.                                                   
  !   ************************                                                  
  !                                                                             
  !        MOVE COMM-GV65-NCODIC TO GA64-NCODIC                                 
  !        STRING 'EM'  COMM-GV65-WEMPORTE                                      
  !               DELIMITED BY SIZE INTO GA64-CMODDEL                           
  !        END-STRING                                                           
  !        MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
  !        EXEC SQL SELECT                                                      
  !                       NCODIC,                                               
  !                       CMODDEL                                               
  !                 INTO                                                        
  !                      :GA64-NCODIC,                                          
  !                      :GA64-CMODDEL                                          
  !                 FROM     RVGA6400                                           
  !                 WHERE                                                       
  !                       NCODIC = :GA64-NCODIC                                 
  !                 AND   CMODDEL = :GA64-CMODDEL                               
                    WITH UR                                                     
  !        END-EXEC                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                44008   
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
V41R0  FIN-SELECT-RTGA64.   EXIT.                                               
      ***************************                                               
NV0910 DECLARE-CRESERV SECTION.                                                 
  !   **************************                                                
  !                                                                             
  !        MOVE FUNC-DECLARE       TO TRACE-SQL-FUNCTION.                       
  !        PERFORM CLEF-GF5500 .                                                
  !                                                                             
  !        MOVE WS-NSOCDEPOT       TO GF55-NSOCDEPOT                            
  !        MOVE WS-NDEPOT          TO GF55-NDEPOT                               
  !        MOVE COMM-GV65-NCODIC   TO GF55-NCODIC                               
  !                                                                             
  !        EXEC SQL DECLARE  RESERV CURSOR FOR                                  
  !                 SELECT   DCDE                                               
  !                         ,QCDE                                               
  !                         ,QCDERES                                            
V43R0                       ,DCALCUL                                            
                            ,dayofweek (                                        
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' !!                    
      *                      substr( dcde , 7 , 2) !! '/' !!                    
      *                      substr( dcde , 1 , 4))                             
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' !!                    
      *                      substr( dcde , 7 , 2) !! '/' ||                    
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' !!                    
      *                      substr( dcde , 7 , 2) || '/' ||                    
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *                      substr( dcde , 5 , 2) !! '/' ||                    
      *--                                                                       
                             substr( dcde , 5 , 2) || '/' ||                    
      *}                                                                        
                             substr( dcde , 7 , 2) || '/' ||                    
      *}                                                                        
      *}                                                                        
                             substr( dcde , 1 , 4))                             
      *}                                                                        
V43R0               FROM     RVGF5501 A                                         
  !                 WHERE    NSOCDEPOT = :GF55-NSOCDEPOT                        
  !                 AND      NDEPOT = :GF55-NDEPOT                              
  !                 AND      NCODIC = :GF55-NCODIC                              
  !                 AND      DCDE   BETWEEN '19000101' AND '20993112'           
L20410*             AND      DAYOFWEEK (                                        
L20410*                      SUBSTR( DCDE , 5 , 2) !! '/' !!                    
L20410*                      SUBSTR( DCDE , 7 , 2) !! '/' !!                    
L20410*                      SUBSTR( DCDE , 1 , 4))                             
L20410*                      ^=  7                                              
                             AND NOT EXISTS                                     
                             (SELECT '*' FROM RVGF4900 B                        
                               WHERE WACTIF = 'O'                               
                                 AND A.DCDE = B.DPHARE                          
                                 AND CPHARE NOT LIKE 'NS%')                     
  !                 ORDER BY dcalcul                                            
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
  !        END-EXEC.                                                            
  !                                                                             
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
  !        MOVE FUNC-OPEN          TO TRACE-SQL-FUNCTION.                       
  !                                                                             
  !        EXEC SQL OPEN   RESERV END-EXEC.                                     
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
  !                                                                             
NV0910 FIN-DECLARE-CRESERV. EXIT.                                               
      ****************************                                              
NV0910 FETCH-CRESERV SECTION.                                                   
  !   ************************                                                  
  !        MOVE FUNC-FETCH         TO TRACE-SQL-FUNCTION.                       
  !        PERFORM CLEF-GF5500 .                                                
  !                                                                             
  !        EXEC SQL FETCH    RESERV                                             
  !                 INTO    :GF55-DCDE                                          
  !                        ,:GF55-QCDE                                          
  !                        ,:GF55-QCDERES                                       
V43R0                      ,:GF55-DCALCUL                                       
                           ,:w-j-sem :w-f-sem-f                                 
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
  !                                                                             
NV0910 FIN-FETCH-CRESERV. EXIT.                                                 
      **************************                                                
NV0910 CLOSE-CRESERV SECTION.                                                   
  !   ************************                                                  
  !                                                                             
  !        MOVE FUNC-CLOSE         TO TRACE-SQL-FUNCTION.                       
  !        PERFORM CLEF-GF5500 .                                                
  !                                                                             
  !        EXEC SQL CLOSE    RESERV                                             
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
NV0910 FIN-CLOSE-CRESERV. EXIT.                                                 
      **************************                                                
NV0910 IMPACTER-CDE-FOURNISSEUR SECTION.                                        
      ***********************************                                       
  !                                                                             
V43R0 *    IF COMM-GV65-SSAAMMJJ  <  W-DATE-ACTIVATION-GF55                     
V43R0 *       MOVE GF55-DCDE          TO GFSAMJ-0                               
V43R0 *       MOVE '5'                TO GFDATA                                 
      *                                                                         
V43R0 *       PERFORM LINK-TETDATC                                              
  ! *                                                                           
V43R0 *       COMPUTE GFQNT0 = GFQNT0 + FL90-QDELAIDEP                          
      *                                                                         
V43R0 *       MOVE '3'                TO GFDATA                                 
      *                                                                         
V43R0 *       PERFORM LINK-TETDATC                                              
      *                                                                         
V43R0 *       MOVE GFSAMJ-0           TO WS-DCDE7                               
  ! *                                                                           
V43R0 *       IF WS-DCDE7 > WS-DJOUR7                                           
V43R0 *          MOVE WS-DCDE7      TO GFSAMJ-0                                 
V43R0 *       ELSE                                                              
V43R0 *          MOVE WS-DJOUR7     TO GFSAMJ-0                                 
V43R0 *       END-IF                                                            
  !   *    ELSE                                                                 
V43R0         MOVE GF55-DCALCUL       TO GFSAMJ-0                               
  !   *    END-IF.                                                              
  !        MOVE '5'                TO GFDATA                                    
  !        PERFORM LINK-TETDATC                                                 
  !                                                                             
  !        MOVE GFSAMJ-0           TO WS-DRECEPT                                
  !        MOVE GFJMA-3            TO WS-DRECEPT6                               
  !                                                                             
  !        IF WQTE  > (GF55-QCDE - GF55-QCDERES)                                
  !                                                                             
  !          COMPUTE WS-QUANT-DIS   = GF55-QCDE - GF55-QCDERES                  
  !          ADD  1                 TO IND-TABC                                 
  !                                                                             
  !          IF IND-TABC NOT > IND-CDE                                          
  !             MOVE WS-DRECEPT6    TO WS-DCDE-FNR(IND-TABC)                    
  !             MOVE '/'            TO WS-FIL1-FNR(IND-TABC)                    
  !             MOVE WS-QUANT-DIS   TO WS-QCDE-FNR(IND-TABC)                    
  !             MOVE SPACE          TO WS-FIL2-FNR(IND-TABC)                    
  !          END-IF                                                             
  !        ELSE                                                                 
L20410        IF COMM-GV65-DDELIV < WS-DRECEPT                                  
L20410           MOVE WS-DRECEPT    TO COMM-GV65-DDELIV                         
L20410        END-IF                                                            
V43R0 *       IF COMM-GV65-SSAAMMJJ  <  W-DATE-ACTIVATION-GF55                  
V43R0 *          IF (WS-DCDE7 = SPACES OR                                       
      *              COMM-GV65-DDELIV NOT < WS-DCDE7) AND                       
V43R0 *              COMM-GV65-DDELIV  NOT < WS-DJOUR7                          
V43R0 *              IF COMM-GV65-DDELIV NOT < WS-DRECEPT                       
  !   *                 SET WS-RESER-FOURN-OK TO TRUE                           
L20410*                 SET COMM-GV65-STOCK-FOURN-OUI TO TRUE                   
      *                 MOVE WS-NSOCDEPOT TO COMM-GV65-NSOCDEPOT                
      *                 MOVE WS-NDEPOT TO COMM-GV65-NDEPOT                      
  !   *              ELSE                                                       
  !   *                 SET  WS-DATE-INVALIDE      TO TRUE                      
  !   *              END-IF                                                     
  !   *          END-IF                                                         
      *       ELSE                                                              
V43R0            IF COMM-GV65-DDELIV NOT < WS-DRECEPT                           
  !                 SET  WS-RESER-FOURN-OK         TO TRUE                      
L20410              SET  COMM-GV65-STOCK-FOURN-OUI TO TRUE                      
  !              ELSE                                                           
  !                 SET  WS-DATE-INVALIDE      TO TRUE                          
  !              END-IF                                                         
      *       END-IF                                                            
  !        END-IF.                                                              
  !                                                                             
NV0910 FIN-IMPACTER-CDE-FOURNISSEUR .   EXIT.                                   
      ****************************************                                  
       CHERCHE-MUTATION SECTION.                                                
      ****************************                                              
           PERFORM RECHERCHE-BORNE-MUT.                                         
      * -> SELECTION ARTICLE                                                    
      *    MOVE 'LSELA'               TO GA01-CTABLEG1                          
      *    STRING WS-NSOCDEPOT '%%%%%' GA00-CFAM '  '                           
      *       DELIMITED BY SIZE        INTO GA01-CTABLEG2                       
      *    END-STRING                                                           
v50r0 *      IF AUTO-DISPLAY                                                    
  !   *          STRING 'cherche mutation '                                     
      *             GA01-CTABLEG2                                               
  !   *          DELIMITED BY SIZE INTO W-MESSAGE                               
  ! *                                                                           
      *          PERFORM ECRITURE-DISPLAY                                       
v50r0 *       END-IF                                                            
LD0610*    PERFORM DECLARE-RTGA01                                               
LD0610*    PERFORM DECLARE-RTGA01-X-NORM                                        
      *    PERFORM FETCH-RTGA01-X                                               
      *    IF OK                                                                
      *       IF NON-TROUVE                                                     
      *         MOVE '1'           TO KONTROL                                   
V50R0 *         IF COMM-GV65-CTRL-MGV64                                         
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *         IF COMM-GV65-CONTROLE                                           
V34CEN*           MOVE '6529'             TO COMM-GV65-CODRET                   
V34CEN*           MOVE GA01-CTABLEG2      TO COMM-GV65-LMESSAGE                 
V34CEN*         ELSE                                                            
      *           MOVE '0029'        TO PTMG-NSEQERR                            
      *           MOVE GA01-CTABLEG2 TO PTMG-ALP (1)                            
      *           PERFORM MLIBERRGEN                                            
      *           PERFORM CLOSE-RTGA01-X                                        
      *           GO TO FIN-CHERCHE-MUTATION                                    
V34CEN*         END-IF                                                          
      *       END-IF                                                            
      *    ELSE                                                                 
      *       PERFORM CLOSE-RTGA01-X                                            
      *       GO TO FIN-CHERCHE-MUTATION                                        
      *    END-IF                                                               
v50r0 *      IF AUTO-DISPLAY                                                    
  !   *          STRING 'cherche date mutation '                                
      *             WS-TOP-MUTATION                                             
      *             GA01-CTABLEG2                                               
  !   *          DELIMITED BY SIZE INTO W-MESSAGE                               
  ! *                                                                           
      *          PERFORM ECRITURE-DISPLAY                                       
v50r0 *       END-IF                                                            
      *    PERFORM UNTIL NON-TROUVE                                             
      *            OR WS-MUTATION-AFFECTEE OR NOT OK                            
      *            MOVE GA01-CTABLEG2 TO LSELA-CTABLEG2                         
v50r0 *      IF AUTO-DISPLAY                                                    
  !   *          STRING 'cherche date mutation '                                
      *             WS-TOP-MUTATION ' '                                         
      *              LSELA-CFAM ' '  GA00-CFAM                                  
      *             GA01-CTABLEG2                                               
  !   *          DELIMITED BY SIZE INTO W-MESSAGE                               
  ! *                                                                           
      *          PERFORM ECRITURE-DISPLAY                                       
v50r0 *       END-IF                                                            
      *            IF LSELA-CFAM = GA00-CFAM                                    
                      PERFORM CHERCHE-DATE-MUTATION                             
      *            END-IF                                                       
      *            PERFORM FETCH-RTGA01-X                                       
      *    END-PERFORM.                                                         
      *    PERFORM CLOSE-RTGA01-X.                                              
           IF WS-MUTATION-NON-TROUVEE                                           
v50r0        IF AUTO-DISPLAY                                                    
  !              STRING 'mutation non trouvee  '                                
  !              DELIMITED BY SIZE INTO W-MESSAGE                               
  !                                                                             
                 PERFORM ECRITURE-DISPLAY                                       
v50r0         END-IF                                                            
V34CEN        IF COMM-GV65-CTRL-MGV64                                           
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V34CEN*       IF COMM-GV65-CONTROLE                                             
V34CEN             MOVE '6505'             TO COMM-GV65-CODRET                  
V34CEN             MOVE SPACES             TO COMM-GV65-LMESSAGE                
V34CEN        ELSE                                                              
                MOVE GB05-NSOCENTR          TO PTMG-ALP (1)                     
                MOVE GB05-NDEPOT            TO PTMG-ALP (2)                     
                MOVE GB05-NSOCIETE          TO PTMG-ALP (3)                     
                MOVE GB05-NLIEU             TO PTMG-ALP (4)                     
                IF WS-HV-DMUTATION-1  > COMM-GV65-SSAAMMJJ                      
                  MOVE WS-HV-DMUTATION-1 (7:2)  TO  PTMG-ALP (5) (1:2)          
                  MOVE '/'                      TO  PTMG-ALP (5) (3:1)          
                  MOVE WS-HV-DMUTATION-1 (5:2)  TO  PTMG-ALP (5) (4:2)          
                  MOVE '/'                      TO  PTMG-ALP (5) (6:1)          
                  MOVE WS-HV-DMUTATION-1 (3:2)  TO  PTMG-ALP (5) (7:2)          
                END-IF                                                          
                MOVE '0005'         TO PTMG-NSEQERR                             
                PERFORM MLIBERRGEN                                              
V34CEN        END-IF                                                            
           ELSE                                                                 
V43R0         MOVE GB05-DMUTATION     TO COMM-GV65-DMUTATION                    
V43R0         MOVE GB05-DMUTATION     TO COMM-GV65-DATE-DISPO                   
              IF  GB05-DMUTATION NOT = W-DDELIV-INITIALE                        
              AND COMM-GV65-CONTROLE                                            
                MOVE '1'                   TO  KONTROL                          
V50r0           IF COMM-GV65-CTRL-MGV64                                         
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50r0 *         IF COMM-GV65-CONTROLE                                           
V34CEN             MOVE GB05-DMUTATION     TO COMM-GV65-LMESSAGE                
V34CEN             MOVE '6523'             TO COMM-GV65-CODRET                  
V52R0             MOVE GB05-NMUTATION TO COMM-GV65-NMUTATION                    
  !   *           IF GB05-LHEURLIMIT(1:4) = SPACE                               
  !   *             MOVE '0430'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
  !   *           IF GB05-LHEURLIMIT(1:4) = '1   ' OR '2   '                    
  !   *             MOVE '1200'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
V34CEN          ELSE                                                            
                  MOVE '0023'                TO  PTMG-NSEQERR                   
                  MOVE GB05-DMUTATION (7:2)  TO  PTMG-ALP (1) (1:2)             
                  MOVE '/'                   TO  PTMG-ALP (1) (3:1)             
                  MOVE GB05-DMUTATION (5:2)  TO  PTMG-ALP (1) (4:2)             
                  MOVE '/'                   TO  PTMG-ALP (1) (6:1)             
                  MOVE GB05-DMUTATION (3:2)  TO  PTMG-ALP (1) (7:2)             
                  PERFORM MLIBERRGEN                                            
V34CEN          END-IF                                                          
      *       ELSE                                                              
V52R0 *           MOVE GB05-NMUTATION TO COMM-GV65-NMUTATION                    
  !   *           IF GB05-LHEURLIMIT(1:4) = SPACE                               
  !   *             MOVE '0430'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
  !   *           IF GB05-LHEURLIMIT(1:4) = '1   ' OR '2   '                    
  !   *             MOVE '1200'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
V43R0 *           MOVE GB05-DMUTATION TO COMM-GV65-DDELIV                       
              END-IF                                                            
v50r0        IF AUTO-DISPLAY                                                    
  !              STRING 'trouve mut  ' gb05-nmutation                           
  !                ' heure mut ' COMM-GV65-HMUTATION                            
  !              DELIMITED BY SIZE INTO W-MESSAGE                               
  !                                                                             
                 PERFORM ECRITURE-DISPLAY                                       
v50r0         END-IF                                                            
           END-IF.                                                              
       FIN-CHERCHE-MUTATION. EXIT.                                              
      *****************************                                             
LD0610 CHERCHE-MUTATION-SPE SECTION.                                            
-     *******************************                                           
-                                                                               
           PERFORM RECHERCHE-BORNE-MUT.                                         
-     *    PERFORM CLEF-GA0100.                                                 
-                                                                               
-     *    MOVE SPACES      TO RVGA0100.                                        
-     *    MOVE 'NCGFC'     TO GA01-CTABLEG1                                    
-     *    STRING 'EXPE' COMM-GV65-WEMPORTE                                     
-     *       DELIMITED BY SIZE        INTO GA01-CTABLEG2                       
-     *    END-STRING                                                           
-                                                                               
      *    PERFORM SELECT-RTGA01                                                
-                                                                               
-     *    IF TROUVE                                                            
-     *       MOVE GA01-WTABLEG(5:4) TO WS-MUT                                  
-     *    ELSE                                                                 
-     *       MOVE SPACES         TO GA01-WTABLEG                               
-     *    END-IF.                                                              
-     * -> SELECTION ARTICLE                                                    
-     *    MOVE 'LSELA'               TO GA01-CTABLEG1                          
-                                                                               
-     *    STRING WS-NSOCDEPOT WS-MUT '%' GA00-CFAM '  '                        
-     *       DELIMITED BY SIZE        INTO GA01-CTABLEG2                       
-     *    END-STRING                                                           
-                                                                               
-     *    PERFORM DECLARE-RTGA01                                               
-                                                                               
-     *    PERFORM FETCH-RTGA01                                                 
-                                                                               
-     *    IF OK                                                                
-     *       IF NON-TROUVE                                                     
-     *          PERFORM CLOSE-RTGA01                                           
-     *          GO TO FIN-CHERCHE-MUTATION-SPE                                 
-     *       END-IF                                                            
-     *    ELSE                                                                 
-     *       PERFORM CLOSE-RTGA01                                              
-     *       GO TO FIN-CHERCHE-MUTATION-SPE                                    
-     *    END-IF                                                               
-                                                                               
-     *    PERFORM UNTIL NON-TROUVE                                             
-     *    OR WS-MUTATION-AFFECTEE OR NOT OK                                    
-     *       MOVE GA01-CTABLEG2 TO LSELA-CTABLEG2                              
-     *       IF LSELA-CFAM = GA00-CFAM                                         
-                PERFORM CHERCHE-DATE-MUTATION                                  
-     *       END-IF                                                            
-     *       PERFORM FETCH-RTGA01                                              
-     *    END-PERFORM.                                                         
- *                                                                             
-     *    PERFORM CLOSE-RTGA01.                                                
-                                                                               
-          IF WS-MUTATION-NON-TROUVEE                                           
-             CONTINUE                                                          
-          ELSE                                                                 
V43R0         MOVE GB05-DMUTATION     TO COMM-GV65-DATE-DISPO                   
V43R2         MOVE GB05-DMUTATION     TO COMM-GV65-DMUTATION                    
-             IF  GB05-DMUTATION NOT = W-DDELIV-INITIALE                        
-             AND COMM-GV65-CONTROLE                                            
V52R0             MOVE GB05-NMUTATION TO COMM-GV65-NMUTATION                    
  !   *           IF GB05-LHEURLIMIT(1:4) = SPACE                               
  !   *             MOVE '0430'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
  !   *           IF GB05-LHEURLIMIT(1:4) = '1   ' OR '2   '                    
  !   *             MOVE '1200'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
-               MOVE '1'                   TO  KONTROL                          
V50r0           IF COMM-GV65-CTRL-MGV64                                         
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50r0 *         IF COMM-GV65-CONTROLE                                           
V34CEN             MOVE '6523'             TO COMM-GV65-CODRET                  
V34CEN             MOVE GB05-DMUTATION TO COMM-GV65-LMESSAGE                    
V34CEN          ELSE                                                            
-                  MOVE '0023'                TO  PTMG-NSEQERR                  
-                  MOVE GB05-DMUTATION (7:2)  TO  PTMG-ALP (1) (1:2)            
-                  MOVE '/'                   TO  PTMG-ALP (1) (3:1)            
-                  MOVE GB05-DMUTATION (5:2)  TO  PTMG-ALP (1) (4:2)            
-                  MOVE '/'                   TO  PTMG-ALP (1) (6:1)            
-                  MOVE GB05-DMUTATION (3:2)  TO  PTMG-ALP (1) (7:2)            
-                  PERFORM MLIBERRGEN                                           
V34CEN          END-IF                                                          
-     *       ELSE                                                              
-     *          MOVE GB05-NMUTATION TO COMM-GV65-NMUTATION                     
V52R0 *           MOVE GB05-NMUTATION TO COMM-GV65-NMUTATION                    
  !   *           IF GB05-LHEURLIMIT(1:4) = SPACE                               
  !   *             MOVE '0430'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
  !   *           IF GB05-LHEURLIMIT(1:4) = '1   ' OR '2   '                    
  !   *             MOVE '1200'               TO COMM-GV65-HMUTATION            
  !   *           ELSE                                                          
  !   *             MOVE GB05-LHEURLIMIT(1:4) TO COMM-GV65-HMUTATION            
V52R0 *           END-IF                                                        
V43R0 *          MOVE GB05-DMUTATION TO COMM-GV65-DDELIV                        
-             END-IF                                                            
-          END-IF.                                                              
-                                                                               
-      FIN-CHERCHE-MUTATION-SPE. EXIT.                                          
      ********************************                                          
      *                                                                         
       CHERCHE-DATE-MUTATION SECTION.                                           
      ********************************                                          
  !         SET PARAM-NON-TROUVE    TO TRUE                                     
  !   * recherche du cselart                                                    
  !        MOVE  '0' TO WF-TSTENVOI                                             
  !        PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
  !          UNTIL  WP-RANG-TSTENVOI  > 999  OR WC-TSTENVOI-FIN                 
  !                                                                             
  !           PERFORM LECTURE-TSTENVOI                                          
      *      IF AUTO-DISPLAY                                                    
test  *          STRING 'cherche cselart '                                      
      *          TSTENVOI-NDEPOT    ' '                                         
      *          TSTENVOI-WEMPORTE  ' '                                         
      *          TSTENVOI-CTRL1     ' '                                         
      *          DELIMITED BY SIZE INTO W-MESSAGE                               
      *                                                                         
      *          PERFORM ECRITURE-DISPLAY                                       
      *       END-IF                                                            
  !           IF  WC-TSTENVOI-SUITE                                             
  !           AND TSTENVOI-NDEPOT       = WS-NDEPOT                             
  !           AND TSTENVOI-WEMPORTE     = COMM-GV65-WEMPORTE                    
              AND TSTENVOI-CTRL1     = 'G'                                      
  !              SET PARAM-TROUVE    TO TRUE                                    
  !              SET WC-TSTENVOI-FIN TO TRUE                                    
  !           END-IF                                                            
  !        END-PERFORM.                                                         
  !                                                                             
V50R0 *    MOVE ZERO                     TO W-NB-TENTATIVES                     
           MOVE WS-NSOCDEPOT             TO GB05-NSOCENTR                       
           MOVE WS-NDEPOT                TO GB05-NDEPOT                         
           MOVE MDLIV-LDEPLIVR-EXP (1:3) TO GB05-NSOCIETE                       
           MOVE MDLIV-LDEPLIVR-EXP (4:3) TO GB05-NLIEU                          
           MOVE TSTENVOI-LIBELLE(1 : 5)  TO GB05-CSELART                        
             IF AUTO-DISPLAY                                                    
test             STRING 'cherche date mut '                                     
                 GB05-NSOCENTR ' '                                              
                 GB05-NDEPOT        ' '                                         
                 GB05-NSOCIETE         ' '                                      
                 GB05-NLIEU            ' '                                      
                 GB05-CSELART                                                   
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
           PERFORM DECLARE-CGB05-MUT.                                           
      * ON EST DANS LE CAS OU ON CHERCHE DES MUT QUI PARTENT EN MEME            
      * TEMPS POUR DACEM ET DARTY. ON A PAS TROUVER DE MUT DACEM                
      * CORRESPONDANT A LA MUT DARTY POUR LES X PREMIERE TENTATIVES             
      * ON PASSE LES MUTS DEJA TRAIT�ES AS TROUVER DE MUT DACEM                 
           PERFORM FETCH-CGB05-MUT                                              
           PERFORM UNTIL NON-TROUVE OR WS-MUTATION-AFFECTEE                     
              PERFORM CTRL-QUOTAS-DATE-MUT                                      
              IF NOT WS-MUTATION-AFFECTEE                                       
                 PERFORM FETCH-CGB05-MUT                                        
      *       ELSE                                                              
V50R0 *         IF W-MUT-GENERIX = 'O'                                          
V50R0 *            IF COMM-GV65-TENTATIVES > 0                                  
V50R0 *               IF W-NB-TENTATIVES < COMM-GV65-TENTATIVES                 
V50R0 *                  ADD 1 TO W-NB-TENTATIVES                               
V50R0 *                  SET WS-MUTATION-NON-TROUVEE TO TRUE                    
v50r0 *                  PERFORM FETCH-CGB05-MUT                                
V50R0 *               END-IF                                                    
V50R0 *            END-IF                                                       
V50R0 *         END-IF                                                          
V50R0         END-IF                                                            
           END-PERFORM.                                                         
           PERFORM CLOSE-CGB05-MUT.                                             
       FIN-CHERCHE-DATE-MUTATION. EXIT.                                         
      *********************************                                         
      *                                                                         
       CTRL-QUOTAS-DATE-MUT SECTION.                                            
      ******************************                                            
      *- > CONTROLE QUOTAS DE LA MUTATION                                       
V41R0      MOVE  '0' TO WF-TSTENVOI                                             
  !        PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
  !        UNTIL  WC-TSTENVOI-FIN                                               
  !                                                                             
  !           PERFORM LECTURE-TSTENVOI                                          
  !           IF COMM-GV65-WEMPORTE  = TSTENVOI-CTENVOI                         
  !           AND TSTENVOI-NSOCZP    = '00000'                                  
  !           AND TSTENVOI-CTRL1     = 'A'                                      
  !              MOVE GB05-DMUTATION TO GFSAMJ-0                                
  !              MOVE '5'            TO GFDATA                                  
  !              PERFORM LINK-TETDATC                                           
  !              IF TSTENVOI-WJOURMUTAUTOR (GFSMN) = 'N'                        
             IF AUTO-DISPLAY                                                    
test             STRING 'mutation non autorise ' gb05-dmutation                 
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
  !                 GO TO FIN-CTRL-QUOTAS-DATE-MUT                              
  !              END-IF                                                         
  !           END-IF                                                            
V41R0      END-PERFORM                                                          
           STRING 'QUOTAS ' GB05-NMUTATION ' ' GB05-CSELART                     
               '!' COMM-GV65-MUT-SPE '!'                                        
            DELIMITED BY SIZE INTO W-MESSAGE                                    
            PERFORM ECRITURE-DISPLAY                                            
           IF COMM-GV65-MUT-SPE NOT = 'O'                                       
              OR GB05-NDEPOT = '090'                                            
              OR GB05-CSELART = 'CHROP'                                         
           COMPUTE GB05-QNBPIECES  = GB05-QNBPIECES + COMM-GV65-QTE             
           COMPUTE WQVOLUME1       = (GA00-QHAUTEUR * GA00-QPROFONDEUR          
                                   * GA00-QLARGEUR * COMM-GV65-QTE              
                                    + GB05-QVOLUME)                             
           DIVIDE WQVOLUME1  BY 1000000  GIVING WQVOLUMEM3 ROUNDED              
           IF GB05-QNBPQUOTA = 999                                              
              CONTINUE                                                          
             IF AUTO-DISPLAY                                                    
test             STRING 'pas de quotas sur mut ' gb05-nmutation                 
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
           ELSE                                                                 
              IF (WQVOLUMEM3 > GB05-QNBM3QUOTA  AND                             
                                      GB05-QNBM3QUOTA NOT = ZEROES)             
      *                         and   GB05-QNBM3QUOTA NOT = 999)                
                  OR (GB05-QNBPIECES > GB05-QNBPQUOTA AND                       
                                      GB05-QNBPQUOTA NOT = ZEROES)              
      *                         and   GB05-QNBPQUOTA NOT = 999)                 
      * -> ON VA CONTINUER A CHERCHER UNE MUT.                                  
      *       CONTINUE                                                          
                IF AUTO-DISPLAY                                                 
test               STRING 'pas de quotas sur mut ' gb05-nmutation               
                   DELIMITED BY SIZE INTO W-MESSAGE                             
                   PERFORM ECRITURE-DISPLAY                                     
                END-IF                                                          
              ELSE                                                              
                 SET WS-MUTATION-AFFECTEE TO TRUE                               
              END-IF                                                            
           END-IF                                                               
           ELSE                                                                 
      *       IF GB05-QNBM3QUOTA = 999 AND                                      
              IF GB05-QNBPQUOTA = 999                                           
      *           SET WS-MUTATION-AFFECTEE TO TRUE                              
           COMPUTE GB05-QNBPIECES  = GB05-QNBPIECES + COMM-GV65-QTE             
           COMPUTE WQVOLUME1       = (GA00-QHAUTEUR * GA00-QPROFONDEUR          
                                   * GA00-QLARGEUR * COMM-GV65-QTE              
                                    + GB05-QVOLUME)                             
           DIVIDE WQVOLUME1  BY 1000000  GIVING WQVOLUMEM3 ROUNDED              
      *    IF (WQVOLUMEM3 > GB05-QNBM3QUOTA  AND                                
      *                               GB05-QNBM3QUOTA  = 999)                   
           IF        (GB05-QNBPIECES > GB05-QNBPQUOTA )                         
      *                               GB05-QNBPQUOTA = 999)                     
      * -> ON VA CONTINUER A CHERCHER UNE MUT.                                  
      *       CONTINUE                                                          
             IF AUTO-DISPLAY                                                    
test             STRING 'pas de quotas sur mut ' gb05-nmutation                 
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
           ELSE                                                                 
              SET WS-MUTATION-AFFECTEE TO TRUE                                  
           END-IF                                                               
              END-IF                                                            
           end-if.                                                              
       FIN-CTRL-QUOTAS-DATE-MUT. EXIT.                                          
      *********************************                                         
      * ---------------------------------------- *                              
      * SECTION APPEL DACEM                      *                              
      * ---------------------------------------- *                              
       APPEL-DACEM SECTION.                                                     
      **********************                                                    
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-PRE-HH.                                
           MOVE W-MINUTE            TO W-PRE-MM.                                
           MOVE W-SECONDE           TO W-PRE-SS.                                
V50R0      IF PAS-APPEL-DACEM                                                   
V50R0         MOVE '6505' TO COMM-GV65-CODRET                                   
v50r0         MOVE 'DACEM INDISPONIBLE   '  TO COMM-GV65-LMESSAGE               
v50r0         MOVE 1         TO KONTROL                                         
V50R0         GO TO FIN-APPEL-DACEM                                             
V50R0      END-IF.                                                              
V52R0      IF COMM-GV65-FILIERE-IN  =    SPACE AND                              
V43R1         TOUCHE-21 = 'V'                                                   
              IF COMM-GV65-MUT-SPE = 'O'                                        
                 PERFORM CHERCHE-FILIERE-300                                    
              ELSE                                                              
                 PERFORM CHERCHE-FILIERE                                        
              END-IF                                                            
           END-IF.                                                              
V43R1      IF TOUCHE-21 = 'C'                                                   
              MOVE '91' TO W-FILIERE                                            
              MOVE SPACE TO COMM-GV65-FILIERE-IN                                
           END-IF.                                                              
V43R1      IF TOUCHE-21 = 'V' AND W-FILIERE = '91'                              
              IF W-GV65-DDELIV <= WS-DATE-JOUR                                  
                 MOVE WS-DATE-JOUR-PLUS1 TO W-GV65-DDELIV                       
              END-IF                                                            
           END-IF.                                                              
           INITIALIZE COMM-MQ21-APPLI.                                          
           MOVE 'FGDA1'            TO GA01-CTABLEG1                             
           MOVE MDLIV-LDEPLIVR-EXP TO GA01-CTABLEG2                             
           PERFORM SELECT-RTGA01                                                
           IF NOT OK                                                            
              GO TO FIN-APPEL-DACEM                                             
           END-IF                                                               
           IF GA01-WTABLEG (1:1) NOT = 'O'                                      
      * -> CLIENT DACEM NON DEFINI.                                             
V50R0         IF COMM-GV65-CTRL-MGV64                                           
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *       IF COMM-GV65-CONTROLE                                             
V34CEN          MOVE '6520'             TO COMM-GV65-CODRET                     
V34CEN        ELSE                                                              
                MOVE '0020'               TO PTMG-NSEQERR                       
                PERFORM MLIBERRGEN                                              
V34CEN        END-IF                                                            
              GO TO FIN-APPEL-DACEM                                             
           END-IF                                                               
           MOVE GA01-WTABLEG (8:5) TO CRITERE-21                                
           MOVE 'MGV65'            TO PROGRAMME                                 
LD    * ICI POUR CHANGEMENT DE CLIENT                                           
           MOVE MDLIV-LDEPLIVR-EXP TO CLIENT-21                                 
LD0210     IF WS-B2B-DACEM-OUI                                                  
LD0210         MOVE WS-B2B-NUM-DACEM TO CLIENT-21                               
LD0210*        STRING GA01-CTABLEG2(1:3) GA01-WTABLEG(4:3)                      
LD0210*        DELIMITED BY SIZE      INTO CLIENT-21                            
LD0210     END-IF                                                               
           MOVE SPACES             TO REFCRITERE-21                             
V43R1      IF TOUCHE-21 = 'M' OR 'V' OR 'S'                                     
V43R1         IF COMM-GV65-NVENTE > '0000000'                                   
V43R1            MOVE COMM-GV65-NVENTE   TO REFCRITERE-21(1 : 7)                
V43R1            MOVE COMM-GV65-NSEQNQ   TO W-NSEQNQ                            
V43R1            MOVE W-NSEQNQ           TO REFCRITERE-21(8 : 2)                
                 IF AUTO-DISPLAY                                                
                    STRING 'REFCRITERE-21 ' REFCRITERE-21(8 : 2)                
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ECRITURE-DISPLAY                                    
                 END-IF                                                         
V43R1         END-IF                                                            
V43R1      END-IF                                                               
v41r1      IF TOUCHE-21 = 'M' OR 'S'                                            
  !   *       MOVE COMM-GV65-NMUTTEMP  TO W-NMUTTEMP(1 : 6)                     
              PERFORM MEF-ZERO-BLANC                                            
test          IF AUTO-DISPLAY                                                   
                 MOVE COMM-GV65-QTE  TO W-EDIT-QTE                              
V43R0            STRING 'zero blanc ' W-NMUTTEMP                                
test             DELIMITED BY SIZE INTO W-MESSAGE                               
test                                                                            
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
  !           MOVE '001'               TO W-NMUTTEMP(7 : 3)                     
  !           MOVE W-NMUTTEMP TO COMMENT-21(1)                                  
V41R1      END-IF                                                               
      * si le programme appelant donne un N� de filiere, on l'utilise           
V52R0      IF COMM-GV65-FILIERE-IN NOT = SPACE                                  
V52R0         MOVE COMM-GV65-FILIERE-IN TO PTLIVR-21                            
test          IF AUTO-DISPLAY                                                   
                 MOVE COMM-GV65-QTE  TO W-EDIT-QTE                              
V43R0            STRING 'FILIERE ' COMM-GV65-FILIERE-IN                         
                     ' ' W-EDIT-QTE ' ' W-GV65-DDELIV                           
test             DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
V52R0      ELSE                                                                 
V52R0         MOVE W-FILIERE            TO PTLIVR-21                            
test          IF AUTO-DISPLAY                                                   
                 MOVE COMM-GV65-QTE  TO W-EDIT-QTE                              
V43R0            STRING 'CALCUL  FILIERE : '  W-FILIERE                         
                     ' ' W-EDIT-QTE ' ' W-GV65-DDELIV                           
test             DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
V52R0      END-IF.                                                              
           MOVE '   '              TO DEVISE-21                                 
v52r0      MOVE PTLIVR-21 TO COMM-GV65-FILIERE                                  
           MOVE 1                  TO NBRLIGNE-21.                              
           MOVE COMM-GV65-NCODIC   TO NCODIC-21 (1).                            
           MOVE COMM-GV65-QTE      TO QTE-21    (1).                            
      *    MOVE COMM-GV65-SSAAMMJJ TO DLIVRE-21 (1).                            
           MOVE W-GV65-DDELIV      TO DLIVRE-21 (1).                            
                IF AUTO-DISPLAY                                                 
                   STRING 'date envoyee a dacem '  DLIVRE-21 (1)                
                      ' ' PTLIVR-21                                             
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ECRITURE-DISPLAY                                    
                END-IF                                                          
      * pour pouvoir faire des tests en integration, on repond ok si            
      * codic 1240471 avec retour N� de resa a 123456                           
           IF W-CICS-APPLID(1:4) = 'CINT'                                       
              IF COMM-GV65-NCODIC = '1240471'                                   
                  MOVE '0000'    TO COMM-GV65-CODRET                            
                  MOVE DLIVRE-21(1) TO COMM-GV65-DATE-DISPO                     
                  MOVE '123456'      TO NCDE-21                                 
                    PERFORM MEF-BLANC-ZERO                                      
                  MOVE PTLIVR-21   TO COMM-GV65-FILIERE                         
                  GO TO FIN-APPEL-DACEM                                         
              END-IF                                                            
           END-IF.                                                              
           PERFORM ENVOI-MESSAGE                                                
           IF COMM-MQ21-CODRET = '00'                                           
              PERFORM RECEPTION-MQSERIES                                        
           ELSE                                                                 
V50R0         IF COMM-GV65-CTRL-MGV64                                           
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *       IF COMM-GV65-CONTROLE                                             
V34CEN          MOVE 'MQXX'             TO COMM-GV65-CODRET                     
V34CEN          MOVE COMM-MQ21-CODRET   TO COMM-GV65-LMESSAGE                   
V34CEN        ELSE                                                              
                MOVE 'MQXX'            TO PTMG-NSEQERR                          
                MOVE COMM-MQ21-CODRET  TO PTMG-NSEQERR(3:2)                     
                PERFORM MLIBERRGEN                                              
V34CEN        END-IF                                                            
              MOVE 1         TO KONTROL                                         
           END-IF                                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-POST-HH.                               
           MOVE W-MINUTE            TO W-POST-MM.                               
           MOVE W-SECONDE           TO W-POST-SS.                               
      * DANS LE CAS D'UNE MODIFICATION, SI C'EST BON, DACEM RENVOIE             
      * 'Modification effectu�e' DANS INFO-21                                   
v41r0 *    IF COMM-GV65-CAPPEL = 'F' OR 'B'                                     
v41r0 *    IF COMM-GV65-ACT-DACEM = 'M'                                         
v41r1 *       IF INFO-21(1)     = 'modification effectu�e'                      
v41r1 *       OR INFO-21(1)     = 'Modification effectu�e'                      
      *          CONTINUE                                                       
      *       ELSE                                                              
      *          SET ERREUR TO TRUE                                             
      *          MOVE INFO-21 (1)        TO COMM-GV65-LMESSAGE                  
      *          MOVE '6506'             TO COMM-GV65-CODRET                    
      *       END-IF                                                            
      *    END-IF.                                                              
      *                                                                         
              IF AUTO-DISPLAY                                                   
                 STRING 'REQUETE GENERIX ' TOUCHE-21                            
                 ' APPEL ' W-H-PRE-GENERIX                                      
                 ' REPONSE ' W-H-POST-GENERIX                                   
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
      * DANS LE CAS D'UNE SUPPRESSION, SI C'EST BON, DACEM RENVOIE              
      * 'Suppression effectu�e'  DANS INFO-21                                   
v41r1 *    IF COMM-GV65-CAPPEL = 'S'                                            
v41r1      IF COMM-GV65-ACT-DACEM = 'S'                                         
              IF INFO-21(1) NOT = 'Suppression effectu�e'                       
v41r1            SET ERREUR TO TRUE                                             
v41r1            MOVE INFO-21 (1)        TO COMM-GV65-LMESSAGE                  
v41r1            MOVE '6506'             TO COMM-GV65-CODRET                    
                IF AUTO-DISPLAY                                                 
                   STRING 'ano sup dacem sur vte: '                             
                       COMM-GV65-NVENTE                                         
                       ' CODIC: ' COMM-GV65-NCODIC                              
                      ' ' INFO-21(1)                                            
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM ECRITURE-DISPLAY                                    
                END-IF                                                          
              END-IF                                                            
           END-IF.                                                              
      * -> CONTROLE RETOUR MESSAGE                                              
           IF OK                                                                
      * si c'est une reservation dacem et que c'est un produit en               
      * contremarque                                                            
v41r1         IF COMM-GV65-ACT-DACEM = 'C'                                      
V43R0            IF GA00-CAPPRO = 'C'                                           
  !                 IF AUTO-DISPLAY                                             
  !                    STRING 'creation 6506 N� resa '                          
                       NCDE-21                                                  
  !                    ' SUR VENTE : ' COMM-GV65-NVENTE                         
                        '!' FLAGNCODIC-21 (1)                                   
                        '!'                                                     
  !                    DELIMITED BY SIZE INTO W-MESSAGE                         
                       PERFORM ECRITURE-DISPLAY                                 
  !                 END-IF                                                      
  !              END-IF                                                         
  !           END-IF                                                            
      * si c'est une contremarque, dacem repond ko mais fait quand              
      * meme la reservation et nous renvoie le N� de resa                       
              IF FLAGNCODIC-21 (1) = '1' OR '3'                                 
V52R0            IF GA00-CAPPRO = 'C'                                           
V52R0 *             MOVE NCDE-21 TO COMM-GV65-NMUTATION                         
                    PERFORM MEF-BLANC-ZERO                                      
v52r0            ELSE                                                           
                    MOVE '1'         TO KONTROL                                 
V50R0               IF COMM-GV65-CTRL-MGV64                                     
V50R0 *               OR COMM-GV65-CTRL-SPE                                     
V50R0 *             IF COMM-GV65-CONTROLE                                       
V34CEN                MOVE '6506'             TO COMM-GV65-CODRET               
V34CEN              ELSE                                                        
                      MOVE NCODIC-21 (1)  TO PTMG-ALP (1)                       
                      MOVE '0006'         TO PTMG-NSEQERR                       
                      PERFORM MLIBERRGEN                                        
V34CEN              END-IF                                                      
v52r0            END-IF                                                         
              END-IF                                                            
              IF FLAGNCODIC-21 (1) = '2'                                        
V52R0            IF GA00-CAPPRO = 'C'                                           
V52R0 *             MOVE NCDE-21 TO COMM-GV65-NMUTATION                         
                    PERFORM MEF-BLANC-ZERO                                      
v52r0            ELSE                                                           
                    MOVE '1'         TO KONTROL                                 
V50R0               IF COMM-GV65-CTRL-MGV64                                     
V50R0 *               OR COMM-GV65-CTRL-SPE                                     
V50R0 *             IF COMM-GV65-CONTROLE                                       
V34CEN                MOVE '6506'             TO COMM-GV65-CODRET               
V34CEN              ELSE                                                        
                      MOVE INFO-21 (1)        TO COMM-GV65-LMESSAGE             
                      MOVE '6506'             TO COMM-GV65-CODRET               
V34CEN              END-IF                                                      
V34CEN           END-IF                                                         
              END-IF                                                            
              IF FLAGCLIENT-21     = '1'                                        
                 IF OK                                                          
                    MOVE '1'         TO KONTROL                                 
V50R0               IF COMM-GV65-CTRL-MGV64                                     
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *             IF COMM-GV65-CONTROLE                                       
V34CEN                MOVE '6507'             TO COMM-GV65-CODRET               
V34CEN                MOVE CLIENT-21      TO COMM-GV65-LMESSAGE                 
V34CEN              ELSE                                                        
                      MOVE CLIENT-21      TO PTMG-ALP (1)                       
                      MOVE '0007'         TO PTMG-NSEQERR                       
                      PERFORM MLIBERRGEN                                        
V34CEN              END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
              IF FLAGCRITERE-21     = '1'                                       
                 IF OK                                                          
                    MOVE '1'         TO KONTROL                                 
V50R0               IF COMM-GV65-CTRL-MGV64                                     
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *             IF COMM-GV65-CONTROLE                                       
V34CEN                MOVE '6508'             TO COMM-GV65-CODRET               
V34CEN                MOVE CRITERE-21     TO COMM-GV65-LMESSAGE                 
V34CEN              ELSE                                                        
                      MOVE CRITERE-21     TO PTMG-ALP (1)                       
                      MOVE '0008'         TO PTMG-NSEQERR                       
                      PERFORM MLIBERRGEN                                        
V34CEN              END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
              IF FLAGPTLIVR-21     = '1'                                        
                 IF OK                                                          
                    MOVE '1'         TO KONTROL                                 
V50R0               IF COMM-GV65-CTRL-MGV64                                     
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *             IF COMM-GV65-CONTROLE                                       
V34CEN                MOVE '6509'             TO COMM-GV65-CODRET               
V34CEN                MOVE PTLIVR-21      TO COMM-GV65-LMESSAGE                 
V34CEN              ELSE                                                        
                      MOVE PTLIVR-21      TO PTMG-ALP (1)                       
                      MOVE '0009'         TO PTMG-NSEQERR                       
                      PERFORM MLIBERRGEN                                        
V34CEN              END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      * SI ON N'EST PAS EN MODE CONSULTATION, ON TRACE L'APPEL DACEM            
           IF TOUCHE-21 NOT = 'C'                                               
              IF OK                                                             
V50R0 *          MOVE COMM-GV65-NMUTATION TO W-SAVE-NMUTATION                   
      *          MOVE NCDE-21        TO COMM-GV65-NMUTATION                     
                 PERFORM MEF-BLANC-ZERO                                         
                 MOVE '0016'          TO MEC15C-NSEQERR                         
                 MOVE 'I'             TO MEC15C-STAT                            
                 MOVE NCDE-21         TO MEC15C-ALP(1)                          
                 MOVE COMM-GV65-NSEQNQ TO MEC15C-NSEQNQ                         
                 PERFORM CREER-EC15                                             
              ELSE                                                              
                 MOVE '0017'          TO MEC15C-NSEQERR                         
                 MOVE 'E'             TO MEC15C-STAT                            
                 MOVE COMM-GV65-NSEQNQ TO MEC15C-NSEQNQ                         
                 PERFORM CREER-EC15                                             
              END-IF                                                            
           END-IF.                                                              
V43R0      IF TOUCHE-21 = 'C' OR 'V'                                            
  !   *       MOVE DLIVRE-21(1) TO COMM-GV65-DATE-DISPO                         
  !           MOVE W-GV65-DDELIV TO COMM-GV65-DATE-DISPO                        
V43R0 *       MOVE '20110915'   TO COMM-GV65-DATE-DISPO                         
  !           IF AUTO-DISPLAY                                                   
  !              STRING 'date creation dacem  '                                 
                 COMM-GV65-DATE-DISPO                                           
  !              DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
  !           END-IF                                                            
           END-IF                                                               
           .                                                                    
       FIN-APPEL-DACEM. EXIT.                                                   
      ************************                                                  
V52R0  MEF-ZERO-BLANC  SECTION.                                                 
  !   ***************************                                               
  ! *                                                                           
      * on doit gerer les N� avec des espaces derriere ou des zero              
      * devant, le principe est donc de tout mettre au format avec              
      * des zeros devant puis de mettre au format avec des espaces devan        
           MOVE COMM-GV65-NMUTTEMP    TO W-NUM-RESA                             
           IF COMM-GV65-NMUTTEMP(2:1) = ' '                                     
              MOVE COMM-GV65-NMUTTEMP(1:1) TO W-NUM-RESA(7:1)                   
              MOVE '000000' TO W-NUM-RESA(1:6)                                  
           ELSE                                                                 
             IF COMM-GV65-NMUTTEMP(3:1) = ' '                                   
                MOVE COMM-GV65-NMUTTEMP(1:2)                                    
                           TO W-NUM-RESA(6:2)                                   
                MOVE '00000'                                                    
                           TO W-NUM-RESA(1:5)                                   
             ELSE                                                               
               IF COMM-GV65-NMUTTEMP(4:1) = ' '                                 
                  MOVE COMM-GV65-NMUTTEMP(1:3)                                  
                             TO W-NUM-RESA(5:3)                                 
                  MOVE '0000'                                                   
                             TO W-NUM-RESA(1:4)                                 
               ELSE                                                             
                 IF COMM-GV65-NMUTTEMP(5:1) = ' '                               
                    MOVE COMM-GV65-NMUTTEMP(1:4)                                
                               TO W-NUM-RESA(4:4)                               
                    MOVE '000'                                                  
                               TO W-NUM-RESA(1:3)                               
                 ELSE                                                           
                   IF COMM-GV65-NMUTTEMP(6:1) = ' '                             
                      MOVE COMM-GV65-NMUTTEMP(1:5)                              
                                 TO W-NUM-RESA(3:5)                             
                      MOVE '00'                                                 
                                 TO W-NUM-RESA(1:2)                             
                   ELSE                                                         
                     IF COMM-GV65-NMUTTEMP(7:1) = ' '                           
                        MOVE COMM-GV65-NMUTTEMP(1:6)                            
                                   TO W-NUM-RESA(2:6)                           
                        MOVE '0'                                                
                                   TO W-NUM-RESA(1:1)                           
                     END-IF                                                     
                   END-IF                                                       
                 END-IF                                                         
               END-IF                                                           
             END-IF                                                             
           END-IF.                                                              
           IF W-NUM-RESA(1:1) = '0'                                             
             IF W-NUM-RESA(2:1) = '0'                                           
               IF W-NUM-RESA(3:1) = '0'                                         
                 IF W-NUM-RESA(4:1) = '0'                                       
                   IF W-NUM-RESA(5:1) = '0'                                     
                     IF W-NUM-RESA(6:1) = '0'                                   
                        MOVE W-NUM-RESA(7:1)                                    
                                       TO W-NMUTTEMP(1 : 1)                     
                        MOVE SPACE                                              
                                       TO W-NMUTTEMP(2 : 6)                     
                     ELSE                                                       
                        MOVE W-NUM-RESA(6:2)                                    
                                       TO W-NMUTTEMP(1 : 2)                     
                        MOVE SPACE                                              
                                       TO W-NMUTTEMP(3 : 5)                     
                     END-IF                                                     
                   ELSE                                                         
                      MOVE W-NUM-RESA(5:3)                                      
                                     TO W-NMUTTEMP(1 : 3)                       
                      MOVE SPACE                                                
                                     TO W-NMUTTEMP(4 : 4)                       
                   END-IF                                                       
                 ELSE                                                           
                    MOVE W-NUM-RESA(4:4)                                        
                                   TO W-NMUTTEMP(1 : 4)                         
                    MOVE SPACE                                                  
                                   TO W-NMUTTEMP(5 : 3)                         
                 END-IF                                                         
               ELSE                                                             
                  MOVE W-NUM-RESA(3:5)                                          
                                 TO W-NMUTTEMP(1 : 5)                           
                  MOVE SPACE                                                    
                                 TO W-NMUTTEMP(6 : 2)                           
               END-IF                                                           
             ELSE                                                               
                MOVE W-NUM-RESA(2:6)                                            
                               TO W-NMUTTEMP(1 : 6)                             
                MOVE SPACE                                                      
                               TO W-NMUTTEMP(7 : 1)                             
             END-IF                                                             
           ELSE                                                                 
              MOVE W-NUM-RESA(1:6)                                              
                             TO W-NMUTTEMP(1 : 6)                               
              MOVE SPACE                                                        
                             TO W-NMUTTEMP(7 : 1)                               
           END-IF.                                                              
      *                                                                         
      *       GO TO FIN-MEF-ZERO-BLANC.                                         
      *    IF COMM-GV65-NMUTTEMP(7:1)     = ' '                                 
      *       MOVE '00000' TO W-NMUTTEMP(1 : 5)                                 
      *       MOVE COMM-GV65-NMUTTEMP(1:6) TO W-NMUTTEMP(1 : 6)                 
      *    ELSE                                                                 
      *       GO TO FIN-MEF-ZERO-BLANC                                          
      *    END-IF.                                                              
      *                                                                         
      *    IF COMM-GV65-NMUTTEMP(2:1)     = '0'                                 
      *       MOVE '00000' TO W-NMUTTEMP(1 : 5)                                 
      *       MOVE COMM-GV65-NMUTTEMP(1:1) TO W-NMUTTEMP(6 : 1)                 
      *    ELSE                                                                 
      *       GO TO FIN-MEF-ZERO-BLANC                                          
      *    END-IF.                                                              
      *                                                                         
      *    IF COMM-GV65-NMUTTEMP(3:1)     = '0'                                 
      *       MOVE '    ' TO W-NMUTTEMP(3 : 4)                                  
      *       MOVE COMM-GV65-NMUTTEMP(1:2) TO W-NMUTTEMP(1 : 2)                 
      *    ELSE                                                                 
      *       GO TO FIN-MEF-ZERO-BLANC                                          
      *    END-IF.                                                              
      *                                                                         
      *    IF COMM-GV65-NMUTTEMP(4:1)     = '0'                                 
      *       MOVE '   ' TO W-NMUTTEMP(4 : 3)                                   
      *       MOVE COMM-GV65-NMUTTEMP(1:3) TO W-NMUTTEMP(1 : 3)                 
      *    ELSE                                                                 
      *       GO TO FIN-MEF-ZERO-BLANC                                          
      *    END-IF.                                                              
      *                                                                         
      *    IF COMM-GV65-NMUTTEMP(5:1)     = '0'                                 
      *       MOVE '  ' TO W-NMUTTEMP(5 : 2)                                    
      *       MOVE COMM-GV65-NMUTTEMP(1:4) TO W-NMUTTEMP(1 : 4)                 
      *    ELSE                                                                 
      *       GO TO FIN-MEF-ZERO-BLANC                                          
      *    END-IF.                                                              
      *    IF COMM-GV65-NMUTTEMP(6:1)     = '0'                                 
      *       MOVE ' ' TO W-NMUTTEMP(6 : 1)                                     
      *       MOVE COMM-GV65-NMUTTEMP(1:5) TO W-NMUTTEMP(1 : 5)                 
      *    ELSE                                                                 
      *       GO TO FIN-MEF-ZERO-BLANC                                          
      *    END-IF.                                                              
      *    IF COMM-GV65-NMUTTEMP(7:1)     = '0'                                 
      *       MOVE COMM-GV65-NMUTTEMP(1:6) TO W-NMUTTEMP(1 : 6)                 
      *    END-IF.                                                              
V52R0  FIN-MEF-ZERO-BLANC. EXIT.                                                
  !   ***************************                                               
  !                                                                             
V52R0  MEF-BLANC-ZERO  SECTION.                                                 
  !   ***************************                                               
  !                                                                             
           MOVE NCDE-21 TO W-NUM-RESA(2:6)                                      
           MOVE '0'              TO W-NUM-RESA(1:1)                             
           IF NCDE-21(2:1) = ' '                                                
              MOVE '00000' TO W-NUM-RESA(2:5)                                   
              MOVE NCDE-21(1:1) TO W-NUM-RESA(7:1)                              
           ELSE                                                                 
             IF NCDE-21(3:1) = ' '                                              
                MOVE '0000'  TO W-NUM-RESA(2:4)                                 
                MOVE NCDE-21(1:2) TO W-NUM-RESA(6:2)                            
             ELSE                                                               
               IF NCDE-21(4:1) = ' '                                            
                  MOVE '000'   TO W-NUM-RESA(2:3)                               
                  MOVE NCDE-21(1:3) TO W-NUM-RESA(5:3)                          
               ELSE                                                             
                 IF NCDE-21(5:1) = ' '                                          
                    MOVE '00'    TO W-NUM-RESA(2:2)                             
                    MOVE NCDE-21(1:4) TO W-NUM-RESA(4:4)                        
                 ELSE                                                           
                   IF NCDE-21(6:1) = ' '                                        
                      MOVE '0'     TO W-NUM-RESA(2:1)                           
                      MOVE NCDE-21(1:5) TO W-NUM-RESA(3:5)                      
                   End-if                                                       
                 End-if                                                         
               End-if                                                           
             End-if                                                             
           End-if.                                                              
      *                                                                         
      *    IF NCDE-21(2:1) = SPACE                                              
      *       MOVE NCDE-21(1:1)     TO W-NUM-RESA(7:1)                          
      *       MOVE '000000'         TO W-NUM-RESA(1:6)                          
      *    END-IF                                                               
      *    IF NCDE-21(3:1) = SPACE                                              
      *       MOVE NCDE-21(1:2)     TO W-NUM-RESA(6:2)                          
      *       MOVE '00000'          TO W-NUM-RESA(1:5)                          
      *    END-IF                                                               
      *    IF NCDE-21(4:1) = SPACE                                              
      *       MOVE NCDE-21(1:3)     TO W-NUM-RESA(5:3)                          
      *       MOVE '0000'           TO W-NUM-RESA(1:4)                          
      *    END-IF                                                               
      *    IF NCDE-21(5:1) = SPACE                                              
      *       MOVE NCDE-21(1:4)     TO W-NUM-RESA(4:4)                          
      *       MOVE '000'            TO W-NUM-RESA(1:3)                          
      *    END-IF                                                               
      *    IF NCDE-21(6:1) = SPACE                                              
      *       MOVE NCDE-21(1:5)     TO W-NUM-RESA(3:5)                          
      *       MOVE '00'             TO W-NUM-RESA(1:2)                          
      *    END-IF.                                                              
           IF NCDE-21      = SPACE                                              
              MOVE SPACE            TO W-NUM-RESA                               
           END-IF.                                                              
V52R0  FIN-MEF-BLANC-ZERO. EXIT.                                                
  !   ***************************                                               
  !                                                                             
v52r0  CHERCHE-FILIERE  SECTION.                                                
  !   ***************************                                               
  !                                                                             
           PERFORM RECHERCHE-DELAI                                              
  !                                                                             
  !   * ON AJOUTE UN DELAI POUR NE PAS S'IMPUTER SUR UNE MUT DE 15H00           
  !   * A 14H59 PAR EXEMPLE                                                     
  !        MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
  !        MOVE W-GV65-DDELIV TO GB07-DMUTATION                                 
  !        IF W-MM + W-DELAI < 60                                               
  !   *    IF W-MM < 49                                                         
  !           COMPUTE W-MM = W-MM + W-DELAI                                     
  !        ELSE                                                                 
  !           COMPUTE W-MM = W-MM - 60 + W-DELAI                                
  !           COMPUTE W-HH = W-HH + 1                                           
  !        END-IF                                                               
  !        MOVE W-HHMM       TO GB07-HDACEM                                     
  !        IF COMM-GV65-SSAAMMJJ < W-GV65-DDELIV                                
  !           MOVE '0000'       TO GB07-HDACEM                                  
  !           IF AUTO-DISPLAY                                                   
  !              STRING 'forcage heure a zero '                                 
                 COMM-GV65-SSAAMMJJ                                             
  !              '< ' W-GV65-DDELIV                                             
  !              DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
  !           END-IF                                                            
  !        END-IF                                                               
  !        IF COMM-GV65-MUT-MULTI                                               
  !   *    IF WS-NB-PROD = 1                                                    
  !           MOVE ' '             TO GB07-WMULTI                               
  !        ELSE                                                                 
  !           MOVE 'N'             TO GB07-WMULTI                               
  !        END-IF                                                               
  !        IF COMM-GV65-WEMPORTE     = 'P'                                      
  !            MOVE 'CHROP'  TO GB05-CSELART                                    
  !        ELSE                                                                 
  !            MOVE 'EXPED'  TO GB05-CSELART                                    
  !        END-IF.                                                              
  !        IF AUTO-DISPLAY                                                      
  !            STRING 'acces gb07  ' GB07-dMUTATION                             
  !              ' ' gb07-hdacem ' ' gb07-wmulti                                
  !            DELIMITED BY SIZE INTO W-MESSAGE                                 
               PERFORM ECRITURE-DISPLAY                                         
  !        END-IF.                                                              
           EXEC SQL SELECT   FILIERE                                            
                       INTO :GB07-FILIERE                                       
                    FROM     RVGB0700 A , RVGB0503 B                            
                    WHERE    A.DMUTATION = :GB07-DMUTATION                      
                    AND      A.HDACEM  >= :GB07-HDACEM                          
                    AND      A.WMULTI  = :GB07-WMULTI                           
                    AND      A.NMUTATION  = B.NMUTATION                         
                    AND      B.CSELART    = :GB05-CSELART                       
      *{Post-translation Correct-Sign-not
      *             AND      B.QNBPQUOTA   ^= 999                               
                    AND      B.QNBPQUOTA   <> 999                               
      *}
                    AND      A.HDACEM  = ( SELECT MIN(HDACEM)                   
                                  FROM RVGB0700 c , rvgb0503 d                  
                                 WHERE  c.DMUTATION = :GB07-DMUTATION           
                      AND    c.HDACEM  >= :GB07-HDACEM                          
                      AND    c.WMULTI  = :GB07-WMULTI                           
                    AND      c.NMUTATION  = d.NMUTATION                         
      *{Post-translation Correct-Sign-not
      *             AND      D.QNBPQUOTA   ^= 999                               
                    AND      D.QNBPQUOTA   <> 999                               
      *}
                    AND      d.CSELART    = :GB05-CSELART)                      
                    WITH UR                                                     
           END-EXEC.                                                            
V34R4      IF AUTO-DISPLAY                                                      
V34R4         MOVE SQLCODE TO EDIT-SQL                                          
V34R4         STRING 'SELECT GB07 '  EDIT-SQL                                   
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF                                                               
  !        IF SQLCODE NOT = 0                                                   
  !           EXEC SQL DECLARE  CGB07 CURSOR FOR SELECT                         
  !                    A.DMUTATION, A.HDACEM , A.FILIERE                        
  !                 FROM     RVGB0700 A , RVGB0503 B                            
  !                 WHERE    A.DMUTATION > :GB07-DMUTATION                      
  !                 AND      A.WMULTI  = :GB07-WMULTI                           
  !                 AND      A.NMUTATION  = B.NMUTATION                         
  !                 AND      B.CSELART    = :GB05-CSELART                       
      *{Post-translation Correct-Sign-not
      *             AND      B.QNBPQUOTA   ^= 999                               
  !                 AND      B.QNBPQUOTA   <> 999                               
      *}
  !                 ORDER BY A.DMUTATION, A.HDACEM                              
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
  !           END-EXEC                                                          
  !           MOVE FUNC-OPEN        TO TRACE-SQL-FUNCTION                       
  !                                                                             
  !           EXEC SQL OPEN   CGB07                                             
  !           END-EXEC                                                          
              IF SQLCODE NOT = 0                                                
      *{ normalize-exec-xx 1.5                                                  
      *          EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                       
      *--                                                                       
                 EXEC CICS ABEND ABCODE ('MAXT')                                
                 END-EXEC                                                       
      *}                                                                        
              END-IF                                                            
  !                                                                             
  !           PERFORM FETCH-RTGB07                                              
  !           PERFORM CLOSE-RTGB07                                              
  !        END-IF.                                                              
  !        MOVE GB07-DMUTATION TO W-GV65-DDELIV.                                
  !        MOVE GB07-FILIERE   TO W-FILIERE.                                    
V34R4      IF AUTO-DISPLAY                                                      
V34R4         MOVE SQLCODE TO EDIT-SQL                                          
V34R4         STRING 'resultat recherche filiere : '                            
                  W-GV65-DDELIV  ' ' W-FILIERE                                  
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-CHERCHE-FILIERE. EXIT.                                               
  !   *****************************                                             
  !                                                                             
  !    FETCH-RTGB07 SECTION.                                                    
  !   ************************                                                  
  !                                                                             
  !        MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
  !                                                                             
  !        EXEC SQL FETCH   CGB07                                               
  !                 INTO   :GB07-DMUTATION                                      
  !                      , :GB07-HDACEM                                         
  !                      , :GB07-FILIERE                                        
  !        END-EXEC.                                                            
V34R4      IF AUTO-DISPLAY                                                      
V34R4         MOVE SQLCODE TO EDIT-SQL                                          
V34R4         STRING 'FETCH  GB07 '  EDIT-SQL                                   
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF                                                               
           IF SQLCODE = +100                                                    
              MOVE '91'  TO GB07-FILIERE                                        
  !        END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-FETCH-RTGB07. EXIT.                                                  
  !   ************************                                                  
  !                                                                             
  !    CLOSE-RTGB07 SECTION.                                                    
  !   ***********************                                                   
  !                                                                             
  !        MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION.                    
  !                                                                             
  !        EXEC SQL CLOSE   CGB07                                               
  !        END-EXEC.                                                            
  !                                                                             
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-CLOSE-RTGB07. EXIT.                                                  
v52r0 **************************                                                
v52r0  CHERCHE-FILIERE-300  SECTION.                                            
  !   ***************************                                               
  !                                                                             
           PERFORM RECHERCHE-DELAI                                              
  !                                                                             
  !   * ON AJOUTE UN DELAI POUR NE PAS S'IMPUTER SUR UNE MUT DE 15H00           
  !   * A 14H59 PAR EXEMPLE                                                     
  !        MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
  !        MOVE W-GV65-DDELIV TO GB07-DMUTATION                                 
  !        IF W-MM + W-DELAI < 60                                               
  !   *    IF W-MM < 49                                                         
  !           COMPUTE W-MM = W-MM + W-DELAI                                     
  !        ELSE                                                                 
  !           COMPUTE W-MM = W-MM - 60 + W-DELAI                                
  !           COMPUTE W-HH = W-HH + 1                                           
  !        END-IF                                                               
  !        MOVE W-HHMM       TO GB07-HDACEM                                     
  !        IF COMM-GV65-SSAAMMJJ < W-GV65-DDELIV                                
  !           MOVE '0000'       TO GB07-HDACEM                                  
  !           IF AUTO-DISPLAY                                                   
  !              STRING 'forcage heure a zero '                                 
                 COMM-GV65-SSAAMMJJ                                             
  !              '< ' W-GV65-DDELIV                                             
  !              DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
  !           END-IF                                                            
  !        END-IF                                                               
  !        IF COMM-GV65-MUT-MULTI                                               
  !           MOVE ' '             TO GB07-WMULTI                               
  !        ELSE                                                                 
  !           MOVE 'N'             TO GB07-WMULTI                               
  !        END-IF                                                               
  !        IF COMM-GV65-WEMPORTE     = 'P'                                      
  !            MOVE 'CHROP'  TO GB05-CSELART                                    
  !        ELSE                                                                 
  !            MOVE 'EXPED'  TO GB05-CSELART                                    
  !        END-IF.                                                              
  !        IF AUTO-DISPLAY                                                      
  !            STRING 'acces gb07  ' GB07-dMUTATION                             
  !              ' ' gb07-hdacem ' ' gb07-wmulti                                
  !            DELIMITED BY SIZE INTO W-MESSAGE                                 
               PERFORM ECRITURE-DISPLAY                                         
  !        END-IF.                                                              
           EXEC SQL SELECT   FILIERE                                            
                       INTO :GB07-FILIERE                                       
                    FROM     RVGB0700 A , RVGB0503 B                            
                    WHERE    A.DMUTATION = :GB07-DMUTATION                      
                    AND      A.HDACEM  >= :GB07-HDACEM                          
                    AND      A.WMULTI  = :GB07-WMULTI                           
                    AND      A.NMUTATION  = B.NMUTATION                         
                    AND      B.CSELART    = :GB05-CSELART                       
                    AND      B.QNBPQUOTA   = 999                                
                    AND      A.HDACEM  = ( SELECT MIN(HDACEM)                   
                                  FROM RVGB0700 c , rvgb0503 d                  
                                 WHERE  c.DMUTATION = :GB07-DMUTATION           
                      AND    C.HDACEM  >= :GB07-HDACEM                          
                      AND    C.WMULTI  = :GB07-WMULTI                           
                    AND      C.NMUTATION  = D.NMUTATION                         
                    AND      D.QNBPQUOTA   = 999                                
                    AND      D.CSELART    = :GB05-CSELART)                      
                    WITH UR                                                     
           END-EXEC.                                                            
V34R4      IF AUTO-DISPLAY                                                      
V34R4         MOVE SQLCODE TO EDIT-SQL                                          
V34R4         STRING 'SELECT GB07 '  EDIT-SQL                                   
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF                                                               
  !        IF SQLCODE = +100                                                    
  !           EXEC SQL DECLARE  CGB07B CURSOR FOR SELECT                        
  !                    A.DMUTATION, A.HDACEM , A.FILIERE                        
  !                 FROM     RVGB0700 A , RVGB0503 B                            
  !                 WHERE    A.DMUTATION > :GB07-DMUTATION                      
  !                 AND      A.WMULTI  = :GB07-WMULTI                           
  !                 AND      A.NMUTATION  = B.NMUTATION                         
  !                 AND      B.CSELART    = :GB05-CSELART                       
  !                 AND      B.QNBPQUOTA   = 999                                
  !                 ORDER BY A.DMUTATION, A.HDACEM                              
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
  !           END-EXEC                                                          
  !           MOVE FUNC-OPEN        TO TRACE-SQL-FUNCTION                       
  !                                                                             
  !           EXEC SQL OPEN   CGB07B                                            
  !           END-EXEC                                                          
              IF SQLCODE NOT = 0                                                
      *{ normalize-exec-xx 1.5                                                  
      *          EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                       
      *--                                                                       
                 EXEC CICS ABEND ABCODE ('MAXT')                                
                 END-EXEC                                                       
      *}                                                                        
              END-IF                                                            
  !                                                                             
  !           PERFORM FETCH-RTGB07B                                             
  !           PERFORM CLOSE-RTGB07B                                             
  !        END-IF.                                                              
  !        MOVE GB07-DMUTATION TO W-GV65-DDELIV.                                
  !        MOVE GB07-FILIERE   TO W-FILIERE.                                    
V34R4      IF AUTO-DISPLAY                                                      
V34R4         MOVE SQLCODE TO EDIT-SQL                                          
V34R4         STRING 'resultat recherche filiere : '                            
                  W-GV65-DDELIV  ' ' W-FILIERE                                  
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-CHERCHE-FILIERE-300. EXIT.                                           
  !   *****************************                                             
  !                                                                             
  !    RECHERCHE-DELAI SECTION.                                                 
  !   *************************                                                 
  !                                                                             
      *{ normalize-exec-xx 1.5                                                  
  !   *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
  !                                                                             
  !        MOVE EIBTIME             TO W-EIBTIME.                               
  !        MOVE W-HEURE             TO W-HHMM(1:2)                              
  !        MOVE W-MINUTE            TO W-HHMM(3:2)                              
  !                                                                             
  !        INITIALIZE RVGA01ZZ                                                  
  !        MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.                 
  !                                                                             
  !        EXEC SQL                                                             
  !             SELECT WPARAM , LIBELLE                                         
  !             INTO  :XCTRL-WPARAM , :XCTRL-LIBELLE                            
  !             FROM   RVGA01ZZ                                                 
  !             WHERE  SOCZP = 'NECEN'                                          
  !             AND    TRANS = 'MEC65 '                                         
  !             WITH UR                                                         
  !        END-EXEC.                                                            
  !                                                                             
  !        IF SQLCODE = 0                                                       
  !           MOVE XCTRL-WPARAM(1:2)  TO  W-DELAI                               
  !        ELSE                                                                 
  !           MOVE  10                 TO  W-DELAI                              
  !        END-IF.                                                              
  !                                                                             
  !    FIN-RECHERCHE-DELAI. EXIT.                                               
  !   *************************                                                 
  !                                                                             
  !    FETCH-RTGB07B SECTION.                                                   
  !   ************************                                                  
  !                                                                             
  !        MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
  !                                                                             
  !        EXEC SQL FETCH   CGB07B                                              
  !                 INTO   :GB07-DMUTATION                                      
  !                      , :GB07-HDACEM                                         
  !                      , :GB07-FILIERE                                        
  !        END-EXEC.                                                            
V34R4      IF AUTO-DISPLAY                                                      
V34R4         MOVE SQLCODE TO EDIT-SQL                                          
V34R4         STRING 'FETCH  GB07B '  EDIT-SQL                                  
V34R4         DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM ECRITURE-DISPLAY                                          
V34R4      END-IF                                                               
           IF SQLCODE = +100                                                    
              MOVE '91'  TO GB07-FILIERE                                        
  !        END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-FETCH-RTGB07B. EXIT.                                                 
  !   ************************                                                  
  !                                                                             
  !    CLOSE-RTGB07B SECTION.                                                   
  !   ***********************                                                   
  !                                                                             
  !        MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION.                    
  !                                                                             
  !        EXEC SQL CLOSE   CGB07B                                              
  !        END-EXEC.                                                            
  !                                                                             
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-CLOSE-RTGB07B. EXIT.                                                 
v52r0 **************************                                                
       RECHERCHE-RTLI00 SECTION.                                                
      ***************************                                               
      *                                                                         
           MOVE MDLIV-LDEPLIVR-EXP(1:3) TO LI00-NSOCIETE.                       
           MOVE MDLIV-LDEPLIVR-EXP(4:3) TO LI00-NLIEU.                          
           MOVE FUNC-SELECT             TO TRACE-SQL-FUNCTION.                  
           EXEC SQL SELECT                                                      
                          LVPARAM                                               
                    INTO                                                        
                         :LI00-LVPARAM                                          
                    FROM     RVLI0000                                           
                    WHERE                                                       
                          NSOCIETE = :LI00-NSOCIETE                             
                    AND   NLIEU    = :LI00-NLIEU                                
                    AND   CPARAM   = :LI00-CPARAM                               
                    WITH UR                                                     
                    END-EXEC.                                                   
           IF DEBUGGIN = 'OUI'                                                  
              PERFORM TRACE-SQL                                                 
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SPACE TO LI00-LVPARAM                                        
           END-IF.                                                              
       F-RECHERCHE-RTLI00.   EXIT.                                              
      *****************************                                             
          EJECT                                                                 
       ENVOI-MESSAGE SECTION.                                                   
      ************************                                                  
           MOVE '996'                    TO  COMM-MQ21-NSOC.                    
           MOVE '000'                    TO  COMM-MQ21-NLIEU.                   
           MOVE '996'                    TO  COMM-MQ13-NSOC.                    
           MOVE  'ORD'                   TO  COMM-MQ21-FONCTION.                
           MOVE  'TCD21'                 TO  COMM-MQ21-NOMPROG.                 
           MOVE  SPACES                  TO  COMM-MQ21-CORRELID                 
           STRING COMM-GV65-NSOCIETE COMM-GV65-NLIEU                            
           DELIMITED BY SIZE INTO            COMM-MQ21-USER                     
           END-STRING                                                           
           MOVE COMM-MQ21-USER TO            COMM-MQ21-MSGID.                   
           MOVE  '00'                    TO  COMM-MQ21-CODRET                   
                                             COMM-MQ21-ERREUR.                  
           MOVE  'ORD'                   TO  COMM-MQ21-TYPE                     
                                             COMM-MQ13-FONCTION.                
           MOVE '996'                    TO  COMM-MQ21-NSOCMSG                  
           MOVE '000'                    TO  COMM-MQ21-NLIEUMSG                 
           MOVE  '996'                   TO  COMM-MQ21-NSOCDST.                 
           MOVE  '000'                   TO  COMM-MQ21-NLIEUDST.                
           MOVE  0                       TO  COMM-MQ21-NORD                     
           MOVE  'TCD21'                 TO  COMM-MQ21-LPROG                    
                                             COMM-MQ13-NOMPROG.                 
           MOVE  'V1'                    TO  COMM-MQ21-VERSION                  
           MOVE  1                       TO  COMM-MQ21-NBRMSG                   
                                             COMM-MQ21-NBRENR.                  
           MOVE  LENGTH OF BUFFER-AS400  TO  COMM-MQ21-TAILLE                   
           COMPUTE COMM-MQ21-LONGMES =                                          
           LENGTH OF COMM-MQ21-ENTETE + COMM-MQ21-TAILLE                        
           COMPUTE WI = 124 + LENGTH OF COMM-MQ21-ENTETE + 1                    
           MOVE BUFFER-AS400  TO COMM-MQ21-APPLI(WI:COMM-MQ21-TAILLE)           
           STRING COMM-MQ21-APPLI(WI:COMM-MQ21-TAILLE)                          
                                DELIMITED BY SIZE INTO  WS-MESS-MGV65           
           INITIALIZE Z-COMMAREA-LINK                                           
           MOVE COMM-MQ21-APPLI        TO Z-COMMAREA-LINK                       
           MOVE 'MMQ21'                TO NOM-PROG-LINK                         
           COMPUTE LONG-COMMAREA-LINK  = WI + COMM-MQ21-TAILLE - 1              
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK        TO COMM-MQ21-APPLI.                      
           MOVE COMM-MQ21-MSGID          TO  COMM-MQ13-CORRELID.                
           MOVE COMM-MQ21-CORRELID       TO  COMM-MQ13-MSGID.                   
LD0709*    LOG DU MESSAGE ENVOYE DANS LE CAS                                    
LD0709*     IF WS-LOG-UP-OUI  AND TOUCHE-21 = 'V'                               
LD0709      IF W-EC05 = 'O'  AND TOUCHE-21 = 'V'                                
LD0709         PERFORM INSERTION-LOG                                            
LD0709      END-IF.                                                             
       FIN-ENVOI-MESSAGE. EXIT.                                                 
      **************************                                                
       RECEPTION-MQSERIES   SECTION.                                            
      *******************************                                           
           MOVE LENGTH OF COMM-MQ13-ENTREE TO LONG-COMMAREA-LINK .              
           MOVE COMM-MQ13-ENTREE           TO Z-COMMAREA-LINK.                  
           MOVE 'MMQ13'                    TO NOM-PROG-LINK.                    
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK TO COMM-MQ13-APPLI                              
           IF COMM-MQ13-CODRET NOT = '00'                                       
              MOVE 'MQXX'            TO PTMG-NSEQERR                            
              MOVE COMM-MQ13-CODRET  TO PTMG-NSEQERR(3:2)                       
              PERFORM MLIBERRGEN                                                
              MOVE 1         TO KONTROL                                         
           ELSE                                                                 
              COMPUTE WI = LENGTH OF BUFFER-AS400                               
              MOVE COMM-MQ13-MESSAGE(106:WI) TO BUFFER-AS400                    
           END-IF.                                                              
       FIN-RECEPTION-MQSERIES. EXIT.                                            
      ********************************                                          
       CHERCHE-MUTATION-MAGIQUE     SECTION.                                    
      ***********************************                                       
              SET WMAGIE-KO                   TO TRUE                           
              MOVE WS-NSOCDEPOT               TO GB05-NSOCENTR                  
              MOVE WS-NDEPOT                  TO GB05-NDEPOT                    
              MOVE COMM-GV65-QTE              TO WQTE                           
LD0709     IF WS-B2B-MUT-MAG-OUI                                                
V41R0         MOVE  '0' TO WF-TSTENVOI                                          
  !           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                     
  !             UNTIL WC-TSTENVOI-FIN                                           
  !                                                                             
  !              PERFORM LECTURE-TSTENVOI                                       
  !              IF  WC-TSTENVOI-SUITE                                          
  !              AND TSTENVOI-NSOCZP    = '00000'                               
  !              AND TSTENVOI-CTRL12    = 'BB'                                  
v41r2            AND TSTENVOI-SOCIETE     = COMM-GV65-NSOCIETE                  
  !                 MOVE TSTENVOI-LIBELLE(1:7) TO GB15-NMUTATION                
  !                                               WGB05-NMUTATION               
  !                 PERFORM SELECT-RTGB15-TEMPORAIRE                            
  !                 IF TROUVE AND  GB15-QDEMANDEE >= WQTE                       
  !                    SET WMAGIE-OK            TO TRUE                         
  !                 END-IF                                                      
  !              END-IF                                                         
V41R0         END-PERFORM                                                       
           END-IF .                                                             
V41R0      IF WMAGIE-KO AND VENTE-INTERNET          AND                         
LD0709        WS-B2B-MUT-MAG-NON                                                
V41R0         MOVE  '0' TO WF-TSTENVOI                                          
  !           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                     
  !             UNTIL WC-TSTENVOI-FIN                                           
  !                                                                             
  !              PERFORM LECTURE-TSTENVOI                                       
  !              IF  WC-TSTENVOI-SUITE                                          
  !              AND TSTENVOI-NSOCZP    = '00000'                               
  !              AND TSTENVOI-CTRL12    = 'BA'                                  
  !              AND TSTENVOI-SOCIETE     = WS-NSOCDEPOT                        
  !              AND TSTENVOI-LIEU        = WS-NDEPOT                           
  !                 MOVE TSTENVOI-NMUTATION    TO GB15-NMUTATION                
  !                                               WGB05-NMUTATION               
  !                 PERFORM SELECT-RTGB15-TEMPORAIRE                            
  !                 IF TROUVE AND GB15-QDEMANDEE >= WQTE                        
  !                    SET WMAGIE-OK             TO TRUE                        
  !                 END-IF                                                      
  !              END-IF                                                         
V41R0         END-PERFORM                                                       
           END-IF.                                                              
       FIN-SCH-MUT-MAGIQUE. EXIT.                                               
      ****************************                                              
       MODULE-INTERNET SECTION.                                                 
      **************************                                                
V34R4      IF AUTO-DISPLAY                                                      
V43R0         STRING 'APPEL MGV65 PAR NET N� ORDRE: '                           
V43r0             COMM-GV65-NORDRE ' '                                          
V43r0             COMM-GV65-NSOCIETE                                            
                  COMM-GV65-NLIEU                                               
V34R4             ' CODIC: ' COMM-GV65-NCODIC                                   
V43R0             ' CAP: !' GA00-CAPPRO '!'                                     
V34R4          DELIMITED BY SIZE INTO W-MESSAGE                                 
               PERFORM ECRITURE-DISPLAY                                         
V34R4      END-IF.                                                              
           MOVE DATHEUR                    TO GV11-DSYST                        
           MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO GV11-NSOCLIVR                     
           MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO GV11-NDEPOT                       
L20410     MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO GV11-NSOCGEST                     
L20410     MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO GV11-NLIEUGEST                    
L20410     MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO GV11-NSOCDEPLIV                   
L20410     MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO GV11-NLIEUDEPLIV                  
V43R0 * si on a un produit dacem en contremarque, on met a jour                 
V43R0 * la ddeliv et le wcqeresf                                                
V43R0      IF COMM-GV65-DACEM-OUI                                               
  !   * on fait une interrogation pour les dacem afin d'envoyer une             
  !   * alerte a darty.com dans le meccc* quand il n'y en a plus                
v50r0            IF  APPEL-DACEM-OUI                                            
  !                 PERFORM CONSULTATION-DACEM                                  
V50R0            ELSE                                                           
V50R0               MOVE '0000' TO COMM-GV65-CODRET                             
V50R0               MOVE '2'    TO COMM-GV65-ETAT-RESERVATION                   
v50r0            END-IF                                                         
  !   *          IF COMM-GV65-CODRET = '0000'                                   
  !   *             MOVE COMM-GV65-DATE-DISPO TO COMM-GV65-DDELIV               
  !   *          END-IF                                                         
  !   *       END-IF                                                            
V43R0      END-IF.                                                              
           PERFORM INSERT-RTGV22.                                               
           IF SQLCODE = 0                                                       
              PERFORM UPDATE-RTGV11                                             
V43R0         IF COMM-GV65-DACEM-OUI                                            
                 CONTINUE                                                       
              ELSE                                                              
                 IF SQLCODE = 0                                                 
                    MOVE '0000'    TO COMM-GV65-CODRET                          
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
       FIN-MODULE-INTERNET. EXIT.                                               
      ****************************                                              
V34R4  RECHERCHE-BORNE-MUT  SECTION.                                            
V34R4 ******************************                                            
V34R4                                                                           
V34R4 * -> EN MODE CONTROLE, ON CHERCHE DES DATES DE MUTATIONS                  
V34R4 *    > A LA DATE DU JOUR POUR DONNER UNE DATE DE DELIVRANCE.              
V34R4 *    DANS TOUS LES AUTRES CAS, ON CHERCHE LES MUTATIONS                   
V34R4 *    DONT LA DATE PEUT-ETRE CELLE DU JOUR.                                
V34R4 *    ON CHERCHE A MUTER LE PLUS VITE POSSIBLE.                            
V34R4 *    IF COMM-GV65-VALIDATION OR COMM-GV65-CRE-GB05-FINAL = 'O'            
V34R4      IF COMM-GV65-CRE-GB05-FINAL = 'O'                                    
v50r0 *       OR W-MUT-GENERIX = 'O'                                            
V34R4         MOVE COMM-GV65-DDELIV TO WS-HV-DMUTATION-1                        
V34R4      ELSE                                                                 
V34R4         IF WS-DRECEPT > COMM-GV65-SSAAMMJJ                                
V34R4            MOVE WS-DRECEPT         TO WS-HV-DMUTATION-1                   
V34R4         ELSE                                                              
V34R4            MOVE COMM-GV65-SSAAMMJJ TO WS-HV-DMUTATION-1                   
V34R4         END-IF                                                            
V34R4      END-IF                                                               
V34R4      MOVE WS-HV-DMUTATION-1 TO WS-HV-DMUTATION-2                          
V34R4      IF NOT COMM-GV65-CONTROLE                                            
V34R4         OR WS-HV-DMUTATION-1  >  COMM-GV65-SSAAMMJJ                       
V50R0 *       OR COMM-GV65-CTRL-SPE                                             
V34R4         MOVE WS-HV-DMUTATION-1  TO GFSAMJ-0                               
V34R4         MOVE '5' TO GFDATA                                                
V34R4         PERFORM LINK-TETDATC                                              
V34R4         COMPUTE  GFQNT0 = GFQNT0 - 1                                      
V34R4         MOVE '3' TO GFDATA                                                
V34R4         PERFORM LINK-TETDATC                                              
V34R4         MOVE GFSAMJ-0           TO WS-HV-DMUTATION-1                      
V34R4      END-IF                                                               
V34R4                                                                           
V34R4 * -> RECHERCHE DES MUTATIONS SUR 10 JOURS MAXI, C'EST COMME �A            
V34R4 *    LE BUT C'EST DE NE PAS RECUPERER LES MUTS DE 2049                    
V34R4 *    QUE L'ON VEUT LA MUTATIONS LA PLUS PROCHE DU JOUR.                   
V34R4 *    ET QU'ON SE DIT QUE SUR 5 JOURS ON DOIT BIEN EN TROUVER.             
V34R4 *    MOVE COMM-GV65-SSAAMMJJ TO GFSAMJ-0                                  
V34R4      MOVE WS-HV-DMUTATION-2  TO GFSAMJ-0                                  
V34R4      MOVE '5' TO GFDATA                                                   
V34R4                                                                           
V34R4      PERFORM LINK-TETDATC                                                 
V34R4                                                                           
V34R4      ADD 10 TO GFQNT0                                                     
V34R4                                                                           
V34R4      MOVE '3' TO GFDATA                                                   
V34R4                                                                           
V34R4      PERFORM LINK-TETDATC                                                 
V34R4                                                                           
V34R4      MOVE GFSAMJ-0           TO WS-HV-DMUTATION-2                         
           IF AUTO-DISPLAY                                                      
              STRING 'bornes mut '                                              
               WS-HV-DMUTATION-1  ' et ' WS-HV-DMUTATION-2                      
               DELIMITED BY SIZE INTO W-MESSAGE                                 
               PERFORM ECRITURE-DISPLAY                                         
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
V34R4 *    .                                                                    
      *}                                                                        
V34R4  FIN-RECHERCHE-BORNE-MUT. EXIT.                                           
V34R4 ********************************                                          
      ********************************************                              
      * SECTION DES ACCES DB2                    *                              
      ********************************************                              
       SELECT-RTGA00 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-SELECT TO TRACE-SQL-FUNCTION.                              
           PERFORM CLEF-GA0000.                                                 
           MOVE COMM-GV65-NCODIC TO GA00-NCODIC                                 
           EXEC SQL                                                             
                SELECT   CMARQ         ,                                        
                         CFAM          ,                                        
                         QLARGEUR      ,                                        
                         QPROFONDEUR   ,                                        
                         QHAUTEUR      ,                                        
                         QPOIDS        ,                                        
                         WDACEM        ,                                        
V43R0                    CAPPRO                                                 
                INTO    :GA00-CMARQ                                             
                       ,:GA00-CFAM                                              
                       ,:GA00-QLARGEUR                                          
                       ,:GA00-QPROFONDEUR                                       
                       ,:GA00-QHAUTEUR                                          
                       ,:GA00-QPOIDS                                            
                       ,:GA00-WDACEM                                            
V43R0                  ,:GA00-CAPPRO                                            
                  FROM RVGA0000                                                 
                 WHERE NCODIC = :GA00-NCODIC                                    
                 WITH UR                                                        
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF OK                                                                
              IF TROUVE                                                         
                 SET RTGA00-TROUVE TO TRUE                                      
                 IF GA00-WDACEM = 'O'                                           
                    SET COMM-GV65-DACEM-OUI TO TRUE                             
V43R0               IF GA00-CAPPRO = 'C'                                        
V43R0                  IF COMM-GV65-SSAAMMJJ NOT <                              
V43R0                                       W-DATE-ACTIVATION-CQE               
V43R0                     MOVE 'O'   TO COMM-GV65-ARTICLE-CQE                   
V43R0                  END-IF                                                   
V43R0               END-IF                                                      
                 END-IF                                                         
              ELSE                                                              
                 IF AUTO-DISPLAY                                                
                    STRING 'RTGA00 NON TROUVE POUR VENTE '                      
                     COMM-GV65-NVENTE ' CODIC ' COMM-GV65-NCODIC                
                     DELIMITED BY SIZE INTO W-MESSAGE                           
                     PERFORM ECRITURE-DISPLAY                                   
                 END-IF                                                         
V50R0            IF COMM-GV65-CTRL-MGV64                                        
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *          IF COMM-GV65-CONTROLE                                          
V34CEN              MOVE GA00-NCODIC        TO COMM-GV65-LMESSAGE               
V34CEN              MOVE '6510'             TO COMM-GV65-CODRET                 
V34CEN           ELSE                                                           
                    MOVE '2'                   TO KONTROL                       
                    MOVE GA00-NCODIC           TO PTMG-ALP (1)                  
                    MOVE   '0010'              TO PTMG-NSEQERR                  
                    PERFORM MLIBERRGEN                                          
V34CEN           END-IF                                                         
               END-IF                                                           
           END-IF.                                                              
       FIN-SELECT-RTGA00. EXIT.                                                 
      **************************                                                
       SELECT-RTGA01 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGA01'              TO TABLE-NAME                             
           PERFORM CLEF-GA0100.                                                 
           EXEC SQL                                                             
                SELECT WTABLEG                                                  
                  INTO :GA01-WTABLEG                                            
                  FROM RVGA0100                                                 
                WHERE  CTABLEG1 = :GA01-CTABLEG1                                
                  AND  CTABLEG2 = :GA01-CTABLEG2                                
                  WITH UR                                                       
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-SELECT-RTGA01. EXIT.                                                 
      **************************                                                
       SELECT-RTGA14 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GA1400.                                                 
           EXEC SQL SELECT   WSEQFAM                                            
                    INTO    :GA14-WSEQFAM                                       
                    FROM     RVGA1400                                           
                    WHERE    CFAM  = :GA00-CFAM                                 
                    WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF OK                                                                
              IF NON-TROUVE                                                     
                 MOVE ZEROES               TO GA14-WSEQFAM                      
              END-IF                                                            
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-SELECT-RTGA14. EXIT.                                                 
      **************************                                                
       DECLARE-RTGA01 SECTION.                                                  
      *************************                                                 
           MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GA0100.                                                 
           EXEC SQL DECLARE  CGA01 CURSOR FOR                                   
                    SELECT   CTABLEG2,                                          
                             WTABLEG                                            
                    FROM     RVGA0100                                           
                    WHERE    CTABLEG1 = :GA01-CTABLEG1                          
                    AND      CTABLEG2 LIKE :GA01-CTABLEG2                       
                    ORDER BY CTABLEG2                                           
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
           END-EXEC.                                                            
           MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
           EXEC SQL OPEN   CGA01                                                
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-DECLARE-RTGA01. EXIT.                                                
      ***************************                                               
       FETCH-RTGA01 SECTION.                                                    
      ************************                                                  
           MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GA0100.                                                 
           EXEC SQL FETCH   CGA01                                               
                    INTO   :GA01-CTABLEG2,                                      
                           :GA01-WTABLEG                                        
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-FETCH-RTGA01. EXIT.                                                  
      ************************                                                  
       CLOSE-RTGA01 SECTION.                                                    
      ***********************                                                   
           MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GA0100.                                                 
           EXEC SQL CLOSE   CGA01                                               
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-CLOSE-RTGA01. EXIT.                                                  
      **************************                                                
LD0610 DECLARE-RTGA01-X-NORM SECTION.                                           
LD0610********************************                                          
LD0610                                                                          
LD0610     MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
LD0610                                                                          
LD0610     PERFORM CLEF-GA0100.                                                 
LD0610                                                                          
LD0610     EXEC SQL DECLARE  CGA01-X CURSOR FOR                                 
LD0610              SELECT   CTABLEG2,                                          
LD0610                       WTABLEG                                            
LD0610              FROM     RVGA0100 A                                         
LD0610              WHERE    A.CTABLEG1 = :GA01-CTABLEG1                        
LD0610              AND      A.CTABLEG2 LIKE :GA01-CTABLEG2                     
LD0610              AND NOT EXISTS(SELECT '*' FROM RVGA0100 B                   
LD0610                          WHERE  B.CTABLEG1 = 'NCGFC'                     
LD0610                          AND B.CTABLEG2 LIKE 'EXPE%'                     
LD0610                          AND SUBSTR( A.CTABLEG2 , 4  , 4 ) =             
LD0610                                      SUBSTR(B.WTABLEG , 5 , 4))          
LD0610              ORDER BY CTABLEG2                                           
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
LD0610     END-EXEC.                                                            
LD0610                                                                          
LD0610     MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
LD0610                                                                          
LD0610     EXEC SQL OPEN   CGA01-X                                              
LD0610     END-EXEC.                                                            
LD0610                                                                          
LD0610     PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
LD0610*    .                                                                    
      *}                                                                        
LD0610 FIN-DECLARE-RTGA01-X-NORM. EXIT.                                         
      **********************************                                        
LD0610 FETCH-RTGA01-X SECTION.                                                  
-     ***************************                                               
-                                                                               
-          MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
-                                                                               
-          PERFORM CLEF-GA0100.                                                 
-                                                                               
-          EXEC SQL FETCH   CGA01-X                                             
-                   INTO   :GA01-CTABLEG2,                                      
-                          :GA01-WTABLEG                                        
-          END-EXEC.                                                            
-                                                                               
-          PERFORM TEST-CODE-RETOUR-SQL.                                        
- .                                                                             
-      FIN-FETCH-RTGA01-X. EXIT.                                                
      ***************************                                               
LD0610 CLOSE-RTGA01-X SECTION.                                                  
-     ************************                                                  
-                                                                               
-          MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION.                    
-                                                                               
-          PERFORM CLEF-GA0100.                                                 
-                                                                               
-          EXEC SQL CLOSE   CGA01-X                                             
-          END-EXEC.                                                            
-                                                                               
-          PERFORM TEST-CODE-RETOUR-SQL.                                        
- .                                                                             
-      FIN-CLOSE-RTGA01-X. EXIT.                                                
      ***************************                                               
       SELECT-RTFL50 SECTION.                                                   
      ************************                                                  
           MOVE WS-NSOCDEPOT          TO FL50-NSOCDEPOT                         
           MOVE WS-NDEPOT             TO FL50-NDEPOT                            
           MOVE COMM-GV65-NCODIC      TO FL50-NCODIC                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-FL5000                                                  
           EXEC SQL SELECT   CAPPRO, QDELAIAPPRO, CMODSTOCK1,           07447009
                             WMODSTOCK1, CMODSTOCK2, WMODSTOCK2,                
                             CMODSTOCK3, WMODSTOCK3, WRESFOURN                  
                    INTO    :FL50-CAPPRO         , :FL50-QDELAIAPPRO  , 07449909
                            :FL50-CMODSTOCK1     , :FL50-WMODSTOCK1   ,         
                            :FL50-CMODSTOCK2     , :FL50-WMODSTOCK2   ,         
                            :FL50-CMODSTOCK3     , :FL50-WMODSTOCK3   ,         
                            :FL50-WRESFOURN                             07449909
                    FROM     RVFL5000                                           
                    WHERE    NSOCDEPOT = :FL50-NSOCDEPOT                        
                    AND      NDEPOT    = :FL50-NDEPOT                           
                    AND      NCODIC    = :FL50-NCODIC                           
                    WITH UR                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF OK                                                                
               IF NON-TROUVE                                                    
                  INITIALIZE  FL50-CAPPRO                                       
                              FL50-QDELAIAPPRO                                  
                              FL50-WRESFOURN                                    
                              FL50-CMODSTOCK1                                   
                              FL50-WMODSTOCK1                                   
                              FL50-CMODSTOCK2                                   
                              FL50-WMODSTOCK2                                   
                              FL50-CMODSTOCK3                                   
                              FL50-WMODSTOCK3                                   
                  MOVE 'N' TO FL50-WRESFOURN                                    
               END-IF                                                           
               MOVE SPACES                      TO WS-CMODSTOCK                 
               IF FL50-WMODSTOCK1 = 'O'                                         
                  MOVE FL50-CMODSTOCK1          TO WS-CMODSTOCK                 
               ELSE                                                             
                  IF FL50-WMODSTOCK2 = 'O'                                      
                     MOVE FL50-CMODSTOCK2       TO WS-CMODSTOCK                 
                  ELSE                                                          
                     IF FL50-WMODSTOCK2 = 'O'                                   
                        MOVE FL50-CMODSTOCK3    TO WS-CMODSTOCK                 
                     END-IF                                                     
                  END-IF                                                        
               END-IF                                                           
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-SELECT-RTFL50. EXIT.                                                 
      **************************                                                
       SELECT-RTGS10 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GS1000.                                                 
           MOVE COMM-GV65-NCODIC      TO GS10-NCODIC                            
           MOVE 'V'                   TO GS10-NSSLIEU                           
           MOVE 'N'                   TO GS10-WSTOCKMASQ                        
           EXEC SQL SELECT   QSTOCK,                                            
                             QSTOCKRES                                          
                    INTO    :GS10-QSTOCK,                                       
                            :GS10-QSTOCKRES                                     
                    FROM     RVGS1000                                           
                    WHERE    NSOCDEPOT  = :GS10-NSOCDEPOT                       
                    AND      NDEPOT     = :GS10-NDEPOT                          
                    AND      NCODIC     = :GS10-NCODIC                          
                    AND      WSTOCKMASQ = :GS10-WSTOCKMASQ                      
                    AND      NSSLIEU    = :GS10-NSSLIEU                         
                    WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF OK                                                                
              IF NON-TROUVE                                                     
                 INITIALIZE GS10-QSTOCK GS10-QSTOCKRES                          
              END-IF                                                            
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-SELECT-RTGS10. EXIT.                                                 
      ***************************                                               
       UPDATE-RTGS10 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB0500.                                                 
           MOVE 'V'                   TO GS10-NSSLIEU                           
           MOVE 'N'                   TO GS10-WSTOCKMASQ                        
           MOVE COMM-GV65-NCODIC      TO GS10-NCODIC                            
           MOVE COMM-GV65-SSAAMMJJ TO GS10-DMAJSTOCK                            
           MOVE DATHEUR            TO GS10-DSYST                                
           EXEC SQL UPDATE   RVGS1000                                           
                    SET      QSTOCKRES  =  QSTOCKRES + :WQTE,                   
                             DMAJSTOCK  = :GS10-DMAJSTOCK,                      
                             DSYST      = :GS10-DSYST                           
                    WHERE    NSOCDEPOT  = :GS10-NSOCDEPOT                       
                    AND      NDEPOT     = :GS10-NDEPOT                          
                    AND      NCODIC     = :GS10-NCODIC                          
                    AND      WSTOCKMASQ = :GS10-WSTOCKMASQ                      
                    AND      NSSLIEU    = :GS10-NSSLIEU                         
      *{Post-translation Correct-Sign-not
      *             AND      QSTOCKRES + :WQTE ^< 0                             
                    AND      QSTOCKRES + :WQTE >= 0                             
      *}
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF OK                                                                
              IF NON-TROUVE AND WQTE > 0                                        
                 INITIALIZE                    GS10-QSTOCK                      
                 MOVE WQTE                  TO GS10-QSTOCKRES                   
                 MOVE 'N'                   TO GS10-WARTINC                     
                 PERFORM INSERT-RTGS10                                          
              END-IF                                                            
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-UPDATE-RTGS10. EXIT.                                                 
      **************************                                                
       INSERT-RTGS10 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GS1000.                                                 
           EXEC SQL INSERT                                                      
                    INTO     RVGS1000                                           
                            (NSOCDEPOT    , NDEPOT      , NCODIC      ,         
                             WSTOCKMASQ   , NSSLIEU     , QSTOCK      ,         
                             QSTOCKRES    , DMAJSTOCK   , WARTINC     ,         
                             DSYST)                                             
                    VALUES (:RVGS1000)                                          
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NOT OK                                                            
V50R0            IF COMM-GV65-CTRL-MGV64                                        
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *          IF COMM-GV65-CONTROLE                                          
V34CEN              MOVE GB05-DMUTATION     TO COMM-GV65-LMESSAGE               
V34CEN              MOVE '6511'             TO COMM-GV65-CODRET                 
V34CEN           ELSE                                                           
                    MOVE GS10-NSOCDEPOT         TO PTMG-ALP (1)                 
                    MOVE GS10-NDEPOT            TO PTMG-ALP (2)                 
                    MOVE GS10-NCODIC            TO PTMG-ALP (3)                 
                    MOVE GS10-WSTOCKMASQ        TO PTMG-ALP (4)                 
                    MOVE GS10-NSSLIEU           TO PTMG-ALP (5)                 
                    MOVE   '0011'               TO PTMG-NSEQERR                 
                    PERFORM MLIBERRGEN                                          
V34CEN           END-IF                                                         
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-INSERT-RTGS10. EXIT.                                                 
      **************************                                                
NV0910 UPDATE-RTGF55 SECTION.                                                   
      ************************                                                  
  !                                                                             
  !        MOVE FUNC-UPDATE        TO TRACE-SQL-FUNCTION.                       
  !        PERFORM CLEF-GF5500.                                                 
  !        MOVE DATHEUR            TO GF55-DSYST.                               
  !                                                                             
v43r0      EXEC SQL UPDATE   RVGF5501                                           
  !            SET      QCDERES   = QCDERES + :WQTE,                            
  !                     DSYST     = :GF55-DSYST                                 
  !            WHERE    NSOCDEPOT = :GF55-NSOCDEPOT                             
  !            AND      NDEPOT    = :GF55-NDEPOT                                
  !            AND      NCODIC    = :GF55-NCODIC                                
  !            AND      DCDE      = :GF55-DCDE                                  
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  ! .                                                                           
NV0910 FIN-UPDATE-RTGF55.    EXIT.                                              
      ****************************                                              
       SELECT-RTGB05 SECTION.                                                   
      ***********************                                                   
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB0500.                                                 
           EXEC SQL SELECT   NSOCENTR     , NDEPOT      , DMUTATION   ,         
                             QNBM3QUOTA   , QNBPQUOTA   , QVOLUME     ,         
                             QNBPIECES,     QNBLIGNES   , QNBLLANCE   ,         
                             WVAL, NSOCIETE, NLIEU                              
                    INTO    :GB05-NSOCENTR       , :GB05-NDEPOT       ,         
                            :GB05-DMUTATION      , :GB05-QNBM3QUOTA   ,         
                            :GB05-QNBPQUOTA      , :GB05-QVOLUME      ,         
                            :GB05-QNBPIECES      , :GB05-QNBLIGNES    ,         
                            :GB05-QNBLLANCE      , :GB05-WVAL ,                 
                            :GB05-NSOCIETE       , :GB05-NLIEU                  
                    FROM     RVGB0501                                           
                    WHERE    NMUTATION = :GB05-NMUTATION                        
                    WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *    IF OK                                                                
      *       IF NON-TROUVE                                                     
V34CEN*          IF COMM-GV65-CTRL-MGV64                                        
V34CEN*             MOVE GB05-DMUTATION     TO COMM-GV65-LMESSAGE               
V34CEN*             MOVE '6512'             TO COMM-GV65-CODRET                 
V34CEN*          ELSE                                                           
      *             MOVE GB05-NMUTATION TO PTMG-ALP (1)                         
      *             MOVE   '0012'        TO PTMG-NSEQERR                        
      *             PERFORM MLIBERRGEN                                          
V34CEN*          END-IF                                                         
      *       END-IF                                                            
      *    END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-SELECT-RTGB05. EXIT.                                                 
      **************************                                                
       UPDATE-RTGB05 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB0500.                                                 
           MOVE DATHEUR TO GB05-DSYST.                                          
           EXEC SQL UPDATE   RVGB0501                                           
                    SET QVOLUME   = QVOLUME   + (:WQVOLUME * :WQTE),            
                             QNBPIECES = QNBPIECES + :WQTE       ,              
                             QNBLIGNES = QNBLIGNES + :WQNBLIGNES ,              
                             DSYST     = :GB05-DSYST                            
                    WHERE    NMUTATION = :GB05-NMUTATION                        
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF OK                                                                
               IF NON-TROUVE                                                    
V50R0            IF COMM-GV65-CTRL-MGV64                                        
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *          IF COMM-GV65-CONTROLE                                          
V34CEN              MOVE GB05-NMUTATION     TO COMM-GV65-LMESSAGE               
V34CEN              MOVE '6515'             TO COMM-GV65-CODRET                 
V34CEN           ELSE                                                           
                    MOVE GB05-NMUTATION     TO PTMG-ALP (1)                     
                    MOVE   '0015'           TO PTMG-NSEQERR                     
                    PERFORM MLIBERRGEN                                          
V34CEN           END-IF                                                         
               END-IF                                                           
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-UPDATE-RTGB05. EXIT.                                                 
      ***************************                                               
V50R0  UPDATE-RTGB07-PLUS SECTION.                                              
  !   ************************                                                  
  !                                                                             
  !        MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
  !                                                                             
           MOVE PTLIVR-21       TO GB07-FILIERE                                 
           MOVE DLIVRE-21(1)    TO GB07-DMUTATION                               
  !   *    PERFORM CLEF-GB0500.                                                 
  !                                                                             
  !        MOVE DATHEUR TO GB07-DSYST.                                          
  !                                                                             
  !        EXEC SQL UPDATE   RVGB0700                                           
  !                 SET      QMUTEE    = QMUTEE    + :WQTE       ,              
  !                          DSYST     = :GB07-DSYST                            
  !                 WHERE    DMUTATION = :GB07-DMUTATION                        
  !                   AND    FILIERE   = :GB07-FILIERE                          
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-UPDATE-RTGB05-PLUS. EXIT.                                            
V50R0 ***************************                                               
V50R0  UPDATE-RTGB07-MOINS SECTION.                                             
  !   ************************                                                  
  !                                                                             
  !        MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
           MOVE COMM-GV65-FILIERE-IN TO GB07-FILIERE                            
           MOVE COMM-GV65-DMUT       TO GB07-DMUTATION                          
  !   *    PERFORM CLEF-GB0500.                                                 
  !                                                                             
  !        MOVE DATHEUR TO GB07-DSYST.                                          
  !                                                                             
  !        EXEC SQL UPDATE   RVGB0700                                           
  !                 SET      QMUTEE    = QMUTEE    - :WQTE       ,              
  !                          DSYST     = :GB07-DSYST                            
  !                 WHERE    DMUTATION = :GB07-DMUTATION                        
  !                   AND    FILIERE   = :GB07-FILIERE                          
  !                   AND    QMUTEE   >= :WQTE                                  
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-UPDATE-RTGB07-MOINS. EXIT.                                           
V50R0 ***************************                                               
V34R4  MAJ-RTGB15-RTGB05 SECTION.                                               
V34R4 ************************                                                  
V34R4                                                                           
V34R4      COMPUTE WQTE       =  COMM-GV65-QTE * -1                             
V34R4      MOVE    WQTE       TO WQNBPIECES                                     
V34R4                                                                           
V34R4      COMPUTE WQVOLUME = GA00-QHAUTEUR    *                                
V34R4                         GA00-QPROFONDEUR *                                
V34R4                         GA00-QLARGEUR                                     
V34R4                                                                           
V34R4      IF COMM-GV65-QTE = GB15-QDEMANDEE                                    
V34R4         PERFORM DELETE-RTGB15                                             
V34R4         MOVE -1   TO WQNBLIGNES                                           
V34R4      ELSE                                                                 
V34R4         PERFORM UPDATE-RTGB15                                             
V34R4         MOVE 0 TO WQNBLIGNES                                              
V34R4      END-IF                                                               
V34R4                                                                           
V34R4      MOVE GB15-NMUTATION  TO GB05-NMUTATION.                              
V34R4      PERFORM SELECT-RTGB05                                                
V34R4      IF TROUVE                                                            
V34R4         MOVE GB05-NSOCENTR  TO WS-NSOCDEPOT                               
V34R4         MOVE GB05-NDEPOT    TO WS-NDEPOT                                  
V34R4         MOVE GB05-NSOCENTR  TO COMM-GV65-NSOCDEPOT                        
V34R4         MOVE GB05-NDEPOT    TO COMM-GV65-NDEPOT                           
V34R4      END-IF                                                               
V34R4      PERFORM UPDATE-RTGB05.                                               
V34R4      IF SQLCODE = 0                                                       
V34R4         MOVE 'N' TO COMM-GV65-SUP-MUT-GB05                                
test          IF AUTO-DISPLAY                                                   
TEST             STRING 'SUPPRESSION GB05/GB15 ' GB05-NMUTATION                 
TEST             ' SUR CODIC ' COMM-GV65-NCODIC                                 
test             DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
test          END-IF                                                            
V34R4      END-IF                                                               
V34R4      .                                                                    
V34R4  FIN-MAJ-RTGB15-RTGB05. EXIT.                                             
V34R4 ***************************                                               
       SELECT-RTGB15-TEMPORAIRE SECTION.                                        
      ***********************************                                       
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB1500.                                                 
           MOVE COMM-GV65-NCODIC   TO GB15-NCODIC                               
           EXEC SQL SELECT     A.QDEMANDEE                                      
                             , A.QVOLUME                                        
                             , A.QCC                                            
                             , A.QDEMINIT                                       
                             , A.NSOCDEPOT                                      
                             , A.NDEPOT                                         
                             , A.QMUTEE                                         
                             , A.QLIEE                                          
                             , A.QCDEPREL                                       
                             , B.WVAL                                           
                             , B.QNBLLANCE                                      
V34R4                        , B.LHEURLIMIT                                     
                    INTO       :GB15-QDEMANDEE                                  
                             , :GB15-QVOLUME                                    
                             , :GB15-QCC                                        
                             , :GB15-QDEMINIT                                   
                             , :GB15-NSOCDEPOT                                  
                             , :GB15-NDEPOT                                     
                             , :GB15-QMUTEE                                     
                             , :GB15-QLIEE                                      
                             , :GB15-QCDEPREL                                   
                             , :GB05-WVAL                                       
                             , :GB05-QNBLLANCE                                  
V34R4                        , :GB05-LHEURLIMIT                                 
                    FROM     RVGB1502 A                                         
V34CEN              INNER JOIN RVGB0503 B                                       
                    ON B.NMUTATION = A.NMUTATION                                
                    WHERE  A.NCODIC     = :GB15-NCODIC                          
                    AND    A.NMUTATION  = :GB15-NMUTATION                       
                    WITH UR                                                     
           END-EXEC.                                                            
      * -> TOUT CE QU'ON VEUT SAVOIR C'EST SI ON A DES CODES RETOURS            
      *    TYPE -805 (BLOQUANTS) ET LE NON-TROUVE (NON BLOQUANT)                
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-SELECT-GB15-TEMPORAIRE. EXIT.                                        
      **********************************                                        
       UPDATE-RTGB15   SECTION.                                                 
      ***************************                                               
           MOVE FUNC-UPDATE          TO TRACE-SQL-FUNCTION.                     
           PERFORM CLEF-GB1500.                                                 
           MOVE DATHEUR              TO GB15-DSYST                              
           EXEC SQL UPDATE   RVGB1502                                           
                    SET      QDEMANDEE = QDEMANDEE + :WQTE,                     
                             DSYST     = :GB15-DSYST,                           
                             QDEMINIT  = QDEMINIT + :WQTE                       
                    WHERE    NMUTATION = :GB15-NMUTATION                        
                    AND      NCODIC    = :GB15-NCODIC                           
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF AUTO-DISPLAY                                                      
              IF WQTE < 0                                                       
                 MOVE '-' TO WSIGNE                                             
                 MULTIPLY WQTE BY -1 GIVING EDT-QTE                             
              ELSE                                                              
                 MOVE '+' TO WSIGNE                                             
                 MOVE WQTE TO EDT-QTE                                           
              END-IF                                                            
              IF COMM-GV65-NVENTE > '0000000'                                   
                STRING 'UPDATE GB15 ' WSIGNE EDT-QTE ' SUR MUT '                
                GB15-NMUTATION                                                  
                ' VTE ' COMM-GV65-NVENTE                                        
                ' CODIC ' COMM-GV65-NCODIC                                      
                DELIMITED BY SIZE INTO W-MESSAGE                                
                PERFORM ECRITURE-DISPLAY                                        
              else                                                              
                STRING 'UPDATE GB15 ' WSIGNE EDT-QTE ' SUR MUT '                
                GB15-NMUTATION                                                  
                ' ORD ' COMM-GV65-NORDRE                                        
                ' ' COMM-GV65-NSOCIETE                                          
                    COMM-GV65-NLIEU                                             
                ' CODIC ' COMM-GV65-NCODIC                                      
                DELIMITED BY SIZE INTO W-MESSAGE                                
                PERFORM ECRITURE-DISPLAY                                        
              END-IF                                                            
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-UPDATE-RTGB15. EXIT.                                                 
      ***************************                                               
       INSERT-RTGB15 SECTION.                                                   
      ************************                                                  
           MOVE GA14-WSEQFAM             TO GB15-WSEQFAM                        
           MOVE GA00-CMARQ               TO GB15-CMARQ                          
           MOVE WQTE                     TO GB15-QDEMANDEE                      
           MOVE GA00-QPOIDS              TO GB15-QPOIDS                         
           MOVE WQVOLUME                 TO GB15-QVOLUME                        
           MOVE WS-CMODSTOCK             TO GB15-CMODSTOCK                      
           MOVE GB05-NSOCENTR            TO GB15-NSOCDEPOT                      
           MOVE GB05-NDEPOT              TO GB15-NDEPOT                         
           INITIALIZE                       GB15-DRAFALE                        
                                            GB15-NRAFALE                        
                                            GB15-NSATELLITE                     
                                            GB15-NCASE                          
                                            GB15-WGROUPE                        
                                            GB15-QDEMINIT                       
                                            GB15-QLIEE                          
                                            GB15-QCDEPREL                       
                                            GB15-QMUTEE                         
                                            GB15-WORIGINE                       
                                            GB15-QCC                            
           MOVE WQTE                     TO GB15-QDEMINIT                       
           MOVE DATHEUR                  TO GB15-DSYST                          
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB1500.                                                 
           EXEC SQL INSERT                                                      
                    INTO     RVGB1502                                           
                            (NMUTATION    , NCODIC      , WSEQFAM     ,         
                             CMARQ        , QDEMANDEE   , QLIEE       ,         
                             QCDEPREL     , QMUTEE      , QPOIDS      ,         
                             QVOLUME      , CMODSTOCK   , NSOCDEPOT   ,         
                             NDEPOT       , DRAFALE     , NRAFALE     ,         
                             NSATELLITE   , NCASE       , WGROUPE     ,         
                             DSYST        , QDEMINIT    , QCC, WORIGINE)        
                    VALUES (:RVGB1502)                                          
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-INSERT-RTGB15. EXIT.                                                 
      **************************                                                
       DELETE-RTGB15 SECTION.                                                   
      ***********************                                                   
           MOVE FUNC-DELETE           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB1500.                                                 
           EXEC SQL DELETE                                                      
                    FROM     RVGB1502                                           
                    WHERE    NMUTATION = :GB15-NMUTATION                        
                    AND      NCODIC    = :GB15-NCODIC                           
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF OK                                                                
              IF NON-TROUVE THEN                                                
                 MOVE GB15-NMUTATION TO PTMG-ALP (1)                            
                 MOVE GB15-NCODIC    TO PTMG-ALP (2)                            
                 MOVE   '0017'                      TO PTMG-NSEQERR             
                 PERFORM MLIBERRGEN                                             
              ELSE                                                              
                IF AUTO-DISPLAY                                                 
                   STRING 'SUPPRESSION GB15 MUT '                               
                   GB15-NMUTATION                                               
                   ' VTE ' COMM-GV65-NVENTE                                     
                   ' CODIC ' COMM-GV65-NCODIC                                   
                   DELIMITED BY SIZE INTO W-MESSAGE                             
                   PERFORM ECRITURE-DISPLAY                                     
                END-IF                                                          
             END-IF                                                             
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-DELETE-RTGB15. EXIT.                                                 
      **************************                                                
       CREATION-RTGB05-TEMPORAIRE SECTION.                                      
      *************************************                                     
      * -> RECHERCHE DU DERNIER NUMERO DE MUTATION ET + 1                       
      *    JUSQU'A CE QUE L'INSERT AIT FONCTIONNE.                              
           PERFORM SELECT-MAX-NMUTATION                                         
           IF NOT OK                                                            
              GO TO FIN-CRE-GB05-TEMPORAIRE                                     
           END-IF.                                                              
           MOVE COMM-GV65-NSOCDEPOT      TO GB05-NSOCENTR                       
           MOVE COMM-GV65-NDEPOT         TO GB05-NDEPOT                         
           MOVE MDLIV-LDEPLIVR-EXP (1:3) TO GB05-NSOCIETE                       
           MOVE MDLIV-LDEPLIVR-EXP (4:3) TO GB05-NLIEU                          
           MOVE  '20491231'              TO GB05-DDEBSAIS                       
                                            GB05-DFINSAIS                       
                                            GB05-DDESTOCK                       
                                            GB05-DMUTATION                      
           MOVE '00'                     TO GB05-DHEURMUT                       
                                            GB05-DMINUMUT                       
           MOVE  0                       TO GB05-QNBCAMIONS                     
                                            GB05-QNBM3QUOTA                     
                                            GB05-QNBPQUOTA                      
                                            GB05-QVOLUME                        
                                            GB05-QNBPIECES                      
                                            GB05-QNBLIGNES                      
                                            GB05-QNBPLANCE                      
                                            GB05-QNBLLANCE                      
                                            GB05-QNBM3PROPOS                    
                                            GB05-QNBPPROPOS                     
           MOVE 'N'                      TO GB05-WPROPERMIS                     
           MOVE SPACES                   TO GB05-WVAL                           
                                            GB05-DVALID                         
           MOVE 'EXP%EXP%'               TO GB05-LHEURLIMIT                     
           MOVE DATHEUR                  TO GB05-DSYST                          
           MOVE 'TOTAL'                  TO GB05-CSELART                        
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB0500.                                                 
           SET MUT-PAS-OK TO TRUE                                               
           PERFORM UNTIL MUT-OK OR  NOT OK                                      
                   ADD 1 TO 9-NMUTATION                                         
                   MOVE 9-NMUTATION TO GB05-NMUTATION                           
                   EXEC SQL                                                     
                        INSERT INTO RVGB0501                                    
                        VALUES (:RVGB0501)                                      
                   END-EXEC                                                     
                   IF SQLCODE NOT = 0 AND -803                                  
                      MOVE SQLCODE            TO TRACE-SQL-CODE                 
                      MOVE '1'                TO KONTROL                        
                      MOVE TRACE-SQL-FUNCTION TO PTMG-ALP (1)                   
                      MOVE TABLE-NAME         TO PTMG-ALP (2)                   
                      MOVE TRACE-SQL-CODE     TO PTMG-ALP (3)                   
                      MOVE '0027'             TO PTMG-NSEQERR                   
                      PERFORM MLIBERRGEN                                        
                   END-IF                                                       
                   IF SQLCODE = 0                                               
                      SET MUT-OK TO TRUE                                        
test                IF AUTO-DISPLAY                                             
test                   STRING 'crea mut tempo gb05 '                            
test                    ' sur CODIC ' COMM-GV65-NCODIC                          
test                    DELIMITED BY SIZE INTO W-MESSAGE                        
                       PERFORM ECRITURE-DISPLAY                                 
test                END-IF                                                      
                   END-IF                                                       
           END-PERFORM.                                                         
V41R0      IF MUT-OK                                                            
V41R0         PERFORM INSERT-RTGA01                                             
V41R0         PERFORM DELETE-TSTENVOI                                           
V41R0         PERFORM GESTION-TSTENVOI                                          
  !           IF WC-TSTENVOI-ERREUR                                             
                  MOVE all '*' to w-message                                     
                  PERFORM ECRITURE-DISPLAY                                      
                  PERFORM ECRITURE-DISPLAY                                      
  !               MOVE SPACE TO W-MESSAGE                                       
  !               MOVE 'PROBLEME LECTURE TSTENVOI  '                            
  !                                   TO W-MESSAGE                              
  !                                                                             
                  PERFORM ECRITURE-DISPLAY                                      
  !               MOVE all '*' to w-message                                     
                  PERFORM ECRITURE-DISPLAY                                      
                  PERFORM ECRITURE-DISPLAY                                      
  !               MOVE '1' TO KONTROL                                           
  !               MOVE   '0099'               TO PTMG-NSEQERR                   
  !               PERFORM MLIBERRGEN                                            
  !           END-IF                                                            
V41R0      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-CRE-GB05-TEMPORAIRE. EXIT.                                           
      ********************************                                          
V41R0  INSERT-RTGA01  SECTION.                                                  
  !   ***************************                                               
  !                                                                             
  !        MOVE 'XCTRL' TO WCTABLEG1.                                           
  !        MOVE 001 TO W-COMPTEUR                                               
  !        MOVE SPACE TO GA01-WTABLEG                                           
  !        STRING '00000EXPDELC' W-COMPTEUR                                     
  !        DELIMITED BY SIZE INTO WCTABLEG2                                     
  !        STRING 'O' COMM-GV65-NSOCDEPOT COMM-GV65-NDEPOT                      
  !        '    ' GB05-NMUTATION                                                
  !        DELIMITED BY SIZE INTO WWTABLEG                                      
  !        MOVE WCTABLEG1 TO GA01-CTABLEG1                                      
  !        MOVE WCTABLEG2 TO GA01-CTABLEG2                                      
  !        MOVE WWTABLEG TO GA01-WTABLEG                                        
  !                                                                             
  !        MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION.                    
  !        MOVE 'RVGV01ZZ'            TO TABLE-NAME.                            
  !                                                                             
  !        PERFORM UNTIL SQLCODE = 0                                            
  !           EXEC SQL INSERT                                                   
  !                    INTO     RVGA0100                                        
  !                         (CTABLEG1                                           
  !                         ,CTABLEG2                                           
  !                         ,WTABLEG)                                           
  !                    VALUES  (                                                
  !                         :GA01-CTABLEG1                                      
  !                        ,:GA01-CTABLEG2                                      
  !                        ,:GA01-WTABLEG)                                      
  !                    END-EXEC                                                 
  !           IF SQLCODE = -803                                                 
  !              ADD 1 TO W-COMPTEUR                                            
  !              STRING '00000EXPDELC' W-COMPTEUR                               
  !              DELIMITED BY SIZE INTO GA01-CTABLEG2                           
  !           END-IF                                                            
  !        END-PERFORM.                                                         
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-INSERT-RTGA01. EXIT.                                                 
  !   ********************************                                          
  !                                                                             
  !    DELETE-TSTENVOI SECTION.                                                 
  !   ******************************                                            
  !                                                                             
  !        EXEC CICS DELETEQ TS                                                 
  !             QUEUE    (WC-ID-TSTENVOI)                                       
  !             NOHANDLE                                                        
  !        END-EXEC.                                                            
  !                                                                             
  !    FIN-DELETE-TYSTENVOI. EXIT.                                              
V41R0 ********************************                                          
       SELECT-MAX-NMUTATION SECTION.                                            
      ******************************                                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB0500.                                                 
           EXEC SQL SELECT                                                      
                          MAX(NMUTATION)                                        
                    INTO                                                        
                         :GB05-NMUTATION   :GB05-NMUTATION-F                    
                    FROM     RVGB0501                                           
                    WHERE  NMUTATION <  '9000000'                               
                    WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           EXEC SQL SELECT                                                      
                    MAX(NMUTATION)                                              
               INTO                                                             
                   :GB55-NMUTATION   :GB55-NMUTATION-F                          
               FROM     RVGB5500                                                
              WHERE  NMUTATION <  '9000000'                                     
              WITH UR                                                           
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           IF OK                                                                
               IF GB55-NMUTATION > GB05-NMUTATION                               
                  MOVE GB55-NMUTATION     TO GB05-NMUTATION                     
               END-IF                                                           
               IF GB05-NMUTATION =  '8999999'                                   
                  MOVE '0000000'          TO GB05-NMUTATION                     
                  MOVE GB05-NMUTATION     TO 9-NMUTATION                        
               ELSE                                                             
                  MOVE GB05-NMUTATION     TO 9-NMUTATION                        
               END-IF                                                           
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-SELECT-MAX-MUTATION. EXIT.                                           
      ********************************                                          
       DECLARE-CGB05-MUT SECTION.                                               
      ****************************                                              
HV         IF COMM-GV65-MUT-UNIQUE                                              
090306        MOVE 'N' TO WS-WMULTI-MIN WS-WMULTI-MAX                           
090306        MOVE ' ' TO WS-WMULTI-VAL                                         
090306     ELSE                                                                 
090306        MOVE LOW-VALUE  TO WS-WMULTI-MIN                                  
090306        MOVE HIGH-VALUE TO WS-WMULTI-MAX                                  
090306        MOVE 'N' TO WS-WMULTI-VAL                                         
HV         END-IF                                                               
           PERFORM CLEF-GB0500.                                                 
           MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
           EXEC SQL DECLARE  CGB05 CURSOR FOR                                   
                    SELECT   NMUTATION    , DDEBSAIS    , DFINSAIS    ,         
                             DDESTOCK     , DMUTATION   , QNBM3QUOTA  ,         
                             QNBPQUOTA    , QVOLUME     , QNBPIECES   ,         
                             QNBLIGNES    , QNBLLANCE   , LHEURLIMIT            
                    FROM     RVGB0503                                           
                    WHERE    NSOCENTR   = :GB05-NSOCENTR                        
                    AND      NDEPOT     = :GB05-NDEPOT                          
                    AND      NSOCIETE   = :GB05-NSOCIETE                        
                    AND      NLIEU      = :GB05-NLIEU                           
                    AND      CSELART    = :GB05-CSELART                         
                    AND      QNBLLANCE  =  0                                    
      *{Post-translation Correct-Sign-not
      *             AND      WVAL      ^= 'O'                                   
                    AND      WVAL      <> 'O'                                   
      *}
                    AND (DMUTATION >  :WS-HV-DMUTATION-1                        
                    AND  DMUTATION <= :WS-HV-DMUTATION-2)                       
090306              AND WMULTI BETWEEN :WS-WMULTI-MIN AND :WS-WMULTI-MAX        
      *{Post-translation Correct-Sign-not
      *             AND WMULTI ^= :WS-WMULTI-VAL                                
                    AND WMULTI <> :WS-WMULTI-VAL                                
      *}
                    ORDER BY DMUTATION , WPROPERMIS , NMUTATION                 
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
           END-EXEC.                                                            
           MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
           EXEC SQL OPEN   CGB05                                                
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-DECLARE-CGB05-MUT. EXIT.                                             
      *****************************                                             
       FETCH-CGB05-MUT SECTION.                                                 
      **************************                                                
           MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB0500.                                                 
           EXEC SQL FETCH   CGB05                                               
                    INTO   :GB05-NMUTATION      , :GB05-DDEBSAIS      ,         
                           :GB05-DFINSAIS       , :GB05-DDESTOCK      ,         
                           :GB05-DMUTATION      , :GB05-QNBM3QUOTA    ,         
                           :GB05-QNBPQUOTA      , :GB05-QVOLUME       ,         
                           :GB05-QNBPIECES      , :GB05-QNBLIGNES     ,         
                           :GB05-QNBLLANCE      , :GB05-LHEURLIMIT              
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
              SET NON-TROUVE TO TRUE                                            
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-FETCH-CGB05-MUT. EXIT.                                               
      ****************************                                              
       CLOSE-CGB05-MUT SECTION.                                                 
      **************************                                                
           MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-GB0500.                                                 
           EXEC SQL CLOSE   CGB05                                               
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-CLOSE-CGB05-MUT. EXIT.                                               
      ***************************                                               
       TEST-CODE-RETOUR-SQL SECTION.                                            
      *******************************                                           
           SET NORMAL TO TRUE.                                                  
           IF CODE-SELECT OR CODE-FETCH OR CODE-UPDATE OR CODE-DELETE           
              IF SQLCODE = +100                                                 
                 SET NON-TROUVE TO TRUE                                         
              END-IF                                                            
           END-IF                                                               
           IF SQLCODE < 0                                                       
              MOVE SQLCODE            TO TRACE-SQL-CODE                         
              MOVE '1'                TO KONTROL                                
              MOVE TRACE-SQL-FUNCTION TO PTMG-ALP (1)                           
              MOVE TABLE-NAME         TO PTMG-ALP (2)                           
              MOVE TRACE-SQL-CODE     TO PTMG-ALP (3)                           
              MOVE '0027'             TO PTMG-NSEQERR                           
              PERFORM MLIBERRGEN                                                
              IF SQLCODE = -811 or -803                                         
                 CONTINUE                                                       
              ELSE                                                              
      *{ normalize-exec-xx 1.5                                                  
      *          EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                       
      *--                                                                       
                 EXEC CICS ABEND ABCODE ('MAXT')                                
                 END-EXEC                                                       
      *}                                                                        
              END-IF                                                            
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-TEST-CODE-RETOUR-SQL. EXIT.                                          
      *********************************                                         
       WRITE-TS-MFL05 SECTION.                                                  
      *************************                                                 
           MOVE LENGTH OF TS-MFL05-DONNEES  TO LONG-TS                          
           MOVE TS-MFL05-DONNEES      TO Z-INOUT                                
           PERFORM WRITE-TS.                                                    
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-WRITE-TS-MFL05. EXIT.                                                
      ***************************                                               
       LINK-TETDATC SECTION.                                                    
      ************************                                                  
           MOVE COMM-DATC-LONG-COMMAREA TO LONG-COMMAREA-LINK                   
           MOVE Z-COMMAREA-TETDATC TO Z-COMMAREA-LINK                           
           MOVE 'TETDATC' TO NOM-PROG-LINK                                      
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK TO Z-COMMAREA-TETDATC.                          
           IF GFVDAT  NOT = '1'                                                 
              MOVE '1' TO KONTROL                                               
              MOVE GF-MESS-ERR    TO COMM-GV65-LMESSAGE                         
              MOVE '65DA'         TO COMM-GV65-CODRET                           
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-APPEL-MODULE-TETDATC. EXIT.                                          
      *********************************                                         
       MLIBERRGEN SECTION.                                                      
      **********************                                                    
           INSPECT PTMG-ALP(1) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(2) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(3) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(4) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(5) REPLACING ALL LOW-VALUE BY SPACE.                
      *    IF PTMG-ALP(1) = LOW-VALUE                                           
      *       MOVE SPACES TO PTMG-ALP(1)                                        
      *    END-IF                                                               
      *                                                                         
      *    IF PTMG-ALP(2) = LOW-VALUE                                           
      *       MOVE SPACES TO PTMG-ALP(2)                                        
      *    END-IF                                                               
      *                                                                         
      *    IF PTMG-ALP(3) = LOW-VALUE                                           
      *       MOVE SPACES TO PTMG-ALP(3)                                        
      *    END-IF                                                               
      *                                                                         
      *    IF PTMG-ALP(4) = LOW-VALUE                                           
      *       MOVE SPACES TO PTMG-ALP(4)                                        
      *    END-IF                                                               
      *                                                                         
      *    IF PTMG-ALP(5) = LOW-VALUE                                           
      *       MOVE SPACES TO PTMG-ALP(5)                                        
      *    END-IF                                                               
           MOVE SPACES                  TO COMM-GV65-LMESSAGE                   
           MOVE '0000'                  TO COMM-GV65-CODRET                     
           MOVE NOM-PROG                TO PTMG-CNOMPGRM                        
           MOVE COMM-PTMG-LONG-COMMAREA TO LONG-COMMAREA-LINK                   
           MOVE Z-COMMAREA-PTMG         TO Z-COMMAREA-LINK                      
           MOVE 'MPTMG'                 TO NOM-PROG-LINK                        
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK         TO Z-COMMAREA-PTMG                      
           MOVE PTMG-LIBERR(6:59)       TO COMM-GV65-LMESSAGE.                  
           MOVE PTMG-LIBERR(1:04)       TO COMM-GV65-CODRET.                    
           MOVE SPACE TO W-MESSAGE                                              
           MOVE COMM-GV65-LMESSAGE TO  W-MESSAGE                                
           PERFORM ECRITURE-DISPLAY                                             
           INITIALIZE                      PTMG-LIBERR.                         
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-MLIBERRGEN. EXIT.                                                    
      ***********************                                                   
      *                                                                         
      **** TABLE RESERVATION MAGASIN (RTGV22)                                   
       INSERT-RTGV22    SECTION.                                                
      ****************************                                              
      *                                                                         
           MOVE COMM-GV65-NSOCIETE    TO GV11-NSOCIETE                          
           MOVE COMM-GV65-NLIEU       TO GV11-NLIEU                             
           MOVE COMM-GV65-NVENTE      TO GV11-NVENTE                            
           MOVE COMM-GV65-NSEQNQ      TO GV11-NSEQNQ                            
           MOVE COMM-GV65-NCODIC      TO GV11-NCODIC                            
LD0610     MOVE COMM-GV65-WEMPORTE    TO GV11-WEMPORTE                          
v43r0      MOVE SPACE                 TO GV11-WCQERESF                          
v43r0      MOVE COMM-GV65-DDELIV      TO GV11-DDELIV                            
L20410     IF WS-RESER-FOURN-OK                                                 
L20410         MOVE 'F' TO GV11-WCQERESF                                        
L20410         MOVE WS-NSOCDEPOT TO GV11-NSOCLIVR                               
L20410         MOVE WS-NDEPOT    TO GV11-NDEPOT                                 
L20410     END-IF                                                               
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION.                    
LD0310*    MOVE 'RVGV2202'            TO TABLE-NAME.                            
LD0310*    MOVE 'RVGV2203'            TO TABLE-NAME.                            
V34CEN     MOVE 'RVGV2204'            TO TABLE-NAME.                            
V43R0      MOVE  GV11-NSOCLIVR        TO GV22-NSOCLIVR                          
V43R0      MOVE  GV11-NDEPOT          TO GV22-NDEPOT                            
           EXEC SQL INSERT                                                      
LD0310*             INTO     RVGV2202                                           
LD0310*             INTO     RVGV2203                                           
V34CEN              INTO     RVGV2204                                           
                            (NSOCIETE     , NLIEU       , NORDRE      ,         
                             NVENTE       , NCODICGRP   , NCODIC      ,         
                             NSEQ         , NMUTATION   , QVENDUE     ,         
                             QRESERV      , NAUTORM     , WEMPORTE    ,         
                             DSYST        , NSOCLIVR    , NDEPOT      ,         
V34CEN                       DDELIV       , CMODDEL     , CSTATUT ,             
LD0310                       CEQUIPE      , WCQERESF    , DCREATION)            
                    SELECT   NSOCIETE     , NLIEU       , NORDRE      ,         
                             NVENTE       , NCODICGRP   , NCODIC      ,         
                             NSEQ         , NMUTATION   , QVENDUE     ,         
                             QVENDUE      , ' '         , WEMPORTE    ,         
V43R0                  :GV11-DSYST    , :GV22-NSOCLIVR , :GV22-NDEPOT ,         
v43r0 *                :GV11-DSYST    , :GV11-NSOCLIVR , :GV11-NDEPOT ,         
V34CEN*                      DDELIV       , CMODDEL ,    :GV22-CSTATUT,         
V34CEN                 :GV11-DDELIV       , CMODDEL ,    :GV22-CSTATUT,         
LD0310*                :GV11-CEQUIPE  , :GV11-WCQERESF , :GV11-DCREATION        
LD0310                       CEQUIPE  , :GV11-WCQERESF , DCREATION              
                    FROM RVGV1106 WHERE                                         
                    NSOCIETE = :GV11-NSOCIETE                                   
                    AND NLIEU = :GV11-NLIEU AND NVENTE = :GV11-NVENTE           
                    AND NSEQNQ = :GV11-NSEQNQ                                   
           END-EXEC.                                                            
           IF SQLCODE NOT = -803                                                
              PERFORM TEST-CODE-RETOUR-SQL                                      
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-INSERT-RTGV22. EXIT.                                                 
      **************************                                                
       UPDATE-RTGV11  SECTION.                                                  
      **************************                                                
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
           MOVE 'RVGV1106'            TO TABLE-NAME.                            
           MOVE SPACE TO GV11-LCOMMENT                                          
           MOVE COMM-GV65-DDELIV TO GV11-DDELIV                                 
           IF COMM-GV65-DACEM-OUI                                               
              STRING 'DACEM' COMM-GV65-NMUTTEMP                                 
              DELIMITED BY SIZE INTO GV11-LCOMMENT                              
           END-IF                                                               
           IF COMM-GV65-STOCK-DISPONIBLE                                        
              STRING 'DARTY' COMM-GV65-NMUTTEMP                                 
              COMM-GV65-NSOCDEPOT COMM-GV65-NDEPOT                              
              DELIMITED BY SIZE INTO GV11-LCOMMENT                              
           END-IF                                                               
           IF GV11-LCOMMENT(1:5) NOT = 'DARTY'                                  
                           AND GV11-LCOMMENT(1:5) NOT = 'DACEM'                 
              STRING 'DARTY' '       '                                          
              COMM-GV65-NSOCDEPOT COMM-GV65-NDEPOT                              
              DELIMITED BY SIZE INTO GV11-LCOMMENT                              
eric          MOVE COMM-GV65-NSOCDEPOT TO GV11-NSOCLIVR                         
eric          MOVE COMM-GV65-NDEPOT    TO GV11-NDEPOT                           
           END-IF                                                               
           EXEC SQL UPDATE                                                      
                    RVGV1106                                                    
                    SET NSOCLIVR   = :GV11-NSOCLIVR                             
                   ,NDEPOT         = :GV11-NDEPOT                               
L20410             ,NSOCGEST       = :GV11-NSOCGEST                             
L20410             ,NLIEUGEST      = :GV11-NLIEUGEST                            
L20410             ,NSOCDEPLIV     = :GV11-NSOCDEPLIV                           
L20410             ,NLIEUDEPLIV    = :GV11-NLIEUDEPLIV                          
L20410             ,DDELIV         = :GV11-DDELIV                               
L20410             ,WCQERESF       = :GV11-WCQERESF                             
                   ,LCOMMENT       = :GV11-LCOMMENT                             
                    WHERE NSOCIETE = :GV11-NSOCIETE                             
                    AND NLIEU      = :GV11-NLIEU                                
                    and NVENTE     = :GV11-NVENTE                               
                    AND NSEQNQ     = :GV11-NSEQNQ                               
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE            TO TRACE-SQL-CODE                         
              MOVE '1'                TO KONTROL                                
              MOVE TRACE-SQL-FUNCTION TO PTMG-ALP (1)                           
              MOVE TABLE-NAME         TO PTMG-ALP (2)                           
              MOVE TRACE-SQL-CODE     TO PTMG-ALP (3)                           
              MOVE '0027'             TO PTMG-NSEQERR                           
              PERFORM MLIBERRGEN                                                
           END-IF.                                                              
L20410     IF TROUVE AND WS-RESER-FOURN-OK                                      
L20410        PERFORM UPDATE-RVGV2203                                           
L20410     END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-UPDATE-RTGV11. EXIT.                                                 
      ***************************                                               
V43R0  UPDATE-RTGV11-DDELIV  SECTION.                                           
  !   ********************************                                          
  !                                                                             
  !        MOVE COMM-GV65-NSOCIETE    TO GV11-NSOCIETE                          
  !        MOVE COMM-GV65-NLIEU       TO GV11-NLIEU                             
  !        MOVE COMM-GV65-NVENTE      TO GV11-NVENTE                            
  !        MOVE COMM-GV65-NSEQNQ      TO GV11-NSEQNQ                            
  !        MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
  !        MOVE 'RVGV1106'            TO TABLE-NAME.                            
  !        MOVE COMM-GV65-DATE-DISPO  TO GV11-DDELIV                            
  !                                                                             
  !        EXEC SQL UPDATE                                                      
  !                 RVGV1106                                                    
  !                 SET DDELIV     = :GV11-DDELIV                               
  !                   , WCQERESF   = :GV11-WCQERESF                             
  !                 WHERE NSOCIETE = :GV11-NSOCIETE                             
  !                 AND NLIEU      = :GV11-NLIEU                                
  !                 and NVENTE     = :GV11-NVENTE                               
  !                 AND NSEQNQ     = :GV11-NSEQNQ                               
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
  !        IF NON-TROUVE                                                        
  !           MOVE SQLCODE            TO TRACE-SQL-CODE                         
  !           MOVE '1'                TO KONTROL                                
  !           MOVE TRACE-SQL-FUNCTION TO PTMG-ALP (1)                           
  !           MOVE TABLE-NAME         TO PTMG-ALP (2)                           
  !           MOVE TRACE-SQL-CODE     TO PTMG-ALP (3)                           
  !           MOVE '0027'             TO PTMG-NSEQERR                           
  !           PERFORM MLIBERRGEN                                                
  !        END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-UPDATE-RTGV11-DDELIV. EXIT.                                          
v43r0 ***************************                                               
L20410 UPDATE-RVGV2203 SECTION.                                                 
L20410***************************                                               
L20410                                                                          
L20410     MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
L20410     MOVE 'RVGV2203'            TO TABLE-NAME.                            
L20410                                                                          
L20410     EXEC SQL UPDATE   RVGV2203                                           
L20410              SET      WCQERESF   = :GV11-WCQERESF,                       
L20410                       DSYST      = :GV11-DSYST ,                         
                             CEQUIPE    = :GV11-CEQUIPE,                        
                             DCREATION  = :GV11-DCREATION                       
L20410              WHERE NSOCIETE = :GV11-NSOCIETE                             
L20410              AND NLIEU = :GV11-NLIEU AND NVENTE = :GV11-NVENTE           
L20410              AND NSEQ = :GV11-NSEQ                                       
L20410     END-EXEC.                                                            
L20410                                                                          
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
L20410*    .                                                                    
      *}                                                                        
L20410 FIN-UPDATE-RVGV2203. EXIT.                                               
      ****************************                                              
       TEST-VTE-INTERNET SECTION.                                               
      ****************************                                              
           INITIALIZE MEC15C-COMMAREA.                                          
           SET WVTEI-KO  TO TRUE.                                               
           MOVE COMM-GV65-NSOCIETE    TO GV11-NSOCIETE.                         
           MOVE COMM-GV65-NLIEU       TO GV11-NLIEU.                            
           MOVE COMM-GV65-NVENTE      TO GV11-NVENTE                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           MOVE 'RVEC0200'            TO TABLE-NAME.                            
           PERFORM SELECT-RTEC02.                                               
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-TEST-VTE-INTERNET. EXIT.                                             
      ******************************                                            
      ******************************************************************        
      * EST-CE QUE LA COMMANDE EXISTE?                                          
      ******************************************************************        
       SELECT-RTEC02 SECTION.                                                   
      ************************                                                  
              if COMM-GV65-NCDEWC is numeric                                    
              and COMM-GV65-NCDEWC > 0                                          
                 move COMM-GV65-NCDEWC to WEC02-NCDEWC                          
                 SET WVTEI-OK  TO TRUE                                          
              else                                                              
              EXEC SQL SELECT NCDEWC INTO :WEC02-NCDEWC                         
                         FROM RVEC0200                                          
                        WHERE NSOCIETE  = :GV11-NSOCIETE                        
                          AND NLIEU     = :GV11-NLIEU                           
                          AND NVENTE    = :GV11-NVENTE                          
                        WITH UR                                                 
              END-EXEC                                                          
              IF SQLCODE  = 0                                                   
                 SET WVTEI-OK  TO TRUE                                          
              END-IF                                                            
              END-IF                                                            
           .                                                                    
       FIN-SELECT-RTEC02. EXIT.                                                 
      **************************                                                
      ******************************************************************        
      * CREE MOUCHARD LIGNE.                                                    
      ******************************************************************        
       CREER-EC15 SECTION.                                                      
      **********************                                                    
v41r2 *    IF WVTEI-OK AND (COMM-GV65-RESERVATION                               
v41r2      IF WVTEI-OK AND                                                      
v41r2 *               OR COMM-GV65-VALIDATION                                   
      *               OR COMM-GV65-CRE-GB05-TEMPO = 'O'                         
                       ( COMM-GV65-CRE-GB05-TEMPO = 'O'                         
                      OR COMM-GV65-CRE-GB05-FINAL = 'O')                        
              MOVE WEC02-NCDEWC          TO MEC15C-NCDEWC                       
              MOVE COMM-GV65-NSOCIETE    TO MEC15C-NSOCIETE                     
              MOVE COMM-GV65-NLIEU       TO MEC15C-NLIEU                        
              MOVE COMM-GV65-NVENTE      TO MEC15C-NVENTE                       
              EXEC CICS START TRANSID ('EC15') FROM (MEC15C-COMMAREA)           
              END-EXEC                                                          
              INITIALIZE                MEC15C-COMMAREA                         
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-CREER-EC15. EXIT.                                                    
      ***********************                                                   
HV     SELECT-NB-RTGV11 SECTION.                                                
      ***************************                                               
090306                                                                          
090306     MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
090306     INITIALIZE                     WS-NB-PRODUIT                         
           MOVE COMM-GV65-NSOCIETE    TO GV11-NSOCIETE                          
           MOVE COMM-GV65-NLIEU       TO GV11-NLIEU                             
           MOVE COMM-GV65-NVENTE      TO GV11-NVENTE                            
090306                                                                          
090306     EXEC SQL                                                             
090306          SELECT SUM(A.QVENDUE)                                           
090306          INTO    :WS-NB-PRODUIT :WS-NB-PRODUIT-F                         
090306            FROM RVGV1106 A                                               
090306           WHERE A.NSOCIETE   = :GV11-NSOCIETE                            
090306             AND A.NLIEU      = :GV11-NLIEU                               
090306             AND A.NVENTE     = :GV11-NVENTE                              
090306             AND A.CTYPENREG  = '1'                                       
090306             AND A.CENREG     = '     '                                   
090306             AND A.CMODDEL    = 'EMR'                                     
V41R0              AND A.WEMPORTE IN (:WS-TYPENVOI)                             
090306             AND A.WTOPELIVRE = 'N'                                       
                   WITH UR                                                      
090306     END-EXEC                                                             
090306                                                                          
090306     PERFORM TEST-CODE-RETOUR-SQL                                         
HV         IF WS-NB-PRODUIT = 1                                                 
090306        SET COMM-GV65-MUT-UNIQUE TO TRUE                                  
090306     ELSE                                                                 
090306        SET COMM-GV65-MUT-MULTI  TO TRUE                                  
090306     END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
090306 FIN-SELECT-NB. EXIT.                                                     
      **********************                                                    
V43R0  DECLARE-RTGV11 SECTION.                                                  
  !   ***************************                                               
  !                                                                             
  !        MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION                     
  !                                                                             
  !        INITIALIZE                     WS-NB-PRODUIT                         
  !                                                                             
  !        MOVE COMM-GV65-NSOCIETE    TO GV11-NSOCIETE                          
  !        MOVE COMM-GV65-NLIEU       TO GV11-NLIEU                             
  !        MOVE COMM-GV65-NVENTE      TO GV11-NVENTE                            
  !                                                                             
  !        EXEC SQL DECLARE  GV11 CURSOR FOR                                    
  !             SELECT A.NVENTE ,                                               
  !                    CASE                                                     
  !                    WHEN B.WPRECO IS NULL                                    
  !                        THEN ' '                                             
  !                    ELSE                                                     
  !                             B.WPRECO                                        
  !                    END                                                      
  !               FROM RVGV1106 A                                               
  !             LEFT OUTER JOIN RVGV9901 B                                      
  !             ON  B.NSOCIETE    = A.NSOCIETE                                  
  !             AND B.NLIEU       = A.NLIEU                                     
  !             AND B.NVENTE      = A.NVENTE                                    
  !             AND B.CTYPENREG   = A.CTYPENREG                                 
  !             AND B.NCODIC      = A.NCODIC                                    
  !             AND B.NSEQ        = A.NSEQ                                      
  !              WHERE A.NSOCIETE   = :GV11-NSOCIETE                            
  !                AND A.NLIEU      = :GV11-NLIEU                               
  !                AND A.NVENTE     = :GV11-NVENTE                              
  !                AND A.CTYPENREG  = '1'                                       
  !                AND A.CENREG     = '     '                                   
  !                AND A.CMODDEL    = 'EMR'                                     
  !                AND A.WEMPORTE IN (:WS-TYPENVOI)                             
  !                AND A.WTOPELIVRE = 'N'                                       
                   FOR FETCH ONLY                                               
                   WITH UR                                                      
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
  !                                                                             
  !        MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
  !        MOVE 'RTGV11'              TO TABLE-NAME.                            
  !                                                                             
  !        EXEC SQL OPEN   GV11                                                 
  !        END-EXEC.                                                            
  !                                                                             
  !        PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-DECLARE-NB. EXIT.                                                    
  !   **********************                                                    
  !                                                                             
  !    FETCH-RTGV11 SECTION.                                            02510008
  !   ***********************                                           02520008
  !                                                                     02530008
  !        MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.            02540008
  !        MOVE 'RTGV11'              TO TABLE-NAME.                    02550008
  !        EXEC SQL FETCH   GV11                                        02560008
  !            INTO    :GV11-NVENTE                                             
  !                   ,:GV99-WPRECO                                             
  !        END-EXEC.                                                    02580008
  !                                                                     02590008
  !        PERFORM TEST-CODE-RETOUR-SQL.                                02600008
  !                                                                     02610008
  !    FIN-FETCH-RTGV11. EXIT.                                          02620008
  !   *************************                                         02630008
  !                                                                     02640008
  !    CLOSE-RTGV11 SECTION.                                            02650008
  !   **********************                                            02660008
  !                                                                     02670008
  !        MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION             02680008
  !        MOVE 'RTGV11'              TO TABLE-NAME.                    02690008
  !        EXEC SQL CLOSE   GV11                                        02700008
  !        END-EXEC                                                     02710008
  !        PERFORM TEST-CODE-RETOUR-SQL.                                02720008
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                            02730008
      *}                                                                        
  !    FIN-CLOSE-RTGV11. EXIT.                                          02740008
V43R0 ************************                                          02750008
                                                                        02900008
L20410 SELECT-GV11 SECTION.                                                     
L20410***********************                                                   
L20410                                                                          
L20410     MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
L20410                                                                          
L20410     INITIALIZE                     WS-NB-PRODUIT                         
L20410                                                                          
L20410     MOVE COMM-GV65-NSOCIETE    TO GV11-NSOCIETE                          
L20410     MOVE COMM-GV65-NLIEU       TO GV11-NLIEU                             
L20410     MOVE COMM-GV65-NVENTE      TO GV11-NVENTE                            
L20410     MOVE COMM-GV65-NSEQNQ      TO GV11-NSEQNQ                            
L20410                                                                          
L20410     EXEC SQL                                                             
L20410          SELECT NSOCLIVR, NDEPOT, DDELIV, NCODIC, WCQERESF               
                    , LCOMMENT                                                  
L20410          INTO    :GV11-NSOCLIVR, :GV11-NDEPOT, :GV11-DDELIV,             
L20410                  :GV11-NCODIC, :GV11-WCQERESF, :GV11-LCOMMENT            
L20410            FROM RVGV1106                                                 
L20410           WHERE NSOCIETE   = :GV11-NSOCIETE                              
L20410             AND NLIEU      = :GV11-NLIEU                                 
L20410             AND NVENTE     = :GV11-NVENTE                                
L20410             AND NSEQNQ     = :GV11-NSEQNQ                                
                   WITH UR                                                      
L20410     END-EXEC.                                                            
L20410                                                                          
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
L20410*    .                                                                    
      *}                                                                        
L20410 FIN-SELECT-GV11. EXIT.                                                   
      ************************                                                  
L20410**** DERESERVATION CDE FNR SUR DATE DECROISSANTE                          
L20410 DECLARE-CGF55D   SECTION.                                                
L20410*****************************                                             
L20410                                                                          
L20410     MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
L20410     PERFORM CLEF-GF5500.                                                 
L20410     EXEC SQL DECLARE  CGF55D  CURSOR FOR                                 
L20410              SELECT   DCDE,                                              
L20410                       QCDE,                                              
L20410                       QCDERES                                            
v43r0               FROM     RVGF5501                                           
l20410              WHERE    NSOCDEPOT = :GF55-NSOCDEPOT                        
L20410              AND      NDEPOT = :GF55-NDEPOT                              
L20410              AND      NCODIC = :GF55-NCODIC                              
      *{Post-translation Correct-Sign-not
      *             AND      DCDE  ^> :GF55-DCDE                                
L20410              AND      DCDE  <= :GF55-DCDE                                
      *}
L20410              ORDER BY DCDE DESC                                          
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
L20410     MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
L20410     EXEC SQL OPEN   CGF55D                                               
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
L20410 FIN-DECLARE-CGF55D. EXIT.                                                
      ***************************                                               
L20410 FETCH-CGF55D  SECTION.                                                   
L20410************************                                                  
L20410                                                                          
L20410     MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
L20410     EXEC SQL FETCH    CGF55D                                             
L20410              INTO    :GF55-DCDE,                                         
L20410                      :GF55-QCDE,                                         
L20410                      :GF55-QCDERES                                       
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
L20410 FIN-FETCH-CGF55D. EXIT.                                                  
      **************************                                                
L20410 CLOSE-CGF55D  SECTION.                                                   
L20410************************                                                  
L20410                                                                          
L20410     MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION.                    
L20410     EXEC SQL CLOSE   CGF55D                                              
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
L20410 FIN-CLOSE-CGF55D. EXIT.                                                  
      ************************                                                  
L20410**** DERESERVATION CDE FNR SUR DATE CROISSANTE                            
L20410 DECLARE-CGF55C   SECTION.                                                
L20410*****************************                                             
L20410                                                                          
L20410     MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
L20410     PERFORM CLEF-GF5500.                                                 
L20410     EXEC SQL DECLARE  CGF55C  CURSOR FOR                                 
L20410              SELECT   DCDE,                                              
L20410                       QCDE,                                              
L20410                       QCDERES                                            
v43r0               FROM     RVGF5501                                           
L20410              WHERE    NSOCDEPOT = :GF55-NSOCDEPOT                        
L20410              AND      NDEPOT = :GF55-NDEPOT                              
L20410              AND      NCODIC = :GF55-NCODIC                              
L20410              AND      DCDE   > :GF55-DCDE                                
L20410              ORDER BY DCDE                                               
                    FOR FETCH ONLY                                              
                    WITH UR                                                     
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
L20410     MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
L20410     EXEC SQL OPEN   CGF55C                                               
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
L20410 FIN-DECLARE-CGF55C. EXIT.                                                
      **************************                                                
L20410 FETCH-CGF55C    SECTION.                                                 
L20410***************************                                               
L20410                                                                          
L20410     MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
L20410     EXEC SQL FETCH    CGF55C                                             
L20410              INTO    :GF55-DCDE,                                         
L20410                      :GF55-QCDE,                                         
L20410                      :GF55-QCDERES                                       
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
L20410 FIN-FETCH-CGF55C. EXIT.                                                  
      *************************                                                 
L20410 CLOSE-CGF55C  SECTION.                                                   
L20410*************************                                                 
L20410                                                                          
L20410     MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION.                    
L20410     EXEC SQL CLOSE    CGF55C                                             
L20410     END-EXEC.                                                            
L20410     PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
L20410 FIN-CLOSE-CGF55C. EXIT.                                                  
      ************************                                                  
LD0709 INSERTION-LOG SECTION.                                                   
      *************************                                                 
LD0709     MOVE FUNC-INSERT TO TRACE-SQL-FUNCTION                               
LD0709     EXEC SQL INSERT  INTO    RVEC0500                                    
LD0709                       ( TIMESTP       ,                                  
LD0709                         WRECENV        ,                                 
LD0709                         XMLDATA        )                                 
LD0709                VALUES (CURRENT TIMESTAMP ,                               
LD0709                        'E'              ,                                
LD0709                        :WS-MESS-MGV65 )                                  
LD0709     END-EXEC.                                                            
LD0709                                                                          
LD0709     PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
LD0709*    .                                                                    
      *}                                                                        
LD0709 FIN-INSERTION-LOG .  EXIT.                                               
      ****************************                                              
V41R0  ACCES-TSTENVOI SECTION.                                                  
  !   *******************************                                           
  !                                                                             
  !   * V�RIFICATION EXISTENCE DE LA TS ET CR�ATION LE CAS �CH�ANT              
  !        PERFORM GESTION-TSTENVOI                                             
  !        IF WC-TSTENVOI-ERREUR                                                
  !            MOVE all '*' to w-message                                        
               PERFORM ECRITURE-DISPLAY                                         
               PERFORM ECRITURE-DISPLAY                                         
  !            MOVE SPACE TO W-MESSAGE                                          
  !            MOVE 'PROBLEME LECTURE TSTENVOI  '                               
  !                                TO W-MESSAGE                                 
  !                                                                             
               PERFORM ECRITURE-DISPLAY                                         
  !            MOVE all '*' to w-message                                        
               PERFORM ECRITURE-DISPLAY                                         
               PERFORM ECRITURE-DISPLAY                                         
  !            MOVE SPACE TO W-MESSAGE                                          
  !            MOVE '1' TO KONTROL                                              
  !            MOVE   '0099'               TO PTMG-NSEQERR                      
  !            PERFORM MLIBERRGEN                                               
  !        END-IF.                                                              
  !                                                                             
  !   * RECHERCHE DES TYPES D'ENVOI � TRAITER                                   
  !        MOVE  LOW-VALUES TO   WS-TYPENVOI                                    
  !        MOVE  '0' TO WF-TSTENVOI                                             
  !        PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
  !          UNTIL WC-TSTENVOI-FIN                                              
  !                                                                             
  !           PERFORM LECTURE-TSTENVOI                                          
  !           IF  WC-TSTENVOI-SUITE                                             
  !           AND TSTENVOI-NSOCZP    = '00000'                                  
  !           AND TSTENVOI-CTRL1        = 'A'                                   
  !              ADD 1 TO IND-RANG-TSTENVOI                                     
  !              MOVE TSTENVOI-CTENVOI     TO                                   
  !                  WS-TAB-TENVOI (IND-RANG-TSTENVOI)                          
  !           END-IF                                                            
  !        END-PERFORM.                                                         
  !                                                                             
  !                                                                             
  !   * RECHERCHE Du type de la vente (internet, b2b ou magasin)                
  !        MOVE  0          TO   W-TYPE-VENTE                                   
  !        MOVE  '0' TO WF-TSTENVOI                                             
  !        PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
  !          UNTIL  WC-TSTENVOI-FIN                                             
  !                                                                             
  !           PERFORM LECTURE-TSTENVOI                                          
  !           IF  WC-TSTENVOI-SUITE                                             
  !           AND TSTENVOI-NSOCZP    = '00000'                                  
  !           AND TSTENVOI-CTRL1        = 'D'                                   
  !           AND TSTENVOI-SOCIETE     = COMM-GV65-NSOCIETE                     
  !           AND TSTENVOI-LIEU        = COMM-GV65-NLIEU                        
  !              MOVE TSTENVOI-TYPE TO W-TYPE-VENTEX                            
  !              IF TSTENVOI-TYPE = '2'                                         
  !                MOVE TSTENVOI-NUMCLI-DACEM  TO WS-B2B-NUM-DACEM              
  !                MOVE TSTENVOI-B2B-MUT-MAG   TO WS-B2B-MUT-MAG                
  !                MOVE TSTENVOI-LOG-DACEM-B2B TO WS-LOG-UP                     
  !                MOVE TSTENVOI-B2B-DACEM     TO WS-B2B-DACEM                  
  !              END-IF                                                         
  !              SET WC-TSTENVOI-FIN TO TRUE                                    
  !           END-IF                                                            
  !        END-PERFORM.                                                         
  !                                                                             
  !   * on a pas trouver le parametrage, donc c'est un magasin                  
  !        IF W-TYPE-VENTE = '0'                                                
  !           MOVE '3'  TO W-TYPE-VENTE                                         
  !           SET VENTE-MAGASIN TO TRUE                                         
  !        END-IF.                                                              
  !        IF W-TYPE-VENTE = '2'                                                
  !           SET VENTE-B2B     TO TRUE                                         
  !        END-IF.                                                              
  !        IF W-TYPE-VENTE = '1'                                                
  !           SET VENTE-INTERNET     TO TRUE                                    
  !        END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-ACCES-TSTENVOI. EXIT.                                                
V41R0 ****************************                                              
V41R0  ACCES-AUTORISATION SECTION.                                              
  !   *******************************                                           
  !                                                                             
  !         SET PARAM-NON-TROUVE    TO TRUE                                     
  !   * CONTROLE COMPATIBILITE ENTRE TYPE DE VENTE ET TYPE D'EXPEDIE            
  !   * recherche des nsoczp differents de zero(exeptions)                      
  !        MOVE  '0' TO WF-TSTENVOI                                             
  !        PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
  !          UNTIL  WP-RANG-TSTENVOI  > 999  OR WC-TSTENVOI-FIN                 
  !                                                                             
  !           PERFORM LECTURE-TSTENVOI                                          
  !           IF  WC-TSTENVOI-SUITE                                             
  !           AND TSTENVOI-NSOCZP(1:3)  = COMM-GV65-NSOCIETE                    
  !           AND TSTENVOI-CTRL1        = 'A'                                   
  !           AND COMM-GV65-WEMPORTE = TSTENVOI-CTENVOI                         
  !              SET PARAM-TROUVE    TO TRUE                                    
  !              SET WC-TSTENVOI-FIN TO TRUE                                    
  !           END-IF                                                            
  !        END-PERFORM.                                                         
  !                                                                             
  !   * recherche du parametrage standard si on a pas trouv� d'exception        
  !        IF PARAM-NON-TROUVE                                                  
  !          MOVE  '0' TO WF-TSTENVOI                                           
  !          PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                      
  !          UNTIL WC-TSTENVOI-FIN                                              
  !                                                                             
  !             PERFORM LECTURE-TSTENVOI                                        
  !             IF  WC-TSTENVOI-SUITE                                           
  !             AND TSTENVOI-NSOCZP       = '00000'                             
  !             AND TSTENVOI-CTRL1        = 'A'                                 
  !             AND COMM-GV65-WEMPORTE = TSTENVOI-CTENVOI                       
  !                SET WC-TSTENVOI-FIN TO TRUE                                  
  !             END-IF                                                          
  !          END-PERFORM                                                        
  !        END-IF.                                                              
  !                                                                             
  !        IF TSTENVOI-AUTO(W-TYPE-VENTE) = 'N'                                 
V34R4        IF COMM-GV65-CRE-GB05-TEMPO = 'O'                                  
  !            MOVE 'mode de d�livrance interdit pour ce magasin'               
  !                          TO COMM-GV65-LMESSAGE                              
  !            MOVE '6533'             TO COMM-GV65-CODRET                      
  !          ELSE                                                               
  !            MOVE '2'            TO KONTROL                                   
  !            MOVE '0033'         TO PTMG-NSEQERR                              
  !            PERFORM MLIBERRGEN                                               
  !          END-IF                                                             
  !        END-IF.                                                              
  !                                                                             
  !   * CONTROLE SI CODIC AUTORISE POUR CE TYPE D'EXPEDIE                       
  !        IF OK                                                                
v43r1        IF NOT VENTE-INTERNET                                              
  !            PERFORM SELECT-RTGA64                                            
  !            IF NON-TROUVE                                                    
V50R0            IF COMM-GV65-CTRL-MGV64                                        
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *          IF COMM-GV65-CONTROLE                                          
                    string 'mode de delivrance ' GA64-CMODDEL                   
                    ' interdit pour ce codic '                                  
                    delimited by size                                           
                    into COMM-GV65-LMESSAGE                                     
  !   *             MOVE 'mode de d�livrance interdit pour ce codic'            
  !   *                      TO COMM-GV65-LMESSAGE                              
  !                 MOVE '6534'             TO COMM-GV65-CODRET                 
  !              ELSE                                                           
  !                 MOVE '2'            TO KONTROL                              
  !                 MOVE '0034'         TO PTMG-NSEQERR                         
  !                 PERFORM MLIBERRGEN                                          
  !              END-IF                                                         
  !            END-IF                                                           
v43r1        END-IF                                                             
  !        END-IF.                                                              
  !        IF OK                                                                
  !          IF COMM-GV65-INTERNET NOT = 'O'                                    
V43R0          IF W-TOP-CONTROLE-EC03 = 'O'                                     
V43R0            IF COMM-GV65-WEMPORTE = 'K'                                    
  !                PERFORM SELECT-RTEC03                                        
  !                IF SQLCODE NOT = 0                                           
V50R0                IF COMM-GV65-CTRL-MGV64                                    
V50R0 *            OR COMM-GV65-CTRL-SPE                                        
V50R0 *              IF COMM-GV65-CONTROLE                                      
  !                MOVE 'MODE DE D�LIVRANCE INTERDIT POUR CE CODIC'             
  !                       TO COMM-GV65-LMESSAGE                                 
  !                    MOVE '6534'             TO COMM-GV65-CODRET              
  !                  ELSE                                                       
  !                    MOVE '2'            TO KONTROL                           
  !                    MOVE '0034'         TO PTMG-NSEQERR                      
  !                    PERFORM MLIBERRGEN                                       
  !                  END-IF                                                     
  !                END-IF                                                       
V43R0            END-IF                                                         
V43R0          END-IF                                                           
V43R0        END-IF                                                             
V43R0      END-IF.                                                              
      *{ Tr-Empty-Sentence 1.6                                                  
  !   *    .                                                                    
      *}                                                                        
  !    FIN-ACCES-AUTORISATION. EXIT.                                            
V41R0 ****************************                                              
V43R0  RECHERCHE-PARAMETRES  SECTION.                                           
   !                                                                            
   !       INITIALIZE RVGA01ZZ                                                  
   !       MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.                 
   !                                                                            
   !       EXEC SQL                                                             
   !            SELECT WPARAM, LIBELLE  , WFLAG                                 
   !            INTO  :XCTRL-WPARAM , :XCTRL-LIBELLE  , :XCTRL-WFLAG            
   !            FROM   RVGA01ZZ                                                 
   !            WHERE  SOCZP = 'NECEN'                                          
   !            AND    TRANS = 'MGV65 '                                         
   !            WITH UR                                                         
   !       END-EXEC.                                                            
   !                                                                            
   !       PERFORM TEST-CODE-RETOUR-SQL.                                03644008
   !       IF TROUVE                                                            
   !          MOVE XCTRL-WPARAM(1:2)   TO WS-DELAI                              
FTFTFT        MOVE XCTRL-WPARAM(3:2)   TO WS-DELTA-DDELIV                       
V50R0         MOVE XCTRL-LIBELLE(1:1)  TO WS-FORCAGE-DDELIV                     
V50R0         MOVE XCTRL-LIBELLE(2:1)  TO W-NB-JOUR-O                           
V50R0         MOVE XCTRL-LIBELLE(3:1)  TO W-NB-JOUR-J                           
V50R0         MOVE XCTRL-LIBELLE(5:1) TO W-MODIF-GENERIX                        
V50R0         MOVE XCTRL-LIBELLE(7:1) TO W-EC05                                 
              MOVE XCTRL-WPARAM (9:1)  TO W-OLD-NEW                             
              MOVE XCTRL-WPARAM (8:1)  TO W-APPEL-DACEM                         
              MOVE XCTRL-WPARAM (10:1) TO W-TOP-CONTROLE-EC03                   
   !       ELSE                                                                 
              MOVE 'N'                    TO W-OLD-NEW                          
              MOVE 'O'                    TO W-APPEL-DACEM                      
              MOVE 'O'                  TO  W-TOP-CONTROLE-EC03                 
   !          MOVE  0                  TO  WS-DELAI                             
   !          MOVE  'O'                TO  W-EC05                               
   !       END-IF.                                                              
   !                                                                            
      * recherche date d'activation de gf55-dcalcul                             
   !  *    INITIALIZE RVGA01ZZ                                                  
   !  *    MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.                 
   !                                                                            
   !  *    EXEC SQL                                                             
   !  *         SELECT WPARAM                                                   
   !  *         INTO  :XCTRL-WPARAM                                             
   !  *         FROM   RVGA01ZZ                                                 
   !  *         WHERE  SOCZP = '00000'                                          
   !  *         AND    TRANS = 'GF55  '                                         
   !  *         WITH UR                                                         
   !  *    END-EXEC.                                                            
   !                                                                            
   !  *    PERFORM TEST-CODE-RETOUR-SQL.                                03644008
   !  *    IF TROUVE                                                            
   !  *       MOVE  XCTRL-WPARAM(1:8)  TO  W-DATE-ACTIVATION-GF55               
   !  *       MOVE  XCTRL-WPARAM(10:1)  TO  W-TOP-CONTROLE-EC03                 
   !  *    ELSE                                                                 
   !  *       MOVE  '29991231'         TO  W-DATE-ACTIVATION-GF55               
   !  *    END-IF.                                                              
   !                                                                            
      *    INITIALIZE RVGA01ZZ                                                  
      *               RVGA0100                                                  
      *    MOVE 'XCTRL'             TO GA01-CTABLEG1.                           
      *    MOVE 'NECENMGV64 0001'   TO GA01-CTABLEG2.                           
      *                                                                         
      *    EXEC SQL SELECT WTABLEG                                              
      *             INTO :GA01-WTABLEG                                          
      *               FROM RVGA0100                                             
      *              WHERE CTABLEG1 =:GA01-CTABLEG1                             
      *                AND CTABLEG2 =:GA01-CTABLEG2                             
      *    END-EXEC.                                                            
      *                                                                         
      *    PERFORM TEST-CODE-RETOUR-SQL.                                        
      *    IF TROUVE                                                            
      *       MOVE GA01-WTABLEG(9:1)      TO W-OLD-NEW                          
      *       MOVE GA01-WTABLEG(8:1)      TO W-APPEL-DACEM                      
      *       MOVE GA01-WTABLEG(10:1)     TO  W-TOP-CONTROLE-EC03               
      *    ELSE                                                                 
      *       MOVE 'N'                    TO W-OLD-NEW                          
      *       MOVE 'O'                    TO W-APPEL-DACEM                      
      *       MOVE 'O'                  TO  W-TOP-CONTROLE-EC03                 
      *    END-IF.                                                              
V50R0 *    MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION             03610008
  !   *    MOVE 'NCGFC '              TO TABLE-NAME                             
  !                                                                     03800050
  !   *    MOVE 'NCGFC' TO GA01-CTABLEG1                                        
  !   *    MOVE 'MGV64' TO GA01-CTABLEG2                                        
  !                                                                             
  !   *    EXEC SQL SELECT   WTABLEG                                            
  !   *             INTO    :GA01-WTABLEG                                       
  !   *             FROM     RVGA0100                                           
  !   *             WHERE    CTABLEG1 = :GA01-CTABLEG1                          
  !   *             AND      CTABLEG2 = :GA01-CTABLEG2                          
      *             WITH UR                                                     
  !   *    END-EXEC                                                             
  !                                                                             
  !   *    PERFORM TEST-CODE-RETOUR-SQL.                                03644008
  !   * param�trage pour bloquer l'envoi des requetes chez Dacem                
V50R0 *    MOVE GA01-WTABLEG(13:1) TO W-APPEL-DACEM.                            
V52R0 *    MOVE GA01-WTABLEG(14:1) TO W-OLD-NEW.                                
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
V43R0  FIN-REC-PARAMETRES. EXIT.                                                
      ***************************                                               
      *                                                                         
V34CEN CALCUL-DELAI  SECTION.                                                   
   !  **********************                                                    
   !                                                                            
  !        IF COMM-GV65-FILIERE-IN NOT = SPACE                                  
              AND COMM-GV65-FILIERE-IN NUMERIC                                  
  !           IF AUTO-DISPLAY                                                   
  !              STRING 'calcul date ' COMM-GV65-FILIERE-IN                     
  !              ' ' COMM-GV65-DMUT                                             
  !              DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
  !           END-IF                                                            
  !           MOVE COMM-GV65-DMUT TO  W-GV65-DDELIV                             
           ELSE                                                                 
              IF AUTO-DISPLAY                                                   
                 STRING 'calcul delai ' COMM-GV65-FILIERE-IN                    
                 ' ' COMM-GV65-DMUT                                             
                 DELIMITED BY SIZE INTO W-MESSAGE                               
                 PERFORM ECRITURE-DISPLAY                                       
              END-IF                                                            
              MOVE COMM-GV65-DDELIV   TO W-GV65-DDELIV                          
           END-IF.                                                              
   !  * CALCUL DATE + DELAI                                                     
   !  *    MOVE '5'                   TO GFDATA                                 
   !  *    PERFORM LINK-TETDATC                                                 
   !  *    COMPUTE GFQNT0 = GFQNT0 + WS-DELAI                                   
   !  *    MOVE '3'                   TO GFDATA                                 
   !  *    PERFORM LINK-TETDATC                                                 
   !  *    MOVE GFSAMJ-0              TO W-GV65-DDELIV.                         
      *                                                                         
      *{ Tr-Empty-Sentence 1.6                                                  
   !  *    .                                                                    
      *}                                                                        
V34CEN FIN-CALCUL-DELAI .  EXIT.                                                
       DATE-CF-CALEE SECTION.                                                   
           IF W-J-SEM  = 1 OR 7                                                 
              initialize MEC15C-COMMAREA                                        
              MOVE '0026'             TO MEC15C-NSEQERR                         
              MOVE 'E'                TO MEC15C-STAT                            
              SET  MEC15E-KO          TO TRUE                                   
              PERFORM CREE-MOUCHARD-LIGNE                                       
           END-IF.                                                              
       F-DATE-CF-CALEE. EXIT.                                                   
      * -> log ec10                                                             
       CREE-MOUCHARD-LIGNE SECTION.                                             
           MOVE WEC02-NCDEWC       TO MEC15C-NCDEWC.                            
           MOVE COMM-GV65-NSOCIETE TO MEC15C-NSOCIETE.                          
           MOVE COMM-GV65-NLIEU    TO MEC15C-NLIEU.                             
           MOVE COMM-GV65-NVENTE   TO MEC15C-NVENTE.                            
           MOVE COMM-GV65-NSEQNQ   TO MEC15C-NSEQNQ.                            
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *       START TRANSID ('EC15') FROM (MEC15E-ENTETE)                       
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS START TRANSID ('EC15') FROM (MEC15E-ENTETE)                
           END-EXEC.                                                            
      *}                                                                        
       F-CREE-MOUCHARD-LIGNE. EXIT.                                             
      ***************************                                               
V41R0 * COPY CSTENVOI                                                           
  !   * -------------------------------------------------------------- *        
  !   * CETTE COPIE GERE TOUT CE QUI TOUCHE LA GESTION DE LA TS        *        
  !   * TSTENVOI: ** CREATION SI CETTE DERNERE N'EXISTE PAS            *        
  !   *           ** LECTURE DE CETTE TS                               *        
  !   * LES ZONES DE TRAVAIL SONT DECLAREES DANS WKTENVOI              *        
  !   *                                                                *        
  !   * CREE LE 01/03/2011 PAR DE02003 (CL)                            *        
  !   *                                                                *        
  !   * -------------------------------------------------------------- *        
  !    GESTION-TSTENVOI  SECTION.                                               
  !        MOVE 1                            TO WP-RANG-TSTENVOI                
  !        PERFORM LECTURE-TSTENVOI                                             
  !        IF WC-TSTENVOI-FIN                                                   
V43R0         PERFORM CREATION-TSTENVOI                                         
  !        ELSE                                                                 
  !   * ON CHERCHE LE DERNIER ENREG DE LA TSTENVOI                              
  !           PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                      
  !           UNTIL WC-TSTENVOI-FIN                                             
  !              PERFORM LECTURE-TSTENVOI                                       
  !           END-PERFORM                                                       
  !           IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '                        
  !              PERFORM DELETE-TSTENVOI                                        
  !              PERFORM CREATION-TSTENVOI                                      
V43R0         END-IF.                                                           
       FIN-GEST-TSTENVOI.  EXIT.                                                
V43R0  CREATION-TSTENVOI SECTION.                                               
  !                                                                             
  !           MOVE '00000'                   TO TSTENVOI-NSOCZP                 
  !           MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
  !           MOVE 'MGV65'                   TO TSTENVOI-WPARAM                 
  !           MOVE 'NOM PROGRAMME'           TO TSTENVOI-LIBELLE                
  !           PERFORM ECRITURE-TSTENVOI                                         
  !           SET WC-XCTRL-EXPDEL-NON-TROUVE TO TRUE                            
  !           INITIALIZE WZ-XCTRL-EXPDEL-SQLCODE                                
  !           PERFORM DECLARE-OPEN-CXCTRL                                       
  !           IF WC-XCTRL-EXPDEL-PB-DB2                                         
  !              SET WC-TSTENVOI-ERREUR TO TRUE                                 
  !              GO TO FIN-CREATION-TSTENVOI                                    
  !           END-IF                                                            
  !           PERFORM FETCH-CXCTRL                                              
  !           IF WC-XCTRL-EXPDEL-PB-DB2                                         
  !              SET WC-TSTENVOI-ERREUR TO TRUE                                 
  !              GO TO FIN-CREATION-TSTENVOI                                    
  !           END-IF                                                            
  !           PERFORM UNTIL WC-XCTRL-EXPDEL-FIN                                 
  !              IF  XCTRL-WPARAM   > SPACES                                    
  !              AND XCTRL-LIBELLE  > SPACES                                    
  !                 MOVE XCTRL-SOCZP         TO TSTENVOI-NSOCZP                 
  !                 MOVE XCTRL-CTRL          TO TSTENVOI-CTRL                   
  !                 MOVE XCTRL-WPARAM        TO TSTENVOI-WPARAM                 
  !                 MOVE XCTRL-LIBELLE       TO TSTENVOI-LIBELLE                
  !                 PERFORM ECRITURE-TSTENVOI                                   
  !              END-IF                                                         
  !              PERFORM FETCH-CXCTRL                                           
  !              IF WC-XCTRL-EXPDEL-PB-DB2                                      
  !                 SET WC-TSTENVOI-ERREUR TO TRUE                              
  !                 GO TO FIN-CREATION-TSTENVOI                                 
  !              END-IF                                                         
  !           END-PERFORM                                                       
  !           PERFORM CLOSE-CXCTRL                                              
  !   * AJOUT D'UN ENREG DE FIN                                                 
  !           MOVE '99999'                   TO TSTENVOI-NSOCZP                 
  !           MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
  !           MOVE 'MGV65'                   TO TSTENVOI-WPARAM                 
  !           MOVE 'FIN TSTENVOI '           TO TSTENVOI-LIBELLE                
  !           PERFORM ECRITURE-TSTENVOI.                                        
  !                                                                             
V43R0  FIN-CREATION-TSTENVOI.  EXIT.                                            
  !    FIN-GEST-TSTENVOI.  EXIT.                                                
  !   *                                                                         
  !   *-----------------------------------------------------------------        
  !    LECTURE-TSTENVOI SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
  !   *    EXEC CICS                                                            
  !   *         READQ TS                                                        
  !   *         QUEUE  (WC-ID-TSTENVOI)                                         
  !   *         INTO   (TS-TSTENVOI-DONNEES)                                    
  !   *         LENTGH (LENGTH OF TS-TSTENVOI-DONNEES)                          
  !   *         ITEM   (WP-RANG-TSTENVOI)                                       
  !   *         NOHANDLE                                                        
  !   *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
      *{Post-translation Correct-mot-LENGTH
      *          LENTGH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                LENGTH(LENGTH OF TS-TSTENVOI-DONNEES)
      *}
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
  !        EVALUATE EIBRESP                                                     
  !        WHEN 0                                                               
  !           SET WC-TSTENVOI-SUITE      TO TRUE                                
  !           SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
  !        WHEN 26                                                              
  !        WHEN 44                                                              
  !           SET WC-TSTENVOI-FIN        TO TRUE                                
  !        WHEN OTHER                                                           
  !           SET WC-TSTENVOI-ERREUR     TO TRUE                                
  !        END-EVALUATE.                                                        
  !    FIN-LECT-TSTENVOI. EXIT.                                                 
  !   *                                                                         
  !   *-----------------------------------------------------------------        
  !    ECRITURE-TSTENVOI SECTION.                                               
      *{ normalize-exec-xx 1.5                                                  
  !   *    EXEC CICS                                                            
  !   *         WRITEQ TS                                                       
  !   *         QUEUE  (WC-ID-TSTENVOI)                                         
  !   *         FROM   (TS-TSTENVOI-DONNEES)                                    
  !   *         LENTGH (LENGTH OF TS-TSTENVOI-DONNEES)                          
  !   *         NOHANDLE                                                        
  !   *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
                QUEUE  (WC-ID-TSTENVOI)                                         
                FROM   (TS-TSTENVOI-DONNEES)                                    
      *{Post-translation Correct-mot-LENGTH
      *          LENTGH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                LENGTH(LENGTH OF TS-TSTENVOI-DONNEES)
      *}
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
  !        INITIALIZE TS-TSTENVOI-DONNEES.                                      
  !    FIN-ECRIT-TSTENVOI. EXIT.                                                
  !   *                                                                         
  !    ECRITURE-DISPLAY SECTION.                                                
      ***************************                                               
           EXEC CICS WRITEQ TD QUEUE ('CESO')                                   
                FROM (W-MESSAGE-S)                                              
                LENGTH (80) NOHANDLE                                            
           END-EXEC                                                             
           MOVE SPACE TO W-MESSAGE.                                             
  !    FIN-ECRITURE-DISPLAY. EXIT.                                              
      ***************************                                               
  !   *-----------------------------------------------------------------        
  !    DECLARE-OPEN-CXCTRL SECTION.                                             
  !        MOVE FUNC-DECLARE             TO TRACE-SQL-FUNCTION.                 
  !        MOVE 'RVGA01ZZ'               TO TABLE-NAME.                         
  !        MOVE 'EXPDEL'                 TO MODEL-NAME.                         
  !        EXEC SQL                                                             
  !             DECLARE CXCTRL CURSOR FOR                                       
  !             SELECT SOCZP, CTRL, WPARAM, LIBELLE                             
  !             FROM RVGA01ZZ                                                   
  !             WHERE TRANS = 'EXPDEL'                                          
  !             AND   WFLAG = 'O'                                               
  !             ORDER BY SOCZP, CTRL                                            
                FOR FETCH ONLY                                                  
                WITH UR                                                         
  !        END-EXEC.                                                            
  !        PERFORM TEST-SQLCODE.                                                
  !        MOVE FUNC-OPEN                TO TRACE-SQL-FUNCTION.                 
  !        EXEC SQL OPEN CXCTRL END-EXEC.                                       
  !        PERFORM TEST-SQLCODE.                                                
           IF SQLCODE NOT = 0                                                   
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS ABEND ABCODE ('MAXT') END-EXEC                          
      *--                                                                       
              EXEC CICS ABEND ABCODE ('MAXT')                                   
              END-EXEC                                                          
      *}                                                                        
           END-IF.                                                              
  !   *                                                                         
  !    FETCH-CXCTRL SECTION.                                                    
  !        MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.                 
  !        EXEC SQL                                                             
  !             FETCH CXCTRL                                                    
  !             INTO :XCTRL-SOCZP                                               
  !                , :XCTRL-CTRL                                                
  !                , :XCTRL-WPARAM                                              
  !                , :XCTRL-LIBELLE                                             
  !        END-EXEC.                                                            
  !        PERFORM TEST-SQLCODE.                                                
  !        IF WC-XCTRL-EXPDEL-SUITE                                             
  !           SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
  !        END-IF.                                                              
  !   *                                                                         
  !    CLOSE-CXCTRL SECTION.                                                    
  !        MOVE FUNC-CLOSE               TO TRACE-SQL-FUNCTION.                 
  !        EXEC SQL CLOSE CXCTRL  END-EXEC.                                     
  !        PERFORM TEST-SQLCODE.                                                
  !   *                                                                         
  !   *-----------------------------------------------------------------        
  !    TEST-SQLCODE SECTION.                                                    
  !         EVALUATE TRUE                                                       
  !         WHEN SQLCODE = 0                                                    
  !              SET WC-XCTRL-EXPDEL-SUITE    TO TRUE                           
  !         WHEN SQLCODE = +100                                                 
  !              SET WC-XCTRL-EXPDEL-FIN      TO TRUE                           
  !         WHEN OTHER                                                          
  !              SET WC-XCTRL-EXPDEL-PB-DB2   TO TRUE                           
  !              SET WC-XCTRL-EXPDEL-FIN      TO TRUE                           
  !              MOVE SQLCODE                 TO WZ-XCTRL-EXPDEL-SQLCODE        
V41R0      END-EVALUATE.                                                        
      ******************************************************************        
       TEMP02-COPY SECTION. CONTINUE.                                           
          COPY SYKCTSRD.                                                        
      *DESCRIPTION DES BRIQUES AIDA CICS                                        
       S15-COPY    SECTION. CONTINUE. COPY SYKCLINK.                            
      *DESCRIPTION DES BRIQUES AIDA SQL                                         
       AIDA-SQL-01 SECTION. CONTINUE. COPY SYKSTRAC.                            
      *MODULE D'ABANDON                                                         
       99-COPY     SECTION. CONTINUE. COPY SYKCERRO.                            
      * PREPARATION DES CLEFS.                                                  
       USER-SQL-1  SECTION. CONTINUE. COPY PRGA0100.                            
       USER-SQL-2  SECTION. CONTINUE. COPY PRGA0000.                            
       USER-SQL-3  SECTION. CONTINUE. COPY PRGA1400.                            
       USER-SQL-4  SECTION. CONTINUE. COPY PRGS1000.                            
       USER-SQL-5  SECTION. CONTINUE. COPY PRGB0500.                            
       USER-SQL-6  SECTION. CONTINUE. COPY PRGB1500.                            
       USER-SQL-7  SECTION. CONTINUE. COPY PRGA9900.                            
       USER-SQL-8  SECTION. CONTINUE. COPY PRFL5000.                            
NV0910 USER-SQL-9  SECTION. CONTINUE. COPY PRGF5500.                            
NV0910 USER-SQL-10 SECTION. CONTINUE. COPY PRFL9000.                            
      *                                                                         
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSWR.                            
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSDE.                            
      * MODULES GENERAUX APPELES PAR LES VENTES (BETDATC, DSYST...)             
       COPY MGV00.                                                              
       COPY SYKPMONT.                                                           
                                                                                
