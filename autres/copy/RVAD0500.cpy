      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
000010*---------------------------------------------------------                
000020*   LISTE DES HOST VARIABLES DE LA TABLE RVAD0500                         
000030*---------------------------------------------------------                
000040*                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
000050*01  RVAD0500.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD0500.                                                            
      *}                                                                        
000060     02  AD05-NCODIC                                                      
000070         PIC X(0007).                                                     
000080     02  AD05-COPCO                                                       
000090         PIC X(0003).                                                     
000100     02  AD05-CDATA                                                       
000110         PIC X(0015).                                                     
000120     02  AD05-VDATA                                                       
000130         PIC X(0020).                                                     
000140     02  AD05-LDATA                                                       
000150         PIC X(0020).                                                     
000160     02  AD05-WDATA                                                       
000170         PIC X(0001).                                                     
000180     02  AD05-DDATA                                                       
000190         PIC X(0008).                                                     
000200     02  AD05-DSYST                                                       
000210         PIC S9(13) COMP-3.                                               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
000220*                                                                         
000230*---------------------------------------------------------                
000240*   LISTE DES FLAGS DE LA TABLE RVAD0500                                  
000250*---------------------------------------------------------                
000260*                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
000270*01  RVAD0500-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD0500-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000280*    02  AD05-NCODIC-F                                                    
000290*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AD05-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000300*    02  AD05-COPCO-F                                                     
000310*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AD05-COPCO-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000320*    02  AD05-CDATA-F                                                     
000330*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AD05-CDATA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000340*    02  AD05-VDATA-F                                                     
000350*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AD05-VDATA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000360*    02  AD05-LDATA-F                                                     
000370*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AD05-LDATA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000380*    02  AD05-WDATA-F                                                     
000390*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AD05-WDATA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000400*    02  AD05-DDATA-F                                                     
000410*        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  AD05-DDATA-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000420*    02  AD05-DSYST-F                                                     
000430*        PIC S9(4) COMP.                                                  
      *                                                                         
      *--                                                                       
           02  AD05-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
