      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           EJECT                                                        00000100
      *==> DARTY ****************************************************** 00010001
      *          * COMMAREA TRAITEMENT BGD100 EN TP = MGD100          * 00020001
      *          * LE MODULE MGD100 EST APPELE PAR TGD30              * 00020002
      ****************************************************** COMMD100 * 00030001
      *                                                                 00040001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-GD100-LONG-COMMAREA PIC S9(4) COMP VALUE +88.           00050001
      *--                                                                       
       01  COMM-GD100-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +88.                 
      *}                                                                        
      *                                                                 00060001
       01  Z-COMMAREA-MGD100.                                           00070001
      *                                                                 00080001
          02 MGD100-NSOCDEPOT            PIC XXX.                       00090001
          02 MGD100-NDEPOT               PIC XXX.                       00090002
          02 MGD100-DDESTOCK             PIC X(8).                      00090003
          02 MGD100-CSELART              PIC X(5).                      00090004
          02 MGD100-CDEPOT-PHYS          PIC X(3).                      00090005
          02 MGD100-CODE-RETOUR          PIC 99.                        00090006
          02 MGD100-MESSAGE-ERREUR       PIC X(64).                     00090007
      *                                                                 00100001
                                                                                
