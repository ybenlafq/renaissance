      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  WS-EGM000.                                                           
           05  EGM-Z-DOUBLEWORD.                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  EGM-DOUBLEWORD    PIC S9(15)   VALUE ZERO   COMP.            
      *--                                                                       
               10  EGM-DOUBLEWORD    PIC S9(15)   VALUE ZERO COMP-5.            
      *}                                                                        
           05  EGM-Z-FULLWORD.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  EGM-FULLWORD      PIC S9(9)   VALUE ZERO    COMP.            
      *--                                                                       
               10  EGM-FULLWORD      PIC S9(9)   VALUE ZERO COMP-5.             
      *}                                                                        
           05  EGM-Z-HALFWORD.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  EGM-HALFWORD      PIC S9(4)   VALUE ZERO    COMP.            
      *--                                                                       
               10  EGM-HALFWORD      PIC S9(4)   VALUE ZERO COMP-5.             
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-XX            PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-XX            PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-A             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-A             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-B             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-B             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-C             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-C             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-D             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-D             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-I             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-I             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-J             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-J             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-K             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-K             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-L             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-L             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-M             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-M             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-N             PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-N             PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-LONG          PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-LONG          PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-LONGUEUR      PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-LONGUEUR      PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-POSIT         PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-POSIT         PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-COMPT         PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-COMPT         PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-IND-DEC       PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-IND-DEC       PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-DECMASQ       PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-DECMASQ       PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-DEB           PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-DEB           PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-DEB-NN        PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-DEB-NN        PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-DEB-CH        PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-DEB-CH        PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-BEG           PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-BEG           PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-POS-VIR       PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-POS-VIR       PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-ECART         PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-ECART         PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-LONG-CH       PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-LONG-CH       PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-LG-PHY        PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-LG-PHY        PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-LG-PHY1       PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-LG-PHY1       PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-LG-PHY2       PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-LG-PHY2       PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-NBDEC         PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-NBDEC         PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-DECCH1        PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-DECCH1        PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-DECCH2        PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-DECCH2        PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EGM-DECCHR        PIC S9(4)  COMP VALUE ZERO.                    
      *--                                                                       
           05  EGM-DECCHR        PIC S9(4) COMP-5 VALUE ZERO.                   
      *}                                                                        
           05  EGM-VIRGULE       PIC X(1)        VALUE '0'.                     
           05  EGM-VERIF         PIC X(1)        VALUE '0'.                     
           05  EGM-ACCORD-1      PIC X(1)        VALUE '0'.                     
           05  EGM-ACCORD-2      PIC X(1)        VALUE '0'.                     
           05  EGM-ARRONDI       PIC X(1)        VALUE ' '.                     
               88  EGM-ARRONDIR                  VALUE 'O'.                     
           05  EGM-CHAMP         PIC X(30)       VALUE SPACES.                  
           05  EGM-XX10          PIC S9(8)V9(7)  VALUE ZERO    COMP-3.          
           05  EGM-RESULT        PIC S9(15)      VALUE ZERO    COMP-3.          
           05  EGM-RESULTI       PIC S9(15)      VALUE ZERO    COMP-3.          
           05  EGM-RESTE         PIC S9(15)      VALUE ZERO    COMP-3.          
           05  EGM-CHPA1.                                                       
               10  EGM-CHP1      PIC S9(15)      VALUE ZERO    COMP-3.          
           05  EGM-CHPA2.                                                       
               10  EGM-CHP2      PIC S9(15)      VALUE ZERO    COMP-3.          
           05  EGM-CHPAR.                                                       
               10  EGM-CHPR      PIC S9(15)      VALUE ZERO    COMP-3.          
           05  EGM-CH1.                                                         
               10  EGM-CHAMP1    PIC S9(15)      VALUE ZERO.                    
           05  EGM-CH2.                                                         
               10  EGM-CHAMP2    PIC S9(15)      VALUE ZERO.                    
           05  EGM-CHR.                                                         
               10  EGM-CHAMPR    PIC S9(15)      VALUE ZERO.                    
           05  EGM-CHN.                                                         
               10  EGM-CHAMPN    PIC  9(15)      VALUE ZERO.                    
           05  EGM-Z-DATE        PIC 9(08)       VALUE ZERO.                    
           05  EGM-TYPE-CH       PIC X(01)       VALUE SPACES.                  
           05  EGM-CONT-CH       PIC X(64)       VALUE SPACES.                  
           05  EGM-CONT1         PIC X(64)       VALUE SPACES.                  
           05  EGM-CONT2         PIC X(64)       VALUE SPACES.                  
           05  EGM-TYPC1         PIC X           VALUE SPACES.                  
           05  EGM-TYPC2         PIC X           VALUE SPACES.                  
                                                                                
