      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE LPA2I LIBELLES A2I                     *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
       01  RVLPA2I.                                                             
           05  LPA2I-CTABLEG2    PIC X(15).                                     
           05  LPA2I-CTABLEG2-REDEF REDEFINES LPA2I-CTABLEG2.                   
               10  LPA2I-CPVENTE         PIC X(05).                             
               10  LPA2I-NVERSION        PIC X(01).                             
               10  LPA2I-CTYPE           PIC X(01).                             
               10  LPA2I-NSEQ            PIC X(02).                             
           05  LPA2I-WTABLEG     PIC X(80).                                     
           05  LPA2I-WTABLEG-REDEF  REDEFINES LPA2I-WTABLEG.                    
               10  LPA2I-LCOMMENT        PIC X(55).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVLPA2I-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LPA2I-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  LPA2I-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  LPA2I-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  LPA2I-WTABLEG-F   PIC S9(4) COMP-5.                              
       EXEC SQL END DECLARE SECTION END-EXEC.                                                                                
      *}                                                                        
