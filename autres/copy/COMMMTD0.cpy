      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:09 >
      
      ******************************************************************
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *
      ******************************************************************
      *                                                                *
      *  PROJET     : GESTION DES DOSSIERS                             *
      *  PROGRAMME  : MGVXX                                            *
      *  TITRE      : COMMAREA DES MODULES DOSSIER                     *
      *  LONGUEUR   : 150 C                                            *
      *                                                                *
      *  UTILISE PAR: MTD00 - ALIMENTATIONS DES DOSSIERS GSM           *
      *                                                                *
      ******************************************************************
       01  COMM-MTD00-APPLI.
      **** DONNEES COMMUNES ***************************************** 60
           05 COMM-MTD00-CODRET         PIC X(01).
              88 COMM-MTD00-CODRET-OK                         VALUE ' '.
              88 COMM-MTD00-CODRET-ERREUR                     VALUE '1'.
           05 COMM-MTD00-LIBERR.
              10 COMM-MTD00-NSEQERR        PIC X(04).
              10 COMM-MTD00-ERRFIL         PIC X(01).
              10 COMM-MTD00-LMESSAGE       PIC X(54).
      *
           05 COMM-MTD00-ZONES-ENTREE      PIC X(45).
      **** ZONES EN ENTREE DU MTD00 - SOCIETE VENTE / CINSEE ***********
           05 COMM-MTD00-ENTREE  REDEFINES COMM-MTD00-ZONES-ENTREE.
              10 COMM-MTD00-NSOCIETE       PIC X(03).
              10 COMM-MTD00-NLIEU          PIC X(03).
              10 COMM-MTD00-NVENTE         PIC X(07).
      * SERT POUR LE MODUEL DE SORTIE
      * S = START RETOUR = RETURN CICS
      * L = LINK  RETOUR = RETURN COMMAREA
              10 COMM-MTD00-APPEL          PIC X(01).
      *
           05 COMM-MTD00-ZONES-SORTIE      PIC X(45).
      **** ZONES EN SORTIE DU MTD00 - LIEU DEPART LIVRAISON ************
           05 COMM-MTD00-SORTIE  REDEFINES COMM-MTD00-ZONES-SORTIE.
              10 COMM-MTD00-FILLER         PIC X(45).
      
