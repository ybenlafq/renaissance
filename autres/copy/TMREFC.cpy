      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************          
      * DSECT DU FICHIER FTMREF DE TRANSMISSION DES DONNEES  *                  
      * DE LA SOUS-TABME QCNBS POUR MISE A JOUR DU .REF DANS LES SAV *          
      * APPLICATION CARTES QUALITE  *                                           
      ****************************************************************          
       01 ENR-T1REF.                                                            
          04 CLE-T1REF.                                                         
             06 T1REF-INDIC.                                                    
                08 T1REF-NSFIC        PIC 999.                                  
                08 T1REF-INDCLE.                                                
                   10 T1REF-CDSOC     PIC X(03).                                
                   10 T1REF-CLE       PIC X(13).                                
              06 T1REF-ANU            PIC X.                                    
              06 T1REF-PART           PIC X.                                    
              06 FILLER               PIC XX.                                   
           04 T1REF-DATA              PIC X(108).                               
       01 ENR-T2REF.                                                            
          04 CLE-T2REF.                                                         
             06 T2REF-NSFIC           PIC 999.                                  
             06 T2REF-SELECT.                                                   
                08 T2REF-SSFIC        PIC 999.                                  
                08 T1REF-INDCLE.                                                
                   10 T2REF-CDSOC     PIC X(03).                                
                   10 T2REF-CLE       PIC X(13).                                
              06 T2REF-TYPMAJ         PIC X.                                    
           04 T2REF-DATA.                                                       
              06 T2REF-SOCTES.                                                  
                 08 T2REF-STE         PIC X(3)   OCCURS 27.                     
              06 T2REF-PART           PIC X.                                    
              06 FILLER               PIC X(25).                                
              06 T2REF-ANU            PIC X.                                    
                                                                                
