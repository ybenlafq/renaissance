      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * LINK � MEC15 - M�J du "mouchard" commandes eCommerce (Ent�tes)          
      ******************************************************************        
       01  MEC15E-ENTETE REDEFINES MEC15C-COMMAREA.                             
INPUT ******************************************************************        
   1       10 MEC15E-NCDEWC   PIC S9(15)V   PACKED-DECIMAL.                     
   9       10 MEC15E-NSOCIETE PIC X(3).                                         
  12       10 MEC15E-NLIEU    PIC X(3).                                         
  15       10 MEC15E-NVENTE   PIC X(7).                                         
  22       10 MEC15E-NSEQNQ   PIC S9(5)V    PACKED-DECIMAL.                     
  25       10 MEC15E-STAT     PIC X(1).                                         
  26       10 MEC15E-NSEQERR  PIC X(0004).                                      
  30       10 MEC15E-NUM      PIC S9(13)V99 PACKED-DECIMAL OCCURS 5.            
  70       10 MEC15E-ALP      PIC X(20)                    OCCURS 5.            
 170       10 MEC15E-TEXTE    PIC X(80).                                        
OUTPUT******************************************************************        
 250       10 MEC15E-RC       PIC X.                                            
           88 MEC15E-OK           VALUE 'O'.                                    
           88 MEC15E-KO           VALUE 'K'.                                    
      *{ Tr-Hexa-Map 1.5                                                        
      *    88 MEC15E-FLAG-ENT     VALUE X'1F'.                                  
      *--                                                                       
           88 MEC15E-FLAG-ENT     VALUE X'1F'.                                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    88 MEC15E-FLAG-LIGNE   VALUE X'2F'.                                  
      *--                                                                       
           88 MEC15E-FLAG-LIGNE   VALUE X'07'.                                  
      *}                                                                        
      *{ Tr-Hexa-Map 1.5                                                        
      *    88 MEC15E-FLAG-DELETE  VALUE X'3F'.                                  
      *--                                                                       
           88 MEC15E-FLAG-DELETE  VALUE X'1A'.                                  
      *}                                                                        
 251       10 MEC15E-MSG      PIC X(80).                                        
           10 MEC15E-MSG-REDEF REDEFINES MEC15E-MSG.                            
 251          15  MEC15E-OPER PIC X(4).                                         
 255          15  MEC15E-NOM  PIC X(25).                                        
 331  * LENGTH=330                                                              
                                                                                
