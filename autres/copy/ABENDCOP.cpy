      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  ABEND-ZONE.                                                          
           05 ABEND-PROG       PIC  X(8).                                       
           05 ABEND-MESS.                                                       
              10  FILLER       PIC  X(35).                                      
              10  ABEND-NCODE.                                                  
                  15  ABEND-CODE   PIC  -ZZZ9.                                  
                                                                                
