      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                               *         
       01  TS-IDDO.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-IDDO-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +95.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-IDDO-DONNEES.                                                  
              03 TS-IDDO-NCHAMP            PIC X(30).                           
              03 TS-IDDO-LENGTH            PIC 9(02).                           
              03 TS-IDDO-VALUE             PIC X(50).                           
              03 TS-IDDO-NVAR              PIC X(11).                           
              03 TS-IDDO-NUMSEG            PIC 9(02).                           
      *                                                                         
                                                                                
