      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE D'EDITION LISTE DESTOCKAGE     *            
      *  PROJET CHS                                                *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MHC10                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MHC1-LONG-COMMAREA PIC S9(4) COMP VALUE +231.                   
      *--                                                                       
       01  COMM-MHC1-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +231.                 
      *}                                                                        
       01  Z-COMMAREA-MHC1.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MHC1-I.                                                      
      *                                                                         
              03 COMM-MHC1-CIMP            PIC    X(04).                        
              03 COMM-MHC1-NLIEUHC         PIC    X(03).                        
              03 COMM-MHC1-LTRAIT          PIC    X(20).                        
              03 COMM-MHC1-DENVOI          PIC    X(08).                        
              03 COMM-MHC1-NENVOI          PIC    X(07).                        
              03 COMM-MHC1-NSOCORIG        PIC    X(03).                        
              03 COMM-MHC1-NLIEUORIG       PIC    X(03).                        
              03 COMM-MHC1-NSSLIEUORIG     PIC    X(03).                        
              03 COMM-MHC1-CLIEUTRTORIG    PIC    X(05).                        
              03 COMM-MHC1-NSOCDEST        PIC    X(03).                        
              03 COMM-MHC1-NLIEUDEST       PIC    X(03).                        
              03 COMM-MHC1-NSSLIEUDEST     PIC    X(03).                        
              03 COMM-MHC1-CLIEUTRTDEST    PIC    X(05).                        
              03 COMM-MHC1-CTIERS          PIC    X(05).                        
              03 COMM-MHC1-LTIERS          PIC    X(20).                        
              03 COMM-MHC1-LADR1           PIC    X(32).                        
              03 COMM-MHC1-LADR2           PIC    X(32).                        
              03 COMM-MHC1-CPOSTAL         PIC    X(05).                        
              03 COMM-MHC1-LCOMMUNE        PIC    X(26).                        
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MHC1-O.                                                      
      *---------------------------------------CODE RETOUR                       
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MHC1-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MHC1-OK           VALUE '0'.                           
                 88 COMM-MHC1-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MHC1-MSG             PIC    X(40).                        
                                                                                
