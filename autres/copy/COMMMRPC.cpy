      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:16 >
      
      ***************************************************************
      *   COMMAREA SOUS-PROGRAMME MRP0 D'ACCES AUX TABLES DB2
      *   CHAINE RELEVES DE PRIX   (VERSION COBOL)        LONG : 500
      ***************************************************************
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  COMM-MRP0-LONG-COMMAREA  PIC S9(4) COMP      VALUE +500.
      *--
       01  COMM-MRP0-LONG-COMMAREA  PIC S9(4) COMP-5      VALUE +500.
      *}
      *
       01  Z-COMMAREA.
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   PARAMETRES DE DETERMINATION DES TRAITEMENTS     LONG : 100
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-MRP0-PARAMETRES.
               03  COMM-MRP0-NOMTACHE  PIC X(04)          VALUE SPACES.
               03  COMM-MRP0-NOMTABLE  PIC X(06)          VALUE SPACES.
               03  COMM-MRP0-SQLFONCT  PIC X(06)          VALUE SPACES.
               03  COMM-MRP0-NITEM     PIC S9(3) COMP-3   VALUE ZEROES.
               03  COMM-MRP0-NBLIG     PIC S9(3) COMP-3   VALUE ZEROES.
               03  COMM-MRP0-CODRET    PIC S9(3) COMP-3   VALUE ZEROES.
               03  COMM-MRP0-MESS      PIC X(72)          VALUE SPACES.
               03  COMM-MRP0-NINDEX    PIC X(02)          VALUE SPACES.
               03  FILLER              PIC X(04)          VALUE SPACES.
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   DESCRIPTION DES DONNEES DES TABLES              LONG : 400
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-MRP0-TABLES        PIC X(400)         VALUE SPACES.
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   LISTE DES VARIABLES DE LA TABLE RVGA0000    LG : 60       *
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-GA00-DONNEES REDEFINES COMM-MRP0-TABLES.
               03  COMM-GA00-NCODIC     PIC X(07).
               03  COMM-GA00-LREFFOURN  PIC X(20).
               03  COMM-GA00-CFAM       PIC X(05).
               03  COMM-GA00-CMARQ      PIC X(05).
               03  COMM-GA00-CASSORT    PIC X(05).
               03  COMM-GA00-DEFSTATUT  PIC X(08).
               03  COMM-GA00-CTAUXTVA   PIC X(05).
               03  COMM-GA00-CAPPRO     PIC X(05).
               03  FILLER               PIC X(340).
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   LISTE DES VARIABLES DE LA TABLE RVGA0100
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-GA01-DONNEES REDEFINES COMM-MRP0-TABLES.
               03  COMM-GA01-CTABLEG1   PIC X(05).
               03  COMM-GA01-CTABLEG2   PIC X(15).
               03  COMM-GA01-WTABLEG    PIC X(80).
               03  FILLER               PIC X(300).
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   LISTE DES VARIABLES DE LA TABLE RVGA0400  LG : 151
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-GA04-DONNEES REDEFINES COMM-MRP0-TABLES.
               03  COMM-GA04-NCONC       PIC X(04).
               03  COMM-GA04-NTYPCONC    PIC X(02).
               03  COMM-GA04-NZONPRIX    PIC X(02).
               03  COMM-GA04-LENSCONC    PIC X(15).
               03  COMM-GA04-LEMPCONC    PIC X(15).
               03  COMM-GA04-LADDR1CONC  PIC X(32).
               03  COMM-GA04-LADDR2CONC  PIC X(32).
               03  COMM-GA04-CPOSTCONC   PIC X(05).
               03  COMM-GA04-LVILLECONC  PIC X(18).
               03  COMM-GA04-DCRECONC    PIC X(08).
               03  COMM-GA04-CSTATUTCONC PIC X(01).
               03  COMM-GA04-DSTATUTCONC PIC X(08).
               03  COMM-GA04-CPONDCONC   PIC X(01).
               03  COMM-GA04-DRELVCONC   PIC X(08).
               03  FILLER                PIC X(249).
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   LISTE DES VARIABLES DE LA TABLE RVGA1400
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-GA14-DONNEES REDEFINES COMM-MRP0-TABLES.
               03  COMM-GA14-CFAM        PIC X(05).
               03  COMM-GA14-LFAM        PIC X(20).
               03  COMM-GA14-WSEQFAM     PIC S9(5) COMP-3.
               03  FILLER                PIC X(372).
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   LISTE DES VARIABLES DE LA TABLE RVGA5900
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-GA59-DONNEES REDEFINES COMM-MRP0-TABLES.
               03  COMM-GA59-NCODIC      PIC X(07).
               03  COMM-GA59-NZONPRIX    PIC X(02).
               03  COMM-GA59-PSTDTTC     PIC S9(7)V9(2) COMP-3.
               03  COMM-GA59-DEFFET      PIC X(08).
               03  COMM-GA59-LCOMMENT    PIC X(10).
               03  FILLER                PIC X(368).
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   LISTE DES VARIABLES DE LA TABLE RVRP0000
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-RP00-DONNEES REDEFINES COMM-MRP0-TABLES.
               03 COMM-RP00-NRELEVE    PIC X(05).
               03 COMM-RP00-NCONC      PIC X(04).
               03 COMM-RP00-CFAM       PIC X(05).
               03 COMM-RP00-LMARQ      PIC X(20).
               03 COMM-RP00-LREFFOURN  PIC X(20).
               03 COMM-RP00-NCODICREL  PIC X(08).
               03 COMM-RP00-NSEQ       PIC S9(05) COMP-3.
               03 COMM-RP00-DRELEVE    PIC X(08).
               03 COMM-RP00-PRELEVE    PIC S9(07) COMP-3.
               03 COMM-RP00-CVALMAR1   PIC X(01).
               03 COMM-RP00-CVALMAR2   PIC X(01).
               03 COMM-RP00-CVALMAR3   PIC X(01).
               03 COMM-RP00-CVALMAR4   PIC X(01).
               03 COMM-RP00-CVALMAR5   PIC X(01).
               03 COMM-RP00-QNBPSAISI  PIC S9(05) COMP-3.
               03 COMM-RP00-NCODICREC  PIC X(08).
               03 COMM-RP00-WPOND      PIC X(01).
               03 COMM-RP00-DSYST      PIC S9(13) COMP-3.
               03 FILLER               PIC X(299).
      *
                    EJECT
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
      *   LISTE DES VARIABLES DE LA TABLE RVRP0500
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
           02  COMM-RP05-DONNEES REDEFINES COMM-MRP0-TABLES.
               03 COMM-RP05-NRELEVE     PIC X(05).
               03 COMM-RP05-LMARQ       PIC X(20).
               03 COMM-RP05-NMARQ       PIC X(05).
               03 COMM-RP05-CMARQ       PIC X(05).
               03 COMM-RP05-DSYST       PIC S9(13) COMP-3.
               03 FILLER                PIC X(358).
      *
                    EJECT
      
