      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION MOUVEMENTS STANDARD                    *
      * NOM FICHIER.: EMVSTD                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 663                                             *
      *****************************************************************
      *
       01  EMVSTD.
      * TYPE ENREGISTREMENT : EMISSION IMAGE STANDARD
           05      EMVSTD-TYP-ENREG       PIC  X(0006).
      * NUMERO DE MOUVEMENT
           05      EMVSTD-NMVT            PIC  9(0009).
      * NATURE DE MOUVEMENT
           05      EMVSTD-CNATMVT         PIC  9(0002).
      * CODE SOCIETE
           05      EMVSTD-CSOCIETE        PIC  X(0005).
      * CODE ARTICLE
           05      EMVSTD-CARTICLE        PIC  X(0018).
      * NUMERO DE LOT
           05      EMVSTD-NLOT            PIC  X(0015).
      * NUMERO DE CARTON
           05      EMVSTD-NCARTON         PIC  X(0010).
      * DATE LIMITE DE VENTE
           05      EMVSTD-DLV             PIC  X(0008).
      * DATE LIMITE DE CONSOMMATION
           05      EMVSTD-DLC             PIC  X(0008).
      * INFORMATION 1
           05      EMVSTD-INFO1           PIC  X(0010).
      * INFORMATION 2
           05      EMVSTD-INFO2           PIC  X(0010).
      * INFORMATION 3
           05      EMVSTD-INFO3           PIC  X(0010).
      * NUMERO DE SITE ORIGINE
           05      EMVSTD-NSOCORIG        PIC  X(0003).
      * NUMERO DE SITE DESTINATION
           05      EMVSTD-NSOCDEST        PIC  X(0003).
      * ETAT DE BLOCAGE INITIAL
           05      EMVSTD-EBLOCINIT       PIC  9(0003).
      * ETAT DE BLOCAGE FINAL
           05      EMVSTD-EBLOCFINAL      PIC  9(0003).
      * ETAT DE STOCK N3 INITIAL
           05      EMVSTD-ESTOCKINIT      PIC  9(0003).
      * ETAT DE STOCK N3 FINAL
           05      EMVSTD-ESTOCKFINAL     PIC  9(0003).
      * RESERVATION INITIALE
           05      EMVSTD-RESERVINIT      PIC  X(0015).
      * RESERVATION FINALE
           05      EMVSTD-RESERVFINAL     PIC  X(0015).
      * QUANTITE
           05      EMVSTD-QTE             PIC  X(0009).
      * CODE FOURNISSEUR
           05      EMVSTD-CFOURN          PIC  X(0015).
      * DATE DE MOUVEMENT
           05      EMVSTD-DMVT            PIC  X(0008).
      * HEURE DE MOUVEMENT
           05      EMVSTD-HMVT            PIC  X(0006).
      * NUMERO DE DOSSIER ARRIVAGE
           05      EMVSTD-NDOS-ARR        PIC  X(0015).
      * NUMERO INVENTAIRE
           05      EMVSTD-NINVENTAIRE     PIC  X(0015).
      * NUMERO DE DOSSIER RECEPTION
           05      EMVSTD-NDOS-REC        PIC  X(0015).
      * NUMERO DE COMMANDE N3
           05      EMVSTD-NCDE-N3         PIC  X(0015).
      * NUMERO DE LIGNE DE RECEPTION
           05      EMVSTD-NLIGNE-REC      PIC  9(0005).
      * NUMERO DE SOUS-LIGNE DE RECEPTION
           05      EMVSTD-NSSLIGNE-REC    PIC  9(0005).
      * COMMENTAIRE
           05      EMVSTD-COMMENTAIRE     PIC  X(0080).
      * CARACTERISTIQUE SUPPLEMENTAIRE
           05      EMVSTD-CARACT-SUPPL    PIC  X(0255).
      * CARACTERISTIQUE 1
           05      EMVSTD-CARACT1         PIC  X(0020).
      * CARACTERISTIQUE 2
           05      EMVSTD-CARACT2         PIC  X(0020).
      * CARACTERISTIQUE 3
           05      EMVSTD-CARACT3         PIC  X(0020).
      * FIN DE L'ENREGISTREMENT
           05      EMVSTD-FIN             PIC  X(0001).
      
