      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *                             DABORD LA COPY DE L"ENTETE MESSMQ  *      13
      *   MESTSV10 COPY                                                *      13
      * - RECEPTION RESA DE SIEBEL                                     *      13
      *   500 LONGEUR                                                  *      13
      * COPY POUR LES PROGRAMMES TSV10 ET MBS75                        *      13
      ******************************************************************        
              05 MESTSV10-DATA.                                                 
                 10 MESTSV10-CONSTANTES.                                        
                   15 MESTSV10-MESSAGEID      PIC X(07).                        
                   15 MESTSV10-MESSAGETYPE    PIC X(18).                        
                   15 MESTSV10-INTOBJECTNAME  PIC X(19).                        
                   15 MESTSV10-INTOBJECTFORM  PIC X(19).                        
      * NUMERO DE VENTE                                                         
                 10 MESTSV10-ENTETE-VENTE.                                      
                   15 MESTSV10-REFCMDEXT      PIC X(30).                        
                   15 MESTSV10-CVENDEUR       PIC X(30).                        
                   15 MESTSV10-MODPAI         PIC X(30).                        
                   15 MESTSV10-IDSIEBEL       PIC X(30).                        
      *---            TYPE DE VENTE (B2B - ASS)                                 
                   15 MESTSV10-TYPE-VENTE     PIC X(30).                        
      *---            RAN MAGASIN                                               
                   15 MESTSV10-RAN-MAG        PIC X(30).                        
      *---            PAIEMENT COMPTANT                                         
                   15 MESTSV10-P-COMPTANT     PIC X(22).                        
      *---            PAIEMENT ARF                                              
                   15 MESTSV10-P-ARF          PIC X(22).                        
      *---            MISE EN SERVICE                                           
                   15 MESTSV10-MSERVICE       PIC X(30).                        
      *---            DEPOSE UNIQUEMENT                                         
                   15 MESTSV10-DEPOSE         PIC X(30).                        
      *---            REPRISE ANCIEN APPAREIL                                   
                   15 MESTSV10-REPRISEANC     PIC X(30).                        
      *---            SOCIETE - LIEU PAIEMENT                                   
                   15 MESTSV10-SOCLIEUP       PIC X(30).                        
      * ADRESSE DE FACTURATION                                                  
                 10 MESTSV10-FACTURATION.                                       
                   15 MESTSV10-NCOMPTE        PIC X(30).                        
                   15 MESTSV10-FARF           PIC X.                            
                   15 MESTSV10-RAISON         PIC X(100).                       
                   15 MESTSV10-TELBUR         PIC X(15).                        
                   15 MESTSV10-CFILIALE       PIC X(20).                        
      *---            FLAG ADRESE DE FACTURATION                                
                   15 MESTSV10-FLAGB2B        PIC X.                            
                   15 MESTSV10-FCVOIE         PIC X(05).                        
                   15 MESTSV10-FCTVOIE        PIC X(30).                        
                   15 MESTSV10-FLVOIE         PIC X(30).                        
                   15 MESTSV10-CPOSTAL        PIC X(30).                        
                   15 MESTSV10-COMMUNE        PIC X(50).                        
                   15 MESTSV10-CPAYS          PIC X(05).                        
                   15 MESTSV10-CMPAD          PIC X(255).                       
      * ADRESSE DE LIVRAISON                                                    
                 10 MESTSV10-LIVRAISON.                                         
                   15 MESTSV10-LNCOMPTE       PIC X(30).                        
                   15 MESTSV10-NOM            PIC X(50).                        
                   15 MESTSV10-PRENOM         PIC X(50).                        
                   15 MESTSV10-CIVILITE       PIC X(15).                        
                   15 MESTSV10-LTELBUR        PIC X(15).                        
                   15 MESTSV10-EMAIL          PIC X(50).                        
                   15 MESTSV10-TELGSM         PIC X(15).                        
                   15 MESTSV10-TELDOM         PIC X(15).                        
                   15 MESTSV10-NCARTE         PIC X(20).                        
      *---            FLAG ADRESSE DE LIVRAISON                                 
                   15 MESTSV10-FADRLIVR       PIC X.                            
                   15 MESTSV10-CVOIE          PIC X(30).                        
                   15 MESTSV10-CTVOIE         PIC X(100).                       
                   15 MESTSV10-LVOIE          PIC X(100).                       
                   15 MESTSV10-LBATIMENT      PIC X(50).                        
                   15 MESTSV10-LESCALIER      PIC X(100).                       
                   15 MESTSV10-LETAGE         PIC X(40).                        
                   15 MESTSV10-LPORTE         PIC X(40).                        
                   15 MESTSV10-LCPOSTAL       PIC X(30).                        
                   15 MESTSV10-LCOMMUNE       PIC X(50).                        
                   15 MESTSV10-LCMPAD         PIC X(255).                       
                   15 MESTSV10-LCPAYS         PIC X(05).                        
      * LIGNE DE VENTE                                                          
      *          10 MESTSV10-LG-VENTE OCCURS 40.                                
                 10 MESTSV10-T-CODIC     OCCURS 40.                             
      *---            CODIC SI LONGUEUR = 7, CENREG SINON                       
                   15 MESTSV10-CODIC          PIC X(100).                       
                 10 MESTSV10-T-QTE       OCCURS 40.                             
                   15 MESTSV10-QTE            PIC S9(10).                       
                 10 MESTSV10-T-PVTOTAL   OCCURS 40.                             
                   15 MESTSV10-PVTOTAL     PIC  X(22).                          
                 10 MESTSV10-T-PVUNIT    OCCURS 40.                             
                   15 MESTSV10-PVUNIT      PIC  X(22).                          
      *            15 MESTSV10-QTE            PIC X(10).                        
      *            15 MESTSV10-PVTOTAL     PIC  X(9).                           
      *            15 MESTSV10-PVUNIT      PIC  X(9).                           
                 10 MESTSV10-T-DDELIV    OCCURS 40.                             
                   15 MESTSV10-DDELIV         PIC X(15).                        
                 10 MESTSV10-T-CMODDEL   OCCURS 40.                             
                   15 MESTSV10-CMODDEL        PIC X(30).                        
                 10 MESTSV10-T-LNBRE     OCCURS 40.                             
                   15 MESTSV10-LNBRE          PIC X(30).                        
                 10 MESTSV10-T-LNBRE2    OCCURS 40.                             
                   15 MESTSV10-LNBRE2         PIC X(30).                        
      *                                                                 00550009
      ***************************************************************** 00740000
                                                                                
