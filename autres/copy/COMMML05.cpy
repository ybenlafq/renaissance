      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : LOGISTIQUE GROUPE                                *        
      *  PROGRAMME  : MFL05                                            *        
      *  TITRE      : COMMAREA DU MODULE TP                            *        
      *               DE DETERMINATION DES ENTREPOTS DE STOCKAGE       *        
      *               D'UN CODIC OU D'UNE FAMILLE (DEMANDE)            *        
      *  LONGUEUR   : 100 C                                            *        
      *                                                                *        
      *  REMARQUE   * LES DEMANDES SONT STOCKES DANS :                 *        
      *             - UNE COMMAREA (COMMML06) EN BATCH                 *        
      *             - UNE TS EN (TSSL05)      EN TP                    *        
      *             * DEFFET A BLANC = DATE DU JOUR                    *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MFL05-APPLI.                                                    
           05 COMM-MFL05-ZONES-ENTREE.                                          
              10 COMM-MFL05-LDEPLIV.                                            
                 15 COMM-MFL05-NSOCIETE         PIC X(03).                      
                 15 COMM-MFL05-NLIEU            PIC X(03).                      
              10 COMM-MFL05-CTYPTRAIT        PIC X(05).                         
              10 COMM-MFL05-DEFFET           PIC X(08).                         
           05 COMM-MFL05-ZONES-SORTIE.                                          
              10 COMM-MFL05-MESSAGE.                                            
                 15 COMM-MFL05-CODRET        PIC X(01).                         
                    88 COMM-MFL05-CODRET-OK                   VALUE ' '.        
      *{ remove-comma-in-dde 1.5                                                
      *             88 COMM-MFL05-CODRET-ERREUR   VALUE '1' , '2' , '3'.        
      *--                                                                       
                    88 COMM-MFL05-CODRET-ERREUR   VALUE '1'   '2'   '3'.        
      *}                                                                        
                    88 COMM-MFL05-CODRET-ERR-LIEU             VALUE '1'.        
                    88 COMM-MFL05-CODRET-ERR-DEMANDE          VALUE '2'.        
                    88 COMM-MFL05-CODRET-ERR-SQL              VALUE '3'.        
                 15 COMM-MFL05-LIBERR.                                          
                    20 COMM-MFL05-NSEQERR        PIC X(04).                     
                    20 COMM-MFL05-ERRFIL         PIC X(01).                     
                    20 COMM-MFL05-LMESSAGE       PIC X(53).                     
              10 COMM-MFL05-NPRIORITE         PIC X(05).                        
              10 COMM-MFL05-CPROAFF           PIC X(05).                        
           05 COMM-MFL05-FILLER         PIC X(12).                              
                                                                                
