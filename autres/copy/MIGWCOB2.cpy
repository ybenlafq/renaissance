      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *==> DARTY ******************************* AIDA - COBOL 2 - DLI *         
      *          ==> EDITION D'UN ETAT CICS                           *         
      *===============================================================*         
      *                                                                         
      *****************************************************************         
      * DECLARATIONS DES 9 ELEMENTS D'EDITION                         *         
      *************************************************** COPY MIGC02 *         
      * -1- NOM ETAT -------------------------------                            
       01  IG1-ETA.                                                             
           05 IG1-ETA1  PIC X(001).                                             
           05 IG1-ETA2  PIC X(004).                                             
           05 IG1-ETA3  PIC X(001).                                             
      * -2- DATE EDITION ---------------------------                            
       01  IG2-DAT      PIC X(006).                                             
      * -3- DESTINATION ----------------------------                            
       01  IG3-DST      PIC X(009).                                             
      * -4- DOCUMENT -------------------------------                            
       01  IG4-DOC      PIC X(015).                                             
      * -5- DETERMINATION DU PROTOCOLE D'IMPRESSION-                            
       01  IG5-ASA      PIC X(001).                                             
      * -6- LIGNE ETAT -----------------------------                            
       01  IG6-FLU.                                                             
           05 IG6-LNG   PIC 9(003).                                             
           05 IG6-LIG   PIC X(512).                                             
      * -7- TRAITEMENT DE L'EXISTANT ---------------                            
       01  IG7-SUP      PIC X(001).                                             
      * -8- CODE ERREUR ----------------------------                            
       01  IG8-ERR      PIC X(004).                                             
      * -9- NUMERO DE PCB --------------------------                            
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  IG9-PCB      PIC 9(4)   COMP VALUE 0.                                
      *--                                                                       
       01  IG9-PCB      PIC 9(4) COMP-5 VALUE 0.                                
      *}                                                                        
       EJECT                                                                    
      *****************************************************************         
      * DECLARATIONS DES ZONES DE TRAVAIL POUR EDITION                *         
      *************************************************** COPY MIGC02 *         
      *----NUMERO DE PCB                                                        
       01  IG-CTL-PCB         PIC X      VALUE SPACES.                          
      *----DATE AAMMJJ DU JOUR                                                  
       01  IG-DAT-JOU         PIC X(6)   VALUE '000000'.                        
      *----HEURE DE TRAITEMENT                                                  
       01  IG-TIM-JOU         PIC X(8).                                         
       01  IG-TIM-DAY                    REDEFINES IG-TIM-JOU.                  
           02 IG-TIM-HMS      PIC 9(6).                                         
           02 IG-TIM-FIL      PIC 9(2).                                         
      *----CLE ACCES ETAT                                                       
       01  IDT-KEY-DLI.                                                         
           05 IDT-KEY-NOM     PIC X(006) VALUE SPACES.                          
           05 IDT-KEY-DAT     PIC X(006) VALUE SPACES.                          
           05 IDT-KEY-DST     PIC X(009) VALUE SPACES.                          
           05 IDT-KEY-DOC     PIC X(015) VALUE SPACES.                          
      *----SAUVEGARDES CONFIGURATION-PROTOCOLE-TYPE IMPRESSION                  
       01  SAU-CFG            PIC X      VALUE SPACES.                          
       01  SAU-PTC            PIC X      VALUE SPACES.                          
       01  SAU-IMP            PIC X      VALUE SPACES.                          
      *----SAUVEGARDE DES LONGUEURS                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  SAU-COL            PIC S9(4)  COMP VALUE +0.                         
      *--                                                                       
       01  SAU-COL            PIC S9(4) COMP-5 VALUE +0.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  SAU-CMD            PIC S9(4)  COMP VALUE +0.                         
      *--                                                                       
       01  SAU-CMD            PIC S9(4) COMP-5 VALUE +0.                        
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  SAU-DON            PIC S9(4)  COMP VALUE +0.                         
      *--                                                                       
       01  SAU-DON            PIC S9(4) COMP-5 VALUE +0.                        
      *}                                                                        
      *----CODE FONCTION DL1 A REALISER                                         
       01  CODDLI             PIC X(004) VALUE SPACES.                          
      *----CODE DETECTION RUPTURE SUR CLE ETAT                                  
       01  CODRUP             PIC X      VALUE SPACES.                          
      *----RUPTURE PARENTAGE DL1 SUR DESTINATION ETAT                           
       01  TOP-CFG            PIC X      VALUE SPACES.                          
      *----TYPE RACINE DIGSA 0=IDT 1=CFG                                        
       01  TOP-RCN            PIC X.                                            
       EJECT                                                                    
                                                                                
