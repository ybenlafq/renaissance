      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TYFRS TYPE DE FOURNISSEUR              *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTYFRS.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTYFRS.                                                             
      *}                                                                        
           05  TYFRS-CTABLEG2    PIC X(15).                                     
           05  TYFRS-CTABLEG2-REDEF REDEFINES TYFRS-CTABLEG2.                   
               10  TYFRS-CCOMPTE         PIC X(02).                             
           05  TYFRS-WTABLEG     PIC X(80).                                     
           05  TYFRS-WTABLEG-REDEF  REDEFINES TYFRS-WTABLEG.                    
               10  TYFRS-LCOMPTE         PIC X(20).                             
               10  TYFRS-WENVSAP         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTYFRS-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTYFRS-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYFRS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TYFRS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYFRS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TYFRS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
