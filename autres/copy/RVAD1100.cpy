      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE INT3.RTAD11                        *        
      ******************************************************************        
       01  RVAD1100.                                                            
           10 AD11-CFAMK           PIC X(5).                                    
           10 AD11-CSOC            PIC X(3).                                    
           10 AD11-CFAM            PIC X(5).                                    
           10 AD11-LFAM            PIC X(20).                                   
           10 AD11-FLAGMOD         PIC X(1).                                    
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  RVAD1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD11-CFAMK-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 AD11-CFAMK-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD11-CSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD11-CSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD11-CFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD11-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD11-LFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD11-LFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD11-FLAGMOD-F       PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 AD11-FLAGMOD-F       PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
