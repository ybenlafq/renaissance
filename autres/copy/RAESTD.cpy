      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION ARTICLE ENT�TE STANDARD               * 00031000
      * NOM FICHIER.: RAESTD                                          * 00040012
      *---------------------------------------------------------------* 00060000
      * CR   .......: 12/10/2011  17:46:00                            * 00070000
      * MODIFIE.....:   /  /                                          * 00080000
      * VERSION N�..: 001                                             * 00090000
      * LONGUEUR....: 301                                             * 00091011
      ***************************************************************** 00110000
      *                                                                 00120000
       01  RAESTD.                                                      00130000
      * TYPE ENREGISTREMENT                                             00140006
           05      RAESTD-TYP-ENREG       PIC  X(0006).                 00150000
      * CODE ACTION                                                     00151006
           05      RAESTD-CODE-ACTION     PIC  X(0001).                 00160000
      * CODE SOCIETE                                                    00160106
           05      RAESTD-CSOCIETE        PIC  X(0005).                 00161003
      * CODE ARTICLE                                                    00161106
           05      RAESTD-CARTICLE        PIC  X(0018).                 00161203
      * LIBELL� ARTICLE                                                 00161306
           05      RAESTD-LARTICLE        PIC  X(0035).                 00162003
      * LIBELL� COMPL�MENTAIRE                                          00162106
           05      RAESTD-LCOMPL          PIC  X(0035).                 00163003
      * CODE RACINE                                                     00163106
           05      RAESTD-CRACINE         PIC  X(0018).                 00164003
      * TYPE D ARTICLE                                                  00164106
           05      RAESTD-TYPE-ARTICLE    PIC  9(0002).                 00165000
      * DISPONIBLE                                                      00165106
           05      RAESTD-DISPONIBLE      PIC  9(0001).                 00166000
      * VERTICAL                                                        00166106
           05      RAESTD-VERTICAL        PIC  9(0001).                 00167000
      * DANGEREUX                                                       00167106
           05      RAESTD-DANGEREUX       PIC  9(0001).                 00168000
      * A�ROSOL                                                         00168106
           05      RAESTD-AEROSOL         PIC  9(0001).                 00169000
      * FRAGILE                                                         00169106
           05      RAESTD-FRAGILE         PIC  9(0001).                 00169200
      * RELEV� LOT                                                      00169306
           05      RAESTD-REL-LOT         PIC  9(0001).                 00169400
      * RELEV� NUM�RO DE S�RIE                                          00169506
           05      RAESTD-REL-NSERIE      PIC  9(0001).                 00169603
      * RELEV� CARTON                                                   00169706
           05      RAESTD-REL-CARTON      PIC  9(0001).                 00169800
      * RELEV� DATE LIMITE DE VENTE                                     00169906
           05      RAESTD-REL-DLV         PIC  9(0001).                 00170000
      * RELEV� DATE LIMITE DE CONSOMMATION                              00170106
           05      RAESTD-REL-DLC         PIC  9(0001).                 00170200
      * RELEV� INFORMATION 1                                            00170306
           05      RAESTD-REL-INFO1       PIC  9(0001).                 00170400
      * RELEV� INFORMATION 2                                            00170506
           05      RAESTD-REL-INFO2       PIC  9(0001).                 00170600
      * RELEV� INFORMATION 3                                            00170706
           05      RAESTD-REL-INFO3       PIC  9(0001).                 00170800
      * ROTATION                                                        00170906
           05      RAESTD-ROTATION        PIC  X(0001).                 00171000
      * PRIX UNITAIRE MOYEN POND�R� (PUMP)                              00171106
           05      RAESTD-PUMP            PIC  9(0010)V9(5).            00171202
      * TYPE DE SORTIE                                                  00171306
           05      RAESTD-TYPE-SORTIE     PIC  9(0003).                 00171400
      * FEN�TRE TYPE DE SORTIE                                          00171506
           05      RAESTD-FEN-TYPE-SORTIE PIC  9(0005).                 00171600
      * POURCENTAGE D ALCOOL                                            00171706
           05      RAESTD-PER-ALCOOL      PIC  9(0003)V9(2).            00171802
      * VOLUME LIQUIDE                                                  00171906
           05      RAESTD-VOL-LIQUIDE     PIC  9(0010).                 00172000
      * CODE-�-BARRES                                                   00172106
           05      RAESTD-CODE-BARRES     PIC  X(0020).                 00172200
      * IDENTIFIANT FAMILLE DE COLISAGE                                 00172306
           05      RAESTD-ID-FAM-COLISAGE PIC  X(0003).                 00172400
      * IDENTIFIANT FAMILLE DE STOCKAGE                                 00172506
           05      RAESTD-ID-FAM-STOCKAGE PIC  X(0005).                 00172610
      * PREMI�RE R�CEPTION                                              00172706
           05      RAESTD-1ERE-RECEPTION  PIC  9(0001).                 00172800
      * CODE UNIT� LOGISTIQUE D�FAUT                                    00172906
           05      RAESTD-CODE-UL         PIC  X(0005).                 00173001
      * QT� TOL�RANCE INVENTAIRE                                        00173106
           05      RAESTD-QTE-TOL-INVENT  PIC  9(0003).                 00173200
      * D�LAI INTER-INVENTAIRE                                          00173306
           05      RAESTD-DELAI-INTER-INV PIC  9(0003).                 00173400
      * TAUX DE SERVICE                                                 00173506
           05      RAESTD-TX-SERVICE      PIC  9(0003).                 00173603
      * CARACT�RISTIQUE 1                                               00173706
           05      RAESTD-CARACT1         PIC  X(0020).                 00173800
      * CARACT�RISTIQUE 2                                               00173906
           05      RAESTD-CARACT2         PIC  X(0020).                 00174000
      * CARACT�RISTIQUE 3                                               00174106
           05      RAESTD-CARACT3         PIC  X(0020).                 00174200
      * COEFFICIENT UNIT�                                               00174306
           05      RAESTD-COEF-UNITE      PIC  9(0006)V9(9).            00174402
      * LIBELL� UNIT�                                                   00174506
           05      RAESTD-LUNITE          PIC  X(0010).                 00174603
      * FILLER                                                          00174707
           05      RAESTD-FILLER          PIC  X(0001).                 00175009
                                                                                
