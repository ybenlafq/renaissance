      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------*  00001000
      *                                                              *  00010000
      *                                                              *  00020000
      *                                                              *  00030000
      *                                                              *  00040000
      *--------------------------------------------------------------*  00060001
LA2111* 21/11/2012 EVOLUTIF     - L.ARMANT                           *  00060101
      *                  - RENDRE SAISISSABLE LE NUMERO DE PAGE      *  00060201
      *                  - DIVERS CORRECTIFS (SELECTION, AFFICHAGE)  *  00060301
      ****************************************************************  00060401
       01  Z-COMMAREA.                                                  00061001
                                                                        00062001
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00063001
           02 FILLER-PC30-AIDA      PIC X(100).                         00064001
                                                                        00065001
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00066001
           02 COMM-CICS-PC30-APPLID PIC X(08).                          00067001
           02 COMM-CICS-PC30-NETNAM PIC X(08).                          00068001
           02 COMM-CICS-PC30-TRANSA PIC X(04).                          00069001
                                                                        00069101
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00069201
           02 COMM-DATE-PC30-SIECLE PIC X(02).                          00069301
           02 COMM-DATE-PC30-ANNEE PIC X(02).                           00069401
           02 COMM-DATE-PC30-MOIS  PIC X(02).                           00069501
           02 COMM-DATE-PC30-JOUR  PIC 99.                              00069601
      *   QUANTIEMES CALENDAIRE ET STANDARD                             00069701
           02 COMM-DATE-PC30-QNTA  PIC 999.                             00069801
           02 COMM-DATE-PC30-QNT0  PIC 99999.                           00069901
      *   ANNEE BISSEXTILE 1=OUI 0=NON                                  00070001
           02 COMM-DATE-PC30-BISX  PIC 9.                               00071001
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00072001
           02 COMM-DATE-PC30-JSM   PIC 9.                               00073001
      *   LIBELLES DU JOUR COURT - LONG                                 00074001
           02 COMM-DATE-PC30-JSM-LC PIC X(03).                          00075001
           02 COMM-DATE-PC30-JSM-LL PIC X(08).                          00076001
      *   LIBELLES DU MOIS COURT - LONG                                 00077001
           02 COMM-DATE-PC30-MOIS-LC PIC X(03).                         00078001
           02 COMM-DATE-PC30-MOIS-LL PIC X(08).                         00079001
      *   DIFFERENTES FORMES DE DATE                                    00079101
           02 COMM-DATE-PC30-SSAAMMJJ PIC X(08).                        00079201
           02 COMM-DATE-PC30-AAMMJJ PIC X(06).                          00079301
           02 COMM-DATE-PC30-JJMMSSAA PIC X(08).                        00079401
           02 COMM-DATE-PC30-JJMMAA PIC X(06).                          00079501
           02 COMM-DATE-PC30-JJ-MM-AA PIC X(08).                        00079601
           02 COMM-DATE-PC30-JJ-MM-SSAA PIC X(10).                      00079701
      *   TRAITEMENT DU NUMERO DE SEMAINE                               00079801
           02 COMM-DATE-PC30-WEEK.                                      00079901
              05 COMM-DATE-PC30-SEMSS PIC 99.                           00080001
              05 COMM-DATE-PC30-SEMAA PIC 99.                           00081001
              05 COMM-DATE-PC30-SEMNU PIC 99.                           00082001
           02 COMM-DATE-PC30-FILLER PIC X(08).                          00083001
      *   ZONES RESERVEES TRAITEMENT DU SWAP                            00084001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 COMM-SWAP-PC30-CURS       PIC S9(4) COMP VALUE -1.        00085001
      *                                                                         
      *--                                                                       
           02 COMM-SWAP-PC30-CURS       PIC S9(4) COMP-5 VALUE -1.              
                                                                        00086001
      *}                                                                        
           02 COMM-PC30-APPLI.                                          00090001
              05 COMM-PC30-NSOCIETE          PIC X(03).                 00090101
              05 COMM-PC30-NLIEU             PIC X(03).                 00090201
              05 COMM-PC30-LEVEL-MAX         PIC X(06).                 00090301
              05 COMM-PC30-ACTION            PIC 9(01).                 00091001
                 88 COMM-PC30-NOSEARCH                  VALUE 0.        00092001
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-PC30-SEARCH                    VALUE 1 , 2.    00093001
      *--                                                                       
                 88 COMM-PC30-SEARCH                    VALUE 1   2.            
      *}                                                                        
                 88 COMM-PC30-SEARCH-DATE               VALUE 2.        00093101
      *{ remove-comma-in-dde 1.5                                                
      *          88 COMM-PC30-AFFICHE                   VALUE 1 , 2 , 3.00094001
      *--                                                                       
                 88 COMM-PC30-AFFICHE                   VALUE 1   2   3.        
      *}                                                                        
              05 COMM-DATA-FILTRE.                                      00100001
                 10 COMM-PC30-FNSOCIETE      PIC X(03).                 00110001
                 10 COMM-PC30-FNLIEU         PIC X(03).                 00111001
                 10 COMM-PC30-FDACTIVD       PIC X(08).                 00120001
                 10 COMM-PC30-FDACTIVF       PIC X(08).                 00121001
                 10 COMM-PC30-FNRANDOMNUM    PIC X(16).                 00130001
                 10 COMM-PC30-FLNOM          PIC X(25).                 00140001
                 10 COMM-PC30-FNSOCR         PIC X(03).                 00140101
                 10 COMM-PC30-FNLIEUR        PIC X(03).                 00140201
                 10 COMM-PC30-FNVENTER       PIC X(07).                 00140301
              05 COMM-PC30-IKEY              PIC 9(03).                 00141001
              05 COMM-PC30-KEY-FETCH.                                   00150001
                 11 COMM-PC30-LAST-KEY OCCURS 100.                      00151001
                    15 COMM-PC30-LKNSOCIETE         PIC X(03).          00160001
                    15 COMM-PC30-LKNLIEU            PIC X(03).          00161001
                    15 COMM-PC30-LKDACTIV           PIC X(08).          00170001
                    15 COMM-PC30-LKNRANDOMNUM       PIC X(16).          00180001
                    15 COMM-PC30-LKLNOM             PIC X(25).          00190001
                    15 COMM-PC30-LNSOCR             PIC X(03).          00190101
                    15 COMM-PC30-LNLIEUR            PIC X(03).          00190201
                    15 COMM-PC30-LNVENTER           PIC X(07).          00190301
              05 COMM-PC30-NEXT-KEY.                                    00190401
                    15 COMM-PC30-NKNSOCIETE         PIC X(03).          00191001
                    15 COMM-PC30-NKNLIEU            PIC X(03).          00192001
                    15 COMM-PC30-NKDACTIV           PIC X(08).          00193001
                    15 COMM-PC30-NKNRANDOMNUM       PIC X(16).          00194001
                    15 COMM-PC30-NKLNOM             PIC X(25).          00195001
                    15 COMM-PC30-NKNSOCR             PIC X(03).         00196001
                    15 COMM-PC30-NKNLIEUR            PIC X(03).         00197001
                    15 COMM-PC30-NKNVENTER           PIC X(07).         00198001
              05 COMM-DATA-PAGINATION-H.                                00200001
LA2111*          10 COMM-PC30-PAG-H        PIC S9(02) COMP-3.           00210001
LA2111           10 COMM-PC30-PAG-H        PIC S9(03) COMP-3.           00211001
                 10 COMM-PC30-MAXPAGE-H    PIC S9(02) COMP-3 VALUE 6.   00220001
                 10 COMM-PC30-ITS          PIC S9(03) COMP-3.           00230001
                 10 COMM-PC30-ITS-MAX      PIC S9(03) COMP-3 VALUE 90.  00231001
                 10 COMM-PC30-SITS         PIC S9(03) COMP-3.           00232001
              05 COMM-DATA-PAGINATION-V.                                00240001
LA2111*          10 COMM-PC30-PAG-V        PIC S9(02) COMP-3.           00250001
LA2111           10 COMM-PC30-PAG-V        PIC S9(03) COMP-3.           00251001
                 10 COMM-PC30-MAXPAGE-V    PIC S9(02) COMP-3 VALUE 6.   00260001
              05 COMM-DATA-SELECTION       PIC S9(01) COMP-3 VALUE 0.   00270001
                 88 COMM-PC30-SEL-NON                VALUE 0.           00290001
                 88 COMM-PC30-SEL-OUI                VALUE 1.           00300001
           02 COMM-PC31-APPLI.                                          00310001
              05 COMM-DATA.                                             00320001
                 10 COMM-PC31-PAG-CARTES   PIC S9(02) COMP-3.           00330001
                 10 COMM-PC31-PAG-VENTES   PIC S9(02) COMP-3.           00340001
                                                                                
