      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * CETTE COPY EST EN FAIT UN PROGRAMME ENTIER; ELLE EST UTILISEE           
      * PAR TOUS LES PROGRAMMES MEC35X, OU X EST LE SUFFIXE FILIALE             
      * CE PROGRAMME AJOUTE DES Prestation de liv POST ACHAT                    
      ******************************************************************        
      * Variables programmes                                                    
      **********************                                                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  LONGUEUR-COMMAREA        PIC S9(4) COMP  VALUE +200.                 
      *--                                                                       
       77  LONGUEUR-COMMAREA        PIC S9(4) COMP-5  VALUE +200.               
      *}                                                                        
       77  WS-LONG-MESSAGE          PIC S9(03) VALUE +134.                      
      * VARIABLE-ALPHA.                                                         
      **********************                                                    
      *==> ZONES POUR messages db2                                              
       01  MSG-SQL.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSG-SQL-LENGTH       PIC S9(4) COMP VALUE +240.                   
      *--                                                                       
           02 MSG-SQL-LENGTH       PIC S9(4) COMP-5 VALUE +240.                 
      *}                                                                        
           02 MSG-SQL-MSG          PIC X(80) OCCURS 3.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  MSG-SQL-LRECL           PIC S9(8) COMP VALUE +80.                    
      *--                                                                       
       01  MSG-SQL-LRECL           PIC S9(8) COMP-5 VALUE +80.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-SQL                   PIC S9(4) BINARY.                            
      *--                                                                       
       01  I-SQL                   PIC S9(4) COMP-5.                            
      *}                                                                        
       01  DSNTIAR                 PIC X(8)  VALUE 'DSNTIAR'.                   
       01 W-EIBTIME                PIC 9(7).                                    
       01 FILLER REDEFINES W-EIBTIME.                                           
          02 FILLER                PIC X.                                       
          02 W-HEURE               PIC XX.                                      
          02 W-MINUTE              PIC XX.                                      
          02 W-SECONDE             PIC XX.                                      
       01 W-CC-NSOC-ECOM           PIC X(003).                                  
       01 W-CC-NLIEU-ECOM          PIC X(003).                                  
       01 W-MESSAGE-S.                                                          
          02 FILLER                PIC X(5) VALUE 'EC35'.                       
          02 W-MESSAGE-H           PIC X(2).                                    
          02 FILLER                PIC X(1) VALUE ':'.                          
          02 W-MESSAGE-M           PIC X(2).                                    
          02 FILLER                PIC X(1) VALUE ' '.                          
          02 W-MESSAGES            PIC X(69).                                   
       01 WS-DATE-DU-JOUR          PIC X(08).                                   
       01 NCDEWC-X                 PIC X(15).                                   
       01 NSEQNQ-X                 PIC X(05).                                   
       01 W-CVENDEUR               PIC X(06).                                   
       01  PIC9-NCDEWC             PIC Z(14)9.                                  
       01  PICX-NCDEWC   REDEFINES PIC9-NCDEWC PIC X(15).                       
      *   VARIABLE NUMERIQUE                                                    
      **********************                                                    
       01 W-MAX-NSEQ               PIC  X(02) VALUE '00'.                       
       01 W-MAX-NSEQ-9 REDEFINES W-MAX-NSEQ   PIC  9(02).                       
       01 W-MAX-NSEQENS            PIC S9(05) COMP-3 value 0.                   
       01 W-MAX-NLIGNE             PIC  X(02) VALUE  '00'.                      
       01 W-MAX-NLIG-9 REDEFINES W-MAX-NLIGNE PIC  9(02).                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 W-SPDATDB2-RC            PIC S9(4)  COMP.                             
      *--                                                                       
       01 W-SPDATDB2-RC            PIC S9(4) COMP-5.                            
      *}                                                                        
       01 W-SPDATDB2-DSYST         PIC S9(13) COMP-3 VALUE 0.                   
       01 EDIT-SQL                 PIC -ZZ999.                                  
      **********************                                                    
      *   VARIABLE-CONDITION.                                                   
      **********************                                                    
      ******************************************************************        
      * ...DATE ET HEURE...                                                     
      ******************************************************************        
           COPY COMMDATC.                                                       
      * ...POUR MBS43 (DUPLI DE VENTE)...                                       
           COPY COMMBS43.                                                       
           COPY COMMGV51.                                                       
      * ... Zones de travail utiles pour la gestion de TSTENVOI                 
           COPY WKTENVOI.                                                       
      * ... POUR ECRIRE LA QUEUE POUR LA DUPLI DE VENTE (MBS70) - DEA           
           COPY COMMMQ21.                                                       
           COPY COMMMQ20.                                                       
      *   COPY MESSAGES MQ INTERROGATION STATUT VENTE LOCAL                     
           10  WS-MESSAGE REDEFINES COMM-MQ20-MESSAGE.                          
            15 MES-ENTETE.                                                      
               20   MES-TYPE      PIC    X(3).                                  
               20   MES-NSOCMSG   PIC    X(3).                                  
               20   MES-NLIEUMSG  PIC    X(3).                                  
               20   MES-NSOCDST   PIC    X(3).                                  
               20   MES-NLIEUDST  PIC    X(3).                                  
               20   MES-NORD      PIC    9(8).                                  
               20   MES-LPROG     PIC    X(10).                                 
               20   MES-DJOUR     PIC    X(8).                                  
               20   MES-WSID      PIC    X(10).                                 
               20   MES-USER      PIC    X(10).                                 
               20   MES-CHRONO    PIC    9(7).                                  
               20   MES-NBRMSG    PIC    9(7).                                  
               20   MES-FILLER    PIC    X(30).                                 
      *  DATA ENTETE : 1 OCCURENCE = 22 CAR                                     
            15 MESSAGE45-DATA.                                                  
      *  SOCIETE ET LIEU DE DEMANDE DE LA VENTE                                 
              25  MES60-NSOCD         PIC    X(3).                              
              25  MES60-NLIEUD        PIC    X(3).                              
      *  SOCIETE ET LIEU DE LA VENTE                                            
              25  MES60-NSOCV         PIC    X(3).                              
              25  MES60-NLIEUV        PIC    X(3).                              
              25  MES60-NVENTE        PIC    X(7).                              
              25  MES60-TYPVTE        PIC    X(1).                              
      *  SOCIETE ET LIEU DE PAIEMENT DE LA VENTE                                
              25  MES60-NSOCP         PIC    X(3).                              
              25  MES60-NLIEUP        PIC    X(3).                              
      *  TOP ACCUSE RECEPTION O/N                                               
              25  MES60-ACCUSE        PIC    X(1).                              
      *  ZONE CODE RETOUR = OK OU KO                                            
              25  MES60-CRET          PIC    X(2).                              
      * ... DESCRIPTION DU MESSAGE DEA ENVOYE VERS LE 400 DE PAIEMENT.          
           COPY MESBS70.                                                        
      * ... POUR MEC15 (LOG DES COMMANDES)...                                   
       01  MEC15C-COMMAREA.                                                     
           COPY MEC15C.                                                         
           COPY MEC15E.                                                         
      * ... POUR MEC00 (ENVOI A DARTY.COM)...                                   
           COPY MEC0MC.                                                         
      *                                                                         
           COPY COMMNV45.                                                       
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           COPY COMMEC35.                                                       
      *=================================================================        
       PROCEDURE DIVISION.                                                      
      *=================================================================        
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL) END-EXEC.                 
      *--                                                                       
           EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL)                           
           END-EXEC.                                                            
      *}                                                                        
           PERFORM MODULE-ENTREE.                                               
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *=================================================================        
      * INITIALISATION  DES DONNEES (DATES, ...)                                
      ******************************************************************        
       MODULE-ENTREE SECTION.                                                   
           PERFORM CALL-SPDATDB2.                                               
           MOVE '4'          TO GFDATA.                                         
           PERFORM FAIS-UN-LINK-A-TETDATC.                                      
           MOVE GFSAMJ-0 TO WS-DATE-DU-JOUR.                                    
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-MESSAGE-H.                             
           MOVE W-MINUTE            TO W-MESSAGE-M.                             
           PERFORM RECH-MAG-INTERNET.                                           
           MOVE '0' TO W-MEC35-CODE-RET                                         
           MOVE 'MEC35' TO NCGFC-CTABLEG2.                                      
           INITIALIZE MESSAGE70.                                                
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                         
      *                                                                         
      *=================================================================        
      *==>  Traitement: Creation de la vente                                    
       MODULE-TRAITEMENT SECTION.                                               
           PERFORM RECHERCHE-VENTE.                                             
           IF SQLCODE = 0                                                       
              PERFORM SELECT-MAX-NSEQ                                           
              PERFORM SELECT-MAX-NSEQENS                                        
              PERFORM SELECT-MAX-NLIGNE                                         
              PERFORM SELECT-CODE-VENDEUR                                       
           END-IF                                                               
           IF W-MEC35-CODE-RET  = 0                                             
              PERFORM MAJ-TABLES-VENTE                                          
           END-IF                                                               
           IF W-MEC35-CODE-RET  = 0                                             
              PERFORM CREATION-RTEC32                                           
           END-IF                                                               
           IF W-MEC35-CODE-RET = 0                                              
              PERFORM CREATION-DOSSIER                                          
           END-IF                                                               
           IF W-MEC35-CODE-RET  = 0                                             
              PERFORM ENVOI-DUP-NEM                                             
           END-IF                                                               
           IF W-MEC35-CODE-RET  = 0                                             
              PERFORM ENVOI-DUP-NMD                                             
           END-IF                                                               
           IF W-MEC35-CODE-RET  = 0                                             
              PERFORM ENVOI-MESSAGE-DEA                                         
           END-IF                                                               
           IF W-MEC35-CODE-RET  = 0                                             
              PERFORM ACCUSE-RECEPTION                                          
           END-IF.                                                              
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *                                                                         
       MODULE-SORTIE SECTION.                                                   
           IF W-MEC35-CODE-RET ='9'                                             
              MOVE W-MESSAGES   TO  W-MEC35-MES-RET                             
           ELSE                                                                 
              STRING 'Sortie ec35 ' GV11-NVENTE                                 
                     ' ' W-MEC35-CODE-RET                                       
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
           END-IF.                                                              
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN END-EXEC.                                           
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
      *-----------------------------------------------------------------        
      * LECTURE DU "MAGASIN INTERNET" PAR DEFAUT.                               
      * V�RIFICATION EXISTENCE DE LA TS ET CR�ATION LE CAS �CH�ANT              
       RECH-MAG-INTERNET              SECTION.                                  
           MOVE 'MEC35     '   TO WS-TSTENVOI-NOMPGM                            
      * RECHERCHE DES TYPES D'ENVOI � TRAITER                                   
           MOVE  LOW-VALUES TO   WS-TYPENVOI                                    
           MOVE  '0' TO WF-TSTENVOI                                             
           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
                   UNTIL  WC-TSTENVOI-FIN                                       
                   OR W-CC-NSOC-ECOM > ' '                                      
              PERFORM LECTURE-TSTENVOI                                          
              IF  WC-TSTENVOI-SUITE                                             
              AND TSTENVOI-NSOCZP    = '00000'                                  
              AND TSTENVOI-CTRL       = 'ECOM'                                  
                 MOVE TSTENVOI-WPARAM(1:3) TO W-CC-NSOC-ECOM                    
                 MOVE TSTENVOI-WPARAM(4:3) TO W-CC-NLIEU-ECOM                   
              END-IF                                                            
           END-PERFORM.                                                         
       FIN-RECH-MAG-INTERNET. EXIT.                                             
       ENVOI-DUP-NEM    SECTION.                                                
      * POUR TRAITER LES DEMANDES DES FILIALES, ON NE PEUX PAS APPELER          
      * DIRECTEMENT LE MBS43, ON PASSE DONC PAR UN MASSAGE MQ ENVOY�            
      * AU MBS60 QUI CE CHARGERA D'APPELER LE MBS43 EN LOCAL                    
           INITIALIZE WS-MESSAGE                                                
                      COMM-MQ20-MESSAGE.                                        
           MOVE 'IV1'               TO       MES-TYPE.                          
           MOVE W-CC-NSOC-ECOM      TO       MES-NSOCMSG                        
           MOVE W-CC-NLIEU-ECOM     TO       MES-NLIEUMSG                       
           MOVE GV10-NSOCIETE       TO       MES-NSOCDST                        
           MOVE '000'               TO       MES-NLIEUDST                       
           MOVE SPACES              TO       MES-WSID                           
                                             MES-USER                           
           MOVE 'TEC02'             TO       MES-LPROG                          
           MOVE ZEROES              TO       MES-NORD                           
           MOVE 1                   TO       MES-CHRONO                         
                                             MES-NBRMSG                         
           MOVE SPACES              TO       MES-FILLER                         
      *    partie data                                                          
           MOVE W-CC-NSOC-ECOM      TO      MES60-NSOCD.                        
           MOVE W-CC-NLIEU-ECOM     TO      MES60-NLIEUD                        
           MOVE GV10-NSOCIETE       TO      MES60-NSOCV.                        
           MOVE GV10-NLIEU          TO      MES60-NLIEUV                        
           MOVE GV10-NVENTE         TO      MES60-NVENTE                        
           MOVE '4'                 TO      MES60-TYPVTE                        
           MOVE W-CC-NSOC-ECOM      TO      MES60-NSOCP.                        
           MOVE W-CC-NLIEU-ECOM     TO      MES60-NLIEUP                        
           MOVE 'N'                 TO      MES60-ACCUSE                        
           MOVE 'OK'                TO      MES60-CRET                          
           PERFORM ENVOI-DUPLI.                                                 
       FIN-ENVOI-DUP-NEM.  EXIT.                                                
      *                                                                         
       ENVOI-DUPLI                    SECTION.                                  
      *---------------------------------------                                  
      *   ENVOI DANS LE MAG OU ON REPREND LA VENTE                              
           MOVE  GV10-NSOCIETE       TO      COMM-MQ20-NSOC                     
           MOVE  '000'               TO      COMM-MQ20-NLIEU                    
           MOVE  'IV1'               TO      COMM-MQ20-FONCTION                 
           MOVE  'TEC02'             TO      COMM-MQ20-NOMPROG                  
           MOVE  WS-LONG-MESSAGE     TO      COMM-MQ20-LONGMES                  
           MOVE  '00'                TO      COMM-MQ20-CODRET.                  
           MOVE  SPACES              TO      COMM-MQ20-CORRELID                 
           EXEC CICS LINK  PROGRAM  ('MMQ20')                                   
                           COMMAREA (COMM-MQ20-APPLI)                           
                           LENGTH   (LENGTH OF COMM-MQ20-APPLI)                 
                           NOHANDLE                                             
           END-EXEC.                                                            
           MOVE EIBRCODE TO EIB-RCODE.                                          
           IF  NOT EIB-NORMAL                                                   
               STRING 'MEC35 DUP  MODULE MMQ20 MAL TERMINE '                    
               DELIMITED BY SIZE INTO W-MESSAGES                                
               PERFORM DISPLAY-W-MESSAGE                                        
      * code retour 1: message non bloquant (pas de rollback)                   
               MOVE '0' TO W-MEC35-CODE-RET                                     
           END-IF.                                                              
       FIN-ENVOI-DUPLI. EXIT.                                                   
      *                                                                         
      *                                                                         
       ENVOI-DUP-NMD    SECTION.                                                
           MOVE GV10-NSOCIETE       TO COMM-MGV51-NSOCIETE                      
           MOVE GV10-NLIEU          TO COMM-MGV51-NLIEU                         
           MOVE GV10-NVENTE         TO COMM-MGV51-NVENTE                        
           MOVE WS-DATE-DU-JOUR     TO COMM-MGV51-DJOUR                         
           MOVE SPACES              TO COMM-MGV51-CCEVENT                       
           MOVE LENGTH OF COMM-MGV51-APPLI TO LONGUEUR-COMMAREA                 
             EXEC CICS LINK   PROGRAM   ('MGV51')                               
                            COMMAREA  (COMM-MGV51-APPLI)                        
                            LENGTH    (LONGUEUR-COMMAREA)                       
                            NOHANDLE                                            
             END-EXEC                                                           
           MOVE EIBRCODE TO EIB-RCODE                                           
           IF NOT EIB-NORMAL                                                    
              STRING 'MEC35 LINK MODULE MGV51 MAL TERMINE '                     
              DELIMITED BY SIZE INTO W-MESSAGEs                                 
              PERFORM DISPLAY-W-MESSAGE                                         
      * code retour 1: message non bloquant (pas de rollback)                   
              MOVE '0' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-ENVOI-DUP-NMD.  EXIT.                                                
      *                                                                         
       ENVOI-MESSAGE-DEA SECTION.                                               
      * -> ZONES MESSAGE.                                                       
           MOVE 'DEA'              TO MES70-TYPE                                
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCMSG                             
           MOVE '000'              TO MES70-NLIEUMSG                            
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCDST                             
           MOVE W-CC-NLIEU-ECOM    TO MES70-NLIEUDST                            
           MOVE ZERO               TO MES70-NORD                                
           MOVE 'MEC35'            TO MES70-LPROG                               
           MOVE WS-DATE-DU-JOUR    TO MES70-DJOUR                               
           MOVE SPACES             TO MES70-WSID                                
           MOVE SPACES             TO MES70-USER                                
           MOVE 1                  TO MES70-CHRONO                              
           MOVE 1                  TO MES70-NBRMSG                              
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCD                               
           MOVE W-CC-NLIEU-ECOM    TO MES70-NLIEUD                              
           MOVE 1                  TO MES70-NBVTES                              
           MOVE 1                  TO MES70-NBMODP                              
           MOVE W-MEC35-PX-VT      TO MES70-PCOMPT                              
           MOVE 'EUR'              TO MES70-CDEVISE                             
           MOVE GV10-NSOCIETE      TO MES70-NSOCV  (1)                          
           MOVE GV10-NLIEU         TO MES70-NLIEUV (1)                          
           MOVE GV10-NVENTE        TO MES70-NVENTE (1)                          
           MOVE GV10-TYPVTE        TO MES70-TYPVTE (1)                          
           MOVE GV10-DVENTE        TO MES70-DVENTE (1)                          
           MOVE W-CC-NSOC-ECOM     TO MES70-NSOCP                               
           MOVE W-CC-NLIEU-ECOM    TO MES70-NLIEUP                              
           MOVE W-MEC35-MD-PMENT   TO EC008-CTABLEG2                            
           PERFORM SELECT-EC008                                                 
           MOVE EC008-MOPAISI      TO MES70-CMOPAI(1)                           
           MOVE MES70-PCOMPT       TO MES70-PMOPAI (1)                          
           MOVE 'EUR'              TO MES70-CDEV    (1)                         
           MOVE SPACES             TO MES70-DESC    (1)                         
           MOVE 'O'                TO MES70-DUP-OK                              
      * POUR INDIQUER ENCAISST P.A. ET DONC LAISSER PASSER                      
           MOVE 'P'                TO MES70-ENC-OK                              
      * -> ZONES MQ.                                                            
           MOVE SPACES             TO COMM-MQ21-MSGID.                          
           MOVE SPACES             TO COMM-MQ21-CORRELID.                       
           MOVE SPACES             TO COMM-MQ21-CODRET.                         
           MOVE SPACES             TO COMM-MQ21-ERREUR.                         
           MOVE 'DEA'              TO COMM-MQ21-FONCTION.                       
           MOVE 'MEC35'            TO COMM-MQ21-NOMPROG.                        
           MOVE 'N'                TO COMM-MQ21-SYNCPOINT.                      
           MOVE 7                  TO COMM-MQ21-PRIORITE.                       
           MOVE W-CC-NSOC-ECOM     TO COMM-MQ21-NSOC.                           
           MOVE W-CC-NLIEU-ECOM    TO COMM-MQ21-NLIEU.                          
           MOVE 650                TO COMM-MQ21-LONGMES.                        
           MOVE MESSAGE70          TO COMM-MQ21-MESSAGE.                        
           COMPUTE LONGUEUR-COMMAREA  = COMM-MQ21-LONGMES + 124.                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         LINK PROGRAM ('MMQ21') COMMAREA (COMM-MQ21-APPLI)               
      *         LENGTH(LONGUEUR-COMMAREA)                                       
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS LINK PROGRAM ('MMQ21') COMMAREA (COMM-MQ21-APPLI)          
                LENGTH(LONGUEUR-COMMAREA)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBRCODE TO EIB-RCODE                                           
           IF NOT EIB-NORMAL                                                    
               STRING 'MEC35 DEA LINK MODULE MMQ21 MAL TERMINE '                
               DELIMITED BY SIZE INTO W-MESSAGES                                
               PERFORM DISPLAY-W-MESSAGE                                        
               MOVE '9' TO W-MEC35-CODE-RET                                     
           ELSE                                                                 
               STRING 'DEA ENVOYE POUR VENTE ' GV10-NVENTE                      
               DELIMITED BY SIZE INTO W-MESSAGES                                
               PERFORM DISPLAY-W-MESSAGE                                        
           END-IF.                                                              
       F-ENVOI-MESSAGE-DEA. EXIT.                                               
      *                                                                         
       ACCUSE-RECEPTION SECTION.                                                
           INITIALIZE              MEC00C-COMMAREA                              
           MOVE 'CMD-MOD3'      TO MEC00C-FICHIER-SI                            
           MOVE 'D'             TO MEC00C-CODE-ACTION                           
           MOVE W-MEC35-ENTETE  TO MEC00C-DATA-SI                               
           MOVE LENGTH OF W-MEC35-ENTETE TO MEC00C-DATA-SI-L                    
           EXEC CICS LINK PROGRAM ('MEC00P') COMMAREA (MEC00C-COMMAREA)         
           END-EXEC                                                             
           MOVE '00'            TO W-MEC35-CRET                                 
           MOVE 'CMD-MOD4'      TO MEC00C-FICHIER-SI                            
           MOVE 'T'             TO MEC00C-CODE-ACTION                           
           MOVE W-MEC35-DETAIL  TO MEC00C-DATA-SI                               
           MOVE LENGTH OF W-MEC35-DETAIL TO MEC00C-DATA-SI-L                    
           EXEC CICS LINK PROGRAM ('MEC00P') COMMAREA (MEC00C-COMMAREA)         
           END-EXEC                                                             
           IF MEC00C-CODE-RETOUR NOT = '0'                                      
              MOVE SPACES           TO W-MESSAGES                               
              STRING 'MEC35-REP-T:'                                             
                     ' MEC00 CDRET='  MEC00C-CODE-RETOUR                        
                     ' SQL='          MEC00C-CODE-DB2-DISPLAY                   
                     ' MESS='         MEC00C-MESSAGE                            
                DELIMITED BY SIZE INTO W-MESSAGES                               
              END-STRING                                                        
              MOVE '9'              TO W-MEC35-CODE-RET                         
              PERFORM DISPLAY-W-MESSAGE                                         
           END-IF.                                                              
           MOVE 'CMD-MOD3'      TO MEC00C-FICHIER-SI                            
           MOVE 'F'             TO MEC00C-CODE-ACTION                           
           MOVE SPACE           TO MEC00C-DATA-SI                               
           MOVE LENGTH OF W-MEC35-ENTETE TO MEC00C-DATA-SI-L                    
           EXEC CICS LINK PROGRAM ('MEC00P') COMMAREA (MEC00C-COMMAREA)         
           END-EXEC                                                             
           IF MEC00C-CODE-RETOUR NOT = '0'                                      
              MOVE SPACES           TO W-MESSAGES                               
              STRING 'MEC35-REP-F:'                                             
                     ' MEC00 CDRET='  MEC00C-CODE-RETOUR                        
                     ' SQL='          MEC00C-CODE-DB2-DISPLAY                   
                     ' MESS='         MEC00C-MESSAGE                            
                DELIMITED BY SIZE     INTO W-MESSAGES                           
              END-STRING                                                        
              MOVE '9'                  TO W-MEC35-CODE-RET                     
              PERFORM DISPLAY-W-MESSAGE                                         
           END-IF.                                                              
           SET MEC00C-FIN-TRAITEMENT TO TRUE                                    
           EXEC CICS LINK PROGRAM ('MEC00P') COMMAREA (MEC00C-COMMAREA)         
           END-EXEC.                                                            
           IF MEC00C-CODE-RETOUR NOT = '0'                                      
              MOVE SPACES           TO W-MESSAGES                               
              STRING 'MEC35-REP-FF:'                                            
                     ' MEC00 CDRET='  MEC00C-CODE-RETOUR                        
                     ' SQL='          MEC00C-CODE-DB2-DISPLAY                   
                     ' MESS='         MEC00C-MESSAGE                            
                DELIMITED BY SIZE   INTO W-MESSAGES                             
              END-STRING                                                        
              MOVE '9'                TO W-MEC35-CODE-RET                       
              PERFORM DISPLAY-W-MESSAGE                                         
           END-IF.                                                              
           PERFORM LOG-LE-MESSAGE.                                              
      *                                                                         
       F-ACCUSE-RECEPTION. EXIT.                                                
      * CREATION D'UN MOUCHARD MESSAGE EN SORTIE.                               
       LOG-LE-MESSAGE SECTION.                                                  
           IF MEC00C-DATA-XML-L > 4034                                          
              MOVE 4034 TO EC05-XMLDATA-LEN                                     
           ELSE                                                                 
              MOVE MEC00C-DATA-XML-L TO EC05-XMLDATA-LEN                        
           END-IF.                                                              
           MOVE MEC00C-DATA-XML TO EC05-XMLDATA-TEXT.                           
           EXEC SQL INSERT INTO RVEC0500 (TIMESTP, WRECENV, XMLDATA)            
                         VALUES (CURRENT TIMESTAMP, 'E', :EC05-XMLDATA)         
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE 'ERREUR INSERT RTEC05:' TO W-MESSAGES                        
              PERFORM DISPLAY-W-MESSAGE                                         
              perform ERREUR-SQL                                                
              MOVE '9'                     TO W-MEC35-CODE-RET                  
           END-IF.                                                              
       F-LOG-LE-MESSAGE. EXIT.                                                  
       ERREUR-SQL SECTION.                                                      
           PERFORM DISPLAY-W-MESSAGE.                                           
           CALL DSNTIAR USING SQLCA, MSG-SQL, MSG-SQL-LRECL.                    
           PERFORM VARYING I-SQL FROM 1 BY 1 UNTIL I-SQL > 3                    
              MOVE MSG-SQL-MSG (I-SQL) TO W-MESSAGE W-MESSAGES                  
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9'                 TO W-MEC35-CODE-RET                      
           END-PERFORM.                                                         
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ABEND ABCODE ('TECS') END-EXEC.                            
      *--                                                                       
           EXEC CICS ABEND ABCODE ('TECS')                                      
           END-EXEC.                                                            
      *}                                                                        
       RECHERCHE-VENTE SECTION.                                                 
      * recherche de la vente dans le courant                                   
           MOVE W-MEC35-NSOC   TO GV10-nsociete                                 
           MOVE W-MEC35-NLIEU  TO GV10-nlieu                                    
           MOVE W-MEC35-NVENTE TO GV10-nvente                                   
           exec sql select nvente ,                                             
                           nordre ,                                             
                           pttvente ,                                           
                           pcompt ,                                             
                           nseqnq ,                                             
                           typvte ,                                             
                           dvente ,                                             
                           cacid                                                
                      into :gv10-nvente,                                        
                           :gv10-nordre,                                        
                           :gv10-pttvente ,                                     
                           :gv10-pcompt  ,                                      
                           :gv10-nseqnq  ,                                      
                           :GV10-TYPVTE  ,                                      
                           :GV10-DVENTE  ,                                      
                           :gv10-cacid                                          
                      from rvgv1005                                             
                     where nsociete = :gv10-nsociete                            
                       and nlieu    = :gv10-nlieu                               
                       and nvente   = :gv10-nvente                              
           end-exec.                                                            
           IF SQLCODE NOT = 0                                                   
              STRING 'Vente non trouv�e:'                                       
                  GV10-NSOCIETE , GV10-NLIEU , GV10-NVENTE                      
                DELIMITED BY SIZE INTO W-MESSAGES                               
              MOVE '9'              TO W-MEC35-CODE-RET                         
              PERFORM DISPLAY-LOG                                               
              PERFORM MODULE-SORTIE                                             
           END-IF.                                                              
       FIN-RECHERCHE-VENTE. EXIT.                                               
       MAJ-TABLES-VENTE      SECTION.                                           
           INITIALIZE                    RVGV1106                               
           MOVE GV10-NSOCIETE         TO GV11-NSOCIETE                          
           MOVE GV10-NLIEU            TO GV11-NLIEU                             
           MOVE GV10-NVENTE           TO GV11-NVENTE                            
           COMPUTE GV11-NSEQNQ  = GV10-NSEQNQ  + 1                              
           COMPUTE GV11-NSEQENS = W-MAX-NSEQENS + 1                             
           ADD     1                  TO W-MAX-NSEQ-9                           
           MOVE W-MAX-NSEQ-9          TO GV11-NSEQ                              
           ADD     1                  TO W-MAX-NLIG-9                           
           MOVE W-MAX-NLIG-9          TO GV11-NLIGNE                            
           MOVE GV11-NSEQNQ           TO GV11-NSEQREF                           
           MOVE '4'                   TO GV11-CTYPENREG                         
           MOVE W-MEC35-NCDEWC        TO PIC9-NCDEWC                            
           MOVE PICX-NCDEWC(8:)       TO GV11-CTOURNEE                          
           MOVE SPACE                 TO GV11-LCOMMENT                          
           MOVE WS-DATE-DU-JOUR       TO GV11-DCREATION                         
           STRING W-HEURE W-MINUTE DELIMITED BY SIZE                            
                                    INTO GV11-HCREATION                         
           MOVE W-SPDATDB2-DSYST      TO GV11-DSYST                             
           MOVE 'LD2'                 TO GV11-CMODDEL                           
           MOVE W-MEC35-DT-LIV        TO GV11-DDELIV                            
           MOVE GV10-NORDRE           TO GV11-NORDRE                            
           MOVE 1                     TO GV11-QVENDUE                           
           MOVE W-CVENDEUR            TO GV11-CVENDEUR                          
           MOVE 0                     TO GV11-QCONDT                            
           MOVE W-CC-NSOC-ECOM        TO GV11-NSOCP                             
           MOVE W-CC-NLIEU-ECOM       TO GV11-NLIEUP    GV11-NLIEUMODIF         
           MOVE 0                     TO GV11-NTRANS                            
           MOVE 'SE'                  TO GV11-CTYPENT                           
           MOVE 'N'                   TO GV11-WTOPELIVRE                        
           MOVE  W-CC-NLIEU-ECOM      TO GV11-NDEPOT                            
           MOVE  W-MEC35-CENREG       TO GV11-CENREG                            
           MOVE  W-MEC35-PX-VT        TO GV11-PVUNIT                            
                       GV11-PVUNITF GV11-PVTOTAL                                
           PERFORM INSERT-RTGV11                                                
           IF W-MEC35-CODE-RET = 0                                              
              PERFORM UPDATE-RTGV10                                             
           END-IF                                                               
           .                                                                    
       FIN-MAJ-TABLES-VENTE. EXIT.                                              
      *                                                                         
       CREATION-DOSSIER     SECTION.                                            
           INITIALIZE  NV45-DONNEES                                             
           MOVE 'CFIN'                 TO NV45-CFIN                             
           MOVE '00'                   TO NV45-CODRET                           
           MOVE 1                      TO IND-NV45                              
           MOVE 'C'                    TO NV45-CODACT (IND-NV45)                
           MOVE ' '                    TO NV45-NCODICGRP (IND-NV45)             
           MOVE ' '                    TO NV45-NCODIC    (IND-NV45)             
           MOVE W-MEC35-CENREG         TO NV45-CENREG    (IND-NV45)             
           MOVE 'N'                    TO NV45-WTOPLIVRE (IND-NV45)             
           MOVE 'SE'                   TO NV45-CTYPENT   (IND-NV45)             
      * chargement des �lements de la balise service                            
           MOVE GV10-NSOCIETE          TO NV45-NSOCIETE                         
           MOVE GV10-NLIEU             TO NV45-NLIEU                            
           MOVE GV10-NVENTE            TO NV45-NVENTE                           
           MOVE GV10-CACID (1:2)       TO NV45-CAGENT                           
           MOVE IND-NV45               TO TAB-NV45-MAX                          
           MOVE LENGTH OF Z-COMMAREA-MNV45 TO LONGUEUR-COMMAREA                 
           EXEC CICS LINK PROGRAM  ('MNV45')                                    
                           COMMAREA (Z-COMMAREA-MNV45)                          
                            LENGTH    (LONGUEUR-COMMAREA)                       
                            NOHANDLE                                            
           END-EXEC                                                             
           IF NV45-CODRET NOT = '00'                                            
              MOVE NV45-LIBRET         TO W-MESSAGES                            
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9'                 TO W-MEC35-CODE-RET                      
           END-IF                                                               
           IF  NV45-CODRET = '00'                                               
           AND NV45-VALCTRL > ' '                                               
              PERFORM UPDATE-RTGV11                                             
           END-IF.                                                              
       FIN-CREATION-DOSSIER. EXIT.                                              
      ******************************************************************        
      *                                                                         
      ******************************************************************        
      * CREATION LIEN ENTRE COMMANDE MERE ET COMMANDE FILLE                     
      ******************************************************************        
       CREATION-RTEC32      SECTION.                                            
      *---------------------------                                              
           INITIALIZE RVEC3200.                                                 
           perform select-rtec02                                                
           if sqlcode = +100                                                    
              MOVE W-MEC35-NCDEWC    TO EC32-NCDEWC                             
           else                                                                 
              MOVE EC02-NCDEWC       TO EC32-NCDEWC                             
           end-if                                                               
           MOVE W-MEC35-NCDEWC    TO EC32-NCDEWC2                               
           MOVE WS-DATE-DU-JOUR   TO EC32-DVENTE                                
           MOVE GV11-PVTOTAL      TO EC32-MTPAYM                                
           MOVE W-MEC35-MD-PMENT  TO EC32-CPAYM                                 
           MOVE W-SPDATDB2-DSYST  TO EC32-DSYST                                 
           MOVE 'MEC35'           TO EC32-CNOMPROG                              
           EXEC SQL INSERT INTO RVEC3200                                        
                            (NCDEWC  , NCDEWC2, DVENTE,                         
                             DVALP, WVALP, CPAYM, MTPAYM,                       
                             CNOMPROG, DSYST)                                   
                    VALUES (:EC32-NCDEWC, :EC32-NCDEWC2,                        
                            :EC32-DVENTE, :EC32-DVALP, :EC32-WVALP,             
                            :EC32-CPAYM, :EC32-MTPAYM,                          
                            :EC32-CNOMPROG,                                     
                            :EC32-DSYST )                                       
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 AND NOT = -803                                    
              MOVE SPACES TO W-MESSAGES                                         
              STRING 'MEC35 ERR INSERT RTEC32: ' NCDEWC-X                       
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-CREATION-RTEC32. EXIT.                                               
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
       LECTURE-TSTENVOI SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         READQ TS                                                        
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         INTO   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         ITEM   (WP-RANG-TSTENVOI)                                       
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
              SET WC-TSTENVOI-SUITE      TO TRUE                                
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
           WHEN 26                                                              
           WHEN 44                                                              
              SET WC-TSTENVOI-FIN        TO TRUE                                
           WHEN OTHER                                                           
              SET WC-TSTENVOI-ERREUR     TO TRUE                                
           END-EVALUATE.                                                        
       FIN-LECT-TSTENVOI. EXIT.                                                 
      ******************************************************************        
      * SPDatDB2                                                                
      ******************************************************************        
       CALL-SPDATDB2 SECTION.                                                   
           CALL 'SPDATDB2' USING W-SPDATDB2-RC W-SPDATDB2-DSYST.                
           IF W-SPDATDB2-RC NOT = ZERO                                          
              MOVE 'Pbm Date Syst�me SPDATDB2' TO W-MESSAGES                    
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9'                     TO W-MEC35-CODE-RET                  
           END-IF.                                                              
       FIN-CALL-SPDATDB2. EXIT.                                                 
      ******************************************************************        
      * Display d'un Message                                                    
      ******************************************************************        
       DISPLAY-W-MESSAGE SECTION.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-MESSAGE-H.                             
           MOVE W-MINUTE            TO W-MESSAGE-M.                             
           EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)                
                     LENGTH (80) NOHANDLE                                       
           END-EXEC.                                                            
       FIN-DISPLAY-W-MESSAGE. EXIT.                                             
      ******************************************************************        
      * Link � Tetdatc.                                                         
      ******************************************************************        
       FAIS-UN-LINK-A-TETDATC SECTION.                                          
           EXEC CICS LINK PROGRAM ('TETDATC')                                   
                     COMMAREA (Z-COMMAREA-TETDATC)                              
           END-EXEC.                                                            
           IF GFVDAT NOT = '1'                                                  
              MOVE 'Code retour TETDATC: ' TO W-MESSAGES                        
              MOVE GFVDAT                  TO W-MESSAGES(24:)                   
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9'                     TO W-MEC35-CODE-RET                  
           END-IF.                                                              
       FIN-FAIS-UN-LINK-A-TETDATC. EXIT.                                        
      *-----------------------------------------------------------------        
       UPDATE-RTGV10 SECTION.                                                   
           MOVE GV11-DCREATION       TO GV10-DMODIFVTE                          
           MOVE GV11-DSYST           TO GV10-DSYST                              
           MOVE GV11-NSEQNQ          TO GV10-NSEQNQ                             
           EXEC SQL UPDATE RVGV1005                                             
                       SET PTTVENTE   = :GV10-PTTVENTE  + :GV11-PVTOTAL,        
                           PCOMPT     = :GV10-PCOMPT + :GV11-PVTOTAL ,          
                           NSEQNQ     = :GV10-NSEQNQ    ,                       
                           DMODIFVTE  = :GV10-DMODIFVTE ,                       
                           DSYST      = :GV10-DSYST                             
                     WHERE NSOCIETE   = :GV10-NSOCIETE                          
                       AND NLIEU      = :GV10-NLIEU                             
                       AND NVENTE     = :GV10-NVENTE                            
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGES                                         
              MOVE SQLCODE TO EDIT-SQL                                          
              STRING 'MEC35 ERR UPD RTGV10 NSOC ' GV10-NSOCIETE                 
                                     ' NLIEU ' GV10-NLIEU                       
                                     ' NVENTE ' GV10-NVENTE                     
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-UPDATE-RTGV10. EXIT.                                                 
       SELECT-MAX-NSEQ SECTION.                                                 
      *-------------------------------                                          
           MOVE 0                 TO W-MAX-NSEQ                                 
           MOVE W-MEC35-NSOC      TO GV11-NSOCIETE                              
           MOVE W-MEC35-NLIEU     TO GV11-NLIEU                                 
           MOVE W-MEC35-NVENTE    TO GV11-NVENTE                                
           EXEC SQL SELECT MAX(NSEQ)                                            
                      INTO :W-MAX-NSEQ                                          
                      FROM RVGV1106                                             
                     WHERE NSOCIETE = :GV11-NSOCIETE                            
                       AND NLIEU    = :GV11-NLIEU                               
                       AND NVENTE   = :GV11-NVENTE                              
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGES                                         
              MOVE SQLCODE TO EDIT-SQL                                          
              STRING 'MEC35 ERR MAX NSEQ:'                                      
                                   ' NSOCIETE ' GV11-NSOCIETE                   
                                      ' NLIEU ' GV11-NLIEU                      
                                     ' DVENTE ' GV11-NVENTE                     
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-SELECT-MAX-NSEQ. EXIT.                                               
       SELECT-MAX-NSEQENS     SECTION.                                          
           MOVE 0                 TO W-MAX-NSEQENS                              
           MOVE W-MEC35-NSOC      TO GV11-NSOCIETE                              
           MOVE W-MEC35-NLIEU     TO GV11-NLIEU                                 
           MOVE W-MEC35-NVENTE    TO GV11-NVENTE                                
           EXEC SQL SELECT MAX(NSEQENS)                                         
                      INTO :W-MAX-NSEQENS                                       
                      FROM RVGV1106                                             
                     WHERE NSOCIETE = :GV11-NSOCIETE                            
                       AND NLIEU    = :GV11-NLIEU                               
                       AND NVENTE   = :GV11-NVENTE                              
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGES                                         
              MOVE SQLCODE TO EDIT-SQL                                          
              STRING 'MEC35 ERR MAX NSEQENS:'                                   
                                   ' NSOCIETE ' GV11-NSOCIETE                   
                                      ' NLIEU ' GV11-NLIEU                      
                                     ' DVENTE ' GV11-NVENTE                     
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-SELECT-MAX-NSEQENS. EXIT.                                            
       SELECT-MAX-NLIGNE       SECTION.                                         
           MOVE 0                 TO W-MAX-Nligne                               
           MOVE W-MEC35-NSOC      TO GV11-NSOCIETE                              
           MOVE W-MEC35-NLIEU     TO GV11-NLIEU                                 
           MOVE W-MEC35-NVENTE    TO GV11-NVENTE                                
           EXEC SQL SELECT MAX(NLIGNE)                                          
                      INTO :W-MAX-Nligne                                        
                      FROM RVGV1106                                             
                     WHERE NSOCIETE = :GV11-NSOCIETE                            
                       AND NLIEU    = :GV11-NLIEU                               
                       AND NVENTE   = :GV11-NVENTE                              
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGES                                         
              MOVE SQLCODE TO EDIT-SQL                                          
              STRING 'MEC35 ERR MAX Nligne:'                                    
                                   ' NSOCIETE ' GV11-NSOCIETE                   
                                      ' NLIEU ' GV11-NLIEU                      
                                     ' DVENTE ' GV11-NVENTE                     
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-SELECT-MAX-NLIGNE. EXIT.                                             
       SELECT-CODE-VENDEUR     SECTION.                                         
           MOVE SPACES            TO W-CVENDEUR                                 
           MOVE W-MEC35-NSOC      TO GV11-NSOCIETE                              
           MOVE W-MEC35-NLIEU     TO GV11-NLIEU                                 
           MOVE W-MEC35-NVENTE    TO GV11-NVENTE                                
           EXEC SQL SELECT CVENDEUR                                             
                      INTO :W-CVENDEUR                                          
                      FROM RVGV1106                                             
                     WHERE NSOCIETE = :GV11-NSOCIETE                            
                       AND NLIEU    = :GV11-NLIEU                               
                       AND NVENTE   = :GV11-NVENTE                              
                     FETCH FIRST 1 ROWS ONLY                                    
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGES                                         
              MOVE SQLCODE TO EDIT-SQL                                          
              STRING 'MEC35 ERR code vendeur '                                  
                                   ' NSOCIETE ' GV11-NSOCIETE                   
                                      ' NLIEU ' GV11-NLIEU                      
                                     ' VENTE ' GV11-NVENTE                      
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-SELECT-CODE-VENDEUR. EXIT.                                           
       SELECT-EC008 SECTION.                                                    
           MOVE SPACES TO EC008-WTABLEG.                                        
           EXEC SQL SELECT WTABLEG INTO :EC008-WTABLEG                          
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = 'EC008'                                   
                       AND CTABLEG2 = :EC008-CTABLEG2                           
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              STRING 'MEC35 MODE PAIEMENT INCONNU : '                           
                 W-MEC35-MD-PMENT                                               
                 DELIMITED BY SIZE INTO W-MESSAGES                              
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-SELECT-EC008. EXIT.                                                  
      ******************************************************************        
      * Insertion de la ligne de vente                                          
      ******************************************************************        
       INSERT-RTGV11 SECTION.                                                   
      *----------------------                                                   
           EXEC SQL INSERT INTO RVGV1106                                        
            VALUES (                                                            
              :GV11-NSOCIETE                                                    
             ,:GV11-NLIEU                                                       
             ,:GV11-NVENTE                                                      
             ,:GV11-CTYPENREG                                                   
             ,:GV11-NCODICGRP                                                   
             ,:GV11-NCODIC                                                      
             ,:GV11-NSEQ                                                        
             ,:GV11-CMODDEL                                                     
             ,:GV11-DDELIV                                                      
             ,:GV11-NORDRE                                                      
             ,:GV11-CENREG                                                      
             ,:GV11-QVENDUE                                                     
             ,:GV11-PVUNIT                                                      
             ,:GV11-PVUNITF                                                     
             ,:GV11-PVTOTAL                                                     
             ,:GV11-CEQUIPE                                                     
             ,:GV11-NLIGNE                                                      
             ,:GV11-TAUXTVA                                                     
             ,:GV11-QCONDT                                                      
             ,:GV11-PRMP                                                        
             ,:GV11-WEMPORTE                                                    
             ,:GV11-CPLAGE                                                      
             ,:GV11-CPROTOUR                                                    
             ,:GV11-CADRTOUR                                                    
             ,:GV11-CPOSTAL                                                     
             ,:GV11-LCOMMENT                                                    
             ,:GV11-CVENDEUR                                                    
             ,:GV11-NSOCLIVR                                                    
             ,:GV11-NDEPOT                                                      
             ,:GV11-NAUTORM                                                     
             ,:GV11-WARTINEX                                                    
             ,:GV11-WEDITBL                                                     
             ,:GV11-WACOMMUTER                                                  
             ,:GV11-WCQERESF                                                    
             ,:GV11-NMUTATION                                                   
             ,:GV11-CTOURNEE                                                    
             ,:GV11-WTOPELIVRE                                                  
             ,:GV11-DCOMPTA                                                     
             ,:GV11-DCREATION                                                   
             ,:GV11-HCREATION                                                   
             ,:GV11-DANNULATION                                                 
             ,:GV11-NLIEUORIG                                                   
             ,:GV11-WINTMAJ                                                     
             ,:GV11-DSYST                                                       
             ,:GV11-DSTAT                                                       
             ,:GV11-CPRESTGRP                                                   
             ,:GV11-NSOCMODIF                                                   
             ,:GV11-NLIEUMODIF                                                  
             ,:GV11-DATENC                                                      
             ,:GV11-CDEV                                                        
             ,:GV11-WUNITR                                                      
             ,:GV11-LRCMMT                                                      
             ,:GV11-NSOCP                                                       
             ,:GV11-NLIEUP                                                      
             ,:GV11-NTRANS                                                      
             ,:GV11-NSEQFV                                                      
             ,:GV11-NSEQNQ                                                      
             ,:GV11-NSEQREF                                                     
             ,:GV11-NMODIF                                                      
             ,:GV11-NSOCORIG                                                    
             ,:GV11-DTOPE                                                       
             ,:GV11-NSOCLIVR1                                                   
             ,:GV11-NDEPOT1                                                     
             ,:GV11-NSOCGEST                                                    
             ,:GV11-NLIEUGEST                                                   
             ,:GV11-NSOCDEPLIV                                                  
             ,:GV11-NLIEUDEPLIV                                                 
             ,:GV11-TYPVTE                                                      
             ,:GV11-CTYPENT                                                     
             ,:GV11-NLIEN                                                       
             ,:GV11-NACTVTE                                                     
             ,:GV11-PVCODIG                                                     
             ,:GV11-QTCODIG                                                     
             ,:GV11-DPRLG                                                       
             ,:GV11-CALERTE                                                     
             ,:GV11-CREPRISE                                                    
             ,:GV11-NSEQENS                                                     
             ,:GV11-MPRIMECLI                                                   
                   )                                                            
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES TO W-MESSAGES                                         
              MOVE SQLCODE TO EDIT-SQL                                          
              STRING 'MEC35 INSERT RTGV11 NSOC: ' GV11-NSOCIETE                 
                                 ' NLIEU: '  GV11-NLIEU                         
                                 ' NORDRE: ' GV11-NORDRE                        
                                 ' NLIGNE: ' GV11-NLIGNE                        
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-INSERT-RTGV11. EXIT.                                                 
       UPDATE-RTGV11 SECTION.                                                   
           COMPUTE GV11-NSEQENS = W-MAX-NSEQENS + 1                             
           MOVE NV45-VALCTRL        TO GV11-LCOMMENT                            
           EXEC SQL UPDATE RVGV1106                                             
                       SET LCOMMENT   = :GV11-LCOMMENT                          
                     WHERE NSOCIETE   = :GV11-NSOCIETE                          
                       AND NLIEU      = :GV11-NLIEU                             
                       AND NVENTE     = :GV11-NVENTE                            
                       AND NSEQNQ     = :GV11-NSEQNQ                            
           END-EXEC.                                                            
           IF SQLCODE NOT = 0                                                   
              MOVE SPACES            TO W-MESSAGES                              
              MOVE GV11-NSEQNQ       TO NSEQNQ-X                                
              MOVE SQLCODE           TO EDIT-SQL                                
              STRING 'MEC35 UPDATE RTGV11 NSOC ' GV11-NSOCIETE                  
                                     ' NLIEU ' GV11-NLIEU                       
                                     ' NVENTE ' GV11-NVENTE                     
                                     ' NSEQNQ ' NSEQNQ-X                        
                      ' '     EDIT-SQL                                          
              DELIMITED BY SIZE INTO W-MESSAGES                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-UPDATE-RTGV11.  EXIT.                                                
       DISPLAY-LOG section.                                                     
           EXEC CICS WRITEQ TD QUEUE ('CESO')                                   
                     FROM (W-MESSAGE-S)                                         
                     LENGTH (80) NOHANDLE                                       
           END-EXEC.                                                            
       F-DISPLAY-LOG. EXIT.                                                     
      *-----------------------------------------------------------------        
      * Abend Label: on s'est plant�; on flingue le code retour et              
      *              on revient l� o� on �tait: on ne cr�e pas de vente.        
       ABEND-LABEL SECTION.                                                     
           MOVE 'Handle Abend effectu�' TO W-MESSAGES.                          
           PERFORM DISPLAY-W-MESSAGE.                                           
           MOVE '9' TO W-MEC35-CODE-RET.                                        
       F-ABEND-LABEL. EXIT.                                                     
       SELECT-RTEC02 SECTION.                                                   
           MOVE W-MEC35-NSOC   TO EC02-NSOCIETE                                 
           MOVE W-MEC35-NLIEU  TO EC02-NLIEU                                    
           MOVE W-MEC35-NVENTE TO EC02-NVENTE                                   
           EXEC SQL SELECT   NCDEWC  INTO :EC02-NCDEWC                          
                      FROM RVEC0200                                             
                     WHERE   NSOCIETE  =  :EC02-NSOCIETE                        
                       AND   NLIEU     =  :EC02-NLIEU                           
                       AND   NVENTE    =  :EC02-NVENTE                          
           END-EXEC.                                                            
           IF SQLCODE NOT = 0 and +100                                          
              MOVE SPACES TO W-MESSAGEs                                         
              MOVE SQLCODE TO EDIT-SQL                                          
              STRING 'SELECT EC02'                                              
                           ' SOC '  W-MEC35-NSOC                                
                          ' LIEU '  W-MEC35-NLIEU                               
                          ' VENTE ' W-MEC35-NVENTE                              
                       ' ' EDIT-SQL                                             
              DELIMITED BY SIZE INTO W-MESSAGEs                                 
              PERFORM DISPLAY-W-MESSAGE                                         
              MOVE '9' TO W-MEC35-CODE-RET                                      
           END-IF.                                                              
       FIN-SELECT-RTEC02. EXIT.                                                 
                                                                                
