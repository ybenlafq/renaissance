      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------                                  
       TRAITEMENT-EGM003               SECTION.                                 
      *---------------------------------------                                  
           INITIALIZE WS-EGM000.                                                
           IF ZW-TYPCHAMP1 = 'Z' OR 'N'                                         
              MOVE ZW-LGCHAMP1                TO EGM-J                          
              COMPUTE EGM-M = 15 - EGM-J + 1                                    
              MOVE ZW-CONTENU1 (1:EGM-J)      TO EGM-CH1 (EGM-M:EGM-J)          
              MOVE EGM-CHAMP1                 TO EGM-CHP1                       
           END-IF.                                                              
           IF ZW-TYPCHAMP1 = 'H'                                                
              MOVE ZW-CONTENU1 (1:2)          TO EGM-Z-HALFWORD                 
              MOVE EGM-HALFWORD               TO EGM-CHP1                       
           END-IF.                                                              
           IF ZW-TYPCHAMP1 = 'F'                                                
              MOVE ZW-CONTENU1 (1:4)          TO EGM-Z-FULLWORD                 
              MOVE EGM-FULLWORD               TO EGM-CHP1                       
           END-IF.                                                              
           IF ZW-TYPCHAMP1 = 'D'                                                
              MOVE ZW-CONTENU1 (1:8)          TO EGM-Z-DOUBLEWORD               
              MOVE EGM-DOUBLEWORD             TO EGM-CHP1                       
           END-IF.                                                              
           IF ZW-TYPCHAMP1 = 'S' OR '*'                                         
              MOVE ZW-CONTENU1 (1:8)          TO GFSAMJ-0                       
              MOVE '5'                        TO GFDATA                         
              PERFORM APPEL-CALCUL-DATE                                         
              MOVE GFQNT0                     TO EGM-CHP1                       
           END-IF.                                                              
           IF ZW-TYPCHAMP1 = 'Y'                                                
              MOVE ZW-CONTENU1 (1:6)          TO GFAMJ-1                        
              MOVE '7'                        TO GFDATA                         
              PERFORM APPEL-CALCUL-DATE                                         
              MOVE GFQNT0                     TO EGM-CHP1                       
           END-IF.                                                              
           IF ZW-TYPCHAMP1 = 'P' OR '?'                                         
              COMPUTE EGM-J ROUNDED = (ZW-LGCHAMP1 + 1) / 2                     
              COMPUTE EGM-M = 8 - EGM-J + 1                                     
              MOVE ZW-CONTENU1 (1:EGM-J)      TO EGM-CHPA1(EGM-M:EGM-J)         
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'Z' OR 'N'                                         
              MOVE ZW-LGCHAMP2                TO EGM-K                          
              COMPUTE EGM-M = 15 - EGM-K + 1                                    
              MOVE ZW-CONTENU2 (1:EGM-K)      TO EGM-CH2 (EGM-M:EGM-K)          
              MOVE EGM-CHAMP2                 TO EGM-CHP2                       
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'H'                                                
              MOVE ZW-CONTENU2 (1:2)          TO EGM-Z-HALFWORD                 
              MOVE EGM-HALFWORD               TO EGM-CHP2                       
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'F'                                                
              MOVE ZW-CONTENU2 (1:4)          TO EGM-Z-FULLWORD                 
              MOVE EGM-FULLWORD               TO EGM-CHP2                       
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'D'                                                
              MOVE ZW-CONTENU2 (1:8)          TO EGM-Z-DOUBLEWORD               
              MOVE EGM-DOUBLEWORD             TO EGM-CHP2                       
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'P' OR '?'                                         
              COMPUTE EGM-K ROUNDED = (ZW-LGCHAMP2 + 1) / 2                     
              COMPUTE EGM-M = 8 - EGM-K + 1                                     
              MOVE ZW-CONTENU2 (1:EGM-K)      TO EGM-CHPA2 (EGM-M:EGM-K)        
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'S' OR '*'                                         
              MOVE ZW-CONTENU2 (1:8)          TO GFSAMJ-0                       
              MOVE '5'                        TO GFDATA                         
              PERFORM APPEL-CALCUL-DATE                                         
              MOVE GFQNT0                     TO EGM-CHP2                       
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'Y'                                                
              MOVE ZW-CONTENU2 (1:6)          TO GFAMJ-1                        
              MOVE '7'                        TO GFDATA                         
              PERFORM APPEL-CALCUL-DATE                                         
              MOVE GFQNT0                     TO EGM-CHP2                       
           END-IF.                                                              
           IF ZW-TYPCHAMP1 = 'K'                                                
              PERFORM VARYING EGM-I FROM LENGTH OF ZW-CONTENU1 BY -1            
                      UNTIL   ZW-CONTENU1 (EGM-I:1) = ''''                      
              END-PERFORM                                                       
              COMPUTE EGM-I = EGM-I - 1                                         
              MOVE 15                TO EGM-M                                   
              PERFORM VARYING EGM-I FROM EGM-I BY -1                            
                      UNTIL   ZW-CONTENU1 (EGM-I:1) = ''''                      
                 IF ZW-CONTENU1 (EGM-I:1) NOT < '0'                             
                    MOVE ZW-CONTENU1 (EGM-I:1)   TO EGM-CH1 (EGM-M:1)           
                    COMPUTE EGM-M = EGM-M - 1                                   
                 END-IF                                                         
              END-PERFORM                                                       
              MOVE EGM-CHAMP1                 TO EGM-CHP1                       
              PERFORM VARYING EGM-I FROM 2 BY 1                                 
                      UNTIL   ZW-CONTENU1 (EGM-I:1) = ''''                      
                 IF ZW-CONTENU1 (EGM-I:1) = '-'                                 
                    COMPUTE EGM-CHP1 = EGM-CHP1 * -1                            
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'K'                                                
              PERFORM VARYING EGM-I FROM LENGTH OF ZW-CONTENU2 BY -1            
                      UNTIL   ZW-CONTENU2 (EGM-I:1) = ''''                      
              END-PERFORM                                                       
              COMPUTE EGM-I = EGM-I - 1                                         
              MOVE 15                TO EGM-M                                   
              PERFORM VARYING EGM-I FROM EGM-I BY -1                            
                      UNTIL ZW-CONTENU2 (EGM-I:1) = ''''                        
                 IF ZW-CONTENU2 (EGM-I:1) NOT < '0'                             
                    MOVE ZW-CONTENU2 (EGM-I:1)   TO EGM-CH2 (EGM-M:1)           
                    COMPUTE EGM-M = EGM-M - 1                                   
                 END-IF                                                         
              END-PERFORM                                                       
              MOVE EGM-CHAMP2                 TO EGM-CHP2                       
              PERFORM VARYING EGM-I FROM 2 BY 1                                 
                      UNTIL   ZW-CONTENU2 (EGM-I:1) = ''''                      
                 IF ZW-CONTENU2 (EGM-I:1) = '-'                                 
                    COMPUTE EGM-CHP2 = EGM-CHP2 * -1                            
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
           MOVE ZW-DECCHAMP1                 TO EGM-DECCH1.                     
           MOVE ZW-DECCHAMP2                 TO EGM-DECCH2.                     
           MOVE ZW-DECCHAMPR                 TO EGM-DECCHR.                     
           IF ZW-TYPCHAMP1 = 'K'                                                
              MOVE ZERO                         TO EGM-DECCH1                   
              PERFORM VARYING EGM-I FROM 1 BY 1                                 
                      UNTIL   EGM-I > LENGTH OF ZW-CONTENU1                     
                      OR      ZW-CONTENU1 (EGM-I:1) = ','                       
              END-PERFORM                                                       
              IF EGM-I NOT > LENGTH OF ZW-CONTENU1                              
                 COMPUTE EGM-I = EGM-I + 1                                      
                 PERFORM VARYING EGM-I FROM EGM-I BY 1                          
                         UNTIL   ZW-CONTENU1 (EGM-I:1) = ''''                   
                    ADD  1                       TO EGM-DECCH1                  
                 END-PERFORM                                                    
              END-IF                                                            
           END-IF.                                                              
           IF ZW-TYPCHAMP2 = 'K'                                                
              MOVE ZERO                         TO EGM-DECCH2                   
              PERFORM VARYING EGM-I FROM 1 BY 1                                 
                      UNTIL   EGM-I > LENGTH OF ZW-CONTENU2                     
                      OR      ZW-CONTENU2 (EGM-I:1) = ','                       
              END-PERFORM                                                       
              IF EGM-I NOT > LENGTH OF ZW-CONTENU2                              
                 COMPUTE EGM-I = EGM-I + 1                                      
                 PERFORM VARYING EGM-I FROM EGM-I BY 1                          
                         UNTIL   ZW-CONTENU2 (EGM-I:1) = ''''                   
                    ADD  1                       TO EGM-DECCH2                  
                 END-PERFORM                                                    
              END-IF                                                            
           END-IF.                                                              
           IF EGM-DECCH1 > 7                                                    
              COMPUTE EGM-XX10 = 10 ** (EGM-DECCH1 - 7)                         
              COMPUTE EGM-CHP1 = EGM-CHP1 / EGM-XX10                            
              MOVE 7                            TO EGM-DECCH1                   
           END-IF.                                                              
           IF EGM-DECCH2 > 7                                                    
              COMPUTE EGM-XX10 = 10 ** (EGM-DECCH2 - 7)                         
              COMPUTE EGM-CHP2 = EGM-CHP2 / EGM-XX10                            
              MOVE 7                            TO EGM-DECCH2                   
           END-IF.                                                              
      *                                                                         
      *    IF EGM-CHP1 NOT NUMERIC                                              
      *       MOVE ZERO                   TO EGM-CHP1                           
      *    END-IF.                                                              
      *                                                                         
      *    IF EGM-CHP2 NOT NUMERIC                                              
      *       MOVE ZERO                   TO EGM-CHP2                           
      *    END-IF.                                                              
           EVALUATE ZW-TYPCALCUL (1:1)                                          
              WHEN '*'       PERFORM MULTIPLIER                                 
              WHEN '/'       PERFORM DIVISER                                    
              WHEN '-'       PERFORM SOUSTRAIRE                                 
              WHEN '+'       PERFORM ADDITIONNER                                
              WHEN '%'       PERFORM POURCENTER                                 
              WHEN '&'       PERFORM POUR-CENT                                  
           END-EVALUATE                                                         
           MOVE SPACES                    TO ZW-CONTENUR.                       
           IF ZW-TYPCHAMPR = 'Z'                                                
              MOVE EGM-CHPR                   TO EGM-CHAMPR                     
              MOVE ZW-LGCHAMPR                TO EGM-L                          
              COMPUTE EGM-M = 15 - EGM-L + 1                                    
              MOVE EGM-CHR (EGM-M:EGM-L)      TO ZW-CONTENUR (1:EGM-L)          
           END-IF.                                                              
           IF ZW-TYPCHAMPR = 'N'                                                
              MOVE EGM-CHPR                   TO EGM-CHAMPN                     
              MOVE ZW-LGCHAMPR                TO EGM-L                          
              COMPUTE EGM-M = 15 - EGM-L + 1                                    
              MOVE EGM-CHN (EGM-M:EGM-L)      TO ZW-CONTENUR (1:EGM-L)          
           END-IF.                                                              
           IF ZW-TYPCHAMPR = 'H'                                                
              MOVE EGM-CHPR                   TO EGM-HALFWORD                   
              MOVE EGM-Z-HALFWORD             TO ZW-CONTENUR (1:2)              
           END-IF.                                                              
           IF ZW-TYPCHAMPR = 'F'                                                
              MOVE EGM-CHPR                   TO EGM-FULLWORD                   
              MOVE EGM-Z-FULLWORD             TO ZW-CONTENUR (1:4)              
           END-IF.                                                              
           IF ZW-TYPCHAMPR = 'D'                                                
              MOVE EGM-CHPR                   TO EGM-DOUBLEWORD                 
              MOVE EGM-Z-DOUBLEWORD           TO ZW-CONTENUR (1:8)              
           END-IF.                                                              
           IF ZW-TYPCHAMPR = 'P' OR '?'                                         
              COMPUTE EGM-L ROUNDED = (ZW-LGCHAMPR + 1) / 2                     
              COMPUTE EGM-M = 8 - EGM-L + 1                                     
              MOVE EGM-CHPAR (EGM-M:EGM-L)    TO ZW-CONTENUR (1:EGM-L)          
           END-IF.                                                              
           IF ZW-TYPCHAMPR = '*'                                                
              MOVE EGM-CHPR                   TO GFQNT0                         
              MOVE '3'                        TO GFDATA                         
              PERFORM APPEL-CALCUL-DATE                                         
              MOVE GFSAMJ-0                   TO ZW-CONTENUR (1:8)              
           END-IF.                                                              
      *---------------------------------------                                  
       POUR-CENT                       SECTION.                                 
      *---------------------------------------                                  
           COMPUTE EGM-DECCH2 = EGM-DECCH2 + 2.                                 
           PERFORM MULTIPLIER.                                                  
      *---------------------------------------                                  
       MULTIPLIER                      SECTION.                                 
      *---------------------------------------                                  
           COMPUTE EGM-NBDEC = EGM-DECCH1 + EGM-DECCH2.                         
           COMPUTE EGM-NBDEC = EGM-NBDEC - EGM-DECCHR.                          
           COMPUTE EGM-XX10 = 10 ** EGM-NBDEC.                                  
           IF ZW-TYPCALCUL (2:1) = 'R'                                          
              COMPUTE EGM-CHPR ROUNDED = (EGM-CHP1 * EGM-CHP2)                  
                                     / EGM-XX10                                 
           ELSE                                                                 
              COMPUTE EGM-CHPR       = (EGM-CHP1 * EGM-CHP2)                    
                                     / EGM-XX10                                 
           END-IF.                                                              
           MOVE EGM-CHPR          TO EGM-CHAMPR.                                
      *---------------------------------------                                  
       SOUSTRAIRE                      SECTION.                                 
      *---------------------------------------                                  
           IF EGM-DECCH1 > EGM-DECCH2                                           
              COMPUTE EGM-NBDEC = EGM-DECCH1 - EGM-DECCH2                       
              COMPUTE EGM-XX10 = 10 ** EGM-NBDEC                                
              COMPUTE EGM-CHP2 = EGM-CHP2 * EGM-XX10                            
              MOVE EGM-DECCH1 TO EGM-NBDEC                                      
           ELSE                                                                 
              IF EGM-DECCH2 > EGM-DECCH1                                        
                 COMPUTE EGM-NBDEC = EGM-DECCH2 - EGM-DECCH1                    
                 COMPUTE EGM-XX10 = 10 ** EGM-NBDEC                             
                 COMPUTE EGM-CHP1 = EGM-CHP1 * EGM-XX10                         
                 MOVE EGM-DECCH2 TO EGM-NBDEC                                   
              ELSE                                                              
                 MOVE EGM-DECCH2 TO EGM-NBDEC                                   
              END-IF                                                            
           END-IF.                                                              
           COMPUTE EGM-CHPR = EGM-CHP1 - EGM-CHP2.                              
           PERFORM ALIGNER.                                                     
      *---------------------------------------                                  
       ADDITIONNER                     SECTION.                                 
      *---------------------------------------                                  
           IF EGM-DECCH1 > EGM-DECCH2                                           
              COMPUTE EGM-NBDEC = EGM-DECCH1 - EGM-DECCH2                       
              COMPUTE EGM-XX10 = 10 ** EGM-NBDEC                                
              COMPUTE EGM-CHP2 = EGM-CHP2 * EGM-XX10                            
              MOVE EGM-DECCH1 TO EGM-NBDEC                                      
           ELSE                                                                 
              IF EGM-DECCH2 > EGM-DECCH1                                        
                 COMPUTE EGM-NBDEC = EGM-DECCH2 - EGM-DECCH1                    
                 COMPUTE EGM-XX10 = 10 ** EGM-NBDEC                             
                 COMPUTE EGM-CHP1 = EGM-CHP1 * EGM-XX10                         
                 MOVE EGM-DECCH2 TO EGM-NBDEC                                   
              ELSE                                                              
                 MOVE EGM-DECCH2 TO EGM-NBDEC                                   
              END-IF                                                            
           END-IF.                                                              
           COMPUTE EGM-CHPR = EGM-CHP1 + EGM-CHP2.                              
           PERFORM ALIGNER.                                                     
      *---------------------------------------                                  
       ALIGNER                         SECTION.                                 
      *---------------------------------------                                  
           IF EGM-DECCHR > EGM-NBDEC                                            
              COMPUTE EGM-NBDEC = EGM-DECCHR - EGM-NBDEC                        
              COMPUTE EGM-XX10 = 10 ** EGM-NBDEC                                
              IF ZW-TYPCALCUL (2:1) = 'R'                                       
                 COMPUTE EGM-CHPR ROUNDED = EGM-CHPR * EGM-XX10                 
              ELSE                                                              
                 COMPUTE EGM-CHPR       = EGM-CHPR * EGM-XX10                   
              END-IF                                                            
           ELSE                                                                 
              COMPUTE EGM-NBDEC = EGM-NBDEC - EGM-DECCHR                        
              COMPUTE EGM-XX10 = 10 ** EGM-NBDEC                                
              IF ZW-TYPCALCUL (2:1) = 'R'                                       
                 COMPUTE EGM-CHPR ROUNDED = EGM-CHPR / EGM-XX10                 
              ELSE                                                              
                 COMPUTE EGM-CHPR       = EGM-CHPR / EGM-XX10                   
              END-IF                                                            
           END-IF.                                                              
           MOVE EGM-CHPR          TO EGM-CHAMPR.                                
      *---------------------------------------                                  
       POURCENTER                      SECTION.                                 
      *---------------------------------------                                  
           COMPUTE EGM-DECCH1 = EGM-DECCH1 - 2.                                 
           PERFORM DIVISER.                                                     
      *---------------------------------------                                  
       DIVISER                         SECTION.                                 
      *---------------------------------------                                  
           IF EGM-CHP2 = ZERO                                                   
              MOVE ZERO TO EGM-CHPR                                             
           ELSE                                                                 
              PERFORM DIVISIONS                                                 
           END-IF.                                                              
           MOVE EGM-CHPR          TO EGM-CHAMPR.                                
      *---------------------------------------                                  
       DIVISIONS                       SECTION.                                 
      *---------------------------------------                                  
           DIVIDE EGM-CHP1 BY EGM-CHP2 GIVING EGM-CHPR                          
                                    REMAINDER EGM-RESTE.                        
           COMPUTE EGM-NBDEC = EGM-DECCH2 - EGM-DECCH1 + EGM-DECCHR.            
           IF EGM-NBDEC < ZERO                                                  
              COMPUTE EGM-XX10 = 10 ** (EGM-NBDEC * -1)                         
              DIVIDE EGM-CHPR BY EGM-XX10 GIVING EGM-CHPR                       
                               REMAINDER EGM-RESULTI                            
              COMPUTE EGM-RESTE = EGM-RESTE + (EGM-CHP2 * EGM-RESULTI)          
           ELSE                                                                 
              PERFORM VARYING EGM-NBDEC FROM EGM-NBDEC BY -1                    
                      UNTIL EGM-NBDEC = 0                                       
                 COMPUTE EGM-CHPR  = EGM-CHPR * 10                              
                 COMPUTE EGM-RESULTI = EGM-RESTE * 10                           
                 DIVIDE EGM-RESULTI BY EGM-CHP2 GIVING EGM-RESULT               
                                  REMAINDER EGM-RESTE                           
                 ADD  EGM-RESULT     TO EGM-CHPR                                
              END-PERFORM                                                       
           END-IF.                                                              
           IF ZW-TYPCALCUL (2:1) = 'R'                                          
              COMPUTE EGM-CHPR  = EGM-CHPR * 10                                 
              COMPUTE EGM-RESULTI = EGM-RESTE * 10                              
              DIVIDE EGM-RESULTI BY EGM-CHP2 GIVING EGM-RESULT                  
                                  REMAINDER EGM-RESTE                           
              ADD  EGM-RESULT     TO EGM-CHPR                                   
              COMPUTE EGM-CHPR ROUNDED = EGM-CHPR / 10                          
           END-IF.                                                              
                                                                                
