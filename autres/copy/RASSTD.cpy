      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION ARTICLE SITE STANDARD LOGISTIQUE      * 00031000
      * NOM FICHIER.: RASSTD                                          * 00032008
      *---------------------------------------------------------------* 00034000
      * CR   .......: 12/10/2011  17:46:00                            * 00035000
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR....: 200                                             * 00037107
      ***************************************************************** 00037300
      *                                                                 00037400
       01  RASSTD.                                                      00037500
      * TYPE ENREGISTREMENT : R�CEPTION ARTICLE SITE STANDARD           00037603
           05      RASSTD-TYP-ENREG       PIC  X(0006).                 00037705
      * CODE ACTION                                                     00037803
           05      RASSTD-CODE-ACTION     PIC  X(0001).                 00037906
      * CODE SOCI�T�                                                    00038003
           05      RASSTD-CSOCIETE        PIC  X(0005).                 00038105
      * CODE ARTICLE                                                    00038203
           05      RASSTD-CARTICLE        PIC  X(0018).                 00038305
      * CODE UNIT� LOGISTIQUE                                           00038403
           05      RASSTD-CODE-UL         PIC  X(0005).                 00039005
      * NUM�RO DE SITE                                                  00039103
           05      RASSTD-NSITE           PIC  9(0003).                 00040005
      * CODE EMBALLAGE                                                  00041003
           05      RASSTD-CEMBALLAGE      PIC  X(0010).                 00050005
      * DISPONIBLE                                                      00051003
           05      RASSTD-DISPONIBLE      PIC  9(0001).                 00060005
      * N� PROFIL RANGEMENT                                             00061003
           05      RASSTD-NPROFIL-RANG    PIC  9(0003).                 00070005
      * TYPE DE PR�PARATION                                             00071003
           05      RASSTD-TYPE-PREP       PIC  9(0003).                 00080005
      * MODE PR�PARATION                                                00081003
           05      RASSTD-MODE-PREP       PIC  9(0003).                 00090005
      * ADRESSE                                                         00091003
           05      RASSTD-ADRESSE         PIC  X(0020).                 00100005
      * TYPE R�APPRO                                                    00101003
           05      RASSTD-TYPE-REAPPRO    PIC  9(0003).                 00110005
      * SEUIL MINI                                                      00111003
           05      RASSTD-SEUIL-MINI      PIC  9(0009).                 00120005
      * SEUIL MAXI                                                      00120103
           05      RASSTD-SEUIL-MAXI      PIC  9(0009).                 00121005
      * N� PROFIL R�APPRO                                               00122003
           05      RASSTD-NPROFIL-REAPPRO PIC  9(0003).                 00130005
      * SEUIL PR�PARATION PALETTE                                       00131003
           05      RASSTD-SEUIL-PREPA-PAL PIC  9(0009).                 00140005
      * N� PROFIL PR�PARATION GLOBALE                                   00141003
           05      RASSTD-NPROFIL-PREP-GLOB PIC 9(003).                 00150005
      * UL DE R�APPRO 1                                                 00151003
           05      RASSTD-UL-REAPPRO1     PIC  X(0005).                 00160005
      * UL DE R�APPRO 2                                                 00160103
           05      RASSTD-UL-REAPPRO2     PIC  X(0005).                 00161005
      * UL DE R�APPRO 3                                                 00161103
           05      RASSTD-UL-REAPPRO3     PIC  X(0005).                 00162005
      * UL DE R�APPRO 4                                                 00162103
           05      RASSTD-UL-REAPPRO4     PIC  X(0005).                 00163005
      * UL DE R�APPRO 5                                                 00163103
           05      RASSTD-UL-REAPPRO5     PIC  X(0005).                 00164005
      * CARACT�RISTIQUE 1                                               00165003
           05      RASSTD-CARACT1         PIC  X(0020).                 00167005
      * CARACT�RISTIQUE 2                                               00167103
           05      RASSTD-CARACT2         PIC  X(0020).                 00168005
      * CARACT�RISTIQUE 3                                               00168103
           05      RASSTD-CARACT3         PIC  X(0020).                 00169005
      * FILLER                                                          00169103
           05      RASSTD-FILLER          PIC  X(0001).                 00170005
                                                                                
