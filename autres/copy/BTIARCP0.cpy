      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************* 14-09-94          
      * BTIARCP0 : PROCEDURE BATCH COBOL                                        
      *          : CONVERT AN SQLCODE INTO A TEXT MESSAGE                       
      ****************************************************************          
       APPEL-DSNTIAR  SECTION.                                                  
           CALL  'DSNTIAR'  USING  SQLCA,                                       
                                   TIAR,                                        
                                   TIAR-LRECL.                                  
           IF RETURN-CODE  >  4                                                 
              GO TO  FIN-APPEL-DSNTIAR                                          
           END-IF.                                                              
           DISPLAY  '***                                        ***'.           
           DISPLAY  '*** DSNTIAR -     MESSAGE SQL    - DSNTIAR ***'.           
           DISPLAY  '***                   DEBUT                ***'.           
           DISPLAY  '***                                        ***'.           
           PERFORM  VARYING  TIAR-I                                             
                    FROM     1  BY  1                                           
                    UNTIL    TIAR-I            >  10                            
                    OR       TIAR-MSG(TIAR-I)  =  SPACES                        
              DISPLAY  TIAR-MSG(TIAR-I)                                         
           END-PERFORM.                                                         
           DISPLAY  '***                                        ***'.           
           DISPLAY  '***                    FIN                 ***'.           
           DISPLAY  '*** DSNTIAR -     MESSAGE SQL    - DSNTIAR ***'.           
           DISPLAY  '***                                        ***'.           
       FIN-APPEL-DSNTIAR.  EXIT.                                                
       EJECT                                                                    
                                                                                
