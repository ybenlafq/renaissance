      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *-----------------------------------------------------------------00000010
      *  F I C H E   D E S C R I P T I V E   D E   C O M M A R E A      00000020
      *-----------------------------------------------------------------00000030
      *                                                                 00000040
      *  PROJET     : BIENS ET SERVICES (NOVEMBRE 2003)                 00000050
      *                                                                 00000060
      *  PROGRAMME  : MBS10                                             00000070
      *  TITRE      : MODULE DE CALCUL DE LA COMMISSION MINIMUM OFFRE   00000080
      *  LONGUEUR   : 100 OCTETS (LENGTH OF COMM-MBS10)                 00000090
      *                                                                 00000100
      *  ENTREE     : CODIC   (DOIT �TRE UN CODE OFFRE)                 00000110
      *  SORTIE     : MONTANT (� D�FAUT = 0)                            00000120
      *                                                                 00000130
      *-----------------------------------------------------------------00000140
       01  COMM-MBS10.                                                  00000150
           03 COMM-MBS10-MESSAGE.                                       00000160
              05 COMM-MBS10-CODE-RETOUR       PIC X(1).                 00000170
                 88 COMM-MBS10-OK                         VALUE ' '.    00000180
                 88 COMM-MBS10-KO                         VALUE '1'.    00000190
              05 COMM-MBS10-LIBERR            PIC X(50).                00000200
           03 COMM-MBS10-ENTREE.                                        00000210
              05 COMM-MBS10-NCODIC            PIC X(7).                 00000220
              05 COMM-MBS10-DEFFET            PIC X(8).                 00000230
           03 COMM-MBS10-SORTIE.                                        00000240
              05 COMM-MBS10-MONTANT           PIC S9(7)V9(2) COMP-3.    00000250
           03 COMM-MBS10-FILLER               PIC X(29).                00000260
                                                                                
