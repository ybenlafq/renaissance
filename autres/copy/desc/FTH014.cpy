      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER FTH014 QUI PERMET D'ALIMENTER                *        
      *  LA TABLE RTTH14 (TABLE DES COMPOSANTES)                       *        
      *----------------------------------------------------------------*        
       01  DSECT-:FTH014:.                                                      
         05 CHAMPS-:FTH014:.                                                    
           10 :FTH014:-CLE.                                             122  005
      *                                               POS 1            *        
              15 :FTH014:-NCODIC        PIC X(07).                      122  005
      *                                               POS 8            *        
              15 :FTH014:-COPCO         PIC X(03).                      122  005
      *                                                                *        
           10 :FTH014:-DONNEES.                                         094  006
      *                                               POS 11           *        
              15 :FTH014:-PTC           PIC S9(5)V9(2) USAGE COMP-3.    197  007
      *                                               POS 15           *        
              15 :FTH014:-PCF           PIC S9(5)V9(2) USAGE COMP-3.    197  007
      *                                               POS 19           *        
              15 :FTH014:-PCFCALC       PIC S9(5)V9(2) USAGE COMP-3.    197  007
      *                                               POS 23                    
              15 :FTH014:-FORCE         PIC X(01).                      197  007
      *                                               POS 24                    
              15 :FTH014:-C1            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 27                    
              15 :FTH014:-C2            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 30                    
              15 :FTH014:-C3            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 33                    
              15 :FTH014:-C4            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 37                    
              15 :FTH014:-C5            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 40                    
              15 :FTH014:-C6            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 43                    
              15 :FTH014:-C7            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 46                    
              15 :FTH014:-C8            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 49                    
              15 :FTH014:-C9            PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 52                    
              15 :FTH014:-C10           PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 55                    
              15 :FTH014:-C11           PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 58                    
              15 :FTH014:-C12           PIC S9(3)V9(2) USAGE COMP-3.    197  007
      *                                               POS 61                    
              15 :FTH014:-FLAGTAXE      PIC X(01).                      197  007
      *                                               POS 62                    
              15 :FTH014:-MNTTAXE       PIC S9(7)V9(2) USAGE COMP-3.            
      *                                               POS 66                    
              15 FILLER                 PIC X(44) VALUE SPACES.         197  007
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FTH014J-LONG          PIC S9(4)   COMP  VALUE +100.            
      *                                                                         
      *--                                                                       
       01  DSECT-:FTH014:-LONG          PIC S9(4) COMP-5  VALUE +100.           
                                                                                
      *}                                                                        
