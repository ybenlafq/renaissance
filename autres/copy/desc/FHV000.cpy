      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00001000
      * ****************       DESCRIPTION     ******************* *    00002000
      *                 *******   FHV000  *******                  *    00002100
      *              FICHIER DES VENTES PAR MAGASIN                *    00002200
      *          POUR TRAITEMENT STATISTIQUES DE VENTES            *    00002300
      *------------------------------------------------------------*    00002400
      *                  FICHIER SAM, LONGUEUR 52                  *    00002500
      **************************************************************    00002600
      *                                                                 00002700
       01  ENR-FHV000.                                                  00002800
      *-------------------------------------  CODE SOCIETE              00002900
             02 FHV000-NSOCIETE      PIC X(03).                         00003000
      *-------------------------------------  CODE LIEU                 00003100
             02 FHV000-NLIEU         PIC X(03).                         00003200
      *-------------------------------------  CODE ARTICLE              00003300
             02 FHV000-NCODIC        PIC X(07).                         00003400
      *-------------------------------------  CODE MODE DE DELIVRANCE   00003900
             02 FHV000-CMODDEL       PIC X(03).                         00004000
      *-------------------------------------  DATE SAISIE VENTE         00004100
             02 FHV000-DOPER         PIC X(08).                         00004200
      *-------------------------------------  QUANTITE MOUVEMENTEE      00004300
             02 FHV000-QMVT          PIC S9(07) COMP-3.                 00004400
      *-------------------------------------  CHIFFRE D AFFAIRE         00004500
             02 FHV000-CA            PIC S9(9)V99 COMP-3.               00004600
      *-------------------------------------  CODE INSEE                00004610
             02 FHV000-CINSEE        PIC X(05).                         00004620
      *-------------------------------------  DATE DE TRAITEMENT        00004700
             02 FHV000-DTRT          PIC X(08).                         00004800
      *-------------------------------------  CODE FAMILLE              00004900
             02 FHV000-CFAM          PIC X(05).                         00005001
      ***************************************************************** 00006000
                                                                                
