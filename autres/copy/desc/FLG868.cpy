      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FLG868  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : REAPPROVISIONNEMENT FOURNISSEUR             *         
      *                                                               *         
      *    FLG868       : FICHIER ARTICLE GROUPE, ELEMENT ET SIMILAIRE*         
      *                                                               *         
      *    LONGUEUR ENR.:  20 C                                       *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *                                                               *         
      * CAS DES CODICS SIMILAIRES WGRP = 'S'                          *         
      *     LE NCODIC2 EST LE CODIC AYANT LE PLUS DE VENTE            *         
      * (DONN�ES EXTRAITES DE LA TABLE RTHV04 - FICHIER FGP765). LE   *         
      * FLAG WDEB EST RENSEIGNE A ' ', POUR LES AUTRES ILS SONT A 'N' *         
      * IL Y A AUTANT DE NCODIC QUE DE CODICS SIMILAIRES              *         
      *                                                               *         
      * CAS DES CODICS GROUPE WGRP = 'D','I','C' OU 'E'               *         
      *          - I CODIC GROUPE INDISSOCIABLE                       *         
      *          - D CODIC GROUPE DISSOCIABLE                         *         
      *          - E CODIC ELEMENT INDISSOCIABLE                      *         
      *          - C CODIC ELEMENT DISSOCIABLE                        *         
      *    POUR UN CODIC GROUPE (G) CONSTITUE DE DEUX ELEMENTS (E1 ET *         
      * E2) ON AURA LES COMBINAISONS SUIVANTES DANS LE FICHIER :      *         
      *              . G - G                                          *         
      *              . G - E1                                         *         
      *              . G - E2                                         *         
      *              . E1 - E1  (AVEC WGRP = ' ')                     *         
      *              . E2 - E2  (AVEC WGRP = ' ')                     *         
      * LES DEUX DERNIERES COMBINAISONS SERONT ECRITES DANS LE FICHIER*         
      * SI ET SEULEMENT SI L'ELEMENT N'EST PAS UN CODIC SIMILAIRE     *         
      *****************************************************************         
      *                                                               *         
       01  FLG868-ENREG.                                                        
         02 FLG868-DATA-ARTICLES-LIES.                                          
           05  FLG868-NCODIC2        PIC X(07) .                                
           05  FLG868-NCODIC         PIC X(07) .                                
           05  FLG868-WDEB           PIC X(01) .                                
           05  FLG868-WGRP           PIC X(01) .                                
           05  FLG868-QTYPLIEN       PIC S9(3) COMP-3.                          
           05  FILLER                PIC X(02) .                                
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
