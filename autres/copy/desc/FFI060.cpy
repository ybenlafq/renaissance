      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FIC60-ENR.                                                           
           03 FIC60-CODAPP        PIC X(010) VALUE SPACES.                      
           03 FIC60-NIDFIC        PIC X(010) VALUE SPACES.                      
           03 FIC60-OPCO          PIC X(006) VALUE SPACES.                      
           03 FIC60-TYPMVT        PIC X(015) VALUE SPACES.                      
           03 FIC60-NORIGINE      PIC X(020) VALUE SPACES.                      
           03 FIC60-DATMVT        PIC X(008) VALUE SPACES.                      
           03 FIC60-DHMSVT        PIC X(006) VALUE SPACES.                      
           03 FIC60-NSOCORIG      PIC X(003) VALUE SPACES.                      
           03 FIC60-NLIEUORIG     PIC X(003) VALUE SPACES.                      
           03 FIC60-NSOCDEST      PIC X(003) VALUE SPACES.                      
           03 FIC60-NLIEUDEST     PIC X(003) VALUE SPACES.                      
           03 FIC60-NSOCVTE       PIC X(003) VALUE SPACES.                      
           03 FIC60-NLIEUVTE      PIC X(003) VALUE SPACES.                      
           03 FIC60-NSEQ          PIC X(002) VALUE SPACES.                      
           03 FIC60-NARTICLE      PIC 9(007) VALUE ZERO.                        
           03 FIC60-REFERENCE     PIC X(040) VALUE SPACES.                      
           03 FIC60-LIBELLE       PIC X(040) VALUE SPACES.                      
           03 FIC60-QMVT          PIC 9(007) VALUE ZERO.                        
           03 FIC60-CMODDEL       PIC X(005) VALUE SPACES.                      
           03 FIC60-NIDLOG        PIC X(015) VALUE SPACES.                      
           03 FIC60-PTTC          PIC 9(007)V99 VALUE ZERO.                     
           03 FIC60-PHT           PIC 9(007)V99 VALUE ZERO.                     
           03 FIC60-TVA           PIC 9(004) VALUE ZERO.                        
           03 FIC60-DCREAT        PIC X(008) VALUE SPACES.                      
           03 FIC60-CHAMP1        PIC X(020) VALUE SPACES.                      
           03 FIC60-CHAMP2        PIC X(020) VALUE SPACES.                      
           03 FIC60-CHAMP3        PIC X(020) VALUE SPACES.                      
           03 FIC60-CHAMP4        PIC X(020) VALUE SPACES.                      
           03 FIC60-CHAMP5        PIC X(020) VALUE SPACES.                      
           03 FILLER              PIC X(059) VALUE SPACES.                      
           03 FIC60-FLAG          PIC X(001) VALUE SPACES.                      
                                                                                
