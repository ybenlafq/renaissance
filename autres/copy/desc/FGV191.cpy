      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FGV191 D'EXTRACTION DES           *00030001
      *       VENTES DE CREDITS, TRIEES, CUMULEES PAR VENDEUR          *00040001
      *       FICHIER EN SORTIE DE BGV190 ET DESTINE HIT-VENDEURS      *00041001
      *       LONGUEUR D'ENREGISTREMENT : 160 C.                       *00050001
      *================================================================*00060000
      *                                                                 00070000
       01  FILLER.                                                      00080000
      *                                                                 00090000
           05  FGV191-ENREG.                                            00100001
               10  FGV191-NSOCIETE       PIC  X(03).                    00110001
               10  FGV191-NLIEU          PIC  X(03).                    00120001
               10  FGV191-LLIEU          PIC  X(15).                    00130001
               10  FGV191-CVENDEUR       PIC  X(06).                    00140001
               10  FGV191-MATRICUL       PIC  X(07).                    00141001
               10  FGV191-LVENDEUR       PIC  X(15).                    00150001
               10  FILLER                PIC  X.                        00151001
               10  FGV191-NBR-GRA        PIC  9(04).                    00155001
               10  FGV191-CA-GRA         PIC  9(8)V99.                  00156001
               10  FGV191-PCT-GRA        PIC  999V99.                   00157001
               10  FILLER                PIC  X.                        00157101
               10  FGV191-NBR-PAY        PIC  9(04).                    00157201
               10  FGV191-CA-PAY         PIC  9(8)V99.                  00157301
               10  FGV191-PCT-PAY        PIC  999V99.                   00157401
               10  FILLER                PIC  X.                        00157501
               10  FGV191-NBR-COMP       PIC  9(04).                    00157601
               10  FGV191-CA-COMP        PIC  9(8)V99.                  00157701
               10  FGV191-PCT-COMP       PIC  999V99.                   00157801
               10  FILLER                PIC  X.                        00157901
               10  FGV191-NBR-REU        PIC  9(04).                    00158001
               10  FGV191-CA-REU         PIC  9(8)V99.                  00158101
               10  FGV191-PCT-REU        PIC  999V99.                   00158201
               10  FILLER                PIC  X.                        00158301
               10  FGV191-NBR-TOT        PIC  9(04).                    00158401
               10  FGV191-CA-TOT         PIC  9(8)V99.                  00158501
               10  FGV191-PCT-TOT        PIC  999V99.                   00158601
               10  FILLER                PIC  X.                        00158701
               10  FGV191-DDEBPER        PIC  X(08).                    00158801
               10  FGV191-DFINPER        PIC  X(08).                    00159001
               10  FILLER                PIC  X(14).                    00160001
      *                                                                 00460000
                                                                                
