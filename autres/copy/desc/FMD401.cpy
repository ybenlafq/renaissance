      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ---------------------------------------------------------- *            
      * LONGUEUR TOTALE   : 200 CARACTèRES                         *            
      * LONGUEUR UTILISEE : 117 CARACTERES                         *            
      * ---------------------------------------------------------- *            
       01  ENREG-FMD401.                                                        
           10 FMD401-NSOCIETE      PIC X(03).                                   
           10 FMD401-NMAG          PIC X(03).                                   
           10 FMD401-LNMAG         PIC X(20).                                   
           10 FMD401-NLIEUEXT      PIC X(03).                                   
           10 FMD401-DMVT          PIC X(08).                                   
           10 FMD401-COPER         PIC X(10).                                   
           10 FMD401-LOPER         PIC X(20).                                   
           10 FMD401-CFAM          PIC X(05).                                   
           10 FMD401-WSEQFAM       PIC S9(05) COMP-3.                           
           10 FMD401-CMARQ         PIC X(05).                                   
           10 FMD401-NCODIC        PIC X(07).                                   
           10 FMD401-LREFFOURN     PIC X(20).                                   
           10 FMD401-QMVT          PIC S9(5)V     USAGE COMP-3.                 
LP         10 FMD401-NORIGINE      PIC X(07).                                   
           10 FILLER               PIC X(083).                                  
                                                                                
