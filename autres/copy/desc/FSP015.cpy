      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER FSP015                                       *        
      *                                                                *        
      *  ISSU DU BSP010                                                *        
      *                                                                *        
      *          CRITERES DE TRI  14,10,CH,A,  MARQ, FAM               *        
      *                           01,06,CH,A,  SOCEMET, SOCRECEP       *        
      *                           79,07,CH,A,  CODIC                   *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-FSP015.                                                        
1          10 FSP015-NSOCEMET           PIC X(03).                      007  003
4          10 FSP015-NSOCRECEP          PIC X(03).                      010  003
7          10 FSP015-NUMFACT            PIC X(07).                      013  007
14         10 FSP015-CMARQ              PIC X(05).                      020  005
19         10 FSP015-CFAM               PIC X(05).                      025  005
24         10 FSP015-LSOCEMET           PIC X(25).                      032  025
49         10 FSP015-LSOCRECEP          PIC X(25).                      057  025
74         10 FSP015-MOISTRT            PIC X(05).                      082  005
79         10 FSP015-NCODIC             PIC X(07).                      087  007
86         10 FSP015-REFERENCE          PIC X(20).                      094  020
           10 FSP015-TEBRB.                                             094  020
106           15 FSP015-TLMELA          PIC X(03).                              
109           15 FSP015-BLBR            PIC X(02).                              
111        10 FSP015-TYPFACT            PIC X(01).                      114  001
112        10 FSP015-DATEFACT           PIC X(08).                      131  006
120        10 FSP015-QTE                PIC S9(07)      COMP-3.         115  004
124        10 FSP015-VALOPRMP           PIC S9(15)V9(2) COMP-3.         119  006
133        10 FSP015-VALOSRP            PIC S9(15)V9(2) COMP-3.         125  006
142        10 FSP015-TXTVA              PIC S9(03)V9(2) COMP-3.         125  006
145        10 FILLER                    PIC X(06).                              
      *                                                                         
                                                                                
LG=150                                                                          
