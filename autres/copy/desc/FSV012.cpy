      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *===============================================================* 00000690
      *                 DESCRIPTION DE FSV012                         * 00000700
      *===============================================================* 00000710
      *                                                                 00001230
       01  WW-FSV012.                                                   00000890
         03  FSV012-NOMPGM           PIC  X(10) VALUE 'FSV012    ' .    00000900
         03  FSV012-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV012.                                                  00000890
           05  FSV012-CFAM             PIC  X(05).                      00000900
           05  FSV012-LFAM             PIC  X(20).                      00001090
           05  FSV012-CTAUXTVA         PIC  X(05).                      00001100
           05  FSV012-CGARANTIE        PIC  X(05).                      00001020
           05  FSV012-WMULTIFAM        PIC  X(01).                      00001020
           05  FSV012-CTYPENT          PIC  X(01).                      00001020
           05  FSV012-WSEQFAM          PIC  X(05).                      00001020
         03  FSV012-FILLEUR          PIC  X(117)  VALUE SPACES.         00001020
         03  FSV010-CLE-TRI      .                                              
           05  FSV012-CLE-XXX          PIC  X(30)  VALUE SPACES.           00001
                                                                                
