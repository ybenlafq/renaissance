      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
      * DARTY.COM : EXTRACTION DES LIGNES D'ENCAISSEMENT DES VENTES EN  00000020
      *             ECART DE CAISSE                                             
      ****************************************************************  00000030
      *                                                                 00000030
      * 12/10/10 : CREATION                                             00000030
      *                                                                 00000030
      ****************************************************************  00000030
       01     FEC01001-FIC-EXTRACT.                                             
           05 FEC01001-CMODPAIMT   PIC X(15).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-DMODIFVTE   PIC X(08).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-HMODIFVTE   PIC X(05).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-NUM-CMD-WS  PIC X(18).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-NUM-CMD-SI  PIC X(13).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-MT-A-ENC    PIC X(10).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-IDENT-CCID  PIC X(10).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-VENTE-INEX  PIC X(10).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-PRES-RD2-NT PIC X(10).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-COMMENTAIRE.                                             
              10 FEC01001-COMMENT-1 PIC X(30).                                  
              10 FEC01001-COMMENT-2 PIC X(30).                                  
              10 FEC01001-COMMENT-3 PIC X(30).                                  
              10 FEC01001-COMMENT-4 PIC X(30).                                  
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC1      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC2      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC3      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC4      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC5      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC6      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC7      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC8      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC9      PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC10     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC11     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC12     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC13     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC14     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC15     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC16     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC17     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC18     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC19     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01001-CODIC20     PIC X(07).                                   
           05 FILLER               PIC X(01) VALUE ';'.                         
                                                                                
