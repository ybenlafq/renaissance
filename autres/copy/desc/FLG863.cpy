      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FLG863  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : REAPPROVISIONNEMENT FOURNISSEUR             *         
      *                                                               *         
      *    FLG863       : FICHIER ARTICLE PAR FILIALE                 *         
      *                                                               *         
      *    LONGUEUR ENR.:  305 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  :FGPXXX:-ENREG.                                                      
         02 :FGPXXX:-DATAGRP.                                                   
      *                                                       1       3         
           05  :FGPXXX:-NSOCIETE     PIC X(03) .                                
      *                                                       4       3         
           05  :FGPXXX:-NSOCDEPOT    PIC X(03) .                                
      *                                                       7       3         
           05  :FGPXXX:-NDEPOT       PIC X(03) .                                
      *                                                      10       7         
           05  :FGPXXX:-NCODIC       PIC X(07) .                                
      *                                                      17       3         
           05  :FGPXXX:-WSEQFAM      PIC S9(5) COMP-3.                          
      *                                                      20       5         
           05  :FGPXXX:-CFAM         PIC X(05) .                                
      *                                                      25      20         
           05  :FGPXXX:-LFAM         PIC X(20) .                                
      *                                                      45       5         
           05  :FGPXXX:-CMARQ        PIC X(05) .                                
      *                                                      50      20         
           05  :FGPXXX:-LMARQ        PIC X(20) .                                
      *                                                      70      20         
           05  :FGPXXX:-LREFFOURN    PIC X(20) .                                
      *                                                      90       1         
           05  :FGPXXX:-LIEN         PIC X(01) .                                
      *                                                      91       2         
           05  :FGPXXX:-NBLIEN       PIC 9(02) .                                
      *                                                      93       4         
           05  :FGPXXX:-QPOIDS       PIC S9(7) COMP-3.                          
      *                                                      97       2         
           05  :FGPXXX:-QLARGEUR     PIC S9(3) COMP-3.                          
      *                                                      99       2         
           05  :FGPXXX:-QPROFONDEUR  PIC S9(3) COMP-3.                          
      *                                                     101       2         
           05  :FGPXXX:-QHAUTEUR     PIC S9(3) COMP-3.                          
      *                                                     103       3         
           05  FILLER                PIC X(03) .                                
         02 :FGPXXX:-DATAFILIALE-P.                                             
      *                                                     106       2         
           05  :FGPXXX:-NAGREGATED-P PIC X(02) .                                
      *                                                     108      20         
           05  :FGPXXX:-LAGREGATED-P PIC X(20) .                                
      *                                                     128       5         
           05  :FGPXXX:-CHEFPRM-P    PIC X(05) .                                
      *                                                     133       5         
           05  :FGPXXX:-CHEFPROD-P   PIC X(05) .                                
      *                                                     138      20         
           05  :FGPXXX:-LCHEFPROD-P  PIC X(20) .                                
      *                                                     158       5         
           05  :FGPXXX:-CEXPO-P      PIC X(05) .                                
      *                                                     163       3         
           05  :FGPXXX:-LSTATCOMP-P  PIC X(03) .                                
      *                                                     166       1         
           05  :FGPXXX:-WOA-P        PIC X(01) .                                
      *                                                     167       5         
           05  :FGPXXX:-PSTDTTC-P    PIC S9(07)V9(02) COMP-3 .                  
      *                                                     172       5         
           05  :FGPXXX:-PSTDTTC-REF  PIC S9(07)V9(02) COMP-3 .                  
      *                                                     177       7         
           05  :FGPXXX:-PRMP-P       PIC S9(07)V9(06) COMP-3 .                  
      *                                                     184       7         
           05  :FGPXXX:-RAYON-P      PIC X(07) .                                
      *                                                     191       5         
           05  :FGPXXX:-TEBRB-P      PIC X(05) .                                
      *                                                     196      13         
           05  FILLER                PIC X(13) .                                
         02 :FGPXXX:-DATAFILIALE.                                               
      *                                                     209       5         
           05  :FGPXXX:-CHEFPRM      PIC X(05) .                                
      *                                                     214       5         
           05  :FGPXXX:-CHEFPROD     PIC X(05) .                                
      *                                                     219       5         
           05  :FGPXXX:-CASSORT      PIC X(05) .                                
      *                                                     224       1         
           05  :FGPXXX:-WDACEM       PIC X(01) .                                
      *                                                     225       5         
           05  :FGPXXX:-CEXPO        PIC X(05) .                                
      *                                                     230       3         
           05  :FGPXXX:-LSTATCOMP    PIC X(03) .                                
      *                                                     233       5         
           05  :FGPXXX:-PSTDTTC      PIC S9(07)V9(02) COMP-3 .                  
      *                                                     238       7         
           05  :FGPXXX:-PRMP         PIC S9(07)V9(06) COMP-3 .                  
      *                                                     245       1         
           05  :FGPXXX:-WOA          PIC X(01) .                                
      *                                                     246       3         
           05  :FGPXXX:-CGESTVTE     PIC X(03) .                                
      *                                                     249      10         
           05  :FGPXXX:-CODPROF      PIC X(10) .                                
      *                                                     259      05         
           05  :FGPXXX:-CAPPRO       PIC X(05) .                                
      *                                                     264       8         
           05  :FGPXXX:-DEFSTATUT    PIC X(08) .                                
      *                                                     272       1         
           05  :FGPXXX:-WSENSAPPRO   PIC X(01) .                                
      *                                                     273      33         
           05  FILLER                PIC X(33) .                                
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
