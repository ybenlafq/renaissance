      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * FFP000 : FICHIER CREE PAR BFP000                               *        
      * FACTURES A REGLER PAR PACIFICA  ET                             *        
      * FICHIER DES FACTURES REJETEES PAR PACIFICA                     *        
      * LONGUEUR = 600                                                 *        
      ******************************************************************        
      *                                                                         
      *--------------ENREGISTREMENT ENTETE = '001'                              
      *                                                                         
       01 ENR-FFP000.                                                           
          03 ENR-FFP000-001.                                                    
      *- '001'                                                                  
           05 FFP000-TYPENR-001    PIC  X(03).                                  
           05 FFP000-NLOT          PIC  9(06).                                  
      *-DATE ET HEURE EMISSION DU LOT ISSUS DATE SYSTEME                        
           05 FFP000-DEMISS        PIC  X(08).                                  
           05 FFP000-HEMISS        PIC  X(06).                                  
      *- 'EUR'                                                                  
           05 FFP000-CDEVISE       PIC  X(03).                                  
      *- '01 '                                                                  
           05 FFP000-CEMETTEUR     PIC  X(03).                                  
      *- 'DAR'                                                                  
           05 FFP000-CDEST         PIC  X(03).                                  
      *- '908'                                                                  
           05 FFP000-SCT           PIC  X(03).                                  
      *- 'RE'                                                                   
           05 FFP000-TYPE-FIC      PIC  X(02).                                  
           05 FILLER               PIC  X(563).                                 
      *                                                                         
      *------ENREGISTREMENT FACTURE 002                                         
      *                                                                         
           03 ENR-FFP000-002 REDEFINES  ENR-FFP000-001.                         
      *- '002'                                                                  
              05 FFP000-TYPENR-002    PIC  X(03).                               
      *--  'DARTY'                                                              
              05 FFP000-NOMFOUR-002   PIC  X(17).                               
              05 FFP000-NFACTURE-002  PIC  X(15).                               
              05 FFP000-NSINISTRE     PIC  X(18).                               
              05 FFP000-DSINISTRE     PIC  X(08).                               
              05 FILLER               PIC  X(12).                               
              05 FILLER               PIC  X(15).                               
              05 FFP000-NOM           PIC  X(32).                               
              05 FFP000-CPOSTAL       PIC  X(05).                               
              05 FILLER               PIC  X(15).                               
              05 FILLER              PIC  X(03).                                
              05 FILLER               PIC  X(03).                               
              05 FFP000-DFACTURE      PIC  X(08).                               
      *-- REFERENCE DU BORDEREAU                                                
      *---JJMMSSAA DE FDATE                                                     
              05 FFP000-FDATE         PIC  X(08).                               
              05 FILLER               PIC  X(15).                               
              05 FILLER               PIC  X(15).                               
      *- 'P'                                                                    
              05 FFP000-CTYPFAC       PIC  X(01).                               
      *- 'SAV' OU 'RA9'                                                         
              05 FFP000-CTYPINTER     PIC  X(03).                               
      *- 'ZCO' OU 'HZO'  OU  'DEP'                                              
              05 FFP000-CLIEUINTER    PIC  X(03).                               
              05 FILLER               PIC X(04) OCCURS 5.                       
              05 FILLER               PIC X(70) OCCURS 3.                       
              05 FFP000-WCARTE        PIC X(01).                                
              05 FILLER               PIC X(14).                                
              05 FILLER               PIC 9(09)V99.                             
      *-  CALCUL                                                                
              05 FFP000-MTTTC        PIC 9(09)V99.                              
              05 FILLER              PIC 9(09)V99.                              
      *-  'O' OU 'N' OU ' '                                                     
              05 FFP000-WFRANCHISE   PIC X(01).                                 
              05 FFP000-MTFRANCHISE  PIC 9(05)V99.                              
              05 FILLER              PIC 9(05)V99.                              
              05 FILLER              PIC 9(09)V99.                              
              05 FFP000-TXREMISE     PIC 9(03)V99.                              
      *- 'N'                                                                    
              05 FFP000-RECUPTVA     PIC X(01).                                 
              05 FILLER              PIC 9(03)V99.                              
              05 FFP000-MTLIVR       PIC 9(09)V99.                              
              05 FILLER              PIC X(37).                                 
      *--------PARTIE RETOUR DE PACIFICA                                        
              05 FFP000-CREJET       PIC X(03).                                 
              05 FFP000-DONNEE       PIC X(05).                                 
              05 FFP000-LREJET       PIC X(30).                                 
      *                                                                         
      *-----ENREGISTREMENT REPARATION                                           
           03 ENR-FFP000-003 REDEFINES  ENR-FFP000-001.                         
      *- '003'                                                                  
             05 FFP000-TYPENR-003    PIC  X(03).                                
      *- 'DARTY'                                                                
             05 FFP000-NOMFOUR-003   PIC  X(17).                                
             05 FFP000-NFACTURE-003  PIC  X(15).                                
             05 FFP000-REPARATION-SUP OCCURS 5.                                 
                07 FILLER            PIC  X(03).                                
                07 FFP000-NATURE     PIC  X(03).                                
                07 FFP000-MARQUE     PIC  X(20).                                
                07 FFP000-CODIC      PIC  X(15).                                
                07 FFP000-FOUR-CODIC PIC  X(15).                                
                07 FFP000-CDIAGNO    PIC  X(05).                                
                07 FFP000-MTREMISE-003   PIC  9(09)V99.                         
                07 FFP000-MTCLIENT   PIC  9(09)V99.                             
                07 FILLER            PIC  9(03)V99.                             
      *-- MONTANT EXTENSION DE GARANTIE A 0 POUR L'INSTANT                      
                07 FFP000-EXT-GAR    PIC  9(09)V99.                             
                07 FFP000-MTTTC-003  PIC  9(09)V99.                             
           05 FILLER               PIC  X(15).                                  
      *                                                                         
      *--------------ENREGISTREMENT TOTAL                                       
      *                                                                         
           03 ENR-FFP000-999 REDEFINES  ENR-FFP000-001.                         
      *-FORCE A '999'                                                           
             05 FFP000-TYPENR-999    PIC  X(03).                                
             05 FFP000-NLOT-999      PIC  9(06).                                
             05 FFP000-DEMISS-999    PIC  X(08).                                
             05 FFP000-HEMISS-999    PIC  X(06).                                
             05 FFP000-CDEVISE-999   PIC  X(03).                                
             05 FFP000-CEMETTEUR-999 PIC  X(03).                                
             05 FFP000-CDEST-999     PIC  X(03).                                
             05 FFP000-SCT-999       PIC  X(03).                                
             05 FFP000-NB-FACTURES   PIC  9(05).                                
             05 FFP000-MTTTC-999     PIC  9(11)V99.                             
             05 FILLER               PIC  X(547).                               
                                                                                
