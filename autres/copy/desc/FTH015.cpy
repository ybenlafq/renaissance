      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER FTH015 QUI PERMET D'ALIMENTER                *        
      *  LA TABLE RTTH15 (DETAIL DES TAXES)                            *        
      *----------------------------------------------------------------*        
       01  DSECT-:FTH015:.                                                      
         05 CHAMPS-:FTH015:.                                                    
           10 :FTH015:-CLE.                                             122  005
      *                                               POS 1            *        
              15 :FTH015:-NCODIC        PIC X(07).                      122  005
      *                                               POS 8            *        
              15 :FTH015:-COPCO         PIC X(03).                      122  005
      *                                               POS 11           *        
              15 :FTH015:-CTAXE         PIC X(05).                      094  006
      *                                                                         
           10 :FTH015:-DONNEES.                                         094  006
      *                                               POS 16           *        
              15 :FTH015:-MTT           PIC S9(5)V9(2) USAGE COMP-3.    197  007
      *                                               POS 20           *        
              15 FILLER                 PIC X(30) VALUE SPACES.         197  007
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FTH015J-LONG          PIC S9(4)   COMP  VALUE +50.             
      *                                                                         
      *--                                                                       
       01  DSECT-:FTH015:-LONG          PIC S9(4) COMP-5  VALUE +50.            
                                                                                
      *}                                                                        
