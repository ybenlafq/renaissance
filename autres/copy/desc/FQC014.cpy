      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *   COPY DU FICHIER POUR BQC171 : EXTRACTION CARTES T 5                   
      *   EXTRACTION DE VENTES                                                  
      *                                                                         
      * REMARQUES :                                                             
      *                                                                         
      * ---> LONGUEUR ARTICLE = 100                                             
      *                                                                         
      * ---> CTYPE                                                              
      *       A = INFOS NB DE VENTES PAR PLATEFORME                             
      *       B = LIGNES VENTES DE LA PLATEFORME                                
      * ---> NVENTE                                                             
      *       LIGNE A --->  = 0                                                 
      *       LIGNE B ---> <> 0                                                 
      * ---> NBVENTE                                                            
      *       LIGNE A ---> <> 0                                                 
      *       LIGNE B --->  = 0                                                 
      ******************************************************************        
      *                                                                         
       01  DSECT-FQC014.                                                        
           10  FQC014-NSOCGEST       PIC X(03).                                 
           10  FQC014-NLIEUGEST      PIC X(03).                                 
           10  FQC014-CTYPE          PIC X(01).                                 
           10  FQC014-NBVENTE        PIC S9(7) COMP-3.                          
           10  FQC014-NVENTE         PIC X(07).                                 
           10  FQC014-DDELIV         PIC X(08).                                 
           10  FQC014-DVENTE         PIC X(08).                                 
           10  FQC014-NCODIC         PIC X(07).                                 
           10  FQC014-CVENDEUR       PIC X(06).                                 
           10  FQC014-CINSEE         PIC X(05).                                 
           10  FQC014-CPROTOUR       PIC X(05).                                 
           10  FQC014-CTOURNEE       PIC X(08).                                 
           10  FQC014-CEQUIPE        PIC X(05).                                 
           10  FQC014-CMODDEL        PIC X(03).                                 
           10  FQC014-WCREDIT        PIC X(01).                                 
           10  FQC014-CPOSTAL        PIC X(05).                                 
           10  FQC014-NSOCIETE       PIC X(03).                                 
           10  FQC014-NLIEU          PIC X(03).                                 
           10  FQC014-CPLAGE         PIC X(02).                                 
           10  FQC014-DSYST          PIC S9(13) COMP-3.                         
           10  FQC014-FILLER         PIC X(12).                                 
                                                                                
