      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  WW-FSV025.                                                   00000890
         03  FSV025-NOMPGM  .                                           00000900
           05  FSV025-NOMPROG          PIC  X(10) VALUE 'FSV025    ' .  00000900
         03  FSV025-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV025.                                                  00000890
           05  FSV025-NCODIC           PIC  X(07).                      00000900
           05  FSV025-PRIX .                                            00000900
      *        SSTABLE TXTVA                                            00001320
              10  FSV025-VALTVA           PIC  ---9V99.                    00000
      *        GN59-PREFTTC                                             00001320
              10  FSV025-PXNATTTC         PIC  -------9V99.                00001
      *                                                                 00001320
              10  FSV025-PXNATHT          PIC  -------9V99.                00001
      *                                                                 00001320
              10  FSV025-PXCESSTTC        PIC  -------9V99.                00001
      *        GG50-PRMPRECYCL   = PRMP (OR MULTIFAMILLES)              00001320
              10  FSV025-PXCESSHT         PIC  -------9V99.                00001
      *                                                                 00001320
              10  FSV025-ECOTTC           PIC  ----9V99.                   00001
      *                                                                 00001320
              10  FSV025-ECOHT            PIC  ----9V99.                   00001
      *        GG50-PCF  = SRP                                          00001320
              10  FSV025-SRPTTC           PIC  -------9V99.                00001
      *                                                                 00001320
              10  FSV025-SRPHT            PIC  -------9V99.                00001
      * BIDOUILLE POUR D�CALER LES DONN�ES                                      
              10  FSV025-BSV200           PIC  X(45)  VALUE SPACES.     00001020
      *                                                                         
              10  FSV025-PRMP             PIC  -------9V99.                00001
              10  FSV025-LIVRABLE         PIC  X(01).                      00001
              10  FSV025-EXPEDIABLE       PIC  X(01).                      00001
              10  FSV025-CIBLEB2B         PIC  X(01).                      00001
          03  FSV025-FILLEUR          PIC  X(044)  VALUE SPACES.        00001020
      *                                                                 00001320
                                                                                
