      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FRB000-RECORD.                                               00000480
           05  FRB000-NFOURNISSEUR    PIC X(25).                        00000480
           05  FRB000-LREFFOURN       PIC X(25).                        00000480
           05  FRB000-LREFCOMM        PIC X(20).                        00000480
           05  FRB000-NSERIE          PIC X(15).                        00000480
           05  FRB000-ADRMAC          PIC X(12).                        00000480
           05  FRB000-NHARDWARE       PIC X(9).                         00000480
           05  FRB000-NFIRMWARE       PIC X(6).                         00000480
      *    05  FRB000-NCOMMANDE       PIC 9(9).                         00000480
           05  FRB000-NCOMMANDE       PIC 9(7).                         00000480
           05  FRB000-DCOMMANDE       PIC X(8).                         00000480
           05  FRB000-DPRODUCTION     PIC X(8).                         00000480
           05  FRB000-NLOTPROD        PIC X(15).                        00000480
           05  FRB000-NPALETTE        PIC X(15).                        00000480
           05  FRB000-LLGRECEPTION    PIC X(13).                        00000480
           05  FRB000-NEAN            PIC X(13).                        00000480
JC0806*    05  FRB000-CLOGIN          PIC X(15).                        00000480
JC0806*    05  FRB000-CPASSE          PIC X(15).                        00000480
JC0806     05  FRB000-CLOGIN          PIC X(16).                        00000480
JC0806     05  FRB000-CPASSE          PIC X(16).                        00000480
           05  FRB000-LCONSTRUCTEUR   PIC X(25).                        00000480
           05  FRB000-NBL             PIC 9(10).                        00000480
           05  FRB000-TEQUIPEMENT     PIC X(5).                         00000480
240608     05  FRB000-ADRMAC2         PIC X(12).                        00000480
      *        FRB000-FILEUR  :                                         00000480
      * CE CHAMPS EST UTILISE DE FACON PROVISOIR POUR LES LIB           00000600
      * ANOMALIE ON PEUT TOUJOURS LE DECALER SI BESOIN EST              00000600
JC0806*    05  FRB000-FILLER          PIC X(52).                        00000480
240608     05  FRB000-FILLER          PIC X(38).                        00000480
      *                                                                 00000600
                                                                                
