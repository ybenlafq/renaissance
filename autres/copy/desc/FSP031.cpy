      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *  DSECT DU FICHIER FSP031: VALO DE L'ECART SRP/PRMP, EN CUMUL  *         
      *  PAR MOIS, PAR MARQUE, SOC EMETTRICE/RECEPTRICE               *         
      *  NB : SI MOISTRT = '000000' --> ON A UNE REPARTITION PAR      *         
      *          FLAG TEBRB (CUMUL JUSQU'AU MOIS TRAITE)              *         
      *       SI MOISTRT = SSAAMM  --> CUMUL RECAP SUR UN MOIS        *         
      *          -> FLAG TEBRB = ' '                                  *         
      *   CE FICHIER SERA A TRIER PAR NSOCEMET, NSOCRECEP, MOISTRT,   *         
      *   TEBRB, TXTVA, AVEC UN SUM DES MONTANTS                      *         
      *****************************************************************         
      *                                                                         
       01 DSECT-FSP031.                                                         
          02 FSP031-ENTETE.                                                     
001          05 FSP031-NSOCEMET         PIC X(03).                              
004          05 FSP031-NSOCRECEP        PIC X(03).                              
007          05 FSP031-MOISTRT          PIC X(06).                              
             05 FSP031-TEBRB.                                                   
013             07 FSP031-TLMELA        PIC X(03).                              
016             07 FSP031-BLBR          PIC X(02).                              
018          05 FSP031-TXTVA            PIC S9(03)V9(2) COMP-3.                 
021          05 FILLER                  PIC X(05).                              
      *                                                                         
          02 FSP031-CHAMPS.                                                     
026          05 FSP031-VALOSRP          PIC S9(15)V9(2) COMP-3.                 
035          05 FSP031-VALOPRMP         PIC S9(15)V9(2) COMP-3.                 
044          05 FSP031-MTTVA            PIC S9(15)V9(2) COMP-3.                 
053          05 FILLER                  PIC X(08).                              
      *                                                                         
                                                                                
LG=060                                                                          
