      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************           
      *   FICHIER DES MOUVEMENTS COMPTABLES ISSUS DES INTERFACES    *           
      *              - NOUVELLE DSECT STANDARD GCT -                *           
      *                                                             *           
      *      - DSECT COBOL                                          *           
      *      - NOM = FFTV01                                         *           
      *      - RECSIZE = 200                                        *           
      *                                                             *           
      ***************************************************************           
       01  FFTV01-ENR.                                                          
      ***************************************************************           
      * TRI DE CE FICHIER : SUR LES 17 PREMIERES POSITIONS          *           
      *                   + LA DEVISE EN POSITION 186 SUR  3        *           
      *                   + LE NUMERO DE PIECE EN  18 SUR 10        *           
      ***************************************************************           
           02  FFTV01-CLE.                                                      
   1           03  FFTV01-NSOC                     PIC X(05).                   
   6           03  FFTV01-NETAB                    PIC X(03).                   
   9           03  FFTV01-NJRN                     PIC X(03).                   
  12           03  FILLER                          PIC X(01).                   
  13           03  FFTV01-CINTERFACE               PIC X(05).                   
      *------> DEVISE                                                           
  18           03  FFTV01-NPIECE                   PIC X(10).                   
      ***************************************************************           
  28       02  FFTV01-NATURE                       PIC X(03).                   
  31       02  FFTV01-REFDOC                       PIC X(12).                   
  43       02  FFTV01-DFTI00                       PIC X(08).                   
  51       02  FFTV01-NCOMPTE                      PIC X(06).                   
  57       02  FFTV01-NSSCOMPTE                    PIC X(06).                   
  63       02  FFTV01-NSECTION                     PIC X(06).                   
  69       02  FFTV01-NRUBR                        PIC X(06).                   
  75       02  FFTV01-NSTEAPP                      PIC X(05).                   
  80       02  FFTV01-NTIERSCV                     PIC X(08).                   
  88       02  FFTV01-NLETTRAGE                    PIC X(10).                   
  98       02  FFTV01-LMVT                         PIC X(25).                   
 123       02  FFTV01-PMONTMVT                     PIC 9(13)V99  COMP-3.        
 131       02  FFTV01-CMONTMVT                     PIC X(01).                   
 132       02  FFTV01-DMVT                         PIC X(08).                   
 140       02  FFTV01-NOECS                        PIC X(05).                   
      * CATEGORIE DE CHARGE ET NATURE DE CHARGE                                 
 145       02  FFTV01-CATCH                        PIC X(01).                   
 146       02  FFTV01-NATCH                        PIC X(06).                   
      * DATE D'ECHEANCE ET BON A PAYER                                          
 152       02  FFTV01-DECH                         PIC X(08).                   
      * BON A PAYER                                                             
 160       02  FFTV01-WBAP                         PIC X(01).                   
      * CONTROLE CENTRALISE DES TRAITEMENTS :                                   
      * -> NOM DE PROGRAMME ET DATE DE TRAITEMENT                               
 161       02  FFTV01-CNOMPROG                     PIC X(08).                   
 169       02  FFTV01-DTRAITEMENT                  PIC X(08).                   
      * ICS                                                                     
 177       02  FFTV01-WCUMUL                       PIC X(01).                   
 178       02  FFTV01-DDOC                         PIC X(08).                   
      * DEVISE --> FAIT PARTIE DE LA CLEF DE TRI                                
 186       02  FFTV01-CDEVISE                      PIC X(03).                   
 189  **** 02  FILLER PIC X(06) (LRECL=200)                                     
                                                                                
