      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************           
      * FICHIER STANDARD A ALIMENTE PAR LES APPLICATIONS DE GESTION *           
      *                                                             *           
      * !!!!!!!!! VERSION SANS ZONE PACKEE !!!!!!!!!!!!!!!          *           
      *                                                             *           
      *      - DSECT COBOL                                          *           
      *      - NOM = FFTI0C                                         *           
      *                             RECSIZE = 410                   *           
      ***************************************************************           
      *                                                                         
       01  FFTI0C-ENR.                                                          
      ***************************************************************           
      *   CLE DU FICHIER : FFTI0C-CLE                               *           
      ***************************************************************           
           02  FFTI0C-CLE.                                                      
   1           03  FFTI0C-CINTERFACE             PIC X(05).                     
   6           03  FFTI0C-NPIECE                 PIC X(10).                     
  16           03  FFTI0C-NSEQ                   PIC 9(05).                     
  21       02  FFTI0C-CTYPOPER                   PIC X(05).                     
  26       02  FFTI0C-CNATOPER                   PIC X(05).                     
  31       02  FFTI0C-ANALYTIQUE  OCCURS  6.                                    
  31           03  FFTI0C-CRITERE                PIC X(05).                     
  61       02  FFTI0C-WSENS                      PIC X(01).                     
  62       02  FFTI0C-TAUXTVA                    PIC 9(4).                      
  66       02  FFTI0C-WGROUPE                    PIC X(01).                     
  67       02  FFTI0C-MONTANT                    PIC 9(14).                     
  81       02  FFTI0C-NCLIENT                    PIC X(15).                     
  96       02  FFTI0C-NFOUR                      PIC X(15).                     
 111       02  FFTI0C-NSOC-E                     PIC X(03).                     
 114       02  FFTI0C-NLIEU-E                    PIC X(03).                     
 117       02  FFTI0C-NSOC-R                     PIC X(03).                     
 120       02  FFTI0C-NLIEU-R                    PIC X(03).                     
 123       02  FFTI0C-NTIERS                     PIC X(15).                     
 138       02  FFTI0C-NOPERATION                 PIC X(15).                     
 153       02  FFTI0C-DOPERATION                 PIC X(08).                     
 161       02  FFTI0C-DECHEANCE                  PIC X(08).                     
 169       02  FFTI0C-LETTRAGE4                  PIC X(15).                     
 184       02  FFTI0C-LETTRAGE5                  PIC X(15).                     
 199       02  FFTI0C-LETTRAGESIM                PIC X(40).                     
 239       02  FFTI0C-CDEVISE                    PIC X(03).                     
 242       02  FFTI0C-DTRAITEMENT                PIC X(08).                     
 250       02  FFTI0C-LIBELLE                    OCCURS 5.                      
               03  FFTI0C-LMVT                   PIC X(25).                     
 375       02  FFTI0C-CPAYS                      PIC X(03).                     
 378       02  FFTI0C-WTVA                       PIC X(01).                     
 379       02  FFTI0C-REFDOC                     PIC X(12).                     
 391       02  FFTI0C-DDOC                       PIC X(08).                     
 399       02  FILLER                            PIC X(12).                     
                                                                                
