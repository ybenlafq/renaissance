      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************                00000010
      *   DESCRIPTION DU FICHIER FBS202 LG = 270                        00000020
      **************************************************                00000030
       01  W-ENR-FBS202.                                                00000040
           02 FBS202-VAR-NCODIC.                                        00000050
1             03 FBS202-NCODIC                  PIC X(07).              00000060
8             03 FBS202-OFFRE                   PIC X(07).              00000070
15            03 FBS202-PREF1                   PIC S9(5)V99 COMP-3.    00000080
19            03 FBS202-PREF2                   PIC S9(5)V99 COMP-3.    00000090
23            03 FBS202-WOA                     PIC X(01).              00000100
              03 FBS202-INFOS.                                          00000110
24               04 FBS202-LREFFOURN            PIC X(20).              00000120
44               04 FBS202-CFAM                 PIC X(05).              00000130
49               04 FBS202-CMARQ                PIC X(05).              00000140
54               04 FBS202-LSTATCOMP            PIC X(03).              00000150
57               04 FBS202-CHEFPROD             PIC X(05).              00000160
62               04 FBS202-CTAUXTVA             PIC X(05).              00000170
67               04 FBS202-DEFSTATUT            PIC X(08).              00000180
75               04 FBS202-WSEQED               PIC X(02).              00000190
77               04 FBS202-NAGREGATED           PIC X(02).              00000200
79               04 FBS202-LAGREGATED           PIC X(20).              00000210
99               04 FBS202-CPROFASS             PIC X(05).              00000220
104              04 FBS202-200-FILLER           PIC X(13).              00000230
117              04 FBS202-NSEQENT              PIC S9(3) COMP-3.       00000240
119              04 FBS202-NENTMIN              PIC S9(3) COMP-3.       00000250
121              04 FBS202-NENTMAX              PIC S9(3) COMP-3.       00000260
123              04 FBS202-WPRIME               PIC X(01).              00000270
124              04 FBS202-WPRIX                PIC X(01).              00000280
125              04 FBS202-TYPE                 PIC XX.                 00000290
127              04 FBS202-MONTANT              PIC S9(5)V99 COMP-3.    00000300
131              04 FBS202-MONTANT-MAX          PIC S9(5)V99 COMP-3.    00000310
135              04 FBS202-REFI                 PIC S9(5)V99 COMP-3.    00000320
139              04 FBS202-WIMPERATIF           PIC X(1).               00000330
140              04 FBS202-WBLOQUE              PIC X(1).               00000340
141              04 FBS202-201-FILLER           PIC X(09).              00000350
              03 FBS202-VAR-NSOCIETE.                                   00000360
150              04 FBS202-NSOCIETE             PIC X(03).              00000370
153              04 FBS202-PRA                  PIC S9(7)V99   COMP-3.  00000380
158              04 FBS202-PRMP                 PIC S9(7)V9(6) COMP-3.  00000390
165              04 FBS202-SRP                  PIC S9(7)V9(6) COMP-3.  00000400
                 04 FBS202-VAR-ZPRIX.                                   00000410
172                 05 FBS202-ZPRIX             PIC X(02).              00000420
174                 05 FBS202-NSOCDEPOT         PIC X(03).              00000430
177                 05 FBS202-NDEPOT            PIC X(03).              00000440
180                 05 FBS202-QPIECES0-HV04     PIC S9(5) COMP-3.       00000450
183                 05 FBS202-QPIECES1-HV04     PIC S9(5) COMP-3.       00000460
186                 05 FBS202-QPIECES2-HV04     PIC S9(5) COMP-3.       00000470
189                 05 FBS202-QPIECES3-HV04     PIC S9(5) COMP-3.       00000480
192                 05 FBS202-QPIECES4-HV04     PIC S9(5) COMP-3.       00000490
195                 05 FBS202-QPIECES0-HV12     PIC S9(5) COMP-3.       00000500
198                 05 FBS202-QPIECES1-HV12     PIC S9(5) COMP-3.       00000510
201                 05 FBS202-QPIECES2-HV12     PIC S9(5) COMP-3.       00000520
204                 05 FBS202-QPIECES3-HV12     PIC S9(5) COMP-3.       00000530
207                 05 FBS202-QPIECES4-HV12     PIC S9(5) COMP-3.       00000540
210                 05 FBS202-QSTOCKMAGD        PIC S9(5) COMP-3.       00000550
213                 05 FBS202-QMUTATT           PIC S9(5) COMP-3.       00000560
216                 05 FBS202-WSENSAPPRO        PIC X(01).              00000570
217                 05 FBS202-QSTOCKDEP         PIC S9(5)      COMP-3.  00000580
220                 05 FBS202-QSTOCKRES         PIC S9(5)      COMP-3.  00000590
223                 05 FBS202-DCDE              PIC X(08).              00000600
231                 05 FBS202-QCDE              PIC S9(5)      COMP-3.  00000610
234                 05 FBS202-QCDERES           PIC S9(5)      COMP-3.  00000620
237                 05 FBS202-TOTQCDE           PIC S9(5)      COMP-3.  00000630
240                 05 FBS202-TOTQCDERES        PIC S9(5)      COMP-3.  00000640
243                 05 FBS202-PSTDTTC           PIC S9(7)V99   COMP-3.  00000650
248                 05 FBS202-PCOMMART          PIC S9(5)V99   COMP-3.  00000660
252                 05 FBS202-PCOMMVOL          PIC S9(5)V99   COMP-3.  00000670
256                 05 FBS202-CPT-PEXPTTC       PIC S9(5)      COMP-3.  00000680
259        02  FILLER                           PIC X(12) VALUE SPACES. 00000690
       01  ST-FBS202                            PIC 99  VALUE ZERO.     00000700
                                                                                
