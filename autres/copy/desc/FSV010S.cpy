      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *===============================================================* 00000690
      *                 DESCRIPTION DE FSV010                         * 00000700
      *===============================================================* 00000710
      * FICHIER PRODUIT FSV010 BASE ARTICLE AVEC LE FAM NCG             00000880
      * DOIT ETRE IDENTIQUE A CELUI DE BSV010                           00000880
      * SINON PROBLEME ...                                              00000880
       01  WW-FSV010.                                                   00000890
         03  FSV010-NOMPGM           PIC  X(10) VALUE 'FSV010    ' .    00000900
         03  FSV010-CLE              PIC  X(01)  VALUE 'C' .            00000900
         03  W-FSV010.                                                  00000890
           05  FSV010-PRODUIT          PIC  X(07).                      00000900
           05  FSV010-CFAM             PIC  X(05).                      00001090
           05  FSV010-CMARQ            PIC  X(05).                      00001100
           05  FSV010-LREFFOURN        PIC  X(20).                      00000960
           05  FSV010-LEMBALLAGE       PIC  X(50).                      00001210
           05  FSV010-DCREATION        PIC  X(08).                      00001010
           05  FSV010-LSTATCOMP        PIC  X(03).                      00001220
           05  FSV010-CAPPRO           PIC  X(03).                              
           05  FSV010-DEFFET           PIC  X(08).                      00001050
           05  FSV010-CCHASSIS         PIC  X(07).                      00001040
           05  FSV010-CRAYON           PIC  X(05).                              
           05  FSV010-WDACEM           PIC  X(01).                              
           05  FSV010-FPRESTA          PIC  X(01).                              
           05  FSV010-CGROUPE          PIC  X(01).                              
           05  FSV010-CGARANTIE        PIC  X(05).                              
           05  FSV010-CTAUXTVA         PIC  X(05).                      00000970
           05  FSV010-NEAN             PIC  X(13).                      00000970
         03  FSV010-FILLER             PIC  X(12)   VALUE SPACES.       00000900
         03  FSV010-CLE-TRI      .                                      00000900
           05  FSV010-CLE-KAIDARA      PIC  X(01).                      00000970
           05  FSV010-CLE-FILLER       PIC  X(29)   VALUE SPACES.       00000970
      *                                                                 00001230
         03    FSV025-PRIX .                                                    
              10  FSV025-VALTVA           PIC  ---9V99.                         
      *        GN59-PREFTTC                                                     
              10  FSV025-PXNATTTC         PIC  -------9V99.                     
              10  FSV025-PXNATHT          PIC  -------9V99.                     
              10  FSV025-PXCESSTTC        PIC  -------9V99.                     
      *        GG50-PRMPRECYCL   = PRMP (OR MULTIFAMILLES)                      
              10  FSV025-PXCESSHT         PIC  -------9V99.                     
              10  FSV025-ECOTTC           PIC  ----9V99.                        
              10  FSV025-ECOHT            PIC  ----9V99.                        
      *        GG50-PCF  = SRP                                                  
              10  FSV025-SRPTTC           PIC  -------9V99.                     
              10  FSV025-SRPHT            PIC  -------9V99.                     
           05  FSV010-MKG OCCURS 3.                                             
              10  FSV010-CMKG             PIC  X(05).                           
              10  FSV010-CVMKG            PIC  X(05).                           
           05  FSV010-DONNEES-ADD.                                              
              10  FSV010-NPROD            PIC  X(15).                           
           05  FSV025-DONNEES-ADD.                                              
              10  FSV025-PRMP             PIC  -------9V99.                     
              10  FSV025-LIVRABLE         PIC  X(01).                           
              10  FSV025-EXPEDIABLE       PIC  X(01).                           
              10  FSV025-CIBLEB2B         PIC  X(01).                           
         03    FSV010-FILLER2             PIC  X(162).                          
                                                                                
