      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DES CDES FOURNISSEURS ET STOCK               *00002209
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FTH033-ENR.                                                       
           10  FTH033-NSOCLIVR        PIC X(03)      VALUE SPACES.              
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-NDEPOT          PIC X(03)      VALUE SPACES.              
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-NCODIC          PIC X(07)      VALUE SPACES.              
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-NCDE            PIC X(07)      VALUE SPACES.              
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-DLIVRAISON      PIC X(08)      VALUE SPACES.              
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-QCDE            PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-STOCKV          PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-STOCKRESV       PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-STOCKC          PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FTH033-STOCKP          PIC Z(05)      VALUE ZERO.                
                                                                                
