      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER DE COMMANDES DARTY SUISSE                    *        
      *  FICHIER TRANSMIS PAR AXAPTA                                   *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-:FEX200:.                                                      
         05 CHAMPS-:FEX200:.                                                    
      *                                               POS 1            *        
           10 :FEX200:-NSOC             PIC X(03).                      122  005
      *                                               POS 4            *        
           10 :FEX200:-TYPCDE           PIC X(01).                      094  006
      *                                               POS 5            *        
           10 :FEX200:-LIBRE            PIC X(04).                      197  007
      *                                               POS 9            *        
           10 :FEX200:-NCDE             PIC X(07).                      100  020
      *                                               POS 16           *        
           10 :FEX200:-NLIGCDE          PIC X(04).                      100  020
      *                                               POS 20           *        
           10 :FEX200:-NCODIC           PIC X(07).                      127  015
      *                                               POS 27           *        
           10 :FEX200:-QTE              PIC X(05).                      127  015
      *                                               POS 32           *        
           10 :FEX200:-NSOCDEST         PIC X(03).                      127  015
      *                                               POS 35           *        
           10 :FEX200:-NLIEUDEST        PIC X(03).                      127  015
      *                                               POS 38           *        
           10 :FEX200:-REFERENCE        PIC X(15).                      207  005
      *                                               POS 53           *        
           10 :FEX200:-COMMENTAIRE      PIC X(028).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FEX510-LONG         PIC S9(4)   COMP  VALUE +80.               
      *                                                                         
      *--                                                                       
       01  DSECT-:FEX200:-LONG         PIC S9(4) COMP-5  VALUE +80.             
                                                                                
      *}                                                                        
