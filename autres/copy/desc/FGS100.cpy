      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
IT     01  FGS100-REC.                                                  00000010
                                                                        00000020
IT         03  FGS100-NSOCIETE         PIC  X(03).                      00000030
IT         03  FGS100-NLIEU            PIC  X(03).                      00000040
IT         03  FGS100-NZONPRIX         PIC  X(02).                      00000050
IT         03  FGS100-NUMCAIS          PIC  X(01).                      00000060
IT         03  FGS100-TERM             PIC  X(04).                      00000070
IT         03  FGS100-TRANS            PIC  X(04).                      00000080
IT         03  FGS100-DVENTE           PIC  X(08).                      00000090
IT         03  FGS100-CMODDEL          PIC  X(01).                      00000100
IT         03  FGS100-CTVA             PIC  X(05).                      00000110
IT         03  FGS100-NCODIC           PIC  X(07).                      00000120
IT         03  FGS100-CTYPE            PIC  X(05).                      00000130
IT         03  FGS100-LMARQUE          PIC  X(20).                      00000140
IT         03  FGS100-LREFERENCE       PIC  X(20).                      00000150
IT         03  FGS100-CTYPCONDT        PIC  X(05).                      00000160
IT         03  FGS100-CVENDEUR         PIC  X(04).                      00000170
IT         03  FGS100-DJOUR            PIC  X(08).                      00000180
IT         03  FGS100-QPIECES          PIC  S9(05)     COMP-3.          00000190
IT         03  FGS100-PVTTC            PIC  S9(07)V99  COMP-3.          00000200
IT         03  FGS100-PVTTCSR          PIC  S9(07)V99  COMP-3.          00000210
                                                                                
