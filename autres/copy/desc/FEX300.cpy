      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER DE COMPTE RENDU RENVOYE A AXAPTA             *        
      *  (MODELE FICHIER DES RELIQUATS DECEM )                         *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-:FEX300:.                                                      
         05 CHAMPS-:FEX300:.                                                    
      *                                               POS 1            *        
           10 :FEX300:-NCDE             PIC X(09).                      100  020
      *                                               POS 10           *        
           10 :FEX300:-NLIGCDE          PIC X(04).                      100  020
      *                                               POS 14           *        
           10 :FEX300:-NCODICK          PIC X(15).                      197  007
      *                                               POS 29           *        
           10 :FEX300:-QTEDEM           PIC 9(9).                       127  015
      *                                               POS 38           *        
           10 :FEX300:-QTENSE           PIC 9(9).                       127  015
      *                                               POS 47           *        
           10 :FEX300:-NUMFOUR          PIC X(05).                      100  020
      *                                               POS 52           *        
           10 :FEX300:-NSOCDEST         PIC X(03).                      127  015
      *                                               POS 55           *        
           10 :FEX300:-NLIEUDEST        PIC X(03).                      127  015
      *                                               POS 58           *        
           10 :FEX300:-RAISON           PIC X(06).                      127  015
      *                                               POS 64           *        
           10 :FEX300:-DRECENT          PIC X(08).                      207  005
      *                                               POS 72           *        
           10 :FEX300:-STATUAPPRO       PIC X(03).                      207  005
      *                                               POS 75           *        
           10 :FEX300:-CODIC            PIC X(07).                      207  005
      *                                               POS 82           *        
           10 :FEX300:-COMMENTAIRE      PIC X(50).                              
      *                                               POS 132          *        
           10 :FEX300:-LIBRE1           PIC X(01).                              
      *                                               POS 133          *        
           10 :FEX300:-LIBRE2           PIC X(01).                              
      *                                               POS 134          *        
           10 :FEX300:-LIBRE3           PIC X(10).                              
      *                                               POS 144          *        
           10 :FEX300:-LIBRE4           PIC X(09).                              
      *                                               POS 153          *        
           10 :FEX300:-LIBRE5           PIC X(11).                              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FEX300-LONG         PIC S9(4)   COMP  VALUE +164.              
      *                                                                         
      *--                                                                       
       01  DSECT-:FEX300:-LONG         PIC S9(4) COMP-5  VALUE +164.            
                                                                                
      *}                                                                        
