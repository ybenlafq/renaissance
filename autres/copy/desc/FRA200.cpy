      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *                   DSECT DU FICHIER FRA200                      *        
      *                                                                *        
      *----------------------------------------------------------------*        
        01  :FRA200:-RECORD.                                                    
            03 :FRA200:-CORIG     PIC X(4).                                     
            03 :FRA200:-CTYPAVOIR PIC X(1).                                     
            03 :FRA200:-CTYPDEM   PIC X(5).                                     
            03 :FRA200:-NSOC      PIC X(3).                                     
            03 :FRA200:-NLIEU     PIC X(3).                                     
            03 :FRA200:-NUMAVOIR  PIC X(6).                                     
            03 :FRA200:-DAVOIR    PIC X(8).                                     
            03 :FRA200:-CMOTIF    PIC X(2).                                     
            03 :FRA200:-NUMLETTR  PIC X(10).                                    
            03 :FRA200:-DDIFF     PIC X(8).                                     
            03 :FRA200:-WTLMELA   PIC X(1).                                     
            03 :FRA200:-WBLBR     PIC X(1).                                     
            03 :FRA200:-REGLEMENT.                                              
               05 :FRA200:-CTYPREGL  PIC X(1).                                  
               05 :FRA200:-MTTREMB   PIC S9(7)V9(2) COMP-3.                     
               05 :FRA200:-CBANQ     PIC X(5).                                  
               05 :FRA200:-CLETTRE   PIC X(2).                                  
            03 :FRA200:-ADRESSE.                                                
               05 :FRA200:-CTITRENOM PIC X(5).                                  
               05 :FRA200:-LNOM      PIC X(25).                                 
               05 :FRA200:-LPRENOM   PIC X(15).                                 
               05 :FRA200:-CVOIE     PIC X(5).                                  
               05 :FRA200:-CTVOIE    PIC X(4).                                  
               05 :FRA200:-LNOMVOIE  PIC X(21).                                 
               05 :FRA200:-LCMPAD1   PIC X(32).                                 
               05 :FRA200:-LBATIMENT PIC X(3).                                  
               05 :FRA200:-LESCALIER PIC X(3).                                  
               05 :FRA200:-LETAGE    PIC X(3).                                  
               05 :FRA200:-LPORTE    PIC X(3).                                  
               05 :FRA200:-CPOSTAL   PIC X(5).                                  
               05 :FRA200:-LCOMMUNE  PIC X(32).                                 
               05 :FRA200:-LBUREAU   PIC X(25).                                 
               05 :FRA200:-UNUSED    PIC X(14).                                 
                                                                                
