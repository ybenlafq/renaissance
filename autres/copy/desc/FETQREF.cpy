      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : ETIQUETTE                                       *        
      *  COPY        : FETQREF                                         *        
      *  CREATION    : 08/06/2007                                      *        
      *  FONCTION    : DSECT DU FICHIER DU REFERENTIEL PRIX ENVOYE     *        
      *                A KLEE                                          *        
      *  UTLISATION  : CETTE DSECT EST UTILISEE PAR LE BNV612          *        
      *                                          ET LE BNV613          *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      * LONGUEUR DU FICHIER : 70                                       *        
BF1   * LONGUEUR DU FICHIER : 91                                       *        
      *                                                                *        
      ******************************************************************        
      * 22/05/2013 BF1                                                 *        
      *            AJOUT D'AUTRES TAXES                                *        
      ******************************************************************        
      *                                                                         
       01  :FETQREF:-ENREGISTREMENT.                                            
           03 :FETQREF:-MAG.                                                    
              05 :FETQREF:-NSOCIETE  PIC X(03).                                 
              05 :FETQREF:-NLIEU     PIC X(03).                                 
           03 :FETQREF:-NCODIC       PIC X(07).                                 
      *    DATE/HEURE DE DERNIERE MODIFICATION AU FORMAT YYYYMMDDHHMISS         
           03 :FETQREF:-DEFFET       PIC X(14).                                 
      *    PRIX TTC                                                             
           03 :FETQREF:-PRIX         PIC 9(08).                                 
      *    PRIME TTC                                                            
           03 :FETQREF:-PRIME        PIC 9(05).                                 
      *    ECOTAXE                                                              
           03 :FETQREF:-ECO          PIC 9(05).                                 
      *    PRIX UNITAIRE (SI COLISAGE) TTC                                      
           03 :FETQREF:-PRIX-UNIT    PIC 9(08).                                 
      *    PRIX UNITAIRE HORS GRATUIT  TTC                                      
           03 :FETQREF:-PRIX-HORSG   PIC 9(08).                                 
      *    OFFRE ACTIVE : 1 / OFFRE ACTIVE - 0 / OFFRE NON ACTIVE               
           03 :FETQREF:-WOA          PIC X(01).                                 
      *    STATUT D'APPROVISIONNEMENT                                           
           03 :FETQREF:-CAPPRO       PIC X(05).                                 
      *    QUANTIEME DERNIERE DATE D'EFFET PRIX                                 
           03 :FETQREF:-QDEFFET      PIC 9(03).                                 
      *    TAXE ADDITIONNELLE                                                   
BF1        03 :FETQREF:-CTAXE1       PIC X(02).                                 
BF1        03 :FETQREF:-ECO-TTC1     PIC 9(05).                                 
      *    TAXE ADDITIONNELLE                                                   
BF1        03 :FETQREF:-CTAXE2       PIC X(02).                                 
BF1        03 :FETQREF:-ECO-TTC2     PIC 9(05).                                 
      *    TAXE ADDITIONNELLE                                                   
BF1        03 :FETQREF:-CTAXE3       PIC X(02).                                 
BF1        03 :FETQREF:-ECO-TTC3     PIC 9(05).                                 
      *                                                                         
                                                                                
