      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
       01  W-FIN130.                                                            
           05  F130-NSOCMAG          PIC  X(3).                                 
           05  F130-NMAG             PIC  X(3).                                 
           05  FILLER                PIC  X(3).                                 
           05  F130-DINVENTAIRE      PIC  X(8).                                 
           05  F130-RAYON            PIC  X(5).                                 
           05  F130-NCODIC           PIC  X(7).                                 
           05  F130-LREFFOURN        PIC  X(20).                                
           05  F130-CAPPRO           PIC  X(5).                                 
           05  F130-CMARQ            PIC  X(5).                                 
           05  F130-CFAM             PIC  X(5).                                 
           05  F130-WSEQED           PIC  S9(5)        COMP-3.                  
           05  F130-WSEQFAM          PIC  S9(5)        COMP-3.                  
           05  F130-WSTOCKMASQ       PIC  X(1).                                 
           05  F130-NSSLIEU          PIC  X(3).                                 
           05  F130-QSTOCK           PIC  S9(5)        COMP-3.                en
           05  F130-PRMP             PIC  S9(7)V9(6)   COMP-3.                en
           05  F130-QSTOCKEC1        PIC  S9(5)        COMP-3.                en
           05  F130-WTLMELA          PIC  X(3).                               en
           05  F130-CSTATUT          PIC  X(1).                                 
           05  F130-NLIEU            PIC  X(5).                                 
           05  F130-CODE             PIC  X(1).                                 
           05  FILLER                PIC  X(03).                                
                                                                                
