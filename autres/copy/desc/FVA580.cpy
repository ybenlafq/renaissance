      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *        FICHIER TOTAL ETAT IVA580                              *         
      *                                                               *         
      *        - DSECT COBOL                                          *         
      *        - NOM  = FVA580                                        *         
      *        - LONGUEUR ENREG = 57                                  *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       01  DSECT-FVA580.                                                        
           05 FVA580-NSOC            PIC X(3).                                  
           05 FVA580-CSEQ            PIC X(2).                                  
           05 FVA580-CRAYON          PIC X(5).                                  
           05 FVA580-NLIEU           PIC X(3).                                  
           05 FVA580-QSTOCKINIT      PIC S9(11) COMP-3.                         
           05 FVA580-QSTOCKFINAL     PIC S9(11) COMP-3.                         
           05 FVA580-PSTOCKINIT      PIC S9(9)V9(6) COMP-3.                     
           05 FVA580-PSTOCKFINAL     PIC S9(9)V9(6) COMP-3.                     
           05 FVA580-PRECYCLENTREE   PIC S9(9)V9(6) COMP-3.                     
           05 FVA580-PRECYCLSORTIE   PIC S9(9)V9(6) COMP-3.                     
                                                                                
