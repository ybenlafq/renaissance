      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FGA053-DSECT.                                                        
           02 FGA053-NCODIC          PIC X(07).                                 
           02 FGA053-CFAM            PIC X(05).                                 
           02 FGA053-CDESCRIPTIF     PIC X(05).                                 
           02 FGA053-CVDESCRIPT      PIC X(05).                                 
           02 FGA053-CMARKETING      PIC X(05).                                 
           02 FGA053-CVMARKETING     PIC X(05).                                 
           02 FGA053-LVMARKETING     PIC X(20).                                 
                                                                                
