      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
       01 ENR-FGF700.                                                           
          02 FGF700-NCDE             PIC X(07)      VALUE SPACES .              
          02 FGF700-NENTCDE          PIC X(05)      VALUE SPACES .              
          02 FGF700-LENTCDE          PIC X(20)      VALUE SPACES .              
          02 FGF700-NSOCLIVR         PIC X(03)      VALUE SPACES .              
          02 FGF700-NDEPOT           PIC X(03)      VALUE SPACES .              
          02 FGF700-LDEPOT           PIC X(20)      VALUE SPACES .              
          02 FGF700-NCODIC           PIC X(07)      VALUE SPACES .              
          02 FGF700-LREFFOURN        PIC X(20)      VALUE SPACES .              
          02 FGF700-NEAN             PIC X(13)      VALUE SPACES .              
          02 FGF700-CFAM             PIC X(05)      VALUE SPACES .              
          02 FGF700-LFAM             PIC X(20)      VALUE SPACES .              
          02 FGF700-QCDE             PIC 9(05)      VALUE ZEROES .              
          02 FGF700-DLIVRAISON       PIC X(08)      VALUE SPACES .              
          02 FGF700-NEW-DLIVRAISON   PIC X(08)      VALUE SPACES .              
                                                                                
