      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **********************************************************                
      *   COPY DU FICHIER POUR BQC160 : EXTRACTION CARTES T VL                  
      *   LG = 100                                                              
      **********************************************************                
      *                                                                         
       01  DSECT-FQC010.                                                        
           02  FQC010-NSOCIETE       PIC X(03).                                 
           02  FQC010-NLIEU          PIC X(03).                                 
           02  FQC010-CTYPE          PIC X(01).                                 
           02  FQC010-NBVENTE        PIC S9(7) COMP-3.                          
           02  FQC010-NVENTE         PIC X(07).                                 
           02  FQC010-DDELIV         PIC X(08).                                 
           02  FQC010-DVENTE         PIC X(08).                                 
           02  FQC010-NCODIC         PIC X(07).                                 
           02  FQC010-CVENDEUR       PIC X(06).                                 
           02  FQC010-CMODDEL        PIC X(03).                                 
           02  FQC010-WCREDIT        PIC X(01).                                 
           02  FQC010-CPOSTAL        PIC X(05).                                 
           02  FQC010-CINSEE         PIC X(05).                                 
           02  FQC010-CPROTOUR       PIC X(05).                                 
           02  FQC010-CTOURNEE       PIC X(08).                                 
           02  FQC010-CEQUIPE        PIC X(05).                                 
           02  FQC010-TOP-MAIL       PIC X(01).                                 
           02  FQC010-NBMAIL         PIC S9(7) COMP-3.                          
           02  FQC010-FILLER         PIC X(16).                                 
           02  FQC010-EMAIL          PIC X(50).                                 
           02  FQC010-WEMPORTE       PIC X(1).                                  
           02  FQC010-PVTOTAL        PIC S9(7)V9(2) USAGE COMP-3.               
           02  FQC010-REMISE         PIC S9(7)V9(2) USAGE COMP-3.               
           02  FQC010-DGARANTIE      PIC X(8).                                  
           02  FQC010-CPSE           PIC X(5).                                  
           02  FQC010-LPSE           PIC X(20).                                 
           02  FQC010-NSEQNQ         PIC S9(5)V USAGE COMP-3.                   
           02  FQC010-QVENDUE        PIC S9(5) COMP-3.                          
           02  FQC010-TYPVTE         PIC X.                                     
           02  FILLER                PIC X(49).                                 
                                                                                
