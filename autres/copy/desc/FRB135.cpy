      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************           
      *   FICHIER DE SORTIE EXTRACTION PRESTATION                   *           
      *                     PRISE DE RENDEZ-VOUS                    *           
      *      - DSECT COBOL                                          *           
      *      - RECSIZE = 520                                        *           
      *                                                             *           
      ***************************************************************           
       01  FRB135-ENR.                                                          
           03  FFRB135-CRAYON               PIC X(005).                         
           03  FIL1                         PIC X.                              
           03  FFRB135-LRAYON               PIC X(050).                         
           03  FIL2                         PIC X.                              
           03  FFRB135-NSEQR                PIC X(002).                         
           03  FIL3                         PIC X.                              
           03  FFRB135-CFAM                 PIC X(005).                         
           03  FIL4                         PIC X.                              
           03  FFRB135-LFAM                 PIC X(050).                         
           03  FIL5                         PIC X.                              
           03  FFRB135-NSEQF                PIC X(002).                         
           03  FIL6                         PIC X.                              
           03  FFRB135-CMARQ                PIC X(005).                         
           03  FIL7                         PIC X.                              
           03  FFRB135-LMARQ                PIC X(020).                         
           03  FIL8                         PIC X.                              
           03  FFRB135-CODIC                PIC X(007).                         
           03  FIL9                         PIC X.                              
           03  FFRB135-REFERENCE            PIC X(050).                         
           03  FIL10                        PIC X.                              
           03  FFRB135-NSEQC                PIC X(002).                         
           03  FIL11                        PIC X.                              
           03  FFRB135-STATUT               PIC X(003).                         
           03  FIL12                        PIC X.                              
           03  FFRB135-PV                   PIC ------9.99.                     
           03  FIL13                        PIC X.                              
           03  FFRB135-FLAGRB               PIC X(005).                         
           03  FIL14                        PIC X.                              
           03  FFRB135-FLAGRDV              PIC X(003).                         
           03  FIL15                        PIC X.                              
           03  FFRB135-CODICLIEN            PIC X(007).                         
           03  FIL16                        PIC X.                              
           03  FFRB135-FLAGQTE              PIC 9(002).                         
           03  FIL17                        PIC X.                              
           03  FFRB135-URLINFO              PIC X(255).                         
           03  FILLER                       PIC X(025).                         
                                                                                
