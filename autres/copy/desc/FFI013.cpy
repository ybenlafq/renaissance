      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      ******* FICHIER EXCEPTION DE FACTURATION (FACTUR.INTERNE)********         
      *****************************************************************         
      *****                                                       *****         
      ***** MODIF ANNE DSA005 - LE 11/12/96                       *****         
      ***** POUR LES BESOINS DU SRP CAPROFEM, NECESSITE DE STOCKER*****         
      ***** LE PRAK                                               *****         
      *****                                                       *****         
      ***** --> AJOUT DE CETTE ZONE DANS LE FICHIER               *****         
      *****    NB : LA LONGUEUR DU FICHIER N'EST PAS MODIFIEE     *****         
      *****         REDUCTION DU FILLER                           *****         
      *****                                                       *****         
      *****************************************************************         
       01  FFI013-DSECT.                                                        
           10  FFI013-DMOUV          PIC  X(8).                                 
           10  FFI013-NSOC           PIC  X(3).                                 
           10  FFI013-NLIEU          PIC  X(3).                                 
           10  FFI013-CSENS          PIC  X(1).                                 
           10  FFI013-CTYPE          PIC  X(5).                                 
           10  FFI013-NDOC           PIC  X(7).                                 
           10  FFI013-CODIC          PIC  X(7).                                 
           10  FFI013-WSEQFAM        PIC  S9(5)      COMP-3.                    
           10  FFI013-QTE            PIC  S9(5)      COMP-3.                    
           10  FFI013-PRMP           PIC  S9(7)V9(6) COMP-3.                    
           10  FFI013-PCF            PIC  S9(7)V9(6) COMP-3.                    
           10  FFI013-CTAUXTVA       PIC  X(5).                                 
           10  FFI013-CFRGEST        PIC  S9(3)V99   COMP-3.                    
           10  FFI013-WDACEM         PIC  X(1).                                 
           10  FFI013-PRAK           PIC  S9(7)V9(6) COMP-3.                    
0397       10  FFI013-DOPER          PIC  X(08).                                
0397       10  FILLER                PIC  X(22).                                
                                                                                
