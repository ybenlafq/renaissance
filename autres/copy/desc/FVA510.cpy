      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  W-FVA510.                                                            
           02 FVA510-NSOCVALO      PIC X(03).                                   
           02 FVA510-NLIEUVALO     PIC X(03).                                   
           02 FVA510-NCODIC        PIC X(07).                                   
           02 FVA510-CTYPMVTVALO   PIC X(01).                                   
           02 FVA510-NSOCMVT       PIC X(03).                                   
           02 FVA510-NLIEUMVT      PIC X(03).                                   
           02 FVA510-QSTOCKENTREE  PIC S9(11)     COMP-3.                       
           02 FVA510-QSTOCKSORTIE  PIC S9(11)     COMP-3.                       
           02 FVA510-PSTOCKENTREE  PIC S9(9)V9(6) COMP-3.                       
           02 FVA510-PSTOCKSORTIE  PIC S9(9)V9(6) COMP-3.                       
           02 FVA510-CRAYON1       PIC X(05).                                   
           02 FVA510-CSEQRAYON     PIC X(02).                                   
                                                                                
