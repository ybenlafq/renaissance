      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DES MOUVEMENTS VOLUME DE DESTOCKAGE          *00002209
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FGS007-ENR.                                                       
           10  FGS007-DCHARG          PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-CSELART         PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-NORIGINE        PIC X(07)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-NSOCORIG        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-NLIEUORIG       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-NSOCDEST        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-NLIEUDEST       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-QCHARG          PIC Z(18)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS007-VOLCHARG        PIC Z(18)      VALUE ZERO.                
                                                                                
