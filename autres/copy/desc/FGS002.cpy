      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DES MOUVEMENTS DE VIDAGE DES PI (PGM=BGS002) *00002209
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FGS002-ENR.                                                       
           10  FGS002-CFAM            PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-CMARQ           PIC X(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NCODIC          PIC X(07)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-LREFFOURN       PIC X(20)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-TYPE            PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NSOCORIG        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NLIEUORIG       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NSSLIEUORIG     PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-CLIEUTRTORIG    PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NSOCDEST        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NLIEUDEST       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NSSLIEUDEST     PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-CLIEUTRTDEST    PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-NORIGINE        PIC X(09)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-DMVT            PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-QMVT-E          PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-QMVT-S          PIC Z(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-VAL-E           PIC -(9)9,9(2) VALUE ZERO.                
           10  FGS002-VAL-E0 REDEFINES FGS002-VAL-E                             
                                      PIC -(10),-(2).                           
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS002-VAL-S           PIC -(9)9,9(2) VALUE ZERO.                
           10  FGS002-VAL-S0 REDEFINES FGS002-VAL-S                             
                                      PIC -(10),-(2).                           
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FILLER                 PIC X(19)      VALUE SPACE.               
           10  FGS002-LIEU-TRI        PIC X(03)      VALUE SPACE.               
                                                                                
