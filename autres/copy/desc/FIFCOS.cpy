      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  DSECT-IFCOS.                                                         
      ******************************************************************        
      *                                                                *        
      * DESCRIPTION DU FICHIER DE COMPTABILISATION INTERFACE COSYS     *        
      *                                                                *        
      ******************************************************************        
      *  SOCIETE COMPTABLE                                                      
           05  IFCOS-NSOCCOMPT  PIC X(3).                               001 003 
      *  CODE ETABLISSEMENT                                                     
           05  IFCOS-NETAB      PIC X(2).                               004 002 
      *  CODE JOURNAL                                                           
           05  IFCOS-NJRN       PIC X(3).                               006 003 
      *  EXERCICE COMPTABLE                                                     
           05  IFCOS-NEXER      PIC X(2).                               009 002 
      *  PERIODE COMPTABLE                                                      
           05  IFCOS-NPER       PIC X(3).                               011 003 
      *  NUMERO DE PIECE (NON RENSEIGNEE)                                       
           05  IFCOS-NPIECE     PIC X(5).                               014 005 
           05  IFCOS-FILLER1    PIC X(3).                               019 003 
      *  TYPE DE MONTANT                                                        
           05  IFCOS-CTYPMONT   PIC X(3).                               022 003 
      *  NATURE DE PIECE                                                        
           05  IFCOS-NATURE     PIC X(3).                               025 003 
      *  DATE DE PIECE (MOUVEMENT)                                              
           05  IFCOS-DPIECE     PIC X(6).                               028 006 
      *  LIBELLE DE LA LIGNE                                                    
           05  IFCOS-LIBELLE    PIC X(30).                              034 030 
      *  NUMERO DE LETTRAGE                                                     
           05  IFCOS-NLETTRAGE  PIC X(8).                               064 008 
           05  IFCOS-FILLER2    PIC X(3).                               072 003 
      *  NUMERO DE COMPTE                                                       
           05  IFCOS-COMPTE     PIC X(6).                               075 006 
           05  IFCOS-FILLER3    PIC X(2).                               081 002 
      *  CODE AUXILIAIRE                                                        
           05  IFCOS-NAUX       PIC X(1).                               083 001 
      *  CODE TIERS                                                             
           05  IFCOS-NTIERS     PIC X(6).                               084 006 
           05  IFCOS-FILLER4    PIC X(7).                               090 007 
      *  CODE SECTION                                                           
           05  IFCOS-SECTION    PIC X(6).                               097 006 
      *  CODE RUBRIQUE                                                          
           05  IFCOS-RUBRIQUE   PIC X(4).                               103 004 
           05  IFCOS-CRUBR1     PIC X(2).                               107 002 
           05  IFCOS-CRUBR2     PIC X(2).                               109 002 
      *  MONTANT DE LA LIGNE                                                    
           05  IFCOS-MONTANT    PIC S9(9)V9(2)   COMP-3.                111 006 
           05  IFCOS-FILLER5    PIC X(1).                               117 001 
      *  SENS DU MOUVEMENT (C/D)                                                
           05  IFCOS-WSENS      PIC X(1).                               118 001 
           05  IFCOS-FILLER     PIC X(10).                              119 010 
                                                                                
