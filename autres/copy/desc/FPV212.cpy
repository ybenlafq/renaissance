      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 DSECT-FPV212.                                                         
          03 FPV212-NSOCIETE             PIC X(0003).                           
          03 FPV212-NLIEU.                                                      
             05 FPV212-NZONPRIX          PIC XX.                                
             05 FPV212-BBTE              PIC X.                                 
          03 FPV212-CODPROF              PIC X(0010).                           
          03 FPV212-CMARKETING           PIC X(0005).                           
          03 FPV212-CVMARKETING          PIC X(0005).                           
          03 FPV212-NCODIC               PIC X(0007).                           
NT        03 FPV212-INTER                PIC X(0003).                           
          03 FPV212-VARINTER             PIC X(0001).                           
          03 FPV212-DATINTER             PIC X(0008).                           
          03 FPV212-LSTATCOMP            PIC X(0003).                           
          03 FPV212-LIBPROFIL            PIC X(0030).                           
          03 FPV212-LMARKETING           PIC X(0020).                           
          03 FPV212-LVMARKETING          PIC X(0020).                           
          03 FPV212-VAR                  PIC X(0001).                           
          03 FPV212-OFFRE                PIC X(0001).                           
          03 FPV212-DATOFFRE             PIC X(0008).                           
          03 FPV212-CMAJ                 PIC X(0001).                           
          03 FPV212-DMAJ                 PIC X(0008).                           
NT        03 FPV212-FILLER               PIC X(0003).                           
                                                                                
