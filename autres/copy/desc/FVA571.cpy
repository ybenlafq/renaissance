      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *        FICHIER DETAIL ETAT IVA570                             *         
      *                                                               *         
      *        - DSECT COBOL                                          *         
      *        - NOM  = FVA571                                        *         
      *        - LONGUEUR ENREG = 71                                  *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       01  DSECT-FVA571.                                                        
           05 FVA571-NSOC          PIC X(3).                                    
           05 FVA571-CSEQ          PIC X(2).                                    
           05 FVA571-CRAYON        PIC X(5).                                    
           05 FVA571-NCONSO        PIC X(6).                                    
           05 FVA571-CTYPMVT       PIC X.                                       
           05 FVA571-NSOCMVT       PIC X(3).                                    
           05 FVA571-NLIEUMVT      PIC X(3).                                    
           05 FVA571-LLIEU         PIC X(20).                                   
           05 FVA571-QSTOCKENTREE  PIC S9(11) COMP-3.                           
           05 FVA571-QSTOCKSORTIE  PIC S9(11) COMP-3.                           
           05 FVA571-PSTOCKENTREE  PIC S9(9)V9(6) COMP-3.                       
           05 FVA571-PSTOCKSORTIE  PIC S9(9)V9(6) COMP-3.                       
                                                                                
