      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FIC61-ENR.                                                           
           03 FIC61-OPCO          PIC X(006)            VALUE SPACES.           
           03 FIC61-STE           PIC X(003)            VALUE SPACES.           
           03 FIC61-LIEU          PIC X(003)            VALUE SPACES.           
           03 FIC61-NSEQ          PIC X(003)            VALUE SPACES.           
           03 FIC61-ESFIL         PIC X(001)            VALUE SPACES.           
           03 FIC61-LIBEL         PIC X(050)            VALUE SPACES.           
           03 FIC61-QTE           PIC 9(013)     COMP-3 VALUE ZERO.             
           03 FIC61-PRIXDIF       PIC 9(12)V9(6) COMP-3 VALUE ZERO.             
           03 FIC61-PRIXFIL       PIC 9(12)V9(6) COMP-3 VALUE ZERO.             
           03 FIC61-PRIXCES       PIC 9(12)V9(6) COMP-3 VALUE ZERO.             
           03 FILLER              PIC X(047)            VALUE SPACES.           
                                                                                
