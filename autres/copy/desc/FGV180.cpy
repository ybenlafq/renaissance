      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FGV183 D'EXTRACTION DES           *00030004
      *       VENTES DE CREDITS, TRIEE, CUMULEE                        *00040004
      *       FICHIER EN SORTIE DE BGV183 ET EN ENTREE DE BGV185       *00041004
      *       BGV186, BGV187, BGV188                                   *00042004
      *       LONGUEUR D'ENREGISTREMENT : 98 C.                        *00050007
      *================================================================*00060000
      *                                                                 00070000
       01  FILLER.                                                      00080000
      *                                                                 00090000
           05  FGV183-ENREG.                                            00100004
               10  FGV183-NSOCIETE       PIC  X(03).                    00110004
               10  FGV183-NLIEU          PIC  X(03).                    00120004
               10  FGV183-COCRED         PIC  X(01).                    00130004
               10  FGV183-CVENDEUR       PIC  X(06).                    00140004
               10  FGV183-WTPCRD         PIC  X(05).                    00150004
               10  FGV183-NVENTE         PIC  X(07).                    00151004
               10  FGV183-CFCRED         PIC  X(05).                    00152004
               10  FGV183-NCREDI         PIC  X(14).                    00153004
               10  FGV183-CDEV           PIC  X(03).                    00154004
               10  FGV183-PREGLTVTE      PIC  S9(7)V99 COMP-3.          00155007
               10  FGV183-LFRMLE         PIC  X(30).                    00157004
               10  FGV183-DDEBPER        PIC  X(08).                    00158006
               10  FGV183-DFINPER        PIC  X(08).                    00159006
      *                                                                 00160000
      *                                                                 00460000
                                                                                
