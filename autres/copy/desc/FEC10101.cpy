      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
      * darty.com : Export articles Host => Websphere                   00000020
      ****************************************************************  00000030
      *                                                                 00000030
      * 16/07/04 : Ajout Flag "offre active " (O/N)                     00000030
      *                                                                 00000030
      * 30/06/11 : Ajout modes de delivrance possibles                  00000030
      *                                                                 00000030
V5.0  * 02/01/12 : AJOUT DU LREFO DE LA GA00 (HP PURE SUPPLY)           00000030
V5.1.3* 24/07/12 : AJOUT DU LREFO DE LA GA00 (HP PURE SUPPLY LE RETOUR) 00000030
V6.5  * 19/07/13 : AJOUT DU UPS, CHRONORELAIS                           00000030
      *                                                                 00000030
      ****************************************************************  00000030
       01     FEC10101-DSECT.                                                   
           05 FEC10101-NCODIC      PIC X(07).                                   
           05 FEC10101-LREFFOURN   PIC X(20).                                   
           05 FEC10101-CFAMCOM     PIC X(05).                                   
           05 FEC10101-CFAM        PIC X(05).                                   
           05 FEC10101-NEAN        PIC X(13).                                   
           05 FEC10101-CMARQ       PIC X(05).                                   
           05 FEC10101-CGARANTIE   PIC X(05).                                   
           05 FEC10101-APPRO       PIC X(05).                                   
           05 FEC10101-CCTM        PIC X(05).                                   
           05 FEC10101-CPROFIL     PIC X(08).                                   
           05 FEC10101-PXVTTC      PIC 9(07)V9(02).                             
           05 FEC10101-COULEUR     PIC X(05).                                   
           05 FEC10101-QPOIDS      PIC 9(07).                           00000080
           05 FEC10101-QLARGEUR    PIC 9(03).                                   
           05 FEC10101-QHAUTEUR    PIC 9(03).                           00000080
           05 FEC10101-QPROFONDEUR PIC 9(03).                                   
           05 FEC10101-TYPE        PIC X(01).                                   
           05 FEC10101-DT-EP       PIC X(08).                                   
           05 FEC10101-C-EXPO      PIC X(01).                                   
V6.5       05 FEC10101-C-UPS       PIC X(01).                                   
V6.5       05 FEC10101-C-CHRONORELAIS PIC X(01).                                
EG         05 FEC10101-C-KIALA     PIC X(01).                                   
EG         05 FEC10101-C-CHRONO    PIC X(01).                                   
EG         05 FEC10101-C-COLISSIMO PIC X(01).                                   
EG         05 FEC10101-C-LIVRE     PIC X(01).                                   
V5.1       05 FEC10101-C-J0        PIC X(03).                                   
V5.1       05 FEC10101-C-J1        PIC X(03).                                   
V5.1       05 FEC10101-C-J2        PIC X(03).                                   
V5.1.3     05 FEC10101-lrefo       PIC X(20).                                   
JG    *    05 FEC10101-WOA         PIC X(01).                                   
      *                                                                         
                                                                                
