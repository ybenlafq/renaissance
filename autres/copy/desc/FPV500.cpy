      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  W-ENR-FPV500.                                                        
1          05 FPV500-NSOCIETE        PIC X(3).                                  
4          05 FPV500-NLIEU           PIC X(3).                                  
7          05 FPV500-NVENTE          PIC X(7).                                  
14         05 FPV500-NSEQNQ          PIC S9(5) COMP-3.                          
17         05 FPV500-CVENDEUR        PIC X(6).                                  
23         05 FPV500-DTOPE           PIC X(8).                                  
31         05 FPV500-DCREATION       PIC X(8).                                  
39         05 FPV500-WOFFRE          PIC X(1).                                  
40         05 FPV500-NCODICGRP       PIC X(7).                                  
47         05 FPV500-CTYPENT         PIC X(2).                                  
49         05 FPV500-WREMISE         PIC X(1).                                  
50         05 FPV500-NCODIC          PIC X(7).                                  
57         05 FPV500-CENREG          PIC X(5).                                  
62         05 FPV500-WEP             PIC X(1).                                  
63         05 FPV500-WOA             PIC X(1).                                  
64         05 FPV500-WPSAB           PIC X(1).                                  
65         05 FPV500-WREMVTE         PIC X(1).                                  
66         05 FPV500-NSEQENT         PIC S9(5) COMP-3.                          
69         05 FPV500-QTVEND          PIC S9(5) COMP-3.                          
72         05 FPV500-QTCODIG         PIC S9(5) COMP-3.                          
75         05 FPV500-MTVTEHT         PIC S9(7)V99 COMP-3.                       
80         05 FPV500-MTVTETVA        PIC S9(7)V99 COMP-3.                       
85         05 FPV500-MTFORHT         PIC S9(7)V99 COMP-3.                       
90         05 FPV500-MTFORTVA        PIC S9(7)V99 COMP-3.                       
95         05 FPV500-MTDIFFHT        PIC S9(7)V99 COMP-3.                       
100        05 FPV500-MTDIFFTVA       PIC S9(7)V99 COMP-3.                       
105        05 FPV500-MTPRMP          PIC S9(7)V99 COMP-3.                       
110        05 FPV500-MTPRIME         PIC S9(7)V99 COMP-3.                       
115        05 FPV500-MTPRIMEO        PIC S9(7)V99 COMP-3.                       
120        05 FPV500-MTPRIMEV        PIC S9(7)V99 COMP-3.                       
125        05 FPV500-MTCOMMOP        PIC S9(7)V99 COMP-3.                       
130        05 FPV500-TYPVTE          PIC X(01).                                 
131        05 FPV500-WTABLE          PIC X(02).                                 
133        05 FPV500-NZONPRIX        PIC X(02).                                 
135        05 FPV500-MTREFINOP       PIC S9(7)V99 COMP-3.                       
139        05 FPV500-DDELIV          PIC X(08).                                 
147   * FIN                                                                     
                                                                                
