      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGP889  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : REAPPROVISIONNEMENT FOURNISSEUR             *         
      *                                                               *         
      *    FGP889       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    LONGUEUR ENR.:  512 C                                      *         
      *                                                               *         
      *    ATTENTION CETTE COPY COMMENCE PAR UN NIVEAU 03             *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       03  :FGPXXX:-ENREG.                                                      
      *                                                         1    6          
           05  :FGPXXX:-CETAT                 PIC  X(06).                       
      *                                                         7    7          
           05  :FGPXXX:-CBOITE                PIC  X(07).                       
      *                                                        14    3          
           05  :FGPXXX:-NSOCIETE              PIC  X(03).                       
      *                                                        17   20          
           05  :FGPXXX:-LLIEU                 PIC  X(20).                       
      *                                                        37    5          
        04 :FGPXXX:-DONNEE-ENTETE.                                              
           05  :FGPXXX:-CHEFPROD              PIC  X(05).                       
      *                                                        42   20          
           05  :FGPXXX:-LCHEFPROD             PIC  X(20).                       
      *                                                        62   15          
           05  :FGPXXX:-LIBELLE               PIC  X(15).                       
      *                                                        77    5          
           05  :FGPXXX:-WSEQFAM               PIC  X(05).                       
      *                                                        82    5          
           05  :FGPXXX:-CFAM                  PIC  X(05).                       
      *                                                        87   20          
           05  :FGPXXX:-LFAM                  PIC  X(20).                       
      *                                                       107   20          
           05  :FGPXXX:-LAGREGATED            PIC  X(20).                       
      *                                                       127    5          
        04 :FGPXXX:-DONNEE-CODIC.                                               
           05  :FGPXXX:-CMARQ                 PIC  X(05).                       
      *                                                       132   20          
           05  :FGPXXX:-LMARQ                 PIC  X(20).                       
      *                                                       152   20          
           05  :FGPXXX:-LREFFOURN             PIC  X(20).                       
      *                                                       172    1          
           05  :FGPXXX:-WOA                   PIC  X(01).                       
      *                                                       173    5          
        04 :FGPXXX:-DONNEE-FILIALE.                                             
           05  :FGPXXX:-SOCCHEF               PIC X(05).                        
      *                                                       178    5          
           05  :FGPXXX:-PSTDTTC               PIC S9(07)V9(02) COMP-3.          
      *                                                       183    4          
           05  :FGPXXX:-QS-0                  PIC S9(06) COMP-3.                
      *                                                       187    4          
           05  :FGPXXX:-QS-1                  PIC S9(06) COMP-3.                
      *                                                       191    4          
           05  :FGPXXX:-QS-2                  PIC S9(06) COMP-3.                
      *                                                       195    4          
           05  :FGPXXX:-QS-3                  PIC S9(06) COMP-3.                
      *                                                       199    4          
           05  :FGPXXX:-QS-4                  PIC S9(06) COMP-3.                
      *                                                       203    2          
           05  :FGPXXX:-NBMAG                 PIC S9(03) COMP-3.                
      *                                                       205    3          
           05  :FGPXXX:-QSTOCKMAG             PIC S9(05) COMP-3.                
      *                                                       208    2          
           05  :FGPXXX:-NBBOU                 PIC S9(03) COMP-3.                
      *                                                       210    3          
           05  :FGPXXX:-QSTOCKBOU             PIC S9(05) COMP-3.                
      *                                                       213    5          
           05  :FGPXXX:-CEXPO                 PIC  X(05).                       
      *                                                       218    3          
           05  :FGPXXX:-LSTATCOMP             PIC  X(03).                       
      *                                                       221    4          
        04 :FGPXXX:-DONNEE-DEPOT.                                               
           05  :FGPXXX:-QSTOCKDIS             PIC S9(07) COMP-3.                
      *                                                       225    4          
           05  :FGPXXX:-QSTOCKRES             PIC S9(06) COMP-3.                
      *                                                       229    3          
           05  :FGPXXX:-NSOCDEPOT             PIC  X(03).                       
      *                                                       232    3          
           05  :FGPXXX:-NDEPOT                PIC  X(03).                       
      *                                                       235    4          
           05  :FGPXXX:-DCDE4                 PIC  X(04).                       
      *                                                       239    3          
           05  :FGPXXX:-QCDE                  PIC S9(05) COMP-3.                
      *                                                       242    3          
           05  :FGPXXX:-QCDERES               PIC S9(05) COMP-3.                
      *                                                       245    3          
           05  :FGPXXX:-QCOLIRECEPT           PIC S9(05) COMP-3.                
      *                                                       248    3          
           05  :FGPXXX:-QNBPRACK              PIC S9(05) COMP-3.                
      *                                                       251    1          
           05  :FGPXXX:-WSOL                  PIC  X(01).                       
      *                                                       252    7          
        04 :FGPXXX:-DONNEE-TRI.                                                 
           05  :FGPXXX:-NCODIC                PIC  X(07).                       
      *                                                       259    7          
           05  :FGPXXX:-NCODIC2               PIC  X(07).                       
      *                                                       266    1          
           05  :FGPXXX:-WDEB                  PIC  X(01).                       
      *                                                       267    1          
           05  :FGPXXX:-WGROUPE               PIC  X(01).                       
      *                                                       268    5          
           05  :FGPXXX:-WCMARQ                PIC  X(05).                       
      *                                                       273    6          
           05  :FGPXXX:-WENTNAT               PIC  X(06).                       
      *                                                       279    4          
           05  :FGPXXX:-WVOLV4S               PIC S9(07) COMP-3.                
      *                                                       283    1          
           05  :FGPXXX:-WTYPENT               PIC  X(01).                       
      *                                                       284    1          
           05  :FGPXXX:-WSENSAPPRO            PIC  X(01).                       
      *                                                       285    8          
           05  :FGPXXX:-DCDE8                 PIC  X(08).                       
      *                                                       293    5          
           05  :FGPXXX:-CAPPRO                PIC  X(05).                       
      *                                                       298    1          
           05  :FGPXXX:-WDACEM                PIC  X(01).                       
      *                                                       299    5          
           05  :FGPXXX:-CAPPRO2               PIC  X(05).                       
      *                                                       304    2          
           05  :FGPXXX:-QTYPLIEN              PIC  S9(3) COMP-3.                
      *                                                       306    8          
           05  :FGPXXX:-DEFFET                PIC  X(08).                       
      *                                                       314    1          
           05  :FGPXXX:-FICHE                 PIC  X(01).                       
      *                                                       315   24          
           05  :FGPXXX:-TAB-FILIALE.                                            
             08  :FGPXXX:-VTE-FILIALE    OCCURS 8.                              
                10  :FGPXXX:-NFILIALE         PIC  X(03).                       
      *                                                       339    3          
           05  :FGPXXX:-LSTATCOMP2            PIC  X(03).                       
      *                                                       342    1          
           05  :FGPXXX:-STAT-ECE              PIC  X(01).                       
      *                                                       343  170          
           05  FILLER                         PIC  X(170).                      
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
