      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER IGM070                                                
      *                                                                *        
      *----------------------------------------------------------------*        
       01  :FGM70:-DSECT.                                                       
          05 FILLER                     PIC X(6) VALUE  SPACES.                 
          05 :FGM70:-NSOCIETE          PIC X(03).                       007  003
          05 :FGM70:-NZONE             PIC X(02).                       010  002
          05 :FGM70:-CHEFPROD          PIC X(05).                       012  005
          05 :FGM70:-WSEQFAM           PIC S9(05)      COMP-3.          017  003
          05 :FGM70:-LVMARKET1         PIC X(20).                       020  020
          05 :FGM70:-LVMARKET2         PIC X(20).                       040  020
          05 :FGM70:-LVMARKET3         PIC X(20).                       060  020
          05 :FGM70:-NCODIC2           PIC X(07).                       080  007
          05 :FGM70:-NCODIC            PIC X(07).                       087  007
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   05 FGM70-SEQUENCE          PIC S9(04) COMP.                           
      *--                                                                       
          05 :FGM70:-SEQUENCE          PIC S9(04) COMP-5.                       
      *}                                                                        
          05 :FGM70:-CAPPRO            PIC X(05).                       096  005
          05 :FGM70:-CFAM              PIC X(05).                       101  005
          05 :FGM70:-CMARQ             PIC X(05).                       106  005
          05 :FGM70:-CODLVM1           PIC X(01).                       111  001
          05 :FGM70:-CODLVM2           PIC X(01).                       112  001
          05 :FGM70:-CODLVM3           PIC X(01).                       113  001
          05 :FGM70:-CVDESCRIPTA       PIC X(05).                       114  005
          05 :FGM70:-CVDESCRIPTB       PIC X(05).                       119  005
          05 :FGM70:-DIFPRELEVE        PIC X(01).                       124  001
          05 :FGM70:-DRELEVE           PIC X(04).                       125  004
          05 :FGM70:-LCHEFPROD         PIC X(20).                       129  020
          05 :FGM70:-LCOMMENT          PIC X(02).                       149  002
          05 :FGM70:-LENSCONC          PIC X(15).                       151  015
          05 :FGM70:-LFAM              PIC X(20).                       166  020
          05 :FGM70:-LMAGASIN          PIC X(20).                       186  020
          05 :FGM70:-LREFFOURN         PIC X(20).                       206  020
          05 :FGM70:-LSTATCOMP         PIC X(03).                       226  003
          05 :FGM70:-LZONPRIX          PIC X(20).                       229  020
          05 :FGM70:-NCONC             PIC X(04).                       249  004
          05 :FGM70:-NMAG              PIC X(03).                       253  003
          05 :FGM70:-NMAGASIN          PIC X(03).                       256  003
          05 :FGM70:-NZONPRIX          PIC X(02).                       259  002
          05 :FGM70:-OAM               PIC X(01).                       261  001
          05 :FGM70:-WANO              PIC X(01).                       262  001
          05 :FGM70:-WANOE             PIC X(01).                       263  001
          05 :FGM70:-WDERO             PIC X(01).                       264  001
          05 :FGM70:-WDEROE            PIC X(01).                       265  001
          05 :FGM70:-WD15              PIC X(01).                       266  001
          05 :FGM70:-WEDIT             PIC X(01).                       267  001
          05 :FGM70:-WGROUPE           PIC X(01).                       268  001
          05 :FGM70:-WOA               PIC X(01).                       269  001
          05 :FGM70:-PCA1S             PIC S9(11)V9(2) COMP-3.          270  007
          05 :FGM70:-PCA4S             PIC S9(11)V9(2) COMP-3.          277  007
          05 :FGM70:-PCHT              PIC S9(07)V9(6) COMP-3.          284  007
          05 :FGM70:-PCOMMART          PIC S9(05)V9(2) COMP-3.          291  004
          05 :FGM70:-PCOMMARTE         PIC S9(05)V9(2) COMP-3.          295  004
          05 :FGM70:-PCOMMFAM          PIC S9(05)V9(2) COMP-3.          299  004
          05 :FGM70:-PCOMMSTD          PIC S9(05)V9(2) COMP-3.          303  004
          05 :FGM70:-PEXPTTC           PIC S9(07)V9(2) COMP-3.          307  005
          05 :FGM70:-PMBU1S            PIC S9(11)V9(2) COMP-3.          312  007
          05 :FGM70:-PMBU4S            PIC S9(11)V9(2) COMP-3.          319  007
          05 :FGM70:-PMTPRIMES         PIC S9(11)V9(2) COMP-3.          326  007
          05 :FGM70:-PRELEVE           PIC S9(07)V9(2) COMP-3.          333  005
          05 :FGM70:-PSTDMAG           PIC S9(07)V9(2) COMP-3.          338  005
          05 :FGM70:-PSTDTTC           PIC S9(07)V9(2) COMP-3.          343  005
          05 :FGM70:-QSTOCKDEP         PIC S9(05)      COMP-3.          348  003
          05 :FGM70:-QSTOCKMAG         PIC S9(05)      COMP-3.          351  003
          05 :FGM70:-QSTOCKMAGP        PIC S9(05)      COMP-3.          354  003
          05 :FGM70:-QSTOCKMAGS        PIC S9(05)      COMP-3.          357  003
          05 :FGM70:-QTAUXTVA          PIC S9(02)V9(3) COMP-3.          360  003
          05 :FGM70:-QV1S              PIC S9(07)      COMP-3.          366  004
          05 :FGM70:-QV2S              PIC S9(07)      COMP-3.          367  004
          05 :FGM70:-QV3S              PIC S9(07)      COMP-3.          371  004
          05 :FGM70:-QV4S              PIC S9(07)      COMP-3.          375  004
          05 :FGM70:-SRP               PIC S9(07)V9(2) COMP-3.          379  005
          05 :FGM70:-ZTRI              PIC S9(07)V9(2) COMP-3.          384  005
          05 :FGM70:-ZTRI2             PIC S9(07)      COMP-3.          389  004
          05 :FGM70:-DFINEFFET         PIC X(08).                       393  008
          05 :FGM70:-PSTDTTC2          PIC S9(07)V9(2) COMP-3.          343  005
          05 :FGM70:-WIMPERATIF        PIC X(01).                       393  008
          05 :FGM70:-WBLOQUE           PIC X(01).                       393  008
          05 FILLER                    PIC X(105).                      401  111
                                                                                
                                                                        512     
