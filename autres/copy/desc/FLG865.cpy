      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FLG865  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : EXTRACTION COMMUNE                          *         
      *                                                               *         
      *    FLG865       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:   210 OCTETS                                *         
      *    PENURIE = 1 SI GESTION PENURIE                             *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  DSECT-FLG865.                                                        
           05  FLG865-NSOCIETE                PIC X(03).                        
           05  FLG865-NLIEU                   PIC X(03).                        
           05  FLG865-NCODIC                  PIC X(07).                        
           05  FLG865-FILIALE                 PIC X(03).                        
           05  FLG865-NSOCDEPOT               PIC X(03).                        
           05  FLG865-NDEPOT                  PIC X(03).                        
           05  FLG865-ZPRIX                   PIC X(02).                        
           05  FLG865-DEXTRACTION             PIC X(08).                        
           05  FLG865-CGROUP                  PIC X(05).                        
           05  FLG865-PENURIE                 PIC S9(3) COMP-3.                 
           05  FLG865-QSTOCKMAG-GS36          PIC S9(5) COMP-3.                 
           05  FLG865-QSTOCKAR-GS36           PIC S9(5) COMP-3.                 
           05  FLG865-QSTOCKHS-GS36           PIC S9(5) COMP-3.                 
           05  FLG865-QSTOCKP-GS36            PIC S9(5) COMP-3.                 
           05  FLG865-QSTOCKT-GS36            PIC S9(5) COMP-3.                 
           05  FLG865-QSTOCKMAG-MU06          PIC S9(5) COMP-3.                 
           05  FLG865-QSTOCKT-MU06            PIC S9(5) COMP-3.                 
           05  FLG865-QMUTATT                 PIC S9(5) COMP-3.                 
           05  FLG865-WASSORTMAG              PIC S9(1) COMP-3.                 
           05  FLG865-QEXPO                   PIC S9(5) COMP-3.                 
           05  FLG865-QLS                     PIC S9(5) COMP-3.                 
           05  FLG865-QSO                     PIC S9(5) COMP-3.                 
           05  FLG865-QSA                     PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES0-HV04           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP0-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC0-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE0-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES1-HV04           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP1-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC1-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE1-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES2-HV04           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP2-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC2-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE2-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES3-HV04           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP3-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC3-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE3-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES4-HV04           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP4-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC4-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE4-HV04        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES0-HV12           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP0-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC0-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE0-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES1-HV12           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP1-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC1-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE1-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES2-HV12           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP2-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC2-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE2-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES3-HV12           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP3-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC3-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE3-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECES4-HV12           PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEMP4-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXC4-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-QPIECESEXE4-HV12        PIC S9(5) COMP-3.                 
           05  FLG865-PSTDTTC                 PIC S9(7)V99 COMP-3.              
           05  FLG865-WASSORSTD               PIC S9(1) COMP-3.                 
           05  FLG865-QSTOCKRES-GS36          PIC S9(5) COMP-3.                 
           05  FLG865-FILLER                  PIC X(05).                        
                                                                                
