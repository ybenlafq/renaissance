      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
!!!!  * CETTE DSECT DU FICHIER FGS100 A LE CODE VENDEUR SU 6 CARACTERES         
!!!!  * POUR ETRE ENTIEREMENT COMPATIBLE AVEC LE NEM                            
!!!!  * AU 31/01/2001 CECI EST UTILE QUE DANS LA CHAINE GS100                   
!!!!  * POUR EVITER TOUT PROBLEMES AVEC LES AUTRES CHAINES UTILISANT            
!!!!  * CETTE COPY, NOUS L'AVONS RENOMMEE FGS101                                
       01  FGS100-REC.                                                  00000010
                                                                        00000020
           03  FGS100-NSOCIETE         PIC  X(03).                      00000030
           03  FGS100-NLIEU            PIC  X(03).                      00000040
           03  FGS100-NZONPRIX         PIC  X(02).                      00000050
           03  FGS100-NUMCAIS          PIC  X(01).                      00000060
           03  FGS100-TERM             PIC  X(04).                      00000070
           03  FGS100-TRANS            PIC  X(04).                      00000080
           03  FGS100-DVENTE           PIC  X(08).                      00000090
           03  FGS100-CMODDEL          PIC  X(01).                      00000100
           03  FGS100-CTVA             PIC  X(05).                      00000110
           03  FGS100-NCODIC           PIC  X(07).                      00000120
           03  FGS100-CTYPE            PIC  X(05).                      00000130
           03  FGS100-LMARQUE          PIC  X(20).                      00000140
           03  FGS100-LREFERENCE       PIC  X(20).                      00000150
           03  FGS100-CTYPCONDT        PIC  X(05).                      00000160
02/01      03  FGS100-CVENDEUR         PIC  X(07).                      00000170
           03  FGS100-DJOUR            PIC  X(08).                      00000180
           03  FGS100-QPIECES          PIC  9(05)      COMP-3.          00000190
           03  FGS100-PVTTC            PIC  S9(07)V99  COMP-3.          00000200
           03  FGS100-PVTTCSR          PIC  S9(07)V99  COMP-3.          00000210
BF         03  FGS100-NCODIG           PIC  X(07).                      00000210
BF         03  FGS100-NSOCVTE          PIC  X(03).                      00000210
BF         03  FGS100-NLIEUVTE         PIC  X(03).                      00000210
BF         03  FGS100-NVENTE           PIC  X(07).                      00000210
BF         03  FGS100-FILLER           PIC  X(14).                      00000210
                                                                                
