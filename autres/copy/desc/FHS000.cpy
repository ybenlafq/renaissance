      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00004360
      * DSECT FICHIER COMPARAISON STOCK HS ET ENCOURS                   00004360
      * LG = 44                                                         00004360
      *                                                                 00004360
       01 DSECT-FHS000.                                                 00004460
      *                                                                 00004360
           05 FHS000-CTRT                PIC X(5).                              
           05 FHS000-CAPPLI              PIC X(1).                              
           05 FHS000-NSOCIETE            PIC X(3).                              
           05 FHS000-NLIEU               PIC X(3).                              
           05 FHS000-NSSLIEU             PIC X(3).                              
           05 FHS000-CLIEUTRT            PIC X(5).                              
           05 FHS000-NLIEUENT            PIC X(3).                              
           05 FHS000-NHS                 PIC X(7).                              
      *                                                                 00004450
           05 FHS000-NCODIC              PIC X(7).                              
           05 FHS000-QHS                 PIC S9(5) COMP-3.                      
           05 FHS000-QENC                PIC S9(5) COMP-3.                      
           05 FHS000-WSOLDE              PIC 9.                                 
                                                                                
