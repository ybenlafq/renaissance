      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGP805  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : SUIVI DES STOCKS DE DéBORDEMENT             *         
      *                                                               *         
      *    FGP805       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    STRUCTURE    :   SAM                                       *         
      *    LONGUEUR ENR.:   150                                       *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  :FGPXXX:-ENREG.                                                      
      *                                                         1  116          
        02 :FGPXXX:-DONNEE-ENTETE.                                              
           05  :FGPXXX:-NSOCIETE              PIC  X(03).                       
      *                                                         1   03          
           05  :FGPXXX:-NSOCDEPAFF            PIC  X(03).                       
      *                                                         4   03          
           05  :FGPXXX:-DEPOTAFF              PIC  X(03).                       
      *                                                         7   03          
           05  :FGPXXX:-NSOCDEPGEN            PIC  X(03).                       
      *                                                        10   03          
           05  :FGPXXX:-NDEPOTGEN             PIC  X(03).                       
      *                                                        13   03          
           05  :FGPXXX:-RAYON                 PIC  X(05).                       
      *                                                        16   05          
           05  :FGPXXX:-WSEQFAM               PIC  X(05).                       
      *                                                        21   05          
      *                                                                         
        02 :FGPXXX:-DONNEE-TRI.                                                 
      *                                                        26    1          
           05  :FGPXXX:-TRIDEP                PIC  X(01).                       
      *                                                                         
        02 :FGPXXX:-DONNEE-CODIC.                                               
      *                                                        27   39          
           05  :FGPXXX:-NCODIC                PIC  X(07).                       
      *                                                        27   07          
           05  :FGPXXX:-CFAM                  PIC  X(05).                       
      *                                                        34   05          
           05  :FGPXXX:-CMARQ                 PIC  X(05).                       
      *                                                        39   05          
           05  :FGPXXX:-LREFFOURN             PIC  X(20).                       
      *                                                        44   20          
           05  :FGPXXX:-LSTATCOMP             PIC  X(03).                       
      *                                                        64   03          
           05  :FGPXXX:-WOA                   PIC  X(01).                       
      *                                                        67   01          
           05  :FGPXXX:-SENSAPPRO             PIC  X(01).                       
      *                                                        68   01          
        02 :FGPXXX:-DONNEE-DEPOT.                                               
      *                                                        69 06            
           05  :FGPXXX:-NSOCDEP               PIC  X(03).                       
      *                                                        69   03          
           05  :FGPXXX:-NDEPOT                PIC  X(03).                       
      *                                                        72   03          
        02 :FGPXXX:-DONNEE-VENTE.                                               
      *                                                           75 26         
           05  :FGPXXX:-QS-0                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-1                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-2                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-3                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-4                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-V4S                   PIC S9(06) COMP-3.                
           05  :FGPXXX:-NBMAG                 PIC S9(03) COMP-3.                
           05  :FGPXXX:-QSTOCKMAG             PIC S9(05) COMP-3.                
      *                                                        64   03          
        02 :FGPXXX:-DONNEE-SORTIE.                                              
      *                                                      101                
           05  :FGPXXX:-SORTIES7J             PIC S9(05)   COMP-3.              
           05  :FGPXXX:-COUV7J                PIC S9(03)V9 COMP-3.              
      *                                                      107    3           
      *                                                                         
        02 :FGPXXX:-DONNEE-STOCK.                                               
      *                                                       110    4          
           05  :FGPXXX:-QSTOCKDIS             PIC S9(07) COMP-3.                
      *                                                       110    4          
           05  :FGPXXX:-QSTOCKRES             PIC S9(06) COMP-3.                
      *                                                       114    4          
      *                                                                         
        02 :FGPXXX:-DONNEE-COMMANDE.                                            
      *                                                       118               
           05  :FGPXXX:-STATUT                PIC  X(03).                       
      *                                                       118    3          
           05  :FGPXXX:-DCDE.                                                   
              10  :FGPXXX:-DCDE-YYYY             PIC  X(04).                    
              10  :FGPXXX:-DCDE-MMJJ             PIC  X(04).                    
      *                                                       121    8          
           05  :FGPXXX:-QCDE                  PIC S9(05) COMP-3.                
      *                                                       129    3          
           05  :FGPXXX:-QCDERES               PIC S9(05) COMP-3.                
      *                                                       132    3          
      *                                                                         
        02  :FGPXXX:-QSTKDISGEN               PIC S9(07) COMP-3.                
      *                                                       135    4          
        02 :FGPXXX:-FILLER                    PIC X(16).                        
      *                                                       139   12          
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
