      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  WW-FSV201.                                                   00000890
         03  FSV201-NOMPGM           PIC  X(07) VALUE 'FSV201 '.        00000900
         03  FSV201-APPDEST          PIC  X(03) .                       00000900
         03  FSV201-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV201.                                                  00000890
           05  FSV201-CFAMSAV          PIC  X(05).                      00000900
           05  FSV201-CMKG             PIC  X(05).                      00001090
           05  FSV201-LMKG             PIC  X(20).                      00001100
         03  FSV201-FILLEUR          PIC  X(159)  VALUE SPACES.         00001020
                                                                                
