      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *            FFC170 - FICHIER D'INTERFACE NCG -> SAGE            *        
      *----------------------------------------------------------------*        
       01 RECORD-FFC170.                                                        
      ***1** Type enreg = (E)nt�te, (L)igne, (T)exte, (P)ied                    
          03 FFC170-TYPE-ENREGISTREMENT                   PIC X(001).           
      ***2** Code statistique                                                   
          03 FFC170-STAT.                                                       
      ***2***** Num�ro de soci�t� DARTY : 907, 961, 945, ...                    
             05 FFC170-STAT-SOCIETE                       PIC X(003).           
      ***5***** Type d'op�ration : (V)ente                                      
             05 FFC170-STAT-OPERATION                     PIC X(001).           
      ***6** Num�ro s�quentiel interne DARTY                                    
          03 FFC170-NUMSEQ.                                                     
      ***6***** Num�ro s�quentiel : date de traitement                          
             05 FFC170-NUMSEQ-FDATE                       PIC X(006).           
      **12***** Num�ro s�quentiel : N� de la pi�ce = facture                    
             05 FFC170-NUMSEQ-NPIECE                      PIC 9(004).           
      **16***** Num�ro s�quentiel : N� de ligne de pi�ce                        
             05 FFC170-NUMSEQ-NLIGNE                      PIC 9(004).           
      **20** Un des quatre enregistrements possibles                            
          03 FFC170-DONNEES                               PIC X(454).           
      * ENTETE DE PIECE                                                         
       01 FFC170-ENTETE.                                                        
      **20** Num�ro de s�rie                                                    
          03 FFC170E-NUMSERIE                             PIC X(002).           
      **22** Date de pi�ce Facture ( date de traitement )                       
          03 FFC170E-DFACTURE                             PIC X(008).           
      **30** Mod�le de facture DARTY                                            
          03 FFC170E-MODELE                               PIC X(003).           
      **33** Code Client factur� DARTY ou client g�n�rique                      
          03 FFC170E-CLIENT                               PIC X(013).           
      **46** Adresse de facturation                                             
          03 FFC170E-ADRESSE-FACTURE.                                           
      **46***** Raison sociale Client factur�                                   
             05 FFC170E-AF-RAISONSOCIALE                  PIC X(032).           
      **78***** Adresse 1 Client factur�                                        
             05 FFC170E-AF-ADRESSE-1                      PIC X(032).           
      *110***** Adresse 2 Client factur�                                        
             05 FFC170E-AF-ADRESSE-2                      PIC X(032).           
      *142***** Code postal Client factur�                                      
             05 FFC170E-AF-CPOSTAL                        PIC X(008).           
      *150***** Ville Client factur�                                            
             05 FFC170E-AF-VILLE                          PIC X(032).           
      *182***** Code pays Client factur� DARTY 2                                
             05 FFC170E-AF-PAYS                           PIC X(003).           
      *185** Num�ro intraco. Client factur� DARTY 14                            
          03 FFC170E-INTRACO                              PIC X(014).           
      *199** Adresse de livraison                                               
          03 FFC170E-ADRESSE-LIVRAISON.                                         
      *199***** Raison sociale Client livr�                                     
             05 FFC170E-AL-RAISONSOCIALE                  PIC X(032).           
      *231***** Adresse 1 Client livr�                                          
             05 FFC170E-AL-ADRESSE-1                      PIC X(032).           
      *263***** Adresse 2 Client livr�                                          
             05 FFC170E-AL-ADRESSE-2                      PIC X(032).           
      *295***** Code postal Client livr�                                        
             05 FFC170E-AL-CPOSTAL                        PIC X(008).           
      *303***** Ville Client livr�                                              
             05 FFC170E-AL-VILLE                          PIC X(032).           
      *335***** Pays Client livr� DARTY 32                                      
             05 FFC170E-AL-PAYS                           PIC X(003).           
      *338** R�f�rence Commande = num�ro de vente                               
          03 FFC170E-REFDAR-NUMVENTE                      PIC X(024).           
      *362** Date de la commande DARTY                                          
          03 FFC170E-REFDAR-DCOMMANDE                     PIC X(008).           
      *370** Date de livraison DARTY                                            
          03 FFC170E-REFDAR-DLIVRAISON                    PIC X(008).           
      *378** Service DARTY                                                      
          03 FFC170E-REFDAR-SERVICE                       PIC X(020).           
      *398** R�f�rence Commande du client                                       
          03 FFC170E-REFCLT-NCOMMANDE                     PIC X(024).           
      *422** date de commande client                                            
          03 FFC170E-REFCLT-DCOMMANDE                     PIC X(008).           
      *430** Service du client                                                  
          03 FFC170E-REFCLT-SERVICE                       PIC X(020).           
      *450** Num�ro dossier du client                                           
          03 FFC170E-REFCLT-DOSSIER                       PIC X(019).           
      *469** flag HT / TTC                                                      
          03 FFC170E-WFACT-TTC                            PIC X(001).           
      *470** Devise                                                             
          03 FFC170E-DEVISE                               PIC X(002).           
      *470** D�p�t                                                              
          03 FFC170E-DEPOT                                PIC X(002).           
      * LIGNE DE PIECE FACTURE                                                  
       01 FFC170-LIGNE.                                                         
      **20** Code article SAGE                                                  
          03 FFC170L-ARTICLE                              PIC X(016).           
      **36** Code article DARTY = codic                                         
          03 FFC170L-CODIC                                PIC X(007).           
      **43** D�signation de l'article                                           
          03 FFC170L-DESIGNATION                          PIC X(032).           
      **75** Quantit� livr�e                                                    
          03 FFC170L-QUANTITE                             PIC X(006).           
      **81** Prix unitaire HT                                                   
          03 FFC170L-PUHT                                 PIC -(10)9V99.        
      **94** Prix unitaire TTC                                                  
          03 FFC170L-PUTTC                                PIC -(10)9V99.        
      *107** Montant total ligne HT                                             
          03 FFC170L-PTHT                                 PIC -(10)9V99.        
      *120** Montant total ligne TTC                                            
          03 FFC170L-PTTTC                                PIC -(10)9V99.        
      *133** Taux de TVA ligne                                                  
          03 FFC170L-TVA                                  PIC Z9V99.            
      ******                                                                    
          03 FFC170L-FILLER                               PIC X(337).           
      * LIGNE DE TEXTE FACTURE                                                  
       01 FFC170-TEXTE.                                                         
      **20** Code article SAGE                                                  
          03 FFC170T-LIBELLE                              PIC X(050).           
      ******                                                                    
          03 FFC170T-FILLER                               PIC X(403).           
      * PIED DE FACTURE                                                         
       01 FFC170-PIED.                                                          
      **20** Tableau des taxes                                                  
          03 FFC170P-TAXES OCCURS 4.                                            
             05 FFC170P-TAXBASE                           PIC -(10)9V99.        
             05 FFC170P-TAXMONTANT                        PIC -(10)9V99.        
             05 FFC170P-TAXTOTAL                          PIC -(10)9V99.        
      *176** Montant HT total                                                   
          03 FFC170P-TOTAL-HT                             PIC -(10)9V99.        
      *189** Montant taxe Total                                                 
          03 FFC170P-TOTAL-TAXE                           PIC -(10)9V99.        
      *202** Montant TTC                                                        
          03 FFC170P-TOTAL-TTC                            PIC -(10)9V99.        
      *215** Montant pay�                                                       
          03 FFC170P-MTVERSE                              PIC -(10)9V99.        
      *228** Date de r�glement de l'acompte                                     
          03 FFC170P-DTVERSE                              PIC X(008).           
      *236** Solde restant d�                                                   
          03 FFC170P-RESTEDU                              PIC -(10)9V99.        
      *249** Date d'�ch�ance pour les clients g�n�riques                        
          03 FFC170P-ECHEANCE                             PIC X(008).           
      *257** Mode de r�glement pour les clients SAGE                            
          03 FFC170P-MODE-REGLEMENT                       PIC X(008).           
      ******                                                                    
          03 FFC170P-FILLER                               PIC X(208).           
                                                                                
