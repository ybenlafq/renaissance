      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * VSAM ==> FICHIER ==> DESCRIPTION ************************* AIDA         
      * KSDS     FSETERM     FICHIER DES TERMINAUX (COBOL)                      
      ********************************************************** FSET1C         
       01  FSET1.                                                               
           02  FSET1-FSET1K.                                                    
               03 FSET1-FSET1K-TERMID  PIC  X(04).                              
           02  FSET10-PR1              PIC  X(04).                              
           02  FSET10-PR2              PIC  X(04).                              
           02  FSET10-SIGNAT.                                                   
               03 FSET10-GRP           PIC  X(08).                              
               03 FSET10-SGP           PIC  X(08).                              
               03 FSET10-UTI           PIC  X(08).                              
           02  FSET10-CRE-DAT          PIC S9(07) COMP-3.                       
           02  FSET10-CRE-USR          PIC  X(08).                              
           02  FSET10-MAJ-DAT          PIC S9(07) COMP-3.                       
           02  FSET10-MAJ-USR          PIC  X(08).                              
           02  FSET10-ANU-DAT          PIC S9(07) COMP-3.                       
           02  FSET10-ANU-USR          PIC  X(08).                              
           02  FSET10-FIL              PIC  X(28).                              
      *---------------------------------------------------------------*         
      * ATTENTION                                                     *         
      * CE COPY COBOL FSET1C A UNE EQUIVALANCE EN PL1 INCLUDE FSET1   *         
      * TOUTES LES MODIFICATIONS DOIVENT ETRE RECIPROQUES             *         
      *---------------------------------------------------------------*         
       EJECT                                                                    
                                                                                
