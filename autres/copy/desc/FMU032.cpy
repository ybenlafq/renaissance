      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DES MUTATIONS                   *        
      *----------------------------------------------------------------*        
       01  DSECT-FMU032.                                                        
         05 CHAMPS-FMU032.                                                      
           10 FMU032-NSOCENTR           PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-NDEPOT             PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-NSOCIETE           PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-NLIEU              PIC X(03).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-CSELART            PIC X(05).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-NMUTATION          PIC X(07).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-DMVT               PIC X(08).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-NCODIC             PIC X(07).                              
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-QMVT               PIC Z(05) VALUE ZERO.                   
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-PRMP               PIC -(9)9,9(6) VALUE ZERO.              
           10 FMU032-PRMP-0 REDEFINES FMU032-PRMP                               
                                        PIC -(10),-(6).                         
           10 FILLER                    PIC X VALUE ';'.                        
           10 FMU032-VAL                PIC -(9)9,9(6) VALUE ZERO.              
           10 FMU032-VAL-0 REDEFINES FMU032-VAL                                 
                                        PIC -(10),-(6).                         
       01 FMU032-LIBELLES.                                                      
         05 FILLER                      PIC X(09) VALUE 'NSOCDEPOT'.            
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'NDEPOT'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'NSOCIETE'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'NLIEU'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'CSELART'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(09) VALUE 'NMUTATION'.            
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'DMVT'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'ARTICLE'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'QMVT'.                 
         05 FILLER                      PIC X     VALUE ';'.                    
         05 FILLER                      PIC X(10) VALUE 'PRMPRECYCL'.           
         05 FILLER                      PIC X     VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'VALEUR'.               
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FMU032-LONG            PIC S9(4) COMP  VALUE +88.              
      *                                                                         
      *--                                                                       
       01  DSECT-FMU032-LONG            PIC S9(4) COMP-5  VALUE +88.            
                                                                                
      *}                                                                        
