      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000880
       01  WW-FSV018.                                                   00000890
         03  FSV018-NOMPGM  .                                           00000900
           05  FSV018-NOMPROG          PIC  X(07) VALUE 'FSV018' .      00000900
           05  FSV018-APPDEST          PIC  X(03).                      00000900
         03  FSV018-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV018.                                                  00000890
           05  FSV018-CODEAPP     .                                     00000900
              10  FSV018-TAPP             PIC  X(05).                      00001
              10  FSV018-TCTLG            PIC  X(03).                      00001
           05  FSV018-FAMCIB           PIC  X(05).                      00001090
           05  FSV018-CMARQ            PIC  X(05).                      00001100
           05  FSV018-PRODUIT          PIC  X(07).                      00000900
         03 FSV018-FILLEUR          PIC  X(120) VALUE SPACES.           00001020
         03  FSV018-CLE-TRI      .                                              
           05  FSV018-CLE-XXX          PIC  X(30)  VALUE SPACES.           00001
      *                                                                 00000880
       01  WW-FSV019.                                                   00000890
         03  FSV019-NOMPGM  .                                           00000900
           05  FSV019-NOMPROG          PIC  X(07) VALUE 'FSV019' .      00000900
           05  FSV019-APPDEST          PIC  X(03).                      00000900
         03  FSV019-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV019.                                                  00000890
           05  FSV019-CODEAPP     .                                     00000900
              10  FSV019-TAPP             PIC  X(05).                      00001
              10  FSV019-TCTLG            PIC  X(03).                      00001
           05  FSV019-FAMCIB           PIC  X(05).                      00001090
           05  FSV019-CMARQ            PIC  X(05).                      00001100
         03 FSV019-FILLEUR          PIC  X(127) VALUE SPACES.           00001020
         03  FSV019-CLE-TRI      .                                              
           05  FSV019-CLE-XXX          PIC  X(30)  VALUE SPACES.           00001
      *                                                                 00001320
                                                                                
