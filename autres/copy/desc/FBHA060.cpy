      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  W-ENR-FBHA06.                                                        
           05 FBHA06-ENTREPOT        PIC X(05).                                 
           05 FILLER                 PIC X VALUE ' '.                           
           05 FBHA06-SOCIETE         PIC X(03).                                 
           05 FILLER                 PIC X VALUE ' '.                           
           05 FBHA06-DEPOT           PIC X(03).                                 
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-FAMILLE         PIC X(9).                                  
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-MARQUE          PIC X(6).                                  
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-CODIC           PIC X(8).                                  
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-REF             PIC X(21).                                 
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-STA             PIC X(5).                                  
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-MUTATION        PIC X(8).                                  
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-1ERE-RECEP      PIC X(22).                                 
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-RECEP-EFF       PIC X(19).                                 
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-STK-DISPO       PIC X(12).                                 
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FBHA06-RESERVE         PIC X(15).                                 
           05 FILLER                 PIC X  VALUE ';'.                          
           05 FILLER                 PIC X(7) VALUE SPACES.                     
      * FIN                                                                     
                                                                                
