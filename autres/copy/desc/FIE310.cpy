      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FIE310-DSECT.                                                00000100
           02 FIE310-NSOCDEPOT            PIC X(03).                    00000200
           02 FIE310-NDEPOT               PIC X(03).                    00000210
           02 FIE310-NSSLIEU              PIC X(03).                    00000220
           02 FIE310-WSEQFAM              PIC S9(5) COMP-3.             00000230
           02 FIE310-CMARQ                PIC X(05).                    00000230
           02 FIE310-NCODIC               PIC X(07).                    00000230
           02 FIE310-CFAM                 PIC X(05).                    00000230
           02 FIE310-LREFFOURN            PIC X(20).                    00000230
           02 FIE310-CLIEUTRT             PIC X(05).                    00000230
           02 FIE310-QSTOCKAV             PIC S9(5) COMP-3.             00000230
           02 FIE310-QSTOCKAP             PIC S9(5) COMP-3.             00000230
           02 FIE310-ECART                PIC S9(5) COMP-3.             00000230
           02 FIE310-DATEINV              PIC X(08).                    00000230
           02 FILLER                      PIC X(09).                    00000230
                                                                                
