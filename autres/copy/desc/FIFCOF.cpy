      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 DSECT-COFINOGA.                                                       
1         05 COF-CODENR                   PIC X(02).                            
3         05 COF-TYPMT                    PIC X(02).                            
5         05 COF-SENS                     PIC X(01).                            
6         05 COF-DTRANS                   PIC X(08).                            
14        05 COF-HTRANS                   PIC X(04).                            
18        05 COF-DREMISE                  PIC X(08).                            
26        05 COF-IDCONTRAT                PIC X(12).                            
38        05 COF-NVENTE                   PIC X(13).                            
51        05 COF-NREMISE                  PIC X(06).                            
57        05 COF-NPORTEUR                 PIC X(19).                            
76        05 COF-NAUTOR                   PIC X(06).                            
82        05 COF-MTBRUT                   PIC 9(08)V99.                         
92        05 COF-MTNET                    PIC 9(08)V99.                         
102       05 COF-MTCOM                    PIC 9(08)V99.                         
112       05 COF-DEVISE                   PIC X(10).                            
115       05 FILLER                       PIC X(35).                            
       01 COF-ENTETE.                                                           
1         05 COF-CODENR-TETE              PIC X(02).                            
3         05 FILLER                       PIC X(03).                            
6         05 COF-DCREAT-TETE.                                                   
             10 COF-DCREAT-TETE-SS        PIC X(02).                            
             10 COF-DCREAT-TETE-AA        PIC X(02).                            
             10 COF-DCREAT-TETE-MM        PIC X(02).                            
             10 COF-DCREAT-TETE-JJ        PIC X(02).                            
14        05 COF-HCREAT-TETE              PIC X(04).                            
       01 COF-FIN.                                                              
1         05 COF-CODENR-FIN               PIC X(02).                            
3         05 FILLER                       PIC X(03).                            
6         05 COF-DCREAT-FIN               PIC X(08).                            
14        05 COF-HCREAT-FIN               PIC X(04).                            
18        05 FILLER                       PIC X(33).                            
51        05 COF-NBENR                    PIC 9(06).                            
57        05 FILLER                       PIC X(25).                            
82        05 COF-TOTBRUT                  PIC 9(10).                            
92        05 COF-TOTNET                   PIC 9(10).                            
102       05 COF-TOTCOM                   PIC 9(10).                            
                                                                                
