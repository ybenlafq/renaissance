      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      ******************************************************************        
      *               F I C H E   D E S C R I P T I V E                *        
      ******************************************************************        
      *                                                                *        
      *  PROJET      : ETIQUETTE                                       *        
      *  COPY        : FETQEEG                                         *        
      *  CREATION    : 25/06/2015                                      *        
      *  FONCTION    : DSECT DU FICHIER DU REFERENTIEL PRIX ENVOYE     *        
      *                A EEG (ETIQUETTES ELECTRONIQUES GONDOLE         *        
      *  UTLISATION  : CETTE DSECT EST UTILISEE PAR LE BNV700          *        
      *                                                                *        
      ******************************************************************        
      ******************************************************************        
      *                                                                *        
      * LONGUEUR DU FICHIER : 104                                      *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       01  :FETQEEG:-ENREGISTREMENT.                                            
           03 :FETQEEG:-MAG.                                                    
              05 :FETQEEG:-NSOCIETE  PIC X(03).                                 
              05 :FETQEEG:-NLIEU     PIC X(03).                                 
           03 :FETQEEG:-NCODIC       PIC X(07).                                 
      *    DATE/HEURE DE DERNIERE MODIFICATION AU FORMAT YYYYMMDDHHMISS         
           03 :FETQEEG:-DEFFET       PIC X(14).                                 
      *    PRIX TTC                                                             
           03 :FETQEEG:-PRIX         PIC 9(08).                                 
      *    PRIME TTC                                                            
           03 :FETQEEG:-PRIME        PIC 9(05).                                 
      *    ECOTAXE                                                              
           03 :FETQEEG:-ECO          PIC 9(05).                                 
      *    PRIX UNITAIRE (SI COLISAGE) TTC                                      
           03 :FETQEEG:-PRIX-UNIT    PIC 9(08).                                 
      *    PRIX UNITAIRE HORS GRATUIT  TTC                                      
           03 :FETQEEG:-PRIX-HORSG   PIC 9(08).                                 
      *    OFFRE ACTIVE : 1 / OFFRE ACTIVE - 0 / OFFRE NON ACTIVE               
           03 :FETQEEG:-WOA          PIC X(01).                                 
      *    STATUT D'APPROVISIONNEMENT                                           
           03 :FETQEEG:-CAPPRO       PIC X(05).                                 
      *    QUANTIEME DERNIERE DATE D'EFFET PRIX                                 
           03 :FETQEEG:-QDEFFET      PIC 9(03).                                 
      *    TAXE ADDITIONNELLE                                                   
           03 :FETQEEG:-CTAXE1       PIC X(02).                                 
           03 :FETQEEG:-ECO-TTC1     PIC 9(05).                                 
      *    TAXE ADDITIONNELLE                                                   
           03 :FETQEEG:-CTAXE2       PIC X(02).                                 
           03 :FETQEEG:-ECO-TTC2     PIC 9(05).                                 
      *    TAXE ADDITIONNELLE                                                   
           03 :FETQEEG:-CTAXE3       PIC X(02).                                 
           03 :FETQEEG:-ECO-TTC3     PIC 9(05).                                 
      *    EAN13 DU CODIC                                                       
           03 :FETQEEG:-EAN          PIC X(13).                                 
      *                                                                         
                                                                                
