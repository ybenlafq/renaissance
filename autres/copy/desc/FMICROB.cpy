      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *---------------------------------------------------------                
      *   ENREGISTREMENT MICRO VERS LA COMPTA GROUPE                            
      *---------------------------------------------------------                
      *                                                                         
       01  FMICROB.                                                             
           02  FMICRO-CAPPLI    PIC X(0003).                                    
      *        CODE APPLICATION                                                 
           02  FMICRO-NSOCORIG  PIC X(0003).                                    
      *        CODE SOCIETE ORIGINE                                             
           02  FMICRO-NLIEUORIG PIC X(0003).                                    
      *        CODE LIEU ORIGINE                                                
           02  FMICRO-NSOCDEST  PIC X(0003).                                    
      *        CODE SOCIETE DEST                                                
           02  FMICRO-NLIEUDEST PIC X(0003).                                    
      *        CODE LIEU DEST                                                   
           02  FMICRO-CTYPFACT  PIC X(0002).                                    
      *        TYPE DE FACTURE CONVERTIR PAR FGTYP                              
           02  FMICRO-NATFACT   PIC X(0005).                                    
      *        NATURE                                                           
           02  FMICRO-CVENT     PIC X(0005).                                    
      *        CRITERES DE VENTILATION                                          
           02  FMICRO-MONTHT    PIC S9(11)V9(0002).                             
      *        MONTANT HORS TAXES                                               
           02  FMICRO-CTAUXTVA  PIC X(0005).                                    
      *        CODE TVA                                                         
           02  FMICRO-DMOUV     PIC X(0008).                                    
      *        DATE DU MOUVEMENT                                                
           02  FMICRO-QNBPIECES PIC S9(0005).                                   
      *        NOMBRE DE PIECES                                                 
           02  FMICRO-FACT.                                                     
             05  FMICRO-NUMFACT   PIC X(0007).                                  
      *        NUMERO FACTURE                                                   
AM           05  FMICRO-DVALEUR   PIC X(0008).                                  
AM    *        DATE VALEUR (REMISE A ZERO COMPTE COURANT)                       
AM           05  FMICRO-FILLER    PIC X(0007).                                  
      *        RESTE LIBRE                                                      
           02  FMICRO-LIB REDEFINES FMICRO-FACT.                                
             05  FMICRO-LIBELLE   PIC X(0022).                                  
      *        LIBELLE LIGNE                                                    
           02  FMICRO-NTIERSE   PIC X(0008).                                    
      *        TIERS EMETTEUR                                                   
           02  FMICRO-NLETTRE   PIC X(0010).                                    
      *        LETTRAGE EMETTEUR                                                
           02  FMICRO-NTIERSR   PIC X(0008).                                    
      *        TIERS RECEPTEUR                                                  
           02  FMICRO-NLETTRR   PIC X(0010).                                    
      *        LETTRAGE RECEPTEUR                                               
                                                                                
