      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION POUR AXAPTA                     *        
      *             EXTRACTION DU STOCK VENDABLE                       *        
      *             ET COMMANDES FOURNISSEUR PAR DEPOT                 *        
      *----------------------------------------------------------------*        
       01  DSECT-FEX100.                                                        
         05 CHAMPS-FEX100.                                                      
           10 FEX100-CFAM               PIC X(05) VALUE SPACES.         122  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-CMARQ              PIC X(06) VALUE SPACES.         094  006
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-NCODIC             PIC X(07) VALUE SPACES.         197  007
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-LREFFOURN          PIC X(20) VALUE SPACES.         100  020
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-LSTATCOMP          PIC X(3) VALUE SPACES.          127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-WSENSAPPRO         PIC X(1) VALUE SPACES.          127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-OFFRE-ACT          PIC X(1) VALUE SPACES.          127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-PENURIE            PIC X(1) VALUE SPACES.          127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-QSTOCK             PIC -9999.                      207  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-QCDE               PIC -9999.                      207  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-QCDERES            PIC -9999.                      207  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-DCDE               PIC X(10) VALUE SPACES.         048  003
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-DSTOCK             PIC X(10) VALUE SPACES.         048  003
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-QPOIDS             PIC -9(7) VALUE SPACES.         048  003
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-CORIGPROD          PIC X(05) VALUE SPACES.         048  003
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-NEAN               PIC X(13) VALUE SPACES.         048  003
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 FEX100-NCODICK            PIC X(07) VALUE SPACES.         048  003
           10 FILLER                    PIC X VALUE ';'.                007  002
AD2        10 FEX100-QMUT               PIC -9999.                      048  003
AD2        10 FILLER                    PIC X VALUE ';'.                007  002
      *  05 FILLER                      PIC X(038).                             
AD2   *  05 FILLER                      PIC X(001) VALUE SPACES.                
AD2      05 FILLER                      PIC X(015) VALUE SPACES.                
       01 FEX100-LIBELLES.                                                      
         05 FILLER                      PIC X(04) VALUE 'CFAM'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'CMARQ'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'NCODIC'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(09) VALUE 'LREFFOURN'.            
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'STATCOMP'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(10) VALUE 'WSENSAPPRO'.           
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(12) VALUE 'OFFRE ACTIVE'.         
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'PENURIE'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'QSTOCK'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'QCDE'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'QCDERES'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'DCDE'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'DSTOCK'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'QPOIDS'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'CORIG'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(04) VALUE 'NEAN'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'NCODICK'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
AD2      05 FILLER                      PIC X(04) VALUE 'QMUT'.                 
AD2      05 FILLER                      PIC X(01) VALUE ';'.                    
AD2   *  05 FILLER                      PIC X(03) VALUE SPACES.                 
         05 FILLER                      PIC X(18) VALUE SPACES.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FEX100-LONG           PIC S9(4)   COMP  VALUE +150.            
      *                                                                         
      *--                                                                       
       01  DSECT-FEX100-LONG           PIC S9(4) COMP-5  VALUE +150.            
                                                                                
      *}                                                                        
