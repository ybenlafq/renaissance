      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FAV000                            *00030016
      *       LONGUEUR D'ENREGISTREMENT : 100 C.                       *00050012
      *================================================================*00060000
      *                                                                 00070000
       01  FILLER.                                                      00080000
      *                                                                 00090000
           05  FAV000-ENREG              PIC  X(100).                   00100016
      *                                                                 00140012
           05  FAV000-ENTETE REDEFINES FAV000-ENREG.                    00150017
             10 FAV000-ARTICLE-E         PIC  X(03).                    00160016
             10 FAV000-PARTENR-E         PIC  X(08).                    00170016
             10 FAV000-IDENFIC-E         PIC  X(12).                    00180016
             10 FAV000-DATCREA-E         PIC  X(08).                    00190016
             10 FAV000-HEUCREA-E         PIC  X(06).                    00200016
             10 FAV000-NUMFICH-E         PIC  9(08).                    00210016
             10 FILLER                   PIC  X(55).                    00220012
      *                                                                 00460000
           05  FAV000-DETAIL REDEFINES FAV000-ENREG.                    00470017
             10 FAV000-ARTICLE-D         PIC  X(03).                    00480016
             10 FAV000-SOCIETE-D         PIC  X(03).                    00481016
             10 FAV000-CONTRAT-D         PIC  X(11).                    00482016
             10 FAV000-NUMCHEQ-D         PIC  X(12).                    00483016
             10 FAV000-MTTCHEQ-D         PIC  9(07)V99.                 00484016
             10 FAV000-DATEMIS-D         PIC  X(08).                    00485016
             10 FAV000-DATFVAL-D         PIC  X(08).                    00486016
             10 FAV000-NAPPORT-D         PIC  X(11).                    00486116
             10 FILLER                   PIC  X(35).                    00487013
      *                                                                 00490012
           05  FAV000-ENRFIN REDEFINES FAV000-ENREG.                    00500017
             10 FAV000-ARTICLE-F         PIC  X(03).                    00510016
             10 FAV000-PARTENR-F         PIC  X(08).                    00511016
             10 FAV000-IDENFIC-F         PIC  X(12).                    00512016
             10 FAV000-NBRLIGN-F         PIC  9(08).                    00513016
             10 FILLER                   PIC  X(69).                    00514012
      *                                                                 00520012
                                                                                
