      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FPR930 D'EXTRACTION DES           *00030001
      *       PRESTATIONS D'ASSURANCE AXA                              *00040001
      *       FICHIER EN SORTIE DE BPR930 ET EN ENTREE DE BPR931       *00041001
      *       LONGUEUR D'ENREGISTREMENT : 359 C.                       *00050001
      *================================================================*00060000
      *                                                                 00070000
       01  FILLER.                                                      00080000
      *                                                                 00090000
           05  FPR930-ENREG.                                            00100001
JM1803         10  FILLER                PIC  X(01) VALUE ','.          00110001
               10  FILLER                PIC  X(01) VALUE '"'.          00110101
               10  FPR930-NSOCIETE       PIC  X(03).                    00111001
               10  FILLER                PIC  X(03) VALUE '","'.        00112001
               10  FPR930-NLIEU          PIC  X(03).                    00120001
               10  FILLER                PIC  X(03) VALUE '","'.        00121001
               10  FPR930-LLIEU          PIC  X(20).                    00122001
               10  FILLER                PIC  X(03) VALUE '","'.        00130001
               10  FPR930-NVENTE         PIC  X(07).                    00151001
               10  FILLER                PIC  X(03) VALUE '","'.        00152001
               10  FPR930-DDELIV         PIC  X(08).                    00159101
               10  FILLER                PIC  X(03) VALUE '","'.        00159201
               10  FPR930-CENREG         PIC  X(05).                    00159301
               10  FILLER                PIC  X(03) VALUE '","'.        00159401
               10  FPR930-LPRESTATION    PIC  X(20).                    00159501
               10  FILLER                PIC  X(03) VALUE '","'.        00159601
               10  FPR930-NCODIC         PIC  X(07).                    00159701
               10  FILLER                PIC  X(03) VALUE '","'.        00159801
               10  FPR930-LREFFOURN      PIC  X(20).                    00159901
               10  FILLER                PIC  X(03) VALUE '","'.        00160001
               10  FPR930-QVENDUE        PIC  9(05).                    00160101
               10  FILLER                PIC  X(02) VALUE '",'.         00160201
               10  FPR930-PVUNITF        PIC  ------9.V99.              00160301
               10  FILLER                PIC  X(01) VALUE ','.          00160401
               10  FPR930-PVTOTAL        PIC  ------9.V99.              00160501
               10  FILLER                PIC  X(02) VALUE ',"'.         00160601
               10  FPR930-CTITRENOM      PIC  X(05).                    00160701
               10  FILLER                PIC  X(03) VALUE '","'.        00160801
               10  FPR930-LNOM           PIC  X(25).                    00160901
               10  FILLER                PIC  X(03) VALUE '","'.        00161001
               10  FPR930-LPRENOM        PIC  X(15).                    00161101
               10  FILLER                PIC  X(03) VALUE '","'.        00161201
               10  FPR930-CVOIE          PIC  X(05).                    00161301
               10  FILLER                PIC  X(03) VALUE '","'.        00161401
               10  FPR930-CTVOIE         PIC  X(04).                    00161501
               10  FILLER                PIC  X(03) VALUE '","'.        00161601
               10  FPR930-LNOMVOIE       PIC  X(21).                    00162001
               10  FILLER                PIC  X(03) VALUE '","'.        00162101
               10  FPR930-LCOMMUNE       PIC  X(32).                    00163001
               10  FILLER                PIC  X(03) VALUE '","'.        00163101
               10  FPR930-CPOSTAL        PIC  X(05).                    00164001
               10  FILLER                PIC  X(03) VALUE '","'.        00164101
               10  FPR930-CINSEE         PIC  X(05).                    00165001
               10  FILLER                PIC  X(03) VALUE '","'.        00165101
               10  FPR930-WANNULATION    PIC  X(06).                    00165201
               10  FILLER                PIC  X(03) VALUE '","'.        00165301
               10  FPR930-DANNULATION    PIC  X(08).                    00165401
               10  FILLER                PIC  X(03) VALUE '","'.        00165501
               10  FPR930-DDEBPER        PIC  X(08).                    00165601
               10  FILLER                PIC  X(03) VALUE '","'.        00165701
               10  FPR930-DFINPER        PIC  X(08).                    00165801
               10  FILLER                PIC  X(03) VALUE '","'.        00165901
      *        10  FILLER                PIC  X(21).                    00166001
JM1803*        10  FILLER                PIC  X(20).                    00166101
AL1401         10  FPR930-INFOS          PIC  X(18).                    00166301
               10  FILLER                PIC  X(01) VALUE '"'.          00167001
AL1401         10  FPR930-NFICHIER       PIC  X(02).                    00168001
                                                                                
