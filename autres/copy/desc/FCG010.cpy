      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************           
      *   FICHIER DES MOUVEMENTS COMPTABLES TRANSCODEES POUR SAP    *           
      *        - FICHIER INTERMEDIAIRE INTERFACE SAP -              *           
      *                                                             *           
      *      - DSECT COBOL                                          *           
      *      - NOM = FCG010                                         *           
      *      - RECSIZE = 200                                        *           
      *                                                             *           
      ***************************************************************           
       01 FCG010-ENR.                                                           
      *       NUMERO DE PIECE SAP                                               
  1       05  FCG010-NPIECESAP     PIC X(02).                                   
      *       NUMERO DE POSTE                                                   
  3       05  FCG010-NPOSTE        PIC 9(03).                                   
          05  FCG010-POSTE.                                                     
              10 FCG010-CLE.                                                    
      *             NUMERO DE SOCIETE                                           
  6              15 FCG010-NSOC                PIC X(05).                       
      *              CODE ETABLISSEMENT                                         
 11              15  FCG010-NETAB              PIC X(03).                       
      *              NUMERO DE JOURNAL GCT                                      
 14              15  FCG010-NJRN               PIC X(03).                       
 17              15  FILLER                    PIC X(01).                       
      *              CODE INTERFACE                                             
 18              15  FCG010-CINTERFACE         PIC X(05).                       
      *              NUMERO DE PIECE                                            
 23              15  FCG010-NPIECE             PIC X(10).                       
      *           TYPE DE PIECE SAP                                             
 33           10  FCG010-TYPPC                 PIC X(02).                       
      *           DATE DE DOCUMENT = DATE DE PIECE                              
 35           10  FCG010-DDOC                  PIC X(08).                       
      *           DATE DU MOUVEMENT = DATE COMPTABLE                            
 43           10  FCG010-DMVT                  PIC X(08).                       
      *           CCT - DATE DE TRAITEMENT                                      
 51           10  FCG010-DTRAITEMENT           PIC X(08).                       
      *           CODE TRANSACTION SAP                                          
 59           10  FCG010-CTRANSAC              PIC X(04).                       
      *           REFERENCE DE DOCUMENT                                         
 63           10  FCG010-REFDOC                PIC X(12).                       
      *           CODE DEVISE                                                   
 75           10  FCG010-CDEVISE               PIC X(03).                       
      *           CLE DE COMPTABILISATION SAP                                   
 78           10  FCG010-CLECOMPTA             PIC X(02).                       
      *           TYPE DE COMPTE                                                
 80           10  FCG010-TYPCPT                PIC X(01).                       
      *           CODE TVA                                                      
 81           10  FCG010-CTVA                  PIC X(02).                       
      *           MONTANT                                                       
 83           10  FCG010-PMONTMVT              PIC 9(13)V99 COMP-3.             
      *           SENS DU MONTANT : DEBIT OU CREDIT                             
 91           10  FCG010-CMONTMVT              PIC X(01).                       
      *           MONTANT DE BASE TVA                                           
 92           10  FCG010-MONTBASTVA            PIC 9(13)V99 COMP-3.             
      *           TYPE DE TVA SAP                                               
100           10  FCG010-TYPTVA                PIC X(01).                       
      *           CLE D'OPERATION SAP                                           
101           10  FCG010-CLEOPE                PIC X(03).                       
      *           LETTRAGE                                                      
104           10  FCG010-NLETTRAGE             PIC X(10).                       
      *           LIBELLE DE MOUVEMENT                                          
114           10  FCG010-LMVT                  PIC X(25).                       
      *           NUMERO SOCIETE APPARENTE                                      
139           10  FCG010-NSTEAPP               PIC X(05).                       
      *           NUMERO DE SECTION                                             
144           10  FCG010-NSECTION              PIC X(10).                       
      *           NUMERO DE COMPTE SAP                                          
154           10  FCG010-CPTSAP                PIC X(08).                       
      *           CENTRE DE PROFIT SAP                                          
162           10  FCG010-CPROFITSAP            PIC X(10).                       
      *           NATURE                                                        
172           10  FCG010-NATURE                PIC X(03).                       
      *           TYPE DE TIERS                                                 
175           10  FCG010-TIERSSAP              PIC X(10).                       
      *           DATE D'ECHEANCE                                               
185           10  FCG010-DECH                  PIC X(08).                       
      *           CODE CGS                                                      
193           10  FCG010-CCGS                  PIC X(01).                       
      *           DATE D'INTERFACE                                              
194           10  FCG010-DFTI00                PIC X(08).                       
      *           SOC GESTION                                                   
202           10  FCG010-NSOCGEST              PIC X(03).                       
      *           ZONES TIERS CPD : NOM, PRENOM, ADRESSE,                       
      *                             CODE POSTAL, VILLE                          
              10  FCG010-TIERSCPD.                                              
205              15  FCG010-NOMCPD             PIC X(25).                       
230              15  FCG010-PNOMCPD            PIC X(25).                       
255              15  FCG010-ADRCPD             PIC X(40).                       
295              15  FCG010-CPOSCPD            PIC X(05).                       
300              15  FCG010-VILLECPD           PIC X(25).                       
      *           ZONE NEUTRE                                                   
325           10  FILLER                       PIC X(35).                       
                                                                                
