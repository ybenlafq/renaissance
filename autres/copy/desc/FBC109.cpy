      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC109  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    FBC109       : FICHIER INDIQUANT LES VENTES CHARGEES DANS  *         
      *                   LA BASE DE DONNEES CLIENT                   *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:   50 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC109-ENREG.                                                        
           05  FBC109-NSOCIETE                PIC  X(03).                       
           05  FBC109-NLIEU                   PIC  X(03).                       
           05  FBC109-NVENTE                  PIC  X(08).                       
           05  FBC109-WTYPEADR                PIC  X(01).                       
           05  FBC109-DVERSION                PIC  X(08).                       
           05  FBC109-IDCLIENT                PIC  X(08).                       
           05  FBC109-NCLIENTADR              PIC  X(08).                       
           05  FBC109-HISTORIQUE              PIC  X(01).                       
           05  FBC109-MODIFICATION            PIC  X(01).                       
           05  FBC109-FILLER                  PIC  X(09).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
