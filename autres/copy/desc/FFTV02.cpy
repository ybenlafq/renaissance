      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      ***************************************************************           
      *   FICHIER DES MOUVEMENTS COMPTABLES ISSUS DES INTERFACES    *           
      *              - NOUVELLE DSECT STANDARD GCT -                *           
      *                                                             *           
      *      - DSECT COBOL                                          *           
      *      - NOM = FFTV02                                         *           
      *      - RECSIZE = 200                                        *           
      *                                                             *           
      ***************************************************************           
       01  FFTV02-ENR.                                                          
      ***************************************************************           
      * TRI DE CE FICHIER : SUR LES 17 PREMIERES POSITIONS          *           
      *                   + LA DEVISE EN POSITION 186 SUR  3        *           
      *                   + LE NUMERO DE PIECE EN  18 SUR 10        *           
      ***************************************************************           
           02  FFTV02-CLE.                                                      
   1           03  FFTV02-NSOC                     PIC X(05).                   
   6           03  FFTV02-NETAB                    PIC X(03).                   
   9           03  FFTV02-NJRN                     PIC X(03).                   
  12           03  FILLER                          PIC X(01).                   
  13           03  FFTV02-CINTERFACE               PIC X(05).                   
      *------> DEVISE                                                           
  18           03  FFTV02-NPIECE                   PIC X(10).                   
      ***************************************************************           
  28       02  FFTV02-NATURE                       PIC X(03).                   
  31       02  FFTV02-REFDOC                       PIC X(12).                   
  43       02  FFTV02-DFTI00                       PIC X(08).                   
  51       02  FFTV02-NCOMPTE                      PIC X(06).                   
  57       02  FFTV02-NSSCOMPTE                    PIC X(06).                   
  63       02  FFTV02-NSECTION                     PIC X(06).                   
  69       02  FFTV02-NRUBR                        PIC X(06).                   
  75       02  FFTV02-NSTEAPP                      PIC X(05).                   
  80       02  FFTV02-NTIERSCV                     PIC X(08).                   
  88       02  FFTV02-NLETTRAGE                    PIC X(10).                   
  98       02  FFTV02-LMVT                         PIC X(25).                   
 123       02  FFTV02-PMONTMVT                     PIC 9(13)V99  COMP-3.        
 131       02  FFTV02-CMONTMVT                     PIC X(01).                   
 132       02  FFTV02-DMVT                         PIC X(08).                   
 140       02  FFTV02-NOECS                        PIC X(05).                   
      * CATEGORIE DE CHARGE ET NATURE DE CHARGE                                 
 145       02  FFTV02-CATCH                        PIC X(01).                   
 146       02  FFTV02-NATCH                        PIC X(06).                   
      * DATE D'ECHEANCE ET BON A PAYER                                          
 152       02  FFTV02-DECH                         PIC X(08).                   
      * BON A PAYER                                                             
 160       02  FFTV02-WBAP                         PIC X(01).                   
      * CONTROLE CENTRALISE DES TRAITEMENTS :                                   
      * -> NOM DE PROGRAMME ET DATE DE TRAITEMENT                               
 161       02  FFTV02-CNOMPROG                     PIC X(08).                   
 169       02  FFTV02-DTRAITEMENT                  PIC X(08).                   
      * ICS                                                                     
 177       02  FFTV02-WCUMUL                       PIC X(01).                   
 178       02  FFTV02-DDOC                         PIC X(08).                   
      * DEVISE --> FAIT PARTIE DE LA CLEF DE TRI                                
 186       02  FFTV02-CDEVISE                      PIC X(03).                   
 189       02  FILLER                              PIC X(12).                   
           02  FFTV02-ICSSAP.                                                   
 201           05  FFTV02-CPTSAP                   PIC X(08).                   
 209           05  FFTV02-TIERSSAP                 PIC X(10).                   
 219           05  FFTV02-CCGS                     PIC X(01).                   
 220       02  FFTV02-TYPTIERS                     PIC X(01).                   
 221       02  FFTV02-CPROFITSAP                   PIC X(10).                   
 231       02  FFTV02-TAUXTVA                      PIC 9(03)V99 COMP-3.         
           02  FFTV02-TYPMT.                                                    
 234           05  FFTV02-NATTVA                   PIC X(01).                   
 235           05  FFTV02-NATMT                    PIC X(01).                   
 236       02  FILLER                              PIC X(15).                   
                                                                                
