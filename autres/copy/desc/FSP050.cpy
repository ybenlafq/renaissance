      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *  DSECT DU FICHIER FSP050: VALO DE L'ECART PRAK/PRMP PAR SOCIETE         
      *  EMETTRICE, POUR CAPROFEM                                               
      *  --> SERT POUR GENERER L'ETAT ISP050                                    
      *****************************************************************         
      *                                                                         
       01 DSECT-FSP050.                                                         
          02 FSP050-ENTETE.                                                     
001          05 FSP050-NSOCEMET         PIC X(03).                              
004          05 FSP050-NSOCRECEP        PIC X(03).                              
             05 FSP050-TEBRB.                                                   
007             07 FSP050-TLMELA        PIC X(03).                              
010             07 FSP050-BLBR          PIC X(02).                              
012          05 FSP050-TXTVA            PIC S9(03)V9(2) COMP-3.                 
015          05 FILLER                  PIC X(05).                              
      *                                                                         
          02 FSP050-CHAMPS.                                                     
020          05 FSP050-VALOPRAK         PIC S9(13)V9(2) COMP-3.                 
028          05 FSP050-VALOPRMP         PIC S9(13)V9(2) COMP-3.                 
036          05 FSP050-LSOCEMET         PIC X(20).                              
056          05 FILLER                  PIC X(05).                              
      *                                                                         
                                                                                
LG=060                                                                          
