      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FSM025-ENREG.                                                        
           05  FSM025-CVERSION       PIC  X(10).                                
           05  FSM025-CSOCIETE       PIC  X(03).                                
           05  FSM025-CLIEU          PIC  X(03).                                
           05  FSM025-LMARK1         PIC  X(20).                                
           05  FSM025-LMARK2         PIC  X(20).                                
           05  FSM025-LMARK3         PIC  X(20).                                
           05  FSM025-CFAMILLE       PIC  X(05).                                
           05  FSM025-NSEQFAM        PIC  X(05).                                
           05  FSM025-CMARQUE        PIC  X(05).                                
           05  FSM025-MONTANT        PIC  S9(7)V99 COMP-3.                      
           05  FSM025-CCODIC1        PIC  X(07).                                
           05  FSM025-CCODIC2        PIC  X(07).                                
           05  FSM025-TYPARTICLE     PIC  X(01).                                
           05  FSM025-CREFERENCE     PIC  X(20).                                
           05  FSM025-SENSVENTE      PIC  X(01).                                
           05  FSM025-STATUTCOMP     PIC  X(03).                                
           05  FSM025-CGESTIONVENT   PIC  X(03).                                
           05  FSM025-COMMENTART     PIC  X(20).                                
           05  FSM025-DESCRIPTION1   PIC  X(05).                                
           05  FSM025-DESCRIPTION2   PIC  X(05).                                
           05  FSM025-DESCRIPTION3   PIC  X(05).                                
           05  FSM025-ZONEPRIX       PIC  X(02).                                
           05  FSM025-PRIXVENTE      PIC  S9(7)V99 COMP-3.                      
           05  FSM025-COMMISSION     PIC  X(01).                                
           05  FSM025-DCOMMANDE      PIC  X(08).                                
           05  FSM025-QDISPO         PIC  S9(5) COMP-3.                         
           05  FSM025-STOCKMAG       PIC  S9(5) COMP-3.                         
           05  FSM025-STOCKDEP       PIC  S9(5) COMP-3.                         
           05  FSM025-PRIXCOMM       PIC  S9(5) COMP-3.                         
           05  FSM025-CAPPRO         PIC  X(05).                                
           05  FSM025-CEXPO          PIC  X(05).                                
           05  FSM025-WDACEM         PIC  X(01).                                
           05  FSM025-WSENSNF        PIC  X(01).                                
           05  FSM025-NEAN           PIC  X(13).                                
           05  FSM025-WBUNDLE        PIC  X(01).                                
                                                                                
