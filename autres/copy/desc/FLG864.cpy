      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FLG864  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      **                                                              *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : EXTRACTION COMMUNE                          *         
      *                                                               *         
      *    FLG864       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:   15 OCTET                                  *         
      *                                                               *         
      *    DIFFERENTES VALEURS DU CODE                                *         
      *    001 : FILIALE                                              *         
      *    002 : ZONE DE PRIX                                         *         
      *    003 : DATE D EXTRACTION                                    *         
      *    004 : STOCK MAGASIN GS36 STOCK DISPO                       *         
      *    005 : STOCK MAGASIN MU06 STOCK DISPO                       *         
      *    006 : MUTATION ATTENDU                                     *         
      *    007 : ASSORTIMENT MAGASIN  ( 1 -> O ) ( 0 -> N )           *         
      *    008 : QUANTITE EXPO MAGASIN                                *         
      *    009 : QUANTITE LIBRE SERVICE MAGASIN                       *         
      *    010 : STOCK OBJECTIF                                       *         
      *    011 : STOCK AVANCE                                         *         
      *    012 : VENTE S0 PIECES TOTALES                 RTHV04       *         
      *    013 : VENTE S0 PIECES EMPORTEES               RTHV04       *         
      *    014 : VENTE S0 PIECES EXCEPTIONS TOTALES      RTHV04       *         
      *    015 : VENTE S0 PIECES EXCEPTIONS EMPORTEES    RTHV04       *         
      *    016 : VENTE S - 1 PIECES TOTALES              RTHV04       *         
      *    017 : VENTE S - 1 PIECES EMPORTEES            RTHV04       *         
      *    018 : VENTE S - 1 PIECES EXCEPTIONS TOTALES   RTHV04       *         
      *    019 : VENTE S - 1 PIECES EXCEPTIONS EMPORTEES RTHV04       *         
      *    020 : VENTE S - 2 PIECES TOTALES              RTHV04       *         
      *    021 : VENTE S - 2 PIECES EMPORTEES            RTHV04       *         
      *    022 : VENTE S - 2 PIECES EXCEPTIONS TOTALES   RTHV04       *         
      *    023 : VENTE S - 2 PIECES EXCEPTIONS EMPORTEES RTHV04       *         
      *    024 : VENTE S - 3 PIECES TOTALES              RTHV04       *         
      *    025 : VENTE S - 3 PIECES EMPORTEES            RTHV04       *         
      *    026 : VENTE S - 3 PIECES EXCEPTIONS TOTALES   RTHV04       *         
      *    027 : VENTE S - 3 PIECES EXCEPTIONS EMPORTEES RTHV04       *         
      *    028 : VENTE S - 4 PIECES TOTALES              RTHV04       *         
      *    029 : VENTE S - 4 PIECES EMPORTEES            RTHV04       *         
      *    030 : VENTE S - 4 PIECES EXCEPTIONS TOTALES   RTHV04       *         
      *    031 : VENTE S - 4 PIECES EXCEPTIONS EMPORTEES RTHV04       *         
      *    032 : VENTE S0 PIECES TOTALES                 RTHV12       *         
      *    033 : VENTE S0 PIECES EMPORTEES               RTHV12       *         
      *    034 : VENTE S0 PIECES EXCEPTIONS TOTALES      RTHV12       *         
      *    035 : VENTE S0 PIECES EXCEPTIONS EMPORTEES    RTHV12       *         
      *    036 : VENTE S - 1 PIECES TOTALES              RTHV12       *         
      *    037 : VENTE S - 1 PIECES EMPORTEES            RTHV12       *         
      *    038 : VENTE S - 1 PIECES EXCEPTIONS TOTALES   RTHV12       *         
      *    039 : VENTE S - 1 PIECES EXCEPTIONS EMPORTEES RTHV12       *         
      *    040 : VENTE S - 2 PIECES TOTALES              RTHV12       *         
      *    041 : VENTE S - 2 PIECES EMPORTEES            RTHV12       *         
      *    042 : VENTE S - 2 PIECES EXCEPTIONS TOTALES   RTHV12       *         
      *    043 : VENTE S - 2 PIECES EXCEPTIONS EMPORTEES RTHV12       *         
      *    044 : VENTE S - 3 PIECES TOTALES              RTHV12       *         
      *    045 : VENTE S - 3 PIECES EMPORTEES            RTHV12       *         
      *    046 : VENTE S - 3 PIECES EXCEPTIONS TOTALES   RTHV12       *         
      *    047 : VENTE S - 3 PIECES EXCEPTIONS EMPORTEES RTHV12       *         
      *    048 : VENTE S - 4 PIECES TOTALES              RTHV12       *         
      *    049 : VENTE S - 4 PIECES EMPORTEES            RTHV12       *         
      *    050 : VENTE S - 4 PIECES EXCEPTIONS TOTALES   RTHV12       *         
      *    051 : VENTE S - 4 PIECES EXCEPTIONS EMPORTEES RTHV12       *         
      *    052 : STOCK MAGASIN GS36 STOCK AR                          *         
      *    053 : STOCK MAGASIN GS36 STOCK HS                          *         
      *    054 : STOCK MAGASIN GS36 STOCK P                           *         
      *    055 : STOCK MAGASIN GS36 STOCK T                           *         
      *    056 : STOCK MAGASIN MU06 STOCK T                           *         
      *    057 : STOCK MAGASIN GS36 STOCK RSV                         *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  DSECT-FLG864.                                                        
           05  FLG864-NSOCIETE                PIC S9(03) COMP-3.                
           05  FLG864-NLIEU                   PIC S9(03) COMP-3.                
           05  FLG864-NCODIC                  PIC S9(07) COMP-3.                
           05  FLG864-CODE                    PIC S9(03) COMP-3.                
           05  FLG864-QTE                     PIC S9(09) COMP-3.                
                                                                                
