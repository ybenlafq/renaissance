      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC104  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST A SOUMETTRE AU    *         
      *                   TRAITEMENT                                  *         
      *                                                               *         
      *    FBC104       : FICHIER DES REGLEMENTS                      *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:   86 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC104-ENREG.                                                        
           03  FBC104-CLE.                                                      
               05  FBC104-NSOCIETE            PIC  X(03).                       
               05  FBC104-NLIEU               PIC  X(03).                       
               05  FBC104-NVENTE              PIC  X(08).                       
           03  FBC104-DSAISIE                 PIC  X(08).                       
           03  FBC104-NSEQ                    PIC  X(02).                       
           03  FBC104-CMODPAIMT               PIC  X(05).                       
           03  FBC104-PREGLTVTE               PIC  S9(07)V9(02).                
           03  FBC104-NREGLTVTE               PIC  X(07).                       
           03  FBC104-DREGLTVTE               PIC  X(08).                       
           03  FBC104-WREGLTVTE               PIC  X(01).                       
           03  FBC104-DCOMPTA                 PIC  X(08).                       
           03  FBC104-CDEVISE                 PIC  X(03).                       
           03  FBC104-DVERSION                PIC  X(08).                       
           03  FBC104-DSYST                   PIC  X(13).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
