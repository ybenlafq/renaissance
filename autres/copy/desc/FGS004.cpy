      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DES MOUVEMENTS DE VIDAGE DES PI (PGM=BGS004) *00002209
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FGS004-ENR.                                                       
           10  FGS004-CFAM            PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-CMARQ           PIC X(05)      VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NCODIC          PIC X(07)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-LREFFOURN       PIC X(20)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-TYPE            PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NSOCORIG        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NLIEUORIG       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NSSLIEUORIG     PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-CLIEUTRTORIG    PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NSOCDEST        PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NLIEUDEST       PIC X(03)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NSSLIEUDEST     PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-CLIEUTRTDEST    PIC X(05)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-NORIGINE        PIC X(09)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-DMVT            PIC X(08)      VALUE SPACE.               
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-QMVTE           PIC Z(04)9     VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-QMVTF           PIC Z(04)9     VALUE ZERO.                
           10  FILLER                 PIC X          VALUE ';'.                 
           10  FGS004-VAL             PIC -(9)9,9(2) VALUE ZERO.                
           10  FGS004-VAL-0 REDEFINES FGS004-VAL                                
                                      PIC -(10),-(2).                           
           10  FILLER                 PIC X          VALUE ';'.                 
                                                                                
