      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *   COPY DU FICHIER POUR BQC170 : EXTRACTION CARTES T 5                   
      *   EXTRACTION DE VENTES                                                  
      *                                                                         
      * REMARQUES :                                                             
      *                                                                         
      * ---> LONGUEUR ARTICLE = 100                                             
      *                                                                         
      * ---> CTYPE                                                              
      *       A = INFOS NB DE VENTES PAR PLATEFORME                             
      *       B = LIGNES VENTES DE LA PLATEFORME                                
      * ---> NVENTE                                                             
      *       LIGNE A --->  = 0                                                 
      *       LIGNE B ---> <> 0                                                 
      * ---> NBVENTE                                                            
      *       LIGNE A ---> <> 0                                                 
      *       LIGNE B --->  = 0                                                 
      ******************************************************************        
      *                                                                         
       01  DSECT-FQC012.                                                        
           10  FQC012-NSOCLIVR       PIC X(03).                                 
           10  FQC012-NDEPOT         PIC X(03).                                 
           10  FQC012-CTYPE          PIC X(01).                                 
           10  FQC012-NBVENTE        PIC S9(7) COMP-3.                          
           10  FQC012-NVENTE         PIC X(07).                                 
           10  FQC012-DDELIV         PIC X(08).                                 
           10  FQC012-DVENTE         PIC X(08).                                 
           10  FQC012-NCODIC         PIC X(07).                                 
           10  FQC012-CVENDEUR       PIC X(06).                                 
           10  FQC012-CINSEE         PIC X(05).                                 
           10  FQC012-CPROTOUR       PIC X(05).                                 
           10  FQC012-CTOURNEE       PIC X(08).                                 
           10  FQC012-CEQUIPE        PIC X(05).                                 
           10  FQC012-CMODDEL        PIC X(03).                                 
           10  FQC012-WCREDIT        PIC X(01).                                 
           10  FQC012-CPOSTAL        PIC X(05).                                 
           10  FQC012-NSOCIETE       PIC X(03).                                 
           10  FQC012-NLIEU          PIC X(03).                                 
           10  FQC012-CPLAGE         PIC X(02).                                 
           10  FQC012-TOP-MAIL       PIC X(01).                                 
           10  FQC012-NBMAIL         PIC S9(7) COMP-3.                          
           10  FQC012-FILLER         PIC X(08).                                 
           10  FQC012-EMAIL          PIC X(50).                                 
           02  FQC012-WEMPORTE       PIC X(1).                                  
           02  FQC012-PVTOTAL        PIC S9(7)V9(2) USAGE COMP-3.               
           02  FQC012-REMISE         PIC S9(7)V9(2) USAGE COMP-3.               
           02  FQC012-DGARANTIE      PIC X(8).                                  
           02  FQC012-CPSE           PIC X(5).                                  
           02  FQC012-LPSE           PIC X(20).                                 
           02  FQC012-NSEQNQ         PIC S9(5)V USAGE COMP-3.                   
           02  FQC012-QVENDUE        PIC S9(5) COMP-3.                          
           02  FQC012-TYPVTE         PIC X.                                     
           02  FILLER                PIC X(49).                                 
                                                                                
