      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       01  FGM069-DSECT.                                                        
           02 GA69-NCODIC                PIC X(07).                             
           02 GA69-CFAM                  PIC X(05).                             
           02 GA69-CMARQ                 PIC X(05).                             
           02 GA69-CHEFPROD              PIC X(05).                             
           02 GA69-LREFFOURN             PIC X(20).                             
           02 GA69-LREFDARTY             PIC X(20).                             
           02 GA69-CTAUXTVA              PIC X.                                 
           02 GA69-CEXPO                 PIC X(05).                             
           02 GA69-LSTATCOMP             PIC X(03).                             
           02 GA69-CGESTVTE              PIC X(03).                             
           02 GA69-CAPPRO                PIC X(05).                             
           02 GA69-WSENSVTE              PIC X.                                 
           02 GA69-WSENSAPPRO            PIC X.                                 
           02 GA69-DEFSTATUT             PIC X(08).                             
           02 GA69-WTLMELA               PIC X.                                 
           02 GA69-WDACEM                PIC X.                                 
           02 GA69-NEAN                  PIC X(13).                             
           02 GA69-PRAR                  PIC S9(7)V99 COMP-3.                   
           02 GA69-QDELAIAPPRO           PIC X(03).                             
           02 GA69-LFAM                  PIC X(20).                             
           02 GA69-WSEQFAM               PIC S9(5) COMP-3.                      
           02 GA69-WMULTIFAM             PIC X.                                 
           02 GA69-WBUNDLE               PIC X.                                 
           02 W-GA69-NCODIC              PIC X(07).                             
           02 GA69-CTYPLIEN              PIC X(05).                             
           02 GA69-QTYPLIEN              PIC S9(3) COMP-3.                      
           02 W-GA69-CFAM                PIC X(05).                             
           02 W-GA69-CMARQ               PIC X(05).                             
           02 W-GA69-LREFFOURN           PIC X(20).                             
           02 W-GA69-CEXPO               PIC X(05).                             
           02 W-GA69-LSTATCOMP           PIC X(03).                             
           02 W-GA69-CAPPRO              PIC X(05).                             
           02 W-GA69-WSENSVTE            PIC X.                                 
           02 W-GA69-WSENSAPPRO          PIC X.                                 
           02 GA69-DONNEES-MARKET.                                              
              05 GA69-WPOSTE-IGM020      OCCURS 3.                              
                 10 GA69-CMARKETING-IGM  PIC X(05).                             
                 10 GA69-CVMARKETING-IGM PIC X(05).                             
                 10 GA69-LVMARKETING-IGM PIC X(20).                             
                 10 GA69-CDESCRIPTIF-IGM PIC X(05).                             
                 10 GA69-CVDESCRIPT-IGM  PIC X(05).                             
              05 GA69-WPOSTE-ISM030      OCCURS 3.                              
                 10 GA69-CMARKETING-ISM  PIC X(05).                             
                 10 GA69-CVMARKETING-ISM PIC X(05).                             
                 10 GA69-LVMARKETING-ISM PIC X(20).                             
                 10 GA69-CDESCRIPTIF-ISM PIC X(05).                             
                 10 GA69-CVDESCRIPT-ISM  PIC X(05).                             
                                                                                
