      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGP889  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : REAPPROVISIONNEMENT FOURNISSEUR             *         
      *                                                               *         
      *    FGP889       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    LONGUEUR ENR.:  512 C                                      *         
      *                                                               *         
      *    ATTENTION CETTE COPY COMMENCE PAR UN NIVEAU 03             *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       03  :FGPXXX:-ENREG.                                                      
      *                                                         1    6          
           05  :FGPXXX:-CETAT                 PIC  X(06).                       
      *                                                         7    7          
           05  :FGPXXX:-CBOITE                PIC  X(07).                       
      *                                                        14    3          
           05  :FGPXXX:-NSOCIETE              PIC  X(03).                       
      *                                                        17   20          
           05  :FGPXXX:-LLIEU                 PIC  X(20).                       
      *                                                        37    5          
        04 :FGPXXX:-DONNEE-ENTETE.                                              
           05  :FGPXXX:-CHEFPROD              PIC  X(05).                       
      *                                                        42   20          
           05  :FGPXXX:-LCHEFPROD             PIC  X(20).                       
      *                                                        62   15          
           05  :FGPXXX:-LIBELLE               PIC  X(15).                       
      *                                                        77    5          
           05  :FGPXXX:-WSEQFAM               PIC  X(05).                       
      *                                                        82    5          
           05  :FGPXXX:-CFAM                  PIC  X(05).                       
      *                                                        87   20          
           05  :FGPXXX:-LFAM                  PIC  X(20).                       
      *                                                       107   20          
           05  :FGPXXX:-LAGREGATED            PIC  X(20).                       
      *                                                       127    5          
        04 :FGPXXX:-DONNEE-CODIC.                                               
           05  :FGPXXX:-CMARQ                 PIC  X(05).                       
      *                                                       132   20          
           05  :FGPXXX:-LMARQ                 PIC  X(20).                       
      *                                                       152   20          
           05  :FGPXXX:-LREFFOURN             PIC  X(20).                       
      *                                                       172    1          
           05  :FGPXXX:-WOA                   PIC  X(01).                       
      *                                                       173    5          
        04 :FGPXXX:-DONNEE-FILIALE.                                             
           05  :FGPXXX:-SOCCHEF               PIC X(05).                        
      *                                                       178    3          
           05  :FGPXXX:-STKNR                 PIC S9(04) COMP-3.                
      *                                                       181    5          
           05  :FGPXXX:-PSTDTTC               PIC S9(07)V9(02) COMP-3.          
      *                                                       186    4          
           05  :FGPXXX:-QS-0                  PIC S9(06) COMP-3.                
      *                                                       190    4          
           05  :FGPXXX:-QS-1                  PIC S9(06) COMP-3.                
      *                                                       194    4          
           05  :FGPXXX:-QS-2                  PIC S9(06) COMP-3.                
      *                                                       198    4          
           05  :FGPXXX:-QS-3                  PIC S9(06) COMP-3.                
      *                                                       202    4          
           05  :FGPXXX:-QS-4                  PIC S9(06) COMP-3.                
      *                                                       206    2          
           05  :FGPXXX:-NBMAG                 PIC S9(03) COMP-3.                
      *                                                       208    3          
           05  :FGPXXX:-QSTOCKMAG             PIC S9(05) COMP-3.                
      *                                                       211    3          
           05  :FGPXXX:-QBESOINMAG            PIC S9(05) COMP-3.                
      *                                                       214    1          
           05  :FGPXXX:-WGP                   PIC  X(01).                       
      *                                                       215    3          
           05  :FGPXXX:-LSTATCOMP             PIC  X(03).                       
      *                                                       218    4          
        04 :FGPXXX:-DONNEE-DEPOT.                                               
           05  :FGPXXX:-QSTOCKDIS             PIC S9(07) COMP-3.                
      *                                                       222    4          
           05  :FGPXXX:-QSTOCKRES             PIC S9(06) COMP-3.                
      *                                                       226    3          
           05  :FGPXXX:-NSOCDEPOT             PIC  X(03).                       
      *                                                       229    3          
           05  :FGPXXX:-NDEPOT                PIC  X(03).                       
      *                                                       232    4          
           05  :FGPXXX:-DCDE4                 PIC  X(04).                       
      *                                                       236    3          
           05  :FGPXXX:-QCDE                  PIC S9(05) COMP-3.                
      *                                                       239    3          
           05  :FGPXXX:-QCDERES               PIC S9(05) COMP-3.                
      *                                                       242    3          
           05  :FGPXXX:-QCOLIRECEPT           PIC S9(05) COMP-3.                
      *                                                       245    3          
           05  :FGPXXX:-QNBPRACK              PIC S9(05) COMP-3.                
      *                                                       248    1          
           05  :FGPXXX:-WSOL                  PIC  X(01).                       
      *                                                       249    7          
        04 :FGPXXX:-DONNEE-TRI.                                                 
           05  :FGPXXX:-NCODIC                PIC  X(07).                       
      *                                                       256    7          
           05  :FGPXXX:-NCODIC2               PIC  X(07).                       
      *                                                       263    1          
           05  :FGPXXX:-WDEB                  PIC  X(01).                       
      *                                                       264    1          
           05  :FGPXXX:-WGROUPE               PIC  X(01).                       
      *                                                       265    5          
           05  :FGPXXX:-WCMARQ                PIC  X(05).                       
      *                                                       270    6          
           05  :FGPXXX:-WENTNAT               PIC  X(06).                       
      *                                                       276    4          
           05  :FGPXXX:-WVOLV4S               PIC S9(07) COMP-3.                
      *                                                       280    1          
           05  :FGPXXX:-WTYPENT               PIC  X(01).                       
      *                                                       281    1          
           05  :FGPXXX:-WSENSAPPRO            PIC  X(01).                       
      *                                                       282    8          
           05  :FGPXXX:-DCDE8                 PIC  X(08).                       
      *                                                       290    5          
           05  :FGPXXX:-CAPPRO                PIC  X(05).                       
      *                                                       295    1          
           05  :FGPXXX:-WDACEM                PIC  X(01).                       
      *                                                       296    5          
           05  :FGPXXX:-CAPPRO2               PIC  X(05).                       
      *                                                       301    2          
           05  :FGPXXX:-QTYPLIEN              PIC  S9(3) COMP-3.                
      *                                                       303    8          
           05  :FGPXXX:-DEFFET                PIC  X(08).                       
      *                                                       311    1          
           05  :FGPXXX:-FICHE                 PIC  X(01).                       
      *                                                       312   21          
           05  :FGPXXX:-TAB-FILIALE.                                            
             08  :FGPXXX:-VTE-FILIALE    OCCURS 7.                              
                10  :FGPXXX:-NFILIALE         PIC  X(03).                       
      *                                                       333    4          
           05  :FGPXXX:-QBL                   PIC S9(6) COMP-3.                 
      *    05  FILLER                         PIC  X(180).                      
      *                                                       337    3          
DXXXX      05  :FGPXXX:-DEP-QSTOCKC           PIC S9(5) COMP-3.                 
XXXX  *    05  FILLER                         PIC  X(176).                      
      *                                                       336  173          
DXXXX      05  FILLER                         PIC  X(173).                      
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
