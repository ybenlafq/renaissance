      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : TICKET HORODATAGE       * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK010                                        * 00100600
      *        - LONGUEUR ENREG = 41 --> 29                           * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      * AJOUT CARACTERE FIN DE CHAMP : <TAB> VALEUR HEXA EBCDIC : 05  * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK010-ENR.                                                          
           05  FGK010-NOM-SOC       PIC X(05)     VALUE 'DARTY'.                
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK010-DATE-EXTR     PIC X(10).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK010-MOIS-EXTR     PIC X(06).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK010-DEV-REF       PIC X(05).                                  
      *                                                                         
                                                                                
