      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * DSECT DE FNV100A : ANNUAIRE UTILISATEUR HRACCES => LDAP         00000020
      ****************************************************************  00000030
      *--> LONGUEUR 200                                                 00000050
                                                                        00000060
       01  FNV100A-ENREG.                                               00000070
001        05  FNV100A-NMATRICULE   PIC X(12).                                  
013        05  FNV100A-NOM          PIC X(40).                                  
053        05  FNV100A-PNOM         PIC X(25).                                  
078        05  FNV100A-PNOM2        PIC X(25).                                  
103        05  FNV100A-NOM2         PIC X(40).                                  
143        05  FNV100A-DATNAIS      PIC X(08).                                  
           05  FNV100A-NSOCLIEU.                                                
151         07 FNV100A-NSOC         PIC X(03).                                  
154         07 FNV100A-NLIEU        PIC X(03).                                  
157        05  FNV100A-FILLER1      PIC X(02).                                  
159        05  FNV100A-CEMPLOI      PIC X(10).                                  
169        05  FNV100A-DATSOR       PIC X(10).                                  
      *                                  ----                                   
      *                                  176                                    
                                                                                
