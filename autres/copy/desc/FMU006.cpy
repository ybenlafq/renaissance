      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************          
      *  DEFINITION DU FICHIER DES STOCK TABLE RTMU06                           
      ****************************************************************          
       01  DSECT-FMU006.                                                        
           02 FMU006-TRI.                                                       
              03 FMU006-NSOCIETE            PIC X(03).                          
              03 FMU006-NLIEU               PIC X(03).                          
              03 FMU006-NSSLIEU             PIC X(03).                          
              03 FMU006-NCODIC              PIC X(07).                          
           02 FMU006-DONNEES-ARTICLE.                                           
              03 FMU006-QSTOCK              PIC S9(5) COMP-3.                   
              03 FMU006-QSTOCKF             PIC S9(5) COMP-3.                   
              03 FMU006-DSYST               PIC S9(13) COMP-3.                  
                                                                                
