      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ************************************************                          
      *               DESCRIPTION                    *                          
      *                 FMU980                       *                          
      *       FICHIER DES REJETS IMPAYES             *                          
      *----------------------------------------------*                          
      *          FICHIER SAM, LONGUEUR 80            *                          
      ************************************************                          
            03    ODMUTI         PIC X(6).                                      
            03    ODTLEL         PIC X(1).                                      
            03    ODRANG         PIC 9(2).                                      
            03    ODCODI         PIC X(7).                                      
            03    ODATRS         PIC X(6).                                      
            03    ODPRMP         PIC 9(5)V99.                                   
            03    ODQTER         PIC 9(5).                                      
            03    ODNSOC         PIC X(3).                                      
            03    ODLIEU         PIC X(3).                                      
            03    ODCESSION      PIC 9(5)V99.                                   
            03    ODFAMMGI       PIC X(5).                                      
            03    ODEXPO         PIC X.                                         
            03    ODCONTRE       PIC X.                                         
            03    ODFAMILLE      PIC X(5).                                      
            03    ODNMUTATION    PIC X(7).                                      
            03    ODSRP          PIC 9(5)V99.                                   
            03    ODLIBR         PIC X(07).                                     
                                                                                
