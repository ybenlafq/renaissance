      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                        00010000
                                                                                
       01  FGG300-ENREG.                                                00020000
           03 FGG300-CLE.                                               00030000
               05 FGG300-WSEQED               PIC S9(5) COMP-3.         00040000
               05 FGG300-CMARQ                PIC X(5).                 00050000
               05 FGG300-NCODIC               PIC X(7).                 00060000
           03 FGG300-CFAMILLE                 PIC X(5).                 00070000
           03 FGG300-LREFFOURN                PIC X(20).                00080000
           03 FGG300-DATEMAJ                  PIC X(8).                 00090000
           03 FGG300-PRA                      PIC S9(7)V9(2) COMP-3.    00100000
           03 FGG300-PRMP                     PIC S9(7)V9(2) COMP-3.    00110000
           03 FGG300-ECART-UNITAIRE           PIC S9(7)V9(2) COMP-3.    00120000
           03 FGG300-POURCENTAGE              PIC S9(3)V9(2) COMP-3.    00130000
           03 FGG300-STOCK                    PIC S9(7) COMP-3.         00140000
           03 FGG300-VALEUR-ECART             PIC S9(7)V9(2) COMP-3.    00150000
           EJECT                                                        00160000
                                                                                
                                                                        00170000
