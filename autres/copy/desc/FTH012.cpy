      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER FTH012 QUI PERMET D'ALIMENTER                *        
      *  LA TABLE RTTH12 (TABLE DES CODICS / OPCO / PCF)               *        
      *----------------------------------------------------------------*        
       01  DSECT-:FTH012:.                                                      
         05 CHAMPS-:FTH012:.                                                    
           10 :FTH012:-CLE.                                             122  005
      *                                               POS 1            *        
              15 :FTH012:-NCODIC        PIC X(07).                      122  005
      *                                               POS 8            *        
              15 :FTH012:-COPCO         PIC X(03).                      122  005
      *        *                                                                
           10 :FTH012:-DONNEES.                                         094  006
      *                                               POS 11           *        
              15 :FTH012:-PCF           PIC S9(7)V9(6) USAGE COMP-3.    197  007
      *                                               POS 18                    
              15 :FTH012:-FORCE         PIC X(01).                      197  007
      *                                               POS 19                    
              15 FILLER                 PIC X(31) VALUE SPACES.         197  007
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FTH012J-LONG          PIC S9(4)   COMP  VALUE +50.             
      *                                                                         
      *--                                                                       
       01  DSECT-:FTH012:-LONG          PIC S9(4) COMP-5  VALUE +50.            
                                                                                
      *}                                                                        
