      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : PRA CODICS FILALES      * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK045                                        * 00100600
      *        - LONGUEUR ENREG =  13                                 * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK045-ENR.                                                          
               05  FGK045-NCODIC        PIC  X(07).                             
               05  FGK045-PRA           PIC S9(09)V99 COMP-3.                   
      *                                                                         
                                                                                
