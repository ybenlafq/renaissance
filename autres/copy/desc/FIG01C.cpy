      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *==> DARTY ****************************** AIDA - COBOL 2 - VSAM *         
      *          ==> FICHIER VSAM DES IMPRIMANTES                     *         
      *              LONGUEUR : 100 OCTETS                            *         
      *              DDNAME CICS : FIGPRT                             *         
      *===============================================================*         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION  COBOL                                            *         
      *************************************************** COPY FIG01C *         
       01  FIG01.                                                               
           02 FIG01-FIG01K.                                                     
              03 FIG01-FIG01K-TCTPRT  PIC X(04).                                
              03 FIG01-FIG01K-APPLID  PIC X(08).                                
           02 FIG01-ITF.                                                        
              03 FIG01-MAR            PIC X(15).                                
              03 FIG01-MOD            PIC X(08).                                
              03 FIG01-PTC            PIC X(01).                                
              03 FIG01-IDE            PIC X(08).                                
           02 FIG01-LOC.                                                        
              03 FIG01-SOC            PIC X(03).                                
              03 FIG01-LIE            PIC X(03).                                
              03 FIG01-SPO            PIC X(03).                                
           02 FIG01-LIG               PIC 9(03).                                
           02 FIG01-FILLER            PIC X(44).                                
      *---------------------------------------------------------------*         
      * ATTENTION                                                     *         
      * CETTE DESCRIPTION COBOL-FIG01C A UNE EQUIVALENCE              *         
      * EN PL1-FIG01P                                                 *         
      * TOUTES LES MODIFICATIONS DOIVENT ETRE RECIPROQUES             *         
      *---------------------------------------------------------------*         
       EJECT                                                                    
                                                                                
