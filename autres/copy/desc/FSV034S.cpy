*@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:11 >

 01  FSV034-RECORD.
     05 FSV034-POSTE OCCURS 100.
         10 FSV034-NSOCIETE             PIC X(03).
         10 FSV034-NLIEU                PIC X(03).
         10 FSV034-NVENTE               PIC X(07).
         10 FSV034-NSEQNQ               PIC 9(5).
         10 FSV034-TBIEN                PIC X.
         10 FSV034-NCODIC               PIC X(07).
         10 FSV034-NPRESTATION          PIC X(05).
         10 FSV034-WTOPELIVRE           PIC X(01).
         10 FSV034-DVENTE               PIC X(08).

