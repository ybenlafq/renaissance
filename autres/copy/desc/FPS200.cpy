      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *************************************************************             
      *  DSECT DU FICHIER MVT ISSU MFPS200 POUR MAJ RTPS01/RTPS02               
      *************************************************************             
       01  DSECT-FPS200.                                                        
           05 FPS200-CMVT        PIC X(1).                              00040000
           05 FPS200-NSOC        PIC X(3).                              00040000
           05 FPS200-NLIEU       PIC X(3).                              00040000
           05 FPS200-NVENTE      PIC X(7).                              00040000
           05 FPS200-CGCPLT      PIC X(5).                                      
           05 FPS200-NGCPLT      PIC X(8).                                      
           05 FPS200-NCODIC      PIC X(7).                                      
           05 FPS200-MTGCPLT     PIC 9(5)V99.                                   
           05 FPS200-CINSEE      PIC X(5).                                      
           05 FPS200-DCOMPTA     PIC X(8).                                      
           05 FPS200-DTRAIT      PIC X(8).                                      
           05 FPS200-FILLER      PIC X(18).                                     
      * LG = 80                                                                 
                                                                                
