      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER FSP051                                       *        
      *                                                                *        
      *  ISSU DU BSP010                                                *        
      *                                                                *        
      *          CRITERES DE TRI  14,10,CH,A,  MARQ, FAM               *        
      *                           01,06,CH,A,  SOCEMET, SOCRECEP       *        
      *                           79,07,CH,A,  CODIC                   *        
      *                                                                *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-FSP051.                                                        
1          10 FSP051-NSOCEMET           PIC X(03).                      007  003
4          10 FSP051-NSOCRECEP          PIC X(03).                      010  003
7          10 FSP051-NUMFACT            PIC X(07).                      013  007
14         10 FSP051-CMARQ              PIC X(05).                      020  005
19         10 FSP051-CFAM               PIC X(05).                      025  005
24         10 FSP051-LSOCEMET           PIC X(25).                      032  025
49         10 FSP051-LSOCRECEP          PIC X(25).                      057  025
74         10 FSP051-MOISTRT            PIC X(05).                      082  005
79         10 FSP051-NCODIC             PIC X(07).                      087  007
86         10 FSP051-REFERENCE          PIC X(20).                      094  020
106        10 FSP051-TYPFACT            PIC X(01).                      114  001
107        10 FSP051-DATEFACT           PIC X(08).                      131  006
115        10 FSP051-QTE                PIC S9(07)      COMP-3.         115  004
119        10 FSP051-VALOPRMP           PIC S9(09)V9(2) COMP-3.         119  006
125        10 FSP051-VALOPRAK           PIC S9(09)V9(2) COMP-3.         125  006
131        10 FSP051-TXTVA              PIC S9(03)V9(2) COMP-3.         125  006
134        10 FILLER                    PIC X(07).                              
                                                                                
