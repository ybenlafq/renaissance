      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : CONSO STAT VENTES       * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK131                                        * 00100600
      *        - LONGUEUR CLE   =  25 ( <  80 )                       * 00101000
      *        - LONGUEUR ENREG =  112( < 129 )                       * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      * AJUSTEMENT LONGUEUR CHAMPS + AJOUT <TAB> ENTRE CHAQUE CHAMP   * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK131-ENR.                                                          
         03  FGK131-CLE.                                                        
           05  FGK131-CODE-FAM      PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-CODE-MARK     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-CODE-MARQ     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-NCODIC        PIC X(07).                                  
           05  FILLER               PIC X(01)     VALUE ' '.                    
         03  FGK131-DONNEES.                                                    
           05  FGK131-PVTE-F-MHT    PIC 9(09)9V99.                              
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-VOL-VTE       PIC S9(09).                                 
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-TOT-VTE-N-HT  PIC S9(09)9V99.                             
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-TOT-MARGE-NET PIC S9(09)9V99.                             
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-STOCKTOT      PIC S9(06)9.                                
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK131-VALOPRMP      PIC S9(11)9V99.                             
      *                                                                         
                                                                                
