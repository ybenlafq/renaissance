      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00010001
      *                                                                *00020001
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT FNM180 AU 19/06/2001  *00030001
      *                                                                *00040001
      * -------------------------------------------------------------- *00041001
      * MODIFICATION :  08/10/02                                       *00042001
      * PROJET       :  BIENS ET SERVICES                              *00043001
      * OBJET        :  LE CODE OPERATEUR PASSE DE 4 A 7               *00044001
      *                 LE CODE OPERATEUR PASSE DE 4 A 8               *00045001
      *                 AJOUT D'UN FILLER                              *00045101
      * LONGUEUR TOTALE DU FICHIER : 120                               *00046002
      *----------------------------------------------------------------*00050001
       01  DSECT-FNM180.                                                00060001
            05 NOMETAT-FNM180           PIC X(7).                       00070001
            05 RUPTURES-FNM180.                                         00080001
           10 FNM180-DATETR             PIC X(08).                      00090001
           10 FNM180-NSOC               PIC X(03).                      00100001
           10 FNM180-NLIEU              PIC X(03).                      00110001
           10 FNM180-DCAISSE            PIC X(08).                      00120001
           10 FNM180-NCAISSE            PIC X(03).                      00130001
      *                                                                 00140001
            05 CHAMPS-FNM180.                                           00150001
AL0810*    10 FNM180-NTRANS             PIC X(04).                      00160002
           10 FNM180-NTRANS             PIC X(08).                      00161002
           10 FNM180-NVENTE-NEMPORT     PIC X(08).                      00170001
           10 FNM180-DHVENTE            PIC X(02).                      00180001
           10 FNM180-DMVENTE            PIC X(02).                      00190001
AL0810*    10 FNM180-NOPERATEUR         PIC X(04).                      00200002
           10 FNM180-NOPERATEUR         PIC X(07).                      00201002
      *    10 FNM180-NCODIC-LREF        PIC X(20).                      00210001
           10 FNM180-LIBELLE            PIC X(20).                      00220001
AL0810*    10 FNM180-CRAYON             PIC X(02).                      00230004
           10 FNM180-NTYPVENTE          PIC X(03).                      00231004
           10 FNM180-PREGLRENDU         PIC S9(07)V9(2).                00240001
           10 FILLER                    PIC X(33).                      00250004
                                                                                
