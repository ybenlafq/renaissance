      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * NEM : DSECT DU FICHIER FEC900                                *  00000020
      ****************************************************************  00000030
      *--> LONGUEUR 385                                                 00000050
                                                                        00000060
       01  FEC900-ENREG.                                                00000070
           05  FEC900-CLEF.                                                     
      *-->     NOM DE L'EXTRACTION (NOM DE LA TABLE EXTRAITE).          00000080
               10  FEC900-FICHIER   PIC X(10).                          00000090
      *-->     CLE INTERNE A CHAQUE EXTRACT.                                    
               10  FEC900-CLE       PIC X(100).                         00000260
      *-->     CODE MISE A JOUR                                         00000200
      *-->     'S' = SUPPRESSION, 'C' = CREATION, 'M' = MAJ             00000200
           05  FEC900-MAJ           PIC X(01).                          00000210
           05  FEC900-DATA          PIC X(252).                         00000270
           05  FILLER               PIC X(22).                                  
      ****************************************************************  00000280
      * NEM : DSECT DU FICHIER FEC901                                *  00000290
      ****************************************************************  00000300
      *--> LONGUEUR 385                                                 00000320
                                                                        00000330
       01  FEC901-ENREG.                                                00000070
           05  FEC901-CLEF.                                                     
      *-->     NOM DE L'EXTRACTION (NOM DE LA TABLE EXTRAITE).          00000080
               10  FEC901-FICHIER   PIC X(10).                          00000090
      *-->     CLE INTERNE A CHAQUE EXTRACT.                                    
               10  FEC901-CLE       PIC X(100).                         00000260
      *-->     CODE MISE A JOUR                                         00000200
      *-->     'S' = SUPPRESSION, 'C' = CREATION, 'M' = MAJ             00000200
           05  FEC901-MAJ           PIC X(01).                          00000210
           05  FEC901-DATA          PIC X(252).                         00000270
           05  FILLER               PIC X(22).                                  
      ****************************************************************  00000580
      * NEM : DSECT DU FICHIER FEC902                                *  00000590
      ****************************************************************  00000600
      *--> LONGUEUR 385                                                 00000620
                                                                        00000630
       01  FEC902-ENREG.                                                00000640
           05  FEC902-CLEF.                                                     
      *-->     NOM DE L'EXTRACTION (NOM DE LA TABLE EXTRAITE).          00000080
               10  FEC902-FICHIER   PIC X(10).                          00000090
      *-->     CLE INTERNE A CHAQUE EXTRACT.                                    
               10  FEC902-CLE       PIC X(100).                         00000260
      *-->     CODE MISE A JOUR                                         00000200
      *-->     'S' = SUPPRESSION, 'C' = CREATION, 'M' = MAJ             00000200
           05  FEC902-MAJ           PIC X(01).                          00000210
           05  FEC902-DATA          PIC X(252).                         00000270
           05  FILLER               PIC X(22).                                  
                                                                                
