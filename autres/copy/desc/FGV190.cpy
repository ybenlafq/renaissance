      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FGV190 D'EXTRACTION DES           *00030009
      *       CHIFFRES D'AFFAIRES TOTAUX PAR VENDEURS                  *00040009
      *       CUMULES SUR LA PERIODE VOULUE (MOIS OU SEMAINE)          *00041008
      *       TRIES PAR NSOC, NLIEU, VENDEUR                           *00042009
      *       LONGUEUR D'ENREGISTREMENT : 80 C.                        *00050008
      *================================================================*00060000
      *                                                                 00070000
       01  FILLER.                                                      00080000
      *                                                                 00090000
           05  FGV190-ENREG.                                            00100009
               10  FGV190-NSOCIETE       PIC  X(03).                    00110009
               10  FGV190-NLIEU          PIC  X(03).                    00120009
               10  FGV190-CVENDEUR       PIC  X(06).                    00130009
               10  FGV190-CA             PIC  S9(11)V99 COMP-3.         00131010
               10  FILLER                PIC  X(61).                    00140010
      *                                                                 00460000
                                                                                
