      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *                                                                *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  ENR-FMD417.                                                          
           10 FMD417-NSOC               PIC X(03).                      099  003
           10 FMD417-NMAG               PIC X(03).                      096  003
           10 FMD417-WSEQFAM            PIC 9(05).                              
           10 FMD417-CFAM               PIC X(05).                      029  020
           10 FMD417-LFAM               PIC X(20).                      029  020
           10 FMD417-COPER              PIC X(10).                      049  020
           10 FMD417-LOPER              PIC X(20).                      049  020
           10 FMD417-PCESSIONCUM        PIC S9(11)V9(2) COMP-3.         102  007
           10 FMD417-PCESSIONCUM-EXC    PIC S9(11)V9(2) COMP-3.         109  007
           10 FMD417-PRMPCUM            PIC S9(11)V9(2) COMP-3.         116  007
           10 FMD417-PRMPCUM-EXC        PIC S9(09)V9(2) COMP-3.         123  006
           10 FMD417-QTECUM             PIC S9(06)      COMP-3.         129  004
           10 FMD417-QTECUM-EXC         PIC S9(06)      COMP-3.         133  004
           10 FMD417-DDEB               PIC X(08).                      137  008
           10 FMD417-DFIN               PIC X(08).                      145  008
           10 FMD417-LNMAG              PIC X(20).                              
            05 FILLER                   PIC X(30).                              
                                                                                
