      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************           
      *   FICHIER DES MOUVEMENTS COMPTABLES ISSUS DES INTERFACES    *           
      *              - NOUVELLE DSECT STANDARD GCT -                *           
      *                                                             *           
      *      - DSECT COBOL                                          *           
      *      - NOM = FRECYC                                         *           
      *      - RECSIZE = 200                                        *           
      *                                                             *           
      ***************************************************************           
       01  FRECYC-ENR.                                                          
      ***************************************************************           
      * TRI DE CE FICHIER : SUR LES 17 PREMIERES POSITIONS          *           
      *                   + LA DEVISE EN POSITION 186 SUR  3        *           
      *                   + LE NUMERO DE PIECE EN  18 SUR 10        *           
      ***************************************************************           
           02  FRECYC-CLE.                                                      
   1           03  FRECYC-NSOC                     PIC X(05).                   
   6           03  FRECYC-NETAB                    PIC X(03).                   
   9           03  FRECYC-NJRN                     PIC X(03).                   
  12           03  FILLER                          PIC X(01).                   
  13           03  FRECYC-CINTERFACE               PIC X(05).                   
      *------> DEVISE                                                           
  18           03  FRECYC-NPIECE                   PIC X(10).                   
      ***************************************************************           
  28       02  FRECYC-NATURE                       PIC X(03).                   
  31       02  FRECYC-REFDOC                       PIC X(12).                   
  43       02  FRECYC-DFTI00                       PIC X(08).                   
  51       02  FRECYC-NCOMPTE                      PIC X(06).                   
  57       02  FRECYC-NSSCOMPTE                    PIC X(06).                   
  63       02  FRECYC-NSECTION                     PIC X(06).                   
  69       02  FRECYC-NRUBR                        PIC X(06).                   
  75       02  FRECYC-NSTEAPP                      PIC X(05).                   
  80       02  FRECYC-NTIERSCV                     PIC X(08).                   
  88       02  FRECYC-NLETTRAGE                    PIC X(10).                   
  98       02  FRECYC-LMVT                         PIC X(25).                   
 123       02  FRECYC-PMONTMVT                     PIC 9(13)V99  COMP-3.        
 131       02  FRECYC-CMONTMVT                     PIC X(01).                   
 132       02  FRECYC-DMVT                         PIC X(08).                   
 140       02  FRECYC-NOECS                        PIC X(05).                   
      * CATEGORIE DE CHARGE ET NATURE DE CHARGE                                 
 145       02  FRECYC-CATCH                        PIC X(01).                   
 146       02  FRECYC-NATCH                        PIC X(06).                   
      * DATE D'ECHEANCE ET BON A PAYER                                          
 152       02  FRECYC-DECH                         PIC X(08).                   
      * BON A PAYER                                                             
 160       02  FRECYC-WBAP                         PIC X(01).                   
      * CONTROLE CENTRALISE DES TRAITEMENTS :                                   
      * -> NOM DE PROGRAMME ET DATE DE TRAITEMENT                               
 161       02  FRECYC-CNOMPROG                     PIC X(08).                   
 169       02  FRECYC-DTRAITEMENT                  PIC X(08).                   
      * ICS                                                                     
 177       02  FRECYC-WCUMUL                       PIC X(01).                   
 178       02  FRECYC-DDOC                         PIC X(08).                   
      * DEVISE --> FAIT PARTIE DE LA CLEF DE TRI                                
 186       02  FRECYC-CDEVISE                      PIC X(03).                   
 189       02  FILLER                              PIC X(12).                   
                                                                                
