      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGR050  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : EDITIONS DES ETATS DE RECEPTIONS VALORISES. *         
      *                                                               *         
      *    FGR050       : FICHIER CONTENANT  LES INFORMATIONS  NECES- *         
      *                 : SAIRES  A L'EDITION DES ETATS DE RECEPTIONS *         
      *                 : VALORISES.                                  *         
      *                                                               *         
      *    STRUCTURE    :    SAM                                      *         
      *    LONGUEUR ENR.:  127 C                                      *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  FGR050-ENREG.                                                        
           03  FGR050-CLEF.                                                     
               05  FGR050-WTLMELA        PIC X(01).                             
               05  FGR050-CHEFPROD       PIC X(05).                             
               05  FGR050-NENTCDE        PIC X(05).                             
               05  FGR050-WSEQFAM        PIC S9(05)       COMP-3.               
               05  FGR050-CMARQ          PIC X(05).                             
               05  FGR050-NCODIC         PIC X(07).                             
               05  FGR050-DREC           PIC X(08).                             
               05  FGR050-BR.                                                   
                   07  FGR050-NREC       PIC X(07).                             
                   07  FGR050-NRECCPT    PIC X(07).                             
                   07  FGR050-NAVENANT   PIC X(02).                             
           03  FGR050-DCDE               PIC X(08).                             
           03  FGR050-NCDE               PIC X(07).                             
           03  FGR050-CFAM               PIC X(05).                             
           03  FGR050-LREFFOURN          PIC X(20).                             
           03  FGR050-QREC               PIC S9(05)       COMP-3.               
           03  FGR050-QSTOCKINIT         PIC S9(09)       COMP-3.               
           03  FGR050-QSTOCKFINAL        PIC S9(09)       COMP-3.               
           03  FGR050-PRMPINIT           PIC S9(07)V9(06) COMP-3.               
           03  FGR050-PRMPFINAL          PIC S9(07)V9(06) COMP-3.               
           03  FGR050-PBFUNIT            PIC S9(07)V9(02) COMP-3.               
           03  FGR050-PRAUNIT            PIC S9(07)V9(02) COMP-3.               
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
