      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *===============================================================* 00000690
      *                 DESCRIPTION DE FSV014                         * 00000700
      *===============================================================* 00000710
      *                                                                 00000880
       01  WW-FSV014.                                                   00000890
         03  FSV014-NOMPGM  .                                           00000900
           05  FSV014-NOMPROG          PIC  X(07) VALUE 'FSV014' .      00000900
           05  FSV014-APPDEST          PIC  X(03).                      00000900
         03  FSV014-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV014.                                                  00000890
           05  FSV014-APPLICAT         PIC  X(08).                      00000900
           05  FSV014-CRAYCIB          PIC  X(05).                      00001100
           05  FSV014-LRAYCIB          PIC  X(30).                      00001100
           05  FSV014-CTRAYCIB         PIC  X(01).                      00001100
           05  FSV014-COMMENT          PIC  X(10).                      00001100
           05  FSV014-COMMENT1         PIC  X(05).                      00001100
         03  FSV014-FILLEUR          PIC  X(100)  VALUE SPACES.         00001020
         03  FSV014-CLE-TRI      .                                              
           05  FSV014-CLE-XXX          PIC  X(30)  VALUE SPACES.           00001
      *                                                                 00001320
                                                                                
