      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00001000
      * ****************       DESCRIPTION     ******************* *    00002000
      *                 *******   FTL076  *******                  *    00002100
      *       FICHIER DES VENTES DU JOUR      PAR MAGASIN          *    00002200
      *       POUR EDITION ETAT DES ANOMALIES DE LIVRAISON         *    00002300
      *------------------------------------------------------------*    00002400
      *                  FICHIER SAM, LONGUEUR 40                  *    00002500
      **************************************************************    00002601
      *CR LE 13/06/2008 . COPIER A PARTIR DU MASTER.SOURCE         *    00002701
      *                 . AJOUT NSOCDEPLIV ET NLIEUDEPLIV A PARTIR *    00002801
      *                   DU FILLER PIC X(07) D'AVANT              *    00002901
      **************************************************************    00003000
      *                                                                 00003100
       01  ENR-FTL076.                                                  00003200
      *-------------------------------------  CODE SOCIETE MAGASIN      00003300
             02 FTL076-NSOCIETE      PIC X(03).                         00003400
      *-------------------------------------  CODE MAGASIN              00003500
             02 FTL076-NLIEU         PIC X(03).                         00003600
      *-------------------------------------  CODE VENTE                00003700
             02 FTL076-NVENTE        PIC 9(07).                         00003800
      *-------------------------------------  DATE DE LIVRAISON         00003900
             02 FTL076-DDELIV        PIC X(08).                         00004000
      *-------------------------------------  NUMERO DE FOLIO           00004100
             02 FTL076-NFOLIO        PIC X(03).                         00004200
      *-------------------------------------  VALEUR RESERVE FOURNISSEUR00004300
             02 FTL076-WCQERESF      PIC X(01).                         00004400
      *-------------------------------------  DATE DE TRAITEMENT        00005000
             02 FTL076-DMVT          PIC X(08).                         00006000
CR1306*-------------------------------------  NSOCDEPLIV                00006102
  "          02 FTL076-NSOCDEPLIV    PIC X(03).                         00006203
  "   *-------------------------------------  NLIEUDEPLIV               00006302
  "          02 FTL076-NLIEUDEPLIV   PIC X(03).                         00006403
  "   *-------------------------------------  LIBRE                     00006502
  "          02 FTL076-FILLER        PIC X(01).                         00006603
      ***************************************************************** 00009000
                                                                                
