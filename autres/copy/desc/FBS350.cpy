      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBS350 AU 12/08/2004  *00000020
      *                                                                *00000030
      *          CRITERES DE TRI (01,05,PD,A,                          *00000040
      *                           06,04,PD,D,                          *00000050
      *                           10,02,BI,A)                          *00000060
      *                                                                *00000070
      *                                                                *00000080
      *----------------------------------------------------------------*00000090
       01  DSECT-FBS350.                                                00000100
            05 RUPTURES-FBS350.                                         00000110
1>5        10 FBS350-WSEQPRO            PIC S9(09)      COMP-3.         00000120
6>4        10 FBS350-PRIX               PIC S9(05)V9(2) COMP-3.         00000130
10>2       10 FBS350-RANGEXPO           PIC X(02).                      00000140
            05 CHAMPS-FBS350.                                           00000150
12>.       10 FBS350-AGRPR1             PIC X(05).                      00000160
           10 FBS350-AGRPR10            PIC X(05).                      00000170
           10 FBS350-AGRPR2             PIC X(05).                      00000180
           10 FBS350-AGRPR3             PIC X(05).                      00000190
           10 FBS350-AGRPR4             PIC X(05).                      00000200
           10 FBS350-AGRPR5             PIC X(05).                      00000210
           10 FBS350-AGRPR6             PIC X(05).                      00000220
           10 FBS350-AGRPR7             PIC X(05).                      00000230
           10 FBS350-AGRPR8             PIC X(05).                      00000240
           10 FBS350-AGRPR9             PIC X(05).                      00000250
           10 FBS350-CFAM               PIC X(05).                      00000260
           10 FBS350-CMARQ              PIC X(05).                      00000270
           10 FBS350-LREFFOURN          PIC X(20).                      00000280
           10 FBS350-LSEQPRO            PIC X(20).                      00000290
           10 FBS350-LSTATCOMP          PIC X(03).                      00000300
           10 FBS350-NCODIC             PIC X(07).                      00000310
           10 FBS350-WOA                PIC X(01).                      00000320
           10 FBS350-V4S                PIC S9(05)      COMP-3.         00000330
           10 FBS350-NBAUTRE            PIC S9(04)      COMP-3.         00000340
           10 FBS350-NBI                PIC S9(03)      COMP-3.         00000350
           10 FBS350-NBPSE              PIC S9(03)      COMP-3.         00000360
           10 FBS350-NBR                PIC S9(03)      COMP-3.         00000370
           10 FBS350-NBTOTAL            PIC S9(03)      COMP-3.         00000380
           10 FBS350-NBU                PIC S9(03)      COMP-3.         00000390
           10 FBS350-NBPRESEL           PIC S9(03)      COMP-3.         00000400
           10 FBS350-TXVL4S             PIC S9(03)V9    COMP-3.         00000410
           10 FBS350-TXVLS              PIC S9(03)V9    COMP-3.         00000420
           10 FBS350-TXV                PIC S9(03)V9    COMP-3.         00000430
           10 FBS350-TXCA               PIC S9(03)V9    COMP-3.         00000440
           10 FBS350-TXMB               PIC S9(03)V9    COMP-3.         00000450
           10 FBS350-TFAM OCCURS 200.                                   00000460
              15 FBS350-T-CFAM          PIC X(7).                       00000470
              15 FBS350-T-QT            PIC S9(3) COMP-3.               00000480
MB2        10 FBS350-NBLIENH            PIC S9(03)      COMP-3.         00000490
MB2        10 FBS350-QT4S               PIC S9(05)      COMP-3.         00000500
MB2        10 FBS350-QTL4S              PIC S9(05)      COMP-3.         00000510
MB2        10 FBS350-QTACCL             PIC S9(05)      COMP-3.         00000520
MB2        10 FBS350-MTHT               PIC S9(07)V99   COMP-3.         00000530
MB2        10 FBS350-MTHTACC            PIC S9(07)V99   COMP-3.         00000540
MB2        10 FBS350-MTPRMP             PIC S9(07)V99   COMP-3.         00000550
MB2        10 FBS350-MTPRMPACC          PIC S9(07)V99   COMP-3.         00000560
MB2        10 FBS350-QTS                PIC S9(07)      COMP-3.         00000570
MB2        10 FBS350-QTLS               PIC S9(05)      COMP-3.         00000580
MB2         05 FILLER                      PIC X(49).                   00000590
                                                                                
