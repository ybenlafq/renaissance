      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * FGK221: PRODUITS POUR KRISP                                             
      ******************************************************************        
       01  FGK221-ENR.                                                          
           02  FGK221-NCODIC         PIC X(07).                                 
           02  FGK221-NSOC           PIC X(03).                                 
           02  FGK221-WDACEM         PIC X(01).                                 
           02  FGK221-DCREATION      PIC X(08).                                 
           02  FGK221-CASSORT        PIC X(05).                                 
           02  FGK221-CAPPRO         PIC X(05).                                 
           02  FGK221-DATE           PIC X(06).                                 
           02  FGK221-CMKT           PIC X(05).                                 
           02  FGK221-LMKT           PIC X(20).                                 
           02  FGK221-CFAM           PIC X(05).                                 
           02  FGK221-LFAM           PIC X(20).                                 
           02  FGK221-LREFFOURN      PIC X(20).                                 
           02  FGK221-CMARQ          PIC X(05).                                 
           02  FGK221-LMARQ          PIC X(20).                                 
           02  FGK221-NEAN           PIC X(13).                                 
           02  FGK221-TAUXTVA        PIC S9(3)V999   PACKED-DECIMAL.            
           02  FGK221-PVUHT          PIC S9(7)V9999  PACKED-DECIMAL.            
           02  FGK221-PCAHT          PIC S9(11)V9999 PACKED-DECIMAL.            
           02  FGK221-QVENDUE        PIC S9(7)       PACKED-DECIMAL.            
           02  FGK221-STOCK          PIC S9(7)       PACKED-DECIMAL.            
           02  FGK221-VALOPRMP       PIC S9(11)V9999 PACKED-DECIMAL.            
           02  FGK221-VALOPV         PIC S9(11)V9999 PACKED-DECIMAL.            
           02  FGK221-PSTDTTC        PIC S9(7)V9999  PACKED-DECIMAL.            
           02  FGK221-STOCKMAG       PIC S9(7)       PACKED-DECIMAL.            
           02  FGK221-STOCKDEP       PIC S9(7)       PACKED-DECIMAL.            
           02  FGK221-PRMP           PIC S9(7)V9(6)  PACKED-DECIMAL.            
     *** ACHATS PAR ENTITE DE COMMANDE                                          
           02  FGK221-ACHATS        OCCURS 25.                                  
               03  FGK221-NENTCDE    PIC X(06).                                 
               03  FGK221-PRA        PIC S9(7)V9999  PACKED-DECIMAL.            
               03  FGK221-PACHATS    PIC S9(11)V9999 PACKED-DECIMAL.            
               03  FGK221-QREC       PIC S9(7)       PACKED-DECIMAL.            
               03  FGK221-QCDE       PIC S9(7)       PACKED-DECIMAL.            
     *** MARGE NET                                                              
           02  FGK221-MARGE-NET      PIC S9(11)V9999 PACKED-DECIMAL.            
     *** CHAQUE POSTE A 30 DE LONG -> 25 POSTES = 750                           
*957  *   02  FILLER                  PIC X(44).                                
 965      02  FILLER                  PIC X(36).                                
1001 *** LRECL=1000                                                             
                                                                                
