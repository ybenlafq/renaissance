      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 FRE010-DSECT.                                                         
           03  FRE010-NSOCIETE       PIC X(03).                                 
           03  FRE010-NLIEU          PIC X(03).                                 
           03  FRE010-DATE           PIC X(06).                                 
           03  FRE010-AGREG2.                                                   
               05 FRE010-CRAYONFAM   PIC X(05).                                 
               05 FRE010-CFAM        PIC X(05).                                 
               05 FRE010-WTLMELA     PIC X(03).                                 
               05 FRE010-AGREGAT     PIC 9(05)     COMP-3.                      
               05 FRE010-LAGREGATED  PIC X(20).                                 
           03  FRE010-REM            PIC S9(07)V99 COMP-3.                      
           03  FRE010-VENTE          PIC S9(07)V99 COMP-3.                      
           03  FRE010-FORCAGE        PIC S9(07)V99 COMP-3.                      
           03  FRE010-PMTACHATS      PIC S9(07)V99 COMP-3.                      
           03  FRE010-QVENDUE        PIC S9(07)    COMP-3.                      
           03  FRE010-RACHAT         PIC S9(07)V99 COMP-3.                      
           03  FRE010-NAGREGATED4    PIC X(05).                                 
           03  FRE010-DSYST          PIC S9(13)    COMP-3.                      
           03  FRE010-NCODIC         PIC X(07).                                 
           03  FILLER                PIC X(04).                                 
      * LONG = 100                                                              
                                                                                
