      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC106  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST DEJA CHARGEE      *         
      *                                                               *         
      *    FBC106       : FICHIER DES EN-TETES DE VENTE               *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:  108 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC106-ENREG.                                                        
           03  FBC106-CLE.                                                      
               05  FBC106-NSOCIETE            PIC  X(03).                       
               05  FBC106-NLIEU               PIC  X(03).                       
               05  FBC106-NVENTE              PIC  X(08).                       
           03  FBC106-DVENTE                  PIC  X(08).                       
           03  FBC106-PTTVENTE                PIC  S9(07)V9(02).                
           03  FBC106-CREMVTE                 PIC  X(05).                       
           03  FBC106-PREMVTE                 PIC  S9(07)V9(02).                
           03  FBC106-DMODIFVTE               PIC  X(08).                       
           03  FBC106-WEXPORT                 PIC  X(01).                       
           03  FBC106-PDIFFERE                PIC  S9(07)V9(02).                
           03  FBC106-CORGORED                PIC  X(05).                       
           03  FBC106-CFCRED                  PIC  X(05).                       
           03  FBC106-NCREDI                  PIC  X(14).                       
           03  FBC106-DVERSION                PIC  X(08).                       
           03  FBC106-DSYST                   PIC  X(13).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
