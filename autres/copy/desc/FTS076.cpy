      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * DEFINITION DU FICHIER DE REMONTEE DES LIENS FACTURES/VENTES             
      * LOCALES                                                                 
      *****************************************************************         
       01 DSECT-FTS076.                                                         
          02 FTS076-DONNEES.                                                    
              05  FTS076-NSOCF          PIC  X(03).                             
              05  FTS076-NLIEUF         PIC  X(03).                             
              05  FTS076-NFACT          PIC  X(07).                             
              05  FTS076-NSOCV          PIC  X(03).                             
              05  FTS076-NLIEUV         PIC  X(03).                             
              05  FTS076-NORDV          PIC  X(08).                             
              05  FTS076-NBCDE          PIC  X(07).                             
              05  FTS076-NORDRE         PIC  X(05).                             
              05  FTS076-NSEQFV         PIC  9(02).                             
          02 FTS076-LIBRE               PIC  X(39).                             
                                                                                
