      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  DSECT-IFSTAT.                                                        
      ******************************************************************        
      *                                                                *        
      * DESCRIPTION DU FICHIER CUMUL MOUVEMENTS POUR STATISTIQUES      *        
      *                                                                *        
      ******************************************************************        
      *  DATE DU JOUR                                                           
           05  IFSTAT-DJOUR      PIC X(8).                               001 008
      *  DATE DE FINANCE                                                        
           05  IFSTAT-DFINANCE   PIC X(8).                               009 008
      *  CODE ORGANISME                                                         
           05  IFSTAT-CORGANISME PIC X(3).                               017 003
      *  TYPE DE MOUVEMENT/CREDIT                                               
           05  IFSTAT-CTYPMVT    PIC X(1).                               020 001
      *  MODE DE PAIEMENT                                                       
           05  IFSTAT-CMODPAI    PIC X(3).                               021 003
      *  CODE SOCIETE                                                           
           05  IFSTAT-NSOCIETE   PIC X(3).                               024 003
      *  NUMERO DE MAGASIN                                                      
           05  IFSTAT-NLIEU      PIC X(3).                               027 003
      *  NOMBRE DE FACTURETTES/TRANSACTIONS                                     
           05  IFSTAT-NFACTURES  PIC S9(7)        COMP-3.                030 004
      *  MONTANT BRUT                                                           
           05  IFSTAT-MTBRUT     PIC S9(7)V9(2)   COMP-3.                034 005
      *  MONTANT COMMISSIONS/AGIOS                                              
           05  IFSTAT-MTCOM      PIC S9(5)V9(2)   COMP-3.                039 004
      *  MONTANT NET FINANCE                                                    
           05  IFSTAT-MTNET      PIC S9(7)V9(2)   COMP-3.                043 005
           05  IFSTAT-FILLER     PIC X(33).                              048 033
                                                                                
