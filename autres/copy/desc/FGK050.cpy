      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : FOURNISSEURS            * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK050                                        * 00100600
      *        - LONGUEUR ENREG = 26 ( <- 70 )                        * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      * AJUSTEMENT LONGUEUR CHAMPS + AJOUT CARACTERE <TAB>            * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK050-ENR.                                                          
           05  FGK050-CODE-ENTCDE   PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE ' '.                    
           05  FGK050-LIB-ENTCDE    PIC X(20).                                  
      *                                                                         
                                                                                
