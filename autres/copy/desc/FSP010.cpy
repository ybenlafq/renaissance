      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *  DSECT DU FICHIER FSP010: VALO DE L'ECART SRP/PRMP, PAR SOCIETE         
      *  EMETTRICE, RECEPTRICE, TYPE TEBRB, TAUX DE TVA                         
      *  --> SERT POUR GENERER LES ETATS ISP010 ET ISP012                       
      *****************************************************************         
      *                                                                         
       01 DSECT-FSP010.                                                         
          02 FSP010-ENTETE.                                                     
001          05 FSP010-NSOCEMET         PIC X(03).                              
004          05 FSP010-NSOCRECEP        PIC X(03).                              
007          05 FSP010-WRETRO           PIC X(01).                              
             05 FSP010-TEBRB.                                                   
008             07 FSP010-TLMELA        PIC X(03).                              
011             07 FSP010-BLBR          PIC X(02).                              
013          05 FILLER                  PIC X(06).                              
      *                                                                         
          02 FSP010-CHAMPS.                                                     
019          05 FSP010-VALOSRP          PIC S9(11)V9(2) COMP-3.                 
026          05 FSP010-VALOPRMP         PIC S9(11)V9(2) COMP-3.                 
033          05 FSP010-TXTVA            PIC S9(03)V9(2) COMP-3.                 
036          05 FSP010-MTTVA            PIC S9(11)V9(2) COMP-3.                 
043          05 FILLER                  PIC X(08).                              
      *                                                                         
                                                                                
LG=050                                                                          
