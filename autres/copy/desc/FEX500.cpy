      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************00002000
      * FICHIER DE SORTIE DU FLUX JOURNALIER ARTICLES (PGM=BEX500)     *00002209
      *                 NCG VERS OPCO                                  *00002309
      ******************************************************************        
      *   DATES    * USERID * NOMS            * FAIRE FIND                      
      ******************************************************************        
      * JJ/MM/SSAA * DSAXXX * X.XXXXXXXXXXXXX * XXXXXXXXXXXXXXXXXX              
      *-----------------------------------------------------------------        
      *                                                                         
      ******************************************************************        
       01  WS-FEX500-ENR.                                                       
           10  FEX500-NCODIC              PIC X(07)    VALUE SPACE.             
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-NEAN                PIC 9(13)    VALUE ZERO.              
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-OPCO-PRODUIT        PIC X(15)    VALUE SPACE.             
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-CFAM                PIC X(05)    VALUE SPACE.             
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-CMARQ               PIC X(05)    VALUE SPACE.             
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-LREFFOURN           PIC X(30)    VALUE SPACE.             
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-EXPO                PIC X(03)    VALUE SPACE.             
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-COLISAGE            PIC 9(05)    VALUE ZERO.              
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-QCONTENU            PIC 9(05)    VALUE ZERO.              
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-PV-DETAIL           PIC 9(07)V99 VALUE ZERO.              
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-PV-GROSSISTE        PIC 9(07)V99 VALUE ZERO.              
           10  FILLER                     PIC X        VALUE ';'.               
           10  FEX500-NCODICLIE           PIC X(07)    VALUE SPACE.             
                                                                                
