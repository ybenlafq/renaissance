      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *================================================================*        
      *   FICHIER DES LIGNES DE MUTATION   FFI005       LG=100         *        
      *================================================================*        
       01  FFI005-ENREG.                                                        
           02  FFI005-NSOCORIG           PIC X(003).                            
           02  FFI005-NLIEUORIG          PIC X(003).                            
           02  FFI005-CTYPLIEUORIG       PIC X(001).                            
           02  FFI005-CTYPFACTORIG       PIC X(005).                            
           02  FFI005-CTYPCONTORIG       PIC X(005).                            
           02  FFI005-NSOCDEST           PIC X(003).                            
           02  FFI005-NLIEUDEST          PIC X(003).                            
           02  FFI005-CTYPLIEUDEST       PIC X(001).                            
           02  FFI005-CTYPFACTDEST       PIC X(005).                            
           02  FFI005-CTYPCONTDEST       PIC X(005).                            
           02  FFI005-NCODIC             PIC X(007).                            
           02  FFI005-NMUTATION          PIC X(007).                            
           02  FFI005-DMOUVEMENT         PIC X(008).                            
           02  FFI005-QMOUVEMENT         PIC S9(05) COMP-3.                     
           02  FFI005-COPER              PIC X(010).                            
0397       02  FFI005-DOPER              PIC X(008).                            
DPMO       02  FFI005-NSOCCTAORIG        PIC X(003).                            
DPMO       02  FFI005-NLIEUCTAORIG       PIC X(003).                            
DPMO       02  FFI005-NSOCCTADEST        PIC X(003).                            
DPMO       02  FFI005-NLIEUCTADEST       PIC X(003).                            
DPMO       02  FFI005-FILLER             PIC X(011).                            
                                                                                
