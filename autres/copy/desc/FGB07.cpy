      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  ENR-FGB07.                                                           
           05 FGB07-CTYPENR        PIC  X.                                      
           05 FGB07-CTYPART        PIC  X.                                      
           05 FGB07-NCODIC7.                                                    
              10 FILLER            PIC  X.                                      
              10 FGB07-NCODIC      PIC  X(6).                                   
           05 FGB07-NSTE.                                                       
              10 FGB07-NSOC        PIC  X(3).                                   
              10 FGB07-NLIEU       PIC  X(3).                                   
           05 FGB07-DREC.                                                       
              10  SS               PIC  XX.                                     
              10 FGB07-DREC6.                                                   
                  15  AA           PIC  XX.                                     
                  15  MM           PIC  XX.                                     
                  15  JJ           PIC  XX.                                     
           05 FGB07-NREC7.                                                      
              10 FILLER            PIC  X.                                      
              10 FGB07-NREC.                                                    
                 15 FILLER         PIC  X.                                      
                 15 FGB07-NREC5    PIC  X(5).                                   
           05 FGB07-NCDE7.                                                      
              10 FILLER            PIC  X.                                      
              10 FGB07-NCDE        PIC  X(6).                                   
           05 FGB07-PRA1           PIC  S9(7)V99 COMP-3.                        
           05 FGB07-PRA2           PIC  S9(7)V99 COMP-3.                        
           05 FGB07-QTE1           PIC  S9(5)    COMP-3.                        
           05 FGB07-QTE2           PIC  S9(5)    COMP-3.                        
           05 FGB07-QSTOCK         PIC  S9(5)    COMP-3.                        
           05 FGB07-PRMP           PIC  S9(7)V99 COMP-3.                        
           05 FGB07-DATMOD.                                                     
              10  AA               PIC  XX.                                     
              10  MM               PIC  XX.                                     
              10  JJ               PIC  XX.                                     
           05 FGB07-DATPAR.                                                     
              10  AA               PIC  XX.                                     
              10  MM               PIC  XX.                                     
              10  JJ               PIC  XX.                                     
           05 FGB07-SOLDE          PIC  X.                                      
           05 FGB07-NENTREPOT.                                                  
              10 FGB07-NSOCLIVR    PIC  X(3).                                   
              10 FGB07-NDEPOT      PIC  X(3).                                   
           05 FGB07-RECYCL         PIC  X(1).                                   
           05 FGB07-DATARET        PIC  X(6).                                   
           05 FGB07-PCF            PIC  S9(7)V99 COMP-3 .                       
           05 FILLER               PIC  X(08).                                  
                                                                                
