      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00000690
      * DSECT PROGRAMME BGV015 LG = 87                                  00000690
      *                                                                 00000690
       01  FGV015-RECORD.                                               00000700
           05 FGV015-NSOCIETE      PIC X(3).                            00000710
           05 FGV015-WSEQPRO       PIC S9(7) COMP-3.                    00000720
           05 FGV015-LSEQPRO       PIC X(26).                           00000730
           05 FGV015-CRAYONFAM     PIC X(5).                            00000730
           05 FGV015-WTYPE         PIC X(1).                            00000750
           05 FGV015-MOIS-N.                                            00000760
              10 FGV015-NB-N       PIC S9(7) COMP-3.                    00000770
              10 FGV015-CA-N       PIC S9(9)V99 COMP-3.                 00000780
              10 FGV015-MTA-N      PIC S9(9)V99 COMP-3.                 00000790
           05 FGV015-MOIS-N-1.                                          00000800
              10 FGV015-NB-N-1     PIC S9(7) COMP-3.                    00000810
              10 FGV015-CA-N-1     PIC S9(9)V99 COMP-3.                 00000820
              10 FGV015-MTA-N-1    PIC S9(9)V99 COMP-3.                 00000830
           05 FGV015-MOIS-N-2.                                          00000840
              10 FGV015-NB-N-2     PIC S9(7) COMP-3.                    00000850
              10 FGV015-CA-N-2     PIC S9(9)V99 COMP-3.                 00000860
              10 FGV015-MTA-N-2    PIC S9(9)V99 COMP-3.                 00000870
           05 FILLER               PIC X(4).                            00000880
      *                                                                 00000890
                                                                                
