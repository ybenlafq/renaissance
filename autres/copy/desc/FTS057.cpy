      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * DEFINITION DU FICHIER DE REMONTEE DES REMISES                           
      *****************************************************************         
       01 DSECT-FTS057.                                                         
          02 FTS057-DONNEES.                                                    
              05  FTS057-NSOC           PIC  X(03).                             
              05  FTS057-NLIEU          PIC  X(03).                             
              05  FTS057-NCAISS         PIC  X(03).                             
              05  FTS057-NTRANS         PIC  X(04).                             
              05  FTS057-NCODIC         PIC  X(07).                             
              05  FTS057-CVEND          PIC  X(06).                             
              05  FTS057-VFORCP         PIC S9(07)V9(02) COMP-3.                
              05  FTS057-VFORCM         PIC S9(07)V9(02) COMP-3.                
              05  FTS057-VALTH          PIC S9(07)V9(02) COMP-3.                
              05  FTS057-QLGVRP         PIC S9(05)       COMP-3.                
              05  FTS057-VALREM         PIC S9(07)V9(02) COMP-3.                
              05  FTS057-DJOUR          PIC  X(08).                             
          02 FTS057-LIBRE               PIC  X(50).                             
                                                                                
