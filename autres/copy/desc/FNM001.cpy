      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *        FICHIER LOG DE CAISSE                                  * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FNM001                                        * 00100600
      *        - LONGUEUR CLE = 50                                    * 00101000
      *        - LONGUEUR ENREG = 200                                 * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      *  01.99       - TRAITEMENT HOST CAISSES  -                    *  00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FNM001-ENR.                                                          
           02  FNM001-CLE.                                                      
             03  FNM001-CLE-DATETR                   PIC X(8).                  
             03  FNM001-CLE-NTRANS                   PIC X(8).                  
             03  FNM001-CLE-CTTRAN                   PIC X(3).                  
             03  FNM001-CLE-NSOC                     PIC X(3).                  
             03  FNM001-CLE-NLIEU                    PIC X(3).                  
             03  FNM001-CLE-NCAISS                   PIC X(3).                  
             03  FNM001-CLE-NSOCV                    PIC X(3).                  
             03  FNM001-CLE-NLIEUV                   PIC X(3).                  
             03  FNM001-CLE-NVENT                    PIC X(8).                  
             03  FNM001-CLE-TYPE                     PIC X(1).                  
             03  FNM001-CLE-CNATUR                   PIC X(3).                  
             03  FNM001-CLE-CNATVE REDEFINES FNM001-CLE-CNATUR.                 
                 04 FNM001-CLE-NATURE                PIC X(2).                  
                 04 FNM001-CLE-NTYPVENTE             PIC X(1).                  
             03  FNM001-CLE-NUM                      PIC X(1).                  
             03  FNM001-CLE-NLIGNE                   PIC 9(3).                  
           02  FNM001-ENR-FILLER                     PIC X(150).                
      **** ENREGISTREMENT (NUM = 1)--> ENTETE DE PAIEMENT                       
      **** TYPE = 0                                                             
      **** SI NATURE = VE ==> VENTE                                             
      **** SINON          ==> MOUVEMENT FINANCIER                               
           02  FNM001-PAIM REDEFINES FNM001-ENR-FILLER.                         
             04  FNM001-PAIM-CODOPE              PIC X(7).                      
             04  FNM001-PAIM-PTOTAL              PIC S9(7)V99 COMP-3.           
             04  FNM001-PAIM-DEV-TRANS           PIC X(3).                      
             04  FNM001-PAIM-HTRANS              PIC X(6).                      
             04  FNM001-PAIM-FILLER              PIC X(128).                    
      **** ENREGISTREMENT (NUM = 2)--> ENTETE DE VENTE                          
      **** 1 OCCUR/VENTE MAIS 1 PAIEMENT = X VENTES + Y MVT FINANCIER           
           02  FNM001-VTE REDEFINES FNM001-ENR-FILLER.                          
             04  FNM001-VTE-NBCOM                PIC X(10).                     
             04  FNM001-VTE-DVENT                PIC X(8).                      
             04  FNM001-VTE-PTOTAL               PIC S9(7)V99 COMP-3.           
             04  FNM001-VTE-PENC                 PIC S9(7)V99 COMP-3.           
             04  FNM001-VTE-L                    PIC 9(3).                      
             04  FNM001-VTE-NCREDI               PIC X(14).                     
             04  FNM001-VTE-CFCRED               PIC X(5).                      
             04  FNM001-VTE-CODCRED              PIC X(1).                      
             04  FNM001-VTE-DEV-REF              PIC X(3).                      
             04  FNM001-VTE-SOCFAC               PIC X(3).                      
             04  FNM001-VTE-MAGFAC               PIC X(3).                      
             04  FNM001-VTE-NFAC                 PIC X(7).                      
             04  FNM001-VTE-HVENT                PIC X(6).                      
             04  FNM001-VTE-CADEAU               PIC S9(7)V99 COMP-3.           
             04  FNM001-VTE-CADEAU-R REDEFINES   FNM001-VTE-CADEAU.             
                 05 FNM001-VTE-CADEAU-X          PIC X(5).                      
      *      04  FNM001-VTE-FILLER               PIC X(67).                     
             04  FNM001-VTE-FILLER               PIC X(62).                     
      **** ENREGISTREMENT (NUM = 3)--> LIGNE DE VENTE                           
           02  FNM001-LVTE REDEFINES FNM001-ENR-FILLER.                         
             04  FNM001-LVTE-LCLVNTE             PIC X(1).                      
             04  FNM001-LVTE-NSOCM               PIC X(3).                      
             04  FNM001-LVTE-NLIEUM              PIC X(3).                      
             04  FNM001-LVTE-CENREG              PIC X(7).                      
             04  FNM001-LVTE-CFAM                PIC X(5).                      
             04  FNM001-LVTE-CMARQ               PIC X(5).                      
             04  FNM001-LVTE-CVEND               PIC X(7).                      
             04  FNM001-LVTE-RABAIS              PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-PMTREM              PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-MTHEO               PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-CREMIS              PIC X(5).                      
             04  FNM001-LVTE-TFORCE              PIC X(1).                      
             04  FNM001-LVTE-NDECOT              PIC X(10).                     
             04  FNM001-LVTE-MODDL               PIC X(3).                      
             04  FNM001-LVTE-QTE-PU              PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-TVA                 PIC S9(3)V99 COMP-3.           
             04  FNM001-LVTE-NLGVTH              PIC 9(3).                      
             04  FNM001-LVTE-CTYPENREG           PIC X(1).                      
             04  FNM001-LVTE-QLGVRP              PIC 9(5).                      
             04  FNM001-LVTE-PUNIT               PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-PVUNRF              PIC S9(7)V99 COMP-3.           
      *      04  FNM001-LVTE-CLVNTE              PIC X(1).                      
             04  FILLER                          PIC X(1).                      
             04  FNM001-LVTE-LREF                PIC X(20).                     
             04  FNM001-LVTE-CODICGRP            PIC X(07).                     
             04  FNM001-LVTE-PUNIT-GRP           PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-QLGVRP-GRP          PIC 9(5).                      
             04  FNM001-LVTE-MPRIMECLI           PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-QTE-PU-GRP          PIC S9(7)V99 COMP-3.           
             04  FNM001-LVTE-FILLER              PIC X(10).                     
      **** ENREGISTREMENT (NUM = 4)--> MODE DE PAIEMENT SAISIE                  
           02  FNM001-MODP REDEFINES FNM001-ENR-FILLER.                         
             04  FNM001-MODP-CMOPAI              PIC X(5).                      
             04  FNM001-MODP-PDPAIE              PIC S9(7)V99 COMP-3.           
             04  FNM001-MODP-CMOPAC              PIC X(5).                      
             04  FNM001-MODP-CDEV                PIC X(3).                      
             04  FNM001-MODP-CIMPAI              PIC X(20).                     
             04  FNM001-MODP-PMDVRF              PIC S9(7)V99 COMP-3.           
             04  FNM001-MODP-CDEVRF              PIC X(3).                      
             04  FNM001-MODP-PMARRD              PIC S9(7)V99 COMP-3.           
             04  FNM001-MODP-FILLER              PIC X(99).                     
      **** ENREGISTREMENT (NUM = 5)--> TYPE DE VENTILATION PAIEMENT             
      **** 5 OCURENCES MAXI                                                     
           02  FNM001-LIGREG REDEFINES FNM001-ENR-FILLER.                       
             04  FNM001-LIGREG-CMDRGT            PIC X(5).                      
             04  FNM001-LIGREG-PMRGLT            PIC S9(7)V99 COMP-3.           
             04  FNM001-LIGREG-GVNAUTO           PIC X(20).                     
             04  FNM001-LIGREG-FILLER            PIC X(120).                    
      **** ENREGISTREMENT (NUM = 6)--> TYPE DE VENTILATION PAIEMENT             
      **** 1 OCCURENCE PAR COUPLE VENTE OU MVT / LIGNE DE PAIEMENT              
           02  FNM001-PAIVE REDEFINES FNM001-ENR-FILLER.                        
             04  FNM001-PAIVE-PVENTL             PIC S9(7)V99 COMP-3.           
             04  FNM001-PAIVE-CMOPAI             PIC X(5).                      
             04  FNM001-PAIVE-CDEV               PIC X(3).                      
             04  FNM001-PAIVE-PVLTRF             PIC S9(7)V99 COMP-3.           
             04  FNM001-PAIVE-CDEVRF             PIC X(3).                      
             04  FNM001-PAIVE-PMECRT             PIC S9(7)V99 COMP-3.           
             04  FNM001-PAIVE-PTECRT             PIC S9(7)V99 COMP-3.           
             04  FNM001-PAIVE-FILLER             PIC X(119).                    
      **** ENREGISTREMENT (NUM = 2)--> ENTETE MOUVEMENTS FINANCIER              
           02  FNM001-MVTFI REDEFINES FNM001-ENR-FILLER.                        
             04  FNM001-MVTFI-NBCOM              PIC X(10).                     
             04  FNM001-MVTFI-PTOTAL             PIC S9(7)V99 COMP-3.           
             04  FNM001-MVTFI-PENC               PIC S9(7)V99 COMP-3.           
             04  FNM001-MVTFI-NLIGN              PIC 9(3).                      
             04  FNM001-MVTFI-NATTR              PIC X(24).                     
             04  FNM001-MVTFI-CFCRED             PIC X(5).                      
             04  FNM001-MVTFI-CODCRED            PIC X(1).                      
             04  FNM001-MVTFI-CDEVRF             PIC X(3).                      
MBEN00*      04  FNM001-MVTFI-FILLER             PIC X(94).                     
MBEN00       04  FNM001-MVTFI-INDA2I              PIC X(1).                     
MBEN00       04  FNM001-MVTFI-FILLER             PIC X(93).                     
      *                                                                         
      *****************************************************************         
      *                                                                         
      **** ENREGISTREMENT (NUM = 1)--> OPERATION ADMINISTRATIVE                 
      **** TYPE = 1                                                             
      **** SI NATURE = CTL             ==> FOND DE CAISSE                       
      ****           = AP              ==> APPORT DE CAISSE                     
      ****           = RE              ==> RETRAIT DE CAISSE                    
      ****           = TR+ OU TR-      ==> TRANSFERT                            
      ****           = DEC             ==> DECOMPTE DE CAISSE                   
      ****           = RBQ             ==> REMISE EN BANQUE                     
           02  FNM001-ADMOP REDEFINES FNM001-ENR-FILLER.                        
             04  FNM001-ADMOP-CODOPE          PIC X(7).                         
             04  FNM001-ADMOP-CVEND           PIC X(7).                         
             04  FNM001-ADMOP-PTOTAL          PIC S9(7)V99 COMP-3.              
             04  FNM001-ADMOP-DEV-TRANS       PIC X(3).                         
             04  FNM001-ADMOP-PTOTALRF        PIC S9(7)V99 COMP-3.              
             04  FNM001-ADMOP-DEVRF           PIC X(3).                         
             04  FNM001-ADMOP-NTRANSXX        PIC X(8).                         
             04  FNM001-ADMOP-TOP             PIC X(1).                         
             04  FNM001-ADMOP-HTRANS          PIC X(6).                         
             04  FNM001-ADMOP-FILLER          PIC X(103).                       
      **** ENREGISTREMENT (NUM = 2)--> DECOMPTE DE CAISSE                       
           02  FNM001-DECPT REDEFINES FNM001-ENR-FILLER.                        
             04  FNM001-DECPT-WDECOK          PIC X(1).                         
             04  FNM001-DECPT-WFLGSP          PIC X(1).                         
             04  FNM001-DECPT-CMOPAI          PIC X(5).                         
             04  FNM001-DECPT-CDEV            PIC X(3).                         
             04  FNM001-DECPT-PMSAIS          PIC S9(7)V99 COMP-3.              
             04  FNM001-DECPT-PMATTN          PIC S9(7)V99 COMP-3.              
             04  FNM001-DECPT-FILLER          PIC X(130).                       
      **** ENREGISTREMENT (NUM = 2)--> FOND DE CAISSE                           
           02  FNM001-FOND  REDEFINES FNM001-ENR-FILLER.                        
             04  FNM001-FOND-PMTHEO           PIC S9(7)V99 COMP-3.              
             04  FNM001-FOND-CDEV             PIC X(3).                         
             04  FNM001-FOND-PMFREF           PIC S9(7)V99 COMP-3.              
             04  FNM001-FOND-PMTFRF           PIC S9(7)V99 COMP-3.              
             04  FNM001-FOND-CDEVRF           PIC X(3).                         
             04  FNM001-FOND-ECRFD            PIC S9(7)V99 COMP-3.              
             04  FNM001-FOND-ECRRF            PIC S9(7)V99 COMP-3.              
             04  FNM001-FOND-WFONOK           PIC X(1).                         
             04  FNM001-FOND-FILLER           PIC X(115).                       
      **** ENREGISTREMENT (NUM = 2)--> APPORT OU RETRAIT                        
           02  FNM001-APRET REDEFINES FNM001-ENR-FILLER.                        
             04  FNM001-APRET-FILLER          PIC X(150).                       
      **** ENREGISTREMENT (NUM = 2)--> TRANSFERT                                
           02  FNM001-TRANSF REDEFINES FNM001-ENR-FILLER.                       
             04  FNM001-TRANSF-CMODPAI        PIC X(5).                         
             04  FNM001-TRANSF-NCAISC         PIC X(3).                         
             04  FNM001-TRANSF-NTRCPT         PIC 9(8).                         
             04  FNM001-TRANSF-FILLER         PIC X(134).                       
      **** ENREGISTREMENT (NUM = 2)--> REMISE EN BANQUE                         
           02  FNM001-RBANK REDEFINES FNM001-ENR-FILLER.                        
             04  FNM001-RBANK-NENVLP            PIC X(10).                      
             04  FNM001-RBANK-CMODPAI           PIC X(5).                       
             04  FNM001-RBANK-PMTENV            PIC S9(7)V99 COMP-3.            
             04  FNM001-RBANK-FILLER            PIC X(130).                     
                                                                                
