      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *        FICHIER DETAIL ETAT IVA580                             *         
      *                                                               *         
      *        - DSECT COBOL                                          *         
      *        - NOM  = FVA581                                        *         
      *        - LONGUEUR ENREG = 68                                  *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       01  DSECT-FVA581.                                                        
           05 FVA581-NSOC          PIC X(3).                                    
           05 FVA581-CSEQ          PIC X(2).                                    
           05 FVA581-CRAYON        PIC X(5).                                    
           05 FVA581-NLIEU         PIC X(3).                                    
           05 FVA581-CTYPMVT       PIC X.                                       
           05 FVA581-NSOCMVT       PIC X(3).                                    
           05 FVA581-NLIEUMVT      PIC X(3).                                    
           05 FVA581-LLIEU         PIC X(20).                                   
           05 FVA581-QSTOCKENTREE  PIC S9(11) COMP-3.                           
           05 FVA581-QSTOCKSORTIE  PIC S9(11) COMP-3.                           
           05 FVA581-PSTOCKENTREE  PIC S9(9)V9(6) COMP-3.                       
           05 FVA581-PSTOCKSORTIE  PIC S9(9)V9(6) COMP-3.                       
                                                                                
