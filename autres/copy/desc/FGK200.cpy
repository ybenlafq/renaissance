      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
                                                                                
      ******************************************************************        
      * FGK200: TYPOLOGIE PRODUITS ET PRODUITS FILIALE POUR KRISP               
      ******************************************************************        
      *{ Post-Translation Correct-copy-replacing                                
      * 01  FGK200-ENR.                                                         
      *     02  FGK200-NCODIC         PIC X(07).                                
      *     02  FGK200-DCREATION      PIC X(08).                                
      *     02  FGK200-WDACEM         PIC X(01).                                
      *     02  FGK200-CMKT           PIC X(05).                                
      *     02  FGK200-LMKT           PIC X(20).                                
      *     02  FGK200-CFAM           PIC X(05).                                
      *     02  FGK200-LFAM           PIC X(20).                                
      *     02  FGK200-LREFFOURN      PIC X(20).                                
      *     02  FGK200-CMARQ          PIC X(05).                                
      *     02  FGK200-LMARQ          PIC X(20).                                
      *     02  FGK200-NEAN           PIC X(13).                                
      *     02  FGK200-TAUXTVA        PIC S9(3)V999 PACKED-DECIMAL.             
      *     02  FGK200-CASSORT        PIC X(05).                                
      *     02  FGK200-CAPPRO         PIC X(05).                                
       01  FGK200:-ENR.                                                         
   1       02  FGK200:-NCODIC         PIC X(07).                                
   8       02  FGK200:-DCREATION      PIC X(08).                                
  16       02  FGK200:-WDACEM         PIC X(01).                                
  17       02  FGK200:-CMKT           PIC X(05).                                
  22       02  FGK200:-LMKT           PIC X(20).                                
  42       02  FGK200:-CFAM           PIC X(05).                                
  47       02  FGK200:-LFAM           PIC X(20).                                
  67       02  FGK200:-LREFFOURN      PIC X(20).                                
  87       02  FGK200:-CMARQ          PIC X(05).                                
  92       02  FGK200:-LMARQ          PIC X(20).                                
 112       02  FGK200:-NEAN           PIC X(13).                                
 125       02  FGK200:-TAUXTVA        PIC S9(3)V999 PACKED-DECIMAL.             
 129       02  FGK200:-CASSORT        PIC X(05).                                
 134       02  FGK200:-CAPPRO         PIC X(05).                                
      *} Post-Translation                                                       
 139       02  FILLER                 PIC X(12).                                
 151 *** LRECL=150                                                              
                                                                                
