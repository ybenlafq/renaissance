      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************          
      *  DEFINITION DU FICHIER DES PROPOS DE MUT                                
      ****************************************************************          
       01  DSECT-FMU205.                                                        
           02 FMU205-TRI.                                                       
              03 FMU205-NSOCIETE            PIC X(03).                          
              03 FMU205-NLIEU               PIC X(03).                          
              03 FMU205-WSEQFAM             PIC S9(5) COMP-3.                   
              03 FMU205-LAGREGATED          PIC X(20).                          
              03 FMU205-TRI-PVTTC.                                              
                 05 FMU205-PVTTC1           PIC S9(7)V99 COMP-3.                
                 05 FMU205-RANG2            PIC S9(5) COMP-3.                   
              03 FMU205-TRI-RANG REDEFINES FMU205-TRI-PVTTC.                    
                 05 FMU205-RANG1            PIC S9(5) COMP-3.                   
                 05 FMU205-PVTTC2           PIC S9(7)V99 COMP-3.                
           02 FMU205-DONNEES-MUTATION.                                          
              03 FMU205-NMUTATION           PIC X(07).                          
              03 FMU205-DMUTATION           PIC X(08).                          
              03 FMU205-DFINSAIS            PIC X(08).                          
              03 FMU205-NSOCENTR            PIC X(03).                          
              03 FMU205-NDEPOT              PIC X(03).                          
           02 FMU205-DONNEES-ARTICLE.                                           
              03 FMU205-NCODIC              PIC X(07).                          
              03 FMU205-CFAM                PIC X(05).                          
              03 FMU205-CMARQ               PIC X(05).                          
              03 FMU205-LREFFOURN           PIC X(20).                          
              03 FMU205-LREFDARTY           PIC X(20).                          
              03 FMU205-CAPPRO              PIC X(05).                          
              03 FMU205-D1RECEPT            PIC X(08).                          
              03 FMU205-DCDE                PIC X(08).                          
              03 FMU205-WGROUPE             PIC X.                              
              03 FMU205-WELEM               PIC X.                              
              03 FMU205-WAFSO               PIC X.                              
              03 FMU205-WPENURIE            PIC X.                              
              03 FMU205-WSENS               PIC X.                              
              03 FMU205-LSTATCOMP           PIC X(3).                           
              03 FMU205-QCOLIDSTOCK         PIC S9(5) COMP-3.                   
              03 FMU205-FLAG-COLI           PIC X.                              
              03 FMU205-CMARKETING          PIC X(05) OCCURS 3.                 
              03 FMU205-CVDESCRIPT          PIC X(05) OCCURS 3.                 
           02 FMU205-DONNEES-VENTES.                                            
              03 FMU205-QPIECES             PIC S9(5) COMP-3  OCCURS 4.         
              03 FMU205-QPIECES8            PIC S9(7) COMP-3.                   
           02 FMU205-DONNEES-STOCK.                                             
              03 FMU205-QSTOCK-DEP          PIC S9(5) COMP-3.                   
              03 FMU205-QSTOCK-MAG          PIC S9(5) COMP-3.                   
              03 FMU205-DEXTRACTION         PIC X(8).                           
              03 FMU205-QSTOCK-PRE          PIC S9(5) COMP-3.                   
              03 FMU205-DATE-STO-PRE        PIC X(8).                           
              03 FMU205-FLAG-PRE            PIC X(1).                           
              03 FMU205-QMUTATT             PIC S9(5) COMP-3.                   
              03 FMU205-QSTOCK-MAX          PIC S9(5) COMP-3.                   
              03 FMU205-QSTOCK-MUT          PIC S9(5) COMP-3.                   
           02 FMU205-TRI-R-P                PIC X(1).                           
           02 FMU205-8020                   PIC X(1).                           
           02 FMU205-WSAUT                  PIC X(1).                           
           02 FMU205-TOP0                   PIC X(1).                           
           02 FMU205-SEQUENCE               PIC 9(1).                           
           02 FMU205-WREMP                  PIC X(1).                           
           02 FMU205-LIBRE                  PIC X(07).                          
                                                                                
