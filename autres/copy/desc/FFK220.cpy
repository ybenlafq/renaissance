      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * FFK220: PRODUITS  POUR GFK                                              
      ** LRECL=150                                                              
      ******************************************************************        
       01  FFK220-ENR.                                                          
   1       02  FFK220-NCODIC          PIC X(07).                                
   8       02  FFK220-CFAM            PIC X(05).                                
  13       02  FFK220-LFAM            PIC X(20).                                
  33       02  FFK220-LREFFOURN       PIC X(20).                                
  53       02  FFK220-LMARQ           PIC X(20).                                
  73       02  FFK220-NEAN            PIC X(13).                                
  86       02  FFK220-PVTOTALSR       PIC 9(14).                                
 100       02  FFK220-QVENDUE         PIC 9(10).                                
 110       02  FFK220-NSOCVTE         PIC X(03).                                
 113       02  FFK220-NLIEUVTE        PIC X(03).                                
 116       02  FFK220-PERIODE         PIC X(06).                                
 122       02  FILLER                 PIC X(29).                                
                                                                                
