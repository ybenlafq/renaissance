      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ************************************************************************  
      *                                                                      *  
      *   FICHIER DES TRANSACTIONS CAISSES 36 MAGASIN FILIALE                *  
      *                                                                      *  
      *               ORGANISATION   :  INDEXEE                              *  
      *               LONGUEUR ENREG :  630                                  *  
      *               LONGUEUR CLE   :  19                                   *  
      *               POSITION DEBUT :   1                                   *  
      *                                                                      *  
      ************************************************************************  
EURO  * 07-98 - OLIVIER BES - SI2 - EURO : TRAITEMENT HOST CAISSES 36  *     *  
      ************************************************************************  
      *  ZONE          !   PIC    !  POSITION !  INTITULE                    *  
      *----------------!----------!-----!-----!------------------------------*  
      *................!..........!...........! INFORMATION CLE TRANSACTION  *  
      * VE-AAQQQ       !  X(005)  !   1 �   5 ! ANNEE + QUANTIEME JOUR TRANS *  
      * VE-SOC         !  X(003)  !   6 �   8 ! NUM SOCIETE                  *  
      * VE-MAG         !  X(003)  !   9 �  11 ! NUM MAGASIN                  *  
      * VE-NUMCAIS     !  X(003)  !  12 �  14 ! NUM CAISSE                   *  
      * VE-TRANS       !  9(004)  !  15 �  18 ! NUM TRANSACTION VENTE        *  
      * VE-NUM         !  9(001)  !  19 �  19 ! NUM D'ENREGISTREMENT 1 � 9   *  
      *                !          !           !                              *  
      *----------------!----------!-----!-----!------------------------------*  
      * ENREGISTREMENT DES 3 PREMIERS CODIC + INFO TRANSACTION VE-NUM = 0    *  
      *----------------!----------!-----!-----!------------------------------*  
      *................!..........!...........! INFORMATION TYPE TRANSACTIO  *  
      * VE-TYPE        !  X(001)  !  20 �  20 ! TYPE TRANSACTION VENTE       *  
      *                !          !           ! '0' RENDU ELA                *  
      *                !          !           ! '1' VENTE EMPORTE ELA        *  
      *                !          !           ! '2' APPORT SUR RESERVATION   *  
      *                !          !           ! '3' APPORT SUR CDE ENREGISTRE*  
      *                !          !           ! '4' EMPORTE MAG AVEC BON CDE *  
      *                !          !           ! '5' LIVRE DEPOT AVEC BON CDE *  
      *                !          !           ! '6' EMPORTE DEPOT AVEC BON CDE  
      *                !          !           ! '7' RENDU EMPORTE MAG + BON CDE 
      *                !          !           ! 'B' APPORT EN CAISSE         *  
      *                !          !           ! 'C' RETRAIT                  *  
      *                !          !           ! 'D' DECOMPTE DE CAISSE       *  
      *                !          !           ! 'F' FOND DE CAISSE           *  
      *                !          !           ! 'A' ANNULANTE                *  
      * VE-ANUL        !  X(001)  !  21 �  21 ! ' ' OU 'A' SI ANNULEE        *  
      * VE-TERM        !  X(002)  !  22 �  23 ! IDENTITE TERMINAL            *  
      * VE-DATTRANS    !  X(006)  !  24 �  29 ! DATE  TRANSACTION AAMMJJ     *  
      * VE-HEURTRANS !    X(004)  !  30 �  33 ! HEURE TRANSACTION HHMN       *  
      *                !          !           !                              *  
      *----------------!----------!-----!-----!------------------------------*  
      *  PARTIE ENREGISTREMENT DE TRANSACTION DE VENTE  0,1,2,3,4,5,6,7,A    *  
      *----------------!----------!-----!-----!------------------------------*  
      *................!..........!...........! INFORMATION ENTETE TRANSACTIO*  
      * VE-CODOPE      !  X(004)  !  34 �  37 ! CODE OPERATRICE              *  
      * VE-LIBOPE      !  X(014)  !  38 �  51 ! NOM  OPERATRICE              *  
      * VE-CAISSE      !  X(003)  !  52 �  54 ! NUM CAISSE                   *  
      * VE-DJOUR       !  X(008)  !  55 �  62 ! JOUR "JJ/MM/AA"              *  
      * VE-HEURE       !  X(005)  !  63 �  67 ! HEURE "HH:MN"                *  
      * VE-FILLER      !  X(005)  !  68 �  72 ! N� TRANSACTION               *  
      * VE-VEND        !  X(004)  !  73 �  76 ! VENDEUR TRANSACTION          *  
      * VE-VENDO       !  X(004)  !  77 �  80 ! VENDEUR ORIGINE POUR TYPE '0'*  
      * VE-BCOM        !  X(009)  !  81 �  89 ! NUM BON COMMANDE MAG+ NUM    *  
      *                !          !           !                              *  
      *................!..........!...........! INFORMATION DETAIL TRANSACTIO*  
      *                !          !           ! 03 OCCURENCES DE 47 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-VEN         !  X(004)  !  90 �  93 ! VENDEUR LIGNE TRANS          *  
      * VE-CODIC       !  X(007)  !  94 � 100 ! CODIC   LIGNE TRANS          *  
      * VE-RAYON       !  X(002)  ! 101 � 102 ! RAYON   51 = ELA             *  
      *                !          !           ! RAYON   52 = TLM             *  
      *                !          !           ! RAYON   54 = ACCESSOIRES     *  
      *                !          !           ! RAYON   59 = PSE             *  
      *                !          !           ! RAYON  53,55 LIBRE SERVICE   *  
      *                !          !           ! RAYON 56,57,58,60 � 65 LIBRES*  
      * VE-QTE         !  9(003)  ! 103 � 105 ! QUANTITE                     *  
      * VE-REM         !  9(002)  ! 106 � 107 ! % REMISE                     *  
      * VE-PSE         !  X(001)  ! 108 � 108 ! ' ' OU '1' SI PSE            *  
      * VE-FORCE       !  X(001)  ! 109 � 109 ! ' ' OU '1' SI PRIX FORCE     *  
      * VE-PUF         !PS9(7)V99 ! 110 � 114 ! PRIX FICHIER PRIX MAGASIN    *  
      * VE-RABAIS      !PS9(7)V99 ! 115 � 119 ! MONTANT RABAIS               *  
      * VE-PU          !PS9(7)V99 ! 120 � 124 ! PRIX VENTE                   *  
      * VE-QTE-PU      !PS9(7)V99 ! 125 � 129 ! PRIX VENTE X QTE             *  
      * VE-NET         !PS9(7)V99 ! 130 � 134 ! QTE-PU     -(REMISE + RABAIS)*  
      * VE-CTVA        !  X(001)  ! 135 � 135 ! CODE TVA                     *  
      * VE-STOCK       !  X(001)  ! 136 � 136 ! '1' SI M�J STOCK PM0103A     *  
      *                !          !  90 � 230 !                              *  
      *................!..........!...........! INFORMATION DETAIL TRANSACTIO*  
      *                !          !           ! 03 OCCURENCES DE 24 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-MREM        !PS9(7)V99 ! 231 � 235 ! MONTANT REMISE               *  
      * VE-REF         !  X(019)  ! 236 � 254 ! REFERENCE CODIC              *  
      *                !          ! 231 � 302 !                              *  
      *................!..........!...........! INFORMATION MODE PAIEMENT    *  
      *                !          !           ! 06 OCCURENCES DE 1  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-P-MODP      !  X(001)  ! 303 � 303 ! MODE DE PAIEMENT             *  
      *                !          ! 303 � 308 !                              *  
      *................!..........!...........! INFORMATION MODE PAIEMENT    *  
      *                !          !           ! 06 OCCURENCES DE 5  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-P-MONP      !PS9(7)V99 ! 309 � 313 ! MONTANT PAR MODE PAIEMENT    *  
      *                !          ! 309 � 338 !                              *  
      * VE-GAR         !  X(001)  ! 339 � 339 ! EDITION AVEC BON GARANTIE    *  
      *                !          !           !                              *  
      *................!..........!...........! INFORMATION TOTAUX TRANS     *  
      * VE-TOTAL       !PS9(7)V99 ! 340 � 344 ! MONTANT TOTAL TRANSACTION    *  
      * VE-ENC         !PS9(7)V99 ! 345 � 349 ! MONTANT � ENCAISSER          *  
      *                !          !           ! 06 OCCURENCES DE 1  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-MODP        !  X(001)  ! 350 � 350 ! MODE DE PAIEMENT SAISI       *  
      *                !          ! 350 � 355 !                              *  
      *                !          !           ! 06 OCCURENCES DE 5  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-MONP        !PS9(7)V99 ! 356 � 360 ! MONTANT MODE PAIEMENT SAISI  *  
      *                !          !           ! SI REPRISE MONTANT AVOIR     *  
      *                !          !           !    DANS VE-MONP(1)           *  
      *                !          ! 360 � 385 !                              *  
      *                !          !           ! 06 OCCURENCES DE 6  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-ATRP        !  X(006)  ! 386 � 391 ! ATTRIBUT DE PAIEMENT SAISI   *  
      *                !          !           ! SI REPRISE NUMERO  AVOIR     *  
      *                !          !           !    DANS VE-ATRP(1)           *  
      *                !          !           ! SI MODP=2 DATE CHEQUE DIFFERE*  
      *                !          !           ! SI MODP=5 NUM AVOIR          *  
      *                !          ! 386 � 421 !                              *  
      *                !          !           ! 05 OCCURENCES DE 1  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-TYPV        !  X(005)  ! 422 � 422 ! TYPE VENTILATION PAIEMENT    *  
      *                !          !           ! 'W' RACHAT PSE               *  
      *                !          !           ! 'X' � LIVRAISON              *  
      *                !          !           !  X CODE CREDIT(TABLE PARAM)  *  
      *                !          !           ! 'Y' � RECEPTION FACTURE      *  
      *                !          !           ! 'Z' DEJ� REGLE               *  
      *                !          ! 422 � 426 !                              *  
      *                !          !           ! 05 OCCURENCES DE 5  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-MONV        !PS9(7)V99 ! 427 � 431 ! MONTANT VENTILATION PAIEMENT *  
      *                !          ! 427 � 451 !                              *  
      * VE-RENDUE      !PS9(7)V99 ! 452 � 456 ! MONAIE RENDUE                *  
      * VE-RDU         !PS9(7)V99 ! 457 � 461 ! RESTE DU                     *  
      *                !          !           !                              *  
      *................!..........!...........!                              *  
      * VE-L           !  9(002)  ! 462 � 463 ! NBRE LIGNE CODIC             *  
      * VE-EMP         !  9(008)  ! 464 � 471 ! NUM EMPORTE (MAG+QQQ+CPT)    *  
      *                !          !           !                              *  
      *................!..........!...........! INFORMATION ANNULATION       *  
      * VE-ANU-TRANS   !  X(004)  ! 472 � 475 ! NUM TRANS ANNULE OU ANNULANTE*  
      *................!..........!...........! INFORMATION TRANS ANNULEE    *  
      * VE-ANU-MAG     !  X(003)  ! 476 � 478 ! NUM MAG                      *  
      * VE-ANU-TERM    !  X(003)  ! 479 � 481 ! NUM CAISSE                   *  
      * VE-ANU-NUMCTR  !  X(002)  ! 482 � 483 ! NUM CONTROLEUR               *  
      * VE-ANU-MONTANT !PS9(7)V99 ! 484 � 488 ! MONTANT ANNULE               *  
      *                !          !           !                              *  
      *................!..........!...........!                              *  
      *                !          !           ! 03 OCCURENCES DE 1  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-TOP         !  X(001)  ! 489 � 489 ! TOP DE MISE � JOUR SIEGE     *  
      *                !          ! 489 � 491 !                              *  
      *................!..........!...........!                              *  
      *                !          !           ! 03 OCCURENCES DE 10 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-PRIME       !PS9(7)V99 ! 492 � 496 ! PRIME UNITAIRE               *  
      * VE-SURPRIME    !PS9(7)V99 ! 497 � 501 ! SUPRIME +THEOPRIME UNITAIRE  *  
      *                !          ! 492 � 521 !                              *  
      *................!..........!...........!                              *  
      * VE-NUMFACT     !  9(006)  ! 522 � 527 ! NUM FACTURE MAGASIN          *  
      *................!..........!...........! INFO RENDU MONAIE/MODE PAIE  *  
      *                !          !           ! 06 OCCURENCES DE 3  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-RENDMONAIE  !PS9(3)V99 ! 528 � 531 ! RENDU PAR MODE PAIEMENT SAISI*  
      *                !          ! 528 � 545 !                              *  
      *                !          !           !                              *  
EURO  * OB - SI2 -  07 1998 : EURO - TRAITEMENT HOST CAISSES                 *  
 "    *      SUPPRESSION DE CODICGRP          !                              *  
 "    *                !          !           ! 10 OCCURENCES DE 7  CARACT.  *  
 "    * VE-CODICGRP    !  X(007)  ! 546 � 552 ! CODIC GROUPE                 *  
 "    *                !          ! 546 � 615 !                              *  
 "    * OB - SI2 -  07 1998 : EURO - TRAITEMENT HOST CAISSES                 *  
 "    * OB - SI2 -  09 1998 : EURO - NOUVELLE STRUCTURE / CREDIT EURO        *  
 "    *      AJOUT DE CCAISREF    !           !                              *  
 "    *               CCAISAUT    !           !                              *  
 "    *               MONPAUT     !           !                              *  
 "    *               EC-CONV     !           !                              *  
 "    *               EC-RENDU    !           !                              *  
 "    *               CODE-REF    !           !                              *  
 "    *                !          !           ! 07 OCCURENCES DE  1 CARACT.  *  
 "    * VE-CCAISREF    !  X(001)  ! 546 � 546 ! CODE DEVISE REFERENCE        *  
 "    *                !          ! 546 � 552 !                              *  
 "    *                !          !           ! 07 OCCURENCES DE  1 CARACT.  *  
 "    * VE-CCAISAUT    !  X(001)  ! 553 � 553 ! CODE AUTRE DEVISE            *  
 "    *                !          ! 553 � 559 !                              *  
 "    *                !          !           ! 07 OCCURENCES DE  5 CARACT.  *  
 "    * VE-MONPAUT     !PS9(7)V99 ! 560 � 564 ! MONTANT MODE PAIEMENT AUTRE  *  
 "    *                !          ! 560 � 594 ! DEVISE                       *  
 "    *                !          !           ! 07 OCCURENCES DE  2 CARACT.  *  
 "    * VE-EC-CONV     !PS9(1)V99 ! 595 � 596 ! ECART CONVERSION AUTRE DEVISE*  
 "    *                !          ! 595 � 608 !                              *  
 "    * VE-EC-RENDU    !PS9(1)V99 ! 609 � 610 ! ECART RENDU-MONNAIE DEV AUTRE*  
 "    * VE-CODE-REF    !  X(001)  ! 611 � 611 ! CODE CAISSE DE REFERENCE     *  
 "    * FILLER         !  X(004)  ! 612 � 615 !                              *  
 "    * OB - SI2 -  07 1998 : EURO - TRAITEMENT HOST CAISSES                 *  
 "    *      FIN MODIFICATION 1   !           !                              *  
      *                !          !           !                              *  
      * VE-RESA1       !  X(005)  ! 616 � 620 !                              *  
      * VE-RESA2       !  X(005)  ! 621 � 625 !                              *  
      * VE-RESA3       !  X(005)  ! 626 � 630 !                              *  
      *----------------!----------!-----!-----!------------------------------*  
      *  PARTIE ENREGISTREMENT DES 7 AUTRES CODIC  VE-NUM = 1                *  
      *----------------!----------!-----!-----!------------------------------*  
      *................!..........!...........! INFORMATION DETAIL TRANSACTIO*  
      *                !          !           ! 07 OCCURENCES DE 47 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-1-VEN       !  X(004)  !  21 �  24 ! VENDEUR LIGNE TRANS          *  
      * VE-1-CODIC     !  X(007)  !  25 �  31 ! CODIC   LIGNE TRANS          *  
      * VE-1-RAYON     !  X(002)  !  32 �  33 ! RAYON                        *  
      * VE-1-QTE       !  9(003)  !  34 �  36 ! QUANTITE                     *  
      * VE-1-REM       !  9(002)  !  37 �  38 ! % REMISE                     *  
      * VE-1-PSE       !  X(001)  !  39 �  39 ! ' ' OU '1' SI PSE            *  
      * VE-1-FORCE     !  X(001)  !  40 �  40 ! ' ' OU '1' SI PRIX FORCE     *  
      * VE-1-PUF       !PS9(7)V99 !  41 �  45 ! PRIX FICHIER PRIX MAGASIN    *  
      * VE-1-RABAIS    !PS9(7)V99 !  46 �  50 ! MONTANT RABAIS               *  
      * VE-1-PU        !PS9(7)V99 !  51 �  55 ! PRIX VENTE                   *  
      * VE-1-QTE-PU    !PS9(7)V99 !  56 �  60 ! PRIX VENTE X QTE             *  
      * VE-1-NET       !PS9(7)V99 !  61 �  65 ! QTE-PU     -(REMISE + RABAIS)*  
      * VE-1-CTVA      !  X(001)  !  66 �  66 ! CODE TVA                     *  
      * VE-1-STOCK     !  X(001)  !  67 �  67 ! '1' SI M�J STOCK PM0103A     *  
      *                !          !  21 � 349 !                              *  
      *................!..........!...........! INFORMATION DETAIL TRANSACTIO*  
      *                !          !           ! 07 OCCURENCES DE 24 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-1-MREM      !PS9(7)V99 ! 350 � 354 ! MONTANT REMISE               *  
      * VE-1-REF       !  X(019)  ! 355 � 373 ! REFERENCE CODIC              *  
      *                !          ! 350 � 517 !                              *  
      *................!..........!...........!                              *  
      *                !          !           ! 07 OCCURENCES DE 1  CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-1-TOP       !  X(001)  ! 518 � 518 ! TOP DE MISE � JOUR SIEGE     *  
      *                !          ! 518 � 524 !                              *  
      *................!..........!...........!                              *  
      *                !          !           ! 07 OCCURENCES DE 10 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-1-PRIME     !PS9(7)V99 ! 525 � 529 ! PRIME UNITAIRE               *  
      * VE-1-SURPRIME!PS9(7)V99 ! 530 � 534 ! SUPRIME +THEOPRIME UNITAIRE    *  
      *                !          ! 525 � 594 !                              *  
      * FILLER         !  X(036)  ! 595 � 630 !                              *  
      *................!..........!...........!                              *  
      *----------------!----------!-----!-----!------------------------------*  
      *  PARTIE ENREGISTREMENT DE TRANSACTION ADMINISTRATIVE B,C,D,F A       *  
      *   VE-NUM = 0                                                            
      *----------------!----------!-----!-----!------------------------------*  
      * VE-FOND        !PS9(7)V99 !  34 �  38 ! FOND DE CAISSE               *  
      * VE-APPORT      !PS9(7)V99 !  39 �  43 ! APPORT EN CAISSE             *  
      * VE-RET         !PS9(7)V99 !  44 �  48 ! RETRAIT DE CAISSE            *  
      *................!..........!...........!                              *  
      *                !          !           ! 16 OCCURENCES DE 05 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-DEC         !PS9(7)V99 !  49 �  53 ! MONTANT DECOMPTE PAR MODE PAIE  
      *                !          !  49 � 148 !                              *  
      *                !          !           ! 20 OCCURENCES DE 01 CARACT.  *  
      *                !          !           ! POSITION PREMIERE OCCURENCE  *  
      * VE-MODP-D      !  X(001)  ! 149 � 149 ! MODE PAIEMENT DECOMPTE       *  
      *                !          ! 149 � 168 !                              *  
      *                !          !           ! '0' ESPECES                  *  
      *                !          !           ! '1' CHEQUES BANCAIRES        *  
      *                !          !           ! '2' CHEQUES DIFFERE          *  
      *                !          !           ! '3' CARTES BANCAIRES         *  
      *                !          !           ! '4' BONS D'ACHAT             *  
      *                !          !           ! '5' AVOIRS                   *  
      *                !          !           ! '9' CCP                      *  
      *                !          !           !  X  AUTRES MODES DE PAIEMENT *  
      *................!..........!...........!                              *  
      * VE-CDAPPRET    !  9(002)  ! 169 � 170 ! CODE APPORT OU RETRAIT       *  
      * VE-ADM1        !  X(090)  ! 171 � 260 ! ZONE ENTETE CS0020A1 ADMINIST*  
      * VE-ADM2        !  X(090)  ! 261 � 350 ! ZONE DETAIL CS0020A1 ADMINIST*  
      *                !          !           !                              *  
EURO  * OB - SI2 - MAI 1998 : EURO - TRAITEMENT HOST CAISSES                 *  
 "    *      AJOUT DE DEVISE      !           !                              *  
 "    *      REDUCTION DE ADM-ECRAN (276 AU LIEU DE 279)                     *  
 "    * VE-DEVISE      !  X(003)  ! 351 � 353 ! DEVISE/APPORT/RETRAIT/DECOMPT*  
 "    * VE-ADM-ECRAN   !  X(276)  ! 354 � 629 ! ZONE AFFICHAGE ECRAN ADMINIST*  
 "    * OB - SI2 - MAI 1998 : EURO - TRAITEMENT HOST CAISSES                 *  
 "    *      FIN MODIFICATION 2   !           !                              *  
      * VE-ADM-SUITE !    X(001)  ! 630 � 630 ! ENREG SUIVANT O = OUI/N = NON*  
      *----------------!----------!-----!-----!------------------------------*  
      *  PARTIE ENREGISTREMENT DE TRANSACTION ADMINISTRATIVE B,C,D,F A       *  
      *   VE-NUM = 1                                                            
      *----------------!----------!-----!-----!------------------------------*  
      * VE-ADM-ECRAN1!    X(610)  !  21 � 630 ! ZONE AFFICHAGE ECRAN ADMINIST*  
      ************************************************************************  
       01             VE-ARTICLE.                                               
           02         VE-CLE.                                                   
              04      VE-AAQQQ.                                                 
                  05  VE-AA                      PIC 9(02).                     
                  05  VE-QQQ                     PIC 9(03).                     
              04      VE-SOC                     PIC X(03).                     
              04      VE-MAG                     PIC X(03).                     
              04      VE-NUMCAIS                 PIC X(03).                     
              04      VE-TRANS                   PIC 9(04).                     
              04      VE-NUM                     PIC 9(01).                     
           02         VE-TYPE                    PIC X(01).                     
           02         VE-ZONE0.                                                 
           03         VE-ZONE1.                                                 
              04      VE-ANUL                    PIC X(01).                     
              04      VE-IDENT.                                                 
                  07  VE-TERM                    PIC X(02).                     
                  07  VE-DATTRANS                PIC X(06).                     
                  07  VE-HEURTRANS               PIC X(04).                     
           03         VE-ZONE2.                                                 
              04      VE-ENTETE.                                                
                  07  VE-CODOPE                  PIC X(04).                     
                  07  VE-LIBOPE                  PIC X(14).                     
                  07  VE-CAISSE                  PIC X(03).                     
                  07  VE-DJOUR                   PIC X(08).                     
                  07  VE-HEURE                   PIC X(05).                     
                  07  VE-FILLER                  PIC X(05).                     
                  07  VE-VEND                    PIC X(04).                     
                  07  VE-VENDO                   PIC X(04).                     
                  07  VE-BCOM                    PIC X(09).                     
             04       VE-LDT.                                                   
               05     VE-LD          OCCURS 03.                                 
                  07  VE-VEN                     PIC X(04).                     
                  07  VE-CODIC                   PIC X(07).                     
                  07  VE-RAYON                   PIC X(02).                     
                  07  VE-QTE                     PIC 9(03).                     
                  07  VE-REM                     PIC 9(02).                     
                  07  VE-PSE                     PIC X(01).                     
                  07  VE-FORCE                   PIC X(01).                     
                  07  VE-PUF                     PIC S9(7)V99 COMP-3.           
                  07  VE-RABAIS                  PIC S9(7)V99 COMP-3.           
                  07  VE-PU                      PIC S9(7)V99 COMP-3.           
                  07  VE-QTE-PU                  PIC S9(7)V99 COMP-3.           
                  07  VE-NET                     PIC S9(7)V99 COMP-3.           
                  07  VE-CTVA                    PIC X(01).                     
                  07  VE-STOCK                   PIC X(01).                     
             04       VE-TABLE2.                                                
               05     VE-LIGNE2      OCCURS 03.                                 
                  07  VE-MREM                    PIC S9(7)V99 COMP-3.           
                  07  VE-REF                     PIC X(19).                     
             04       VE-P-MODP-T.                                              
                  07  VE-P-MODP      OCCURS 06   PIC X.                         
             04       VE-P-MONP-T.                                              
                  07  VE-P-MONP      OCCURS 06   PIC S9(7)V99 COMP-3.           
             04       VE-GAR                     PIC X(001).                    
             04       VE-TOTAUX.                                                
                  07  VE-TOTAL                   PIC S9(7)V99 COMP-3.           
                  07  VE-ENC                     PIC S9(7)V99 COMP-3.           
                  07  VE-MODP        OCCURS 06   PIC X.                         
                  07  VE-MONP        OCCURS 06   PIC S9(7)V99 COMP-3.           
                  07  VE-ATRP        OCCURS 06.                                 
                      08  VE-ATRPAK              PIC S9(11)   COMP-3.           
                  07  VE-TYPV        OCCURS 05   PIC X.                         
                  07  VE-MONV        OCCURS 05   PIC S9(7)V99 COMP-3.           
                  07  VE-RENDUE                  PIC S9(7)V99 COMP-3.           
                  07  VE-RDU                     PIC S9(7)V99 COMP-3.           
             04       VE-L                       PIC 9(02).                     
             04       VE-EMP.                                                   
                  07  VE-EMP-MAG                 PIC 9(02).                     
                  07  VE-EMP-QQQ                 PIC 9(03).                     
                  07  VE-EMP-CPT                 PIC 9(03).                     
             04       VE-ANU.                                                   
                  07  VE-ANU-TRANS               PIC X(04).                     
                  07  VE-ANU-MAG                 PIC X(03).                     
                  07  VE-ANU-TERM                PIC X(03).                     
                  07  VE-ANU-NUMCTR              PIC X(02).                     
                  07  VE-ANU-MONTANT             PIC S9(7)V99 COMP-3.           
             04       VE-TOPS.                                                  
                  07  VE-TOP         OCCURS 03   PIC X(01).                     
             04       VE-PRIME-T.                                               
               05     VE-PRIME-TC    OCCURS 03.                                 
                  07  VE-PRIME                   PIC S9(7)V99 COMP-3.           
                  07  VE-SURPRIME                PIC S9(7)V99 COMP-3.           
             04       VE-NUMER-FACT.                                            
                  07  VE-NUMFACT                 PIC 9(06).                     
             04       VE-REND-MONAIE.                                           
                  07  VE-RENDMONAIE OCCURS 06   PIC S9(3)V99 COMP-3.            
EURO  *      04       VE-CODIC-GRP.                                             
 "    *           07  VE-CODICGRP   OCCURS 10   PIC X(007).                     
EURO2        04       VE-CCAISREF   OCCURS 07   PIC X(001).                     
 "           04       VE-CCAISAUT   OCCURS 07   PIC X(001).                     
 "           04       VE-MONPAUT    OCCURS 07   PIC S9(7)V99 COMP-3.            
 "           04       VE-EC-CONV    OCCURS 07   PIC S9V99    COMP-3.            
 "           04       VE-EC-RENDU               PIC S9V99    COMP-3.            
 "           04       VE-CODE-REF               PIC X.                          
 "    *      04       FILLER                    PIC X(028).                     
 "           04       FILLER                    PIC X(004).                     
             04       VE-RESA-T.                                                
                  07  VE-RESA        OCCURS 03   PIC X(05).                     
           03         VE-ADMINISTRATIF REDEFINES      VE-ZONE2.                 
             04       VE-ADM-COMPTEURS.                                         
                  07  VE-FOND                    PIC S9(7)V99 COMP-3.           
                  07  VE-APPORT                  PIC S9(7)V99 COMP-3.           
                  07  VE-RET                     PIC S9(7)V99 COMP-3.           
                  07  VE-DEC         OCCURS 20   PIC S9(7)V99 COMP-3.           
             04       VE-ADM-MODPAIE.                                           
                  07  VE-MODP-D      OCCURS 20   PIC X(001).                    
             04       VE-CDAPPRET                PIC X(002).                    
             04       VE-ADM1                   PIC X(090).                     
             04       VE-ADM2                   PIC X(090).                     
EURO         04       VE-DEVISE                 PIC X(003).                     
 "           04       VE-DEC-AUT-X    OCCURS 20.                                
 "                07  VE-DEC-AUT                PIC S9(7)V99 COMP-3.            
 "    *      04       VE-ADM-ECRAN              PIC X(279).                     
 "           04       VE-ADM-ECRAN              PIC X(176).                     
             04       VE-ADM-SUITE              PIC X(001).                     
      *  PARTIE ENREGISTREMENT DES 7 AUTRES CODIC  VE-NUM = 1                *  
           02         VE-1-ZONE2         REDEFINES    VE-ZONE0.                 
             04       VE-1-LDT.                                                 
               05     VE-1-LD          OCCURS 07.                               
                  07  VE-1-VEN                     PIC X(04).                   
                  07  VE-1-CODIC                   PIC X(07).                   
                  07  VE-1-RAYON                   PIC X(02).                   
                  07  VE-1-QTE                     PIC 9(03).                   
                  07  VE-1-REM                     PIC 9(02).                   
                  07  VE-1-PSE                     PIC X(01).                   
                  07  VE-1-FORCE                   PIC X(01).                   
                  07  VE-1-PUF                     PIC S9(7)V99 COMP-3.         
                  07  VE-1-RABAIS                  PIC S9(7)V99 COMP-3.         
                  07  VE-1-PU                      PIC S9(7)V99 COMP-3.         
                  07  VE-1-QTE-PU                  PIC S9(7)V99 COMP-3.         
                  07  VE-1-NET                     PIC S9(7)V99 COMP-3.         
                  07  VE-1-CTVA                    PIC X(01).                   
                  07  VE-1-STOCK                   PIC X(01).                   
             04       VE-1-TABLE2.                                              
               05     VE-1-LIGNE2      OCCURS 07.                               
                  07  VE-1-MREM                    PIC S9(7)V99 COMP-3.         
                  07  VE-1-REF                     PIC X(19).                   
             04       VE-1-TOPS.                                                
                  07  VE-1-TOP         OCCURS 07   PIC X(01).                   
             04       VE-1-PRIME-T.                                             
               05     VE-1-PRIME-TC    OCCURS 07.                               
                  07  VE-1-PRIME                   PIC S9(7)V99 COMP-3.         
                  07  VE-1-SURPRIME                PIC S9(7)V99 COMP-3.         
             04       FILLER                     PIC X(36).                     
      *  PARTIE ENREGISTREMENT DE TRANSACTION ADMINISTRATIVE VE-NUM = 1      *  
           02         VE-1-ADMINISTRATIF     REDEFINES    VE-ZONE0.             
             04       VE-1-ADM-ECRAN             PIC X(610).                    
                                                                                
