      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : PRODUITS COMPLET        * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK020                                        * 00100600
      *        - LONGUEUR ENREG = 113 ( <- 290)                       * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      *  AJUSTEMENT LONGUEUR DES CHAMPS POUR ECONOMIE VOLUMES         *         
      ***************************************************************** 00101400
      *                                                                         
       01  FGK020-ENR.                                                          
           05  FGK020-CODE-FAM      PIC X(05).                                  
           05  FGK020-LIB-FAM       PIC X(20).                                  
           05  FGK020-CODE-MARK     PIC X(05).                                  
           05  FGK020-LIB-MARK      PIC X(20).                                  
           05  FGK020-CODICTVA.                                                 
               10  FGK020-NCODIC    PIC X(07).                                  
               10  FGK020-CTAUXTVA  PIC X(01).                                  
           05  FGK020-REF-COMM      PIC X(20).                                  
           05  FGK020-CODE-MARQ     PIC X(05).                                  
           05  FGK020-LIB-MARQ      PIC X(20).                                  
           05  FGK020-DATE-CRE      PIC X(10).                                  
      *                                                                         
                                                                                
