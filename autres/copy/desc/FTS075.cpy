      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * DEFINITION DU FICHIER DE REMONTEE DES FACTURES LOCALES                  
      *****************************************************************         
       01 DSECT-FTS075.                                                         
          02 FTS075-DONNEES.                                                    
              05  FTS075-NSOC           PIC  X(03).                             
              05  FTS075-NLIEU          PIC  X(03).                             
              05  FTS075-NFACT          PIC  X(07).                             
              05  FTS075-DFACT          PIC  X(08).                             
              05  FTS075-HFACT          PIC  9(06).                             
              05  FTS075-CODOPE         PIC  X(07).                             
              05  FTS075-PFACRF         PIC S9(07)V9(02) COMP-3.                
              05  FTS075-CDEVRF         PIC  X(03).                             
          02 FTS075-LIBRE               PIC  X(38).                             
                                                                                
