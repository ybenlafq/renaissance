      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * DESCRIPTION DU FICHIER CONSTITUE PAR LE FMD200.                *        
      * EXTRACTION DES PRESTATIONS LOCALES SUR LE MOIS EN COURS ET LE  *        
      * MOIS PRéCéDENT.                                                *        
      * -------------------------------------------------------------- *        
       01  DSECT-FMD200.                                                        
           05  FMD200-NLIEUDEPLIV     PIC X(03).                                
FVM        05  FILLER                 PIC X(6).                                 
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-NLIEU           PIC X(03).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-NVENTE          PIC X(07).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-CMARQ           PIC X(07).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-CFAM            PIC X(05).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-LREFFOURN       PIC X(20).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-NCODIC          PIC X(07).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-QVENDUE         PIC 9(3) .                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
FVM        05  FILLER                 PIC XX    VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-PVTOTAL         PIC +9(7),9(0002).                        
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-CMODDEL         PIC X(03).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
FVM        05  FILLER                 PIC XX    VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FMD200-DDELIV          PIC X(08).                                
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ' '.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
FVM        05  FILLER                 PIC X(11) VALUE ' '.                      
FVM   *    05  FILLER                 PIC X(21) VALUE SPACES.                   
                                                                                
