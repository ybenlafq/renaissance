      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * FFP000 : FICHIER CREE PAR BFP000                               *        
      * FACTURES A REGLER PAR PACIFICA  ET                             *        
      * FICHIER DES FACTURES REJETEES PAR PACIFICA                     *        
      * LONGUEUR = 600                                                 *        
      ******************************************************************        
      *                                                                         
      *--------------ENREGISTREMENT ENTETE = '001'                              
      *                                                                         
       01 ENR-FFP000.                                                           
          03 ENR-FFP000-001.                                                    
      *- '001'                                                                  
           05 FFP000-TYPENR-001    PIC  X(03).                                  
030215     05 FFP000-NUMSEQ-001    PIC  X(06).                                  
      *- 'DARTY'                                                                
           05 FFP000-CEMETTEUR     PIC  X(09).                                  
      *- 'FACTEMIS'                                                             
030215     05 FFP000-IDENT-FIC     PIC  X(08).                                  
      *- 'VERSION=2'                                                            
030215     05 FFP000-VERSION       PIC  X(01).                                  
      *-DATE ET HEURE EMISSION DU LOT ISSUS DATE SYSTEME                        
           05 FFP000-DEMISS        PIC  X(08).                                  
      *- 'PACIFICA'                                                             
           05 FFP000-DESTINATAIRE  PIC  X(09).                                  
           05 FFP000-NLOT          PIC  9(06).                                  
      *- 'EUR'                                                                  
           05 FFP000-CDEVISE       PIC  X(03).                                  
      *-                                                                        
           05 FILLER               PIC  X(088).                                 
      *-                                                                        
           05 FFP000-C-MOTIF-REJET-1 PIC  X(03).                                
      *-                                                                        
           05 FFP000-MOTIF-REJET-1   PIC  X(120).                               
      *-                                                                        
           05 FILLER               PIC  X(136).                                 
      *                                                                         
      *------ENREGISTREMENT FACTURE 002                                         
      *                                                                         
           03 ENR-FFP000-002 REDEFINES  ENR-FFP000-001.                         
      *- '002'                                                                  
              05 FFP000-TYPENR-002    PIC  X(03).                               
030215        05 FFP000-NUMSEQ-002    PIC  X(06).                               
              05 FFP000-NFACTURE-002  PIC  X(15).                               
              05 FFP000-NSINISTRE     PIC  X(10).                               
              05 FFP000-NOM           PIC  X(32).                               
              05 FFP000-DFACTURE      PIC  X(08).                               
              05 FFP000-STAT-DFACTURE PIC  X(01).                               
      *--  'DARTY'                                                              
              05 FFP000-NOM-EXP       PIC  X(17).                               
      *--  'PACIFICA'                                                           
              05 FFP000-NOM-DEST      PIC  X(17).                               
              05 FILLER               PIC  X(28).                               
              05 FFP000-CODE-ETAT     PIC  X(04).                               
      *-                                                                        
              05 FFP000-C-MOTIF-REJET-2 PIC  X(03).                             
      *-                                                                        
              05 FFP000-MOTIF-REJET-2   PIC  X(120).                            
              05 FFP000-NUM-SEQ-REJET-2 PIC  X(06).                             
              05 FILLER                 PIC  X(107).                            
              05 FFP000-CD-PART-RB-2    PIC  X(03).                             
              05 FILLER               PIC  X(20).                               
           03 ENR-FFP000-004 REDEFINES  ENR-FFP000-001.                         
      *- '004'                                                                  
              05 FFP000-TYPENR-004      PIC  X(03).                             
030215        05 FFP000-NUMSEQ-004      PIC  X(06).                             
              05 FFP000-NFACTURE-004    PIC  X(15).                             
              05 FFP000-NOM-PREST       PIC  X(06).                             
              05 FILLER                 PIC  X(90).                             
              05 FFP000-DATE-PREST      PIC  X(08).                             
      *-  CALCUL                                                                
              05 FFP000-HT-PREST        PIC 9(07)V99.                           
              05 FILLER                 PIC X(04).                              
      *-                                                                        
              05 FFP000-C-MOTIF-REJET-4 PIC  X(03).                             
      *-                                                                        
              05 FFP000-MOTIF-REJET-4   PIC  X(120).                            
              05 FFP000-NUM-SEQ-REJET-4 PIC  X(06).                             
              05 FILLER                 PIC  X(107).                            
              05 FFP000-CD-PART-RB-4    PIC  X(03).                             
              05 FILLER                 PIC  X(20).                             
           03 ENR-FFP000-006 REDEFINES  ENR-FFP000-001.                         
      *- '006'                                                                  
              05 FFP000-TYPENR-006      PIC  X(03).                             
030215        05 FFP000-NUMSEQ-006      PIC  X(06).                             
              05 FFP000-NFACTURE-006    PIC  X(15).                             
              05 FFP000-NATURE          PIC  X(03).                             
              05 FFP000-MARQUE          PIC  X(20).                             
              05 FFP000-FOUR-CODIC      PIC  X(15).                             
              05 FFP000-CODIC           PIC  X(15).                             
              05 FFP000-DATE-RA9        PIC  X(08).                             
              05 FFP000-HT-RA9          PIC  9(07)V99.                          
              05 FILLER                 PIC  X(47).                             
              05 FFP000-C-MOTIF-REJET-6 PIC  X(03).                             
              05 FFP000-MOTIF-REJET-6   PIC  X(120).                            
              05 FFP000-NUM-SEQ-REJET-6 PIC  X(06).                             
              05 FILLER                 PIC  X(107).                            
              05 FFP000-CD-PART-RB-6    PIC  X(03).                             
              05 FILLER                 PIC  X(20).                             
           03 ENR-FFP000-007 REDEFINES  ENR-FFP000-001.                         
      *- '007'                                                                  
              05 FFP000-TYPENR-007      PIC  X(03).                             
030215        05 FFP000-NUMSEQ-007      PIC  X(06).                             
              05 FFP000-NFACTURE-007    PIC  X(15).                             
              05 FFP000-TAUX-TVA        PIC  9(03)V99.                          
              05 FFP000-TOTAL-HT        PIC  9(07)V99.                          
              05 FFP000-TOTAL-TVA       PIC  9(07)V99.                          
              05 FFP000-MTFRANCHISE     PIC  9(07)V99.                          
              05 FFP000-WFRANCHISE      PIC  X(01).                             
              05 FFP000-TOTAL-TTC       PIC  9(07)V99.                          
              05 FFP000-MTCLIENT        PIC  9(07)V99.                          
              05 FFP000-REGLEMENT       PIC  9(07)V99.                          
              05 FILLER                 PIC  X(57).                             
              05 FFP000-C-MOTIF-REJET-7 PIC  X(03).                             
              05 FFP000-MOTIF-REJET-7   PIC  X(120).                            
              05 FFP000-NUM-SEQ-REJET-7 PIC  X(06).                             
              05 FILLER                 PIC  X(107).                            
              05 FFP000-CD-PART-RB-7    PIC  X(03).                             
              05 FILLER                 PIC  X(20).                             
      *                                                                         
      *                                                                         
      *--------------ENREGISTREMENT TOTAL                                       
      *                                                                         
           03 ENR-FFP000-999 REDEFINES  ENR-FFP000-001.                         
      *-FORCE A '999'                                                           
             05 FFP000-TYPENR-999    PIC  X(03).                                
030215       05 FFP000-NUMSEQ-999    PIC  X(06).                                
      *- 'DARTY'                                                                
             05 FFP000-CEMETTEUR-999 PIC  X(09).                                
      *- 'FACTEMIS'                                                             
030215       05 FFP000-IDENT-FIC-999 PIC  X(08).                                
      *- 'VERSION=2'                                                            
030215       05 FFP000-VERSION-999   PIC  X(01).                                
      *-DATE ET HEURE EMISSION DU LOT ISSUS DATE SYSTEME                        
             05 FFP000-DEMISS-999    PIC  X(08).                                
      *- 'PACIFICA'                                                             
             05 FFP000-DESTINATAIRE-999  PIC  X(09).                            
             05 FFP000-NLOT-999      PIC  9(06).                                
      *- 'EUR'                                                                  
             05 FFP000-CDEVISE-999   PIC  X(03).                                
             05 FFP000-NB-FACT       PIC  9(06).                                
             05 FFP000-NB-PREST      PIC  9(06).                                
             05 FFP000-NB-REMPL      PIC  9(06).                                
             05 FFP000-HT-999        PIC  9(08)V99.                             
             05 FFP000-TVA-999       PIC  9(08)V99.                             
             05 FFP000-TTC-999       PIC  9(08)V99.                             
             05 FFP000-CLI-999       PIC  9(08)V99.                             
             05 FFP000-REGLT-999     PIC  9(08)V99.                             
             05 FILLER               PIC  X(020).                               
             05 FFP000-C-MOTIF-REJET-999 PIC  X(03).                            
      *-                                                                        
             05 FFP000-MOTIF-REJET-999   PIC  X(120).                           
      *-                                                                        
             05 FILLER                   PIC  X(136).                           
                                                                                
