      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * DESCRIPTION DU FICHIER CONSTITUE PAR LE FMD201.                *        
      * EXTRACTION DES PRESTATIONS LOCALES SUR LE MOIS EN COURS ET LE  *        
      * MOIS PRéCéDENT.                                                *        
      * -------------------------------------------------------------- *        
       01  DSECT-FMD201.                                                        
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-NSOCIETE        PIC X(03).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-NLIEU           PIC X(03).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-NVENTE          PIC X(07).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-CFAM            PIC X(07).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-NCODIC          PIC X(07).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-LREFFOURN       PIC X(20).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-DDELIV          PIC X(08).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-QVENDUE         PIC 9(03) .                               
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-PVTOTAL         PIC +9(7),9(2) .                          
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FMD201-DTOPE           PIC X(08).                                
           05  FILLER                 PIC X(01) VALUE '"'.                      
           05  FILLER                 PIC X(01) VALUE ';'.                      
           05  FILLER                 PIC X(36) VALUE SPACES.                   
                                                                                
