      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FGV184                            *00030009
      *       REMONTEE MGI TRC%                                        *00040009
      *       TRI : TYPENR , NSOCIETE , NLIEU , DATE                   *00040110
      *       2 TYPES D'ENREG : PREFIX CA  = CHIFFRE AFFAIRE DU MGI    *00041009
      *                         PREFIX CRE = DONNEES DU CREDIT         *00041109
      *       LONGUEUR D'ENREGISTREMENT : 40 C.                        *00050009
      *================================================================*00060000
      *                                                                 00070000
       01  FILLER.                                                      00080000
      *                                                                 00090000
           05  FGV184-ENREG              PIC  X(40).                    00100009
      *                                                                 00100109
           05  FGV184-CLE-W REDEFINES FGV184-ENREG.                     00101011
               10  FGV184-CLE             PIC  X(17).                   00110011
               10  FILLER                 PIC  X(23).                   00110111
      *                                                                 00110211
           05  FGV184-CA REDEFINES FGV184-ENREG.                        00110311
               10  FGV184-CA-TYPENR       PIC  X(03).                   00110411
               10  FGV184-CA-NSOCIETE     PIC  X(03).                   00111009
               10  FGV184-CA-NLIEU        PIC  X(03).                   00120009
               10  FGV184-CA-DTRAITEMENT  PIC  X(08).                   00121009
               10  FGV184-CA-CA           PIC S9(11)V99 COMP-3.         00130009
               10  FILLER                 PIC  X(16).                   00140009
      *                                                                 00460000
           05  FGV184-CRE REDEFINES FGV184-ENREG.                       00470009
               10  FGV184-CRE-TYPENR      PIC  X(03).                   00480009
               10  FGV184-CRE-NSOCIETE    PIC  X(03).                   00490009
               10  FGV184-CRE-NLIEU       PIC  X(03).                   00500009
               10  FGV184-CRE-DTRAITEMENT PIC  X(08).                   00500109
               10  FGV184-CRE-COCRED      PIC  X(01).                   00501009
               10  FGV184-CRE-WTPCRD      PIC  X(05).                   00502009
               10  FGV184-CRE-CFCRED      PIC  X(05).                   00503009
               10  FGV184-CRE-NBENR       PIC S9(05) COMP-3.            00504009
               10  FGV184-CRE-PREGLTVTE   PIC S9(07)V99 COMP-3.         00520009
               10  FILLER                 PIC  X(04).                   00530009
      *                                                                 00540009
                                                                                
