      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *        FICHIER TOTAL ETAT IVA570                              *         
      *                                                               *         
      *        - DSECT COBOL                                          *         
      *        - NOM  = FVA570                                        *         
      *        - LONGUEUR ENREG = 80                                  *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
       01  DSECT-FVA570.                                                        
           05 FVA570-NSOC            PIC X(3).                                  
           05 FVA570-CSEQ            PIC X(2).                                  
           05 FVA570-CRAYON          PIC X(5).                                  
           05 FVA570-NCONSO          PIC X(6).                                  
           05 FVA570-LCONSO          PIC X(20).                                 
           05 FVA570-QSTOCKINIT      PIC S9(11) COMP-3.                         
           05 FVA570-QSTOCKFINAL     PIC S9(11) COMP-3.                         
           05 FVA570-PSTOCKINIT      PIC S9(9)V9(6) COMP-3.                     
           05 FVA570-PSTOCKFINAL     PIC S9(9)V9(6) COMP-3.                     
           05 FVA570-PRECYCLENTREE   PIC S9(9)V9(6) COMP-3.                     
           05 FVA570-PRECYCLSORTIE   PIC S9(9)V9(6) COMP-3.                     
                                                                                
