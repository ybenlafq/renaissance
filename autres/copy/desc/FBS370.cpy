      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*00000010
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IBS37* AU 07/09/2004  *00000020
      *                                                                *00000030
      *          CRITERES DE TRI (01,03,BI,A,                          *00000040
      *                           04,02,BI,A,                          *00000050
      *                           06,03,BI,A,                          *00000060
      *                           09,04,PD,A)                          *00000070
      *                                                                *00000080
      *                                                                *00000090
      *----------------------------------------------------------------*00000100
       01  DSECT-FBS370.                                                00000110
            05 RUPTURES-FBS370.                                         00000120
1,  3      10 FBS370-NSOCIETE           PIC X(3).                       00000130
4,  2      10 FBS370-CGRPMAG            PIC X(2).                       00000140
6,  3      10 FBS370-NLIEU              PIC X(3).                       00000150
9,  4      10 FBS370-WSEQPRO            PIC S9(07) COMP-3.              00000160
            05 CHAMPS-FBS370.                                           00000170
13, 8      10 FBS370-DEXTRACT           PIC X(08).                      00000180
21, 8      10 FBS370-DEB                PIC X(08).                      00000190
29, 8      10 FBS370-FIN                PIC X(08).                      00000200
37, 6      10 FBS370-MOIS               PIC X(06).                      00000210
43, 6      10 FBS370-SEMAINE            PIC X(06).                      00000220
49, 20     10 FBS370-LLIEU              PIC X(20).                      00000230
69, 5      10 FBS370-AGRPR1             PIC X(05).                      00000240
74, 5      10 FBS370-AGRPR2             PIC X(05).                      00000250
79, 5      10 FBS370-AGRPR3             PIC X(05).                      00000260
84, 5      10 FBS370-AGRPR4             PIC X(05).                      00000270
89, 5      10 FBS370-AGRPR5             PIC X(05).                      00000280
94, 5      10 FBS370-AGRPR6             PIC X(05).                      00000290
99, 5      10 FBS370-AGRPR7             PIC X(05).                      00000300
104,5      10 FBS370-AGRPR8             PIC X(05).                      00000310
109,5      10 FBS370-AGRPR9             PIC X(05).                      00000320
114,5      10 FBS370-AGRPR10            PIC X(05).                      00000330
119,4      10 FBS370-1-QT                 PIC S9(7) COMP-3.             00000340
123,4      10 FBS370-1-QTL                PIC S9(7) COMP-3.             00000350
127,4      10 FBS370-1-QTACCL             PIC S9(7) COMP-3.             00000360
131,6      10 FBS370-1-MTHT               PIC S9(9)V99 COMP-3.          00000370
137,6      10 FBS370-1-MTHTACC            PIC S9(9)V99 COMP-3.          00000380
143,6      10 FBS370-1-MTPRMP             PIC S9(9)V99 COMP-3.          00000390
149,6      10 FBS370-1-MTPRMPACC          PIC S9(9)V99 COMP-3.          00000400
155,4      10 FBS370-2-QT                 PIC S9(7) COMP-3.             00000410
159,4      10 FBS370-2-QTL                PIC S9(7) COMP-3.             00000420
163,4      10 FBS370-2-QTACCL             PIC S9(7) COMP-3.             00000430
167,6      10 FBS370-2-MTHT               PIC S9(9)V99 COMP-3.          00000440
173,6      10 FBS370-2-MTHTACC            PIC S9(9)V99 COMP-3.          00000450
179,6      10 FBS370-2-MTPRMP             PIC S9(9)V99 COMP-3.          00000460
185,6      10 FBS370-2-MTPRMPACC          PIC S9(9)V99 COMP-3.          00000470
191         05 FILLER                      PIC X(322).                  00000480
                                                                                
