      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 FTD022-DSECT.                                                         
          05 FTD022-CAPPLI           PIC X(05).                                 
          05 FTD022-CLE OCCURS 5.                                               
                10 FTD022-CCTRL      PIC X(5).                                  
                10 FTD022-VCTRL      PIC X(30).                                 
          05 FTD022-NSOCIETE         PIC X(03).                                 
          05 FTD022-NLIEU            PIC X(03).                                 
          05 FTD022-NVENTE           PIC X(07).                                 
          05 FTD022-NDOSSIER         PIC X(03).                                 
          05 FTD022-DCREATION        PIC X(08).                                 
          05 FTD022-WARO             PIC X(05).                                 
          05 FTD022-FLAGCTRL         PIC X(01).                                 
          05 FILLER                  PIC X(20).                                 
          05 FTD022-TYPEFI           PIC X(05).                                 
          05 FTD022-CTYPPREST        PIC X(05).                                 
          05 FTD022-CODREM           PIC X(05).                                 
          05 FTD022-WREFUS           PIC X(05).                                 
          05 FTD022-MTRGLT           PIC X(11).                                 
          05 FTD022-DRGLT            PIC X(08).                                 
          05 FTD022-CLESUPP          PIC X(15).                                 
          05 FILLER                  PIC X(16).                                 
          05 FTD022-GAMME            PIC X(05).                                 
          05 FTD022-CMARQ            PIC X(05).                                 
          05 FTD022-POINTVTE         PIC X(15).                                 
          05 FTD022-NOMTITULAIRE     PIC X(30).                                 
          05 FTD022-DSOUSCRIPTION    PIC X(08).                                 
          05 FTD022-DRESILIATION     PIC X(08).                                 
          05 FTD022-COFFRE           PIC X(10).                                 
          05 FTD022-LOFFRE           PIC X(30).                                 
          05 FTD022-TYPREM           PIC X(10).                                 
          05 FTD022-REMREPR          PIC X(04).                                 
          05 FTD022-CREM1            PIC X(10).                                 
          05 FTD022-CREM2            PIC X(10).                                 
          05 FTD022-CRGESTION        PIC X(10).                                 
          05 FILLER                  PIC X(47).                                 
                                                                                
