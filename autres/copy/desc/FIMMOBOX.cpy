      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *-----------------------------------------------------------------        
      * IMMOBILISATION BOX                                                      
      * DESCRIPTION DES ZONES DU FICHIER FIMMOBOX                               
      * LONGUEUR : 80 CARACTERES                                                
      *-----------------------------------------------------------------        
      * DESCRIPTION IMMOBILISATION A DESTINATION DE BOUYGUES TELECOM            
      * TRANSFERT ACTIVITE DARTY BOX                                            
      * - 3 TYPES D'ENREGISTREMENT :                                            
      *   - CODE ENREGISTREMENT 0  : ENTETE                                     
      *   - CODE ENREGISTREMENT 1  : DETAIL                                     
      *   - CODE ENREGISTREMENT 9  : FIN                                        
      *-----------------------------------------------------------------        
      *                                                                         
      *** ENREGISTREMENT ENTETE                                                 
      *                                                                         
       01  W-BOX0-RECORD.                                                       
      * CODE ENREGISTREMENT                                                     
           05   W-BOX0-CENREG           PIC X(01).                              
                88  W-BOX0-TETE             VALUE '0'.                          
           05   W-BOX0-SEPAR1           PIC X(01).                              
      * NOM DU FICHIER                                                          
           05   W-BOX0-NOM              PIC X(10).                              
           05   W-BOX0-SEPAR2           PIC X(01).                              
      * DATE DE PRODUCTION                                                      
           05   W-BOX0-DATE.                                                    
                10 W-BOX0-DATEJJ        PIC X(02).                              
                10 W-BOX0-DATSE1        PIC X(01).                              
                10 W-BOX0-DATEMM        PIC X(02).                              
                10 W-BOX0-DATSE2        PIC X(01).                              
                10 W-BOX0-DATESSAA.                                             
                   15 W-BOX0-DATESS     PIC X(02).                              
                   15 W-BOX0-DATEAA     PIC X(02).                              
           05   W-BOX0-SEPAR3           PIC X(01).                              
      * HEURE DE PRODUCTION                                                     
           05   W-BOX0-HEUR.                                                    
                10 W-BOX0-HEURHH        PIC X(02).                              
                10 W-BOX0-HEUSE1        PIC X(01).                              
                10 W-BOX0-HEURMM        PIC X(02).                              
                10 W-BOX0-HEUSE2        PIC X(01).                              
                10 W-BOX0-HEURSS        PIC X(02).                              
           05   W-BOX0-SEPAR4           PIC X(01).                              
      * ZONE LIBRE                                                              
           05   W-BOX0-LIBRE            PIC X(46).                              
           05   W-BOX0-SEPAR5           PIC X(01).                              
      *                                                                         
      *** ENREGISTREMENT DETAIL                                                 
      *                                                                         
       01  W-BOX1-RECORD.                                                       
      * CODE ENREGISTREMENT                                                     
           05   W-BOX1-CENREG           PIC X(01).                              
                88  W-BOX1-DETAIL           VALUE '1'.                          
           05   W-BOX1-SEPAR1           PIC X(01).                              
      * CODE SOCIETE 920 - DARTY BOX                                            
           05   W-BOX1-SOCIETE          PIC X(03).                              
           05   W-BOX1-SEPAR2           PIC X(01).                              
      * TYPE MOUVEMENT : RB31.TYPMVT                                            
           05   W-BOX1-TYPMVT           PIC X(03).                              
           05   W-BOX1-SEPAR3           PIC X(01).                              
      * DATE MOUVEMENT : RB31.DMVMENT                                           
           05   W-BOX1-DATMVT.                                                  
                10 W-BOX1-DMVTJJ        PIC X(02).                              
                10 W-BOX1-DMVSE1        PIC X(01).                              
                10 W-BOX1-DMVTMM        PIC X(02).                              
                10 W-BOX1-DMVSE2        PIC X(01).                              
                10 W-BOX1-DMSSAA.                                               
                   15 W-BOX1-DMVTSS     PIC X(02).                              
                   15 W-BOX1-DMVTAA     PIC X(02).                              
           05   W-BOX1-SEPAR4           PIC X(01).                              
      * VALEUR (EN CENTIMES SANS VIRGULE) : RB31.MFACTUREHT                     
           05   W-BOX1-VALEUR           PIC 9(12).                              
           05   W-BOX1-SEPAR5           PIC X(01).                              
      * NUMERO DE SERIE : RB30.NSERIE                                           
           05   W-BOX1-NSERIE           PIC X(18).                              
           05   W-BOX1-SEPAR6           PIC X(01).                              
      * NATURE : RB30.TEQUIPEMENT                                               
           05   W-BOX1-NATURE           PIC X(04).                              
           05   W-BOX1-SEPAR7           PIC X(01).                              
      * NUMERO IMMOBILISATION (N� ABEL - FACULTATIF) : RB30.NIMMO               
           05   W-BOX1-NIMMO            PIC X(10).                              
           05   W-BOX1-SEPAR8           PIC X(01).                              
      * CONCATENATION NCODIC ET NSERIE                                          
           05   W-BOX1-FILLER           PIC X(10) VALUE SPACES.                 
           05   W-BOX1-SEPAR9           PIC X(01).                              
      *                                                                         
      *** ENREGISTREMENT FIN                                                    
      *                                                                         
       01  W-BOX9-RECORD.                                                       
      * CODE ENREGISTREMENT                                                     
           05   W-BOX9-CENREG           PIC X(01).                              
                88  W-BOX9-FIN             VALUE '9'.                           
           05   W-BOX9-SEPAR1           PIC X(01).                              
      * NOM DU FICHIER                                                          
           05   W-BOX9-NOM              PIC X(10).                              
           05   W-BOX9-SEPAR2           PIC X(01).                              
      * DATE DE PRODUCTION                                                      
           05   W-BOX9-DATE.                                                    
                10 W-BOX9-DATEJJ        PIC X(02).                              
                10 W-BOX9-DATSE1        PIC X(01).                              
                10 W-BOX9-DATEMM        PIC X(02).                              
                10 W-BOX9-DATSE2        PIC X(01).                              
                10 W-BOX9-DATESSAA.                                             
                   15 W-BOX9-DATESS     PIC X(02).                              
                   15 W-BOX9-DATEAA     PIC X(02).                              
           05   W-BOX9-SEPAR3           PIC X(01).                              
      * HEURE DE PRODUCTION                                                     
           05   W-BOX9-HEUR.                                                    
                10 W-BOX9-HEURHH        PIC X(02).                              
                10 W-BOX9-HEUSE1        PIC X(01).                              
                10 W-BOX9-HEURMM        PIC X(02).                              
                10 W-BOX9-HEUSE2        PIC X(01).                              
                10 W-BOX9-HEURSS        PIC X(02).                              
           05   W-BOX9-SEPAR4           PIC X(01).                              
      * NOMBRES DE LIGNES                                                       
           05   W-BOX9-NENREG           PIC 9(06).                              
           05   W-BOX9-SEPAR5           PIC X(01).                              
      * ZONE LIBRE                                                              
           05   W-BOX9-LIBRE            PIC X(39).                              
           05   W-BOX9-SEPAR6           PIC X(01).                              
      *                                                                         
      *** FIN COPY FIMMOBOX ***                                                 
      *                                                                         
                                                                                
