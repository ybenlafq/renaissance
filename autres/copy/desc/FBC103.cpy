      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC103  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST A SOUMETTRE AU    *         
      *                   TRAITEMENT                                  *         
      *                                                               *         
      *    FBC103       : FICHIER DES LIGNES DE VENTE                 *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:  157 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC103-ENREG.                                                        
           03  FBC103-CLE.                                                      
               05  FBC103-NSOCIETE            PIC  X(03).                       
               05  FBC103-NLIEU               PIC  X(03).                       
               05  FBC103-NVENTE              PIC  X(08).                       
           03  FBC103-CTYPENREG               PIC  X(01).                       
           03  FBC103-NCODICGRP               PIC  X(07).                       
           03  FBC103-NCODIC                  PIC  X(07).                       
           03  FBC103-NSEQ                    PIC  X(02).                       
           03  FBC103-CMODDEL                 PIC  X(03).                       
           03  FBC103-DDELIV                  PIC  X(08).                       
           03  FBC103-CENREG                  PIC  X(05).                       
           03  FBC103-QVENDUE                 PIC  S9(05).                      
           03  FBC103-PVUNIT                  PIC  S9(07)V9(02).                
           03  FBC103-PVUNITF                 PIC  S9(07)V9(02).                
           03  FBC103-PVTOTAL                 PIC  S9(07)V9(02).                
           03  FBC103-NLIGNE                  PIC  X(02).                       
           03  FBC103-TAUXTVA                 PIC  S9(03)V9(02).                
           03  FBC103-PRMP                    PIC  S9(07)V9(02).                
           03  FBC103-WEMPORTE                PIC  X(01).                       
           03  FBC103-CVENDEUR                PIC  X(06).                       
           03  FBC103-WARTINEX                PIC  X(01).                       
           03  FBC103-WTOPELIVRE              PIC  X(01).                       
           03  FBC103-DCOMPTA                 PIC  X(08).                       
           03  FBC103-DCREATION               PIC  X(08).                       
           03  FBC103-DANNULATION             PIC  X(08).                       
           03  FBC103-DSTAT                   PIC  X(08).                       
           03  FBC103-DVERSION                PIC  X(08).                       
           03  FBC103-DSYST                   PIC  X(13).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
