      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FAV001                            *00030011
      *       LONGUEUR D'ENREGISTREMENT : 100 C.                       *00050012
      *================================================================*00060000
      *                                                                 00070000
       01  FILLER.                                                      00080000
      *                                                                 00090000
           05  FAV001-ENREG              PIC  X(100).                   00100012
      *                                                                 00140012
           05  FAV001-ENTETE REDEFINES FAV001-ENREG.                    00150012
             10 FAV001-ARTICLE-E         PIC  X(03).                    00160012
             10 FAV001-PARTENR-E         PIC  X(08).                    00170012
             10 FAV001-IDENFIC-E         PIC  X(12).                    00180012
             10 FAV001-DATCREA-E         PIC  X(08).                    00190012
             10 FAV001-HEUCREA-E         PIC  X(06).                    00200012
             10 FAV001-NUMFICH-E         PIC  9(08).                    00210012
             10 FILLER                   PIC  X(55).                    00220012
      *                                                                 00460000
           05  FAV001-DETAIL REDEFINES FAV001-ENREG.                    00470012
             10 FAV001-ARTICLE-D         PIC  X(03).                    00480012
             10 FAV001-SOCIETE-D         PIC  X(03).                    00481012
             10 FAV001-CONTRAT-D         PIC  X(11).                    00482012
             10 FAV001-NUMCHEQ-D         PIC  X(12).                    00483012
             10 FAV001-NUMCHEQ-R REDEFINES FAV001-NUMCHEQ-D.            00483116
                15 FAV001-SOCDART-R      PIC  X(03).                    00483216
                15 FAV001-MENAFIN-R      PIC  X(03).                    00483316
                15 FAV001-NCHEQUE-R      PIC  X(06).                    00483416
             10 FAV001-MTTCHEQ-D         PIC  9(07)V99.                 00484012
             10 FAV001-DATEMIS-D         PIC  X(08).                    00485012
             10 FAV001-DATFVAL-D         PIC  X(08).                    00486012
             10 FAV001-NAPPORT-D         PIC  X(11).                    00486113
             10 FAV001-NUMLIEU-D         PIC  X(03).                    00486214
             10 FILLER                   PIC  X(32).                    00487015
      *                                                                 00490012
           05  FAV001-ENRFIN REDEFINES FAV001-ENREG.                    00500012
             10 FAV001-ARTICLE-F         PIC  X(03).                    00510012
             10 FAV001-PARTENR-F         PIC  X(08).                    00511012
             10 FAV001-IDENFIC-F         PIC  X(12).                    00512012
             10 FAV001-NBRLIGN-F         PIC  9(08).                    00513012
             10 FILLER                   PIC  X(69).                    00514012
      *                                                                 00520012
                                                                                
