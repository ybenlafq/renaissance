      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00000010
      * DARTY.COM : EXTRACTION DES LIGNES D'ENCAISSEMENT DES VENTES EN  00000020
      *             ECART DE CAISSE                                             
      ****************************************************************  00000030
      *                                                                 00000030
      * 12/10/10 : CREATION                                             00000030
      *                                                                 00000030
      ****************************************************************  00000030
       01     FEC01002-ENTETE-FIC.                                              
           05 FEC01002-CMODPAIMT   PIC X(15) VALUE 'MODE PAIEM INIT'.           
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-DMODIFVTE   PIC X(08) VALUE 'DATE MOD'.                  
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-HMODIFVTE   PIC X(05) VALUE 'H MOD'.                     
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-NUM-CMD-WS  PIC X(18) VALUE '     NUM CMD WS   '.        
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-NUM-CMD-SI  PIC X(13) VALUE ' NUM CMD NCG '.             
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-MT-A-ENC    PIC X(10) VALUE 'MT A ENCAI'.                
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-IDENT-CCID  PIC X(10) VALUE 'IDENT CTI ' .               
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-VENTE-INEX  PIC X(10) VALUE ' VT INEX  '.                
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-PRES-RD2-NT PIC X(10) VALUE 'PRES RD2  '.                
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-COMMENTAIRE PIC X(120) VALUE ' COMMENTAIRE '.            
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC1      PIC X(07) VALUE 'CODIC1 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC2      PIC X(07) VALUE 'CODIC2 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC3      PIC X(07) VALUE 'CODIC3 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC4      PIC X(07) VALUE 'CODIC4 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC5      PIC X(07) VALUE 'CODIC5 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC6      PIC X(07) VALUE 'CODIC6 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC7      PIC X(07) VALUE 'CODIC7 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC8      PIC X(07) VALUE 'CODIC8 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC9      PIC X(07) VALUE 'CODIC9 '.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC10     PIC X(07) VALUE 'CODIC10'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC11     PIC X(07) VALUE 'CODIC11'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC12     PIC X(07) VALUE 'CODIC12'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC13     PIC X(07) VALUE 'CODIC13'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC14     PIC X(07) VALUE 'CODIC14'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC15     PIC X(07) VALUE 'CODIC15'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC16     PIC X(07) VALUE 'CODIC16'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC17     PIC X(07) VALUE 'CODIC17'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC18     PIC X(07) VALUE 'CODIC18'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC19     PIC X(07) VALUE 'CODIC19'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
           05 FEC01002-CODIC20     PIC X(07) VALUE 'CODIC20'.                   
           05 FILLER               PIC X(01) VALUE ';'.                         
                                                                                
