      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * DSECT DE FNV100B : LIEUX A GERER PAR LDAP                       00000020
      ****************************************************************  00000030
      *                                                                 00000060
       01  FNV100B-ENREG.                                               00000070
001        05  FNV100B-NSOC         PIC X(03).                                  
004        05  FNV100B-NLIEU        PIC X(03).                                  
007        05  FNV100B-LLIEU        PIC X(20).                                  
>26   *                                                                         
                                                                                
