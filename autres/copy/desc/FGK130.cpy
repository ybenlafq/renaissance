      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : CONSO STAT VENTES       * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK130                                        * 00100600
      *        - LONGUEUR CLE   =  25 ( <  80 )                       * 00101000
      *        - LONGUEUR ENREG =  78 ( < 129 )                       * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      * AJUSTEMENT LONGUEUR CHAMPS + AJOUT <TAB> ENTRE CHAQUE CHAMP   * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK130-ENR.                                                          
         03  FGK130-CLE.                                                        
           05  FGK130-CODE-FAM      PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK130-CODE-MARK     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK130-CODE-MARQ     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK130-NCODIC        PIC X(07).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
         03  FGK130-DONNEES.                                                    
           05  FGK130-PVTE-F-MHT    PIC Z(09)9,99.                              
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK130-PVTE-N-MHT    PIC Z(09)9,99.                              
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK130-PRMP          PIC Z(09)9,99.                              
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK130-VOL-VTE       PIC +(09)9.                                 
      *                                                                         
                                                                                
