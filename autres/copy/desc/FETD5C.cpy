      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * FICHIER DE SIMULATION DE DATE CICS                            *         
      *****************************************************************         
       01  FETD5           VALUE SPACES.                                        
           03 FETD5-FETD5K.                                                     
              05 ETD5-TRMID        PIC  X(04).                                  
           03 ETD5-APPLID      PIC X(08).                                       
           03 ETD5-DATJOU      PIC X(08).                                       
                                                                                
