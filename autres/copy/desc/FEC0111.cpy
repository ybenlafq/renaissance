      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT FEC011 AU 27/04/2001 *         
      *                                                                *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-FEC011.                                                        
           10 FEC011-CFAM               PIC X(08).                      007  008
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-DMAJSTOCK          PIC X(10).                      015  008
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-CMARQ              PIC X(06).                      023  006
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-LREFFOURN          PIC X(20).                      029  020
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NCODIC             PIC X(07).                      049  007
           10 FILLER                    PIC X VALUE ';'.                        
      *    10 FEC011-QSTOCK             PIC S9(5).                      049  007
           10 FEC011-QSTOCK             PIC -ZZZZ9.                     049  007
           10 FILLER                    PIC X VALUE ';'.                        
      *    10 FEC011-SEQUENCE           PIC S9(04) COMP.                056  002
M14930*    10 FEC011-SEQUENCE           PIC -ZZZ9.                      056  002
      *    10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NLIEUDEST          PIC X(04).                      058  004
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NLIEUORIG          PIC X(04).                      062  004
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NLIEUVTE           PIC X(03).                      066  003
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-DMVT               PIC X(10).                      069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-CPROG              PIC X(05).                      069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-COPER              PIC X(10).                      069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NORIGINE           PIC X(07).                      069  007
           10 FILLER                    PIC X VALUE ';'.                        
M14930     10 FEC011-COMMENTAIRE        PIC X(75).                      069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-QMVT               PIC X(07).                      076  007
           10 FILLER                    PIC X VALUE ';'.                        
      *    10 FEC011-PRMPRECYCL         PIC S9(06)V9(06).               083  007
           10 FEC011-PRMPRECYCL         PIC -ZZZZZ9,99.                 083  007
           10 FILLER                    PIC X VALUE ';'.                        
       01  DSECT-FEC011-ENT.                                                    
           10 FEC011-CFAM-ENT           PIC X(04) VALUE 'CFAM' .        007  008
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-DMAJSTOCK-ENT      PIC X(09) VALUE 'DMAJSTOCK'.    015  008
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-CMARQ-ENT          PIC X(05) VALUE 'CMARQ'.        023  006
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-LREFFOURN-ENT      PIC X(09) VALUE 'LREFFOURN'.    029  020
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NCODIC-ENT         PIC X(06) VALUE 'NCODIC' .      049  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-QSTOCK-ENT         PIC X(6)  VALUE 'QSTOCK'.       049  007
           10 FILLER                    PIC X VALUE ';'.                        
M14930*    10 FEC011-SEQUENCE-ENT       PIC X(8)  VALUE 'SEQUENCE'.     056  002
      *    10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NLIEUDEST-ENT      PIC X(09) VALUE 'NLIEUDEST'.    058  004
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NLIEUORIG-ENT      PIC X(09) VALUE 'NLIEUORIG'.    062  004
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NLIEUVTE-ENT       PIC X(08) VALUE 'NLIEUVTE'.     066  003
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-DMVT-ENT           PIC X(04) VALUE 'DMVT'.         069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-CPROG-ENT          PIC X(05) VALUE 'CPROG'.        069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-COPER-ENT          PIC X(05) VALUE  'COPER'.       069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-NORIGINE-ENT       PIC X(08) VALUE  'NORIGINE'.    069  007
           10 FILLER                    PIC X VALUE ';'.                        
M14930     10 FEC011-COMMENTAIRE-ENT    PIC X(11) VALUE  'COMMENTAIRE'. 069  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-QMVT-ENT           PIC X(04) VALUE  'QMVT'.        076  007
           10 FILLER                    PIC X VALUE ';'.                        
           10 FEC011-PRMPRECYCL-ENT     PIC X(10) VALUE 'PRMPRECYCL'.   083  007
           10 FILLER                    PIC X VALUE ';'.                        
      * 01  DSECT-FEC011-LONG           PIC S9(4)   COMP  VALUE +089.           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-FEC011-LONG           PIC S9(4)   COMP  VALUE +138.           
      *                                                                         
      *--                                                                       
        01  DSECT-FEC011-LONG           PIC S9(4) COMP-5  VALUE +138.           
                                                                                
      *}                                                                        
