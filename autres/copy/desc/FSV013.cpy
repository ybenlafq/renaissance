      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *===============================================================* 00000690
      *                 DESCRIPTION DE FSV013                         * 00000700
      *===============================================================* 00000710
      *                                                                 00000880
       01  WW-FSV013.                                                   00000890
         03  FSV013-NOMPGM           PIC  X(10) VALUE 'FSV013    ' .    00000900
         03  FSV013-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV013.                                                  00000890
           05  FSV013-CMARQ            PIC  X(05).                      00000900
           05  FSV013-LMARQ            PIC  X(20).                      00001100
         03  FSV013-FILLEUR          PIC  X(144)  VALUE SPACES.         00001020
         03  FSV013-CLE-TRI      .                                              
           05  FSV013-CLE-XXX          PIC  X(30)  VALUE SPACES.           00001
                                                                                
