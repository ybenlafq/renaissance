      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC101  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST A SOUMETTRE AU    *         
      *                   TRAITEMENT                                  *         
      *                                                               *         
      *    FBC101       : FICHIER DES ADRESSES                        *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:  350 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC101-ENREG.                                                        
           03  FBC101-CLE.                                                      
               05  FBC101-NSOCIETE            PIC  X(03).                       
               05  FBC101-NLIEU               PIC  X(03).                       
               05  FBC101-NVENTE              PIC  X(08).                       
           03  FBC101-CTITRENOM               PIC  X(05).                       
           03  FBC101-LNOM                    PIC  X(25).                       
           03  FBC101-LPRENOM                 PIC  X(15).                       
           03  FBC101-LIGNE-2.                                                  
               05  FBC101-FILLER-L2-1         PIC  X(04).                       
               05  FBC101-LESCALIER           PIC  X(03).                       
               05  FBC101-FILLER-L2-2         PIC  X(05).                       
               05  FBC101-LETAGE              PIC  X(03).                       
               05  FBC101-FILLER-L2-3         PIC  X(05).                       
               05  FBC101-LPORTE              PIC  X(03).                       
               05  FBC101-FILLER-L2-4         PIC  X(15).                       
           03  FBC101-LIGNE-3.                                                  
               05  FBC101-FILLER-L3-1         PIC  X(04).                       
               05  FBC101-LBATIMENT           PIC  X(03).                       
               05  FBC101-FILLER-L3-2         PIC  X(01).                       
               05  FBC101-LCMPAD1             PIC  X(30).                       
           03  FBC101-LIGNE-4.                                                  
               05  FBC101-CVOIE               PIC  X(06).                       
               05  FBC101-CTVOIE              PIC  X(05).                       
               05  FBC101-LNOMVOIE            PIC  X(21).                       
               05  FBC101-FILLER-L4           PIC  X(06).                       
           03  FBC101-LIGNE-5.                                                  
               05  FBC101-LCOMMUNE            PIC  X(32).                       
               05  FBC101-FILLER-L5           PIC  X(06).                       
           03  FBC101-LIGNE-6.                                                  
               05  FBC101-CPOSTAL             PIC  X(06).                       
               05  FBC101-LBUREAU             PIC  X(32).                       
           03  FBC101-WTYPEADR                PIC  X(01).                       
           03  FBC101-DVENTE                  PIC  X(08).                       
           03  FBC101-TELDOM                  PIC  X(15).                       
           03  FBC101-TELBUR                  PIC  X(15).                       
           03  FBC101-LPOSTEBUR               PIC  X(05).                       
           03  FBC101-DVERSION                PIC  X(08).                       
           03  FBC101-IDCLIENT                PIC  X(08).                       
           03  FBC101-CPAYS                   PIC  X(03).                       
           03  FBC101-NGSM                    PIC  X(15).                       
           03  FBC101-NCARTE.                                                   
               05  FBC101-TILOT                   PIC  X(01).                   
               05  FBC101-CILOT                   PIC  X(08).                   
           03  FBC101-FILLER                  PIC  X(14).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
