      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
***********************************************************************         
************* FICHIER COMPTE-RENDU EXTRACTION * GESTION DES TOURNEES **         
***********************************************************************         
       01  FTL001-DSECT1.                                                       
           03  FTL001-1-CJOUR          PIC  X(8).                               
           03  FTL001-1-NSOC           PIC  X(3).                               
           03  FTL001-1-CPLAN          PIC  X(5).                               
           03  FTL001-1-CTYPE          PIC  X(1).                               
           03  FTL001-1-ZONTRI.                                                 
              04  FTL001-1-CPROTOUR    PIC  X(5).                               
              04  FILLER               PIC  X(12).                              
           03  FTL001-1-QTE            PIC  S9(5) COMP-3.                       
           03  FTL001-1-LPLAN          PIC  X(20).                              
           03  FTL001-1-LSOC           PIC  X(20).                              
           03  FTL001-1-LPROTOUR       PIC  X(20).                              
           03  FILLER                  PIC  X(33).                              
       01  FTL001-DSECT2.                                                       
           03  FTL001-2-CJOUR          PIC  X(8).                               
           03  FTL001-2-NSOC           PIC  X(3).                               
           03  FTL001-2-CPLAN          PIC  X(5).                               
           03  FTL001-2-CTYPE          PIC  X(1).                               
           03  FTL001-2-ZONTRI.                                                 
              04  FTL001-2-NMAG        PIC  X(3).                               
              04  FTL001-2-NVENTE      PIC  X(7).                               
              04  FTL001-2-NCODIC      PIC  X(7).                               
           03  FTL001-2-LNOM           PIC  X(25).                              
           03  FTL001-2-CPTT           PIC  X(5).                               
           03  FTL001-2-LOCALITE       PIC  X(32).                              
           03  FTL001-2-QNONDISP       PIC  9(5) COMP-3.                        
           03  FILLER                  PIC  X(31).                              
                                                                                
