      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FRB015-RECORD.                                               00000480
           05  FRB015-NFOURNISSEUR    PIC X(25).                        00000480
           05  FRB015-SSAAMMJJ        PIC X(08).                        00000480
           05  FRB015-HHMMSSCC        PIC X(08).                        00000480
           05  FRB015-TEQUIPEMENT     PIC X(05).                        00000480
           05  FRB015-CFAM            PIC X(05).                        00000480
           05  FRB015-LFAM            PIC X(20).                        00000480
           05  FRB015-CMARQ           PIC X(05).                        00000480
           05  FRB015-LMARQ           PIC X(20).                        00000480
           05  FRB015-LREFFOURN       PIC X(20).                        00000480
           05  FRB015-LEMBALLAGE      PIC X(50).                        00000480
           05  FRB015-NCODIC          PIC X(07).                        00000480
           05  FRB015-NEAN            PIC X(13).                        00000480
           05  FRB015-NSERIE          PIC X(15).                        00000480
           05  FRB015-ADRMAC          PIC X(12).                        00000480
           05  FRB015-NHARDWARE       PIC X(09).                        00000480
           05  FRB015-NFIRMWARE       PIC X(06).                        00000480
JC0806*    05  FRB015-CLOGIN          PIC X(15).                        00000480
JC0806     05  FRB015-CLOGIN          PIC X(16).                        00000480
JC0806*    05  FRB015-CPASSE          PIC X(15).                        00000480
JC0806     05  FRB015-CPASSE          PIC X(16).                        00000480
           05  FRB015-LCONSTRUCTEUR   PIC X(25).                        00000480
           05  FRB015-WCMPL-ALP       PIC X(02).                        00000480
           05  FRB015-WCOMPLETEL      PIC X(01).                        00000480
      * CE CHAMPS EST UTILISE DE FACON PROVISOIR POUR LES LIB           00000600
      * ANOMALIE ON PEUT TOUJOURS LE DECALER SI BESOIN EST              00000600
JC0806*    05  FRB015-FILLER          PIC X(34).                        00000480
JC0806     05  FRB015-FILLER          PIC X(32).                        00000480
      * LG TOTALE = 320                                                 00000600
                                                                                
