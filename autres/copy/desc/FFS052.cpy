      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **********************************************************                
      *   COPY DU FICHIER DES LIGNES DE VENTES COMPTABILISEES                   
      *   DU JOUR         LG = 300                                              
      **********************************************************                
      *                                                                         
       01  DSECT-FFS052.                                                        
           02  FFS052-CTYPENREG      PIC X(01).                                 
           02  FFS052-NSOCIETE       PIC X(03).                                 
           02  FFS052-NLIEU          PIC X(03).                                 
           02  FFS052-NVENTE         PIC X(07).                                 
           02  FFS052-NLIGNE         PIC X(02).                                 
           02  FFS052-NSEQ           PIC X(02).                                 
           02  FFS052-NCODICGRP      PIC X(07).                                 
           02  FFS052-NCODIC         PIC X(07).                                 
           02  FFS052-CMODDEL        PIC X(03).                                 
           02  FFS052-CTOURNEE       PIC X(08).                                 
           02  FFS052-DDELIV         PIC X(08).                                 
           02  FFS052-DCREATION      PIC X(08).                                 
           02  FFS052-DVENTE         PIC X(08).                                 
           02  FFS052-DANNULATION    PIC X(08).                                 
           02  FFS052-CVENDEUR       PIC X(06).                                 
           02  FFS052-CENREG         PIC X(05).                                 
           02  FFS052-QVENDUE        PIC S9(05) COMP-3.                         
           02  FFS052-PVTOTAL        PIC S9(07)V9(02) COMP-3.                   
           02  FFS052-TAUXTVA        PIC S9(03)V9(02) COMP-3.                   
           02  FFS052-CPRESTGRP      PIC X(05).                                 
           02  FFS052-CFAM           PIC X(05).                                 
           02  FFS052-CORGORED       PIC X(05).                                 
           02  FFS052-LNOM           PIC X(25).                                 
           02  FFS052-CPOSTAL        PIC X(05).                                 
           02  FFS052-CINSEE         PIC X(05).                                 
           02  FFS052-CTITRENOM      PIC X(05).                                 
           02  FFS052-LPRENOM        PIC X(15).                                 
           02  FFS052-CVOIE          PIC X(05).                                 
           02  FFS052-CTVOIE         PIC X(04).                                 
           02  FFS052-LNOMVOIE       PIC X(21).                                 
           02  FFS052-LBATIMENT      PIC X(03).                                 
           02  FFS052-LESCALIER      PIC X(03).                                 
           02  FFS052-LETAGE         PIC X(03).                                 
           02  FFS052-LPORTE         PIC X(03).                                 
           02  FFS052-LCOMMUNE       PIC X(32).                                 
           02  FFS052-TELDOM         PIC X(10).                                 
           02  FFS052-TELBUR         PIC X(10).                                 
           02  FFS052-DCOMPTA        PIC X(08).                                 
           02  FFS052-TYPVTE         PIC X(01).                                 
           02  FFS052-FILLER         PIC X(30).                                 
                                                                                
