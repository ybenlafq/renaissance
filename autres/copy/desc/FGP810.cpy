      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGP805  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : SUIVI DES CODIC AVEC BL DISPONIBLE          *         
      *                                                               *         
      *    FGP805       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    STRUCTURE    :   SAM                                       *         
      *    LONGUEUR ENR.:   200                                       *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  :FGPXXX:-ENREG.                                                      
      *                                                         1  116          
        02 :FGPXXX:-DONNEE-ENTETE.                                              
           05  :FGPXXX:-NSOCIETE              PIC  X(03).                       
      *                                                         1   03          
           05  :FGPXXX:-NSOCDEPAFF            PIC  X(03).                       
      *                                                         4   03          
           05  :FGPXXX:-DEPOTAFF              PIC  X(03).                       
      *                                                         7   03          
           05  :FGPXXX:-NSOCDEPGEN            PIC  X(03).                       
      *                                                        10   03          
           05  :FGPXXX:-NDEPOTGEN             PIC  X(03).                       
      *                                                        13   03          
           05  :FGPXXX:-CHEFPROD              PIC  X(05).                       
      *                                                        16   05          
      *                                                                         
        02 :FGPXXX:-DONNEE-TRI.                                                 
      *                                                                         
           05  :FGPXXX:-TRIDEP                PIC  X(01).                       
      *                                                                         
        02 :FGPXXX:-DONNEE-CODIC.                                               
      *                                                        21   01          
           05  :FGPXXX:-NCODIC                PIC  X(07).                       
      *                                                        22   07          
           05  :FGPXXX:-CFAM                  PIC  X(05).                       
      *                                                        29   05          
           05  :FGPXXX:-CMARQ                 PIC  X(05).                       
      *                                                        34   05          
           05  :FGPXXX:-LREFFOURN             PIC  X(20).                       
      *                                                        39   20          
           05  :FGPXXX:-LSTATCOMP             PIC  X(03).                       
      *                                                        59   03          
           05  :FGPXXX:-WOA                   PIC  X(01).                       
      *                                                        62   01          
           05  :FGPXXX:-TLMELA                PIC  X(03).                       
      *                                                        63   03          
        02 :FGPXXX:-DONNEE-DEPOT.                                               
      *                                                        66 06            
           05  :FGPXXX:-NSOCDEP               PIC  X(03).                       
      *                                                        66   03          
           05  :FGPXXX:-NDEPOT                PIC  X(03).                       
      *                                                        69   03          
        02 :FGPXXX:-DONNEE-VENTE.                                               
      *                                                           72 17         
           05  :FGPXXX:-QS-0                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-QS-1                  PIC S9(06) COMP-3.                
           05  :FGPXXX:-V4S                   PIC S9(06) COMP-3.                
           05  :FGPXXX:-NBMAG                 PIC S9(03) COMP-3.                
           05  :FGPXXX:-QSTOCKMAG             PIC S9(05) COMP-3.                
      *                                                         72   17         
        02 :FGPXXX:-DONNEE-SORTIE.                                              
      *                                                         89              
           05  :FGPXXX:-SORTIES7J             PIC S9(05)   COMP-3.              
           05  :FGPXXX:-COUV7J                PIC S9(03)V9 COMP-3.              
      *                                                       89    6           
      *                                                                         
        02 :FGPXXX:-DONNEE-STOCK.                                               
      * 95                                                                      
           05  :FGPXXX:-QSTOCKDIS             PIC S9(07) COMP-3.                
      *                                                        95    4          
           05  :FGPXXX:-QSTOCKRES             PIC S9(06) COMP-3.                
      *                                                        99    4          
      *                                                                         
        02 :FGPXXX:-DONNEE-COMMANDE.                                            
      *                                                        103              
           05  :FGPXXX:-STATUT                PIC  X(03).                       
      *                                                        103   3          
           05  :FGPXXX:-NCDE                  PIC  X(07).                       
      *                                                        106   7          
           05  :FGPXXX:-DCDE.                                                   
              10  :FGPXXX:-DCDE-YYYY             PIC  X(04).                    
              10  :FGPXXX:-DCDE-MMJJ             PIC  X(04).                    
      *                                                       113    8          
           05  :FGPXXX:-QCDE                  PIC S9(05) COMP-3.                
      *                                                       121    3          
           05  :FGPXXX:-QBL                   PIC S9(05) COMP-3.                
      *                                                       124    3          
           05  :FGPXXX:-NVOLUME               PIC S9(04)V9(2) COMP-3.           
      *                                                       127    4          
        02  :FGPXXX:-QSTKDISGEN               PIC S9(07) COMP-3.                
      *                                                       131    4          
        02  :FGPXXX:-TRI1                     PIC X(1).                         
        02  :FGPXXX:-TRI2                     PIC X(1).                         
        02  :FGPXXX:-VALEUR                   PIC S9(6)V9 COMP-3.               
E0576   02  :FGPXXX:-TOTQCDERES               PIC S9(5)   COMP-3.               
E0576   02  :FGPXXX:-DEPOTEXP                 PIC X(20).                        
      *                                                      135   29           
E0576 * 02  :FGPXXX:-FILLER                   PIC X(09).                        
E0576   02  :FGPXXX:-FILLER                   PIC X(36).                        
E0576 *                                                       164  36           
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
