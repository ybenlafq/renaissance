      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *                                                                *        
      *  PROJET      : PREPARATION DE PAYE                             *        
      *  FICHIER     : FPP000                                          *        
      *  TITRE       : FICHIER DES PRIMES INDIVIDUELLES                *        
      *  LONGUEUR    : 95                                              *        
      *  TRI         : 01,23,CH,A,                                     *        
      *                                                                *        
      ******************************************************************        
       01  DSECT-FPP000.                                                        
           05 RUPTURES-FPP000.                                                  
              10 FPP000-NSOCRAT         PIC X(03).                      007  008
              10 FPP000-NMAGRAT         PIC X(03).                      007  008
              10 FPP000-CPRIME          PIC X(05).                      007  008
              10 FPP000-CVENDEUR        PIC X(06).                      007  008
           05 CHAMPS-FPP000.                                                    
              10 FPP000-DONNEES-MAGASIN.                                        
                 15 FPP000-NSOCVTE      PIC X(03).                      007  008
                 15 FPP000-NMAGVTE      PIC X(03).                      007  008
                 15 FPP000-DMOISPAYE    PIC X(06).                              
              10 FPP000-DONNEES-VENDEUR.                                        
                 15 FPP000-LVENDEUR     PIC X(15).                              
                 15 FPP000-NMATSIGA     PIC X(07).                              
                 15 FPP000-WPRIME       PIC X(01).                              
              10 FPP000-DONNEES-PRIME.                                          
                 15 FPP000-QPIECEVOL    PIC S9(07)     COMP-3.                  
                 15 FPP000-PMTCA        PIC S9(11)     COMP-3.                  
                 15 FPP000-QPOURCVOL    PIC S9(3)V9(3) COMP-3.                  
                 15 FPP000-QPOURCCA     PIC S9(3)V9(3) COMP-3.                  
                 15 FPP000-PPAMM        OCCURS 3                                
0202                                    PIC S9(5)V9(2) COMP-3.                  
              10 FPP000-DONNEES-PPPRI.                                          
                 15 FPP000-LNOMZONE     PIC X(10).                              
                 15 FPP000-CPRISIGA     PIC X(03).                              
                                                                                
