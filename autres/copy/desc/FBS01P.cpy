      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ---------------------------------------------------------------         
      *                         APPLICATION : TD00.                             
      * LONGUEUR : 100                                                          
      * ---------------------------------------------------------------         
      * PROVISIONNEMENT RéMUNéRATION OPéRATEURS (GSM)                           
      * CE FICHIER SE TROUVE EN SORTIE DU BBS001 (INTERFACE COMPTABLE           
      * DES COMMISSIONS )                                                       
      * ---------------------------------------------------------------         
       01  FBS01P.                                                              
           05  FBS01P-NSOCIETE                  PIC X(03).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-NLIEU                     PIC X(03).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-NVENTE                    PIC X(07).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-NCODICGRP                 PIC X(07).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-NCODIC                    PIC X(07).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-CPREST                    PIC X(05).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-CTYPPREST                 PIC X(05).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-CTYPCND                   PIC X(05).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-NSEQNQ                    PIC 9(05).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-NDOSSIER                  PIC 9(03).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-MONTANT                   PIC +9(07),9(02).               
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FBS01P-DATE                      PIC X(08).                      
           05  FILLER                           PIC X(01) VALUE ';'.            
           05  FILLER                           PIC X(319) VALUE SPACES.        
                                                                                
