      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * FGK230: FOURNISSEURS POUR KRISP                                         
      ******************************************************************        
       01  FGK230-ENR.                                                          
   1       02  FGK230-NENTCDE         PIC X(06).                                
   7       02  FGK230-LENTCDE         PIC X(20).                                
  27       02  FGK230-LENTCDEADR1     PIC X(32).                                
  59       02  FGK230-LENTCDEADR2     PIC X(32).                                
  91       02  FGK230-NENTCDEADR3     PIC X(05).                                
  96       02  FGK230-LENTCDEADR4     PIC X(26).                                
 122       02  FGK230-NENTCDETEL      PIC X(15).                                
 137       02  FGK230-CENTCDETELX     PIC X(15).                                
 152       02  FILLER                 PIC X(09).                                
 161 *** LRECL=160                                                              
                                                                                
