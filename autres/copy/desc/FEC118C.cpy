      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * darty.com : Export articles Host => Websphere                   00000020
      * v5.1P quotas soir�e                                             00000020
      ****************************************************************  00000030
       01     FEC118-DSECT.                                                     
           05 FEC118-CLE.                                                       
            7 FEC118-NSOC          PIC X(03).                                   
            7 FEC118-NLIEU         PIC X(03).                                   
            7 FEC118-CZONL         PIC X(05).                                   
            7 FEC118-DJOUR         PIC X(08).                                   
            7 FEC118-CPLAGE        PIC X(02).                                   
           05 FEC118-QQUOTA        PIC 9(05).                                   
           05 FEC118-HDEBUT        PIC X(04).                                   
           05 FEC118-HFIN          PIC X(04).                                   
           05 FEC118-CMODDEL       PIC X(03).                                   
           05 FEC118-CSERVICE      PIC X(05).                                   
LA1408     05 FEC118-TP-MUTATION   PIC X(05).                                   
                                                                                
