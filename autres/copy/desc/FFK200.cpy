      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * FFK200: PRODUITS  POUR GFK                                              
      ** LRECL=100                                                              
      ******************************************************************        
       01  FFK200-ENR.                                                          
   1       02  FFK200-NCODIC          PIC X(07).                                
   8       02  FFK200-CFAM            PIC X(05).                                
  13       02  FFK200-LFAM            PIC X(20).                                
  33       02  FFK200-LREFFOURN       PIC X(20).                                
  53       02  FFK200-LMARQ           PIC X(20).                                
  73       02  FFK200-NEAN            PIC X(13).                                
  86       02  FFK200-PERIODE         PIC X(06).                                
  92       02  FILLER                 PIC X(09).                                
                                                                                
