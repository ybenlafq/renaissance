      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : PRODUITS MIS A JOUR     * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK120                                        * 00100600
      *        - LONGUEUR ENREG = 120 ( <- 290 )                      * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      * AJUSTEMENT LONGUEUR CHAMPS + AJOUT <TAB> ENTRE CHAQUE CHAMP   * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK120-ENR.                                                          
           05  FGK120-CODE-FAM      PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-LIB-FAM       PIC X(20).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-CODE-MARK     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-LIB-MARK      PIC X(20).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-NCODIC        PIC X(07).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-REF-COMM      PIC X(20).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-CODE-MARQ     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-LIB-MARQ      PIC X(20).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK120-DATE-CRE      PIC X(10).                                  
      *                                                                         
                                                                                
