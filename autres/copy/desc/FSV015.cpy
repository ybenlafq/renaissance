      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00001230
      * SS TABLE SVRYL                                                  00001230
      *                                                                 00001230
       01  WW-FSV015.                                                   00000890
         03  FSV015-NOMPGM  .                                           00000900
           05  FSV015-NOMPROG          PIC  X(07) VALUE 'FSV015' .      00000900
           05  FSV015-APPDEST          PIC  X(03).                      00000900
         03  FSV015-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV015.                                                  00000890
           05  FSV015-APPLICAT         PIC  X(08).                      00000900
           05  FSV015-CRAYC            PIC  X(05).                      00001100
           05  FSV015-CRAYM            PIC  X(05).                      00001100
           05  FSV015-NSEC             PIC  X(03).                      00001100
           05  FSV015-COMMENT          PIC  X(10).                      00001100
         03  FSV015-FILLEUR          PIC  X(128)  VALUE SPACES.         00001020
         03  FSV015-CLE-TRI      .                                              
           05  FSV015-CLE-XXX          PIC  X(30)  VALUE SPACES.           00001
      *                                                                 00001320
                                                                                
