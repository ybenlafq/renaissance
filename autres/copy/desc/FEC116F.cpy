      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * darty.com : zones geographiques et de livraison (filtre)     *  00000020
      ****************************************************************  00000030
       01  FEC116F-DSECT.                                                       
  1   *    05  FEC116F-CPAYS      PIC X(02).                                    
  3   *    05  FEC116F-NSOC       PIC X(03).                                    
  6   *    05  FEC116F-CELEMEN    PIC X(05).                                    
 11   *    05  FEC116F-NSOCLIV    PIC X(03).                                    
 14   *    05  FEC116F-NLIEULIV   PIC X(03).                                    
   2       05  FEC116F-CPAYS      PIC X(02).                                    
   5       05  FEC116F-NSOC       PIC X(03).                                    
  10       05  FEC116F-SERVICE    PIC X(05).                                    
  13       05  FEC116F-CMODDEL    PIC X(03).                                    
  18       05  FEC116F-CELEMEN    PIC X(05).                                    
  21       05  FEC116F-NSOCLIV    PIC X(03).                                    
  24       05  FEC116F-NLIEULIV   PIC X(03).                                    
      * L=24                                                            00000050
                                                                                
