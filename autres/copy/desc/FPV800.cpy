      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  W-ENR-FPV800.                                                        
           05 FPV800-NSOCIETE        PIC X(3).                                  
           05 FPV800-NLIEU           PIC X(3).                                  
           05 FPV800-NVENTE          PIC X(7).                                  
           05 FPV800-NSEQNQ          PIC +9(5).                                 
           05 FPV800-CVENDEUR        PIC X(6).                                  
           05 FPV800-DTOPE           PIC X(8).                                  
           05 FPV800-DDELIV          PIC X(8).                                  
           05 FPV800-DCREATION       PIC X(8).                                  
           05 FPV800-WOFFRE          PIC X(1).                                  
           05 FPV800-NCODICGRP       PIC X(7).                                  
           05 FPV800-CTYPENT         PIC X(2).                                  
           05 FPV800-WREMISE         PIC X(1).                                  
           05 FPV800-NCODIC          PIC X(7).                                  
           05 FPV800-CENREG          PIC X(5).                                  
           05 FPV800-WEP             PIC X(1).                                  
           05 FPV800-WOA             PIC X(1).                                  
           05 FPV800-WPSAB           PIC X(1).                                  
           05 FPV800-WREMVTE         PIC X(1).                                  
           05 FPV800-NSEQENT         PIC +9(5).                                 
           05 FPV800-QTVEND          PIC +9(5).                                 
           05 FPV800-QTCODIG         PIC +9(5).                                 
           05 FPV800-MTVTEHT         PIC +9(7)V99.                              
           05 FPV800-MTVTETVA        PIC +9(7)V99.                              
           05 FPV800-MTFORHT         PIC +9(7)V99.                              
           05 FPV800-MTFORTVA        PIC +9(7)V99.                              
           05 FPV800-MTDIFFHT        PIC +9(7)V99.                              
           05 FPV800-MTDIFFTVA       PIC +9(7)V99.                              
           05 FPV800-MTPRMP          PIC +9(7)V99.                              
           05 FPV800-MTPRIME         PIC +9(7)V99.                              
           05 FPV800-MTPRIMEO        PIC +9(7)V99.                              
           05 FPV800-MTPRIMEV        PIC +9(7)V99.                              
           05 FPV800-MTCOMMOP        PIC +9(7)V99.                              
           05 FPV800-MTREFINOP       PIC +9(7)V99.                              
           05 FPV800-TYPVTE          PIC X(01).                                 
           05 FPV800-WTABLE          PIC X(02).                                 
           05 FPV800-NZONPRIX        PIC X(02).                                 
      * FIN                                                                     
                                                                                
