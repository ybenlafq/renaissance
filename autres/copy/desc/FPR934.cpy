      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00010000
      *================================================================*00020000
      *       DESCRIPTION DU FICHIER FPR934 D'EXTRACTION DES           *00030000
      *       PRESTATIONS DE TELESURVEILLANCE EPS                      *00040000
      *       FICHIER EN SORTIE DE BPR934                              *00041000
      *            ET EN ENTREE DE BPR935                              *00042000
      *       LONGUEUR D'ENREGISTREMENT : 400 C.                       *00043000
      *================================================================*00044000
      *                                                                 00045000
       01  FILLER.                                                      00046000
      *                                                                 00047000
           05  FPR934-ENREG.                                            00048000
               10  FILLER                PIC  X(01) VALUE '"'.          00050000
               10  FPR934-NSOCIETE       PIC  X(03).                    00060000
               10  FILLER                PIC  X(03) VALUE '","'.        00070000
               10  FPR934-NLIEU          PIC  X(03).                    00080000
               10  FILLER                PIC  X(03) VALUE '","'.        00090000
               10  FPR934-LLIEU          PIC  X(20).                    00100000
               10  FILLER                PIC  X(03) VALUE '","'.        00110000
               10  FPR934-NVENTE         PIC  X(07).                    00120000
               10  FILLER                PIC  X(03) VALUE '","'.        00130000
               10  FPR934-CTYPENREG      PIC  X(01).                    00140000
               10  FILLER                PIC  X(03) VALUE '","'.        00150000
               10  FPR934-CVENDEUR       PIC  X(06).                    00151000
               10  FILLER                PIC  X(03) VALUE '","'.        00152000
               10  FPR934-LVENDEUR       PIC  X(15).                    00153000
               10  FILLER                PIC  X(03) VALUE '","'.        00154000
               10  FPR934-DTOPE          PIC  X(08).                    00155000
               10  FILLER                PIC  X(03) VALUE '","'.        00156000
               10  FPR934-WTOPELIVRE     PIC  X(01).                    00157000
               10  FILLER                PIC  X(03) VALUE '","'.        00158000
               10  FPR934-CTITRENOM      PIC  X(05).                    00159000
               10  FILLER                PIC  X(03) VALUE '","'.        00159100
               10  FPR934-LNOM           PIC  X(25).                    00159200
               10  FILLER                PIC  X(03) VALUE '","'.        00159300
               10  FPR934-LPRENOM        PIC  X(15).                    00159400
               10  FILLER                PIC  X(03) VALUE '","'.        00159500
               10  FPR934-CVOIE          PIC  X(05).                    00159600
               10  FILLER                PIC  X(03) VALUE '","'.        00159700
               10  FPR934-CTVOIE         PIC  X(04).                    00159800
               10  FILLER                PIC  X(03) VALUE '","'.        00159900
               10  FPR934-LNOMVOIE       PIC  X(21).                    00160000
               10  FILLER                PIC  X(03) VALUE '","'.        00160100
               10  FPR934-CPOSTAL        PIC  X(05).                    00160200
               10  FILLER                PIC  X(03) VALUE '","'.        00160300
               10  FPR934-LCOMMUNE       PIC  X(32).                    00160400
               10  FILLER                PIC  X(03) VALUE '","'.        00160500
               10  FPR934-TELDOM         PIC  X(10).                    00160600
               10  FILLER                PIC  X(03) VALUE '","'.        00160700
               10  FPR934-CENREG         PIC  X(05).                    00160800
               10  FILLER                PIC  X(03) VALUE '","'.        00160900
               10  FPR934-NLIGNE         PIC  X(02).                    00161000
               10  FILLER                PIC  X(03) VALUE '","'.        00161100
               10  FPR934-NCODIC         PIC  X(07).                    00161200
               10  FILLER                PIC  X(03) VALUE '","'.        00161300
               10  FPR934-CFAM           PIC  X(05).                    00161400
               10  FILLER                PIC  X(03) VALUE '","'.        00161500
               10  FPR934-CMARQ          PIC  X(05).                    00161600
               10  FILLER                PIC  X(03) VALUE '","'.        00161700
               10  FPR934-LREFFOURN      PIC  X(20).                    00161800
               10  FILLER                PIC  X(03) VALUE '","'.        00161900
               10  FPR934-QVENDUE        PIC  9(05).                    00162000
               10  FILLER                PIC  X(03) VALUE '","'.        00163000
               10  FPR934-DANNULATION    PIC  X(08).                    00163100
               10  FILLER                PIC  X(03) VALUE '","'.        00163200
               10  FPR934-WANNULATION    PIC  X(06).                    00163300
               10  FILLER                PIC  X(03) VALUE '","'.        00163400
               10  FPR934-TAUXTVA        PIC  --9.V99.                  00163500
               10  FILLER                PIC  X(03) VALUE '","'.        00163600
               10  FPR934-PVTOTAL        PIC  ------9.V99.              00163700
               10  FILLER                PIC  X(03) VALUE '","'.        00163800
               10  FPR934-REMISE         PIC  ------9.V99.              00163900
               10  FILLER                PIC  X(03) VALUE '","'.        00164000
               10  FPR934-NCONTRAT       PIC  X(18).                    00165000
               10  FILLER                PIC  X(01) VALUE '"'.          00166001
               10  FILLER                PIC  X(13).                    00167001
               10  FPR934-NFICHIER       PIC  X(02).                    00169000
                                                                                
