      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * NMD : DSECT DU FICHIER FTF844 MAJ A ENVOYER EN MAGASIN       *  00000020
      *       CETTE COPIE DOIT �TRE IDENTIQUE � LA D�FINITION DE     *  00000020
      *       LA TABLE AS400 : FEMSL22  (HORS IMAJ ET DSYST)         *  00000020
      *                                                              *  00000020
      *       EN BATCH, CE FICHIER EST INT�GR� DANS LE TRASFERT      *  00000020
      *       (FICHIER FTF600C)                                      *  00000020
      *      EN TP, LE MEME MESSAGE EST ENVOY� DIRECTEMENT VIA MQ,   *  00000020
      *      RECEPTIONNE DANS LES DEUX CAS PAR BCBMDMQ0NN SUR AS400  *  00000020
      *                                                              *  00000020
      ****************************************************************  00000030
      * 2001/09/28 : FC   : CREATION                                 *  00000020
      ****************************************************************  00000030
      *                                                                 00000040
       01     TF844-ENREG.                                                      
           05 TF844-NSOC          PIC  X(03).                                   
           05 TF844-NLIEU         PIC  X(03).                                   
           05 TF844-NVENTE        PIC  X(08).                                   
           05 TF844-NSEQNQ        PIC S9(05) COMP-3.                            
           05 TF844-NCODIC        PIC  X(07).                                   
           05 TF844-NCODICGRP     PIC  X(07).                                   
           05 TF844-QVENDUE       PIC S9(05) COMP-3.                            
           05 TF844-NSOCDEP       PIC  X(03).                                   
           05 TF844-NLIEUDEP      PIC  X(03).                                   
           05 TF844-NSOCDEL       PIC  X(03).                                   
           05 TF844-NLIEUDEL      PIC  X(03).                                   
           05 TF844-NSOCSTK       PIC  X(03).                                   
           05 TF844-NLIEUSTK      PIC  X(03).                                   
           05 TF844-WCQERESF      PIC  X(01).                                   
           05 TF844-NMUTATION     PIC  X(07).                                   
           05 TF844-WMUTETAT      PIC  X(01).                                   
           05 TF844-CVENDEUR      PIC  X(06).                                   
           05 TF844-CMODDEL       PIC  X(03).                                   
           05 TF844-DDELIV        PIC  X(08).                                   
           05 TF844-CPLAGE        PIC  X(02).                                   
           05 TF844-CEQUIPE       PIC  X(05).                                   
           05 TF844-CTOURNEE      PIC  X(08).                                   
           05 TF844-WACOMMUTER    PIC  X(01).                                   
           05 TF844-NBONENLV      PIC  X(07).                                   
           05 TF844-IMAJ          PIC  S9(07) COMP-3.                           
           05 TF844-DSYST         PIC S9(13)  COMP-3.                           
           05 TF844-NSATELLITE    PIC  X(02).                                   
           05 TF844-NCASE         PIC  X(03).                                   
      *                                                                         
      *                                                                 00000050
                                                                                
