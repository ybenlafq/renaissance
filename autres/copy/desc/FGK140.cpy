      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : CONSO STAT ACHATS       * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK140                                        * 00100600
      *        - LONGUEUR CLE   =  32 ( <- 100 )                      * 00101000
      *        - LONGUEUR ENREG =  71 ( <- 136 )                      * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      * AJUSTEMENT LONGUEUR CHAMPS + AJOUT <TAB> ENTRE CHAQUE CHAMP   * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK140-ENR.                                                          
         03  FGK140-CLE.                                                        
           05  FGK140-CODE-FAM      PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK140-CODE-MARK     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK140-CODE-ENTCDE   PIC X(06).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK140-CODE-MARQ     PIC X(05).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK140-NCODIC        PIC X(07).                                  
           05  FILLER               PIC X(01)     VALUE '	'.                    
         03  FGK140-DONNEES.                                                    
           05  FGK140-PRA-M-MOIS    PIC Z(09)9,99.                              
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK140-PRA           PIC Z(09)9,99.                              
           05  FILLER               PIC X(01)     VALUE '	'.                    
           05  FGK140-VOL-ACHAT     PIC +(9)9.                                  
      *                                                                         
                                                                                
