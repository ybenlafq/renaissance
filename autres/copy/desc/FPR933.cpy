      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * ------------------------------------------------------------ *          
      * DESCRIPTION DU FICHIER D'EXTRACTION DES VENTES CONTENANT     *          
      * UNE PRESTATION PARTICULIéRE                                  *          
      * PROGRAMME L'UTILISANT : BPR933.                              *          
      * LONGUEUR :                                                   *          
      * CONTIENT DES DELIMITEUR POUR EXPLOITATION MICRO.             *          
      * ------------------------------------------------------------ *          
       01  FPR933-DSECT.                                                        
           05  FPR933-NSOCIETE            PIC X(03).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-NLIEU               PIC X(03).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-NVENTE              PIC X(07).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-DCREATION           PIC X(08).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-DANNULATION         PIC X(08).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-CPRESTATION         PIC X(05).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-NCODIC              PIC X(07).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-NCODICGRP           PIC X(07).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-LIBELLE             PIC X(20).                            
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FPR933-PVTOTAL             PIC +9(07),9(0002).                   
           05  FILLER                     PIC X(01) VALUE ';'.                  
           05  FILLER                     PIC X(67) VALUE SPACES.               
                                                                                
