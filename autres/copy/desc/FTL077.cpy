      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00001000
      * ****************       DESCRIPTION     ******************* *    00002000
      *                 *******   FTL077  *******                  *    00002100
      *       FICHIER DES VENTES EN ANOMALIES DE LIVRAISON / MAG   *    00002200
      *       POUR EDITION ETAT DES ANOMALIES DE LIVRAISON         *    00002300
      *------------------------------------------------------------*    00002400
      *                  FICHIER SAM, LONGUEUR 50                  *    00002500
      **************************************************************    00002601
      *CR LE 13/06/2008 . COPIER A PARTIR DU MASTER.SOURCE         *    00002701
      *                 . AJOUT NSOCDEPLIV ET NLIEUDEPLIV A PARTIR *    00002801
      *                   DU FILLER PIC X(08) D'AVANT              *    00002901
      **************************************************************    00003000
      *                                                                 00003100
       01  ENR-FTL077.                                                  00003200
      *-------------------------------------  CODE SOCIETE MAGASIN      00003300
             02 FTL077-NSOCIETE      PIC X(03).                         00003400
      *-------------------------------------  CODE MAGASIN              00003500
             02 FTL077-NLIEU         PIC X(03).                         00003600
      *-------------------------------------  CODE VENTE                00003700
             02 FTL077-NVENTE        PIC 9(07).                         00003800
      *-------------------------------------  DATE DE LIVRAISON         00003900
             02 FTL077-DDELIV        PIC X(08).                         00004000
      *-------------------------------------  NUMERO DE FOLIO           00004100
             02 FTL077-NFOLIO        PIC X(03).                         00004200
      *-------------------------------------  CODE PROFIL TOURNEE       00004300
             02 FTL077-CPROTOUR      PIC X(05).                         00004400
      *-------------------------------------  CODE TOURNEE              00004500
             02 FTL077-CTOURNEE      PIC X(03).                         00004600
      *-------------------------------------  VALEUR RESERVE FOURNISSEUR00004700
             02 FTL077-WCQERESF      PIC X(01).                         00004800
      *-------------------------------------  CODE TYPE D'ANOMALIE      00004900
             02 FTL077-CTYPEANO      PIC X(01).                         00005000
      *-------------------------------------  DATE DE TRAITEMENT        00005100
             02 FTL077-DMVT          PIC X(08).                         00006000
CR1306*-------------------------------------  NSOCDEPLIV                00006102
  "          02 FTL077-NSOCDEPLIV    PIC X(03).                         00006203
  "   *-------------------------------------  NLIEUDEPLIV               00006302
  "          02 FTL077-NLIEUDEPLIV   PIC X(03).                         00006403
  "   *-------------------------------------  LIBRE                     00006502
  "          02 FTL077-FILLER        PIC X(02).                         00006603
      ***************************************************************** 00009000
                                                                                
