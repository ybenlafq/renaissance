      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * FIK001 : PRODUITS                                                       
      *                                                                         
      *-----------------------------------------------------------------00000140
      *    M O D I F I C A T I O N S / M A I N T E N A N C E S          00000150
      *-----------------------------------------------------------------00000160
      *  DATE       :                                                   00000170
      *  SIGNET     :                                                           
      *  AUTEUR     :                                                   00000180
      *  OBJET      :                                                   00000200
      *                                                                 00000200
      *-----------------------------------------------------------------00000210
       01  W-ENR-FIK001.                                                        
1          05 FIK001-NCODIC          PIC X(7).                                  
8          05 FIK001-NEAN            PIC X(13).                                 
21         05 FIK001-LREFFOURN       PIC X(20).                                 
41         05 FIK001-QHAUTEUR        PIC 9(4).                                  
45         05 FIK001-QLARGEUR        PIC 9(4).                                  
49         05 FIK001-QPROFONDEUR     PIC 9(4).                                  
53         05 FIK001-QHAUTEURDE      PIC 9(4).                                  
57         05 FIK001-QLARGEURDE      PIC 9(4).                                  
61         05 FIK001-QPROFONDEURDE   PIC 9(4).                                  
65         05 FIK001-DEFFET          PIC X(8).                                  
73         05 FIK001-LSTATCOMP       PIC X(3).                                  
76         05 FIK001-WOA             PIC X(1).                                  
77         05 FIK001-WACTIF          PIC X(1).                                  
78         05 FIK001-CMARQ           PIC X(5).                                  
83         05 FIK001-CFAM            PIC X(5).                                  
88         05 FIK001-SEGMENT         PIC X(5).                                  
AD5 93*    05 FILLER                 PIC X(58).                                 
AD5 93     05 FIK001-PREFTTC         PIC X(15).                                 
AD5108*    05 FILLER                 PIC X(43).                                 
MBEN00     05 FIK001-WSENSAPPRO      PIC X(01).                                 
MBEN00     05 FILLER                 PIC X(42).                                 
150   * FIN FIK001                                                              
      * FIK002 : FAMILLES                                                       
       01  W-ENR-FIK002 REDEFINES W-ENR-FIK001.                                 
1          05 FIK002-CFAM            PIC X(5).                                  
6          05 FIK002-LFAM            PIC X(20).                                 
26         05 FILLER                 PIC X(125).                                
150   * FIN FIK002                                                              
      * FIK003 : SEGMENTS                                                       
       01  W-ENR-FIK003 REDEFINES W-ENR-FIK001.                                 
1          05 FIK003-CFAM            PIC X(5).                                  
6          05 FIK003-NAGREGATED      PIC X(5).                                  
8          05 FIK003-LAGREGATED      PIC X(20).                                 
28         05 FILLER                 PIC X(120).                                
150   * FIN FIK003                                                              
      * FIK004 : MARQUES                                                        
       01  W-ENR-FIK004 REDEFINES W-ENR-FIK001.                                 
1          05 FIK004-CMARQ           PIC X(5).                                  
6          05 FIK004-LMARQ           PIC X(20).                                 
26         05 FILLER                 PIC X(125).                                
150   * FIN FIK004                                                              
      * FIK005 : FILIALES                                                       
       01  W-ENR-FIK005 REDEFINES W-ENR-FIK001.                                 
1          05 FIK005-NSOCIETE        PIC X(3).                                  
4          05 FIK005-LLIEU           PIC X(20).                                 
24         05 FILLER                 PIC X(127).                                
150   * FIN FIK005                                                              
      * FIK006 : MAGASINS                                                       
       01  W-ENR-FIK006 REDEFINES W-ENR-FIK001.                                 
1          05 FIK006-NSOCIETE        PIC X(3).                                  
4          05 FIK006-NLIEU           PIC X(3).                                  
7          05 FIK006-LLIEU           PIC X(20).                                 
27         05 FILLER                 PIC X(124).                                
150   * FIN FIK006                                                              
                                                                                
