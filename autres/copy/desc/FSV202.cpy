      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                 00001230
       01  WW-FSV202.                                                   00000890
         03  FSV202-NOMPGM           PIC  X(07) VALUE 'FSV202 ' .       00000900
         03  FSV202-APPDEST          PIC  X(03) .                               
         03  FSV202-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV202.                                                  00000890
           05  FSV202-CMKG             PIC  X(05).                      00000900
           05  FSV202-CVMKG            PIC  X(05).                      00000900
           05  FSV202-LVMKG            PIC  X(20).                      00001100
         03  FSV202-FILLEUR          PIC  X(159)  VALUE SPACES.         00001020
      *                                                                 00001230
                                                                                
