      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *** DESCRIPTION DU FICHIER FCX85  LONGUEUR = 69                   00000100
                                                                        00000200
       01  FCX85-ENREG.                                                 00000300
           02  FCX85-DSAISIE         PIC X(08).                         00000400
           02  FCX85-CPROTOUR        PIC X(05).                         00000500
           02  FCX85-CTOURNEE        PIC X(03).                         00000600
           02  FCX85-SOCLIEUVENTE.                                      00000610
               05 FCX85-NSOCIETE     PIC X(03).                         00000700
               05 FCX85-NLIEU        PIC X(03).                         00000800
               05 FCX85-NVENTE       PIC X(07).                         00000900
      *                                                                 00000910
           02  FCX85-CMODPAIMT       PIC X(05).                         00001000
           02  FCX85-PREGLTVTE       PIC S9(5)V99.                      00001100
           02  FCX85-LNOM            PIC X(20).                         00001200
           02  FCX85-DTOPE           PIC X(08).                         00001300
      ***                                                               00001400
                                                                                
