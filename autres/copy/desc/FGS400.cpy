      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FGS400-ENREG.                                                        
           03  FGS400-CLEF.                                                     
               05  FGS400-RAYON               PIC X(5).                         
               05  FGS400-COPER               PIC X(10).                        
               05  FGS400-NLIEUINTERNE        PIC X(3).                         
               05  FGS400-NSLIEUINTERNE       PIC X(3).                         
               05  FGS400-NTLIEUINTERNE       PIC X(5).                         
               05  FGS400-NLIEUEXTERNE        PIC X(3).                         
               05  FGS400-NSLIEUEXTERNE       PIC X(3).                         
               05  FGS400-NTLIEUEXTERNE       PIC X(5).                         
               05  FGS400-SENS                PIC X.                            
               05  FGS400-CMARQ               PIC X(5).                         
               05  FGS400-NCODIC              PIC X(7).                         
           03  FGS400-ORIGINE.                                                  
               05  FGS400-NLIEUORIG           PIC X(3).                         
               05  FGS400-NSSLIEUORIG         PIC X(3).                         
               05  FGS400-CLIEUTRTORIG        PIC X(5).                         
           03  FGS400-DESTINATION.                                              
               05  FGS400-NLIEUDEST           PIC X(3).                         
               05  FGS400-NSSLIEUDEST         PIC X(3).                         
               05  FGS400-CLIEUTRTDEST        PIC X(5).                         
           03  FGS400-NORIGINE                PIC X(7).                         
           03  FGS400-DMVT                    PIC X(8).                         
           03  FGS400-CFAM                    PIC X(5).                         
           03  FGS400-LREFFOURN               PIC X(20).                        
           03  FGS400-QMVT                    PIC S9(5) COMP-3.                 
           03  FGS400-PRMPRECYCL              PIC S9(7)V9(6) COMP-3.            
                                                                                
