      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FRB002-RECORD.                                               00000480
           05  FRB002-NFOURNISSEUR    PIC X(25).                        00000480
           05  FRB002-NBL             PIC 9(10).                        00000480
           05  FRB002-NCOMMANDE       PIC 9(7).                         00000480
           05  FRB002-DCOMMANDE       PIC X(8).                         00000480
           05  FRB002-NLOTPROD        PIC X(15).                        00000480
           05  FRB002-NBARQUETTE      PIC X(15).                        00000480
           05  FRB002-CPRODUIT        PIC 9(03).                        00000480
           05  FRB002-NSERIE          PIC 9(09).                        00000480
           05  FRB002-FILLER          PIC X(106).                       00000480
      *                                                                 00000600
                                                                                
