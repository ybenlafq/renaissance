      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 DSECT-FPV220.                                                         
          03 FPV220-NSOCIETE             PIC X(0003).                           
          03 FPV220-NLIEU.                                                      
             05 FPV220-NZONPRIX             PIC XX.                             
             05 FPV220-BBTE                 PIC X.                              
          03 FPV220-WSEQFAM              PIC X(0005).                           
          03 FPV220-CODPROF              PIC X(0010).                           
          03 FPV220-CMARKETING           PIC X(0005).                           
          03 FPV220-CVMARKETING          PIC X(0005).                           
          03 FPV220-PVENTE               PIC 9(0005).                           
          03 FPV220-CMARQ                PIC X(0005).                           
          03 FPV220-NCODIC               PIC X(0007).                           
          03 FPV220-CFAM                 PIC X(0005).                           
NT        03 FPV220-INTER                PIC X(0003).                           
          03 FPV220-VARINTER             PIC X(0001).                           
          03 FPV220-DATVARINTER          PIC X(0008).                           
          03 FPV220-LSTATCOMP            PIC X(0003).                           
          03 FPV220-LIBPROFIL            PIC X(0030).                           
          03 FPV220-LMARKETING           PIC X(0020).                           
          03 FPV220-LVMARKETING          PIC X(0020).                           
          03 FPV220-LREFFOURN            PIC X(0020).                           
          03 FPV220-VAR                  PIC X(0001).                           
          03 FPV220-OFFRE                PIC X(0001).                           
          03 FPV220-DATOFFRE             PIC X(0008).                           
NT        03 FPV220-FILLER               PIC X(0012).                           
                                                                                
