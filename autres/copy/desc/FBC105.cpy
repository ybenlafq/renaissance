      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC105  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST DEJA CHARGEE      *         
      *                                                               *         
      *    FBC105       : FICHIER DES ADRESSES                        *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:  350 C                                      *         
      *    LONGUEUR ENR.:  750 C  PASSAGE LE 29/07/2004               *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC105-ENREG.                                                        
           03  FBC105-CLE.                                                      
               05  FBC105-NSOCIETE            PIC  X(03).                       
               05  FBC105-NLIEU               PIC  X(03).                       
               05  FBC105-NVENTE              PIC  X(08).                       
           03  FBC105-CTITRENOM               PIC  X(05).                       
           03  FBC105-LNOM                    PIC  X(25).                       
           03  FBC105-LPRENOM                 PIC  X(15).                       
           03  FBC105-LIGNE-2.                                                  
               05  FBC105-FILLER-L2-1         PIC  X(04).                       
               05  FBC105-LESCALIER           PIC  X(03).                       
               05  FBC105-FILLER-L2-2         PIC  X(05).                       
               05  FBC105-LETAGE              PIC  X(03).                       
               05  FBC105-FILLER-L2-3         PIC  X(05).                       
               05  FBC105-LPORTE              PIC  X(03).                       
               05  FBC105-FILLER-L2-4         PIC  X(15).                       
           03  FBC105-LIGNE-3.                                                  
               05  FBC105-FILLER-L3-1         PIC  X(04).                       
               05  FBC105-LBATIMENT           PIC  X(03).                       
               05  FBC105-FILLER-L3-2         PIC  X(01).                       
               05  FBC105-LCMPAD1             PIC  X(30).                       
           03  FBC105-LIGNE-4.                                                  
               05  FBC105-CVOIE               PIC  X(06).                       
               05  FBC105-CTVOIE              PIC  X(05).                       
               05  FBC105-LNOMVOIE            PIC  X(21).                       
               05  FBC105-FILLER-L4           PIC  X(06).                       
           03  FBC105-LIGNE-5.                                                  
               05  FBC105-LCOMMUNE            PIC  X(32).                       
               05  FBC105-FILLER-L5           PIC  X(06).                       
           03  FBC105-LIGNE-6.                                                  
               05  FBC105-CPOSTAL             PIC  X(06).                       
               05  FBC105-LBUREAU             PIC  X(32).                       
           03  FBC105-WTYPEADR                PIC  X(01).                       
           03  FBC105-DVENTE                  PIC  X(08).                       
           03  FBC105-TELDOM                  PIC  X(15).                       
           03  FBC105-TELBUR                  PIC  X(15).                       
           03  FBC105-LPOSTEBUR               PIC  X(05).                       
           03  FBC105-FILLER0                 PIC  X(286).                      
           03  FBC105-DVERSION                PIC  X(08).                       
           03  FBC105-FILLER1                 PIC  X(108).                      
           03  FBC105-IDCLIENT                PIC  X(08).                       
           03  FBC105-CPAYS                   PIC  X(03).                       
           03  FBC105-NGSM                    PIC  X(15).                       
           03  FBC105-TILOT                   PIC  X(01).                       
           03  FBC105-CILOT                   PIC  X(08).                       
           03  FBC105-FILLER2                 PIC  X(20).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
