      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *===============================================================* 00000690
      *                 DESCRIPTION DE FSV010                         * 00000700
      *===============================================================* 00000710
      * FICHIER PRODUIT FSV010 BASE ARTICLE AVEC LE FAM NCG             00000880
      * DOIT ETRE IDENTIQUE A CELUI DE BSV010                           00000880
      * SINON PROBLEME ...                                              00000880
       01  WW-FSV010.                                                   00000890
         03  FSV010-NOMPGM           PIC  X(10) VALUE 'FSV010    ' .    00000900
         03  FSV010-CLE              PIC  X(01)  VALUE 'C' .            00000900
         03  W-FSV010.                                                  00000890
           05  FSV010-PRODUIT          PIC  X(07).                      00000900
           05  FSV010-CFAM             PIC  X(05).                      00001090
           05  FSV010-CMARQ            PIC  X(05).                      00001100
           05  FSV010-LREFFOURN        PIC  X(20).                      00000960
           05  FSV010-LEMBALLAGE       PIC  X(50).                      00001210
           05  FSV010-DCREATION        PIC  X(08).                      00001010
           05  FSV010-LSTATCOMP        PIC  X(03).                      00001220
           05  FSV010-CAPPRO           PIC  X(03).                              
           05  FSV010-DEFFET           PIC  X(08).                      00001050
           05  FSV010-CCHASSIS         PIC  X(07).                      00001040
           05  FSV010-CRAYON           PIC  X(05).                              
           05  FSV010-WDACEM           PIC  X(01).                              
           05  FSV010-FPRESTA          PIC  X(01).                              
           05  FSV010-CGROUPE          PIC  X(01).                              
           05  FSV010-CGARANTIE        PIC  X(05).                              
           05  FSV010-CTAUXTVA         PIC  X(05).                      00000970
           05  FSV010-NEAN             PIC  X(13).                      00000970
         03  FSV010-FILLER             PIC  X(12)   VALUE SPACES.       00000900
         03  FSV010-CLE-TRI      .                                      00000900
           05  FSV010-CLE-KAIDARA      PIC  X(01).                      00000970
           05  FSV010-CLE-FILLER       PIC  X(29)   VALUE SPACES.       00000970
      *                                                                 00001230
      * FICHIER PRODUIT BSV001 BASE ARTICLE AVEC LE FAM SAV             00000880
      *                                                                 00000880
       01  WW-FSV040.                                                   00000890
         03  W-FSV040.                                                  00000900
           05  FSV040-PRODUIT          PIC  X(07).                      00000900
           05  FSV040-CFAM             PIC  X(05).                      00001090
           05  FSV040-CMARQ            PIC  X(05).                      00001100
           05  FSV040-LREFFOURN        PIC  X(20).                      00000960
           05  FSV040-LEMBALLAGE       PIC  X(50).                      00001210
           05  FSV040-DCREATION        PIC  X(08).                      00001010
           05  FSV040-LSTATCOMP        PIC  X(03).                      00001220
           05  FSV040-CAPPRO           PIC  X(03).                              
           05  FSV040-DEFFET           PIC  X(08).                      00001050
           05  FSV040-CCHASSIS         PIC  X(07).                      00001040
           05  FSV040-CRAYON           PIC  X(05).                              
           05  FSV040-WDACEM           PIC  X(01).                              
           05  FSV040-FPRESTA          PIC  X(01).                              
           05  FSV040-CGROUPE          PIC  X(01).                              
           05  FSV040-CGARANTIE        PIC  X(05).                              
           05  FSV040-CTAUXTVA         PIC  X(05).                      00000970
           05  FSV040-NEAN             PIC  X(13).                      00000970
         03  FSV040-CFAMSAV            PIC  X(05).                      00000900
         03  FSV040-NPROD              PIC  X(15).                      00000900
         03  FSV040-FILLER             PIC  X(33)   VALUE SPACES.       00000900
      *                                                                 00001230
      *                                                                 00001230
                                                                                
