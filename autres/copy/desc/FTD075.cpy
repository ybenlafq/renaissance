      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00011001
      *  DESCRIPTION DE LA CLAUSE COPY DU FICHIER CATALOGUE EM        * 00012001
      *    ELECTRO-MENAGER DANS BIZSERVEUR                            * 00013001
      *    LONGUEUR : 140 CARACT.                                      *00014001
      ***************************************************************** 00015001
       01 FTD075-SUP.                                                   00020001
          03 FTD075-ENTETE.                                             00020101
JF0307*      05 FTD075-NSOC-SP    PIC X(03).                            00021001
JF0307*      05 FTD075-NLIEU-SP   PIC X(03).                            00022001
JF0307*      05 FTD075-ENT-SP     PIC X(01).                            00022101
      ***SP PERMET DE DISPATCH   MICRO *********                        00022201
             05 FTD075-NSOC-E     PIC X(03).                            00022301
             05 FTD075-NLIEU-E    PIC X(03).                            00022401
             05 FTD075-ENT-01     PIC X(01).                            00023101
             05 FTD075-TITRE      PIC X(29).                            00023201
             05 FTD075-ENT-02     PIC X(01).                            00023301
             05 FTD075-ENT-03     PIC X(01).                            00023401
             05 FTD075-ENT-04     PIC X(01).                            00023501
             05 FTD075-ENT-05     PIC X(01).                            00023601
             05 FTD075-ENT-06     PIC X(01).                            00023701
             05 FTD075-ENT-07     PIC X(01).                            00023801
JF0307*      05 FTD075-FILLER     PIC X(96).                            00028001
             05 FTD075-FILLER     PIC X(98).                            00028101
          03 FTD075-DETAIL REDEFINES FTD075-ENTETE.                     00029001
             05 FTD075-LREFO      PIC X(20).                            00030001
             05 FTD075-DET-01     PIC X(01).                            00030101
             05 FTD075-DESCRIPTIF.                                      00030201
                07 FTD075-CFAM-DES  PIC X(05).                          00030301
                07 FTD075-DET-02    PIC X(01).                          00030401
                07 FTD075-CMARQ     PIC X(05).                          00030501
                07 FTD075-DET-03    PIC X(01).                          00030601
                07 FTD075-LREFOURN  PIC X(20).                          00030701
                07 FTD075-DET-04    PIC X(01).                          00030801
             05 FTD075-LMARQ        PIC X(20).                          00030901
             05 FTD075-DET-05       PIC X(01).                          00031001
             05 FTD075-NCODIC       PIC X(7).                           00031101
             05 FTD075-DET-06       PIC X(01).                          00031201
             05 FTD075-CFAM         PIC X(5).                           00032001
             05 FTD075-DET-07       PIC X(01).                          00032101
JF0307*      05 FTD075-PRMP-S       PIC X(01).                          00032201
             05 FTD075-PRMP         PIC 9(7)V9(6).                      00033001
             05 FTD075-DET-08       PIC X(01).                          00033101
JF0307*      05 FTD075-PV-S         PIC X(01).                          00034201
             05 FTD075-PV           PIC 9(7)V9(2).                      00035001
             05 FTD075-DET-09       PIC X(01).                          00035101
             05 FTD075-ECO          PIC 9(5)V9(2).                      00036001
JF0307       05 FTD075-DET-10       PIC X(01).                          00037001
             05 FTD075-CTARIF       PIC X(5).                           00037101
JF0307*      05 FTD075-DET-10       PIC X(01).                          00037201
JF0307*      05 FTD075-FILLER       PIC X(17).                          00038001
             05 FTD075-FILLER       PIC X(13).                          00039001
                                                                                
