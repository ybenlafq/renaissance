      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FGV305-ENREG.                                                00000520
           03 FGV305-NSOCIETE      PIC  X(3).                           00000530
           03 FGV305-NLIEU         PIC  X(3).                           00000560
           03 FGV305-NZONPRIX      PIC  X(02).                          00000620
           03 FGV305-CGRPMAG       PIC  X(02).                          00000630
           03 FGV305-WSEQPRO       PIC  S9(7)  COMP-3.                  00000540
           03 FGV305-LSEQPRO       PIC  X(26).                          00000540
           03 FGV305-CRAYONFAM     PIC  X(5).                           00000550
           03 FGV305-WTYPE         PIC  X(1).                           00000550
           03 FILLER               PIC  X(3).                           00000550
           03 FGV305-SEMAINE-0.                                         00000570
              05 FGV305-C0-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-C0-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C0-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C0-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P0-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-P0-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P0-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P0-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
           03 FGV305-SEMAINE-1.                                         00000570
              05 FGV305-C1-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-C1-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C1-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C1-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P1-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-P1-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P1-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P1-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
           03 FGV305-SEMAINE-2.                                         00000570
              05 FGV305-C2-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-C2-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C2-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C2-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P2-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-P2-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P2-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P2-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
           03 FGV305-SEMAINE-3.                                         00000570
              05 FGV305-C3-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-C3-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C3-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-C3-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P3-QPIECES       PIC  S9(7)    COMP-3.             00000
              05 FGV305-P3-PCA           PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P3-PMTACHATS     PIC  S9(9)V99 COMP-3.             00000
              05 FGV305-P3-PMTPRIMES     PIC  S9(9)V99 COMP-3.             00000
                                                                                
