      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FGM01-DSECT.                                                 00000090
           02  FGM01-DATETRAIT                                          00000100
               PIC X(008).                                                      
           02  FGM01-CHEFPROD                                           00000120
               PIC X(0005).                                             00000130
           02  FGM01-CFAM                                               00000140
               PIC X(0005).                                             00000150
           02  FGM01-WSEQFAM                                            00000160
               PIC X(0005).                                             00000170
           02  FGM01-WTYPART                                            00000180
               PIC X(0001).                                             00000190
           02  FGM01-NCODIC1                                            00000200
               PIC X(0007).                                             00000210
           02  FGM01-NCODIC2                                            00000220
               PIC X(0007).                                             00000230
           02  FGM01-CMARKETIN1                                         00000240
               PIC X(0005).                                             00000250
           02  FGM01-CVMARKETIN1                                        00000260
               PIC X(0005).                                             00000270
           02  FGM01-LVMARKETIN1                                        00000280
               PIC X(0020).                                             00000290
           02  FGM01-CMARKETIN2                                         00000300
               PIC X(0005).                                             00000310
           02  FGM01-CVMARKETIN2                                        00000320
               PIC X(0005).                                             00000330
           02  FGM01-LVMARKETIN2                                        00000340
               PIC X(0020).                                             00000350
           02  FGM01-CMARKETIN3                                         00000360
               PIC X(0005).                                             00000370
           02  FGM01-CVMARKETIN3                                        00000380
               PIC X(0005).                                             00000390
           02  FGM01-LVMARKETIN3                                        00000400
               PIC X(0020).                                             00000410
           02  FGM01-QTRI                                               00000420
               PIC S9(7)V9(0002) COMP-3.                                00000430
           02  FGM01-CMARQ                                              00000440
               PIC X(0005).                                             00000450
           02  FGM01-LREFFOURN                                          00000460
               PIC X(0020).                                             00000470
           02  FGM01-CFAMELE                                            00000480
               PIC X(0005).                                             00000490
           02  FGM01-WLIBERTE                                           00000500
               PIC X(0001).                                             00000510
           02  FGM01-CASSORT                                            00000520
               PIC X(0005).                                             00000530
           02  FGM01-CAPPRO                                             00000540
               PIC X(0005).                                             00000550
           02  FGM01-CEXPO                                              00000560
               PIC X(0005).                                             00000570
           02  FGM01-STATCOMP                                           00000580
               PIC X(0003).                                             00000590
           02  FGM01-WSENSVTE                                           00000600
               PIC X(0001).                                             00000610
           02  FGM01-WSENSAPPRO                                         00000620
               PIC X(0001).                                             00000630
           02  FGM01-DEFSTATUT                                          00000640
               PIC X(0008).                                             00000650
           02  FGM01-WTLMELA                                            00000660
               PIC X(0001).                                             00000670
           02  FGM01-CVDESCRIPT1                                        00000680
               PIC X(0005).                                             00000690
           02  FGM01-CVDESCRIPT2                                        00000700
               PIC X(0005).                                             00000710
           02  FGM01-CVDESCRIPT3                                        00000720
               PIC X(0005).                                             00000730
           02  FGM01-QTXTVA                                             00000740
               PIC S9(3)V9(0002) COMP-3.                                00000750
           02  FGM01-QDELAIAPPRO                                        00000760
               PIC S9(3) COMP-3.                                        00000770
           02  FGM01-PRAR                                               00000780
               PIC S9(7)V9(0002) COMP-3.                                00000790
           02  FGM01-PRMP                                               00000800
               PIC S9(7)V9(0002) COMP-3.                                00000810
           02  FGM01-PMBUZ1                                             00000820
               PIC S9(7)V9(0002) COMP-3.                                00000830
           02  FGM01-CZPRIXZ1                                           00000840
               PIC X(0002).                                             00000850
           02  FGM01-PSTDTTCZ1                                          00000860
               PIC S9(7)V9(0002) COMP-3.                                00000870
           02  FGM01-PCOMMZ1                                            00000880
               PIC S9(5) COMP-3.                                        00000890
           02  FGM01-QNBREART                                           00000900
               PIC S9(7) COMP-3.                                        00000910
           02  FGM01-QTYPLIEN                                           00000920
               PIC S9(3) COMP-3.                                        00000930
           02  FGM01-QSTOCKDEP                                          00000940
               PIC S9(7) COMP-3.                                        00000950
           02  FGM01-QSTOCKMAG                                          00000960
               PIC S9(7) COMP-3.                                        00000970
           02  FGM01-DSYST                                              00000980
               PIC S9(13) COMP-3.                                       00000990
           02  FGM01-PCOMMARTZ1                                         00001000
               PIC S9(5)V9(0002) COMP-3.                                00001010
           02  FGM01-PCOMMVOLZ1                                         00001020
               PIC S9(5)V9(0002) COMP-3.                                00001030
           02  FGM01-WSENSNF                                            00001040
               PIC X(0001).                                                     
           02  FGM01-PSTDTTCZ2                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  FGM01-WIMPERATIF                                                 
               PIC X(0001).                                                     
           02  FGM01-WBLOQUE                                                    
               PIC X(0001).                                             00001050
                                                                                
