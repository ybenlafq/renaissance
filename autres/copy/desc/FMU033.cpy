      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DES MUTATIONS                   *        
      *----------------------------------------------------------------*        
       01  DSECT-FMU033.                                                        
           05 CHAMPS-FMU033.                                                    
              10 FMU033-CFAM               PIC X(05).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU033-CMARQ              PIC X(05).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU033-NCODIC             PIC X(07).                           
              10 FILLER                    PIC X VALUE ';'.                     
              10 FMU033-LREFFOURN          PIC X(20).                           
           05 FMU033-QTES.                                                      
              10 FMU033-OCC OCCURS 11.                                          
                 15 FMU033-PV1                PIC X(01).                        
                 15 FMU033-QTE                PIC Z(09).                        
           05 FMU033-QTES-R REDEFINES FMU033-QTES.                              
              10 FMU033-OCC-R OCCURS 11.                                        
                 15 FMU033-PV1-R              PIC X(01).                        
                 15 FMU033-QTE-R              PIC X(09).                        
       01  FMU033-LIBELLES.                                                     
           05 FILLER                       PIC X(04) VALUE 'CFAM'.              
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(05) VALUE 'CMARQ'.             
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(06) VALUE 'NCODIC'.            
           05 FILLER                       PIC X(01) VALUE ';'.                 
           05 FILLER                       PIC X(09) VALUE 'LREFFOURN'.         
           05 FMU033-MUTS OCCURS 11.                                            
              10 FMU033-PV2                PIC X(01) VALUE SPACES.              
              10 FMU033-INF                PIC X(01) VALUE SPACES.              
              10 FMU033-MUT                PIC X(07) VALUE SPACES.              
              10 FMU033-SUP                PIC X(01) VALUE SPACES.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-FMU033-LONG               PIC S9(4) COMP  VALUE +150.          
      *                                                                         
      *--                                                                       
       01  DSECT-FMU033-LONG               PIC S9(4) COMP-5  VALUE +150.        
                                                                                
      *}                                                                        
