      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************    00001000
      * ****************       DESCRIPTION     ******************* *    00002000
      *                 *******   FEX001  *******                  *    00002100
      *   FICHIER DES ARTICLES SELECTIONNES POUR LES TRAITEMENTS   *    00002200
      *           NECESSAIRES A L'EDITION DE STATISTIQUES          *    00002300
      *------------------------------------------------------------*    00002400
      *                  FICHIER SAM, LONGUEUR 600                 *    00002500
      **************************************************************    00002600
      *                                                                 00002700
      * AJOUT CAPPRO POSITION 583 ; LONGUEUR 5                                  
      * POUR PRISE EN COMPTE DES DONNEES DES ARTICLES EPUISES                   
      *                                                                         
      * 26/04/2000 - AL                                                         
      *                                                                         
      * AJOUT FLAGPSE POSITION 586 ; LONGUEUR 1                                 
      * PERMET DE DETERMINER LA QUANTITE " PSABLE" PAR PRODUIT                  
      *                                                                         
      * 20/07/2000 - AL                                                         
      *                                                                         
      * AJOUT DU CODE ASSORTIMENT EN POSITION 587                               
      *                                                                         
      * 26/09/2000 - AL                                                         
      *                                                                         
       01  ENR-FEX001.                                                  00002800
      *---1---------------------------------  CODE SOCIETE              00002901
             02 FEX001-NSOCIETE      PIC X(03).                         00003001
      *---4---------------------------------  CODE TRI                  00003101
             02 FEX001-NTRI          PIC S9(05) COMP-3.                 00003202
      *-------------------------------------  ZONE RUPTURE              00003301
             02 FEX001-ZRUPTURE.                                        00003401
      *---7---------------------------------  LIBELLE RUPTURE           00003702
                03 FEX001-LRUPTURE   PIC X(26) OCCURS 5.                00003802
      *-------------------------------------  ZONE AGREGAT PRODUIT      00003902
             02 FEX001-ZAGREGPRO.                                       00004002
      *-137---------------------------------  LIBELLE AGREGAT PRODUIT   00004102
                03 FEX001-LAGREGPRO  PIC X(10) OCCURS 4.                00004202
      *-177---------------------------------  ORDRE SEQUENCE PRODUIT    00004302
             02 FEX001-WSEQPRO       PIC S9(07) COMP-3.                 00004402
      *-181---------------------------------  ORDRE SEQUENCE LIEU       00004502
             02 FEX001-WSEQLIEU      PIC S9(07) COMP-3.                 00004602
      *-------------------------------------  ZONE CONSOLIDATION LIEU   00004702
             02 FEX001-ZCONSOLLIEU.                                     00004803
      *-185---------------------------------  CODE CONSOLIDATION LIEU   00004902
                03 FEX001-CCONSOLLIEU PIC X(10) OCCURS 3.               00005002
      *-215---------------------------------  DATE OUVERTURE LIEU       00005102
             02 FEX001-DOUVLIEU      PIC X(08).                         00005202
      *-------------------------------------  ZONE DONNEES NUMERIQUES   00005302
             02 FEX001-ZDONNEESNUM.                                     00005403
      *-223---------------------------------  VALEURS DONNEES NUMERIQUES00005502
                03 FEX001-QDONNEESNUM PIC S9(13)V99 COMP-3 OCCURS 42.   00005602
      *-559---------------------------------  NUMERO ARTICLE            00005700
             02 FEX001-NCODIC        PIC X(07).                         00006000
      *-566---------------------------------  DATE DE TRAITEMENT        00009600
             02 FEX001-DMVT          PIC X(08).                         00009700
      *-574---------------------------------  CODE LIEU                 00009710
             02 FEX001-NLIEU         PIC X(03).                         00009720
      *-577---------------------------------  CODE RAYON OU FAMILLE     00009730
             02 FEX001-CRAYONFAM     PIC X(05).                         00009740
      *-582---------------------------------  FLAG RAYON OU FAMILLE     00009750
             02 FEX001-WRAYONFAM     PIC X(01).                         00009760
      *-583---------------------------------  CODE APPROVISIONNEMENT    00009800
             02 FEX001-CAPPRO        PIC X(03).                         00009904
      *-586---------------------------------  FLAG PSE                  00009800
             02 FEX001-FLAGPSE       PIC X(1).                          00009904
      *-587---------------------------------  CODE ASSORT               00009800
             02 FEX001-CASSORT       PIC X(5).                          00009904
      *-592---------------------------------  ZONE LIBRE                00009800
             02 FEX001-FILLER        PIC X(9).                          00009904
      **L=600********************************************************** 00010000
                                                                                
