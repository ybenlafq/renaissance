      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 FRB007-RECORD.                                                        
      *    DATE D'EXTRACTION EN SSAAMMJJ                                        
           05 FRB007-DATEXTRACT               PIC X(08).                        
      *    IDENTIFIANT LIEU                                                     
           05 FRB007-IDDNS                    PIC X(10).                        
      *    N� DE CONTRAT                                                        
           05 FRB007-NCONTRATINS              PIC X(18).                        
      *    N� DE VENTE                                                          
           05 FRB007-NVENTEINS                PIC X(13).                        
      *    N� D'IDENTIFIANT EXTERNE                                             
           05 FRB007-IDEXTINS                 PIC X(20).                        
      *    TYPE D'INSTALLATION                                                  
           05 FRB007-TYPEINS                  PIC X(05).                        
      *    N� INTERVENTION                                                      
           05 FRB007-NINTERINS                PIC X(20).                        
      *    MONTANT DE L'INTERVENTION                                            
           05 FRB007-MNTINS                   PIC S9(7)V9(02).                  
      *    DATE DE TOP� EN SSAAMMJJ                                             
           05 FRB007-DATEINS                  PIC X(08).                        
      *    CODE TARIF PRINCIPAL                                                 
           05 FRB007-CTARIF                   PIC X(05).                        
      *    FLAG TOPAGE NASC O = OK N = KO                                       
           05 FRB007-WTOPAGE                  PIC X(01).                        
      *    FLAG RETOUR O = NI�ME INTER , N = 1I�RE INTER                        
           05 FRB007-WINTER                   PIC X(01).                        
      *                                                                         
           05 FRB007-FILLER                   PIC X(31).                        
                                                                                
