      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************           
      * FICHIER STANDARD A ALIMENTE PAR LES APPLICATIONS DE GESTION *           
      *                                                             *           
      *      - DSECT COBOL                                          *           
      *      - NOM = FFTI00                                         *           
      *                             RECSIZE = 400                   *           
      ***************************************************************           
      *                                                                         
       01  FFTI00-ENR.                                                          
      ***************************************************************           
      *   CLE DU FICHIER : FFTI00-CLE                               *           
      ***************************************************************           
           02  FFTI00-CLE.                                                      
   1           03  FFTI00-CINTERFACE             PIC X(05).                     
   6           03  FFTI00-NPIECE                 PIC X(10).                     
  16           03  FFTI00-NSEQ                   PIC 9(05) COMP-3.              
  19       02  FFTI00-CTYPOPER                   PIC X(05).                     
  24       02  FFTI00-CNATOPER                   PIC X(05).                     
  29       02  FFTI00-ANALYTIQUE  OCCURS  6.                                    
  29           03  FFTI00-CRITERE                PIC X(05).                     
  59       02  FFTI00-WSENS                      PIC X(01).                     
  60       02  FFTI00-TAUXTVA                    PIC 9(3)V99 COMP-3.            
  63       02  FFTI00-WGROUPE                    PIC X(01).                     
  64       02  FFTI00-MONTANT                    PIC 9(13)V99  COMP-3.          
  72       02  FFTI00-NCLIENT                    PIC X(15).                     
  87       02  FFTI00-NFOUR                      PIC X(15).                     
 102       02  FFTI00-NSOC-E                     PIC X(03).                     
 105       02  FFTI00-NLIEU-E                    PIC X(03).                     
 108       02  FFTI00-NSOC-R                     PIC X(03).                     
 111       02  FFTI00-NLIEU-R                    PIC X(03).                     
 114       02  FFTI00-NTIERS                     PIC X(15).                     
 129       02  FFTI00-NOPERATION                 PIC X(15).                     
 144       02  FFTI00-DOPERATION                 PIC X(08).                     
 152       02  FFTI00-DECHEANCE                  PIC X(08).                     
 160       02  FFTI00-LETTRAGE4                  PIC X(15).                     
 175       02  FFTI00-LETTRAGE5                  PIC X(15).                     
 190       02  FFTI00-LETTRAGESIM                PIC X(40).                     
 230       02  FFTI00-CDEVISE                    PIC X(03).                     
 233       02  FFTI00-DTRAITEMENT                PIC X(08).                     
 241       02  FFTI00-LIBELLE                    OCCURS 5.                      
               03  FFTI00-LMVT                   PIC X(25).                     
 366       02  FFTI00-CPAYS                      PIC X(03).                     
 369       02  FFTI00-WTVA                       PIC X(01).                     
 370       02  FFTI00-REFDOC                     PIC X(12).                     
 382       02  FFTI00-DDOC                       PIC X(08).                     
 390       02  FILLER                            PIC X(11).                     
                                                                                
