      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *================================================================*        
      *   FICHIER DES LIGNES DE MUTATION FILIALE FFI011 LG=110         *        
      *================================================================*        
       01  FFI011-ENREG.                                                        
           02  FFI011-NSOC-TRI           PIC X(003).                            
           02  FFI011-NLIEU-TRI          PIC X(003).                            
           02  FFI011-CTYP-TRI           PIC X(001).                            
           02  FFI011-CTYPSOC            PIC X(003).                            
           02  FFI011-CTYPE              PIC X(005).                            
           02  FFI011-NMUTATION          PIC X(007).                            
           02  FFI011-NSOCORIG           PIC X(003).                            
           02  FFI011-NLIEUORIG          PIC X(003).                            
           02  FFI011-NSOCDEST           PIC X(003).                            
           02  FFI011-NLIEUDEST          PIC X(003).                            
           02  FFI011-CTYPINIT           PIC X(001).                            
           02  FFI011-CTYPFIN            PIC X(001).                            
           02  FFI011-WSENSMVT           PIC X(001).                            
           02  FFI011-WTRAITE            PIC X(001).                            
           02  FFI011-CFAM               PIC X(005).                            
           02  FFI011-NCODIC             PIC X(007).                            
           02  FFI011-LREFFOURN          PIC X(020).                            
           02  FFI011-QCDEPREL           PIC S9(05) COMP-3.                     
           02  FFI011-DVALID.                                                   
               10 SS                     PIC XX.                                
               10 AA                     PIC XX.                                
               10 MM                     PIC XX.                                
               10 JJ                     PIC XX.                                
           02  FFI011-QMUTEE             PIC S9(05) COMP-3.                     
           02  FFI011-DMODIF.                                                   
               10 SS                     PIC XX.                                
               10 AA                     PIC XX.                                
               10 MM                     PIC XX.                                
               10 JJ                     PIC XX.                                
           02  FFI011-QECART             PIC S9(05) COMP-3.                     
           02  FFI011-CMARQ              PIC X(005).                            
           02  FFI011-WSEQFAM            PIC S9(05) COMP-3.                     
           02  FILLER                    PIC X(007).                            
                                                                                
