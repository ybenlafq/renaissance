      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                      DESCRIPTION                              *         
      *                         FGD11                                 *         
      *     FICHIER DES DEMANDES DE DESTOCKAGE 8 SEMAINES "RACK"      *         
      *               FICHIER SAM DE LONGUEUR : 41                    *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
       01  ENR-FGD11.                                                           
           02  FGD11-NSOCDEPOT             PIC X(03).                           
           02  FGD11-NDEPOT                PIC X(03).                           
           02  FGD11-NCODIC                PIC X(07).                           
           02  FGD11-NBLIGNE               PIC S9(5) COMP-3.                    
           02  FGD11-NBJOUR                PIC S9(5) COMP-3.                    
           02  FGD11-QTRAITEE              PIC S9(9) COMP-3.                    
           02  FGD11-DJOUR                 PIC X(08).                           
           02  FGD11-QCDEDEM-P             PIC S9(5) COMP-3.                    
           02  FGD11-QCDEDEM-S             PIC S9(5) COMP-3.                    
           02  FILLER                      PIC X(03).                           
                                                                                
