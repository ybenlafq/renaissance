      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : STAT VENTES FILIALES    * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK030                                        * 00100600
      *        - LONGUEUR CLE   =  16                                 * 00101000
      *        - LONGUEUR ENREG =  60                                 * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK030-ENR.                                                          
         03  FGK030-CLE.                                                        
           05  FGK030-NSOCVTE       PIC X(03).                                  
           05  FGK030-NCODIC        PIC X(07).                                  
           05  FGK030-DMOIS-TRT     PIC X(06).                                  
         03  FGK030-DONNEES.                                                    
           05  FGK030-PVTE-FIC      PIC S9(09)V99 COMP-3.                       
           05  FGK030-PVTE-NET      PIC S9(09)V99 COMP-3.                       
           05  FGK030-VOL-VTE       PIC S9(09)    COMP-3.                       
           05  FGK030-QNB-VTE       PIC S9(09)    COMP-3.                       
           05  FGK030-PRMP          PIC S9(09)V99 COMP-3.                       
           05  FGK030-MTACHAT       PIC S9(09)V99 COMP-3.                       
           05  FILLER               PIC X(10).                                  
      *                                                                         
                                                                                
