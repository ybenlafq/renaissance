      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *        FICHIER TOPES J+1                                      * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FTS60                                         * 00100600
      *        - LONGUEUR CLE = 26                                    * 00101000
      *        - LONGUEUR ENREG = 120                                 * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      *  01.99       - TRAITEMENT HOST CAISSES  -                    *  00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FTS60-ENR.                                                           
           02  FTS60-CLE.                                                       
      * CODE SOCIETE                                                            
             03  FTS60-CLE-NSOC                     PIC X(3).                   
      * CODE MAGASIN                                                            
             03  FTS60-CLE-NLIEU                    PIC X(3).                   
      * CODE ZONE DE PRIX                                                       
             03  FTS60-CLE-NZPRIX                   PIC X(2).                   
      * NUMERO DE CAISSE                                                        
             03  FTS60-CLE-NCAISS                   PIC X(1).                   
      * NUMERO DE TERMINAL                                                      
             03  FTS60-CLE-NTERM                    PIC X(4).                   
      * NUMERO DE VENTE                                                         
             03  FTS60-CLE-NTRAN                    PIC X(8).                   
      * DATE DE VENTE                                                           
             03  FTS60-CLE-DVENT                    PIC X(8).                   
      * CODE MODE DE DELIVRANCE                                                 
             03  FTS60-CLE-CMODD                    PIC X(1).                   
      * CODE TVA                                                                
           02  FTS60-CTVA                           PIC X(5).                   
      * CODE ARTICLE                                                            
           02  FTS60-NCODIC                         PIC X(7).                   
      * CODE FAMILLE                                                            
           02  FTS60-CFAM                           PIC X(5).                   
      * LIBELLE MARQUE                                                          
           02  FTS60-CMARQ                          PIC X(20).                  
      * LIBELLE ARTICLE                                                         
           02  FTS60-LREF                           PIC X(20).                  
      * TYPE DE CONDITIONNEMENT                                                 
           02  FTS60-CCOND                          PIC X(5).                   
      * CODE VENDEUR                                                            
           02  FTS60-CVEND                          PIC X(7).                   
      * DATE DU JOUR                                                            
           02  FTS60-DJOUR                          PIC X(8).                   
      * QUANTITE DE PIECES                                                      
           02  FTS60-QPIECE                         PIC S9(5)    COMP-3.        
      * PRIX DE VENTE TTC                                                       
           02  FTS60-PVTTC                          PIC S9(7)V99 COMP-3.        
      * PRIX DE VENTE REMISE COMPRISE                                           
           02  FTS60-PVSR                           PIC S9(7)V99 COMP-3.        
                                                                                
