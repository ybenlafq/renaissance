      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *================================================================*00001180
      *       DESCRIPTION DE LA ZONE D'ENREGISTREMENT (FICOUT)         *00001190
      *================================================================*00001200
       01  FGV01-DSECT.                                                 00001220
001         03  F-NSOCIETE            PIC  X(03).                       01230   
004         03  F-WSEQPRO             PIC S9(07)    COMP-3.             01240   
008         03  F-LSEQPRO             PIC  X(26).                       01250   
034         03  F-CRAYONFAM           PIC  X(05).                       01250   
039         03  F-WTYPE               PIC  X(01).                       01260   
            03  F-SEMAINE-1.                                            01280   
040             04  F-SEM-1-NB        PIC S9(07)    COMP-3.             01290   
044             04  F-SEM-1-CA        PIC S9(09)V99 COMP-3.             01300   
050             04  F-SEM-1-MT        PIC S9(09)V99 COMP-3.             01310   
056             04  F-SEM-PRIM        PIC S9(09)V99 COMP-3.             01320   
062             04  F-SEM-CONB        PIC S9(07)    COMP-3.             01330   
066             04  F-SEM-COCA        PIC S9(09)V99 COMP-3.             01340   
072             04  F-SEM-COMT        PIC S9(09)V99 COMP-3.             01350   
            03  F-SEMAINE-2.                                            01360   
078             04  F-SEM-2-NB        PIC S9(07)    COMP-3.             01370   
082             04  F-SEM-2-CA        PIC S9(09)V99 COMP-3.             01380   
088             04  F-SEM-2-MT        PIC S9(09)V99 COMP-3.             01390   
            03  F-CUMUL-1.                                              01400   
094             04  F-CUM-1-NB        PIC S9(07)    COMP-3.             01410   
098             04  F-CUM-1-CA        PIC S9(09)V99 COMP-3.             01420   
104             04  F-CUM-1-MT        PIC S9(09)V99 COMP-3.             01430   
110             04  F-CUM-PRIM        PIC S9(09)V99 COMP-3.             01440   
116             04  F-CUM-CONB        PIC S9(07)    COMP-3.             01450   
120             04  F-CUM-COCA        PIC S9(09)V99 COMP-3.             01460   
126             04  F-CUM-COMT        PIC S9(09)V99 COMP-3.             01470   
            03  F-CUMUL-2.                                              01480   
132             04  F-CUM-2-NB        PIC S9(07)    COMP-3.             01490   
136             04  F-CUM-2-CA        PIC S9(09)V99 COMP-3.             01500   
142             04  F-CUM-2-MT        PIC S9(09)V99 COMP-3.             01510   
            03  F-SEMAINE-1-OA.                                         01280   
148             04  F-SEM-1-OANB      PIC S9(07)    COMP-3.             01290   
152             04  F-SEM-1-OACA      PIC S9(09)V99 COMP-3.             01300   
158             04  F-SEM-1-OAMT      PIC S9(09)V99 COMP-3.             01310   
            03  F-CUMUL-1-OA.                                           01400   
164             04  F-CUM-1-OANB      PIC S9(07)    COMP-3.             01410   
168             04  F-CUM-1-OACA      PIC S9(09)V99 COMP-3.             01420   
174             04  F-CUM-1-OAMT      PIC S9(09)V99 COMP-3.             01430   
180         03  FILLER                PIC X.                            01400   
      * LRECL=180                                                       01520   
                                                                                
