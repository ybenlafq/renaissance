      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      *  DSECT FICHIER FBHA30 : FICHIER DES ARTICLES CREES                      
      *  OU AYANT CHANGE DE STATUT DURANT LA SEMAINE PRECEDENTE                 
      *    (PROG. BHA030 ET BHA031)   LG =150                                   
      *                                                                         
           02  FHA30-CFAM       PIC X(05).                                      
           02  FHA30-WSEQFAM    PIC S9(5) COMP-3.                               
           02  FHA30-NCODIC     PIC X(07).                                      
           02  FHA30-LREFFOURN  PIC X(20).                                      
           02  FHA30-CMARQ      PIC X(05).                                      
           02  FHA30-DCREATION  PIC X(08).                                      
           02  FHA30-WCREATION  PIC X(01).                                      
           02  FHA30-DMAJ       PIC X(08).                                      
           02  FHA30-DEFSTATUT  PIC X(08).                                      
           02  FHA30-WTLMELA    PIC X(01).                                      
           02  FHA30-WDACEM     PIC X(01).                                      
           02  FHA30-WLCONF     PIC X(01).                                      
           02  FHA30-WSENSVTE   PIC X(01).                                      
           02  FHA30-CGESTVTE   PIC X(03).                                      
           02  FHA30-CASSORT    PIC X(05).                                      
           02  FHA30-CAPPRO     PIC X(05).                                      
           02  FHA30-LSTATCOMP  PIC X(03).                                      
           02  FHA30-CEXPO      PIC X(05).                                      
           02  FHA30-PSTDTTC    PIC S9(7)V99 COMP-3.                            
           02  FHA30-DATDEB     PIC X(08).                                      
           02  FHA30-DATFIN     PIC X(08).                                      
           02  FHA30-LCOMMENT   PIC X(20).                                      
           02  FHA30-PERIOD     PIC X(12).                                      
           02  FHA30-DSYST      PIC S9(013) COMP-3.                             
                                                                                
