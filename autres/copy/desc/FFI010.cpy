      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      ******* FICHIER FACTURACTION (PROJET FACTURATION INTERNE)********         
      *****************************************************************         
      *****                                                       *****         
      ***** MODIF ANNE DSA005 - LE 11/12/96                       *****         
      ***** POUR LES BESOINS DU SRP CAPROFEM, NECESSITE DE STOCKER*****         
      ***** LE PRAK                                               *****         
      *****                                                       *****         
      ***** --> AJOUT DE CETTE ZONE DANS LE FICHIER               *****         
      *****    NB : LA LONGUEUR DU FICHIER N'EST PAS MODIFIEE     *****         
      *****         REDUCTION DU FILLER                           *****         
      *****                                                       *****         
      ***** 03/97 : AJOUTER LA ZONE DOPER, ISSUE DE FFI005        *****         
      *****         SERVIRA A CHARGER RTFI10-DCOMPTA              *****         
      *****                                                       *****         
      *****************************************************************         
       01  FFI010-DSECT.                                                        
           10  FFI010-DMOUV          PIC  X(8).                                 
           10  FFI010-NSOC           PIC  X(3).                                 
           10  FFI010-NLIEU          PIC  X(3).                                 
           10  FFI010-CSENS          PIC  X(1).                                 
           10  FFI010-CTYPE          PIC  X(5).                                 
           10  FFI010-NDOC           PIC  X(7).                                 
           10  FFI010-CODIC          PIC  X(7).                                 
           10  FFI010-WSEQFAM        PIC  S9(5)      COMP-3.                    
           10  FFI010-QTE            PIC  S9(5)      COMP-3.                    
           10  FFI010-PRMP           PIC  S9(7)V9(6) COMP-3.                    
           10  FFI010-PCF            PIC  S9(7)V9(6) COMP-3.                    
           10  FFI010-CTAUXTVA       PIC  X(5).                                 
           10  FFI010-CFRGEST        PIC  S9(3)V99   COMP-3.                    
           10  FFI010-WDACEM         PIC  X(1).                                 
           10  FFI010-PRAK           PIC  S9(7)V9(6) COMP-3.                    
0397       10  FFI010-DOPER          PIC  X(8).                                 
0397  *    10  FILLER                PIC  X(22).                                
MBEN00     10  FFI010-WIMPORTE       PIC  X(1).                                 
MBEN00     10  FFI010-PCONTR         PIC  S9(7)V9(6) COMP-3.                    
MBEN00*    10  FILLER                PIC  X(14).                                
ECOM       10  FFI010-WIMPORT-ECOM   PIC  X(1).                                 
ECOM       10  FFI010-PECOM          PIC  S9(5)V9(2) COMP-3.                    
ECOM       10  FILLER                PIC  X(9).                                 
                                                                                
