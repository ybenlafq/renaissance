      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * -------------------------------------------------------- *              
      * FICHIER ENTREE DU BCE652                                 *              
      * CONTIENT L'EXTRACTION DE LA RTCE52                       *              
      * -------------------------------------------------------- *              
       01 :FCE652O:-ENREG.                                                      
          05 :FCE652O:-MAGASIN.                                                 
             15 :FCE652O:-NSOCIETE             PIC X(03).                       
             15 :FCE652O:-NMAG                 PIC X(03).                       
          05 :FCE652O:-NCODIC1                 PIC X(07).                       
          05 :FCE652O:-PRIX                    PIC X(01).                       
          05 :FCE652O:-FAMCLI                  PIC X(20).                       
          05 :FCE652O:-LREFFOURN               PIC X(10).                       
          05 :FCE652O:-NENTCDE                 PIC X(05).                       
          05 :FCE652O:-COBJET                  PIC X(05).                       
          05 :FCE652O:-NCODIC2                 PIC X(07).                       
          05 :FCE652O:-FAMCLI2                 PIC X(30).                       
          05 :FCE652O:-PRIX2                   PIC X(01).                       
                                                                                
