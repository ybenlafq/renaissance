      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  WW-FSV016.                                                   00000890
         03  FSV016-NOMPGM  .                                           00000900
           05  FSV016-NOMPROG          PIC  X(10) VALUE 'FSV016    ' .  00000900
         03  FSV016-CLE              PIC  X(01) VALUE 'C' .             00000900
         03  W-FSV016.                                                  00000890
           05  FSV016-CCHASSIS         PIC  X(07).                      00000900
           05  FSV016-LCHASSIS         PIC  X(20).                      00001100
           05  FSV016-CGROUPE          PIC  X(05).                      00001100
           05  FSV016-LGROUPE          PIC  X(20).                      00001100
         03  FSV016-FILLEUR          PIC  X(148)  VALUE SPACES.         00001020
      *                                                                 00001320
                                                                                
