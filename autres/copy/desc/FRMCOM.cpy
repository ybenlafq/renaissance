      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * DEFINITION DU FICHIER COMMUN POUR LE NOUVEAU REAPROVISIONNEMENT         
      * MAGASIN                                                                 
      *****************************************************************         
OB98  * OLIVIER BES - SI2 - KESA : CONSOLIDATION DONNEES ACHATS VENTES          
      *****************************************************************         
       01 DSECT-FRMCOM.                                                         
          02 FRMCOM-DONNEES.                                                    
             05 FRMCOM-NCODIC       PIC X(07).                                  
             05 FRMCOM-CFAM         PIC X(05).                                  
             05 FRMCOM-LAGREGATED   PIC X(20).                                  
OB98         05 FRMCOM-TYPAGREG     PIC X(01).                                  
OB98         05 FRMCOM-LFAM         PIC X(20).                                  
OB98         05 FRMCOM-CMARKETING   PIC X(05).                                  
OB98         05 FRMCOM-LMARKETING   PIC X(20).                                  
OB98  *   02 FRMCOM-LIBRE           PIC X(48).                                  
OB98      02 FRMCOM-LIBRE           PIC X(02).                                  
                                                                                
