      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FGM065-DSECT.                                                        
           02 FGM065-NCODIC            PIC X(07).                               
           02 FGM065-NSOCIETE          PIC X(03).                               
           02 FGM065-NLIEU             PIC X(03).                               
           02 FGM065-NZONPRIX          PIC X(02).                               
           02 FGM065-QVENTE1S          PIC S9(7)      COMP-3.                   
           02 FGM065-PCA1S             PIC S9(11)V99  COMP-3.                   
           02 FGM065-PMBU1S            PIC S9(11)V99  COMP-3.                   
           02 FGM065-QVENTE4S          PIC S9(7)      COMP-3.                   
           02 FGM065-PCA4S             PIC S9(11)V99  COMP-3.                   
           02 FGM065-PMBU4S            PIC S9(11)V99  COMP-3.                   
           02 FGM065-PMTPRIMES         PIC S9(11)V99  COMP-3.                   
           02 FGM065-QVENTES-4S        OCCURS 4.                                
              05 FGM065-QVENTES        PIC S9(7)      COMP-3.                   
           02 FGM065-FILLER            PIC X(26).                               
                                                                                
