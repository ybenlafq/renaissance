      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * NEM : DSECT DU FICHIER FTF600 MAJ A ENVOYER EN MAGASIN       *  00000020
      ****************************************************************  00000030
      *                                                                 00000040
      * LONGUEUR 385                                                    00000050
      *                                                                 00000060
       01  TF600-ENREG.                                                 00000070
      * NOM DU FICHIER A METTRE A JOUR SUR 36 MAGASIN                   00000080
           02  TF600-FICHIER       PIC X(10).                           00000090
      * DATE DE MISE DE CREATION DU MOUVEMENT SSAAMMJJ                  00000100
           02  TF600-DATMAJ        PIC X(08).                           00000110
      * HEURE DE MISE DE CREATION DU MOUVEMENT SSAAMMJJ                 00000120
           02  TF600-HEUREMAJ      PIC X(06).                           00000130
      * SOCIETE CONCERNEE                                               00000140
           02  TF600-SOC           PIC X(03).                           00000150
      * ZONE    CONCERNEE                                               00000160
           02  TF600-ZONE          PIC X(02).                           00000170
      * MAGASIN CONCERNE                                                00000180
           02  TF600-MAG           PIC X(03).                           00000190
      * CODE MISE A JOUR 'S' = ANNULATION , 'C' = CREATION OU 'M' = MAJ 00000200
           02  TF600-MAJ           PIC X(01).                           00000210
      * CONTENU DE L'ENREGISTREMENT TRAITE                              00000220
           02  TF600-ENRG          PIC X(352).                          00000230
      * CONTENU DE L'ENREGISTREMENT TRAITE                              00000240
           02  TF600-DETAIL REDEFINES TF600-ENRG.                       00000250
              05  TF600-CLE           PIC X(100).                       00000260
              05  TF600-DATA          PIC X(252).                       00000270
       01  TF600-ENREGL REDEFINES TF600-ENREG.                          00000850
              05  TF600-CLEL          PIC X(133).                       00000860
              05  TF600-DATAL         PIC X(252).                       00000870
      ****************************************************************  00000280
      * NEM : DSECT DU FICHIER FTF601 MAJ A ENVOYER EN MAGASIN       *  00000290
      ****************************************************************  00000300
      *                                                                 00000310
      * LONGUEUR 385                                                    00000320
      *                                                                 00000330
       01  TF601-ENREG.                                                 00000340
      * NOM DU FICHIER A METTRE A JOUR SUR 36 MAGASIN                   00000350
           02  TF601-FICHIER       PIC X(10).                           00000360
      * DATE DE MISE DE CREATION DU MOUVEMENT SSAAMMJJ                  00000370
           02  TF601-DATMAJ        PIC X(08).                           00000380
      * HEURE DE MISE DE CREATION DU MOUVEMENT SSAAMMJJ                 00000390
           02  TF601-HEUREMAJ      PIC X(06).                           00000400
      * SOCIETE CONCERNEE                                               00000410
           02  TF601-SOC           PIC X(03).                           00000420
      * ZONE    CONCERNEE                                               00000430
           02  TF601-ZONE          PIC X(02).                           00000440
      * MAGASIN CONCERNE                                                00000450
           02  TF601-MAG           PIC X(03).                           00000460
      * CODE MISE A JOUR 'S' = ANNULATION , 'C' = CREATION OU 'M' = MAJ 00000470
           02  TF601-MAJ           PIC X(01).                           00000480
      * CONTENU DE L'ENREGISTREMENT TRAITE                              00000490
           02  TF601-ENRG          PIC X(352).                          00000500
      * CONTENU DE L'ENREGISTREMENT TRAITE                              00000510
           02  TF601-DETAIL REDEFINES TF601-ENRG.                       00000520
              05  TF601-CLE           PIC X(100).                       00000530
              05  TF601-DATA          PIC X(252).                       00000540
       01  TF601-ENREGL REDEFINES TF601-ENREG.                          00000550
              05  TF601-CLEL          PIC X(133).                       00000560
              05  TF601-DATAL         PIC X(252).                       00000570
      ****************************************************************  00000580
      * NEM : DSECT DU FICHIER FTF602 MAJ A ENVOYER EN MAGASIN       *  00000590
      ****************************************************************  00000600
      *                                                                 00000610
      * LONGUEUR 385                                                    00000620
      *                                                                 00000630
       01  TF602-ENREG.                                                 00000640
      * NOM DU FICHIER A METTRE A JOUR SUR 36 MAGASIN                   00000650
           02  TF602-FICHIER       PIC X(10).                           00000660
      * DATE DE MISE DE CREATION DU MOUVEMENT SSAAMMJJ                  00000670
           02  TF602-DATMAJ        PIC X(08).                           00000680
      * HEURE DE MISE DE CREATION DU MOUVEMENT SSAAMMJJ                 00000690
           02  TF602-HEUREMAJ      PIC X(06).                           00000700
      * SOCIETE CONCERNEE                                               00000710
           02  TF602-SOC           PIC X(03).                           00000720
      * ZONE    CONCERNEE                                               00000730
           02  TF602-ZONE          PIC X(02).                           00000740
      * MAGASIN CONCERNE                                                00000750
           02  TF602-MAG           PIC X(03).                           00000760
      * CODE MISE A JOUR 'S' = ANNULATION , 'C' = CREATION OU 'M' = MAJ 00000770
           02  TF602-MAJ           PIC X(01).                           00000780
      * CONTENU DE L'ENREGISTREMENT TRAITE                              00000790
           02  TF602-ENRG          PIC X(352).                          00000800
      * CONTENU DE L'ENREGISTREMENT TRAITE                              00000810
           02  TF602-DETAIL REDEFINES TF602-ENRG.                       00000820
              05  TF602-CLE           PIC X(100).                       00000830
              05  TF602-DATA          PIC X(252).                       00000840
                                                                                
