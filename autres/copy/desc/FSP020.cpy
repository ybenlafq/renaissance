      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *  DSECT DU FICHIER FSP020: VALO DE L'ECART SRP/PRMP, PAR SOCIETE         
      *  EMETTRICE, RECEPTRICE, MARQUE, TYPE TEBRB, TAUX DE TVA                 
      *  --> SERT POUR GENERER LES ETATS ISP020 ET ISP021                       
      *****************************************************************         
      *                                                                         
       01 DSECT-FSP020.                                                         
          02 FSP020-ENTETE.                                                     
001          05 FSP020-NSOCEMET         PIC X(03).                              
004          05 FSP020-NSOCRECEP        PIC X(03).                              
007          05 FSP020-CMARQ            PIC X(05).                              
             05 FSP020-TEBRB.                                                   
012             07 FSP020-TLMELA        PIC X(03).                              
015             07 FSP020-BLBR          PIC X(02).                              
017          05 FSP020-TXTVA            PIC S9(03)V9(2) COMP-3.                 
020          05 FILLER                  PIC X(05).                              
      *                                                                         
          02 FSP020-CHAMPS.                                                     
025          05 FSP020-VALOSRP          PIC S9(13)V9(2) COMP-3.                 
033          05 FSP020-VALOPRMP         PIC S9(13)V9(2) COMP-3.                 
             05 FSP020-DEBEXER.                                                 
041             07 FSP020-DEBEXER-MM    PIC X(02).                              
043             07 FSP020-DEBEXER-AA    PIC X(02).                              
             05 FSP020-FINEXER.                                                 
045             07 FSP020-FINEXER-MM    PIC X(02).                              
047             07 FSP020-FINEXER-AA    PIC X(02).                              
             05 FSP020-DAVOIR.                                                  
049             07 FSP020-DAVOIR-MM     PIC X(02).                              
051             07 FSP020-DAVOIR-AA     PIC X(02).                              
053          05 FSP020-LMARQ            PIC X(25).                              
078          05 FILLER                  PIC X(08).                              
      *                                                                         
                                                                                
LG=085                                                                          
