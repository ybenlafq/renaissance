      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00000010
      * darty.com : zones geographiques et de livraison              *  00000020
      * V5.1P : LIVRAISON SOIREE -                                   *  00000020
      * V5.1P : LIVRAISON SOIREE -                                   *  00000020
      ****************************************************************  00000030
       01     FEC105-DSECT.                                                     
           02 FEC105-CLE.                                                       
            5 FEC105-CPAYS      PIC X(02).                                      
            5 FEC105-CINSEE     PIC X(05).                                      
            5 FEC105-CPOSTAL    PIC X(05).                                      
            5 FEC105-LCOMUNE    PIC X(32).                                      
V5.1.p      5 FEC105-SERVICE    PIC X(05).                                      
           02 FEC105-NSOC       PIC X(03).                                      
           02 FEC105-DATA.                                                      
            5 FEC105-NSOCLIV    PIC X(03).                                      
            5 FEC105-NLIEULIV   PIC X(03).                                      
      *       Zone élémentaire (GQ01)                                   00000080
            5 FEC105-CELEMEN    PIC X(05).                                      
      *       Zone bien desservie (0=non ; 1=oui)                       00000080
            5 FEC105-WDES       PIC 9(01).                                      
V5.1P       5 FEC105-CMODDEL    PIC X(03).                                      
      *                            -----                                00000050
      *                               59                                00000070
                                                                                
