      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ************************************************************              
      *                                                          *              
      *   FICHIER EDITION DES FACTURES L = 250                   *              
      *   UTILISE DS BFP000 BFP050 BFP100 BFP110 BFP120          *              
      *                                                          *              
      ************************************************************              
       01  FFP050-ENR.                                                          
           03  FFP050-NSOCCOMPT       PIC  X(03).                               
           03  FFP050-NSOCDEST        PIC  X(03).                               
           03  FFP050-NLIEUDEST       PIC  X(03).                               
           03  FFP050-CTYPINTER       PIC  X(03).                               
           03  FFP050-NFACTURE        PIC  X(15).                               
           03  FFP050-NLOT            PIC  X(06).                               
      *DATE DE TRAITEMENT = JJMMSSAA DE FDATE                                   
           03  FFP050-FDATE.                                                    
               05  FFP050-FDATE-JJ    PIC  X(02).                               
               05  FFP050-FDATE-MM    PIC  X(02).                               
               05  FFP050-FDATE-SSAA  PIC  X(04).                               
      *DATE D'INTERVENTION = DFACTURE                                           
           03  FFP050-DFACTURE.                                                 
               05  FFP050-DFACTURE-SSAA PIC  X(04).                             
               05  FFP050-DFACTURE-MM   PIC  X(02).                             
               05  FFP050-DFACTURE-JJ   PIC  X(02).                             
      *DATE DE MAJ = DMAJ                                                       
           03  FFP050-DMAJ.                                                     
               05  FFP050-DMAJ-SSAA PIC  X(04).                                 
               05  FFP050-DMAJ-MM   PIC  X(02).                                 
               05  FFP050-DMAJ-JJ   PIC  X(02).                                 
      *NOMBRE DE PRESENTATION                                                   
           03  FFP050-NPRESENT        PIC  X(02).                               
      *--------SI CTYPINTER = 'RA9'                                             
      *--FFP050-MTTTC = SUM MTTTC - SUM MTREMISE - SUM MTCLIENT                 
      *--------SI CTYPINTER = 'SAV'                                             
      *--FFP050-MTTTC = SUM MTTTC                                               
      *--FFP050-MTTTC ISSU DU BFP120 = NET A REGLER                             
           03  FFP050-MTTTC          PIC  9(09)V99.                             
           03  FFP050-MTFRANCHISE    PIC  9(09)V99.                             
           03  FFP050-MTLIVR         PIC  9(09)V99.                             
           03  FFP050-CREJET         PIC  X(03).                                
           03  FFP050-NLIEUTIERS     PIC  X(03).                                
           03  FFP050-TIERS          PIC  X(03).                                
           03  FFP050-NETTING        PIC  X(01).                                
           03  FFP050-NSINISTRE      PIC  X(20).                                
           03  FFP050-NCRI           PIC  X(10).                                
           03  FFP050-REFDOC         PIC  X(10).                                
           03  FFP050-LIBNSOC        PIC  X(25).                                
           03  FFP050-LIBNLIEU       PIC  X(25).                                
           03  FFP050-NOM            PIC  X(32).                                
           03  FILLER                PIC  X(26).                                
      *                                                                         
                                                                                
