      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * DESCRIPTION FICHIER FSM141                                              
      *                            LONGUEUR ENREG. = 382                        
      *                                                                         
       01 ENR-FSM141.                                                           
          05  FSM141-WSEQED                      PIC S9(5) COMP-3.              
          05  FSM141-NSOC                        PIC X(03).                     
          05  FSM141-CGRP                        PIC X(02).                     
          05  FSM141-TAB-LIEU     OCCURS 20.                                    
              10  FSM141-NLIEU                   PIC X(03).                     
              10  FSM141-NSEQ                    PIC X(02).                     
          05  FSM141-TAB-VTE-STK  OCCURS 20.                                    
              10  FSM141-QVTE                    PIC S9(5) COMP-3.              
              10  FSM141-QSTK                    PIC S9(5) COMP-3.              
          05  FSM141-TOTVTE                      PIC S9(7) COMP-3.              
          05  FSM141-TOTSTK                      PIC S9(7) COMP-3.              
          05  FSM141-NCODIC                      PIC X(07).                     
          05  FSM141-CFAM                        PIC X(05).                     
          05  FSM141-LFAM                        PIC X(20).                     
          05  FSM141-CMARQ                       PIC X(05).                     
          05  FSM141-LREFFOURN                   PIC X(20).                     
          05  FSM141-WSENSVTE                    PIC X(01).                     
          05  FSM141-LSTATCOMP                   PIC X(03).                     
          05  FSM141-LREFDARTY                   PIC X(20).                     
          05  FSM141-PRIX                        PIC S9(7)V9(2) COMP-3.         
          05  FSM141-PCOMM                       PIC S9(5)V9(2) COMP-3.         
          05  FSM141-DCDE                        PIC X(08).                     
          05  FSM141-QCDE                        PIC S9(5) COMP-3.              
          05  FSM141-ZMARKET      OCCURS 3.                                     
              10  FSM141-CMARKET                 PIC X(05).                     
              10  FSM141-CVMARKET                PIC X(05).                     
              10  FSM141-CVDESCRIPTIF            PIC X(05).                     
                                                                                
