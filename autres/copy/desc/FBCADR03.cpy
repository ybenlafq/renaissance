      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------------- 00010000
      *   DESCRIPTION DE LA CLAUSE COPY FBCADR03                        00020002
      *       (FICHIER DES ADRESSES TOPEES CHARGEES DANS LA BASE        00030004
      *        ADRESSES)                                                00031004
      *   LONGUEUR DU FICHIER : 050 CARACT}RES                          00043108
      *---------------------------------------------------------------- 00044000
      *   HISTORIQUE DES MODIFICATIONS                                  00045000
      *       ATTENTION : CHANGER LA VERSION A CHAQUE MODIFICATION      00046000
      *---------------------------------------------------------------- 00047000
      * THM 17/08/2000 V1.0.00 CREATION                                 00048004
      *---------------------------------------------------------------- 00049000
                                                                        00080009
         03 :XXX-:ENREG                   PIC X(050).                   00081009
                                                                        00082009
         03 FILLER REDEFINES :XXX-:ENREG.                               00083009
                                                                        00084009
      *... NUMERO DE VERSION                                            00086010
           05 FILLER                      PIC X(014).                   00086109
           05 :XXX-:VERSION               PIC X(005).                   00087009
           05 FILLER                      PIC X(031).                   00089209
                                                                        00089309
         03 FILLER REDEFINES :XXX-:ENREG.                               00089409
                                                                        00089509
      *... CODE SOCIETE                                                 00091009
           05 :XXX-:NSOCIETE              PIC X(003).                   00100008
                                                                        00101000
      *... CODE MAGASIN                                                 00102000
           05 :XXX-:NLIEU                 PIC X(003).                   00103008
                                                                        00104000
      *... NUMERO DE VENTE                                              00105000
           05 :XXX-:NVENTE                PIC X(008).                   00106008
                                                                        00107000
      *... TYPE ADRESSE                                                 00107107
           05 :XXX-:WTYPEADR              PIC X(001).                   00107208
                                                                        00107307
      *... DATE DE VERSION DE VENTE (FORMAT SSAAMMJJ)                   00107407
           05 :XXX-:DVENTE                PIC X(008).                   00107508
      *                                                                 00330000
      *... NUMERO DE CLIENT DE LA VENTE                                 00330100
           05 :XXX-:IDCLIENT    PIC X(8).                               00350010
      *... NUMERO DE CLIENT FINALE                                      00350011
           05 :XXX-:NCLIENTADR  PIC X(8).                               00350020
      *... HISTORIQUE � BLANC                                           00350021
           05 :XXX-:HISTORIQUE  PIC X(1).                               00350022
      *... MODIFICATION                                                 00350023
           05 :XXX-:MODIFICATION PIC X(1).                              00350024
           05 :XXX-:FILLER      PIC X(09).                              00350030
      *                                                                 00360006
                                                                                
