      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FLG866  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : EXTRACTION COMMUNE                          *         
      *                                                               *         
      *    FLG866       : FICHIER DE TRAVAIL (CODIC, DEPOT)           *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:   140 OCTETS                                *         
      *                                                               *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  DSECT-FLG866.                                                        
           05  FLG866-NCODIC                  PIC X(07).                        
           05  FLG866-DONNEES-DEP.                                              
               10 FLG866-NSOCDEPOT            PIC X(03).                        
               10 FLG866-NDEPOT               PIC X(03).                        
               10 FLG866-WGENGRO              PIC X(03).                        
               10 FLG866-WABC                 PIC X(01).                        
               10 FLG866-NSOCDEPOTRT          PIC X(03).                        
               10 FLG866-NDEPOTRT             PIC X(03).                        
               10 FLG866-NSOCDEPOTAF          PIC X(03).                        
               10 FLG866-NDEPOTAF             PIC X(03).                        
           05  FLG866-DONNEES1.                                                 
               10 FLG866-D1RECEPT             PIC X(8).                         
               10 FLG866-CAPPRO               PIC X(5).                         
               10 FLG866-DEFFET               PIC X(8).                         
               10 FLG866-WSENSAPPRO           PIC X(1).                         
               10 FLG866-QDELAIAPPRO          PIC X(3).                         
               10 FLG866-QCOLIRECEPT          PIC S9(5) COMP-3.                 
               10 FLG866-QCOLIDSTOCK          PIC S9(5) COMP-3.                 
               10 FLG866-CMODSTOCK            PIC X(5).                         
               10 FLG866-QNBPRACKSOL          PIC S9(5) COMP-3.                 
               10 FLG866-CLASSE               PIC X(1).                         
           05  FLG866-DONNEES2.                                                 
               10 FLG866-GS10-QSTOCK          PIC S9(5) COMP-3.                 
               10 FLG866-GS10-QSTOCKRES       PIC S9(5) COMP-3.                 
               10 FLG866-GS10-QSTOCKC         PIC S9(5) COMP-3.                 
               10 FLG866-GS10-QSTOCKH         PIC S9(5) COMP-3.                 
               10 FLG866-GS10-QSTOCKP         PIC S9(5) COMP-3.                 
               10 FLG866-GS10-QSTOCKR         PIC S9(5) COMP-3.                 
               10 FLG866-GS10-QSTOCKT         PIC S9(5) COMP-3.                 
               10 FLG866-MU06-QSTOCK          PIC S9(5) COMP-3.                 
               10 FLG866-MU06-QSTOCKRES       PIC S9(5) COMP-3.                 
               10 FLG866-MU06-QSTOCKC         PIC S9(5) COMP-3.                 
               10 FLG866-MU06-QSTOCKTR        PIC S9(5) COMP-3.                 
               10 FLG866-DCDE                 PIC X(8).                         
               10 FLG866-QCDE                 PIC S9(5) COMP-3.                 
               10 FLG866-QCDERES              PIC S9(5) COMP-3.                 
               10 FLG866-TOTQCDE              PIC S9(5) COMP-3.                 
               10 FLG866-TOTQCDERES           PIC S9(5) COMP-3.                 
           05  FLG866-DONNEES3.                                                 
               10 FLG866-LSTATCOMP            PIC X(3).                         
               10 FLG866-CEXPO                PIC X(5).                         
           05  FILLER                         PIC X(10).                        
                                                                                
