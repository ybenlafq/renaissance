      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01 FEF090-ENREG.                                                         
      *   STATUT DU DOSSIER                                                     
001       05 FEF090-CFONC-DOS      PIC X(03).                                   
      *   STATUT DE LA LIGNE                                                    
004       05 FEF090-CFONC          PIC X(03).                                   
      *   NUMERO D'EXPEDITION                                                   
007       05 FEF090-NSOCENVOI      PIC X(03).                                   
      *   CENTRE DE TRAITEMENT                                                  
010       05 FEF090-CTRAIT         PIC X(05).                                   
      *   NUMERO D'EXPEDITION                                                   
015       05 FEF090-NENVOI         PIC X(07).                                   
      *   NUMERO DE SOCIETE DU DOSSIER HS                                       
022       05 FEF090-NSOCORIG       PIC X(03).                                   
      *   NUMERO DE LIEU DU DOSSIER HS                                          
025       05 FEF090-NLIEUORIG      PIC X(03).                                   
      *   NUMERO DU DOSSIER HS                                                  
028       05 FEF090-NORIGINE       PIC X(07).                                   
      *   NUMERO DE POSTE                                                       
035       05 FEF090-NPOSTE         PIC X(05).                                   
      *   CODIC                                                                 
040       05 FEF090-NCODIC         PIC X(07).                                   
      *   QUANTITE                                                              
047       05 FEF090-QTENV          PIC 9(05).                                   
      *   PRIX                                                                  
052       05 FEF090-PABASEFACT     PIC 9(07)V9(02).                             
      *   DESTINATAIRE                                                          
062       05 FEF090-CTIERS         PIC X(05).                                   
      *   ENTITE DE COMMANDE                                                    
067       05 FEF090-NENTCDE        PIC X(05).                                   
      *   NUMERO RMA                                                            
072       05 FEF090-RMA            PIC X(40).                                   
      *   NUMERO DE SERIE                                                       
112       05 FEF090-NSERIE         PIC X(16).                                   
      *   NUMERO D'ACCORD                                                       
128       05 FEF090-NACCORD        PIC X(12).                                   
      *   DATE D'ACCORD                                                         
140       05 FEF090-DACCORD        PIC X(08).                                   
      *   INTERLOCUTEUR DE L'ACCORD                                             
148       05 FEF090-LNOMACCORD     PIC X(10).                                   
      *   DATE D'ENVOI                                                          
158       05 FEF090-DENVOI         PIC X(08).                                   
      *   PREFIXE DOSSIER NASC                                                  
166       05 FEF090-PDOSNASC       PIC X(03).                                   
      *   DOSSIER NASC                                                          
169       05 FEF090-DOSNASC        PIC X(09).                                   
      *   TYPE D ECHANGE                                                        
178       05 FEF090-CTYINASC       PIC X(05).                                   
      *                                                                         
183       05 FILLER                PIC X(18).                                   
                                                                                
