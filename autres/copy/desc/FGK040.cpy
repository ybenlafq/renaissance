      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00100000
      *                                                               * 00100100
      *  FICHIER CONSOLIDATION DONNEES KESA : STAT ACHATS FILIALES    * 00100200
      *                                                               * 00100300
      *        - DSECT COBOL                                          * 00100400
      *        - NOM  = FGK040                                        * 00100600
      *        - LONGUEUR CLE   =  21                                 * 00101000
      *        - LONGUEUR ENREG =  60                                 * 00101000
      *                                                               * 00101100
      ***************************************************************** 00101400
      *                                                                         
       01  FGK040-ENR.                                                          
           03  FGK040-CLE.                                                      
               05  FGK040-NSOCIETE      PIC  X(03).                             
               05  FGK040-NENTCDE       PIC  X(05).                             
               05  FGK040-NCODIC        PIC  X(07).                             
               05  FGK040-DMOIS-TRT     PIC  X(06).                             
           03  FGK040-DONNEES.                                                  
               05  FGK040-PRA-M-MOIS    PIC S9(09)V99 COMP-3.                   
               05  FGK040-PRA           PIC S9(09)V99 COMP-3.                   
               05  FGK040-VOL-ACHAT     PIC S9(09)    COMP-3.                   
               05  FGK040-QNB-ACHAT     PIC S9(09)    COMP-3.                   
               05  FILLER               PIC  X(17).                             
      *                                                                         
                                                                                
