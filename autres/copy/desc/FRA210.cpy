      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *                   DSECT DU FICHIER FRA210                      *        
      *                                                                *        
      *----------------------------------------------------------------*        
      * 01  :FRA210:-RECORD.                                                    
            05 :FRA210:-CORIG     PIC X(4).                                     
            05 :FRA210:-CTYPAVOIR PIC X(1).                                     
            05 :FRA210:-CTYPDEM   PIC X(5).                                     
            05 :FRA210:-NSOC      PIC X(3).                                     
            05 :FRA210:-NLIEU     PIC X(3).                                     
            05 :FRA210:-NUMAVOIR  PIC X(6).                                     
            05 :FRA210:-DAVOIR    PIC X(8).                                     
            05 :FRA210:-CMOTIF    PIC X(2).                                     
            05 :FRA210:-NUMLETTR  PIC X(10).                                    
            05 :FRA210:-DDIFF     PIC X(8).                                     
            05 :FRA210:-WTLMELA   PIC X(1).                                     
            05 :FRA210:-WBLBR     PIC X(1).                                     
            05 :FRA210:-REGLEMENT.                                              
               07 :FRA210:-CTYPREGL  PIC X(1).                                  
               07 :FRA210:-MTTREMB   PIC 9999999V,99.                           
               07 :FRA210:-CBANQ     PIC X(5).                                  
               07 :FRA210:-CLETTRE   PIC X(2).                                  
            05 :FRA210:-ADRESSE.                                                
               07 :FRA210:-CTITRENOM PIC X(5).                                  
               07 :FRA210:-LNOM      PIC X(25).                                 
               07 :FRA210:-LPRENOM   PIC X(15).                                 
               07 :FRA210:-CVOIE     PIC X(5).                                  
               07 :FRA210:-CTVOIE    PIC X(4).                                  
               07 :FRA210:-LNOMVOIE  PIC X(21).                                 
               07 :FRA210:-LCMPAD1   PIC X(32).                                 
               07 :FRA210:-LBATIMENT PIC X(3).                                  
               07 :FRA210:-LESCALIER PIC X(3).                                  
               07 :FRA210:-LETAGE    PIC X(3).                                  
               07 :FRA210:-LPORTE    PIC X(3).                                  
               07 :FRA210:-CPOSTAL   PIC X(5).                                  
               07 :FRA210:-LCOMMUNE  PIC X(32).                                 
               07 :FRA210:-LBUREAU   PIC X(25).                                 
            05 :FRA210:-NCODIC       PIC X(7).                                  
            05 :FRA210:-CCONC        PIC X(4).                                  
            05 :FRA210:-PRIX-ACHAT   PIC 9999999V,99.                           
            05 :FRA210:-PRIX-CONC    PIC 9999999V,99.                           
            05 :FRA210:-QUANTITE     PIC X(5).                                  
            05 FILLER               PIC X(13).                                  
                                                                                
