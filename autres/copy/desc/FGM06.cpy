      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FGM06-DSECT.                                                 00000090
           02  FGM06-NCODIC1                                            00000100
               PIC X(0007).                                             00000110
           02  FGM06-NCODIC2                                            00000120
               PIC X(0007).                                             00000130
           02  FGM06-CZPRIX                                             00000140
               PIC X(0002).                                             00000150
           02  FGM06-CFAM                                               00000160
               PIC X(0005).                                             00000170
           02  FGM06-WTYPART                                            00000180
               PIC X(0001).                                             00000190
           02  FGM06-PSTDTTC                                            00000200
               PIC S9(7)V9(0002) COMP-3.                                00000210
           02  FGM06-PCOMM                                              00000220
               PIC S9(5) COMP-3.                                        00000230
           02  FGM06-QNBREART                                           00000240
               PIC S9(7) COMP-3.                                        00000250
           02  FGM06-QSTOCKZONE                                         00000260
               PIC S9(7) COMP-3.                                        00000270
           02  FGM06-DSYST                                              00000280
               PIC S9(13) COMP-3.                                       00000290
           02  FGM06-PCOMMART                                           00000300
               PIC S9(5)V9(0002) COMP-3.                                00000310
           02  FGM06-PCOMMVOL                                           00000320
               PIC S9(5)V9(0002) COMP-3.                                00000330
                                                                                
