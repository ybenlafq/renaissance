      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  W-ENR-FPV600.                                                        
1          05 FPV600-NSOCIETE        PIC X(3).                                  
4          05 FPV600-NLIEU           PIC X(3).                                  
7          05 FPV600-NVENTE          PIC X(7).                                  
14         05 FPV600-NSEQNQ          PIC S9(5) COMP-3.                          
17         05 FPV600-CVENDEUR        PIC X(6).                                  
23         05 FPV600-DTOPE           PIC X(8).                                  
31         05 FPV600-DCREATION       PIC X(8).                                  
39         05 FPV600-WOFFRE          PIC X(1).                                  
40         05 FPV600-NCODICGRP       PIC X(7).                                  
47         05 FPV600-CTYPENT         PIC X(2).                                  
49         05 FPV600-WREMISE         PIC X(1).                                  
50         05 FPV600-NCODIC          PIC X(7).                                  
57         05 FPV600-CENREG          PIC X(5).                                  
62         05 FPV600-WEP             PIC X(1).                                  
63         05 FPV600-WOA             PIC X(1).                                  
64         05 FPV600-WPSAB           PIC X(1).                                  
65         05 FPV600-WREMVTE         PIC X(1).                                  
66         05 FPV600-NSEQENT         PIC S9(5) COMP-3.                          
69         05 FPV600-QTVEND          PIC S9(5) COMP-3.                          
72         05 FPV600-QTCODIG         PIC S9(5) COMP-3.                          
75         05 FPV600-MTVTEHT         PIC S9(7)V99 COMP-3.                       
80         05 FPV600-MTVTETVA        PIC S9(7)V99 COMP-3.                       
85         05 FPV600-MTFORHT         PIC S9(7)V99 COMP-3.                       
90         05 FPV600-MTFORTVA        PIC S9(7)V99 COMP-3.                       
95         05 FPV600-MTDIFFHT        PIC S9(7)V99 COMP-3.                       
100        05 FPV600-MTDIFFTVA       PIC S9(7)V99 COMP-3.                       
105        05 FPV600-MTPRMP          PIC S9(7)V99 COMP-3.                       
110        05 FPV600-MTPRIME         PIC S9(7)V99 COMP-3.                       
115        05 FPV600-MTPRIMEO        PIC S9(7)V99 COMP-3.                       
120        05 FPV600-MTPRIMEV        PIC S9(7)V99 COMP-3.                       
125        05 FPV600-MTCOMMOP        PIC S9(7)V99 COMP-3.                       
130        05 FPV600-TYPVTE          PIC X(01).                                 
131        05 FPV600-WTABLE          PIC X(02).                                 
133        05 FPV600-NZONPRIX        PIC X(02).                                 
135        05 FPV600-MTREFINOP       PIC S9(7)V99 COMP-3.                       
139        05 FPV600-DDELIV          PIC X(08).                                 
147   * FIN                                                                     
                                                                                
