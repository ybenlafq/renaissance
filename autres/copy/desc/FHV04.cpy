      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000010
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FHV04 <<<<<<<<<<<<<< *   00000020
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 00000030
      *                                                               * 00000040
      ***************************************************************** 00000050
      *                                                               * 00000060
      *    PROJETS      : NCG - INITIALISATIONS                       * 00000070
      *                 : REAPPRO MGI - "SUIVI DES STOCKS".           * 00000080
      *                                                               * 00000090
      *    FHV04        : FICHIER DES VENTES MGI  (RESULTANT  DE LA   * 00000100
      *                 : REMONTEE QUOTIDIENNE DES DONNEES 36 MGI).   * 00000110
      *                                                               * 00000120
      *    LONGUEUR ENR.:   28 C                                      * 00000140
      *                                                               * 00000150
      ***************************************************************** 00000260
      *                                                               * 00000270
                                                                        00000280
       01  FHV04-ENREG.                                                 00000290
               10  FHV04-SOCIETE                 PIC  X(03).            00000400
               10  FHV04-DATE                    PIC  X(08).            00000410
               10  FHV04-TYPENREG                PIC  X(01).            00000420
               10  FHV04-NCODIC                  PIC  X(07).            00000430
               10  FHV04-GROUPE                  PIC  X(01).            00000420
               10  FHV04-VENTESEMP               PIC  S9(03) COMP-3.    00000440
               10  FHV04-VENTESLIV               PIC  S9(03) COMP-3.    00000440
               10  FHV04-VENTESEMPECR            PIC  S9(03) COMP-3.    00000440
               10  FHV04-VENTESLIVECR            PIC  S9(03) COMP-3.    00000440
                                                                                
