      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **********************************************************        00010001
      *  DESCRIPTION FICHIER CORRESPONDANT TABLE RTGA14        *        00020001
      *                                                                 00030001
MB01  * MAINTENANCE > CONCERNANT L'AJOUT D'UNE COLONNE DANS LA          00040001
      *               TABLE RTGA14                                      00050001
      *             > GA14-CTYPENT                                      00060001
      *             > LA LONGUEUR PASSE DE 52 � 54                      00070001
      *                                                                 00080001
      **********************************************************        00090001
BF1   * 04/02/2015 ALTER RTGA14                                         00100001
      * AJOUT CGARANMGD => LA TAILLE PASSE � 59                         00110001
      **********************************************************        00110101
       01  FDM000-DSECT.                                                00110201
           02  FDM000-CFAM           PIC X(5).                          00110301
           02  FDM000-LFAM           PIC X(20).                         00110401
           02  FDM000-CMODSTOCK      PIC X(5).                          00110501
           02  FDM000-CTAUXTVA       PIC X(5).                          00110601
           02  FDM000-CGARANTIE      PIC X(5).                          00110701
           02  FDM000-QBORNESOL      PIC S9(2)V9(3) COMP-3.             00110801
           02  FDM000-QBORNEVRAC     PIC S9(2)V9(3) COMP-3.             00110901
           02  FDM000-QNBPVSOL       PIC S9(3)      COMP-3.             00111001
           02  FDM000-WMULTIFAM      PIC X(1).                          00112001
           02  FDM000-WSEQFAM        PIC S9(5)      COMP-3.             00113001
MB01       02  FDM000-CTYPENT        PIC X(2).                          00114001
BF1        02  FDM000-CGARANMGD      PIC X(5).                          00115001
                                                                                
