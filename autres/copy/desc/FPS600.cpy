      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****                                                                     
      *  LG = 127                                                               
      *****                                                                     
       01  DSECT-FPS600.                                                        
           05 FPS600-FMOIS      PIC X(6).                                       
           05 FPS600-NSOC       PIC X(3).                                       
           05 FPS600-CGCPLT     PIC X(5).                                       
           05 FPS600-CGRP       PIC X(5).                                       
           05 FPS600-NOSAV      PIC X(3).                                       
           05 FPS600-CSECTEUR   PIC X(5).                                       
           05 FPS600-DEFFET     PIC X(6).                                       
           05 FPS600-DVENTE     PIC X(6).                                       
           05 FPS600-NBPSE      PIC S9(7)       COMP-3.                         
           05 FPS600-MTTC       PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTHT       PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTFRAIS    PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-NBPSER     PIC S9(7)       COMP-3.                         
           05 FPS600-MTTCR      PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTHTR      PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTVAFACM   PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTFACM     PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTDJFC     PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTRAFC     PIC S9(9)V9(6)  COMP-3.                         
           05 FPS600-MTREMB     PIC S9(9)V9(6)  COMP-3.                         
                                                                                
