      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      ******* FICHIER LISTE DES LIVRAISONS PREVUES FTL065 LG120********         
      *****************************************************************         
       01  FTL065-DSECT.                                                        
           10  FTL065-DDELIV         PIC  X(08).                                
           10  FTL065-NLIEU          PIC  X(03).                                
           10  FTL065-NVENTE         PIC  X(07).                                
           10  FTL065-CPROTOUR       PIC  X(05).                                
           10  FTL065-NTOURNEE       PIC  X(03).                                
           10  FTL065-NORDRE         PIC  X(02).                                
           10  FTL065-LNOM           PIC  X(25).                                
           10  FTL065-LCOMMUNE       PIC  X(32).                                
           10  FTL065-CPOSTAL        PIC  X(05).                                
           10  FTL065-TELDOM         PIC  X(10).                                
           10  FTL065-TELBUR         PIC  X(10).                                
           10  FTL065-LPOSTEBUR      PIC  X(05).                                
           10  FILLER                PIC  X(05).                                
                                                                                
