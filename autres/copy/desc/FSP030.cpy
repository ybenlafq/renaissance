      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *  DSECT DU FICHIER FSP030: VALO DE L'ECART SRP/PRMP, PAR SOCIETE         
      *  EMETTRICE, RECEPTRICE, TYPE TEBRB, TAUX DE TVA ET MARQUE               
      *  --> SERT POUR GENERER LES PROVISIONS ET STOCKER LES MONTANTS           
      *      PROVISIONNÚS                                                       
      *****************************************************************         
      *                                                                         
       01 DSECT-FSP030.                                                         
          02 FSP030-ENTETE.                                                     
001          05 FSP030-NSOCEMET         PIC X(03).                              
004          05 FSP030-NSOCRECEP        PIC X(03).                              
007          05 FSP030-CMARQ            PIC X(05).                              
012          05 FSP030-TXTVA            PIC S9(3)V9(2)  COMP-3.                 
             05 FSP030-TEBRB.                                                   
015             07 FSP030-TLMELA        PIC X(03).                              
018             07 FSP030-BLBR          PIC X(02).                              
      *                                                                         
          02 FSP030-CHAMPS.                                                     
020          05 FSP030-VALOSRP          PIC S9(15)V9(2) COMP-3.                 
029          05 FSP030-VALOPRMP         PIC S9(15)V9(2) COMP-3.                 
038          05 FILLER                  PIC X(03).                              
      *                                                                         
                                                                                
LG=040                                                                          
