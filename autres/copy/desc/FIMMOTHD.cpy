      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *-----------------------------------------------------------------        
      * IMMOBILISATION THD TRES HAUTE DEFINITION                                
      * DESCRIPTION DES ZONES DU FICHIER FIMMOTHD                               
      * LONGUEUR : 80 CARACTERES                                                
      *-----------------------------------------------------------------        
      * DESCRIPTION IMMOBILISATION A DESTINATION DE BOUYGUES TELECOM            
      * SUITE TRANSFERT ACTIVITE DARTY BOX                                      
      * - 3 TYPES D'ENREGISTREMENT :                                            
      *   - CODE ENREGISTREMENT 0  : ENTETE                                     
      *   - CODE ENREGISTREMENT 1  : DETAIL                                     
      *   - CODE ENREGISTREMENT 9  : FIN                                        
      *-----------------------------------------------------------------        
      *                                                                         
      *** ENREGISTREMENT ENTETE                                                 
      *                                                                         
       01  W-THD0-RECORD.                                                       
      * CODE ENREGISTREMENT                                                     
           05   W-THD0-CENREG           PIC X(01).                              
                88  W-THD0-TETE             VALUE '0'.                          
           05   W-THD0-SEPAR1           PIC X(01).                              
      * NOM DU FICHIER                                                          
           05   W-THD0-NOM              PIC X(10).                              
           05   W-THD0-SEPAR2           PIC X(01).                              
      * DATE DE PRODUCTION                                                      
           05   W-THD0-DATE.                                                    
                10 W-THD0-DATEJJ        PIC X(02).                              
                10 W-THD0-DATSE1        PIC X(01).                              
                10 W-THD0-DATEMM        PIC X(02).                              
                10 W-THD0-DATSE2        PIC X(01).                              
                10 W-THD0-DATESSAA.                                             
                   15 W-THD0-DATESS     PIC X(02).                              
                   15 W-THD0-DATEAA     PIC X(02).                              
           05   W-THD0-SEPAR3           PIC X(01).                              
      * HEURE DE PRODUCTION                                                     
           05   W-THD0-HEUR.                                                    
                10 W-THD0-HEURHH        PIC X(02).                              
                10 W-THD0-HEUSE1        PIC X(01).                              
                10 W-THD0-HEURMM        PIC X(02).                              
                10 W-THD0-HEUSE2        PIC X(01).                              
                10 W-THD0-HEURSS        PIC X(02).                              
           05   W-THD0-SEPAR4           PIC X(01).                              
      * ZONE LIBRE                                                              
           05   W-THD0-LIBRE            PIC X(46).                              
           05   W-THD0-SEPAR5           PIC X(01).                              
      *                                                                         
      *** ENREGISTREMENT DETAIL                                                 
      *                                                                         
       01  W-THD1-RECORD.                                                       
      * CODE ENREGISTREMENT                                                     
           05   W-THD1-CENREG           PIC X(01).                              
                88  W-THD1-DETAIL           VALUE '1'.                          
           05   W-THD1-SEPAR1           PIC X(01).                              
      * CODE SOCIETE 920 - DARTY BOX                                            
           05   W-THD1-SOCIETE          PIC X(03).                              
           05   W-THD1-SEPAR2           PIC X(01).                              
      * TYPE MOUVEMENT : IES - MAR                                              
           05   W-THD1-TYPMVT           PIC X(03).                              
           05   W-THD1-SEPAR3           PIC X(01).                              
      * DATE MOUVEMENT                                                          
           05   W-THD1-DATMVT.                                                  
                10 W-THD1-DMVTJJ        PIC X(02).                              
                10 W-THD1-DMVSE1        PIC X(01).                              
                10 W-THD1-DMVTMM        PIC X(02).                              
                10 W-THD1-DMVSE2        PIC X(01).                              
                10 W-THD1-DMSSAA.                                               
                   15 W-THD1-DMVTSS     PIC X(02).                              
                   15 W-THD1-DMVTAA     PIC X(02).                              
           05   W-THD1-SEPAR4           PIC X(01).                              
      * VALEUR (EN CENTIMES SANS VIRGULE)                                       
           05   W-THD1-VALEUR           PIC 9(12).                              
           05   W-THD1-SEPAR5           PIC X(01).                              
      * IDENTIFIANT : NUMERO DE SERIE OU NUMERO CONTRAT                         
           05   W-THD1-IDENT            PIC X(18).                              
           05   W-THD1-SEPAR6           PIC X(01).                              
      * NATURE : THD                                                            
           05   W-THD1-NATURE           PIC X(04).                              
           05   W-THD1-SEPAR7           PIC X(01).                              
      * NUMERO IMMOBILISATION (N� ABEL - FACULTATIF)                            
           05   W-THD1-NIMMO            PIC X(12).                              
           05   W-THD1-SEPAR8           PIC X(01).                              
      * ZONE LIBRE                                                              
           05   W-THD1-LIBRE            PIC X(08).                              
           05   W-THD1-SEPAR9           PIC X(01).                              
      *                                                                         
      *** ENREGISTREMENT FIN                                                    
      *                                                                         
       01  W-THD9-RECORD.                                                       
      * CODE ENREGISTREMENT                                                     
           05   W-THD9-CENREG           PIC X(01).                              
                88  W-THD9-FIN             VALUE '9'.                           
           05   W-THD9-SEPAR1           PIC X(01).                              
      * NOM DU FICHIER                                                          
           05   W-THD9-NOM              PIC X(10).                              
           05   W-THD9-SEPAR2           PIC X(01).                              
      * DATE DE PRODUCTION                                                      
           05   W-THD9-DATE.                                                    
                10 W-THD9-DATEJJ        PIC X(02).                              
                10 W-THD9-DATSE1        PIC X(01).                              
                10 W-THD9-DATEMM        PIC X(02).                              
                10 W-THD9-DATSE2        PIC X(01).                              
                10 W-THD9-DATESSAA.                                             
                   15 W-THD9-DATESS     PIC X(02).                              
                   15 W-THD9-DATEAA     PIC X(02).                              
           05   W-THD9-SEPAR3           PIC X(01).                              
      * HEURE DE PRODUCTION                                                     
           05   W-THD9-HEUR.                                                    
                10 W-THD9-HEURHH        PIC X(02).                              
                10 W-THD9-HEUSE1        PIC X(01).                              
                10 W-THD9-HEURMM        PIC X(02).                              
                10 W-THD9-HEUSE2        PIC X(01).                              
                10 W-THD9-HEURSS        PIC X(02).                              
           05   W-THD9-SEPAR4           PIC X(01).                              
      * NOMBRES DE LIGNES                                                       
           05   W-THD9-NENREG           PIC 9(06).                              
           05   W-THD9-SEPAR5           PIC X(01).                              
      * ZONE LIBRE                                                              
           05   W-THD9-LIBRE            PIC X(39).                              
           05   W-THD9-SEPAR6           PIC X(01).                              
      *                                                                         
      *** FIN COPY FIMMOTHD ***                                                 
      *                                                                         
                                                                                
