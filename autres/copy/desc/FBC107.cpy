      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FBC107  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : BASE DE DONNEES CLIENT                      *         
      *                                                               *         
      *    LOT          : VENTES DONT L'ADRESSE EST DEJA CHARGEE      *         
      *                                                               *         
      *    FBC107       : FICHIER DES LIGNES DE VENTE                 *         
      *                                                               *         
      *    STRUCTURE    :   VSAM                                      *         
      *    LONGUEUR ENR.:  157 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  FBC107-ENREG.                                                        
           03  FBC107-CLE.                                                      
               05  FBC107-NSOCIETE            PIC  X(03).                       
               05  FBC107-NLIEU               PIC  X(03).                       
               05  FBC107-NVENTE              PIC  X(08).                       
           03  FBC107-CTYPENREG               PIC  X(01).                       
           03  FBC107-NCODICGRP               PIC  X(07).                       
           03  FBC107-NCODIC                  PIC  X(07).                       
           03  FBC107-NSEQ                    PIC  X(02).                       
           03  FBC107-CMODDEL                 PIC  X(03).                       
           03  FBC107-DDELIV                  PIC  X(08).                       
           03  FBC107-CENREG                  PIC  X(05).                       
           03  FBC107-QVENDUE                 PIC  S9(05).                      
           03  FBC107-PVUNIT                  PIC  S9(07)V9(02).                
           03  FBC107-PVUNITF                 PIC  S9(07)V9(02).                
           03  FBC107-PVTOTAL                 PIC  S9(07)V9(02).                
           03  FBC107-NLIGNE                  PIC  X(02).                       
           03  FBC107-TAUXTVA                 PIC  S9(03)V9(02).                
           03  FBC107-PRMP                    PIC  S9(07)V9(02).                
           03  FBC107-WEMPORTE                PIC  X(01).                       
           03  FBC107-CVENDEUR                PIC  X(06).                       
           03  FBC107-WARTINEX                PIC  X(01).                       
           03  FBC107-WTOPELIVRE              PIC  X(01).                       
           03  FBC107-DCOMPTA                 PIC  X(08).                       
           03  FBC107-DCREATION               PIC  X(08).                       
           03  FBC107-DANNULATION             PIC  X(08).                       
           03  FBC107-DSTAT                   PIC  X(08).                       
           03  FBC107-DVERSION                PIC  X(08).                       
           03  FBC107-DSYST                   PIC  X(13).                       
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
