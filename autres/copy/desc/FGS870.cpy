      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>> DESCRIPTION DU FICHIER  FGS870  <<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
      *    PROJET       : ETAT DES STOCK DEPOT PAR FAMILLE            *         
      *                                                               *         
      *    FGS870       : FICHIER DE TRAVAIL                          *         
      *                                                               *         
      *    STRUCTURE    :   SAM                                       *         
      *    LONGUEUR ENR.:  305 C                                      *         
      *                                                               *         
      *****************************************************************         
      * DESCRIPTION DE L'ENREGISTREMENT:                              *         
      *****************************************************************         
      *                                                               *         
       01  :FGPXXX:-ENREG.                                                      
      *                                                         1    6          
        02 :FGPXXX:-CETAT                     PIC X(06).                        
        02 :FGPXXX:-DONNEE-NCODIC.                                              
      *                                                         7    7          
           05  :FGPXXX:-RAYON                 PIC X(07).                        
      *                                                        14    5          
           05  :FGPXXX:-WSEQFAM               PIC X(05).                        
      *                                                        19    5          
           05  :FGPXXX:-CFAM                  PIC X(05).                        
      *                                                        24   20          
           05  :FGPXXX:-LFAM                  PIC X(20).                        
PM0205*                                                        44    5          
PM0205     05  :FGPXXX:-CMODSTOCK             PIC X(05).                        
      *                                                                         
        02 :FGPXXX:-DONNEE-DEPOT.                                               
           05  :FGPXXX:-ENTAFF.                                                 
      *                                                        49    3          
               10  :FGPXXX:-NSOCDEPOTAFF PIC X(03).                             
      *                                                        52    3          
               10  :FGPXXX:-NDEPOTAFF    PIC X(03).                             
      *                                                        55    3          
           05  :FGPXXX:-WGENGRO          PIC X(03).                             
           05  :FGPXXX:-ENTREPOT.                                               
      *                                                        58    3          
              10   :FGPXXX:-NSOCDEPOT    PIC X(03).                             
      *                                                        61    3          
              10   :FGPXXX:-NDEPOT       PIC X(03).                             
      *                                                                         
        02 :FGPXXX:-DONNEE-STOCK.                                               
      *                                                        64    9          
           05  :FGPXXX:-APP-QSTOCK       PIC S9(11)V9(06) COMP-3.               
      *                                                        73    3          
           05  :FGPXXX:-APP-TOT-REF      PIC S9(5) COMP-3.                      
      *                                                        76    3          
           05  :FGPXXX:-APP-QSTK-REF     PIC S9(5) COMP-3.                      
      *                                                        79    9          
           05  :FGPXXX:-APP-QSTOCKDIS    PIC S9(11)V9(06) COMP-3.               
      *                                                        88    3          
           05  :FGPXXX:-APP-QSTKDIS-REF  PIC S9(5) COMP-3.                      
      *                                                        91    9          
           05  :FGPXXX:-APP-QSTOCKRES    PIC S9(11)V9(06) COMP-3.               
      *                                                       100    3          
           05  :FGPXXX:-APP-QSTKRES-REF  PIC S9(5) COMP-3.                      
      *                                                       105    9          
           05  :FGPXXX:-QSTOCKC          PIC S9(11)V9(06) COMP-3.               
      *                                                       112    3          
           05  :FGPXXX:-QSTKC-REF        PIC S9(5) COMP-3.                      
      *                                                       115    9          
           05  :FGPXXX:-CQE-QSTOCK       PIC S9(11)V9(06) COMP-3.               
      *                                                       124    3          
           05  :FGPXXX:-CQE-TOT-REF      PIC S9(5) COMP-3.                      
      *                                                       127    3          
           05  :FGPXXX:-CQE-QSTK-REF     PIC S9(5) COMP-3.                      
      *                                                       130    9          
           05  :FGPXXX:-CQE-QSTOCKRES    PIC S9(11)V9(06) COMP-3.               
      *                                                       139    3          
           05  :FGPXXX:-CQE-QSTKRES-REF  PIC S9(5) COMP-3.                      
      *                                                       142    9          
           05  :FGPXXX:-EPU-QSTOCK       PIC S9(11)V9(06) COMP-3.               
      *                                                       151    3          
           05  :FGPXXX:-EPU-QSTK-REF     PIC S9(5) COMP-3.                      
      *                                                       154    9          
           05  :FGPXXX:-EPU-QSTOCKRES    PIC S9(11)V9(06) COMP-3.               
      *                                                       163    3          
           05  :FGPXXX:-EPU-QSTKRES-REF  PIC S9(5) COMP-3.                      
      *                                                       166    9          
           05  :FGPXXX:-QSTOCKHS         PIC S9(11)V9(06) COMP-3.               
      *                                                       175    3          
           05  :FGPXXX:-QSTKHS-REF       PIC S9(5) COMP-3.                      
      *                                                       178    9          
           05  :FGPXXX:-QSTOCKP          PIC S9(11)V9(06) COMP-3.               
      *                                                       187    3          
           05  :FGPXXX:-QSTKP-REF        PIC S9(5) COMP-3.                      
      *                                                       190    9          
           05  :FGPXXX:-QSTOCK-GENGRO    PIC S9(11)V9(06) COMP-3.               
      *                                                       199    3          
           05  :FGPXXX:-QSTKGENGRO-REF   PIC S9(5) COMP-3.                      
      *                                                       202    9          
           05  :FGPXXX:-QSTOCK-PTF       PIC S9(11)V9(06) COMP-3.               
      *                                                       211    3          
           05  :FGPXXX:-QSTKPTF-REF      PIC S9(5) COMP-3.                      
      *                                                       214    9          
           05  :FGPXXX:-QSTOCKMAG        PIC S9(11)V9(06) COMP-3.               
      *                                                       223    3          
           05  :FGPXXX:-QSTKMAG-REF      PIC S9(5) COMP-3.                      
      *                                                       226    9          
           05  :FGPXXX:-QSTOCKMAGHS      PIC S9(11)V9(06) COMP-3.               
      *                                                       235    9          
           05  :FGPXXX:-QSTOCKMAGEPF     PIC S9(11)V9(06) COMP-3.               
      *                                                       244    9          
           05  :FGPXXX:-TOTQCDE          PIC S9(11)V9(06) COMP-3.               
      *                                                       253    3          
           05  :FGPXXX:-QCDE-REF         PIC S9(5) COMP-3.                      
      *                                                       256    8          
           05  :FGPXXX:-VOLUME           PIC S9(15) COMP-3.                     
      *                                                       264    3          
           05  :FGPXXX:-NBPALETTE        PIC S9(5) COMP-3.                      
      *                                                       267   20          
        02 :FGPXXX:-LLIEU                PIC X(20).                             
      *                                                       287   19          
        02 FILLER                         PIC  X(19).                           
      *                                                               *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
      * >>>>>>>>>>>>>>>>>>> FIN DE LA DESCRIPTION <<<<<<<<<<<<<<<<<<< *         
      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *         
                                                                                
