      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC00                             *         
      *****************************************************************         
      *--> LONGUEUR MAX ADMIS POUR UNE COMMAREA 32 767.                         
       01  MEC00C-COMMAREA.                                                     
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           05  MEC00C-DATA-ENTREE.                                              
               10  MEC00C-FICHIER-SI               PIC  X(10).                  
               10  MEC00C-DATA-SI                  PIC  X(4000).                
               10  MEC00C-DATA-SI-L                PIC  9(05).                  
               10  MEC00C-CODE-MAJ                 PIC  X(01).                  
               10  MEC00C-CODE-ACTION              PIC  X(01).                  
               10  MEC00C-IND-FIN-TRAITEMENT       PIC  X(01).                  
                   88  MEC00C-FIN-TRAITEMENT            VALUE 'T'.              
               10  MEC00C-PARAMETRES-MQ-OPTION.                                 
                   15  MEC00C-ALIAS                PIC  X(18).                  
                   15  MEC00C-MSGID                PIC  X(24).                  
                   15  MEC00C-DUREE                PIC S9(05).                  
                   15  MEC00C-PERSISTANCE          PIC  X(01).                  
                   15  MEC00C-PRIORITE             PIC S9(01).                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  MEC00C-SYNCPOINT            PIC S9(09) BINARY.           
      *--                                                                       
                   15  MEC00C-SYNCPOINT            PIC S9(09) COMP-5.           
      *}                                                                        
      *--> DONNEES RESULTANTES.                                                 
           05  MEC00C-DATA-SORTIE.                                              
               10  MEC00C-NBR-MESSAGE-ENVOYE       PIC  9(09).                  
               10  MEC00C-MESSAGE-ENVOYE-L         PIC  9(05).                  
               10  MEC00C-DERN-TABLE-SI-TRAITE     PIC  X(10).                  
               10  MEC00C-DERN-TABLE-ECOM-TRAITE   PIC  X(10).                  
               10  MEC00C-NBR-MESS-PAR-TABLE-T     PIC  9(09).                  
               10  MEC00C-NBR-ENREG-PAR-TABLE-T    PIC  9(09).                  
               10  MEC00C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC00C-CDRET-OK                  VALUE '0'.              
                   88  MEC00C-CDRET-ERR-NON-BLQ         VALUE '1'.              
                   88  MEC00C-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.              
                   88  MEC00C-CDRET-ERR-BLQ             VALUE '8'.              
                   88  MEC00C-CDRET-ERR-DB2-BLQ         VALUE '9'.              
               10  MEC00C-CODE-DB2                 PIC ++++9.                   
               10  MEC00C-CODE-DB2-DISPLAY         PIC  X(05).                  
               10  MEC00C-MESSAGE                  PIC  X(500).                 
      *--> DONNEES DE TRAVAIL A CONSERVE ENTRE CHACUN DES APPELS.               
           05  MEC00C-DATA-TRAVAIL.                                             
      *-->     PARAMETRES LU DE LA SOUS-TABLE PARAMETRES NCGFC(RVGA01FI)        
               10  MEC00C-TYPE-ENVOI-MQ            PIC  X(01).                  
                   88  MEC00C-TYPE-ENVOI-MQ-UNIQUE      VALUE '1'.              
                   88  MEC00C-TYPE-ENVOI-MQ-MULTI       VALUE '2'.              
               10  MEC00C-LONG-MESSAGE-MAX         PIC  9(05).                  
      *-->     PARAMETRES LU DE LA SOUS-TABLE EC001 (RVEC001)                   
               10  MEC00C-TABLE-SI                 PIC  X(10).                  
               10  MEC00C-TABLE-ECOMM              PIC  X(16).                  
               10  MEC00C-TABLE-ECOMM-L            PIC  9(02).                  
               10  MEC00C-ROOT-ECOMM               PIC  X(17).                  
               10  MEC00C-ROOT-ECOMM-L             PIC  9(02).                  
               10  MEC00C-DTD-ECOMM                PIC  X(20).                  
               10  MEC00C-DTD-ECOMM-L              PIC  9(02).                  
               10  MEC00C-WTRT                     PIC  X(01).                  
                   88  MEC00C-WTRT-MQ                   VALUE '1'.              
                   88  MEC00C-WTRT-FICHIER              VALUE '2'.              
                   88  MEC00C-WTRT-MQ-ET-FICHIER        VALUE '3'.              
      *{ remove-comma-in-dde 1.5                                                
      *            88  MEC00C-WTRT-ENVOIE-MQ            VALUE '1', '3'.         
      *--                                                                       
                   88  MEC00C-WTRT-ENVOIE-MQ            VALUE '1'  '3'.         
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *            88  MEC00C-WTRT-FICHIER-REQUIS       VALUE '2', '3'.         
      *--                                                                       
                   88  MEC00C-WTRT-FICHIER-REQUIS       VALUE '2'  '3'.         
      *}                                                                        
      *-->     PARAMETRES LU DE LA SOUS-TABLE EC002 (RVEC002)                   
               10  MEC00C-NBR-OCCURENCE            PIC  9(03).                  
               10  MEC00C-NBR-OCCURENCE-MAX        PIC  9(03) VALUE 100.        
               10  MEC00C-TABLE-ATTRIBUT OCCURS 100 INDEXED I-ATTR.             
                   15  MEC00C-CMASQ                PIC  X(07).                  
                   15  MEC00C-CMASQ-REDEF REDEFINES MEC00C-CMASQ.               
                       20  MEC00C-CMASQ-LONG       PIC  9(05).                  
                       20  MEC00C-CMASQ-TYPE       PIC  X(01).                  
      *{ remove-comma-in-dde 1.5                                                
      *                    88  MEC00C-TYPE-ALPHA      VALUE 'A' , 'B'.          
      *--                                                                       
                           88  MEC00C-TYPE-ALPHA      VALUE 'A'   'B'.          
      *}                                                                        
                           88  MEC00C-TYPE-BLANC      VALUE 'B'.                
                           88  MEC00C-TYPE-CODE       VALUE 'C'.                
                           88  MEC00C-TYPE-HTML       VALUE 'H'.                
                           88  MEC00C-TYPE-NUMERIC    VALUE 'N'.                
                           88  MEC00C-TYPE-PACKED     VALUE 'P'.                
                       20  MEC00C-CMASQ-DEC        PIC  9(01).                  
                   15  MEC00C-ATTRIBUT             PIC  X(20).                  
                   15  MEC00C-ATTRIBUT-L           PIC  9(02).                  
      *--> DONNEES DE TRAVAIL DIVERSES.                                         
               10  MEC00C-DATA-XML                 PIC  X(24671).               
               10  MEC00C-DATA-XML-L               PIC  9(05).                  
               10  MEC00C-BALISE-DEBUT-FIN.                                     
                   15  MEC00C-BALISE-DEBUT         PIC  X(50).                  
                   15  MEC00C-BALISE-DEBUT-L       PIC  9(02).                  
                   15  MEC00C-BALISE-FIN           PIC  X(50).                  
                   15  MEC00C-BALISE-FIN-L         PIC  9(02).                  
               10  MEC00C-LIGNE-DTD                PIC  X(60).                  
               10  MEC00C-LIGNE-DTD-L              PIC  9(02).                  
               10  MEC00C-DONNEES-COMPTE-RENDU.                                 
                   15  MEC00C-NBR-ENREG-LU         PIC  9(07).                  
                   15  MEC00C-TABLE-SI-TRAITE      PIC  X(10).                  
                   15  MEC00C-TABLE-ECOMM-TRAITE   PIC  X(10).                  
                   15  MEC00C-NBR-MESS-PAR-TABLE   PIC  9(07).                  
                   15  MEC00C-NBR-ENREG-PAR-TABLE PIC   9(07).                  
           05  FILLER                              PIC  X(268).                 
      ********* FIN DE LA ZONE DE COMMUNICATION DU MEC00 *************          
                                                                                
