      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT  "QCHS01" *               
      *    LONGUEUR : 88                                        *               
      *---------------------------------------------------------*               
       01  HS01-ENREG.                                                          
           05  FILLER                PIC X(07)  VALUE 'QCHS01;'.                
           05  HS01-NLIEUHED         PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-WTLMELA          PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-CTRAIT           PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-CFAM             PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-LVPARAM          PIC X(13).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-NBRENR           PIC 9(09).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-PRMP             PIC -9(9)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-DATDEB.                                                     
               10  HS01-DEB-SA       PIC X(04).                                 
               10  HS01-DEB-MM       PIC X(02).                                 
               10  HS01-DEB-JJ       PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-DATFIN.                                                     
               10  HS01-FIN-SA       PIC X(04).                                 
               10  HS01-FIN-MM       PIC X(02).                                 
               10  HS01-FIN-JJ       PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  HS01-CTYPHS           PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
