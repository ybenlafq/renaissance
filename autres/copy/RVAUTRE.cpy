      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE AUTRE AUTORISATION SAISIR REMISE       *        
      *----------------------------------------------------------------*        
       01  RVAUTRE .                                                            
           05  AUTRE-CTABLEG2    PIC X(15).                                     
           05  AUTRE-CTABLEG2-REDEF REDEFINES AUTRE-CTABLEG2.                   
               10  AUTRE-USER            PIC X(07).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  AUTRE-WTABLEG     PIC X(80).                                     
           EXEC SQL END DECLARE SECTION END-EXEC.
           05  AUTRE-WTABLEG-REDEF  REDEFINES AUTRE-WTABLEG.                    
               10  AUTRE-NATURE          PIC X(01).                             
               10  AUTRE-DELAI           PIC X(03).                             
               10  AUTRE-DELAI-N        REDEFINES AUTRE-DELAI                   
                                         PIC 9(03).                             
               10  AUTRE-AUTOR           PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVAUTRE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTRE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  AUTRE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  AUTRE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  AUTRE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
