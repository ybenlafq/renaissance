      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT "QHS96101"*               
      *    LONGUEUR : 51                                        *               
      *---------------------------------------------------------*               
       01  H101-ENREG.                                                          
           05  H101-NOM              PIC X(08)  VALUE 'QHS96101'.               
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H101-DATET.                                                      
               10  H101-DEB-SA       PIC X(04).                                 
               10  H101-DEB-MM       PIC X(02).                                 
               10  H101-DEB-JJ       PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H101-CLIEUHET         PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H101-NLIEUHED         PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H101-NBRENR           PIC Z(8)9.                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H101-PRMP             PIC -(8)9,99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
