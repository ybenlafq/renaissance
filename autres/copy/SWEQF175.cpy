      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------------*                
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT "QMF175" *                
      *    LONGUEUR : 174                                      *                
      *--------------------------------------------------------*                
       01  F175-ENREG.                                                          
           05  F175-CODETAT          PIC X(06)  VALUE 'QMF175'.                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-SOCIETE          PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-DATEDEB.                                                    
               10  F175-DAT-SADE     PIC X(04).                                 
               10  F175-DAT-MDEB     PIC X(02).                                 
               10  F175-DAT-JDEB     PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-DATEFIN.                                                    
               10  F175-DAT-SAFI     PIC X(04).                                 
               10  F175-DAT-MFIN     PIC X(02).                                 
               10  F175-DAT-JFIN     PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NSOCORIG         PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NLIEUORIG        PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NSSLIEUORIG      PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-CLIEUTRTORIG     PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NSOCDEST         PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NLIEUDEST        PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NSSLIEUDEST      PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-CLIEUTRTDEST     PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NORIGINE         PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-QMVT             PIC -9(5).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-NCODIC           PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-CFAM             PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-CMARQ            PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-LREFFOURN        PIC X(20).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-DATMVT           PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-MAGASIN          PIC X(06).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-TYPEHS           PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-PRMP             PIC -9(7)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-PVTTC            PIC -9(7)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  F175-ECOPA            PIC -9(5)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
