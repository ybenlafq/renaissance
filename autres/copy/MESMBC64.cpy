      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:02 >
      
      ******************************************************************
      *                             DABORD LA COPY DE L"ENTETE MESSMQ  *
      *   MESMBC64 COPY                                                *
      * - CREATION / MAJ CLIENT                                        *
      * COPY POUR LES PROGRAMMES MBC64 CONTROLE DE VOIE POUR AS/400    *
      ******************************************************************
      *
      * LONGUEUR DE 34  OCTETS
           05 MESMBC64-RETOUR.
              10 MESMBC64-CODE-RETOUR.
                 15  MESMBC64-CODRET  PIC X(1).
                      88  MESMBC64-CODRET-OK        VALUE ' '.
                      88  MESMBC64-CODRET-ERREUR    VALUE '1'.
                      88  MESMBC64-CODRET-INFO      VALUE '2'.
                 15  MESMBC64-LIBERR  PIC X(60).
           05 MESMBC64-DATA.
      *    DNN�ES SELECTION
              10 MESMBC64-DATA-SELECTION.
      *          CODE INSEE
                 15 MESMBC64-CINSEE   PIC X(05).
      *          TYPE DE VOIE
                 15 MESMBC64-CTVOIE   PIC X(04).
      *          CRITERE DE RECHERCHE
                 15 MESMBC64-LNOMVOIE PIC X(42).
      *          NUMERO DANS LA VOIE
                 15 MESMBC64-CVOIE    PIC X(05).
      *          NB MAXI ENREGISTREMENTS RETOUR
                 15 MESMBC64-MAXI     PIC 9(02).
      *          NOMBRE POSTES
                 15 MESMBC64-R-NBP    PIC 9(02).
      *          DEMANDE DE LA RECHERCHE
                 15 MESMBC64-TYPE     PIC X VALUE ' '.
                    88 MESMBC64-INFOCLI              VALUE ' '.
                    88 MESMBC64-PF4                  VALUE '1'.
      *
                 15 FILLER            PIC X(50).
      *
           05 MESMBC64-DATA-REPONSE     OCCURS 30.
      *              CODE INSEE
                     25 MESMBC64-R-CINSEE     PIC X(05).
      *              NUMERO NATIONAL DE VOIE
                     25 MESMBC64-R-NVOIE      PIC X(04).
      *              TYPE VOIE
                     25 MESMBC64-R-CTVOIE     PIC X(04).
      *              NOM VOIE
                     25 MESMBC64-R-LNOMV      PIC X(21).
      *              NOM RECHERCHE
                     25 MESMBC64-R-LNOMPV     PIC X(42).
      *              CODE CILOT
                     25 MESMBC64-R-CILOT      PIC X(08).
      *              CODE PARITE
                     25 MESMBC64-R-CPARITE    PIC X(01).
      *              DATE DERNIERE MAJ
                     25 MESMBC64-R-DSYST      PIC 9(13) COMP-3.
      *
                     25 MESMBC64-R-NUMVOIE    PIC X(08).
      *              DERNIER MOT
                     25 MESMBC64-R-DERMOT     PIC X(20).
                     25 FILLER                PIC X(20).
      *
      *****************************************************************
      
