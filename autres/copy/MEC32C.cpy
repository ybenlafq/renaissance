      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC32                             *         
      *****************************************************************         
      *--> LONGUEUR MAX ADMIS POUR UNE COMMAREA 32 767.                         
       01  MEC32C-COMMAREA.                                                     
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           02  MEC32C-DATA-ENTREE.                                              
             05  MEC32C-PSE.                                                    
               10  MEC32C-NSOC-PSE            PIC X(03).                        
               10  MEC32C-NLIEU-PSE           PIC X(03).                        
               10  MEC32C-NVENTE-PSE          PIC X(07).                        
               10  MEC32C-NCDEWC              PIC X(08).                        
               10  MEC32C-NCDEWC9 REDEFINES MEC32C-NCDEWC                       
                                              PIC 9(08).                        
               10  MEC32C-PREGLTVTE  PIC S9(7)V9(0002) COMP-3.                  
             05    FILLER                     PIC X(74).                        
      *--> DONNEES RESULTANTES.                                                 
           02  MEC32C-DATA-SORTIE.                                              
      * NBR MESSSAGE ENVOY�                                                     
               10  MEC32C-NBR-MESSAGE-ENVOYE       PIC  9(09).                  
               10  MEC32C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC32C-CDRET-OK                  VALUE '0'.              
                   88  MEC32C-CDRET-ERR-NON-BLQ         VALUE '1'.              
                   88  MEC32C-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.              
                   88  MEC32C-CDRET-ERR-BLQ             VALUE '8'.              
                   88  MEC32C-CDRET-ERR-DB2-BLQ         VALUE '9'.              
               10  MEC32C-CODE-DB2                 PIC ++++9.                   
               10  MEC32C-CODE-DB2-DISPLAY         PIC  X(05).                  
      ********* FIN DE LA ZONE DE COMMUNICATION      *************              
                                                                                
