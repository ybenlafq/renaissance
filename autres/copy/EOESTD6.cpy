      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        *
      * FICHIER.....:     : DECLARATION PRODUIT          (OBLIGATOIRE)*
      * FICHIER.....: EMISSION OP ENT�TE STANDARD (MUTATION)         *
      * NOM FICHIER.: EOESTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 098                                             *
      *****************************************************************
      *
       01  EOESTD6.
      * CODE IDENTIFICATEUR
           05      EOESTD6-TYP-ENREG      PIC  X(0006).
      * CODE SOCI�T�
           05      EOESTD6-CSOCIETE       PIC  X(0005).
      * NUM�RO D'OP
           05      EOESTD6-NOP            PIC  X(0015).
      * CODE TRANSPORTEUR
           05      EOESTD6-CTRANSPORTEUR  PIC  X(0013).
      * MODE DE TRANSPORT
           05      EOESTD6-MODE-EXPED     PIC  X(0003).
      * ETAT
           05      EOESTD6-ETAT           PIC  X(0001).
      * DATE DU PASSAGE � L'ETAT
           05      EOESTD6-DETAT          PIC  X(0008).
      * HEURE DU PASSAGE � L'ETAT
           05      EOESTD6-HETAT          PIC  X(0006).
      * NUM�RO D'EXP�DITON
           05      EOESTD6-NEXPED         PIC  9(0005).
      * NUM�RO VAGUE
           05      EOESTD6-NVAGUE         PIC  9(0005).
      * POIDS DE L'OP
           05      EOESTD6-POIDS          PIC  9(0009).
      * VOLUME DE L'OP
           05      EOESTD6-VOLUME         PIC  9(0012).
      * NOMBRE DE COLIS DE L'OP
           05      EOESTD6-NB-COLIS       PIC  9(0005).
      * FILLER 30
           05      EOESTD6-FILLER         PIC  X(0030).
      * FIN
           05      EOESTD6-FIN            PIC  X(0001).
      
