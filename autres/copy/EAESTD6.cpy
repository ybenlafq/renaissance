      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE ENTETE STANDARD                *
      * NOM FICHIER.: EAESTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 301                                             *
      *****************************************************************
      *
       01  EAESTD6.
      * TYPE ENREGISTREMENT
           05      EAESTD6-TYP-ENREG      PIC  X(0006).
      * CODE ETAT
           05      EAESTD6-CETAT          PIC  X(0001).
      * CODE SOCIETE
           05      EAESTD6-CSOCIETE       PIC  X(0005).
      * CODE ARTICLE
           05      EAESTD6-CARTICLE       PIC  X(0018).
      * LIBELLE ARTICLE
           05      EAESTD6-LARTICLE       PIC  X(0035).
      * LIBELLE COMPLEMENTAIRE
           05      EAESTD6-LCOMPL         PIC  X(0035).
      * CODE RACINE
           05      EAESTD6-CRACINE        PIC  X(0018).
      * TYPE D ARTICLE
           05      EAESTD6-TYPE-ARTICLE   PIC  9(0002).
      * DISPONIBLE
           05      EAESTD6-DISPONIBLE     PIC  9(0001).
      * VERTICAL
           05      EAESTD6-VERTICAL       PIC  9(0001).
      * DANGEREUX
           05      EAESTD6-DANGEREUX      PIC  9(0001).
      * AEROSOL
           05      EAESTD6-AEROSOL        PIC  9(0001).
      * FRAGILE
           05      EAESTD6-FRAGILE        PIC  9(0001).
      * RELEVE LOT
           05      EAESTD6-REL-LOT        PIC  9(0001).
      * RELEVE NUMERO DE SERIE
           05      EAESTD6-REL-NSERIE     PIC  9(0001).
      * ROTATION
           05      EAESTD6-ROTATION       PIC  X(0001).
      * PRIX UNITAIRE MOYEN PONDERE (PUMP)
           05      EAESTD6-PUMP           PIC  9(0010)V9(5).
      * TYPE DE SORTIE
           05      EAESTD6-TYPE-SORTIE    PIC  9(0003).
      * FENETRE TYPE DE SORTIE
           05      EAESTD6-FEN-TYPE-SORTIE PIC 9(0005).
      * POURCENTAGE D ALCOOL
           05      EAESTD6-PER-ALCOOL     PIC  9(0003)V9(2).
      * VOLUME LIQUIDE
           05      EAESTD6-VOL-LIQUIDE    PIC  9(0010).
      * CODE-A-BARRES
           05      EAESTD6-CODE-BARRES    PIC  X(0020).
      * IDENTIFIANT FAMILLE DE COLISAGE
           05      EAESTD6-ID-FAM-COLISAGE PIC X(0005).
      * IDENTIFIANT FAMILLE DE STOCKAGE
           05      EAESTD6-ID-FAM-STOCKAGE PIC 9(0003).
      * SEUIL PREPA PALETTE
           05      EAESTD6-SEUIL-PREP-PAL PIC  9(0009).
      * PREMIERE RECEPTION
           05      EAESTD6-1ERE-RECEPTION PIC  9(0001).
      * QTE TOLERANCE INVENTAIRE
           05      EAESTD6-QTE-TOL-INVENT PIC  9(0003).
      * DELAI INTER-INVENTAIRE
           05      EAESTD6-DELAI-INTER-INV PIC 9(0003).
      * CARACTERISTIQUE 1
           05      EAESTD6-CARACT1        PIC  X(0020).
      * CARACTERISTIQUE 2
           05      EAESTD6-CARACT2        PIC  X(0020).
      * CARACTERISTIQUE 3
           05      EAESTD6-CARACT3        PIC  X(0020).
      * FILLER 30
           05      EAESTD6-FILLER         PIC  X(0030).
      * FIN
           05      EAESTD6-FIN            PIC  X(0001).
      
