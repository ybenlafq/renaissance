      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE EDITION APPAREILS TERMINES            *            
      *  PROJET CHS                                                *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MHC50                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MHC5-LONG-COMMAREA PIC S9(4) COMP VALUE +100.                   
      *--                                                                       
       01  COMM-MHC5-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +100.                 
      *}                                                                        
       01  Z-COMMAREA-MHC5.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MHC5-I.                                                      
      *                                                                         
              03 COMM-MHC5-NLIEUHC         PIC    X(03).                        
              03 COMM-MHC5-TEBRB           PIC    X(02).                        
              03 COMM-MHC5-CIMP            PIC    X(04).                        
              03 FILLER                    PIC    X(50).                        
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MHC5-O.                                                      
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MHC5-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MHC5-OK           VALUE '0'.                           
                 88 COMM-MHC5-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MHC5-MSG             PIC    X(40).                        
                                                                                
