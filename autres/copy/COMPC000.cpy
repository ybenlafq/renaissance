      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *                                                                *        
      *  PROJET     : APPLICATION TRAITEMENT HISTORISATION DES BATCH   *        
      *  PROGRAMMES : MPC00                                            *        
      *  TITRE      : COMMAREA DU MODULE D'ALIMENTATION TABLE RTPC00   *        
      *                   TABLE COMPTE-RENDU ACTIVITE DES CHAINES      *        
      *  LONGUEUR   :                                                  *        
      *                                                                *        
      ******************************************************************        
       01  COMM-PC00-APPLI.                                                     
      *--- DONNEES EN ENTREE DU MODULE ------------------------------ 12        
           05 COMM-PC00-DONNEES-ENTREE.                                         
              10 COMM-PC00-CAPPLI          PIC X(10).                           
              10 COMM-PC00-CFONCTION       PIC X(10).                           
              10 COMM-PC00-DDEBUT          PIC X(8).                            
              10 COMM-PC00-HDEBUT          PIC X(6).                            
              10 COMM-PC00-NSOC            PIC X(3).                            
              10 COMM-PC00-NLIEU           PIC X(3).                            
              10 COMM-PC00-LIBELLE         PIC X(30).                           
              10 COMM-PC00-NBENREGL        PIC S9(7)V USAGE COMP-3.             
              10 COMM-PC00-NBENREGT        PIC S9(7)V USAGE COMP-3.             
              10 COMM-PC00-STATUT          PIC X(2).                            
      *--- CODE RETOUR + MESSAGE ------------------------------------ 59        
           05 COMM-PC00-MESSAGE.                                                
              10 COMM-PC00-CODRET           PIC X(01).                          
                 88 COMM-PC00-CODRET-OK                  VALUE ' '.             
                 88 COMM-PC00-CODRET-ERREUR              VALUE '1'.             
              10 COMM-PC00-LIBERR           PIC X(58).                          
                                                                                
