      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                         
      *                                                                         
      ******************************************************************        
      * PREPARATION DE L'ACCES AU FICHIER FETD5C PAR LE CHEMIN FETD5K           
      ******************************************************************        
      *                                                                         
       CLEF-FETD5K             SECTION.                                         
      *                                                                         
      *                                                                         
           MOVE FETD5-FETD5K TO VSAM-KEY.                                       
      *                                                                         
           MOVE 'FETD5K' TO PATH-NAME.                                          
           MOVE 'FETD50' TO FILE-NAME.                                          
           MOVE +20  TO FILE-LONG.                                              
           MOVE FETD5    TO Z-INOUT.                                            
      *                                                                         
       FIN-CLEF-FETD5K.  EXIT.                                                  
                EJECT                                                           
                                                                                
