      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA POUR LE MODULE XML                                             
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 COM-XML-LONG-COMMAREA PIC S9(7) COMP VALUE +66085.                    
      *--                                                                       
       01 COM-XML-LONG-COMMAREA PIC S9(7) COMP-5 VALUE +66085.                  
      *}                                                                        
      * ZONES APPLICATIVES XML GENERALES                                        
       01 COMM-XML.                                                             
          02 COMM-XML-NOMPROG              PIC X(06).                           
          02 COMM-XML-DTD                  PIC X(20).                           
          02 COMM-XML-CRETOUR              PIC X(02).                           
          02 COMM-XML-LRETOUR              PIC X(50).                           
          02 COMM-XML-ITEM-XSD             PIC 9(05) COMP-3.                    
          02 COMM-XML-DONNEES-LG           PIC 9(07) COMP-3.                    
          02 COMM-XML-DONNEES              PIC X(66000).                        
      *                                                                         
      *****************************************************************         
                                                                                
