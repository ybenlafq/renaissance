      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *    COMMAREA POUR LES MESSAGES GENERALISES (MPTMG)                       
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-PTMG-LONG-COMMAREA PIC S9(4)  COMP VALUE +200.                  
      *--                                                                       
       01  COMM-PTMG-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
      *}                                                                        
       01  Z-COMMAREA-PTMG.                                                     
   1       02  PTMG-CODLANG  PIC X(0002) VALUE 'FR'.                            
   3       02  PTMG-CODPIC   PIC X(0002) VALUE '03'.                            
   5       02  PTMG-CNOMPGRM PIC X(0005).                                       
  10       02  PTMG-NSEQERR  PIC X(0004).                                       
  14       02  PTMG-NUM      PIC S9(13)V99 COMP-3 OCCURS 5.                     
  54       02  PTMG-LIBERR   PIC X(100).                                        
  54       02  PTMG-ALP REDEFINES PTMG-LIBERR PIC X(20) OCCURS 5.               
 154 ***L=153                                                                   
                                                                                
