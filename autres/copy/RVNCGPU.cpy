      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE NCGPU MENUS NCG PLUS                   *        
      *----------------------------------------------------------------*        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVNCGPU.                                                             
           05  NCGPU-CTABLEG2    PIC X(15).                                     
           05  NCGPU-CTABLEG2-REDEF REDEFINES NCGPU-CTABLEG2.                   
               10  NCGPU-ZUSER           PIC X(07).                             
               10  NCGPU-NIV1            PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  NCGPU-NIV1-N         REDEFINES NCGPU-NIV1                    
                                         PIC 9(02).                             
               10  NCGPU-NIV2            PIC X(02).                             
               10  NCGPU-NIV2-N         REDEFINES NCGPU-NIV2                    
                                         PIC 9(02).                             
               10  NCGPU-NIV3            PIC X(02).                             
               10  NCGPU-NIV3-N         REDEFINES NCGPU-NIV3                    
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  NCGPU-WTABLEG     PIC X(80).                                     
           05  NCGPU-WTABLEG-REDEF  REDEFINES NCGPU-WTABLEG.                    
               10  NCGPU-LIBELLE         PIC X(40).                             
               10  NCGPU-TRAN            PIC X(04).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVNCGPU-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NCGPU-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  NCGPU-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  NCGPU-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  NCGPU-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
