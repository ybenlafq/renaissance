      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQUTY TYPE D EQUIPAGE                  *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUTY.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUTY.                                                             
      *}                                                                        
           05  EQUTY-CTABLEG2    PIC X(15).                                     
           05  EQUTY-CTABLEG2-REDEF REDEFINES EQUTY-CTABLEG2.                   
               10  EQUTY-CEQUI           PIC X(03).                             
           05  EQUTY-WTABLEG     PIC X(80).                                     
           05  EQUTY-WTABLEG-REDEF  REDEFINES EQUTY-WTABLEG.                    
               10  EQUTY-LEQUI           PIC X(20).                             
               10  EQUTY-TLIVR           PIC X(05).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUTY-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUTY-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUTY-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQUTY-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUTY-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQUTY-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
