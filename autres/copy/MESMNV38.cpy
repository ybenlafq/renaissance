      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                            DABORD LA COPY DE L"ENTETE MESSMQ  *         
      *                                                               *         
      *   COPY MESSAGES MQ ENTRE LES PROGRAMMES :                     *         
      *   - MNV38  : MISE A JOUR DES PRIX INNOVENTE                   *         
      *   - MKL00  : ENVOI DES ETIQUETTES DE PRIX A KLEE              *         
      *                                                               *         
      *   LONGUEUR : 5807                                             *         
      *****************************************************************         
      *                                                                         
              05 MESMNV38-DATA.                                                 
                 10  MESMNV38-NCODIC              PIC X(07).                    
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          10  I-PX-MAX PIC S9(4) COMP.                                   
      *--                                                                       
                 10  I-PX-MAX PIC S9(4) COMP-5.                                 
      *}                                                                        
                 10  MESMNV38-PX OCCURS 300.                                    
                     15  MESMNV38-MAG-PX.                                       
                         20  MESMNV38-NSOC-PX     PIC X(03).                    
                         20  MESMNV38-NLIEU-PX    PIC X(03).                    
                     15  MESMNV38-PRIX-PX     PIC S9(07)V99 COMP-3.             
                     15  MESMNV38-PRIX-DSYST  PIC S9(13)V   COMP-3.             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          10  I-PM-MAX PIC S9(4) COMP.                                   
      *--                                                                       
                 10  I-PM-MAX PIC S9(4) COMP-5.                                 
      *}                                                                        
                 10  MESMNV38-PM OCCURS 300.                                    
                     15  MESMNV38-MAG-PM.                                       
                         20  MESMNV38-NSOC-PM     PIC X(03).                    
                         20  MESMNV38-NLIEU-PM    PIC X(03).                    
                     15  MESMNV38-PRIME-PM    PIC S9(05)V99 COMP-3.             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          10  I-CH-MAX PIC S9(4) COMP.                                   
      *--                                                                       
                 10  I-CH-MAX PIC S9(4) COMP-5.                                 
      *}                                                                        
                 10  MESMNV38-CH OCCURS 300.                                    
                     15  MESMNV38-CTYPE       PIC X(02).                        
                     15  MESMNV38-MAG.                                          
                         20  MESMNV38-NSOC    PIC X(03).                        
                         20  MESMNV38-NLIEU   PIC X(03).                        
      *                                                                         
                                                                                
