      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COMMAREA SPECIFIQUE AU MODULE TP MSP01 EQUIVALENT AU MSP00            
      *   MAIS SPECIFIQUE AUX TRANSACTIONS DE TYPE LOGISTIQUE                   
      *   NOTAMMENT APPELE PAR LE TGF24                                         
      *****************************************************************         
       01  COMM-MSP01-APPLI.                                                    
           02 COMM-MSP01-ZONES-ENTREE.                                          
              05 COMM-MSP01-NSOCDEPOT          PIC X(03).                       
              05 COMM-MSP01-NCODIC             PIC X(07).                       
              05 COMM-MSP01-DATE               PIC X(08).                       
              05 COMM-MSP01-NCDE               PIC X(07).                       
              05 COMM-MSP01-NREC               PIC X(07).                       
              05 COMM-MSP01-NBL                PIC X(10).                       
              05 COMM-MSP01-CFAM               PIC X(05).                       
           02 COMM-MSP01-ZONES-SORTIE.                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MSP01-CODRET      COMP   PIC  S9(4).                      
      *--                                                                       
              05 COMM-MSP01-CODRET COMP-5   PIC  S9(4).                         
      *}                                                                        
              05 COMM-MSP01-SRPREC             PIC S9(7)V9(6) COMP-3.           
              05 COMM-MSP01-SRPCDE             PIC S9(7)V9(6) COMP-3.           
              05 COMM-MSP01-PABASEFACT         PIC S9(7)V9(2) COMP-3.           
              05 COMM-MSP01-PROMOFACT          PIC S9(7)V9(2) COMP-3.           
              05 COMM-MSP01-QCOEFF1            PIC S9(1)V9(4) COMP-3.           
              05 COMM-MSP01-QCOEFF2            PIC S9(1)V9(4) COMP-3.           
              05 COMM-MSP01-TABLE              PIC X(06).                       
                                                                                
