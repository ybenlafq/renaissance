      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC05                             *         
      *****************************************************************         
      *--> LONGUEUR MAX ADMIS POUR UNE COMMAREA 32 767.                         
       01  MEC65-COMMAREA.                                                      
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           02  MEC65-DATE-JOUR              PIC X(08).                          
           02  MEC65-DDELIV                 PIC X(08).                          
           02  MEC65-MUT-BSL300             PIC X(01).                          
           02  MEC65-MUT-MULTI              PIC X(01).                          
           02  MEC65-REC-FILIERE            PIC X(01).                          
           02  MEC65-DATA-ENTREE.                                               
             05  MEC65-TABLE  OCCURS  40.                                       
               10  MEC65-CODIC             PIC  X(7).                           
               10  MEC65-DEPOT             PIC  X(3).                           
               10  MEC65-QTE               PIC S9(5) COMP-3.                    
               10  MEC65-WEMPORTE          PIC  X(1).                           
               10  MEC65-MUTATION          PIC  X(7).                           
             05    FILLER                  PIC  X(50).                          
      *--> DONNEES RESULTANTES.                                                 
           02  MEC65-DATA-SORTIE.                                               
      * NBR MESSSAGE ENVOY�                                                     
               10  MEC65-FILIERE                   PIC  X(02).                  
               10  MEC65-DATE-DACEM                PIC  X(08).                  
               10  MEC65-CODE-RET                 PIC  X(01).                   
                   88  MEC65-CDRET-OK                  VALUE '0'.               
                   88  MEC65-CDRET-ERR-NON-BLQ         VALUE '1'.               
                   88  MEC65-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.               
                   88  MEC65-CDRET-ERR-BLQ             VALUE '8'.               
                   88  MEC65-CDRET-ERR-DB2-BLQ         VALUE '9'.               
               10  MEC65-CODE-DB2                 PIC ++++9.                    
               10  MEC65-CODE-DB2-DISPLAY         PIC  X(05).                   
               10  MEC65-PRESENCE-095             PIC  X(01).                   
      ********* FIN DE LA ZONE DE COMMUNICATION      *************              
                                                                                
