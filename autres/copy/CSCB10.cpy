      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * COMMUNICATION GESTION PRIX PRIMES                                       
E0265 ******************************************************************        
      * DSA057 29/05/06 SUPPORT EVOLUTION CBN                                   
      *                 TS AFFICHAGE TRANS CB10                                 
      * !!!!! LES LIGNES MARQUEES PAR H!!!! CONTIENNENT                         
      * !!!!! DES CARACTERES NON VISUALISABLES                                  
      * !!!!! NE PAS Y TOUCHER                                                  
************************************************************************        
      * TS AFFICHAGE                                                            
      ******************************************************************        
       EFFACE-TS-CB10 SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB10-IDENT)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB10-IDENT)                                         
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB10-IDENT                    
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB10-NB                                             
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB10 SECTION.                                                   
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB10-IDENT)                                         
      *        FROM     (TS-CB10-ENR)                                           
      *        NUMITEMS (TB-CB10-NB)                                            
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB10-IDENT)                                         
               FROM     (TS-CB10-ENR)                                           
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB10-NB)                                            
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB10-ENR TO DFHB0020                        
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB10-IDENT TS-CB10-ENR        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB10-NB                                  
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB10-IDENT)
                  NUMITEMS (TB-CB10-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB10 SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB10-IDENT)                                         
      *        INTO     (TS-CB10-ENR)                                           
      *        LENGTH   (LENGTH OF TS-CB10-ENR)                                 
      *        ITEM     (TB-CB10-POS)                                           
      *        NUMITEMS (TB-CB10-NB)                                            
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB10-IDENT)                                         
               INTO     (TS-CB10-ENR)                                           
               LENGTH   (LENGTH OF TS-CB10-ENR)                                 
               ITEM     (TB-CB10-POS)                                           
               NUMITEMS (TB-CB10-NB)                                            
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB10-ENR TO DFHB0020                        
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB10-IDENT TS-CB10-ENR        
      *dfhei*     DFHB0020 TB-CB10-NB TB-CB10-POS                               
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB10 SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB10-IDENT)                                         
      *        FROM     (TS-CB10-ENR)                                           
      *        ITEM     (TB-CB10-POS)                                           
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB10-IDENT)                                         
               FROM     (TS-CB10-ENR)                                           
               ITEM     (TB-CB10-POS)                                           
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB10-ENR TO DFHB0020                        
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB10-IDENT TS-CB10-ENR        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB10-POS                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
                                                                                
                                                                                
