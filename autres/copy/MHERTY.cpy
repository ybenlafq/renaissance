      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
       RECHERCHE-NTYOPE        SECTION.                                         
      *    SI TRAITEMENT "MARCHE AVANT"                                         
           IF AV                                                                
              PERFORM VARYING COMM-HE-I      FROM  1  BY 1                      
                UNTIL         COMM-HE-I   >  COMM-HE-MAX                        
                OR            COMM-HE-FLAV  (COMM-HE-I)  =  W-FLAGAV            
              END-PERFORM                                                       
              IF COMM-HE-I  > COMM-HE-MAX                                       
                 STRING 'ERREUR PARAMETRAGE S/TABLE HEGOP POUR FLAG '           
                 'AVANT = ' W-FLAGAV '.' DELIMITED SIZE INTO MESS               
                 PERFORM ABANDON-TACHE                                          
              ELSE                                                              
                 MOVE COMM-HE-OPAVDE(COMM-HE-I) TO W-TYOPDEB                    
                 MOVE COMM-HE-OPAVFI(COMM-HE-I) TO W-TYOPFIN                    
              END-IF                                                            
           ELSE                                                                 
      *       SINON TRAITEMENT "MARCHE ARRIERE"                                 
              IF AR                                                             
                 PERFORM VARYING COMM-HE-I     FROM  1  BY 1                    
                   UNTIL         COMM-HE-I   > COMM-HE-MAX                      
                   OR            COMM-HE-FLAR (COMM-HE-I)  =  W-FLAGAR          
                 END-PERFORM                                                    
                 IF COMM-HE-I >  COMM-HE-MAX                                    
                    STRING 'ERREUR PARAMETRAGE S/TABLE HEGOP POUR FLAG '        
                   'ARRIERE = ' W-FLAGAR '.' DELIMITED SIZE INTO MESS           
                   PERFORM ABANDON-TACHE                                        
                 ELSE                                                           
                    MOVE COMM-HE-OPARDE(COMM-HE-I) TO W-TYOPDEB                 
                    MOVE COMM-HE-OPARFI(COMM-HE-I) TO W-TYOPFIN                 
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
                                                                                
