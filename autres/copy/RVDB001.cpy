      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DB001 DESCRIPTION DES OFFRES           *        
      *----------------------------------------------------------------*        
       01  RVDB001.                                                             
           05  DB001-CTABLEG2    PIC X(15).                                     
           05  DB001-CTABLEG2-REDEF REDEFINES DB001-CTABLEG2.                   
               10  DB001-COFFRE          PIC X(05).                             
               10  DB001-CENREG          PIC X(07).                             
           05  DB001-WTABLEG     PIC X(80).                                     
           05  DB001-WTABLEG-REDEF  REDEFINES DB001-WTABLEG.                    
               10  DB001-CTYPE           PIC X(01).                             
               10  DB001-LIBELLE         PIC X(20).                             
               10  DB001-FLAG            PIC X(20).                             
               10  DB001-FLAG2           PIC X(05).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVDB001-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DB001-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DB001-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DB001-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DB001-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
