      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:32 >
      
      *==> DARTY *** EDITION ETAT / CICS *****************************
      *          ==> DESCRIPTION DE LA ZONE DE COMMANDE COBOL        *
      *          ==> PROTOCOLE IMPRIMANTE GRAPHIQUE                  *
      ***************************************************** MIGGRA0C *
       01  IGG-CMD.
           05 IGG-ORD       PIC  X.
           05 IGG-CCC       PIC  XXX.
           05 IGG-LL6       PIC  XXX.
           05 IGG-L12       PIC  XXX.
           05 IGG-RVR       PIC  X.
           05 IGG-VVV       PIC  XX.
           05 IGG-HHH       PIC  XX.
           05 IGG-SNS       PIC  XX.
           05 IGG-EEE       PIC  X.
           05 IGG-EBE       PIC  X.
           05 IGG-EEL       PIC  X.
           05 IGG-EBL       PIC  X.
           05 IGG-EPA       PIC  X.
           05 IGG-GEN       PIC  X.
           05 IGG-TAI       PIC  X.
           05 IGG-ZCC       PIC  XXX.
           05 IGG-Z12       PIC  XXX.
           05 IGG-FIL       PIC  X(102).
      *-------------------------------------------------------------*
      * ==> ATTENTION <==                                           *
      * CETTE DESCRIPTION COBOL MIGGRA0C A UNE CORRESPONDANCE EN    *
      * PL1-MIGGRA0P                                                *
      * LES MODIFICATIONS DOIVENT ETRE RECIPROQUES.                 *
      *-------------------------------------------------------------*
       EJECT
      
