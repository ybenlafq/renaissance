      * @(#) MetaWare Technologies FrameWork 0.6.21
      *****************************************************************
      *   NOM : STATCODE     NOM DE MEMBRE DU MODULE
      *   TYPE: COPY         COB, COBDM, COBTP, COPY, SPROG
      *   LANG: COBOL        COBOL OU COBOL2
      *****************************************************************
       01  (FILE)--STATUS
                                      PIC X(2) EXTERNAL.
           88 (FILE)--OK
                                      VALUE "00" "04" "41" "43" "46".
           88 (FILE)--RETURN
                                      VALUE "00" "04" "41" "43" "46".
           88 (FILE)--FOUND
                                      VALUE "00".
           88 (FILE)--FND
                                      VALUE "00".
           88 (FILE)--ENDFILE
                                      VALUE "10" "23".
           88 (FILE)--ERROR
                                      VALUE "99".
           88 (FILE)--NOTFOUND
                                      VALUE "10" "23".
           88 (FILE)--NFND
                                      VALUE "10" "23".
           88 (FILE)--DUPKEY
                                      VALUE "22".
      * RPL GET statement
           88 (FILE)--SYNTAX
                                      VALUE "10" "23" "99".
           88 (FILE)--TILL
                                      VALUE "04".
      * For internal use
           88 (FILE)--ZERO
                                      VALUE "00".
           88 (FILE)--EOF
                                      VALUE "10".
           88 (FILE)--NOTFND
                                      VALUE "23".
      * Not used
      *    88 (FILE)--NOTOPEN
      *                               VALUE "47".
      * For LIB file
      *    88 (FILE)--ALREADY-CLOSE
      *                               VALUE "42".
           88 (FILE)--UNKNOWN
                                      VALUE "90".

