      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA TABLE RVEE9800                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVEE9800                         
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEE9800.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEE9800.                                                            
      *}                                                                        
           02  EE98-CPROGRAMME                                                  
               PIC X(0006).                                                     
           02  EE98-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  EE98-NLIEU                                                       
               PIC X(0003).                                                     
           02  EE98-ENTITE                                                      
               PIC X(0015).                                                     
           02  EE98-DTRAITEMENT                                                 
               PIC X(0008).                                                     
           02  EE98-DSYSTQUAI                                                   
               PIC S9(13) COMP-3.                                               
           02  EE98-DSYSTADM                                                    
               PIC S9(13) COMP-3.                                               
           02  EE98-ENTITE2                                                     
               PIC X(0007).                                                     
           02  EE98-WEDITE                                                      
               PIC X(0001).                                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVEE9800                                  
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEE9800-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEE9800-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-CPROGRAMME-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-CPROGRAMME-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-ENTITE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-ENTITE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-DTRAITEMENT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-DTRAITEMENT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-DSYSTQUAI-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-DSYSTQUAI-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-DSYSTADM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-DSYSTADM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-ENTITE2-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-ENTITE2-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  EE98-WEDITE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  EE98-WEDITE-F                                                    
               PIC S9(4) COMP-5.                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
