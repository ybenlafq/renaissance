      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                        00000860
                                                                                
      * CREATION DANS LA TMC02                                                  
      * MISE A JOURS POSSIBLE DANS TMC03                                        
       01  TSMC02-DATA.                                                         
           05 TSMC02-CGROUPE      PIC X(5).                                     
           05 TSMC02-LGROUPE      PIC X(20).                                    
           05 TSMC02-TGROUPE      PIC X(5).                                     
           05 TSMC02-NBR          PIC ZZZZ9.                                    
      * CREATION DANS LA TMC03                                                  
      * MISE A JOURS POSSIBLE DANS TMC04                                        
       01  TSMC03-DATA.                                                         
           05 TSMC03-MSEL         PIC X(1).                                     
           05 TSMC03-FACTIF       PIC X(1).                                     
           05 TSMC03-CFAM         PIC X(5).                                     
           05 TSMC03-LCHASSIS     PIC X(20).                                    
           05 TSMC03-CCHASSIS     PIC 9(07).                                    
           05 TSMC03-DCHASSIS     PIC X(10).                                    
           05 TSMC03-DCHASSIS-8   PIC X(8).                                     
           05 TSMC03-NBR          PIC ZZZZZ.                                    
           05 TSMC03-IND-TS       PIC 9(5).                                     
      * D = DELET I = INSERT U =UPDATE                                          
           05 TSMC03-ACTION       PIC X(1).                                     
                                                                                
                                                                        00000840
                                                                        00000840
