      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE QAUTO QUOTA AS PAR LIEU ET TYPE        *        
      *----------------------------------------------------------------*        
       01  RVQAUTO .                                                            
           05  QAUTO-CTABLEG2    PIC X(15).                                     
           05  QAUTO-CTABLEG2-REDEF REDEFINES QAUTO-CTABLEG2.                   
               10  QAUTO-SOCLIEU         PIC X(06).                             
               10  QAUTO-CAUTO           PIC X(05).                             
           05  QAUTO-WTABLEG     PIC X(80).                                     
           05  QAUTO-WTABLEG-REDEF  REDEFINES QAUTO-WTABLEG.                    
               10  QAUTO-WACTIF          PIC X(01).                             
               10  QAUTO-QUOTA           PIC X(04).                             
               10  QAUTO-QUOTA-N        REDEFINES QAUTO-QUOTA                   
                                         PIC 9(04).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVQAUTO-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QAUTO-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  QAUTO-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  QAUTO-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  QAUTO-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
