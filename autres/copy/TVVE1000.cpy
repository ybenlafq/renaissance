      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      **********************************************************                
      *   COPY DE LA VUE TVVE1000                                               
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA VUE TVVE1000                           
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  TVVE1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  TVVE1000.                                                            
      *}                                                                        
           02  VE10C-NSOCIETE                                                   
               PIC X(0003).                                                     
           02  VE10C-NLIEU                                                      
               PIC X(0003).                                                     
           02  VE10C-NVENTE                                                     
               PIC X(0007).                                                     
           02  VE10C-NORDRE                                                     
               PIC X(0005).                                                     
           02  VE10C-NCLIENT                                                    
               PIC X(0009).                                                     
           02  VE10C-DVENTE                                                     
               PIC X(0008).                                                     
           02  VE10C-DHVENTE                                                    
               PIC X(0002).                                                     
           02  VE10C-DMVENTE                                                    
               PIC X(0002).                                                     
           02  VE10C-PTTVENTE                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-PVERSE                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-PCOMPT                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-PLIVR                                                      
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-PDIFFERE                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-PRFACT                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-CREMVTE                                                    
               PIC X(0005).                                                     
           02  VE10C-PREMVTE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-LCOMVTE1                                                   
               PIC X(0030).                                                     
           02  VE10C-LCOMVTE2                                                   
               PIC X(0030).                                                     
           02  VE10C-LCOMVTE3                                                   
               PIC X(0030).                                                     
           02  VE10C-LCOMVTE4                                                   
               PIC X(0030).                                                     
           02  VE10C-DMODIFVTE                                                  
               PIC X(0008).                                                     
           02  VE10C-WFACTURE                                                   
               PIC X(0001).                                                     
           02  VE10C-WEXPORT                                                    
               PIC X(0001).                                                     
           02  VE10C-WDETAXEC                                                   
               PIC X(0001).                                                     
           02  VE10C-WDETAXEHC                                                  
               PIC X(0001).                                                     
           02  VE10C-CORGORED                                                   
               PIC X(0005).                                                     
           02  VE10C-CMODPAIMTI                                                 
               PIC X(0005).                                                     
           02  VE10C-LDESCRIPTIF1                                               
               PIC X(0030).                                                     
           02  VE10C-LDESCRIPTIF2                                               
               PIC X(0030).                                                     
           02  VE10C-DLIVRBL                                                    
               PIC X(0008).                                                     
           02  VE10C-NFOLIOBL                                                   
               PIC X(0003).                                                     
           02  VE10C-LAUTORM                                                    
               PIC X(0005).                                                     
           02  VE10C-NAUTORD                                                    
               PIC X(0005).                                                     
           02  VE10C-DSYST                                                      
               PIC S9(13) COMP-3.                                               
           02  VE10C-DSTAT                                                      
               PIC X(04).                                                       
           02  VE10C-DFACTURE                                                   
               PIC X(08).                                                       
           02  VE10C-CACID                                                      
               PIC X(08).                                                       
           02  VE10C-NSOCMODIF                                                  
               PIC X(03).                                                       
           02  VE10C-NLIEUMODIF                                                 
               PIC X(03).                                                       
           02  VE10C-NSOCP                                                      
               PIC X(03).                                                       
           02  VE10C-NLIEUP                                                     
               PIC X(03).                                                       
           02  VE10C-CDEV                                                       
               PIC X(03).                                                       
           02  VE10C-CFCRED                                                     
               PIC X(05).                                                       
           02  VE10C-NCREDI                                                     
               PIC X(14).                                                       
           02  VE10C-NSEQNQ                                                     
               PIC S9(5) COMP-3.                                                
           02  VE10C-TYPVTE                                                     
               PIC X(01).                                                       
           02  VE10C-VTEGPE                                                     
               PIC X(01).                                                       
           02  VE10C-CTRMRQ                                                     
               PIC X(08).                                                       
           02  VE10C-DATENC                                                     
               PIC X(08).                                                       
           02  VE10C-WDGRAD                                                     
               PIC X(01).                                                       
           02  VE10C-NAUTO                                                      
               PIC X(20).                                                       
           02  VE10C-NSEQENS                                                    
               PIC S9(5) COMP-3.                                                
           02  VE10C-NSOCO                                                      
               PIC X(03).                                                       
           02  VE10C-NLIEUO                                                     
               PIC X(03).                                                       
           02  VE10C-NVENTO                                                     
               PIC X(08).                                                       
           02  VE10C-XNSOCIETE                                                  
               PIC X(0003).                                                     
           02  VE10C-XNLIEU                                                     
               PIC X(0003).                                                     
           02  VE10C-XNVENTE                                                    
               PIC X(0007).                                                     
           02  VE10C-XNORDRE                                                    
               PIC X(0005).                                                     
           02  VE10C-XNCLIENT                                                   
               PIC X(0009).                                                     
           02  VE10C-XDVENTE                                                    
               PIC X(0008).                                                     
           02  VE10C-XDHVENTE                                                   
               PIC X(0002).                                                     
           02  VE10C-XDMVENTE                                                   
               PIC X(0002).                                                     
           02  VE10C-XPTTVENTE                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-XPVERSE                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-XPCOMPT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-XPLIVR                                                     
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-XPDIFFERE                                                  
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-XPRFACT                                                    
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-XCREMVTE                                                   
               PIC X(0005).                                                     
           02  VE10C-XPREMVTE                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  VE10C-XLCOMVTE1                                                  
               PIC X(0030).                                                     
           02  VE10C-XLCOMVTE2                                                  
               PIC X(0030).                                                     
           02  VE10C-XLCOMVTE3                                                  
               PIC X(0030).                                                     
           02  VE10C-XLCOMVTE4                                                  
               PIC X(0030).                                                     
           02  VE10C-XDMODIFVTE                                                 
               PIC X(0008).                                                     
           02  VE10C-XWFACTURE                                                  
               PIC X(0001).                                                     
           02  VE10C-XWEXPORT                                                   
               PIC X(0001).                                                     
           02  VE10C-XWDETAXEC                                                  
               PIC X(0001).                                                     
           02  VE10C-XWDETAXEHC                                                 
               PIC X(0001).                                                     
           02  VE10C-XCORGORED                                                  
               PIC X(0005).                                                     
           02  VE10C-XCMODPAIMTI                                                
               PIC X(0005).                                                     
           02  VE10C-XLDESCRIPTIF1                                              
               PIC X(0030).                                                     
           02  VE10C-XLDESCRIPTIF2                                              
               PIC X(0030).                                                     
           02  VE10C-XDLIVRBL                                                   
               PIC X(0008).                                                     
           02  VE10C-XNFOLIOBL                                                  
               PIC X(0003).                                                     
           02  VE10C-XLAUTORM                                                   
               PIC X(0005).                                                     
           02  VE10C-XNAUTORD                                                   
               PIC X(0005).                                                     
           02  VE10C-XDSYST                                                     
               PIC S9(13) COMP-3.                                               
           02  VE10C-XDSTAT                                                     
               PIC X(04).                                                       
           02  VE10C-XDFACTURE                                                  
               PIC X(08).                                                       
           02  VE10C-XCACID                                                     
               PIC X(08).                                                       
           02  VE10C-XNSOCMODIF                                                 
               PIC X(03).                                                       
           02  VE10C-XNLIEUMODIF                                                
               PIC X(03).                                                       
           02  VE10C-XNSOCP                                                     
               PIC X(03).                                                       
           02  VE10C-XNLIEUP                                                    
               PIC X(03).                                                       
           02  VE10C-XCDEV                                                      
               PIC X(03).                                                       
           02  VE10C-XCFCRED                                                    
               PIC X(05).                                                       
           02  VE10C-XNCREDI                                                    
               PIC X(14).                                                       
           02  VE10C-XNSEQNQ                                                    
               PIC S9(5) COMP-3.                                                
           02  VE10C-XTYPVTE                                                    
               PIC X(01).                                                       
           02  VE10C-XVTEGPE                                                    
               PIC X(01).                                                       
           02  VE10C-XCTRMRQ                                                    
               PIC X(08).                                                       
           02  VE10C-XDATENC                                                    
               PIC X(08).                                                       
           02  VE10C-XWDGRAD                                                    
               PIC X(01).                                                       
           02  VE10C-XNAUTO                                                     
               PIC X(20).                                                       
           02  VE10C-XNSEQENS                                                   
               PIC S9(5) COMP-3.                                                
           02  VE10C-XNSOCO                                                     
               PIC X(03).                                                       
           02  VE10C-XNLIEUO                                                    
               PIC X(03).                                                       
           02  VE10C-XNVENTO                                                    
               PIC X(08).                                                       
      *    *************************************************************        
      *                       IBMSNAP_COMMITSEQ                                 
           02 VE10C-IBMSNAP-COMMITSEQ  PIC X(10).                               
      *    *************************************************************        
      *                       IBMSNAP_INTENTSEQ                                 
           02 VE10C-IBMSNAP-INTENTSEQ  PIC X(10).                               
      *    *************************************************************        
      *                       IBMSNAP_OPERATION                                 
           02 VE10C-IBMSNAP-OPERATION  PIC X(1).                                
      *    *************************************************************        
      *                       IBMSNAP_LOGMARKER                                 
           02 VE10C-IBMSNAP-LOGMARKER  PIC X(26).                               
      *    *************************************************************        
      *                       DSYSTUPD                                          
           02 VE10C-DSYSTUPD       PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       CTYPEUPD                                          
           02 VE10C-CTYPUPD       PIC X(1).                                     
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA VUE TVVE1000                                    
      *---------------------------------------------------------                
      *                                                                         
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  TVVE1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  TVVE1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NSOCIETE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NSOCIETE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NLIEU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NLIEU-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NORDRE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NORDRE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NCLIENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NCLIENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DVENTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DVENTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DHVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DHVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DMVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DMVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-PTTVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-PTTVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-PVERSE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-PVERSE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-PCOMPT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-PCOMPT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-PLIVR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-PLIVR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-PDIFFERE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-PDIFFERE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-PRFACT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-PRFACT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-CREMVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-CREMVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-PREMVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-PREMVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-LCOMVTE1-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-LCOMVTE1-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-LCOMVTE2-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-LCOMVTE2-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-LCOMVTE3-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-LCOMVTE3-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-LCOMVTE4-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-LCOMVTE4-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DMODIFVTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DMODIFVTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-WFACTURE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-WFACTURE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-WEXPORT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-WEXPORT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-WDETAXEC-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-WDETAXEC-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-WDETAXEHC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-WDETAXEHC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-CORGORED-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-CORGORED-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-CMODPAIMTI-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-CMODPAIMTI-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-LDESCRIPTIF1-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-LDESCRIPTIF1-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-LDESCRIPTIF2-F                                             
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-LDESCRIPTIF2-F                                             
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DLIVRBL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DLIVRBL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NFOLIOBL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NFOLIOBL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-LAUTORM-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-LAUTORM-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NAUTORD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NAUTORD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DSYST-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DSYST-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DSTAT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DSTAT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DFACTURE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DFACTURE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-CACID-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-CACID-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NSOCMODIF-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NSOCMODIF-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NLIEUMODIF-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NLIEUMODIF-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NSOCP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NSOCP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NLIEUP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NLIEUP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-CDEV-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-CDEV-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-CFCRED-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-CFCRED-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NCREDI-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NCREDI-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NSEQNQ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NSEQNQ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-TYPVTE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-TYPVTE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-VTEGPE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-VTEGPE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-CTRMRQ-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-CTRMRQ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-DATENC-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-DATENC-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-WDGRAD-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-WDGRAD-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NAUTO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NAUTO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NSEQENS-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NSEQENS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NSOCO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NSOCO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NLIEUO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NLIEUO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-NVENTO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-NVENTO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNSOCIETE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNSOCIETE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNLIEU-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNLIEU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNORDRE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNORDRE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNCLIENT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNCLIENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDVENTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDVENTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDHVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDHVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDMVENTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDMVENTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XPTTVENTE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XPTTVENTE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XPVERSE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XPVERSE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XPCOMPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XPCOMPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XPLIVR-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XPLIVR-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XPDIFFERE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XPDIFFERE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XPRFACT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XPRFACT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XCREMVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XCREMVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XPREMVTE-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XPREMVTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XLCOMVTE1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XLCOMVTE1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XLCOMVTE2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XLCOMVTE2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XLCOMVTE3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XLCOMVTE3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XLCOMVTE4-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XLCOMVTE4-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDMODIFVTE-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDMODIFVTE-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XWFACTURE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XWFACTURE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XWEXPORT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XWEXPORT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XWDETAXEC-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XWDETAXEC-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XWDETAXEHC-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XWDETAXEHC-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XCORGORED-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XCORGORED-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XCMODPAIMTI-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XCMODPAIMTI-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XLDESCRIPTIF1-F                                            
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XLDESCRIPTIF1-F                                            
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XLDESCRIPTIF2-F                                            
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XLDESCRIPTIF2-F                                            
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDLIVRBL-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDLIVRBL-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNFOLIOBL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNFOLIOBL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XLAUTORM-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XLAUTORM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNAUTORD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNAUTORD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDSYST-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDSYST-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDSTAT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDSTAT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDFACTURE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDFACTURE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XCACID-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XCACID-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNSOCMODIF-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNSOCMODIF-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNLIEUMODIF-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNLIEUMODIF-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNSOCP-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNSOCP-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNLIEUP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNLIEUP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XCDEV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XCDEV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XCFCRED-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XCFCRED-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNCREDI-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNCREDI-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNSEQNQ-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNSEQNQ-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XTYPVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XTYPVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XVTEGPE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XVTEGPE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XCTRMRQ-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XCTRMRQ-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XDATENC-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XDATENC-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XWDGRAD-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XWDGRAD-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNAUTO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNAUTO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNSEQENS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNSEQENS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNSOCO-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNSOCO-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNLIEUO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNLIEUO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  VE10C-XNVENTO-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  VE10C-XNVENTO-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 VE10C-IBMSNAP-COMMITSEQ-F   PIC S9(4) COMP.                       
      *--                                                                       
           02 VE10C-IBMSNAP-COMMITSEQ-F   PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 VE10C-IBMSNAP-INTENTSEQ-F   PIC S9(4) COMP.                       
      *--                                                                       
           02 VE10C-IBMSNAP-INTENTSEQ-F   PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 VE10C-IBMSNAP-OPERATION-F   PIC S9(4) COMP.                       
      *--                                                                       
           02 VE10C-IBMSNAP-OPERATION-F   PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 VE10C-IBMSNAP-LOGMARKER-F   PIC S9(4) COMP.                       
      *--                                                                       
           02 VE10C-IBMSNAP-LOGMARKER-F   PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 VE10C-DSYSTUPD-F            PIC S9(4) COMP.                       
      *--                                                                       
           02 VE10C-DSYSTUPD-F            PIC S9(4) COMP-5.                     
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 VE10C-CTYPUPD-F            PIC S9(4) COMP.                        
      *--                                                                       
           02 VE10C-CTYPUPD-F            PIC S9(4) COMP-5.                      
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
