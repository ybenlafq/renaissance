      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION MOUVEMENTS STANDARD                    *
      * NOM FICHIER.: EMVSTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 600                                             *
      *****************************************************************
      *
       01  EMVSTD6.
      * TYPE ENREGISTREMENT : EMISSION IMAGE STANDARD
           05      EMVSTD6-TYP-ENREG      PIC  X(0006).
      * NUMERO DE MOUVEMENT
           05      EMVSTD6-NMVT           PIC  9(0009).
      * NATURE DE MOUVEMENT
           05      EMVSTD6-CNATMVT        PIC  9(0002).
      * CODE SOCIETE
           05      EMVSTD6-CSOCIETE       PIC  X(0005).
      * R�SERV�
           05      EMVSTD6-RESERVE        PIC  X(0005).
      * CODE ARTICLE
           05      EMVSTD6-CARTICLE       PIC  X(0018).
      * NUMERO DE LOT
           05      EMVSTD6-NLOT           PIC  X(0015).
      * ETAT DE BLOCAGE INITIAL
           05      EMVSTD6-EBLOCINIT      PIC  9(0003).
      * ETAT DE BLOCAGE FINAL
           05      EMVSTD6-EBLOCFINAL     PIC  9(0003).
      * QUANTITE
           05      EMVSTD6-QTE            PIC  X(0009).
      * CODE FOURNISSEUR (NUMERO ENTITE COMMANDE)
           05      EMVSTD6-NENTCDE        PIC  X(0015).
      * DATE DE MOUVEMENT
           05      EMVSTD6-DMVT           PIC  X(0008).
      * HEURE DE MOUVEMENT
           05      EMVSTD6-HMVT           PIC  X(0006).
      * NUMERO DE DOSSIER ARRIVAGE
           05      EMVSTD6-NDOS-ARR       PIC  X(0015).
      * NUMERO INVENTAIRE
           05      EMVSTD6-NINVENTAIRE    PIC  X(0015).
      * NUMERO DE DOSSIER RECEPTION
           05      EMVSTD6-NDOS-REC       PIC  X(0015).
      * NUMERO DE COMMANDE N3
           05      EMVSTD6-NCDE-N3        PIC  X(0015).
      * NUMERO DE LIGNE DE RECEPTION
           05      EMVSTD6-NLIGNE-REC     PIC  9(0005).
      * NUMERO DE SOUS-LIGNE DE RECEPTION
           05      EMVSTD6-NSSLIGNE-REC   PIC  9(0005).
      * COMMENTAIRE
           05      EMVSTD6-COMMENTAIRE    PIC  X(0080).
      * CARACTERISTIQUE SUPPLEMENTAIRE
           05      EMVSTD6-CARACT-SUPPL   PIC  X(0255).
      * CARACTERISTIQUE 1
           05      EMVSTD6-CARACT1        PIC  X(0020).
      * CARACTERISTIQUE 2
           05      EMVSTD6-CARACT2        PIC  X(0020).
      * CARACTERISTIQUE 3
           05      EMVSTD6-CARACT3        PIC  X(0020).
      * FILLER 30
           05      EMVSTD6-FILLER         PIC  X(0030).
      * FIN DE L'ENREGISTREMENT
           05      EMVSTD6-FIN            PIC  X(0001).
      
