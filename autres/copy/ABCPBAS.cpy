      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------------------           
      *                       ABWCPBAS.CPY                                      
      *    POUR ABBAS VOIR AUSSI :            ABWS5BAS.CPY                      
      *    POUR ABBAS VOIR AUSSI :            ABCPBAS.CPY                       
      *    POUR ABBAS VOIR AUSSI :            ABCPCOMA.CPY                      
      *    POUR ABBAS VOIR AUSSI :            ABIOSBAS.COBOL ET SES 01..        
      *                                                                         
      *    ************   COPY  DU  FICHIER  ABBAS  ****************            
      *--------------------------------------------------------------           
      *  05 AVRIL 1998:                                  REF: A2000             
      *        R�VISION 2000 : NOUVELLE LONGUEUR ABBAS = 1500 (+300)            
      *                  TOUS LES CHAMPS ANNEE SONT A PRESENT PIC 9(4).         
      *                                                    OU PIC X(4).         
      *                  300 OCTETS ETANT DISPONIBLES :                         
      *            01 BASPARA.....CROIT DE 27*AA   SOIT:  FILLER (246).         
      *            01 BASRECORD...CROIT DE 11*AA PLUS UN FILLER DE 22           
      *  DE RESYNCHRO AVEC LA AALTKEY IL RESTE DONC UN FILLER (245).            
      *--------------------------------------------------------------           
      *                                                                         
       01  BASPARA.                                                             
           02  BASPARAKEY.                                                      
               03  SOCPARA                     PIC XX.                          
               03  NATENRO                     PIC X.                           
               03  NOBLANC                     PIC X(12).                       
           02  BASPARAENR.                                                      
               03  DATJOU.                                                      
AN2000             05  DATJOU-AA               PIC 9(4).                        
                   05  DATJOU-MM               PIC 9(2).                        
                   05  DATJOU-JJ               PIC 9(2).                        
             03   RAISOC                       PIC X(20).                       
             03   DATCLO.                                                       
AN2000          05   DATCLO-AA                 PIC 9(4).                        
                05   DATCLO-MM                 PIC 9(2).                        
                05   DATCLO-JJ                 PIC 9(2).                        
             03   DUREX                        PIC 99.                          
             03   INDCUM                       PIC 9.                           
             03   NEUFBAT                      PIC 999V99.                      
             03   NEUFAUT                      PIC 999V99.                      
             03   PARITE                       PIC 9(5)V9(5).                   
             03   TAUXACT                      PIC 99V99.                       
             03   CALSIM                       PIC X.                           
             03   CALPAM                       PIC X(1).                        
310894       03   CALSIM-FIC                   PIC X(1).                        
310894       03   FILLER                       PIC X(1).                        
             03   TXTVA                        PIC 99V99.                       
             03   CALCTP                       PIC X.                           
      *---- ZONE SPECIFIQUE ALCATEL                                             
150591       03   REAC                         PIC X.                           
             03   FILLER                       PIC X(70).                       
             03   DATPER1.                                                      
AN2000            10  DATPER1-AA               PIC 9(4).                        
                  10  DATPER1-MM               PIC 9(2).                        
                  10  DATPER1-JJ               PIC 9(2).                        
             03  SIMEX1                        PIC X.                           
             03  DATCLO2.                                                       
AN2000            10  DATCLO2-AA               PIC 9(4).                        
                  10  DATCLO2-MM               PIC 9(2).                        
                  10  DATCLO2-JJ               PIC 9(2).                        
             03  DUREE2                        PIC 9(2).                        
             03  DATPER2.                                                       
AN2000             10  DATPER2-AA              PIC 9(4).                        
                   10  DATPER2-MM              PIC 9(2).                        
                   10  DATPER2-JJ              PIC 9(2).                        
               03  SIMEX2                      PIC X.                           
               03  DATCLO3.                                                     
AN2000             10  DATCLO3-AA              PIC 9(4).                        
                   10  DATCLO3-MM              PIC 9(2).                        
                   10  DATCLO3-JJ              PIC 9(2).                        
               03  DUREE3                      PIC 9(2).                        
               03  DATPER3.                                                     
AN2000             10  DATPER3-AA              PIC 9(4).                        
                   10  DATPER3-MM              PIC 9(2).                        
                   10  DATPER3-JJ              PIC 9(2).                        
               03  SIMEX3                      PIC X.                           
               03  TXTVAR                      PIC 999V99.                      
               03  PMVR                        PIC X.                           
               03  EDTVR                       PIC X.                           
               03  CAL                         PIC X.                           
               03  RECAL                       PIC X.                           
               03  CODDIF                      PIC X.                           
               03  TXDIF                       PIC 999V99.                      
               03  DMDCLO1                     PIC X.                           
               03  DMDCLO2                     PIC X.                           
               03  DMDCLO3                     PIC X.                           
               03  HISTOF                      PIC X.                           
               03  HISTOR                      PIC X.                           
               03  HISTOT                      PIC X.                           
               03  HISTOA                      PIC X.                           
               03  ADR-ZU1F                    PIC 9(4).                        
               03  LGN-ZU1F                    PIC 99.                          
               03  ADR-ZU2F                    PIC 9(4).                        
               03  LGN-ZU2F                    PIC 99.                          
               03  ADR-ZU3F                    PIC 9(4).                        
               03  LGN-ZU3F                    PIC 99.                          
               03  ADR-ZU4F                    PIC 9(4).                        
               03  LGN-ZU4F                    PIC 99.                          
               03  ADR-ZU5F                    PIC 9(4).                        
               03  LGN-ZU5F                    PIC 99.                          
               03  ADR-ZU1R                    PIC 9(4).                        
               03  LGN-ZU1R                    PIC 99.                          
               03  ADR-ZU2R                    PIC 9(4).                        
               03  LGN-ZU2R                    PIC 99.                          
               03  ADR-ZU3R                    PIC 9(4).                        
               03  LGN-ZU3R                    PIC 99.                          
               03  ADR-ZU4R                    PIC 9(4).                        
               03  LGN-ZU4R                    PIC 99.                          
               03  ADR-ZU5R                    PIC 9(4).                        
               03  LGN-ZU5R                    PIC 99.                          
               03  ADR-ZU1T                    PIC 9(4).                        
               03  LGN-ZU1T                    PIC 99.                          
               03  ADR-ZU2T                    PIC 9(4).                        
               03  LGN-ZU2T                    PIC 99.                          
               03  ADR-ZU3T                    PIC 9(4).                        
               03  LGN-ZU3T                    PIC 99.                          
               03  ADR-ZU4T                    PIC 9(4).                        
               03  LGN-ZU4T                    PIC 99.                          
               03  ADR-ZU5T                    PIC 9(4).                        
               03  LGN-ZU5T                    PIC 99.                          
               03  PARASIMA                    PIC X.                           
               03  SDATCLO1.                                                    
AN2000             10  SDATCLO1-AA             PIC 9(4).                        
                   10  SDATCLO1-MM             PIC 9(2).                        
                   10  SDATCLO1-JJ             PIC 9(2).                        
               03  SDATPER1.                                                    
AN2000             10  SDATPER1-AA             PIC 9(4).                        
                   10  SDATPER1-MM             PIC 9(2).                        
                   10  SDATPER1-JJ             PIC 9(2).                        
               03  SDATCLO2.                                                    
AN2000             10  SDATCLO2-AA             PIC 9(4).                        
                   10  SDATCLO2-MM             PIC 9(2).                        
                   10  SDATCLO2-JJ             PIC 9(2).                        
               03  SDATPER2.                                                    
AN2000             10  SDATPER2-AA             PIC 9(4).                        
                   10  SDATPER2-MM             PIC 9(2).                        
                   10  SDATPER2-JJ             PIC 9(2).                        
               03  SDATCLO3.                                                    
AN2000             10  SDATCLO3-AA             PIC 9(4).                        
                   10  SDATCLO3-MM             PIC 9(2).                        
                   10  SDATCLO3-JJ             PIC 9(2).                        
               03  SDATPER3.                                                    
AN2000             10  SDATPER3-AA             PIC 9(4).                        
                   10  SDATPER3-MM             PIC 9(2).                        
                   10  SDATPER3-JJ             PIC 9(2).                        
               03  PASSSOC                     PIC X(6).                        
               03  CALLS02                     PIC X(01).                       
               03  MOVEMINI                    PIC X(01).                       
AN200-         03  DATESY                      PIC X.                           
               03  DATDEB1.                                                     
AN2000             10  DATDEB1-AA              PIC 9(4).                        
                   10  DATDEB1-MM              PIC 9(2).                        
                   10  DATDEB1-JJ              PIC 9(2).                        
               03  DATDEB2.                                                     
AN2000             10  DATDEB2-AA              PIC 9(4).                        
                   10  DATDEB2-MM              PIC 9(2).                        
                   10  DATDEB2-JJ              PIC 9(2).                        
               03  DATDEB3.                                                     
AN2000             10  DATDEB3-AA              PIC 9(4).                        
                   10  DATDEB3-MM              PIC 9(2).                        
                   10  DATDEB3-JJ              PIC 9(2).                        
               03  ZONESYS                     PIC X(200).                      
               03  DATRANS-MICRO               PIC 9(08).                       
               03  DATIN-MICRO                 PIC 9(08).                       
               03  DATDCAL-MICRO               PIC 9(08).                       
               03  DUREXP1-MICRO               PIC 9(02).                       
               03  PMVR-MICRO                  PIC X.                           
               03  ECODER-MICRO                PIC X.                           
               03  DATDER-MICRO                PIC 9(08).                       
               03  CODEQ1-MICRO                PIC X.                           
               03  CODEQ2-MICRO                PIC X.                           
               03  CODEQ3-MICRO                PIC X.                           
               03  FILLER                      PIC X(450).                      
           02  ALTKEYS                         PIC X(26).                       
           02  BASPARA-LG.                                                      
               03  CLEADR1                     PIC 9(4).                        
               03  CLELGN1                     PIC 99.                          
               03  CLEADR2                     PIC 9(4).                        
               03  CLELGN2                     PIC 99.                          
               03  CLEADR3                     PIC 9(4).                        
               03  CLELGN3                     PIC 99.                          
               03  CLEADR4                     PIC 9(4).                        
               03  CLELGN4                     PIC 99.                          
               03  NUMAUTO                     PIC X(12).                       
               03  PARTICUL                    PIC X(12).                       
               03  COTRE                       PIC X(1).                        
               03  NUMAUTODEP                  PIC 9(2).                        
               03  NUMAUTOLONG                 PIC 9(2).                        
               03  NUMAUTOPAS                  PIC 9(2).                        
               03  NUMAUTOTOP                  PIC X(1).                        
               03  INDCED                      PIC X.                           
               03  INVIOL-MONTACQ              PIC X.                           
               03  CTVAF                       PIC X.                           
               03  CTVAR                       PIC X.                           
               03  TEMPS-LIMITE                PIC XXX.                         
               03  TOP-TRAIT-LOYER             PIC X.                           
               03  MOIS-TRAIT-LOYER            PIC XX.                          
090492         03  PURG-DAT.                                                    
AN2000             10  PURG-DAT-AA             PIC 9(4).                        
!                  10  PURG-DAT-MM             PIC 9(2).                        
090492             10  PURG-DAT-JJ             PIC 9(2).                        
181096         03  DATPER1S.                                                    
AN2000             10  DATPER1S-AA             PIC X(4).                        
  !                10  DATPER1S-MM             PIC X(2).                        
  !                10  DATPER1S-JJ             PIC X(2).                        
181096         03  DATPER2S.                                                    
AN2000             10  DATPER2S-AA             PIC X(4).                        
  !                10  DATPER2S-MM             PIC X(2).                        
  !                10  DATPER2S-JJ             PIC X(2).                        
181096         03  DATPER3S.                                                    
AN2000             10  DATPER3S-AA             PIC X(4).                        
  !                10  DATPER3S-MM             PIC X(2).                        
  !                10  DATPER3S-JJ             PIC X(2).                        
181096         03  DATEXT.                                                      
AN2000             10  DATEXT-AA               PIC X(4).                        
  !                10  DATEXT-MM               PIC X(2).                        
  !                10  DATEXT-JJ               PIC X(2).                        
970123         03  DEGR96-R                    PIC X.                           
970123         03  DEGR96-T                    PIC X.                           
221100         03  DATE-CPTLEG                 PIC X(08).                       
221100*        03  FILLER                      PIC X(12).                       
271100*        03  FILLER                      PIC X(04).                       
271100         03  DEGR01-R                    PIC X.                           
271100         03  DEGR01-T                    PIC X.                           
271100         03  FILLER                      PIC X(02).                       
181096*        03  FILLER                      PIC X(38).                       
               03  TP-DATE.                                                     
                   10  TP-DATE-JJ              PIC 99.                          
                   10  TP-DATE-01              PIC X.                           
                   10  TP-DATE-MM              PIC 99.                          
                   10  TP-DATE-02              PIC X.                           
AN2000             10  TP-DATE-AA              PIC 9999.                        
               03  TP-DATEC.                                                    
                   10  TP-DATEC-JJ             PIC 99.                          
                   10  TP-DATEC-01             PIC X.                           
                   10  TP-DATEC-MM             PIC 99.                          
                   10  TP-DATEC-02             PIC X.                           
AN2000             10  TP-DATEC-AA             PIC 9999.                        
EURO           03  DEV-GEST                    PIC X(03).                       
EURO           03  DEV-CONSO                   PIC X(03).                       
EURO           03  FILLER                      PIC X(240).                      
      *                                                                         
       01  BASRECORD REDEFINES BASPARA.                                         
           02  BASKEY.                                                          
               03  SOC                         PIC XX.                          
               03  NATENR                      PIC X.                           
               03  NUMIMMO                     PIC X(12).                       
           02  BASENR1.                                                         
               03  ETAB                        PIC XX.                          
               03  SITE                        PIC X(4).                        
               03  QTT                         PIC 9(8).                        
               03  LIB1                        PIC X(30).                       
               03  LIB2                        PIC X(30).                       
               03  TYPINFO                     PIC X.                           
               03  NATACH                      PIC X.                           
               03  STAB                        PIC X.                           
               03  TYPIM                       PIC X.                           
               03  TYPINV                      PIC X.                           
               03  FAM                         PIC XXX.                         
               03  NATCOM                      PIC X(10).                       
               03  SECTIONA                    PIC X(10).                       
               03  NUMPHY                      PIC X(10).                       
               03  ENSEMB                      PIC X(12).                       
               03  NUMPLAN                     PIC X(8).                        
               03  CATASS                      PIC XXX.                         
               03  PERCOM.                                                      
AN2000             05   PERCOM-AA              PIC 9999.                        
                   05   PERCOM-MM              PIC 99.                          
               03  INFOUR.                                                      
                   05   LIBFO                  PIC X(20).                       
                   05   COFO                   PIC X(06).                       
                   05   APE                    PIC X(06).                       
               03  INFOURBIS   REDEFINES  INFOUR.                               
                   05   LIBFOU                 PIC X(17).                       
                   05   CODFOU                 PIC X(15).                       
               03  TAXE                        PIC 9(8)V99.                     
               03  MAMF                        PIC X.                           
               03  DURF                        PIC 99V99.                       
               03  DURF-I REDEFINES DURF       PIC 9(04).                       
               03  DURF-X REDEFINES DURF       PIC X(04).                       
               03  CTRAIT1                     PIC X.                           
               03  CTRAIT2                     PIC X.                           
               03  CTRAIT3                     PIC X.                           
               03  CTRAIT4                     PIC X.                           
               03  CTRAIT5                     PIC X.                           
               03  MONTACQ                     PIC 9(8)V99.                     
               03  MONTACQ-I REDEFINES MONTACQ PIC 9(10).                       
               03  MONTACQ-X REDEFINES MONTACQ PIC X(10).                       
               03  SIGNE                       PIC X.                           
               03  MONTREV                     PIC 9(8)V99.                     
               03  COEFREV                     PIC 9(3)V9(3).                   
               03  COEFREV-I REDEFINES COEFREV PIC 9(6).                        
               03  COEFREV-X REDEFINES COEFREV PIC X(6).                        
               03  MONTCO                      PIC 9(8)V99.                     
               03  COEFREAC                    PIC 999V9(6).                    
               03  COEFREAC-I REDEFINES COEFREAC PIC 9(9).                      
               03  COEFREAC-X REDEFINES COEFREAC PIC X(9).                      
               03  MONTEXP                     PIC 9(8)V99.                     
               03  COEFAS                      PIC 999V9(6).                    
               03  COEFAS-I REDEFINES COEFAS   PIC 9(9).                        
               03  COEFAS-X REDEFINES COEFAS   PIC X(9).                        
               03  MONTSOR                     PIC 9(8)V99.                     
               03  MONTSOR-X REDEFINES MONTSOR PIC X(10).                       
               03  DATACH.                                                      
AN2000             05   DATACH-AA              PIC 9999.                        
                   05   DATACH-MM              PIC 99.                          
                   05   DATACH-JJ              PIC 99.                          
               03  DATSERV.                                                     
AN2000             05   DATSERV-AA             PIC 9999.                        
                   05   DATSERV-MM             PIC 99.                          
                   05   DATSERV-JJ             PIC 99.                          
               03  DATAMOR.                                                     
AN2000             05   DATAMOR-AA             PIC 9999.                        
                   05   DATAMOR-MM             PIC 99.                          
                   05   DATAMOR-JJ             PIC 99.                          
               03  DATLIM.                                                      
                   05   DATLIM-AA.                                              
                        10   DATLIM-AS         PIC 99.                          
                        10   DATLIM-AD         PIC 99.                          
                   05   DATLIM-MM              PIC 99.                          
               03  DATEXP.                                                      
AN2000             05   DATEXP-AA              PIC 9999.                        
                   05   DATEXP-MM              PIC 99.                          
               03  DATCO.                                                       
AN2000             05   DATCO-AA               PIC 9999.                        
                   05   DATCO-MM               PIC 99.                          
                   05   DATCO-JJ               PIC 99.                          
               03  DATSOR.                                                      
AN2000             05   DATSOR-AA              PIC 9999.                        
                   05   DATSOR-MM              PIC 99.                          
                   05   DATSOR-JJ              PIC 99.                          
               03   ECREV                      PIC 9(8)V99.                     
               03   ECREV-X REDEFINES ECREV    PIC X(10).                       
               03   VR76R                      PIC 9(8)V99.                     
               03   VR76R-X REDEFINES VR76R    PIC X(10).                       
               03   CUMAVM1                    PIC 9(8)V99.                     
               03   CUMEXM1                    PIC 9(8)V99.                     
               03   DOTE                       PIC 9(8)V99.                     
               03   DOTEX                      PIC 9(8)V99.                     
               03   CUMMIN                     PIC 9(8)V99.                     
               03   DOTMIN                     PIC 9(8)V99.                     
               03   CUMARM1                    PIC 9(8)V99.                     
               03   DOTRE                      PIC 9(8)V99.                     
               03   CUMECOAV                   PIC 9(8)V99.                     
               03   PM                         PIC X.                           
               03   MVALUC                     PIC 9(8)V99.                     
               03   MVALUL                     PIC 9(8)V99.                     
               03   PRUO                       PIC 9(8).                        
               03   RUO                        PIC 9(8).                        
               03   ENTFIX                     PIC 9(8)V99.                     
               03   ENTVAR                     PIC 9(8)V99.                     
               03   ENTCUM                     PIC 9(8)V99.                     
               03   BASTX                      PIC 9(8)V99.                     
               03   MAMTEC                     PIC X.                           
               03   CORT                       PIC 9(3)V9(6).                   
               03   CORT-I REDEFINES CORT      PIC 9(9).                        
               03   CORT-X REDEFINES CORT      PIC X(9).                        
               03   CUMTECM1                   PIC 9(8)V99.                     
               03   DOTECE                     PIC 9(8)V99.                     
               03   CUMECOEX                   PIC 9(8)V99.                     
               03   VALUS                      PIC 9(8)V99.                     
               03   VALUS-X REDEFINES VALUS    PIC X(10).                       
               03   Z1                         PIC X(10).                       
               03   Z1-9B REDEFINES Z1         PIC 9(08)V99.                    
               03   Z2                         PIC X(10).                       
               03   Z2-9B REDEFINES Z2         PIC 9(08)V99.                    
               03   Z3                         PIC X(10).                       
               03   Z3-9B REDEFINES Z3         PIC 9(08)V99.                    
               03   Z4                         PIC X(10).                       
               03   Z4-9B REDEFINES Z4         PIC 9(08)V99.                    
               03   Z5                         PIC X(10).                       
               03   Z5-9B REDEFINES Z5         PIC 9(08)V99.                    
               03   DECAS                      PIC 9(3)V9(6).                   
               03   DECAS-I REDEFINES DECAS    PIC 9(9).                        
               03   DECAS-X REDEFINES DECAS    PIC X(9).                        
               03   VALAS                      PIC 9(8)V99.                     
               03   DEVIS                      PIC X(5).                        
               03   PAR                        PIC 9(5)V9(5).                   
               03   DURCO                      PIC 99V99.                       
               03   DURCO-I REDEFINES DURCO    PIC 9(4).                        
               03   DURCO-X REDEFINES DURCO    PIC X(4).                        
               03   CUMCOM1                    PIC 9(8)V99.                     
               03   DOTCOE                     PIC 9(8)V99.                     
               03   DOTNORMECO                 PIC 9(8)V99.                     
               03   F1                         PIC 9.                           
               03   F1-X REDEFINES F1          PIC X.                           
               03   F2                         PIC 9.                           
               03   F2-X REDEFINES F2          PIC X.                           
               03   F3                         PIC 9.                           
               03   F3-X REDEFINES F3          PIC X.                           
               03  DATDERM.                                                     
AN2000             05   DATDERM-AA             PIC 9999.                        
                   05   DATDERM-MM             PIC 99.                          
                   05   DATDERM-JJ             PIC 99.                          
               03  DATDERC.                                                     
AN2000             05   DATDERC-AA             PIC 9999.                        
                   05   DATDERC-MM             PIC 99.                          
                   05   DATDERC-JJ             PIC 99.                          
               03   NUMORD                     PIC 9(4).                        
               03   NUMORD-X REDEFINES NUMORD  PIC X(4).                        
               03   CAMEX83M1                  PIC 9(8)V99.                     
               03   AMEX83                     PIC 9(8)V99.                     
               03   DOTCOMPECO                 PIC 9(8)V99.                     
               03   DURTEC                     PIC 99V99.                       
               03   DURTEC-I REDEFINES DURTEC  PIC 9(4).                        
               03   DURTEC-X REDEFINES DURTEC  PIC X(4).                        
               03   TYPENT                     PIC X.                           
               03   TYPTAX                     PIC X.                           
               03   REVTVA                     PIC 9(8)V99.                     
               03   MINI                       PIC 9(8)V99.                     
               03   RESPON                     PIC X(4).                        
               03   NUPIECE                    PIC X(6).                        
               03   REINTEG                    PIC 9(8)V99.                     
               03   DOTF1C                     PIC 9(8)V99.                     
               03   DOTF1C-X REDEFINES DOTF1C  PIC X(10).                       
               03   DOTF1P                     PIC 9(8)V99.                     
               03   DOTF1P-X REDEFINES DOTF1P  PIC X(10).                       
               03   DOTF2C                     PIC 9(8)V99.                     
               03   DOTF2C-X REDEFINES DOTF2C  PIC X(10).                       
               03   DOTF2P                     PIC 9(8)V99.                     
               03   DOTF2P-X REDEFINES DOTF2P  PIC X(10).                       
               03   DOTR1C                     PIC 9(8)V99.                     
               03   DOTR1C-X REDEFINES DOTR1C  PIC X(10).                       
               03   DOTR1P                     PIC 9(8)V99.                     
               03   DOTR1P-X REDEFINES DOTR1P  PIC X(10).                       
               03   DOTE1C                     PIC 9(8)V99.                     
               03   DOTE1C-X REDEFINES DOTE1C  PIC X(10).                       
               03   DOTE1P                     PIC 9(8)V99.                     
               03   DOTE1P-X REDEFINES DOTE1P  PIC X(10).                       
               03   MAMCO                      PIC X.                           
               03   DIFCUM                     PIC 9(8)V99.                     
               03   DIFEXE                     PIC 9(8)V99.                     
               03   DIFREC                     PIC 9(8)V99.                     
               03   DIFERAB                    PIC 9(8)V99.                     
               03   DIFCUM2                    PIC 9(8)V99.                     
               03   DOTMIN2                    PIC 9(8)V99.                     
               03   DEROPR                     PIC 9(8)V99.                     
               03   DERORE                     PIC 9(8)V99.                     
               03   CDEROPR                    PIC 9(8)V99.                     
               03   CDERORE                    PIC 9(8)V99.                     
               03   DIFCUMR                    PIC 9(8)V99.                     
               03   Z6                         PIC X(10).                       
               03   Z6-9B REDEFINES Z6         PIC 9(08)V99.                    
               03   Z7Z8.                                                       
                   05   Z7                     PIC X(10).                       
                   05   Z7-9B REDEFINES Z7     PIC 9(08)V99.                    
                   05   Z8                     PIC X(10).                       
                   05   Z8-9B REDEFINES Z8     PIC 9(08)V99.                    
               03   BASECO                     PIC 9(8)V99.                     
               03   COACHA                     PIC X(6).                        
               03   LIBACHA                    PIC X(20).                       
               03   CUMAEM1                    PIC 9(8)V99.                     
               03   DOTAE                      PIC 9(8)V99.                     
AN2000     02  FILLER                          PIC X(22).                       
           02  ALTKEY.                                                          
               03   ALTKEY0                    PIC X(02).                       
               03   ALTKEY1                    PIC X(12).                       
               03   ALTKEY2                    PIC X(12).                       
           02  DOTF3C                          PIC 9(8)V99.                     
           02  DOTF3C-X REDEFINES DOTF3C       PIC X(10).                       
           02  DOTF3P                          PIC 9(8)V99.                     
           02  DOTF3P-X REDEFINES DOTF3P       PIC X(10).                       
           02  DOTF4C                          PIC 9(8)V99.                     
           02  DOTF4C-X REDEFINES DOTF4C       PIC X(10).                       
           02  DOTF4P                          PIC 9(8)V99.                     
           02  DOTF4P-X REDEFINES DOTF4P       PIC X(10).                       
           02  FREVTVA                         PIC X.                           
           02  FPMV                            PIC X.                           
           02  PMF                             PIC X.                           
           02  MVALUCF                         PIC 9(8)V99.                     
           02  MVALULF                         PIC 9(8)V99.                     
           02  FPMVF                           PIC X.                           
           02  DEDUCCOMP                       PIC 9(8)V99.                     
           02  DEDUCCOMP-X REDEFINES DEDUCCOMP PIC X(10).                       
           02  REGR                            PIC X(04).                       
           02  SOC-EMET                        PIC X(02).                       
           02  SOC-DEST                        PIC X(02).                       
           02  SOC-TRANS.                                                       
AN2000         05  SOC-TRANS-AA                PIC 9999.                        
               05  SOC-TRANS-MM                PIC 99.                          
               05  SOC-TRANS-JJ                PIC 99.                          
           02  MONTORIG                        PIC 9(8)V99.                     
           02  SOLDE-CT   REDEFINES MONTORIG   PIC 9(8)V99.                     
050193     02  ECART-TP   REDEFINES MONTORIG   PIC S9(8)V99.                    
270494     02  MONSOR-FIC REDEFINES MONTORIG   PIC 9(8)V99.                     
           02  DATORIG.                                                         
AN2000         05   DATORIG-AA                 PIC 9999.                        
               05   DATORIG-MM                 PIC 99.                          
               05   DATORIG-JJ                 PIC 99.                          
AN2000     02  DATSOR-FIC REDEFINES DATORIG    PIC X(8).                        
           02  CTXTVA                          PIC X(01).                       
           02  CTXTVAR                         PIC X(01).                       
           02  CTXTVAV                         PIC X(01).                       
           02  MLOYER                          PIC 9(8)V99.                     
           02  SOLDE-LT REDEFINES MLOYER       PIC 9(8)V99.                     
           02  TLOYER                          PIC 9(3).                        
270694     02  NATSOR-FIC                      PIC X.                           
           02  FILLER                          PIC X(04).                       
           02  TOPBATCH                        PIC X(01).                       
           02  BASERET-X.                                                       
               05  BASERET                     PIC 9(8)V99.                     
           02  CALBRET                         PIC X(01).                       
      *----------------------                                                   
           02  ZUA20-1                         PIC X(20).                       
           02  ZUA20-2                         PIC X(20).                       
           02  ZUA10-1                         PIC X(10).                       
           02  ZUA10-2                         PIC X(10).                       
           02  ZUA05-1                         PIC X(5).                        
           02  ZUA05-2                         PIC X(5).                        
           02  ZUA05-3                         PIC X(5).                        
           02  ZUA01-1                         PIC X.                           
           02  ZUA01-2                         PIC X.                           
           02  ZUA01-3                         PIC X.                           
           02  ZUDT01.                                                          
               05   ZUDT01-AA                  PIC 9999.                        
               05   ZUDT01-MM                  PIC 99.                          
               05   ZUDT01-JJ                  PIC 99.                          
           02  ZUDT02.                                                          
               05   ZUDT02-AA                  PIC 9999.                        
               05   ZUDT02-MM                  PIC 99.                          
               05   ZUDT02-JJ                  PIC 99.                          
           02  ZUDT03.                                                          
               05   ZUDT03-AA                  PIC 9999.                        
               05   ZUDT03-MM                  PIC 99.                          
               05   ZUDT03-JJ                  PIC 99.                          
           02  ZUMT01-X.                                                        
               05  ZUMT01                      PIC 9(8)V99.                     
           02  ZUMT02-X.                                                        
               05  ZUMT02                      PIC 9(8)V99.                     
           02  ZUMT03-X.                                                        
               05  ZUMT03                      PIC 9(8)V99.                     
           02  DATCRE.                                                          
               05   DATCRE-AA                  PIC 9999.                        
               05   DATCRE-MM                  PIC 99.                          
               05   DATCRE-JJ                  PIC 99.                          
AN2000     02  FILLER                          PIC X(105).                      
      *                                                                         
      ***************   WSS  ABELBAS  TERMINEE     ***************              
                                                                                
