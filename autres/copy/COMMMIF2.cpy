      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  Z-COMMAREA-MIF2.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MIF2-CDRET      PIC S9(4)     COMP.                         
      *--                                                                       
           05  COMM-MIF2-CDRET      PIC S9(4) COMP-5.                           
      *}                                                                        
           05  COMM-MIF2-DATE       PIC X(8).                                   
           05  COMM-MIF2-NSOCCOMPT  PIC X(3).                                   
           05  COMM-MIF2-NETAB      PIC X(3).                                   
           05  COMM-MIF2-NSOCIETE   PIC X(3).                                   
           05  COMM-MIF2-NLIEU      PIC X(3).                                   
           05  COMM-MIF2-NOECS      PIC X(5).                                   
           05  COMM-MIF2-CRITERE1   PIC X(5).                                   
           05  COMM-MIF2-CRITERE2   PIC X(5).                                   
           05  COMM-MIF2-CRITERE3   PIC X(5).                                   
           05  COMM-MIF2-COMPTE     PIC X(6).                                   
           05  COMM-MIF2-SECTION    PIC X(6).                                   
           05  COMM-MIF2-RUBRIQUE   PIC X(6).                                   
                                                                                
