      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *    Copie utilis�e pour MGV51, MGV52, MGV53                      00580008
      *                                                                 00590008
      *                                                                         
      *    Pour envoyer la dupli GV11 vers les Plateformes NMD                  
      *                                                                 01290008
      *                                                                 01310008
      *                                                                 01310008
      *                                                                 01310008
      *    Stockage des diff�rentes plateformes de livraisons/stockage  01310008
       AJOUT-PLATF SECTION.                                                     
PV0307     IF W-LIEU-STOCK                                                      
PV0307     OR W-LIEU-LIVR                                                       
              IF W-NSM-NSOC  > ' ' AND W-NSM-NLIEU > ' ' AND                    
                (W-NSM-NSOC  NOT = W-NSM-NSOCVTE  OR                            
                 W-NSM-NLIEU NOT = W-NSM-NLIEUVTE)                              
                 PERFORM VARYING INSM FROM 1 BY 1 UNTIL INSM > WNSM-NBP         
                    OR (WNSM-NSOC (INSM) = W-NSM-NSOC  AND                      
                        WNSM-NLIEU(INSM) = W-NSM-NLIEU)                         
                    CONTINUE                                                    
                 END-PERFORM                                                    
                 IF INSM > WNSM-NBP                                             
                    IF INSM > WNSM-MAX                                          
                       MOVE '0001'  TO GA99-NSEQERR                             
                       PERFORM MLIBERRGEN                                       
                       MOVE GA99-LIBERR     TO MESS                             
                       GO TO ABANDON-TACHE                                      
                    ELSE                                                        
                       ADD  1           TO WNSM-NBP                             
                       SET  INSM        TO WNSM-NBP                             
                       MOVE W-NSM-NSOC  TO WNSM-NSOC   (INSM)                   
                       MOVE W-NSM-NLIEU TO WNSM-NLIEU  (INSM)                   
PV0307                 MOVE W-NSOC      TO WNSM-NSOCD  (INSM)                   
PV0307                 MOVE W-NLIEU     TO WNSM-NLIEUD (INSM)                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
      *                                                                         
       ENVOI-PLATF-NSM SECTION.                                                 
      *    Ajout de l'envoi si plateforme de livraison nmd                      
           PERFORM VARYING INSM FROM 1 BY 1 UNTIL INSM > WNSM-NBP               
              MOVE 'MDMAG'       TO GA01-CTABLEG1                               
PV0307        MOVE WNSM-NSOCD (INSM) TO GA01-CTABLEG2                           
PV0307        MOVE WNSM-NLIEUD(INSM) TO GA01-CTABLEG2(4:3)                      
              PERFORM SELECT-RTGA01                                             
              IF TROUVE AND GA01-WTABLEG(8:1) = 'O'                             
                 MOVE WNSM-NSOC (INSM)       TO W-NSM-NSOC                      
                 MOVE WNSM-NLIEU(INSM)       TO W-NSM-NLIEU                     
PV0307           MOVE WNSM-NSOCD  (INSM)     TO W-NSOC                          
PV0307           MOVE WNSM-NLIEUD (INSM)     TO W-NLIEU                         
                 PERFORM ENVOI-MESSAGE                                          
              END-IF                                                            
           END-PERFORM                                                          
           INITIALIZE WNSM-ZONE.                                        01280008
                                                                                
