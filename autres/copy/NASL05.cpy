      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
000010 01  NASL05.                                                              
000020     02  NASL05-DSAISIE                                                   
000030         PIC X(0008).                                                     
000040     02  NASL05-NSOCIETE                                                  
000050         PIC X(0003).                                                     
000060     02  NASL05-NLIEU                                                     
000070         PIC X(0003).                                                     
000080     02  NASL05-NSEQ                                                      
000090         PIC X(0003).                                                     
000100     02  NASL05-DOMAINE                                                   
000110         PIC X(0005).                                                     
000120     02  NASL05-DEFGE                                                     
000130         PIC X(0008).                                                     
000140     02  NASL05-NSOCENC                                                   
000150         PIC X(0003).                                                     
000160     02  NASL05-NLIEUENC                                                  
000170         PIC X(0003).                                                     
000180     02  NASL05-NCOMPT                                                    
000190         PIC X(0002).                                                     
000200     02  NASL05-NREFERENCE                                                
000210         PIC X(0020).                                                     
000220     02  NASL05-NREFERENC2                                                
000230         PIC X(0020).                                                     
000240     02  NASL05-PMONTANT                                                  
000250         PIC S9(7)V9(0002) COMP-3.                                        
000260     02  NASL05-DRAPPRO                                                   
000270         PIC X(0008).                                                     
                                                                                
