      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TYREG TYPE DE REGROUPEMENT UNIVERS     *        
      *----------------------------------------------------------------*        
       01  RVTYREG .                                                            
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  TYREG-CTABLEG2    PIC X(15).                                     
           05  TYREG-CTABLEG2-REDEF REDEFINES TYREG-CTABLEG2.                   
               10  TYREG-SOC             PIC X(03).                             
               10  TYREG-UNIVR           PIC X(05).                             
           05  TYREG-WTABLEG     PIC X(80).                                     
           05  TYREG-WTABLEG-REDEF  REDEFINES TYREG-WTABLEG.                    
               10  TYREG-W               PIC X(01).                             
               10  TYREG-SEUIL           PIC X(03).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  TYREG-SEUIL-N        REDEFINES TYREG-SEUIL                   
                                         PIC 9(03).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  TYREG-LCOMMENT        PIC X(20).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTYREG-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYREG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TYREG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYREG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TYREG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
