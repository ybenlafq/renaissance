      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TYPTR CODES TRANSACTIONS / VENTES      *        
      *----------------------------------------------------------------*        
       01  RVTYPTR.                                                             
           05  TYPTR-CTABLEG2    PIC X(15).                                     
           05  TYPTR-CTABLEG2-REDEF REDEFINES TYPTR-CTABLEG2.                   
               10  TYPTR-CODE            PIC X(03).                             
           05  TYPTR-WTABLEG     PIC X(80).                                     
           05  TYPTR-WTABLEG-REDEF  REDEFINES TYPTR-WTABLEG.                    
               10  TYPTR-LIBELLE         PIC X(20).                             
               10  TYPTR-TYPE            PIC X(01).                             
               10  TYPTR-APVF            PIC X(04).                             
               10  TYPTR-DIVERS          PIC X(20).                             
               10  TYPTR-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTYPTR-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYPTR-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TYPTR-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TYPTR-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TYPTR-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
