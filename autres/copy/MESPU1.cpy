      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ VALIDATION D'ECHANGE PORTAIL UNIQUE DANS              
      *   NASC MESSAGE PU1                                                      
      *****************************************************************         
      *                                                                         
      *                                                                         
       01 MESPU1-DATA.                                                          
      *                                                                         
      * DONN�ES CLIENT                                                          
      *                                                                         
      *   N� BON ECHANGE                                                        
          10 MESPU1-NUECH          PIC    X(09).                                
      *   DATE UTILISATION                                                      
          10 MESPU1-DATREC         PIC    X(08).                                
      *   TYPE DE CONTREPARTIE (A AVOIR P PRODUIT)                              
          10 MESPU1-RESECH         PIC    X(01).                                
      *   MONTANT CONTREPARTIE                                                  
          10 MESPU1-PECHA          PIC    S9(07)V9(02).                         
      *   SOCIETE                                                               
          10 MESPU1-NNSOC          PIC    X(03).                                
      *   LIEU                                                                  
          10 MESPU1-NNLIEU         PIC    X(03).                                
      *   VENTE                                                                 
          10 MESPU1-NNVENTE        PIC    X(07).                                
      *   NSEQNQ                                                                
          10 MESPU1-NNSEQNQ        PIC    9(05).                                
      *   CODIC                                                                 
          10 MESPU1-NNCODIC        PIC    X(07).                                
      *   FIN DE GARANTIE                                                       
          10 MESPU1-DTFGAR         PIC    X(08).                                
                                                                                
