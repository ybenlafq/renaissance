      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *              ----------------------------------               *         
      *              - TRAITEMENTS COMMUNS AUX VENTES -               *         
      *              ----------------------------------               *         
      *                                                               *         
      * MGV00 REGROUPE LES TRAITEMENTS COMMUNS                        *         
      * A TOUS LES PROGRAMMES DE VENTE.                               *         
      *                                                               *         
      * TRAITEMENTS COMMUNS : - APPELER-TETDATC                       *         
      *                       - CHARGEMENT-DSYST                      *         
      *                       - REMPLISSAGE-ZONE-NUMERIQUE            *         
      *                       - TRAITEMENT-ZONE-NUMERIQUE             *         
      *                                                               *         
      * MGV00 EST UTILISE PAR TOUS LES PROGRAMMES DE VENTE            *         
      *                                                               *         
      *****************************************************************         
      *                                                                         
      *                                                                         
       APPELER-TETDATC                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE  LENGTH OF Z-COMMAREA-TETDATC                                   
                                          TO LONG-COMMAREA-LINK                 
           MOVE  Z-COMMAREA-TETDATC       TO Z-COMMAREA-LINK                    
           MOVE 'TETDATC'                 TO NOM-PROG-LINK                      
           PERFORM  LINK-PROG                                                   
           MOVE  Z-COMMAREA-LINK          TO Z-COMMAREA-TETDATC.                
      *                                                                         
       CHARGEMENT-DSYST                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           CALL 'SPDATDB2' USING WRET DATHEUR                                   
           IF  WRET NOT = ZERO                                                  
               MOVE WRET                  TO  WRET-PIC                          
               MOVE W-MESS-SPDATDB2       TO  MESS                              
               GO TO ABANDON-TACHE                                              
           END-IF                                                               
           MOVE DATHEUR                   TO  WDATHEUR.                         
      *                                                                         
       REMPLISSAGE-ZONE-NUMERIQUE      SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      *    CHARGEMENT DE LA ZONE WS-ALPHA                                       
      *                                                                         
           MOVE WS-DECIMAL-7S-D           TO WS-ENTIER-2                        
           IF WS-DECIMAL-7S   > ZEROES                                          
              IF WS-ENTIER-2 = ZEROES                                           
                 MOVE WS-DECIMAL-7S       TO WS-MONTANT-3Z                      
                 MOVE WS-MONTANT-3Z       TO W-MONT-USER                        
              ELSE                                                              
                 MOVE WS-DECIMAL-7S       TO WS-MONTANT-4Z                      
                 MOVE WS-MONTANT-4Z       TO W-MONT-USER                        
              END-IF                                                            
           ELSE                                                                 
              IF WS-ENTIER-2 = ZEROES                                           
                 MOVE WS-DECIMAL-7S       TO WS-MONTANT-3                       
                 MOVE WS-MONTANT-3        TO W-MONT-USER                        
              ELSE                                                              
                 MOVE WS-DECIMAL-7S       TO WS-MONTANT-4                       
                 MOVE WS-MONTANT-4        TO W-MONT-USER                        
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       TRAITEMENT-ZONE-NUMERIQUE       SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      *    CHARGEMENT DE LA ZONE WS-ALPHA                                       
      *                                                                         
           PERFORM TRAITEMENT-MONTANT                                           
           IF NORMAL                                                            
              IF W-MONT-NB-DEC NOT = ZEROES                                     
                 IF W-MONT-SIGNE = SPACE                                        
                    MOVE W-MONT-FILE   TO WS-DECIMAL-6                          
                    MOVE WS-DECIMAL-6  TO WS-MONTANT-4Z                         
                    MOVE WS-MONTANT-4Z TO WS-ALPHA                              
                 ELSE                                                           
                    MOVE W-MONT-FILE   TO WS-DECIMAL-5S                         
                    MOVE WS-DECIMAL-5S TO WS-MONTANT-4                          
                    MOVE WS-MONTANT-4  TO WS-ALPHA                              
                 END-IF                                                         
              ELSE                                                              
                 IF W-MONT-SIGNE = SPACE                                        
                    MOVE W-MONT-FILE   TO WS-ENTIER-9                           
                    MOVE WS-ENTIER-9   TO WS-MONTANT-3Z                         
                    MOVE WS-MONTANT-3Z TO WS-ALPHA                              
                 ELSE                                                           
                    MOVE W-MONT-FILE   TO WS-ENTIER-8S                          
                    MOVE WS-ENTIER-8S  TO WS-MONTANT-3                          
                    MOVE WS-MONTANT-3  TO WS-ALPHA                              
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
                                                                                
