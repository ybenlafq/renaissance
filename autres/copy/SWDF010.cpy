      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *--------------------------------------------------*                      
      *    DEFINITION DU FICHIER CSV POUR ETAT  IEF010   *                      
      *    LONGUEUR : 194                                *                      
      *--------------------------------------------------*                      
       01  FDF010-DSECT.                                                        
           05  FDF010-NOMETAT        PIC X(06)  VALUE 'IEF010'.                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-DATETRT        PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-CTRAIT         PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-LTRAIT         PIC X(20).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-NCODIC         PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-CTIERS-AFF     PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-LTIERS         PIC X(20).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-CTIERS         PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-NENTCDE        PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-CTIERSELA      PIC X(04).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-NENVOI         PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-DENVOI         PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-CRENDU         PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-QTENV          PIC -9(5).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-MTPROV         PIC -9(7)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-PRMP           PIC -9(7)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-PSTDTTC        PIC -9(7)V99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-CTYPT          PIC X(01).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-DEVREF         PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-DEVEQU         PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-PTAUX          PIC -9V9(6).                               
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  FDF010-NSERIE         PIC X(16).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
      *                                                                         
      *--------------------------------------------------*                      
                                                                                
