      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      * -------------------------------------------------------------- *        
      * CETTE COPIE GERE TOUT CE QUI TOUCHE LA GESTION DE LA TS        *        
      * TSTENVOI: ** CREATION SI CETTE DERNERE N'EXISTE PAS            *        
      *           ** LECTURE DE CETTE TS                               *        
      * LES ZONES DE TRAVAIL SONT DECLAREES DANS WKTENVOI              *        
      *                                                                *        
      * CREE LE 01/03/2011 PAR DE02003 (CL)                            *        
      *                                                                *        
      * MODIF LE 16/08/2011 PAR DE02074 (EG)                           *        
      *          DANS GESTION TSTENVOI, ON AJOUTE UN ENREG DE FIN, SI  *        
      *          ON NE LE TROUVE PAS LORS DE LA LECTURE DE LA TS, ON   *        
      *          DELETE LA TS POUR LA RE CREER                         *        
      * -------------------------------------------------------------- *        
       GESTION-TSTENVOI  SECTION.                                               
           MOVE 1                            TO WP-RANG-TSTENVOI                
           PERFORM LECTURE-TSTENVOI                                             
           IF WC-TSTENVOI-FIN                                                   
              PERFORM CREATION-TSTENVOI                                         
           ELSE                                                                 
      * ON CHERCHE LE DERNIER ENREG DE LA TSTENVOI                              
              PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                      
              UNTIL WC-TSTENVOI-FIN                                             
                 PERFORM LECTURE-TSTENVOI                                       
              END-PERFORM                                                       
              IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '                        
                 PERFORM DELETE-TSTENVOI                                        
                 PERFORM CREATION-TSTENVOI                                      
              END-IF                                                            
           END-IF.                                                              
       FIN-GEST-TSTENVOI.  EXIT.                                                
      *                                                                         
      *-----------------------------------------------------------------        
       CREATION-TSTENVOI SECTION.                                               
                                                                                
              MOVE '00000'                   TO TSTENVOI-NSOCZP                 
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM                 
              MOVE 'NOM PROGRAMME'           TO TSTENVOI-LIBELLE                
              PERFORM ECRITURE-TSTENVOI                                         
              SET WC-XCTRL-EXPDEL-NON-TROUVE TO TRUE                            
              INITIALIZE WZ-XCTRL-EXPDEL-SQLCODE                                
              PERFORM DECLARE-OPEN-CXCTRL                                       
              IF WC-XCTRL-EXPDEL-PB-DB2                                         
                 SET WC-TSTENVOI-ERREUR TO TRUE                                 
                 GO TO FIN-CREATION-TSTENVOI                                    
              END-IF                                                            
              PERFORM FETCH-CXCTRL                                              
              IF WC-XCTRL-EXPDEL-PB-DB2                                         
                 SET WC-TSTENVOI-ERREUR TO TRUE                                 
                 GO TO FIN-CREATION-TSTENVOI                                    
              END-IF                                                            
              PERFORM UNTIL WC-XCTRL-EXPDEL-FIN                                 
                 IF  XCTRL-WPARAM   > SPACES                                    
                 AND XCTRL-LIBELLE  > SPACES                                    
                    MOVE XCTRL-SOCZP         TO TSTENVOI-NSOCZP                 
                    MOVE XCTRL-CTRL          TO TSTENVOI-CTRL                   
                    MOVE XCTRL-WPARAM        TO TSTENVOI-WPARAM                 
                    MOVE XCTRL-LIBELLE       TO TSTENVOI-LIBELLE                
                    PERFORM ECRITURE-TSTENVOI                                   
                 END-IF                                                         
                 PERFORM FETCH-CXCTRL                                           
                 IF WC-XCTRL-EXPDEL-PB-DB2                                      
                    SET WC-TSTENVOI-ERREUR TO TRUE                              
                    GO TO FIN-CREATION-TSTENVOI                                 
                 END-IF                                                         
              END-PERFORM                                                       
              PERFORM CLOSE-CXCTRL                                              
      * AJOUT D'UN ENREG DE FIN                                                 
              MOVE '99999'                   TO TSTENVOI-NSOCZP                 
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM                 
              MOVE 'FIN TSTENVOI '           TO TSTENVOI-LIBELLE                
              PERFORM ECRITURE-TSTENVOI.                                        
                                                                                
       FIN-CREATION-TSTENVOI.  EXIT.                                            
      *                                                                         
      *-----------------------------------------------------------------        
       LECTURE-TSTENVOI SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         READQ TS                                                        
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         INTO   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         ITEM   (WP-RANG-TSTENVOI)                                       
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
              SET WC-TSTENVOI-SUITE      TO TRUE                                
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
           WHEN 26                                                              
           WHEN 44                                                              
              SET WC-TSTENVOI-FIN        TO TRUE                                
           WHEN OTHER                                                           
              SET WC-TSTENVOI-ERREUR     TO TRUE                                
           END-EVALUATE.                                                        
       FIN-LECT-TSTENVOI. EXIT.                                                 
      *                                                                         
      *-----------------------------------------------------------------        
       ECRITURE-TSTENVOI SECTION.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         WRITEQ TS                                                       
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         FROM   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
                QUEUE  (WC-ID-TSTENVOI)                                         
                FROM   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           INITIALIZE TS-TSTENVOI-DONNEES.                                      
       FIN-ECRIT-TSTENVOI. EXIT.                                                
      *                                                                         
      *-----------------------------------------------------------------        
       DECLARE-OPEN-CXCTRL SECTION.                                             
           MOVE FUNC-DECLARE             TO TRACE-SQL-FUNCTION.                 
           MOVE 'RVGA01ZZ'               TO TABLE-NAME.                         
           MOVE 'EXPDEL'                 TO MODEL-NAME.                         
           EXEC SQL                                                             
                DECLARE CXCTRL CURSOR FOR                                       
                SELECT SOCZP, CTRL, WPARAM, LIBELLE                             
                FROM RVGA01ZZ                                                   
                WHERE TRANS = 'EXPDEL'                                          
                AND   WFLAG = 'O'                                               
                ORDER BY SOCZP, CTRL                                            
           END-EXEC.                                                            
           PERFORM TEST-SQLCODE.                                                
           MOVE FUNC-OPEN                TO TRACE-SQL-FUNCTION.                 
           EXEC SQL OPEN CXCTRL END-EXEC.                                       
           PERFORM TEST-SQLCODE.                                                
      *                                                                         
       FETCH-CXCTRL SECTION.                                                    
           MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.                 
           EXEC SQL                                                             
                FETCH CXCTRL                                                    
                INTO :XCTRL-SOCZP                                               
                   , :XCTRL-CTRL                                                
                   , :XCTRL-WPARAM                                              
                   , :XCTRL-LIBELLE                                             
           END-EXEC.                                                            
           PERFORM TEST-SQLCODE.                                                
           IF WC-XCTRL-EXPDEL-SUITE                                             
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
           END-IF.                                                              
      *                                                                         
       CLOSE-CXCTRL SECTION.                                                    
           MOVE FUNC-CLOSE               TO TRACE-SQL-FUNCTION.                 
           EXEC SQL CLOSE CXCTRL  END-EXEC.                                     
           PERFORM TEST-SQLCODE.                                                
      *                                                                         
       DELETE-TSTENVOI SECTION.                                                 
      ******************************                                            
                                                                                
           EXEC CICS DELETEQ TS                                                 
                QUEUE    (WC-ID-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
                                                                                
       FIN-DELETE-TSTENVOI. EXIT.                                               
      ********************************                                          
      *-----------------------------------------------------------------        
       TEST-SQLCODE SECTION.                                                    
            EVALUATE TRUE                                                       
            WHEN SQLCODE = 0                                                    
                 SET WC-XCTRL-EXPDEL-SUITE    TO TRUE                           
            WHEN SQLCODE = +100                                                 
                 SET WC-XCTRL-EXPDEL-FIN      TO TRUE                           
            WHEN OTHER                                                          
                 SET WC-XCTRL-EXPDEL-PB-DB2   TO TRUE                           
                 SET WC-XCTRL-EXPDEL-FIN      TO TRUE                           
                 MOVE SQLCODE                 TO WZ-XCTRL-EXPDEL-SQLCODE        
           END-EVALUATE.                                                        
                                                                                
