      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION ARTICLE ENT�TE SPECIFIQUE             * 00030000
      * NOM FICHIER.: RAESPC6                                         * 00031000
      *---------------------------------------------------------------* 00032000
      * CR   .......: 27/08/2013                                      * 00033000
      * MODIFIE.....:   /  /                                          * 00034000
      * VERSION N�..: 001                                             * 00035000
      * LONGUEUR....: 259                                             * 00036000
      ***************************************************************** 00037000
      *                                                                 00038000
       01  RAESPC6.                                                     00039000
      * TYPE ENREGISTREMENT : R�CEPTION ARTICLE ENT�TE SP�CIFIQUE       00040000
           05      RAESPC6-TYP-ENREG      PIC  X(0006).                 00050000
      * CODE SOCI�T�                                                    00060000
           05      RAESPC6-CSOCIETE       PIC  X(0005).                 00070000
      * CODE ARTICLE                                                    00080000
           05      RAESPC6-CARTICLE       PIC  X(0018).                 00090000
      * CODE FAMILLE NCG                                                00100000
           05      RAESPC6-CFAM           PIC  X(0006).                 00110000
      * CODE RAYON DACEM                                                00110100
           05      RAESPC6-RAYON-DACEM    PIC  X(0006).                 00110200
      * CHEF DE PRODUIT                                                 00110300
           05      RAESPC6-CHEFPROD       PIC  X(0012).                 00110400
      * LIBELLE MARQUE                                                  00110500
           05      RAESPC6-LMARQ          PIC  X(0010).                 00110600
      * CODE STATUT APPROVISIONNEMENT                                   00110700
           05      RAESPC6-CAPPRO         PIC  X(0003).                 00110800
      * MULTIPLE DE VENTE                                               00110900
           05      RAESPC6-MULTIPLE-VENTE PIC  9(0005).                 00111000
      * TYPE ARTICLE                                                    00112000
           05      RAESPC6-TYPE-ARTICLE   PIC  X(0003).                 00113000
      * PRODUIT DANGEREUX                                               00114000
           05      RAESPC6-PROD-DANGEREUX PIC  X(0010).                 00115000
      * NATURE DU DANGER                                                00116000
           05      RAESPC6-NATURE-DANGER  PIC  X(0030).                 00117000
      * PRODUIT ALIMENTAIRE                                             00118000
           05      RAESPC6-PROD-ALIMENTAIRE PIC X(001).                 00119000
      * PRODUIT LIQUIDE                                                 00120000
           05      RAESPC6-PROD-LIQUIDE   PIC  X(0001).                 00130000
      * DATE DE PEREMPTION                                              00140000
           05      RAESPC6-DATE-PEREMPTION PIC  X(001).                 00141000
      * DELAI D'ACCEPTABILITE                                           00142000
           05      RAESPC6-DELAI-ACCEPT   PIC  X(0001).                 00143000
      * CODE NOMENCLATURE DOUANIERE                                     00144000
           05      RAESPC6-CNOMDOU        PIC  X(0008).                 00145000
      * PAYS ORIGINE                                                    00146000
           05      RAESPC6-PAYS-ORIGINE   PIC  X(0003).                 00147000
      * LIBELL� COMMERCIAL COMPLET                                      00148000
           05      RAESPC6-LIB-CIAL       PIC  X(0040).                 00149000
      * LIBELL� LOGISTIQUE COMPLET                                      00150000
           05      RAESPC6-LIB-LOGISTIQUE PIC  X(0040).                 00160000
      * PRODUIT REMPLAC�                                                00161000
           05      RAESPC6-PRODUIT-REMPLACE PIC X(012).                 00162000
      * COLLECTION DARTY                                                00162100
           05      RAESPC6-COLL-DARTY     PIC  X(0003).                 00162200
      * PRODUIT D3E (O/N)                                               00162300
           05      RAESPC6-PRODUIT-D3E    PIC  X(0001).                 00162400
      * NOTION DE PRODUIT FABRIQU� (O/N)                                00162500
           05      RAESPC6-PRODUIT-FABRIQUE PIC X(001).                 00162600
      * TYPE DE FLUX                                                    00162700
           05      RAESPC6-TYPE-FLUX      PIC  X(0002).                 00162800
      * CODE EAN 2                                                      00162900
           05      RAESPC6-EAN2           PIC  X(0013).                 00163000
      * CODE EAN 3                                                      00164000
           05      RAESPC6-EAN3           PIC  X(0013).                 00165000
      * CODE EAN 4                                                      00166000
           05      RAESPC6-EAN4           PIC  X(0013).                 00167000
      * CODE EAN 5                                                      00168000
           05      RAESPC6-EAN5           PIC  X(0013).                 00169000
      * CODE EAN 6                                                      00170000
           05      RAESPC6-EAN6           PIC  X(0013).                 00170100
      * FILLER DE 30                                                    00170200
           05      RAESPC6-FILLER         PIC  X(0030).                 00170300
      * FIN                                                             00170400
           05      RAESPC6-FIN            PIC  X(0001).                 00170500
                                                                                
