      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.TTSL11                        *        
      ******************************************************************        
       01  TVSL1100.                                                            
      *                       NSOCIETE                                          
           10 SL11C-NSOCIETE       PIC X(3).                                    
      *                       NLIEU                                             
           10 SL11C-NLIEU          PIC X(3).                                    
      *                       NCODIC                                            
           10 SL11C-NCODIC         PIC X(7).                                    
      *                       CRSL                                              
           10 SL11C-CRSL           PIC X(5).                                    
      *                       QUANTITE                                          
           10 SL11C-QUANTITE       PIC S9(7)V USAGE COMP-3.                     
      *                       DSYST                                             
           10 SL11C-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                       XNSOCIETE                                         
           10 SL11C-XNSOCIETE      PIC X(3).                                    
      *                       XNLIEU                                            
           10 SL11C-XNLIEU         PIC X(3).                                    
      *                       XNCODIC                                           
           10 SL11C-XNCODIC        PIC X(7).                                    
      *                       XCRSL                                             
           10 SL11C-XCRSL          PIC X(5).                                    
      *                       XQUANTITE                                         
           10 SL11C-XQUANTITE      PIC S9(7)V USAGE COMP-3.                     
      *                       XDSYST                                            
           10 SL11C-XDSYST         PIC S9(13)V USAGE COMP-3.                    
      *                       IBMSNAP_COMMITSEQ                                 
           10 SL11C-IBMSNAP-COMMITSEQ PIC X(10).                                
      *                       IBMSNAP_INTENTSEQ                                 
           10 SL11C-IBMSNAP-INTENTSEQ PIC X(10).                                
      *                       IBMSNAP_OPERATION                                 
           10 SL11C-IBMSNAP-OPERATION PIC X(1).                                 
      *                       IBMSNAP_LOGMARKER                                 
           10 SL11C-IBMSNAP-LOGMARKER PIC X(26).                                
      *                       NSEQ                                              
           10 SL11C-NSEQ           PIC S9(18)V USAGE COMP-3.                    
      *                       STATUT                                            
           10 SL11C-STATUT         PIC X(10).                                   
      *                       IDENTIFIANT                                       
           10 SL11C-IDENTIFIANT    PIC X(12).                                   
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 19      *        
      ******************************************************************        
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE TVSL1100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  TVSL1100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-NSOCIETE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-NSOCIETE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-NLIEU-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-NLIEU-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-NCODIC-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-NCODIC-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-CRSL-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-CRSL-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-QUANTITE-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-QUANTITE-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-DSYST-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-DSYST-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-XNSOCIETE-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-XNSOCIETE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-XNLIEU-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-XNLIEU-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-XNCODIC-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-XNCODIC-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-XCRSL-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-XCRSL-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-XQUANTITE-F    PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-XQUANTITE-F    PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-XDSYST-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 SL11C-XDSYST-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-IBMSNAP-COMMITSEQ-F PIC S9(4) COMP.                         
      *--                                                                       
           10 SL11C-IBMSNAP-COMMITSEQ-F PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-IBMSNAP-INTENTSEQ-F PIC S9(4) COMP.                         
      *--                                                                       
           10 SL11C-IBMSNAP-INTENTSEQ-F PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-IBMSNAP-OPERATION-F PIC S9(4) COMP.                         
      *--                                                                       
           10 SL11C-IBMSNAP-OPERATION-F PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-IBMSNAP-LOGMARKER-F PIC S9(4) COMP.                         
      *--                                                                       
           10 SL11C-IBMSNAP-LOGMARKER-F PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-NSEQ-F              PIC S9(4) COMP.                         
      *--                                                                       
           10 SL11C-NSEQ-F              PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-STATUT-F            PIC S9(4) COMP.                         
      *--                                                                       
           10 SL11C-STATUT-F            PIC S9(4) COMP-5.                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 SL11C-IDENTIFIANT-F       PIC S9(4) COMP.                         
      *                                                                         
      *--                                                                       
           10 SL11C-IDENTIFIANT-F       PIC S9(4) COMP-5.                       
                                                                                
      *}                                                                        
