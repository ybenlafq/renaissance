      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVWS0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVWS0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVWS0000.                                                            
           02  WS00-NSOCDEPOT                                                   
               PIC X(0003).                                                     
           02  WS00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  WS00-NCODIC                                                      
               PIC X(0007).                                                     
           02  WS00-CFAM                                                        
               PIC X(0005).                                                     
           02  WS00-CMARQ                                                       
               PIC X(0005).                                                     
           02  WS00-LREFFOURN                                                   
               PIC X(0020).                                                     
           02  WS00-LCOMMENT                                                    
               PIC X(0050).                                                     
           02  WS00-WDACEM                                                      
               PIC X(0001).                                                     
           02  WS00-DMAJ                                                        
               PIC X(0008).                                                     
           02  WS00-DMAJWMS                                                     
               PIC X(0008).                                                     
           02  WS00-D1RECEPT                                                    
               PIC X(0008).                                                     
           02  WS00-NEAN                                                        
               PIC X(0013).                                                     
           02  WS00-QPOIDS                                                      
               PIC S9(7) COMP-3.                                                
           02  WS00-QLARGEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  WS00-QPROFONDEUR                                                 
               PIC S9(3) COMP-3.                                                
           02  WS00-QHAUTEUR                                                    
               PIC S9(3) COMP-3.                                                
           02  WS00-CASSORT                                                     
               PIC X(0005).                                                     
           02  WS00-QCOLIVTE                                                    
               PIC S9(5) COMP-3.                                                
           02  WS00-CAPPRO                                                      
               PIC X(0005).                                                     
           02  WS00-LEMBALLAGE                                                  
               PIC X(0050).                                                     
           02  WS00-QCOLIRECEPT                                                 
               PIC S9(5) COMP-3.                                                
           02  WS00-QCOLIDSTOCK                                                 
               PIC S9(5) COMP-3.                                                
           02  WS00-CFETEMPL                                                    
               PIC X(0001).                                                     
           02  WS00-CMODSTOCK1                                                  
               PIC X(0005).                                                     
           02  WS00-WMODSTOCK1                                                  
               PIC X(0001).                                                     
           02  WS00-CMODSTOCK2                                                  
               PIC X(0005).                                                     
           02  WS00-WMODSTOCK2                                                  
               PIC X(0001).                                                     
           02  WS00-CMODSTOCK3                                                  
               PIC X(0005).                                                     
           02  WS00-WMODSTOCK3                                                  
               PIC X(0001).                                                     
           02  WS00-QNBRANMAIL                                                  
               PIC S9(3) COMP-3.                                                
           02  WS00-QNBNIVGERB                                                  
               PIC S9(3) COMP-3.                                                
           02  WS00-CZONEACCES                                                  
               PIC X(0001).                                                     
           02  WS00-CCONTENEUR                                                  
               PIC X(0001).                                                     
           02  WS00-CSPECIFSTK                                                  
               PIC X(0001).                                                     
           02  WS00-CCOTEHOLD                                                   
               PIC X(0001).                                                     
           02  WS00-QNBPVSOL                                                    
               PIC S9(3) COMP-3.                                                
           02  WS00-QNBPRACK                                                    
               PIC S9(5) COMP-3.                                                
           02  WS00-CTPSUP                                                      
               PIC X(0001).                                                     
           02  WS00-CQUOTA                                                      
               PIC X(0005).                                                     
           02  WS00-CLASSE                                                      
               PIC X(0001).                                                     
           02  WS00-WCDEENCOURS                                                 
               PIC X(0001).                                                     
           02  WS00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVWS0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVWS0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-NSOCDEPOT-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-NSOCDEPOT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CMARQ-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CMARQ-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-LREFFOURN-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-LREFFOURN-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-LCOMMENT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-LCOMMENT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-WDACEM-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-WDACEM-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-DMAJ-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-DMAJ-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-DMAJWMS-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-DMAJWMS-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-D1RECEPT-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-D1RECEPT-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-NEAN-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-NEAN-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QPOIDS-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QPOIDS-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QLARGEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QLARGEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QPROFONDEUR-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QPROFONDEUR-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QHAUTEUR-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QHAUTEUR-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CASSORT-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CASSORT-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QCOLIVTE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QCOLIVTE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CAPPRO-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CAPPRO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-LEMBALLAGE-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-LEMBALLAGE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QCOLIRECEPT-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QCOLIRECEPT-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QCOLIDSTOCK-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QCOLIDSTOCK-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CFETEMPL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CFETEMPL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-WMODSTOCK1-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-WMODSTOCK1-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-WMODSTOCK2-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-WMODSTOCK2-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-WMODSTOCK3-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-WMODSTOCK3-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QNBRANMAIL-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QNBRANMAIL-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QNBNIVGERB-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QNBNIVGERB-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CZONEACCES-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CZONEACCES-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CCONTENEUR-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CCONTENEUR-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CSPECIFSTK-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CSPECIFSTK-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CCOTEHOLD-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CCOTEHOLD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QNBPVSOL-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QNBPVSOL-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-QNBPRACK-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-QNBPRACK-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CTPSUP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CTPSUP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CQUOTA-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CQUOTA-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-CLASSE-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-CLASSE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-WCDEENCOURS-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-WCDEENCOURS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  WS00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  WS00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
