      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION MOUVEMENTS SPECIFIQUE                  *
      * NOM FICHIER.: EMVSPC                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 24                                              *
      *****************************************************************
      *
       01  EMVSPC.
      * TYPE ENREGISTREMENT : EMISSION IMAGE STANDARD
           05      EMVSPC-TYP-ENREG       PIC  X(0006).
      * NUMERO DE MOUVEMENT
           05      EMVSPC-NMVT            PIC  9(0009).
      * DATE LIVRAISON PREVUE POUR COMMANDE FOURNISSEUR
           05      EMVSPC-DPREVCDE        PIC  X(0008).
      * FIN DE L'ENREGISTREMENT
           05      EMVSPC-FIN             PIC  X(0001).
      
