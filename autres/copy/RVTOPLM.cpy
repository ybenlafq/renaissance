      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TOPLM TYPE OP ET CODE URGENCE - LM7    *        
      *----------------------------------------------------------------*        
       01  RVTOPLM .                                                            
           05  TOPLM-CTABLEG2    PIC X(15).                                     
           05  TOPLM-CTABLEG2-REDEF REDEFINES TOPLM-CTABLEG2.                   
               10  TOPLM-NSOCDEP         PIC X(06).                             
               10  TOPLM-NSOCLIE         PIC X(06).                             
               10  TOPLM-NSEQ            PIC X(03).                             
           05  TOPLM-WTABLEG     PIC X(80).                                     
           05  TOPLM-WTABLEG-REDEF  REDEFINES TOPLM-WTABLEG.                    
               10  TOPLM-CSELMU          PIC X(07).                             
               10  TOPLM-TYPOP           PIC X(02).                             
               10  TOPLM-CURGE           PIC X(01).                             
               10  TOPLM-CURGE-N        REDEFINES TOPLM-CURGE                   
                                         PIC 9(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVTOPLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TOPLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TOPLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TOPLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TOPLM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
