      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DB002 LES MAJ REDBOX                   *        
      *----------------------------------------------------------------*        
       01  RVDB002.                                                             
           05  DB002-CTABLEG2    PIC X(15).                                     
           05  DB002-CTABLEG2-REDEF REDEFINES DB002-CTABLEG2.                   
               10  DB002-CHGT            PIC X(05).                             
               10  DB002-CENREG          PIC X(07).                             
           05  DB002-WTABLEG     PIC X(80).                                     
           05  DB002-WTABLEG-REDEF  REDEFINES DB002-WTABLEG.                    
               10  DB002-LIBELLE         PIC X(20).                             
               10  DB002-ACTION          PIC X(01).                             
               10  DB002-WFLAG           PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVDB002-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DB002-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DB002-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DB002-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DB002-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
