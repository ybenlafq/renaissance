           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
      * Table RTIGSA      
       01 H-RTIGSA.
          10 H-DIGSA.
             15 H-DIGFA0   PIC X(36).
             15 H-COL01001 PIC X(92).
      
      * Table RTIGSB
       01 H-RTIGSB.
          10 H-DIGSB-DIGFA0   PIC X(36).
          10 H-NT-DIGSB       PIC S9(1)V COMP-3.
          10 H-DIGSB.
             15 H-COL05001    PIC X(90).

      * Table RTIGSC
       01 H-RTIGSC.
          10 H-DIGSC-DIGFA0   PIC X(36).
          10 H-SM-DIGSC       PIC X(26).
          10 H-DIGSC.
             15 H-COL06001    PIC X(2).
             15 H-DIGFC0      PIC X(12).
             15 H-COL06002    PIC X(144).

      * Table RTIGSD
       01 H-RTIGSD.
          10 H-DIGSD-DIGFA0   PIC X(36).
          10 H-DIGSD.
             15 H-DIGFD0      PIC X(24).
             15 H-COL02001    PIC X(40).

      * Table RTIGSE
       01 H-RTIGSE.
          10 H-DIGSE-DIGFA0   PIC X(36).
          10 H-DIGSE.
             15 H-DIGFE0      PIC X(9).
             15 H-COL03001    PIC X(55).

      * Table RTIGSF
       01 H-RTIGSF.
          10 H-DIGSF-DIGFA0   PIC X(36).
          10 H-SM-DIGSF       PIC X(26).
          10 H-DIGSF.
             15 H-DIGFF0      PIC X(4).
             15 H-COL04001    PIC X(60).

           EXEC SQL END DECLARE SECTION END-EXEC.
