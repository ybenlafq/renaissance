      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES LIGNES DE MUTATIONS                  *        
      *  PROGRAMME  : MMU95                                            *        
      *  TITRE      : COMMAREA DU MODULE MMU95                         *        
      *               CHANGEMENT DE MUTATION D'UNE VENTE               *        
      *  LONGUEUR   : 150 C                                            *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MMU95-APPLI.                                                    
      *--  DONNEES EN ENTREE DU MODULE ------------------------------ 54        
           02 COMM-MMU95-DONNEES-ENTREE.                                        
              03 COMM-MMU95-TYPE-APPEL        PIC X(01).                        
                 88 COMM-MMU95-CODICGRP                  VALUE '1'.             
                 88 COMM-MMU95-CODIC                     VALUE '2'.             
                 88 COMM-MMU95-VENTE                     VALUE '3'.             
              03 COMM-MMU95-NMUTATION-ORIG    PIC X(07).                00890001
              03 COMM-MMU95-NMUTATION-DEST    PIC X(07).                00890001
              03 COMM-MMU95-NSOCIETE          PIC X(03).                00890001
              03 COMM-MMU95-NLIEU             PIC X(03).                00890001
              03 COMM-MMU95-NVENTE            PIC X(07).                00890001
              03 COMM-MMU95-NSOCLIVR          PIC X(03).                00890001
              03 COMM-MMU95-NDEPOT            PIC X(03).                00890001
              03 COMM-MMU95-NSOCDEPLIV        PIC X(03).                00890001
              03 COMM-MMU95-NLIEUDEPLIV       PIC X(03).                00890001
              03 COMM-MMU95-NCODIC            PIC X(07).                00890001
              03 COMM-MMU95-NCODICGRP         PIC X(07).                00890001
      *--- DONNEES EN SORTIE DU MODULE ------------------------------ 59        
           02 COMM-MMU95-DONNEES-SORTIE.                                        
              03 COMM-MMU95-MESSAGE.                                            
                 05 COMM-MMU95-CODRET         PIC X(01).                        
                    88 COMM-MMU95-CODRET-OK              VALUE ' '.             
                    88 COMM-MMU95-CODRET-KO              VALUE '1'.             
                 05 COMM-MMU95-LIBERR.                                          
                    20 COMM-MMU95-NSEQERR     PIC X(04).                        
                    20 COMM-MMU95-ERRFIL      PIC X(01).                        
                    20 COMM-MMU95-LMESSAGE    PIC X(53).                        
      *--- DONNEES EN ENTREE DU MODULE -AJOUT------------------------ 13        
CL3107     02 COMM-MMU95-DONNEES-ENTREE-BIS.                                    
  -           03 COMM-MMU95-CACID            PIC X(08).                         
CL3107        03 COMM-MMU95-PGM-APPELLANT    PIC X(05).                         
      *--- RESERVE -------------------------------------------------- 24        
           02 COMM-MMU95-FILLER               PIC X(24).                        
                                                                                
