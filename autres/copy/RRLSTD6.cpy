      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION R�CEPTION LIGNE STANDARD (MUTATION)   * 00030000
      * NOM FICHIER.: RRLSTD6                                         * 00031000
      *---------------------------------------------------------------* 00032000
      * CR   .......: 27/08/2013                                      * 00033000
      * MODIFIE.....:   /  /                                          * 00034000
      * VERSION N�..: 001                                             * 00035000
      * LONGUEUR....: 400                                             * 00036000
      ***************************************************************** 00037000
      *                                                                 00037100
       01  RRLSTD6.                                                     00037200
      * TYPE ENREGISTREMENT                                             00037300
           05      RRLSTD6-TYP-ENREG      PIC  X(0006).                 00037400
      * CODE ACTION                                                     00037500
           05      RRLSTD6-CACTION        PIC  X(0001).                 00037600
      * CODE SOCI�T�                                                    00037700
           05      RRLSTD6-CSOCIETE       PIC  X(0005).                 00037800
      * NUM�RO DE DOSSIER DE R�CEPTION                                  00037900
           05      RRLSTD6-NDOS-REC       PIC  X(0015).                 00038000
      * NUM�RO DE LIGNE DE R�CEPTION                                    00038100
           05      RRLSTD6-NLIGNE-REC     PIC  9(0005).                 00038200
      * NUM�RO DE SOUS-LIGNE DE R�CEPTION                               00038300
           05      RRLSTD6-NSSLIGNE-REC   PIC  9(0005).                 00038400
      * NUM�RO DE COMMANDE N3                                           00038500
           05      RRLSTD6-NCDE-N3        PIC  X(0015).                 00038600
      * FABRICATION                                                     00038700
           05      RRLSTD6-FABRICATION    PIC  9(0001).                 00038800
      * ADRESSE FABRICATION                                             00038900
           05      RRLSTD6-ADR-FABRICATION PIC X(0020).                 00039000
      * TYPE DE LIGNE DE R�CEPTION                                      00040000
           05      RRLSTD6-TYPE-LIG-REC   PIC  9(0002).                 00050000
      * CODE ARTICLE                                                    00060000
           05      RRLSTD6-ARTICLE        PIC  X(0018).                 00070000
      * NUM�RO DE LOT                                                   00080000
           05      RRLSTD6-NLOT           PIC  X(0015).                 00090000
      * QUANTIT� PR�VUE                                                 00100000
           05      RRLSTD6-QTE-PREVUE     PIC  9(0009).                 00110000
      * QUANTIT� PR�VUE MAXIMUM                                         00120000
           05      RRLSTD6-QTE-PREVUE-MAX PIC  9(0009).                 00121000
      * CODE FOURNISSEUR                                                00121100
           05      RRLSTD6-CFOURNISSEUR   PIC  X(0015).                 00121200
      * LIBELL� FOURNISSEUR                                             00121300
           05      RRLSTD6-LFOURNISSEUR   PIC  X(0035).                 00121400
      * ORIGINE                                                         00121500
           05      RRLSTD6-ORIGINE        PIC  X(0010).                 00121600
      * PROPRI�TAIRE                                                    00121700
           05      RRLSTD6-PROPRIETAIRE   PIC  X(0020).                 00121800
      * �TAT BLOCAGE                                                    00121900
           05      RRLSTD6-ETAT-BLOCAGE   PIC  9(0003).                 00122000
      * DESCRIPTION SUPPORT                                             00123000
           05      RRLSTD6-DESCR-SUPPORT  PIC  9(0001).                 00124000
      * NUM�RO SUPPORT FOURNISSEUR                                      00125000
           05      RRLSTD6-NSUPPORT-FOURN PIC  X(0010).                 00126000
      * CONDITIONNEMENT DE STOCKAGE                                     00126100
           05      RRLSTD6-COND-STOCKAGE  PIC  X(0005).                 00126200
      * ZONE DE STOCKAGE                                                00126300
           05      RRLSTD6-ZONE-STOCKAGE  PIC  9(0003).                 00126400
      * CLASSE HAUTEUR                                                  00126500
           05      RRLSTD6-CLASSE-HAUTEUR PIC  9(0003).                 00126600
      * TYPE DE SUPPORT                                                 00126700
           05      RRLSTD6-TYPE-SUPPORT   PIC  9(0002).                 00126800
      * POIDS MAXIMUM                                                   00126900
           05      RRLSTD6-POIDS-MAXIMUM  PIC  9(0009).                 00127000
      * DATE DE LIVRAISON PR�VUE                                        00127100
           05      RRLSTD6-DLIVRAISON     PIC  X(0008).                 00127200
      * DATE DE COMMANDE                                                00127300
           05      RRLSTD6-DCDE           PIC  X(0008).                 00127400
      * DATE DE FABRICATION                                             00127500
           05      RRLSTD6-DFABRICATION   PIC  X(0008).                 00127600
      * DATE DE PEREMPTION                                              00127700
           05      RRLSTD6-DPEREMPTION    PIC  X(0008).                 00127800
      * COMMENTAIRE                                                     00127900
           05      RRLSTD6-COMMENTAIRE    PIC  X(0035).                 00128000
      * CARACT�RISTIQUE 1                                               00128100
           05      RRLSTD6-CARACT1        PIC  X(0020).                 00128200
      * CARACT�RISTIQUE 2                                               00128300
           05      RRLSTD6-CARACT2        PIC  X(0020).                 00128400
      * CARACT�RISTIQUE 3                                               00128500
           05      RRLSTD6-CARACT3        PIC  X(0020).                 00128600
      * FILLER 30                                                       00128700
           05      RRLSTD6-FILLER         PIC  X(0030).                 00128800
      * FIN                                                             00128900
           05      RRLSTD6-FIN            PIC  X(0001).                 00129000
                                                                                
