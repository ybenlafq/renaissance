      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC001                            *         
      *****************************************************************         
      *--> LONGUEUR 66 621.                                                     
       01  MEC001C-COMMAREA.                                                    
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           05  MEC001C-DATA-ENTREE.                                             
               10  MEC001C-MESSAGE-XML             PIC  X(65536).               
               10  MEC001C-MESSAGE-XML-L           PIC  9(05).                  
               10  MEC001C-PARAMETRES-MQ.                                       
                   15  MEC001C-PARAMETRES-MQ-OBLIGA.                            
                       20  MEC001C-NOM-PG-APPELANT PIC  X(06).                  
                       20  MEC001C-CODE-FONCTION   PIC  X(03).                  
                       20  MEC001C-TYPE-ENVOI-MQ   PIC  X(01).                  
                           88  MEC001C-TYPE-ENVOI-MQ-UNIQUE VALUE '1'.          
                           88  MEC001C-TYPE-ENVOI-MQ-MULTI  VALUE '2'.          
                   15  MEC001C-PARAMETRES-MQ-OPTION.                            
                       20  MEC001C-ALIAS           PIC  X(18).                  
                       20  MEC001C-MSGID           PIC  X(24).                  
                       20  MEC001C-DUREE           PIC S9(05).                  
                       20  MEC001C-PERSISTANCE     PIC  X(01).                  
                       20  MEC001C-PRIORITE        PIC S9(01).                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20  MEC001C-SYNCPOINT       PIC S9(09) BINARY.           
      *--                                                                       
                       20  MEC001C-SYNCPOINT       PIC S9(09) COMP-5.           
      *}                                                                        
      *--> DONNEES RESULTANTES.                                                 
           05  MEC001C-DATA-SORTIE.                                             
               10  MEC001C-CODE-RETOUR             PIC  X(01).                  
                   88  MEC001C-CDRET-OK                 VALUE '0'.              
                   88  MEC001C-CDRET-ERR-NON-BLQ        VALUE '1'.              
                   88  MEC001C-CDRET-ERR-DB2-NON-BLQ    VALUE '2'.              
                   88  MEC001C-CDRET-ERR-BLQ            VALUE '8'.              
                   88  MEC001C-CDRET-ERR-DB2-BLQ        VALUE '9'.              
               10  MEC001C-CODE-DB2                PIC -Z(07)9.                 
               10  MEC001C-CODE-DB2-DISPLAY        PIC  X(08).                  
               10  MEC001C-MESSAGE                 PIC  X(500).                 
           05  FILLER                              PIC  X(499).                 
      ********* FIN DE LA ZONE DE COMMUNICATION DU MEC001 *************         
                                                                                
