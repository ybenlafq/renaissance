      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      * CETTE COPY EST UN PROGRAMME ENTIER. ELLE EST UTILISEE                   
      * PAR TOUS LES PROGRAMMES MEC64X, OU X EST LE SUFFIXE FILIALE             
      * CE PROGRAMME GERE LES VENTES POUR DARTY.COM (STOCK, MUTATION)           
      ******************************************************************        
      *                                                                *        
      *  PROJET     : TRAITEMENT DES EXPEDIES                          *        
      *  PROGRAMME  : MEC64                                            *        
      *  TITRE      :                                                  *        
      *  CREATION   : MARS 2013                                        *        
      *  FONCTION   : MODULE APPELE PAR LES PROGRAMMES GV, EC, GR, ... *        
      ******************************************************************        
      * CE PROGRAMME EST APPELE  AVEC UN NUMERO DE VENTE EN PARAMETRE, *        
      * LE TRAITEMENT COMMENCE PAR ALLER CHERCHER L'�TAT ACTUEL DE LA  *        
      * VENTE (INCOMPLETE OU PAS) ET RAM_NE TOUS LES ARTICLES EXP�DI�S *        
      * POUR TRAITEMENT.                                               *        
      * TYPES DE TRAITEMENT :                                          *        
      *   MODE CONTROLE : ON APPEL LE MGV65 EN MODE CONTROLE POUR AVOIR*        
      *                   L'�TAT DE CHAQUE LIGNE ET D�TERMINER SI LA   *        
      *                   VENTE EST COMPLETE OU PAS                    *        
      *                                                                *        
      *  MODE CONTROLE MAJ : C'EST LA MEME CHOSE QUE LE MODE CONTROLE  *        
      *                      AVEC UN PAVE SUPPL�MENTAIRE QUI EST       *        
      *                      CHARGER D'ENVOYE UNE RESERVATION SI LA    *        
      *                      VENTE EST TOUJOURS INCOMPLETE MAIS QU'UN  *        
      *                      OU PLUSIEURS DES ARTICLES DE LA VENTE QUI *        
      *                      ETAIT INDISPONIBLE EST DEVENU DISPONIBLE  *        
      *                                                                *        
      *   mode Valid-date : la vente est d�sormais complete, on cree   *        
      *                     la MUT finale et on met a jour la vente    *        
      *                                                                *        
      *                                                                *        
      *   mode MAJ-ddeliv : la vente est toujours incomplete, la       *        
      *                     ddeliv est passee, on recule la date de    *        
      *                     reservation DACEM                          *        
      *                                                                *        
      *   mode SET-incomplete : la vente est incomplete, on cree la    *        
      *                     SL20 ET ON POSITIONNE LES STATUTS          *        
      *                                                                *        
      *   mode Action-sortie :  suite a modification GV, la vente      *        
      *                     N'EST PLUS INCOMPLETE, ON SUPPRIME SL20    *        
      *                                                                *        
      ******************************************************************        
CEN012* 21/11/13   ! CORRECTION ANOMALIES DE PRODUCTION                *        
      ******************************************************************        
ASI   * 30/12/13   ! AFFECTATION DES CAMPAGNES ASILAGE AUX VENTES      *        
      ******************************************************************        
THEMIS* 13/01/14   ! THEMIS : PLUS DE GENERIX - 907 125                *        
      ******************************************************************        
RESAJ * 20/02/14   ! PARAMETRER LES MUTS A J OU A DATE DANS EXPDEL     *        
      ******************************************************************        
26865 * 24/03/14   ! Bug affectation aux campagnes mantis 26865        *        
      ******************************************************************        
PBGB15* 14/04/14   ! CORRECTION ALIMENTATION DE LA GB15                *        
      ******************************************************************        
LA1807* 18/07/14   ! CORRECTION TRAITEMENT VALIDATION D'UNE VENTE      *        
      *            ! ET SUPPRESSION DES MODULES DACEM                  *        
      ******************************************************************        
DA0909* 09/09/14   ! CORRECTION SUR CHOIX MONO/MULTI ASILAGE           *        
      *            ! JIRA DCOM-247                                     *        
      ******************************************************************        
LA0611* 06/11/14   ! EVOLUTION : MULTI CEN                             *        
      ******************************************************************        
FT2910* 29/10/15   ! ARRETER DE REMUTER A CHAQUE F5 DE GV              *        
      ******************************************************************        
      *                                                                         
       01  FILLER                    PIC X(16) VALUE '*** WORKING  ***'.        
      *                                                                         
       01 WS-GV23NSEQ                PIC 99 VALUE 0.                            
       01  MSG-SQL.                                                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 MSG-SQL-LENGTH       PIC S9(4) COMP VALUE +240.                   
      *--                                                                       
           02 MSG-SQL-LENGTH       PIC S9(4) COMP-5 VALUE +240.                 
      *}                                                                        
           02 MSG-SQL-MSG          PIC X(80) OCCURS 3.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  MSG-SQL-LRECL           PIC S9(8) COMP VALUE +80.                    
      *--                                                                       
       01  MSG-SQL-LRECL           PIC S9(8) COMP-5 VALUE +80.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  I-SQL                   PIC S9(4) BINARY.                            
      *--                                                                       
       01  I-SQL                   PIC S9(4) COMP-5.                            
      *}                                                                        
       01  DSNTIAR                 PIC X(8)  VALUE 'DSNTIAR'.                   
      *                                                                         
       01  TOP-DISPLAY               PIC X(01) VALUE 'N'.                       
      *{ remove-comma-in-dde 1.5                                                
      *    88 AUTO-DISPLAY                     VALUE 'O',                       
      *                                              'T'.                       
      *--                                                                       
           88 AUTO-DISPLAY                     VALUE 'O'                        
                                                     'T'.                       
      *}                                                                        
           88 AUTO-DISPLAY-TEST                VALUE 'T'.                       
           88 PAS-DE-DISPLAY                   VALUE 'N'.                       
       01  TOP-LCOMMENT              PIC X(01) VALUE 'N'.                       
           88 LCOMMENT-VALIDE                  VALUE 'O'.                       
           88 LCOMMENT-INVALIDE                VALUE 'N'.                       
       01  TOP-PARAM-NECEN           PIC X(01).                                 
           88 PARAM-NECEN-TROUVE               VALUE 'O'.                       
           88 PARAM-NECEN-NON-TROUVE           VALUE 'N'.                       
       01  WS-TOP-CREAT-SL21          PIC 9(01)            VALUE 0.             
           88 CREATION-RTSL21                              VALUE 0.             
           88 PAS-CREATION-RTSL21                          VALUE 1.             
       01  WS-TRT-CODIC-ENC           PIC 9(01).                                
           88 CODIC-TRT-OK                                 VALUE 0.             
           88 CODIC-TRT-KO                                 VALUE 1.             
       01  WS-COMMANDE-PRECO          PIC 9(01).                                
           88 PAS-COMMANDE-PRECO                           VALUE 0.             
           88 COMMANDE-PRECO                               VALUE 1.             
       01  WF-TOP-GV99                PIC 9(01).                                
           88 GV99-PRECO-OK                                VALUE 0.             
           88 GV99-PRECO-KO                                VALUE 1.             
       01  WF-TOP-JPLUS15             PIC 9(01).                                
           88 JPLUS15-OK                                   VALUE 0.             
           88 JPLUS15-KO                                   VALUE 1.             
       01  W-VTE-MONO                PIC  X(01).                                
       01  W-VTE-MULTI               PIC  X(01).                                
       01  W-PROD-RETARD             PIC  X(01).                                
       01  W-PROD-PAS-RETARD         PIC  X(01).                                
       01  W-ENTETE-MUT              PIC  X(01).                                
       01  WS-MUT-SL300              PIC  X(01).                                
       01  WS-MUT-A-J                PIC  X(01).                                
       01  W-NBLIGNENS               PIC S9(05) COMP-3.                         
       01  W-DELAI                   PIC  9(02).                                
       01  EDT-NSEQ                  PIC  9(05).                                
       01  EDT-SQL                   PIC -999999.                               
       01  W-QTE                     PIC S9(03) COMP-3.                         
       01  W-SL20-STATUT             PIC X(02).                                 
       01  W-TOP-CONTROL             PIC X(01).                                 
       01  WDATHEUR-ALPHA             PIC X(13).                                
       01  WNAUTORD-NUM               PIC 9(07).                                
       01 W-NSEQNQ-9                      PIC 9(05).                            
       01 W-NSEQNQ-X REDEFINES W-NSEQNQ-9 PIC X(05).                            
      *                                                                         
       01  TOP-TRAITEMENT            PIC X(01).                                 
           88 TRAITEMENT-OK                    VALUE '1'.                       
           88 TRAITEMENT-KO                    VALUE '0'.                       
      *                                                                         
       01  TOP-MULTIDA.                                                         
           05 W-CONTIENT-DACEM  PIC X(01) VALUE '0'.                            
           05 W-CONTIENT-DARTY  PIC X(01) VALUE '0'.                            
      *                                                                         
       01  TOP-MUTATION            PIC X(01).                                   
           88 MUTATION-FINAL                   VALUE '1'.                       
           88 MUTATION-TEMPO                   VALUE '0'.                       
      *                                                                         
       01  TOP-VALID-MUT           PIC X(01).                                   
           88 MUT-VALIDE                       VALUE '1'.                       
           88 MUT-NON-VALIDE                   VALUE '0'.                       
      *                                                                         
       01  TOP-ERREUR-MEC64          PIC X(4)        VALUE '0000'.              
           88 MEC64-PAS-ERREUR                       VALUE '0000'.              
           88 MEC64-MFL05                            VALUE 'FL05'.              
           88 MEC64-DAC-PB-MQ                        VALUE 'MQXX'.              
           88 MEC64-MDELIV-NT                        VALUE '6502'.              
           88 MEC64-MUT-NT                           VALUE '6505'.              
      *{ remove-comma-in-dde 1.5                                                
      *    88 MEC64-DAC-PAS-DISPO                    VALUE '6804',              
      *                                                    '0004'.              
      *--                                                                       
           88 MEC64-DAC-PAS-DISPO                    VALUE '6804'               
                                                           '0004'.              
      *}                                                                        
           88 MEC64-DAC-CLIENT-INVAL                 VALUE '6805'.              
           88 MEC64-DAC-CRIT-INVAL                   VALUE '6806'.              
           88 MEC64-DAC-LIEU-INVAL                   VALUE '6807'.              
      *{ remove-comma-in-dde 1.5                                                
      *    88 MEC64-GA00-NT                          VALUE '6705',              
      *                                                    '6808',              
      *                                                    '6905'.              
      *--                                                                       
           88 MEC64-GA00-NT                          VALUE '6705'               
                                                           '6808'               
                                                           '6905'.              
      *}                                                                        
           88 MEC64-PB-INSERT-GS10                   VALUE '6706'.              
           88 MEC64-PB-GB05                          VALUE '6903'.              
      *{ remove-comma-in-dde 1.5                                                
      *    88 MEC64-PB-MAJ-GB05                      VALUE '6707',              
      *                                                    '6906'.              
      *--                                                                       
           88 MEC64-PB-MAJ-GB05                      VALUE '6707'               
                                                           '6906'.              
      *}                                                                        
           88 MEC64-DAC-CLIENT-NT                    VALUE '6803'.              
           88 MEC64-MUT-DATE-DIF                     VALUE '6904'.              
      *{ remove-comma-in-dde 1.5                                                
      *    88 MEC64-DDELIV-DEP                       VALUE '6802',              
      *                                                    '6524'.              
      *--                                                                       
           88 MEC64-DDELIV-DEP                       VALUE '6802'               
      *    EN CAS D'UTILISATION ANCIENS MESSAGES (CONVERTIGO)                   
                                                           '6524'.              
      *}                                                                        
           88 MEC64-PB-STOCK                         VALUE '6704'.              
           88 MEC64-CF-DATE                          VALUE '6702'.              
           88 MEC64-CF-QTE                           VALUE '6703'.              
      *                                                                         
       01  TOP-ERREUR                PIC XX                VALUE '00'.          
           88 PAS-ERREUR                       VALUE '00'.                      
           88 ERREUR-PUT-OPEN                  VALUE '01'.                      
           88 ERREUR-PUT-PUT                   VALUE '02'.                      
           88 ERREUR-MQPUT-NON-T               VALUE '03'.                      
           88 ERREUR-MQPUT-INF-0               VALUE '04'.                      
           88 ERREUR-PUT-CLOSE                 VALUE '05'.                      
           88 ERREUR-GET-OPEN                  VALUE '06'.                      
           88 ERREUR-GET-GET                   VALUE '07'.                      
           88 ERREUR-MQGET-NON-T               VALUE '08'.                      
           88 ERREUR-MQGET-INF-0               VALUE '09'.                      
           88 ERREUR-GET-CLOSE                 VALUE '10'.                      
      *                                                                         
      * ZONES DE TRAVAIL UTILISEES POUR GESTION ASILAGE                         
ASI    01 I-AS    PIC S9(05) COMP-3 VALUE 0.                                    
 "     01 WS-DESC PIC X(30).                                                    
 "     01 WS-TAB-DESC   REDEFINES WS-DESC.                                      
 "         05 WS-TAB-X    OCCURS 6.                                             
 "           10 DESC          PIC X(4).                                         
 "           10 FILLER        PIC X(1).                                         
 "    *                                                                         
 "     01 WF-CAS00             PIC  X(01).                                      
 "        88 WC-CAS00-SUITE            VALUE '0'.                               
 "        88 WC-CAS00-FIN              VALUE '1'.                               
 "     01 OLD-NSOCIETE   PIC X(003).                                            
 "     01 OLD-NLIEU      PIC X(003).                                            
 "     01 OLD-NVENTE     PIC X(007).                                            
ASI    01 OLD-CCAMPAGNE  PIC X(004).                                            
      * ZONES DE TRAVAIL UTILISEES POUR LA GESTION DE LA TS TSTENVOI            
       01  WS-PROG  PIC X(8) VALUE SPACES.                                      
       01  I                         PIC S9(05) COMP-3 VALUE 0.                 
       01  I-NL                      PIC S9(02) COMP-3 VALUE 0.                 
       01  I-NL-MAX                  PIC S9(02) COMP-3 VALUE 0.                 
       01  edit-nb-prod              PIC 99999.                                 
       01  WS-NB-PRODUIT             PIC S9(05) COMP-3 VALUE 0.                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-NB-PRODUIT-F           PIC S9(04) COMP-4     VALUE ZEROES.        
      *--                                                                       
       01  WS-NB-PRODUIT-F           PIC S9(04) COMP-5     VALUE ZEROES.        
      *}                                                                        
       01  W-DACEM-TRT               PIC  9(05).                                
       01  W-DARTY90-TRT             PIC  9(05).                                
       01  W-DARTY95-TRT             PIC  9(05).                                
       01  WS-LCOMMENT               PIC X(35) VALUE SPACES.                    
       01  DIS-SQLCODE               PIC -ZZ9.                                  
       01  W-FILIERE                 PIC X(02).                                 
       01  IND                        PIC S9(05) COMP-3 VALUE 0.                
       01  WN-NB-TOPE                 PIC S9(02) COMP-3 VALUE 0.                
       01  W-NB-LIGNE-SL21            PIC  9(02)           VALUE ZEROES.        
       01  W-DATE-J-PLUS1             PIC  X(08).                               
       01  W-DATE-J-PLUS15            PIC  X(08).                               
       01  W-APPEL-DACEM              PIC  X.                                   
           88 APPEL-DACEM                  VALUE 'O'.                           
           88 PAS-APPEL-DACEM              VALUE 'N'.                           
       01 P-EIBTASKN PIC S9(7) PACKED-DECIMAL.                                  
       01 X-EIBTASKN REDEFINES P-EIBTASKN PIC X(4).                             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  IDEP                       PIC S9(04) COMP      VALUE ZEROES.        
      *--                                                                       
       01  IDEP                       PIC S9(04) COMP-5      VALUE              
                                                                 ZEROES.        
      *}                                                                        
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.      
LA0611 01  WS-MDLIV-CMODDEL.                                                    
LA0611     05 FILLER            PIC X(02) VALUE 'EM'.                           
LA0611     05 WS-MDLIV-WEMPORTE PIC X(01).                                      
       EXEC SQL END DECLARE SECTION END-EXEC.
      *                                                                         
      * -> PARAMETRE MDLIV                                                      
       01  TOP-MDLIV                 PIC 9(01) VALUE 0.                         
           88 MDLIV-NON-TROUVE                 VALUE 0.                         
           88 MDLIV-TROUVE                     VALUE 1.                         
       01 WS-WDONNEES.                                                          
          05 FILLER                  PIC X(01).                                 
          05 MDLIV-CTYPTRAIT         PIC X(05).                                 
          05 MDLIV-CORIGLIEU         PIC X(05).                                 
          05 MDLIV-GEST              PIC X(06).                                 
          05 MDLIV-DEPLIV            PIC X(06).                                 
          05 MDLIV-WEXP              PIC X(01).                                 
          05 MDLIV-LDEPLIVR-EXP.                                                
            10 MDLIV-SOCIETE           PIC X(03).                               
            10 MDLIV-LIEU              PIC X(03).                               
          05 MDLIV-FILLER            PIC X(03).                                 
      *                                                                         
        01 ETAT-CRSGV11              PIC 9(01) VALUE 1.                         
           88 FIN-CRSGV11                      VALUE 0.                         
           88 CRSGV11-OK                       VALUE 1.                         
      *                                                                         
      * ZONES DE TRAVAIL UTILISEES POUR LA GESTION DE LA TS TSTNECEN            
       77 WC-ID-TSTNECEN          PIC  X(08)      VALUE 'TSTNECEN'.             
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WP-RANG-TSTNECEN        PIC S9(04) COMP VALUE +0.                     
      *--                                                                       
       77 WP-RANG-TSTNECEN        PIC S9(04) COMP-5 VALUE +0.                   
      *}                                                                        
       77 WS-TSTNECEN-NOMPGM      PIC  X(10) VALUE 'MEC64'.                     
      *                                                                         
       01 WF-TSTNECEN             PIC X.                                        
          88 WC-TSTNECEN-SUITE           VALUE '0'.                             
          88 WC-TSTNECEN-FIN             VALUE '1'.                             
          88 WC-TSTNECEN-ERREUR          VALUE '2'.                             
       01 WF-XCTRL-NECEN          PIC X.                                        
          88 WC-XCTRL-NECEN-TROUVE       VALUE '0'.                             
          88 WC-XCTRL-NECEN-FIN          VALUE '1'.                             
      * DESCRIPTION DE LA TS.                                                   
       01 TS-TSTNECEN-DONNEES.                                                  
          05 TSTNECEN-NSOCZP                       PIC X(05).                   
          05 TSTNECEN-TRANS                        PIC X(05).                   
          05 TSTNECEN-WFLAG                        PIC X(01).                   
          05 TSTNECEN-WPARAM                       PIC X(10).                   
          05 TSTNECEN-LIBELLE                      PIC X(30).                   
      *                                                                         
THEMIS* ENTREPOTS NATIONAUX POSSIBLES (5)                                       
      * DANS L'ORDRE DE PRIORITE                                                
       01 W-NECEN-ENTR.                                                         
          05 W-ENTR1                               PIC X(06).                   
          05 W-QTE-ENTR1                           PIC S9(6) COMP-3.            
          05 W-NMUT1                               PIC X(7).                    
          05 W-ENTR2                               PIC X(06).                   
          05 W-QTE-ENTR2                           PIC S9(6) COMP-3.            
          05 W-NMUT2                               PIC X(7).                    
          05 W-ENTR3                               PIC X(06).                   
          05 W-QTE-ENTR3                           PIC S9(6) COMP-3.            
          05 W-NMUT3                               PIC X(7).                    
          05 W-ENTR4                               PIC X(06).                   
          05 W-QTE-ENTR4                           PIC S9(6) COMP-3.            
          05 W-NMUT4                               PIC X(7).                    
          05 W-ENTR5                               PIC X(06).                   
          05 W-QTE-ENTR5                           PIC S9(6) COMP-3.            
          05 W-NMUT5                               PIC X(7).                    
      *                                                                         
THEMIS 01 W-THEMIS                              PIC X(01).                      
FT2710 01 W-ENC-GV-F5                           PIC X(01).                      
THEMIS 01 TALLY-CPT                             PIC 9(5).                       
      *                                                                         
THEMIS 01  TOP-MULTIENTR.                                                       
           05 W-CONTIENT-ENTR1  PIC X(01) VALUE '0'.                            
           05 W-CONTIENT-ENTR2  PIC X(01) VALUE '0'.                            
           05 W-CONTIENT-ENTR3  PIC X(01) VALUE '0'.                            
           05 W-CONTIENT-ENTR4  PIC X(01) VALUE '0'.                            
           05 W-CONTIENT-ENTR5  PIC X(01) VALUE '0'.                            
      *                                                                         
      * ZONE POUR AFFICHAGE DANS LA LOG CICS OU DANS L'�CRAN TP                 
       01  W-MESSAGE-S.                                                         
           02  FILLER      PIC X(5) VALUE 'MEC64'.                              
           02  W-MESSAGE-H PIC X(2).                                            
           02  FILLER      PIC X(1) VALUE ':'.                                  
           02  W-MESSAGE-M PIC X(2).                                            
           02  FILLER      PIC X(1) VALUE ' '.                                  
           02  W-MESSAGE   PIC X(69).                                           
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  W-MESS-EC05     pic x(80).                                           
       EXEC SQL END DECLARE SECTION END-EXEC.
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  W-SPDATDB2-RC                 PIC S9(4)  COMP.                       
      *--                                                                       
       01  W-SPDATDB2-RC                 PIC S9(4) COMP-5.                      
      *}                                                                        
       01  W-SPDATDB2-DSYST              PIC S9(13) COMP-3 VALUE ZERO.          
       01  W-EIBTIME        PIC 9(7).                                           
       01  FILLER REDEFINES W-EIBTIME.                                          
           02  FILLER    PIC X.                                                 
           02  W-HEURE   PIC XX.                                                
           02  W-MINUTE  PIC XX.                                                
           02  W-SECONDE PIC XX.                                                
      *                                                                         
       77  WS-LONG-MESSAGE   PIC S9(03) VALUE +134.                             
      *                                                                         
       01  FILLER                    PIC X(16) VALUE '*** COMMAREA ***'.        
       01  Z-COMMAREA-LINK           PIC X(60000).                              
           COPY COMMDATC.                                                       
           COPY COMMMQ21.                                                       
           COPY COMMEC64.                                                       
           COPY COMMEC67.                                                       
LA1807*    COPY COMMEC68.                                                       
           COPY COMMEC69.                                                       
           COPY MEC65C.                                                         
           COPY COMMPTMG.                                                       
       01  FILLER                    PIC X(16) VALUE '*** AIDA ***'.            
           COPY SYKWDIV0.                                                       
           COPY SYKWSQ10.                                                       
           COPY SYKWERRO.                                                       
           COPY SYKWEIB0.                                                       
           COPY SYKWECRA.                                                       
           COPY ZLIBERRG.                                                       
           COPY SYKWZINO.                                                       
       01  FILLER                PIC X(16) VALUE '* TS MFL05     *'.            
           COPY TSSL05.                                                         
       01  FILLER                PIC X(16) VALUE '* COMM MFL05   *'.            
           COPY COMMML05.                                                       
      *                                                                         
       LINKAGE SECTION.                                                         
       01  DFHCOMMAREA.                                                         
           05 FILLER                PIC X(11350).                               
       PROCEDURE DIVISION.                                                      
      *********************                                                     
      ******************************************************************        
      *                      T R A I T E M E N T                       *        
      ******************************************************************        
       MODULE-MEC64 SECTION.                                                    
      ***********************                                                   
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT                                            
           PERFORM MODULE-SORTIE.                                               
       FIN-MODULE-MEC64. EXIT.                                                  
      ************************                                                  
       MODULE-ENTREE SECTION.                                                   
      ************************                                                  
      * RECUPERATION DONNEES DE LA COMMAREA TRANSMISE PAR MQSERIES              
           MOVE DFHCOMMAREA                    TO COMM-MEC64-APPLI.             
           MOVE 'N' TO EC64-VENTE-CQE                                           
           MOVE '0000'  TO COMM-MEC64-CODE-RETOUR.                              
           PERFORM CALL-SPDATDB2.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-MESSAGE-H.                             
           MOVE W-MINUTE            TO W-MESSAGE-M.                             
           MOVE 'MEC64     ' TO WS-TSTENVOI-NOMPGM                              
           PERFORM GESTION-TSTENVOI.                                            
           IF WC-TSTENVOI-ERREUR                                                
           IF AUTO-DISPLAY                                                      
              MOVE 'TSTENVOI INVALIDE ' TO W-MESSAGE                            
              PERFORM DISPLAY-LOG                                               
           END-IF                                                               
              MOVE 'TSTENVOI INVALIDE ' TO COMM-MEC64-MESSAGE                   
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              SET TRAITEMENT-KO TO TRUE                                         
              PERFORM MODULE-SORTIE                                             
           END-IF                                                               
           PERFORM GESTION-TSTNECEN                                             
           PERFORM OBTENIR-PARAMETRES                                           
LA0611*    PERFORM CHERCHE-MDLIV-EMX                                            
           .                                                                    
       FIN-MODULE-ENTREE. EXIT.                                                 
      *************************                                                 
       MODULE-TRAITEMENT SECTION.                                               
      ****************************                                              
      * -> LECTURE DE LA VENTE, ETUDE DE SES LIGNES DE VENTE                    
      *    ET RECHERCHE DE SON ETAT PRECEDENT.                                  
           IF (NOT COMM-MEC64-ACTION-RESERVATION)                               
           AND (NOT COMM-MEC64-ACTION-TRT-CODIC)                                
              PERFORM RECHERCHE-VENTE                                           
           END-IF                                                               
LA0611     IF COMM-MEC64-TAB-WEMPORTE (1) NOT = SPACE AND LOW-VALUE             
LA0611        MOVE COMM-MEC64-TAB-WEMPORTE (1)  TO WS-MDLIV-WEMPORTE            
LA0611     ELSE                                                                 
LA0611        PERFORM SELECT-GV11-CMODDEL                                       
LA0611     END-IF                                                               
LA0611     PERFORM CHERCHE-MDLIV-CMODDEL                                        
           MOVE ZEROES TO EC64-NB-INDISPO                                       
           IF COMM-MEC64-TYPE-NB-MULTI                                          
              MOVE 'O' TO W-TOP-CONTROL                                         
           ELSE                                                                 
              MOVE 'N' TO W-TOP-CONTROL                                         
           END-IF                                                               
           EVALUATE TRUE                                                        
               WHEN COMM-MEC64-ACTION-CONTROLE                                  
      * -> RECHERCHE DE CE QUE PEUT DEVENIR LA VENTE                            
      *    ETUDE DES DISPOS PRODUITS RAPATRIEMENT DES DATES POSSIBLES           
      *    D'ACHEMINEMENT.                                                      
                    PERFORM ACTION-CONTROLE                                     
      * MODULE DE MISE � JOUR D'UNE VENTE SUITE AU CONSTAT QUE LA VENTE         
      * EST DESORMAIS COMPLETE                                                  
               WHEN COMM-MEC64-ACTION-VALID-DATE                                
                    PERFORM ACTION-VALID-DATE                                   
               WHEN COMM-MEC64-ACTION-VALID-CODIC                               
                    PERFORM ACTION-VALID-CODIC                                  
      * MODULE DE MISE � JOUR DE LA DATE DE RESERVATION CHEZ DACEM              
      * ET REPOUSSER LA MUTATION DARTY                                          
LA1807*        WHEN COMM-MEC64-ACTION-MAJ-DDELIV                                
LA1807*             PERFORM ACTION-MAJ-DDELIV                                   
      * MODULE DE SUPPRESSION DE LA DATE DE RESERVATION CHEZ DACEM              
LA1807*        WHEN COMM-MEC64-ACTION-DEL-DDELIV                                
LA1807*             PERFORM ACTION-DEL-DDELIV                                   
      * MODULE DE RESERVATION (EC00 OU GV00)                                    
               WHEN COMM-MEC64-ACTION-RESERVATION                               
                    PERFORM TRAITEMENT-RESA                                     
               WHEN COMM-MEC64-ACTION-ENCAISSEMENT                              
                    PERFORM ACTION-ENCAISSEMENT                                 
               WHEN COMM-MEC64-ACTION-VALID-EC06                                
                    PERFORM ACTION-VALID-EC06                                   
               WHEN COMM-MEC64-ACTION-CONTROLE-GV                               
                    PERFORM ACTION-CONTROLE-GV                                  
               WHEN COMM-MEC64-ACTION-VALIDE-GV                                 
                    PERFORM ACTION-VALIDE-GV                                    
      * MODULE DE MAJ DE LA VENTE POUR LA SORTIR DES VENTES INCOMPLETES         
      * SUITE � UNE MISE � JOUR DANS GV00                                       
               WHEN COMM-MEC64-ACTION-SORTIE                                    
                    PERFORM ACTION-SORTIE                                       
           END-EVALUATE                                                         
           .                                                                    
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      ***************************                                               
       ACTION-ASILAGE   SECTION.                                                
      ***************************                                               
      * INIT DES ZONES DE TRAVAIL                                               
           MOVE SPACES TO WS-DESC.                                              
           MOVE 0 TO I-AS.                                                      
           PERFORM DECLARE-RTGV11-AS00                                          
           PERFORM FETCH-RTGV11-AS00                                            
           PERFORM UNTIL WC-CAS00-FIN                                   01820001
             IF ( GV10-NSOCIETE NOT = OLD-NSOCIETE                      01830001
                OR GV10-NLIEU NOT = OLD-NLIEU                           01840001
                OR GV10-NVENTE NOT = OLD-NVENTE                         01850001
                OR AS00-CCAMPAGNE NOT = OLD-CCAMPAGNE )                 01860001
                AND OLD-NSOCIETE > ' '                                  01870001
                   ADD 1 TO I-AS                                        01980001
                   MOVE OLD-CCAMPAGNE TO DESC(I-AS)                     01990001
                   IF GV10-NSOCIETE NOT = OLD-NSOCIETE                  02000001
                     OR GV10-NLIEU NOT = OLD-NLIEU                      02010001
                     OR GV10-NVENTE NOT = OLD-NVENTE                    02020001
                   PERFORM UPDATE-RTGV10                                02130001
                   MOVE 0 TO I-AS                                       02150001
                   MOVE SPACES TO WS-DESC                               02160001
                   END-IF                                               02170001
                   MOVE GV10-NSOCIETE     TO OLD-NSOCIETE               02180001
                   MOVE GV10-NLIEU        TO OLD-NLIEU                  02190001
                   MOVE GV10-NVENTE       TO OLD-NVENTE                 02200001
                   MOVE AS00-CCAMPAGNE    TO OLD-CCAMPAGNE              02210001
             ELSE                                                       02220001
                   IF OLD-NSOCIETE > ' '                                02230001
                      ADD 1 TO I-AS                                     02240001
                      MOVE OLD-CCAMPAGNE TO DESC(I-AS)                  02250001
                   END-IF                                               02260001
                   MOVE GV10-NSOCIETE     TO OLD-NSOCIETE               02270001
                   MOVE GV10-NLIEU        TO OLD-NLIEU                  02280001
                   MOVE GV10-NVENTE       TO OLD-NVENTE                 02290001
                   MOVE AS00-CCAMPAGNE    TO OLD-CCAMPAGNE              02300001
             END-IF                                                     02310001
             PERFORM FETCH-RTGV11-AS00                                  02320001
           END-PERFORM                                                  02330001
           IF ( GV10-NSOCIETE     = OLD-NSOCIETE                        02340001
            AND GV10-NLIEU     = OLD-NLIEU                              02350001
            AND GV10-NVENTE     = OLD-NVENTE )                          02360001
            AND OLD-NSOCIETE > ' '                                      02370001
               ADD 1 TO I-AS                                            02390001
               MOVE AS00-CCAMPAGNE TO DESC(I-AS)                        02400001
               PERFORM UPDATE-RTGV10                                    02510001
           END-IF.                                                      02530001
           PERFORM CLOSE-RTGV11-AS00.                                           
       FIN-ACTION-ASILAGE.  EXIT.                                               
       RECHERCHE-VENTE  SECTION.                                                
      ***************************                                               
           SET EC64-VTE-SANS-CQE-NEW   TO TRUE                                  
           SET EC64-STATUT-NON-TRAITE  TO TRUE                                  
           SET COMM-MEC64-TYPE-NB-MONO TO TRUE                                  
           SET PAS-COMMANDE-PRECO TO TRUE                                       
LA1807*    SET EC68-PAS-COMMANDE-PRECO TO TRUE                                  
           SET EC69-PAS-COMMANDE-PRECO TO TRUE                                  
           SET JPLUS15-KO              TO TRUE                                  
      * REMPLIT LE TABLEAU DES LIGNES DE LA VENTE (GV11 + GV22                  
           MOVE  0  TO I-NL-MAX                                                 
           MOVE '0' TO W-CONTIENT-DACEM                                         
           MOVE '0' TO W-CONTIENT-DARTY                                         
THEMIS     MOVE '0' TO W-CONTIENT-ENTR1                                         
                       W-CONTIENT-ENTR2                                         
                       W-CONTIENT-ENTR3                                         
                       W-CONTIENT-ENTR4                                         
                       W-CONTIENT-ENTR5                                         
THEMIS     MOVE 0   TO W-QTE-ENTR1                                              
                       W-QTE-ENTR2                                              
                       W-QTE-ENTR3                                              
                       W-QTE-ENTR4                                              
                       W-QTE-ENTR5                                              
           INITIALIZE COMM-MEC64-TABLEAU-NLIGNE.                                
           MOVE COMM-MEC64-DJOUR TO COMM-MEC64-DDELIV-COM                       
           PERFORM DECLARE-RTGV11                                               
           PERFORM FETCH-RTGV11                                                 
           PERFORM UNTIL NON-TROUVE                                             
      * ON NE TRAITE QUE LES VENTES EXPEDIEES (CODES PRESENTS DANS TS)          
            IF LCOMMENT-VALIDE                                                  
             SET WC-TSTENVOI-SUITE TO TRUE                                      
             PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                      
             UNTIL  WC-TSTENVOI-FIN                                             
               PERFORM LECTURE-TSTENVOI                                         
               IF  WC-TSTENVOI-SUITE                                            
               AND TSTENVOI-NSOCZP      = '00000'                               
               AND TSTENVOI-CTRL(1:1)   = 'A'                                   
               AND TSTENVOI-WPARAM(4:1) = GV11-WEMPORTE                         
                 ADD 1 TO I-NL-MAX                                              
                 MOVE GV11-NCODIC                                               
                   TO COMM-MEC64-TAB-NCODIC   (I-NL-MAX)                        
                 MOVE GV11-NSEQ                                                 
                   TO COMM-MEC64-TAB-NSEQ     (I-NL-MAX)                        
                 MOVE GV11-NSEQNQ                                               
                   TO COMM-MEC64-TAB-NSEQNQ   (I-NL-MAX)                        
                 MOVE GV11-CTYPENREG                                            
                   TO COMM-MEC64-TAB-CTYPENREG (I-NL-MAX)                       
                 MOVE GV11-NCODICGRP                                            
                   TO COMM-MEC64-TAB-NCODICGRP (I-NL-MAX)                       
                 MOVE GV11-LCOMMENT                                             
                   TO COMM-MEC64-TAB-LCOMMENT (I-NL-MAX)                        
                 MOVE GV11-LCOMMENT(1:5)                                        
                   TO COMM-MEC64-TAB-TYPECDE  (I-NL-MAX)                        
                 MOVE GV11-QVENDUE                                              
                   TO COMM-MEC64-TAB-QVENDUE  (I-NL-MAX)                        
                 MOVE GV11-DDELIV                                               
                   TO COMM-MEC64-TAB-DDELIVO   (I-NL-MAX)                       
                 MOVE GV11-WCQERESF                                             
                   TO COMM-MEC64-TAB-WCQERESF  (I-NL-MAX)                       
                 MOVE GV11-WEMPORTE                                             
                   TO COMM-MEC64-TAB-WEMPORTE (I-NL-MAX)                        
                 MOVE GV11-CEQUIPE                                              
                   TO COMM-MEC64-TAB-CEQUIPE (I-NL-MAX)                         
                 MOVE GV11-NSOCLIVR                                             
                   TO COMM-MEC64-TAB-NSOCLIVR (I-NL-MAX)                        
                 MOVE GV11-NDEPOT                                               
                   TO COMM-MEC64-TAB-NDEPOT  (I-NL-MAX)                         
                 MOVE GV22-CSTATUT                                              
                   TO COMM-MEC64-TAB-CSTATUT (I-NL-MAX)                         
                 ADD GV11-QVENDUE TO WS-NB-PRODUIT                              
      * L'ARTICLE EST EN COMMANDE FOURNISSEUR                                   
                 IF GV11-WCQERESF = 'F'                                         
                    SET EC64-STOCK-ATT-FOURN (I-NL-MAX) TO TRUE                 
                    SET EC64-ARTICLE-EN-CF   (I-NL-MAX) TO TRUE                 
                 END-IF                                                         
                 IF GV11-DTOPE NOT = SPACES AND LOW-VALUES                      
                    MOVE 'O' TO  COMM-MEC64-TAB-TOPE (I-NL-MAX)                 
                 END-IF                                                         
                 IF GV11-CEQUIPE = 'MGV64' OR 'MGV66' OR '66VGM'                
                    CONTINUE                                                    
                 ELSE                                                           
                    SET COMM-EC64-LIGNE-A-TRAITER TO TRUE                       
                 END-IF                                                         
      * L'ARTICLE EST DEJA MUTE AU CEN                                          
                 IF GV22-CSTATUT = 'M'                                          
                    SET EC64-DEJA-MUTE(I-NL-MAX) TO TRUE                        
                 END-IF                                                         
      * CERTAINES VENTE ONT LE LCOMMENT A SPACE, CE QUI CAUSE DES               
      * PROBLEMES, ON RENSEIGNE DONC CETTE ZONE SI ELLE NE L'EST PAS            
      *          TOUJOURS CHERCHER LE CAPPRO POUR MEC65 / CQE                   
                 MOVE GV11-NCODIC      TO GA00-NCODIC                           
                 PERFORM SELECT-RTGA00                                          
                 IF GV11-LCOMMENT(1:5)     = 'DARTY'                            
                    SET COMM-MEC64-CDEDARTY (I-NL-MAX) TO TRUE                  
LA1807*          ELSE                                                           
LA1807*           IF GV11-LCOMMENT(1:5)     = 'DACEM'                           
LA1807*             SET COMM-MEC64-CDEDACEM (I-NL-MAX) TO TRUE                  
LA1807*           ELSE                                                          
LA1807*             IF GA00-WDACEM = 'O'                                        
LA1807*                MOVE 'DACEM'  TO GV11-LCOMMENT (1:5)                     
LA1807*                SET COMM-MEC64-CDEDACEM(I-NL-MAX) TO TRUE                
LA1807*             ELSE                                                        
LA1807*                MOVE 'DARTY'  TO GV11-LCOMMENT (1:5)                     
LA1807*                SET COMM-MEC64-CDEDARTY(I-NL-MAX) TO TRUE                
LA1807*             END-IF                                                      
LA1807*           END-IF                                                        
                 END-IF                                                         
                 MOVE GA00-CFAM TO COMM-MEC64-TAB-CFAM(I-NL-MAX)                
      * IL EXISTE UNE RESERVATION                                               
                 IF GV22-CSTATUT = 'R'                                          
LA1807           AND  GV11-LCOMMENT(1:5) = 'DARTY'                              
LA1807*            IF GV11-LCOMMENT(1:5) = 'DACEM'                              
LA1807*               SET EC64-STOCK-DACEM(I-NL-MAX) TO TRUE                    
LA1807*            ELSE                                                         
                      SET EC64-STOCK-DISPONIBLE(I-NL-MAX) TO TRUE               
LA1807*            END-IF                                                       
LA1807*          ELSE                                                           
LA1807*             IF GV11-LCOMMENT(1:5) = 'DACEM'                             
LA1807*                 SET JPLUS15-OK      TO TRUE                             
LA1807*             END-IF                                                      
                 END-IF                                                         
      * POUR NE PAS APPELER DACEM LORS DE LA MIGRATION VERS GENERIX ET          
      * (DACEM SERA FERME), ON FAIT COMME SI LE PRODUIT ETAIT                   
      * EN COMMANDE FOURNISSEUR. CECI EST VALABLE POUR TOUS LES PRODUITS        
      * DACEM MEME CEUX QUI SONT RESERVES.                                      
LA1807*          IF PAS-APPEL-DACEM                                             
LA1807*            IF GV11-LCOMMENT(1:5) = 'DACEM'                              
LA1807*              SET EC64-DACEM-FERME(I-NL-MAX) TO TRUE                     
LA1807*            END-IF                                                       
LA1807*          END-IF                                                         
LA1807*          IF GV11-LCOMMENT(1:5) = 'DACEM'                                
LA1807*             MOVE '1' TO W-CONTIENT-DACEM                                
LA1807*          ELSE                                                           
LA1807           IF GV11-LCOMMENT(1:5) = 'DARTY'                                
LA1807              MOVE '1' TO W-CONTIENT-DARTY                                
LA1807           END-IF                                                         
THEMIS           IF W-THEMIS = 'O'                                              
THEMIS           AND COMM-MEC64-TAB-TOPE (I-NL-MAX) NOT = 'O'                   
THEMIS              EVALUATE GV11-LCOMMENT(13:6)                                
THEMIS              WHEN W-ENTR1                                                
THEMIS                 MOVE '1' TO W-CONTIENT-ENTR1                             
THEMIS                 ADD GV11-QVENDUE TO W-QTE-ENTR1                          
THEMIS              WHEN W-ENTR2                                                
THEMIS                 MOVE '1' TO W-CONTIENT-ENTR2                             
THEMIS                 ADD GV11-QVENDUE TO W-QTE-ENTR2                          
THEMIS              WHEN W-ENTR3                                                
THEMIS                 MOVE '1' TO W-CONTIENT-ENTR3                             
THEMIS                 ADD GV11-QVENDUE TO W-QTE-ENTR3                          
THEMIS              WHEN W-ENTR4                                                
THEMIS                 MOVE '1' TO W-CONTIENT-ENTR4                             
THEMIS                 ADD GV11-QVENDUE TO W-QTE-ENTR4                          
THEMIS              WHEN W-ENTR5                                                
THEMIS                 MOVE '1' TO W-CONTIENT-ENTR5                             
THEMIS                 ADD GV11-QVENDUE TO W-QTE-ENTR5                          
THEMIS              END-EVALUATE                                                
THEMIS           END-IF                                                         
      * ON RECHERCHE LA DDELIV MAX POUR ALIGNER LES DDELIV SUR LA PLUS          
      * GRANDE SI ON EST DANS LE CAS D'UNE VENTE INCOMPLETE                     
                   IF  COMM-MEC64-TAB-DDELIVO (I-NL-MAX)                        
                            >   COMM-MEC64-DDELIV-COM                           
                      MOVE COMM-MEC64-TAB-DDELIVO (I-NL-MAX)                    
                          TO   COMM-MEC64-DDELIV-COM                            
                   END-IF                                                       
      * LE DEPOT EST IMPORTANT POUR LE CAS OU ON APPEL LE MEC65 CAR             
      * LA RECHERCHE DE MUT EST DIFFERENTE SI C'EST UN DEPOT 90 OU 95           
      *          IF GV11-NDEPOT = '090' OR '095'                                
THEMIS           IF GV11-NDEPOT > SPACES                                        
                    MOVE GV11-NORDRE TO COMM-MEC64-NORDRE                       
                    MOVE GV11-NDEPOT TO                                         
                            COMM-MEC64-TAB-NDEPOT(I-NL-MAX)                     
                 ELSE                                                           
                    IF GV11-LCOMMENT(16:3) = '090' OR '095'                     
THEMIS                          OR '125'                                        
                       MOVE GV11-LCOMMENT(16:3) TO                              
                            COMM-MEC64-TAB-NDEPOT(I-NL-MAX)                     
                    ELSE                                                        
                        MOVE SPACE TO COMM-MEC64-TAB-NDEPOT(I-NL-MAX)           
                    END-IF                                                      
                 END-IF                                                         
LA1807*          IF GV11-LCOMMENT(1:5) = 'DACEM'                                
LA1807*            MOVE 'DAC' TO COMM-MEC64-TAB-NDEPOT(I-NL-MAX)                
LA1807*          END-IF                                                         
                 SET WC-TSTENVOI-FIN TO TRUE                                    
               END-IF                                                           
             END-PERFORM                                                        
            END-IF                                                              
            IF GV99-WPRECO = '1'                                                
               SET COMMANDE-PRECO      TO TRUE                                  
LA1807*        SET EC68-COMMANDE-PRECO TO TRUE                                  
               SET EC69-COMMANDE-PRECO TO TRUE                                  
            END-IF                                                              
            PERFORM FETCH-RTGV11                                                
           END-PERFORM                                                          
           PERFORM CLOSE-RTGV11                                                 
           IF I-NL-MAX > 1 OR WS-NB-PRODUIT > 1                                 
              SET COMM-MEC64-TYPE-NB-MULTI TO TRUE                              
           END-IF                                                               
           IF I-NL-MAX = 0 AND WS-NB-PRODUIT = 0                                
              SET COMM-MEC64-TYPE-ANNUL    TO TRUE                              
           END-IF                                                               
      * 'O' VENTE MULTI DARTY-DACEM                                             
      * 'D' VENTE MONO OU MULTI DACEM                                           
      * 'N' VENTE MONO OU MULTI DARTY                                           
           EVALUATE TOP-MULTIDA                                                 
               WHEN '11'    MOVE 'O' TO EC64-MULTIDA                            
               WHEN '10'    MOVE 'D' TO EC64-MULTIDA                            
               WHEN OTHER   MOVE 'N' TO EC64-MULTIDA                            
           END-EVALUATE                                                         
THEMIS     IF W-THEMIS = 'O'                                                    
             MOVE 0 TO TALLY-CPT                                                
             INSPECT TOP-MULTIENTR TALLYING TALLY-CPT FOR ALL '1'               
             IF TALLY-CPT > 1                                                   
                MOVE 'O' TO EC64-MULTIDA                                        
             END-IF                                                             
           END-IF                                                               
           .                                                                    
       FIN-RECHERCHE-VENTE. EXIT.                                               
      ****************************                                              
       ACTION-CONTROLE  SECTION.                                                
      ***************************                                               
           SET EC64-STATUT-COMPLETE TO TRUE                                     
           MOVE SPACE TO COMM-MEC64-FILIERE                                     
           PERFORM VARYING I-NL FROM 1 BY 1 UNTIL I-NL > I-NL-MAX               
      * DETERMINATION DU STATUT DE LA VENTE                                     
      * PRIORITE 1) dacem ferme, 2) cqe dacem, 3) ligne en ano                  
      *          4) PB DISPO, 5) ATTENTE FOURN                                  
      * LA VENTE SERA COMPLETE SI TOUS LES ARTICLES SONT DISPO (ETAT= 1)        
      *                                                                         
              IF EC64-STOCK-ATT-FOURN (I-NL)                                    
                 SET EC64-STATUT-ATTENTE-FOURN TO TRUE                          
              END-IF                                                            
              IF EC64-STATUT-COMPLETE                                           
                 IF EC64-DACEM-FERME (I-NL)                                     
                       SET EC64-STATUT-DACEM-FERME   TO TRUE                    
                 END-IF                                                         
                 IF EC64-AUCUN-STOCK-DISPO(I-NL)                                
LA1807*             IF  (COMM-MEC64-CREATION)                                   
LA1807*             AND (COMM-MEC64-CDEDACEM (I-NL))                            
LA1807*                CONTINUE                                                 
LA1807*             ELSE                                                        
                       IF NOT EC64-STATUT-ANO                                   
                          SET EC64-STATUT-PB-DISPO  TO TRUE                     
                       END-IF                                                   
LA1807*             END-IF                                                      
                 END-IF                                                         
                 IF EC64-STOCK-ERREUR (I-NL)                                    
                     SET EC64-STATUT-ANO             TO TRUE                    
                 END-IF                                                         
              END-IF                                                            
              IF NOT EC64-STATUT-COMPLETE                                       
                IF COMM-MEC64-DDELIV-COM NOT > COMM-MEC64-DJOUR                 
                   MOVE W-DATE-J-PLUS1 TO COMM-MEC64-DDELIV-COM                 
                END-IF                                                          
              END-IF                                                            
           END-PERFORM                                                          
      * 'O' VENTE MULTI DARTY-DACEM                                             
      * 'D' VENTE MONO OU MULTI DACEM                                           
      * 'N' VENTE MONO OU MULTI DARTY                                           
           EVALUATE TOP-MULTIDA                                                 
               WHEN '11'    MOVE 'O' TO EC64-MULTIDA                            
               WHEN '10'    MOVE 'D' TO EC64-MULTIDA                            
               WHEN OTHER   MOVE 'N' TO EC64-MULTIDA                            
           END-EVALUATE                                                         
THEMIS     IF W-THEMIS = 'O'                                                    
             MOVE 0 TO TALLY-CPT                                                
             INSPECT TOP-MULTIENTR TALLYING TALLY-CPT FOR ALL '1'               
             IF TALLY-CPT > 1                                                   
                MOVE 'O' TO EC64-MULTIDA                                        
             END-IF                                                             
           END-IF                                                               
           .                                                                    
       FIN-ACTION-CONTROLE. EXIT.                                               
       ACTION-VALID-DATE  SECTION.                                              
           IF COMM-MEC64-ACTION-VALID-DATE                                      
              PERFORM SELECT-RTSL20                                             
           END-IF                                                               
           INITIALIZE COMM-EC69-APPLI                                           
RESAJ         MOVE 1 TO I-NL                                                    
 "            PERFORM CHERCHE-LIEU-STOCKAGE                                     
 "            PERFORM MUT-SL300-O-N                                             
05/14 *    FIN THEMIS                                                           
      *    IF W-THEMIS = 'O'                                                    
             PERFORM RECHERCHE-MUT-MUCEN                                        
      *    ELSE                                                                 
      *      PERFORM RECHERCHE-MUT-MEC65                                        
      *    END-IF                                                               
           SET TRAITEMENT-OK TO TRUE                                            
           MOVE ZERO TO W-DARTY90-TRT                                           
           MOVE ZERO TO W-DACEM-TRT                                             
           MOVE ZERO TO W-DARTY95-TRT                                           
      *                                                                         
           PERFORM VARYING I-NL FROM 1 BY 1                                     
           UNTIL COMM-MEC64-TAB-NCODIC(I-NL) <= SPACE                           
           OR TRAITEMENT-KO                                                     
                   PERFORM CHERCHE-LIEU-STOCKAGE                                
              PERFORM MUT-SL300-O-N                                             
              IF (COMM-MEC64-ACTION-VALID-DATE)                                 
              AND (SL20-INCOMPLETE NOT = 'O')                                   
                 PERFORM SELECT-RTSL21                                          
              ELSE                                                              
                 SET CODIC-TRT-OK  TO TRUE                                      
              END-IF                                                            
              IF CODIC-TRT-OK                                                   
                PERFORM SELECT-RTGV11                                           
                PERFORM SELECT-RTGV22                                           
      * ON NE TRAITE PAS LES CONTREMARQUES DACEM DEJA MUTEES                    
                IF NOT EC64-ARTICLE-MUTE (I-NL)                                 
                   MOVE GV11-NCODIC      TO GA00-NCODIC                         
                   PERFORM SELECT-RTGA00                                        
                   PERFORM CHERCHE-LIEU-STOCKAGE                                
                   IF TRAITEMENT-OK                                             
                     PERFORM MEF-REQ-VALID-DATE                                 
                     IF COMM-MEC64-CDEDARTY(I-NL)                               
                       PERFORM APPEL-MEC69                                      
LA1807* MISE EN COMMENTAIRE : SUPPRESSION DES APPELS AU MEC68                   
      *              ELSE                                                       
      *                PERFORM APPEL-MEC68                                      
      *                IF (COMM-EC68-SUP-DACEM)                                 
      *                AND ((COMM-EC68-CODRET = '0000')                         
      *                OR (COMM-EC68-CODRET = '6806'))                          
      *                  IF (COMM-EC68-CODRET = '6806')                         
      *                  AND (COMM-EC68-DMUT  = MEC65-DATE-DACEM)               
      *                     MOVE '0000'  TO  COMM-EC68-CODRET                   
CEN012*                     MOVE SPACES  TO  COMM-EC68-LMESSAGE                 
CEN012* ALIMENTATION DES ZONES                                                  
CEN012*                     MOVE GV11-LCOMMENT(6:7)                             
CEN012*                                  TO COMM-EC68-NMUTATION                 
CEN012*                     MOVE GV11-LCOMMENT(14:8)                            
CEN012*                                  TO COMM-EC68-DATE-DISPO                
CEN012*                     MOVE GV11-LCOMMENT(23:4)                            
CEN012*                                  TO COMM-EC68-FILIERE                   
      *                  ELSE                                                   
      *                     INITIALIZE COMM-EC68-APPLI                          
      *                     MOVE ' NN' TO COMM-EC68-NEW-MODE                    
      *                     SET COMM-EC68-NEW-APPEL TO TRUE                     
      *                     SET COMM-EC68-CREATION-DACEM TO TRUE                
      *                     MOVE MEC65-FILIERE TO COMM-MEC64-FILIERE            
      *                     MOVE MEC65-DATE-DACEM TO COMM-EC68-DMUT             
      *                     PERFORM PARAM-MUT-DACEM                             
      *                     PERFORM APPEL-MEC68                                 
      *                  END-IF                                                 
      *                END-IF                                                   
LA1807* FIN MISE EN COMMENTAIRE                                                 
                     END-IF                                                     
                     PERFORM TRT-RETOUR-REQ-VALIDATION                          
                  END-IF                                                        
                END-IF                                                          
                IF TRAITEMENT-OK                                                
                   PERFORM TRT-MAJ-VALIDATION                                   
                ELSE                                                            
                   IF COMM-MEC64-ACTION-VALID-DATE                              
LA1807*            AND ((COMM-EC68-CODRET NOT = '0000')                         
LA1807*            OR   (COMM-EC69-CODRET NOT = '0000'))                        
LA1807             AND  (COMM-EC69-CODRET NOT = '0000')                         
                      SET TRAITEMENT-OK TO TRUE                                 
                      EVALUATE TRUE                                             
                      WHEN COMM-MEC64-CDEDARTY(I-NL)                            
                         MOVE 'N'    TO COMM-EC69-MUT-SL300                     
                         PERFORM APPEL-MEC69                                    
LA1807*               WHEN COMM-MEC64-CDEDACEM(I-NL)                            
LA1807*                  MOVE 'N'    TO COMM-EC68-MUT-SL300                     
LA1807*                  PERFORM APPEL-MEC68                                    
                      END-EVALUATE                                              
                      PERFORM TRT-RETOUR-REQ-VALIDATION                         
LA1807*               IF ((COMM-EC68-CODRET NOT = '0000')                       
LA1807*               OR (COMM-EC69-CODRET NOT = '0000'))                       
LA1807                IF (COMM-EC69-CODRET NOT = '0000')                        
                         IF WS-NB-PRODUIT = 1                                   
                            MOVE '66VGM' TO GV11-CEQUIPE                        
                            MOVE '66VGM' TO GV22-CEQUIPE                        
                         ELSE                                                   
                            MOVE 'MGV64' TO GV11-CEQUIPE                        
                            MOVE 'MGV64' TO GV22-CEQUIPE                        
                         END-IF                                                 
                         EVALUATE TRUE                                          
                         WHEN COMM-MEC64-CDEDARTY(I-NL)                         
                            IF COMM-EC69-CODRET = '6903'                        
                               MOVE SPACES TO GV11-LCOMMENT (6:7)               
                                              GV22-NMUTATION                    
                            END-IF                                              
LA1807*                  WHEN COMM-MEC64-CDEDACEM(I-NL)                         
LA1807*                     IF (COMM-EC68-CREATION-DACEM)                       
LA1807*                     AND (COMM-EC68-NMUTATION = SPACES                   
LA1807*                         OR LOW-VALUES)                                  
LA1807*                        MOVE SPACES TO  GV11-LCOMMENT(6:20)              
LA1807*                     END-IF                                              
                         END-EVALUATE                                           
                         MOVE 'A'        TO GV22-CSTATUT                        
                         PERFORM UPDATE-GV11-ANO                                
                         PERFORM UPDATE-GV22-ANO                                
                         MOVE  1 TO W-QTE                                       
                         PERFORM UPDATE-SL20-ANO                                
LA1807                ELSE                                                      
LA1807                   PERFORM TRT-MAJ-VALIDATION                             
                      END-IF                                                    
                   END-IF                                                       
                END-IF                                                          
              END-IF                                                            
           END-PERFORM                                                          
      * LA SORTIE DE LA GESTION DES VENTES COMPLETES C'EST MAL PASS�, IL        
      * FAUT RECREER L'ENREG SL20 ET SUPPRIMER LES MUT FINALES POUR LES         
      * ARTICLES QUI ONT �T� TRAIT� AVANT L'ERREUR                              
      *                                                                         
           IF TRAITEMENT-KO                                                     
              IF NOT COMM-MEC64-ACTION-VALID-DATE                               
                 SET EC64-STATUT-PREC-COMPLETE TO TRUE                          
                 PERFORM ACTION-SET-INCOMPLETE                                  
                 MOVE '0000'    TO COMM-MEC64-CODE-RETOUR                       
                 MOVE SPACES    TO COMM-MEC64-MESSAGE                           
                 IF AUTO-DISPLAY                                                
                    STRING 'valid ko, remise en incomplete'                     
                     DELIMITED BY SIZE INTO W-MESSAGE                           
                     PERFORM DISPLAY-LOG                                        
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
      * SUPPRESSION DE L'ENREG SL20                                             
      * ON LE FAIT A LA FIN ET NON PLUS AU D�BUT, CAR SI LA VENTE A             
      * CONTENUE UN CQE DACEM, LA VENTE A ETE DUPLIQUE AU CEN, ON DOIS          
      * DONC TOUJOURS LAISSER LE CODE CQE DANS VALCTL MEME SI L'ARTICLE         
      * EN CQE � �T� SUPPRIM� DE LA VENTE.                                      
               MOVE '0000' TO COMM-MEC64-CODE-RETOUR                            
               MOVE W-DARTY90-TRT   TO EC64-NB-DARTY90-TRT                      
               MOVE W-DACEM-TRT     TO EC64-NB-DACEM-TRT                        
               MOVE W-DARTY95-TRT   TO EC64-NB-DARTY95-TRT                      
               PERFORM ACTION-SORTIE                                            
           END-IF                                                               
            .                                                                   
       FIN-ACTION-VALID-DATE. EXIT.                                             
      *****************************                                             
       RECHERCHE-MUT-MEC65  SECTION.                                            
      ******************************                                            
      * SI LA VENTE CONTIENT DES PRODUITS DARTY ET DACEM, ON APPEL LE           
      * MEC65 POUR CHERCHER LES MUTATIONS COMMUNES                              
           INITIALIZE MEC65-COMMAREA                                            
           MOVE SPACE TO MEC65-COMMAREA                                         
           PERFORM SELECT-RTSL20                                                
           IF TROUVE                                                            
              IF SL20-INCOMPLETE  = 'O'                                         
                 MOVE 'I' TO COMM-EC69-ETAT-PREC-VTE                            
              ELSE                                                              
                 MOVE 'C' TO COMM-EC69-ETAT-PREC-VTE                            
              END-IF                                                            
           ELSE                                                                 
              IF COMM-EC69-ETAT-PREC-VTE NOT = 'I'                              
                 MOVE 'C' TO COMM-EC69-ETAT-PREC-VTE                            
              END-IF                                                            
           END-IF                                                               
      * SI VENTE MULTI DACEM-DARTY ON APPEL LE MEC65                            
           IF EC64-MULTIDA = 'O'                                                
              MOVE 'O'  TO MEC65-REC-FILIERE                                    
      * SI LA DDELIV EST < OU = A LA DATE DU JOUR, ON CHERCHE A MUTER           
      * A J, SI NON, ON CHERCHE A MUTER A J+1                                   
              MOVE COMM-MEC64-DJOUR   TO MEC65-DATE-JOUR                        
LA1807*       IF COMM-MEC64-ACTION-MAJ-DDELIV                                   
LA1807*          MOVE COMM-MEC64-DATE-MAJ     TO MEC65-DDELIV                   
LA1807*          MOVE 'O'                     TO MEC65-MUT-BSL300               
LA1807*       ELSE                                                              
                 IF EC64-VENTE-CQE = 'O'                                        
                    MOVE COMM-MEC64-DDELIV-COM TO MEC65-DDELIV                  
                    MOVE 'O'                   TO MEC65-MUT-BSL300              
                 ELSE                                                           
                    IF COMM-MEC64-DDELIV-COM > COMM-MEC64-DJOUR                 
                       IF COMMANDE-PRECO                                        
                       OR EC64-STATUT-ATTENTE-FOURN                             
                         MOVE COMM-MEC64-DDELIV-COM TO MEC65-DDELIV             
                       ELSE                                                     
RESAJ                    IF WS-MUT-A-J = 'J'                                    
                           MOVE COMM-MEC64-DJOUR   TO MEC65-DDELIV              
 "                       ELSE                                                   
 "                         MOVE COMM-MEC64-DDELIV-COM  TO MEC65-DDELIV          
 "                       END-IF                                                 
                       END-IF                                                   
                    ELSE                                                        
                       MOVE COMM-MEC64-DJOUR TO MEC65-DDELIV                    
                    END-IF                                                      
                    IF COMM-MEC64-ACTION-ENCAISSEMENT                           
                       MOVE W-VTE-MULTI TO MEC65-MUT-BSL300                     
                    ELSE                                                        
                       MOVE 'O'         TO MEC65-MUT-BSL300                     
                    END-IF                                                      
                 END-IF                                                         
LA1807*       END-IF                                                            
              PERFORM VARYING I-NL FROM 1 BY 1                                  
              UNTIL COMM-MEC64-TAB-NCODIC(I-NL) = SPACE                         
              OR TRAITEMENT-KO                                                  
                 MOVE COMM-MEC64-TAB-NCODIC(I-NL)                               
                                    TO MEC65-CODIC(I-NL)                        
                 MOVE COMM-MEC64-TAB-QVENDUE(I-NL)                              
                                    TO MEC65-QTE(I-NL)                          
                 MOVE COMM-MEC64-TAB-WEMPORTE(I-NL)                             
                                    TO MEC65-WEMPORTE(I-NL)                     
                 MOVE SPACE         TO MEC65-MUTATION(I-NL)                     
              END-PERFORM                                                       
              MOVE ' '              TO MEC65-MUT-MULTI                          
              MOVE MEC65-COMMAREA            TO Z-COMMAREA-LINK                 
              MOVE 'MEC65'                   TO NOM-PROG-LINK                   
              PERFORM LINK-PROG                                                 
              MOVE Z-COMMAREA-LINK           TO MEC65-COMMAREA                  
              IF MEC65-CODE-RET NOT = '0'                                       
                 IF AUTO-DISPLAY                                                
                    STRING 'MEC65 CODE RET ' MEC65-CODE-RET                     
                    ' VTE ' COMM-MEC64-NVENTE                                   
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-LOG                                         
                 END-IF                                                         
              ELSE                                                              
                 IF AUTO-DISPLAY                                                
                    STRING 'RETOUR MEC65 OK ' MEC65-FILIERE                     
                     ' ' MEC65-DATE-DACEM                                       
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-LOG                                         
                 END-IF                                                         
                 PERFORM VARYING I-NL FROM 1 BY 1                               
                 UNTIL COMM-MEC64-TAB-NCODIC(I-NL) = SPACE                      
                 OR TRAITEMENT-KO                                               
                    MOVE MEC65-MUTATION(I-NL)                                   
                           TO COMM-MEC64-TAB-NMUT(I-NL)                         
                    IF MEC65-DATE-DACEM NOT = SPACE                             
                       MOVE MEC65-DATE-DACEM                                    
                           TO COMM-MEC64-TAB-DDELIVO(I-NL)                      
                              COMM-MEC64-DDELIV                                 
                    END-IF                                                      
                 END-PERFORM                                                    
      * ON A APPELE LE MEC65 CAR ON AVAIT DU DARTY ET DU DACEM. COMME           
      * LES DARTY SONT UNIQUEMENT SUR LE DEPOT 90, ON NE CONSIDERE PAS          
      * LA VENTE COMME MULTI DARTY-DACEM                                        
              END-IF                                                            
           END-IF                                                               
      * SI C'EST UNE VENTE AVEC QUE DU DACEM, ON APPEL LE MEC65 POUR            
      * CHERCHER LA FILIERE                                                     
LA1807* DEBUT MISE EN COMMENTAIRE                                               
      *    IF EC64-MULTIDA = 'D'                                                
      *       MOVE 'O'  TO MEC65-REC-FILIERE                                    
      * ON CHERCHE A MUTER LE PLUS VITE POSSIBLE                                
      *       MOVE COMM-MEC64-DJOUR   TO MEC65-DATE-JOUR                        
RESAJ *       IF WS-MUT-A-J = 'J'                                               
      *          MOVE COMM-MEC64-DJOUR   TO MEC65-DDELIV                        
 "    *       ELSE                                                              
 "    *         IF COMM-MEC64-DDELIV-COM > COMM-MEC64-DJOUR                     
 "    *           MOVE COMM-MEC64-DDELIV-COM  TO MEC65-DDELIV                   
 "    *         ELSE                                                            
      *          MOVE COMM-MEC64-DJOUR   TO MEC65-DDELIV                        
 "    *         END-IF                                                          
 "    *       END-IF                                                            
LA1807*       IF COMM-MEC64-ACTION-MAJ-DDELIV                                   
LA1807*          MOVE COMM-MEC64-DATE-MAJ     TO MEC65-DDELIV                   
LA1807*          MOVE 'O'                     TO MEC65-MUT-BSL300               
LA1807*       ELSE                                                              
                 IF COMM-MEC64-ACTION-VALID-EC06                                
                    MOVE WS-MUT-SL300  TO MEC65-MUT-BSL300                      
                 ELSE                                                           
                    IF EC64-VENTE-CQE = 'O'                                     
                       MOVE COMM-MEC64-DDELIV-COM TO MEC65-DDELIV               
                       MOVE 'O'         TO MEC65-MUT-BSL300                     
                    ELSE                                                        
                       IF COMMANDE-PRECO                                        
                          MOVE COMM-MEC64-DDELIV-COM TO MEC65-DDELIV            
                       END-IF                                                   
                       IF COMM-MEC64-ACTION-ENCAISSEMENT                        
                          MOVE 'N'         TO MEC65-MUT-BSL300                  
                       ELSE                                                     
                          MOVE 'O'         TO MEC65-MUT-BSL300                  
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
LA1807*       END-IF                                                            
              IF WS-NB-PRODUIT = 1                                              
                 MOVE 'N' TO MEC65-MUT-MULTI                                    
              ELSE                                                              
                 MOVE ' ' TO MEC65-MUT-MULTI                                    
              END-IF                                                            
              PERFORM VARYING I-NL FROM 1 BY 1                                  
              UNTIL COMM-MEC64-TAB-NCODIC(I-NL) = SPACE                         
              OR TRAITEMENT-KO                                                  
                 MOVE COMM-MEC64-TAB-NCODIC(I-NL)                               
                                    TO MEC65-CODIC(I-NL)                        
                 MOVE COMM-MEC64-TAB-QVENDUE(I-NL)                              
                                    TO MEC65-QTE(I-NL)                          
                 MOVE COMM-MEC64-TAB-WEMPORTE(I-NL)                             
                                    TO MEC65-WEMPORTE(I-NL)                     
                 MOVE SPACE         TO MEC65-MUTATION(I-NL)                     
              END-PERFORM                                                       
              MOVE MEC65-COMMAREA            TO Z-COMMAREA-LINK                 
              MOVE 'MEC65'                   TO NOM-PROG-LINK                   
              PERFORM LINK-PROG                                                 
              MOVE Z-COMMAREA-LINK           TO MEC65-COMMAREA                  
              IF MEC65-CODE-RET NOT = '0'                                       
                 IF AUTO-DISPLAY                                                
                    STRING 'MEC65 CODE RET ' MEC65-CODE-RET                     
                    ' VTE ' COMM-MEC64-NVENTE                                   
                       '  ' MEC65-DATE-DACEM                                    
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-LOG                                         
                 END-IF                                                         
              ELSE                                                              
                 IF AUTO-DISPLAY                                                
                    STRING 'RETOUR MEC65 OK ' MEC65-FILIERE                     
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-LOG                                         
                 END-IF                                                         
                 PERFORM VARYING I-NL FROM 1 BY 1                               
                 UNTIL COMM-MEC64-TAB-NCODIC(I-NL) = SPACE                      
                 OR TRAITEMENT-KO                                               
                    MOVE MEC65-MUTATION(I-NL)                                   
                           TO COMM-MEC64-TAB-NMUT(I-NL)                         
                    IF MEC65-DATE-DACEM NOT = SPACE                             
                       MOVE MEC65-DATE-DACEM                                    
                           TO COMM-MEC64-TAB-DDELIVO(I-NL)                      
                              COMM-MEC64-DDELIV                                 
                    END-IF                                                      
                 END-PERFORM                                                    
                 IF MEC65-DATE-DACEM NOT = SPACE                                
                    MOVE MEC65-DATE-DACEM TO COMM-MEC64-DATE-MAJ                
                 END-IF                                                         
              END-IF                                                            
LA1807*    END-IF                                                               
           .                                                                    
       FIN-RECHERCHE-MUT-MEC65. EXIT.                                           
      ******************************                                            
THEMIS RECHERCHE-MUT-MUCEN  SECTION.                                            
      ******************************                                            
      *                                                                         
      *                                                                         
           INITIALIZE COMM-EC69-APPLI                                           
           PERFORM SELECT-RTSL20                                                
           IF TROUVE                                                            
              IF SL20-INCOMPLETE  = 'O'                                         
                 MOVE 'I' TO COMM-EC69-ETAT-PREC-VTE                            
              ELSE                                                              
                 MOVE 'C' TO COMM-EC69-ETAT-PREC-VTE                            
              END-IF                                                            
           ELSE                                                                 
              IF COMM-EC69-ETAT-PREC-VTE NOT = 'I'                              
                 MOVE 'C' TO COMM-EC69-ETAT-PREC-VTE                            
              END-IF                                                            
           END-IF                                                               
           IF EC64-MULTIDA = 'O'                                                
THEMIS     IF COMMANDE-PRECO                                                    
             SET EC69-COMMANDE-PRECO TO TRUE                                    
           END-IF                                                               
           MOVE SPACE TO COMM-EC69-NMUTATION                                    
           MOVE SPACE TO COMM-EC69-NMUT                                         
LA1807*    IF COMM-MEC64-ACTION-MAJ-DDELIV                                      
LA1807*        MOVE COMM-MEC64-DATE-MAJ TO COMM-EC69-DDELIV                     
LA1807*        MOVE 'O'              TO COMM-EC69-MUT-SL300                     
LA1807*    ELSE                                                                 
               IF EC64-VENTE-CQE = 'O'                                          
                  MOVE COMM-MEC64-DDELIV-COM TO COMM-EC69-DDELIV                
                  MOVE 'O'           TO COMM-EC69-MUT-SL300                     
               ELSE                                                             
                  IF COMM-MEC64-DDELIV-COM > COMM-MEC64-DJOUR                   
                     IF COMMANDE-PRECO                                          
                     OR EC64-STATUT-ATTENTE-FOURN                               
                       MOVE COMM-MEC64-DDELIV-COM TO COMM-EC69-DDELIV           
                     ELSE                                                       
                       MOVE COMM-MEC64-DJOUR TO COMM-EC69-DDELIV                
                     END-IF                                                     
                  ELSE                                                          
                     MOVE COMM-MEC64-DJOUR TO COMM-EC69-DDELIV                  
                  END-IF                                                        
                  IF COMM-MEC64-ACTION-ENCAISSEMENT                             
                     MOVE W-VTE-MULTI TO COMM-EC69-MUT-SL300                    
                  ELSE                                                          
                     MOVE 'O'         TO COMM-EC69-MUT-SL300                    
                  END-IF                                                        
               END-IF                                                           
LA1807*     END-IF                                                              
            PERFORM ALIM-MEC69-MUCEN                                            
            PERFORM APPEL-MEC69                                                 
            IF MEC64-PAS-ERREUR                                                 
                 MOVE COMM-EC69-NMUTATION  TO W-NMUT1                           
                 MOVE COMM-EC69-NMUTATION2 TO W-NMUT2                           
            END-IF                                                              
           END-IF                                                               
           .                                                                    
       FIN-RECHERCHE-MUT-MUCEN. EXIT.                                           
      ******************************                                            
      * -------------------------------------------------------------           
      * CE MODULE EST APPELE PAR LE TGR32 ET PAR L'EC06,L'OBJECTIF EST          
      * JUSTE DE RESERVER LE PRODUIT, L'EC01 SE CHARGERA D'EXPEDIER             
      * LE PRODUIT SI C'EST UNE VENTE DEVENUE COMPLETE                          
      * -------------------------------------------------------------           
       ACTION-VALID-CODIC SECTION.                                              
      ****************************                                              
           MOVE ZERO   TO W-DARTY90-TRT                                         
                          W-DACEM-TRT                                           
                          W-DARTY95-TRT                                         
           INITIALIZE     COMM-EC67-APPLI                                       
                          COMM-EC69-APPLI                                       
           MOVE '0000' TO COMM-EC67-CODRET                                      
                          COMM-EC69-CODRET                                      
           SET GV99-PRECO-KO     TO TRUE                                        
           SET TRAITEMENT-OK        TO TRUE                                     
           SET EC64-STATUT-COMPLETE TO TRUE                                     
           IF COMM-MEC64-ACTION-VALID-CODIC                                     
              PERFORM SELECT-RTGV11-NQ                                          
              PERFORM SELECT-RTGV22-NQ                                          
              PERFORM SELECT-RTGV99                                             
              MOVE GV11-NCODIC      TO GA00-NCODIC                              
              PERFORM SELECT-RTGA00                                             
              MOVE 1  TO I-NL                                                   
              MOVE GA00-CFAM   TO COMM-MEC64-TAB-CFAM(I-NL)                     
              PERFORM CHERCHE-LIEU-STOCKAGE                                     
           END-IF                                                               
           IF GV11-LCOMMENT(1:5) = 'DARTY'                                      
              SET COMM-EC67-NEW-APPEL TO TRUE                                   
              MOVE GV11-WCQERESF      TO COMM-MEC64-TAB-WCQERESF(I-NL)          
              IF EC64-ARTICLE-EN-CF(I-NL)                                       
                 MOVE '='                  TO COMM-MEC64-MAJ-STOCK              
              ELSE                                                              
                 MOVE '+'                  TO COMM-MEC64-MAJ-STOCK              
              END-IF                                                            
              PERFORM APPEL-MEC67                                               
              IF COMM-EC67-CODRET = '0000' OR '6523'                            
                 PERFORM MUT-SL300-O-N                                          
                 PERFORM ALIM-MEC69-VALID                                       
                 PERFORM APPEL-MEC69                                            
      * SI MUTATION NON TROUVEE, RECHERCHE D'UNE MUT TEMPO                      
                 IF COMM-EC69-CODRET NOT = '0000'                               
                    PERFORM ALIM-MEC69-RESA                                     
                    PERFORM APPEL-MEC69                                         
                    MOVE COMM-EC69-NMUTTEMP  TO COMM-EC69-NMUTATION             
                 END-IF                                                         
      *                                                                         
                 IF MEC64-PAS-ERREUR                                            
                    MOVE COMM-EC69-FL05-NSOCDEPOT(1) TO GV11-NSOCLIVR           
                    MOVE COMM-EC69-FL05-NDEPOT(1) TO GV11-NDEPOT                
                    MOVE COMM-EC69-NMUTATION   TO GV22-NMUTATION                
                                                  GV11-LCOMMENT (6:7)           
                 END-IF                                                         
                 MOVE W-SPDATDB2-DSYST     TO GV11-DSYST                        
                 IF GV11-LCOMMENT(16:3) = '095'                                 
                    ADD 1 TO W-DARTY95-TRT                                      
                 END-IF                                                         
                 IF GV11-LCOMMENT(16:3) = '090'                                 
                    ADD 1 TO W-DARTY90-TRT                                      
                 END-IF                                                         
      * SI LE PRODUIT EST EN COMMANDE FOURNISSEUR ET QU'IL ETAIT DEJA           
      * EN COMMANDE FOURNISSEUR, ON NE FAIT RIEN                                
                 IF COMM-EC67-STOCK-FOURN-OUI                                   
                    SET EC64-STATUT-ATTENTE-FOURN TO TRUE                       
                    MOVE 'F' TO GV11-WCQERESF                                   
                    MOVE 'F' TO GV22-WCQERESF                                   
                    PERFORM UPDATE-RTSL21                                       
                 ELSE                                                           
                    MOVE ' ' TO GV11-WCQERESF                                   
                    MOVE ' ' TO GV22-WCQERESF                                   
                    MOVE 'R' TO GV22-CSTATUT                                    
                    MOVE -1 TO W-QTE                                            
                    PERFORM UPDATE-RTSL20-UN                                    
                 END-IF                                                         
      * SI VENTE MONO -> CEQUIPE = MGV66                                        
                 IF SL20-INCOMPLETE = 'N'                                       
                    MOVE 'MGV66'  TO GV11-CEQUIPE                               
                                     GV22-CEQUIPE                               
                 END-IF                                                         
                 PERFORM UPDATE-GV1100                                          
                 IF COMM-MEC64-CPROG   = 'MGV66'                                
                    PERFORM LOG-GV23                                            
                 END-IF                                                         
                 MOVE GV11-NDEPOT    TO GV22-NDEPOT                             
                 MOVE GV11-NSOCLIVR  TO GV22-NSOCLIVR                           
                 PERFORM UPDATE-RTGV22                                          
              ELSE                                                              
                 IF AUTO-DISPLAY                                                
                    STRING 'PRODUIT NON DISPONIBLE ' COMM-EC67-CODRET           
                     DELIMITED BY SIZE INTO W-MESSAGE                           
                     PERFORM DISPLAY-LOG                                        
                 END-IF                                                         
                 SET EC64-STATUT-ANO   TO TRUE                                  
                 MOVE COMM-EC67-CODRET TO COMM-MEC64-CODE-RETOUR                
              END-IF                                                            
           ELSE                                                                 
              STRING 'PRODUIT DACEM EN VALID CODIC '                            
              COMM-MEC64-TAB-NCODIC(1)                                          
              ' VENTE ' COMM-MEC64-NVENTE                                       
               DELIMITED BY SIZE INTO W-MESSAGE                                 
               PERFORM DISPLAY-LOG                                              
           END-IF                                                               
           .                                                                    
       FIN-ACTION-VALID-CODIC. EXIT.                                            
      ******************************                                            
       ACTION-VALID-EC06  SECTION.                                              
      ****************************                                              
           MOVE ZEROES      TO W-DARTY90-TRT                                    
           MOVE ZEROES      TO W-DACEM-TRT                                      
           MOVE ZEROES      TO W-DARTY95-TRT                                    
           MOVE 1           TO I-NL                                             
                               WS-NB-PRODUIT                                    
           PERFORM SELECT-RTGV11-NQ                                             
           PERFORM SELECT-RTGV22-NQ                                             
THEMIS*        IF W-THEMIS = 'O'                                                
?     *          PERFORM RECHERCHE-MUT-MUCEN                                    
NON   *        END-IF                                                           
           IF GV22-CSTATUT NOT = 'R'                                            
              IF GV11-DTOPE = SPACES OR LOW-VALUES                              
                 IF GV11-LCOMMENT(1:5) = 'DARTY'                                
                    SET COMM-MEC64-CDEDARTY(I-NL) TO TRUE                       
                    PERFORM TRT-CODIC-DARTY-EC06                                
LA1807*          ELSE                                                           
LA1807*             SET COMM-MEC64-CDEDACEM(I-NL) TO TRUE                       
LA1807*             PERFORM TRT-CODIC-DACEM-EC06                                
                 END-IF                                                         
              ELSE                                                              
      * LA VENTE D'UN PRODUIT TOPE NE PEUT PAS ETRE EN ANOMALIE                 
                 MOVE COMM-MEC64-NLIEU             TO SL21-NLIEU                
                 MOVE COMM-MEC64-NSOCIETE          TO SL21-NSOCIETE             
                 MOVE COMM-MEC64-NVENTE            TO SL21-NVENTE               
                 MOVE COMM-MEC64-TAB-NCODIC(1)     TO SL21-NCODIC               
                 MOVE COMM-MEC64-TAB-NSEQNQ(1)     TO SL21-NSEQNQ               
                 PERFORM DELETE-RTSL21-UN                                       
                 PERFORM UPDATE-SL20-ANNULE                                     
              END-IF                                                            
           END-IF                                                               
           MOVE W-DARTY90-TRT     TO EC64-NB-DARTY90-TRT                        
           MOVE W-DACEM-TRT       TO EC64-NB-DACEM-TRT                          
           MOVE W-DARTY95-TRT     TO EC64-NB-DARTY95-TRT                        
           MOVE TOP-ERREUR-MEC64  TO COMM-MEC64-CODE-RETOUR                     
           .                                                                    
       FIN-ACTION-VALID-EC06.  EXIT.                                            
      ******************************                                            
       ACTION-CONTROLE-GV SECTION.                                              
      ****************************                                              
           MOVE  1      TO I-NL                                                 
           MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GA00-NCODIC                      
           PERFORM SELECT-RTGA00                                                
           MOVE GA00-CFAM   TO  COMM-MEC64-TAB-CFAM(I-NL)                       
LA1807*    IF GA00-WDACEM = 'O'                                                 
LA1807*       SET COMM-MEC64-CDEDACEM(I-NL) TO TRUE                             
LA1807*       IF (COMM-MEC64-TAB-DDELIVO(I-NL) = COMM-MEC64-DJOUR)              
LA1807*          MOVE '0001'                TO  PTMG-NSEQERR                    
LA1807*          MOVE W-DATE-J-PLUS1 (7:2)  TO  PTMG-ALP (1) (1:2)              
LA1807*          MOVE '/'                   TO  PTMG-ALP (1) (3:1)              
LA1807*          MOVE W-DATE-J-PLUS1 (5:2)  TO  PTMG-ALP (1) (4:2)              
LA1807*          MOVE '/'                   TO  PTMG-ALP (1) (6:1)              
LA1807*          MOVE W-DATE-J-PLUS1 (3:2)  TO  PTMG-ALP (1) (7:2)              
LA1807*          PERFORM MLIBERRGEN                                             
LA1807*       ELSE                                                              
LA1807*          SET COMM-EC68-CONTROLE TO TRUE                                 
LA1807*          PERFORM APPEL-MEC68                                            
LA1807*          MOVE COMM-EC68-LMESSAGE  TO COMM-MEC64-MESSAGE                 
LA1807*       END-IF                                                            
LA1807*    ELSE                                                                 
LA1807     IF GA00-WDACEM = 'N'                                                 
              SET COMM-MEC64-CDEDARTY(I-NL) TO TRUE                             
                 PERFORM CHERCHE-LIEU-STOCKAGE                                  
                 SET COMM-EC67-CTRL-MGV64 TO TRUE                               
                 SET COMM-EC67-TRANS-GV   TO TRUE                               
                 MOVE '+' TO COMM-MEC64-MAJ-STOCK                               
                 PERFORM APPEL-MEC67                                            
                 IF COMM-EC67-CODRET = '0000'                                   
                 OR '6523'                                                      
                    IF COMM-EC67-STOCK-FOURN-OUI                                
                       SET EC64-ARTICLE-EN-CF(I-NL) TO TRUE                     
                       IF COMM-MEC64-TAB-DDELIVO(I-NL)                          
                       >  COMM-EC67-DDELIV                                      
                          MOVE COMM-MEC64-TAB-DDELIVO(I-NL)                     
                          TO   COMM-EC67-DDELIV                                 
                       END-IF                                                   
                    ELSE                                                        
                       IF COMM-EC67-DDELIV = COMM-EC67-SSAAMMJJ                 
                          MOVE W-DATE-J-PLUS1 TO COMM-EC67-DDELIV               
                       END-IF                                                   
                    END-IF                                                      
                    PERFORM ALIM-MEC69-CONTROLE                                 
                    PERFORM APPEL-MEC69                                         
                    IF MEC64-PAS-ERREUR                                         
                       IF COMM-EC69-DMUTATION NOT =                             
                          COMM-MEC64-TAB-DDELIVO(I-NL)                          
                          MOVE '0002'          TO  PTMG-NSEQERR                 
                          MOVE COMM-EC69-DMUTATION (7:2)                        
                                         TO  PTMG-ALP (1) (1:2)                 
                          MOVE '/'       TO  PTMG-ALP (1) (3:1)                 
                          MOVE COMM-EC69-DMUTATION (5:2)                        
                                         TO  PTMG-ALP (1) (4:2)                 
                          MOVE '/'       TO  PTMG-ALP (1) (6:1)                 
                          MOVE COMM-EC69-DMUTATION (3:2)                        
                                         TO  PTMG-ALP (1) (7:2)                 
                          PERFORM MLIBERRGEN                                    
                       ELSE                                                     
                          MOVE COMM-EC69-NMUTATION                              
                               TO COMM-MEC64-TAB-NMUTTEMP(I-NL)                 
                       END-IF                                                   
                    ELSE                                                        
                       IF (COMM-EC69-CODRET = '6904' OR '6523')                 
                       AND (COMM-EC69-DMUTATION  =                              
                           COMM-MEC64-TAB-DDELIVO(I-NL))                        
                          MOVE SPACE  TO COMM-MEC64-MESSAGE                     
                          MOVE '0000' TO TOP-ERREUR-MEC64                       
                          MOVE COMM-EC69-NMUTATION                              
                               TO COMM-MEC64-TAB-NMUTTEMP(I-NL)                 
                       ELSE                                                     
                          MOVE COMM-EC69-LMESSAGE  TO COMM-MEC64-MESSAGE        
                       END-IF                                                   
                    END-IF                                                      
                 ELSE                                                           
                    MOVE COMM-EC67-LMESSAGE  TO COMM-MEC64-MESSAGE              
                 END-IF                                                         
           END-IF                                                               
           MOVE TOP-ERREUR-MEC64   TO COMM-MEC64-CODE-RETOUR                    
           .                                                                    
       FIN-ACTION-CONTROLE-GV. EXIT.                                            
      ******************************                                            
       ACTION-VALIDE-GV SECTION.                                                
      ****************************                                              
           MOVE  1      TO I-NL                                                 
           MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GA00-NCODIC                      
           PERFORM SELECT-RTGA00                                                
RESAJ         PERFORM MUT-SL300-O-N                                             
LA1807* DEBUT MISE EN COMMENTAIRE                                               
      *    IF GA00-WDACEM = 'O'                                                 
      *       SET COMM-MEC64-CDEDACEM(I-NL) TO TRUE                             
      *       INITIALIZE COMM-EC68-APPLI                                        
      *       MOVE ' NN' TO COMM-EC68-NEW-MODE                                  
      *       SET COMM-EC68-NEW-APPEL TO TRUE                                   
RESAJ *       MOVE WS-MUT-A-J TO COMM-EC68-RESA-A-J                             
      *       MOVE 'N'       TO COMM-EC68-MUT-SL300                             
      *       MOVE COMM-MEC64-ACT-DACEM  TO COMM-EC68-ACT-DACEM                 
      *       IF COMM-MEC64-ACT-DACEM = 'S'                                     
      *         PERFORM SELECT-RTGV11-NQ                                        
      *         MOVE GV11-LCOMMENT(14:8)       TO COMM-EC68-DMUT                
      *         MOVE GV11-LCOMMENT(23:4)       TO COMM-EC68-FILIERE-IN          
      *         MOVE GV11-LCOMMENT (6:7)       TO COMM-EC68-NMUTTEMP            
      *       ELSE                                                              
      *          MOVE COMM-MEC64-DMUT TO COMM-EC68-DMUT                         
      *          MOVE COMM-MEC64-FILIERE-IN TO COMM-EC68-FILIERE-IN             
      *       END-IF                                                            
      *       IF COMM-MEC64-TYPE-NB-MONO                                        
      *          SET COMM-EC68-MUT-UNIQUE TO TRUE                               
      *       ELSE                                                              
      *          SET COMM-EC68-MUT-MULTI  TO TRUE                               
      *       END-IF                                                            
      *       PERFORM APPEL-MEC68                                               
      *       IF COMM-MEC64-ACT-DACEM = 'C'                                     
      *          MOVE COMM-EC68-NMUTATION                                       
      *                              TO COMM-MEC64-TAB-NMUTTEMP(I-NL)           
      *          MOVE COMM-EC68-DATE-DISPO                                      
      *                              TO COMM-MEC64-DATE-DISPO                   
      *          MOVE COMM-EC68-FILIERE    TO COMM-MEC64-FILIERE                
      *       END-IF                                                            
      *    ELSE                                                                 
LA1807*  FIN MISE EN COMMENTAIRE                                                
LA1807     IF GA00-WDACEM = 'N'                                                 
              SET COMM-MEC64-CDEDARTY(I-NL) TO TRUE                             
              PERFORM CHERCHE-LIEU-STOCKAGE                                     
              INITIALIZE COMM-EC69-APPLI                                        
              IF (COMM-MEC64-MAJ-STOCK = '-')                                   
                 SET COMM-EC69-CTRL-GV           TO TRUE                        
                 MOVE COMM-MEC64-TAB-NCODIC(1)   TO COMM-EC69-NCODIC            
                 MOVE COMM-MEC64-NVENTE    TO COMM-EC69-NVENTE                  
                 MOVE COMM-MEC64-NSOCIETE  TO COMM-EC69-NSOCIETE                
                 MOVE COMM-MEC64-NLIEU     TO COMM-EC69-NLIEU                   
                 MOVE COMM-MEC64-DJOUR     TO COMM-EC69-SSAAMMJJ                
                 MOVE COMM-MEC64-FL05-DEPOT (I-NL, 1)                           
                              TO COMM-EC69-FL05-DEPOT (1)                       
RESAJ            MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                          
THEMIS           IF COMMANDE-PRECO                                              
                   SET EC69-COMMANDE-PRECO TO TRUE                              
                 END-IF                                                         
                 PERFORM APPEL-MEC69                                            
              END-IF                                                            
              IF COMM-EC69-DMUTATION = SPACES OR LOW-VALUES                     
                 INITIALIZE COMM-EC67-APPLI                                     
                 SET COMM-EC67-NEW-APPEL     TO TRUE                            
                 MOVE COMM-MEC64-MAJ-STOCK   TO COMM-EC67-MAJ-STOCK             
                 PERFORM APPEL-MEC67                                            
                 IF (COMM-EC67-STOCK-DISPONIBLE)                                
                 OR (COMM-MEC64-MAJ-STOCK = '-')                                
                    IF COMM-EC67-STOCK-FOURN-OUI                                
                       SET EC64-STOCK-ATT-FOURN(I-NL) TO TRUE                   
                       SET EC64-STOCK-FOURN-OUI(I-NL) TO TRUE                   
                       SET EC64-ARTICLE-EN-CF(I-NL) TO TRUE                     
                    ELSE                                                        
                       SET EC64-STOCK-FOURN-NON(I-NL) TO TRUE                   
                       SET EC64-ARTICLE-PAS-EN-CF(I-NL) TO TRUE                 
                    END-IF                                                      
                    IF (COMM-MEC64-MAJ-STOCK = '-')                             
                       PERFORM SELECT-RTGV11-NQ                                 
                       MOVE GV11-LCOMMENT (6:7) TO                              
                            COMM-MEC64-TAB-NMUTTEMP(1)                          
                    END-IF                                                      
                    PERFORM ALIM-MEC69-GV                                       
                    PERFORM APPEL-MEC69                                         
                    IF COMM-EC69-CRE-GB05-TEMPO = 'O'                           
                       MOVE COMM-EC69-NMUTTEMP                                  
                                 TO COMM-MEC64-TAB-NMUTTEMP(I-NL)               
                    ELSE                                                        
                       MOVE COMM-EC69-NMUTATION                                 
                                 TO COMM-MEC64-TAB-NMUTTEMP(I-NL)               
                    END-IF                                                      
                 ELSE                                                           
                    SET EC64-AUCUN-STOCK-DISPO(I-NL) TO TRUE                    
                 END-IF                                                         
              END-IF                                                            
           END-IF                                                               
           PERFORM TRT-RETOUR-REQ-VALIDATION                                    
           .                                                                    
       FIN-ACTION-VALIDE-GV. EXIT.                                              
      ******************************                                            
       ACTION-SORTIE  SECTION.                                                  
      **************************                                                
           SET TRAITEMENT-OK TO TRUE                                            
           SET EC64-STATUT-COMPLETE TO TRUE                                     
           PERFORM DELETE-RTSL20                                                
           PERFORM DELETE-RTSL21-TOT                                            
           .                                                                    
       FIN-ACTION-SORTIE. EXIT.                                                 
      ***************************                                               
       ACTION-SET-INCOMPLETE  SECTION.                                          
      ******************************                                            
      *   ON POSITIONNE LA VENTE COMME INCOMPLETE                               
      *   - DANS RTSL20 (CREATION DE L'ENREG)                                   
      *   - DANS RTGV11 ET RTGV22 (MAJ CEQUIPE � 'MEC64' PLUS MAJ               
      *   - DU CSTATUT ET WCQERESF)                                             
           MOVE 0 TO W-NB-LIGNE-SL21.                                           
           SET TRAITEMENT-OK TO TRUE.                                           
           PERFORM DELETE-RTSL21-TOT                                            
           IF W-THEMIS = 'O'                                                    
             PERFORM RECHERCHE-MUT-MUCEN                                        
           ELSE                                                                 
             PERFORM RECHERCHE-MUT-MEC65                                        
           END-IF                                                               
           PERFORM VARYING I-NL FROM 1 BY 1                                     
           UNTIL COMM-MEC64-TAB-NCODIC(I-NL) <= SPACE                           
              IF NOT EC64-ARTICLE-MUTE (I-NL)                                   
                 PERFORM RECHERCHE-DONNEES-AV-RESA                              
                 IF COMM-MEC64-CDEDARTY(I-NL)                                   
      * SI L'ARTICLE EST DEJA MUTE, ON NE LE TRAITE PAS                         
                   MOVE GV11-LCOMMENT(6 : 7) TO GB05-NMUTATION                  
                   PERFORM SELECT-MUTATION                                      
      * ON RECHERCHE LA MUTATION, SI LA MUTATION A ETE VALIDEE, ON NE           
      * FAIT RIEN, C'EST TROP TARD, SI NON, ON APPEL LE MGV65 POUR              
      * RESERVER LES ARTICLES DISPONIBLES                                       
                   IF TROUVE AND MUTATION-FINAL AND  MUT-VALIDE                 
                      CONTINUE                                                  
                   ELSE                                                         
                     MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GA00-NCODIC            
                     PERFORM SELECT-RTGA00                                      
                     PERFORM CHERCHE-LIEU-STOCKAGE                              
THEMIS* IMPACT THEMIS ?                                                         
                     PERFORM ALIM-MEC69-INC                                     
                     PERFORM APPEL-MEC69                                        
                     PERFORM TRT-RETOUR-MEC69-INC                               
                     MOVE COMM-MEC64-FL05-NSOCDEPOT (I-NL, 1 )                  
                                          TO GV11-NSOCLIVR                      
                     MOVE COMM-MEC64-FL05-NDEPOT (I-NL, 1 )                     
                                          TO GV11-NDEPOT                        
                   END-IF                                                       
LA1807*          ELSE                                                           
      *        DACEM : ENVOYER RESA A DDELIV COMMUNE                            
LA1807*            IF GV11-LCOMMENT(6 : 7) NOT = SPACES OR LOW-VALUES           
LA1807*                INITIALIZE COMM-EC68-APPLI                               
LA1807*                MOVE ' NNNNNNN' TO COMM-EC68-NEW-MODE                    
LA1807*                MOVE 'S' TO COMM-EC68-ACT-DACEM                          
LA1807*                IF GV11-LCOMMENT(13:1) = SPACE OR LOW-VALUE              
LA1807*                   MOVE GV11-LCOMMENT(14:8) TO                           
LA1807*                             COMM-MEC64-TAB-DDELIVO(I-NL)                
LA1807*                             COMM-EC68-DMUT                              
LA1807*                   MOVE GV11-LCOMMENT(23:2) TO COMM-MEC64-FILIERE        
LA1807*                ELSE                                                     
LA1807*                   MOVE GV11-LCOMMENT(13:8) TO                           
LA1807*                             COMM-MEC64-TAB-DDELIVO(I-NL)                
LA1807*                             COMM-EC68-DMUT                              
LA1807*                   MOVE GV11-LCOMMENT(22:2) TO COMM-MEC64-FILIERE        
LA1807*                END-IF                                                   
LA1807*                MOVE GV11-LCOMMENT(6:7) TO COMM-EC68-NMUTTEMP            
LA1807*                IF COMM-MEC64-TYPE-NB-MULTI                              
LA1807*                   SET COMM-EC68-MUT-MULTI TO TRUE                       
LA1807*                ELSE                                                     
LA1807*                   SET COMM-EC68-MUT-UNIQUE TO TRUE                      
LA1807*                END-IF                                                   
LA1807*                PERFORM APPEL-MEC68                                      
LA1807*            END-IF                                                       
LA1807*                                                                         
LA1807*            INITIALIZE COMM-EC68-APPLI                                   
LA1807*            MOVE ' NN' TO COMM-EC68-NEW-MODE                             
LA1807*            SET COMM-EC68-NEW-APPEL TO TRUE                              
LA1807*            SET COMM-EC68-CREATION-DACEM TO TRUE                         
LA1807*            MOVE 'N'       TO COMM-EC68-RESA-A-J                         
LA1807*            MOVE 'N'       TO COMM-EC68-MUT-SL300                        
LA1807*            MOVE COMM-MEC64-DDELIV                                       
LA1807*                           TO COMM-MEC64-TAB-DDELIVO(I-NL)               
LA1807*                              COMM-EC68-DMUT                             
LA1807*            MOVE MEC65-FILIERE TO COMM-MEC64-FILIERE                     
LA1807*            SET COMM-EC68-MUT-MULTI TO TRUE                              
LA1807*            PERFORM APPEL-MEC68                                          
LA1807*            MOVE SPACES   TO  GV11-LCOMMENT(6:20)                        
LA1807*            IF COMM-EC68-CODRET NOT = '0000'                             
LA1807*               SET TRAITEMENT-KO       TO TRUE                           
LA1807*               MOVE COMM-EC68-LMESSAGE TO COMM-MEC64-MESSAGE             
LA1807*               MOVE SPACES TO COMM-EC68-NMUTATION                        
LA1807*               MOVE SPACES TO COMM-EC68-DATE-DISPO                       
LA1807*               MOVE SPACES TO COMM-EC68-FILIERE                          
LA1807*               IF AUTO-DISPLAY                                           
LA1807*                  STRING 'TRT KO SUR SET INCOMPL DACEM '                 
LA1807*                  COMM-EC68-CODRET                                       
LA1807*                   DELIMITED BY SIZE INTO W-MESSAGE                      
LA1807*                   PERFORM DISPLAY-LOG                                   
LA1807*               END-IF                                                    
LA1807*               MOVE 'A' TO GV22-CSTATUT                                  
LA1807*               PERFORM INSERT-RTSL21                                     
LA1807*            ELSE                                                         
LA1807*               MOVE SPACE        TO GV22-NMUTATION                       
LA1807*               MOVE 'R'  TO GV22-CSTATUT                                 
LA1807*               PERFORM INSERT-RTSL21                                     
LA1807*            END-IF                                                       
                 END-IF                                                         
                 PERFORM TRT-MAJ-SET-INCOMP                                     
              END-IF                                                            
           END-PERFORM                                                          
CEN012*    IF  W-NB-LIGNE-SL21 > 0                                              
              PERFORM SELECT-RTSL20                                             
              IF TROUVE                                                         
      *  ON LE PASSE A KO S'IL EXISTE DEJA                                      
                 PERFORM UPDATE-RTSL20                                          
              ELSE                                                              
CEN012           IF  W-NB-LIGNE-SL21 = 0                                        
CEN012              MOVE 'OK' TO W-SL20-STATUT                                  
CEN012           ELSE                                                           
                    MOVE 'IN' TO W-SL20-STATUT                                  
CEN012           END-IF                                                         
                 PERFORM INSERT-RTSL20                                          
              END-IF                                                            
CEN012*    END-IF                                                               
           .                                                                    
       FIN-ACTION-SET-INCOMPLETE. EXIT.                                         
      ******************************                                            
LA1807*ACTION-MAJ-DDELIV SECTION.                                               
LA1807***************************                                               
LA1807*                                                                         
LA1807*    MOVE ZEROES   TO WN-NB-TOPE                                          
LA1807*    IF COMM-MEC64-TYPE-NB-MULTI                                          
LA1807*       SET TRAITEMENT-OK TO TRUE                                         
      * DANS LE CAS D'UNE ANOMALIE DE RECEPTION POUR UNE VENTE MULTI            
      * DARTY - DACEM, SI LE PRODUIT DACEM EST DISPO, LA DATE COMMUNE           
      * EST EGALE A LA DATE DE MUTATION LA PLUS LOINTAINE DES PRODUITS          
      * DE LA VENTE                                                             
LA1807*       PERFORM SELECT-RTSL20                                             
LA1807*       IF EC64-MULTIDA = 'O' OR 'N'                                      
LA1807*          IF TROUVE AND JPLUS15-KO                                       
LA1807*          AND SL20-DATERESERVATION = '00000000'                          
LA1807*             PERFORM SELECT-GV11-GB05                                    
LA1807*             IF TROUVE                                                   
LA1807*                MOVE GB05-DMUTATION TO COMM-MEC64-DATE-MAJ               
LA1807*             END-IF                                                      
LA1807*          END-IF                                                         
LA1807*       END-IF                                                            
LA1807*                                                                         
LA1807*       IF W-THEMIS = 'O'                                                 
LA1807*         PERFORM RECHERCHE-MUT-MUCEN                                     
LA1807*       ELSE                                                              
LA1807*         PERFORM RECHERCHE-MUT-MEC65                                     
LA1807*       END-IF                                                            
LA1807*                                                                         
LA1807*       IF (MEC65-CDRET-OK)                                               
LA1807*       OR (EC64-MULTIDA = 'N')                                           
LA1807*          PERFORM VARYING I-NL FROM 1 BY 1 UNTIL I-NL > I-NL-MAX         
LA1807*          OR COMM-MEC64-CODE-RETOUR > '0000'                             
LA1807*          OR COMM-MEC64-TAB-NCODIC(I-NL) = SPACE                         
LA1807*             IF COMM-MEC64-TAB-TOPE(I-NL) NOT = 'O'                      
LA1807*                IF (SL20-INCOMPLETE = 'O')                               
LA1807*                   SET CODIC-TRT-OK  TO TRUE                             
LA1807*                ELSE                                                     
LA1807*                   PERFORM SELECT-RTSL21                                 
LA1807*                END-IF                                                   
LA1807*                IF CODIC-TRT-OK                                          
      * ON APPEL LE MEC68 EN MODE MAJ-DACEM POUR DECALER LA DATE DE             
      * RESERVATION CHEZ DACEM POUR TOUS LES ARTICLES DACEM RESERVES            
LA1807*                   PERFORM MUT-SL300-O-N                                 
LA1807*                   IF EC64-STOCK-DACEM(I-NL)                             
LA1807*                      MOVE ' NNNNNNN' TO COMM-EC68-NEW-MODE              
LA1807*                      SET COMM-EC68-NEW-APPEL TO TRUE                    
LA1807*                      IF COMM-MEC64-TAB-LCOMMENT(I-NL)(6:7)              
LA1807*                      NOT = SPACES AND LOW-VALUES                        
LA1807*                         MOVE 'S' TO COMM-EC68-ACT-DACEM                 
LA1807*                         MOVE COMM-MEC64-TAB-LCOMMENT(I-NL)              
LA1807*                                             TO WS-LCOMMENT              
LA1807*                         MOVE WS-LCOMMENT(6:7)                           
LA1807*                                            TO COMM-EC68-NMUTTEMP        
      * ON A DECALE LA DATE DE RESERVATION DACEM DANS LCOMMENT ON               
      * VA DONC CHERCHER LA DATE A L'ANCIENNE OU A LA NOUVELLE PLACE            
LA1807*                         IF WS-LCOMMENT(13:1) = SPACE                    
LA1807*                            MOVE WS-LCOMMENT(14:8)                       
LA1807*                                                TO COMM-EC68-DMUT        
LA1807*                            MOVE WS-LCOMMENT(23:2)                       
LA1807*                                          TO COMM-EC68-FILIERE-IN        
LA1807*                         ELSE                                            
LA1807*                            MOVE WS-LCOMMENT(13:8)                       
LA1807*                                                TO COMM-EC68-DMUT        
LA1807*                            MOVE WS-LCOMMENT(22:2)                       
LA1807*                                          TO COMM-EC68-FILIERE-IN        
LA1807*                         END-IF                                          
LA1807*                         PERFORM APPEL-MEC68                             
LA1807*                      ELSE                                               
LA1807*                         MOVE '0000'   TO COMM-EC68-CODRET               
LA1807*                      END-IF                                             
LA1807*                      MOVE COMM-EC68-CODRET TO TOP-ERREUR-MEC64          
LA1807*                      IF COMM-EC68-CODRET NOT = '0000'                   
LA1807*                         IF AUTO-DISPLAY                                 
LA1807*                            STRING 'MEC64 - MAJ DDELIV ANO '             
LA1807*                            'SUP DACEM, VENTE : '                        
LA1807*                            COMM-MEC64-NVENTE                            
LA1807*                            ' CODIC : '                                  
LA1807*                            COMM-MEC64-TAB-NCODIC(I-NL)                  
LA1807*                            DELIMITED BY SIZE INTO W-MESSAGE             
LA1807*                            PERFORM DISPLAY-LOG                          
LA1807*                         END-IF                                          
LA1807*                         SET TRAITEMENT-KO TO TRUE                       
LA1807*                      ELSE                                               
LA1807*                         SET COMM-EC68-MUT-MULTI  TO TRUE                
LA1807*                         MOVE COMM-MEC64-TAB-DDELIVO(I-NL)               
LA1807*                                          TO COMM-EC68-DDELIV            
LA1807*                                             COMM-EC68-DMUT              
LA1807*                         MOVE 'N'           TO COMM-EC68-RESA-A-J        
LA1807*                         MOVE WS-MUT-SL300                               
LA1807*                                         TO COMM-EC68-MUT-SL300          
LA1807*                         MOVE MEC65-FILIERE                              
LA1807*                         TO COMM-EC68-FILIERE-IN                         
LA1807*                         MOVE 'C' TO COMM-EC68-ACT-DACEM                 
LA1807*                         PERFORM APPEL-MEC68                             
LA1807*                         MOVE COMM-EC68-CODRET                           
LA1807*                                         TO TOP-ERREUR-MEC64             
LA1807*                         IF COMM-EC68-CODRET NOT = '0000'                
LA1807*                            IF MEC64-DAC-PAS-DISPO                       
LA1807*                               PERFORM TRT-MAJ-DDELIV-INI                
LA1807*                            ELSE                                         
LA1807*                               SET TRAITEMENT-KO TO TRUE                 
LA1807*                               IF AUTO-DISPLAY                           
LA1807*                                 STRING 'TRT KO SUR'                     
LA1807*                                 ' MAJ DDELIV, CODE '                    
LA1807*                                 COMM-EC68-CODRET                        
LA1807*                                 DELIMITED BY SIZE INTO W-MESSAGE        
LA1807*                                 PERFORM DISPLAY-LOG                     
LA1807*                               END-IF                                    
LA1807*                            END-IF                                       
LA1807*                         ELSE                                            
LA1807*                            PERFORM TRT-MAJ-CMD-DAC                      
LA1807*                         END-IF                                          
LA1807*                      END-IF                                             
LA1807*                   ELSE                                                  
LA1807*                      IF EC64-STOCK-DISPONIBLE(I-NL) OR                  
LA1807*                         EC64-STOCK-ATT-FOURN(I-NL)                      
LA1807*                         MOVE COMM-MEC64-TAB-NCODIC(I-NL)                
LA1807*                                                   TO GA00-NCODIC        
LA1807*                         PERFORM SELECT-RTGA00                           
LA1807*                         PERFORM ALIM-MEC69-MAJ-DATE                     
LA1807*                         PERFORM APPEL-MEC69                             
LA1807*                         IF COMM-EC69-CODRET NOT = '0000'                
LA1807*                            SET TRAITEMENT-KO TO TRUE                    
LA1807*                            IF AUTO-DISPLAY                              
LA1807*                            STRING 'TRT KO SUR MAJ DDELIV, CODE '        
LA1807*                            COMM-EC69-CODRET                             
LA1807*                            DELIMITED BY SIZE INTO W-MESSAGE             
LA1807*                            PERFORM DISPLAY-LOG                          
LA1807*                            END-IF                                       
LA1807*                         ELSE                                            
LA1807*                           PERFORM TRT-MAJ-NUM-MUT                       
LA1807*                         END-IF                                          
LA1807*                      END-IF                                             
LA1807*                   END-IF                                                
LA1807*                END-IF                                                   
LA1807*             ELSE                                                        
LA1807*                ADD   1    TO WN-NB-TOPE                                 
LA1807*             END-IF                                                      
LA1807*         END-PERFORM                                                     
LA1807*                                                                         
LA1807*         IF I-NL-MAX > WN-NB-TOPE                                        
LA1807*            IF TRAITEMENT-OK                                             
LA1807*                PERFORM UPDATE-RTSL20-DRES                               
LA1807*            ELSE                                                         
LA1807*               MOVE TOP-ERREUR-MEC64  TO COMM-MEC64-CODE-RETOUR          
LA1807*            END-IF                                                       
LA1807*         ELSE                                                            
LA1807*            IF AUTO-DISPLAY                                              
LA1807*               STRING 'MEC64 MAJ DDELIV KO - VENTE DEJA TOPE '           
LA1807*               COMM-MEC64-NVENTE                                         
LA1807*               DELIMITED BY SIZE INTO W-MESSAGE                          
LA1807*               PERFORM DISPLAY-LOG                                       
LA1807*            END-IF                                                       
LA1807*            MOVE '9999'            TO COMM-MEC64-CODE-RETOUR             
LA1807*         END-IF                                                          
LA1807*       ELSE                                                              
LA1807*          IF AUTO-DISPLAY                                                
LA1807*             STRING 'MEC64 MAJ DDELIV KO - CODE RET MEC65 '              
LA1807*             MEC65-CODE-RET ' VENTE ' COMM-MEC64-NVENTE                  
LA1807*             DELIMITED BY SIZE INTO W-MESSAGE                            
LA1807*             PERFORM DISPLAY-LOG                                         
LA1807*          END-IF                                                         
LA1807*          MOVE MEC65-CODE-RET      TO  COMM-MEC64-CODE-RETOUR            
LA1807*       END-IF                                                            
LA1807*    ELSE                                                                 
LA1807*       IF AUTO-DISPLAY                                                   
LA1807*          STRING 'MEC64 MAJ DDELIV KO - VENTE MONO '                     
LA1807*          COMM-MEC64-NVENTE                                              
LA1807*          DELIMITED BY SIZE INTO W-MESSAGE                               
LA1807*          PERFORM DISPLAY-LOG                                            
LA1807*       END-IF                                                            
LA1807*       MOVE '9999'    TO COMM-MEC64-CODE-RETOUR                          
LA1807*       PERFORM UPDATE-RTSL20-DRES                                        
LA1807*    END-IF                                                               
LA1807*    .                                                                    
LA1807*FIN-ACTION-MAJ-DDELIV. EXIT.                                             
LA1807*****************************                                             
LA1807*ACTION-DEL-DDELIV SECTION.                                               
LA1807***************************                                               
LA1807*                                                                         
LA1807*    MOVE ' NNNNNNN' TO COMM-EC68-NEW-MODE                                
LA1807*    SET COMM-EC68-NEW-APPEL TO TRUE                                      
LA1807*    MOVE 'S' TO COMM-EC68-ACT-DACEM                                      
LA1807*    MOVE COMM-MEC64-TAB-NMUTTEMP(1) TO COMM-EC68-NMUTTEMP                
LA1807*                                                                         
LA1807*    IF WS-LCOMMENT(13:1) = SPACE                                         
LA1807*      MOVE WS-LCOMMENT(14:8)     TO COMM-EC68-DMUT                       
LA1807*      MOVE WS-LCOMMENT(23:2)     TO COMM-EC68-FILIERE-IN                 
LA1807*    ELSE                                                                 
LA1807*      MOVE WS-LCOMMENT(13:8)     TO COMM-EC68-DMUT                       
LA1807*      MOVE WS-LCOMMENT(22:2)     TO COMM-EC68-FILIERE-IN                 
LA1807*    END-IF                                                               
LA1807*    IF COMM-EC68-FILIERE-IN = SPACE                                      
LA1807*       MOVE W-FILIERE TO COMM-EC68-FILIERE-IN                            
LA1807*    END-IF                                                               
LA1807*    IF COMM-EC68-DMUT = SPACE                                            
LA1807*       MOVE COMM-MEC64-DJOUR TO COMM-EC68-DMUT                           
LA1807*    END-IF                                                               
LA1807*    MOVE COMM-MEC64-TAB-DDELIVO(1)    TO COMM-MEC64-DDELIV               
LA1807*    PERFORM APPEL-MEC68                                                  
LA1807*    .                                                                    
LA1807*FIN-ACTION-DEL-DDELIV. EXIT.                                             
      *****************************                                             
       TRT-MAJ-VALIDATION  SECTION.                                             
      ******************************                                            
           MOVE 'MGV66' TO GV11-CEQUIPE                                         
           MOVE 'MGV66' TO GV22-CEQUIPE                                         
           MOVE 'MGV66' TO COMM-MEC64-TAB-CEQUIPE(I-NL)                         
           MOVE W-SPDATDB2-DSYST               TO GV11-DSYST                    
           IF NOT COMM-MEC64-ACTION-VALID-DATE                                  
              MOVE COMM-MEC64-DDELIV-COM          TO GV11-DDELIV                
           END-IF                                                               
           SET EC64-ARTICLE-RESERVE(I-NL) TO TRUE                               
           MOVE GV11-LCOMMENT TO COMM-MEC64-TAB-LCOMMENT(I-NL)                  
           MOVE SPACE                          TO GV11-WCQERESF                 
           MOVE SPACE                          TO GV22-WCQERESF                 
           MOVE COMM-MEC64-TAB-WCQERESF(I-NL)  TO GV11-WCQERESF                 
                                                  GV22-WCQERESF                 
           IF GV11-LCOMMENT(1:5) = '     '                                      
              MOVE GV11-NCODIC      TO GA00-NCODIC                              
              PERFORM SELECT-RTGA00                                             
LA1807*       IF GA00-WDACEM = 'O'                                              
LA1807*          MOVE 'DACEM'  TO GV11-LCOMMENT (1:5)                           
LA1807*       ELSE                                                              
LA1807        IF GA00-WDACEM = 'N'                                              
                 MOVE 'DARTY'  TO GV11-LCOMMENT (1:5)                           
              END-IF                                                            
           END-IF                                                               
           IF GV11-LCOMMENT(1:5) = 'DARTY'                                      
              MOVE COMM-EC69-NMUTATION      TO GV11-LCOMMENT (6:7)              
              MOVE COMM-EC69-NMUTATION         TO GV22-NMUTATION                
              MOVE COMM-EC69-NSOCDEPOTO        TO GV11-NSOCLIVR                 
              MOVE COMM-EC69-NDEPOTO           TO GV11-NDEPOT                   
              IF GV11-LCOMMENT(16:3) = '095'                                    
                 ADD 1 TO W-DARTY95-TRT                                         
              END-IF                                                            
              IF GV11-LCOMMENT(16:3) = '090'                                    
                 ADD 1 TO W-DARTY90-TRT                                         
              END-IF                                                            
LA1807*    ELSE                                                                 
LA1807*       MOVE SPACES TO GV11-LCOMMENT(6:20)                                
LA1807*       MOVE COMM-EC68-NMUTATION   TO GV11-LCOMMENT (6:7)                 
LA1807*       MOVE COMM-EC68-DATE-DISPO TO                                      
LA1807*                     GV11-LCOMMENT(14:8)                                 
LA1807*       MOVE COMM-EC68-FILIERE  TO                                        
LA1807*                     GV11-LCOMMENT(23:4)                                 
LA1807*       ADD 1 TO W-DACEM-TRT                                              
LA1807*       MOVE SPACE        TO GV22-NMUTATION                               
           END-IF                                                               
           PERFORM UPDATE-GV1100-ENC                                            
           IF COMM-MEC64-CPROG   = 'MGV66'                                      
              PERFORM LOG-GV23                                                  
           END-IF                                                               
           IF TRAITEMENT-OK                                                     
              IF NOT EC64-ARTICLE-MUTE(I-NL)                                    
                 MOVE 'R'   TO GV22-CSTATUT                                     
                 MOVE GV11-NDEPOT    TO GV22-NDEPOT                             
                 MOVE GV11-NSOCLIVR  TO GV22-NSOCLIVR                           
              END-IF                                                            
              PERFORM UPDATE-RTGV22                                             
           END-IF                                                               
            .                                                                   
       FIN-TRT-MAJ-VALIDATION. EXIT.                                            
      ******************************                                            
       TRT-MAJ-NUM-MUT  SECTION.                                                
      ******************************                                            
           IF NOT EC64-DEJA-MUTE(I-NL)                                          
              PERFORM SELECT-RTGV11                                             
              PERFORM SELECT-RTGV22                                             
              MOVE W-SPDATDB2-DSYST     TO GV11-DSYST                           
              MOVE COMM-EC69-NMUTATION  TO GV11-LCOMMENT (6:7)                  
                                           GV22-NMUTATION                       
              PERFORM UPDATE-GV1100                                             
              PERFORM UPDATE-RTGV22                                             
              PERFORM LOG-GV23                                                  
           END-IF                                                               
            .                                                                   
       FIN-TRT-MAJ-NUM-MUT. EXIT.                                               
      ******************************                                            
LA1807*TRT-MAJ-CMD-DAC  SECTION.                                                
LA1807******************************                                            
LA1807*                                                                         
LA1807*    IF NOT EC64-DEJA-MUTE(I-NL)                                          
LA1807*       PERFORM SELECT-RTGV11                                             
LA1807*       MOVE W-SPDATDB2-DSYST           TO GV11-DSYST                     
LA1807*       MOVE SPACES TO GV11-LCOMMENT(6:20)                                
LA1807*       MOVE COMM-EC68-NMUTATION   TO GV11-LCOMMENT (6:7)                 
LA1807*       MOVE COMM-EC68-DATE-DISPO  TO GV11-LCOMMENT(14:8)                 
LA1807*       MOVE COMM-EC68-FILIERE     TO GV11-LCOMMENT(23:4)                 
LA1807*       PERFORM UPDATE-GV1100                                             
LA1807*       PERFORM LOG-GV23                                                  
LA1807*    END-IF                                                               
LA1807*     .                                                                   
LA1807*FIN-TRAIT-MAJ-CMD-DAC. EXIT.                                             
      ******************************                                            
       TRT-CODIC-DARTY-EC06   SECTION.                                          
      ******************************                                            
           MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GA00-NCODIC                      
           PERFORM SELECT-RTGA00                                                
           MOVE GA00-CFAM   TO COMM-MEC64-TAB-CFAM(I-NL)                        
           PERFORM CHERCHE-LIEU-STOCKAGE                                        
           INITIALIZE COMM-EC67-APPLI                                           
           SET COMM-EC67-CONTROLE  TO TRUE                                      
           SET COMM-EC67-CTRL-MGV64 TO TRUE                                     
           PERFORM APPEL-MEC67                                                  
           IF COMM-EC67-STOCK-DISPONIBLE                                        
              IF ((COMM-EC67-STOCK-FOURN-OUI)                                   
              AND (GV11-WCQERESF = 'F'))                                        
                 CONTINUE                                                       
              ELSE                                                              
                 PERFORM ACTION-VALID-CODIC                                     
              END-IF                                                            
              SET EC64-STOCK-DISPONIBLE(I-NL) TO TRUE                           
              MOVE COMM-EC67-ETAT-FOURNISSEUR TO                                
                   COMM-MEC64-TAB-ETAT-FOURN(I-NL)                              
           ELSE                                                                 
              SET EC64-AUCUN-STOCK-DISPO(I-NL) TO TRUE                          
           END-IF                                                               
           .                                                                    
       FIN-TRT-CODIC-DARTY-EC06. EXIT.                                          
      ******************************                                            
LA1807*TRT-CODIC-DACEM-EC06   SECTION.                                          
LA1807******************************                                            
LA1807*    INITIALIZE COMM-EC68-APPLI                                           
LA1807*                                                                         
LA1807*    PERFORM MUT-SL300-O-N                                                
LA1807*    MOVE WS-MUT-SL300        TO COMM-EC68-MUT-SL300                      
LA1807*    MOVE COMM-MEC64-TAB-DDELIVO(I-NL)        TO COMM-EC68-DDELIV         
LA1807*    MOVE SPACE                TO COMM-EC68-FILIERE-IN                    
LA1807*    MOVE SPACE                TO COMM-EC68-DMUT                          
LA1807*                                                                         
LA1807*    SET COMM-EC68-CONTROLE   TO TRUE                                     
LA1807*    MOVE ' NN' TO COMM-EC68-NEW-MODE                                     
LA1807*    MOVE WS-MUT-A-J          TO COMM-EC68-RESA-A-J                       
LA1807*    MOVE COMM-MEC64-TAB-DDELIVO(I-NL)  TO COMM-MEC64-DDELIV-COM          
LA1807*    MOVE COMM-MEC64-NSOCIETE           TO GV11-NSOCIETE                  
LA1807*    MOVE COMM-MEC64-NLIEU              TO GV11-NLIEU                     
LA1807*    MOVE COMM-MEC64-NVENTE             TO GV11-NVENTE                    
LA1807*    PERFORM SELECT-NB-RTGV11                                             
LA1807*    IF WS-NB-PRODUIT = 1                                                 
LA1807*       SET COMM-EC68-MUT-UNIQUE  TO TRUE                                 
LA1807*    ELSE                                                                 
LA1807*       SET COMM-EC68-MUT-MULTI   TO TRUE                                 
LA1807*    END-IF                                                               
LA1807*    PERFORM APPEL-MEC68                                                  
LA1807*    SET EC64-STOCK-FOURN-NON(I-NL)  TO TRUE                              
LA1807*                                                                         
LA1807*    IF (MEC64-PAS-ERREUR)                                                
LA1807*    OR (MEC64-DDELIV-DEP)                                                
LA1807*       MOVE 'D'  TO EC64-MULTIDA                                         
LA1807*       PERFORM RECHERCHE-MUT-MEC65                                       
LA1807*       MOVE 1                   TO I-NL                                  
LA1807*       MOVE MEC65-FILIERE       TO COMM-MEC64-FILIERE                    
LA1807*       MOVE COMM-MEC64-TAB-DDELIVO(I-NL)  TO COMM-EC68-DMUT              
LA1807*       PERFORM MODULE-RESERVATION-DACEM                                  
LA1807*       IF MEC64-PAS-ERREUR                                               
LA1807*          ADD 1   TO W-DACEM-TRT                                         
LA1807*          SET EC64-STOCK-DISPONIBLE(I-NL)   TO TRUE                      
LA1807*       END-IF                                                            
LA1807*    ELSE                                                                 
LA1807*       SET EC64-AUCUN-STOCK-DISPO(I-NL)  TO TRUE                         
LA1807*    END-IF                                                               
LA1807*    .                                                                    
LA1807*FIN-TRT-CODIC-DACEM-EC06. EXIT.                                          
LA1807******************************                                            
       MEF-REQ-VALID-DATE     SECTION.                                          
      ******************************                                            
THEMIS     IF COMMANDE-PRECO                                                    
             SET EC69-COMMANDE-PRECO TO TRUE                                    
           END-IF                                                               
           IF COMM-MEC64-CDEDARTY(I-NL)                                         
               IF COMM-MEC64-FL05-DEPOT(I-NL, 1) > SPACES                       
                  SET COMM-EC69-NEW-APPEL TO TRUE                               
                  MOVE ' NNNNNNN' TO COMM-EC69-NEW-MODE                         
                  MOVE 'O' TO COMM-EC69-CRE-GB05-FINAL                          
                  MOVE 'O' TO COMM-EC69-CRE-GB15-FINAL                          
                  MOVE 'O' TO COMM-EC69-SUP-MUT-GB05                            
                  MOVE COMM-MEC64-TAB-LCOMMENT(I-NL) TO WS-LCOMMENT             
                  MOVE WS-LCOMMENT (6 : 7) TO COMM-EC69-NMUTTEMP                
                  IF EC64-STATUT-PREC-OK                                        
                     MOVE 'O' TO COMM-EC69-SUP-MUT-GB15                         
                  ELSE                                                          
                     MOVE COMM-EC69-NMUTTEMP TO GB05-NMUTATION                  
                     PERFORM SELECT-MUTATION                                    
                     IF MUTATION-TEMPO                                          
                        MOVE 'O' TO COMM-EC69-SUP-MUT-GB15                      
                     END-IF                                                     
                  END-IF                                                        
                  IF COMM-MEC64-FL05-NDEPOT(I-NL, 1) = '095'                    
                     MOVE COMM-MEC64-TAB-NMUT(I-NL) TO COMM-EC69-NMUT           
                  ELSE                                                          
                     MOVE SPACE                     TO COMM-EC69-NMUT           
                  END-IF                                                        
THEMIS*    DANS NMUTATION ON A LE FL05(1) ; DANS NMUTATION2 : FL05(2)           
                  IF W-THEMIS = 'O'                                             
                  AND EC64-MULTIDA = 'O'                                        
                    IF W-ENTR1 = COMM-MEC64-FL05-DEPOT (I-NL, 1)                
                      MOVE W-NMUT1                 TO COMM-EC69-NMUT            
                    END-IF                                                      
                    IF W-ENTR2 = COMM-MEC64-FL05-DEPOT (I-NL, 1)                
                      MOVE W-NMUT2                 TO COMM-EC69-NMUT            
                    END-IF                                                      
                  END-IF                                                        
                  EVALUATE TRUE                                                 
                  WHEN COMM-MEC64-TYPE-NB-MONO                                  
                     SET COMM-EC69-MUT-UNIQUE TO TRUE                           
                     IF COMM-MEC64-ACTION-VALID-DATE                            
                        MOVE WS-MUT-SL300  TO COMM-EC69-MUT-SL300               
                     ELSE                                                       
                        MOVE 'N'           TO COMM-EC69-MUT-SL300               
                     END-IF                                                     
                  WHEN COMM-MEC64-TYPE-NB-MULTI                                 
                     SET COMM-EC69-MUT-MULTI  TO TRUE                           
                     IF COMM-MEC64-ACTION-VALID-DATE                            
                        MOVE WS-MUT-SL300  TO COMM-EC69-MUT-SL300               
                     ELSE                                                       
                        MOVE 'N'           TO COMM-EC69-MUT-SL300               
                     END-IF                                                     
                  END-EVALUATE                                                  
                  IF (EC64-STATUT-COMPLETE AND PAS-COMMANDE-PRECO               
                  AND EC64-MULTIDA = 'N')                                       
                  OR COMM-MEC64-ACTION-VALID-DATE                               
                     IF COMMANDE-PRECO                                          
                     AND COMM-MEC64-TAB-DDELIVO (I-NL) >                        
                                        COMM-MEC64-DJOUR                        
                        MOVE 'N'      TO COMM-EC69-RESA-A-J                     
                     ELSE                                                       
RESAJ *                 display 'mut J1 ' WS-MUT-A-J                            
RESAJ                   MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                   
                     END-IF                                                     
                  ELSE                                                          
RESAJ *                 display 'mut J2 ' WS-MUT-A-J                            
RESAJ                   MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                   
      *              MOVE 'N'      TO COMM-EC69-RESA-A-J                        
                  END-IF                                                        
                  MOVE '0000'                     TO COMM-EC69-CODRET           
                  MOVE COMM-MEC64-NSOCIETE        TO COMM-EC69-NSOCIETE         
                  MOVE COMM-MEC64-NLIEU           TO COMM-EC69-NLIEU            
                  MOVE COMM-MEC64-NVENTE          TO COMM-EC69-NVENTE           
                  MOVE COMM-MEC64-TAB-NSEQNQ(I-NL) TO COMM-EC69-NSEQNQ          
                  MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO COMM-EC69-NCODIC          
                  MOVE COMM-MEC64-NORDRE         TO COMM-EC69-NORDRE            
                  MOVE COMM-MEC64-TAB-DDELIVO (I-NL) TO COMM-EC69-DDELIV        
                  IF COMM-MEC64-DJOUR > COMM-EC69-DDELIV                        
                     MOVE COMM-MEC64-DJOUR   TO COMM-EC69-DDELIV                
                  END-IF                                                        
                  MOVE COMM-MEC64-TAB-QVENDUE(I-NL)  TO COMM-EC69-QTE           
                  MOVE COMM-MEC64-TAB-WCQERESF(I-NL)                            
                                    TO COMM-EC69-WCQERESF                       
                  MOVE COMM-MEC64-TAB-NSOCLIVR(I-NL)                            
                                    TO COMM-EC69-NSOCLIVR                       
                  MOVE COMM-MEC64-TAB-NDEPOT  (I-NL) TO COMM-EC69-NDEPOT        
                  MOVE COMM-MEC64-TAB-LCOMMENT(I-NL)                            
                                    TO COMM-EC69-LCOMMENT                       
                  MOVE COMM-MEC64-TAB-WEMPORTE(I-NL)                            
                                    TO COMM-EC69-WEMPORTE                       
                  MOVE COMM-MEC64-DJOUR         TO COMM-EC69-SSAAMMJJ           
                  MOVE COMM-MEC64-FL05-DEPOT(I-NL, 1)                           
                                    TO COMM-EC69-FL05-DEPOT (1)                 
                  MOVE MDLIV-LDEPLIVR-EXP (1:3) TO COMM-EC69-MDLIV-NSOC         
                  MOVE MDLIV-LDEPLIVR-EXP (4:3) TO COMM-EC69-MDLIV-NLIEU        
PBGB15            MOVE GA00-CMARQ               TO COMM-EC69-GA00-CMARQ         
PBGB15            MOVE GA00-CFAM                TO COMM-EC69-GA00-CFAM          
PBGB15            MOVE GA00-CAPPRO              TO COMM-EC69-GA00-CAPPRO        
PBGB15            MOVE GA00-QLARGEUR       TO COMM-EC69-GA00-QLARGEUR           
PBGB15            MOVE GA00-QPROFONDEUR    TO COMM-EC69-GA00-QPROFONDEUR        
PBGB15            MOVE GA00-QHAUTEUR       TO COMM-EC69-GA00-QHAUTEUR           
PBGB15            MOVE GA00-QPOIDS         TO COMM-EC69-GA00-QPOIDS             
               END-IF                                                           
LA1807*     ELSE                                                                
LA1807*       IF COMM-MEC64-CDEDACEM(I-NL)                                      
LA1807*          INITIALIZE COMM-EC68-ZOUT                                      
LA1807*          SET COMM-EC68-NEW-APPEL TO TRUE                                
LA1807*          MOVE '0000'                        TO COMM-EC68-CODRET         
LA1807*          MOVE COMM-MEC64-NSOCIETE         TO COMM-EC68-NSOCIETE         
LA1807*          MOVE COMM-MEC64-NLIEU              TO COMM-EC68-NLIEU          
LA1807*          MOVE COMM-MEC64-NVENTE             TO COMM-EC68-NVENTE         
LA1807*          MOVE COMM-MEC64-TAB-NSEQNQ  (I-NL) TO COMM-EC68-NSEQNQ         
LA1807*          MOVE COMM-MEC64-TAB-NCODIC  (I-NL) TO COMM-EC68-NCODIC         
LA1807*          MOVE COMM-MEC64-NORDRE             TO COMM-EC68-NORDRE         
LA1807*          MOVE COMM-MEC64-TAB-DDELIVO (I-NL) TO COMM-EC68-DDELIV         
LA1807*          IF COMM-MEC64-DJOUR > COMM-EC68-DDELIV                         
LA1807*             MOVE COMM-MEC64-DJOUR   TO COMM-EC68-DDELIV                 
LA1807*          END-IF                                                         
LA1807*          MOVE COMM-MEC64-TAB-QVENDUE(I-NL)  TO COMM-EC68-QTE            
LA1807*          MOVE COMM-MEC64-TAB-WEMPORTE(I-NL)                             
LA1807*                            TO COMM-EC68-WEMPORTE                        
LA1807*          MOVE COMM-MEC64-DJOUR           TO COMM-EC68-SSAAMMJJ          
LA1807*          EVALUATE TRUE                                                  
LA1807*          WHEN COMM-MEC64-TYPE-NB-MONO                                   
LA1807*             SET COMM-EC68-MUT-UNIQUE TO TRUE                            
LA1807*             IF COMM-MEC64-ACTION-VALID-DATE                             
LA1807*                IF COMMANDE-PRECO                                        
LA1807*                AND COMM-MEC64-TAB-DDELIVO (I-NL) >                      
LA1807*                                 COMM-MEC64-DJOUR                        
LA1807*                   MOVE 'N'           TO COMM-EC68-RESA-A-J              
LA1807*                ELSE                                                     
LA1807*                   MOVE WS-MUT-A-J TO COMM-EC68-RESA-A-J                 
LA1807*                END-IF                                                   
LA1807*                MOVE WS-MUT-SL300  TO COMM-EC68-MUT-SL300                
LA1807*             ELSE                                                        
LA1807*                MOVE 'N'           TO COMM-EC68-MUT-SL300                
LA1807*                   MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                 
LA1807*             END-IF                                                      
LA1807*          WHEN COMM-MEC64-TYPE-NB-MULTI                                  
LA1807*             SET COMM-EC68-MUT-MULTI  TO TRUE                            
LA1807*             IF COMM-MEC64-ACTION-VALID-DATE                             
LA1807*                IF COMMANDE-PRECO                                        
LA1807*                AND COMM-MEC64-TAB-DDELIVO (I-NL) >                      
LA1807*                                 COMM-MEC64-DJOUR                        
LA1807*                   MOVE 'N'           TO COMM-EC68-RESA-A-J              
LA1807*                ELSE                                                     
LA1807*                   MOVE WS-MUT-A-J TO COMM-EC68-RESA-A-J                 
LA1807*                END-IF                                                   
LA1807*                MOVE WS-MUT-SL300  TO COMM-EC68-MUT-SL300                
LA1807*             ELSE                                                        
LA1807*                MOVE 'N'           TO COMM-EC68-MUT-SL300                
LA1807*                   MOVE WS-MUT-A-J TO COMM-EC68-RESA-A-J                 
LA1807*             END-IF                                                      
LA1807*          END-EVALUATE                                                   
LA1807*                                                                         
LA1807*          IF GV11-LCOMMENT(6 : 7) = SPACES OR LOW-VALUES                 
LA1807*             SET COMM-EC68-CREATION-DACEM TO TRUE                        
LA1807*             MOVE MEC65-FILIERE    TO COMM-MEC64-FILIERE                 
LA1807*             MOVE MEC65-DATE-DACEM TO COMM-EC68-DMUT                     
LA1807*             PERFORM PARAM-MUT-DACEM                                     
LA1807*          ELSE                                                           
LA1807*             MOVE ' NNNNNNN' TO COMM-EC68-NEW-MODE                       
LA1807*             MOVE 'S' TO COMM-EC68-ACT-DACEM                             
LA1807*             IF GV11-LCOMMENT(13:1) = SPACE OR LOW-VALUE                 
LA1807*                MOVE GV11-LCOMMENT(14:8) TO COMM-EC68-DMUT               
LA1807*                MOVE GV11-LCOMMENT(23:2) TO COMM-MEC64-FILIERE           
LA1807*             ELSE                                                        
LA1807*                MOVE GV11-LCOMMENT(13:8) TO COMM-EC68-DMUT               
LA1807*                MOVE GV11-LCOMMENT(22:2) TO COMM-MEC64-FILIERE           
LA1807*             END-IF                                                      
LA1807*             MOVE GV11-LCOMMENT(6:7) TO COMM-EC68-NMUTTEMP               
LA1807*          END-IF                                                         
LA1807*       END-IF                                                            
           END-IF                                                               
           MOVE COMM-MEC64-DDELIV-COM TO COMM-MEC64-DDELIV                      
           .                                                                    
       FIN-MEF-REQ-VALID-DATE. EXIT.                                            
      ******************************                                            
       TRT-RETOUR-CONTROLE     SECTION.                                         
      ***********************************                                       
           IF TOP-ERREUR-MEC64(1 : 2) = 'MQ'                                    
              SET EC64-STOCK-ERREUR(I-NL) TO TRUE                               
              SET MEC64-DAC-PB-MQ TO TRUE                                       
              ADD 1 TO EC64-NB-INDISPO                                          
           END-IF                                                               
      * PAS D'ANOMALIE OU ANOMALIE MINEUR                                       
           IF (MEC64-PAS-ERREUR                                                 
              OR MEC64-MUT-DATE-DIF                                             
              OR MEC64-DDELIV-DEP                                               
              OR MEC64-MUT-NT)                                                  
              IF COMM-MEC64-CDEDARTY(I-NL)                                      
                 MOVE COMM-EC67-ETAT-RESERVATION                                
                         TO COMM-MEC64-TAB-ETAT-RESA(I-NL)                      
                 IF COMM-EC67-STOCK-FOURN-OUI                                   
                    SET EC64-STOCK-ATT-FOURN (I-NL) TO TRUE                     
                    MOVE COMM-EC67-DDELIV                                       
                         TO COMM-MEC64-TAB-DDELIVO(I-NL)                        
                 END-IF                                                         
                 MOVE COMM-EC67-ETAT-FOURNISSEUR                                
                         TO COMM-MEC64-TAB-ETAT-FOURN(I-NL)                     
              END-IF                                                            
LA1807*       IF COMM-MEC64-CDEDACEM(I-NL)                                      
LA1807*          MOVE COMM-EC68-ETAT-RESERVATION                                
LA1807*                  TO COMM-MEC64-TAB-ETAT-RESA(I-NL)                      
LA1807*       END-IF                                                            
           ELSE                                                                 
              IF MEC64-PB-STOCK                                                 
                 SET EC64-AUCUN-STOCK-DISPO(I-NL) TO TRUE                       
              ELSE                                                              
                 IF MEC64-DAC-PAS-DISPO                                         
                      SET EC64-AUCUN-STOCK-DISPO(I-NL) TO TRUE                  
                 ELSE                                                           
      * ANOMALIE FONCTIONNELLE, ON NE CONNAIT PAS L'ETAT DE LA LIGNE            
                    IF (MEC64-MFL05                                             
                      OR MEC64-DAC-PB-MQ                                        
                      OR MEC64-PB-GB05                                          
                      OR MEC64-PB-MAJ-GB05                                      
                      OR MEC64-DAC-CLIENT-INVAL                                 
                      OR MEC64-DAC-CRIT-INVAL                                   
                      OR MEC64-DAC-LIEU-INVAL                                   
                      OR MEC64-MDELIV-NT                                        
                      OR MEC64-GA00-NT                                          
                      OR MEC64-CF-DATE                                          
                      OR MEC64-CF-QTE                                           
                      OR MEC64-DAC-CLIENT-NT)                                   
                         SET EC64-STOCK-ERREUR(I-NL) TO TRUE                    
                    ELSE                                                        
      * ANOMALIE TECHNIQUE GRAVE ET BLOCANTE                                    
                       SET EC64-STATUT-ERREUR-GRAVE TO TRUE                     
                       MOVE COMM-EC67-CODRET   TO                               
                                  COMM-MEC64-CODE-RETOUR                        
                       PERFORM MODULE-SORTIE                                    
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       FIN-TRT-RETOUR-CONTROLE. EXIT.                                           
      *********************************                                         
       TRT-RETOUR-REQ-VALIDATION     SECTION.                                   
      ***************************************                                   
           IF COMM-MEC64-CDEDARTY (I-NL)                                        
             IF COMM-EC69-CODRET NOT = '0000'                                   
                 SET TRAITEMENT-KO       TO TRUE                                
                 MOVE COMM-EC69-CODRET   TO COMM-MEC64-CODE-RETOUR              
                 MOVE COMM-EC69-LMESSAGE TO COMM-MEC64-MESSAGE                  
                 IF AUTO-DISPLAY                                                
                    STRING 'TRT KO SUR VALIDATION DARTY, CODE '                 
                    COMM-EC69-CODRET                                            
                     DELIMITED BY SIZE INTO W-MESSAGE                           
                     PERFORM DISPLAY-LOG                                        
                 END-IF                                                         
             ELSE                                                               
                IF EC64-STATUT-ATTENTE-FOURN                                    
                    SET TRAITEMENT-KO       TO TRUE                             
                END-IF                                                          
             END-IF                                                             
LA1807*    ELSE                                                                 
LA1807*      IF COMM-EC68-CODRET NOT = '0000'                                   
LA1807*          IF NOT COMM-MEC64-ACTION-VALIDE-GV                             
LA1807*             SET TRAITEMENT-KO       TO TRUE                             
LA1807*             MOVE COMM-EC68-CODRET   TO COMM-MEC64-CODE-RETOUR           
LA1807*             MOVE COMM-EC68-LMESSAGE TO COMM-MEC64-MESSAGE               
LA1807*          END-IF                                                         
LA1807*          IF AUTO-DISPLAY                                                
LA1807*             STRING 'TRT KO SUR VALIDATION DACEM, CODE '                 
LA1807*             COMM-EC68-CODRET                                            
LA1807*              DELIMITED BY SIZE INTO W-MESSAGE                           
LA1807*              PERFORM DISPLAY-LOG                                        
LA1807*          END-IF                                                         
LA1807*      END-IF                                                             
           END-IF                                                               
           .                                                                    
       FIN-TRT-RETOUR-REQ-VALIDATION. EXIT.                                     
       TRT-RETOUR-MEC69-INC          SECTION.                                   
      ***************************************                                   
THEMIS*    IF COMM-EC69-CODRET = '0000'                                         
                 IF EC64-STOCK-DISPONIBLE(I-NL)                                 
                    MOVE 'R'  TO GV22-CSTATUT                                   
                 ELSE                                                           
                    MOVE 'A'  TO GV22-CSTATUT                                   
                 END-IF                                                         
THEMIS*    ELSE                                                                 
THEMIS*       MOVE 'A' TO GV22-CSTATUT                                          
THEMIS*    END-IF                                                               
           PERFORM INSERT-RTSL21                                                
           .                                                                    
       FIN-TRT-RETOUR-MEC69-INC. EXIT.                                          
      **************************************                                    
       RECHERCHE-DONNEES-AV-RESA    SECTION.                                    
      ***************************************                                   
           PERFORM SELECT-RTGV11                                                
           PERFORM SELECT-RTGV22                                                
           IF GV11-LCOMMENT(1:5) = SPACES OR LOW-VALUES                         
               MOVE GV11-NCODIC      TO GA00-NCODIC                             
               PERFORM SELECT-RTGA00                                            
LA1807*        IF GA00-WDACEM = 'O'                                             
LA1807*           MOVE 'DACEM'  TO GV11-LCOMMENT (1:5)                          
LA1807*        ELSE                                                             
LA1807         IF GA00-WDACEM = 'N'                                             
                  MOVE 'DARTY'  TO GV11-LCOMMENT (1:5)                          
               END-IF                                                           
           END-IF                                                               
           .                                                                    
       FIN-RECHERCHE-DONNEES-AV-RESA. EXIT.                                     
      **************************************                                    
       TRT-MAJ-SET-INCOMP    SECTION.                                           
      *********************************                                         
           MOVE W-SPDATDB2-DSYST           TO GV11-DSYST                        
           MOVE COMM-MEC64-DDELIV          TO GV11-DDELIV                       
           IF WS-NB-PRODUIT = 1                                                 
              MOVE '66VGM' TO GV11-CEQUIPE                                      
              MOVE '66VGM' TO GV22-CEQUIPE                                      
           ELSE                                                                 
              MOVE 'MGV64' TO GV11-CEQUIPE                                      
              MOVE 'MGV64' TO GV22-CEQUIPE                                      
           END-IF                                                               
           IF GV11-LCOMMENT(1:5) = 'DARTY'                                      
              MOVE COMM-EC69-NMUTATION      TO GV11-LCOMMENT (6:7)              
              MOVE COMM-EC69-NMUTATION         TO GV22-NMUTATION                
              MOVE COMM-EC69-NSOCDEPOTO        TO GV11-NSOCLIVR                 
              MOVE COMM-EC69-NDEPOTO           TO GV11-NDEPOT                   
LA1807*    ELSE                                                                 
LA1807*       MOVE SPACES TO GV11-LCOMMENT(6:24)                                
LA1807*       MOVE COMM-EC68-NMUTATION   TO GV11-LCOMMENT (6:7)                 
LA1807*       MOVE COMM-EC68-DATE-DISPO TO                                      
LA1807*                     GV11-LCOMMENT(14:8)                                 
LA1807*       MOVE COMM-EC68-FILIERE  TO                                        
LA1807*                     GV11-LCOMMENT(23:4)                                 
LA1807*       MOVE SPACES       TO GV22-NMUTATION                               
           END-IF                                                               
           PERFORM UPDATE-GV1100-ENC                                            
           IF COMM-MEC64-CPROG   = 'MGV66'                                      
              PERFORM LOG-GV23                                                  
           END-IF                                                               
           MOVE GV11-NSOCLIVR              TO GV22-NSOCLIVR                     
           MOVE GV11-NDEPOT                TO GV22-NDEPOT                       
           PERFORM UPDATE-RTGV22                                                
           .                                                                    
       FIN-TRT-MAJ-SET-INCOMP. EXIT.                                            
      ********************************                                          
       APPEL-MEC69  SECTION.                                                    
      ***********************                                                   
           INITIALIZE          WS-PROG                                          
           MOVE 'MEC69'     TO WS-PROG                                          
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-MESSAGE-H.                             
           MOVE W-MINUTE            TO W-MESSAGE-M.                             
           MOVE LENGTH OF COMM-EC69-APPLI TO LONG-COMMAREA-LINK                 
           MOVE COMM-EC69-APPLI               TO Z-COMMAREA-LINK                
           MOVE WS-PROG                   TO NOM-PROG-LINK                      
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK               TO COMM-EC69-APPLI                
           MOVE COMM-EC69-CODRET TO TOP-ERREUR-MEC64                            
           .                                                                    
       FIN-APPEL-MEC69. EXIT.                                                   
      ***********************                                                   
LA1807*APPEL-MEC68  SECTION.                                                    
LA1807***********************                                                   
LA1807*                                                                         
LA1807*    INITIALIZE WS-PROG                                                   
LA1807*    MOVE 'MEC68'     TO WS-PROG                                          
LA1807*                                                                         
LA1807*    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
LA1807*                                                                         
LA1807*    MOVE EIBTIME             TO W-EIBTIME.                               
LA1807*    MOVE W-HEURE             TO W-MESSAGE-H.                             
LA1807*    MOVE W-MINUTE            TO W-MESSAGE-M.                             
LA1807*    IF NOT COMM-MEC64-ACTION-MAJ-DDELIV                                  
LA1807*       MOVE COMM-MEC64-FILIERE            TO COMM-EC68-FILIERE-IN        
LA1807*       MOVE COMM-MEC64-TAB-DDELIVO(I-NL)  TO COMM-EC68-DDELIV            
LA1807*    END-IF                                                               
LA1807*    MOVE COMM-MEC64-NSOCIETE           TO COMM-EC68-NSOCIETE             
LA1807*    MOVE COMM-MEC64-NLIEU              TO COMM-EC68-NLIEU                
LA1807*    MOVE COMM-MEC64-NVENTE             TO COMM-EC68-NVENTE               
LA1807*    MOVE MDLIV-LDEPLIVR-EXP (1:3)      TO COMM-EC68-MDLIV-NSOC           
LA1807*    MOVE MDLIV-LDEPLIVR-EXP (4:3)      TO COMM-EC68-MDLIV-NLIEU          
LA1807*    MOVE COMM-MEC64-TAB-NCODIC(I-NL)   TO COMM-EC68-NCODIC               
LA1807*    MOVE COMM-MEC64-TAB-NSEQNQ(I-NL)   TO COMM-EC68-NSEQNQ               
LA1807*    MOVE COMM-MEC64-TAB-WEMPORTE(I-NL) TO COMM-EC68-WEMPORTE             
LA1807*    MOVE COMM-MEC64-TAB-QVENDUE(I-NL)  TO COMM-EC68-QTE                  
LA1807*    IF (WS-NB-PRODUIT = 1)                                               
LA1807*    OR (COMM-MEC64-TYPE-NB-MONO)                                         
LA1807*       SET COMM-EC68-MUT-UNIQUE  TO TRUE                                 
LA1807*    ELSE                                                                 
LA1807*       SET COMM-EC68-MUT-MULTI   TO TRUE                                 
LA1807*    END-IF                                                               
LA1807*    MOVE COMM-MEC64-DJOUR          TO COMM-EC68-SSAAMMJJ                 
LA1807*                                                                         
LA1807*    MOVE COMM-EC68-APPLI           TO Z-COMMAREA-LINK                    
LA1807*    MOVE WS-PROG                   TO NOM-PROG-LINK                      
LA1807*    PERFORM LINK-PROG                                                    
LA1807*    MOVE Z-COMMAREA-LINK               TO COMM-EC68-APPLI                
LA1807*    MOVE COMM-EC68-CODRET TO TOP-ERREUR-MEC64                            
LA1807*    .                                                                    
LA1807*FIN-APPEL-MEC68. EXIT.                                                   
LA1807***********************                                                   
       APPEL-MEC67  SECTION.                                                    
      ***********************                                                   
           INITIALIZE WS-PROG                                                   
           MOVE 'MEC67'     TO WS-PROG                                          
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.                                 
      *--                                                                       
           EXEC CICS ASKTIME NOHANDLE                                           
           END-EXEC.                                                            
      *}                                                                        
           MOVE EIBTIME             TO W-EIBTIME.                               
           MOVE W-HEURE             TO W-MESSAGE-H.                             
           MOVE W-MINUTE            TO W-MESSAGE-M.                             
           MOVE COMM-MEC64-MAJ-STOCK   TO COMM-EC67-MAJ-STOCK                   
           MOVE COMM-MEC64-INTERNET    TO COMM-EC67-INTERNET                    
           MOVE COMM-MEC64-TAB-DDELIVO(I-NL)  TO COMM-EC67-DDELIV               
           MOVE COMM-MEC64-NSOCIETE           TO COMM-EC67-NSOCIETE             
           MOVE COMM-MEC64-NLIEU              TO COMM-EC67-NLIEU                
           MOVE MDLIV-LDEPLIVR-EXP (1:3)      TO COMM-EC67-MDLIV-NSOC           
           MOVE MDLIV-LDEPLIVR-EXP (4:3)      TO COMM-EC67-MDLIV-NLIEU          
           MOVE COMM-MEC64-NVENTE             TO COMM-EC67-NVENTE               
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)   TO COMM-EC67-NCODIC               
           MOVE GA00-QHAUTEUR                 TO COMM-EC67-GA00-QHAUTEUR        
           MOVE GA00-QPROFONDEUR           TO COMM-EC67-GA00-QPROFONDEUR        
           MOVE GA00-QLARGEUR                 TO COMM-EC67-GA00-QLARGEUR        
           MOVE COMM-MEC64-TAB-NSEQNQ(I-NL)   TO COMM-EC67-NSEQNQ               
           MOVE COMM-MEC64-TAB-WEMPORTE(I-NL) TO COMM-EC67-WEMPORTE             
           MOVE COMM-MEC64-TAB-WCQERESF(I-NL) TO COMM-EC67-WCQERESF             
           MOVE COMM-MEC64-TAB-QVENDUE(I-NL)  TO COMM-EC67-QTE                  
           MOVE COMM-MEC64-DJOUR              TO COMM-EC67-SSAAMMJJ             
           MOVE COMM-MEC64-FL05(I-NL)         TO COMM-EC67-FL05                 
           MOVE COMM-EC67-APPLI               TO Z-COMMAREA-LINK                
           MOVE WS-PROG                   TO NOM-PROG-LINK                      
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK               TO COMM-EC67-APPLI                
           IF COMM-EC67-CODRET = '0000'                                         
              MOVE COMM-EC67-CSTATUT TO GV22-CSTATUT                            
           END-IF                                                               
           MOVE COMM-EC67-CODRET TO TOP-ERREUR-MEC64                            
           .                                                                    
       FIN-APPEL-MEC67. EXIT.                                                   
      ***********************                                                   
       ALIM-MEC69-ENC            SECTION.                                       
      ***********************************                                       
           INITIALIZE COMM-EC69-APPLI                                           
           MOVE SPACE TO COMM-EC69-NMUTATION                                    
           MOVE SPACE TO COMM-EC69-NMUT                                         
      * ON CHERCHE UNE MUT POUR LE JOUR MEME, SI LA VENTE N'EST PAS             
      * EN PRE-COMMANDE                                                         
           IF COMMANDE-PRECO OR EC64-STATUT-ATTENTE-FOURN                       
              MOVE 'N'                 TO COMM-EC69-RESA-A-J                    
           ELSE                                                                 
RESAJ *                 display 'mut J5 ' WS-MUT-A-J                            
RESAJ         MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                             
           END-IF                                                               
           MOVE '0000'              TO COMM-EC69-CODRET                         
           MOVE ' NNNNNNN'          TO COMM-EC69-NEW-MODE                       
           SET COMM-EC69-NEW-APPEL TO TRUE                                      
           MOVE 'O' TO COMM-EC69-CRE-GB05-FINAL                                 
           IF (EC64-STATUT-COMPLETE)                                            
              MOVE 'O' TO COMM-EC69-CRE-GB15-FINAL                              
PBGB15        MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GA00-NCODIC                   
PBGB15        PERFORM SELECT-RTGA00                                             
PBGB15        MOVE GA00-CMARQ          TO COMM-EC69-GA00-CMARQ                  
PBGB15        MOVE GA00-CFAM           TO COMM-EC69-GA00-CFAM                   
PBGB15        MOVE GA00-CAPPRO         TO COMM-EC69-GA00-CAPPRO                 
PBGB15        MOVE GA00-QLARGEUR       TO COMM-EC69-GA00-QLARGEUR               
PBGB15        MOVE GA00-QPROFONDEUR    TO COMM-EC69-GA00-QPROFONDEUR            
PBGB15        MOVE GA00-QHAUTEUR       TO COMM-EC69-GA00-QHAUTEUR               
PBGB15        MOVE GA00-QPOIDS         TO COMM-EC69-GA00-QPOIDS                 
           END-IF                                                               
           MOVE 'O' TO COMM-EC69-SUP-MUT-GB05                                   
           MOVE 'O' TO COMM-EC69-SUP-MUT-GB15                                   
           IF EC64-STATUT-PREC-INCOMPLETE                                       
              MOVE 'I' TO COMM-EC69-ETAT-PREC-VTE                               
              MOVE WS-MUT-SL300     TO COMM-EC69-MUT-SL300                      
           ELSE                                                                 
              MOVE 'C' TO COMM-EC69-ETAT-PREC-VTE                               
              MOVE 'N' TO COMM-EC69-MUT-SL300                                   
           END-IF                                                               
           IF COMM-MEC64-TYPE-NB-MONO                                           
               SET COMM-EC69-MUT-UNIQUE TO TRUE                                 
           ELSE                                                                 
               SET COMM-EC69-MUT-MULTI  TO TRUE                                 
           END-IF                                                               
           MOVE COMM-MEC64-NSOCIETE TO COMM-EC69-NSOCIETE                       
           MOVE COMM-MEC64-NLIEU    TO COMM-EC69-NLIEU                          
           MOVE MDLIV-LDEPLIVR-EXP (1:3)    TO COMM-EC69-MDLIV-NSOC             
           MOVE MDLIV-LDEPLIVR-EXP (4:3)    TO COMM-EC69-MDLIV-NLIEU            
           MOVE COMM-MEC64-NORDRE   TO COMM-EC69-NORDRE                         
           MOVE COMM-MEC64-NVENTE   TO COMM-EC69-NVENTE                         
           MOVE COMM-MEC64-TAB-NSEQNQ(I-NL)    TO COMM-EC69-NSEQNQ              
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)    TO COMM-EC69-NCODIC              
           MOVE COMM-MEC64-TAB-QVENDUE(I-NL)   TO COMM-EC69-QTE                 
           MOVE COMM-MEC64-TAB-WCQERESF(I-NL)  TO COMM-EC69-WCQERESF            
           MOVE COMM-MEC64-TAB-WEMPORTE(I-NL)  TO COMM-EC69-WEMPORTE            
           MOVE COMM-MEC64-TAB-DDELIVO(I-NL)   TO COMM-EC69-DDELIV              
           MOVE COMM-MEC64-TAB-LCOMMENT(I-NL)(6:7) TO COMM-EC69-NMUTTEMP        
           MOVE COMM-MEC64-TAB-NSOCLIVR(I-NL) TO COMM-EC69-NSOCLIVR             
           MOVE COMM-MEC64-TAB-NDEPOT  (I-NL) TO COMM-EC69-NDEPOT               
           MOVE COMM-MEC64-DJOUR              TO COMM-EC69-SSAAMMJJ             
           MOVE COMM-MEC64-FL05-DEPOT(I-NL, 1)                                  
                              TO COMM-EC69-FL05-DEPOT (1)                       
RESAJ *     display 'commec69 resa a j fin alim ' comm-ec69-resa-a-j            
           .                                                                    
       FIN-ALIM-MEC69-ENC.  EXIT.                                               
      ***********************                                                   
       ALIM-MEC69-INC            SECTION.                                       
      ***********************************                                       
           INITIALIZE COMM-EC69-APPLI                                           
           MOVE SPACE TO COMM-EC69-NMUTATION                                    
           MOVE SPACE TO COMM-EC69-NMUT                                         
           MOVE 'N'                 TO COMM-EC69-RESA-A-J                       
           MOVE '0000'              TO COMM-EC69-CODRET                         
           MOVE ' NNNNNNN'          TO COMM-EC69-NEW-MODE                       
           SET COMM-EC69-NEW-APPEL TO TRUE                                      
           MOVE 'O' TO COMM-EC69-CRE-GB05-FINAL                                 
           MOVE 'O' TO COMM-EC69-SUP-MUT-GB05                                   
           IF EC64-STATUT-PREC-OK OR MUTATION-TEMPO                             
              MOVE 'O' TO COMM-EC69-SUP-MUT-GB15                                
           END-IF                                                               
           MOVE GB05-NMUTATION TO COMM-EC69-NMUTTEMP                            
           IF EC64-STATUT-PREC-INCOMPLETE                                       
              MOVE 'I' TO COMM-EC69-ETAT-PREC-VTE                               
           ELSE                                                                 
              MOVE 'C' TO COMM-EC69-ETAT-PREC-VTE                               
           END-IF                                                               
           MOVE 'N' TO COMM-EC69-MUT-SL300                                      
           IF WS-NB-PRODUIT = 1                                                 
               SET COMM-EC69-MUT-UNIQUE TO TRUE                                 
           ELSE                                                                 
               SET COMM-EC69-MUT-MULTI  TO TRUE                                 
           END-IF                                                               
           MOVE COMM-MEC64-NSOCIETE TO COMM-EC69-NSOCIETE                       
           MOVE COMM-MEC64-NLIEU    TO COMM-EC69-NLIEU                          
           MOVE MDLIV-LDEPLIVR-EXP (1:3)    TO COMM-EC69-MDLIV-NSOC             
           MOVE MDLIV-LDEPLIVR-EXP (4:3)    TO COMM-EC69-MDLIV-NLIEU            
           MOVE COMM-MEC64-NORDRE   TO COMM-EC69-NORDRE                         
           MOVE COMM-MEC64-NVENTE   TO COMM-EC69-NVENTE                         
           MOVE COMM-MEC64-TAB-NSEQNQ(I-NL)    TO COMM-EC69-NSEQNQ              
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)    TO COMM-EC69-NCODIC              
           MOVE COMM-MEC64-TAB-QVENDUE(I-NL)   TO COMM-EC69-QTE                 
           MOVE COMM-MEC64-TAB-WCQERESF(I-NL)  TO COMM-EC69-WCQERESF            
           MOVE COMM-MEC64-TAB-WEMPORTE(I-NL)  TO COMM-EC69-WEMPORTE            
           MOVE COMM-MEC64-DDELIV              TO COMM-EC69-DDELIV              
           MOVE COMM-MEC64-TAB-LCOMMENT(I-NL)(6:7) TO COMM-EC69-NMUTTEMP        
           MOVE COMM-MEC64-TAB-NSOCLIVR(I-NL) TO COMM-EC69-NSOCLIVR             
           MOVE COMM-MEC64-TAB-NDEPOT  (I-NL) TO COMM-EC69-NDEPOT               
           MOVE COMM-MEC64-DJOUR              TO COMM-EC69-SSAAMMJJ             
           MOVE COMM-MEC64-FL05-DEPOT (I-NL, 1)                                 
                              TO COMM-EC69-FL05-DEPOT (1)                       
THEMIS*    DANS NMUTATION ON A LE FL05(1) ; DANS NMUTATION2 : FL05(2)           
           IF W-THEMIS = 'O'                                                    
           AND EC64-MULTIDA = 'O'                                               
              IF W-ENTR1 = COMM-MEC64-FL05-DEPOT (I-NL, 1)                      
                 MOVE W-NMUT1                 TO COMM-EC69-NMUT                 
              END-IF                                                            
              IF W-ENTR2 = COMM-MEC64-FL05-DEPOT (I-NL, 1)                      
                 MOVE W-NMUT2                 TO COMM-EC69-NMUT                 
              END-IF                                                            
           ELSE                                                                 
AVANT         IF COMM-MEC64-FL05-NDEPOT(I-NL, 1) = '095'                        
THEMIS          MOVE COMM-MEC64-TAB-NMUT(I-NL) TO COMM-EC69-NMUT                
 '            ELSE                                                              
 '              MOVE SPACE                     TO COMM-EC69-NMUT                
 '            END-IF                                                            
           END-IF                                                               
THEMIS     IF COMMANDE-PRECO                                                    
             SET EC69-COMMANDE-PRECO TO TRUE                                    
           END-IF                                                               
           MOVE GA00-CMARQ                 TO COMM-EC69-GA00-CMARQ              
           MOVE GA00-CFAM                  TO COMM-EC69-GA00-CFAM               
           MOVE GA00-CAPPRO                TO COMM-EC69-GA00-CAPPRO             
           MOVE GA00-QLARGEUR              TO COMM-EC69-GA00-QLARGEUR           
           MOVE GA00-QPROFONDEUR           TO COMM-EC69-GA00-QPROFONDEUR        
           MOVE GA00-QHAUTEUR              TO COMM-EC69-GA00-QHAUTEUR           
           MOVE GA00-QPOIDS                TO COMM-EC69-GA00-QPOIDS             
           .                                                                    
       FIN-ALIM-MEC69-INC.  EXIT.                                               
      ***********************                                                   
       ALIM-MEC69-RESA           SECTION.                                       
      ***********************************                                       
           INITIALIZE COMM-EC69-APPLI                                           
           SET  COMM-EC69-NEW-APPEL       TO TRUE                               
           MOVE 'O'                       TO COMM-EC69-CRE-GB05-TEMPO           
           MOVE COMM-MEC64-INTERNET       TO COMM-EC69-INTERNET                 
           MOVE SPACE                     TO COMM-EC69-NMUT                     
           MOVE SPACE                     TO COMM-EC69-LCOMMENT                 
           MOVE SPACE                     TO COMM-EC69-NMUTTEMP                 
           MOVE COMM-EC67-NCODIC          TO COMM-EC69-NCODIC                   
           MOVE COMM-EC67-NVENTE          TO COMM-EC69-NVENTE                   
           MOVE COMM-EC67-WEMPORTE        TO COMM-EC69-WEMPORTE                 
           MOVE COMM-EC67-QTE             TO COMM-EC69-QTE                      
           MOVE COMM-EC67-NSOCDEPOT       TO COMM-EC69-NSOCLIVR                 
           MOVE COMM-EC67-NDEPOT          TO COMM-EC69-NDEPOT                   
           MOVE COMM-EC67-NSOCIETE        TO COMM-EC69-NSOCIETE                 
           MOVE COMM-EC67-NLIEU           TO COMM-EC69-NLIEU                    
           MOVE COMM-EC67-MDLIV-NSOC      TO COMM-EC69-MDLIV-NSOC               
           MOVE COMM-EC67-MDLIV-NLIEU     TO COMM-EC69-MDLIV-NLIEU              
           MOVE COMM-EC67-INTERNET        TO COMM-EC69-INTERNET                 
           MOVE GA00-CMARQ            TO COMM-EC69-GA00-CMARQ                   
           MOVE GA00-CFAM             TO COMM-EC69-GA00-CFAM                    
           MOVE GA00-CAPPRO           TO COMM-EC69-GA00-CAPPRO                  
           MOVE COMM-EC67-GA00-QLARGEUR TO COMM-EC69-GA00-QLARGEUR              
           MOVE COMM-EC67-GA00-QPROFONDEUR TO COMM-EC69-GA00-QPROFONDEUR        
           MOVE COMM-EC67-GA00-QHAUTEUR TO COMM-EC69-GA00-QHAUTEUR              
           MOVE GA00-QPOIDS             TO COMM-EC69-GA00-QPOIDS                
           MOVE COMM-EC67-SSAAMMJJ      TO COMM-EC69-SSAAMMJJ                   
           MOVE COMM-EC67-DDELIV        TO COMM-EC69-DDELIV                     
           MOVE COMM-EC67-FL05          TO COMM-EC69-FL05                       
           .                                                                    
       FIN-ALIM-MEC69-RESA.  EXIT.                                              
      ***********************                                                   
       ALIM-MEC69-MAJ-DATE       SECTION.                                       
      ***********************************                                       
           INITIALIZE COMM-EC69-APPLI                                           
           SET COMM-EC69-NEW-APPEL TO TRUE                                      
           MOVE ' NNNNNNN' TO COMM-EC69-NEW-MODE                                
           MOVE COMM-MEC64-TAB-LCOMMENT(I-NL)(6:7)                              
                                 TO COMM-EC69-NMUTTEMP                          
           MOVE 'O' TO COMM-EC69-SUP-MUT-GB05                                   
           MOVE 'O' TO COMM-EC69-CRE-GB05-FINAL                                 
      *    MOVE 'N'       TO COMM-EC69-RESA-A-J                                 
RESAJ         MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                             
THEMIS     IF COMMANDE-PRECO                                                    
             SET EC69-COMMANDE-PRECO TO TRUE                                    
           END-IF                                                               
           MOVE WS-MUT-SL300  TO COMM-EC69-MUT-SL300                            
           SET COMM-EC69-MUT-MULTI   TO TRUE                                    
           IF EC64-MULTIDA = 'O'                                                
              MOVE COMM-MEC64-TAB-DDELIVO(I-NL) TO COMM-EC69-DDELIV             
           ELSE                                                                 
              MOVE COMM-MEC64-DATE-MAJ TO COMM-EC69-DDELIV                      
           END-IF                                                               
           MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO COMM-EC69-NCODIC                 
           MOVE COMM-MEC64-NVENTE         TO COMM-EC69-NVENTE                   
           MOVE COMM-MEC64-TAB-WEMPORTE(I-NL)  TO COMM-EC69-WEMPORTE            
           MOVE COMM-MEC64-TAB-QVENDUE(I-NL) TO COMM-EC69-QTE                   
           MOVE COMM-MEC64-NSOCIETE        TO COMM-EC69-NSOCIETE                
           MOVE COMM-MEC64-NLIEU           TO COMM-EC69-NLIEU                   
           MOVE MDLIV-LDEPLIVR-EXP (1:3) TO COMM-EC69-MDLIV-NSOC                
           MOVE MDLIV-LDEPLIVR-EXP (4:3) TO COMM-EC69-MDLIV-NLIEU               
           MOVE GA00-CMARQ                 TO COMM-EC69-GA00-CMARQ              
           MOVE GA00-CFAM                  TO COMM-EC69-GA00-CFAM               
           MOVE GA00-CAPPRO                TO COMM-EC69-GA00-CAPPRO             
           MOVE GA00-QLARGEUR              TO COMM-EC69-GA00-QLARGEUR           
           MOVE GA00-QPROFONDEUR           TO COMM-EC69-GA00-QPROFONDEUR        
           MOVE GA00-QHAUTEUR              TO COMM-EC69-GA00-QHAUTEUR           
           MOVE GA00-QPOIDS                TO COMM-EC69-GA00-QPOIDS             
           MOVE COMM-MEC64-DJOUR           TO COMM-EC69-SSAAMMJJ                
           MOVE COMM-MEC64-DATE-MAJ        TO COMM-EC69-DDELIV                  
           MOVE COMM-MEC64-TAB-LCOMMENT(I-NL)(13:6)                             
                                  TO COMM-EC69-FL05-DEPOT(1)                    
           .                                                                    
       FIN-ALIM-MEC69-MAJ-DATE.  EXIT.                                          
      ***********************                                                   
       ALIM-MEC69-VALID          SECTION.                                       
      *                                                                         
           SET  COMM-EC69-NEW-APPEL       TO TRUE                               
           MOVE 'I'                TO COMM-EC69-ETAT-PREC-VTE                   
           MOVE ' NNNNNNN'         TO COMM-EC69-NEW-MODE                        
      *    MOVE 'N'                TO COMM-EC69-RESA-A-J                        
           MOVE 'O'                TO COMM-EC69-SUP-MUT-GB05                    
           MOVE 'O'                TO COMM-EC69-SUP-MUT-GB15                    
           MOVE 'O'                TO COMM-EC69-CRE-GB05-FINAL                  
           MOVE 'N'                TO COMM-EC69-CRE-GB15-FINAL                  
           MOVE SPACE              TO COMM-EC69-NMUT                            
           IF COMM-EC67-STOCK-FOURN-OUI                                         
              MOVE 'N'                TO COMM-EC69-RESA-A-J                     
RESAJ *       MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                             
           ELSE                                                                 
              IF GV99-PRECO-OK  AND GV99-WPRECO = 'O'                           
                 IF GV11-DDELIV > COMM-MEC64-DJOUR                              
                    MOVE 'N'                TO COMM-EC69-RESA-A-J               
                 ELSE                                                           
RESAJ               MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                       
                 END-IF                                                         
              ELSE                                                              
RESAJ               MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                       
              END-IF                                                            
           END-IF                                                               
           MOVE GV11-LCOMMENT(6 : 7) TO COMM-EC69-NMUTTEMP                      
           PERFORM SELECT-NB-RTGV11                                             
           IF WS-NB-PRODUIT = 1                                                 
              SET COMM-EC69-MUT-UNIQUE  TO TRUE                                 
           ELSE                                                                 
              SET COMM-EC69-MUT-MULTI   TO TRUE                                 
           END-IF                                                               
           MOVE WS-MUT-SL300        TO COMM-EC69-MUT-SL300                      
      *                                                                         
           MOVE COMM-EC67-NCODIC   TO COMM-EC69-NCODIC                          
           MOVE COMM-EC67-NVENTE   TO COMM-EC69-NVENTE                          
           MOVE GV11-WCQERESF      TO COMM-EC69-WCQERESF                        
           MOVE GV11-WEMPORTE      TO COMM-EC69-WEMPORTE                        
           MOVE COMM-EC67-QTE      TO COMM-EC69-QTE                             
           MOVE COMM-EC67-NSOCIETE   TO COMM-EC69-NSOCIETE                      
           MOVE COMM-EC67-NLIEU      TO COMM-EC69-NLIEU                         
           MOVE COMM-EC67-MDLIV-NSOC  TO COMM-EC69-MDLIV-NSOC                   
           MOVE COMM-EC67-MDLIV-NLIEU  TO COMM-EC69-MDLIV-NLIEU                 
           MOVE SPACES                 TO  COMM-EC69-LCOMMENT                   
      *                                                                         
           MOVE GA00-CMARQ   TO COMM-EC69-GA00-CMARQ                            
           MOVE GA00-CFAM    TO COMM-EC69-GA00-CFAM                             
           MOVE GA00-CAPPRO TO COMM-EC69-GA00-CAPPRO                            
           MOVE GA00-QLARGEUR TO COMM-EC69-GA00-QLARGEUR                        
           MOVE GA00-QPROFONDEUR TO COMM-EC69-GA00-QPROFONDEUR                  
           MOVE GA00-QHAUTEUR  TO COMM-EC69-GA00-QHAUTEUR                       
           MOVE GA00-QPOIDS TO COMM-EC69-GA00-QPOIDS                            
      *                                                                         
           MOVE COMM-EC67-FL05 TO COMM-EC69-FL05                                
           MOVE COMM-EC67-DDELIV TO COMM-EC69-DDELIV                            
           MOVE COMM-EC67-SSAAMMJJ TO COMM-EC69-SSAAMMJJ                        
           .                                                                    
       FIN-ALIM-MEC69-VALID. EXIT.                                              
      ***********************                                                   
       ALIM-MEC69-GV             SECTION.                                       
      *                                                                         
           INITIALIZE COMM-EC69-APPLI                                           
           SET  COMM-EC69-NEW-APPEL       TO TRUE                               
           MOVE COMM-MEC64-ETAT-PREC-VTE   TO COMM-EC69-ETAT-PREC-VTE           
           MOVE ' NNNNNNN'                 TO COMM-EC69-NEW-MODE                
           MOVE COMM-MEC64-CRE-GB05-TEMPO TO COMM-EC69-CRE-GB05-TEMPO           
           MOVE COMM-MEC64-SUP-MUT-GB05  TO COMM-EC69-SUP-MUT-GB05              
           MOVE COMM-MEC64-SUP-MUT-GB15  TO COMM-EC69-SUP-MUT-GB15              
           MOVE 'N'                      TO COMM-EC69-CRE-GB05-FINAL            
           MOVE 'N'                      TO COMM-EC69-CRE-GB15-FINAL            
           MOVE COMM-MEC64-TAB-NMUTTEMP(I-NL) TO COMM-EC69-NMUTTEMP             
           MOVE COMM-MEC64-ETAT-PREC-VTE TO COMM-EC69-ETAT-PREC-VTE             
      *    MOVE 'N'                TO COMM-EC69-RESA-A-J                        
RESAJ         MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                             
THEMIS     IF COMMANDE-PRECO                                                    
             SET EC69-COMMANDE-PRECO TO TRUE                                    
           END-IF                                                               
           IF (COMM-MEC64-SUP-MUT-GB05 = 'O')                                   
           AND (COMM-MEC64-SUP-MUT-GB15 NOT = 'O')                              
              MOVE COMM-EC69-NMUTTEMP   TO GB05-NMUTATION                       
              PERFORM SELECT-MUTATION                                           
              IF MUTATION-TEMPO                                                 
                 MOVE 'O'        TO COMM-EC69-SUP-MUT-GB15                      
              END-IF                                                            
           END-IF                                                               
           IF COMM-MEC64-TYPE-NB-MONO                                           
              SET COMM-EC69-MUT-UNIQUE  TO TRUE                                 
           ELSE                                                                 
              SET COMM-EC69-MUT-MULTI   TO TRUE                                 
           END-IF                                                               
           MOVE 'N'                 TO COMM-EC69-MUT-SL300                      
      *                                                                         
           MOVE COMM-EC67-NCODIC   TO COMM-EC69-NCODIC                          
           MOVE COMM-EC67-NVENTE   TO COMM-EC69-NVENTE                          
           MOVE COMM-EC67-NSOCDEPOT       TO COMM-EC69-NSOCLIVR                 
           MOVE COMM-EC67-NDEPOT          TO COMM-EC69-NDEPOT                   
           MOVE COMM-MEC64-TAB-WCQERESF(I-NL) TO COMM-EC69-WCQERESF             
           MOVE COMM-MEC64-TAB-WEMPORTE(I-NL) TO COMM-EC69-WEMPORTE             
           MOVE COMM-EC67-QTE      TO COMM-EC69-QTE                             
           MOVE COMM-EC67-NSOCIETE   TO COMM-EC69-NSOCIETE                      
           MOVE COMM-EC67-NLIEU      TO COMM-EC69-NLIEU                         
           MOVE COMM-EC67-MDLIV-NSOC  TO COMM-EC69-MDLIV-NSOC                   
           MOVE COMM-EC67-MDLIV-NLIEU  TO COMM-EC69-MDLIV-NLIEU                 
           MOVE SPACES                 TO  COMM-EC69-LCOMMENT                   
      *                                                                         
           MOVE GA00-CMARQ   TO COMM-EC69-GA00-CMARQ                            
           MOVE GA00-CFAM    TO COMM-EC69-GA00-CFAM                             
           MOVE GA00-CAPPRO TO COMM-EC69-GA00-CAPPRO                            
           MOVE GA00-QLARGEUR TO COMM-EC69-GA00-QLARGEUR                        
           MOVE GA00-QPROFONDEUR TO COMM-EC69-GA00-QPROFONDEUR                  
           MOVE GA00-QHAUTEUR  TO COMM-EC69-GA00-QHAUTEUR                       
           MOVE GA00-QPOIDS TO COMM-EC69-GA00-QPOIDS                            
      *                                                                         
           MOVE COMM-EC67-FL05 TO COMM-EC69-FL05                                
           MOVE COMM-EC67-DDELIV TO COMM-EC69-DDELIV                            
           MOVE COMM-EC67-SSAAMMJJ TO COMM-EC69-SSAAMMJJ                        
           .                                                                    
       FIN-ALIM-MEC69-GV.    EXIT.                                              
      ***********************                                                   
       ALIM-MEC69-CONTROLE       SECTION.                                       
      *                                                                         
THEMIS     IF COMMANDE-PRECO                                                    
             SET EC69-COMMANDE-PRECO TO TRUE                                    
           END-IF                                                               
           SET  COMM-EC69-CONTROLE        TO TRUE                               
           MOVE 'O' TO COMM-EC69-CRE-GB05-FINAL                                 
           IF COMM-MEC64-TYPE-NB-MONO                                           
              SET COMM-EC69-MUT-UNIQUE  TO TRUE                                 
           ELSE                                                                 
              SET COMM-EC69-MUT-MULTI   TO TRUE                                 
           END-IF                                                               
      *                                                                         
           MOVE COMM-EC67-NCODIC   TO COMM-EC69-NCODIC                          
           MOVE COMM-EC67-NVENTE   TO COMM-EC69-NVENTE                          
           MOVE COMM-MEC64-TAB-WEMPORTE(1) TO COMM-EC69-WEMPORTE                
           MOVE COMM-EC67-QTE      TO COMM-EC69-QTE                             
           MOVE COMM-EC67-NSOCIETE   TO COMM-EC69-NSOCIETE                      
           MOVE COMM-EC67-NLIEU      TO COMM-EC69-NLIEU                         
           MOVE COMM-EC67-MDLIV-NSOC  TO COMM-EC69-MDLIV-NSOC                   
           MOVE COMM-EC67-MDLIV-NLIEU  TO COMM-EC69-MDLIV-NLIEU                 
           MOVE SPACES                 TO  COMM-EC69-LCOMMENT                   
      *                                                                         
           MOVE GA00-CMARQ   TO COMM-EC69-GA00-CMARQ                            
           MOVE GA00-CFAM    TO COMM-EC69-GA00-CFAM                             
           MOVE GA00-CAPPRO TO COMM-EC69-GA00-CAPPRO                            
           MOVE GA00-QLARGEUR TO COMM-EC69-GA00-QLARGEUR                        
           MOVE GA00-QPROFONDEUR TO COMM-EC69-GA00-QPROFONDEUR                  
           MOVE GA00-QHAUTEUR  TO COMM-EC69-GA00-QHAUTEUR                       
           MOVE GA00-QPOIDS TO COMM-EC69-GA00-QPOIDS                            
      *                                                                         
           MOVE COMM-EC67-FL05 TO COMM-EC69-FL05                                
           MOVE COMM-EC67-DDELIV TO COMM-EC69-DDELIV                            
           MOVE COMM-EC67-SSAAMMJJ TO COMM-EC69-SSAAMMJJ                        
           .                                                                    
       FIN-ALIM-MEC69-CONTROLE. EXIT.                                           
THEMIS ALIM-MEC69-MUCEN          SECTION.                                       
      *                                                                         
           SET  COMM-EC69-MUCEN           TO TRUE                               
           MOVE 'O' TO COMM-EC69-CRE-GB05-FINAL                                 
      *    IF COMM-MEC64-TYPE-NB-MONO                                           
      *       SET COMM-EC69-MUT-UNIQUE  TO TRUE                                 
      *    ELSE                                                                 
              SET COMM-EC69-MUT-MULTI   TO TRUE                                 
      *    END-IF                                                               
      *                                                                         
           MOVE W-QTE-ENTR1 TO COMM-EC69-QTE                                    
           MOVE W-QTE-ENTR2 TO COMM-EC69-QTE2                                   
           MOVE COMM-MEC64-NSOCIETE TO COMM-EC69-NSOCIETE                       
           MOVE COMM-MEC64-NLIEU    TO COMM-EC69-NLIEU                          
           MOVE MDLIV-LDEPLIVR-EXP (1:3)    TO COMM-EC69-MDLIV-NSOC             
           MOVE MDLIV-LDEPLIVR-EXP (4:3)    TO COMM-EC69-MDLIV-NLIEU            
      *    QUE 2 ENTREPOTS POUR CETTE V1                                        
           MOVE W-ENTR1             TO COMM-EC69-FL05-DEPOT (1)                 
           MOVE W-ENTR2             TO COMM-EC69-FL05-DEPOT (2)                 
           MOVE COMM-MEC64-NORDRE   TO COMM-EC69-NORDRE                         
           MOVE COMM-MEC64-NVENTE   TO COMM-EC69-NVENTE                         
           MOVE COMM-MEC64-TAB-WEMPORTE(1) TO COMM-EC69-WEMPORTE                
           MOVE SPACES                 TO  COMM-EC69-LCOMMENT                   
      *                                                                         
      *    MOVE GA00-CMARQ   TO COMM-EC69-GA00-CMARQ                            
      *    MOVE GA00-CFAM    TO COMM-EC69-GA00-CFAM                             
      *    MOVE GA00-CAPPRO TO COMM-EC69-GA00-CAPPRO                            
      *    MOVE GA00-QLARGEUR TO COMM-EC69-GA00-QLARGEUR                        
      *    MOVE GA00-QPROFONDEUR TO COMM-EC69-GA00-QPROFONDEUR                  
      *    MOVE GA00-QHAUTEUR  TO COMM-EC69-GA00-QHAUTEUR                       
      *    MOVE GA00-QPOIDS TO COMM-EC69-GA00-QPOIDS                            
      *                                                                         
RESAJ      PERFORM MUT-SL300-O-N                                                
THEMIS     IF COMMANDE-PRECO                                                    
 "            MOVE COMM-MEC64-DDELIV-COM  TO COMM-EC69-DDELIV                   
           ELSE                                                                 
RESAJ        IF WS-MUT-A-J = 'J'                                                
               MOVE COMM-MEC64-DJOUR   TO COMM-EC69-DDELIV                      
 "           ELSE                                                               
 "             MOVE COMM-MEC64-DDELIV-COM  TO COMM-EC69-DDELIV                  
 "           END-IF                                                             
 "         END-IF                                                               
           MOVE COMM-MEC64-DJOUR      TO COMM-EC69-SSAAMMJJ                     
           .                                                                    
       FIN-ALIM-MEC69-MUCEN. EXIT.                                              
      ***********************                                                   
       ALIM-SL21-UN              SECTION.                                       
      *                                                                         
           MOVE GV11-NSOCIETE         TO SL21-NSOCIETE                          
           MOVE GV11-NLIEU            TO SL21-NLIEU                             
           MOVE GV11-NVENTE           TO SL21-NVENTE                            
           MOVE GV11-NCODIC           TO SL21-NCODIC                            
           MOVE GV11-NSEQNQ           TO SL21-NSEQNQ                            
           .                                                                    
       FIN-ALIM-SL21-UN.     EXIT.                                              
      ***********************                                                   
LA1807*MODULE-RESERVATION-DACEM  SECTION.                                       
LA1807***********************************                                       
LA1807*                                                                         
LA1807*    MOVE ' NNNNNNN' TO COMM-EC68-NEW-MODE                                
LA1807*    SET COMM-EC68-NEW-APPEL TO TRUE                                      
LA1807*    SET COMM-EC68-CREATION-DACEM TO TRUE                                 
LA1807*       MOVE WS-MUT-A-J TO COMM-EC69-RESA-A-J                             
LA1807*    PERFORM APPEL-MEC68                                                  
LA1807*    MOVE COMM-EC68-CODRET TO TOP-ERREUR-MEC64                            
LA1807*    IF MEC64-PAS-ERREUR                                                  
LA1807*       MOVE SPACES TO GV11-LCOMMENT(6:20)                                
LA1807*       MOVE COMM-EC68-NMUTATION   TO GV11-LCOMMENT (6:7)                 
LA1807*       MOVE COMM-EC68-DATE-DISPO  TO GV11-LCOMMENT(14:8)                 
LA1807*       MOVE COMM-EC68-FILIERE     TO GV11-LCOMMENT(23:4)                 
LA1807*       MOVE SPACE                 TO GV22-NMUTATION                      
LA1807*                                                                         
LA1807*       MOVE W-SPDATDB2-DSYST      TO GV11-DSYST                          
LA1807*                                     GV22-DSYST                          
LA1807*       MOVE 'R'   TO GV22-CSTATUT                                        
LA1807*       MOVE ' '   TO GV11-WCQERESF                                       
LA1807*       MOVE ' '   TO GV22-WCQERESF                                       
LA1807*       MOVE -1 TO W-QTE                                                  
LA1807*       PERFORM UPDATE-RTSL20-UN                                          
LA1807*       PERFORM UPDATE-GV1100                                             
LA1807*                                                                         
LA1807*       MOVE GV11-NDEPOT    TO GV22-NDEPOT                                
LA1807*       MOVE GV11-NSOCLIVR  TO GV22-NSOCLIVR                              
LA1807* SI VENTE MONO -> CEQUIPE = MGV66                                        
LA1807*       IF SL20-INCOMPLETE = 'N'                                          
LA1807*          MOVE 'MGV66'  TO GV11-CEQUIPE                                  
LA1807*       END-IF                                                            
LA1807*                                                                         
LA1807*       PERFORM UPDATE-RTGV22                                             
LA1807*    ELSE                                                                 
LA1807*       MOVE COMM-EC68-CODRET   TO COMM-MEC64-CODE-RETOUR                 
LA1807*       MOVE COMM-EC68-LMESSAGE TO COMM-MEC64-MESSAGE                     
LA1807*    END-IF                                                               
LA1807*    .                                                                    
LA1807*FIN-MODULE-RESERVATION-DACEM. EXIT.                                      
      ************************************                                      
       MODULE-SORTIE  SECTION.                                                  
      *************************                                                 
           IF COMM-MEC64-CODE-RETOUR = '9999'                                   
              SET EC64-STATUT-ERREUR-GRAVE TO TRUE                              
           END-IF                                                               
ASI   *    VERSION DE TEST ON Y VA TOUT LE TEMPS                                
ASI   *    IF COMM-MEC64-CODE-RETOUR NOT > '0000'                               
 "            PERFORM ACTION-ASILAGE                                            
ASI   *    END-IF                                                               
           IF COMM-MEC64-ACTION-CONTROLE                                        
              AND W-TOP-CONTROL = 'O'                                           
              EVALUATE COMM-MEC64-STATUT                                        
                  WHEN '0'                                                      
                    STRING '<<<<<<<< LA VENTE ' COMM-MEC64-NVENTE               
                    ' EST COMPLETE >>>>>>>>'                                    
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                  WHEN '1'                                                      
                    STRING '<<<<<<<< LA VENTE ' COMM-MEC64-NVENTE               
                    ' EST EN ATTENTE FOURNISSEUR >>>>>>>>'                      
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                  WHEN '2'                                                      
                    STRING '<<<<<<<< LA VENTE ' COMM-MEC64-NVENTE               
                    ' EST INDISPONIBLE >>>>>>>>'                                
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                  WHEN '5'                                                      
                    STRING '<<<< LA VENTE ' COMM-MEC64-NVENTE                   
                    ' EST EN ATTENTE CONTREMAQUE >>>>>>>>'                      
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                  WHEN '6'                                                      
                    STRING '<<<< LA VENTE ' COMM-MEC64-NVENTE                   
                    ' EST INCOMPLETE, MIGRATION VERS GENERIX >>>>'              
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                  WHEN '3'                                                      
                  WHEN '9'                                                      
                    STRING '<<<<<<<< LA VENTE ' COMM-MEC64-NVENTE               
                    ' EST EN STATUT INDETERMINE >>>>>>>>'                       
                    DELIMITED BY SIZE INTO W-MESSAGE                            
              END-EVALUATE                                                      
              IF AUTO-DISPLAY                                                   
                 PERFORM DISPLAY-LOG                                            
              END-IF                                                            
           ELSE                                                                 
              IF COMM-MEC64-CODE-RETOUR > '0000'                                
                 STRING 'SORTIE, CODE RET : '                                   
                    COMM-MEC64-CODE-RETOUR ' STATUT :'                          
                    COMM-MEC64-STATUT  ' SUR VTE : '                            
                    COMM-MEC64-NVENTE  ' CODE APP :'                            
                    COMM-MEC64-ACTION                                           
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
           END-IF                                                               
           MOVE COMM-MEC64-APPLI            TO DFHCOMMAREA                      
           EXEC CICS RETURN                                                     
           END-EXEC                                                             
           .                                                                    
       FIN-MODULE-SORTIE. EXIT.                                                 
      **************************                                                
       TRT-MAJ-DDELIV-INI SECTION.                                              
      ****************************                                              
           PERFORM SELECT-RTGV11                                                
           PERFORM SELECT-RTGV22                                                
           MOVE W-SPDATDB2-DSYST           TO GV11-DSYST                        
           MOVE SPACES                     TO GV11-LCOMMENT (6:20)              
           PERFORM UPDATE-GV1100                                                
           if COMM-MEC64-CPROG   = 'MGV66'                                      
              PERFORM LOG-GV23                                                  
           END-IF                                                               
           MOVE 'A' TO GV22-CSTATUT                                             
           PERFORM UPDATE-RTGV22                                                
           PERFORM INSERT-RTSL21                                                
           MOVE  1 TO W-QTE                                                     
           PERFORM UPDATE-RTSL20-UN                                             
           .                                                                    
       FIN-TRT-MAJ-DDELIV-INI. EXIT.                                            
      *********************************                                         
      ******************************************************************        
      *                     G E S T I O N   D B 2                      *        
      ******************************************************************        
      *                                                                         
       DECLARE-RTGV11  SECTION.                                                 
      ***************************                                               
           MOVE COMM-MEC64-NSOCIETE        TO GV11-NSOCIETE.                    
           MOVE COMM-MEC64-NLIEU           TO GV11-NLIEU.                       
           MOVE COMM-MEC64-NVENTE          TO GV11-NVENTE.                      
           MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           EXEC SQL DECLARE  GV11 CURSOR FOR                                    
                    SELECT   GV11.NSOCIETE, GV11.NLIEU,                         
                        GV11.NVENTE,        GV11.CTYPENREG,                     
                        GV11.NCODICGRP,     GV11.NCODIC,                        
                        GV11.NSEQ,          GV11.CMODDEL,                       
                        GV11.DDELIV,        GV11.NORDRE,                        
                        GV11.CENREG,        GV11.QVENDUE,                       
                        GV11.WEMPORTE,      GV11.LCOMMENT,                      
                        GV11.NSOCLIVR,      GV11.NDEPOT,                        
                        GV11.WCQERESF,      GV11.NMUTATION,                     
                        GV11.WTOPELIVRE,    GV11.NSOCDEPLIV,                    
                        GV11.NLIEUDEPLIV,   GV11.CEQUIPE,                       
                        GV11.DCREATION ,                                        
                        GV22.CSTATUT ,                                          
                        GV11.NSEQNQ                                             
                     ,  CASE                                                    
                        WHEN GV99.WPRECO IS NULL                                
                            THEN ' '                                            
                        ELSE                                                    
                             GV99.WPRECO                                        
                        END                                                     
                    FROM     RVGV1199 GV11                                      
                        INNER JOIN RVGV2204 GV22                                
                         ON GV22.NSOCIETE  = GV11.NSOCIETE                      
                        AND GV22.NLIEU     = GV11.NLIEU                         
                        AND GV22.NVENTE    = GV11.NVENTE                        
                        AND GV22.NCODIC    = GV11.NCODIC                        
                        AND GV22.NCODICGRP = GV11.NCODICGRP                     
                        AND GV22.NSEQ      = GV11.NSEQ                          
                        LEFT OUTER JOIN RVGV9901 GV99                           
                        ON  GV99.NSOCIETE    = GV11.NSOCIETE                    
                        AND GV99.NLIEU       = GV11.NLIEU                       
                        AND GV99.NVENTE      = GV11.NVENTE                      
                        AND GV99.CTYPENREG   = GV11.CTYPENREG                   
                        AND GV99.NCODIC      = GV11.NCODIC                      
                        AND GV99.NCODICGRP   = GV11.NCODICGRP                   
                        AND GV99.NSEQ        = GV11.NSEQ                        
                    WHERE    GV11.NSOCIETE = :GV11-NSOCIETE                     
                    AND      GV11.NLIEU    = :GV11-NLIEU                        
                    AND      GV11.NVENTE   = :GV11-NVENTE                       
                    AND      GV11.CTYPENREG  = '1'                              
                    AND      GV11.CMODDEL    = 'EMR'                            
                    AND      GV11.WEMPORTE   <> ' '                             
                    ORDER BY LCOMMENT DESC                                      
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           EXEC SQL OPEN   GV11                                                 
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-DECLARE-RTGV11. EXIT.                                                
      ***************************                                               
       FETCH-RTGV11 SECTION.                                                    
      ***********************                                                   
           MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           EXEC SQL FETCH   GV11                                                
               INTO    :GV11-NSOCIETE,     :GV11-NLIEU,                         
                       :GV11-NVENTE,       :GV11-CTYPENREG,                     
                       :GV11-NCODICGRP,    :GV11-NCODIC,                        
                       :GV11-NSEQ,         :GV11-CMODDEL,                       
                       :GV11-DDELIV,       :GV11-NORDRE,                        
                       :GV11-CENREG,       :GV11-QVENDUE,                       
                       :GV11-WEMPORTE,     :GV11-LCOMMENT,                      
                       :GV11-NSOCLIVR,     :GV11-NDEPOT,                        
                       :GV11-WCQERESF,     :GV11-NMUTATION,                     
                       :GV11-WTOPELIVRE,   :GV11-NSOCDEPLIV,                    
                       :GV11-NLIEUDEPLIV,  :GV11-CEQUIPE,                       
                       :GV11-DCREATION,                                         
                       :GV22-CSTATUT                                            
                     , :GV11-NSEQNQ                                             
                     , :GV99-WPRECO                                             
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF TROUVE                                                            
LA1807*      IF GV11-LCOMMENT(1:5) = 'DARTY' OR 'DACEM' OR '     '              
LA1807       IF GV11-LCOMMENT(1:5) = 'DARTY' OR '     '                         
                SET LCOMMENT-VALIDE TO TRUE                                     
             ELSE                                                               
               STRING 'GV11-LCOMMENT BIZARRE ' GV11-LCOMMENT                    
                ' ON NE TRAITE PAS CETTE LIGNE'                                 
                DELIMITED BY SIZE INTO W-MESSAGE                                
                PERFORM DISPLAY-LOG                                             
               SET LCOMMENT-INVALIDE TO TRUE                                    
             END-IF                                                             
           END-IF                                                               
           .                                                                    
       FIN-FETCH-RTGV11. EXIT.                                                  
      *************************                                                 
       CLOSE-RTGV11 SECTION.                                                    
      **********************                                                    
           MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION                     
           MOVE 'RTGV11'              TO TABLE-NAME                             
           EXEC SQL CLOSE   GV11                                                
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-CLOSE-RTGV11. EXIT.                                                  
      *************************                                                 
LA0611 SELECT-GV11-CMODDEL SECTION.                                             
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           MOVE 'RTGV11'              TO TABLE-NAME                             
           MOVE COMM-MEC64-NSOCIETE        TO GV11-NSOCIETE.                    
           MOVE COMM-MEC64-NLIEU           TO GV11-NLIEU.                       
           MOVE COMM-MEC64-NVENTE          TO GV11-NVENTE.                      
           EXEC SQL                                                             
                SELECT GV11.WEMPORTE                                            
                INTO :WS-MDLIV-WEMPORTE                                         
                FROM     RVGV1199 GV11                                          
                    INNER JOIN RVGV2204 GV22                                    
                     ON GV22.NSOCIETE  = GV11.NSOCIETE                          
                    AND GV22.NLIEU     = GV11.NLIEU                             
                    AND GV22.NVENTE    = GV11.NVENTE                            
                    AND GV22.NCODIC    = GV11.NCODIC                            
                    AND GV22.NCODICGRP = GV11.NCODICGRP                         
                    AND GV22.NSEQ      = GV11.NSEQ                              
                WHERE    GV11.NSOCIETE = :GV11-NSOCIETE                         
                AND      GV11.NLIEU    = :GV11-NLIEU                            
                AND      GV11.NVENTE   = :GV11-NVENTE                           
                AND      GV11.CTYPENREG  = '1'                                  
                AND      GV11.CMODDEL    = 'EMR'                                
                AND      GV11.WEMPORTE   <> ' '                                 
                FETCH FIRST 1 ROW ONLY                                          
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
LA0611     .                                                                    
      *************************                                                 
       UPDATE-RTGV11 SECTION.                                                   
      *----------------------                                                   
           MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION                     
           MOVE COMM-MEC64-NSOCIETE            TO GV11-NSOCIETE                 
           MOVE COMM-MEC64-NLIEU               TO GV11-NLIEU                    
           MOVE COMM-MEC64-NVENTE              TO GV11-NVENTE                   
           MOVE COMM-MEC64-TAB-NCODICGRP(I-NL) TO GV11-NCODICGRP                
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)    TO GV11-NCODIC                   
           MOVE COMM-MEC64-TAB-NSEQ(I-NL)      TO GV11-NSEQ                     
           MOVE COMM-MEC64-TAB-CTYPENREG(I-NL) TO GV11-CTYPENREG                
           MOVE COMM-MEC64-TAB-DDELIVO(I-NL)   TO GV11-DDELIV                   
           MOVE W-SPDATDB2-DSYST               TO GV11-DSYST                    
           EXEC SQL                                                             
                UPDATE RVGV1106                                                 
                SET DDELIV  = :GV11-DDELIV                                      
               ,CEQUIPE     = :GV11-CEQUIPE                                     
               ,LCOMMENT    = :GV11-LCOMMENT                                    
               ,NSOCLIVR    = :GV11-NSOCLIVR                                    
               ,NDEPOT      = :GV11-NDEPOT                                      
               ,WCQERESF    = :GV11-WCQERESF                                    
               ,DSYST       = :GV11-DSYST                                       
                WHERE                                                           
                    NSOCIETE    =:GV11-NSOCIETE                                 
                AND NLIEU       =:GV11-NLIEU                                    
                AND NVENTE      =:GV11-NVENTE                                   
                AND NCODICGRP   =:GV11-NCODICGRP                                
                AND NCODIC      =:GV11-NCODIC                                   
                AND NSEQ        =:GV11-NSEQ                                     
                AND CTYPENREG   =:GV11-CTYPENREG                                
                WITH CS                                                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-UPDATE-RTGV11. EXIT.                                                 
      *                                                                         
       UPDATE-RTGV22-ENC SECTION.                                               
      *----------------------                                                   
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE W-SPDATDB2-DSYST      TO GV22-DSYST                             
           EXEC SQL                                                             
                UPDATE RVGV2204                                                 
                SET CEQUIPE    = :GV11-CEQUIPE                                  
                   ,CSTATUT    = :GV22-CSTATUT                                  
                   ,NSOCLIVR   = :GV22-NSOCLIVR                                 
                   ,NDEPOT     = :GV22-NDEPOT                                   
                   ,WCQERESF   = :GV22-WCQERESF                                 
                   ,NMUTATION  = :GV22-NMUTATION                                
                   ,DSYST      = :GV22-DSYST                                    
                WHERE NSOCIETE = :GV11-NSOCIETE                                 
                AND   NLIEU    = :GV11-NLIEU                                    
                AND   NVENTE   = :GV11-NVENTE                                   
                AND   NSEQ     = :GV11-NSEQ                                     
                AND   NCODIC   = :GV11-NCODIC                                   
                AND   NCODICGRP = :GV11-NCODICGRP                               
                WITH CS                                                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-UPDATE-RTGV22-ENC. EXIT.                                             
      *                                                                         
       INSERT-RTGV27 SECTION.                                                   
      *----------------------                                                   
           MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION                     
           MOVE COMM-MEC64-DJOUR      TO GV27-DAUTORD                           
           EXEC SQL SELECT MAX(NAUTORD)                                         
                    INTO  :GV27-NAUTORD  :GV27-NAUTORD-F                        
                    FROM  RVGV2700                                              
                    WHERE DAUTORD = :GV27-DAUTORD                               
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF GV27-NAUTORD-F < ZEROES                                           
             MOVE '0000000' TO GV27-NAUTORD                                     
           END-IF.                                                              
           MOVE GV27-NAUTORD TO WNAUTORD-NUM                                    
           ADD 1             TO WNAUTORD-NUM                                    
           MOVE WNAUTORD-NUM TO GV27-NAUTORD                                    
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           MOVE COMM-MEC64-NSOCIETE     TO GV27-NSOCIETE                        
           MOVE COMM-MEC64-NLIEU        TO GV27-NLIEU                           
           MOVE COMM-MEC64-NORDRE       TO GV27-NORDRE                          
           MOVE COMM-MEC64-DJOUR        TO GV27-DAUTORD                         
           MOVE 'EXPPB'                 TO GV27-CAUTORD                         
           MOVE  GV11-DCREATION         TO GV27-DVENTE                          
           MOVE W-SPDATDB2-DSYST          TO GV27-DSYST                         
                                             WDATHEUR-ALPHA                     
           MOVE WDATHEUR-ALPHA (6:2)      TO GV27-DHAUTORD                      
           MOVE COMM-EC69-CODRET (1:2)  TO GV27-DHAUTORDUTI                     
           MOVE WDATHEUR-ALPHA (8:2)      TO GV27-DMAUTORD                      
           MOVE COMM-EC69-CODRET(3:2)     TO GV27-DMAUTORDUTI                   
           EXEC SQL                                                             
                INSERT INTO RVGV2700                                            
                       (NSOCIETE       ,                                        
                        NLIEU          ,                                        
                        NORDRE         ,                                        
                        DAUTORD        ,                                        
                        DHAUTORD       ,                                        
                        DMAUTORD       ,                                        
                        DHAUTORDUTI    ,                                        
                        DMAUTORDUTI    ,                                        
                        NAUTORD        ,                                        
                        CAUTORD        ,                                        
                        DVENTE         ,                                        
                        DSYST          )                                        
                        VALUES (:RVGV2700)                                      
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-INSERT-RTGV27. EXIT.                                                 
      *************************                                                 
       SELECT-RTSL21              SECTION.                                      
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           MOVE 'RTSL21'              TO TABLE-NAME.                            
           MOVE COMM-MEC64-NSOCIETE          TO SL21-NSOCIETE                   
           MOVE COMM-MEC64-NLIEU             TO SL21-NLIEU                      
           MOVE COMM-MEC64-NVENTE            TO SL21-NVENTE                     
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)  TO SL21-NCODIC                     
           MOVE COMM-MEC64-TAB-NSEQNQ(I-NL)  TO SL21-NSEQNQ                     
           EXEC SQL SELECT   QTEMANQUANTE                                       
                    INTO    :SL21-QTEMANQUANTE                                  
                    FROM     RVSL2100                                           
                    WHERE    NSOCIETE = :SL21-NSOCIETE                          
                    AND      NLIEU    = :SL21-NLIEU                             
                    AND      NVENTE   = :SL21-NVENTE                            
                    AND      NCODIC   = :SL21-NCODIC                            
                    AND      NSEQNQ   = :SL21-NSEQNQ                            
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           IF TROUVE                                                            
              MOVE SL21-QTEMANQUANTE  TO COMM-MEC64-TAB-QVENDUE(I-NL)           
              SET CODIC-TRT-OK        TO TRUE                                   
           ELSE                                                                 
              SET CODIC-TRT-KO        TO TRUE                                   
           END-IF                                                               
           .                                                                    
       FIN-SELECT-RTSL21.      EXIT.                                            
      *************************                                                 
       INSERT-RTSL21 SECTION.                                                   
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2100'              TO TABLE-NAME                             
           INITIALIZE RVSL2100                                                  
           MOVE GV11-NCODIC      TO SL21-NCODIC                                 
           MOVE GV11-DDELIV      TO SL21-DDELIV                                 
           MOVE GV11-NSOCIETE    TO SL21-NSOCIETE                               
           MOVE GV11-NLIEU       TO SL21-NLIEU                                  
           MOVE GV11-NVENTE      TO SL21-NVENTE                                 
           MOVE GV11-NSEQNQ      TO SL21-NSEQNQ                                 
           MOVE GV11-WEMPORTE    TO SL21-WEMPORTE                               
           MOVE GV11-QVENDUE     TO SL21-QTE                                    
           MOVE GV11-QVENDUE     TO SL21-QTEMANQUANTE                           
           IF  GV11-DDELIV < COMM-EC67-DDELIV                                   
               MOVE 'KO' TO W-SL20-STATUT                                       
           ELSE                                                                 
               MOVE 'IN' TO W-SL20-STATUT                                       
           END-IF                                                               
           IF GV11-WCQERESF = 'F'                                               
              MOVE 'F'   TO SL21-STATUT                                         
           ELSE                                                                 
              MOVE 'A'   TO SL21-STATUT                                         
           END-IF.                                                              
           MOVE 'MEC64'          TO SL21-PGMMAJ                                 
           MOVE W-SPDATDB2-DSYST TO SL21-DSYST                                  
           EXEC SQL INSERT INTO RVSL2100                                        
                            (NSOCIETE, NLIEU, NVENTE, NCODIC,                   
                             NSEQNQ, WEMPORTE, QTE , QTEMANQUANTE,              
                             STATUT, DDELIV, PGMMAJ,                            
                             DSYST)                                             
                    VALUES  (:SL21-NSOCIETE, :SL21-NLIEU,                       
                             :SL21-NVENTE,   :SL21-NCODIC,                      
                             :SL21-NSEQNQ,   :SL21-WEMPORTE,                    
                             :SL21-QTE,                                         
                             :SL21-QTEMANQUANTE, :SL21-STATUT,                  
                             :SL21-DDELIV,   :SL21-PGMMAJ,                      
                             :SL21-DSYST)                                       
           END-EXEC.                                                            
           IF SQLCODE NOT = -803                                                
              PERFORM TEST-CODE-RETOUR-SQL                                      
           END-IF                                                               
           IF GV22-CSTATUT = 'A'                                                
              ADD 1 TO W-NB-LIGNE-SL21                                          
           END-IF                                                               
           .                                                                    
       FIN-INSERT-RTSL21. EXIT.                                                 
       INSERT-RTSL21-ENC SECTION.                                               
      ***********************                                                   
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2100'              TO TABLE-NAME                             
           INITIALIZE RVSL2100                                                  
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)   TO SL21-NCODIC                    
           MOVE COMM-MEC64-TAB-DDELIVO(I-NL)  TO SL21-DDELIV                    
           MOVE COMM-MEC64-NSOCIETE           TO SL21-NSOCIETE                  
           MOVE COMM-MEC64-NLIEU              TO SL21-NLIEU                     
           MOVE COMM-MEC64-NVENTE             TO SL21-NVENTE                    
           MOVE COMM-MEC64-TAB-NSEQNQ(I-NL)   TO SL21-NSEQNQ                    
           MOVE COMM-MEC64-TAB-QVENDUE(I-NL)  TO SL21-QTE                       
                                                 SL21-QTEMANQUANTE              
           MOVE COMM-MEC64-TAB-WEMPORTE(I-NL) TO SL21-WEMPORTE                  
           MOVE W-SPDATDB2-DSYST              TO SL21-DSYST                     
           MOVE 'KO' TO W-SL20-STATUT                                           
           IF EC64-ARTICLE-EN-CF (I-NL)                                         
              MOVE 'F'   TO SL21-STATUT                                         
           ELSE                                                                 
              MOVE 'A'   TO SL21-STATUT                                         
           END-IF.                                                              
           MOVE 'MEC64'          TO SL21-PGMMAJ                                 
           EXEC SQL INSERT INTO RVSL2100                                        
                            (NSOCIETE, NLIEU, NVENTE, NCODIC,                   
                             NSEQNQ, WEMPORTE, QTE , QTEMANQUANTE,              
                             STATUT, DDELIV, PGMMAJ,                            
                             DSYST)                                             
                    VALUES  (:SL21-NSOCIETE, :SL21-NLIEU,                       
                             :SL21-NVENTE,   :SL21-NCODIC,                      
                             :SL21-NSEQNQ,   :SL21-WEMPORTE,                    
                             :SL21-QTE,                                         
                             :SL21-QTEMANQUANTE, :SL21-STATUT,                  
                             :SL21-DDELIV,   :SL21-PGMMAJ,                      
                             :SL21-DSYST)                                       
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           MOVE SQLCODE TO EDT-SQL                                              
           MOVE SL21-NSEQNQ TO EDT-NSEQ                                         
           IF TROUVE                                                            
              SET CREATION-RTSL21 TO TRUE                                       
              ADD 1 TO W-NB-LIGNE-SL21                                          
           END-IF                                                               
           .                                                                    
       FIN-INSERT-RTSL21-ENC. EXIT.                                             
      **************************                                                
      *                                                                         
ASI    DECLARE-RTGV11-AS00  SECTION.                                            
      ***************************                                               
           MOVE COMM-MEC64-NSOCIETE        TO GV10-NSOCIETE.                    
           MOVE COMM-MEC64-NLIEU           TO GV10-NLIEU.                       
           MOVE COMM-MEC64-NVENTE          TO GV10-NVENTE.                      
           MOVE COMM-MEC64-DJOUR           TO GV11-DDELIV.                      
           MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           EXEC SQL DECLARE CAS00 CURSOR FOR                                    
          SELECT A.NSOCIETE , A.NLIEU, A.NVENTE,SUM(QVENDUE),                   
          E.CCAMPAGNE                                                           
          FROM RVGV1099 A , RVGV1199 B , RVGA0000 C                             
          INNER JOIN RVAS1000 D ON C.CFAM = D.CFAM                              
                                OR D.CFAM = '*TOUT'                             
          INNER JOIN RVAS0000 E ON D.CCAMPAGNE = E.CCAMPAGNE                    
            AND ( B.WEMPORTE = E.CTRANS1                                        
               OR B.WEMPORTE = E.CTRANS2                                        
               OR B.WEMPORTE = E.CTRANS3                                        
               OR B.WEMPORTE = E.CTRANS4                                        
               OR B.WEMPORTE = E.CTRANS5                                        
               OR B.WEMPORTE = E.CTRANS6                                        
               OR B.WEMPORTE = E.CTRANS7                                        
               OR B.WEMPORTE = E.CTRANS8 )                                      
            AND B.DDELIV >= E.DDEBUT AND B.DDELIV <= E.DFIN                     
            AND A.PTTVENTE >= E.DMINI AND A.PTTVENTE <= E.DMAXI                 
            AND (( A.NLIEU = '040' AND E.CDCOM = 'O' )                          
              OR ( A.NLIEU = '099' AND E.CBTOB = 'O' )                          
MGD           OR ( A.NLIEU = '240' AND E.CMGD  = 'O' )                          
              OR ( A. NLIEU NOT IN ('040','099','240')                          
                        AND E.CMAG = 'O' ))                                     
      *                 AND E.CDCOM = 'N'                                       
MGD   *                 AND E.CMGD  = 'N'                                       
      *                 AND E.CBTOB = 'N')                                      
      *        OR ( E.CMAG = 'O' AND E.CDCOM = 'O' AND E.CBTOB = 'O'            
MGD   *                          AND E.CMGD  = 'O' ))                           
      *     AND (QVENDUE <= 1 AND E.CMONO = 'O'                                 
            AND E.CMONO = 'O'                                                   
      *      OR  QVENDUE >  1 AND E.CMULTI = 'O')                               
                 WHERE A.NSOCIETE = :GV10-NSOCIETE                              
                 AND A.NLIEU = :GV10-NLIEU                                      
                 AND A.NVENTE = :GV10-NVENTE                                    
                 AND A.NSOCIETE = B.NSOCIETE                                    
                 AND A.NLIEU = B.NLIEU                                          
                 AND A.NVENTE = B.NVENTE                                        
                 AND B.NCODIC = C.NCODIC                                        
                 AND B.CTYPENREG = '1'                                          
                 AND B.WEMPORTE > ' '   AND B.WTOPELIVRE = 'N'                  
      *          AND B.NSOCLIVR = '907' AND B.NDEPOT = '088'                    
                 AND NOT EXISTS (SELECT '1' FROM RVEC0301 F                     
                                WHERE F.NSOCIETE   = B.NSOCIETE                 
                                 AND  F.NLIEU      = B.NLIEU                    
                                 AND  F.NVENTE     = B.NVENTE                   
                                 AND  F.NSEQNQ     = B.NSEQNQ                   
                                 AND  F.CCOLIS  > ' ')                          
DA0909           AND EXISTS (SELECT SUM(QVENDUE) FROM RVGV1199 G                
  "                 WHERE A.NSOCIETE   = G.NSOCIETE                             
JIRA                 AND  A.NLIEU      = G.NLIEU                                
DCOM                 AND  A.NVENTE     = G.NVENTE                               
 247                 AND  G.CTYPENREG  = '1'                                    
  "                  AND  G.CENREG     = ' '                                    
  "                  GROUP BY  G.NSOCIETE,G.NLIEU,G.NVENTE                      
DA0909               HAVING SUM(QVENDUE) = 1 )                                  
                 GROUP BY A.NSOCIETE , A.NLIEU, A.NVENTE , E.CCAMPAGNE          
      *          HAVING SUM(QVENDUE) <= 1                                       
          UNION ALL                                                             
          SELECT A.NSOCIETE , A.NLIEU, A.NVENTE,SUM(QVENDUE),                   
          E.CCAMPAGNE                                                           
          FROM RVGV1099 A , RVGV1199 B , RVGA0000 C                             
          INNER JOIN RVAS1000 D ON C.CFAM = D.CFAM                              
                                OR D.CFAM = '*TOUT'                             
          INNER JOIN RVAS0000 E ON D.CCAMPAGNE = E.CCAMPAGNE                    
            AND ( B.WEMPORTE = E.CTRANS1                                        
               OR B.WEMPORTE = E.CTRANS2                                        
               OR B.WEMPORTE = E.CTRANS3                                        
               OR B.WEMPORTE = E.CTRANS4                                        
               OR B.WEMPORTE = E.CTRANS5                                        
               OR B.WEMPORTE = E.CTRANS6                                        
               OR B.WEMPORTE = E.CTRANS7                                        
               OR B.WEMPORTE = E.CTRANS8 )                                      
            AND B.DDELIV >= E.DDEBUT AND B.DDELIV <= E.DFIN                     
            AND A.PTTVENTE >= E.DMINI AND A.PTTVENTE <= E.DMAXI                 
            AND (( A.NLIEU = '040' AND E.CDCOM = 'O' )                          
MGD           OR ( A.NLIEU = '240' AND E.CMGD  = 'O' )                          
              OR ( A. NLIEU NOT IN ('040','099','240')                          
                        AND E.CMAG = 'O' ))                                     
      *                 AND E.CDCOM = 'N'                                       
MGD   *                 AND E.CMGD  = 'N'                                       
      *                 AND E.CBTOB = 'N')                                      
      *        OR ( E.CMAG = 'O' AND E.CDCOM = 'O' AND E.CBTOB = 'O'            
MGD   *                          AND E.CMGD  = 'O' ))                           
      *     AND (QVENDUE <= 1 AND E.CMONO = 'O'                                 
            AND E.CMULTI = 'O'                                                  
      *      OR  QVENDUE >  1 AND E.CMULTI = 'O')                               
                 WHERE A.NSOCIETE = :GV10-NSOCIETE                              
                 AND A.NLIEU = :GV10-NLIEU                                      
                 AND A.NVENTE = :GV10-NVENTE                                    
                 AND A.NSOCIETE = B.NSOCIETE                                    
                 AND A.NLIEU = B.NLIEU                                          
                 AND A.NVENTE = B.NVENTE                                        
                 AND B.NCODIC = C.NCODIC                                        
                 AND B.CTYPENREG = '1'                                          
                 AND B.WEMPORTE > ' '   AND B.WTOPELIVRE = 'N'                  
      *          AND B.NSOCLIVR = '907' AND B.NDEPOT = '088'                    
                 AND NOT EXISTS (SELECT '1' FROM RVEC0301 F                     
                                WHERE F.NSOCIETE   = B.NSOCIETE                 
                                 AND  F.NLIEU      = B.NLIEU                    
                                 AND  F.NVENTE     = B.NVENTE                   
                                 AND  F.NSEQNQ     = B.NSEQNQ                   
                                 AND  F.CCOLIS  > ' ')                          
DA0909           AND EXISTS (SELECT SUM(QVENDUE) FROM RVGV1199 G                
  "                 WHERE A.NSOCIETE   = G.NSOCIETE                             
JIRA                 AND  A.NLIEU      = G.NLIEU                                
DCOM                 AND  A.NVENTE     = G.NVENTE                               
 247                 AND  G.CTYPENREG  = '1'                                    
  "                  AND  G.CENREG     = ' '                                    
  "                  GROUP BY  G.NSOCIETE,G.NLIEU,G.NVENTE                      
DA0909               HAVING SUM(QVENDUE) > 1 )                                  
                 GROUP BY A.NSOCIETE , A.NLIEU, A.NVENTE , E.CCAMPAGNE          
      *          HAVING SUM(QVENDUE) > 1                                        
                FOR FETCH ONLY                                                  
                WITH UR                                                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           EXEC SQL OPEN  CAS00                                                 
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-DECLARE-RTGV11-AS00. EXIT.                                           
      ***************************                                               
ASI    FETCH-RTGV11-AS00 SECTION.                                               
      ***********************                                                   
           MOVE FUNC-FETCH            TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           EXEC SQL FETCH  CAS00                                                
               INTO   :GV10-NSOCIETE                                            
                    , :GV10-NLIEU                                               
                    , :GV10-NVENTE                                              
                    , :GV11-QVENDUE                                             
                    , :AS00-CCAMPAGNE                                           
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF TROUVE                                                            
              SET WC-CAS00-SUITE TO TRUE                                        
           ELSE                                                                 
              SET WC-CAS00-FIN TO TRUE                                          
           END-IF                                                               
           .                                                                    
       FIN-FETCH-RTGV11-AS00. EXIT.                                             
      *************************                                                 
ASI    CLOSE-RTGV11-AS00 SECTION.                                               
      **********************                                                    
           MOVE FUNC-CLOSE            TO TRACE-SQL-FUNCTION                     
           MOVE 'RTGV11'              TO TABLE-NAME                             
           EXEC SQL CLOSE  CAS00                                                
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-CLOSE-RTGV11-AS00. EXIT.                                             
       UPDATE-RTGV10 SECTION.                                           04160001
           MOVE FUNC-UPDATE  TO TRACE-SQL-FUNCTION.                     04170001
           MOVE 'RTGV10  '    TO TABLE-NAME.                            04180001
           MOVE 'CAS00 '      TO MODEL-NAME.                            04190001
           EXEC SQL                                                     04230001
                UPDATE RVGV1005                                         04240004
                SET LDESCRIPTIF1 = 'ELIGIBLE CAMPAGNE(S) :' ,           04250001
                LDESCRIPTIF2 = :WS-DESC                                 04260001
                 WHERE NSOCIETE = :OLD-NSOCIETE                         04270001
                 AND NLIEU = :OLD-NLIEU                                 04280001
                 AND NVENTE = :OLD-NVENTE                               04290001
           END-EXEC                                                     04300004
           PERFORM TEST-CODE-RETOUR-SQL.                                04870001
                                                                        04880001
       FIN-UPDATE-RTGV10. EXIT.                                         04890001
       CHERCHE-LIEU-STOCKAGE SECTION.                                           
      *******************************                                           
      * TS                                                                      
           INITIALIZE TS-MFL05-DONNEES.                                         
           SET TS-MFL05-DEMANDE-CODIC   TO TRUE.                                
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)   TO TS-MFL05-NCODIC                
           MOVE COMM-MEC64-TAB-CFAM(I-NL)    TO TS-MFL05-CFAM                   
           IF EIBTRMID = LOW-VALUES                                             
              MOVE EIBTASKN              TO P-EIBTASKN                          
              MOVE X-EIBTASKN TO EIBTRMID                                       
           END-IF                                                               
           STRING 'FL05' EIBTRMID DELIMITED BY SIZE INTO IDENT-TS               
           END-STRING                                                           
           PERFORM WRITE-TS-MFL05                                               
      * COMM                                                                    
           MOVE MDLIV-CTYPTRAIT              TO COMM-MFL05-CTYPTRAIT            
           MOVE MDLIV-LDEPLIVR-EXP           TO COMM-MFL05-LDEPLIV              
THEMIS     IF COMM-MEC64-TAB-DDELIVO (I-NL) > COMM-MEC64-DJOUR                  
              MOVE COMM-MEC64-TAB-DDELIVO (I-NL) TO COMM-MFL05-DEFFET           
           ELSE                                                                 
              MOVE COMM-MEC64-DJOUR             TO COMM-MFL05-DEFFET            
           END-IF                                                               
           MOVE LENGTH OF COMM-MFL05-APPLI  TO LONG-COMMAREA-LINK               
           MOVE COMM-MFL05-APPLI            TO Z-COMMAREA-LINK                  
           MOVE 'MFL05'                     TO NOM-PROG-LINK                    
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK             TO COMM-MFL05-APPLI                 
           IF COMM-MFL05-CODRET-ERREUR                                          
              MOVE '1'                 TO KONTROL                               
           ELSE                                                                 
      * -> LECTURE TS DE RETOUR DU MFL05 ET CONTROLE DES STOCKS.                
              MOVE LENGTH OF TS-MFL05-DONNEES  TO LONG-TS                       
              MOVE 1                     TO RANG-TS                             
              PERFORM READ-TS                                                   
              MOVE Z-INOUT               TO TS-MFL05-DONNEES                    
              PERFORM VARYING IDEP FROM 1 BY 1                                  
              UNTIL (TS-MFL05-DEPOT (IDEP) = SPACES)                            
              OR (IDEP > 5)                                                     
                 MOVE TS-MFL05-DEPOT (IDEP)(1:3)                                
                            TO COMM-MEC64-FL05-NSOCDEPOT(I-NL, IDEP)            
                 MOVE TS-MFL05-DEPOT (IDEP)(4:3)                                
                            TO COMM-MEC64-FL05-NDEPOT(I-NL, IDEP)               
              END-PERFORM                                                       
           END-IF                                                               
           PERFORM DELETE-TS                                                    
           .                                                                    
       FIN-CHERCHE-LIEU-STOCKAGE. EXIT.                                         
      **********************************                                        
       TEST-CODE-RETOUR-SQL SECTION.                                            
      *********************************                                         
            MOVE '0'                  TO CODE-RETOUR                            
            MOVE SQLCODE              TO SQL-CODE                               
            MOVE SQLCODE              TO TRACE-SQL-CODE                         
            IF CODE-DECLARE                                                     
               MOVE 0                    TO SQL-CODE                            
            END-IF                                                              
            IF CODE-SELECT OR CODE-FETCH OR CODE-UPDATE                         
               IF SQL-CODE = +100                                               
                  MOVE '1'                  TO CODE-RETOUR                      
               END-IF                                                           
            END-IF.                                                             
            IF SQL-CODE < +0                                                    
               CALL DSNTIAR USING SQLCA, MSG-SQL, MSG-SQL-LRECL                 
               PERFORM VARYING I-SQL FROM 1 BY 1 UNTIL I-SQL > 3                
                  move  MSG-SQL-MSG (I-SQL) to w-message                        
                  perform DISPLAY-LOG                                           
               END-PERFORM                                                      
               MOVE '9999' TO COMM-MEC64-CODE-RETOUR                            
               PERFORM MODULE-SORTIE                                            
            END-IF                                                              
            .                                                                   
       FIN-TEST-CODE-RETOUR-SQL. EXIT.                                          
      **********************************                                        
       SELECT-RTSL20  SECTION.                                                  
      *************************                                                 
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           MOVE 'RTSL20'              TO TABLE-NAME.                            
           EXEC SQL SELECT   INCOMPLETE, DATERESERVATION                        
                             , STATUT                                           
                    INTO    :SL20-INCOMPLETE                                    
                          , :SL20-DATERESERVATION                               
                          , :SL20-STATUT                                        
                    FROM     RVSL2000                                           
                    WHERE    NSOCIETE = :GV11-NSOCIETE                          
                    AND      NLIEU    = :GV11-NLIEU                             
                    AND      NVENTE   = :GV11-NVENTE                            
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-RTSL20. EXIT.                                                 
      ************************                                                  
       SELECT-SL20  SECTION.                                                    
      *************************                                                 
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           MOVE 'RTSL20'              TO TABLE-NAME.                            
           EXEC SQL SELECT   NBLIGNENS , INCOMPLETE                             
                             , STATUT, DATERESERVATION                          
                INTO    :SL20-NBLIGNENS                                         
                      , :SL20-INCOMPLETE                                        
                      , :SL20-STATUT                                            
                      , :SL20-DATERESERVATION                                   
                FROM     RVSL2000                                               
                WHERE    NSOCIETE = :GV11-NSOCIETE                              
                AND      NLIEU    = :GV11-NLIEU                                 
                AND      NVENTE   = :GV11-NVENTE                                
CEN012*         WITH UR                                                         
           END-EXEC                                                             
           .                                                                    
       FIN-SELECT-SL20. EXIT.                                                   
      ************************                                                  
       SELECT-RTGV11  SECTION.                                                  
      *************************                                                 
           MOVE COMM-MEC64-NLIEU               TO GV11-NLIEU                    
           MOVE COMM-MEC64-NSOCIETE            TO GV11-NSOCIETE                 
           MOVE COMM-MEC64-NVENTE              TO GV11-NVENTE                   
           MOVE COMM-MEC64-TAB-NCODIC(I-NL)    TO GV11-NCODIC                   
           MOVE COMM-MEC64-TAB-NSEQ  (I-NL)    TO GV11-NSEQ                     
           MOVE COMM-MEC64-TAB-NCODICGRP(I-NL) TO GV11-NCODICGRP                
           MOVE COMM-MEC64-TAB-CTYPENREG(I-NL) TO GV11-CTYPENREG                
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           EXEC SQL SELECT   DDELIV                                             
                            ,CEQUIPE                                            
                            ,LCOMMENT                                           
                            ,WCQERESF                                           
                            ,NSOCLIVR                                           
                            ,NDEPOT                                             
                            ,QVENDUE                                            
                            ,NSEQNQ                                             
                            ,WEMPORTE                                           
                    INTO    :GV11-DDELIV                                        
                           ,:GV11-CEQUIPE                                       
                           ,:GV11-LCOMMENT                                      
                           ,:GV11-WCQERESF                                      
                           ,:GV11-NSOCLIVR                                      
                           ,:GV11-NDEPOT                                        
                           ,:GV11-QVENDUE                                       
                           ,:GV11-NSEQNQ                                        
                           ,:GV11-WEMPORTE                                      
                    FROM     RVGV1199 GV11                                      
                    WHERE    NSOCIETE  = :GV11-NSOCIETE                         
                    AND      NLIEU     = :GV11-NLIEU                            
                    AND      NVENTE    = :GV11-NVENTE                           
                    AND      NCODIC    = :GV11-NCODIC                           
                    AND      NCODICGRP = :GV11-NCODICGRP                        
                    AND      CTYPENREG = :GV11-CTYPENREG                        
                    AND      NSEQ      = :GV11-NSEQ                             
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-RTGV11. EXIT.                                                 
      ************************                                                  
       SELECT-RTGV11-NQ SECTION.                                                
      *************************                                                 
           MOVE COMM-MEC64-NLIEU               TO GV11-NLIEU                    
           MOVE COMM-MEC64-NSOCIETE            TO GV11-NSOCIETE                 
           MOVE COMM-MEC64-NVENTE              TO GV11-NVENTE                   
           MOVE COMM-MEC64-TAB-NCODIC(1)       TO GV11-NCODIC                   
           MOVE COMM-MEC64-TAB-NSEQNQ(1)       TO GV11-NSEQNQ                   
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           EXEC SQL SELECT   DDELIV                                             
                            ,CEQUIPE                                            
                            ,LCOMMENT                                           
                            ,WCQERESF                                           
                            ,NSOCLIVR                                           
                            ,NDEPOT                                             
                            ,QVENDUE                                            
                            ,NSEQ                                               
                            ,WEMPORTE                                           
                            ,CTYPENREG                                          
                            ,NCODICGRP                                          
                            ,NSOCLIVR                                           
                            ,NDEPOT                                             
                            ,DTOPE                                              
                    INTO    :GV11-DDELIV                                        
                           ,:GV11-CEQUIPE                                       
                           ,:GV11-LCOMMENT                                      
                           ,:GV11-WCQERESF                                      
                           ,:GV11-NSOCLIVR                                      
                           ,:GV11-NDEPOT                                        
                           ,:GV11-QVENDUE                                       
                           ,:GV11-NSEQ                                          
                           ,:GV11-WEMPORTE                                      
                           ,:GV11-CTYPENREG                                     
                           ,:GV11-NCODICGRP                                     
                           ,:GV11-NSOCLIVR                                      
                           ,:GV11-NDEPOT                                        
                           ,:GV11-DTOPE                                         
                    FROM     RVGV1199 GV11                                      
                    WHERE    NSOCIETE  = :GV11-NSOCIETE                         
                    AND      NLIEU     = :GV11-NLIEU                            
                    AND      NVENTE    = :GV11-NVENTE                           
                    AND      NCODIC    = :GV11-NCODIC                           
                    AND      NSEQNQ    = :GV11-NSEQNQ                           
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-RTGV11-NQ. EXIT.                                              
      ************************                                                  
       SELECT-GV11-GB05   SECTION.                                              
      *************************                                                 
           MOVE COMM-MEC64-NLIEU               TO GV11-NLIEU                    
           MOVE COMM-MEC64-NSOCIETE            TO GV11-NSOCIETE                 
           MOVE COMM-MEC64-NVENTE              TO GV11-NVENTE                   
           MOVE 'RTGV11'              TO TABLE-NAME.                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           EXEC SQL SELECT   DMUTATION                                          
                    INTO    :GB05-DMUTATION                                     
                    FROM     RVGV1199 GV11                                      
                    INNER JOIN RVGB0501 GB05 ON                                 
                    GB05.NMUTATION     = SUBSTR(GV11.LCOMMENT, 6, 7)            
                    WHERE    GV11.NSOCIETE  = :GV11-NSOCIETE                    
                    AND      GV11.NLIEU     = :GV11-NLIEU                       
                    AND      GV11.NVENTE    = :GV11-NVENTE                      
                    AND      GV11.WCQERESF  = 'F'                               
                    ORDER BY DMUTATION DESC                                     
                    FETCH FIRST 1 ROW ONLY                                      
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-GV11-GB05. EXIT.                                              
      ************************                                                  
       SELECT-RTGV22  SECTION.                                                  
      *************************                                                 
           MOVE COMM-MEC64-NLIEU            TO GV22-NLIEU                       
           MOVE COMM-MEC64-NSOCIETE         TO GV22-NSOCIETE                    
           MOVE COMM-MEC64-NVENTE           TO GV22-NVENTE                      
           MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GV22-NCODIC                      
           MOVE COMM-MEC64-TAB-NSEQ  (I-NL) TO GV22-NSEQ                        
           MOVE COMM-MEC64-TAB-NCODICGRP(I-NL) TO GV22-NCODICGRP                
           MOVE 'RTGV22'              TO TABLE-NAME.                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           EXEC SQL SELECT   CSTATUT                                            
                           , CEQUIPE                                            
                           , WCQERESF                                           
                           , DDELIV                                             
                           , NMUTATION                                          
                           , NSOCLIVR                                           
                           , NDEPOT                                             
                           , DDELIV                                             
                    INTO    :GV22-CSTATUT                                       
                           ,:GV22-CEQUIPE                                       
                           ,:GV22-WCQERESF                                      
                           ,:GV22-DDELIV                                        
                           ,:GV22-NMUTATION                                     
                           ,:GV22-NSOCLIVR                                      
                           ,:GV22-NDEPOT                                        
                           ,:GV22-DDELIV                                        
                    FROM     RVGV2204                                           
                    WHERE    NSOCIETE  = :GV22-NSOCIETE                         
                    AND      NLIEU     = :GV22-NLIEU                            
                    AND      NVENTE    = :GV22-NVENTE                           
                    AND      NCODIC    = :GV22-NCODIC                           
                    AND      NCODICGRP = :GV22-NCODICGRP                        
                    AND      NSEQ      = :GV22-NSEQ                             
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-RTGV22. EXIT.                                                 
      ************************                                                  
       SELECT-RTGV22-NQ  SECTION.                                               
      *************************                                                 
           MOVE COMM-MEC64-NLIEU            TO GV22-NLIEU                       
           MOVE COMM-MEC64-NSOCIETE         TO GV22-NSOCIETE                    
           MOVE COMM-MEC64-NVENTE           TO GV22-NVENTE                      
           MOVE COMM-MEC64-TAB-NCODIC(1)    TO GV22-NCODIC                      
           MOVE GV11-NCODICGRP              TO GV22-NCODICGRP                   
           MOVE GV11-NSEQ                   TO GV22-NSEQ                        
           MOVE 'RTGV22'              TO TABLE-NAME.                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           EXEC SQL SELECT   CSTATUT                                            
                           , CEQUIPE                                            
                           , WCQERESF                                           
                           , DDELIV                                             
                           , NMUTATION                                          
                           , NSOCLIVR                                           
                           , NDEPOT                                             
                           , DDELIV                                             
                           , NCODICGRP                                          
                    INTO    :GV22-CSTATUT                                       
                           ,:GV22-CEQUIPE                                       
                           ,:GV22-WCQERESF                                      
                           ,:GV22-DDELIV                                        
                           ,:GV22-NMUTATION                                     
                           ,:GV22-NSOCLIVR                                      
                           ,:GV22-NDEPOT                                        
                           ,:GV22-DDELIV                                        
                           ,:GV22-NCODICGRP                                     
                    FROM     RVGV2204                                           
                    WHERE    NSOCIETE  = :GV22-NSOCIETE                         
                    AND      NLIEU     = :GV22-NLIEU                            
                    AND      NVENTE    = :GV22-NVENTE                           
                    AND      NCODIC    = :GV22-NCODIC                           
                    AND      NCODICGRP = :GV22-NCODICGRP                        
                    AND      NSEQ      = :GV22-NSEQ                             
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-RTGV22-NQ. EXIT.                                              
      *                                                                         
       SELECT-RTGV22-ANO SECTION.                                               
      *************************                                                 
           MOVE COMM-MEC64-NLIEU            TO GV22-NLIEU                       
           MOVE COMM-MEC64-NSOCIETE         TO GV22-NSOCIETE                    
           MOVE COMM-MEC64-NVENTE           TO GV22-NVENTE                      
           MOVE 'RTGV22'              TO TABLE-NAME.                            
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           EXEC SQL SELECT   COUNT(NCODIC)                                      
                    INTO    :W-NBLIGNENS                                        
                    FROM     RVGV2204                                           
                    WHERE    NSOCIETE  = :GV22-NSOCIETE                         
                    AND      NLIEU     = :GV22-NLIEU                            
                    AND      NVENTE    = :GV22-NVENTE                           
                    AND      CSTATUT   <> 'R'                                   
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-RTGV22-ANO. EXIT.                                             
      *                                                                         
       SELECT-RTGV99   SECTION.                                                 
      ***************************                                               
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV99'              TO TABLE-NAME.                            
           EXEC SQL                                                             
                    SELECT WPRECO                                               
                    INTO  :GV99-WPRECO                                          
                    FROM     RVGV9901                                           
                    WHERE        NSOCIETE    = :GV11-NSOCIETE                   
                        AND      NLIEU       = :GV11-NLIEU                      
                        AND      NVENTE      = :GV11-NVENTE                     
                    FETCH FIRST 1 ROWS ONLY                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           IF TROUVE                                                            
              SET GV99-PRECO-OK       TO TRUE                                   
           END-IF                                                               
           .                                                                    
       FIN-SELECT-RTGV99.  EXIT.                                                
      ***************************                                               
       01-COPY SECTION. CONTINUE. COPY SYKCLINK.                                
       02-COPY SECTION. CONTINUE. COPY SYKCERRO.                                
       DECLARE-CSL21 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-DECLARE          TO TRACE-SQL-FUNCTION.                    
           MOVE 'SL2100'              TO TABLE-NAME.                            
           MOVE COMM-MEC64-NSOCIETE    TO SL21-NSOCIETE                         
           MOVE COMM-MEC64-NLIEU       TO SL21-NLIEU                            
           MOVE COMM-MEC64-NVENTE      TO SL21-NVENTE                           
           EXEC SQL DECLARE CSL21   CURSOR FOR                                  
                 SELECT NSOCIETE                                                
                      , NLIEU                                                   
                      , NVENTE                                                  
                      , NCODIC                                                  
                      , NSEQNQ                                                  
                  FROM RVSL2100 del                                             
                  WHERE NSOCIETE = :SL21-NSOCIETE                               
                   AND  NLIEU    = :SL21-NLIEU                                  
                   AND  NVENTE   = :SL21-NVENTE                                 
                   AND NOT EXISTS(                                              
                 SELECT 1 FROM RVGV1106                                         
                    WHERE                                                       
                    NSOCIETE = del.NSOCIETE                                     
              AND   NLIEU    = del.NLIEU                                        
              AND   NVENTE   = del.NVENTE                                       
              AND   NCODIC   = del.NCODIC                                       
              AND   NSEQNQ   = del.NSEQNQ)                                      
CEN012*       WITH UR                                                           
           END-EXEC.                                                            
           MOVE FUNC-OPEN             TO TRACE-SQL-FUNCTION                     
           EXEC SQL OPEN CSL21   END-EXEC                                       
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-DECLARE-CSL21. EXIT.                                                 
      *************************                                                 
       FETCH-CSL21 SECTION.                                                     
      ***********************                                                   
           MOVE FUNC-FETCH               TO TRACE-SQL-FUNCTION.                 
           EXEC SQL FETCH CSL21 INTO                                            
               :SL21-NSOCIETE                                                   
              ,:SL21-NLIEU                                                      
              ,:SL21-NVENTE                                                     
              ,:SL21-NCODIC                                                     
              ,:SL21-NSEQNQ                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
       CLOSE-CSL21  SECTION.                                                    
      ***********************                                                   
           EXEC SQL CLOSE CSL21  END-EXEC                                       
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
      ***********************                                                   
       UPDATE-SL20 SECTION.                                                     
      ****************************                                              
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2000'              TO TABLE-NAME                             
           EXEC SQL UPDATE RVSL2000                                             
                  SET STATUT            = :SL20-STATUT                          
                     ,NBLIGNENS         = :SL20-NBLIGNENS                       
                     ,INCOMPLETE        = :SL20-INCOMPLETE                      
                     ,DATERESERVATION   = :SL20-DATERESERVATION                 
                     ,PGMMAJ            = :SL20-PGMMAJ                          
                     ,DSYST             = :SL20-DSYST                           
                    WHERE                                                       
                          NSOCIETE  = :SL20-NSOCIETE                            
                    AND   NLIEU     = :SL20-NLIEU                               
                    AND   NVENTE    = :SL20-NVENTE                              
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-UPDATE-SL20. EXIT.                                                   
      **************************                                                
       UPDATE-SL20-ANO SECTION.                                                 
      ****************************                                              
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2000'              TO TABLE-NAME                             
           MOVE 'MEC64'               TO SL20-PGMMAJ                            
           MOVE COMM-MEC64-DJOUR      TO SL20-DATERESERVATION                   
           MOVE W-SPDATDB2-DSYST      TO SL20-DSYST                             
           EXEC SQL UPDATE RVSL2000                                             
                  SET STATUT            = 'KO'                                  
                     ,NBLIGNENS         = NBLIGNENS + :W-QTE                    
                     ,PGMMAJ            = :SL20-PGMMAJ                          
                     ,DATERESERVATION   = :SL20-DATERESERVATION                 
                     ,DSYST             = :SL20-DSYST                           
                    WHERE                                                       
                          NSOCIETE  = :GV11-NSOCIETE                            
                    AND   NLIEU     = :GV11-NLIEU                               
                    AND   NVENTE    = :GV11-NVENTE                              
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-UPDATE-SL20-ANO. EXIT.                                               
      **************************                                                
       UPDATE-RTSL20 SECTION.                                                   
      ****************************                                              
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2000'              TO TABLE-NAME                             
           MOVE W-NB-LIGNE-SL21       TO SL20-NBLIGNENS                         
CEN012     IF W-NB-LIGNE-SL21 = 0                                               
CEN012        MOVE 'OK'                  TO SL20-STATUT                         
CEN012     ELSE                                                                 
              MOVE 'KO'                  TO SL20-STATUT                         
CEN012     END-IF                                                               
           MOVE COMM-MEC64-DDELIV-COM TO SL20-DATERESERVATION                   
           MOVE W-SPDATDB2-DSYST      TO SL20-DSYST.                            
           MOVE 'MEC64'               TO SL20-PGMMAJ                            
           EXEC SQL UPDATE RVSL2000                                             
                  SET STATUT            = :SL20-STATUT                          
                     ,NBLIGNENS         = :SL20-NBLIGNENS                       
                     ,DATERESERVATION   = :SL20-DATERESERVATION                 
                     ,DSYST             = :SL20-DSYST                           
                     ,PGMMAJ            = :SL20-PGMMAJ                          
                    WHERE                                                       
                          NSOCIETE  = :GV11-NSOCIETE                            
                    AND   NLIEU     = :GV11-NLIEU                               
                    AND   NVENTE    = :GV11-NVENTE                              
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ SL2000'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-RTSL20. EXIT.                                                 
      **************************                                                
       UPDATE-RTSL20-DRES SECTION.                                              
      ****************************                                              
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2000'              TO TABLE-NAME                             
           IF COMM-MEC64-TYPE-NB-MULTI                                          
              MOVE COMM-MEC64-DATE-MAJ   TO SL20-DATERESERVATION                
           ELSE                                                                 
              MOVE '20491231'            TO SL20-DATERESERVATION                
           END-IF                                                               
           MOVE 'MEC64'               TO SL20-PGMMAJ                            
           MOVE W-SPDATDB2-DSYST      TO SL20-DSYST                             
           EXEC SQL UPDATE RVSL2000                                             
                  SET DATERESERVATION   = :SL20-DATERESERVATION                 
                     ,DSYST             = :SL20-DSYST                           
                     ,PGMMAJ            = :SL20-PGMMAJ                          
                    WHERE                                                       
                          NSOCIETE  = :GV11-NSOCIETE                            
                    AND   NLIEU     = :GV11-NLIEU                               
                    AND   NVENTE    = :GV11-NVENTE                              
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ SL200B'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                 STRING                                                         
             'CODE RETOUR ' DIS-SQLCODE                                         
              ' SUR MAJ SL200B'                                                 
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           ELSE                                                                 
              IF SQLCODE NOT = 0                                                
                 MOVE SQLCODE TO DIS-SQLCODE                                    
                 STRING                                                         
                 'CODE RETOUR ' DIS-SQLCODE                                     
                  ' SUR MAJ SL200B'                                             
                  DELIMITED BY SIZE                                             
                  INTO COMM-MEC64-MESSAGE                                       
                 MOVE '9999' TO COMM-MEC64-CODE-RETOUR                          
                 IF AUTO-DISPLAY                                                
                    STRING                                                      
                'CODE RETOUR ' DIS-SQLCODE                                      
                 ' SUR MAJ SL200B'                                              
                     DELIMITED BY SIZE INTO W-MESSAGE                           
                     PERFORM DISPLAY-LOG                                        
                 END-IF                                                         
                 SET TRAITEMENT-KO TO TRUE                                      
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-RTSL20-DRES. EXIT.                                            
      **************************                                                
       UPDATE-RTSL21 SECTION.                                                   
      ****************************                                              
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2100'              TO TABLE-NAME                             
           MOVE COMM-MEC64-NSOCIETE   TO SL21-NSOCIETE                          
           MOVE COMM-MEC64-NLIEU      TO SL21-NLIEU                             
           MOVE COMM-MEC64-NVENTE     TO SL21-NVENTE                            
           MOVE COMM-MEC64-TAB-NCODIC(1)   TO SL21-NCODIC                       
           MOVE COMM-MEC64-TAB-NSEQNQ(1)   TO SL21-NSEQNQ                       
           MOVE 'MEC64'               TO SL21-PGMMAJ                            
           MOVE W-SPDATDB2-DSYST      TO SL21-DSYST                             
           EXEC SQL UPDATE RVSL2100                                             
                  SET STATUT            = 'F'                                   
                     ,DSYST             = :SL21-DSYST                           
                     ,PGMMAJ            = :SL21-PGMMAJ                          
                    WHERE                                                       
                          NSOCIETE  = :SL21-NSOCIETE                            
                    AND   NLIEU     = :SL21-NLIEU                               
                    AND   NVENTE    = :SL21-NVENTE                              
                    AND   NCODIC    = :SL21-NCODIC                              
                    AND   NSEQNQ    = :SL21-NSEQNQ                              
                    WITH CS                                                     
           END-EXEC                                                             
           IF SQLCODE NOT = ZERO                                                
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ SL2100'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-RTSL21. EXIT.                                                 
      **************************                                                
       UPDATE-RTSL20-UN SECTION.                                                
      ****************************                                              
           PERFORM SELECT-SL20                                                  
           IF SQLCODE NOT = ZERO                                                
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING 'CODE RETOUR ' DIS-SQLCODE ' SUR SEL SL2000'               
                       DELIMITED BY SIZE INTO COMM-MEC64-MESSAGE                
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                STRING 'CODE RETOUR ' DIS-SQLCODE ' SUR SEL SL2000'             
                        DELIMITED BY SIZE INTO W-MESSAGE                        
                PERFORM DISPLAY-LOG                                             
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           ELSE                                                                 
             COMPUTE SL20-NBLIGNENS = SL20-NBLIGNENS + W-QTE                    
             IF SL20-NBLIGNENS > 0                                              
                IF SL20-STATUT = 'EX'                                           
                 STRING 'STATUT EX PAS TOUCHE ' GV11-NVENTE                     
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-LOG                                         
                ELSE                                                            
                  MOVE 'KO' TO SL20-STATUT                                      
                END-IF                                                          
             ELSE                                                               
                MOVE 'OK' TO SL20-STATUT                                        
                MOVE 0     TO      SL20-NBLIGNENS                               
             END-IF                                                             
LA1807*      IF MEC64-PAS-ERREUR                                                
LA1807*      AND COMM-MEC64-CDEDACEM(I-NL)                                      
LA1807*         MOVE COMM-EC68-DATE-DISPO  TO SL20-DATERESERVATION              
LA1807*      END-IF                                                             
             MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                   
             MOVE 'SL20'                TO TABLE-NAME                           
             MOVE 'MEC64'               TO SL20-PGMMAJ                          
             MOVE W-SPDATDB2-DSYST      TO SL20-DSYST                           
             IF SL20-INCOMPLETE = 'O'                                           
                EXEC SQL UPDATE RVSL2000                                        
                   SET NBLIGNENS = :SL20-NBLIGNENS                              
                      ,STATUT    = :SL20-STATUT                                 
                      ,DSYST     = :SL20-DSYST                                  
                      ,PGMMAJ    = :SL20-PGMMAJ                                 
                      ,DATERESERVATION = :SL20-DATERESERVATION                  
                     WHERE                                                      
                           NSOCIETE  = :GV11-NSOCIETE                           
                     AND   NLIEU     = :GV11-NLIEU                              
                     AND   NVENTE    = :GV11-NVENTE                             
                     WITH CS                                                    
                END-EXEC                                                        
                IF SQLCODE NOT = ZERO                                           
                  MOVE SQLCODE TO DIS-SQLCODE                                   
                  STRING 'CODE RETOUR ' DIS-SQLCODE ' SUR MAJ SL2000'           
                      DELIMITED BY SIZE INTO COMM-MEC64-MESSAGE                 
                  MOVE '9999' TO COMM-MEC64-CODE-RETOUR                         
                  IF AUTO-DISPLAY                                               
                    STRING 'CODE RETOUR ' DIS-SQLCODE ' SUR MAJ SL2000'         
                       DELIMITED BY SIZE INTO W-MESSAGE                         
                       PERFORM DISPLAY-LOG                                      
                  END-IF                                                        
                  SET TRAITEMENT-KO TO TRUE                                     
                END-IF                                                          
             ELSE                                                               
                  EXEC SQL UPDATE RVSL2000                                      
                     SET NBLIGNENS = :SL20-NBLIGNENS                            
                        ,STATUT    = :SL20-STATUT                               
                        ,DSYST     = :SL20-DSYST                                
                        ,PGMMAJ    = :SL20-PGMMAJ                               
                       WHERE                                                    
                             NSOCIETE  = :GV11-NSOCIETE                         
                       AND   NLIEU     = :GV11-NLIEU                            
                       AND   NVENTE    = :GV11-NVENTE                           
                       WITH CS                                                  
                  END-EXEC                                                      
                  IF SQLCODE NOT = ZERO                                         
                    MOVE SQLCODE TO DIS-SQLCODE                                 
                    STRING                                                      
                    'CODE RETOUR ' DIS-SQLCODE                                  
                     ' SUR MAJ SL2000'                                          
                     DELIMITED BY SIZE                                          
                     INTO COMM-MEC64-MESSAGE                                    
                    MOVE '9999' TO COMM-MEC64-CODE-RETOUR                       
                    IF AUTO-DISPLAY                                             
                       STRING                                                   
                   'CODE RETOUR ' DIS-SQLCODE                                   
                    ' SUR MAJ SL2000'                                           
                        DELIMITED BY SIZE INTO W-MESSAGE                        
                        PERFORM DISPLAY-LOG                                     
                    END-IF                                                      
                     SET TRAITEMENT-KO TO TRUE                                  
                  END-IF                                                        
             END-IF                                                             
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-RTSL20-UN. EXIT.                                              
      **************************                                                
       UPDATE-SL20-ANNULE SECTION.                                              
      ****************************                                              
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL20'                TO TABLE-NAME                             
           EXEC SQL SELECT   NBLIGNENS                                          
                INTO    :SL20-NBLIGNENS                                         
                FROM     RVSL2000                                               
                WHERE    NSOCIETE = :GV11-NSOCIETE                              
                AND      NLIEU    = :GV11-NLIEU                                 
                AND      NVENTE   = :GV11-NVENTE                                
CEN012*         WITH UR                                                         
           END-EXEC                                                             
           IF SQLCODE NOT = ZERO                                                
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING 'CODE RETOUR ' DIS-SQLCODE ' SUR SEL SL2000'               
                       DELIMITED BY SIZE INTO COMM-MEC64-MESSAGE                
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                STRING 'CODE RETOUR ' DIS-SQLCODE ' SUR SEL SL2000'             
                        DELIMITED BY SIZE INTO W-MESSAGE                        
                PERFORM DISPLAY-LOG                                             
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           ELSE                                                                 
             IF SL20-NBLIGNENS > 1                                              
                MOVE 'MEC64'               TO SL20-PGMMAJ                       
                MOVE W-SPDATDB2-DSYST      TO SL20-DSYST                        
                EXEC SQL UPDATE RVSL2000                                        
                   SET NBLIGNENS = NBLIGNENS - 1                                
                      ,DSYST     = :SL20-DSYST                                  
                      ,PGMMAJ    = :SL20-PGMMAJ                                 
                     WHERE                                                      
                           NSOCIETE  = :GV11-NSOCIETE                           
                     AND   NLIEU     = :GV11-NLIEU                              
                     AND   NVENTE    = :GV11-NVENTE                             
                     WITH CS                                                    
                END-EXEC                                                        
                PERFORM TEST-CODE-RETOUR-SQL                                    
             ELSE                                                               
                IF SL20-NBLIGNENS = 1                                           
                   EXEC SQL DELETE FROM RVSL2000                                
                          WHERE                                                 
                            NSOCIETE = :GV11-NSOCIETE                           
                      AND   NLIEU    = :GV11-NLIEU                              
                      AND   NVENTE   = :GV11-NVENTE                             
CEN012*               WITH CS                                                   
                   END-EXEC                                                     
                   PERFORM TEST-CODE-RETOUR-SQL                                 
                END-IF                                                          
             END-IF                                                             
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-RTSL20.    EXIT.                                              
      **************************                                                
       UPDATE-GV1100 SECTION.                                                   
      ***************************                                               
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'GV1106'              TO TABLE-NAME                             
           EXEC SQL UPDATE RVGV1106                                             
                  SET CEQUIPE   = :GV11-CEQUIPE                                 
                     ,LCOMMENT  = :GV11-LCOMMENT                                
                     ,DSYST     = :GV11-DSYST                                   
                     ,NSOCLIVR  = :GV11-NSOCLIVR                                
                     ,NDEPOT    = :GV11-NDEPOT                                  
                     ,WCQERESF  = :GV11-WCQERESF                                
                    WHERE                                                       
                          NSOCIETE  = :GV11-NSOCIETE                            
                    AND   NLIEU     = :GV11-NLIEU                               
                    AND   NVENTE    = :GV11-NVENTE                              
                    AND   NCODIC    = :GV11-NCODIC                              
                    AND   NCODICGRP = :GV11-NCODICGRP                           
                    AND   CENREG    = '     '                                   
                    AND   CTYPENREG = :GV11-CTYPENREG                           
                    AND   NSEQ      = :GV11-NSEQ                                
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ GV1100'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                 STRING                                                         
             'CODE RETOUR ' DIS-SQLCODE                                         
              ' SUR MAJ GV1100'                                                 
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-GV1100. EXIT.                                                 
      ****************************                                              
       UPDATE-GV1100-ENC SECTION.                                               
      ***************************                                               
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'GV1106'              TO TABLE-NAME                             
           EXEC SQL UPDATE RVGV1106                                             
                  SET DDELIV    = :GV11-DDELIV                                  
                     ,CEQUIPE   = :GV11-CEQUIPE                                 
                     ,LCOMMENT  = :GV11-LCOMMENT                                
                     ,DSYST     = :GV11-DSYST                                   
                     ,NSOCLIVR  = :GV11-NSOCLIVR                                
                     ,NDEPOT    = :GV11-NDEPOT                                  
                     ,WCQERESF  = :GV11-WCQERESF                                
                    WHERE                                                       
                          NSOCIETE  = :GV11-NSOCIETE                            
                    AND   NLIEU     = :GV11-NLIEU                               
                    AND   NVENTE    = :GV11-NVENTE                              
                    AND   NCODIC    = :GV11-NCODIC                              
                    AND   NCODICGRP = :GV11-NCODICGRP                           
                    AND   CENREG    = '     '                                   
                    AND   CTYPENREG = :GV11-CTYPENREG                           
                    AND   NSEQ      = :GV11-NSEQ                                
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ GV1100'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                 STRING                                                         
             'CODE RETOUR ' DIS-SQLCODE                                         
              ' SUR MAJ GV1100'                                                 
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-GV1100-ENC. EXIT.                                             
      ****************************                                              
       UPDATE-GV1100-RESA SECTION.                                              
      ***************************                                               
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'GV1106'              TO TABLE-NAME                             
           EXEC SQL UPDATE RVGV1106                                             
                  SET LCOMMENT  = :GV11-LCOMMENT                                
                     ,DSYST     = :GV11-DSYST                                   
                     ,NSOCLIVR  = :GV11-NSOCLIVR                                
                     ,NDEPOT    = :GV11-NDEPOT                                  
                     ,WCQERESF  = :GV11-WCQERESF                                
                     ,NSOCGEST     = :GV11-NSOCGEST                             
                     ,NLIEUGEST    = :GV11-NLIEUGEST                            
                     ,NSOCDEPLIV   = :GV11-NSOCDEPLIV                           
                     ,NLIEUDEPLIV  = :GV11-NLIEUDEPLIV                          
                    WHERE                                                       
                          NSOCIETE  = :GV11-NSOCIETE                            
                    AND   NLIEU     = :GV11-NLIEU                               
                    AND   NVENTE    = :GV11-NVENTE                              
                    AND   NCODIC    = :GV11-NCODIC                              
                    AND   NCODICGRP = :GV11-NCODICGRP                           
                    AND   CENREG    = '     '                                   
                    AND   CTYPENREG = :GV11-CTYPENREG                           
                    AND   NSEQ      = :GV11-NSEQ                                
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ GV1100'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                 STRING                                                         
             'CODE RETOUR ' DIS-SQLCODE                                         
              ' SUR MAJ GV1100'                                                 
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-GV1100-RESA. EXIT.                                            
      ****************************                                              
       UPDATE-GV11-ANO  SECTION.                                                
      ***************************                                               
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'GV1106'              TO TABLE-NAME                             
           MOVE W-SPDATDB2-DSYST      TO GV11-DSYST                             
           EXEC SQL UPDATE RVGV1106                                             
                  SET DSYST     = :GV11-DSYST                                   
                     ,CEQUIPE   = :GV11-CEQUIPE                                 
                     ,LCOMMENT  = :GV11-LCOMMENT                                
                    WHERE                                                       
                          NSOCIETE  = :GV11-NSOCIETE                            
                    AND   NLIEU     = :GV11-NLIEU                               
                    AND   NVENTE    = :GV11-NVENTE                              
                    AND   NCODIC    = :GV11-NCODIC                              
                    AND   NCODICGRP = :GV11-NCODICGRP                           
                    AND   CENREG    = '     '                                   
                    AND   CTYPENREG = :GV11-CTYPENREG                           
                    AND   NSEQ      = :GV11-NSEQ                                
                    WITH CS                                                     
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ GV1100'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                 STRING                                                         
             'CODE RETOUR ' DIS-SQLCODE                                         
              ' SUR MAJ GV1100'                                                 
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-GV11-ANO.  EXIT.                                              
      ****************************                                              
       UPDATE-RTGV22  SECTION.                                                  
      *************************                                                 
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION.                    
           MOVE 'RTGV22'              TO TABLE-NAME.                            
           MOVE W-SPDATDB2-DSYST               TO GV22-DSYST.                   
           EXEC SQL UPDATE RVGV2204                                             
                    SET CSTATUT   = :GV22-CSTATUT                               
                       ,DSYST     = :GV22-DSYST                                 
                       ,CEQUIPE   = :GV22-CEQUIPE                               
                       ,WCQERESF  = :GV22-WCQERESF                              
                       ,NMUTATION = :GV22-NMUTATION                             
                       ,NSOCLIVR  = :GV22-NSOCLIVR                              
                       ,NDEPOT    = :GV22-NDEPOT                                
                       ,DDELIV    = :GV22-DDELIV                                
                    WHERE                                                       
                          NSOCIETE  = :GV22-NSOCIETE                            
                    AND   NLIEU     = :GV22-NLIEU                               
                    AND   NVENTE    = :GV22-NVENTE                              
                    AND   NCODIC    = :GV22-NCODIC                              
                    AND   NCODICGRP = :GV22-NCODICGRP                           
                    AND   NSEQ      = :GV22-NSEQ                                
                    WITH CS                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ GV2204'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                 STRING ' sur '                                                 
                 GV22-NSOCIETE ' '                                              
                 GV22-NLIEU ' '                                                 
                 GV22-NVENTE ' '                                                
                 GV22-NCODIC ' '                                                
                 GV22-NCODICGRP ' '                                             
                 GV22-NSEQ                                                      
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-RTGV22. EXIT.                                                 
      **************************                                                
       UPDATE-GV22-ANO SECTION.                                                 
      *************************                                                 
           MOVE FUNC-UPDATE           TO TRACE-SQL-FUNCTION                     
           MOVE 'RTGV22'              TO TABLE-NAME                             
           MOVE W-SPDATDB2-DSYST      TO GV22-DSYST                             
           EXEC SQL UPDATE RVGV2204                                             
                    SET CSTATUT   = :GV22-CSTATUT                               
                       ,CEQUIPE   = :GV22-CEQUIPE                               
                       ,NMUTATION = :GV22-NMUTATION                             
                       ,DSYST     = :GV22-DSYST                                 
                    WHERE                                                       
                          NSOCIETE  = :GV22-NSOCIETE                            
                    AND   NLIEU     = :GV22-NLIEU                               
                    AND   NVENTE    = :GV22-NVENTE                              
                    AND   NCODIC    = :GV22-NCODIC                              
                    AND   NCODICGRP = :GV22-NCODICGRP                           
                    AND   NSEQ      = :GV22-NSEQ                                
                    WITH CS                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
              MOVE SQLCODE TO DIS-SQLCODE                                       
              STRING                                                            
              'CODE RETOUR ' DIS-SQLCODE                                        
               ' SUR MAJ GV2204'                                                
               DELIMITED BY SIZE                                                
               INTO COMM-MEC64-MESSAGE                                          
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
              IF AUTO-DISPLAY                                                   
                 STRING ' sur '                                                 
                 GV22-NSOCIETE ' '                                              
                 GV22-NLIEU ' '                                                 
                 GV22-NVENTE ' '                                                
                 GV22-NCODIC ' '                                                
                 GV22-NCODICGRP ' '                                             
                 GV22-NSEQ                                                      
                  DELIMITED BY SIZE INTO W-MESSAGE                              
                  PERFORM DISPLAY-LOG                                           
              END-IF                                                            
              SET TRAITEMENT-KO TO TRUE                                         
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-GV22-ANO. EXIT.                                               
      **************************                                                
       INSERT-RTGV22  SECTION.                                                  
      *************************                                                 
           MOVE W-SPDATDB2-DSYST      TO GV22-DSYST                             
           MOVE COMM-MEC64-NSOCIETE   TO GV11-NSOCIETE                          
           MOVE COMM-MEC64-NLIEU      TO GV11-NLIEU                             
           MOVE COMM-MEC64-NVENTE     TO GV11-NVENTE                            
           MOVE COMM-MEC64-TAB-NSEQNQ(I-NL) TO GV11-NSEQNQ                      
           MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GV11-NCODIC                      
           MOVE COMM-MEC64-TAB-WEMPORTE(I-NL) TO GV11-WEMPORTE                  
           MOVE SPACE                 TO GV11-WCQERESF                          
           MOVE COMM-MEC64-TAB-DDELIVO (I-NL)  TO GV11-DDELIV                   
           IF EC64-ARTICLE-EN-CF (I-NL)                                         
               MOVE 'F' TO GV11-WCQERESF                                        
               MOVE COMM-EC67-NSOCDEPOT     TO GV11-NSOCLIVR                    
               MOVE COMM-EC67-NDEPOT        TO GV11-NDEPOT                      
           END-IF                                                               
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           MOVE 'RVGV2204'            TO TABLE-NAME                             
           MOVE  GV11-NSOCLIVR        TO GV22-NSOCLIVR                          
           MOVE  GV11-NDEPOT          TO GV22-NDEPOT                            
           EXEC SQL INSERT                                                      
                    INTO     RVGV2204                                           
                            (NSOCIETE     , NLIEU       , NORDRE      ,         
                             NVENTE       , NCODICGRP   , NCODIC      ,         
                             NSEQ         , NMUTATION   , QVENDUE     ,         
                             QRESERV      , NAUTORM     , WEMPORTE    ,         
                             DSYST        , NSOCLIVR    , NDEPOT      ,         
                             DDELIV       , CMODDEL     , CSTATUT ,             
                             CEQUIPE      , WCQERESF    , DCREATION)            
                    SELECT   NSOCIETE     , NLIEU       , NORDRE      ,         
                             NVENTE       , NCODICGRP   , NCODIC      ,         
                             NSEQ         , NMUTATION   , QVENDUE     ,         
                             QVENDUE      , ' '         , WEMPORTE    ,         
                       :GV11-DSYST    , :GV22-NSOCLIVR , :GV22-NDEPOT ,         
                       :GV11-DDELIV       , CMODDEL ,    :GV22-CSTATUT,         
                        CEQUIPE, :GV11-WCQERESF, DCREATION                      
                    FROM RVGV1106 WHERE                                         
                    NSOCIETE = :GV11-NSOCIETE                                   
                    AND NLIEU = :GV11-NLIEU AND NVENTE = :GV11-NVENTE           
                    AND NSEQNQ = :GV11-NSEQNQ                                  
                    WITH CS                                                    
           END-EXEC                                                             
           IF SQLCODE NOT = -803                                                
              PERFORM TEST-CODE-RETOUR-SQL                                      
           END-IF                                                               
           .                                                                    
       FIN-UPDATE-RTGV22. EXIT.                                                 
      **************************                                                
       INSERT-RTSL20 SECTION.                                                   
           IF W-NB-LIGNE-SL21 = 0                                               
           AND (NOT COMM-MEC64-ACTION-ENCAISSEMENT)                             
              GO TO FIN-INSERT-RTSL20                                           
           END-IF                                                               
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           MOVE 'SL2000'              TO TABLE-NAME                             
           INITIALIZE RVSL2000                                                  
           MOVE GV11-NSOCIETE    TO SL20-NSOCIETE                               
           MOVE GV11-NLIEU       TO SL20-NLIEU                                  
           MOVE GV11-NVENTE      TO SL20-NVENTE                                 
           IF COMM-MEC64-TYPE-NB-MULTI                                          
              MOVE 'O'                   TO SL20-INCOMPLETE                     
              MOVE COMM-MEC64-DDELIV-COM TO SL20-DATERESERVATION                
           ELSE                                                                 
              MOVE 'N'              TO SL20-INCOMPLETE                          
              MOVE '20491231'       TO SL20-DATERESERVATION                     
           END-IF                                                               
           MOVE W-SL20-STATUT    TO SL20-STATUT                                 
           MOVE 'N'              TO SL20-DUPLIQUE                               
           MOVE W-NB-LIGNE-SL21       TO SL20-NBLIGNENS                         
           MOVE 'MEC64'               TO SL20-PGMMAJ                            
           MOVE W-SPDATDB2-DSYST      TO SL20-DSYST                             
           EXEC SQL INSERT INTO RVSL2000                                        
                            (NSOCIETE, NLIEU, NVENTE, STATUT,                   
                             INCOMPLETE, NBLIGNENS, DUPLIQUE,                   
                             DATERESERVATION, PGMMAJ, DSYST)                    
                    VALUES  (:SL20-NSOCIETE, :SL20-NLIEU,                       
                             :SL20-NVENTE,   :SL20-STATUT,                      
                             :SL20-INCOMPLETE, :SL20-NBLIGNENS,                 
                             :SL20-DUPLIQUE, :SL20-DATERESERVATION,             
                             :SL20-PGMMAJ,   :SL20-DSYST)                       
           END-EXEC.                                                            
           IF SQLCODE NOT = -803                                                
              PERFORM TEST-CODE-RETOUR-SQL                                      
           ELSE                                                                 
              PERFORM UPDATE-SL20                                               
           END-IF                                                               
           .                                                                    
       FIN-INSERT-RTSL20. EXIT.                                                 
      *                                                                         
      ******************************************************************        
      * SUPPRESSION RTSL20 SI COMMANDE COMPLETE                                 
      ******************************************************************        
       DELETE-RTSL20 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-DELETE           TO TRACE-SQL-FUNCTION.                    
           MOVE 'SL2000'              TO TABLE-NAME.                            
           MOVE COMM-MEC64-NSOCIETE   TO SL20-NSOCIETE                          
           MOVE COMM-MEC64-NLIEU      TO SL20-NLIEU                             
           MOVE COMM-MEC64-NVENTE     TO SL20-NVENTE                            
           EXEC SQL DELETE FROM RVSL2000                                        
                  WHERE                                                         
                    NSOCIETE = :SL20-NSOCIETE                                   
              AND   NLIEU    = :SL20-NLIEU                                      
              AND   NVENTE   = :SL20-NVENTE                                     
CEN012*       WITH CS                                                           
              END-EXEC.                                                         
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-DELETE-RTSL20. EXIT.                                                 
      *************************                                                 
      ******************************************************************        
      * SUPPRESSION TOTALE DE RTSL21                                            
      ******************************************************************        
       DELETE-RTSL21-TOT SECTION.                                               
      ************************                                                  
           MOVE FUNC-DELETE           TO TRACE-SQL-FUNCTION.                    
           MOVE 'SL2100'              TO TABLE-NAME.                            
           MOVE COMM-MEC64-NSOCIETE   TO SL21-NSOCIETE                          
           MOVE COMM-MEC64-NLIEU      TO SL21-NLIEU                             
           MOVE COMM-MEC64-NVENTE     TO SL21-NVENTE                            
           EXEC SQL DELETE FROM RVSL2100                                        
                  WHERE                                                         
                    NSOCIETE = :SL21-NSOCIETE                                   
              AND   NLIEU    = :SL21-NLIEU                                      
              AND   NVENTE   = :SL21-NVENTE                                     
CEN012*       WITH CS                                                           
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-DELETE-RTSL21-TOT. EXIT.                                             
      *************************                                                 
      ******************************************************************        
      * SUPPRESSION D'UNE LIGNE DE SL21                                         
      ******************************************************************        
       DELETE-RTSL21-UN SECTION.                                                
      ************************                                                  
           MOVE FUNC-DELETE           TO TRACE-SQL-FUNCTION.                    
           MOVE 'SL2100'              TO TABLE-NAME.                            
           EXEC SQL DELETE FROM RVSL2100                                        
                  WHERE                                                         
                    NSOCIETE = :SL21-NSOCIETE                                   
              AND   NLIEU    = :SL21-NLIEU                                      
              AND   NVENTE   = :SL21-NVENTE                                     
              AND   NCODIC   = :SL21-NCODIC                                     
              AND   NSEQNQ   = :SL21-NSEQNQ                                     
CEN012*       WITH CS                                                           
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-DELETE-RTSL21-UN. EXIT.                                              
      *************************                                                 
       SELECT-MUTATION SECTION.                                                 
      ***************************                                               
           MOVE FUNC-SELECT        TO TRACE-SQL-FUNCTION                        
           MOVE 'RTGB05'              TO TABLE-NAME                             
           SET MUTATION-FINAL TO TRUE.                                          
           EXEC SQL SELECT     WVAL                                             
                             , QNBLLANCE                                        
                             , LHEURLIMIT                                       
                    INTO       :GB05-WVAL                                       
                             , :GB05-QNBLLANCE                                  
                             , :GB05-LHEURLIMIT                                 
                    FROM     RVGB0501                                           
                    WHERE    NMUTATION  = :GB05-NMUTATION                       
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF TROUVE                                                            
              MOVE  '0' TO WF-TSTENVOI                                          
              PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                     
              UNTIL  WC-TSTENVOI-FIN                                            
                 PERFORM LECTURE-TSTENVOI                                       
                 IF  WC-TSTENVOI-SUITE                                          
                    AND TSTENVOI-NSOCZP    = '00000'                            
                    AND TSTENVOI-CTRL(1:1) = 'C'                                
                       IF TSTENVOI-LIBELLE(1:7) = GB05-NMUTATION                
                          SET MUTATION-TEMPO TO TRUE                            
                          SET WC-TSTENVOI-FIN TO TRUE                           
                       END-IF                                                   
                 END-IF                                                         
              END-PERFORM                                                       
              IF NOT MUTATION-TEMPO                                             
                 IF GB05-WVAL = ' ' AND                                         
                    GB05-QNBLLANCE = 0                                          
                    SET MUT-NON-VALIDE TO TRUE                                  
                 ELSE                                                           
                    SET MUT-VALIDE TO TRUE                                      
                 END-IF                                                         
              ELSE                                                              
                 SET MUT-NON-VALIDE TO TRUE                                     
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       FIN-SELECT-MUTATION. EXIT.                                               
      ******************************                                            
       MUT-SL300-O-N   SECTION.                                                 
      ***************************                                               
      * CONTROLE LA COMPATIBILITE DES MUTATIONS SL300 EN FONCTION               
      * DE LA SELECTION ARTICLE                                                 
           MOVE  '0' TO WF-TSTENVOI                                             
           MOVE  'O' TO WS-MUT-SL300                                            
           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1                        
           UNTIL  WC-TSTENVOI-FIN                                               
              PERFORM LECTURE-TSTENVOI                                          
              IF  WC-TSTENVOI-SUITE                                             
              AND TSTENVOI-CTRL(1:1)    = 'G'                                   
              AND TSTENVOI-WPARAM(4:1)  = COMM-MEC64-TAB-WEMPORTE(I-NL)         
              AND (                                                             
               (TSTENVOI-WPARAM(1:3) = COMM-MEC64-FL05-NDEPOT (I-NL, 1))        
LA1807*            OR ( COMM-MEC64-CDEDACEM (I-NL)                              
LA1807*               AND TSTENVOI-WPARAM(1:3) = '996' )                        
                   )                                                            
                  MOVE TSTENVOI-LIBELLE(6:1) TO WS-MUT-SL300                    
RESAJ             MOVE TSTENVOI-LIBELLE(8:1) TO WS-MUT-A-J                      
RESAJ             display 'mut JR ' WS-MUT-A-J '/' COMM-MEC64-NVENTE            
      *           EN CAS DE PARAMETRAGE OUBLIE                                  
                  IF WS-MUT-A-J NOT = 'N'                                       
                     MOVE 'J' TO WS-MUT-A-J                                     
                  END-IF                                                        
                  SET WC-TSTENVOI-FIN   TO TRUE                                 
              END-IF                                                            
           END-PERFORM                                                          
           .                                                                    
       FIN-MUT-SL300-O-N.   EXIT.                                               
      ******************************                                            
       SELECT-RTGB05    SECTION.                                                
      ***************************                                               
           MOVE FUNC-SELECT        TO TRACE-SQL-FUNCTION                        
           MOVE 'RTGB05'              TO TABLE-NAME                             
           MOVE COMM-MEC64-TAB-LCOMMENT(I-NL)(6:7) TO GB05-NMUTATION            
           EXEC SQL SELECT     DMUTATION                                        
                    INTO       :GB05-DMUTATION                                  
                    FROM     RVGB0501                                           
                    WHERE    NMUTATION  = :GB05-NMUTATION                       
CEN012*             WITH UR                                                     
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-RTGB05.   EXIT.                                               
      ******************************                                            
       SELECT-RTGA00 SECTION.                                                   
      ************************                                                  
           MOVE FUNC-SELECT TO TRACE-SQL-FUNCTION.                              
           EXEC SQL                                                             
                SELECT   WDACEM                                                 
                       , CAPPRO                                                 
                       , CFAM                                                   
                       , CMARQ                                                  
                       , CAPPRO                                                 
                       , QHAUTEUR                                               
                       , QPROFONDEUR                                            
                       , QLARGEUR                                               
                       , QPOIDS                                                 
                INTO    :GA00-WDACEM                                            
                      , :GA00-CAPPRO                                            
                      , :GA00-CFAM                                              
                      , :GA00-CMARQ                                             
                      , :GA00-CAPPRO                                            
                      , :GA00-QHAUTEUR                                          
                      , :GA00-QPROFONDEUR                                       
                      , :GA00-QLARGEUR                                          
                      , :GA00-QPOIDS                                            
                  FROM RVGA0000                                                 
                 WHERE NCODIC = :GA00-NCODIC                                    
CEN012*          WITH UR                                                        
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           IF GA00-CAPPRO = 'C'                                                 
              MOVE 'O' TO EC64-VENTE-CQE                                        
           END-IF                                                               
           .                                                                    
       FIN-SELECT-RTGA00. EXIT.                                                 
      **************************                                                
       SELECT-NB-RTGV11 SECTION.                                                
      *-------------------------                                                
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           INITIALIZE                     WS-NB-PRODUIT                         
           EXEC SQL                                                             
                SELECT SUM(QVENDUE)                                             
                INTO    :WS-NB-PRODUIT :WS-NB-PRODUIT-F                         
                  FROM RVGV1106                                                 
                 WHERE NSOCIETE   = :GV11-NSOCIETE                              
                   AND NLIEU      = :GV11-NLIEU                                 
                   AND NVENTE     = :GV11-NVENTE                                
                   AND CTYPENREG  = '1'                                         
CEN012*          WITH UR                                                        
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           .                                                                    
       FIN-SELECT-NB. EXIT.                                                     
       LOG-GV23 SECTION.                                                        
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           MOVE GV11-NSOCIETE     TO       GV23-NSOCIETE                        
           move gv11-NLIEU        TO       GV23-NLIEU                           
           move gv11-NVENTE       TO       GV23-NVENTE                          
           move gv11-CTYPENREG    TO       GV23-CTYPENREG                       
           move gv11-NCODICGRP    TO       GV23-NCODICGRP                       
           move gv11-NCODIC       TO       GV23-NCODIC                          
           move '00'              TO       GV23-NSEQ                            
           move gv11-CMODDEL      TO       GV23-CMODDEL                         
           move gv11-DDELIV       TO       GV23-DDELIV                          
           move gv11-NORDRE       TO       GV23-NORDRE                          
           move spaces            TO       GV23-CENREG                          
           move 0                 TO       GV23-QVENDUE                         
           move 0                 TO       GV23-PVUNIT                          
           move 0                 TO       GV23-PVUNITF                         
           move 0                 TO       GV23-PVTOTAL                         
           move gv11-CEQUIPE      TO       GV23-CEQUIPE                         
           move '00'              TO       GV23-NLIGNE                          
           move 0                 TO       GV23-TAUXTVA                         
           move 0                 TO       GV23-QCONDT                          
           move 0                 TO       GV23-PRMP                            
           move gv11-WEMPORTE     TO       GV23-WEMPORTE                        
           move SPACES            TO       GV23-CPLAGE                          
           move SPACES            TO       GV23-CPROTOUR                        
           move SPACES            TO       GV23-CADRTOUR                        
           move SPACES            TO       GV23-CPOSTAL                         
           move gv11-LCOMMENT     TO       GV23-LCOMMENT                        
           move SPACES            TO       GV23-CVENDEUR                        
           move SPACES            TO       GV23-NSOCLIVR                        
           move SPACES            TO       GV23-NDEPOT                          
           move SPACES            TO       GV23-NAUTORM                         
           move SPACES            TO       GV23-WARTINEX                        
           move SPACES            TO       GV23-WEDITBL                         
           move SPACES            TO       GV23-WACOMMUTER                      
           move gv11-WCQERESF     TO       GV23-WCQERESF                        
           move SPACES            TO       GV23-NMUTATION                       
           move SPACES            TO       GV23-CTOURNEE                        
           move SPACES            TO       GV23-WTOPELIVRE                      
           move spaces            TO       GV23-DCOMPTA                         
           move COMM-MEC64-DJOUR  TO       GV23-DCREATION                       
           move spaces            TO       GV23-HCREATION                       
           move spaces            TO       GV23-DANNULATION                     
           move spaces            TO       GV23-NLIEUORIG                       
           move space             TO       GV23-WINTMAJ                         
           move gv11-DSYST        TO       GV23-DSYST                           
           move spaces            TO       GV23-DSTAT                           
           move 0                 TO       GV23-QVENDUE1                        
           move 0                 TO       GV23-PVUNIT1                         
           move 0                 TO       GV23-PVTOTAL1                        
           move COMM-MEC64-DJOUR  TO       GV23-DCREATION1                      
           move COMM-MEC64-DJOUR  TO       GV23-DDELIV1                         
           move 'MEC64'           TO       GV23-NETNAME                         
           move 'MEC64'           TO       GV23-CACID                           
           move gv11-CMODDEL      TO       GV23-CMODDEL1                        
           move spaces            TO       GV23-NSOCMODIF                       
           move spaces            TO       GV23-NLIEUMODIF                      
           move spaces            TO       GV23-DATENC                          
           move spaces            TO       GV23-CDEV                            
           move spaces            TO       GV23-WUNITR                          
           move spaces            TO       GV23-LRCMMT                          
           move spaces            TO       GV23-NSOCP                           
           move spaces            TO       GV23-NLIEUP                          
           move 0                 TO       GV23-NTRANS                          
           move 0                 TO       GV23-NSEQFV                          
           move 0                 TO       GV23-NSEQNQ                          
           move 0                 TO       GV23-NSEQREF                         
           move 0                 TO       GV23-NMODIF                          
           move spaces            TO       GV23-NSOCORIG                        
           move spaces            TO       GV23-DTOPE                           
           move spaces            TO       GV23-NSOCDEPLIV                      
           move spaces            TO       GV23-NLIEUDEPLIV                     
           move +100 to sqlcode                                                 
           PERFORM UNTIL SQLCODE = 0                                            
                   OR GV23-NSEQ = '99'                                          
               MOVE GV23-NSEQ       TO WS-GV23NSEQ                              
               ADD  1               TO WS-GV23NSEQ                              
               MOVE WS-GV23NSEQ     TO GV23-NSEQ                                
                     PERFORM LOG-GV2300                                         
           end-perform.                                                         
       F-LOG-GV23. EXIT.                                                        
       log-gv2300 section.                                                      
           EXEC SQL INSERT                                              2941    
                    INTO     RVGV2305                                   2942    
                            (NSOCIETE,                                  2943    
                             NLIEU,                                     2944    
                             NVENTE,                                    2945    
                             CTYPENREG,                                 2946    
                             NCODICGRP,                                 2947    
                             NCODIC,                                    2948    
                             NSEQ,                                      2949    
                             CMODDEL,                                   2950    
                             DDELIV,                                    2951    
                             NORDRE,                                    2952    
                             CENREG,                                    2953    
                             QVENDUE,                                   2954    
                             PVUNIT,                                    2955    
                             PVUNITF,                                   2956    
                             PVTOTAL,                                   2957    
                             CEQUIPE,                                   2958    
                             NLIGNE,                                    2959    
                             TAUXTVA,                                   2960    
                             QCONDT,                                    2961    
                             PRMP,                                      2962    
                             WEMPORTE,                                  2963    
                             CPLAGE,                                    2964    
                             CPROTOUR,                                  2965    
                             CADRTOUR,                                  2966    
                             CPOSTAL,                                   2967    
                             LCOMMENT,                                  2968    
                             CVENDEUR,                                  2969    
                             NSOCLIVR,                                  2970    
                             NDEPOT,                                    2971    
                             NAUTORM,                                   2972    
                             WARTINEX,                                  2973    
                             WEDITBL,                                   2974    
                             WACOMMUTER,                                2975    
                             WCQERESF,                                  2976    
                             NMUTATION,                                 2977    
                             CTOURNEE,                                  2978    
                             WTOPELIVRE,                                2979    
                             DCOMPTA,                                   2980    
                             DCREATION,                                 2981    
                             HCREATION,                                 2982    
                             DANNULATION,                               2983    
                             NLIEUORIG,                                 2984    
                             WINTMAJ,                                   2985    
                             DSYST,                                     2986    
                             DSTAT,                                     2987    
                             QVENDUE1,                                  2988    
                             PVUNIT1,                                   2989    
                             PVTOTAL1,                                  2990    
                             DCREATION1,                                2991    
                             DDELIV1,                                   2992    
                             NETNAME,                                   2993    
                             CACID,                                     2994    
                             CMODDEL1,                                  2995    
                             NSOCMODIF,                                 2996    
                             NLIEUMODIF,                                2997    
                             DATENC,                                    2998    
                             CDEV,                                      2999    
                             WUNITR,                                    3000    
                             LRCMMT,                                    3001    
                             NSOCP,                                     3002    
                             NLIEUP,                                    3003    
                             NTRANS,                                    3004    
                             NSEQFV,                                    3005    
                             NSEQNQ,                                    3006    
                             NSEQREF,                                   3007    
                             NMODIF,                                    3008    
                             NSOCORIG,                                  3009    
                             DTOPE,                                     3010    
                             NSOCDEPLIV,                                3011    
                             NLIEUDEPLIV)                               3012    
                       VALUES  (                                        3013    
                            :GV23-NSOCIETE,                             3014    
                            :GV23-NLIEU,                                3015    
                            :GV23-NVENTE,                               3016    
                            :GV23-CTYPENREG,                            3017    
                            :GV23-NCODICGRP,                            3018    
                            :GV23-NCODIC,                               3019    
                            :GV23-NSEQ,                                 3020    
                            :GV23-CMODDEL,                              3021    
                            :GV23-DDELIV,                               3022    
                            :GV23-NORDRE,                               3023    
                            :GV23-CENREG,                               3024    
                            :GV23-QVENDUE,                              3025    
                            :GV23-PVUNIT,                               3026    
                            :GV23-PVUNITF,                              3027    
                            :GV23-PVTOTAL,                              3028    
                            :GV23-CEQUIPE,                              3029    
                            :GV23-NLIGNE,                               3030    
                            :GV23-TAUXTVA,                              3031    
                            :GV23-QCONDT,                               3032    
                            :GV23-PRMP,                                 3033    
                            :GV23-WEMPORTE,                             3034    
                            :GV23-CPLAGE,                               3035    
                            :GV23-CPROTOUR,                             3036    
                            :GV23-CADRTOUR,                             3037    
                            :GV23-CPOSTAL,                              3038    
                            :GV23-LCOMMENT,                             3039    
                            :GV23-CVENDEUR,                             3040    
                            :GV23-NSOCLIVR,                             3041    
                            :GV23-NDEPOT,                               3042    
                            :GV23-NAUTORM,                              3043    
                            :GV23-WARTINEX,                             3044    
                            :GV23-WEDITBL,                              3045    
                            :GV23-WACOMMUTER,                           3046    
                            :GV23-WCQERESF,                             3047    
                            :GV23-NMUTATION,                            3048    
                            :GV23-CTOURNEE,                             3049    
                            :GV23-WTOPELIVRE,                           3050    
                            :GV23-DCOMPTA,                              3051    
                            :GV23-DCREATION,                            3052    
                            :GV23-HCREATION,                            3053    
                            :GV23-DANNULATION,                          3054    
                            :GV23-NLIEUORIG,                            3055    
                            :GV23-WINTMAJ,                              3056    
                            :GV23-DSYST,                                3057    
                            :GV23-DSTAT,                                3058    
                            :GV23-QVENDUE1,                             3059    
                            :GV23-PVUNIT1,                              3060    
                            :GV23-PVTOTAL1,                             3061    
                            :GV23-DCREATION1,                           3062    
                            :GV23-DDELIV1,                              3063    
                            :GV23-NETNAME,                              3064    
                            :GV23-CACID,                                3065    
                            :GV23-CMODDEL1,                             3066    
                            :GV23-NSOCMODIF,                            3067    
                            :GV23-NLIEUMODIF,                           3068    
                            :GV23-DATENC,                               3069    
                            :GV23-CDEV,                                 3070    
                            :GV23-WUNITR,                               3071    
                            :GV23-LRCMMT,                               3072    
                            :GV23-NSOCP,                                3073    
                            :GV23-NLIEUP,                               3074    
                            :GV23-NTRANS,                               3075    
                            :GV23-NSEQFV,                               3076    
                            :GV23-NSEQNQ,                               3077    
                            :GV23-NSEQREF,                              3078    
                            :GV23-NMODIF,                               3079    
                            :GV23-NSOCORIG,                             3080    
                            :GV23-DTOPE,                                3081    
                            :GV23-NSOCDEPLIV,                           3082    
                            :GV23-NLIEUDEPLIV)                          3083    
           END-EXEC.                                                    3084    
       f-log-gv2300. exit.                                                      
       OBTENIR-PARAMETRES SECTION.                                              
      ******************************                                            
           MOVE 'N' TO TOP-DISPLAY                                              
THEMIS     MOVE 'N' TO W-THEMIS                                                 
FT2910     MOVE 'N' TO W-ENC-GV-F5                                              
           SET PARAM-NECEN-NON-TROUVE TO TRUE                                   
           PERFORM VARYING WP-RANG-TSTNECEN  FROM 1 BY 1                        
           UNTIL  WC-TSTNECEN-FIN OR PARAM-NECEN-TROUVE                         
             PERFORM LECTURE-TSTNECEN                                           
             IF  WC-TSTNECEN-SUITE                                              
                EVALUATE TSTNECEN-NSOCZP                                        
                   WHEN 'DISPL'                                                 
                      MOVE TSTNECEN-WFLAG    TO TOP-DISPLAY                     
                   WHEN 'NECEN'                                                 
                      IF TSTNECEN-TRANS  = 'MGV65'                              
                         MOVE TSTNECEN-WPARAM(8:1)   TO  W-APPEL-DACEM          
                         MOVE TSTNECEN-WPARAM(3:2)   TO  W-FILIERE              
                         MOVE TSTNECEN-WPARAM(6:2)   TO  W-DELAI                
                         MOVE TSTNECEN-LIBELLE(9:1)  TO  W-VTE-MONO             
                         MOVE TSTNECEN-LIBELLE(10:1) TO  W-VTE-MULTI            
                         MOVE TSTNECEN-LIBELLE(11:1) TO  W-PROD-RETARD          
                         MOVE TSTNECEN-LIBELLE(12:1)                            
                                                  TO W-PROD-PAS-RETARD          
                         MOVE TSTNECEN-LIBELLE(13:1) TO  W-ENTETE-MUT           
THEMIS*                  SET PARAM-NECEN-TROUVE      TO TRUE                    
                      END-IF                                                    
THEMIS                IF TSTNECEN-TRANS  = 'MEC64'                              
                        IF TSTNECEN-WPARAM(8:1) = '1'                           
                          MOVE TSTNECEN-WPARAM(1:6)   TO  W-ENTR1               
                        END-IF                                                  
                        IF TSTNECEN-WPARAM(8:1) = '2'                           
                          MOVE TSTNECEN-WPARAM(1:6)   TO  W-ENTR2               
                        END-IF                                                  
                        IF TSTNECEN-WPARAM(8:1) = '3'                           
                          MOVE TSTNECEN-WPARAM(1:6)   TO  W-ENTR3               
                        END-IF                                                  
                        IF TSTNECEN-WPARAM(8:1) = '4'                           
                          MOVE TSTNECEN-WPARAM(1:6)   TO  W-ENTR4               
                        END-IF                                                  
                        IF TSTNECEN-WPARAM(8:1) = '5'                           
                          MOVE TSTNECEN-WPARAM(1:6)   TO  W-ENTR5               
                        END-IF                                                  
                      END-IF                                                    
THEMIS                IF TSTNECEN-TRANS  = 'THEMI'                              
                        IF TSTNECEN-WFLAG = 'O'                                 
                          MOVE 'O' TO W-THEMIS                                  
                        END-IF                                                  
                      END-IF                                                    
FT2910                IF TSTNECEN-TRANS  = 'ENC64'                              
                        IF TSTNECEN-WFLAG = 'O'                                 
                          MOVE 'O' TO W-ENC-GV-F5                               
                        END-IF                                                  
                      END-IF                                                    
                END-EVALUATE                                                    
             END-IF                                                             
           END-PERFORM                                                          
           IF PARAM-NECEN-NON-TROUVE                                            
              MOVE 'O'                  TO  W-APPEL-DACEM                       
              MOVE '91'                 TO  W-FILIERE                           
              MOVE '15'                 TO  W-DELAI                             
              MOVE  'N'                 TO  W-VTE-MONO                          
              MOVE  'N'                 TO  W-VTE-MULTI                         
              MOVE  'O'                 TO  W-PROD-RETARD                       
              MOVE  'N'                 TO  W-PROD-PAS-RETARD                   
              MOVE  'O'                 TO  W-ENTETE-MUT                        
           END-IF                                                               
           INITIALIZE GFQNT0                                                    
           MOVE COMM-MEC64-DJOUR TO GFSAMJ-0                                    
           MOVE '5' TO GFDATA                                                   
           PERFORM APPEL-TETDATC                                                
           COMPUTE GFQNT0 = GFQNT0 + 1                                          
           MOVE '3' TO GFDATA                                                   
           PERFORM APPEL-TETDATC                                                
           MOVE GFSAMJ-0           TO W-DATE-J-PLUS1                            
      * CALCUL DATE DU JOUR +15 POUR LES RESA TEMPO                             
           INITIALIZE GFQNT0                                                    
           MOVE COMM-MEC64-DJOUR TO GFSAMJ-0                                    
           MOVE '5' TO GFDATA                                                   
           PERFORM APPEL-TETDATC                                                
           COMPUTE GFQNT0 = GFQNT0 + W-DELAI                                    
           MOVE '3' TO GFDATA                                                   
           PERFORM APPEL-TETDATC                                                
           MOVE GFSAMJ-0           TO W-DATE-J-PLUS15                           
           .                                                                    
       FIN-OBTENIR-PARAMETRES. EXIT.                                            
LA0611*CHERCHE-MDLIV-EMX SECTION.                                               
LA0611 CHERCHE-MDLIV-CMODDEL SECTION.                                           
           MOVE FUNC-SELECT   TO TRACE-SQL-FUNCTION                             
           MOVE 'RTGA01'      TO TABLE-NAME                                     
           MOVE SPACES        TO RVGA0100                                       
           MOVE 'MDLIV'       TO GA01-CTABLEG1                                  
LA0611*    MOVE 'EMX'         TO GA01-CTABLEG2                                  
LA0611     MOVE WS-MDLIV-CMODDEL  TO GA01-CTABLEG2                              
           EXEC SQL SELECT WTABLEG INTO :GA01-WTABLEG                           
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 =:GA01-CTABLEG1                             
                       AND CTABLEG2 =:GA01-CTABLEG2                             
CEN012          WITH UR                                                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF TROUVE                                                            
              SET MDLIV-TROUVE    TO TRUE                                       
              MOVE GA01-WTABLEG   TO MDLIV-WTABLEG                              
              MOVE MDLIV-WDONNEES TO WS-WDONNEES                                
           ELSE                                                                 
              SET MDLIV-NON-TROUVE    TO TRUE                                   
              STRING 'RECHERCHE MDLIV ' COMM-MEC64-NVENTE                       
              DELIMITED BY SIZE INTO W-MESSAGE                                  
              PERFORM DISPLAY-LOG                                               
           END-IF                                                               
           .                                                                    
LA0611 F-CHERCHE-MDLIV-CMODDEL SECTION. EXIT.                                   
LA0611*F-CHERCHE-MDLIV-EMX SECTION. EXIT.                                       
      *                                                                         
       TRAITEMENT-RESA                SECTION.                                  
      * DARTY : ON RESERVE LE STOCK                                             
      * DACEM : ON CONTROLE LE STOCK, POUR ENVOI EVENTUEL A DCOM                
      *         DES STOCKS A ZERO PAR LE MEC03                                  
           IF MDLIV-TROUVE                                                      
              PERFORM VARYING I-NL FROM 1 BY 1                                  
              UNTIL COMM-MEC64-TAB-NCODIC(I-NL) = SPACES OR LOW-VALUES          
                 MOVE COMM-MEC64-TAB-NCODIC(I-NL) TO GA00-NCODIC                
                 PERFORM SELECT-RTGA00                                          
                 MOVE GA00-CFAM  TO COMM-MEC64-TAB-CFAM(I-NL)                   
                 IF (GA00-WDACEM = 'N')                                         
                    SET COMM-MEC64-CDEDARTY(I-NL) TO TRUE                       
                    PERFORM CHERCHE-LIEU-STOCKAGE                               
                    INITIALIZE COMM-EC67-APPLI                                  
                    SET COMM-EC67-NEW-APPEL     TO TRUE                         
                    PERFORM APPEL-MEC67                                         
                    IF COMM-EC67-STOCK-DISPONIBLE                               
      * MODULE DEDIE AUX MUTATIONS                                              
                       PERFORM ALIM-MEC69-RESA                                  
                       PERFORM APPEL-MEC69                                      
                    END-IF                                                      
                    IF COMM-EC67-STOCK-FOURN-OUI                                
                       SET EC64-ARTICLE-EN-CF (I-NL) TO TRUE                    
                    END-IF                                                      
LA1807*          ELSE                                                           
LA1807*             SET COMM-MEC64-CDEDACEM (I-NL) TO TRUE                      
LA1807*             MOVE SPACES            TO GV22-CSTATUT                      
                 END-IF                                                         
                 IF COMM-MEC64-INTERNET = 'O'                                   
                    PERFORM MODULE-INTERNET                                     
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF                                                               
           .                                                                    
       F-TRT-RESA.       EXIT.                                                  
      *                                                                         
       MODULE-INTERNET SECTION.                                                 
      **************************                                                
           MOVE W-SPDATDB2-DSYST           TO GV11-DSYST                        
           MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO GV11-NSOCLIVR                     
           MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO GV11-NDEPOT                       
           MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO GV11-NSOCGEST                     
           MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO GV11-NLIEUGEST                    
           MOVE MDLIV-LDEPLIVR-EXP (1:3)   TO GV11-NSOCDEPLIV                   
           MOVE MDLIV-LDEPLIVR-EXP (4:3)   TO GV11-NLIEUDEPLIV                  
           PERFORM INSERT-RTGV22                                                
           IF SQLCODE = 0                                                       
CEN012        MOVE SPACES TO GV11-LCOMMENT                                      
LA1807*       IF COMM-MEC64-CDEDACEM (I-NL)                                     
LA1807*          MOVE   'DACEM' TO GV11-LCOMMENT                                
LA1807*       ELSE                                                              
                 IF COMM-EC67-STOCK-DISPONIBLE                                  
                    STRING 'DARTY' COMM-EC69-NMUTTEMP                           
                    COMM-EC67-LIEU-STOCK                                        
                    DELIMITED BY SIZE INTO GV11-LCOMMENT                        
                 ELSE                                                           
                   STRING 'DARTY' '       '                                     
                   COMM-EC67-LIEU-STOCK                                         
                   DELIMITED BY SIZE INTO GV11-LCOMMENT                         
                   MOVE COMM-EC67-NSOCDEPOT TO GV11-NSOCLIVR                    
                   MOVE COMM-EC67-NDEPOT    TO GV11-NDEPOT                      
                 END-IF                                                         
LA1807*       END-IF                                                            
      *       IF GV11-LCOMMENT(1:5) NOT = 'DARTY'                               
      *       AND GV11-LCOMMENT(1:5) NOT = 'DACEM'                              
      *          STRING 'DARTY' '       '                                       
      *          COMM-EC67-LIEU-STOCK                                           
      *          DELIMITED BY SIZE INTO GV11-LCOMMENT                           
      *          MOVE COMM-EC67-NSOCDEPOT TO GV11-NSOCLIVR                      
      *          MOVE COMM-EC67-NDEPOT    TO GV11-NDEPOT                        
      *       END-IF                                                            
              MOVE COMM-MEC64-TAB-NSEQ  (I-NL)    TO GV11-NSEQ                  
              MOVE COMM-MEC64-TAB-NCODICGRP(I-NL) TO GV11-NCODICGRP             
              MOVE COMM-MEC64-TAB-CTYPENREG(I-NL) TO GV11-CTYPENREG             
              PERFORM UPDATE-GV1100-RESA                                        
              IF (SQLCODE = 0) AND (COMM-MEC64-CDEDARTY(I-NL))                  
                 MOVE '0000'    TO COMM-EC67-CODRET                             
              END-IF                                                            
           END-IF.                                                              
           INITIALIZE RVGV2204                                                  
           .                                                                    
       FIN-MODULE-INTERNET. EXIT.                                               
      *                                                                         
       ACTION-ENCAISSEMENT            SECTION.                                  
      *                                                                         
           STRING 'MEC64 ACTION ENCAISSEMENT ***** '                            
                    COMM-MEC64-NVENTE                                           
                    DELIMITED BY SIZE INTO W-MESSAGE                            
                    PERFORM DISPLAY-LOG                                         
           MOVE 'MGV66'             TO COMM-MEC64-CPROG                         
           PERFORM STATUT-VENTE                                                 
           SET COMM-EC67-NEW-APPEL  TO TRUE                                     
           PERFORM ACTION-CONTROLE                                              
           EVALUATE TRUE                                                        
           WHEN COMM-MEC64-TYPE-NB-MONO                                         
              PERFORM GESTION-VENTE-MONO-PRODUIT                                
           WHEN COMM-MEC64-TYPE-NB-MULTI                                        
              PERFORM GESTION-VENTE-MULTI-PRODUITS                              
           WHEN COMM-MEC64-TYPE-ANNUL                                           
              IF NOT COMM-MEC64-CREATION                                        
                 PERFORM ACTION-SORTIE                                          
              END-IF                                                            
           END-EVALUATE                                                         
           .                                                                    
       F-ACTION-ENCAISSEMENT.  EXIT.                                            
      *                                                                         
       STATUT-VENTE                   SECTION.                                  
      *                                                                         
           SET EC64-STATUT-COMPLETE TO TRUE                                     
           SET EC64-STATUT-PREC-NON-DEFINI TO TRUE                              
           PERFORM SELECT-RTSL20                                                
           IF TROUVE                                                            
              IF SL20-incomplete =  'O'                                         
                 SET EC64-STATUT-PREC-INCOMPLETE TO TRUE                        
              ELSE                                                              
                 SET EC64-STATUT-PREC-COMPLETE TO TRUE                          
              END-IF                                                            
           ELSE                                                                 
              SET EC64-STATUT-PREC-OK       TO TRUE                             
           END-IF                                                               
           .                                                                    
       F-STATUT-VENTE.         EXIT.                                            
      *                                                                         
      ***************************************                                   
       COMMANDE-VENTE-INCOMPLETE SECTION.                                       
      ************************************                                      
      * VENTE INCOMPLETE : STATUT A POSITIONNER                                 
      * VENTE AVEC AU MOINS 1 ARTICLE DARTY EN COMMANDE FOURNISSEUR             
      * VENTE AVEC AU MOINS 1 ARTICLE INDISPONIBLE                              
      * VENTE AVEC AU MOINS 1 ARTICLE DACEM EN CONTREMARQUE                     
      * VENTE AVEC AU MOINS 1 ARTICLE DACEM(DACEM HS PENDANT MIGRATION          
      * VERS GENERIX)                                                           
      * VENTE AVEC AU MOINS 1 ARTICLE EN STATUT INDETERMINE SUITE A UN          
      * PROBLEME, DANS LE DOUTE ON MET EN INCOMPLETE                            
           IF COMM-MEC64-CREATION                                               
             MOVE COMM-MEC64-DDELIV-COM       TO COMM-MEC64-DDELIV              
             PERFORM ACTION-SET-INCOMPLETE                                      
             MOVE '0000'    TO COMM-MEC64-CODE-RETOUR                           
             MOVE SPACES    TO COMM-MEC64-MESSAGE                               
           ELSE                                                                 
              MOVE COMM-MEC64-DDELIV-COM       TO COMM-MEC64-DDELIV             
              PERFORM ACTION-SET-INCOMPLETE                                     
           END-IF                                                               
           .                                                                    
       F-COMMANDE-VENTE-INCOMPLETE. EXIT.                                       
      ******************************                                            
       GESTION-VENTE-MONO-PRODUIT SECTION.                                      
      ************************************                                      
           IF EC64-STATUT-PREC-INCOMPLETE                                       
              PERFORM DELETE-RTSL20                                             
              PERFORM GESTION-RESERVATION                                       
           ELSE                                                                 
      * -> UN MONO SE TRAITE LORS DE SA CREATION.                               
              IF (COMM-EC64-LIGNE-A-TRAITER)                                    
FT2910        OR (COMM-MEC64-MODIF AND W-ENC-GV-F5 = 'O')                       
                 IF EC64-MULTIDA = 'O' OR 'D'                                   
                    PERFORM ACTION-VALID-DATE                                   
                 ELSE                                                           
                   PERFORM DELETE-RTSL20                                        
                   PERFORM GESTION-RESERVATION                                  
                 END-IF                                                         
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       F-GESTION-VENTE-MONO-PRODUIT. EXIT.                                      
      *************************************                                     
       GESTION-VENTE-MULTI-PRODUITS SECTION.                                    
      * -> STATUT COMPLET TRAITE A :                                            
      *    1ER ENCAISSEMENT                                                     
      *    AJOUT PRODUIT OU LIGNE A TRAITER                                     
      *    STATUT PRECEDENT INCOMPLET                                           
      * -> STATUT INCOMPLET TRAITE A                                            
      *    1ER ENCAISSEMENT                                                     
      *    AJOUT TRAITE OU LIGNE A TRAITER                                      
      *    ETAT PRECEDENT NON DEFINI ------------> REDONDANT.                   
           IF EC64-STATUT-COMPLETE                                              
              IF COMM-MEC64-CREATION                                            
              OR COMM-EC64-LIGNE-A-TRAITER                                      
              OR EC64-STATUT-PREC-INCOMPLETE                                    
                 PERFORM ACTION-VALID-DATE                                      
              ELSE                                                              
                 IF EC64-STATUT-PREC-COMPLETE                                   
                    PERFORM ACTION-SORTIE                                       
                   IF (COMM-EC64-LIGNE-A-TRAITER)                               
FT2910             OR (W-ENC-GV-F5   = 'O' )                                    
                    PERFORM ACTION-VALID-DATE                                   
                   END-IF                                                       
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              IF COMM-MEC64-CREATION                                            
              OR COMM-EC64-LIGNE-A-TRAITER                                      
              OR EC64-STATUT-PREC-NON-DEFINI                                    
                 IF EC64-STATUT-INCOMPLETE                                      
                    PERFORM COMMANDE-VENTE-INCOMPLETE                           
                 END-IF                                                         
              ELSE                                                              
      * SI ON EST DANS LE CAS D'UNE SUPPRESSION DE LIGNE SUR GV, ON             
      * SUPPRIME TOUTES LES LIGNES EVENTUELLES DE SL21 QUI NE SONT              
      * PLUS DANS GV11                                                          
                 MOVE ZERO TO W-NBLIGNENS                                       
                 PERFORM DECLARE-CSL21                                          
                 PERFORM FETCH-CSL21                                            
                 PERFORM UNTIL SQLCODE = 100                                    
                     PERFORM DELETE-RTSL21-UN                                   
                     IF TROUVE                                                  
                        ADD  1 TO W-NBLIGNENS                                   
                     END-IF                                                     
                     PERFORM FETCH-CSL21                                        
                 END-PERFORM                                                    
                 IF W-NBLIGNENS > 0                                             
                    PERFORM SELECT-RTGV22-ANO                                   
                    COMPUTE W-NB-LIGNE-SL21 =  W-NBLIGNENS                      
                    IF W-NB-LIGNE-SL21 = 0                                      
                       PERFORM DELETE-RTSL20                                    
                    ELSE                                                        
                       PERFORM UPDATE-RTSL20                                    
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF                                                               
           .                                                                    
       F-GESTION-VENTE-MULTI-PRODUITS. EXIT.                                    
      ***************************************                                   
       GESTION-RESERVATION  SECTION.                                            
      ******************************                                            
           MOVE COMM-MEC64-NSOCIETE TO GV11-NSOCIETE                            
           MOVE COMM-MEC64-NLIEU    TO GV11-NLIEU                               
           MOVE COMM-MEC64-NVENTE   TO GV11-NVENTE                              
           MOVE 0                   TO W-NB-LIGNE-SL21                          
           PERFORM DELETE-RTSL21-TOT                                            
           SET PAS-CREATION-RTSL21 TO TRUE                                      
           PERFORM VARYING I-NL FROM 1 BY 1                                     
           UNTIL COMM-MEC64-TAB-NCODIC(I-NL) = SPACES OR LOW-VALUES             
              IF NOT EC64-ARTICLE-MUTE (I-NL)                                   
LA1807*          MOVE '0000'              TO COMM-EC68-CODRET                   
                 MOVE '0000'              TO COMM-EC69-CODRET                   
                 SET CODIC-TRT-KO         TO TRUE                               
                 MOVE COMM-MEC64-TAB-LCOMMENT(I-NL) TO GV11-LCOMMENT            
                 IF (COMM-MEC64-CDEDARTY(I-NL))                                 
                    MOVE GV11-LCOMMENT(6:7) TO GV22-NMUTATION                   
                 ELSE                                                           
                    MOVE SPACES             TO GV22-NMUTATION                   
                 END-IF                                                         
                 IF COMM-MEC64-TYPE-NB-MULTI                                    
                    MOVE COMM-MEC64-DDELIV-COM                                  
                                         TO COMM-MEC64-TAB-DDELIVO(I-NL)        
                 END-IF                                                         
                 IF (EC64-STOCK-DISPONIBLE(I-NL)                                
                 OR  EC64-STOCK-ATT-FOURN(I-NL))                                
                 AND (COMM-MEC64-CDEDARTY(I-NL))                                
                    SET CODIC-TRT-OK         TO TRUE                            
                    PERFORM CHERCHE-LIEU-STOCKAGE                               
                    PERFORM MUT-SL300-O-N                                       
                    PERFORM ALIM-MEC69-ENC                                      
                    PERFORM APPEL-MEC69                                         
                    IF EC64-STOCK-ATT-FOURN(I-NL)                               
                       MOVE 'A'     TO GV22-CSTATUT                             
                       MOVE 'F'     TO GV11-WCQERESF                            
                       MOVE 'F'     TO GV22-WCQERESF                            
                    ELSE                                                        
                       MOVE 'R'     TO GV22-CSTATUT                             
                       MOVE ' '     TO GV11-WCQERESF                            
                       MOVE ' '     TO GV22-WCQERESF                            
                    END-IF                                                      
                    IF COMM-EC69-CODRET = '0000'                                
                       IF COMM-MEC64-TAB-LCOMMENT(I-NL)(1:5) = 'DARTY'          
                          MOVE COMM-EC69-NSOCDEPOTO     TO GV11-NSOCLIVR        
                          MOVE COMM-EC69-NDEPOTO        TO GV11-NDEPOT          
                          MOVE COMM-EC69-NSOCDEPOTO     TO GV22-NSOCLIVR        
                          MOVE COMM-EC69-NDEPOTO        TO GV22-NDEPOT          
                       END-IF                                                   
                       MOVE 'MGV66' TO GV11-CEQUIPE                             
                       MOVE COMM-EC69-NMUTATION  TO GV11-LCOMMENT(6:7)          
                                                    GV22-NMUTATION              
                       IF EC64-STOCK-ATT-FOURN(I-NL)                            
                          PERFORM INSERT-RTSL21-ENC                             
                       END-IF                                                   
                    END-IF                                                      
LA1807*          ELSE                                                           
LA1807*             IF COMM-MEC64-CDEDACEM(I-NL)                                
LA1807*                SET CODIC-TRT-OK         TO TRUE                         
LA1807*                MOVE ' NN' TO COMM-EC68-NEW-MODE                         
LA1807*                SET COMM-EC68-NEW-APPEL TO TRUE                          
LA1807*                SET COMM-EC68-CREATION-DACEM TO TRUE                     
LA1807*                PERFORM MUT-SL300-O-N                                    
LA1807*                MOVE WS-MUT-A-J TO COMM-EC68-RESA-A-J                    
LA1807*                PERFORM APPEL-MEC68                                      
LA1807*                IF COMM-EC68-CODRET = '0000'                             
LA1807*                   MOVE 'MGV66' TO GV11-CEQUIPE                          
LA1807*                   MOVE 'R'     TO GV22-CSTATUT                          
LA1807*                   MOVE ' '   TO GV11-WCQERESF                           
LA1807*                   MOVE ' '   TO GV22-WCQERESF                           
LA1807*                END-IF                                                   
LA1807*             END-IF                                                      
                 END-IF                                                         
                 IF (CODIC-TRT-OK                                               
LA1807*            AND ((COMM-EC68-CODRET NOT = '0000')                         
LA1807*               OR (COMM-EC69-CODRET NOT = '0000'))  )                    
LA1807             AND (COMM-EC69-CODRET NOT = '0000'))                         
                 OR EC64-aucun-stock-dispo (I-NL)                               
      * la r�servation c'est mal pass�e, on met le statut � en attente          
      * on met le cequipe � 66vgm et on cr�� un enregistrement dans             
      * SL21                                                                    
                    PERFORM INSERT-RTGV27                                       
                    MOVE SPACE   TO COMM-EC69-NMUTATION                         
                    MOVE '66VGM' TO GV11-CEQUIPE                                
                    IF COMM-MEC64-TAB-LCOMMENT(I-NL)(1:5) = 'DARTY'             
                       MOVE COMM-MEC64-TAB-NSOCLIVR(I-NL)                       
                                         TO GV11-NSOCLIVR                       
                       MOVE COMM-MEC64-TAB-NDEPOT(I-NL)                         
                                         TO GV11-NDEPOT                         
                       MOVE COMM-MEC64-TAB-NSOCLIVR(I-NL)                       
                                         TO GV22-NSOCLIVR                       
                       MOVE COMM-MEC64-TAB-NDEPOT(I-NL)                         
                                         TO GV22-NDEPOT                         
                    END-IF                                                      
LA1807*             IF COMM-MEC64-CDEDACEM(I-NL)                                
LA1807*             OR ((COMM-MEC64-CDEDARTY(I-NL))                             
LA1807              IF ((COMM-MEC64-CDEDARTY(I-NL))                             
                    AND ( NOT EC64-STOCK-DISPONIBLE(I-NL)))                     
                       MOVE 'A'     TO GV22-CSTATUT                             
                       PERFORM INSERT-RTSL21-ENC                                
                    ELSE                                                        
      * STOCK DARTY DISPO                                                       
                       PERFORM INSERT-RTSL21-ENC                                
                       IF W-NB-LIGNE-SL21 > 1                                   
                          SUBTRACT 1 FROM W-NB-LIGNE-SL21                       
                       ELSE                                                     
                          MOVE 0               TO W-NB-LIGNE-SL21               
                          MOVE 'OK'            TO W-SL20-STATUT                 
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
                 PERFORM UPDATE-RTGV11                                          
                 PERFORM LOG-GV23                                               
                 PERFORM UPDATE-RTGV22-ENC                                      
              END-IF                                                            
           END-PERFORM                                                          
      * si on a cr�� des enregistrements dans SL21, on met � jour ou            
      * on cr�� la SL20 correspondante                                          
           IF CREATION-RTSL21                                                   
              PERFORM INSERT-RTSL20                                             
           END-IF                                                               
           .                                                                    
       FIN-GESTION-RESERVATION. EXIT.                                           
      ********************************                                          
LA1807*PARAM-MUT-DACEM                SECTION.                                  
LA1807*                                                                         
LA1807*    IF EC64-MULTIDA = 'D'                                                
LA1807*       IF EC64-VENTE-CQE = 'N'                                           
LA1807*          MOVE 'N'           TO COMM-EC68-MUT-SL300                      
LA1807*       ELSE                                                              
LA1807*          MOVE WS-MUT-SL300  TO COMM-EC68-MUT-SL300                      
LA1807*       END-IF                                                            
LA1807*    ELSE                                                                 
LA1807*      MOVE 'N'           TO COMM-EC68-MUT-SL300                          
LA1807*    END-IF                                                               
LA1807*    .                                                                    
LA1807*FIN-PARAM-MUT-DACEM.     EXIT.                                           
      ******************************************************************        
      * LINK A TETDATC.                                                         
      ******************************************************************        
       APPEL-TETDATC SECTION.                                                   
      ************************                                                  
           EXEC CICS LINK PROGRAM ('TETDATC')                                   
                     COMMAREA (Z-COMMAREA-TETDATC)                              
           END-EXEC.                                                            
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-APPEL-TETDATC. EXIT.                                                 
       MESSAGE-ERR SECTION.                                                     
           MOVE '0001'                TO  PTMG-NSEQERR                          
           MOVE W-DATE-J-PLUS1 (7:2)  TO  PTMG-ALP (1) (1:2)                    
           MOVE '/'                   TO  PTMG-ALP (1) (3:1)                    
           MOVE W-DATE-J-PLUS1 (5:2)  TO  PTMG-ALP (1) (4:2)                    
           MOVE '/'                   TO  PTMG-ALP (1) (6:1)                    
           MOVE W-DATE-J-PLUS1 (3:2)  TO  PTMG-ALP (1) (7:2)                    
           PERFORM MLIBERRGEN                                                   
           .                                                                    
       FIN-MESSAGE-ERR.   EXIT.                                                 
       DISPLAY-LOG SECTION.                                                     
           EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)                
                LENGTH (80) NOHANDLE                                            
           END-EXEC.                                                            
           IF AUTO-DISPLAY-TEST                                                 
              STRING 'MEC64 ' W-MESSAGE                                         
              DELIMITED BY SIZE INTO W-MESS-EC05                                
              EXEC SQL INSERT  INTO    RVEC0500                                 
                                ( TIMESTP       ,                               
                                  WRECENV        ,                              
                                  XMLDATA        )                              
                         VALUES (CURRENT TIMESTAMP ,                            
                                 'E'              ,                             
                                 :W-MESS-EC05   )                               
              END-EXEC                                                          
           END-IF                                                               
           MOVE SPACE TO W-MESSAGE.                                             
       F-DISPLAY-LOG. EXIT.                                                     
       MLIBERRGEN SECTION.                                                      
      **********************                                                    
           INSPECT PTMG-ALP(1) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(2) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(3) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(4) REPLACING ALL LOW-VALUE BY SPACE.                
           INSPECT PTMG-ALP(5) REPLACING ALL LOW-VALUE BY SPACE.                
           MOVE 'MEC64'                 TO PTMG-CNOMPGRM                        
           MOVE COMM-PTMG-LONG-COMMAREA TO LONG-COMMAREA-LINK                   
           MOVE Z-COMMAREA-PTMG         TO Z-COMMAREA-LINK                      
           MOVE 'MPTMG'                 TO NOM-PROG-LINK                        
           PERFORM LINK-PROG                                                    
           MOVE Z-COMMAREA-LINK         TO Z-COMMAREA-PTMG                      
           MOVE PTMG-LIBERR(6:59)       TO COMM-MEC64-MESSAGE.                  
           MOVE PTMG-LIBERR(1:04)       TO TOP-ERREUR-MEC64.                    
           INITIALIZE                      PTMG-LIBERR.                         
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-MLIBERRGEN. EXIT.                                                    
      ***********************                                                   
      ******************************************************************        
       WRITE-TS-MFL05 SECTION.                                                  
      *************************                                                 
           MOVE LENGTH OF TS-MFL05-DONNEES  TO LONG-TS                          
           MOVE TS-MFL05-DONNEES      TO Z-INOUT                                
           PERFORM WRITE-TS.                                                    
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-WRITE-TS-MFL05. EXIT.                                                
      ***************************                                               
       TEMP02-COPY SECTION. CONTINUE.                                           
         COPY SYKCTSRD.                                                         
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSWR.                            
       TEMP01-COPY SECTION. CONTINUE. COPY SYKCTSDE.                            
      *                                                                         
       CALL-SPDATDB2 SECTION.                                                   
      ***********************                                                   
           CALL 'SPDATDB2' USING W-SPDATDB2-RC W-SPDATDB2-DSYST.                
           IF W-SPDATDB2-RC NOT = ZERO                                          
              MOVE 'PBM DATE SYST�ME SPDATDB2' TO COMM-MEC64-CODE-RETOUR        
              MOVE '9999' TO COMM-MEC64-CODE-RETOUR                             
           END-IF                                                               
           .                                                                    
      *                                                                         
       GESTION-TSTENVOI  SECTION.                                               
           MOVE 1                            TO WP-RANG-TSTENVOI                
           PERFORM LECTURE-TSTENVOI                                             
           IF WC-TSTENVOI-FIN                                                   
              PERFORM CREATION-TSTENVOI                                         
           ELSE                                                                 
      * ON CHERCHE LE DERNIER ENREG DE LA TSTENVOI                              
              PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                      
              UNTIL WC-TSTENVOI-FIN                                             
                 PERFORM LECTURE-TSTENVOI                                       
              END-PERFORM                                                       
              IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '                        
                 PERFORM TRT-TSTENVOI                                           
                 IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '                     
                    PERFORM DELETE-TSTENVOI                                     
                    PERFORM CREATION-TSTENVOI                                   
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
       FIN-GEST-TSTENVOI.  EXIT.                                                
      *                                                                         
      *-----------------------------------------------------------------        
       CREATION-TSTENVOI SECTION.                                               
              MOVE '00000'                   TO TSTENVOI-NSOCZP                 
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM                 
              MOVE 'NOM PROGRAMME'           TO TSTENVOI-LIBELLE                
              PERFORM ECRITURE-TSTENVOI                                         
              SET WC-XCTRL-EXPDEL-NON-TROUVE TO TRUE                            
              PERFORM DECLARE-OPEN-CXCTRL                                       
              IF WC-XCTRL-EXPDEL-PB-DB2                                         
                 SET WC-TSTENVOI-ERREUR TO TRUE                                 
                 GO TO FIN-CREATION-TSTENVOI                                    
              END-IF                                                            
              PERFORM FETCH-CXCTRL                                              
              IF WC-XCTRL-EXPDEL-PB-DB2                                         
                 SET WC-TSTENVOI-ERREUR TO TRUE                                 
                 GO TO FIN-CREATION-TSTENVOI                                    
              END-IF                                                            
              PERFORM UNTIL WC-XCTRL-EXPDEL-FIN                                 
                 IF  XCTRL-WPARAM   > SPACES                                    
                 AND XCTRL-LIBELLE  > SPACES                                    
                    MOVE XCTRL-SOCZP         TO TSTENVOI-NSOCZP                 
                    MOVE XCTRL-CTRL          TO TSTENVOI-CTRL                   
                    MOVE XCTRL-WPARAM        TO TSTENVOI-WPARAM                 
                    MOVE XCTRL-LIBELLE       TO TSTENVOI-LIBELLE                
                    PERFORM ECRITURE-TSTENVOI                                   
                 END-IF                                                         
                 PERFORM FETCH-CXCTRL                                           
                 IF WC-XCTRL-EXPDEL-PB-DB2                                      
                    SET WC-TSTENVOI-ERREUR TO TRUE                              
                    GO TO FIN-CREATION-TSTENVOI                                 
                 END-IF                                                         
              END-PERFORM                                                       
              PERFORM CLOSE-CXCTRL                                              
      * AJOUT D'UN ENREG DE FIN                                                 
              MOVE '99999'                   TO TSTENVOI-NSOCZP                 
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM                 
              MOVE 'FIN TSTENVOI '           TO TSTENVOI-LIBELLE                
              PERFORM ECRITURE-TSTENVOI.                                        
       FIN-CREATION-TSTENVOI.  EXIT.                                            
      *                                                                         
      *-----------------------------------------------------------------        
       LECTURE-TSTENVOI SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         READQ TS                                                        
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         INTO   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         ITEM   (WP-RANG-TSTENVOI)                                       
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
              SET WC-TSTENVOI-SUITE      TO TRUE                                
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
           WHEN 26                                                              
           WHEN 44                                                              
              SET WC-TSTENVOI-FIN        TO TRUE                                
           WHEN OTHER                                                           
              SET WC-TSTENVOI-ERREUR     TO TRUE                                
           END-EVALUATE.                                                        
       FIN-LECT-TSTENVOI. EXIT.                                                 
      *                                                                         
       TRT-TSTENVOI SECTION.                                                    
      ***********************                                                   
      * ON ATTENT 2 SECONDE AVANT DE RELIRE LA TS                               
           INITIALIZE WN-TIME WN-TIME2 WN-DELTATIME                             
           ACCEPT WN-TIME  FROM TIME                                            
           PERFORM UNTIL WN-DELTATIME > 00000200                                
             ACCEPT WN-TIME2 FROM TIME                                          
             COMPUTE WN-DELTATIME = WN-TIME2 - WN-TIME                          
           END-PERFORM                                                          
           SET WC-TSTENVOI-SUITE      TO TRUE                                   
           PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                         
           UNTIL WC-TSTENVOI-FIN                                                
              PERFORM LECTURE-TSTENVOI                                          
           END-PERFORM                                                          
           .                                                                    
       FIN-TRT-TSTENVOI. EXIT.                                                  
      *                                                                         
      *-----------------------------------------------------------------        
       ECRITURE-TSTENVOI SECTION.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         WRITEQ TS                                                       
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         FROM   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
                QUEUE  (WC-ID-TSTENVOI)                                         
                FROM   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           INITIALIZE TS-TSTENVOI-DONNEES.                                      
       FIN-ECRIT-TSTENVOI. EXIT.                                                
      *                                                                         
       DELETE-TSTENVOI SECTION.                                                 
      ******************************                                            
           EXEC CICS DELETEQ TS                                                 
                QUEUE    (WC-ID-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
       FIN-DELETE-TSTENVOI. EXIT.                                               
      ********************************                                          
       DECLARE-OPEN-CXCTRL SECTION.                                             
           MOVE FUNC-DECLARE             TO TRACE-SQL-FUNCTION.                 
           MOVE 'RVGA01ZZ'               TO TABLE-NAME.                         
           MOVE 'EXPDEL'                 TO MODEL-NAME.                         
           EXEC SQL                                                             
                DECLARE CXCTRL CURSOR FOR                                       
                SELECT SOCZP, CTRL, WPARAM, LIBELLE                             
                FROM RVGA01ZZ                                                   
                WHERE TRANS = 'EXPDEL'                                          
                AND   WFLAG = 'O'                                               
                ORDER BY SOCZP, CTRL                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           PERFORM STATUT-CXCTRL                                                
           MOVE FUNC-OPEN                TO TRACE-SQL-FUNCTION                  
           EXEC SQL OPEN CXCTRL END-EXEC                                        
           PERFORM TEST-CODE-RETOUR-SQL                                         
           PERFORM STATUT-CXCTRL                                                
           .                                                                    
      *                                                                         
       FETCH-CXCTRL SECTION.                                                    
           MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.                 
           EXEC SQL                                                             
                FETCH CXCTRL                                                    
                INTO :XCTRL-SOCZP                                               
                   , :XCTRL-CTRL                                                
                   , :XCTRL-WPARAM                                              
                   , :XCTRL-LIBELLE                                             
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL                                         
           PERFORM STATUT-CXCTRL                                                
           .                                                                    
      *                                                                         
       CLOSE-CXCTRL SECTION.                                                    
           MOVE FUNC-CLOSE               TO TRACE-SQL-FUNCTION.                 
           EXEC SQL CLOSE CXCTRL  END-EXEC.                                     
           PERFORM TEST-CODE-RETOUR-SQL                                         
           PERFORM STATUT-CXCTRL                                                
           .                                                                    
      *                                                                         
       STATUT-CXCTRL SECTION.                                                   
           EVALUATE TRUE                                                        
           WHEN SQL-CODE = 0                                                    
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
           WHEN SQL-CODE = +100                                                 
              SET WC-XCTRL-EXPDEL-FIN    TO TRUE                                
           WHEN OTHER                                                           
              SET WC-XCTRL-EXPDEL-PB-DB2   TO TRUE                              
              SET WC-XCTRL-EXPDEL-FIN    TO TRUE                                
           END-EVALUATE                                                         
           .                                                                    
      ******************************                                            
      *                                                                         
       GESTION-TSTNECEN  SECTION.                                               
           MOVE 1                            TO WP-RANG-TSTNECEN                
           PERFORM LECTURE-TSTNECEN                                             
           IF WC-TSTNECEN-FIN                                                   
              PERFORM CREATION-TSTNECEN                                         
           ELSE                                                                 
      * ON CHERCHE LE DERNIER ENREG DE LA TSTNECEN                              
              PERFORM VARYING WP-RANG-TSTNECEN FROM 1 BY 1                      
              UNTIL WC-TSTNECEN-FIN                                             
                 PERFORM LECTURE-TSTNECEN                                       
              END-PERFORM                                                       
              IF TSTNECEN-LIBELLE  NOT = 'FIN TSTENVOI '                        
                 PERFORM DELETE-TSTNECEN                                        
                 PERFORM CREATION-TSTNECEN                                      
              END-IF                                                            
           END-IF                                                               
           SET WC-TSTNECEN-SUITE          TO TRUE                               
           .                                                                    
       FIN-GEST-TSTNECEN.  EXIT.                                                
      *                                                                         
      *-----------------------------------------------------------------        
       CREATION-TSTNECEN SECTION.                                               
              MOVE '00000'                   TO TSTNECEN-NSOCZP                 
              MOVE 'ZZZZZ'                   TO TSTNECEN-TRANS                  
              MOVE WS-TSTNECEN-NOMPGM        TO TSTNECEN-WPARAM                 
              MOVE 'NOM PROGRAMME'           TO TSTNECEN-LIBELLE                
              PERFORM ECRITURE-TSTNECEN                                         
              PERFORM DECLARE-OPEN-CPARAM                                       
              PERFORM FETCH-CPARAM                                              
              PERFORM UNTIL WC-XCTRL-NECEN-FIN                                  
                 MOVE XCTRL-SOCZP         TO TSTNECEN-NSOCZP                    
                 MOVE XCTRL-TRANS         TO TSTNECEN-TRANS                     
                 MOVE XCTRL-WPARAM        TO TSTNECEN-WPARAM                    
                 MOVE XCTRL-LIBELLE       TO TSTNECEN-LIBELLE                   
                 MOVE XCTRL-WFLAG         TO TSTNECEN-WFLAG                     
                 PERFORM ECRITURE-TSTNECEN                                      
                 PERFORM FETCH-CPARAM                                           
              END-PERFORM                                                       
              PERFORM CLOSE-CPARAM                                              
      * AJOUT D'UN ENREG DE FIN                                                 
              MOVE '99999'                   TO TSTNECEN-NSOCZP                 
              MOVE 'ZZZZZ'                   TO TSTNECEN-TRANS                  
              MOVE WS-TSTNECEN-NOMPGM        TO TSTNECEN-WPARAM                 
              MOVE 'FIN TSTENVOI '           TO TSTNECEN-LIBELLE                
              PERFORM ECRITURE-TSTNECEN                                         
              .                                                                 
       FIN-CREATION-TSTNECEN.  EXIT.                                            
      *                                                                         
      *-----------------------------------------------------------------        
       LECTURE-TSTNECEN SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         READQ TS                                                        
      *         QUEUE  (WC-ID-TSTNECEN)                                         
      *         INTO   (TS-TSTNECEN-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTNECEN-DONNEES)                          
      *         ITEM   (WP-RANG-TSTNECEN)                                       
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTNECEN)                                         
                INTO   (TS-TSTNECEN-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTNECEN-DONNEES)                          
                ITEM   (WP-RANG-TSTNECEN)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
              SET WC-TSTNECEN-SUITE      TO TRUE                                
           WHEN 26                                                              
           WHEN 44                                                              
              SET WC-TSTNECEN-FIN        TO TRUE                                
           WHEN OTHER                                                           
              SET WC-TSTNECEN-ERREUR     TO TRUE                                
           END-EVALUATE.                                                        
       FIN-LECT-TSTNECEN. EXIT.                                                 
      *                                                                         
      *-----------------------------------------------------------------        
       ECRITURE-TSTNECEN SECTION.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         WRITEQ TS                                                       
      *         QUEUE  (WC-ID-TSTNECEN)                                         
      *         FROM   (TS-TSTNECEN-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTNECEN-DONNEES)                          
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
                QUEUE  (WC-ID-TSTNECEN)                                         
                FROM   (TS-TSTNECEN-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTNECEN-DONNEES)                          
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           INITIALIZE TS-TSTNECEN-DONNEES.                                      
       FIN-ECRIT-TSTNECEN. EXIT.                                                
      *                                                                         
       DELETE-TSTNECEN SECTION.                                                 
      ******************************                                            
           EXEC CICS DELETEQ TS                                                 
                QUEUE    (WC-ID-TSTNECEN)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
       FIN-DELETE-TSTNECEN. EXIT.                                               
      *                                                                         
       DECLARE-OPEN-CPARAM  SECTION.                                            
      ******************************                                            
           MOVE FUNC-DECLARE             TO TRACE-SQL-FUNCTION.                 
           MOVE 'RVGA01ZZ'               TO TABLE-NAME.                         
           MOVE 'NECEN '                 TO MODEL-NAME.                         
           EXEC SQL                                                             
                DECLARE CPARAM CURSOR FOR                                       
                  SELECT SOCZP, TRANS, WPARAM, LIBELLE, WFLAG                   
                  FROM RVGA01ZZ                                                 
                  WHERE SOCZP = 'NECEN'                                         
THEMIS*           AND   TRANS IN ('MGV65', 'MEC69')                             
                  AND   WFLAG = 'O'                                             
                  UNION ALL                                                     
                  SELECT SOCZP, TRANS, WPARAM, LIBELLE, WFLAG                   
                  FROM RVGA01ZZ                                                 
                  WHERE SOCZP = 'DISPL'                                         
                  AND   TRANS = 'MEC64'                                         
                  ORDER BY SOCZP, TRANS                                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           MOVE FUNC-OPEN                TO TRACE-SQL-FUNCTION.                 
           EXEC SQL OPEN CPARAM END-EXEC.                                       
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *{ Tr-Empty-Sentence 1.6                                                  
      *    .                                                                    
      *}                                                                        
       FIN-DECLARE-OPEN-CPARAM.  EXIT.                                          
      *                                                                         
       FETCH-CPARAM SECTION.                                                    
           MOVE FUNC-FETCH               TO TRACE-SQL-FUNCTION.                 
           EXEC SQL                                                             
                FETCH CPARAM                                                    
                INTO :XCTRL-SOCZP                                               
                   , :XCTRL-TRANS                                               
                   , :XCTRL-WPARAM                                              
                   , :XCTRL-LIBELLE                                             
                   , :XCTRL-WFLAG                                               
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF TROUVE                                                            
              SET WC-XCTRL-NECEN-TROUVE TO TRUE                                 
           ELSE                                                                 
              SET WC-XCTRL-NECEN-FIN        TO TRUE                             
           END-IF.                                                              
      *                                                                         
       CLOSE-CPARAM SECTION.                                                    
           MOVE FUNC-CLOSE               TO TRACE-SQL-FUNCTION.                 
           EXEC SQL CLOSE CPARAM  END-EXEC.                                     
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      ********************************                                          
                                                                                
