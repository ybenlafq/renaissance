      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQUEX PARAM. P/ EXCEPT  MONOEQUIPAGE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUEX .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUEX .                                                            
      *}                                                                        
           05  EQUEX-CTABLEG2    PIC X(15).                                     
           05  EQUEX-CTABLEG2-REDEF REDEFINES EQUEX-CTABLEG2.                   
               10  EQUEX-NSOCLIEU        PIC X(06).                             
               10  EQUEX-ENTITE          PIC X(07).                             
           05  EQUEX-WTABLEG     PIC X(80).                                     
           05  EQUEX-WTABLEG-REDEF  REDEFINES EQUEX-WTABLEG.                    
               10  EQUEX-TYPEQU          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUEX-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUEX-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUEX-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQUEX-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUEX-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQUEX-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
