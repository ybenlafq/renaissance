      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------*              
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT "QDIFEVSG" *              
      *    LONGUEUR : 107                                        *              
      *----------------------------------------------------------*              
       01  EVSG-ENREG.                                                          
           05  EVSG-NOMETAT          PIC X(08)  VALUE 'QDIFEVSG'.               
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-DATETRT.                                                    
               10  EVSG-DAT-DEBUT.                                              
                   15  EVSG-DAT-DSA  PIC X(04).                                 
                   15  EVSG-DAT-DMM  PIC X(02).                                 
                   15  EVSG-DAT-DJJ  PIC X(02).                                 
               10  FILLER            PIC X(01)  VALUE ';'.                      
               10  EVSG-DAT-FINAL.                                              
                   15  EVSG-DAT-FSA  PIC X(04).                                 
                   15  EVSG-DAT-FMM  PIC X(02).                                 
                   15  EVSG-DAT-FJJ  PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-CLIEUHET         PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-CMARQ            PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-CFAM             PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-NCODIC           PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-LREFFOURN        PIC X(20).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-NLIEUHED         PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-NGHE             PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-CONSTANTE        PIC X(01)  VALUE '1'.                      
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-DENVOI           PIC X(08).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  EVSG-NENVOI           PIC X(07).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
      *                                                                         
      *----------------------------------------------------------*              
                                                                                
