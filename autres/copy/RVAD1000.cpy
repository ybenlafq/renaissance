      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE INT3.RTAD10                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD1000.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD1000.                                                            
      *}                                                                        
           10 AD10-CSOC            PIC X(3).                                    
           10 AD10-LSOC            PIC X(20).                                   
           10 AD10-NSOC            PIC X(03).                                   
           10 AD10-NLIEU           PIC X(03).                                   
           10 AD10-WENVOI          PIC X(01).                                   
           10 AD10-WOPCO           PIC X(01).                                   
           10 AD10-NSEQ            PIC S9(3) COMP-3.                            
           10 AD10-WDROIT          PIC X(01).                                   
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD1000-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD1000-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-CSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD10-CSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-LSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD10-LSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-NSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD10-NSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-NLIEU-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 AD10-NLIEU-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-WENVOI-F        PIC S9(4) COMP.                              
      *--                                                                       
           10 AD10-WENVOI-F        PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-WOPCO-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 AD10-WOPCO-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-NSEQ-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD10-NSEQ-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD10-WDROIT-F        PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 AD10-WDROIT-F        PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
