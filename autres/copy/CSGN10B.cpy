      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * CAHIER BLEU GESTION NATIONALE PRIX GROUPE                               
C0423 ******************************************************************        
      * DE02005 31/03/08 SUPPORT MAINTENANCE                                    
      *                                                                         
      * !!!!! LES LIGNES MARQUEES PAR H!!!! CONTIENNENT                         
      * !!!!! DES CARACTERES NON VISUALISABLES                                  
      * !!!!! NE PAS Y TOUCHER                                                  
************************************************************************        
      * TS AFFICHAGE                                                            
      ******************************************************************        
       EFFACE-TS-GN10B SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-GN10B-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-GN10B-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-GN10B-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-GN10B-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-GN10B SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-GN10B-IDENT)                                        
      *        FROM     (COMM-GN10-TAB-GROUP)                                   
      *        NUMITEMS (TB-GN10B-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-GN10B-IDENT)                                        
               FROM     (COMM-GN10-TAB-GROUP)                                   
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-GN10B-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF COMM-GN10-TAB-GROUP TO DFHB0020                
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-GN10B-IDENT                   
      *dfhei*     COMM-GN10-TAB-GROUP DFHB0020 DFHDUMMY TB-GN10B-NB             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-GN10B-IDENT)
                  NUMITEMS (TB-GN10B-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-GN10B SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-GN10B-IDENT)                                        
      *        INTO     (COMM-GN10-TAB-GROUP)                                   
      *        LENGTH   (LENGTH OF COMM-GN10-TAB-GROUP)                         
      *        ITEM     (TB-GN10B-POS)                                          
      *        NUMITEMS (TB-GN10B-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-GN10B-IDENT)                                        
               INTO     (COMM-GN10-TAB-GROUP)                                   
               LENGTH   (LENGTH OF COMM-GN10-TAB-GROUP)                         
               ITEM     (TB-GN10B-POS)                                          
               NUMITEMS (TB-GN10B-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF COMM-GN10-TAB-GROUP TO DFHB0020                
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-GN10B-IDENT                   
      *dfhei*     COMM-GN10-TAB-GROUP DFHB0020 TB-GN10B-NB TB-GN10B-POS         
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-GN10B SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-GN10B-IDENT)                                        
      *        FROM     (COMM-GN10-TAB-GROUP)                                   
      *        ITEM     (TB-GN10B-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-GN10B-IDENT)                                        
               FROM     (COMM-GN10-TAB-GROUP)                                   
               ITEM     (TB-GN10B-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF COMM-GN10-TAB-GROUP TO DFHB0020                
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-GN10B-IDENT                   
      *dfhei*     COMM-GN10-TAB-GROUP DFHB0020 DFHDUMMY TB-GN10B-POS            
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
                                                                                
                                                                                
