      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CPA2I LIEN PRESTA NASC / PRESTA VTE    *        
      *----------------------------------------------------------------*        
       01  RVCPA2I.                                                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  CPA2I-CTABLEG2    PIC X(15).                                     
           05  CPA2I-CTABLEG2-REDEF REDEFINES CPA2I-CTABLEG2.                   
               10  CPA2I-CPNASC          PIC X(05).                             
               10  CPA2I-DEFFET          PIC X(08).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  CPA2I-DEFFET-N       REDEFINES CPA2I-DEFFET                  
                                         PIC 9(08).                             
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  CPA2I-WTABLEG     PIC X(80).                                     
           05  CPA2I-WTABLEG-REDEF  REDEFINES CPA2I-WTABLEG.                    
               10  CPA2I-CPVENTE         PIC X(05).                             
               10  CPA2I-NVERSION        PIC X(01).                             
               10  CPA2I-DUREEL          PIC X(40).                             
               10  CPA2I-DUREEN          PIC X(02).                             
           EXEC SQL END DECLARE SECTION END-EXEC.
               10  CPA2I-DUREEN-N       REDEFINES CPA2I-DUREEN                  
                                         PIC 9(01)V9(01).                       
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCPA2I-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CPA2I-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CPA2I-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CPA2I-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CPA2I-WTABLEG-F   PIC S9(4) COMP-5.                              
      *}                                                                        
