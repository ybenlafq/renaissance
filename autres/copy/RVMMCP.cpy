      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MMCP  CODES POSTAUX 1000 MERCIS        *        
      *----------------------------------------------------------------*        
       01  RVMMCP  .                                                            
           05  MMCP-CTABLEG2     PIC X(15).                                     
           05  MMCP-CTABLEG2-REDEF  REDEFINES MMCP-CTABLEG2.                    
               10  MMCP-NLIGNE           PIC X(02).                             
           05  MMCP-WTABLEG      PIC X(80).                                     
           05  MMCP-WTABLEG-REDEF   REDEFINES MMCP-WTABLEG.                     
               10  MMCP-CPDEB            PIC X(05).                             
               10  MMCP-CPFIN            PIC X(05).                             
               10  MMCP-WFLAG            PIC X(01).                             
               10  MMCP-COMMENT          PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMMCP-FLAGS.                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMCP-CTABLEG2-F   PIC S9(4)  COMP.                               
      *--                                                                       
           05  MMCP-CTABLEG2-F   PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMCP-WTABLEG-F    PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MMCP-WTABLEG-F    PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
