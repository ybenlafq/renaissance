      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES PASSES EN PARAMETRE POUR LE MBS011                                
       01 WKMBS011.                                                             
           05 WKMBS011-ENTREE.                                                  
              10 WKMBS011-NCODIC        PIC X(7).                       00000220
              10 WKMBS011-DEFFET        PIC X(8).                       00000230
           05 WKMBS011-SORTIE.                                                  
              10 WKMBS011-MONTANT       PIC S9(7)V9(2) COMP-3.          00000250
              10 WKMBS011-MESSAGE.                                              
                 15 WKMBS011-CRET       PIC 9(04).                              
                 15 WKMBS011-LIBERR     PIC X(50).                              
                                                                                
