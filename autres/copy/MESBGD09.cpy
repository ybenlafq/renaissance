      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * - MESBGD09 COPY GENERALISES  29974 OCTETS                      *      13
      *   = 34  DE DEFINITION DE CODE RETOUR                           *        
      *   + 29940 DE MESSAGE REEL                                      *        
      * COPY POUR LE PROGRAMME BGD009 <-> MWD09                        *      13
      ******************************************************************        
              05 MESBGD09-RETOUR.                                               
                 10 MESBGD09-CODE-RETOUR.                                       
                    15  MESBGD09-CODRET  PIC X(1).                              
                         88  MESBGD09-CODRET-OK        VALUE ' '.               
                         88  MESBGD09-CODRET-ERREUR    VALUE '1'.               
                    15  MESBGD09-LIBERR  PIC X(33).                             
              05 MESBGD09-DATA           PIC X(29940).                          
              05 MESBGD09C-DATA REDEFINES MESBGD09-DATA.                        
                 10 MESBGD09-TRANSFERT       PIC X.                             
      * LONGUEUR DE 29000 OCTETS                                                
                 10 MESBGD09-DATA-X          OCCURS 1000.                       
                    15 MESBGD09-NSOCDEPOT    PIC X(3).                          
                    15 MESBGD09-NDEPOT       PIC X(3).                          
                    15 MESBGD09-DRAFALE      PIC X(8).                          
                    15 MESBGD09-NRAFALE      PIC X(3).                          
                    15 MESBGD09-FILLER       PIC X(12).                         
                 10 MESBGD09C-DATA-INUTILE   PIC X(939).                        
                                                                                
