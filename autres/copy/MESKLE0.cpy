      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:07 >
      
      *****************************************************************
      * COMMAREA POUR LE MODULE MKLE0
      *****************************************************************
      * ZONES APPLICATIVES
       01 MESKLE0-MESSAGE.
      *
          02 MESKLE0-ENTETE.
             03 MESKLE0-NCODIC           PIC X(07).
             03 MESKLE0-NB-PX            PIC 9(03).
             03 MESKLE0-NB-MAG           PIC 9(03).
          02 MESKLE0-BLOC-PRIX           OCCURS 300.
             03 MESKLE0-BP-MAG.
                05 MESKLE0-BP-NSOCIETE   PIC X(03).
                05 MESKLE0-BP-NLIEU      PIC X(03).
             03 MESKLE0-BP-DEFFET        PIC X(14).
             03 MESKLE0-BP-PRIX          PIC 9(08).
             03 MESKLE0-BP-PRIME         PIC 9(05).
             03 MESKLE0-BP-PRIX-UNIT     PIC 9(08).
             03 MESKLE0-BP-PRIX-HORSG    PIC 9(08).
             03 MESKLE0-BP-WOA           PIC X(01).
             03 MESKLE0-BP-CAPPRO        PIC X(05).
             03 MESKLE0-BP-QDEFFET       PIC 9(03).
      *
          02 MESKLE0-BLOC-MAG            OCCURS 300.
             03 MESKLE0-BM-MAGASIN.
                05 MESKLE0-BM-NSOC       PIC X(03).
                05 MESKLE0-BM-NLIEU      PIC X(03).
             03 MESKLE0-BM-TYPE-ETQ      PIC X(01).
      *
      
