      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *                                                               *         
      *    TSMTD10 = TS D'EDITION DES ETATS GSM                       *         
      *                                                               *         
      *                                                               *         
      *                                                               *         
      *****************************************************************         
      *                                                               *         
       01  TSMTD10.                                                             
           05 TS-MTD10-NSOCIETE    PIC X(03).                                   
           05 TS-MTD10-NLIEU       PIC X(03).                                   
           05 TS-MTD10-NVENTE      PIC X(07).                                   
           05 TS-MTD10-CTITRENOM   PIC X(05).                                   
           05 TS-MTD10-LNOM        PIC X(25).                                   
           05 TS-MTD10-LPRENOM     PIC X(15).                                   
           05 TS-MTD10-CDOSSIER    PIC X(05).                                   
           05 TS-MTD10-NENTITE     PIC X(07).                                   
           05 TS-MTD10-CAGENT      PIC X(02).                                   
           05 TS-MTD10-CMARQ       PIC X(05).                                   
           05 TS-MTD10-WARI        PIC X(05).                                   
           05 TS-MTD10-DARI        PIC X(08).                                   
           05 TS-MTD10-WARO        PIC X(05).                                   
           05 TS-MTD10-DARO        PIC X(08).                                   
           05 TS-MTD10-DCREATION   PIC X(08).                                   
           05 TS-MTD10-CTYPE       PIC X(05).                                   
           05 TS-MTD10-NDOSSIER    PIC S9(03) COMP-3.                           
           05 TS-MTD10-FILLER      PIC X(20).                                   
                                                                                
