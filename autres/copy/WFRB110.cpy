      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FRB110-RECORD.                                               00000480
      *------------------------------------------ CONSTANTE '0010'      00000480
           05  FRB110-CGRP            PIC X(04).                        00000480
      *-------------------------------- N� DE DOSSIER = N� DE HS        00000480
           05  FRB110-NDOS            PIC X(20).                        00000480
      *-------------------------- CODE PRODUIT = NUMERO DE CODIC        00000480
           05  FRB110-NPRD            PIC X(10).                        00000480
      *--------------------------------- NUMERO DE SERIE ORIGINE        00000480
           05  FRB110-NSERIE          PIC X(20).                        00000480
      *----------------------------------------- DATE EXPEDITION        00000480
           05  FRB110-DEXPED          PIC X(08).                        00000480
      *----------------------------------------- STATUT GARANTIE        00000480
           05  FRB110-CSTTGAR         PIC X(02).                        00000480
      *------------------------------------ DATE MISE EN SERVICE        00000480
           05  FRB110-DSERV           PIC X(08).                        00000480
      *------------------------------------------------ DATE SAV        00000480
           05  FRB110-DSAV            PIC X(08).                        00000480
      *------------------------------------- CODE SYMPTOME PANNE        00000480
           05  FRB110-CSYMPT          PIC X(03).                        00000480
      *--------------------------------------- CODE COMPORTEMENT        00000480
           05  FRB110-CCOMPTM         PIC X(01).                        00000480
      *------------------------------ COMMENTAIRE = N� BL SUR 15        00000480
           05  FRB110-LCOMMENT.                                         00000480
              10  FRB110-LCOMMENT-BL     PIC X(015).                       00000
              10  FRB110-LCOMMENT-TYPEHS PIC X(003).                       00000
              10  FRB110-LCOMMENT-FILLER PIC X(237).                       00000
      *---------------------------- N� EXPEDITION = N� BL SUR 10        00000480
           05  FRB110-NEXPED          PIC X(10).                        00000480
      *----------------------- CODIC PRODUIT DARTY ENVOYE A SAGEM       00000480
           05  FRB110-NPRDENV         PIC X(10).                        00000480
      *-------------------- CODIC PRODUIT DARTY EXPEDIE PAR SAGEM       00000480
           05  FRB110-NPRDEXP         PIC X(10).                        00000480
      *----------------------------------------------------------       00000480
           05  FRB110-FILLER          PIC X(115).                       00000480
      *                                                                 00000600
                                                                                
