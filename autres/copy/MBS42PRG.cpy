      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      ******************************************************************        
      *  F I C H E   D E S C R I P T I V E   D E   P R O G R A M M E   *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : MBS42                                            *        
      *  TITRE      : RECREATION D'UNE VENTE A PARTIR DE L'ARCHIVAGE   *        
      *  FONCTION   : CE MODULE EST APPELE PAR LE PROGRAMME TGV00      *        
      *               LORSQUE L'UTILISATEUR DEMANDE L'OPTION 1 AVEC    *        
      *               UNE VENTE QUI N'EXISTE PAS DANS LE COURANT MAIS  *        
      *               SE TROUVE DANS L'ARCHIVAGE (DANS RTVT01)         *        
      *               (MODIFICATION D'UNE VENTE)                       *        
      *                                                                *        
      *               ON RECHERCHE TOUTES LES DONNEES DE LA VENTE DANS *        
      *               LES TABLES RTVT02 � RTVT15 ET SI TOUT EST OK ON  *        
      *               RECREE LES DONNEES DANS RTGV02, RTGV10, RTGV11   *        
      *               ET RTGV14.                                       *        
      *               SINON ON PASSE UN MESSAGE D'ERREUR A L'UTILISA-  *        
      *               TEUR D'UTILISER L'OPTION 4 DE GV00 (CREATION DE  *        
      *               VENTE INEXISTANTE).                              *        
      ******************************************************************        
      * MODIF 20/01/00 (REPERE 200100) : TENIR COMPTE DES REMISES      *        
      *  ET DES RACHATS PSE LORSQUE ON RECALCULE LE MONTANT, SI CE     *        
      *  DERNIER EST 'FAUX' DANS VT14                                  *        
      ******************************************************************        
      * MODIF 11/03/03 : ADAPTATION DE MGV42 POUR B&S => CREATION MBS42*        
      * AUTEUR : DSA049 - QUADRA-INFORMATIQUE (MH)                     *        
      ******************************************************************        
      * MODIF 07/04/04 : ON AUTORISE LA SORTIE D'ARCHIVAGE DES VENTES  *        
      *                  SANS ADRESSE ( B&S )                          *        
      ******************************************************************        
      * MODIF 20/07/04 : RENSEIGNER LA DATE D'ENCAISSEMENT GV11/VE11   *        
      *                  REPERE DATENC                                 *        
      ******************************************************************        
      * MODIF 09/08/04 : REDONNER LE N� D'ORDRE PACKE POUR LES VENTES  *        
      * REPERE AD01      NON GV                                        *        
      *                  + SELECT * POUR VT05/06/08 CAR FETCH SE FAIT  *        
      *                  DANS RVVT05/06/08                             *        
      ******************************************************************        
      * MODIF 10/08/04 : RECUPERER LE CODE VENDEUR DE LA LIGNE DE      *        
      * REPERE CVEND     VENTE S'IL EXISTE SINON VENDEUR ARCHIVAGE     *        
      ******************************************************************        
      * MODIF 09/09/04 : AUTORISER LA SORTIE D'ARCHIVAGE DEPUIS TD00   *        
      * REPERE MTD03                                                   *        
      ******************************************************************        
      * MODIF 14/09/04 : ZONES MANQUANTES EN SORTIE D'ARCHIVAGE        *        
      * D'OFFRES   . REPERE MPRIM                                      *        
      ******************************************************************        
      * MODIF 19/10/04 : COPY DE RTVT50 DANS RTVT57 PUIS DELETE VT50   *        
      * REPERE VT50                                                    *        
      ******************************************************************        
      * MODIF 26/10/04 : SUITE AU MODIF SUR TGV00 POUR POUVOIR RAPPELER*        
      * UNE VENTE LOCALE ARCHIV�E (YT1)                                *        
      ******************************************************************        
      *  MODIFICATION                                                  *        
      *  DENIS A.   : TYPTR 25/04/2005                                 *        
      *               GESTION DES TYPES DE VENTES ET AUTORISATIONS     *        
      *               DEPUIS SOUS-TABLE TYPTR                          *        
      ******************************************************************        
      *  MODIFICATION (27/07/2005)                                     *        
      *  HV         : GESTION DU WS-CMODDELA / R / ' '                 *        
      *  H50727       DANS LE CAS O� SEULES LES LIGNES D�PENDANTES     *        
      *               SONT ANNUL�ES (PAS LA LIGNE ARTICLE)             *        
      *               LE MODE DE D�LIVRANCE EST � BLANC ET CELA        *        
      *               POSE UN PB DANS LES STATS NMD (LIGNE EN DOUBLE)  *        
      ******************************************************************        
      *  DSA008     : CTRM  26/06/2006                                 *        
      *               GESTION DES AUTORISATIONS REVUE POUR CONTREMARQUE*        
      ******************************************************************        
      *  DSA025     : FACTURATION SAGE 25/09/06 (OA EN COL 1)          *        
      *               DONNER UNE VALEUR A NSEQFV (SPACE = NON FACTURE) *        
      ******************************************************************        
      * AJOUT TRAITEMENT CONTREMARQUE :  TYPVTE = 'E'                  *        
      *     SI CANNULREP =  'A'  =>  LD2  ->  CMOFDEL                  *        
      *                     'R'  =>  RD2  ->  CMOFDEL                  *        
      * JF1006              ' '  =>  LD2  ->  CMOFDEL                  *        
      *-----------------------------------------------------------------        
      *  MM0107 = TYPE DE VENTE MAL TESTE                                       
      *-----------------------------------------------------------------        
      *  DSA008     : GV52  06/03/2009                                 *        
      *               DESCENDRE UNE PHOTO NMD DE LA VENTE SINON ELLE   *        
      *               ENTRERA DE NOUVEAU EN STATS NMD SI ELLE N'EXISTE *        
      *               PLUS EN LOCAL                                    *        
      ******************************************************************        
      *  DSA008     : VT02  01/04/2009                                 *        
      *               AJOUT DES COLONNE DE LA VT02 MANQUANTES          *        
      *               NORDRE , IDCLIENT , CPAYS ET NGSM                *        
      *            + AU TEST RETOUR SQL ON POSITIONNE EXISTE-DEJA POUR *        
      *               TOUS LES RC < 0                                  *        
      ******************************************************************        
M8653 *  M8653      : RECUPERATION DE CFCRED / NCREDI   DANS RTVT14    *        
      * 25/08/2011   POUR ALIMENTER GV10                               *        
      ******************************************************************        
M13813*  J. BICHY   : MODIFICATION DE VENTE POUR RE-EDITION FACTURE    *        
      *               METTRE NSQFV = '00' SI VENTE B2B  SINON SPACE    *        
      ******************************************************************        
V5.0  *  F. T.      : EDITION DE FACTURE DEPUIS EC DARTY.COM           *        
      *               ON ECRIT DANS TS ET PAS DANS LES TABLES          *        
      *               COMME ON VIENT DU MECFC ON CHERCHE DANS LE MBS42 *        
      *               SI LA VENTE EST DANS L'ARCHI OU PAS (AU LIEU DE  *        
      *               COLLER DES SELECTS INNAPROPRIES DANS MECFC)      *        
      ******************************************************************        
      * 02/03/2012 ! POUR VENTE B2B SIEBEL.                            *        
      * SIEBEL     ! RECUPERER VALEUR CTRL B2BEX POUR ALIMENTATION     *        
      *            ! GV10-NAUTO                                        *        
      *            ! IL EUT EXIST� UN MANTIS LE 8674                   *        
      *            ! CONCERNANT LA MODIF DE STRUCTURE DE LA VT01 POUR  *        
      *            ! AJOUTER LE NAUTO DE LA GV10...                    *        
      *            ! COMME IL DATE ET QU'IL EST TOUJOURS EN ATTENTE ET *        
      *            ! QUE JE N'AI AUCUN SCRUPULE...                     *        
      *            ! JE VIRE LES MODIFS COMMENC�ES SUR LE SUJET POUR   *        
      *            ! RAJOUTER MA MODIF SIEBEL.                         *        
      ******************************************************************        
      * 11/02/2014 : AJOUT ACC�S BS01 ET BS03 POUR OFFRE NE CONTENANT  *        
      * C1402        QUE DES SERVICES                                  *        
      *              COMME ON S'ATTEND � AVOIR UN PRODUIT DANS L'OFFRE *        
      *              ET UN CODE OFFRE ON D�SARCHIVE SANS CES DONN�ES   *        
      ******************************************************************        
       01  FILLER                    PIC X(16) VALUE '*** WORKING  ***'.        
       01 P-EIBTASKN PIC S9(7) PACKED-DECIMAL.                                  
       01 X-EIBTASKN REDEFINES P-EIBTASKN PIC X(4).                             
       01  MODE-PAIEMENT             PIC 9(01) VALUE 0.                         
           88 METS-ARF-OUI                     VALUE 1.                         
      *                                                                         
       01  FILLER                    PIC X(16) VALUE '*TOP ETAT OBJET*'.        
       01  LIG-COM-GV10.                                                        
           05  TYP-DONNEE           PIC X(08).                                  
           05  LIB-DONNEE           PIC X(22).                                  
PM     01  WS-SEQPSE                    PIC  9 VALUE 0.                         
PM     01  W-NPSE.                                                              
PM         05  W-NUMPSE   PIC X(7) VALUE SPACE.                                 
PM         05  W-SEQPSE   PIC X    VALUE SPACE.                                 
MTD03  01  WS-TOP-SORTIE             PIC 9                 VALUE ZERO.          
MTD03      88 OK-SORTIE                        VALUE 1.                         
MTD03      88 NON-OK-SORTIE                    VALUE 0.                         
V5.0       88 OK-TS-SORTIE                     VALUE 2.                         
       01  WS-TOP-NCLUSTER           PIC 9                 VALUE ZERO.          
           88 OK-NCLUSTER                      VALUE 1.                         
           88 NON-OK-NCLUSTER                  VALUE 0.                         
       01  WS-TOP-ADRESSE            PIC 9                 VALUE ZERO.          
           88 OK-ADRESSE                       VALUE 1.                         
           88 NON-OK-ADRESSE                   VALUE 0.                         
       01  WS-TOP-ENTETE             PIC 9                 VALUE ZERO.          
           88 OK-ENTETE                        VALUE 1.                         
           88 NON-OK-ENTETE                    VALUE 0.                         
       01  WS-TOP-ARTICLE            PIC 9                 VALUE ZERO.          
           88 OK-ARTICLE                       VALUE 1.                         
           88 NON-OK-ARTICLE                   VALUE 0.                         
       01  WS-TOP-REGLEMENT          PIC 9                 VALUE ZERO.          
           88 OK-REGLEMENT                     VALUE 1.                         
           88 NON-OK-REGLEMENT                 VALUE 0.                         
       01  WS-TOP-MAJ-REGLEMENT      PIC 9                 VALUE ZERO.          
           88 OK-MAJ-REGLEMENT                     VALUE 1.                     
           88 NON-OK-MAJ-REGLEMENT                 VALUE 0.                     
       01  WS-TOP-MAJ-ENTETE         PIC 9                 VALUE ZERO.          
           88 OK-MAJ-ENTETE                        VALUE 1.                     
           88 NON-OK-MAJ-ENTETE                    VALUE 0.                     
       01  WS-TOP-MAJ-ADRESSE        PIC 9                 VALUE ZERO.          
           88 OK-MAJ-ADRESSE                       VALUE 1.                     
           88 NON-OK-MAJ-ADRESSE                   VALUE 0.                     
       01  WS-TOP-MAJ-ARTICLES       PIC 9                 VALUE ZERO.          
           88 OK-MAJ-ARTICLES                      VALUE 1.                     
           88 NON-OK-MAJ-ARTICLES                  VALUE 0.                     
       01  WS-TOP-DOSSIER5           PIC 9                 VALUE ZERO.          
           88 OK-DOSSIER5                      VALUE 1.                         
           88 NON-OK-DOSSIER5                  VALUE 0.                         
       01  WS-TOP-DOSSIER6           PIC 9                 VALUE ZERO.          
           88 OK-DOSSIER6                      VALUE 1.                         
           88 NON-OK-DOSSIER6                  VALUE 0.                         
       01  WS-TOP-DOSSIER8           PIC 9                 VALUE ZERO.          
           88 OK-DOSSIER8                      VALUE 1.                         
           88 NON-OK-DOSSIER8                  VALUE 0.                         
       01  WS-TOP-MAJ-DOSSIER        PIC 9                 VALUE ZERO.          
           88 OK-MAJ-DOSSIER                       VALUE 1.                     
           88 NON-OK-MAJ-DOSSIER                   VALUE 0.                     
       01  WS-TOP-DELETE-ACH         PIC 9                 VALUE ZERO.          
           88 OK-DELETE                            VALUE 1.                     
           88 NON-OK-DELETE                        VALUE 0.                     
       01  WS-TOP-LECTURE-TS         PIC 9                 VALUE ZERO.          
           88 PAS-FIN-TS                           VALUE 1.                     
           88 FIN-TS                               VALUE 0.                     
       01  WS-TOP-ADRESSE-LIVRAISON  PIC 9                 VALUE ZERO.          
           88 ADRESSE-LIVRAISON                    VALUE 1.                     
           88 PAS-ADRESSE-LIVRAISON                VALUE 0.                     
       01  WS-TOP-ADRESSE-REPRISE    PIC 9                 VALUE ZERO.          
           88 ADRESSE-REPRISE                      VALUE 1.                     
           88 PAS-ADRESSE-REPRISE                  VALUE 0.                     
PM    *                                                                         
PM     01  WS-TOP-PSE                PIC 9                 VALUE ZERO.          
PM         88 TOP-PSE                              VALUE 1.                     
PM         88 PAS-PSE                              VALUE 0.                     
PM     01  W-LNOM                    PIC X(25) VALUE SPACES.                    
PM     01  W-CINSEE                  PIC X(05) VALUE SPACES.                    
PM    *                                                                         
       01  WS-CANNULREP              PIC X     VALUE SPACES.                    
       01  WS-NLIGNE                 PIC X(3)  VALUE SPACES.                    
       01  WS-NLCODIC                PIC X(3)  VALUE SPACES.                    
       01  WS-NCLUSTER               PIC X(13) VALUE SPACES.                    
       01  WS-NFLAG                  PIC X(01) VALUE SPACES.                    
       01  WS-NORDRE                 PIC 9(5)  VALUE ZEROES.                    
DA01   01  WS-NORDRE1 REDEFINES WS-NORDRE  PIC  9(9) COMP-3.                    
DA01   01  WS-NORDRE2                PIC 9(9)  VALUE ZEROES.                    
       01  WS-NCODIC                 PIC X(7)  VALUE SPACES.                    
       01  WS-NCODICGRP              PIC X(7)  VALUE SPACES.                    
       01  WS-CMODDEL                PIC X(3)  VALUE SPACES.                    
       01  WS-CMODDELA               PIC X(3)  VALUE SPACES.                    
       01  WS-CMODDELR               PIC X(3)  VALUE SPACES.                    
       01  WS-DFACTURE               PIC X(8)  VALUE SPACES.                    
       01  WS-NSEQ                   PIC 99  VALUE ZEROES.                      
       01  WS-NSEQNQ                 PIC S9(5) COMP-3.                          
       01  WS-NSEQREF                PIC S9(5) COMP-3.                          
       01  WS-NSEQENS1               PIC S9(5) COMP-3.                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  WS-RANGTS                 PIC S9(4) COMP  VALUE +0.                  
      *--                                                                       
       01  WS-RANGTS                 PIC S9(4) COMP-5  VALUE +0.                
      *}                                                                        
       01  WS-AD-LIVR-CPOSTAL        PIC X(5)  VALUE SPACES.                    
       01  WS-AD-REPR-CPOSTAL        PIC X(5)  VALUE SPACES.                    
      *                                                                         
       01  WS-CLE-GV11               PIC X(12) VALUE SPACES.                    
       01  WS-CLE-TS42               PIC X(12) VALUE SPACES.                    
      *                                                                         
V41R0  01  W-TYPE-VENTES             PIC 9(01).                                 
V41R0  01  W-TYPE-VENTEX REDEFINES W-TYPE-VENTES  PIC X(01).                    
       LINKAGE                         SECTION.                                 
      *----------------------------------------                                 
       01  DFHCOMMAREA.                                                         
           05  FILLER                  PIC X(200).                              
      *                                                                         
       PROCEDURE DIVISION.                                                      
      *                                                                         
      *****************************************************************         
      *              T R A M E   DU   P R O G R A M M E               *         
      *****************************************************************         
      *                                                                         
      *                                                                         
       MODULE-MBS42                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           PERFORM MODULE-ENTREE                                                
           PERFORM MODULE-TRAITEMENT.                                           
           PERFORM MODULE-SORTIE.                                               
      *                                                                         
       FIN-MODULE-MBS42. EXIT.                                                  
      *                                                                         
      *                                                                         
       MODULE-ENTREE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           PERFORM INIT-HANDLE.                                                 
           EXEC  CICS  HANDLE  CONDITION                                        
                       ITEMERR (ABANDON-CICS)                                   
                       QIDERR  (ABANDON-CICS)                                   
           END-EXEC                                                             
           MOVE DFHCOMMAREA            TO COMM-BS42-APPLI.                      
      * EFFACEMENT DE LA TS                                                     
           MOVE 'MBS42'                TO NOM-PROG.                             
           MOVE 1 TO RANG-TS                                                    
           PERFORM READ-TSBS42                                                  
           IF TROUVE                                                            
      *{ normalize-exec-xx 1.5                                                  
      *       EXEC CICS DELETEQ TS QUEUE (IDENT-TS) END-EXEC                    
      *--                                                                       
              EXEC CICS DELETEQ TS QUEUE (IDENT-TS)                             
              END-EXEC                                                          
      *}                                                                        
           ELSE                                                                 
              INITIALIZE TS42-DONNEES                                           
           END-IF.                                                              
      * POUR MESSAGES D'ERREUR GENERALISES                                      
           INITIALIZE MESSAGE-VARIABLE.                                         
      *                                                                         
       FIN-MODULE-ENTREE. EXIT.                                                 
      *                                                                         
      *****************************************************************         
      * TRAITEMENT :                                                  *         
      *                                                               *         
      * 1) ON RECHERCHE TOUTES LES DONNEES                            *         
      * 2) SI OK ON INSERE LES DONNEES DANS LES TABLES DE VENTES      *         
      *    SINON ON RENVOIE UN MESSAGE D'ERREUR (PASSER PAR L'OPTION  *         
      *    4 DE GV00)                                                 *         
      * 3) SI INSERTION OK ON EFFACE LES DONNEES DE L'ARCHIVAGE       *         
      *    SINON ON REVIENT EN ARRIERE SUR LES INSERTIONS DEJA        *         
      *          EFFECTUEES                                           *         
      *****************************************************************         
      *                                                                         
       MODULE-TRAITEMENT               SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           PERFORM RECHERCHE-NCLUSTER                                           
           IF OK-NCLUSTER                                                       
M8653         PERFORM SELECT-VT14-CFCRED                                        
              PERFORM RECHERCHE-DONNEES-ENTETE                                  
              IF OK-ENTETE                                                      
                 MOVE 1 TO RANG-TS                                              
                 PERFORM RECHERCHE-DONNEES-ADRESSE                              
                 PERFORM RECHERCHE-DONNEES-REGLEMENT                            
                 PERFORM RECHERCHE-DONNEES-ARTICLE                              
                 PERFORM RECHERCHE-DONNEES-DOSSIER                              
V5.0             IF OK-TS-SORTIE                                                
                    PERFORM ECRITURE-TS-ENTETE                                  
                    MOVE IDENT-TS TO COMM-BS42-IDENT-TS                         
                    SET COMM-BS42-CODRET-TS-OK TO TRUE                          
                 ELSE                                                           
                  IF OK-ARTICLE OR NON-OK-ARTICLE                               
                    PERFORM MISE-A-JOUR-ENTETE                                  
                    IF OK-MAJ-ENTETE                                            
                       IF OK-ADRESSE                                            
                          PERFORM MISE-A-JOUR-ADRESSE                           
                       ELSE                                                     
      *----> LIGNES ADRESSE OBLIGATOIRE UNIQUEMENT DANS LE CAS GV               
B&S   *----> PLUS DE CONTROLE DU TYPE DE VENTE : ON AUTORISE                    
 "    *                                                                         
 "    *                   IF NOT                                                
 "    *                    (VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M')          
 "                           SET OK-MAJ-ADRESSE TO TRUE                         
 "    *                   ELSE                                                  
 "    *                      SET NON-OK-MAJ-ADRESSE TO TRUE                     
 "    *                   END-IF                                                
 "                     END-IF                                                   
                       IF OK-MAJ-ADRESSE                                        
                          IF OK-ARTICLE                                         
                             PERFORM MISE-A-JOUR-ARTICLES                       
                          ELSE                                                  
                             SET OK-MAJ-ARTICLES TO TRUE                        
                          END-IF                                                
                          IF OK-MAJ-ARTICLES                                    
                             PERFORM MISE-A-JOUR-REGLEMENT                      
                             IF OK-MAJ-REGLEMENT                                
                                PERFORM MISE-A-JOUR-DOSSIER                     
                                IF OK-MAJ-DOSSIER                               
                                   PERFORM DELETE-ARCHIVAGE                     
                                   SET COMM-BS42-CODRET-OK TO TRUE              
                                ELSE                                            
                                   SET NON-OK-NCLUSTER         TO TRUE          
                                   SET COMM-BS42-CODRET-ERREUR TO TRUE          
                                   PERFORM DELETE-RTGV10                        
                                   PERFORM DELETE-RTGV11                        
                                   PERFORM DELETE-RTGV14                        
                                   PERFORM DELETE-RTGV02                        
                                   PERFORM DELETE-RTGV05                        
                                   PERFORM DELETE-RTGV06                        
                                   PERFORM DELETE-RTGV08                        
                                END-IF                                          
                             ELSE                                               
                                SET COMM-BS42-CODRET-ERREUR TO TRUE             
                                SET NON-OK-NCLUSTER         TO TRUE             
                                PERFORM DELETE-RTGV10                           
                                PERFORM DELETE-RTGV11                           
                                PERFORM DELETE-RTGV14                           
                                PERFORM DELETE-RTGV02                           
                             END-IF                                             
                          ELSE                                                  
                             PERFORM DELETE-RTGV10                              
                             PERFORM DELETE-RTGV11                              
                             PERFORM DELETE-RTGV02                              
                          END-IF                                                
                       ELSE                                                     
                          PERFORM DELETE-RTGV10                                 
                          PERFORM DELETE-RTGV02                                 
                       END-IF                                                   
                    ELSE                                                        
                       PERFORM DELETE-RTGV10                                    
                    END-IF                                                      
                 END-IF                                                         
V5.0            END-IF                                                          
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-MODULE-TRAITEMENT. EXIT.                                             
      *                                                                         
      *------- RECHERCHE DES DONNEES -------------------------------*           
      *                                                                         
       RECHERCHE-NCLUSTER              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
TYPTR * ON RECHERCHE DANS TYPTR LE TYPE DE VENTE                                
TYPTR * AVEC/SANS ADRESSE HOST/LOCALE.                                          
TYPTR * SORTIE D'ARCHIVAGE AUTORISEE O/N                                        
TYPTR      IF COMM-BS42-TYPVTE > SPACES                                         
TYPTR         MOVE COMM-BS42-TYPVTE TO W-TYPTR-CODE                             
TYPTR         PERFORM RECHERCHE-TYPTR                                           
TYPTR      END-IF                                                               
V5.0       IF COMM-BS42-PGAPPEL = 'MECFC' OR 'MEC31'                            
              PERFORM SELECT-VT01                                               
           END-IF                                                               
           PERFORM SELECT-NCLUSTER                                              
           IF TROUVE                                                            
TYPTR         IF COMM-BS42-TYPVTE NOT > SPACES                                  
TYPTR            MOVE VT01-NTYPVENTE TO W-TYPTR-CODE                            
TYPTR            IF   VT01-NTYPVENTE NOT > SPACES                               
TYPTR               SET W-VTE-HOST TO TRUE                                      
TYPTR               SET W-ARCHIV-AUTORISE TO TRUE                               
TYPTR            ELSE                                                           
TYPTR               PERFORM RECHERCHE-TYPTR                                     
TYPTR            END-IF                                                         
TYPTR         END-IF                                                            
              MOVE VT01-NCLUSTER TO WS-NCLUSTER                                 
              MOVE VT01-NFLAG    TO WS-NFLAG                                    
              SET OK-NCLUSTER TO TRUE                                           
           END-IF.                                                              
           IF NON-TROUVE                                                        
      *             VENTE NON TROUVEE DANS L'ARCHIVAGE                          
              SET NON-OK-NCLUSTER TO TRUE                                       
              MOVE '0001'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           END-IF.                                                              
AVANT *    IF TROUVE AND ((COMM-BS42-PGAPPEL = 'MBS60'                          
AVANT *        AND (VT01-NTYPVENTE = 'G' OR ' ' OR 'M') )                       
AVANT *        OR (COMM-BS42-PGAPPEL NOT = 'MBS60'                              
MTD03 * ON AUTORISE LA SORTIE D'ARCHIVAGE DEPUIS TD00                           
MTD03 *            AND COMM-BS42-PGAPPEL NOT = 'MTD03'                          
AVANT *            AND NOT (VT01-NTYPVENTE = 'G' OR ' ' OR 'M') ))              
APRES      IF TROUVE                                                            
              SET OK-SORTIE TO TRUE                                             
              EVALUATE COMM-BS42-PGAPPEL                                        
MBS60 * ON INTERDIT LA SORTIE D'ARCHIVAGE DEPUIS LE RAPPEL                      
MBS60 * DE VENTES DE TYPES 'G', ' ' OU 'M'                                      
                WHEN 'MBS60'                                                    
TYPTR *          IF (VT01-NTYPVENTE = 'G' OR ' ' OR 'M')                        
CTRM  *          IF (W-VTE-HOST)                                                
CTRM  *          ON AUTORISE LA SORTIE D'ARCHIVAGE DE TOUTES                    
CTRM  *          LES VENTES PAR MBS60 CAR IL AUTORISE LUI MEME                  
CTRM  *          LE RAPPEL DE CES VENTES ( EXEMPLE TYPE 'E' )                   
CTRM  *             SET NON-OK-SORTIE TO TRUE                                   
                    SET OK-SORTIE TO TRUE                                       
CTRM  *          END-IF                                                         
MTD03 * ON AUTORISE LA SORTIE D'ARCHIVAGE DEPUIS TD00                           
MTD03 * TOUS TYPES DE VENTES                                                    
                WHEN 'MTD03'                                                    
                    SET OK-SORTIE TO TRUE                                       
V5.0  * ON AUTORISE LA LECTURE D'ARCHIVAGE DEPUIS EC DARTY.COM                  
      * ON ECRIT EN TS, ON NE SORT PAS LA VENTE DE L'ARCHIVAGE                  
      * TOUS TYPES DE VENTES                                                    
                WHEN 'MECFC'                                                    
                    SET OK-TS-SORTIE TO TRUE                                    
AUTRE * ON INTERDIT LA SORTIE D'ARCHIVAGE POUR LES AUTRES PROGRAMMES            
AUTRE * DE VENTES DE TYPES NON 'G', ' ' OU 'M'                                  
                WHEN OTHER                                                      
YT1   *          IF NOT (VT01-NTYPVENTE = 'G' OR ' ' OR 'M')                    
YT1   *TYPTR     IF NOT (VT01-NTYPVENTE = 'G' OR ' ' OR 'M'                     
YT1   *TYPTR                           OR 'A' OR 'B' OR 'L')                    
TYPTR            IF (W-VTE-TYPE-INCONNU                                         
TYPTR                OR  W-TYPE-INCONNU )                                       
                    SET NON-OK-SORTIE TO TRUE                                   
                 END-IF                                                         
              END-EVALUATE                                                      
           END-IF.                                                              
TYPTR      IF W-ARCHIV-AUTORISE-N                                               
TYPTR         SET NON-OK-SORTIE TO TRUE                                         
TYPTR      END-IF                                                               
           IF NON-OK-SORTIE                                                     
      *  SORTIE D'ARCHIVAGE INTERDITE POUR CE TYPE DE VENTE                     
              SET NON-OK-NCLUSTER TO TRUE                                       
              MOVE '0002'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           END-IF.                                                              
      *  RECHERCHE SI VENTE MGI ( CMODPAIMT = '777')                            
      *  SI TROUVE :                                                            
      *  SORTIE D'ARCHIVAGE INTERDITE POUR CE TYPE DE VENTE                     
      *    IF TROUVE AND OK-NCLUSTER                                            
      *       MOVE VT01-NCLUSTER TO VT14-NCLUSTER                               
      *       MOVE VT01-NFLAG    TO VT14-NFLAG                                  
      *       PERFORM SELECT-VT14-PAI777                                        
      *       IF TROUVE                                                         
      *  SORTIE D'ARCHIVAGE INTERDITE POUR CE TYPE DE VENTE                     
      *          SET NON-OK-NCLUSTER TO TRUE                                    
      *          MOVE '0002'                TO GA99-NSEQERR                     
      *          PERFORM MLIBERRGEN                                             
      *          MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                    
      *          SET  COMM-BS42-CODRET-ERREUR  TO TRUE                          
      *       END-IF                                                            
      *    END-IF.                                                              
      *                                                                         
       FIN-RECHERCHE-NCLUSTER. EXIT.                                            
      *                                                                         
       RECHERCHE-DONNEES-ENTETE        SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE RVGV1005 RVVE1000.                                        
              MOVE COMM-BS42-NSOCIETE    TO GV10-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV10-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV10-NVENTE                         
YT1   *       IF VT01-NTYPVENTE = 'G' OR ' ' OR 'M'                             
YT1   *       IF VT01-NTYPVENTE = 'G' OR ' ' OR 'M' OR 'B' OR 'L'               
YT1   *TYPTR  IF VT01-NTYPVENTE = 'G' OR ' ' OR 'M'                             
YT1           IF W-VTE-HOST                                                     
                  PERFORM CALCUL-NO-ORDRE                                       
              ELSE                                                              
                  MOVE GV10-NVENTE TO WS-NORDRE2                                
                  MOVE WS-NORDRE2      TO WS-NORDRE1                            
              END-IF                                                            
              MOVE         WS-NORDRE        TO GV10-NORDRE                      
              MOVE         SPACES           TO GV10-NCLIENT                     
              MOVE         VT01-DVENTE      TO GV10-DVENTE                      
              MOVE         SPACES           TO GV10-DHVENTE                     
              MOVE         SPACES           TO GV10-DMVENTE                     
              MOVE         0                TO GV10-PTTVENTE                    
              MOVE         0                TO GV10-PVERSE                      
              MOVE         0                TO GV10-PCOMPT                      
              MOVE         0                TO GV10-PLIVR                       
              MOVE         0                TO GV10-PDIFFERE                    
              MOVE         0                TO GV10-PRFACT                      
              MOVE         '1    '          TO GV10-CREMVTE                     
              MOVE         0                TO GV10-PREMVTE                     
              MOVE         SPACES           TO GV10-CORGORED                    
              MOVE         SPACES           TO GV10-LCOMVTE1                    
              MOVE         SPACES           TO GV10-LCOMVTE2                    
              MOVE         SPACES           TO GV10-LCOMVTE3                    
              MOVE         SPACES           TO GV10-LCOMVTE4                    
              MOVE         SPACES           TO GV10-DMODIFVTE                   
              MOVE         'N'              TO GV10-WFACTURE                    
              MOVE         'N'              TO GV10-WEXPORT                     
              MOVE         'N'              TO GV10-WDETAXEC                    
              MOVE         'N'              TO GV10-WDETAXEHC                   
              MOVE         SPACES           TO GV10-LDESCRIPTIF1                
              MOVE         SPACES           TO GV10-LDESCRIPTIF2                
              MOVE         SPACES           TO GV10-DLIVRBL                     
              MOVE         SPACES           TO GV10-NFOLIOBL                    
              MOVE         SPACES           TO GV10-LAUTORM                     
              MOVE         SPACES           TO GV10-NAUTORD                     
              MOVE    VT01-DVENTE(5:4)      TO GV10-DSTAT                       
              MOVE         SPACES           TO WS-DFACTURE                      
              PERFORM CHARGEMENT-DSYST                                          
              MOVE         DATHEUR          TO GV10-DSYST                       
              MOVE         SPACES           TO GV10-CACID                       
              MOVE         SPACES           TO GV10-NSOCMODIF                   
              MOVE         SPACES           TO GV10-NLIEUMODIF                  
              MOVE         SPACES           TO GV10-NSOCP                       
              MOVE         SPACES           TO GV10-NLIEUP                      
              MOVE         SPACES           TO GV10-CDEV                        
M8653 *       MOVE         SPACES           TO GV10-CFCRED                      
"     *       MOVE         SPACES           TO GV10-NCREDI                      
"             MOVE  VT14-CFCRED             TO GV10-CFCRED                      
M8653         MOVE  VT14-NCREDI             TO GV10-NCREDI                      
              MOVE         SPACES           TO GV10-VTEGPE                      
              MOVE         SPACES           TO GV10-CTRMRQ                      
              MOVE         SPACES           TO GV10-DATENC                      
              MOVE         SPACES           TO GV10-WDGRAD                      
              MOVE         SPACES           TO GV10-NAUTO                       
      *       MOVE  VT01-NAUTO              TO GV10-NAUTO                       
              MOVE         SPACES           TO GV10-NSOCO                       
              MOVE         SPACES           TO GV10-NLIEUO                      
              MOVE         SPACES           TO GV10-NVENTO                      
              MOVE  VT01-NTYPVENTE          TO GV10-TYPVTE                      
              IF    VT01-NTYPVENTE NOT > SPACES                                 
              OR    VT01-NTYPVENTE = 'M'                                        
                 MOVE  'G'  TO GV10-TYPVTE                                      
              END-IF                                                            
              PERFORM SELECT-MAX-NSEQ                                           
              MOVE WS-NSEQNQ                TO GV10-NSEQNQ                      
              MOVE WS-NSEQENS1              TO GV10-NSEQENS.                    
      *                                                                         
              SET OK-ENTETE TO TRUE.                                            
      *{ Tr-Empty-Sentence 1.6                                                  
      *       .                                                                 
      *}                                                                        
      *                                                                         
       FIN-RECHERCHE-DONNEES-ENTETE. EXIT.                                      
      *                                                                         
V5.0   ECRITURE-TS-ENTETE              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE TS42-DONNEES.                                             
              MOVE 'GV10' TO TS42-TABLE                                         
              MOVE  COMM-BS42-NSOCIETE    TO TS42-NSOCIETE                      
              MOVE  COMM-BS42-NLIEU       TO TS42-NLIEU                         
              MOVE  COMM-BS42-NVENTE      TO TS42-NVENTE                        
              MOVE  GV10-DVENTE           TO TS42-E-DVENTE                      
              MOVE  GV10-PTTVENTE         TO TS42-E-PTTVENTE                    
              MOVE  GV10-PVERSE           TO TS42-E-PVERSE                      
              MOVE  GV10-PCOMPT           TO TS42-E-PCOMPT                      
              MOVE  GV10-PLIVR            TO TS42-E-PLIVR                       
              MOVE  GV10-PDIFFERE         TO TS42-E-PDIFFERE                    
              MOVE  GV10-PRFACT           TO TS42-E-PRFACT                      
              MOVE  GV10-WFACTURE         TO TS42-E-WFACTURE                    
              MOVE  GV10-WEXPORT          TO TS42-E-WEXPORT                     
              MOVE  GV10-WDETAXEC         TO TS42-E-WDETAXEC                    
              MOVE  GV10-WDETAXEHC        TO TS42-E-WDETAXEHC                   
              MOVE  GV10-DSTAT            TO TS42-E-DSTAT                       
              MOVE  GV10-CFCRED           TO TS42-E-CFCRED                      
              MOVE  GV10-NCREDI           TO TS42-E-NCREDI                      
              MOVE  GV10-NAUTO            TO TS42-E-NAUTO                       
              MOVE  GV10-TYPVTE           TO TS42-E-TYPVTE                      
              MOVE  GV10-NSEQNQ           TO TS42-E-NSEQNQ                      
              MOVE  GV10-NSEQENS          TO TS42-E-NSEQENS                     
              MOVE  GV10-WDETAXEC         TO TS42-E-WDETAXEC                    
              MOVE  GV10-WEXPORT          TO TS42-E-WEXPORT                     
              MOVE  GV10-LCOMVTE1         TO TS42-E-LCOMVTE1                    
              MOVE  GV10-LCOMVTE2         TO TS42-E-LCOMVTE2                    
              MOVE  GV10-LCOMVTE3         TO TS42-E-LCOMVTE3                    
              MOVE  GV10-LCOMVTE4         TO TS42-E-LCOMVTE4                    
              PERFORM ECRITURE-LIGNE-TS                                         
              .                                                                 
      *                                                                         
       FIN-ECRITURE-TS-ENTETE. EXIT.                                            
      *                                                                         
      *                                                                         
       RECHERCHE-DONNEES-ADRESSE       SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE TS42-DONNEES.                                             
           PERFORM DECLARE-RTVT02.                                              
           PERFORM FETCH-RTVT02.                                                
      * MODIF 07/04/04 : OK MEME SI NON TROUVE, SERA DEMANDE DANS GV            
      * LE TOP SERT A NE PAS PASSER DANS LE PAVE MISE-A-JOUR-ADRESSE            
B&S   *    IF NON-TROUVE                                                        
B&S   *       AND  (VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M')                  
 "    *            ADRESSE NON TROUVEE DANS L'ARCHIVAGE                         
 "    *        MOVE '0006'                TO GA99-NSEQERR                       
 "    *        PERFORM MLIBERRGEN                                               
 "    *        MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                      
 "    *        SET  COMM-BS42-CODRET-ERREUR  TO TRUE                            
 "         IF NON-TROUVE                                                        
 "            SET NON-OK-ADRESSE TO TRUE                                        
 "         ELSE                                                                 
 "            SET OK-ADRESSE TO TRUE                                            
B&S        END-IF                                                               
           PERFORM UNTIL NON-TROUVE                                             
              MOVE 'GV02' TO TS42-TABLE                                         
              MOVE  COMM-BS42-NSOCIETE    TO TS42-NSOCIETE                      
              MOVE  COMM-BS42-NLIEU       TO TS42-NLIEU                         
              MOVE  COMM-BS42-NVENTE      TO TS42-NVENTE                        
              MOVE         VT02-WTYPEADR  TO TS42-WTYPEADR                      
              MOVE         VT02-CTITRENOM TO TS42-CTITRENOM                     
              MOVE         VT02-LNOM      TO TS42-LNOM                          
              MOVE         VT02-LPRENOM   TO TS42-LPRENOM                       
              MOVE         VT02-LBATIMENT TO TS42-LBATIMENT                     
              MOVE         VT02-LESCALIER TO TS42-LESCALIER                     
              MOVE         VT02-LETAGE    TO TS42-LETAGE                        
              MOVE         VT02-LPORTE    TO TS42-LPORTE                        
              MOVE         VT02-LCMPAD1   TO TS42-LCOMPAD1                      
              MOVE         VT02-CVOIE     TO TS42-CVOIE                         
              MOVE         VT02-CTVOIE    TO TS42-CTVOIE                        
              MOVE         VT02-LNOMVOIE  TO TS42-LNOMVOIE                      
              MOVE         VT02-LCOMMUNE  TO TS42-LCOMMUNE                      
              MOVE         VT02-CPOSTAL   TO TS42-CPOSTAL                       
              MOVE         VT02-TELDOM    TO TS42-TELDOM                        
              MOVE         VT02-TELBUR    TO TS42-TELBUR                        
              MOVE         VT02-WEXPTAXE  TO TS42-WEXPTAXE                      
              MOVE         VT02-NORDRE    TO TS42-NORDRE                        
              MOVE         VT02-IDCLIENT  TO TS42-IDCLIENT                      
              MOVE         VT02-CPAYS     TO TS42-CPAYS                         
              MOVE         VT02-NGSM      TO TS42-NGSM                          
              PERFORM ECRITURE-LIGNE-TS                                         
              IF VT02-WTYPEADR = 'A'                                            
                IF VT02-WEXPTAXE = '1'                                          
                   MOVE 'O' TO GV10-WEXPORT                                     
                ELSE                                                            
                    IF VT02-WEXPTAXE = '2'                                      
                       MOVE 'O' TO GV10-WDETAXEC                                
                    END-IF                                                      
                    IF VT02-WEXPTAXE = '3'                                      
                       MOVE 'O' TO GV10-WDETAXEC                                
                       MOVE 'O' TO GV10-WEXPORT                                 
                    END-IF                                                      
                END-IF                                                          
              END-IF                                                            
              PERFORM FETCH-RTVT02                                              
           END-PERFORM.                                                         
      *                                                                         
       FIN-RECHERCHE-DONNEES-ADRESSE. EXIT.                                     
      *                                                                         
       RECHERCHE-DONNEES-REGLEMENT     SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE RVGV1404 RVVE1400.                                        
           PERFORM DECLARE-RTVT14.                                              
           PERFORM FETCH-RTVT14.                                                
           IF NON-TROUVE                                                        
      *          REGLEMENT NON TROUVEE DANS L'ARCHIVAGE                         
              SET NON-OK-REGLEMENT TO TRUE                                      
           ELSE                                                                 
              SET OK-REGLEMENT TO TRUE                                          
           END-IF                                                               
           PERFORM UNTIL NON-TROUVE                                             
              MOVE  'GV14'   TO TS42-TABLE                                      
              MOVE  COMM-BS42-NSOCIETE    TO TS42-NSOCIETE                      
              MOVE  COMM-BS42-NLIEU       TO TS42-NLIEU                         
              MOVE  COMM-BS42-NVENTE      TO TS42-NVENTE                        
              MOVE         VT14-NSEQ      TO TS42-NSEQ                          
              MOVE         VT14-DSAISIE   TO TS42-DSAISIE                       
              MOVE         VT14-CMODPAIMT TO TS42-CMODPAIMT                     
              MOVE         VT14-PREGLTVTE TO TS42-PREGLTVTE                     
              MOVE         VT14-NREGLTVTE TO TS42-NREGLTVTE                     
              MOVE         VT14-DREGLTVTE TO TS42-DREGLTVTE                     
              MOVE         VT14-WREGLTVTE TO TS42-WREGLTVTE                     
              MOVE         VT14-NSOCP     TO TS42-NSOCP                         
              MOVE         VT14-NLIEUP    TO TS42-NLIEUP                        
              MOVE         VT14-NTRANS    TO TS42-NTRANS                        
              MOVE         VT14-CVENDEUR  TO TS42-CVENDEUR                      
              MOVE         VT14-CFCRED    TO TS42-CFCRED                        
              MOVE         VT14-NCREDI    TO TS42-NCREDI                        
              ADD VT14-PREGLTVTE          TO GV10-PTTVENTE                      
              EVALUATE VT14-WREGLTVTE                                           
                WHEN 'D'                                                        
                   ADD VT14-PREGLTVTE     TO GV10-PDIFFERE                      
                   MOVE VT14-CMODPAIMT    TO GV10-CORGORED                      
M8653              MOVE VT14-CFCRED       TO GV10-CFCRED                        
"                  MOVE VT14-NCREDI       TO GV10-NCREDI                        
                WHEN 'R'                                                        
                   ADD VT14-PREGLTVTE   TO GV10-PRFACT                          
      * NE PAS METTRE UN DEUXIEME FACTURE LORSQUE DEJA ENVOYEE                  
                   MOVE GV10-DVENTE     TO WS-DFACTURE                          
AL0906* PERMETTRE LA SAISIE DE L'ARF SANS INTERVENTION                          
     |* POUR LES VENTES FAITES AVANT LE 15 04 2008                              
     |             IF GV10-DVENTE <= '20080415'                                 
     |                SET METS-ARF-OUI TO TRUE                                  
     |             END-IF                                                       
                WHEN 'L'                                                        
      * DEJA LIVRE --> METTRE TOUT AU COMPTANT                                  
                   ADD VT14-PREGLTVTE   TO GV10-PCOMPT                          
                WHEN 'V'                                                        
                   ADD VT14-PREGLTVTE   TO GV10-PCOMPT                          
                WHEN  OTHER                                                     
                   CONTINUE                                                     
               END-EVALUATE                                                     
               PERFORM ECRITURE-LIGNE-TS                                        
               PERFORM FETCH-RTVT14                                             
           END-PERFORM.                                                         
      *                                                                         
       FIN-RECH-DONNEES-REGLEMENT. EXIT.                                        
      *                                                                         
      *                                                                         
       RECHERCHE-DONNEES-ARTICLE       SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           INITIALIZE RVGV1106 RVVE1100.                                        
           PERFORM DECLARE-ARTICLES.                                            
           PERFORM FETCH-ARTICLES.                                              
           IF NON-TROUVE                                                        
      *          ARTICLE OU PRESTATION NON TROUVE(E) DANS L'ARCHIVAGE           
               MOVE '0003'                TO GA99-NSEQERR                       
               PERFORM MLIBERRGEN                                               
               MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                      
               SET  COMM-BS42-CODRET-ERREUR  TO TRUE                            
               SET NON-OK-ARTICLE TO TRUE                                       
           ELSE                                                                 
               SET OK-ARTICLE TO TRUE                                           
           END-IF                                                               
      *    LIGNES ARTICLES                                                      
           PERFORM UNTIL NON-TROUVE                                             
              IF  GV11-CTYPENREG = '2'                                          
                    OR GV11-CTYPENREG = '3'                                     
H50727        OR  (GV11-CTYPENREG = '4' AND WS-NLCODIC NOT = '   ')             
                 MOVE WS-NCODIC    TO GV11-NCODIC                               
                 MOVE WS-NCODICGRP TO GV11-NCODICGRP                            
                 IF  WS-CANNULREP = 'A'                                         
H50727              IF WS-CMODDELA NOT = SPACE                                  
H50727                 MOVE WS-CMODDELA TO GV11-CMODDEL                         
H50727              ELSE                                                        
H50727                 MOVE WS-CMODDEL  TO GV11-CMODDEL                         
H50727              END-IF                                                      
                 END-IF                                                         
                 IF  WS-CANNULREP = 'R'                                         
H50727              IF WS-CMODDELR NOT = SPACE                                  
H50727                 MOVE WS-CMODDELR TO GV11-CMODDEL                         
H50727              ELSE                                                        
H50727                 IF WS-CMODDEL(1:1) = 'E'                                 
H50727                    MOVE 'RM '    TO GV11-CMODDEL                         
H50727                 ELSE                                                     
H50727                    MOVE 'RD2'    TO GV11-CMODDEL                         
H50727                 END-IF                                                   
H50727              END-IF                                                      
                 END-IF                                                         
                 IF  WS-CANNULREP = ' '                                         
                    MOVE WS-CMODDEL  TO GV11-CMODDEL                            
                 END-IF                                                         
              END-IF                                                            
              IF  GV11-CTYPENREG = '4' AND WS-NLCODIC  = '   '                  
C1402 * -> RECHERCHE CODE OFFRE SI NE CONTIENT QUE DES SERVICES                 
                 IF GV11-NLIEN > 0 AND GV11-NCODICGRP <= SPACES                 
                    PERFORM CHERCHE-OFFRE                                       
                 END-IF                                                         
                 IF  WS-CANNULREP = 'A'                                         
                    MOVE 'EMR'       TO GV11-CMODDEL                            
                 END-IF                                                         
                 IF  WS-CANNULREP = 'R'                                         
                    MOVE 'RM '       TO GV11-CMODDEL                            
                 END-IF                                                         
                 IF  WS-CANNULREP = ' '                                         
                    MOVE 'EMR'       TO GV11-CMODDEL                            
                 END-IF                                                         
MM0107           IF  VT01-NTYPVENTE = 'E'                                       
                    IF  WS-CANNULREP = 'A'                                      
                       MOVE 'LD2'       TO GV11-CMODDEL                         
  !!                END-IF                                                      
  !!                IF  WS-CANNULREP = 'R'                                      
  !!                   MOVE 'RD2'       TO GV11-CMODDEL                         
  !!                END-IF                                                      
  !!                 IF  WS-CANNULREP = ' '                                     
  !!                    MOVE 'LD2'       TO GV11-CMODDEL                        
  !!                 END-IF                                                     
JF1006           END-IF                                                         
              END-IF                                                            
              STRING                                                            
                     GV11-CTYPENREG GV11-NCODIC 'GV11'                          
                     DELIMITED BY SIZE INTO WS-CLE-GV11                         
              MOVE RANG-TS               TO WS-RANGTS                           
              MOVE 1 TO RANG-TS                                                 
              MOVE 1 TO WS-NSEQ                                                 
              PERFORM READ-TSBS42                                               
              PERFORM UNTIL  FIN-TS                                             
                 STRING                                                         
                        TS42-CTYPENREG TS42-NCODIC TS42-TABLE                   
                     DELIMITED BY SIZE INTO WS-CLE-TS42                         
                 IF WS-CLE-GV11 = WS-CLE-TS42                                   
                    ADD 1 TO WS-NSEQ                                            
                 END-IF                                                         
                 PERFORM READ-TSBS42                                            
              END-PERFORM                                                       
              INITIALIZE TS42-DONNEES                                           
              MOVE   COMM-BS42-NSOCIETE    TO TS42-NSOCIETE                     
              MOVE   COMM-BS42-NLIEU       TO TS42-NLIEU                        
              MOVE   COMM-BS42-NVENTE      TO TS42-NVENTE                       
              MOVE   WS-RANGTS TO RANG-TS                                       
              MOVE   'GV11'  TO   TS42-TABLE                                    
              MOVE   GV11-CTYPENREG  TO   TS42-CTYPENREG                        
              MOVE   GV11-NCODIC     TO   TS42-NCODIC                           
              MOVE   GV11-NCODICGRP  TO   TS42-NCODICGRP                        
              MOVE   WS-NSEQ         TO   TS42-NSEQA                            
              MOVE   GV11-CMODDEL    TO   TS42-CMODDEL                          
              MOVE   GV11-DDELIV     TO   TS42-DDELIV                           
              MOVE   GV11-CENREG     TO   TS42-CENREG                           
              MOVE   GV11-QVENDUE    TO   TS42-QVENDUE                          
              MOVE   GV11-PVUNIT     TO   TS42-PVUNIT                           
              MOVE   GV11-PVUNITF    TO   TS42-PVUNITF                          
CVEND         MOVE   GV11-CVENDEUR   TO   TS42-NVENDEUR                         
              MOVE   GV11-NSEQNQ     TO   TS42-NSEQNQ                           
              MOVE   GV11-NSEQREF    TO   TS42-NSEQREF                          
MPRIM         MOVE   GV11-NSEQENS    TO   TS42-NSEQENS                          
  "           MOVE   GV11-CTYPENT    TO   TS42-CTYPENT                          
  "           MOVE   GV11-NLIEN      TO   TS42-NLIEN                            
  "           MOVE   GV11-NACTVTE    TO   TS42-NACTVTE                          
MPRIM         MOVE   GV11-MPRIMECLI  TO   TS42-MPRIMECLI                        
              IF GV11-PVTOTAL = 0                                               
               COMPUTE GV11-PVTOTAL = GV11-PVUNIT * GV11-QVENDUE                
              END-IF                                                            
              MOVE   GV11-PVTOTAL    TO   TS42-PVTOTAL                          
              IF WS-NLIGNE NOT NUMERIC                                          
                 MOVE WS-NLIGNE (1:2) TO  TS42-NLIGNE                           
              ELSE                                                              
                 MOVE WS-NLIGNE (2:2) TO  TS42-NLIGNE                           
              END-IF                                                            
              MOVE   GV11-TAUXTVA    TO   TS42-TAUXTVA                          
              MOVE   GV11-QCONDT     TO   TS42-QCONDT                           
              MOVE   GV11-LCOMMENT   TO   TS42-LCOMMENT                         
              MOVE   GV11-PVCODIG    TO   TS42-PVCODIG                          
              MOVE   GV11-QTCODIG    TO   TS42-QTCODIG                          
              IF  WS-CANNULREP = 'A'                                            
                 MOVE VT01-DVENTE TO TS42-DANNULATION                           
              END-IF                                                            
              IF  GV11-CTYPENREG = '1' AND GV11-CENREG = '     '                
                 MOVE GV11-NCODIC    TO WS-NCODIC                               
                 MOVE GV11-NCODICGRP TO WS-NCODICGRP                            
                 IF  WS-CANNULREP = 'A'                                         
                    MOVE GV11-CMODDEL TO WS-CMODDELA                            
                 END-IF                                                         
                 IF  WS-CANNULREP = 'R'                                         
                    MOVE GV11-CMODDEL TO WS-CMODDELR                            
                 END-IF                                                         
                 IF  WS-CANNULREP = ' '                                         
                    MOVE GV11-CMODDEL TO WS-CMODDEL                             
                 END-IF                                                         
              END-IF                                                            
      * AU CAS OU ON A PAS TROUVE DE LIGNE REGLEMENT --> DONNEES GV10           
              IF  NON-OK-REGLEMENT                                              
200100          IF  GV11-CTYPENREG = '2'                                        
  "              OR GV11-CTYPENREG  = '1' AND GV11-CENREG > ' '                 
  "              SUBTRACT GV11-PVTOTAL FROM GV10-PTTVENTE                       
  "              SUBTRACT GV11-PVTOTAL FROM GV10-PCOMPT                         
  "             ELSE                                                            
                 ADD GV11-PVTOTAL TO GV10-PTTVENTE                              
                 ADD GV11-PVTOTAL TO GV10-PCOMPT                                
  "             END-IF                                                          
              END-IF                                                            
               PERFORM ECRITURE-LIGNE-TS                                        
               PERFORM FETCH-ARTICLES                                           
           END-PERFORM.                                                         
      *                                                                         
       FIN-RECHERCHE-DONNEES-ARTICLE. EXIT.                                     
      *                                                                         
      *                                                                         
      *                                                                         
       RECHERCHE-DONNEES-DOSSIER       SECTION.                                 
      *----------------------------------------                                 
      *------------------TABLE RTVT05-------------------------*                 
           INITIALIZE TS42-DONNEES.                                             
           PERFORM DECLARE-RTVT05.                                              
           PERFORM FETCH-RTVT05.                                                
           IF TROUVE                                                            
              SET OK-DOSSIER5     TO TRUE                                       
           ELSE                                                                 
              SET NON-OK-DOSSIER5 TO TRUE                                       
           END-IF.                                                              
           PERFORM UNTIL NON-TROUVE                                             
              MOVE 'GV05'                 TO TS42-TABLE                         
              MOVE  COMM-BS42-NSOCIETE    TO TS42-NSOCIETE                      
              MOVE  COMM-BS42-NLIEU       TO TS42-NLIEU                         
              MOVE  COMM-BS42-NVENTE      TO TS42-NVENTE                        
              MOVE         VT05-NDOSSIER  TO TS42-NDOSSIER5                     
              MOVE         VT05-CDOSSIER  TO TS42-CDOSSIER                      
              MOVE         VT05-CTYPENT   TO TS42-CTYPENT5                      
              MOVE         VT05-NENTITE   TO TS42-NENTITE                       
              MOVE         VT05-NSEQENS   TO TS42-NSEQENS5                      
              MOVE         VT05-CAGENT    TO TS42-CAGENT                        
              MOVE         VT05-DCREATION TO TS42-DCREATION                     
              MOVE         VT05-DMODIF    TO TS42-DMODIF5                       
              MOVE         VT05-DANNUL    TO TS42-DANNUL                        
              PERFORM ECRITURE-LIGNE-TS                                         
              PERFORM FETCH-RTVT05                                              
           END-PERFORM.                                                         
      *------------------TABLE RTVT06-------------------------*                 
           INITIALIZE TS42-DONNEES.                                             
           PERFORM DECLARE-RTVT06.                                              
           PERFORM FETCH-RTVT06.                                                
           IF TROUVE                                                            
              SET OK-DOSSIER6     TO TRUE                                       
           ELSE                                                                 
              SET NON-OK-DOSSIER6 TO TRUE                                       
           END-IF.                                                              
           PERFORM UNTIL NON-TROUVE                                             
              MOVE 'GV06'                 TO TS42-TABLE                         
              MOVE  COMM-BS42-NSOCIETE    TO TS42-NSOCIETE                      
              MOVE  COMM-BS42-NLIEU       TO TS42-NLIEU                         
              MOVE  COMM-BS42-NVENTE      TO TS42-NVENTE                        
              MOVE         VT06-NDOSSIER  TO TS42-NDOSSIER6                     
              MOVE         VT06-NSEQNQ    TO TS42-NSEQNQ6                       
              MOVE         VT06-NSEQ      TO TS42-NSEQ6                         
              PERFORM ECRITURE-LIGNE-TS                                         
              PERFORM FETCH-RTVT06                                              
           END-PERFORM.                                                         
      *------------------TABLE RTVT08-------------------------*                 
           INITIALIZE TS42-DONNEES.                                             
           PERFORM DECLARE-RTVT08.                                              
           PERFORM FETCH-RTVT08.                                                
           IF TROUVE                                                            
              SET OK-DOSSIER8     TO TRUE                                       
           ELSE                                                                 
              SET NON-OK-DOSSIER8 TO TRUE                                       
           END-IF.                                                              
           PERFORM UNTIL NON-TROUVE                                             
              MOVE 'GV08'                 TO TS42-TABLE                         
              MOVE  COMM-BS42-NSOCIETE    TO TS42-NSOCIETE                      
              MOVE  COMM-BS42-NLIEU       TO TS42-NLIEU                         
              MOVE  COMM-BS42-NVENTE      TO TS42-NVENTE                        
              MOVE         VT08-NDOSSIER  TO TS42-NDOSSIER8                     
              MOVE         VT08-NSEQ      TO TS42-NSEQ8                         
              MOVE         VT08-CCTRL     TO TS42-CCTRL                         
              IF VT08-CCTRL = 'NCRED'                                           
                 MOVE 'CONTRAT'         TO TYP-DONNEE                           
                 MOVE VT08-VALCTRL      TO LIB-DONNEE                           
                 MOVE LIG-COM-GV10      TO GV10-LCOMVTE2                        
              END-IF                                                            
              IF VT08-CCTRL = 'NVRED'                                           
                 MOVE 'VENTE'           TO TYP-DONNEE                           
                 MOVE VT08-VALCTRL      TO LIB-DONNEE                           
                 MOVE LIG-COM-GV10      TO GV10-LCOMVTE1                        
              END-IF                                                            
      *                                                                         
JM            IF VT08-CCTRL = 'CHANG'                                           
JM               MOVE 'CHANGMT'         TO TYP-DONNEE                           
JM               MOVE VT08-VALCTRL      TO LIB-DONNEE                           
JM               MOVE LIG-COM-GV10      TO GV10-LCOMVTE3                        
JM            END-IF                                                            
      *                                                                         
JM            IF VT08-CCTRL = 'CHGPR'                                           
JM               MOVE 'CHGPREC'         TO TYP-DONNEE                           
JM               MOVE VT08-VALCTRL      TO LIB-DONNEE                           
JM               MOVE LIG-COM-GV10      TO GV10-LCOMVTE4                        
JM            END-IF                                                            
SIEBEL        IF VT08-CCTRL = 'B2BEX'                                           
                 MOVE VT08-VALCTRL (1:20) TO GV10-NAUTO                         
              END-IF                                                            
      *                                                                         
              MOVE         VT08-LGCTRL    TO TS42-LGCTRL                        
              MOVE         VT08-VALCTRL   TO TS42-VALCTRL                       
              MOVE         VT08-CAUTOR    TO TS42-CAUTOR                        
              MOVE         VT08-CJUSTIF   TO TS42-CJUSTIF                       
              MOVE         VT08-DMODIF    TO TS42-DMODIF8                       
              PERFORM ECRITURE-LIGNE-TS                                         
              PERFORM FETCH-RTVT08                                              
           END-PERFORM.                                                         
      *                                                                         
       FIN-RECHERCHE-DONNEES-DOSSIER. EXIT.                                     
      *                                                                         
      *------- MISE A JOUR DES TABLES ------------------------------*           
      *                                                                         
       MISE-A-JOUR-ENTETE              SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              PERFORM INSERT-ENTETE-GV                                          
           ELSE                                                                 
              MOVE RVGV1005 TO RVVE1000                                         
              PERFORM INSERT-ENTETE-VE                                          
           END-IF.                                                              
      *                                                                         
       FIN-MAJ-ENTETE. EXIT.                                                    
      *                                                                         
      *                                                                         
       MISE-A-JOUR-ADRESSE             SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE 1 TO RANG-TS.                                                   
           PERFORM READ-TSBS42.                                                 
           PERFORM UNTIL FIN-TS OR TS42-TABLE = 'GV02'                          
               PERFORM READ-TSBS42                                              
           END-PERFORM.                                                         
           IF FIN-TS                                                            
      * OBJET NON TROUVE EN TS                                                  
               MOVE '0004'                TO GA99-NSEQERR                       
               PERFORM MLIBERRGEN                                               
               MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                      
               SET  COMM-BS42-CODRET-ERREUR  TO TRUE                            
               SET NON-OK-MAJ-ADRESSE TO TRUE                                   
           END-IF                                                               
           PERFORM UNTIL FIN-TS OR TS42-TABLE NOT = 'GV02'                      
              INITIALIZE RVGV0202 RVVE0200                                      
              MOVE  TS42-NSOCIETE           TO GV02-NSOCIETE                    
              MOVE  TS42-NLIEU              TO GV02-NLIEU                       
              MOVE  WS-NORDRE               TO GV02-NORDRE                      
              MOVE  TS42-NVENTE             TO GV02-NVENTE                      
              MOVE  TS42-WTYPEADR           TO GV02-WTYPEADR                    
              MOVE  TS42-CTITRENOM          TO GV02-CTITRENOM                   
              MOVE  TS42-LNOM               TO GV02-LNOM                        
              MOVE  TS42-LNOM               TO W-LNOM                           
              MOVE  TS42-LPRENOM            TO GV02-LPRENOM                     
              MOVE  TS42-LBATIMENT          TO GV02-LBATIMENT                   
              MOVE  TS42-LESCALIER          TO GV02-LESCALIER                   
              MOVE  TS42-LETAGE             TO GV02-LETAGE                      
              MOVE  TS42-LPORTE             TO GV02-LPORTE                      
              MOVE  TS42-LCOMPAD1           TO GV02-LCMPAD1                     
              MOVE  SPACES                  TO GV02-LCMPAD2                     
AL0906        IF METS-ARF-OUI AND GV02-WTYPEADR = 'A'                           
     |        MOVE  'A'                     TO GV02-LCMPAD2 (32:1)              
     |        END-IF                                                            
              MOVE  TS42-CVOIE              TO GV02-CVOIE                       
              MOVE  TS42-CTVOIE             TO GV02-CTVOIE                      
              MOVE  TS42-LNOMVOIE           TO GV02-LNOMVOIE                    
              MOVE  TS42-LCOMMUNE           TO GV02-LCOMMUNE                    
              MOVE  TS42-CPOSTAL            TO GV02-CPOSTAL                     
              MOVE  TS42-TELDOM             TO GV02-TELDOM                      
              MOVE  TS42-TELBUR             TO GV02-TELBUR                      
              MOVE  TS42-IDCLIENT           TO GV02-IDCLIENT                    
              MOVE  TS42-CPAYS              TO GV02-CPAYS                       
              MOVE  TS42-NGSM               TO GV02-NGSM                        
              MOVE  SPACES                  TO GV02-LPOSTEBUR                   
              MOVE  SPACES                  TO GV02-LCOMLIV1                    
              MOVE  SPACES                  TO GV02-LCOMLIV2                    
              MOVE  SPACES                  TO GV02-WETRANGER                   
              MOVE  SPACES                  TO GV02-CZONLIV                     
              PERFORM GQ06-CPOSTAL-CTYPE                                        
              IF TROUVE                                                         
                 MOVE GQ06-LBUREAU             TO GV02-LBUREAU                  
                 MOVE GQ06-CINSEE              TO GV02-CINSEE                   
              ELSE                                                              
                 MOVE TS42-LCOMMUNE            TO GV02-LBUREAU                  
              END-IF                                                            
              MOVE SPACES                   TO GV02-WCONTRA                     
              MOVE DATHEUR                  TO GV02-DSYST                       
              MOVE SPACES                   TO GV02-CILOT                       
              IF GV02-WTYPEADR = 'B'                                            
                 SET ADRESSE-LIVRAISON TO TRUE                                  
                 MOVE GV02-CPOSTAL TO WS-AD-LIVR-CPOSTAL                        
              END-IF                                                            
              IF GV02-WTYPEADR = 'C'                                            
                 SET ADRESSE-REPRISE   TO TRUE                                  
                 MOVE GV02-CPOSTAL TO WS-AD-REPR-CPOSTAL                        
              END-IF                                                            
TYPTR *       IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                      
TYPTR         IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                               
                 PERFORM INSERT-ADRESSE-GV                                      
              ELSE                                                              
                 MOVE RVGV0202 TO RVVE0200                                      
                 PERFORM INSERT-ADRESSE-VE                                      
              END-IF                                                            
              PERFORM READ-TSBS42                                               
           END-PERFORM.                                                         
      *                                                                         
       FIN-MAJ-ADRESSE. EXIT.                                                   
      *                                                                         
       MISE-A-JOUR-REGLEMENT           SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           IF OK-REGLEMENT                                                      
             MOVE 1 TO RANG-TS                                                  
             PERFORM READ-TSBS42                                                
             PERFORM UNTIL FIN-TS OR TS42-TABLE = 'GV14'                        
                 PERFORM READ-TSBS42                                            
             END-PERFORM                                                        
             IF FIN-TS                                                          
      * NON TROUVE EN TS                                                        
               MOVE '0004'                TO GA99-NSEQERR                       
               PERFORM MLIBERRGEN                                               
               MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                      
               SET  COMM-BS42-CODRET-ERREUR  TO TRUE                            
               SET NON-OK-MAJ-REGLEMENT TO TRUE                                 
             END-IF                                                             
           PERFORM UNTIL FIN-TS OR TS42-TABLE NOT = 'GV14'                      
              INITIALIZE RVGV1404 RVVE1400                                      
              MOVE  TS42-NSOCIETE           TO GV14-NSOCIETE                    
              MOVE  TS42-NLIEU              TO GV14-NLIEU                       
              MOVE  WS-NORDRE               TO GV14-NORDRE                      
              MOVE  TS42-NVENTE             TO GV14-NVENTE                      
              MOVE  TS42-DSAISIE            TO GV14-DSAISIE                     
              MOVE  TS42-NSEQ               TO GV14-NSEQ                        
              MOVE  TS42-CMODPAIMT          TO GV14-CMODPAIMT                   
              MOVE  TS42-PREGLTVTE          TO GV14-PREGLTVTE                   
              MOVE  TS42-NREGLTVTE          TO GV14-NREGLTVTE                   
              MOVE  TS42-DREGLTVTE          TO GV14-DREGLTVTE                   
              MOVE  TS42-WREGLTVTE          TO GV14-WREGLTVTE                   
              MOVE  TS42-NSOCP              TO GV14-NSOCP                       
              MOVE  TS42-NLIEUP             TO GV14-NLIEUP                      
              MOVE  TS42-NTRANS             TO GV14-NTRANS                      
              MOVE  TS42-CVENDEUR           TO GV14-CVENDEUR                    
              MOVE  TS42-CFCRED             TO GV14-CFCRED                      
              MOVE  TS42-NCREDI             TO GV14-NCREDI                      
              MOVE  GV10-DVENTE             TO GV14-DCOMPTA                     
              MOVE         DATHEUR          TO GV14-DSYST                       
              MOVE  SPACES                  TO GV14-CPROTOUR                    
              MOVE  SPACES                  TO GV14-CTOURNEE                    
TYPTR *       IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                      
TYPTR         IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                               
                 PERFORM INSERT-REGLEMENT-GV                                    
              ELSE                                                              
                 MOVE RVGV1404 TO RVVE1400                                      
                 PERFORM INSERT-REGLEMENT-VE                                    
              END-IF                                                            
               PERFORM READ-TSBS42                                              
           END-PERFORM                                                          
           ELSE                                                                 
              MOVE  GV10-NSOCIETE           TO GV14-NSOCIETE                    
              MOVE  GV10-NLIEU              TO GV14-NLIEU                       
              MOVE  WS-NORDRE               TO GV14-NORDRE                      
              MOVE  GV10-NVENTE             TO GV14-NVENTE                      
              MOVE  GV10-DVENTE             TO GV14-DSAISIE                     
              MOVE  '01'                    TO GV14-NSEQ                        
              MOVE  '999'                   TO GV14-CMODPAIMT                   
              MOVE  GV10-PTTVENTE           TO GV14-PREGLTVTE                   
              MOVE  SPACES                  TO GV14-NREGLTVTE                   
              MOVE  GV10-DVENTE             TO GV14-DREGLTVTE                   
              MOVE  'V'                     TO GV14-WREGLTVTE                   
              MOVE  GV10-DVENTE             TO GV14-DCOMPTA                     
              MOVE         DATHEUR          TO GV14-DSYST                       
              MOVE  SPACES                  TO GV14-CPROTOUR                    
              MOVE  SPACES                  TO GV14-CTOURNEE                    
TYPTR *       IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                      
TYPTR         IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                               
                 PERFORM INSERT-REGLEMENT-GV                                    
              ELSE                                                              
                 MOVE RVGV1404 TO RVVE1400                                      
                 PERFORM INSERT-REGLEMENT-VE                                    
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-MAJ-REGLEMENT. EXIT.                                                 
      *                                                                         
      *                                                                         
       MISE-A-JOUR-DOSSIER             SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      *------------------TRAIT-RTVT05--------------------------*                
           SET OK-MAJ-DOSSIER TO TRUE.                                          
           IF OK-DOSSIER5                                                       
             MOVE 1 TO RANG-TS                                                  
             PERFORM READ-TSBS42                                                
             PERFORM UNTIL FIN-TS OR TS42-TABLE = 'GV05'                        
                 PERFORM READ-TSBS42                                            
             END-PERFORM                                                        
             IF FIN-TS                                                          
      * NON TROUVE EN TS                                                        
               MOVE '0004'                  TO GA99-NSEQERR                     
               PERFORM MLIBERRGEN                                               
               MOVE LIBERR-MESSAGE          TO COMM-BS42-LIBERR                 
               SET  COMM-BS42-CODRET-ERREUR TO TRUE                             
               SET NON-OK-MAJ-DOSSIER       TO TRUE                             
             END-IF                                                             
      *                                                                         
             PERFORM UNTIL FIN-TS OR TS42-TABLE NOT = 'GV05'                    
                INITIALIZE RVGV0500 RVVE0500                                    
                MOVE  TS42-NSOCIETE         TO GV05-NSOCIETE                    
                MOVE  TS42-NLIEU            TO GV05-NLIEU                       
                MOVE  TS42-NVENTE           TO GV05-NVENTE                      
                MOVE  TS42-NDOSSIER5        TO GV05-NDOSSIER                    
                MOVE  TS42-CDOSSIER         TO GV05-CDOSSIER                    
                MOVE  TS42-CTYPENT5         TO GV05-CTYPENT                     
                MOVE  TS42-NENTITE          TO GV05-NENTITE                     
                MOVE  TS42-NSEQENS5         TO GV05-NSEQENS                     
                MOVE  TS42-CAGENT           TO GV05-CAGENT                      
                MOVE  TS42-DCREATION        TO GV05-DCREATION                   
                MOVE  TS42-DMODIF5          TO GV05-DMODIF                      
                MOVE  TS42-DANNUL           TO GV05-DANNUL                      
                MOVE         DATHEUR        TO GV05-DSYST                       
      *                                                                         
TYPTR *         IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                    
TYPTR           IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                             
                   PERFORM INSERT-RTGV05                                        
                ELSE                                                            
                   MOVE RVGV0500 TO RVVE0500                                    
                   PERFORM INSERT-RTVE05                                        
                END-IF                                                          
                PERFORM READ-TSBS42                                             
             END-PERFORM                                                        
           END-IF.                                                              
      *------------------TRAIT-RTVT06--------------------------*                
           IF OK-DOSSIER6                                                       
             MOVE 1 TO RANG-TS                                                  
             PERFORM READ-TSBS42                                                
             PERFORM UNTIL FIN-TS OR TS42-TABLE = 'GV06'                        
                 PERFORM READ-TSBS42                                            
             END-PERFORM                                                        
             IF FIN-TS                                                          
      * NON TROUVE EN TS                                                        
               MOVE '0004'                  TO GA99-NSEQERR                     
               PERFORM MLIBERRGEN                                               
               MOVE LIBERR-MESSAGE          TO COMM-BS42-LIBERR                 
               SET  COMM-BS42-CODRET-ERREUR TO TRUE                             
               SET NON-OK-MAJ-DOSSIER       TO TRUE                             
             END-IF                                                             
      *                                                                         
             PERFORM UNTIL FIN-TS OR TS42-TABLE NOT = 'GV06'                    
                INITIALIZE RVGV0600 RVVE0600                                    
                MOVE  TS42-NSOCIETE         TO GV06-NSOCIETE                    
                MOVE  TS42-NLIEU            TO GV06-NLIEU                       
                MOVE  TS42-NVENTE           TO GV06-NVENTE                      
                MOVE  TS42-NDOSSIER6        TO GV06-NDOSSIER                    
                MOVE  TS42-NSEQNQ6          TO GV06-NSEQNQ                      
                MOVE  TS42-NSEQ6            TO GV06-NSEQ                        
                MOVE         DATHEUR        TO GV06-DSYST                       
      *                                                                         
TYPTR *         IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                    
TYPTR           IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                             
                   PERFORM INSERT-RTGV06                                        
                ELSE                                                            
                   MOVE RVGV0600 TO RVVE0600                                    
                   PERFORM INSERT-RTVE06                                        
                END-IF                                                          
                PERFORM READ-TSBS42                                             
             END-PERFORM                                                        
           END-IF.                                                              
      *------------------TRAIT-RTVT08--------------------------*                
           IF OK-DOSSIER8                                                       
             MOVE 1 TO RANG-TS                                                  
             PERFORM READ-TSBS42                                                
             PERFORM UNTIL FIN-TS OR TS42-TABLE = 'GV08'                        
                 PERFORM READ-TSBS42                                            
             END-PERFORM                                                        
             IF FIN-TS                                                          
      * NON TROUVE EN TS                                                        
               MOVE '0004'                  TO GA99-NSEQERR                     
               PERFORM MLIBERRGEN                                               
               MOVE LIBERR-MESSAGE          TO COMM-BS42-LIBERR                 
               SET  COMM-BS42-CODRET-ERREUR TO TRUE                             
               SET NON-OK-MAJ-DOSSIER       TO TRUE                             
             END-IF                                                             
      *                                                                         
             PERFORM UNTIL FIN-TS OR TS42-TABLE NOT = 'GV08'                    
                INITIALIZE RVGV0800 RVVE0800                                    
                MOVE  TS42-NSOCIETE         TO GV08-NSOCIETE                    
                MOVE  TS42-NLIEU            TO GV08-NLIEU                       
                MOVE  TS42-NVENTE           TO GV08-NVENTE                      
                MOVE  TS42-NDOSSIER8        TO GV08-NDOSSIER                    
                MOVE  TS42-NSEQ8            TO GV08-NSEQ                        
                MOVE  TS42-CCTRL            TO GV08-CCTRL                       
                MOVE  TS42-LGCTRL           TO GV08-LGCTRL                      
                MOVE  TS42-VALCTRL          TO GV08-VALCTRL                     
                MOVE  TS42-CAUTOR           TO GV08-CAUTOR                      
                MOVE  TS42-CJUSTIF          TO GV08-CJUSTIF                     
                MOVE  TS42-DMODIF8          TO GV08-DMODIF                      
                MOVE         DATHEUR        TO GV08-DSYST                       
      *                                                                         
TYPTR *         IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                    
TYPTR           IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                             
                   PERFORM INSERT-RTGV08                                        
                   IF  'SERIE' = GV08-CCTRL                                     
                     PERFORM UPDATE-RTGV11                                      
                   END-IF                                                       
                ELSE                                                            
                   MOVE RVGV0800 TO RVVE0800                                    
                   PERFORM INSERT-RTVE08                                        
                END-IF                                                          
                PERFORM READ-TSBS42                                             
             END-PERFORM                                                        
           END-IF.                                                              
      *                                                                         
       FIN-MAJ-DOSSIER. EXIT.                                                   
      *                                                                         
      *                                                                         
      *                                                                         
       MISE-A-JOUR-ARTICLES            SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE 1 TO RANG-TS.                                                   
           PERFORM READ-TSBS42.                                                 
           PERFORM UNTIL FIN-TS OR TS42-TABLE = 'GV11'                          
               PERFORM READ-TSBS42                                              
           END-PERFORM.                                                         
           IF FIN-TS                                                            
      * NON TROUVE EN TS                                                        
               MOVE '0004'                TO GA99-NSEQERR                       
               PERFORM MLIBERRGEN                                               
               MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                      
               SET  COMM-BS42-CODRET-ERREUR  TO TRUE                            
               SET NON-OK-MAJ-ARTICLES TO TRUE                                  
           END-IF.                                                              
           PERFORM SELECT-CVENDEUR-ARCH.                                        
      *                                                                         
           MOVE 0 TO WS-NSEQNQ.                                                 
      *                                                                         
           PERFORM UNTIL FIN-TS OR TS42-TABLE NOT = 'GV11'                      
              INITIALIZE RVGV1106                                               
              MOVE  TS42-NSOCIETE     TO GV11-NSOCIETE                          
              MOVE  TS42-NLIEU        TO GV11-NLIEU                             
              MOVE  TS42-NVENTE       TO GV11-NVENTE                            
              MOVE    TS42-CTYPENREG  TO GV11-CTYPENREG                         
              MOVE    TS42-NCODICGRP  TO GV11-NCODICGRP                         
              MOVE    TS42-NCODIC     TO GV11-NCODIC                            
              MOVE    TS42-NSEQA      TO GV11-NSEQ                              
              MOVE    TS42-CMODDEL    TO GV11-CMODDEL                           
              MOVE    TS42-DDELIV     TO GV11-DDELIV                            
              MOVE    WS-NORDRE       TO GV11-NORDRE                            
              MOVE    TS42-CENREG     TO GV11-CENREG                            
              MOVE    TS42-QVENDUE    TO GV11-QVENDUE                           
              MOVE    TS42-PVUNIT     TO GV11-PVUNIT                            
              MOVE    TS42-PVUNITF    TO GV11-PVUNITF                           
              MOVE    TS42-PVTOTAL    TO GV11-PVTOTAL                           
              MOVE    SPACES          TO GV11-CEQUIPE                           
              MOVE    TS42-NLIGNE     TO GV11-NLIGNE                            
              MOVE    TS42-TAUXTVA    TO GV11-TAUXTVA                           
              MOVE    TS42-QCONDT     TO GV11-QCONDT                            
              MOVE    0               TO GV11-PRMP                              
              MOVE    SPACES          TO GV11-WEMPORTE                          
              MOVE    SPACES          TO GV11-CPLAGE                            
              MOVE    SPACES          TO GV11-CPROTOUR                          
              MOVE    SPACES          TO GV11-CADRTOUR                          
              MOVE    SPACES          TO GV11-CPOSTAL                           
              MOVE    SPACES          TO GV11-WEMPORTE                          
              MOVE    TS42-LCOMMENT   TO GV11-LCOMMENT                          
CVEND         IF TS42-NVENDEUR > SPACES                                         
CVEND            MOVE TS42-NVENDEUR   TO GV11-CVENDEUR                          
                 EXEC SQL SELECT CVENDEUR                                       
                         INTO :GV11-CVENDEUR                                    
                         FROM     RVGV3100                                      
                         WHERE CVENDEUR = :GV11-CVENDEUR                        
                         END-EXEC                                               
                 IF SQLCODE NOT = 0 THEN                                        
                     MOVE  VESPE-CVESPE   TO GV11-CVENDEUR                      
                 END-IF                                                         
CVEND         ELSE                                                              
                 MOVE  VESPE-CVESPE   TO GV11-CVENDEUR                          
CVEND         END-IF                                                            
              MOVE    SPACES          TO GV11-NSOCLIVR                          
              MOVE    SPACES          TO GV11-NDEPOT                            
              MOVE    SPACES          TO GV11-NAUTORM                           
              MOVE    SPACES          TO GV11-WARTINEX                          
              MOVE    'N'             TO GV11-WEDITBL                           
              MOVE    SPACES          TO GV11-WACOMMUTER                        
              MOVE    SPACES          TO GV11-WCQERESF                          
              MOVE    SPACES          TO GV11-NMUTATION                         
              MOVE    SPACES          TO GV11-CTOURNEE                          
              MOVE    'O'             TO GV11-WTOPELIVRE                        
              MOVE    GV10-DVENTE     TO GV11-DCOMPTA                           
              MOVE    GV10-DVENTE     TO GV11-DCREATION                         
              MOVE    SPACES          TO GV11-HCREATION                         
              MOVE   TS42-DANNULATION TO GV11-DANNULATION                       
              MOVE    SPACES          TO GV11-NLIEUORIG                         
              MOVE    'N'             TO GV11-WINTMAJ                           
              MOVE         DATHEUR    TO GV11-DSYST                             
              MOVE    GV10-DSTAT      TO GV11-DSTAT                             
              MOVE    SPACES          TO GV11-CPRESTGRP                         
              MOVE    SPACES          TO GV11-NSOCMODIF                         
              MOVE    SPACES          TO GV11-NLIEUMODIF                        
DATENC        MOVE    GV10-DVENTE     TO GV11-DATENC                            
              MOVE    SPACES          TO GV11-CDEV                              
              MOVE    SPACES          TO GV11-WUNITR                            
              MOVE    SPACES          TO GV11-LRCMMT                            
              MOVE    SPACES          TO GV11-NSOCP                             
              MOVE    SPACES          TO GV11-NLIEUP                            
              MOVE    0               TO GV11-NTRANS                            
OA    *       MOVE    SPACES          TO GV11-NSEQFV                            
M13813        MOVE VT01-NCLUSTER TO VT14-NCLUSTER                               
"             MOVE VT01-NFLAG    TO VT14-NFLAG                                  
"             PERFORM SELECT-VT14-REFAC                                         
"             IF TROUVE                                                         
"                   MOVE '00'               TO GV11-NSEQFV                      
"             ELSE                                                              
"                   MOVE    SPACES          TO GV11-NSEQFV                      
M13813        END-IF                                                            
DA            IF TS42-NSEQNQ NOT > 0                                            
DA               IF (GV11-CTYPENREG = '1' AND GV11-CENREG = '     '             
DA                  AND GV11-CMODDEL (1:1) NOT = 'R')                           
DA               OR (GV11-CTYPENREG = '4' AND GV11-NCODIC = SPACES)             
DA                  ADD     1               TO WS-NSEQNQ                        
DA                  MOVE    WS-NSEQNQ       TO WS-NSEQREF                       
DA               ELSE                                                           
DA                  ADD     1               TO WS-NSEQNQ                        
DA               END-IF                                                         
DA               MOVE    WS-NSEQNQ       TO GV11-NSEQNQ                         
DA               MOVE    WS-NSEQREF      TO GV11-NSEQREF                        
DA            ELSE                                                              
DA               MOVE    TS42-NSEQNQ     TO GV11-NSEQNQ                         
DA               MOVE    TS42-NSEQREF    TO GV11-NSEQREF                        
DA            END-IF                                                            
EJ0301        MOVE    0               TO GV11-NMODIF                            
  "           MOVE    SPACES          TO GV11-NSOCORIG                          
  "           MOVE    SPACES          TO GV11-NSOCLIVR1                         
  "           MOVE    SPACES          TO GV11-NDEPOT1                           
  "           MOVE    SPACES          TO GV11-NSOCGEST                          
  "           MOVE    SPACES          TO GV11-NLIEUGEST                         
  "           MOVE    SPACES          TO GV11-NSOCDEPLIV                        
  "           MOVE    SPACES          TO GV11-NLIEUDEPLIV                       
  "           MOVE    SPACES          TO GV11-DPRLG                             
  "           MOVE    SPACES          TO GV11-CALERTE                           
  "           MOVE    SPACES          TO GV11-CREPRISE                          
  "           MOVE VT01-NTYPVENTE     TO GV11-TYPVTE                            
              IF    VT01-NTYPVENTE NOT > SPACES                                 
              OR    VT01-NTYPVENTE = 'M'                                        
                 MOVE  'G'  TO GV11-TYPVTE                                      
              END-IF                                                            
  "           MOVE TS42-CTYPENT       TO GV11-CTYPENT                           
  "           MOVE TS42-NLIEN         TO GV11-NLIEN                             
  "           MOVE TS42-NACTVTE       TO GV11-NACTVTE                           
  "           MOVE TS42-PVCODIG       TO GV11-PVCODIG                           
  "           MOVE TS42-QTCODIG       TO GV11-QTCODIG                           
  "           MOVE TS42-NSEQENS       TO GV11-NSEQENS                           
  "           MOVE TS42-MPRIMECLI     TO GV11-MPRIMECLI                         
EJ0301        MOVE    TS42-DDELIV     TO GV11-DTOPE                             
              IF GV11-CMODDEL(1:1) = 'L'                                        
                IF ADRESSE-LIVRAISON                                            
                  MOVE    'B'                TO GV11-CADRTOUR                   
                  MOVE    WS-AD-LIVR-CPOSTAL TO GV11-CPOSTAL                    
                END-IF                                                          
              END-IF                                                            
              IF GV11-CMODDEL(1:1) = 'R'                                        
                IF ADRESSE-REPRISE                                              
                  MOVE    'C'                TO GV11-CADRTOUR                   
                  MOVE    WS-AD-REPR-CPOSTAL TO GV11-CPOSTAL                    
                END-IF                                                          
              END-IF                                                            
TYPTR *       IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                      
TYPTR         IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                               
PM               IF GV11-CTYPENREG = '3' AND                                    
PM                  GV11-LCOMMENT = SPACE                                       
PM                  PERFORM INSERT-PSE                                          
PM                  SET TOP-PSE TO TRUE                                         
PM               END-IF                                                         
                 PERFORM INSERT-ARTICLES-GV                                     
              ELSE                                                              
                 MOVE RVGV1106 TO RVVE1100                                      
                 PERFORM INSERT-ARTICLES-VE                                     
              END-IF                                                            
              PERFORM READ-TSBS42                                               
           END-PERFORM.                                                         
      *                                                                         
      *---MISE A JOUR DU NSEQNQ DE L'ENTETE DE VENTE                            
      *                                                                         
           IF WS-NSEQNQ > 0                                                     
TYPTR *       IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                      
TYPTR         IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                               
                 PERFORM UPD-NSEQNQ-GV10                                        
              ELSE                                                              
                 PERFORM UPD-NSEQNQ-VE10                                        
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
       FIN-MAJ-ARTICLES. EXIT.                                                  
      *                                                                         
       ECRITURE-LIGNE-TS               SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      ****                                                                      
           PERFORM WRITE-TSBS42.                                                
           INITIALIZE TS42-DONNEES.                                             
           ADD 1 TO RANG-TS.                                                    
      *                                                                         
       FIN-ECRITURE-LIGNE-TS. EXIT.                                             
      *                                                                         
      *                                                                         
       CALCUL-NO-ORDRE                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      **** POUR LE CALCUL DU NUMERO D' ORDRE ON AJOUTE 1 AU NUMERO              
      **** D' ORDRE LE PLUS GRAND POUR LA SOCIETE ET LE LIEU TRAITE             
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              PERFORM SELECT-RTGV10-2                                           
              IF TROUVE                                                         
              THEN MOVE GV10-NORDRE    TO WS-NORDRE                             
              ELSE MOVE ZEROES         TO WS-NORDRE                             
              END-IF                                                            
           ELSE                                                                 
              PERFORM SELECT-RTVE10-2                                           
              IF TROUVE                                                         
              THEN MOVE VE10-NORDRE    TO WS-NORDRE                             
              ELSE MOVE ZEROES         TO WS-NORDRE                             
              END-IF                                                            
           END-IF.                                                              
           ADD  1                      TO WS-NORDRE.                            
      *                                                                         
       FIN-CALCUL-NUMERO-ORDRE. EXIT.                                           
      *                                                                         
      *                                                                         
       DELETE-ARCHIVAGE                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      ****                                                                      
           PERFORM DELETE-RTVT02.                                               
           PERFORM DELETE-RTVT11.                                               
           PERFORM DELETE-RTVT16.                                               
           PERFORM DELETE-RTVT12.                                               
           PERFORM DELETE-RTVT17.                                               
           PERFORM DELETE-RTVT13.                                               
           PERFORM DELETE-RTVT15.                                               
           PERFORM DELETE-RTVT14.                                               
           PERFORM DELETE-RTVT01.                                               
           IF OK-DOSSIER5                                                       
             PERFORM DELETE-RTVT05                                              
           END-IF.                                                              
           IF OK-DOSSIER6                                                       
             PERFORM DELETE-RTVT06                                              
           END-IF.                                                              
           IF OK-DOSSIER8                                                       
             PERFORM DELETE-RTVT08                                              
           END-IF.                                                              
           PERFORM DELETE-RTVT50.                                               
      *                                                                         
       FIN-DELETE-ARCHIVAGE. EXIT.                                              
      *                                                                         
GV52   DUPLI-VENTE-NMD                 SECTION.                                 
 "    *----------------------------------------                                 
 "    *                                                                         
 "         IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
 "            MOVE GV10-NSOCIETE          TO COMM-MGV51-NSOCIETE                
 "            MOVE GV10-NLIEU             TO COMM-MGV51-NLIEU                   
 "            MOVE GV10-NVENTE            TO COMM-MGV51-NVENTE                  
 "            MOVE LENGTH OF COMM-MGV51-APPLI TO LONG-COMMAREA-LINK             
 "            MOVE COMM-MGV51-APPLI       TO Z-COMMAREA-LINK                    
 "            MOVE 'MGV52'                TO NOM-PROG-LINK                      
 "            PERFORM LINK-PROG                                                 
 "            MOVE Z-COMMAREA-LINK        TO COMM-MGV51-APPLI                   
 "         END-IF.                                                              
 "    *                                                                         
GV52   FIN-DUPLI-VENTE-NMD. EXIT.                                               
      *                                                                         
      *                                                                         
       READ-TSBS42                    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           IF EIBTRMID  = LOW-VALUES                                            
              MOVE EIBTASKN              TO P-EIBTASKN                          
              MOVE X-EIBTASKN TO EIBTRMID                                       
           END-IF                                                               
           STRING 'GV42' EIBTRMID                                               
              DELIMITED BY SIZE       INTO IDENT-TS                             
           END-STRING                                                           
           MOVE LENGTH OF TS42-DONNEES TO LONG-TS                               
           PERFORM READ-TS                                                      
           IF TROUVE                                                            
              SET PAS-FIN-TS TO TRUE                                            
              MOVE Z-INOUT               TO TS42-DONNEES                        
              ADD 1 TO RANG-TS                                                  
           ELSE                                                                 
              SET FIN-TS TO TRUE                                                
           END-IF.                                                              
      *                                                                         
       FIN-READ-TSGV01. EXIT.                                                   
      *                                                                         
      *                                                                         
       MODULE-SORTIE                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
GV52  * SI TOUT S'EST BIEN PASS� , ON ENVOIE UNE PHOTO NMD DE LA VENTE          
 "         IF COMM-BS42-CODRET-OK                                               
 "            PERFORM DUPLI-VENTE-NMD                                           
GV52       END-IF                                                               
      **** PASSAGE DE LA COMMAREA - RETOUR                                      
           MOVE COMM-BS42-APPLI             TO DFHCOMMAREA.                     
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS RETURN  END-EXEC.                                          
      *--                                                                       
           EXEC CICS RETURN                                                     
           END-EXEC.                                                            
      *}                                                                        
           GOBACK.                                                              
      *                                                                         
       FIN-MODULE-SORTIE. EXIT.                                                 
      *                                                                         
      *================================================================         
      *  SELECTION TABLE RTGQ06 (CODES POSTAUX,COMMUNES,BUREAU DIST)  *         
      *                ACCES AVEC LE CODE POSTAL ET LE TYPE           *         
      *                                                               *         
      * SELECT  * * * * * * ( EXEC SQL )  * * * * * * * * * * * * * * *         
      *================================================================         
       GQ06-CPOSTAL-CTYPE             SECTION.                                  
      *--------------------------------------*                                  
           MOVE  FUNC-SELECT   TO  TRACE-SQL-FUNCTION.                          
           MOVE  GV02-CPOSTAL  TO  GQ06-CPOSTAL.                                
           MOVE  GV02-LCOMMUNE TO  GQ06-LCOMUNE.                                
      *                                                                         
           PERFORM CLEF-GQ0600.                                                 
      *                                                                         
      *                                                                         
           EXEC SQL SELECT                                                      
                          CTYPE,                                                
                          CINSEE,                                               
                          LBUREAU                                               
                    INTO                                                        
                         :GQ06-CTYPE,                                           
                         :GQ06-CINSEE,                                          
                         :GQ06-LBUREAU                                          
                    FROM     RVGQ0600                                           
                    WHERE CPOSTAL = :GQ06-CPOSTAL                               
                      AND NOT (CTYPE   = 'S')                                   
                      AND LCOMUNE = :GQ06-LCOMUNE                               
                END-EXEC.                                                       
                IF SQLCODE = -811                                               
                   MOVE 0 TO SQLCODE                                            
                END-IF.                                                         
                PERFORM TEST-CODE-RETOUR-SQL.                                   
                IF NON-TROUVE                                                   
                    PERFORM GQ06-CPOSTAL-2                                      
                END-IF.                                                         
      *                                                                         
      *                                                                         
       FIN-GQ06-CPOSTAL-CTYPE. EXIT.                                            
      *                                                                         
       GQ06-CPOSTAL-2                 SECTION.                                  
      *--------------------------------------*                                  
           MOVE  FUNC-SELECT   TO  TRACE-SQL-FUNCTION.                          
           MOVE  GV02-CPOSTAL  TO  GQ06-CPOSTAL.                                
      *                                                                         
           PERFORM CLEF-GQ0600.                                                 
      *                                                                         
           EXEC SQL SELECT                                                      
                          CTYPE,                                                
                          CINSEE,                                               
                          LBUREAU                                               
                    INTO                                                        
                         :GQ06-CTYPE,                                           
                         :GQ06-CINSEE,                                          
                         :GQ06-LBUREAU                                          
                    FROM     RVGQ0600                                           
                    WHERE CPOSTAL = :GQ06-CPOSTAL                               
                      AND NOT (CTYPE   = 'S')                                   
                END-EXEC.                                                       
                IF SQLCODE = -811                                               
                   MOVE 0 TO SQLCODE                                            
                END-IF.                                                         
                PERFORM TEST-CODE-RETOUR-SQL.                                   
      *                                                                         
      *                                                                         
       FIN-GQ06-CPOSTAL-2. EXIT.                                                
      *                                                                         
       SELECT-NCLUSTER                SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      ****                                                                      
           MOVE COMM-BS42-NSOCIETE    TO VT01-NSOCIETE                          
           MOVE COMM-BS42-NLIEU       TO VT01-NLIEU                             
           MOVE COMM-BS42-NVENTE      TO VT01-NVENTE                            
           MOVE COMM-BS42-NFLAG       TO VT01-NFLAG                             
           MOVE COMM-BS42-NCLUSTER    TO VT01-NCLUSTER                          
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
YT1   *TYPTR IF COMM-BS42-TYPVTE = 'B' OR 'L' OR 'A'                            
TYPTR      IF W-VTE-LOC-SANS-ADR OR W-VTE-LOC-AVEC-ADR                          
TYPTR         OR W-VTE-ACOMPTE                                                  
 "           MOVE COMM-BS42-TYPVTE    TO VT01-NTYPVENTE                         
 "           EXEC SQL SELECT   NFLAG , NCLUSTER , DVENTE , NTYPVENTE            
 "                 INTO :VT01-NFLAG , :VT01-NCLUSTER , :VT01-DVENTE ,           
 "                      :VT01-NTYPVENTE                                         
 "                    FROM     RVVT0100                                         
 "                    WHERE    NFLAG     = :VT01-NFLAG                          
 "                    AND      NCLUSTER  = :VT01-NCLUSTER                       
 "                    AND      NVENTE    = :VT01-NVENTE                         
 "                    AND      NTYPVENTE = :VT01-NTYPVENTE                      
 "           END-EXEC                                                           
YT1        ELSE                                                                 
      *    LE SELECT IMBRIQUE PERMET D'AVOIR LA DVENTE                          
      *    (SELECT MAX(NCLUSTERP) NE MARCHE PAS SANS GROUP BY                   
      *    ET IL FAUDRAIT AVOIR UN CURSEUR)                                     
             EXEC SQL SELECT   NFLAG , NCLUSTER , DVENTE , NTYPVENTE            
                   INTO :VT01-NFLAG , :VT01-NCLUSTER , :VT01-DVENTE ,           
                        :VT01-NTYPVENTE                                         
                      FROM     RVVT0100                                         
                      WHERE    NFLAG     = :VT01-NFLAG                          
                      AND      NCLUSTER  = :VT01-NCLUSTER                       
CINMD *               AND      NSOCIETE  = :VT01-NSOCIETE                       
CINMD *               AND      NLIEU     = :VT01-NLIEU                          
CINMD *               AND      NVENTE    = :VT01-NVENTE                         
      *               AND      NCLUSTERP = (SELECT MAX(NCLUSTERP)               
      *                           FROM     RVVT0100                             
      *                       WHERE    NFLAG     = :VT01-NFLAG                  
      *                       AND      NCLUSTER  = :VT01-NCLUSTER               
      *                       AND      NSOCIETE  = :VT01-NSOCIETE               
      *                       AND      NLIEU     = :VT01-NLIEU                  
      *                       AND      NVENTE    = :VT01-NVENTE )               
             END-EXEC                                                           
YT1        END-IF                                                               
           IF SQLCODE = -811                                                    
                MOVE 0 TO SQLCODE                                               
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-SELECT-NCLUSTER. EXIT.                                               
      *                                                                         
       SELECT-VT14-PAI777             SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
      *    RECHERCHE D'AU MOINS UN MODE DE PAIEMENT '777'                       
             EXEC SQL SELECT   CMODPAIMT                                        
                      INTO :VT14-CMODPAIMT                                      
                      FROM     RVVT1400                                         
                      WHERE    NFLAG     = :VT14-NFLAG                          
                      AND      NCLUSTER  = :VT14-NCLUSTER                       
                      AND     CMODPAIMT  = '777'                                
             END-EXEC                                                           
           IF SQLCODE = -811                                                    
                MOVE 0 TO SQLCODE                                               
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
       FIN-SELECT-VT14-PAI777.  EXIT.                                           
      *                                                                         
M8653  SELECT-VT14-CFCRED             SECTION.                                  
"     *---------------------------------------                                  
"     *                                                                         
"          MOVE SPACES                TO VT14-CFCRED                            
"          MOVE SPACES                TO VT14-NCREDI                            
"          MOVE VT01-NCLUSTER         TO VT14-NCLUSTER                          
"          MOVE VT01-NFLAG            TO VT14-NFLAG                             
"          MOVE VT01-NSOCIETE         TO VT14-NSOCIETE                          
"          MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
"     *    RECHERCHE D'AU MOINS UN                                              
"            EXEC SQL SELECT   CFCRED ,  NCREDI                                 
"                     INTO :VT14-CFCRED  ,                                      
"                          :VT14-NCREDI                                         
"                     FROM     RVVT1400                                         
"                     WHERE    NFLAG     = :VT14-NFLAG                          
"                     AND      NCLUSTER  = :VT14-NCLUSTER                       
"                     AND      NSOCIETE  = :VT14-NSOCIETE                       
"                     AND      WREGLTVTE = 'D'                                  
"                     AND      CFCRED    > ''                                   
"                     AND      NCREDI    > ''                                   
"            END-EXEC                                                           
"                                                                               
"          IF SQLCODE = -811                                                    
"               MOVE 0 TO SQLCODE                                               
"          END-IF.                                                              
"                                                                               
"          PERFORM TEST-CODE-RETOUR-SQL.                                        
"                                                                               
M8653  FIN-SELECT-VT14-CFCRED.  EXIT.                                           
M13813 SELECT-VT14-REFAC              SECTION.                                  
"     *---------------------------------------                                  
"     *                                                                         
"          MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
"     *    RECHERCHE D'AU MOINS UN MODE DE PAIEMENT 'REFAC'                     
"            EXEC SQL SELECT   CMODPAIMT                                        
"                     INTO     :VT14-CMODPAIMT                                  
"                     FROM     RVVT1400                                         
"                     WHERE    NFLAG     = :VT14-NFLAG                          
"                     AND      NCLUSTER  = :VT14-NCLUSTER                       
"                     AND     CMODPAIMT  = 'REFAC'                              
"            END-EXEC                                                           
"                                                                               
"          IF SQLCODE = -811                                                    
"               MOVE 0 TO SQLCODE                                               
"          END-IF.                                                              
"                                                                               
"          PERFORM TEST-CODE-RETOUR-SQL.                                        
"                                                                               
M13813 FIN-SELECT-VT14-REFAC.  EXIT.                                            
      *                                                                         
       SELECT-MAX-NSEQ                SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      ****                                                                      
           MOVE 0 TO VT11-NSEQNQ VT11-NSEQENS                                   
                     VT12-NSEQNQ VT12-NSEQENS                                   
                     VT13-NSEQNQ VT13-NSEQENS                                   
                     VT15-NSEQNQ VT15-NSEQENS                                   
                     VT16-NSEQNQ VT16-NSEQENS                                   
                     VT17-NSEQNQ VT17-NSEQENS                                   
           MOVE COMM-BS42-NSOCIETE    TO VT11-NSOCIETE VT13-NSOCIETE            
                VT15-NSOCIETE VT16-NSOCIETE VT17-NSOCIETE                       
                VT12-NSOCIETE.                                                  
           MOVE VT01-NCLUSTER         TO VT11-NCLUSTER VT13-NCLUSTER            
                VT15-NCLUSTER VT16-NCLUSTER VT17-NCLUSTER                       
                VT12-NCLUSTER.                                                  
           MOVE VT01-NFLAG            TO VT11-NFLAG    VT13-NFLAG               
                VT15-NFLAG    VT16-NFLAG    VT17-NFLAG VT12-NFLAG.              
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
      *                                                                         
           EXEC SQL SELECT   MAX(NSEQNQ ) ,                                     
                             MAX(NSEQENS )                                      
                      INTO   :VT11-NSEQNQ :VT11-NSEQNQ-F ,                      
                             :VT11-NSEQENS  :VT11-NSEQENS-F                     
                      FROM   RVVT1100                                           
                      WHERE  NFLAG     = :VT11-NFLAG                            
                      AND    NCLUSTER  = :VT11-NCLUSTER                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *                                                                         
           EXEC SQL SELECT   MAX(NSEQNQ ) ,                                     
                             MAX(NSEQENS )                                      
                      INTO   :VT12-NSEQNQ :VT12-NSEQNQ-F ,                      
                             :VT12-NSEQENS  :VT12-NSEQENS-F                     
                      FROM   RVVT1200                                           
                      WHERE  NFLAG     = :VT12-NFLAG                            
                      AND    NCLUSTER  = :VT12-NCLUSTER                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           EXEC SQL SELECT   MAX(NSEQNQ ) ,                                     
                             MAX(NSEQENS )                                      
                      INTO   :VT15-NSEQNQ :VT15-NSEQNQ-F ,                      
                             :VT15-NSEQENS  :VT15-NSEQENS-F                     
                      FROM   RVVT1500                                           
                      WHERE  NFLAG     = :VT15-NFLAG                            
                      AND    NCLUSTER  = :VT15-NCLUSTER                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           EXEC SQL SELECT   MAX(NSEQNQ ) ,                                     
                             MAX(NSEQENS )                                      
                      INTO   :VT16-NSEQNQ :VT16-NSEQNQ-F ,                      
                             :VT16-NSEQENS  :VT16-NSEQENS-F                     
                      FROM   RVVT1600                                           
                      WHERE  NFLAG     = :VT16-NFLAG                            
                      AND    NCLUSTER  = :VT16-NCLUSTER                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           EXEC SQL SELECT   MAX(NSEQNQ ) ,                                     
                             MAX(NSEQENS )                                      
                      INTO   :VT13-NSEQNQ :VT13-NSEQNQ-F ,                      
                             :VT13-NSEQENS  :VT13-NSEQENS-F                     
                      FROM   RVVT1300                                           
                      WHERE  NFLAG     = :VT13-NFLAG                            
                      AND    NCLUSTER  = :VT13-NCLUSTER                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           EXEC SQL SELECT   MAX(NSEQNQ ) ,                                     
                             MAX(NSEQENS )                                      
                      INTO   :VT17-NSEQNQ :VT17-NSEQNQ-F ,                      
                             :VT17-NSEQENS  :VT17-NSEQENS-F                     
                      FROM   RVVT1700                                           
                      WHERE  NFLAG     = :VT17-NFLAG                            
                      AND    NCLUSTER  = :VT17-NCLUSTER                         
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           MOVE VT11-NSEQNQ  TO WS-NSEQNQ.                                      
           MOVE VT11-NSEQENS TO WS-NSEQENS1.                                    
      *                                                                         
           IF WS-NSEQNQ < VT12-NSEQNQ                                           
              MOVE VT12-NSEQNQ TO WS-NSEQNQ                                     
           END-IF.                                                              
           IF WS-NSEQNQ < VT13-NSEQNQ                                           
              MOVE VT13-NSEQNQ TO WS-NSEQNQ                                     
           END-IF.                                                              
           IF WS-NSEQNQ < VT15-NSEQNQ                                           
              MOVE VT15-NSEQNQ TO WS-NSEQNQ                                     
           END-IF.                                                              
           IF WS-NSEQNQ < VT16-NSEQNQ                                           
              MOVE VT16-NSEQNQ TO WS-NSEQNQ                                     
           END-IF.                                                              
           IF WS-NSEQNQ < VT17-NSEQNQ                                           
              MOVE VT17-NSEQNQ TO WS-NSEQNQ                                     
           END-IF.                                                              
      *                                                                         
           IF WS-NSEQENS1 < VT12-NSEQENS                                        
              MOVE VT12-NSEQENS TO WS-NSEQENS1                                  
           END-IF.                                                              
           IF WS-NSEQENS1 < VT13-NSEQENS                                        
              MOVE VT13-NSEQENS TO WS-NSEQENS1                                  
           END-IF.                                                              
      *                                                                         
           IF WS-NSEQENS1 < VT15-NSEQENS                                        
              MOVE VT15-NSEQENS TO WS-NSEQENS1                                  
           END-IF.                                                              
      *                                                                         
           IF WS-NSEQENS1 < VT16-NSEQENS                                        
              MOVE VT16-NSEQENS TO WS-NSEQENS1                                  
           END-IF.                                                              
      *                                                                         
           IF WS-NSEQENS1 < VT17-NSEQENS                                        
              MOVE VT17-NSEQENS TO WS-NSEQENS1                                  
           END-IF.                                                              
      *                                                                         
      *                                                                         
       FIN-SELECT-MAX-NSEQ. EXIT.                                               
      *                                                                         
      *                                                                         
       UPDATE-RTGV11                   SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-UPDATE        TO TRACE-SQL-FUNCTION.                       
      *                                                                         
           EXEC SQL UPDATE RVGV1106                                             
                SET   LCOMMENT = :GV08-VALCTRL ,                                
                      WUNITR   = :GV08-VALCTRL                                  
                WHERE NSOCIETE = :GV08-NSOCIETE                                 
                AND   NLIEU    = :GV08-NLIEU                                    
                AND   NVENTE   = :GV08-NVENTE                                   
                AND   NSEQNQ   = :GV08-NSEQ                                     
           END-EXEC.                                                            
      *                                                                         
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-UPDATE-RTGV11. EXIT.                                                 
      *                                                                         
      *                                                                         
       UPD-NSEQNQ-GV10                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-UPDATE        TO TRACE-SQL-FUNCTION.                       
           MOVE COMM-BS42-NSOCIETE TO GV10-NSOCIETE.                            
           MOVE COMM-BS42-NLIEU    TO GV10-NLIEU.                               
           MOVE COMM-BS42-NVENTE   TO GV10-NVENTE.                              
      *                                                                         
           EXEC SQL UPDATE RVGV1005                                             
                SET   NSEQNQ   = :WS-NSEQNQ ,                                   
                      NSEQENS  = :WS-NSEQENS1                                   
                WHERE NSOCIETE = :GV10-NSOCIETE                                 
                AND   NLIEU    = :GV10-NLIEU                                    
                AND   NVENTE   = :GV10-NVENTE                                   
           END-EXEC.                                                            
      *                                                                         
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-UPD-NSEQNQ-GV10. EXIT.                                               
      *                                                                         
      *                                                                         
       UPD-NSEQNQ-VE10                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-UPDATE        TO TRACE-SQL-FUNCTION.                       
           MOVE COMM-BS42-NSOCIETE TO VE10-NSOCIETE.                            
           MOVE COMM-BS42-NLIEU    TO VE10-NLIEU.                               
           MOVE COMM-BS42-NVENTE   TO VE10-NVENTE.                              
      *                                                                         
           EXEC SQL UPDATE RVVE1000                                             
                SET   NSEQNQ   = :WS-NSEQNQ ,                                   
                      NSEQENS  = :WS-NSEQENS1                                   
                WHERE NSOCIETE = :VE10-NSOCIETE                                 
                AND   NLIEU    = :VE10-NLIEU                                    
                AND   NVENTE   = :VE10-NVENTE                                   
           END-EXEC.                                                            
      *                                                                         
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-UPD-NSEQNQ-VE10. EXIT.                                               
      *                                                                         
      *                                                                         
       SELECT-RTGV10-2                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE COMM-BS42-NSOCIETE     TO GV10-NSOCIETE                         
           MOVE COMM-BS42-NLIEU        TO GV10-NLIEU                            
           MOVE VT01-DVENTE            TO GV10-DVENTE                           
           MOVE '00000'                TO GV10-NORDRE                           
           MOVE FUNC-SELECT            TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-GV1000                                                  
           EXEC SQL SELECT   MAX(NORDRE)                                        
                    INTO    :GV10-NORDRE    :GV10-NORDRE-F                      
                    FROM     RVGV1005                                           
                    WHERE    NSOCIETE = :GV10-NSOCIETE                          
                    AND      NLIEU    = :GV10-NLIEU                             
                    AND      DVENTE   = :GV10-DVENTE                            
                    AND      NORDRE   > '00000'                                 
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-SELECT-RTGV10-2. EXIT.                                               
      *                                                                         
      *                                                                         
       SELECT-RTVE10-2                 SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE COMM-BS42-NSOCIETE     TO VE10-NSOCIETE                         
           MOVE COMM-BS42-NLIEU        TO VE10-NLIEU                            
           MOVE VT01-DVENTE            TO VE10-DVENTE                           
           MOVE '00000'                TO VE10-NORDRE                           
           MOVE FUNC-SELECT            TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VE1000                                                  
           EXEC SQL SELECT   MAX(NORDRE)                                        
                    INTO    :VE10-NORDRE    :VE10-NORDRE-F                      
                    FROM     RVVE1000                                           
                    WHERE    NSOCIETE = :VE10-NSOCIETE                          
                    AND      NLIEU    = :VE10-NLIEU                             
                    AND      DVENTE   = :VE10-DVENTE                            
                    AND      NORDRE   > '00000'                                 
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-SELECT-RTVE10-2. EXIT.                                               
      *                                                                         
      *                                                                         
      *                                                                         
       SELECT-CVENDEUR-ARCH            SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-SELECT            TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-GA0100.                                                 
           EXEC SQL SELECT   CTABLEG2                                           
                    INTO    :GA01-CTABLEG2                                      
                    FROM     RVGA0100                                           
                    WHERE    CTABLEG1 = 'VESPE'                                 
                    AND      SUBSTR(WTABLEG, 31, 1) = 'O'                       
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
      *            VENDEUR NON TROUVE                                           
              SET NON-OK-MAJ-ARTICLES TO TRUE                                   
              SET FIN-TS              TO TRUE                                   
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              MOVE GA01-CTABLEG2      TO VESPE-CVESPE                           
           END-IF.                                                              
      *                                                                         
       FIN-SELECT-CVENDEUR-ARCH. EXIT.                                          
      *                                                                         
      *                                                                         
      *                                                                         
       DECLARE-RTVT02                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      **** LECTURE DES LIGNES ADRESSE DE LA VENTE                               
           MOVE WS-NCLUSTER            TO VT02-NCLUSTER                         
           MOVE WS-NFLAG               TO VT02-NFLAG                            
           MOVE FUNC-DECLARE           TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0200                                                  
           EXEC SQL DECLARE  CADRESSE   CURSOR FOR                              
                    SELECT      NCLUSTER     ,                                  
                                WTYPEADR     ,                                  
                                CTITRENOM    ,                                  
                                LNOM         ,                                  
                                LPRENOM      ,                                  
                                LBATIMENT    ,                                  
                                LESCALIER    ,                                  
                                LETAGE       ,                                  
                                LPORTE       ,                                  
                                LCMPAD1     ,                                   
                                CVOIE        ,                                  
                                CTVOIE       ,                                  
                                LNOMVOIE     ,                                  
                                LCOMMUNE     ,                                  
                                CPOSTAL      ,                                  
                                TELDOM       ,                                  
                                TELBUR       ,                                  
                                WEXPTAXE     ,                                  
VT02                            NORDRE       ,                                  
VT02                            IDCLIENT     ,                                  
VT02                            CPAYS        ,                                  
VT02                            NGSM                                            
                    FROM     RVVT0200                                           
                    WHERE    NCLUSTER = :VT02-NCLUSTER                          
                      AND    NFLAG    = :VT02-NFLAG                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           MOVE FUNC-OPEN              TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0200                                                  
           EXEC SQL OPEN   CADRESSE                                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DECLARE-RTVT02. EXIT.                                                
      *                                                                         
       FETCH-RTVT02                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-FETCH             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0200                                                  
           EXEC SQL FETCH   CADRESSE                                            
      *             INTO   :RVVT0200                                            
                    INTO  :VT02-NCLUSTER     ,                                  
                          :VT02-WTYPEADR     ,                                  
                          :VT02-CTITRENOM    ,                                  
                          :VT02-LNOM         ,                                  
                          :VT02-LPRENOM      ,                                  
                          :VT02-LBATIMENT    ,                                  
                          :VT02-LESCALIER    ,                                  
                          :VT02-LETAGE       ,                                  
                          :VT02-LPORTE       ,                                  
                          :VT02-LCMPAD1     ,                                   
                          :VT02-CVOIE        ,                                  
                          :VT02-CTVOIE       ,                                  
                          :VT02-LNOMVOIE     ,                                  
                          :VT02-LCOMMUNE     ,                                  
                          :VT02-CPOSTAL      ,                                  
                          :VT02-TELDOM       ,                                  
                          :VT02-TELBUR       ,                                  
                          :VT02-WEXPTAXE     ,                                  
VT02                      :VT02-NORDRE       ,                                  
VT02                      :VT02-IDCLIENT     ,                                  
VT02                      :VT02-CPAYS        ,                                  
VT02                      :VT02-NGSM                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-FETCH-RTVT02. EXIT.                                                  
      *                                                                         
       CLOSE-RTVT02                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-CLOSE             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0200                                                  
           EXEC SQL CLOSE CADRESSE                                              
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-CLOSE-RTVT02. EXIT.                                                  
      *                                                                         
      *                                                                         
       DECLARE-ARTICLES                SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      **** LECTURE DES LIGNES CODIC DE LA VENTE                                 
           MOVE WS-NCLUSTER            TO VT11-NCLUSTER                         
                                          VT16-NCLUSTER                         
                                          VT12-NCLUSTER                         
                                          VT17-NCLUSTER                         
                                          VT13-NCLUSTER                         
                                          VT15-NCLUSTER                         
           MOVE WS-NFLAG               TO VT11-NFLAG                            
                                          VT16-NFLAG                            
                                          VT12-NFLAG                            
                                          VT17-NFLAG                            
                                          VT13-NFLAG                            
                                          VT15-NFLAG                            
           MOVE FUNC-DECLARE           TO TRACE-SQL-FUNCTION                    
           EXEC SQL DECLARE  CARTICLE CURSOR FOR                                
                    SELECT   '1'          ,                                     
                             NCODIC       ,                                     
                             CMODDEL      ,                                     
                             DDELIV       ,                                     
                             '     '      ,                                     
                             QVENDUE      ,                                     
                             PVUNIT       ,                                     
                             PVUNITF      ,                                     
                             0            ,                                     
                             NLIGNE       ,                                     
                             TAUXTVA      ,                                     
                             QCONDT       ,                                     
                             '   '        ,                                     
                             '          ' ,                                     
CVEND                        CVENDEUR     ,                                     
                             CANNULREP    ,                                     
                             NCODICGRP    ,                                     
                             CTYPENT      ,                                     
                             NLIEN        ,                                     
                             NACTVTE      ,                                     
                             QVENDUEGRP   ,                                     
                             PVUNITGRP    ,                                     
                             NSEQNQ       ,                                     
                             NSEQREF      ,                                     
                             NSEQENS      ,                                     
                             MPRIMECLI                                          
                    FROM     RVVT1100                                           
                    WHERE    NCLUSTER = :VT11-NCLUSTER                          
                      AND    NFLAG    = :VT11-NFLAG                             
              UNION                                                             
                    SELECT   '3'          ,                                     
                             '       '    ,                                     
                             '   '        ,                                     
                             DDELIV       ,                                     
                             CENREG       ,                                     
                             1            ,                                     
                             PVTOTAL      ,                                     
                             PVTOTAL      ,                                     
                             PVTOTAL      ,                                     
                             NLIGNE       ,                                     
                             TAUXTVA      ,                                     
                             0            ,                                     
                             NLCODIC      ,                                     
                             NPSE         ,                                     
CVEND                        CVENDEUR     ,                                     
                             CANNULREP    ,                                     
                             '       '    ,                                     
                             CTYPENT      ,                                     
                             NLIEN        ,                                     
                             NACTVTE      ,                                     
                             0            ,                                     
                             0            ,                                     
                             NSEQNQ       ,                                     
                             NSEQREF      ,                                     
                             NSEQENS      ,                                     
                             MPRIMECLI                                          
                    FROM     RVVT1600                                           
                    WHERE    NCLUSTER = :VT16-NCLUSTER                          
                      AND    NFLAG    = :VT16-NFLAG                             
              UNION                                                             
                    SELECT   '2'          ,                                     
                             '       '    ,                                     
                             '   '        ,                                     
                             DDELIV       ,                                     
                             CENREG       ,                                     
                             QVENDUE      ,                                     
                             0            ,                                     
                             0            ,                                     
                             PVTOTAL      ,                                     
                             NLIGNE       ,                                     
                             TAUXTVA      ,                                     
                             0            ,                                     
                             NLCODIC      ,                                     
                             '          ' ,                                     
CVEND                        CVENDEUR     ,                                     
                             CANNULREP    ,                                     
                             '       '    ,                                     
                             CTYPENT      ,                                     
                             NLIEN        ,                                     
                             NACTVTE      ,                                     
                             0            ,                                     
                             0            ,                                     
                             NSEQNQ       ,                                     
                             NSEQREF      ,                                     
                             NSEQENS      ,                                     
                             MPRIMECLI                                          
                    FROM     RVVT1200                                           
                    WHERE    NCLUSTER = :VT12-NCLUSTER                          
                      AND    NFLAG    = :VT12-NFLAG                             
              UNION                                                             
                    SELECT   '1'          ,                                     
                             NCODIC       ,                                     
                             CMODDEL      ,                                     
                             DDELIV       ,                                     
                             CENREG       ,                                     
                             1            ,                                     
                             PVTOTAL      ,                                     
                             PVTOTAL      ,                                     
                             PVTOTAL      ,                                     
                             NLIGNE       ,                                     
                             TAUXTVA      ,                                     
                             0            ,                                     
                             NLCODIC      ,                                     
                             LCOMMENT     ,                                     
CVEND                        CVENDEUR     ,                                     
                             CANNULREP    ,                                     
                             '       '    ,                                     
                             CTYPENT      ,                                     
                             NLIEN        ,                                     
                             NACTVTE      ,                                     
                             0            ,                                     
                             0            ,                                     
                             NSEQNQ       ,                                     
                             NSEQREF      ,                                     
                             NSEQENS      ,                                     
                             MPRIMECLI                                          
                    FROM     RVVT1700                                           
                    WHERE    NCLUSTER = :VT17-NCLUSTER                          
                      AND    NFLAG    = :VT17-NFLAG                             
              UNION                                                             
                    SELECT   '4'          ,                                     
                             '       '    ,                                     
                             '   '        ,                                     
                             DDELIV       ,                                     
                             CENREG       ,                                     
                             QVENDUE      ,                                     
                             PVUNIT       ,                                     
                             0            ,                                     
                             PVTOTAL      ,                                     
                             NLIGNE       ,                                     
                             TAUXTVA      ,                                     
                             0            ,                                     
                             NLCODIC      ,                                     
                             NCONTRAT     ,                                     
CVEND                        CVENDEUR     ,                                     
                             CANNULREP    ,                                     
                             '       '    ,                                     
                             CTYPENT      ,                                     
                             NLIEN        ,                                     
                             NACTVTE      ,                                     
                             0            ,                                     
                             0            ,                                     
                             NSEQNQ       ,                                     
                             NSEQREF      ,                                     
                             NSEQENS      ,                                     
                             MPRIMECLI                                          
                    FROM     RVVT1300                                           
                    WHERE    NCLUSTER = :VT13-NCLUSTER                          
                      AND    NFLAG    = :VT13-NFLAG                             
              UNION                                                             
                    SELECT   '4'          ,                                     
                             '       '    ,                                     
                             '   '        ,                                     
                             DDELIV       ,                                     
                             CPRESTATION  ,                                     
                             1            ,                                     
                             0            ,                                     
                             0            ,                                     
                             PVTOTAL      ,                                     
                             NLIGNE       ,                                     
                             TAUXTVA      ,                                     
                             0            ,                                     
                             '   '        ,                                     
                             NCONTRAT     ,                                     
CVEND                        CVENDEUR     ,                                     
                             CANNULREP    ,                                     
                             '       '    ,                                     
                             CTYPENT      ,                                     
                             NLIEN        ,                                     
                             NACTVTE      ,                                     
                             0            ,                                     
                             0            ,                                     
                             NSEQNQ       ,                                     
                             NSEQREF      ,                                     
                             NSEQENS      ,                                     
                             MPRIMECLI                                          
                    FROM     RVVT1500                                           
                    WHERE    NCLUSTER = :VT15-NCLUSTER                          
                      AND    NFLAG    = :VT15-NFLAG                             
                    ORDER BY 10                                                 
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           MOVE FUNC-OPEN              TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT1100                                                  
           EXEC SQL OPEN   CARTICLE                                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DECLARE-ARTICLES. EXIT.                                              
      *                                                                         
       FETCH-ARTICLES                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-FETCH             TO TRACE-SQL-FUNCTION                    
           EXEC SQL FETCH   CARTICLE                                            
                    INTO   :GV11-CTYPENREG ,                                    
                           :GV11-NCODIC    ,                                    
                           :GV11-CMODDEL   ,                                    
                           :GV11-DDELIV    ,                                    
                           :GV11-CENREG    ,                                    
                           :GV11-QVENDUE   ,                                    
                           :GV11-PVUNIT    ,                                    
                           :GV11-PVUNITF   ,                                    
                           :GV11-PVTOTAL   ,                                    
                           :WS-NLIGNE      ,                                    
                           :GV11-TAUXTVA   ,                                    
                           :GV11-QCONDT    ,                                    
                           :WS-NLCODIC     ,                                    
                           :GV11-LCOMMENT  ,                                    
CVEND                      :GV11-CVENDEUR  ,                                    
                           :WS-CANNULREP   ,                                    
                           :GV11-NCODICGRP ,                                    
                           :GV11-CTYPENT   ,                                    
                           :GV11-NLIEN     ,                                    
                           :GV11-NACTVTE   ,                                    
                           :GV11-QTCODIG   ,                                    
                           :GV11-PVCODIG   ,                                    
                           :GV11-NSEQNQ    ,                                    
                           :GV11-NSEQREF   ,                                    
                           :GV11-NSEQENS   ,                                    
                           :GV11-MPRIMECLI                                      
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-FETCH-ARTICLES. EXIT.                                                
      *                                                                         
       CLOSE-ARTICLES                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-CLOSE             TO TRACE-SQL-FUNCTION                    
           EXEC SQL CLOSE CARTICLE                                              
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-CLOSE-ARTICLES. EXIT.                                                
      *                                                                         
      *                                                                         
       DECLARE-RTVT14                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
      **** LECTURE DES LIGNES DE LA VENTE ORDONNEES PAR NLIGNE                  
           MOVE WS-NCLUSTER            TO VT14-NCLUSTER                         
           MOVE WS-NFLAG               TO VT14-NFLAG                            
           MOVE FUNC-DECLARE           TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT1400                                                  
           EXEC SQL DECLARE  CREGLEMENT CURSOR FOR                              
                    SELECT   NCLUSTER     ,                                     
                             NSEQ         ,                                     
                             DSAISIE      ,                                     
                             CMODPAIMT    ,                                     
                             PREGLTVTE    ,                                     
                             NREGLTVTE    ,                                     
                             DREGLTVTE    ,                                     
                             WREGLTVTE    ,                                     
                             NSOCP        ,                                     
                             NLIEUP       ,                                     
                             NTRANS       ,                                     
                             CVENDEUR     ,                                     
                             CFCRED       ,                                     
                             NCREDI                                             
                    FROM     RVVT1400                                           
                    WHERE    NCLUSTER = :VT14-NCLUSTER                          
                      AND    NFLAG    = :VT14-NFLAG                             
                    ORDER BY NSEQ                                               
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           MOVE FUNC-OPEN              TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT1400                                                  
           EXEC SQL OPEN   CREGLEMENT                                           
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DECLARE-RTVT14. EXIT.                                                
      *                                                                         
       FETCH-RTVT14                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-FETCH             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT1400                                                  
           EXEC SQL FETCH   CREGLEMENT                                          
                    INTO                                                        
                       :VT14-NCLUSTER     ,                                     
                       :VT14-NSEQ         ,                                     
                       :VT14-DSAISIE      ,                                     
                       :VT14-CMODPAIMT    ,                                     
                       :VT14-PREGLTVTE    ,                                     
                       :VT14-NREGLTVTE    ,                                     
                       :VT14-DREGLTVTE    ,                                     
                       :VT14-WREGLTVTE    ,                                     
                       :VT14-NSOCP        ,                                     
                       :VT14-NLIEUP       ,                                     
                       :VT14-NTRANS       ,                                     
                       :VT14-CVENDEUR     ,                                     
                       :VT14-CFCRED       ,                                     
                       :VT14-NCREDI                                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-FETCH-RTVT14. EXIT.                                                  
      *                                                                         
       CLOSE-RTVT14                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-CLOSE             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT1400                                                  
           EXEC SQL CLOSE CREGLEMENT                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-CLOSE-RTVT14. EXIT.                                                  
      *                                                                         
      *                                                                         
       DECLARE-RTVT05                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE WS-NCLUSTER            TO VT05-NCLUSTER                         
           MOVE WS-NFLAG               TO VT05-NFLAG                            
           MOVE FUNC-DECLARE           TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0500                                                  
           EXEC SQL DECLARE  CDOSSIER5  CURSOR FOR                              
AD01                SELECT   *                                                  
                    FROM     RVVT0500                                           
                    WHERE    NCLUSTER = :VT05-NCLUSTER                          
                      AND    NFLAG    = :VT05-NFLAG                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           MOVE FUNC-OPEN              TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0500                                                  
           EXEC SQL OPEN   CDOSSIER5                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DECLARE-RTVT05. EXIT.                                                
      *                                                                         
       FETCH-RTVT05                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-FETCH             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0500                                                  
           EXEC SQL FETCH   CDOSSIER5                                           
                    INTO   :RVVT0500                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-FETCH-RTVT05. EXIT.                                                  
      *                                                                         
       CLOSE-RTVT05                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-CLOSE             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0500                                                  
           EXEC SQL CLOSE CDOSSIER5                                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-CLOSE-RTVT05. EXIT.                                                  
      *                                                                         
      *                                                                         
       DECLARE-RTVT06                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE WS-NCLUSTER            TO VT06-NCLUSTER                         
           MOVE WS-NFLAG               TO VT06-NFLAG                            
           MOVE FUNC-DECLARE           TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0600                                                  
           EXEC SQL DECLARE  CDOSSIER6  CURSOR FOR                              
AD01                SELECT   *                                                  
                    FROM     RVVT0600                                           
                    WHERE    NCLUSTER = :VT06-NCLUSTER                          
                      AND    NFLAG    = :VT06-NFLAG                             
                    ORDER BY NSEQ                                               
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           MOVE FUNC-OPEN              TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0600                                                  
           EXEC SQL OPEN   CDOSSIER6                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DECLARE-RTVT06. EXIT.                                                
      *                                                                         
       FETCH-RTVT06                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-FETCH             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0600                                                  
           EXEC SQL FETCH   CDOSSIER6                                           
                    INTO   :RVVT0600                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-FETCH-RTVT06. EXIT.                                                  
      *                                                                         
       CLOSE-RTVT06                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-CLOSE             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0600                                                  
           EXEC SQL CLOSE CDOSSIER6                                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-CLOSE-RTVT06. EXIT.                                                  
      *                                                                         
      *                                                                         
       DECLARE-RTVT08                  SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE WS-NCLUSTER            TO VT08-NCLUSTER                         
           MOVE WS-NFLAG               TO VT08-NFLAG                            
           MOVE FUNC-DECLARE           TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0800                                                  
           EXEC SQL DECLARE  CDOSSIER8  CURSOR FOR                              
AD01                SELECT   *                                                  
                    FROM     RVVT0800                                           
                    WHERE    NCLUSTER = :VT08-NCLUSTER                          
                      AND    NFLAG    = :VT08-NFLAG                             
                    ORDER BY NSEQ                                               
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL                                         
           MOVE FUNC-OPEN              TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0800                                                  
           EXEC SQL OPEN   CDOSSIER8                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DECLARE-RTVT08. EXIT.                                                
      *                                                                         
       FETCH-RTVT08                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-FETCH             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0800                                                  
           EXEC SQL FETCH   CDOSSIER8                                           
                    INTO   :RVVT0800                                            
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-FETCH-RTVT08. EXIT.                                                  
      *                                                                         
       CLOSE-RTVT08                    SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE FUNC-CLOSE             TO TRACE-SQL-FUNCTION                    
           PERFORM CLEF-VT0800                                                  
           EXEC SQL CLOSE CDOSSIER8                                             
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-CLOSE-RTVT08. EXIT.                                                  
      *                                                                         
      *                                                                         
      ******************************************************************        
      *                                                                *        
      *                       INSERT DB2                               *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       INSERT-ENTETE-GV             SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GV1000                                                  
           EXEC SQL INSERT                                                      
                    INTO    RVGV1005                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NORDRE       ,                                         
                         NCLIENT      ,                                         
                         DVENTE       ,                                         
                         DHVENTE      ,                                         
                         DMVENTE      ,                                         
                         PTTVENTE     ,                                         
                         PVERSE       ,                                         
                         PCOMPT       ,                                         
                         PLIVR        ,                                         
                         PDIFFERE     ,                                         
                         PRFACT       ,                                         
                         CREMVTE      ,                                         
                         PREMVTE      ,                                         
                         LCOMVTE1     ,                                         
                         LCOMVTE2     ,                                         
                         LCOMVTE3     ,                                         
                         LCOMVTE4     ,                                         
                         DMODIFVTE    ,                                         
                         WFACTURE     ,                                         
                         WEXPORT      ,                                         
                         WDETAXEC     ,                                         
                         WDETAXEHC    ,                                         
                         CORGORED     ,                                         
                         CMODPAIMTI   ,                                         
                         LDESCRIPTIF1 ,                                         
                         LDESCRIPTIF2 ,                                         
                         DLIVRBL      ,                                         
                         NFOLIOBL     ,                                         
                         LAUTORM      ,                                         
                         NAUTORD      ,                                         
                         DSYST        ,                                         
                         DSTAT        ,                                         
                         DFACTURE     ,                                         
MH0303                   CACID        ,                                         
  "                      NSOCMODIF    ,                                         
  "                      NLIEUMODIF   ,                                         
  "                      NSOCP        ,                                         
  "                      NLIEUP       ,                                         
  "                      CDEV         ,                                         
  "                      CFCRED       ,                                         
  "                      NCREDI       ,                                         
  "                      NSEQNQ       ,                                         
  "                      TYPVTE       ,                                         
  "                      VTEGPE       ,                                         
  "                      CTRMRQ       ,                                         
  "                      DATENC       ,                                         
  "                      WDGRAD       ,                                         
  "                      NAUTO        ,                                         
  "                      NSEQENS      ,                                         
  "                      NSOCO        ,                                         
  "                      NLIEUO       ,                                         
MH0303                   NVENTO       )                                         
                    VALUES (                                                    
                   :GV10-NSOCIETE     ,                                         
                   :GV10-NLIEU        ,                                         
                   :GV10-NVENTE       ,                                         
                   :GV10-NORDRE       ,                                         
                   :GV10-NCLIENT      ,                                         
                   :GV10-DVENTE       ,                                         
                   :GV10-DHVENTE      ,                                         
                   :GV10-DMVENTE      ,                                         
                   :GV10-PTTVENTE     ,                                         
                   :GV10-PVERSE       ,                                         
                   :GV10-PCOMPT       ,                                         
                   :GV10-PLIVR        ,                                         
                   :GV10-PDIFFERE     ,                                         
                   :GV10-PRFACT       ,                                         
                   :GV10-CREMVTE      ,                                         
                   :GV10-PREMVTE      ,                                         
                   :GV10-LCOMVTE1     ,                                         
                   :GV10-LCOMVTE2     ,                                         
                   :GV10-LCOMVTE3     ,                                         
                   :GV10-LCOMVTE4     ,                                         
                   :GV10-DMODIFVTE    ,                                         
                   :GV10-WFACTURE     ,                                         
                   :GV10-WEXPORT      ,                                         
                   :GV10-WDETAXEC     ,                                         
                   :GV10-WDETAXEHC    ,                                         
                   :GV10-CORGORED     ,                                         
                   :GV10-CMODPAIMTI   ,                                         
                   :GV10-LDESCRIPTIF1 ,                                         
                   :GV10-LDESCRIPTIF2 ,                                         
                   :GV10-DLIVRBL      ,                                         
                   :GV10-NFOLIOBL     ,                                         
                   :GV10-LAUTORM      ,                                         
                   :GV10-NAUTORD      ,                                         
                   :GV10-DSYST        ,                                         
                   :GV10-DSTAT        ,                                         
                   :WS-DFACTURE       ,                                         
MH0303             :GV10-CACID        ,                                         
  "                :GV10-NSOCMODIF    ,                                         
  "                :GV10-NLIEUMODIF   ,                                         
  "                :GV10-NSOCP        ,                                         
  "                :GV10-NLIEUP       ,                                         
  "                :GV10-CDEV         ,                                         
  "                :GV10-CFCRED       ,                                         
  "                :GV10-NCREDI       ,                                         
  "                :GV10-NSEQNQ       ,                                         
  "                :GV10-TYPVTE       ,                                         
  "                :GV10-VTEGPE       ,                                         
  "                :GV10-CTRMRQ       ,                                         
  "                :GV10-DATENC       ,                                         
  "                :GV10-WDGRAD       ,                                         
  "                :GV10-NAUTO        ,                                         
  "                :GV10-NSEQENS      ,                                         
  "                :GV10-NSOCO        ,                                         
  "                :GV10-NLIEUO       ,                                         
MH0303             :GV10-NVENTO       )                                         
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF EXISTE-DEJA  THEN                                                 
      *             UPDATE RTGV10 CLE EN DOUBLE                                 
              SET NON-OK-MAJ-ENTETE TO TRUE                                     
                 STRING 'PB INSERT RTGV10 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-ENTETE TO TRUE                                         
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-ENTETE-GV. EXIT.                                              
      *                                                                         
      *                                                                         
       INSERT-ENTETE-VE             SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-VE1000                                                  
           EXEC SQL INSERT                                                      
                    INTO    RVVE1000                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NORDRE       ,                                         
                         NCLIENT      ,                                         
                         DVENTE       ,                                         
                         DHVENTE      ,                                         
                         DMVENTE      ,                                         
                         PTTVENTE     ,                                         
                         PVERSE       ,                                         
                         PCOMPT       ,                                         
                         PLIVR        ,                                         
                         PDIFFERE     ,                                         
                         PRFACT       ,                                         
                         CREMVTE      ,                                         
                         PREMVTE      ,                                         
                         LCOMVTE1     ,                                         
                         LCOMVTE2     ,                                         
                         LCOMVTE3     ,                                         
                         LCOMVTE4     ,                                         
                         DMODIFVTE    ,                                         
                         WFACTURE     ,                                         
                         WEXPORT      ,                                         
                         WDETAXEC     ,                                         
                         WDETAXEHC    ,                                         
                         CORGORED     ,                                         
                         CMODPAIMTI   ,                                         
                         LDESCRIPTIF1 ,                                         
                         LDESCRIPTIF2 ,                                         
                         DLIVRBL      ,                                         
                         NFOLIOBL     ,                                         
                         LAUTORM      ,                                         
                         NAUTORD      ,                                         
                         DSYST        ,                                         
                         DSTAT        ,                                         
                         DFACTURE     ,                                         
MH0303                   CACID        ,                                         
  "                      NSOCMODIF    ,                                         
  "                      NLIEUMODIF   ,                                         
  "                      NSOCP        ,                                         
  "                      NLIEUP       ,                                         
  "                      CDEV         ,                                         
  "                      CFCRED       ,                                         
  "                      NCREDI       ,                                         
  "                      NSEQNQ       ,                                         
  "                      TYPVTE       ,                                         
  "                      VTEGPE       ,                                         
  "                      CTRMRQ       ,                                         
  "                      DATENC       ,                                         
  "                      WDGRAD       ,                                         
  "                      NAUTO        ,                                         
  "                      NSEQENS      ,                                         
  "                      NSOCO        ,                                         
  "                      NLIEUO       ,                                         
MH0303                   NVENTO       )                                         
                    VALUES (                                                    
                   :VE10-NSOCIETE     ,                                         
                   :VE10-NLIEU        ,                                         
                   :VE10-NVENTE       ,                                         
                   :VE10-NORDRE       ,                                         
                   :VE10-NCLIENT      ,                                         
                   :VE10-DVENTE       ,                                         
                   :VE10-DHVENTE      ,                                         
                   :VE10-DMVENTE      ,                                         
                   :VE10-PTTVENTE     ,                                         
                   :VE10-PVERSE       ,                                         
                   :VE10-PCOMPT       ,                                         
                   :VE10-PLIVR        ,                                         
                   :VE10-PDIFFERE     ,                                         
                   :VE10-PRFACT       ,                                         
                   :VE10-CREMVTE      ,                                         
                   :VE10-PREMVTE      ,                                         
                   :VE10-LCOMVTE1     ,                                         
                   :VE10-LCOMVTE2     ,                                         
                   :VE10-LCOMVTE3     ,                                         
                   :VE10-LCOMVTE4     ,                                         
                   :VE10-DMODIFVTE    ,                                         
                   :VE10-WFACTURE     ,                                         
                   :VE10-WEXPORT      ,                                         
                   :VE10-WDETAXEC     ,                                         
                   :VE10-WDETAXEHC    ,                                         
                   :VE10-CORGORED     ,                                         
                   :VE10-CMODPAIMTI   ,                                         
                   :VE10-LDESCRIPTIF1 ,                                         
                   :VE10-LDESCRIPTIF2 ,                                         
                   :VE10-DLIVRBL      ,                                         
                   :VE10-NFOLIOBL     ,                                         
                   :VE10-LAUTORM      ,                                         
                   :VE10-NAUTORD      ,                                         
                   :VE10-DSYST        ,                                         
                   :VE10-DSTAT        ,                                         
                   :WS-DFACTURE       ,                                         
MH0303             :VE10-CACID        ,                                         
  "                :VE10-NSOCMODIF    ,                                         
  "                :VE10-NLIEUMODIF   ,                                         
  "                :VE10-NSOCP        ,                                         
  "                :VE10-NLIEUP       ,                                         
  "                :VE10-CDEV         ,                                         
  "                :VE10-CFCRED       ,                                         
  "                :VE10-NCREDI       ,                                         
  "                :VE10-NSEQNQ       ,                                         
  "                :VE10-TYPVTE       ,                                         
  "                :VE10-VTEGPE       ,                                         
  "                :VE10-CTRMRQ       ,                                         
  "                :VE10-DATENC       ,                                         
  "                :VE10-WDGRAD       ,                                         
  "                :VE10-NAUTO        ,                                         
  "                :VE10-NSEQENS      ,                                         
  "                :VE10-NSOCO        ,                                         
  "                :VE10-NLIEUO       ,                                         
MH0303             :VE10-NVENTO       )                                         
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF EXISTE-DEJA  THEN                                                 
      *             UPDATE RTVE10 CLE EN DOUBLE                                 
              SET NON-OK-MAJ-ENTETE TO TRUE                                     
                 STRING 'PB INSERT RTVE10 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-ENTETE TO TRUE                                         
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-ENTETE-VE. EXIT.                                              
      *                                                                         
      *                                                                         
       INSERT-REGLEMENT-GV          SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GV1400                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVGV1404                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NORDRE       ,                                         
                         NVENTE       ,                                         
                         DSAISIE      ,                                         
                         NSEQ         ,                                         
                         CMODPAIMT    ,                                         
                         PREGLTVTE    ,                                         
                         NREGLTVTE    ,                                         
                         DREGLTVTE    ,                                         
                         WREGLTVTE    ,                                         
                         DCOMPTA      ,                                         
                         DSYST        ,                                         
                         CPROTOUR     ,                                         
                         CTOURNEE     ,                                         
                         CDEVISE      ,                                         
                         MDEVISE      ,                                         
                         MECART       ,                                         
                         CDEV         ,                                         
                         NSOCP        ,                                         
                         NLIEUP       ,                                         
                         NTRANS       ,                                         
                         PMODPAIMT    ,                                         
                         CVENDEUR     ,                                         
                         CFCRED       ,                                         
                         NCREDI       )                                         
                    VALUES (                                                    
                        :GV14-NSOCIETE     ,                                    
                        :GV14-NLIEU        ,                                    
                        :GV14-NORDRE       ,                                    
                        :GV14-NVENTE       ,                                    
                        :GV14-DSAISIE      ,                                    
                        :GV14-NSEQ         ,                                    
                        :GV14-CMODPAIMT    ,                                    
                        :GV14-PREGLTVTE    ,                                    
                        :GV14-NREGLTVTE    ,                                    
                        :GV14-DREGLTVTE    ,                                    
                        :GV14-WREGLTVTE    ,                                    
                        :GV14-DCOMPTA      ,                                    
                        :GV14-DSYST        ,                                    
                        :GV14-CPROTOUR     ,                                    
                        :GV14-CTOURNEE     ,                                    
                        :GV14-CDEVISE      ,                                    
                        :GV14-MDEVISE      ,                                    
                        :GV14-MECART       ,                                    
                        :GV14-CDEV         ,                                    
                        :GV14-NSOCP        ,                                    
                        :GV14-NLIEUP       ,                                    
                        :GV14-NTRANS       ,                                    
                        :GV14-PMODPAIMT    ,                                    
                        :GV14-CVENDEUR     ,                                    
                        :GV14-CFCRED       ,                                    
                        :GV14-NCREDI       )                                    
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA  THEN                                                 
      *           INSERT REGLEMENT MAL PASSE                                    
              SET NON-OK-MAJ-REGLEMENT TO TRUE                                  
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTGV14 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-REGLEMENT TO TRUE                                      
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-REGLEMENT-GV. EXIT.                                           
      *                                                                         
      *                                                                         
       INSERT-REGLEMENT-VE          SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-VE1400                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVVE1400                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NORDRE       ,                                         
                         NVENTE       ,                                         
                         DSAISIE      ,                                         
                         NSEQ         ,                                         
                         CMODPAIMT    ,                                         
                         PREGLTVTE    ,                                         
                         NREGLTVTE    ,                                         
                         DREGLTVTE    ,                                         
                         WREGLTVTE    ,                                         
                         DCOMPTA      ,                                         
                         DSYST        ,                                         
                         CPROTOUR     ,                                         
                         CTOURNEE     ,                                         
                         CDEVISE      ,                                         
                         MDEVISE      ,                                         
                         MECART       ,                                         
                         CDEV         ,                                         
                         NSOCP        ,                                         
                         NLIEUP       ,                                         
                         NTRANS       ,                                         
                         PMODPAIMT    ,                                         
                         CVENDEUR     ,                                         
                         CFCRED       ,                                         
                         NCREDI       )                                         
                    VALUES (                                                    
                        :VE14-NSOCIETE     ,                                    
                        :VE14-NLIEU        ,                                    
                        :VE14-NORDRE       ,                                    
                        :VE14-NVENTE       ,                                    
                        :VE14-DSAISIE      ,                                    
                        :VE14-NSEQ         ,                                    
                        :VE14-CMODPAIMT    ,                                    
                        :VE14-PREGLTVTE    ,                                    
                        :VE14-NREGLTVTE    ,                                    
                        :VE14-DREGLTVTE    ,                                    
                        :VE14-WREGLTVTE    ,                                    
                        :VE14-DCOMPTA      ,                                    
                        :VE14-DSYST        ,                                    
                        :VE14-CPROTOUR     ,                                    
                        :VE14-CTOURNEE     ,                                    
                        :VE14-CDEVISE      ,                                    
                        :VE14-MDEVISE      ,                                    
                        :VE14-MECART       ,                                    
                        :VE14-CDEV         ,                                    
                        :VE14-NSOCP        ,                                    
                        :VE14-NLIEUP       ,                                    
                        :VE14-NTRANS       ,                                    
                        :VE14-PMODPAIMT    ,                                    
                        :VE14-CVENDEUR     ,                                    
                        :VE14-CFCRED       ,                                    
                        :VE14-NCREDI       )                                    
           END-EXEC                                                             
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA  THEN                                                 
      *           INSERT REGLEMENT MAL PASSE                                    
              SET NON-OK-MAJ-REGLEMENT TO TRUE                                  
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTVE14 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-REGLEMENT TO TRUE                                      
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-REGLEMENT-VE. EXIT.                                           
      *                                                                         
      *                                                                         
       INSERT-ADRESSE-GV            SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GV0202                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVGV0202                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NORDRE       ,                                         
                         NVENTE       ,                                         
                         WTYPEADR     ,                                         
                         CTITRENOM    ,                                         
                         LNOM         ,                                         
                         LPRENOM      ,                                         
                         LBATIMENT    ,                                         
                         LESCALIER    ,                                         
                         LETAGE       ,                                         
                         LPORTE       ,                                         
                         LCMPAD1     ,                                          
                         LCMPAD2     ,                                          
                         CVOIE        ,                                         
                         CTVOIE       ,                                         
                         LNOMVOIE     ,                                         
                         LCOMMUNE     ,                                         
                         CPOSTAL      ,                                         
                         LBUREAU      ,                                         
                         TELDOM       ,                                         
                         TELBUR       ,                                         
                         LPOSTEBUR    ,                                         
                         LCOMLIV1     ,                                         
                         LCOMLIV2     ,                                         
                         WETRANGER    ,                                         
                         CZONLIV      ,                                         
                         CINSEE       ,                                         
                         WCONTRA      ,                                         
                         DSYST        ,                                         
                         CILOT        ,                                         
                         IDCLIENT     ,                                         
                         CPAYS        ,                                         
                         NGSM         )                                         
                    VALUES (                                                    
                        :GV02-NSOCIETE     ,                                    
                        :GV02-NLIEU        ,                                    
                        :GV02-NORDRE       ,                                    
                        :GV02-NVENTE       ,                                    
                        :GV02-WTYPEADR     ,                                    
                        :GV02-CTITRENOM    ,                                    
                        :GV02-LNOM         ,                                    
                        :GV02-LPRENOM      ,                                    
                        :GV02-LBATIMENT    ,                                    
                        :GV02-LESCALIER    ,                                    
                        :GV02-LETAGE       ,                                    
                        :GV02-LPORTE       ,                                    
                        :GV02-LCMPAD1     ,                                     
                        :GV02-LCMPAD2     ,                                     
                        :GV02-CVOIE        ,                                    
                        :GV02-CTVOIE       ,                                    
                        :GV02-LNOMVOIE     ,                                    
                        :GV02-LCOMMUNE     ,                                    
                        :GV02-CPOSTAL      ,                                    
                        :GV02-LBUREAU      ,                                    
                        :GV02-TELDOM       ,                                    
                        :GV02-TELBUR       ,                                    
                        :GV02-LPOSTEBUR    ,                                    
                        :GV02-LCOMLIV1     ,                                    
                        :GV02-LCOMLIV2     ,                                    
                        :GV02-WETRANGER    ,                                    
                        :GV02-CZONLIV      ,                                    
                        :GV02-CINSEE       ,                                    
                        :GV02-WCONTRA      ,                                    
                        :GV02-DSYST        ,                                    
                        :GV02-CILOT        ,                                    
                        :GV02-IDCLIENT     ,                                    
                        :GV02-CPAYS        ,                                    
                        :GV02-NGSM         )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT ADRESSE MAL PASSE                                      
              SET NON-OK-MAJ-ADRESSE TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTGV02 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-ADRESSE TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-ADRESSE-GV. EXIT.                                             
      *                                                                         
      *                                                                         
       INSERT-ADRESSE-VE            SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-VE0200                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVVE0200                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NORDRE       ,                                         
                         NVENTE       ,                                         
                         WTYPEADR     ,                                         
                         CTITRENOM    ,                                         
                         LNOM         ,                                         
                         LPRENOM      ,                                         
                         LBATIMENT    ,                                         
                         LESCALIER    ,                                         
                         LETAGE       ,                                         
                         LPORTE       ,                                         
                         LCMPAD1     ,                                          
                         LCMPAD2     ,                                          
                         CVOIE        ,                                         
                         CTVOIE       ,                                         
                         LNOMVOIE     ,                                         
                         LCOMMUNE     ,                                         
                         CPOSTAL      ,                                         
                         LBUREAU      ,                                         
                         TELDOM       ,                                         
                         TELBUR       ,                                         
                         LPOSTEBUR    ,                                         
                         LCOMLIV1     ,                                         
                         LCOMLIV2     ,                                         
                         WETRANGER    ,                                         
                         CZONLIV      ,                                         
                         CINSEE       ,                                         
                         WCONTRA      ,                                         
                         DSYST        ,                                         
                         CILOT        ,                                         
                         IDCLIENT     ,                                         
                         CPAYS        ,                                         
                         NGSM         )                                         
                    VALUES (                                                    
                        :VE02-NSOCIETE     ,                                    
                        :VE02-NLIEU        ,                                    
                        :VE02-NORDRE       ,                                    
                        :VE02-NVENTE       ,                                    
                        :VE02-WTYPEADR     ,                                    
                        :VE02-CTITRENOM    ,                                    
                        :VE02-LNOM         ,                                    
                        :VE02-LPRENOM      ,                                    
                        :VE02-LBATIMENT    ,                                    
                        :VE02-LESCALIER    ,                                    
                        :VE02-LETAGE       ,                                    
                        :VE02-LPORTE       ,                                    
                        :VE02-LCMPAD1     ,                                     
                        :VE02-LCMPAD2     ,                                     
                        :VE02-CVOIE        ,                                    
                        :VE02-CTVOIE       ,                                    
                        :VE02-LNOMVOIE     ,                                    
                        :VE02-LCOMMUNE     ,                                    
                        :VE02-CPOSTAL      ,                                    
                        :VE02-LBUREAU      ,                                    
                        :VE02-TELDOM       ,                                    
                        :VE02-TELBUR       ,                                    
                        :VE02-LPOSTEBUR    ,                                    
                        :VE02-LCOMLIV1     ,                                    
                        :VE02-LCOMLIV2     ,                                    
                        :VE02-WETRANGER    ,                                    
                        :VE02-CZONLIV      ,                                    
                        :VE02-CINSEE       ,                                    
                        :VE02-WCONTRA      ,                                    
                        :VE02-DSYST        ,                                    
                        :VE02-CILOT        ,                                    
                        :VE02-IDCLIENT     ,                                    
                        :VE02-CPAYS        ,                                    
                        :VE02-NGSM         )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT ADRESSE MAL PASSE                                      
              SET NON-OK-MAJ-ADRESSE TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTVE02 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-ADRESSE TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-ADRESSE-VE. EXIT.                                             
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
      *                                                                         
       INSERT-ARTICLES-GV           SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GV1100                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
EJ0301              INTO    RVGV1106                                            
                          (   NSOCIETE     ,                                    
                              NLIEU        ,                                    
                              NVENTE       ,                                    
                              CTYPENREG    ,                                    
                              NCODICGRP    ,                                    
                              NCODIC       ,                                    
                              NSEQ         ,                                    
                              CMODDEL      ,                                    
                              DDELIV       ,                                    
                              NORDRE       ,                                    
                              CENREG       ,                                    
                              QVENDUE      ,                                    
                              PVUNIT       ,                                    
                              PVUNITF      ,                                    
                              PVTOTAL      ,                                    
                              CEQUIPE      ,                                    
                              NLIGNE       ,                                    
                              TAUXTVA      ,                                    
                              QCONDT       ,                                    
                              PRMP         ,                                    
                              WEMPORTE     ,                                    
                              CPLAGE       ,                                    
                              CPROTOUR     ,                                    
                              CADRTOUR     ,                                    
                              CPOSTAL      ,                                    
                              LCOMMENT     ,                                    
                              CVENDEUR     ,                                    
                              NSOCLIVR     ,                                    
                              NDEPOT       ,                                    
                              NAUTORM      ,                                    
                              WARTINEX     ,                                    
                              WEDITBL      ,                                    
                              WACOMMUTER   ,                                    
                              WCQERESF     ,                                    
                              NMUTATION    ,                                    
                              CTOURNEE     ,                                    
                              WTOPELIVRE   ,                                    
                              DCOMPTA      ,                                    
                              DCREATION    ,                                    
                              HCREATION    ,                                    
                              DANNULATION  ,                                    
                              NLIEUORIG    ,                                    
                              WINTMAJ      ,                                    
                              DSYST        ,                                    
                              DSTAT        ,                                    
EJ0301                        CPRESTGRP    ,                                    
  "                           NSOCMODIF    ,                                    
  "                           NLIEUMODIF   ,                                    
  "                           DATENC       ,                                    
  "                           CDEV         ,                                    
  "                           WUNITR       ,                                    
  "                           LRCMMT       ,                                    
  "                           NSOCP        ,                                    
  "                           NLIEUP       ,                                    
  "                           NTRANS       ,                                    
  "                           NSEQFV       ,                                    
  "                           NSEQNQ       ,                                    
  "                           NSEQREF      ,                                    
  "                           NMODIF       ,                                    
EJ0301                        NSOCORIG     ,                                    
MH0303                        DTOPE        ,                                    
  "                           NSOCLIVR1    ,                                    
  "                           NDEPOT1      ,                                    
  "                           NSOCGEST     ,                                    
  "                           NLIEUGEST    ,                                    
  "                           NSOCDEPLIV   ,                                    
  "                           NLIEUDEPLIV  ,                                    
  "                           TYPVTE       ,                                    
  "                           CTYPENT      ,                                    
  "                           NLIEN        ,                                    
  "                           NACTVTE      ,                                    
  "                           PVCODIG      ,                                    
  "                           QTCODIG      ,                                    
  "                           DPRLG        ,                                    
  "                           CALERTE      ,                                    
  "                           CREPRISE     ,                                    
  "                           NSEQENS      ,                                    
MH0303                        MPRIMECLI    )                                    
                    VALUES (                                                    
                        :GV11-NSOCIETE     ,                                    
                        :GV11-NLIEU        ,                                    
                        :GV11-NVENTE       ,                                    
                        :GV11-CTYPENREG    ,                                    
                        :GV11-NCODICGRP    ,                                    
                        :GV11-NCODIC       ,                                    
                        :GV11-NSEQ         ,                                    
                        :GV11-CMODDEL      ,                                    
                        :GV11-DDELIV       ,                                    
                        :GV11-NORDRE       ,                                    
                        :GV11-CENREG       ,                                    
                        :GV11-QVENDUE      ,                                    
                        :GV11-PVUNIT       ,                                    
                        :GV11-PVUNITF      ,                                    
                        :GV11-PVTOTAL      ,                                    
                        :GV11-CEQUIPE      ,                                    
                        :GV11-NLIGNE       ,                                    
                        :GV11-TAUXTVA      ,                                    
                        :GV11-QCONDT       ,                                    
                        :GV11-PRMP         ,                                    
                        :GV11-WEMPORTE     ,                                    
                        :GV11-CPLAGE       ,                                    
                        :GV11-CPROTOUR     ,                                    
                        :GV11-CADRTOUR     ,                                    
                        :GV11-CPOSTAL      ,                                    
                        :GV11-LCOMMENT     ,                                    
                        :GV11-CVENDEUR     ,                                    
                        :GV11-NSOCLIVR     ,                                    
                        :GV11-NDEPOT       ,                                    
                        :GV11-NAUTORM      ,                                    
                        :GV11-WARTINEX     ,                                    
                        :GV11-WEDITBL      ,                                    
                        :GV11-WACOMMUTER   ,                                    
                        :GV11-WCQERESF     ,                                    
                        :GV11-NMUTATION    ,                                    
                        :GV11-CTOURNEE     ,                                    
                        :GV11-WTOPELIVRE   ,                                    
                        :GV11-DCOMPTA      ,                                    
                        :GV11-DCREATION    ,                                    
                        :GV11-HCREATION    ,                                    
                        :GV11-DANNULATION  ,                                    
                        :GV11-NLIEUORIG    ,                                    
                        :GV11-WINTMAJ      ,                                    
                        :GV11-DSYST        ,                                    
                        :GV11-DSTAT        ,                                    
EJ0301                  :GV11-CPRESTGRP    ,                                    
  "                     :GV11-NSOCMODIF    ,                                    
  "                     :GV11-NLIEUMODIF   ,                                    
  "                     :GV11-DATENC       ,                                    
  "                     :GV11-CDEV         ,                                    
  "                     :GV11-WUNITR       ,                                    
  "                     :GV11-LRCMMT       ,                                    
  "                     :GV11-NSOCP        ,                                    
  "                     :GV11-NLIEUP       ,                                    
  "                     :GV11-NTRANS       ,                                    
  "                     :GV11-NSEQFV       ,                                    
  "                     :GV11-NSEQNQ       ,                                    
  "                     :GV11-NSEQREF      ,                                    
  "                     :GV11-NMODIF       ,                                    
EJ0301                  :GV11-NSOCORIG     ,                                    
MH0303                  :GV11-DTOPE        ,                                    
  "                     :GV11-NSOCLIVR1    ,                                    
  "                     :GV11-NDEPOT1      ,                                    
  "                     :GV11-NSOCGEST     ,                                    
  "                     :GV11-NLIEUGEST    ,                                    
  "                     :GV11-NSOCDEPLIV   ,                                    
  "                     :GV11-NLIEUDEPLIV  ,                                    
  "                     :GV11-TYPVTE       ,                                    
  "                     :GV11-CTYPENT      ,                                    
  "                     :GV11-NLIEN        ,                                    
  "                     :GV11-NACTVTE      ,                                    
  "                     :GV11-PVCODIG      ,                                    
  "                     :GV11-QTCODIG      ,                                    
  "                     :GV11-DPRLG        ,                                    
  "                     :GV11-CALERTE      ,                                    
  "                     :GV11-CREPRISE     ,                                    
  "                     :GV11-NSEQENS      ,                                    
MH0303                  :GV11-MPRIMECLI    )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT ARTICLES MAL PASSE                                     
              SET NON-OK-MAJ-ARTICLES TO TRUE                                   
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTGV11 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-ARTICLES TO TRUE                                       
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-ARTICLES-GV. EXIT.                                            
      *                                                                         
PM     INSERT-PSE            SECTION.                                           
PM    *-------------------------------------                                    
PM    *                                                                         
PM    ****                                                                      
PM    *    CALCUL DU NUMERO DE PSE                                              
PM         ADD  1                     TO WS-SEQPSE                              
PM         MOVE VT01-NVENTE TO W-NUMPSE                                         
PM         MOVE WS-SEQPSE   TO W-SEQPSE                                         
PM         MOVE W-NPSE      TO GV11-LCOMMENT                                    
PM         MOVE GV11-NSOCIETE TO    PS00-NSOC                                   
PM         MOVE GV11-CENREG   TO    PS00-CGCPLT                                 
PM         MOVE GV11-LCOMMENT TO    PS00-NGCPLT                                 
PM         MOVE GV11-NLIEU    TO    PS00-NLIEU                                  
PM         MOVE GV11-NVENTE   TO    PS00-NVENTE                                 
PM         MOVE GV11-DDELIV   TO    PS00-DDELIV                                 
PM         MOVE GV11-DCOMPTA  TO    PS00-DCOMPTA                                
PM         MOVE W-CINSEE      TO    PS00-CINSEE                                 
PM         MOVE GV11-NCODIC   TO    PS00-NCODIC                                 
PM         MOVE GV11-PVTOTAL  TO    PS00-MTGCPLT                                
PM         MOVE ' '           TO    PS00-DANNUL                                 
PM         MOVE 0             TO    PS00-MTREMB                                 
PM         MOVE ' '           TO    PS00-DREMB                                  
PM         MOVE 0             TO    PS00-MTRACHAT                               
PM         MOVE ' '           TO    PS00-DRACHAT                                
PM         MOVE W-LNOM        TO    PS00-NOMCLI                                 
PM         MOVE ' '           TO    PS00-DTRAIT                                 
PM         MOVE DATHEUR       TO    PS00-DSYST                                  
PM         MOVE '99999999'    TO    PS00-DTRAITR                                
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-PS0000                                                  
PM                                                                              
PM         EXEC SQL INSERT                                                      
PM                  INTO    RVPS0001                                            
PM                      (NSOC         ,                                         
PM                       CGCPLT       ,                                         
PM                       NGCPLT       ,                                         
PM                       NLIEU        ,                                         
PM                       NVENTE       ,                                         
PM                       DDELIV       ,                                         
PM                       DCOMPTA      ,                                         
PM                       CINSEE       ,                                         
PM                       NCODIC       ,                                         
PM                       MTGCPLT      ,                                         
PM                       DANNUL       ,                                         
PM                       MTREMB       ,                                         
PM                       DREMB       ,                                          
PM                       MTRACHAT    ,                                          
PM                       DRACHAT      ,                                         
PM                       NOMCLI       ,                                         
PM                       DTRAIT       ,                                         
PM                       DSYST        ,                                         
PM                       DTRAITR      )                                         
PM                  VALUES (                                                    
PM                      :PS00-NSOC         ,                                    
PM                      :PS00-CGCPLT       ,                                    
PM                      :PS00-NGCPLT       ,                                    
PM                      :PS00-NLIEU        ,                                    
PM                      :PS00-NVENTE       ,                                    
PM                      :PS00-DDELIV       ,                                    
PM                      :PS00-DCOMPTA      ,                                    
PM                      :PS00-CINSEE       ,                                    
PM                      :PS00-NCODIC       ,                                    
PM                      :PS00-MTGCPLT      ,                                    
PM                      :PS00-DANNUL       ,                                    
PM                      :PS00-MTREMB       ,                                    
PM                      :PS00-DREMB       ,                                     
PM                      :PS00-MTRACHAT    ,                                     
PM                      :PS00-DRACHAT      ,                                    
PM                      :PS00-NOMCLI       ,                                    
PM                      :PS00-DTRAIT       ,                                    
PM                      :PS00-DSYST        ,                                    
PM                      :PS00-DTRAITR      )                                    
PM         END-EXEC.                                                            
PM                                                                              
PM         PERFORM TEST-CODE-RETOUR-SQL.                                        
PM    *                                                                         
PM         IF EXISTE-DEJA                                                       
PM    *           INSERT ADRESSE MAL PASSE                                      
PM            SET FIN-TS             TO TRUE                                    
PM               STRING 'PB INSERT RTPS00 - PASSER PAR L OPTION 4'              
PM                       DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
PM               END-STRING                                                     
PM            MOVE '0005'                TO GA99-NSEQERR                        
PM            PERFORM MLIBERRGEN                                                
PM            MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
PM            SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
PM            SET NON-OK-MAJ-ARTICLES TO TRUE                                   
PM         END-IF.                                                              
PM                                                                              
PM    *                                                                         
PM     FIN-INSERT-PSE. EXIT.                                                    
      *                                                                         
      *                                                                         
       INSERT-ARTICLES-VE           SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-VE1100                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
EJ0301              INTO    RVVE1100                                            
                          (   NSOCIETE     ,                                    
                              NLIEU        ,                                    
                              NVENTE       ,                                    
                              CTYPENREG    ,                                    
                              NCODICGRP    ,                                    
                              NCODIC       ,                                    
                              NSEQ         ,                                    
                              CMODDEL      ,                                    
                              DDELIV       ,                                    
                              NORDRE       ,                                    
                              CENREG       ,                                    
                              QVENDUE      ,                                    
                              PVUNIT       ,                                    
                              PVUNITF      ,                                    
                              PVTOTAL      ,                                    
                              CEQUIPE      ,                                    
                              NLIGNE       ,                                    
                              TAUXTVA      ,                                    
                              QCONDT       ,                                    
                              PRMP         ,                                    
                              WEMPORTE     ,                                    
                              CPLAGE       ,                                    
                              CPROTOUR     ,                                    
                              CADRTOUR     ,                                    
                              CPOSTAL      ,                                    
                              LCOMMENT     ,                                    
                              CVENDEUR     ,                                    
                              NSOCLIVR     ,                                    
                              NDEPOT       ,                                    
                              NAUTORM      ,                                    
                              WARTINEX     ,                                    
                              WEDITBL      ,                                    
                              WACOMMUTER   ,                                    
                              WCQERESF     ,                                    
                              NMUTATION    ,                                    
                              CTOURNEE     ,                                    
                              WTOPELIVRE   ,                                    
                              DCOMPTA      ,                                    
                              DCREATION    ,                                    
                              HCREATION    ,                                    
                              DANNULATION  ,                                    
                              NLIEUORIG    ,                                    
                              WINTMAJ      ,                                    
                              DSYST        ,                                    
                              DSTAT        ,                                    
EJ0301                        CPRESTGRP    ,                                    
  "                           NSOCMODIF    ,                                    
  "                           NLIEUMODIF   ,                                    
  "                           DATENC       ,                                    
  "                           CDEV         ,                                    
  "                           WUNITR       ,                                    
  "                           LRCMMT       ,                                    
  "                           NSOCP        ,                                    
  "                           NLIEUP       ,                                    
  "                           NTRANS       ,                                    
  "                           NSEQFV       ,                                    
  "                           NSEQNQ       ,                                    
  "                           NSEQREF      ,                                    
  "                           NMODIF       ,                                    
EJ0301                        NSOCORIG     ,                                    
MH0303                        DTOPE        ,                                    
  "                           NSOCLIVR1    ,                                    
  "                           NDEPOT1      ,                                    
  "                           NSOCGEST     ,                                    
  "                           NLIEUGEST    ,                                    
  "                           NSOCDEPLIV   ,                                    
  "                           NLIEUDEPLIV  ,                                    
  "                           TYPVTE       ,                                    
  "                           CTYPENT      ,                                    
  "                           NLIEN        ,                                    
  "                           NACTVTE      ,                                    
  "                           PVCODIG      ,                                    
  "                           QTCODIG      ,                                    
  "                           DPRLG        ,                                    
  "                           CALERTE      ,                                    
  "                           CREPRISE     ,                                    
  "                           NSEQENS      ,                                    
MH0303                        MPRIMECLI    )                                    
                    VALUES (                                                    
                        :VE11-NSOCIETE     ,                                    
                        :VE11-NLIEU        ,                                    
                        :VE11-NVENTE       ,                                    
                        :VE11-CTYPENREG    ,                                    
                        :VE11-NCODICGRP    ,                                    
                        :VE11-NCODIC       ,                                    
                        :VE11-NSEQ         ,                                    
                        :VE11-CMODDEL      ,                                    
                        :VE11-DDELIV       ,                                    
                        :VE11-NORDRE       ,                                    
                        :VE11-CENREG       ,                                    
                        :VE11-QVENDUE      ,                                    
                        :VE11-PVUNIT       ,                                    
                        :VE11-PVUNITF      ,                                    
                        :VE11-PVTOTAL      ,                                    
                        :VE11-CEQUIPE      ,                                    
                        :VE11-NLIGNE       ,                                    
                        :VE11-TAUXTVA      ,                                    
                        :VE11-QCONDT       ,                                    
                        :VE11-PRMP         ,                                    
                        :VE11-WEMPORTE     ,                                    
                        :VE11-CPLAGE       ,                                    
                        :VE11-CPROTOUR     ,                                    
                        :VE11-CADRTOUR     ,                                    
                        :VE11-CPOSTAL      ,                                    
                        :VE11-LCOMMENT     ,                                    
                        :VE11-CVENDEUR     ,                                    
                        :VE11-NSOCLIVR     ,                                    
                        :VE11-NDEPOT       ,                                    
                        :VE11-NAUTORM      ,                                    
                        :VE11-WARTINEX     ,                                    
                        :VE11-WEDITBL      ,                                    
                        :VE11-WACOMMUTER   ,                                    
                        :VE11-WCQERESF     ,                                    
                        :VE11-NMUTATION    ,                                    
                        :VE11-CTOURNEE     ,                                    
                        :VE11-WTOPELIVRE   ,                                    
                        :VE11-DCOMPTA      ,                                    
                        :VE11-DCREATION    ,                                    
                        :VE11-HCREATION    ,                                    
                        :VE11-DANNULATION  ,                                    
                        :VE11-NLIEUORIG    ,                                    
                        :VE11-WINTMAJ      ,                                    
                        :VE11-DSYST        ,                                    
                        :VE11-DSTAT        ,                                    
EJ0301                  :VE11-CPRESTGRP    ,                                    
  "                     :VE11-NSOCMODIF    ,                                    
  "                     :VE11-NLIEUMODIF   ,                                    
  "                     :VE11-DATENC       ,                                    
  "                     :VE11-CDEV         ,                                    
  "                     :VE11-WUNITR       ,                                    
  "                     :VE11-LRCMMT       ,                                    
  "                     :VE11-NSOCP        ,                                    
  "                     :VE11-NLIEUP       ,                                    
  "                     :VE11-NTRANS       ,                                    
  "                     :VE11-NSEQFV       ,                                    
  "                     :VE11-NSEQNQ       ,                                    
  "                     :VE11-NSEQREF      ,                                    
  "                     :VE11-NMODIF       ,                                    
EJ0301                  :VE11-NSOCORIG     ,                                    
MH0303                  :VE11-DTOPE        ,                                    
  "                     :VE11-NSOCLIVR1    ,                                    
  "                     :VE11-NDEPOT1      ,                                    
  "                     :VE11-NSOCGEST     ,                                    
  "                     :VE11-NLIEUGEST    ,                                    
  "                     :VE11-NSOCDEPLIV   ,                                    
  "                     :VE11-NLIEUDEPLIV  ,                                    
  "                     :VE11-TYPVTE       ,                                    
  "                     :VE11-CTYPENT      ,                                    
  "                     :VE11-NLIEN        ,                                    
  "                     :VE11-NACTVTE      ,                                    
  "                     :VE11-PVCODIG      ,                                    
  "                     :VE11-QTCODIG      ,                                    
  "                     :VE11-DPRLG        ,                                    
  "                     :VE11-CALERTE      ,                                    
  "                     :VE11-CREPRISE     ,                                    
  "                     :VE11-NSEQENS      ,                                    
MH0303                  :VE11-MPRIMECLI    )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT ARTICLES MAL PASSE                                     
              SET NON-OK-MAJ-ARTICLES TO TRUE                                   
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTVE11 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-ARTICLES TO TRUE                                       
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-ARTICLES-VE. EXIT.                                            
      *                                                                         
      *                                                                         
       INSERT-RTGV05            SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GV0500                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVGV0500                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NDOSSIER     ,                                         
                         CDOSSIER     ,                                         
                         CTYPENT      ,                                         
                         NENTITE      ,                                         
                         NSEQENS      ,                                         
                         CAGENT       ,                                         
                         DCREATION    ,                                         
                         DMODIF       ,                                         
                         DANNUL       ,                                         
                         DSYST        )                                         
                    VALUES (                                                    
                        :GV05-NSOCIETE     ,                                    
                        :GV05-NLIEU        ,                                    
                        :GV05-NVENTE       ,                                    
                        :GV05-NDOSSIER     ,                                    
                        :GV05-CDOSSIER     ,                                    
                        :GV05-CTYPENT      ,                                    
                        :GV05-NENTITE      ,                                    
                        :GV05-NSEQENS      ,                                    
                        :GV05-CAGENT       ,                                    
                        :GV05-DCREATION    ,                                    
                        :GV05-DMODIF       ,                                    
                        :GV05-DANNUL       ,                                    
                        :GV05-DSYST        )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT ADRESSE MAL PASSE                                      
              SET NON-OK-MAJ-DOSSIER TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTGV05 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-DOSSIER TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-RTGV05. EXIT.                                                 
      *                                                                         
      *                                                                         
       INSERT-RTVE05            SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-VE0500                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVVE0500                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NDOSSIER     ,                                         
                         CDOSSIER     ,                                         
                         CTYPENT      ,                                         
                         NENTITE      ,                                         
                         NSEQENS      ,                                         
                         CAGENT       ,                                         
                         DCREATION    ,                                         
                         DMODIF       ,                                         
                         DANNUL       ,                                         
                         DSYST        )                                         
                    VALUES (                                                    
                        :VE05-NSOCIETE     ,                                    
                        :VE05-NLIEU        ,                                    
                        :VE05-NVENTE       ,                                    
                        :VE05-NDOSSIER     ,                                    
                        :VE05-CDOSSIER     ,                                    
                        :VE05-CTYPENT      ,                                    
                        :VE05-NENTITE      ,                                    
                        :VE05-NSEQENS      ,                                    
                        :VE05-CAGENT       ,                                    
                        :VE05-DCREATION    ,                                    
                        :VE05-DMODIF       ,                                    
                        :VE05-DANNUL       ,                                    
                        :VE05-DSYST        )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT ADRESSE MAL PASSE                                      
              SET NON-OK-MAJ-DOSSIER TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTVE05 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-DOSSIER TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-RTVE05. EXIT.                                                 
      *                                                                         
      *                                                                         
       INSERT-RTGV06            SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GV0600                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVGV0600                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NDOSSIER     ,                                         
                         NSEQNQ       ,                                         
                         NSEQ         ,                                         
                         DSYST        )                                         
                    VALUES (                                                    
                        :GV06-NSOCIETE     ,                                    
                        :GV06-NLIEU        ,                                    
                        :GV06-NVENTE       ,                                    
                        :GV06-NDOSSIER     ,                                    
                        :GV06-NSEQNQ       ,                                    
                        :GV06-NSEQ         ,                                    
                        :GV06-DSYST        )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT DOSSIER MAL PASSE                                      
              SET NON-OK-MAJ-DOSSIER TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTGV06 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-DOSSIER TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-RTGV06. EXIT.                                                 
      *                                                                         
      *                                                                         
       INSERT-RTVE06            SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-VE0600                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVVE0600                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NDOSSIER     ,                                         
                         NSEQNQ       ,                                         
                         NSEQ         ,                                         
                         DSYST        )                                         
                    VALUES (                                                    
                        :VE06-NSOCIETE     ,                                    
                        :VE06-NLIEU        ,                                    
                        :VE06-NVENTE       ,                                    
                        :VE06-NDOSSIER     ,                                    
                        :VE06-NSEQNQ       ,                                    
                        :VE06-NSEQ         ,                                    
                        :VE06-DSYST        )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT DOSSIER MAL PASSE                                      
              SET NON-OK-MAJ-DOSSIER TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTVE06 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-DOSSIER TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-RTVE06. EXIT.                                                 
      *                                                                         
      *                                                                         
       INSERT-RTGV08            SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GV0800                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVGV0800                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NDOSSIER     ,                                         
                         NSEQ         ,                                         
                         CCTRL        ,                                         
                         LGCTRL       ,                                         
                         VALCTRL      ,                                         
                         CAUTOR       ,                                         
                         CJUSTIF      ,                                         
                         DMODIF       ,                                         
                         DSYST        )                                         
                    VALUES (                                                    
                        :GV08-NSOCIETE     ,                                    
                        :GV08-NLIEU        ,                                    
                        :GV08-NVENTE       ,                                    
                        :GV08-NDOSSIER     ,                                    
                        :GV08-NSEQ         ,                                    
                        :GV08-CCTRL        ,                                    
                        :GV08-LGCTRL       ,                                    
                        :GV08-VALCTRL      ,                                    
                        :GV08-CAUTOR       ,                                    
                        :GV08-CJUSTIF      ,                                    
                        :GV08-DMODIF       ,                                    
                        :GV08-DSYST        )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT DOSSIER MAL PASSE                                      
              SET NON-OK-MAJ-DOSSIER TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTGV08 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-DOSSIER TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-RTGV08. EXIT.                                                 
      *                                                                         
      *                                                                         
       INSERT-RTVE08            SECTION.                                        
      *---------------------------------                                        
      *                                                                         
           MOVE FUNC-INSERT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-VE0800                                                  
      ****                                                                      
           EXEC SQL INSERT                                                      
                    INTO    RVVE0800                                            
                        (NSOCIETE     ,                                         
                         NLIEU        ,                                         
                         NVENTE       ,                                         
                         NDOSSIER     ,                                         
                         NSEQ         ,                                         
                         CCTRL        ,                                         
                         LGCTRL       ,                                         
                         VALCTRL      ,                                         
                         CAUTOR       ,                                         
                         CJUSTIF      ,                                         
                         DMODIF       ,                                         
                         DSYST        )                                         
                    VALUES (                                                    
                        :VE08-NSOCIETE     ,                                    
                        :VE08-NLIEU        ,                                    
                        :VE08-NVENTE       ,                                    
                        :VE08-NDOSSIER     ,                                    
                        :VE08-NSEQ         ,                                    
                        :VE08-CCTRL        ,                                    
                        :VE08-LGCTRL       ,                                    
                        :VE08-VALCTRL      ,                                    
                        :VE08-CAUTOR       ,                                    
                        :VE08-CJUSTIF      ,                                    
                        :VE08-DMODIF       ,                                    
                        :VE08-DSYST        )                                    
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
           IF EXISTE-DEJA                                                       
      *           INSERT DOSSIER MAL PASSE                                      
              SET NON-OK-MAJ-DOSSIER TO TRUE                                    
              SET FIN-TS             TO TRUE                                    
                 STRING 'PB INSERT RTVE08 - PASSER PAR L OPTION 4'              
                         DELIMITED BY SIZE INTO MESSAGE-VARIABLE                
                 END-STRING                                                     
              MOVE '0005'                TO GA99-NSEQERR                        
              PERFORM MLIBERRGEN                                                
              MOVE LIBERR-MESSAGE     TO COMM-BS42-LIBERR                       
              SET  COMM-BS42-CODRET-ERREUR  TO TRUE                             
           ELSE                                                                 
              SET OK-MAJ-DOSSIER TO TRUE                                        
           END-IF.                                                              
      *                                                                         
       FIN-INSERT-RTVE08. EXIT.                                                 
      *                                                                         
      *                                                                         
      ******************************************************************        
      *                                                                *        
      *                       DELETE DB2                               *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       DELETE-RTVT02                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT0200                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT02. EXIT.                                                 
      *                                                                         
       DELETE-RTVT11                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT1100                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT11. EXIT.                                                 
      *                                                                         
       DELETE-RTVT16                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT1600                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT16. EXIT.                                                 
      *                                                                         
       DELETE-RTVT12                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT1200                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT12. EXIT.                                                 
      *                                                                         
       DELETE-RTVT17                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT1700                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT17. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTVT13                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
      *                                                                         
             EXEC SQL DELETE                                                    
                      FROM     RVVT1300                                         
                      WHERE    NCLUSTER  = :WS-NCLUSTER                         
                      AND NFLAG    = :WS-NFLAG                                  
             END-EXEC                                                           
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT13. EXIT.                                                 
      *                                                                         
       DELETE-RTVT15                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT1500                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT15. EXIT.                                                 
      *                                                                         
       DELETE-RTVT14                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT1400                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *                                                                         
       FIN-DELETE-RTVT14. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTVT01                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
YT1   *TYPTR    IF COMM-BS42-TYPVTE = 'B' OR 'L' OR 'A'                         
TYPTR      IF W-VTE-LOC-SANS-ADR OR W-VTE-LOC-AVEC-ADR                          
TYPTR         OR W-VTE-ACOMPTE                                                  
 "           EXEC SQL DELETE                                                    
 "                    FROM     RVVT0100                                         
 "                    WHERE    NFLAG     = :WS-NFLAG                            
 "                    AND      NCLUSTER  = :WS-NCLUSTER                         
 "                    AND      NVENTE    = :VT01-NVENTE                         
 "                    AND      NTYPVENTE = :VT01-NTYPVENTE                      
 "           END-EXEC                                                           
YT1        ELSE                                                                 
             EXEC SQL DELETE                                                    
                    FROM RVVT0100                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
CINMD                 AND      NSOCIETE  = :VT01-NSOCIETE                       
CINMD                 AND      NLIEU     = :VT01-NLIEU                          
CINMD                 AND      NVENTE    = :VT01-NVENTE                         
             END-EXEC                                                           
YT1        END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT01. EXIT.                                                 
      *                                                                         
VT50   DELETE-RTVT50                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      **** ON REMET D'ABORD VT50 DANS VT57                                      
           EXEC SQL INSERT                                                      
                  INTO RVVT5700                                                 
           SELECT NFLAG , NVENTE, NLIEU, NSOCIETE, NDOC                         
                , NSERVICE, NBOX, NBOITE, NFICHE, NSEQ                          
           FROM RVVT5000 A                                                      
                    WHERE NFLAG    = :WS-NFLAG                                  
                      AND NSOCIETE = :VT01-NSOCIETE                             
                      AND NLIEU    = :VT01-NLIEU                                
                      AND NVENTE   = :VT01-NVENTE                               
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      ****                                                                      
      ****                                                                      
           IF TROUVE                                                            
              EXEC SQL DELETE                                                   
                    FROM RVVT5000                                               
                    WHERE NFLAG    = :WS-NFLAG                                  
                      AND NSOCIETE = :VT01-NSOCIETE                             
                      AND NLIEU    = :VT01-NLIEU                                
                      AND NVENTE   = :VT01-NVENTE                               
              END-EXEC                                                          
              PERFORM TEST-CODE-RETOUR-SQL                                      
           END-IF.                                                              
      *                                                                         
       FIN-DELETE-RTVT50. EXIT.                                                 
      *                                                                         
       DELETE-RTVT05                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT0500                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT05. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTVT06                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT0600                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT06. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTVT08                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
           EXEC SQL DELETE                                                      
                    FROM RVVT0800                                               
                    WHERE NCLUSTER = :WS-NCLUSTER                               
                      AND NFLAG    = :WS-NFLAG                                  
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTVT08. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTGV10                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
              MOVE COMM-BS42-NSOCIETE    TO GV10-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV10-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV10-NVENTE                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              EXEC SQL DELETE                                                   
                    FROM RTGV10                                                 
                    WHERE NSOCIETE = :GV10-NSOCIETE                             
                    AND   NLIEU    = :GV10-NLIEU                                
                    AND   NVENTE   = :GV10-NVENTE                               
              END-EXEC                                                          
           ELSE                                                                 
              EXEC SQL DELETE                                                   
                    FROM RTVE10                                                 
                    WHERE NSOCIETE = :GV10-NSOCIETE                             
                    AND   NLIEU    = :GV10-NLIEU                                
                    AND   NVENTE   = :GV10-NVENTE                               
              END-EXEC                                                          
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTGV10. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTGV11                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
              MOVE COMM-BS42-NSOCIETE    TO GV11-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV11-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV11-NVENTE                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              EXEC SQL DELETE                                                   
                    FROM RTGV11                                                 
                    WHERE NSOCIETE = :GV11-NSOCIETE                             
                    AND   NLIEU    = :GV11-NLIEU                                
                    AND   NVENTE   = :GV11-NVENTE                               
              END-EXEC                                                          
PM            IF TOP-PSE                                                        
PM               PERFORM DELETE-RTPS00                                          
PM            END-IF                                                            
           ELSE                                                                 
              EXEC SQL DELETE                                                   
                    FROM RTVE11                                                 
                    WHERE NSOCIETE = :GV11-NSOCIETE                             
                    AND   NLIEU    = :GV11-NLIEU                                
                    AND   NVENTE   = :GV11-NVENTE                               
              END-EXEC                                                          
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTGV11. EXIT.                                                 
      *                                                                         
PM     DELETE-RTPS00                SECTION.                                    
PM    *-------------------------------------                                    
PM    *                                                                         
PM    ****                                                                      
PM            EXEC SQL DELETE                                                   
PM                  FROM RVPS0001                                               
PM                  WHERE NSOC     = :GV11-NSOCIETE                             
PM                  AND   NLIEU    = :GV11-NLIEU                                
PM                  AND   NVENTE   = :GV11-NVENTE                               
PM            END-EXEC                                                          
PM                                                                              
PM         PERFORM TEST-CODE-RETOUR-SQL.                                        
PM    *                                                                         
PM     FIN-DELETE-RTPS00. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTGV14                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
              MOVE COMM-BS42-NSOCIETE    TO GV14-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV14-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV14-NVENTE                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              EXEC SQL DELETE                                                   
                    FROM RTGV14                                                 
                    WHERE NSOCIETE = :GV14-NSOCIETE                             
                    AND   NLIEU    = :GV14-NLIEU                                
                    AND   NVENTE   = :GV14-NVENTE                               
              END-EXEC                                                          
           ELSE                                                                 
              EXEC SQL DELETE                                                   
                    FROM RTVE14                                                 
                    WHERE NSOCIETE = :GV14-NSOCIETE                             
                    AND   NLIEU    = :GV14-NLIEU                                
                    AND   NVENTE   = :GV14-NVENTE                               
              END-EXEC                                                          
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-DELETE-RTGV14. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTGV02                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
              MOVE COMM-BS42-NSOCIETE    TO GV02-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV02-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV02-NVENTE                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              EXEC SQL DELETE                                                   
                    FROM RTGV02                                                 
                    WHERE NSOCIETE = :GV02-NSOCIETE                             
                    AND   NLIEU    = :GV02-NLIEU                                
                    AND   NVENTE   = :GV02-NVENTE                               
              END-EXEC                                                          
           ELSE                                                                 
              EXEC SQL DELETE                                                   
                    FROM RTVE02                                                 
                    WHERE NSOCIETE = :GV02-NSOCIETE                             
                    AND   NLIEU    = :GV02-NLIEU                                
                    AND   NVENTE   = :GV02-NVENTE                               
              END-EXEC                                                          
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *                                                                         
       FIN-DELETE-RTGV02. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTGV05                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
              MOVE COMM-BS42-NSOCIETE    TO GV05-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV05-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV05-NVENTE                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              EXEC SQL DELETE                                                   
                    FROM RTGV05                                                 
                    WHERE NSOCIETE = :GV05-NSOCIETE                             
                    AND   NLIEU    = :GV05-NLIEU                                
                    AND   NVENTE   = :GV05-NVENTE                               
              END-EXEC                                                          
           ELSE                                                                 
              EXEC SQL DELETE                                                   
                    FROM RTVE05                                                 
                    WHERE NSOCIETE = :GV05-NSOCIETE                             
                    AND   NLIEU    = :GV05-NLIEU                                
                    AND   NVENTE   = :GV05-NVENTE                               
              END-EXEC                                                          
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *                                                                         
       FIN-DELETE-RTGV05. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTGV06                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
              MOVE COMM-BS42-NSOCIETE    TO GV06-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV06-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV06-NVENTE                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              EXEC SQL DELETE                                                   
                    FROM RTGV06                                                 
                    WHERE NSOCIETE = :GV06-NSOCIETE                             
                    AND   NLIEU    = :GV06-NLIEU                                
                    AND   NVENTE   = :GV06-NVENTE                               
              END-EXEC                                                          
           ELSE                                                                 
              EXEC SQL DELETE                                                   
                    FROM RTVE06                                                 
                    WHERE NSOCIETE = :GV06-NSOCIETE                             
                    AND   NLIEU    = :GV06-NLIEU                                
                    AND   NVENTE   = :GV06-NVENTE                               
              END-EXEC                                                          
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *                                                                         
       FIN-DELETE-RTGV06. EXIT.                                                 
      *                                                                         
      *                                                                         
       DELETE-RTGV08                SECTION.                                    
      *-------------------------------------                                    
      *                                                                         
      ****                                                                      
              MOVE COMM-BS42-NSOCIETE    TO GV08-NSOCIETE                       
              MOVE COMM-BS42-NLIEU       TO GV08-NLIEU                          
              MOVE COMM-BS42-NVENTE      TO GV08-NVENTE                         
TYPTR *    IF VT01-NTYPVENTE = 'G' OR 'B' OR ' ' OR 'M'                         
TYPTR      IF W-VTE-HOST OR W-VTE-LOC-AVEC-ADR                                  
              EXEC SQL DELETE                                                   
                    FROM RTGV08                                                 
                    WHERE NSOCIETE = :GV08-NSOCIETE                             
                    AND   NLIEU    = :GV08-NLIEU                                
                    AND   NVENTE   = :GV08-NVENTE                               
              END-EXEC                                                          
           ELSE                                                                 
              EXEC SQL DELETE                                                   
                    FROM RTVE08                                                 
                    WHERE NSOCIETE = :GV08-NSOCIETE                             
                    AND   NLIEU    = :GV08-NLIEU                                
                    AND   NVENTE   = :GV08-NVENTE                               
              END-EXEC                                                          
           END-IF.                                                              
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
      *                                                                         
       FIN-DELETE-RTGV08. EXIT.                                                 
      *                                                               *         
      *---------------------------------------                                  
       RECHERCHE-TYPTR                SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
      *    RECHERCHE DU CODE VENTE + TYPE DE VENTE                              
      *                                                                         
           MOVE 'TYPTR'                TO WS-GA01-CTABLEG1.                     
           MOVE SPACES                 TO WS-GA01-CTABLEG2.                     
           MOVE W-TYPTR-CODE           TO WS-GA01-CODE-VENTE.                   
           PERFORM LECTURE-TABLE-GENERALISEE.                                   
           IF TROUVE                                                            
              MOVE GA01-WTABLEG             TO WS-GA01-TYPTR                    
      *                                                                         
              EVALUATE  WS-TYPTR-TYPE-TRANS                                     
                 WHEN  'V'                                                      
      *  LE CODE EST DE TYPE VENTE                                              
                     SET W-TYPE-VENTE TO TRUE                                   
                 WHEN  'T'                                                      
      *  LE CODE EST DE TYPE TRANSACTION                                        
                     SET W-TYPE-TRANS TO TRUE                                   
                 WHEN  'G'                                                      
      *  LE CODE EST DE TYPE GENERAL                                            
                     SET W-TYPE-GENERAL TO TRUE                                 
                 WHEN  'N'                                                      
      *  LE CODE EST DE TYPE NATURE                                             
                     SET W-TYPE-NATURE TO TRUE                                  
                 WHEN  OTHER                                                    
      *  LE CODE N'EST PAS RECONNU                                              
                     SET W-TYPE-INCONNU TO TRUE                                 
              END-EVALUATE                                                      
      *  ON REGARDE LE CODE TYPE DE VENTE                                       
              IF W-TYPE-VENTE                                                   
                 EVALUATE WS-TYPTR-TYPE-VENTE                                   
                    WHEN 'H'                                                    
                       SET W-VTE-HOST TO TRUE                                   
                    WHEN 'L'                                                    
                       SET W-VTE-LOC-AVEC-ADR TO TRUE                           
                    WHEN 'A'                                                    
                       SET W-VTE-ACOMPTE      TO TRUE                           
                    WHEN 'S'                                                    
                       SET W-VTE-LOC-SANS-ADR TO TRUE                           
                    WHEN 'X'                                                    
                       SET W-VTE-INTER-MAG TO TRUE                              
                    WHEN  OTHER                                                 
      *  LE TYPE DE VENTE N'EST PAS RECONNU                                     
                       SET W-VTE-TYPE-INCONNU TO TRUE                           
                 END-EVALUATE                                                   
      *  SI NON TROUVE                                                          
           ELSE                                                                 
      *  LE CODE N'EST PAS RECONNU                                              
              SET W-TYPE-INCONNU      TO TRUE                                   
              SET W-VTE-TYPE-INCONNU  TO TRUE                                   
              SET W-ARCHIV-AUTORISE-N TO TRUE                                   
           END-IF.                                                              
      *                                                                         
       FIN-RECHERCHE-TYPTR. EXIT.                                               
      *                                                                         
      *******             LECTURES D B 2                    ***********         
      *                                                                         
      *---------------------------------------                                  
       LECTURE-TABLE-GENERALISEE       SECTION.                                 
      *----------------------------------------                                 
      *                                                                         
           MOVE WS-GA01-CTABLEG1       TO GA01-CTABLEG1                         
           MOVE WS-GA01-CTABLEG2       TO GA01-CTABLEG2                         
      *                                                                         
           MOVE FUNC-SELECT            TO TRACE-SQL-FUNCTION                    
      *                                                                         
           EXEC SQL SELECT                                                      
                          WTABLEG                                               
                    INTO                                                        
                         :GA01-WTABLEG                                          
                    FROM     RVGA0100                                           
                    WHERE                                                       
                          CTABLEG1 = :GA01-CTABLEG1                             
                    AND   CTABLEG2 = :GA01-CTABLEG2                             
                    END-EXEC.                                                   
      *                                                                         
           PERFORM TEST-CODE-RETOUR-SQL.                                        
      *                                                                         
       FIN-LECTURE-TABLE-GENERALISEE. EXIT.                                     
      *                                                                         
      *                                                                         
V5.0   SELECT-VT01                    SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE COMM-BS42-NSOCIETE    TO VT01-NSOCIETE.                         
           MOVE COMM-BS42-NLIEU       TO VT01-NLIEU.                            
           MOVE COMM-BS42-NVENTE      TO VT01-NVENTE.                           
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION.                    
           PERFORM CLEF-VT0100.                                                 
      *                                                                         
      * RECHERCHE ENTETE DE VENTE SUR ARCHIVE : RVVT0100                        
      *                                                                         
           EXEC SQL SELECT NFLAG , NCLUSTER , NTYPVENTE                         
                  INTO  :VT01-NFLAG , :VT01-NCLUSTER , :VT01-NTYPVENTE          
                  FROM   RVVT0100                                               
                  WHERE  NSOCIETE = :VT01-NSOCIETE                              
                  AND    NLIEU    = :VT01-NLIEU                                 
                  AND    NVENTE   = :VT01-NVENTE                                
           END-EXEC                                                             
      *                                                                         
           IF SQLCODE = -811                                                    
                MOVE 0 TO SQLCODE                                               
           END-IF.                                                              
      *                                                                         
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF TROUVE                                                            
              MOVE VT01-NFLAG     TO COMM-BS42-NFLAG                            
              MOVE VT01-NCLUSTER  TO COMM-BS42-NCLUSTER                         
              MOVE VT01-NTYPVENTE TO COMM-BS42-TYPVTE                           
           END-IF.                                                              
      *                                                                         
      *                                                                         
       FIN-SELECT-VT01. EXIT.                                                   
       CHERCHE-OFFRE SECTION.                                                   
           MOVE FUNC-SELECT TO TRACE-SQL-FUNCTION                               
           MOVE GV11-CENREG    TO BS03-NENTITE                                  
           MOVE GV11-DDELIV    TO BS01-DEFFET                                   
           MOVE SPACES         TO BS01-NENTITE1                                 
           EXEC SQL                                                             
                SELECT TMP.DEFFET
                     , TMP.NENTITE1
                  INTO :BS01-DEFFET
                     , :BS01-NENTITE1
      *{Post-translation Correct-req-Union
               FROM ( 
                SELECT DEFFET                                                   
                     , NENTITE1                                                 
                  FROM RVBS0100 BS01                                            
                  INNER JOIN RVBS0300 BS03                                      
                ON BS01.NENTITE2 = BS03.NLIST                                   
                WHERE BS03.NENTITE = :BS03-NENTITE                              
                  AND :BS01-DEFFET                                              
                  BETWEEN BS01.DEFFET AND BS01.DFINEFFET                        
                  AND BS01.CTYPENT1 = 'OC'                                      
                  AND BS01.CTYPENT2 = 'LI'                                      
                UNION ALL                                                       
                SELECT DEFFET                                                   
                     , NENTITE1                                                 
                  FROM RVBS0100 BS01                                            
                WHERE :BS01-DEFFET                                              
                BETWEEN BS01.DEFFET AND BS01.DFINEFFET                          
                AND BS01.CTYPENT1 = 'OC'                                        
                AND BS01.CTYPENT2 = 'SE'                                        
                AND BS01.NENTITE2 = :BS03-NENTITE                               
                ORDER BY DEFFET DESC                                            
                FETCH FIRST 1 ROW ONLY     ) AS TMP                                     
      *}
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF TROUVE                                                            
              MOVE BS01-NENTITE1 TO GV11-NCODICGRP                              
           END-IF.                                                              
       F-CHERCHE-OFFRE. EXIT.                                                   
       MLIBERRGEN                     SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           MOVE CODE-RETOUR           TO W-CODE-RETOUR-MLIBERRG                 
           MOVE NOM-PROG              TO GA99-CNOMPGRM                          
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
           PERFORM CLEF-GA9900.                                                 
           EXEC SQL SELECT   CERRUT,                                            
                             LIBERR                                             
                    INTO    :GA99-CERRUT,                                       
                            :GA99-LIBERR                                        
                    FROM     RVGA9900                                           
                    WHERE    CNOMPGRM = :GA99-CNOMPGRM                          
                    AND      NSEQERR = :GA99-NSEQERR                            
           END-EXEC.                                                            
           PERFORM TEST-CODE-RETOUR-SQL.                                        
           IF NON-TROUVE                                                        
           THEN MOVE 'MESSAGE NON REFERENCE' TO LIBERR-MESSAGE                  
                MOVE GA99-NSEQERR TO ERREUR-UTILISATEUR                         
           ELSE MOVE GA99-CERRUT TO ERREUR-UTILISATEUR                          
                MOVE GA99-LIBERR TO LIBERR-MESSAGE                              
           END-IF.                                                              
           IF MESSAGE-VARIABLE NOT = SPACES                                     
              MOVE MESSAGE-VARIABLE TO LIBERR-MESSAGE                           
           END-IF.                                                              
           MOVE ERREUR-UTILISATEUR    TO COM-CODERR.                            
           MOVE W-CODE-RETOUR-MLIBERRG   TO CODE-RETOUR.                        
      *                                                                         
       FIN-MLIBERRGEN. EXIT.                                                    
       WRITE-TSBS42                   SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
           IF EIBTRMID  = LOW-VALUES                                            
              MOVE EIBTASKN              TO P-EIBTASKN                          
              MOVE X-EIBTASKN TO EIBTRMID                                       
           END-IF                                                               
           STRING 'GV42' EIBTRMID                                               
              DELIMITED BY SIZE     INTO IDENT-TS                               
           END-STRING                                                           
           MOVE LENGTH OF TS42-DONNEES   TO LONG-TS                             
           MOVE TS42-DONNEES          TO Z-INOUT                                
           PERFORM WRITE-TS.                                                    
      *                                                                         
       FIN-WRITE-TSBS42. EXIT.                                                  
      *                                                                         
       TEST-CODE-RETOUR-SQL           SECTION.                                  
      *---------------------------------------                                  
      *                                                                         
            MOVE '0'                  TO CODE-RETOUR                            
            MOVE SQLCODE              TO SQL-CODE                               
            MOVE SQLCODE              TO TRACE-SQL-CODE                         
            IF CODE-DECLARE                                                     
               MOVE 0                    TO SQL-CODE                            
            END-IF                                                              
            IF CODE-SELECT OR CODE-FETCH                                        
               IF SQL-CODE = +100                                               
                  MOVE '1'                  TO CODE-RETOUR                      
               END-IF                                                           
            END-IF                                                              
            IF SQL-CODE < 0                                                     
               SET  COMM-BS42-CODRET-ERREUR TO TRUE                             
               STRING TRACE-SQL-MESSAGE(1:22) TRACE-SQL-MESSAGE(33:31)          
                     '     '                                                    
                      DELIMITED BY SIZE INTO COMM-BS42-LIBERR                   
               END-STRING                                                       
               SET EXISTE-DEJA TO TRUE                                          
      *        PERFORM MODULE-SORTIE                                            
            END-IF.                                                             
      *                                                                         
       FIN-TEST-CODE-RETOUR-SQL. EXIT.                                          
      *                                                                         
      *========================FIN MBS42==============================*         
                                                                                
