      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE VENTE POUR GSM (TD00)                                    
      *                                                                         
      *****************************************************************         
      *                                                                         
      *                                                                         
       01  WS-MQ10.                                                             
         02  WS-QUEUE.                                                          
               10   MQ10-CORRELID PIC    X(24).                                 
         02  WS-CODRET            PIC    XX.                                    
         02  WS-MESSAGE.                                                        
            15  MESS-ENTETE.                                                    
               20   MES-TYPE      PIC    X(3).                                  
               20   MES-NSOCMSG   PIC    X(3).                                  
               20   MES-NLIEUMSG  PIC    X(3).                                  
               20   MES-NSOCDST   PIC    X(3).                                  
               20   MES-NLIEUDST  PIC    X(3).                                  
               20   MES-NORD      PIC    9(8).                                  
               20   MES-LPROG     PIC    X(10).                                 
               20   MES-DJOUR     PIC    X(8).                                  
               20   MES-WSID      PIC    X(10).                                 
               20   MES-USER      PIC    X(10).                                 
               20   MES-CHRONO    PIC    9(7).                                  
               20   MES-NBRMSG    PIC    9(7).                                  
               20   MES-FILLER    PIC    X(30).                                 
            15  MESS-DATA                    PIC X(100).                        
            15  MESS-DATA-TD0 REDEFINES MESS-DATA.                              
               20  MESS-SOC                  PIC X(3).                          
               20  MESS-LIEU                 PIC X(3).                          
               20  MESS-VENTE                PIC X(7).                          
               20  FILLER                    PIC X(87).                         
            15  MESS-REP-ERREUR REDEFINES MESS-DATA.                            
               20  FILLER                    PIC X(13).                         
               20  MESS-CODRET               PIC X(2).                          
                   88  MESS-CODRET-OK        VALUE '  '.                        
                   88  MESS-CODRET-ERREUR    VALUE '01'.                        
               20  MESS-LIBERR               PIC X(85).                         
                                                                                
