      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      * - MESTWA56 COPY MESSAGE POUR L ENVOI ET LA RECEPTION LONGUEUR  *      13
      *  49835                                                         *      13
      * COPY POUR LES PROGRAMMES TGA50 MWA56                           *      13
      ******************************************************************        
              05 MESTWA56-DATA REDEFINES MESTWA50-DATA.                         
      * LONGUEUR DE 100 OCTETS * 495                                            
                 10 MESTWA56-DATA-X            OCCURS  495.                     
                    15 MESTWA56-NSOCDEPOT      PIC X(3).                        
                    15 MESTWA56-NDEPOT         PIC X(3).                        
                    15 MESTWA56-NCODIC         PIC X(7).                        
                    15 MESTWA56-NEAN           PIC X(13).                       
                    15 MESTWA56-QCOLIRECEPT    PIC 9(5).                        
                    15 MESTWA56-CMODSTOCK1     PIC X(5).                        
                    15 MESTWA56-CMODSTOCK2     PIC X(5).                        
                    15 MESTWA56-CMODSTOCK3     PIC X(5).                        
                    15 MESTWA56-WMODSTOCK1     PIC X(1).                        
                    15 MESTWA56-WMODSTOCK2     PIC X(1).                        
                    15 MESTWA56-WMODSTOCK3     PIC X(1).                        
                    15 MESTWA56-TOP-CMODSTOCK  PIC X(1).                        
                    15 MESTWA56-CQUOTA         PIC X(5).                        
                    15 MESTWA56-QNBPRACK       PIC 9(5).                        
                    15 MESTWA56-QNBPVSOL       PIC 9(3).                        
                    15 MESTWA56-QNBRANMAIL     PIC 9(3).                        
                    15 MESTWA56-QNBNIVGERB     PIC 9(3).                        
                    15 MESTWA56-CCONTENEUR     PIC X(1).                        
                    15 MESTWA56-CSPECIFSTK     PIC X(1).                        
                    15 MESTWA56-CTPSUP         PIC X(1).                        
                    15 MESTWA56-CLASSE         PIC X(1).                        
                    15 MESTWA56-SSAAMMJJ       PIC X(8).                        
                    15 MESTWA56-TRANSFERT      PIC X(1).                        
                    15 MESTWA56-FILLER         PIC X(18).                       
                 10 MESTWA56-DATA-INUTILE      PIC X(335).                      
                                                                                
