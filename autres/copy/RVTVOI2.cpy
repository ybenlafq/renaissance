      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 22/10/2016 0        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TVOI2 TYPE DE VOIE LONG, TVOIE INVER   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTVOI2.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTVOI2.                                                             
      *}                                                                        
           05  TVOI2-CTABLEG2    PIC X(15).                                     
           05  TVOI2-CTABLEG2-REDEF REDEFINES TVOI2-CTABLEG2.                   
               10  TVOI2-LVOIE           PIC X(15).                             
           05  TVOI2-WTABLEG     PIC X(80).                                     
           05  TVOI2-WTABLEG-REDEF  REDEFINES TVOI2-WTABLEG.                    
               10  TVOI2-TVOIE           PIC X(04).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTVOI2-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTVOI2-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TVOI2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TVOI2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TVOI2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TVOI2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
