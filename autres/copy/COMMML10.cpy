      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MFL10-LONG-COMMAREA         PIC S9(4) COMP VALUE +4096. 00000010
      *--                                                                       
       01  COMM-MFL10-LONG-COMMAREA         PIC S9(4) COMP-5 VALUE              
                                                                  +4096.        
      *}                                                                        
       01  COMM-MFL10-APPLI.                                            00000011
           02 COMM-MFL10-ZONES-ENTREE.                                  00000020
              05 COMM-MFL10-CACID              PIC X(07).               00000021
           02 COMM-MFL10-ZONES-SORTIE.                                  00000100
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MFL10-CODRET      COMP   PIC  S9(4).              00000110
      *--                                                                       
              05 COMM-MFL10-CODRET COMP-5   PIC  S9(4).                         
      *}                                                                        
              05 COMM-MFL10-LMESS              PIC X(66).               00000120
              05 COMM-MFL10-NSOCCICS           PIC X(03).                       
              05 COMM-MFL10-NSOCIETE           PIC X(03).                       
              05 COMM-MFL10-NLIEU              PIC X(03).                       
              05 COMM-MFL10-CFLGMAG            PIC X(05).                       
                                                                                
