      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 REF-CPREF.                                                    00000010
          04 REF-CLE.                                                   00000020
             06 REF-CLE1          PIC X(03).                            00000030
             06 REF-CDSOC         PIC X(03).                            00000040
             06 REF-CLE2          PIC X(13).                            00000050
          04 REF-DATA             PIC X(108).                           00000060
          04 REF-DATA-56 REDEFINES REF-DATA.                            00000070
             06 REF-DEBZONES      PIC X(15).                            00000080
             06 REF-SUITZONE      PIC X(17).                            00000090
             06 REF-NBDECIM       PIC S9(01) COMP-3.                    00000100
             06 REF-PSEURO        PIC X(01).                            00000110
             06 REF-ARRONDI       PIC S9(01)V99 COMP-3.                 00000120
             06 FILLER            PIC X(72).                            00000130
          04 REF-DATA-57 REDEFINES REF-DATA.                            00000140
             06 REF-DEFFET        PIC X(08).                            00000150
             06 REF-OPERATOR      PIC X(01).                            00000160
             06 REF-TAUX          PIC S9(04)V9(5) COMP-3.               00000170
             06 REF-WZONES        PIC X(30).                            00000180
             06 FILLER            PIC X(64).                            00000190
          04 REF-ANU           PIC X(01).                               00000200
                                                                                
