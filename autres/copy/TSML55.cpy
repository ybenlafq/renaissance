      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *                                                                *        
      *  PROJET     : LOGISTIQUE GROUPE                                *        
      *  PROGRAMME  : MSL55                                            *        
      *  TITRE      : TS DU MODULE TP MSL55                            *        
      *               RECHERCHE EXPEDIES SELON CRITERES TSL54          *        
      *  LONGUEUR   : 55500 C                                          *        
      *                                                                *        
      *  CONTENU    : CETTE TS CONTIENT LES EXPEDIES ET CHRONOPOST     *        
      *               CORRESPONDANT AUX CRITERES DE RECHERCHE          *        
      ******************************************************************        
       01  TSML55-ITEM.                                                         
           05  TSML55-LONGUEUR              PIC S9(3) VALUE +91.                
           05  TSML55-DATAS.                                                    
               10  TSML55-SOCIETE    PIC X(03).                                 
               10  TSML55-LIEU       PIC X(03).                                 
               10  TSML55-CODIC      PIC X(07).                                 
               10  TSML55-VENTE      PIC X(07).                                 
               10  TSML55-SEQNQ      PIC 9(05).                                 
               10  TSML55-QVENDUE    PIC 9(05).                                 
               10  TSML55-SOCDEPLIV  PIC X(03).                                 
               10  TSML55-LIEUDEPLIV PIC X(03).                                 
               10  TSML55-COMMENT    PIC X(19).                                 
               10  TSML55-EQUIPE     PIC X(07).                                 
               10  TSML55-DDELIV     PIC X(08).                                 
               10  TSML55-MANQUANT   PIC X(01).                                 
               10  TSML55-CODICGRP   PIC X(07).                                 
               10  TSML55-NSEQ       PIC X(02).                                 
               10  TSML55-CSTATUT    PIC X(01).                                 
               10  TSML55-CCTRL      PIC X(05).                                 
               10  TSML55-DCREATION  PIC X(08).                                 
V51R0          10  TSML55-DDELIVMAX  PIC X(08).                                 
               10  TSML55-QMANQUANT  PIC 9(05).                                 
               10  TSML55-QRECUE     PIC 9(05).                                 
                                                                                
