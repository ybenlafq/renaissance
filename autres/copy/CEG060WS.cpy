      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
       01  WS-EG060.                                                            
           05  W-NUMREC-X.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-NUMREC           PIC S9(8)     COMP VALUE ZERO.            
      *--                                                                       
               10  W-NUMREC           PIC S9(8) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I                      PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I                      PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-C                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-C                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-D                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-D                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-F                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-F                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-K                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-K                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-L                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-L                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-LRES                 PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-LRES                 PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-LG                   PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-LG                   PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-LR                   PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-LR                   PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-LK                   PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-LK                   PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-LM                   PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-LM                   PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-P                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-P                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-PZ                   PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-PZ                   PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-T                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-T                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-CL1                  PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-CL1                  PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-CL2                  PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-CL2                  PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-R                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-R                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-M                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-M                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-MC                   PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-MC                   PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-Q                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-Q                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-V                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-V                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  L-V                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  L-V                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-G                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-G                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  I-S                    PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  I-S                    PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-NLIGNES              PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  W-NLIGNES              PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
           05  W-NOSEQ.                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-SEQUENCE         PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
               10  W-SEQUENCE         PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-NIVRUPT              PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  W-NIVRUPT              PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-RUPTIMPR             PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  W-RUPTIMPR             PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-POSPAGE              PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  W-POSPAGE              PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-OCCURENCES           PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  W-OCCURENCES           PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-LGPHYS               PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  W-LGPHYS               PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ITEM-TS                PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  ITEM-TS                PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CODE-RETOUR-SQL        PIC S9(4)     COMP VALUE ZERO.            
      *--                                                                       
           05  CODE-RETOUR-SQL        PIC S9(4) COMP-5 VALUE ZERO.              
      *}                                                                        
               88  SQL-FIN-CURSEUR    VALUE +100.                               
               88  SQL-NON-TROUVE     VALUE +100.                               
               88  SQL-TROUVE         VALUE +0.                                 
               88  SQL-DOUBLE         VALUE -811.                               
               88  SQL-VALIDE         VALUES +0 +100 -811.                      
           05  W-TYPEDIT           PIC X       VALUE SPACES.                    
               88  W-EDIT132                   VALUES 'A' 'C'.                  
               88  W-EDIT198                   VALUES 'B' 'D'.                  
               88  W-EDITION                   VALUES ' ' 'C' 'D'.              
           05  W-DEBA.                                                          
               10  W-DEB           PIC 999         VALUE ZERO.                  
           05  W-LGNA.                                                          
               10  W-LGN           PIC 999         VALUE ZERO.                  
           05  W-SAUT.                                                          
               10  W-SAUTPAGE          PIC Z.                                   
           05  W-RESULTATS.                                                     
               10  FILLER          OCCURS 15.                                   
                   15  W-LIGNECALC     PIC S999    COMP-3.                      
                   15  W-RESULTAT.                                              
                       20  W-RESULT        PIC S9(15)  COMP-3.                  
                   15  W-NBDEC         PIC S999    COMP-3.                      
                   15  W-TYPCALC       PIC XX.                                  
           05  NOM-TS              PIC X(8)        VALUE SPACES.                
           05  W-APPLID            PIC X(8)        VALUE SPACES.                
           05  W-CLASSE-CHAMP      PIC X           VALUE SPACES.                
           05  W-TYPCHAMP          PIC X           VALUE LOW-VALUE.             
           05  W-LGCHAMP           PIC S999 COMP-3 VALUE ZERO.                  
           05  W-DECCHAMP          PIC S999 COMP-3 VALUE ZERO.                  
           05  W-NOCALCUL          PIC S999 COMP-3 VALUE ZERO.                  
           05  W-SKIPBEFORE        PIC S999 COMP-3 VALUE ZERO.                  
           05  W-SKIPAFTER         PIC S999 COMP-3 VALUE ZERO.                  
           05  W-CHCALCA.                                                       
               10  W-CHCALC        PIC 999         VALUE ZERO.                  
           05  W-DECIMA.                                                        
               10  W-DECIM         PIC S9   COMP-3 VALUE ZERO.                  
           05  W-NOMCHAMP          PIC X(18)       VALUE SPACES.                
           05  W-EDPAGE            PIC ZZ9.                                     
           05  W-EDHEURE           PIC Z9.                                      
           05  W-EDMINUTE          PIC 99.                                      
           05  W-TIME              PIC 9(6).                                    
           05  FILLER REDEFINES W-TIME.                                         
               10  W-HEURE         PIC XX.                                      
               10  W-MINUTE        PIC XX.                                      
               10  W-SECONDE       PIC XX.                                      
           05  W-DATEJ.                                                         
               10  W-SS            PIC XX.                                      
               10  W-AAMMJJ.                                                    
                   15  W-AA            PIC XX.                                  
                   15  W-MM            PIC XX.                                  
                   15  W-JJ            PIC XX.                                  
           05  W-NOMCALL           PIC X(8)        VALUE SPACES.                
       01  WS.                                                                  
           05  W-SQLTRACE             VALUE SPACES.                             
               10  SQL-NOMTABLE           PIC X(8).                             
               10  SQL-FONCTION           PIC X(8).                             
                   88  FONCT-SELECT       VALUE ' SELECT '.                     
                   88  FONCT-OPEN         VALUE ' OPEN   '.                     
                   88  FONCT-CLOSE        VALUE ' CLOSE  '.                     
                   88  FONCT-FETCH        VALUE ' FETCH  '.                     
                   88  FONCT-DELETE       VALUE ' DELETE '.                     
                   88  FONCT-UPDATE       VALUE ' UPDATE '.                     
                   88  FONCT-COMMIT       VALUE ' COMMIT '.                     
               10  SQL-CODE               PIC ---9.                             
           05  W-SQL-MESS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-SQL-LRECL     PIC S9(8) COMP  VALUE +72.                   
      *--                                                                       
               10  W-SQL-LRECL     PIC S9(8) COMP-5  VALUE +72.                 
      *}                                                                        
               10  W-SQL-MSGS.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *            15  W-SQL-LGTH      PIC S9(4)   COMP   VALUE +288.           
      *--                                                                       
                   15  W-SQL-LGTH      PIC S9(4) COMP-5   VALUE +288.           
      *}                                                                        
                   15  W-SQL-MSG       PIC X(72)   OCCURS 4.                    
           05  W-TYPREPORT         PIC X(10)       VALUE SPACES.                
           05  W-MREPORT           PIC X(10)       VALUE '$$$$$$$$$$'.          
           05  W-AREPORTER         PIC X(10)       VALUE 'A REPORTER'.          
           05  W-REPORT            PIC X(10)       VALUE '    REPORT'.          
           05  W-TOTREPORT         PIC X(10)       VALUE '     TOTAL'.          
           05  Z-SOUSPROG          PIC X(128)      VALUE SPACES.                
      ******************************************************************        
      *        TYPES DE LIGNES                                                  
      ******************************************************************        
           05  W-TYPLI             PIC X           VALUE LOW-VALUE.             
           05  W-TYPLC             PIC X           VALUE '0'.                   
           05  W-TYPLE             PIC X           VALUE '1'.                   
           05  W-TYPLD             PIC X           VALUE '2'.                   
           05  W-TYPLT             PIC X           VALUE '4'.                   
           05  W-TYPLF             PIC X           VALUE '5'.                   
           05  W-TYPLB             PIC X           VALUE '6'.                   
           05  W-TYPLR             PIC X           VALUE '3'.                   
       01  FEG100-ENREG.                                                        
           05  FEG100-CLE.                                                      
               10  FEG100-ETAT    PIC X(8).                                     
               10  FEG100-NSEQ    PIC S9(7) COMP-3 VALUE ZERO.                  
               10  FEG100-NSOC    PIC X(3)         VALUE SPACES.                
               10  FEG100-NLIEU   PIC X(3)         VALUE SPACES.                
               10  FEG100-CRITERE PIC X(5)         VALUE SPACES.                
           05  FEG100-DATA.                                                     
               10  FEG100-ASA         PIC X.                                    
               10  FEG100-LIGNE       PIC X(198).                               
       01  WORK-BEG060.                                                         
           05  EG-NOMETAT             PIC X(6)      VALUE SPACES.               
      *===============================================================*         
      *         ZONES GENERALES DE L'ETAT                             *         
      *===============================================================*         
           05  EG-COMMUN.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  EG-LGRUPT              PIC S9(4)     COMP.                   
      *--                                                                       
               10  EG-LGRUPT              PIC S9(4) COMP-5.                     
      *}                                                                        
               10  EG-RUPTCHTPAGE         PIC S999      COMP-3.                 
               10  EG-RUPTRAZPAGE         PIC S999      COMP-3.                 
               10  EG-LARGEUR             PIC S999      COMP-3.                 
               10  EG-LONGUEUR            PIC S999      COMP-3.                 
               10  EG-LONGORIG            PIC S999      COMP-3.                 
               10  EG-I-LIGNEI            PIC S999      COMP-3.                 
               10  EG-I-LIGNEC            PIC S999      COMP-3.                 
               10  EG-I-LIGNEE            PIC S999      COMP-3.                 
               10  EG-I-LIGNED            PIC S999      COMP-3.                 
               10  EG-I-LIGNER            PIC S999      COMP-3.                 
               10  EG-I-LIGNET            PIC S999      COMP-3.                 
               10  EG-I-LIGNEF            PIC S999      COMP-3.                 
               10  EG-I-LIGNEB            PIC S999      COMP-3.                 
               10  EG-I-SOC               PIC S999      COMP-3.                 
               10  EG-I-MAG               PIC S999      COMP-3.                 
               10  EG-I-DECIM             PIC S999      COMP-3.                 
               10  EG-NOPAGE              PIC S999      COMP-3.                 
               10  EG-CNTLIGNE            PIC S999      COMP-3.                 
               10  EG-NSOC                PIC XXX.                              
               10  EG-DATE                PIC X(8).                             
               10  EG-MOIS                PIC X(8).                             
               10  EG-HEUREJ              PIC 9(8).                             
               10  EG-TIME      REDEFINES EG-HEUREJ.                            
                   15  EG-HEURE               PIC XX.                           
                   15  EG-MINUTE              PIC XX.                           
                   15  EG-SECONDE             PIC XX.                           
                   15  EG-CENTIEME            PIC XX.                           
               10  EG-ENTETE              PIC X.                                
                   88  EG-NEWPAGE                       VALUES 'O' 'C'.         
                   88  EG-NEXTPAGE                      VALUE  'O'.             
                   88  EG-TETECRE                       VALUE  'C'.             
               10  EG-IG                  PIC X.                                
      *===============================================================*         
      *         ZONES DE RUPTURES DE L'ETAT                           *         
      *===============================================================*         
               10  EG-RUPT                OCCURS 15.                            
                   15  EGR-NORUPT             PIC S999      COMP-3.             
                   15  EGR-I-CHDESC           PIC S999      COMP-3.             
                   15  EGR-LGPHYS             PIC S999      COMP-3.             
                   15  EGR-DESCIG             PIC S9        COMP-3.             
      *===============================================================*         
      *         LIGNES DE L'ETAT                                      *         
      *===============================================================*         
           05  EG-LIGNES.                                                       
               10  EG-LIGNE               OCCURS 66.                            
                   15  EGL-TYPLIGNE           PIC X.                            
                   15  EGL-NOLIGNE            PIC S999      COMP-3.             
                   15  EGL-CONTINUER          PIC X.                            
                   15  EGL-SKIPBEFORE         PIC S999      COMP-3.             
                   15  EGL-SKIPAFTER          PIC S999      COMP-3.             
                   15  EGL-RUPTIMPR           PIC S999      COMP-3.             
                   15  EGL-POSREPORT          PIC S999      COMP-3.             
                   15  EGL-I-CHVAR            PIC S999      COMP-3.             
                   15  EGL-I-CHVENT           PIC S999      COMP-3.             
                   15  EGL-I-CHTIMP           PIC S999      COMP-3.             
                   15  EGL-I-TOTAL            PIC S999      COMP-3.             
                   15  EGL-I-CALCUL           PIC S999      COMP-3.             
                   15  EGL-POSPAGE            PIC S999      COMP-3.             
                   15  EGL-FIXES              PIC X(255).                       
      *===============================================================*         
      *         CHAMPS D'EDITION DE L'ETAT                            *         
      *===============================================================*         
           05  EG-CHAMP.                                                        
               10  EGC-CHAMPI.                                                  
                   15  EGC-IMPRIME            PIC X     OCCURS 255.             
               10  EGC-CHAMP              OCCURS 255.                           
                   15  EGC-TYPLIGNE           PIC X.                            
                   15  EGC-NOLIGNE            PIC S999      COMP-3.             
                   15  EGC-CONTINUER          PIC X.                            
                   15  EGC-POSCHAMP           PIC S999      COMP-3.             
                   15  EGC-RUPTIMPR           PIC S999      COMP-3.             
      *            15  EGC-IMPRIME            PIC X.                            
                   15  EGC-CLASSE             PIC X.                            
      *===============================================================*         
      *         CHAMPS CONSTANTS                                      *         
      *===============================================================*         
                   15  EGC-CONSTANT.                                            
                       20  EGK-I-CHAMPC           PIC S999      COMP-3.         
                       20  EGK-I-LCHAMP           PIC S999      COMP-3.         
                       20  EGK-NOMCHAMPS          PIC X(8).                     
                       20  EGK-SOUSTABLE          PIC X(5).                     
                       20  EGK-I-CHCOND1          PIC S999      COMP-3.         
                       20  EGK-CONDITION          PIC XX.                       
                       20  EGK-I-CHCOND2          PIC S999      COMP-3.         
      *===============================================================*         
      *         CHAMPS VARIABLES                                      *         
      *===============================================================*         
                   15  EGC-VARIABLES         REDEFINES EGC-CONSTANT.            
                       20  EGV-I-CHDESC           PIC S999      COMP-3.         
                       20  EGV-I-CHCOND1          PIC S999      COMP-3.         
                       20  EGV-CONDITION          PIC XX.                       
                       20  EGV-I-CHCOND2          PIC S999      COMP-3.         
                       20  EGV-I-MCHAMP           PIC S999      COMP-3.         
                       20  EGV-I-LCHAMP           PIC S999      COMP-3.         
                       20  EGV-I-CHTOT            PIC S999      COMP-3.         
      *===============================================================*         
      *         CONTENU DES CHAMPS CONSTANTS (MAX = 20)               *         
      *===============================================================*         
               10  EGK-LIBCHAMP            OCCURS 20.                           
                   15  EGK-LIBCHAMPC.                                           
                       20  EGK-LIBCHAMP-L         PIC S999      COMP-3.         
                       20  EGK-LIBCHAMP-V         PIC X(64).                    
      *===============================================================*         
      *         MASQUES DES CHAMPS VARIABLES (MAX = 120)              *         
      *===============================================================*         
               10  EGV-MASKCHAMP           OCCURS 120.                          
                   15  EGV-MASKCHAMPV.                                          
                       20  EGV-MASKCHAMP-L        PIC S999      COMP-3.         
                       20  EGV-MASKCHAMP-V        PIC X(30).                    
      *===============================================================*         
      *         CHAMPS TOTAUX                                         *         
      *===============================================================*         
            05  EG-TOTAUX.                                                      
                10  EGT-TOTAUX            OCCURS 100.                           
                   15  EGT-TYPLIGNE           PIC X.                            
                   15  EGT-NOLIGNE            PIC S999      COMP-3.             
                   15  EGT-CONTINUER          PIC X.                            
                   15  EGT-POSCHAMP           PIC S999      COMP-3.             
                   15  EGT-RUPTRAZ            PIC S999      COMP-3.             
                   15  EGT-I-CHDESC           PIC S999      COMP-3.             
                   15  EGT-I-CHTOT            PIC S999      COMP-3.             
                   15  EGT-I-CHAMP            PIC S999      COMP-3.             
      *===============================================================*         
      *         DESCRIPTION DES CHAMPS DE L'ETAT                      *         
      *===============================================================*         
           05  EG-DESCRIPTION.                                                  
               10  EGD-CHAMPS             OCCURS 300.                           
                   15  EGD-TYPCHAMP           PIC X.                            
                   15  EGD-LGCHAMP            PIC S999      COMP-3.             
                   15  EGD-DECCHAMP           PIC S999      COMP-3.             
                   15  EGD-OCCURENCES         PIC S999      COMP-3.             
                   15  EGD-NOCALCUL           PIC S999      COMP-3.             
                   15  EGD-CONTENU            PIC X(18).                        
                   15  FILLER REDEFINES EGD-CONTENU.                            
                       20  EGD-I-CONT         PIC S999      COMP-3.             
                       20  FILLER             PIC X(16).                        
                   15  FILLER REDEFINES EGD-CONTENU.                            
                       20  FILLER             PIC X(10).                        
                       20  EGD-INITCTR.                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                    25  EGD-INITC          PIC S9(9)     COMP.           
      *--                                                                       
                           25  EGD-INITC          PIC S9(9) COMP-5.             
      *}                                                                        
                       20  EGD-INCREMENT.                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                    25  EGD-INCR           PIC S9(9)     COMP.           
      *--                                                                       
                           25  EGD-INCR           PIC S9(9) COMP-5.             
      *}                                                                        
           05  EG-CONTENUS-LONG.                                                
               10  EGD-CONTENUL          PIC X(64)   OCCURS 50.                 
      *===============================================================*         
      *         POSITIONS DES CHAMPS DE LA DSECT                      *         
      *===============================================================*         
           05  EG-DSECT-CHAMPS.                                                 
               10  EGD-DSECT              OCCURS 256.                           
                   15  EGD-POSDSECT           PIC S999      COMP-3.             
                   15  EGD-I-CHDESC           PIC S999      COMP-3.             
      *===============================================================*         
      *         CALCULS D'UN ETAT                                     *         
      *===============================================================*         
           05  EG-CALCULS.                                                      
               10  EGQ-CALCULS            OCCURS 150.                           
                   15  EGQ-NOCALCUL           PIC S999      COMP-3.             
                   15  EGQ-LIGNECALC          PIC S999      COMP-3.             
                   15  EGQ-I-CHCALC1          PIC S999      COMP-3.             
                   15  EGQ-TYPCALCUL          PIC XX.                           
                   15  EGQ-I-CHCALC2          PIC S999      COMP-3.             
                                                                                
