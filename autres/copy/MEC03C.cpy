      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC03                             *         
      *****************************************************************         
      *--> LONGUEUR MAX ADMIS POUR UNE COMMAREA 32 767.                         
       01  MEC03C-COMMAREA.                                                     
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           02  MEC03C-NBP                   PIC 9(03).                          
           02  MEC03C-NBP-MAX               PIC 9(03) VALUE 40.                 
           02  MEC03C-DATA-ENTREE.                                              
             05  MEC03C-TAB-CODIC  OCCURS  41.                                  
               10  MEC03C-CODIC-ENT         PIC  X(7).                          
               10  MEC03C-SOCIETE-ENT       PIC  X(3).                          
               10  MEC03C-LIEU-ENT          PIC  X(3).                          
               10  MEC03C-WDACEM            PIC 9(01).                          
                   88 MEC03C-WDACEM-OUI    VALUE 1.                             
                   88 MEC03C-WDACEM-NON    VALUE 0.                             
               10  MEC03C-ETAT-RESERVATION  PIC 9(01).                          
                   88 MEC03C-AUCUN-STOCK   VALUE 0.                             
                   88 MEC03C-STOCK-DISPO   VALUE 1.                             
                   88 MEC03C-STOCK-DACEM   VALUE 2.                             
             05    FILLER                   PIC  X(99).                         
      *--> DONNEES RESULTANTES.                                                 
           02  MEC03C-DATA-SORTIE.                                              
      * NBR MESSSAGE ENVOY�                                                     
               10  MEC03C-NBR-MESSAGE-ENVOYE       PIC  9(09).                  
               10  MEC03C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC03C-CDRET-OK                  VALUE '0'.              
                   88  MEC03C-CDRET-ERR-NON-BLQ         VALUE '1'.              
                   88  MEC03C-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.              
                   88  MEC03C-CDRET-ERR-BLQ             VALUE '8'.              
                   88  MEC03C-CDRET-ERR-DB2-BLQ         VALUE '9'.              
               10  MEC03C-CODE-DB2                 PIC ++++9.                   
               10  MEC03C-CODE-DB2-DISPLAY         PIC  X(05).                  
               10  MEC03C-STOCK.                                                
                   20 MEC03C-NCODIC-SOR      PIC  X(07) VALUE SPACES.           
                   20 MEC03C-NSOCIETE-SOR    PIC  X(03) VALUE SPACES.           
                   20 MEC03C-NLIEU-SOR       PIC  X(03) VALUE SPACES.           
                   20 MEC03C-QSTOCK-SOR      PIC  X(05) VALUE '00000'.          
                   20 MEC03C-DDISPO-SOR      PIC  X(08) VALUE SPACES.           
                   20 MEC03C-CC-DEPOT-SOR    PIC  X(07) VALUE '0000000'.        
                   20 MEC03C-TIMESTP-SOR     PIC  9(13)   VALUE 0.              
              10  MEC03C-STOCK-CC.                                              
                   20 MEC03C-CC-NCODIC-SOR   PIC  X(07) VALUE SPACES.           
                   20 MEC03C-CC-NSOCIETE-SOR PIC  X(03) VALUE SPACES.           
                   20 MEC03C-CC-NLIEU-SOR    PIC  X(03) VALUE SPACES.           
                   20 MEC03C-CC-BUYABLE-SOR  PIC  X(07) VALUE '0000000'.        
                   20 MEC03C-CC-VISIBLE-SOR  PIC  X(01) VALUE '0'.              
                   20 MEC03C-CC-TIMESTP-SOR  PIC  9(13)  VALUE 0.               
      ********* FIN DE LA ZONE DE COMMUNICATION      *************              
                                                                                
