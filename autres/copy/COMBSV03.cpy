      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************          
      *    COMMAREA DE MBSV03 MODULE APPELE PAR LINK POUR RECHERCHER            
      *    UN SAV ASSOCIE A UN CODE POSTAL ET UNE FAMILLE                       
      *    LES PARAMETRES A PASSER SONT LE CODE INSEE                           
      *               ---  ET  ---      LE CODIC  OU  LA FAMILLE                
      ****************************************************************          
      * PROGRAMME  MBSV03                                                       
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MBSV03-LONG-COMMAREA PIC S9(4) COMP VALUE +265.                 
      *--                                                                       
       01  COMM-MBSV03-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +265.               
      *}                                                                        
       01  Z-COMMAREA-MBSV03.                                                   
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MBSV03-I.                                                    
      *                                                                         
              05  COMM-MBSV03-CFAM     PIC X(5).                                
              05  COMM-MBSV03-NCODIC   PIC X(7).                                
      *                                                                         
              05  COMM-MBSV03-CGCPLT   PIC X(5).                                
              05  COMM-MBSV03-CGRP     PIC X(5).                                
              05  COMM-MBSV03-DMVT     PIC X(8).                                
              05  COMM-MBSV03-CINSEE   PIC X(5).                                
              05  COMM-MBSV03-NLIEU    PIC X(3).                                
              05  COMM-MBSV03-NSOCIETE PIC X(3).                                
      *                                                                         
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MBSV03-O.                                                    
      *                                                                         
              05  COMM-MBSV03-TABLE    OCCURS 20.                               
                  10  COMM-MBSV03-NOSAV1   PIC X(3).                            
                  10  COMM-MBSV03-NOSAV2   PIC X(3).                            
                  10  COMM-MBSV03-WTAUXSAV PIC 9(3) COMP-3.                     
      *                                                                         
              05  COMM-MBSV03-NBSAV        PIC 9(3) COMP-3.                     
              05  COMM-MBSV03-CSECTEUR1    PIC X(5).                            
              05  COMM-MBSV03-CSECTEUR2    PIC X(5).                            
      *---------------------------------------CODE RETOUR                       
      * ERREUR --> ABANDON PROGRAMME                                            
              05 COMM-MBSV03-CODRET        PIC    X VALUE '0'.                  
                 88 COMM-MBSV03-OK         VALUE '0'.                           
                 88 COMM-MBSV03-KO         VALUE '1'.                           
                 88 COMM-MBSV03-KK         VALUE '2'.                           
      * MESSAGE D'ERREUR                                                        
              05 COMM-MBSV03-MSG           PIC X(50).                           
                                                                                
