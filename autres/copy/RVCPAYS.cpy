      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE CPAYS CODE PAYS SUR 3 CARACTERES       *        
      *----------------------------------------------------------------*        
       01  RVCPAYS .                                                            
           05  CPAYS-CTABLEG2    PIC X(15).                                     
           05  CPAYS-CTABLEG2-REDEF REDEFINES CPAYS-CTABLEG2.                   
               10  CPAYS-CPAYS           PIC X(03).                             
           05  CPAYS-WTABLEG     PIC X(80).                                     
           05  CPAYS-WTABLEG-REDEF  REDEFINES CPAYS-WTABLEG.                    
               10  CPAYS-LPAYS           PIC X(20).                             
               10  CPAYS-LPAYS2          PIC X(20).                             
               10  CPAYS-CODE            PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVCPAYS-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CPAYS-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  CPAYS-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  CPAYS-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  CPAYS-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
