      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE MRM60                                          *  00020001
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LONG                     PIC S9(3) COMP-3 VALUE 489.      00060005
       01  TS-DONNEES.                                                  00070000
              10 TS-NCODIC             PIC X(7).                        00080002
              10 TS-A-TRAITER          PIC X.                           00090002
              10 TS-S-1                PIC S9(5)    COMP-3.             00100002
              10 TS-S-2                PIC S9(5)    COMP-3.             00110002
              10 TS-S-3                PIC S9(5)    COMP-3.             00120002
              10 TS-S-4                PIC S9(5)    COMP-3.             00130002
              10 TS-QNBMAG             PIC S9(3)    COMP-3.             00140002
              10 TS-CFAM               PIC X(5).                        00150002
              10 TS-CMARQ              PIC X(5).                        00160002
              10 TS-LREFFOURN          PIC X(20).                       00170002
              10 TS-VALIDATION         PIC X.                           00180002
              10 TS-QSO-DIFF           PIC S9(7)    COMP-3.             00190005
              10 TS-QSA-DIFF           PIC S9(7)    COMP-3.             00200005
              10 TS-QSOSIMU            PIC S9(7)    COMP-3.             00210005
              10 TS-QSASIMU            PIC S9(7)    COMP-3.             00220005
              10 TS-DETAIL-GROUPE      OCCURS 10.                       00230002
                 15 TS-DETAIL-MRM60.                                    00240003
                    20 TS-RANGP           PIC S9(5)    COMP-3.          00250003
                    20 TS-POIDSP          PIC S9(3)V99 COMP-3.          00260003
                    20 TS-NBREFP          PIC S9(5)    COMP-3.          00270003
                    20 TS-QPV             PIC S9(7)    COMP-3.          00280003
                    20 TS-QRANGHER        PIC S9(5)    COMP-3.          00290003
                    20 TS-QPOIDSR         PIC S9(3)V99 COMP-3.          00300003
                    20 TS-QNBREFR         PIC S9(5)    COMP-3.          00310003
                    20 TS-QRANG           PIC S9(5)    COMP-3.          00320003
                    20 TS-QNBREF          PIC S9(5)    COMP-3.          00330003
                    20 TS-PREV-CODIC      PIC X.                        00340003
                    20 TS-QPREV-CODIC     PIC S9(7)    COMP-3.          00350003
                    20 TS-ATRAITER        PIC X.                        00360003
                 15 TS-DETAIL-AUTRE.                                    00370003
                    20 TS-LSTATCOMP       PIC X(3).                     00380003
                    20 TS-CEXPO           PIC X(5).                     00390003
                                                                                
