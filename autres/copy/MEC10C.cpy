      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * COMM DU MEC10: TRANSFO RESERVATION EN VENTE                             
      ******************************************************************        
      *--> DONNEES FOURNIES PAR LE PROGRAMME APPELANT.                          
           05  MEC10C-ENTREE.                                                   
               10  MEC10C-NSOCIETE        PIC  XXX.                             
               10  MEC10C-NLIEU           PIC  XXX.                             
               10  MEC10C-NRESERVATION    PIC  X(7).                            
               10  MEC10C-NVENTE          PIC  X(7).                            
               10  MEC10C-DVENTE          PIC  X(8).                            
      *--> DONNEES RESULTANTES.                                                 
           05  MEC10C-SORTIE.                                                   
               10  MEC10C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC10C-OK                  VALUE '0'.                    
                   88  MEC10C-KO                  VALUE '1'.                    
      *--> DONNEES FOURNIES PAR LE PROGRAMME APPELANT.                          
      ** Code �v�nement Click & Collect                                         
           02  MEC10C-CCEVENT   PIC X(4).                                       
      ********* FIN DE LA ZONE DE COMMUNICATION DU MEC10 *************          
                                                                                
