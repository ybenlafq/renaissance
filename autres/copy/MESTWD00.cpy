      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      ******************************************************************
      * - MESTWD00 COPY GENERALISES  49974 OCTETS                      *
      *   = 105 DE DEFINITION D ENTETE                                 *
      *   + 34  DE DEFINITION DE CODE RETOUR                           *
      *   + 49835 DE MESSAGE REEL                                      *
      * COPY POUR LA SERIE DE TRANSACTION TWD00 <-> MGD00              *
      ******************************************************************
              05 MESTWD00-ENTETE.
                 10 MESTWD00-TYPE        PIC X(03).
                 10 MESTWD00-NSOCMSG     PIC X(03).
                 10 MESTWD00-NLIEUMSG    PIC X(03).
                 10 MESTWD00-NSOCDST     PIC X(03).
                 10 MESTWD00-NLIEUDST    PIC X(03).
                 10 MESTWD00-NORD        PIC 9(08).
                 10 MESTWD00-LPROG       PIC X(10).
                 10 MESTWD00-DJOUR       PIC X(08).
                 10 MESTWD00-WSID        PIC X(10).
                 10 MESTWD00-USER        PIC X(10).
                 10 MESTWD00-CHRONO      PIC 9(07).
                 10 MESTWD00-NBRMSG      PIC 9(07).
                 10 MESTWD00-NBRENR      PIC 9(5).
                 10 MESTWD00-TAILLE      PIC 9(5).
                 10 MESTWD00-VERSION     PIC X(2).
                 10 MESTWD00-FILLER      PIC X(18).
              05 MESTWD00-RETOUR.
                 10 MESTWD00-CODE-RETOUR.
                    15  MESTWD00-CODRET  PIC X(1).
                         88  MESTWD00-CODRET-OK        VALUE ' '.
                         88  MESTWD00-CODRET-ERREUR    VALUE '1'.
                    15  MESTWD00-LIBERR  PIC X(33).
              05 MESTWD00-DATA           PIC X(49835).
              05 MESTWD00-DATAS REDEFINES MESTWD00-DATA.
      * LONGUEUR DE 100 OCTETS
                 10 MESTWD00-CONTROLE.
                    15 MESTWD00C-NSOCDEPOT     PIC X(3).
                    15 MESTWD00C-NDEPOT        PIC X(3).
                    15 MESTWD00C-DRAFALE       PIC X(8).
                    15 MESTWD00C-NRAFALE       PIC X(3).
                    15 MESTWD00C-DHSAISIE      PIC X(2).
                    15 MESTWD00C-DMSAISIE      PIC X(2).
                    15 MESTWD00C-DSAISIE       PIC X(8).
                    15 MESTWD00C-DVALIDITE     PIC X(8).
                    15 MESTWD00C-WSTOCKRACK    PIC X(1).
                    15 MESTWD00C-WSTOCKVRAC    PIC X(1).
                    15 MESTWD00C-WSTOCKSOL     PIC X(1).
                    15 MESTWD00C-WETATRAFALE   PIC X(1).
                    15 MESTWD00C-QTRANCVOL1    PIC 9(5).
                    15 MESTWD00C-QTRANCVOL2    PIC 9(5).
                    15 MESTWD00C-QTRANCPOI1    PIC 9(5).
                    15 MESTWD00C-QTRANCPOI2    PIC 9(5).
                    15 MESTWD00C-WLISTESAT     PIC X(1).
                    15 MESTWD00C-WAUTOMATIQUE  PIC X(1).
                    15 MESTWD00C-TRANSFERT     PIC X(1).
                    15 MESTWD00C-FILLER        PIC X(36).
      * LONGUEUR DE 49 OCTETS * 1000 = 49000
                 10 MESTWD00-DONNEES           OCCURS  1000.
                    15 MESTWD00D-NCODIC        PIC X(7).
                    15 MESTWD00D-WSEQ          PIC 9(3).
                    15 MESTWD00D-WVENTMUT      PIC X(1).
                    15 MESTWD00D-NSOC          PIC X(3).
                    15 MESTWD00D-NMAGASIN      PIC X(3).
                    15 MESTWD00D-NORIGINE      PIC X(7).
                    15 MESTWD00D-CMODSTOCK     PIC X(5).
                    15 MESTWD00D-CZONE         PIC X(2).
                    15 MESTWD00D-CNIVEAU       PIC X(2).
                    15 MESTWD00D-NPOSITION     PIC X(3).
                    15 MESTWD00D-WTYPEMPL      PIC X(1).
                    15 MESTWD00D-QCDERES       PIC 9(5).
                    15 MESTWD00D-QCDEPREL      PIC 9(5).
                    15 MESTWD00D-FILLER        PIC X(2).
                 10 MESTWD00-DATA-INUTILE      PIC X(735).
      
