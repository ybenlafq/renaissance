      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION OP ENT�TE STANDARD (MUTATION)         * 00031002
      * NOM FICHIER.: ROESTD                                          * 00032005
      *---------------------------------------------------------------* 00034000
      * CR   .......: 12/10/2011  17:46:00                            * 00035000
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR....: 1017                                            * 00037102
      ***************************************************************** 00039000
      *                                                                 00040000
       01  ROESTD.                                                      00050000
      * TYPE ENREGISTREMENT                                             00060004
           05      ROESTD-TYP-ENREG       PIC  X(0006).                 00070000
      * CODE SOCI�T�                                                    00080004
           05      ROESTD-CSOCIETE        PIC  X(0005).                 00090003
      * NUM�RO D OP                                                     00091004
           05      ROESTD-NOP             PIC  X(0015).                 00100003
      * NUM�RO DE SITE                                                  00101004
           05      ROESTD-NSITE           PIC  9(0003).                 00110003
      * NB DE LIGNES DE L OP                                            00111004
           05      ROESTD-NBLIG-OP        PIC  9(0005).                 00120003
      * TYPE D OP                                                       00121004
           05      ROESTD-TYPE-OP         PIC  9(0002).                 00130001
      * CODE CLIENT LIVR�                                               00131004
           05      ROESTD-CCLIENT         PIC  X(0013).                 00140003
      * ADRESSE DE LIVRAISON                                            00141004
           05      ROESTD-ADR-LIVRAISON.                                00150001
      * RAISON SOCIALE - ADRESSE DE LIVRAISON                           00150104
              10   ROESTD-RAISOC-LIVR     PIC  X(0035).                 00151001
      * ADRESSE 1- ADRESSE DE LIVRAISON                                 00152004
              10   ROESTD-ADR1-LIVR       PIC  X(0035).                 00160001
      * ADRESSE 2 - ADRESSE DE LIVRAISON                                00160104
              10   ROESTD-ADR2-LIVR       PIC  X(0035).                 00161001
      * ADRESSE 3 - ADRESSE DE LIVRAISON                                00161104
              10   ROESTD-ADR3-LIVR       PIC  X(0035).                 00162001
      * ADRESSE 4 - ADRESSE DE LIVRAISON                                00162104
              10   ROESTD-ADR4-LIVR       PIC  X(0035).                 00163001
      * CODE POSTAL - ADRESSE DE LIVRAISON                              00163104
              10   ROESTD-CPOSTAL-LIVR    PIC  X(0009).                 00164001
      *  VILLE - ADRESSE DE LIVRAISON                                   00164104
              10   ROESTD-VILLE-LIVR      PIC  X(0035).                 00165001
      *  CODE PAYS - ADRESSE DE LIVRAISON                               00165104
              10   ROESTD-CPAYS-LIVR      PIC  X(0003).                 00166001
      * LIBELL� PAYS LIVRAISON                                          00166104
              10   ROESTD-LPAYS-LIVR      PIC  X(0035).                 00167001
      * ADRESSE INTERM�DIAIRE                                           00167104
           05      ROESTD-ADR-INTERMEDIAIRE.                            00167201
      * RAISON SOCIALE - ADRESSE INTERM�DIAIRE                          00167304
              10   ROESTD-RAISOC-INTER    PIC  X(0035).                 00167401
      * ADRESSE 1 - ADRESSE INTERM�DIAIRE                               00167504
              10   ROESTD-ADR1-INTER      PIC  X(0035).                 00167601
      * ADRESSE 2 - ADRESSE INTERM�DIAIRE                               00167704
              10   ROESTD-ADR2-INTER      PIC  X(0035).                 00167801
      * ADRESSE 3 - ADRESSE INTERM�DIAIRE                               00167904
              10   ROESTD-ADR3-INTER      PIC  X(0035).                 00168001
      * ADRESSE 4 - ADRESSE INTERM�DIAIRE                               00168104
              10   ROESTD-ADR4-INTER      PIC  X(0035).                 00168201
      * CODE POSTAL - ADRESSE INTERM�DIAIRE                             00168304
              10   ROESTD-CPOSTAL-INTER   PIC  X(0009).                 00168401
      * VILLE - ADRESSE INTERM�DIAIRE                                   00168504
              10   ROESTD-VILLE-INTER     PIC  X(0035).                 00168601
      * CODE PAYS - ADRESSE INTERM�DIAIRE                               00168704
              10   ROESTD-CPAYS-INTER     PIC  X(0003).                 00168801
      * LIBELL� PAYS - ADRESSE INTERM�DIAIRE                            00168904
              10   ROESTD-LPAYS-INTER     PIC  X(0035).                 00169001
      * ADRESSE FACTURATION                                             00169104
           05      ROESTD-ADR-FACTURATION.                              00169201
      * RAISON SOCIALE - ADRESSE FACTURATION                            00169304
              10   ROESTD-RAISOC-FACT     PIC  X(0035).                 00169401
      * ADRESSE 1 - ADRESSE FACTURATION                                 00169504
              10   ROESTD-ADR1-FACT       PIC  X(0035).                 00169601
      * ADRESSE 2 - ADRESSE FACTURATION                                 00169704
              10   ROESTD-ADR2-FACT       PIC  X(0035).                 00169801
      * ADRESSE 3 - ADRESSE FACTURATION                                 00169904
              10   ROESTD-ADR3-FACT       PIC  X(0035).                 00170001
      * ADRESSE 4 - ADRESSE FACTURATION                                 00170104
              10   ROESTD-ADR4-FACT       PIC  X(0035).                 00170201
      * CODE POSTAL - ADRESSE FACTURATION                               00170304
              10   ROESTD-CPOSTAL-FACT    PIC  X(0009).                 00170401
      * VILLE - ADRESSE FACTURATION                                     00170504
              10   ROESTD-VILLE-FACT      PIC  X(0035).                 00170601
      * CODE PAYS - ADRESSE FACTURATION                                 00170704
              10   ROESTD-CPAYS-FACT      PIC  X(0003).                 00170801
      * LIBELL� PAYS - ADRESSE FACTURATION                              00170904
              10   ROESTD-LPAYS-FACT      PIC  X(0035).                 00171001
      * ZONE G�OGRAPHIQUE                                               00171104
           05      ROESTD-ZONE-GEO        PIC  X(0020).                 00171201
      * CODE TRANSPORTEUR                                               00171304
           05      ROESTD-CTRANSPORTEUR   PIC  X(0013).                 00171403
      * MODE DE TRANSPORT                                               00171504
           05      ROESTD-MODE-TRANSPORT  PIC  X(0003).                 00171601
      * MODE EXP�DITION                                                 00171704
           05      ROESTD-MODE-EXPED      PIC  X(0003).                 00171801
      * � PALETTISER                                                    00171904
           05      ROESTD-A-PALETTISER    PIC  9(0001).                 00172001
      * LIVRAISON PARTIELLE                                             00172104
           05      ROESTD-LIVR-PARTIELLE  PIC  9(0001).                 00172201
      * EMPLACEMENT D EXP�DITION                                        00172304
           05      ROESTD-EMPL-EXPED      PIC  X(0020).                 00172401
      * DATE DE LIVRAISON SOUHAIT�E                                     00172504
           05      ROESTD-DLIVRAISON      PIC  X(0008).                 00172601
      * DATE DE PRISE DE COMMANDE                                       00172704
           05      ROESTD-DPRISE-CDE      PIC  X(0008).                 00172803
      * HEURE DE PRISE DE COMMANDE                                      00172904
           05      ROESTD-HPRISE-CDE      PIC  X(0006).                 00173003
      * MONOR�F�RENCE                                                   00173104
           05      ROESTD-MONOREF         PIC  9(0001).                 00173201
      * URGENCE                                                         00173304
           05      ROESTD-URGENCE         PIC  9(0001).                 00173401
      * CONTRE-REMBOURSEMENT                                            00173504
           05      ROESTD-CONTRE-REMB     PIC  9(0001).                 00173601
      * LANGUE                                                          00173704
           05      ROESTD-LANGUE          PIC  X(0003).                 00173801
      *    NUM�RO DE COMMANDE CLIENT                                    00173904
           05      ROESTD-NCDE-CLIENT     PIC  X(0015).                 00174001
      * NUM�RO DE COMMANDE N3                                           00174104
           05      ROESTD-NCDE-N3         PIC  X(0015).                 00174201
      * NUM�RO CONTRAINTE COLISAGE                                      00174304
           05      ROESTD-NCONTRAINTE-COL PIC  9(0003).                 00174401
      * NOMBRE DE BON DE LIVRAISON                                      00174504
           05      ROESTD-NB-BL           PIC  9(0002).                 00174601
      * NOMBRE DE LISTE DE COLISAGE                                     00174704
           05      ROESTD-NB-LST-COLISAGE PIC  9(0002).                 00174801
      * COLISAGE AUTOMATIQUE                                            00174904
           05      ROESTD-COLISAGE-AUTOM  PIC  9(0001).                 00175001
      * POIDS LIMITE COLIS                                              00175104
           05      ROESTD-POIDS-LIM-COLIS PIC  9(0009).                 00175201
      * CARACT�RISTIQUE 1                                               00175304
           05      ROESTD-CARACT1         PIC  X(0020).                 00175401
      * CARACT�RISTIQUE 2                                               00175504
           05      ROESTD-CARACT2         PIC  X(0020).                 00175601
      * CARACT�RISTIQUE 3                                               00175704
           05      ROESTD-CARACT3         PIC  X(0020).                 00175801
      *                                                                 00175904
           05      ROESTD-FIN             PIC  X(0001).                 00176001
                                                                                
