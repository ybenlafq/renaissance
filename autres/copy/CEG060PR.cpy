      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------                                 
       TRAITEMENT-EG060                 SECTION.                                
      *----------------------------------------                                 
           INITIALIZE WS-EG060.                                                 
           INITIALIZE WORK-BEG060.                                              
           INITIALIZE FEG100-ENREG.                                             
           INITIALIZE RVGA01E6.                                                 
           INITIALIZE IG1-ETA                                                   
                      IG2-DAT                                                   
                      IG3-DST                                                   
                      IG4-DOC.                                                  
           MOVE FEXTRAC-NOMETAT          TO EG-NOMETAT.                         
      *... RECUPERATION DU CODE PAYS                                            
           PERFORM SELECT-RTPT99.                                               
           PERFORM TRAITEMENT-INITIALISATION.                                   
      *                                                                         
      *    IF EG-I-MAG NOT = ZERO                                               
      *       PERFORM DETERMINATION-TYPEDIT                                     
      *    END-IF.                                                              
           COMPUTE I-S = EG-LGRUPT + 1.                                         
           PERFORM TRAITEMENT-ETAT.                                             
           IF EGIMP-NBLIGNES-N NOT = ZERO                                       
              MOVE 'F'                  TO EGIMP-DEB-FIN                        
              MOVE SPACES               TO EGIMP-COMMANDE                       
              PERFORM SELECT-RVGA01IG                                           
              IF SQL-TROUVE                                                     
                  PERFORM COMMANDE-IMPRIMANTE                                   
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-ETAT                   SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITEMENT-DEBUT-ETAT.                                       
           PERFORM TRAITEMENT-DETAIL                                            
                   UNTIL FIN-FEXTRAC                                            
                   OR    FEXTRAC-NOMETAT NOT = EG-NOMETAT.                      
           PERFORM TRAITEMENT-FINAL.                                            
      *-----------------------------------------                                
       TRAITEMENT-DEBUT-ETAT             SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITEMENT-PARAMETRES.                                       
           PERFORM TRAITEMENT-DATE-HEURE.                                       
           PERFORM TRAITEMENT-RAZ-INIT.                                         
           PERFORM REMPLISSAGE-DONNEES.                                         
           IF EG-I-MAG NOT = ZERO                                               
              PERFORM DETERMINATION-TYPEDIT                                     
           END-IF.                                                              
           PERFORM INITIALISATION-RUPTURES.                                     
           MOVE EG-NOMETAT           TO EGIMP-CETAT.                            
           MOVE 'D'                  TO EGIMP-DEB-FIN                           
           MOVE SPACES               TO EGIMP-COMMANDE                          
           MOVE ZERO                 TO EGIMP-NBLIGNES-N                        
           PERFORM SELECT-RVGA01IG                                              
           IF SQL-TROUVE                                                        
              IF EGIMP-NBLIGNES-N NOT = ZERO                                    
                 MOVE EGIMP-NBLIGNES-N  TO EG-LONGUEUR                          
                 PERFORM COMMANDE-IMPRIMANTE                                    
              END-IF                                                            
           END-IF.                                                              
           MOVE 'N'                    TO EG-ENTETE.                            
           IF EG-I-LIGNEI NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNEI BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLI                    
                 PERFORM TRAITER-LES-CHAMPS                                     
                 PERFORM ECRITURE-LIGNE                                         
              END-PERFORM                                                       
           END-IF.                                                              
           PERFORM TRAITEMENT-ENTETE.                                           
           PERFORM TRAITEMENT-DETAIL-RUPTURE.                                   
           MOVE 'C'                   TO EG-ENTETE.                             
      *-----------------------------------------                                
       TRAITEMENT-DATE-HEURE             SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I FROM EG-I-LIGNEE BY 1                              
                   UNTIL   I > 66                                               
                   OR EGL-TYPLIGNE (I) = ' '                                    
              PERFORM REMPLISSAGE-DATE-HEURE                                    
           END-PERFORM.                                                         
      *-----------------------------------------                                
       TRAITEMENT-PARAMETRES             SECTION.                               
      *-----------------------------------------                                
           MOVE ZERO                  TO I-K.                                   
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL   I > 300                                              
                   OR EGD-TYPCHAMP (I) = ' '                                    
              IF EGD-CONTENU (I) (1:2) = '$$'                                   
                 PERFORM REMPLISSAGE-PARAMETRES                                 
              ELSE                                                              
                 IF EGD-CONTENU (I) (1:1) = '&'                                 
                    PERFORM REMPLISSAGE-INTERNES                                
                 END-IF                                                         
              END-IF                                                            
              MOVE EGD-LGCHAMP (I)       TO W-LGCHAMP                           
              MOVE EGD-TYPCHAMP (I)      TO W-TYPCHAMP                          
              PERFORM CALCUL-LONGUEUR-PHYS                                      
              IF W-LGPHYS > 18                                                  
                 ADD  1                  TO I-K                                 
                 MOVE I-K                TO EGD-I-CONT (I)                      
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       REMPLISSAGE-PARAMETRES            SECTION.                               
      *-----------------------------------------                                
           IF EGD-CONTENU (I) (1:7) = '$$FNSOC'                                 
              MOVE EG-NSOC             TO EGD-CONTENU (I)                       
              MOVE 3                   TO EGD-LGCHAMP (I)                       
           END-IF.                                                              
           IF EGD-CONTENU (I) (1:7) = '$$FDATE'                                 
                                   OR '$$FMOIS'                                 
              MOVE 'S'                       TO EGD-TYPCHAMP (I)                
              IF EGD-CONTENU (I) (1:7) = '$$FDATE'                              
                 MOVE EG-DATE             TO EGD-CONTENU (I)                    
              ELSE                                                              
                 MOVE EG-MOIS             TO EGD-CONTENU (I)                    
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       REMPLISSAGE-INTERNES              SECTION.                               
      *-----------------------------------------                                
           MOVE ZERO                   TO EGD-LGCHAMP (I).                      
           MOVE ZERO                   TO EGD-DECCHAMP (I).                     
           IF EGD-CONTENU (I) (1:4) = '&CTR'                                    
              MOVE 'I'                  TO EGD-TYPCHAMP (I)                     
              MOVE ZERO                 TO EGD-INITC    (I)                     
              MOVE 1                    TO EGD-INCR     (I)                     
              PERFORM VARYING I-P FROM 1 BY 1                                   
                      UNTIL   I-P > 18                                          
                      OR      EGD-CONTENU (I) (I-P:1) = '('                     
              END-PERFORM                                                       
              IF I-P NOT > 18                                                   
                 PERFORM INITIALISATION-COMPTEURS                               
              END-IF                                                            
           END-IF.                                                              
           IF EGD-CONTENU (I)       = '&NUMREC'                                 
              MOVE 'R'                     TO EGD-TYPCHAMP (I)                  
              MOVE SPACES                  TO EGD-CONTENU (I)                   
           END-IF.                                                              
      *-----------------------------------------                                
       INITIALISATION-COMPTEURS          SECTION.                               
      *-----------------------------------------                                
           MOVE EGD-CONTENU (I)       TO W-NOMCHAMP.                            
           MOVE SPACES                TO EGD-CONTENU (I) (1:10).                
           COMPUTE I-P = I-P + 1.                                               
           MOVE ZERO                  TO I-L.                                   
           INITIALIZE ZWORK-CALCULS.                                            
           PERFORM VARYING I-P FROM I-P BY 1                                    
                   UNTIL   W-NOMCHAMP (I-P:1) = (',' OR ')')                    
              COMPUTE I-L = I-L + 1                                             
              IF W-NOMCHAMP (I-P:1) NOT = '-'                                   
                                AND NOT = '+'                                   
                 MOVE W-NOMCHAMP (I-P:1)     TO ZW-CONTENU2 (I-L:1)             
              ELSE                                                              
                 MOVE W-NOMCHAMP (I-P:1)     TO ZW-TYPCALCUL (1:1)              
              END-IF                                                            
           END-PERFORM.                                                         
           IF I-L > ZERO                                                        
              PERFORM INITIALISER-COMPTEUR                                      
              MOVE EGD-CONTENU (I) (1:4)        TO EGD-INCREMENT (I)            
           END-IF.                                                              
           IF W-NOMCHAMP (I-P:1) = ','                                          
              COMPUTE I-P = I-P + 1                                             
              MOVE ZERO                  TO I-L                                 
              INITIALIZE ZWORK-CALCULS                                          
              PERFORM VARYING I-P FROM I-P BY 1                                 
                      UNTIL   W-NOMCHAMP (I-P:1) = ')'                          
                 COMPUTE I-L = I-L + 1                                          
                 IF W-NOMCHAMP (I-P:1) NOT = '-'                                
                                   AND NOT = '+'                                
                    MOVE W-NOMCHAMP (I-P:1)     TO ZW-CONTENU2 (I-L:1)          
                 ELSE                                                           
                    MOVE W-NOMCHAMP (I-P:1)     TO ZW-TYPCALCUL (1:1)           
                 END-IF                                                         
              END-PERFORM                                                       
              IF I-L > ZERO                                                     
                 PERFORM INITIALISER-COMPTEUR                                   
                 MOVE EGD-CONTENU (I) (1:4)        TO EGD-INITCTR (I)           
              END-IF                                                            
              IF EGD-INCR (I-R) < ZERO                                          
                 COMPUTE EGD-INITC (I) = EGD-INITC (I) + 1                      
              END-IF                                                            
              IF EGD-INCR (I-R) > ZERO                                          
                 IF EGD-INITC (I) NOT = ZERO                                    
                    COMPUTE EGD-INITC (I) = EGD-INITC (I) - 1                   
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       INITIALISER-COMPTEUR              SECTION.                               
      *-----------------------------------------                                
           MOVE I                     TO I-R.                                   
           IF ZW-TYPCALCUL = SPACES                                             
              MOVE '+'                   TO ZW-TYPCALCUL                        
           END-IF.                                                              
           MOVE 'N'                   TO ZW-TYPCHAMP1.                          
           MOVE 1                     TO ZW-LGCHAMP1.                           
           MOVE ZERO                  TO ZW-DECCHAMP1.                          
           MOVE '0'                   TO ZW-CONTENU1.                           
           MOVE 'N'                   TO ZW-TYPCHAMP2.                          
           MOVE I-L                   TO ZW-LGCHAMP2.                           
           MOVE ZERO                  TO ZW-DECCHAMP2.                          
           MOVE 'F'                   TO ZW-TYPCHAMPR.                          
           MOVE ZERO                  TO ZW-LGCHAMPR.                           
           MOVE ZERO                  TO ZW-DECCHAMPR.                          
           PERFORM APPEL-CALCULS.                                               
      *-----------------------------------------                                
       TRAITEMENT-RAZ-INIT               SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-R FROM 1 BY 1                                      
                   UNTIL I-R > 255                                              
                   OR EGD-TYPCHAMP (I-R) = ' '                                  
              IF EGD-TYPCHAMP (I-R) = ('?' OR '*' OR 'R')                       
                 PERFORM RAZ-CHAMP-CALCULE                                      
              ELSE                                                              
                 IF EGD-TYPCHAMP (I-R) = 'I'                                    
                    PERFORM RAZ-COMPTEUR                                        
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       REMPLISSAGE-DATE-HEURE            SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-P FROM 1 BY 1                                      
                   UNTIL I-P > EG-LARGEUR                                       
              IF EGL-FIXES (I) (I-P:2) = '?J'                                   
                 MOVE W-JJ            TO EGL-FIXES (I) (I-P:2)                  
              END-IF                                                            
              IF EGL-FIXES (I) (I-P:2) = '?M'                                   
                 MOVE W-MM            TO EGL-FIXES (I) (I-P:2)                  
              END-IF                                                            
              IF EGL-FIXES (I) (I-P:2) = '?S'                                   
                 MOVE W-SS            TO EGL-FIXES (I) (I-P:2)                  
              END-IF                                                            
              IF EGL-FIXES (I) (I-P:2) = '?A'                                   
                 MOVE W-AA            TO EGL-FIXES (I) (I-P:2)                  
              END-IF                                                            
              IF EGL-FIXES (I) (I-P:2) = ':H'                                   
                 MOVE EG-HEURE        TO EGL-FIXES (I) (I-P:2)                  
              END-IF                                                            
              IF EGL-FIXES (I) (I-P:2) = ':M'                                   
                 MOVE EG-MINUTE       TO EGL-FIXES (I) (I-P:2)                  
              END-IF                                                            
              IF EGL-FIXES (I) (I-P:3) = '$PP'                                  
                 MOVE I-P             TO EGL-POSPAGE (I)                        
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       TRAITEMENT-DETAIL                 SECTION.                               
      *-----------------------------------------                                
           IF RES-RUPT (1:EG-LGRUPT) NOT = FEXTRAC-LIGNE (1:EG-LGRUPT)          
              PERFORM CALCUL-NIVEAU-RUPTURE                                     
           ELSE                                                                 
              MOVE ZERO                  TO W-NIVRUPT                           
           END-IF.                                                              
           IF W-NIVRUPT NOT = ZERO                                              
              PERFORM TRAITEMENT-TOTAL                                          
      *       MOVE SPACES                TO EGC-CHAMPI                          
              PERFORM VARYING I FROM 1 BY 1                                     
                      UNTIL I > 255                                             
                      OR EGC-TYPLIGNE (I) = ' '                                 
                 IF EGC-RUPTIMPR (I) NOT < W-NIVRUPT                            
                    MOVE '0'                  TO EGC-IMPRIME (I)                
                 END-IF                                                         
              END-PERFORM                                                       
              IF W-NIVRUPT NOT > EG-RUPTCHTPAGE                                 
                 MOVE 'O'                   TO EG-ENTETE                        
              END-IF                                                            
              IF W-NIVRUPT NOT > EG-RUPTRAZPAGE                                 
                 MOVE ZERO                  TO EG-NOPAGE                        
              END-IF                                                            
           END-IF.                                                              
           IF EG-NEXTPAGE                                                       
              PERFORM TRAITEMENT-REPORTER                                       
              PERFORM TRAITEMENT-FINPAGE                                        
           END-IF.                                                              
           PERFORM REMPLISSAGE-DONNEES.                                         
           ADD  1                   TO W-NUMREC.                                
           IF NOT EG-NEWPAGE                                                    
              IF W-NIVRUPT NOT = ZERO                                           
                 IF EG-I-LIGNEE NOT = ZERO                                      
                    PERFORM VARYING I-L FROM EG-I-LIGNEE BY 1                   
                            UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLE              
                       IF EGL-RUPTIMPR (I-L) NOT < W-NIVRUPT                    
                          PERFORM TRAITEMENT-LIGNE-ENTETE                       
                       END-IF                                                   
                    END-PERFORM                                                 
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              IF NOT EG-TETECRE                                                 
                 PERFORM TRAITEMENT-DES-ENTETE                                  
              END-IF                                                            
           END-IF.                                                              
           IF EG-I-LIGNEC NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNEC BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLC                    
                 PERFORM TRAITEMENT-LIGNE-CACHEE                                
              END-PERFORM                                                       
           END-IF.                                                              
           IF EG-I-LIGNED NOT = ZERO                                            
              PERFORM VARYING I-D FROM EG-I-LIGNED BY 1                         
                      UNTIL EGL-TYPLIGNE (I-D) NOT = W-TYPLD                    
                 PERFORM TRAITEMENT-LIGNE-DETAIL                                
              END-PERFORM                                                       
           END-IF.                                                              
           COMPUTE W-NLIGNES = W-NLIGNES + 1.                                   
           PERFORM LECTURE-FEXTRAC.                                             
      *-----------------------------------------                                
       TRAITEMENT-LIGNE-CACHEE           SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITER-LES-CHAMPS.                                          
      *-----------------------------------------                                
       TRAITEMENT-LIGNE-DETAIL           SECTION.                               
      *-----------------------------------------                                
           MOVE I-D                      TO I-L.                                
           PERFORM TRAITER-LES-CHAMPS.                                          
           IF EGL-RUPTIMPR (I-L)     = ZERO                                     
              PERFORM ECRITURE-LIGNE                                            
           ELSE                                                                 
              IF NOT EG-TETECRE                                                 
                 IF W-NIVRUPT          NOT = ZERO AND                           
                    EGL-RUPTIMPR (I-L) NOT < W-NIVRUPT                          
                    PERFORM ECRITURE-LIGNE                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF EG-TETECRE                                                        
              MOVE 'N'                      TO EG-ENTETE                        
           END-IF.                                                              
      *== CETTE FONCTION N'EST UTILISEE QUE PAR CICS POUR                       
      *== CREATION DES TOTAUX VENTILES                                          
           PERFORM TOTAUX-VENTILES.                                             
      *-----------------------------------------                                
       TRAITEMENT-DES-ENTETE             SECTION.                               
      *-----------------------------------------                                
           MOVE I-L                TO I-LRES.                                   
           IF NOT EG-TETECRE                                                    
              PERFORM TRAITEMENT-ENTETE                                         
              PERFORM TRAITEMENT-REPORT                                         
              PERFORM TRAITEMENT-DETAIL-RUPTURE                                 
              MOVE 'C'             TO EG-ENTETE                                 
           END-IF.                                                              
           MOVE I-LRES             TO I-L.                                      
      *-----------------------------------------                                
       TRAITEMENT-ENTETE                 SECTION.                               
      *-----------------------------------------                                
           IF EG-I-LIGNEE NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNEE BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLE                    
                 PERFORM TRAITEMENT-LIGNE-ENTETE                                
              END-PERFORM                                                       
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-LIGNE-ENTETE           SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITER-LES-CHAMPS.                                          
           PERFORM ECRITURE-LIGNE.                                              
      *-----------------------------------------                                
       TRAITEMENT-REPORT                 SECTION.                               
      *-----------------------------------------                                
           IF EG-I-LIGNER NOT = ZERO                                            
              IF W-TYPREPORT NOT = W-TOTREPORT                                  
                 MOVE W-REPORT              TO W-TYPREPORT                      
                 PERFORM VARYING I-L FROM EG-I-LIGNER BY 1                      
                         UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLR                 
                 END-PERFORM                                                    
                 COMPUTE I-L = I-L - 1                                          
                 PERFORM VARYING I-L FROM I-L BY -1                             
                         UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLR                 
                    PERFORM TRAITEMENT-LIGNE-REPORT                             
                 END-PERFORM                                                    
              END-IF                                                            
           END-IF.                                                              
           MOVE SPACES                       TO W-TYPREPORT.                    
      *-----------------------------------------                                
       TRAITEMENT-FINAL                  SECTION.                               
      *-----------------------------------------                                
           MOVE 1                   TO W-NIVRUPT.                               
           MOVE RES-RUPT            TO W-LECTCUMULS.                            
           PERFORM TRAITEMENT-TOTAL.                                            
           MOVE W-TOTREPORT         TO W-TYPREPORT.                             
           PERFORM TRAITEMENT-REPORTER.                                         
           IF EG-I-LIGNEF NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNEF BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLF                    
                 PERFORM TRAITEMENT-LIGNE-FINAL                                 
              END-PERFORM                                                       
           END-IF.                                                              
           PERFORM TRAITEMENT-FINPAGE.                                          
      *-----------------------------------------                                
       TRAITEMENT-LIGNE-FINAL            SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITER-LES-CHAMPS.                                          
           PERFORM ECRITURE-LIGNE.                                              
      *-----------------------------------------                                
       TRAITEMENT-TOTAL                  SECTION.                               
      *-----------------------------------------                                
           IF EG-I-LIGNET NOT = ZERO                                            
              MOVE ZERO                TO W-RUPTIMPR                            
              PERFORM VARYING I-L FROM EG-I-LIGNET BY 1                         
                      UNTIL   EGL-TYPLIGNE (I-L) NOT = W-TYPLT                  
                 IF EGL-RUPTIMPR (I-L) NOT < W-NIVRUPT                          
                    PERFORM TRAITEMENT-LIGNE-TOTAL                              
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
           PERFORM VARYING I-T FROM 1 BY 1                                      
                   UNTIL   I-T > 100                                            
                   OR      EGT-TYPLIGNE (I-T) = ' '                             
              IF EGT-RUPTRAZ (I-T) NOT = ZERO                                   
                 IF EGT-RUPTRAZ (I-T) NOT < W-NIVRUPT                           
                    PERFORM TRAITEMENT-RAZ                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       TRAITEMENT-LIGNE-TOTAL            SECTION.                               
      *-----------------------------------------                                
           IF EGL-RUPTIMPR (I-L) NOT = W-RUPTIMPR                               
              IF EG-NEXTPAGE                                                    
                 IF EGL-SKIPBEFORE (I-L) = 901                                  
                    PERFORM TRAITEMENT-REPORTER-RUPT                            
                    PERFORM TRAITEMENT-FINPAGE                                  
                    PERFORM TRAITEMENT-DES-ENTETE                               
                 ELSE                                                           
                    IF EGL-SKIPBEFORE (I-L)     > ZERO                          
                       IF EGL-I-CHVAR (I-L) NOT = ZERO                          
                          PERFORM TRAITEMENT-REPORTER-RUPT                      
                          PERFORM TRAITEMENT-FINPAGE                            
                          PERFORM TRAITEMENT-DES-ENTETE                         
                       ELSE                                                     
                          IF W-RUPTIMPR  NOT = ZERO                             
                             PERFORM TRAITEMENT-REPORTER-RUPT                   
                             PERFORM TRAITEMENT-FINPAGE                         
                             PERFORM TRAITEMENT-DES-ENTETE                      
                          END-IF                                                
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              ELSE                                                              
                 IF EGL-SKIPBEFORE (I-L) = 901                                  
                    PERFORM TRAITEMENT-FINPAGE                                  
                 END-IF                                                         
              END-IF                                                            
              MOVE EGL-RUPTIMPR (I-L)    TO W-RUPTIMPR                          
           ELSE                                                                 
              IF EGL-SKIPBEFORE (I-L) = 901                                     
                 PERFORM TRAITEMENT-FINPAGE                                     
              END-IF                                                            
           END-IF.                                                              
           MOVE EGL-RUPTIMPR (I-L)  TO I.                                       
           MOVE EGR-I-CHDESC (I)    TO I-V.                                     
           MOVE EGR-LGPHYS (I)      TO L-V.                                     
           IF EGL-I-CHVENT (I-L) NOT = ZERO                                     
              PERFORM VENTILATION-HORIZONTALE                                   
           ELSE                                                                 
               PERFORM TRAITER-LES-CHAMPS                                       
               PERFORM ECRITURE-LIGNE                                           
           END-IF.                                                              
           IF EG-TETECRE                                                        
              MOVE 'N'                           TO EG-ENTETE                   
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-REPORTER               SECTION.                               
      *-----------------------------------------                                
           MOVE I-L                TO I-LRES.                                   
           IF EG-I-LIGNER NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNER BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLR                    
                 IF W-NIVRUPT          = ZERO                                   
                    MOVE SPACES             TO W-TYPREPORT                      
                    PERFORM TRAITEMENT-LIGNE-REPORT                             
                 ELSE                                                           
                    IF EGL-RUPTIMPR (I-L) = ZERO                                
                    OR EGL-RUPTIMPR (I-L) < W-NIVRUPT                           
                       MOVE SPACES             TO W-TYPREPORT                   
                       PERFORM TRAITEMENT-LIGNE-REPORT                          
                    ELSE                                                        
                       PERFORM RECHERCHE-TOTAUX-NIVEAU                          
                    END-IF                                                      
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
           MOVE I-LRES             TO I-L.                                      
      *-----------------------------------------                                
       RECHERCHE-TOTAUX-NIVEAU           SECTION.                               
      *-----------------------------------------                                
           MOVE W-TOTREPORT           TO W-TYPREPORT.                           
           IF EG-I-LIGNET NOT = ZERO                                            
              PERFORM VARYING I FROM EG-I-LIGNET BY 1                           
                      UNTIL   EGL-TYPLIGNE (I) NOT = W-TYPLT                    
                      OR      EGL-RUPTIMPR (I) NOT > EGL-RUPTIMPR (I-L)         
              END-PERFORM                                                       
              IF EGL-TYPLIGNE (I) NOT = W-TYPLT                                 
                 PERFORM TRAITEMENT-LIGNE-REPORT                                
              END-IF                                                            
           ELSE                                                                 
              PERFORM TRAITEMENT-LIGNE-REPORT                                   
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-REPORTER-RUPT          SECTION.                               
      *-----------------------------------------                                
           MOVE I-L                 TO I-LRES.                                  
           MOVE W-TOTREPORT         TO W-TYPREPORT.                             
           IF EG-I-LIGNER NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNER BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLR                    
                 IF W-RUPTIMPR         = ZERO                                   
                    MOVE SPACES           TO W-TYPREPORT                        
                    PERFORM TRAITEMENT-LIGNE-REPORT                             
                 ELSE                                                           
                    IF EGL-RUPTIMPR (I-L) = ZERO                                
                    OR EGL-RUPTIMPR (I-L) < W-RUPTIMPR                          
                       MOVE SPACES           TO W-TYPREPORT                     
                       PERFORM TRAITEMENT-LIGNE-REPORT                          
                    END-IF                                                      
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
           MOVE I-LRES             TO I-L.                                      
      *-----------------------------------------                                
       TRAITEMENT-LIGNE-REPORT           SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITER-LES-CHAMPS.                                          
           IF W-TYPREPORT = SPACES                                              
              MOVE W-AREPORTER                  TO W-TYPREPORT                  
           END-IF.                                                              
           IF EGL-POSREPORT (I-L) > ZERO                                        
              MOVE EGL-POSREPORT (I-L)    TO I                                  
              MOVE W-TYPREPORT            TO WEDITION-LIGNE (I:10)              
           END-IF.                                                              
           PERFORM ECRITURE-LIGNE.                                              
      *-----------------------------------------                                
       TRAITEMENT-FINPAGE                SECTION.                               
      *-----------------------------------------                                
           MOVE I-L                TO I-LRES.                                   
           IF EG-I-LIGNEB NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNEB BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLB                    
                 PERFORM TRAITEMENT-LIGNE-FINPAGE                               
              END-PERFORM                                                       
           END-IF.                                                              
           MOVE I-LRES             TO I-L.                                      
      *-----------------------------------------                                
       TRAITEMENT-LIGNE-FINPAGE          SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITER-LES-CHAMPS.                                          
           PERFORM ECRITURE-LIGNE.                                              
      *-----------------------------------------                                
       TRAITEMENT-DETAIL-RUPTURE         SECTION.                               
      *-----------------------------------------                                
           IF EG-I-LIGNED NOT = ZERO                                            
              PERFORM VARYING I-L FROM EG-I-LIGNED BY 1                         
                      UNTIL EGL-TYPLIGNE (I-L) NOT = W-TYPLD                    
                 IF EGL-RUPTIMPR (I-L) NOT = ZERO                               
                    PERFORM TRAITEMENT-UNE-LIGNE                                
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-UNE-LIGNE              SECTION.                               
      *-----------------------------------------                                
           PERFORM TRAITER-LES-CHAMPS.                                          
           PERFORM ECRITURE-LIGNE.                                              
      *-----------------------------------------                                
       VENTILATION-HORIZONTALE           SECTION.                               
      *-----------------------------------------                                
           MOVE 1                        TO I-V.                                
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL I > 15                                                 
                   OR EGR-NORUPT (I) = ZERO                                     
              MOVE EGR-LGPHYS (I)           TO L-V                              
              IF I > EGL-RUPTIMPR (I-L)                                         
                 MOVE HIGH-VALUE            TO W-LECTCUMULS (I-V:L-V)           
              END-IF                                                            
              ADD  L-V                      TO I-V                              
           END-PERFORM.                                                         
           MOVE EGL-I-CHVENT (I-L)          TO I.                               
           MOVE EGD-LGCHAMP (I)             TO W-LGCHAMP.                       
           MOVE EGD-TYPCHAMP (I)            TO W-TYPCHAMP.                      
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           MOVE W-LGPHYS                    TO L-V.                             
           MOVE SPACES                      TO W-LECTCUMULS (I-V:L-V)           
      *== POUR TRAITEMENT PAR CICS                                              
           PERFORM PREPARE-FCUMULS.                                             
           PERFORM LECTURE-FCUMULS                                              
                   UNTIL FCUMULS-NOMETAT NOT < EG-NOMETAT                       
           IF FCUMULS-NOMETAT = EG-NOMETAT                                      
              PERFORM LECTURE-FCUMULS                                           
                      UNTIL FCUMULS-LIGNE  (1:EG-LGRUPT) NOT <                  
                            W-LECTCUMULS   (1:EG-LGRUPT)                        
                      OR    FCUMULS-NOMETAT > EG-NOMETAT                        
              IF FCUMULS-NOMETAT = EG-NOMETAT AND                               
                 FCUMULS-LIGNE (1:EG-LGRUPT) =                                  
                  W-LECTCUMULS (1:EG-LGRUPT)                                    
                 MOVE FCUMULS-LIGNE (I-V:L-V) TO W-LECTCUMULS (I-V:L-V)         
                 PERFORM RAZ-CHAMPS-VENTILES                                    
                 PERFORM TRAITER-LIGNES-VENTILEES                               
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITER-LIGNES-VENTILEES          SECTION.                               
      *-----------------------------------------                                
           PERFORM UNTIL FCUMULS-LIGNE (1:EG-LGRUPT) >                          
                         W-LECTCUMULS  (1:EG-LGRUPT)                            
                   OR    FCUMULS-NOMETAT > EG-NOMETAT                           
              COMPUTE I = I-V + L-V                                             
              MOVE EGL-I-TOTAL (I-L)   TO I-T                                   
              PERFORM VARYING I-T FROM I-T BY 1                                 
                      UNTIL   I-T > 100                                         
                      OR      EGT-TYPLIGNE (I-T) NOT =                          
                              EGL-TYPLIGNE (I-L)                                
                      OR      EGT-NOLIGNE (I-T)  NOT =                          
                              EGL-NOLIGNE (I-L)                                 
                 MOVE EGT-I-CHAMP (I-T)   TO I-C                                
                 MOVE EGV-I-CHDESC (I-C)  TO I-R                                
                                             I-CL1                              
                                             I-CL2                              
                 MOVE EGD-TYPCHAMP (I-R)   TO ZW-TYPCHAMP1                      
                                              ZW-TYPCHAMP2                      
                                              ZW-TYPCHAMPR                      
                                              W-TYPCHAMP                        
                 MOVE EGD-LGCHAMP (I-R)    TO ZW-LGCHAMP1                       
                                              ZW-LGCHAMP2                       
                                              ZW-LGCHAMPR                       
                                              W-LGCHAMP                         
                 MOVE EGD-DECCHAMP (I-R)   TO ZW-DECCHAMP1                      
                                              ZW-DECCHAMP2                      
                                              ZW-DECCHAMPR                      
                 PERFORM CALCUL-LONGUEUR-PHYS                                   
                 MOVE EGD-CONTENU (I-CL2) (1:W-LGPHYS)                          
                                         TO ZW-CONTENU2                         
                 MOVE FCUMULS-LIGNE (I:W-LGPHYS)                                
                                         TO ZW-CONTENU1                         
                 MOVE '+ '               TO ZW-TYPCALCUL                        
                 MOVE SPACES             TO ZW-CONTENUR                         
                 PERFORM APPEL-CALCULS                                          
                 ADD  W-LGPHYS           TO I                                   
                 MOVE EGT-I-CHDESC (I-T) TO I-M                                 
                 IF EGD-OCCURENCES (I-M) NOT = ZERO                             
                    MOVE EGD-CONTENU (I-CL2) (1:W-LGPHYS)                       
                                            TO ZW-CONTENU2                      
                    MOVE SPACES             TO ZW-CONTENUR                      
                    MOVE '+ '               TO ZW-TYPCALCUL                     
                    MOVE FCUMULS-LIGNE (I:W-LGPHYS)                             
                                            TO ZW-CONTENU1                      
                    PERFORM APPEL-CALCULS                                       
                    ADD  W-LGPHYS           TO I                                
                 END-IF                                                         
              END-PERFORM                                                       
              PERFORM LECTURE-FCUMULS                                           
              IF FCUMULS-LIGNE (I-V:L-V)     NOT =                              
                         W-LECTCUMULS (I-V:L-V)                                 
              OR FCUMULS-LIGNE (1:EG-LGRUPT) NOT =                              
                         W-LECTCUMULS (1:EG-LGRUPT)                             
              OR FCUMULS-NOMETAT             NOT = EG-NOMETAT                   
                 PERFORM ECRIRE-LIGNE-VENTILEE                                  
                 PERFORM RAZ-CHAMPS-VENTILES                                    
                 MOVE FCUMULS-LIGNE (I-V:L-V) TO                                
                      W-LECTCUMULS (I-V:L-V)                                    
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       RAZ-CHAMPS-VENTILES               SECTION.                               
      *-----------------------------------------                                
           IF EGL-I-TOTAL (I-L) > ZERO                                          
              MOVE EGL-I-TOTAL (I-L)   TO I-T                                   
              PERFORM VARYING I-T FROM I-T BY 1                                 
                      UNTIL   EGT-TYPLIGNE (I-T)  NOT =                         
                              EGL-TYPLIGNE (I-L)                                
                      OR      EGT-NOLIGNE (I-T)   NOT =                         
                              EGL-NOLIGNE (I-L)                                 
                      OR      EGT-CONTINUER (I-T) NOT =                         
                              EGL-CONTINUER (I-L)                               
                 MOVE EGT-I-CHAMP (I-T)   TO I-C                                
                 MOVE EGV-I-CHDESC (I-C)  TO I-R                                
                 PERFORM RAZ-CHAMP-CALCULE                                      
              END-PERFORM                                                       
           END-IF.                                                              
      *-----------------------------------------                                
       ECRIRE-LIGNE-VENTILEE             SECTION.                               
      *-----------------------------------------                                
           MOVE EGL-I-CHVENT (I-L)       TO I.                                  
           IF L-V > 18                                                          
              MOVE EGD-I-CONT (I)        TO I-K                                 
              MOVE W-LECTCUMULS (I-V:L-V) TO EGD-CONTENUL (I-K) (1:L-V)         
           ELSE                                                                 
              MOVE W-LECTCUMULS (I-V:L-V) TO EGD-CONTENU (I) (1:L-V)            
           END-IF.                                                              
           PERFORM TRAITER-LES-CHAMPS.                                          
           PERFORM ECRITURE-LIGNE.                                              
      *-----------------------------------------                                
       TRAITER-LES-CHAMPS                SECTION.                               
      *-----------------------------------------                                
           IF EGL-I-CALCUL (I-L)  > ZERO                                        
              PERFORM CALCULER-LES-CHAMPS                                       
           END-IF.                                                              
           MOVE EGL-FIXES (I-L)          TO WEDITION-LIGNE.                     
           IF EGL-I-CHVAR (I-L) NOT = ZERO                                      
              PERFORM VARYING I-C FROM EGL-I-CHVAR (I-L) BY 1                   
                      UNTIL EGC-TYPLIGNE (I-C) NOT =                            
                            EGL-TYPLIGNE (I-L)                                  
                      OR    EGC-NOLIGNE (I-C) NOT =                             
                            EGL-NOLIGNE (I-L)                                   
                      OR    EGC-CONTINUER (I-C) NOT =                           
                            EGL-CONTINUER (I-L)                                 
                 MOVE EGC-POSCHAMP (I-C) TO I-P                                 
                 PERFORM TRAITEMENT-CHAMP                                       
              END-PERFORM                                                       
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-CHAMP                  SECTION.                               
      *-----------------------------------------                                
           IF EGC-CLASSE (I-C) = 'C'                                            
              IF EGL-TYPLIGNE (I-L) NOT = W-TYPLD                               
                 PERFORM TRAITEMENT-CONSTANTS                                   
              ELSE                                                              
                 IF EG-TETECRE                                                  
                 OR W-SEQUENCE = 1                                              
                    PERFORM TRAITEMENT-CONSTANTS                                
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF EGC-CLASSE (I-C) = 'V'                                            
              MOVE EGV-I-CHDESC (I-C)         TO I                              
              IF EGL-TYPLIGNE (I-L) NOT = W-TYPLD                               
                 PERFORM TRAITEMENT-VARIABLES                                   
              ELSE                                                              
                 IF W-NIVRUPT > ZERO                                            
                 OR EG-TETECRE                                                  
                 OR W-SEQUENCE = 1                                              
                 OR EGD-OCCURENCES (I) NOT = ZERO                               
                    PERFORM TRAITEMENT-VARIABLES                                
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-CONSTANTS              SECTION.                               
      *-----------------------------------------                                
           IF EGC-RUPTIMPR (I-C) = ZERO                                         
           OR W-NIVRUPT NOT > EGC-RUPTIMPR (I-C)                                
           OR EG-TETECRE                                                        
              IF EGK-I-CHCOND1 (I-C) = ZERO                                     
                 PERFORM TRAITEMENT-CHAMP-CONSTANT                              
              ELSE                                                              
                 PERFORM COMPARER-DEUX-CHAMPK                                   
                 IF ZWC-RETCODE = '1'                                           
                    PERFORM TRAITEMENT-CHAMP-CONSTANT                           
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-VARIABLES              SECTION.                               
      *-----------------------------------------                                
           IF EGV-I-CHCOND1 (I-C) = ZERO                                        
              PERFORM TRAITEMENT-CHAMP-VARIABLE                                 
           ELSE                                                                 
              PERFORM COMPARER-DEUX-CHAMPV                                      
              IF ZWC-RETCODE = '1'                                              
                 PERFORM TRAITEMENT-CHAMP-VARIABLE                              
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-CHAMP-VARIABLE         SECTION.                               
      *-----------------------------------------                                
           IF EGD-OCCURENCES (I) NOT = ZERO                                     
              IF EGC-CONTINUER (I-C) = '+'                                      
                 COMPUTE I = I + (EGD-OCCURENCES (I) - 1)                       
              END-IF                                                            
           END-IF.                                                              
           IF EGC-RUPTIMPR (I-C) = ZERO                                         
           OR EG-TETECRE                                                        
              PERFORM MOUVEMENTER-CHAMP-VARIABLE                                
           ELSE                                                                 
              IF EGC-IMPRIME (I-C) NOT = '1'                                    
                 PERFORM MOUVEMENTER-CHAMP-VARIABLE                             
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       MOUVEMENTER-CHAMP-VARIABLE        SECTION.                               
      *-----------------------------------------                                
           IF EGL-TYPLIGNE (I-L)   NOT = W-TYPLC AND                            
              EGL-SKIPBEFORE (I-L) NOT = -1                                     
              IF EGD-TYPCHAMP (I) = ('I' OR 'R')                                
                 MOVE 'F'                    TO W-TYPCHAMP                      
              ELSE                                                              
                 MOVE EGD-TYPCHAMP (I)       TO W-TYPCHAMP                      
              END-IF                                                            
              MOVE EGD-LGCHAMP (I)        TO W-LGCHAMP                          
              MOVE EGD-DECCHAMP (I)       TO W-DECCHAMP                         
              PERFORM CALCUL-LONGUEUR-PHYS                                      
              IF EGV-I-MCHAMP (I-C) = ZERO                                      
                 MOVE W-LGPHYS                       TO I-M                     
                 IF EGV-I-LCHAMP (I-C) < W-LGPHYS                               
                    MOVE EGV-I-LCHAMP (I-C)          TO I-M                     
                 END-IF                                                         
                 IF W-LGPHYS > 18                                               
                    MOVE EGD-I-CONT (I)              TO I-K                     
                    MOVE EGD-CONTENUL (I-K) (1:I-M)  TO                         
                         WEDITION-LIGNE (I-P:I-M)                               
                 ELSE                                                           
                    MOVE EGD-CONTENU (I) (1:I-M)     TO                         
                         WEDITION-LIGNE (I-P:I-M)                               
                 END-IF                                                         
              ELSE                                                              
                 MOVE EGV-I-MCHAMP (I-C)            TO I-LM                     
                 MOVE EGV-MASKCHAMP-L (I-LM)        TO I-M                      
                 IF EGV-MASKCHAMP-V (I-LM) (1:I-M) NOT = SPACES                 
                    IF EGV-MASKCHAMP-V (I-LM) (1:1) = '('                       
                       PERFORM TRAITEMENT-MASQUE-ALPHA                          
                    ELSE                                                        
                       PERFORM TRAITEMENT-MASQUE                                
                    END-IF                                                      
                 ELSE                                                           
                    IF W-LGPHYS > 18                                            
                       MOVE EGD-I-CONT (I)              TO I-K                  
                       MOVE EGD-CONTENUL (I-K) (1:I-M)  TO                      
                            WEDITION-LIGNE (I-P:I-M)                            
                    ELSE                                                        
                       MOVE EGD-CONTENU (I) (1:I-M) TO                          
                            WEDITION-LIGNE (I-P:I-M)                            
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF EGV-I-CHTOT (I-C) > ZERO                                          
              PERFORM VARYING I-T FROM 1 BY 1                                   
                      UNTIL   I-T > 100                                         
                      OR      EGT-TYPLIGNE (I-T) = ' '                          
                 IF EGT-I-CHTOT (I-T)  = I-C                                    
                    PERFORM TOTALISER-CHAMP                                     
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
           MOVE '1'                        TO EGC-IMPRIME (I-C).                
      *-----------------------------------------                                
       TRAITEMENT-CHAMP-CONSTANT         SECTION.                               
      *-----------------------------------------                                
           IF EGK-I-CHAMPC (I-C) = ZERO                                         
              PERFORM MOUVEMENTER-CHAMP-CONSTANT                                
           ELSE                                                                 
              IF EGK-SOUSTABLE (I-C) = 'SPROG'                                  
                 PERFORM APPEL-SOUS-PROGRAMME                                   
              ELSE                                                              
                 PERFORM RECHERCHE-LIBELLE-CONSTANT                             
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       MOUVEMENTER-CHAMP-CONSTANT        SECTION.                               
      *-----------------------------------------                                
           MOVE EGK-I-LCHAMP (I-C)         TO I-LK.                             
           MOVE EGK-LIBCHAMP-L (I-LK)      TO I.                                
           IF EG-TETECRE OR EGC-RUPTIMPR (I-C) = ZERO                           
              MOVE EGK-LIBCHAMP-V (I-LK) (1:I) TO                               
                                   WEDITION-LIGNE (I-P:I)                       
              MOVE '1'                        TO EGC-IMPRIME (I-C)              
           ELSE                                                                 
              IF EGC-IMPRIME (I-C) NOT = '1'                                    
                 MOVE EGK-LIBCHAMP-V (I-LK) (1:I) TO                            
                                      WEDITION-LIGNE (I-P:I)                    
                 MOVE '1'                        TO EGC-IMPRIME (I-C)           
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       RECHERCHE-LIBELLE-CONSTANT        SECTION.                               
      *-----------------------------------------                                
           MOVE SPACES                        TO GA01-CTABLEG2.                 
           MOVE EGK-I-CHAMPC (I-C)            TO I.                             
           MOVE EGD-TYPCHAMP (I)              TO W-TYPCHAMP.                    
           MOVE EGD-LGCHAMP (I)               TO W-LGCHAMP.                     
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I)                   TO I-K                      
              MOVE EGD-CONTENUL (I-K) (1:W-LGPHYS)  TO GA01-CTABLEG2            
           ELSE                                                                 
              MOVE EGD-CONTENU (I) (1:W-LGPHYS)     TO GA01-CTABLEG2            
           END-IF.                                                              
           MOVE EGK-SOUSTABLE (I-C)           TO GA01-CTABLEG1                  
                                                 GA71-CSTABLE.                  
           PERFORM LECTURE-RTGA71.                                              
           MOVE 1                       TO I-M.                                 
           IF GA71-CHAMP1 NOT = EGK-NOMCHAMPS (I-C)                             
              IF GA71-WPOSCLE1 = ZERO                                           
                 ADD  GA71-QCHAMP1            TO I-M                            
              END-IF                                                            
              IF GA71-CHAMP2 NOT = EGK-NOMCHAMPS (I-C)                          
                 IF GA71-WPOSCLE2 = ZERO                                        
                    ADD  GA71-QCHAMP2            TO I-M                         
                 END-IF                                                         
                 IF GA71-CHAMP3 NOT = EGK-NOMCHAMPS (I-C)                       
                    IF GA71-WPOSCLE3 = ZERO                                     
                       ADD  GA71-QCHAMP3            TO I-M                      
                    END-IF                                                      
                    IF GA71-CHAMP4 NOT = EGK-NOMCHAMPS (I-C)                    
                       IF GA71-WPOSCLE4 = ZERO                                  
                          ADD  GA71-QCHAMP4            TO I-M                   
                       END-IF                                                   
                       IF GA71-CHAMP5 NOT = EGK-NOMCHAMPS (I-C)                 
                          IF GA71-WPOSCLE5 = ZERO                               
                             ADD  GA71-QCHAMP5            TO I-M                
                          END-IF                                                
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           MOVE EGK-I-LCHAMP (I-C)         TO I-LK.                             
           MOVE EGK-LIBCHAMP-L (I-LK)      TO I.                                
           PERFORM LECTURE-RTGA01.                                              
           IF SQL-TROUVE                                                        
              IF EG-TETECRE OR EGC-RUPTIMPR (I-C) = ZERO                        
                 MOVE GA01-WTABLEG (I-M:I) TO                                   
                                   WEDITION-LIGNE (I-P:I)                       
                 MOVE '1'                  TO EGC-IMPRIME (I-C)                 
              ELSE                                                              
                 IF EGC-IMPRIME (I-C) NOT = '1'                                 
                    MOVE GA01-WTABLEG (I-M:I) TO                                
                                      WEDITION-LIGNE (I-P:I)                    
                    MOVE '1'                  TO EGC-IMPRIME (I-C)              
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              MOVE SPACES           TO WEDITION-LIGNE (I-P:I)                   
              MOVE '1'              TO EGC-IMPRIME (I-C)                        
           END-IF.                                                              
      *-----------------------------------------                                
       APPEL-SOUS-PROGRAMME              SECTION.                               
      *-----------------------------------------                                
           MOVE SPACES                        TO Z-SOUSPROG.                    
           MOVE EGK-I-CHAMPC (I-C)            TO I.                             
           MOVE EGD-TYPCHAMP (I)              TO W-TYPCHAMP.                    
           MOVE EGD-LGCHAMP (I)               TO W-LGCHAMP.                     
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I)                   TO I-K                      
              MOVE EGD-CONTENUL (I-K) (1:W-LGPHYS)  TO                          
                   Z-SOUSPROG (1:W-LGPHYS)                                      
           ELSE                                                                 
              MOVE EGD-CONTENU (I) (1:W-LGPHYS)     TO                          
                   Z-SOUSPROG (1:W-LGPHYS)                                      
           END-IF.                                                              
           MOVE EGK-NOMCHAMPS (I-C)           TO W-NOMCALL.                     
           CALL W-NOMCALL USING Z-SOUSPROG.                                     
           COMPUTE I-M = W-LGPHYS + 1.                                          
           MOVE EGK-I-LCHAMP (I-C)         TO I-LK.                             
           MOVE EGK-LIBCHAMP-L (I-LK)         TO I.                             
           IF EG-TETECRE OR EGC-RUPTIMPR (I-C) = ZERO                           
              MOVE Z-SOUSPROG (I-M:I)    TO                                     
                              WEDITION-LIGNE (I-P:I)                            
              MOVE '1'                   TO EGC-IMPRIME (I-C)                   
           ELSE                                                                 
              IF EGC-IMPRIME (I-C) NOT = '1'                                    
                 MOVE Z-SOUSPROG (I-M:I)    TO                                  
                                 WEDITION-LIGNE (I-P:I)                         
                 MOVE '1'                   TO EGC-IMPRIME (I-C)                
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-MASQUE-ALPHA           SECTION.                               
      *-----------------------------------------                                
           MOVE ZERO                  TO W-DEB.                                 
           MOVE ZERO                  TO W-LGN.                                 
           PERFORM VARYING I-PZ FROM I-M BY -1                                  
                   UNTIL EGV-MASKCHAMP-V (I-LM) (I-PZ:1) NOT = ')'              
           END-PERFORM.                                                         
           MOVE 3                     TO I-M.                                   
           PERFORM VARYING I-PZ FROM I-PZ BY -1                                 
                   UNTIL EGV-MASKCHAMP-V (I-LM) (I-PZ:1) = ':'                  
              MOVE EGV-MASKCHAMP-V (I-LM) (I-PZ:1) TO W-LGNA (I-M:1)            
              SUBTRACT 1              FROM I-M                                  
           END-PERFORM.                                                         
           PERFORM VARYING I-PZ FROM I-PZ BY -1                                 
                   UNTIL EGV-MASKCHAMP-V (I-LM) (I-PZ:1) NOT = ':'              
           END-PERFORM.                                                         
           MOVE 3                     TO I-M.                                   
           PERFORM VARYING I-PZ FROM I-PZ BY -1                                 
                   UNTIL EGV-MASKCHAMP-V (I-LM) (I-PZ:1) = '('                  
              MOVE EGV-MASKCHAMP-V (I-LM) (I-PZ:1) TO W-DEBA (I-M:1)            
              SUBTRACT 1              FROM I-M                                  
           END-PERFORM.                                                         
           MOVE W-LGN                      TO I-M.                              
           MOVE W-DEB                      TO I-PZ.                             
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I)                TO I-K                         
              MOVE EGD-CONTENUL (I-K) (I-PZ:I-M) TO                             
                   WEDITION-LIGNE (I-P:I-M)                                     
           ELSE                                                                 
              MOVE EGD-CONTENU (I) (I-PZ:I-M) TO                                
                   WEDITION-LIGNE (I-P:I-M)                                     
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-MASQUE                 SECTION.                               
      *-----------------------------------------                                
           MOVE SPACES                         TO ZWM-MASQUE.                   
           IF EGD-TYPCHAMP (I) = ('I' OR 'R')                                   
              MOVE 'F'                            TO W-TYPCHAMP                 
                                                     ZWM-TYPCHAMP               
           ELSE                                                                 
              MOVE EGD-TYPCHAMP (I)               TO W-TYPCHAMP                 
                                                     ZWM-TYPCHAMP               
           END-IF.                                                              
           MOVE EGD-LGCHAMP (I)                TO W-LGCHAMP                     
                                                  ZWM-LGCHAMP.                  
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           MOVE EGD-DECCHAMP (I)               TO ZWM-DECCHAMP.                 
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I)              TO I-K                           
              MOVE EGD-CONTENUL (I-K) (1:W-LGPHYS)   TO ZWM-CONTENU             
           ELSE                                                                 
              MOVE EGD-CONTENU (I) (1:W-LGPHYS)      TO ZWM-CONTENU             
           END-IF.                                                              
           MOVE EGV-MASKCHAMP-L (I-LM)         TO ZWM-LGMASQUE.                 
           MOVE EGV-MASKCHAMP-V (I-LM) (1:I-M) TO ZWM-MASQUE (1:I-M).           
           IF ZWM-MASQUE (1:1) = '�'                                            
           OR ZWM-MASQUE (2:1) = '�'                                            
              MOVE EG-I-DECIM                  TO I                             
              MOVE EGD-CONTENU (I) (1:1)       TO W-DECIMA                      
              MOVE W-DECIM                     TO ZWM-DECEDIT                   
           ELSE                                                                 
              MOVE ZWM-DECCHAMP                TO ZWM-DECEDIT                   
           END-IF.                                                              
           PERFORM APPEL-MASQUE.                                                
           IF PT9901-CODPIC = '02'                                              
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > ZWM-LGMASQUE              
                 IF ZWM-MASQUE(I:1) = '.'                                       
                    MOVE ',' TO ZWM-MASQUE(I:1)                                 
                 ELSE                                                           
                    IF ZWM-MASQUE(I:1) = ','                                    
                       MOVE '.' TO ZWM-MASQUE(I:1)                              
                    END-IF                                                      
               END-IF                                                           
           END-PERFORM.                                                         
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL I > EGV-MASKCHAMP-L (I-LM)                             
              IF EGV-MASKCHAMP-V (I-LM) (I:1) = '<'                             
                 COMPUTE I-M = I-M - 1                                          
              END-IF                                                            
              IF EGV-MASKCHAMP-V (I-LM) (I:2) = '/R'                            
                 COMPUTE I-M = I-M - 2                                          
              END-IF                                                            
           END-PERFORM.                                                         
           MOVE ZWM-MASQUE (1:I-M)       TO WEDITION-LIGNE (I-P:I-M).           
      *-----------------------------------------                                
       COMPARER-DEUX-CHAMPV              SECTION.                               
      *-----------------------------------------                                
           MOVE EGV-CONDITION (I-C)           TO ZWC-COMPARAISON.               
           MOVE EGV-I-CHCOND1 (I-C)           TO I-CL1.                         
           MOVE EGD-TYPCHAMP (I-CL1)          TO ZWC-TYPCHAMP1.                 
           MOVE EGD-LGCHAMP (I-CL1)           TO ZWC-LGCHAMP1.                  
           MOVE EGD-DECCHAMP (I-CL1)          TO ZWC-DECCHAMP1.                 
           MOVE EGD-TYPCHAMP (I-CL1)          TO W-TYPCHAMP.                    
           MOVE EGD-LGCHAMP (I-CL1)           TO W-LGCHAMP.                     
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I-CL1)         TO I-K                            
              MOVE EGD-CONTENUL (I-K)         TO ZWC-CONTENU1                   
           ELSE                                                                 
              MOVE EGD-CONTENU (I-CL1)        TO ZWC-CONTENU1                   
           END-IF.                                                              
           MOVE EGV-I-CHCOND2 (I-C)           TO I-CL2.                         
           MOVE EGD-TYPCHAMP (I-CL2)          TO ZWC-TYPCHAMP2.                 
           MOVE EGD-LGCHAMP (I-CL2)           TO ZWC-LGCHAMP2.                  
           MOVE EGD-DECCHAMP (I-CL2)          TO ZWC-DECCHAMP2.                 
           MOVE EGD-TYPCHAMP (I-CL2)          TO W-TYPCHAMP.                    
           MOVE EGD-LGCHAMP (I-CL2)           TO W-LGCHAMP.                     
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I-CL2)         TO I-K                            
              MOVE EGD-CONTENUL (I-K)         TO ZWC-CONTENU2                   
           ELSE                                                                 
              MOVE EGD-CONTENU (I-CL2)        TO ZWC-CONTENU2                   
           END-IF.                                                              
           PERFORM APPEL-COMPARAISON.                                           
      *-----------------------------------------                                
       COMPARER-DEUX-CHAMPK              SECTION.                               
      *-----------------------------------------                                
           MOVE EGK-CONDITION (I-C)      TO ZWC-COMPARAISON.                    
           MOVE EGK-I-CHCOND1 (I-C)      TO I-CL1.                              
           MOVE EGD-TYPCHAMP (I-CL1)     TO ZWC-TYPCHAMP1.                      
           MOVE EGD-LGCHAMP (I-CL1)      TO ZWC-LGCHAMP1.                       
           MOVE EGD-DECCHAMP (I-CL1)     TO ZWC-DECCHAMP1.                      
           MOVE EGD-TYPCHAMP (I-CL1)          TO W-TYPCHAMP.                    
           MOVE EGD-LGCHAMP (I-CL1)           TO W-LGCHAMP.                     
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I-CL1)         TO I-K                            
              MOVE EGD-CONTENUL (I-K)         TO ZWC-CONTENU1                   
           ELSE                                                                 
              MOVE EGD-CONTENU (I-CL1)        TO ZWC-CONTENU1                   
           END-IF.                                                              
           MOVE EGK-I-CHCOND2 (I-C)      TO I-CL2.                              
           MOVE EGD-TYPCHAMP (I-CL2)     TO ZWC-TYPCHAMP2.                      
           MOVE EGD-LGCHAMP (I-CL2)      TO ZWC-LGCHAMP2.                       
           MOVE EGD-DECCHAMP (I-CL2)     TO ZWC-DECCHAMP2.                      
           MOVE EGD-TYPCHAMP (I-CL2)          TO W-TYPCHAMP.                    
           MOVE EGD-LGCHAMP (I-CL2)           TO W-LGCHAMP.                     
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I-CL2)         TO I-K                            
              MOVE EGD-CONTENUL (I-K)         TO ZWC-CONTENU2                   
           ELSE                                                                 
              MOVE EGD-CONTENU (I-CL2)        TO ZWC-CONTENU2                   
           END-IF.                                                              
           PERFORM APPEL-COMPARAISON.                                           
      *-----------------------------------------                                
       TOTALISER-CHAMP                   SECTION.                               
      *-----------------------------------------                                
           IF W-SEQUENCE = 1                                                    
           OR EGD-OCCURENCES (I) NOT = ZERO                                     
              IF EGC-RUPTIMPR (I-C) = ZERO                                      
                 PERFORM TOTALISER-UN-CHAMP                                     
              ELSE                                                              
                 IF W-NIVRUPT NOT > EGC-RUPTIMPR (I-C)                          
                    PERFORM TOTALISER-UN-CHAMP                                  
                 ELSE                                                           
                    MOVE SPACES       TO WEDITION-LIGNE (I-P:I-M)               
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TOTALISER-UN-CHAMP                SECTION.                               
      *-----------------------------------------                                
           MOVE '+ '                     TO ZW-TYPCALCUL.                       
           MOVE EGV-I-CHDESC (I-C)       TO I-CL1                               
           IF EGD-OCCURENCES (I-CL1) NOT = ZERO                                 
              IF EGC-CONTINUER (I-C) = '+'                                      
                 COMPUTE I-CL1 = I-CL1 + (EGD-OCCURENCES (I-CL1) - 1)           
              END-IF                                                            
           END-IF.                                                              
           MOVE EGD-TYPCHAMP (I-CL1)     TO ZW-TYPCHAMP1.                       
           MOVE EGD-LGCHAMP (I-CL1)      TO ZW-LGCHAMP1.                        
           MOVE EGD-DECCHAMP (I-CL1)     TO ZW-DECCHAMP1.                       
           MOVE EGD-CONTENU (I-CL1)      TO ZW-CONTENU1.                        
           MOVE EGT-I-CHAMP (I-T)        TO I.                                  
           MOVE EGV-I-CHDESC (I)         TO I-CL2.                              
           IF EGD-OCCURENCES (I-CL2) NOT = ZERO                                 
              IF EGC-CONTINUER (I-C) = '+'                                      
                 COMPUTE I-CL2 = I-CL2 + (EGD-OCCURENCES (I-CL2) - 1)           
              END-IF                                                            
           END-IF.                                                              
           MOVE EGD-TYPCHAMP (I-CL2)     TO ZW-TYPCHAMP2.                       
           MOVE EGD-LGCHAMP (I-CL2)      TO ZW-LGCHAMP2.                        
           MOVE EGD-DECCHAMP (I-CL2)     TO ZW-DECCHAMP2.                       
           MOVE EGD-CONTENU (I-CL2)      TO ZW-CONTENU2.                        
           MOVE I-CL2                    TO I-R.                                
           MOVE EGD-TYPCHAMP (I-R)       TO ZW-TYPCHAMPR.                       
           MOVE EGD-LGCHAMP (I-R)        TO ZW-LGCHAMPR.                        
           MOVE EGD-DECCHAMP (I-R)       TO ZW-DECCHAMPR.                       
           MOVE SPACES                   TO ZW-CONTENUR.                        
           PERFORM APPEL-CALCULS.                                               
      *-----------------------------------------                                
       CALCULER-LES-CHAMPS               SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-C FROM EGL-I-CALCUL (I-L) BY 1                     
                   UNTIL   I-C > 255                                            
                   OR      EGC-TYPLIGNE (I-C)  NOT =                            
                           EGL-TYPLIGNE (I-L)                                   
                   OR      EGC-NOLIGNE (I-C)   NOT =                            
                           EGL-NOLIGNE (I-L)                                    
                   OR      EGC-CONTINUER (I-C) NOT =                            
                           EGL-CONTINUER (I-L)                                  
              IF EGC-CLASSE (I-C) = 'V'                                         
                 MOVE EGV-I-CHDESC (I-C)    TO I-R                              
                 IF EGD-TYPCHAMP (I-R) = '?'                                    
                 OR EGD-TYPCHAMP (I-R) = '*'                                    
                    IF EGD-NOCALCUL (I-R) NOT = ZERO                            
                       PERFORM CALCULER-CHAMP                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       CALCULER-CHAMP                    SECTION.                               
      *-----------------------------------------                                
           IF EGC-RUPTIMPR (I-C) = ZERO                                         
           OR W-NLIGNES          = ZERO                                         
              PERFORM SELECTION-CALCUL-CHAMP                                    
           ELSE                                                                 
              IF W-NIVRUPT          = ZERO                                      
                 PERFORM RAZ-CHAMP-CALCULE                                      
              ELSE                                                              
                 IF W-NIVRUPT NOT > EGC-RUPTIMPR (I-C)                          
                    PERFORM SELECTION-CALCUL-CHAMP                              
                 ELSE                                                           
                    PERFORM RAZ-CHAMP-CALCULE                                   
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       SELECTION-CALCUL-CHAMP            SECTION.                               
      *-----------------------------------------                                
           IF EGV-I-CHCOND1 (I-C) = ZERO                                        
              PERFORM TRAITEMENT-CALCUL-CHAMP                                   
           ELSE                                                                 
              PERFORM COMPARER-DEUX-CHAMPV                                      
              IF ZWC-RETCODE = '1'                                              
                 PERFORM TRAITEMENT-CALCUL-CHAMP                                
              ELSE                                                              
                 PERFORM RAZ-CHAMP-CALCULE                                      
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TRAITEMENT-CALCUL-CHAMP           SECTION.                               
      *-----------------------------------------                                
           INITIALIZE W-RESULTATS.                                              
           MOVE EGV-I-CHDESC (I-C) TO I-R.                                      
           IF EGD-TYPCHAMP (I-R) = 'I'                                          
              PERFORM INCREMENTER-COMPTEUR                                      
           ELSE                                                                 
              IF EGD-TYPCHAMP (I-R) = 'R'                                       
                 MOVE W-NUMREC-X         TO EGD-CONTENU (I-R) (1:4)             
              ELSE                                                              
                 MOVE ZERO                     TO I-Q                           
                 MOVE EGD-NOCALCUL (I-R)       TO I-P                           
                 PERFORM VARYING I-P FROM I-P BY 1                              
                         UNTIL   I-P > 150                                      
                         OR      EGQ-NOCALCUL (I-P) NOT =                       
                                 EGD-NOCALCUL (I-R)                             
                       PERFORM CALCULER-UN-CHAMP                                
                 END-PERFORM                                                    
                 PERFORM FIN-CALCULER-UN-CHAMP                                  
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       INCREMENTER-COMPTEUR              SECTION.                               
      *-----------------------------------------                                
           IF EGD-INCR (I-R) < ZERO                                             
              MOVE '> '                     TO ZWC-COMPARAISON                  
              MOVE 'F'                      TO ZWC-TYPCHAMP1                    
              MOVE ZERO                     TO ZWC-LGCHAMP1                     
              MOVE ZERO                     TO ZWC-DECCHAMP1                    
              MOVE EGD-CONTENU (I-R) (1:4)  TO ZWC-CONTENU1                     
              MOVE 'N'                      TO ZWC-TYPCHAMP2                    
              MOVE 1                        TO ZWC-LGCHAMP2                     
              MOVE ZERO                     TO ZWC-DECCHAMP2                    
              MOVE '0'                      TO ZWC-CONTENU2                     
              PERFORM APPEL-COMPARAISON                                         
              IF ZWC-RETCODE = '1'                                              
                 PERFORM ADDITIONNER-DANS-COMPTEUR                              
              END-IF                                                            
           ELSE                                                                 
              PERFORM ADDITIONNER-DANS-COMPTEUR                                 
           END-IF.                                                              
      *-----------------------------------------                                
       ADDITIONNER-DANS-COMPTEUR         SECTION.                               
      *-----------------------------------------                                
           MOVE '+ '                     TO ZW-TYPCALCUL.                       
           MOVE 'F'                      TO ZW-TYPCHAMP1.                       
           MOVE EGD-CONTENU (I-R) (1:4)  TO ZW-CONTENU1.                        
           MOVE ZERO                     TO ZW-LGCHAMP1.                        
           MOVE ZERO                     TO ZW-DECCHAMP1.                       
           MOVE 'F'                      TO ZW-TYPCHAMP2.                       
           MOVE EGD-INCREMENT (I-R)      TO ZW-CONTENU2.                        
           MOVE ZERO                     TO ZW-LGCHAMP2.                        
           MOVE ZERO                     TO ZW-DECCHAMP2.                       
           MOVE 'F'                      TO ZW-TYPCHAMPR.                       
           MOVE SPACES                   TO ZW-CONTENUR.                        
           MOVE ZERO                     TO ZW-LGCHAMPR.                        
           MOVE ZERO                     TO ZW-DECCHAMPR.                       
           PERFORM APPEL-CALCULS.                                               
      *-----------------------------------------                                
       CALCULER-UN-CHAMP                 SECTION.                               
      *-----------------------------------------                                
           MOVE EGQ-TYPCALCUL (I-P)      TO ZW-TYPCALCUL.                       
           IF EGQ-I-CHCALC1 (I-P) > ZERO                                        
              MOVE EGQ-I-CHCALC1 (I-P)      TO I-CL1                            
              MOVE EGD-TYPCHAMP (I-CL1)     TO ZW-TYPCHAMP1                     
              MOVE EGD-LGCHAMP (I-CL1)      TO ZW-LGCHAMP1                      
              MOVE EGD-DECCHAMP (I-CL1)     TO ZW-DECCHAMP1                     
              MOVE EGD-CONTENU (I-CL1)      TO ZW-CONTENU1                      
           ELSE                                                                 
              COMPUTE I-K = EGQ-I-CHCALC1 (I-P) * -1                            
              MOVE 'P'                      TO ZW-TYPCHAMP1                     
              MOVE 15                       TO ZW-LGCHAMP1                      
              PERFORM VARYING I-CL1 FROM 1 BY 1                                 
                      UNTIL I-CL1 > 15                                          
                      OR W-LIGNECALC (I-CL1) = I-K                              
              END-PERFORM                                                       
              MOVE W-NBDEC (I-CL1)          TO ZW-DECCHAMP1                     
              MOVE W-RESULTAT (I-CL1)       TO ZW-CONTENU1                      
           END-IF.                                                              
           IF EGQ-I-CHCALC2 (I-P) > ZERO                                        
              MOVE EGQ-I-CHCALC2 (I-P)      TO I-CL2                            
              MOVE EGD-TYPCHAMP (I-CL2)     TO ZW-TYPCHAMP2                     
              MOVE EGD-LGCHAMP (I-CL2)      TO ZW-LGCHAMP2                      
              MOVE EGD-DECCHAMP (I-CL2)     TO ZW-DECCHAMP2                     
              MOVE EGD-CONTENU (I-CL2)      TO ZW-CONTENU2                      
           ELSE                                                                 
              COMPUTE I-K = EGQ-I-CHCALC2 (I-P) * -1                            
              MOVE 'P'                      TO ZW-TYPCHAMP2                     
              MOVE 15                       TO ZW-LGCHAMP2                      
              PERFORM VARYING I-CL2 FROM 1 BY 1                                 
                      UNTIL I-CL2 > 15                                          
                      OR W-LIGNECALC (I-CL2) = I-K                              
              END-PERFORM                                                       
              MOVE W-NBDEC (I-CL2)          TO ZW-DECCHAMP2                     
              MOVE W-RESULTAT (I-CL2)       TO ZW-CONTENU2                      
           END-IF.                                                              
           ADD  1                        TO I-Q                                 
           MOVE 'P'                      TO ZW-TYPCHAMPR.                       
           MOVE 15                       TO ZW-LGCHAMPR.                        
           IF ZW-DECCHAMP1 > ZW-DECCHAMP2                                       
              MOVE ZW-DECCHAMP1             TO ZW-DECCHAMPR                     
           ELSE                                                                 
              MOVE ZW-DECCHAMP2             TO ZW-DECCHAMPR                     
           END-IF.                                                              
           IF EGD-DECCHAMP (I-R) > ZW-DECCHAMPR                                 
              MOVE EGD-DECCHAMP (I-R)       TO ZW-DECCHAMPR                     
           END-IF.                                                              
           IF ZW-TYPCALCUL (2:1) NOT = 'R'                                      
              EVALUATE ZW-TYPCALCUL (1:1)                                       
                 WHEN '*'                                                       
                    COMPUTE ZW-DECCHAMPR = ZW-DECCHAMP1 + ZW-DECCHAMP2          
                 WHEN '&'                                                       
                    COMPUTE ZW-DECCHAMPR = ZW-DECCHAMP1 + ZW-DECCHAMP2          
                                         + 2                                    
                 WHEN '/'                                                       
                    ADD  1                  TO ZW-DECCHAMPR                     
                 WHEN '%'                                                       
                    ADD  3                  TO ZW-DECCHAMPR                     
              END-EVALUATE                                                      
           ELSE                                                                 
              EVALUATE ZW-TYPCALCUL (1:1)                                       
                 WHEN '&'                                                       
                    ADD  2                  TO ZW-DECCHAMPR                     
                 WHEN '%'                                                       
                    ADD  2                  TO ZW-DECCHAMPR                     
              END-EVALUATE                                                      
           END-IF.                                                              
           MOVE SPACES                   TO ZW-CONTENUR.                        
           PERFORM APPEL-CALCULS.                                               
           MOVE ZW-CONTENUR (1:8)        TO W-RESULTAT (I-Q).                   
           MOVE ZW-DECCHAMPR             TO W-NBDEC (I-Q).                      
           MOVE ZW-TYPCALCUL             TO W-TYPCALC (I-Q).                    
           MOVE EGQ-LIGNECALC (I-P)      TO W-LIGNECALC (I-Q).                  
      *-----------------------------------------                                
       FIN-CALCULER-UN-CHAMP             SECTION.                               
      *-----------------------------------------                                
           IF W-TYPCALC (I-Q) (2:1) = 'R'                                       
              MOVE '*R'                     TO ZW-TYPCALCUL                     
           ELSE                                                                 
              MOVE '* '                     TO ZW-TYPCALCUL                     
           END-IF.                                                              
           MOVE 'P'                      TO ZW-TYPCHAMP1                        
           MOVE 15                       TO ZW-LGCHAMP1                         
           MOVE W-NBDEC (I-Q)            TO ZW-DECCHAMP1                        
           MOVE W-RESULTAT (I-Q)         TO ZW-CONTENU1 (1:8)                   
           MOVE 'N'                      TO ZW-TYPCHAMP2                        
           MOVE 1                        TO ZW-LGCHAMP2                         
           MOVE 0                        TO ZW-DECCHAMP2                        
           MOVE '1'                      TO ZW-CONTENU2                         
           MOVE EGD-TYPCHAMP (I-R)       TO ZW-TYPCHAMPR.                       
           MOVE EGD-LGCHAMP (I-R)        TO ZW-LGCHAMPR.                        
           MOVE EGD-DECCHAMP (I-R)       TO ZW-DECCHAMPR.                       
           MOVE SPACES                   TO ZW-CONTENUR.                        
           PERFORM APPEL-CALCULS.                                               
      *-----------------------------------------                                
       ECRITURE-LIGNE                    SECTION.                               
      *-----------------------------------------                                
           IF EGL-SKIPBEFORE (I-L) NOT NEGATIVE                                 
              IF EGL-I-CHTIMP (I-L) = ZERO                                      
                 PERFORM ECRITURE-UNE-LIGNE                                     
              ELSE                                                              
                 PERFORM TEST-ECRITURE-LIGNE                                    
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       TEST-ECRITURE-LIGNE               SECTION.                               
      *-----------------------------------------                                
           MOVE EGL-I-CHTIMP (I-L)    TO I.                                     
           MOVE '= '                          TO ZWC-COMPARAISON.               
           MOVE EGD-TYPCHAMP (I)              TO ZWC-TYPCHAMP1.                 
           MOVE EGD-LGCHAMP (I)               TO ZWC-LGCHAMP1.                  
           MOVE EGD-DECCHAMP (I)              TO ZWC-DECCHAMP1.                 
           MOVE EGD-TYPCHAMP (I)              TO W-TYPCHAMP.                    
           MOVE EGD-LGCHAMP (I)               TO W-LGCHAMP.                     
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           IF W-LGPHYS > 18                                                     
              MOVE EGD-I-CONT (I)             TO I-K                            
              MOVE EGD-CONTENUL (I-K)         TO ZWC-CONTENU1                   
           ELSE                                                                 
              MOVE EGD-CONTENU (I)            TO ZWC-CONTENU1                   
           END-IF.                                                              
           MOVE 'K'                           TO ZWC-TYPCHAMP2.                 
           MOVE 1                             TO ZWC-LGCHAMP2.                  
           MOVE ZERO                          TO ZWC-DECCHAMP2.                 
           IF EGD-TYPCHAMP (I) = 'N'                                            
           OR EGD-TYPCHAMP (I) = 'Z'                                            
           OR EGD-TYPCHAMP (I) = 'P'                                            
           OR EGD-TYPCHAMP (I) = '?'                                            
           OR EGD-TYPCHAMP (I) = '*'                                            
              MOVE '''0'''                       TO ZWC-CONTENU2                
           ELSE                                                                 
              MOVE ''' '''                       TO ZWC-CONTENU2                
           END-IF.                                                              
           PERFORM APPEL-COMPARAISON.                                           
           IF ZWC-RETCODE NOT = '1'                                             
              PERFORM ECRITURE-UNE-LIGNE                                        
           END-IF.                                                              
      *-----------------------------------------                                
       ECRITURE-UNE-LIGNE                SECTION.                               
      *-----------------------------------------                                
           IF EGL-SKIPBEFORE (I-L) = 901                                        
              ADD  1                  TO EG-NOPAGE                              
              MOVE EG-NOPAGE          TO W-EDPAGE                               
           END-IF.                                                              
           IF EGL-POSPAGE (I-L) NOT = ZERO                                      
              MOVE EGL-POSPAGE (I-L)  TO W-POSPAGE                              
              MOVE W-EDPAGE           TO WEDITION-LIGNE (W-POSPAGE:3)           
           END-IF.                                                              
           IF EGL-SKIPBEFORE (I-L) > 900                                        
              COMPUTE W-SAUTPAGE = EGL-SKIPBEFORE (I-L) - 900                   
              MOVE W-SAUT                TO WEDITION-SAUT                       
              MOVE ZERO                  TO EG-CNTLIGNE                         
           ELSE                                                                 
              COMPUTE W-SKIPBEFORE = EGL-SKIPBEFORE (I-L)                       
                                   + W-SKIPAFTER                                
              IF W-SKIPBEFORE > 3                                               
                 MOVE 3                  TO W-SKIPBEFORE                        
              END-IF                                                            
              ADD  W-SKIPBEFORE          TO EG-CNTLIGNE                         
              EVALUATE W-SKIPBEFORE                                             
                 WHEN 0                                                         
                    MOVE '+'             TO WEDITION-SAUT                       
                 WHEN 1                                                         
                    MOVE ' '             TO WEDITION-SAUT                       
                 WHEN 2                                                         
                    MOVE '0'             TO WEDITION-SAUT                       
                 WHEN 3                                                         
                    MOVE '-'             TO WEDITION-SAUT                       
                 WHEN OTHER                                                     
                    MOVE '-'             TO WEDITION-SAUT                       
              END-EVALUATE                                                      
           END-IF.                                                              
           INSPECT WEDITION-LIGNE REPLACING ALL LOW-VALUE BY SPACES.            
           PERFORM ECRITURE-FEDITION.                                           
           MOVE ZERO                 TO W-SKIPAFTER.                            
           IF EGL-SKIPAFTER (I-L) > ZERO                                        
              IF EGL-SKIPAFTER (I-L) < 901                                      
                 MOVE EGL-SKIPAFTER (I-L)  TO W-SKIPAFTER                       
              ELSE                                                              
                 IF EGL-SKIPAFTER (I-L) = 901                                   
                    MOVE 'O'                   TO EG-ENTETE                     
                 ELSE                                                           
                    COMPUTE W-SAUTPAGE = EGL-SKIPAFTER (I-L) - 900              
                    MOVE W-SAUT                TO WEDITION-SAUT                 
                    MOVE SPACES                TO WEDITION-LIGNE                
                    PERFORM ECRITURE-FEDITION                                   
                 END-IF                                                         
              END-IF                                                            
           ELSE                                                                 
              IF EGL-SKIPAFTER (I-L) < ZERO                                     
                 MOVE EGL-SKIPAFTER (I-L)  TO W-SKIPAFTER                       
                 PERFORM VARYING W-SKIPAFTER FROM W-SKIPAFTER BY 1              
                         UNTIL   W-SKIPAFTER = ZERO                             
                    MOVE '+'             TO WEDITION-SAUT                       
                    PERFORM ECRITURE-FEDITION                                   
                 END-PERFORM                                                    
              END-IF                                                            
           END-IF.                                                              
           IF EG-CNTLIGNE > EG-LONGUEUR                                         
              MOVE 'O'                   TO EG-ENTETE                           
              MOVE ZERO                  TO EG-CNTLIGNE                         
           END-IF.                                                              
      *-----------------------------------------                                
       REMPLISSAGE-DONNEES               SECTION.                               
      *-----------------------------------------                                
      *================================================================*        
      *    REMPLISSAGE DES CHAMPS EN PROVENANCE DU FICHIER             *        
      *================================================================*        
           PERFORM VARYING I-C FROM 1 BY 1                                      
                   UNTIL   I-C > 256                                            
                   OR EGD-POSDSECT (I-C) = ZERO                                 
              MOVE EGD-I-CHDESC (I-C)  TO I                                     
              MOVE EGD-TYPCHAMP (I)    TO W-TYPCHAMP                            
              MOVE EGD-LGCHAMP (I)     TO W-LGCHAMP                             
              PERFORM CALCUL-LONGUEUR-PHYS                                      
              COMPUTE I-M = EGD-POSDSECT (I-C) - 6                              
              IF W-LGPHYS > 18                                                  
                 MOVE EGD-I-CONT (I)            TO I-K                          
                 MOVE FEXTRAC-LIGNE (I-M:W-LGPHYS) TO                           
                      EGD-CONTENUL (I-K) (1:W-LGPHYS)                           
              ELSE                                                              
                 MOVE FEXTRAC-LIGNE (I-M:W-LGPHYS) TO                           
                      EGD-CONTENU (I) (1:W-LGPHYS)                              
              END-IF                                                            
              IF EGD-OCCURENCES (I) NOT = ZERO                                  
                 ADD  1                   TO I                                  
                 ADD  W-LGPHYS            TO I-M                                
                 IF W-LGPHYS > 18                                               
                    MOVE EGD-I-CONT (I)            TO I-K                       
                    MOVE FEXTRAC-LIGNE (I-M:W-LGPHYS) TO                        
                         EGD-CONTENUL (I-K) (1:W-LGPHYS)                        
                 ELSE                                                           
                    MOVE FEXTRAC-LIGNE (I-M:W-LGPHYS) TO                        
                         EGD-CONTENU (I) (1:W-LGPHYS)                           
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           IF W-OCCURENCES = ZERO                                               
              MOVE 1                        TO W-SEQUENCE                       
           ELSE                                                                 
              MOVE FEXTRAC-LIGNE (I-S:2)    TO W-NOSEQ                          
              IF W-SEQUENCE NOT > ZERO                                          
                 MOVE 1                     TO W-SEQUENCE                       
              END-IF                                                            
           END-IF.                                                              
           IF EG-IG = '1'                                                       
              PERFORM REMPLISSAGE-IG                                            
           END-IF.                                                              
           IF W-NIVRUPT > ZERO                                                  
              IF W-NIVRUPT NOT > EG-I-MAG                                       
                 PERFORM DETERMINATION-TYPEDIT                                  
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       DETERMINATION-TYPEDIT             SECTION.                               
      *-----------------------------------------                                
           IF EG-I-SOC NOT = ZERO                                               
              MOVE EGR-I-CHDESC (EG-I-SOC)  TO I                                
              IF EGD-CONTENU (I) NOT = MEG36-SOCIETE                            
                 MOVE EGD-CONTENU (I)       TO MEG36-SOCIETE                    
                 MOVE SPACES                TO W-TYPEDIT                        
                 PERFORM SELECT-RVGA01E6                                        
              END-IF                                                            
           ELSE                                                                 
              IF EG-NSOC NOT = MEG36-SOCIETE                                    
                 MOVE EG-NSOC               TO MEG36-SOCIETE                    
                 MOVE SPACES                TO W-TYPEDIT                        
                 PERFORM SELECT-RVGA01E6                                        
              END-IF                                                            
           END-IF.                                                              
           IF W-EDIT132                                                         
           OR W-EDIT198                                                         
              IF EG-I-SOC NOT = ZERO                                            
                 MOVE EGR-I-CHDESC (EG-I-SOC)     TO I                          
                 MOVE EGD-CONTENU (I)        TO FEG100-NSOC                     
              ELSE                                                              
                 MOVE EG-NSOC                TO FEG100-NSOC                     
              END-IF                                                            
              IF EG-I-MAG NOT = ZERO                                            
                 MOVE EGR-I-CHDESC (EG-I-MAG)   TO I                            
                 MOVE EGD-CONTENU (I)        TO FEG100-NLIEU                    
              ELSE                                                              
                 MOVE SPACES                 TO FEG100-NLIEU                    
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       REMPLISSAGE-IG                    SECTION.                               
      *-----------------------------------------                                
           MOVE SPACES                      TO IG2-DAT                          
                                               IG3-DST                          
                                               IG4-DOC.                         
           PERFORM VARYING I-G FROM 1 BY 1                                      
                   UNTIL   I-G > 15                                             
                   OR      EGR-NORUPT (I-G) = ZERO                              
              IF EGR-DESCIG (I-G) NOT = ZERO                                    
                 MOVE EGR-I-CHDESC (I-G)       TO I                             
                 MOVE EGD-LGCHAMP (I)          TO W-LGCHAMP                     
                 MOVE EGD-TYPCHAMP (I)         TO W-TYPCHAMP                    
                 PERFORM CALCUL-LONGUEUR-PHYS                                   
                 IF EGR-DESCIG (I-G) = 2                                        
                    IF W-LGPHYS > 18                                            
                       MOVE EGD-I-CONT (I)     TO I-K                           
                       MOVE EGD-CONTENUL (I-K) (1:W-LGPHYS) TO IG2-DAT          
                    ELSE                                                        
                       MOVE EGD-CONTENU (I) (1:W-LGPHYS)    TO IG2-DAT          
                    END-IF                                                      
                 END-IF                                                         
                 IF EGR-DESCIG (I-G) = 3                                        
                    IF W-LGPHYS > 18                                            
                       MOVE EGD-I-CONT (I)     TO I-K                           
                       MOVE EGD-CONTENUL (I-K) (1:W-LGPHYS) TO IG3-DST          
                    ELSE                                                        
                       MOVE EGD-CONTENU (I) (1:W-LGPHYS)    TO IG3-DST          
                    END-IF                                                      
                 END-IF                                                         
                 IF EGR-DESCIG (I-G) = 4                                        
                    IF W-LGPHYS > 18                                            
                       MOVE EGD-I-CONT (I)     TO I-K                           
                       MOVE EGD-CONTENUL (I-K) (1:W-LGPHYS) TO IG4-DOC          
                    ELSE                                                        
                       MOVE EGD-CONTENU (I) (1:W-LGPHYS)    TO IG4-DOC          
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
           IF IG3-DST NOT = SPACES                                              
           OR IG4-DOC NOT = SPACES                                              
              IF IG2-DAT = SPACES                                               
                 MOVE EG-DATE (3:6)         TO IG2-DAT                          
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       CALCUL-NIVEAU-RUPTURE             SECTION.                               
      *-----------------------------------------                                
           MOVE 1                        TO W-NIVRUPT.                          
           MOVE 1                        TO I-P.                                
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL I > 15                                                 
                   OR EGR-NORUPT (I) = ZERO                                     
              MOVE EGR-I-CHDESC (I)         TO I-C                              
              MOVE EGD-TYPCHAMP (I-C)       TO W-TYPCHAMP                       
              MOVE EGD-LGCHAMP (I-C)        TO W-LGCHAMP                        
              MOVE EGR-LGPHYS (I)           TO W-LGPHYS                         
              IF FEXTRAC-LIGNE (I-P:W-LGPHYS) = RES-RUPT (I-P:W-LGPHYS)         
                 ADD  1                        TO W-NIVRUPT                     
              ELSE                                                              
                 MOVE 15                       TO I                             
              END-IF                                                            
              ADD  W-LGPHYS                    TO I-P                           
           END-PERFORM.                                                         
           PERFORM INITIALISATION-RUPTURES.                                     
      *-----------------------------------------                                
       INITIALISATION-RUPTURES           SECTION.                               
      *-----------------------------------------                                
           MOVE RES-RUPT                    TO W-LECTCUMULS.                    
           MOVE FEXTRAC-LIGNE (1:EG-LGRUPT) TO RES-RUPT (1:EG-LGRUPT).          
      *-----------------------------------------                                
       TRAITEMENT-RAZ                    SECTION.                               
      *-----------------------------------------                                
           MOVE EGT-I-CHAMP (I-T)   TO I-C.                                     
           MOVE EGV-I-CHDESC (I-C)  TO I-R.                                     
           IF EGD-TYPCHAMP (I-R) = 'I'                                          
              PERFORM RAZ-COMPTEUR                                              
           ELSE                                                                 
              PERFORM RAZ-CHAMP-CALCULE                                         
           END-IF.                                                              
      *-----------------------------------------                                
       RAZ-CHAMP-CALCULE                 SECTION.                               
      *-----------------------------------------                                
           MOVE I-R                      TO I-CL1.                              
           MOVE I-R                      TO I-CL2.                              
           MOVE 'Z '                     TO ZW-TYPCALCUL.                       
           MOVE ' '                      TO ZW-TYPCHAMP1.                       
           MOVE ZERO                     TO ZW-LGCHAMP1.                        
           MOVE ZERO                     TO ZW-DECCHAMP1.                       
           MOVE SPACES                   TO ZW-CONTENU1.                        
           MOVE ' '                      TO ZW-TYPCHAMP2.                       
           MOVE ZERO                     TO ZW-LGCHAMP2.                        
           MOVE ZERO                     TO ZW-DECCHAMP2.                       
           MOVE SPACES                   TO ZW-CONTENU2.                        
           IF EGD-TYPCHAMP (I-R) = 'R'                                          
              MOVE 'F'                      TO ZW-TYPCHAMPR                     
           ELSE                                                                 
              MOVE EGD-TYPCHAMP (I-R)       TO ZW-TYPCHAMPR                     
           END-IF.                                                              
           MOVE EGD-LGCHAMP (I-R)        TO ZW-LGCHAMPR.                        
           MOVE EGD-DECCHAMP (I-R)       TO ZW-DECCHAMPR.                       
           MOVE SPACES                   TO ZW-CONTENUR.                        
           PERFORM APPEL-CALCULS.                                               
           IF EGD-OCCURENCES (I-R) > ZERO                                       
              COMPUTE I-R = I-R + (EGD-OCCURENCES (I-R) - 1)                    
              MOVE EGD-TYPCHAMP (I-R)       TO ZW-TYPCHAMPR                     
              MOVE EGD-LGCHAMP (I-R)        TO ZW-LGCHAMPR                      
              MOVE EGD-DECCHAMP (I-R)       TO ZW-DECCHAMPR                     
              MOVE SPACES                   TO ZW-CONTENUR                      
              PERFORM APPEL-CALCULS                                             
           END-IF.                                                              
      *-----------------------------------------                                
       RAZ-COMPTEUR                      SECTION.                               
      *-----------------------------------------                                
           INITIALIZE ZWORK-CALCULS.                                            
           MOVE '+ '                  TO ZW-TYPCALCUL.                          
           MOVE EGD-INITCTR (I-R)     TO ZW-CONTENU1.                           
           MOVE 'F'                   TO ZW-TYPCHAMP1.                          
           MOVE ZERO                  TO ZW-LGCHAMP1.                           
           MOVE ZERO                  TO ZW-DECCHAMP1.                          
           MOVE 'N'                   TO ZW-TYPCHAMP2.                          
           MOVE 1                     TO ZW-LGCHAMP2.                           
           MOVE ZERO                  TO ZW-DECCHAMP2.                          
           MOVE '0'                   TO ZW-CONTENU2.                           
           MOVE 'F'                   TO ZW-TYPCHAMPR.                          
           MOVE ZERO                  TO ZW-LGCHAMPR.                           
           MOVE ZERO                  TO ZW-DECCHAMPR.                          
           MOVE SPACES                TO ZW-CONTENUR.                           
           PERFORM APPEL-CALCULS.                                               
      *-----------------------------------------                                
       INITIALISATION-ETAT               SECTION.                               
      *-----------------------------------------                                
           MOVE EG-NOMETAT               TO EG00-NOMETAT                        
                                            EG05-NOMETAT                        
                                            EG10-NOMETAT                        
                                            EG11-NOMETAT                        
                                            EG15-NOMETAT                        
                                            EG20-NOMETAT                        
                                            EG25-NOMETAT                        
                                            EG30-NOMETAT.                       
           PERFORM SELECT-RTEG00.                                               
           IF EG00-DSECTCREE NOT = 'O'                                          
              STRING EG00-NOMETAT                                               
                     ' DSECT NON RECREE POUR CET ETAT'                          
                     DELIMITED BY SIZE                                          
              INTO ABEND-MESS                                                   
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                              
           MOVE EG00-RUPTCHTPAGE         TO EG-RUPTCHTPAGE.                     
           MOVE EG00-RUPTRAZPAGE         TO EG-RUPTRAZPAGE.                     
           MOVE EG00-LARGEUR             TO EG-LARGEUR.                         
           MOVE EG00-LONGUEUR            TO EG-LONGUEUR.                        
           MOVE EG00-LONGUEUR            TO EG-LONGORIG.                        
           IF EG-RUPTRAZPAGE NOT = ZERO                                         
              IF EG-RUPTCHTPAGE < EG-RUPTRAZPAGE                                
                 MOVE EG-RUPTRAZPAGE        TO EG-RUPTCHTPAGE                   
              END-IF                                                            
           END-IF.                                                              
           PERFORM RECHERCHE-RUPTURES.                                          
           PERFORM RECHERCHE-LIGNES.                                            
           PERFORM RECHERCHE-CHAMPS.                                            
           PERFORM RECHERCHE-NOMCHAMPS.                                         
           MOVE I                   TO I-F.                                     
           PERFORM RECHERCHE-CONSTANTES                                         
           PERFORM RECHERCHE-CALCULS.                                           
           PERFORM RECHERCHE-TOTALISATIONS.                                     
           PERFORM RECHERCHE-NLIEU.                                             
           IF SQL-TROUVE                                                        
              MOVE EG10-POSCHAMP         TO EG-I-MAG                            
           END-IF.                                                              
           PERFORM RECHERCHE-NSOCIETE.                                          
           IF SQL-TROUVE                                                        
              MOVE EG10-POSCHAMP      TO EG-I-SOC                               
           END-IF.                                                              
      *-----------------------------------------                                
       RECHERCHE-RUPTURES                SECTION.                               
      *-----------------------------------------                                
           PERFORM OPEN-CURSEUR-RUPT.                                           
           MOVE ZERO                     TO I.                                  
           PERFORM FETCH-CURSEUR-RUPT.                                          
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              ADD  1                        TO I                                
              MOVE EG10-POSCHAMP            TO EGR-NORUPT (I)                   
              MOVE EG10-RUPTIMPR            TO EGR-DESCIG (I)                   
              PERFORM FETCH-CURSEUR-RUPT                                        
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-RUPT.                                          
           PERFORM VARYING I FROM 1 BY 1                                        
                   UNTIL   I > 15                                               
                   OR      EGR-NORUPT (I) = ZERO                                
                   OR      EG-IG > SPACES                                       
              IF EGR-DESCIG (I) > ZERO                                          
                 MOVE '1'           TO EG-IG                                    
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       RECHERCHE-LIGNES                  SECTION.                               
      *-----------------------------------------                                
           PERFORM OPEN-CURSEUR-EG05.                                           
           MOVE ZERO                     TO I.                                  
           PERFORM FETCH-CURSEUR-EG05.                                          
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              PERFORM REMPLISSAGE-LIGNE                                         
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-EG05.                                          
      *-----------------------------------------                                
       REMPLISSAGE-LIGNE                 SECTION.                               
      *-----------------------------------------                                
           MOVE EG05-TYPLIGNE         TO EG10-TYPLIGNE.                         
           MOVE EG05-NOLIGNE          TO EG10-NOLIGNE.                          
           MOVE EG05-CONTINUER        TO EG10-CONTINUER.                        
           ADD  1                     TO I.                                     
           MOVE EG05-TYPLIGNE         TO EGL-TYPLIGNE (I).                      
           MOVE EG05-NOLIGNE          TO EGL-NOLIGNE (I).                       
           MOVE EG05-CONTINUER        TO EGL-CONTINUER (I).                     
           MOVE EG05-SKIPBEFORE       TO EGL-SKIPBEFORE (I).                    
           MOVE EG05-SKIPAFTER        TO EGL-SKIPAFTER (I).                     
           MOVE EG05-RUPTIMPR         TO EGL-RUPTIMPR (I).                      
           MOVE EG05-TYPLIGNE         TO EG20-TYPLIGNE.                         
           MOVE EG05-NOLIGNE          TO EG20-NOLIGNE.                          
           MOVE EG05-CONTINUER        TO EG20-CONTINUER.                        
           PERFORM OPEN-CURSEUR-EG20.                                           
           PERFORM FETCH-CURSEUR-EG20.                                          
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              MOVE EG20-POSCHAMP            TO I-P                              
              MOVE EG20-LIBCHAMP-L          TO I-L                              
              MOVE EG20-LIBCHAMP-V (1:I-L)  TO                                  
                        EGL-FIXES (I) (I-P:I-L)                                 
              PERFORM FETCH-CURSEUR-EG20                                        
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-EG20.                                          
           IF EG05-TYPLIGNE = W-TYPLR                                           
              PERFORM VARYING I-P FROM 1 BY 1                                   
                      UNTIL   I-P > EG-LARGEUR                                  
                      OR      EGL-FIXES (I) (I-P:10)  = W-MREPORT               
              END-PERFORM                                                       
              IF I-P NOT > EG-LARGEUR                                           
                 MOVE I-P                 TO EGL-POSREPORT (I)                  
              END-IF                                                            
           END-IF.                                                              
      *=============================================================*           
      *     ELIMINATION DES CHAMPS VARIABLES ET CONSTANTS           *           
      *=============================================================*           
           PERFORM VARYING I-P FROM 1 BY 1                                      
                   UNTIL I-P > EG-LARGEUR                                       
              IF EGL-FIXES (I) (I-P:1) = '�' OR '�'                             
                 MOVE ' '             TO EGL-FIXES (I) (I-P:1)                  
                 COMPUTE I-L = I-P + 1                                          
                 PERFORM VARYING I-L FROM I-L BY 1                              
                         UNTIL I-L > EG-LARGEUR                                 
                         OR EGL-FIXES (I) (I-L:1) NOT = '_'                     
                    MOVE ' '          TO EGL-FIXES (I) (I-L:1)                  
                 END-PERFORM                                                    
              END-IF                                                            
           END-PERFORM.                                                         
      *=============================================================*           
           EVALUATE EG05-TYPLIGNE                                               
              WHEN W-TYPLI                                                      
                 IF EG-I-LIGNEI = ZERO                                          
                    MOVE I                     TO EG-I-LIGNEI                   
                 END-IF                                                         
              WHEN W-TYPLC                                                      
                 IF EG-I-LIGNEC = ZERO                                          
                    MOVE I                     TO EG-I-LIGNEC                   
                 END-IF                                                         
              WHEN W-TYPLE                                                      
                 IF EG-I-LIGNEE = ZERO                                          
                    MOVE I                     TO EG-I-LIGNEE                   
                 END-IF                                                         
              WHEN W-TYPLD                                                      
                 IF EG-I-LIGNED = ZERO                                          
                    MOVE I                     TO EG-I-LIGNED                   
                 END-IF                                                         
              WHEN W-TYPLR                                                      
                 IF EG-I-LIGNER = ZERO                                          
                    MOVE I                     TO EG-I-LIGNER                   
                 END-IF                                                         
              WHEN W-TYPLT                                                      
                 IF EG-I-LIGNET = ZERO                                          
                    MOVE I                     TO EG-I-LIGNET                   
                 END-IF                                                         
              WHEN W-TYPLF                                                      
                 IF EG-I-LIGNEF = ZERO                                          
                    MOVE I                     TO EG-I-LIGNEF                   
                 END-IF                                                         
              WHEN W-TYPLB                                                      
                 IF EG-I-LIGNEB = ZERO                                          
                    MOVE I                     TO EG-I-LIGNEB                   
                 END-IF                                                         
           END-EVALUATE.                                                        
           PERFORM FETCH-CURSEUR-EG05.                                          
      *-----------------------------------------                                
       RECHERCHE-CHAMPS                  SECTION.                               
      *-----------------------------------------                                
      *    MOVE SPACES                TO EGC-CHAMPI.                            
           PERFORM OPEN-CURSEUR-CHAMPS.                                         
           MOVE ZERO                     TO I.                                  
           PERFORM FETCH-CURSEUR-CHAMPS.                                        
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              ADD  1                        TO I                                
              IF I > 255                                                        
                 MOVE 'TABLE DES CHAMPS DE L''ETAT TROP PETITE'                 
                      TO ABEND-MESS                                             
                 PERFORM ABEND-PROGRAMME                                        
              END-IF                                                            
              MOVE EG10-TYPLIGNE         TO EGC-TYPLIGNE (I)                    
              MOVE EG10-NOLIGNE          TO EGC-NOLIGNE (I)                     
              MOVE EG10-CONTINUER        TO EGC-CONTINUER (I)                   
              MOVE EG10-POSCHAMP         TO EGC-POSCHAMP (I)                    
              MOVE EG10-RUPTIMPR         TO EGC-RUPTIMPR (I)                    
              MOVE '0'                   TO EGC-IMPRIME (I)                     
              MOVE W-CLASSE-CHAMP        TO EGC-CLASSE (I)                      
              EVALUATE W-CLASSE-CHAMP                                           
                 WHEN 'V'                                                       
                    PERFORM REMPLISSAGE-CHAMPV                                  
                 WHEN 'C'                                                       
                    PERFORM REMPLISSAGE-CHAMPC                                  
              END-EVALUATE                                                      
              PERFORM VARYING I-L FROM 1 BY 1                                   
                      UNTIL I-L > 66                                            
                      OR (EGL-TYPLIGNE (I-L)  = EG10-TYPLIGNE                   
                      AND EGL-NOLIGNE (I-L)   = EG10-NOLIGNE                    
                      AND EGL-CONTINUER (I-L) = EG10-CONTINUER)                 
              END-PERFORM                                                       
              IF I-L NOT > 66                                                   
                 IF EGL-I-CHVAR (I-L) = ZERO                                    
                    MOVE I               TO EGL-I-CHVAR (I-L)                   
                 END-IF                                                         
              END-IF                                                            
              PERFORM FETCH-CURSEUR-CHAMPS                                      
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-CHAMPS.                                        
      *-----------------------------------------                                
       REMPLISSAGE-CHAMPV                SECTION.                               
      *-----------------------------------------                                
           MOVE SPACES                   TO EG10-MASKCHAMP-V.                   
           PERFORM SELECT-RTEG10.                                               
           MOVE EG10-CONDITION           TO EGV-CONDITION (I).                  
           MOVE ZERO                     TO EGV-I-CHCOND1 (I).                  
           MOVE ZERO                     TO EGV-I-CHCOND2 (I).                  
           MOVE ZERO                     TO EGV-I-MCHAMP (I).                   
           MOVE ZERO                     TO EGV-I-LCHAMP (I).                   
           IF EG10-MASKCHAMP-V NOT = SPACES                                     
              ADD  1                        TO I-LM                             
              IF I-LM > 120                                                     
                 MOVE 'TABLE DES MASQUES TROP PETITE'                           
                      TO ABEND-MESS                                             
                 PERFORM ABEND-PROGRAMME                                        
              END-IF                                                            
              MOVE I-LM                     TO EGV-I-MCHAMP (I)                 
              MOVE EG10-MASKCHAMP-L         TO EGV-MASKCHAMP-L (I-LM)           
              MOVE EG10-MASKCHAMP-V         TO EGV-MASKCHAMP-V (I-LM)           
           ELSE                                                                 
              MOVE EG10-MASKCHAMP-L         TO EGV-I-LCHAMP (I)                 
           END-IF.                                                              
      *-----------------------------------------                                
       REMPLISSAGE-CHAMPC                SECTION.                               
      *-----------------------------------------                                
           MOVE SPACES                   TO EG11-LIBCHAMP-V.                    
           MOVE EG10-TYPLIGNE            TO EG11-TYPLIGNE.                      
           MOVE EG10-NOLIGNE             TO EG11-NOLIGNE.                       
           MOVE EG10-CONTINUER           TO EG11-CONTINUER.                     
           MOVE EG10-POSCHAMP            TO EG11-POSCHAMP.                      
           PERFORM SELECT-RTEG11.                                               
           MOVE EG11-NOMCHAMPS           TO EGK-NOMCHAMPS (I).                  
           MOVE EG11-SOUSTABLE           TO EGK-SOUSTABLE (I).                  
           MOVE EG11-CONDITION           TO EGK-CONDITION (I).                  
           MOVE ZERO                     TO EGK-I-CHAMPC (I).                   
           MOVE ZERO                     TO EGK-I-CHCOND1 (I).                  
           MOVE ZERO                     TO EGK-I-CHCOND2 (I).                  
           MOVE ZERO                     TO EGK-I-LCHAMP (I).                   
           ADD  1                        TO I-LK.                               
           IF I-LK > 20                                                         
              MOVE 'TABLE LIBELLES CONSTANTS TROP PETITE'                       
                   TO ABEND-MESS                                                
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                              
           MOVE I-LK                     TO EGK-I-LCHAMP (I).                   
           MOVE EG11-LIBCHAMP-L          TO EGK-LIBCHAMP-L (I-LK).              
           MOVE EG11-LIBCHAMP-V          TO EGK-LIBCHAMP-V (I-LK).              
      *-----------------------------------------                                
       RECHERCHE-NOMCHAMPS               SECTION.                               
      *-----------------------------------------                                
           PERFORM OPEN-CURSEUR-EG30.                                           
           PERFORM OPEN-CURSEUR-POSIT.                                          
           MOVE ZERO                     TO I.                                  
           MOVE ZERO                     TO I-C.                                
           PERFORM FETCH-CURSEUR-EG30.                                          
           PERFORM FETCH-CURSEUR-POSIT.                                         
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              ADD  1                        TO I                                
              IF I > 300                                                        
                 MOVE 'TABLE DES CHAMPS TROP PETITE'                            
                      TO ABEND-MESS                                             
                 PERFORM ABEND-PROGRAMME                                        
              END-IF                                                            
              PERFORM REMPLISSAGE-NOMCHAMPS                                     
              PERFORM FETCH-CURSEUR-EG30                                        
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-EG30.                                          
           PERFORM CLOSE-CURSEUR-POSIT.                                         
      *-----------------------------------------                                
       REMPLISSAGE-NOMCHAMPS             SECTION.                               
      *-----------------------------------------                                
           PERFORM FETCH-CURSEUR-POSIT                                          
                   UNTIL EG10-NOMCHAMP NOT < EG30-NOMCHAMP.                     
           MOVE EG30-TYPCHAMP       TO EGD-TYPCHAMP (I).                        
           MOVE EG30-LGCHAMP        TO EGD-LGCHAMP (I).                         
           MOVE EG30-DECCHAMP       TO EGD-DECCHAMP (I).                        
           MOVE EG30-NOMCHAMP       TO EGD-CONTENU (I).                         
           MOVE EG30-OCCURENCES     TO EGD-OCCURENCES (I).                      
           IF EG30-OCCURENCES NOT = ZERO                                        
              MOVE EG30-OCCURENCES       TO W-OCCURENCES                        
           END-IF.                                                              
           IF EG30-NOMCHAMP = 'DECIMALES'                                       
              MOVE I                     TO EG-I-DECIM                          
           END-IF.                                                              
           IF EG30-POSDSECT > ZERO                                              
              ADD  1                TO I-C                                      
              MOVE EG30-POSDSECT    TO EGD-POSDSECT (I-C)                       
              MOVE I                TO EGD-I-CHDESC (I-C)                       
           END-IF.                                                              
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
                   OR EG10-NOMCHAMP > EG30-NOMCHAMP                             
              PERFORM INITIALISE-POSITIONS                                      
              PERFORM FETCH-CURSEUR-POSIT                                       
           END-PERFORM.                                                         
           IF EG30-OCCURENCES NOT = ZERO                                        
              PERFORM VARYING I-R FROM 2 BY 1                                   
                      UNTIL I-R > EG30-OCCURENCES                               
                 ADD  1                  TO I                                   
                 MOVE EG30-TYPCHAMP      TO EGD-TYPCHAMP (I)                    
                 MOVE EG30-LGCHAMP       TO EGD-LGCHAMP (I)                     
                 MOVE EG30-DECCHAMP      TO EGD-DECCHAMP (I)                    
                 MOVE EG30-OCCURENCES    TO EGD-OCCURENCES (I)                  
                 MOVE EG30-NOMCHAMP      TO EGD-CONTENU (I)                     
              END-PERFORM                                                       
           END-IF.                                                              
      *-----------------------------------------                                
       INITIALISE-POSITIONS              SECTION.                               
      *-----------------------------------------                                
           EVALUATE W-CLASSE-CHAMP                                              
              WHEN 'R'                                                          
                 PERFORM INITIALISE-POS-RUPT                                    
              WHEN 'C'                                                          
                 PERFORM INITIALISE-POS-CONST                                   
              WHEN 'V'                                                          
                 PERFORM INITIALISE-POS-VAR                                     
              WHEN '1'                                                          
                 PERFORM INITIALISE-POS-COND1                                   
              WHEN '2'                                                          
                 PERFORM INITIALISE-POS-COND2                                   
              WHEN '5'                                                          
                 PERFORM INITIALISE-POS-COND1                                   
              WHEN '6'                                                          
                 PERFORM INITIALISE-POS-COND2                                   
              WHEN 'L'                                                          
                 PERFORM INITIALISE-POS-LIGNE                                   
              WHEN 'T'                                                          
                 PERFORM INITIALISE-POS-CONDL                                   
           END-EVALUATE.                                                        
      *-----------------------------------------                                
       INITIALISE-POS-RUPT               SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-POSCHAMP = EGR-NORUPT (I-L)                            
           END-PERFORM.                                                         
           MOVE I                        TO EGR-I-CHDESC (I-L).                 
           MOVE EGD-TYPCHAMP (I)         TO W-TYPCHAMP.                         
           MOVE EGD-LGCHAMP (I)          TO W-LGCHAMP.                          
           PERFORM CALCUL-LONGUEUR-PHYS.                                        
           ADD  W-LGPHYS                 TO EG-LGRUPT.                          
           MOVE W-LGPHYS                 TO EGR-LGPHYS (I-L).                   
      *-----------------------------------------                                
       INITIALISE-POS-CONST              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-TYPLIGNE  = EGC-TYPLIGNE (I-L)                         
                AND EG10-NOLIGNE   = EGC-NOLIGNE (I-L)                          
                AND EG10-CONTINUER = EGC-CONTINUER (I-L)                        
                AND EG10-POSCHAMP  = EGC-POSCHAMP (I-L)                         
           END-PERFORM.                                                         
           MOVE I                       TO EGK-I-CHAMPC (I-L).                  
      *-----------------------------------------                                
       INITIALISE-POS-VAR                SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-TYPLIGNE  = EGC-TYPLIGNE (I-L)                         
                AND EG10-NOLIGNE   = EGC-NOLIGNE (I-L)                          
                AND EG10-CONTINUER = EGC-CONTINUER (I-L)                        
                AND EG10-POSCHAMP  = EGC-POSCHAMP (I-L)                         
           END-PERFORM.                                                         
           MOVE I                        TO EGV-I-CHDESC (I-L).                 
           MOVE ZERO                     TO EGV-I-CHTOT (I-L).                  
      *-----------------------------------------                                
       INITIALISE-POS-COND1              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-TYPLIGNE  = EGC-TYPLIGNE (I-L)                         
                AND EG10-NOLIGNE   = EGC-NOLIGNE (I-L)                          
                AND EG10-CONTINUER = EGC-CONTINUER (I-L)                        
                AND EG10-POSCHAMP  = EGC-POSCHAMP (I-L)                         
           END-PERFORM.                                                         
           IF W-CLASSE-CHAMP = '1'                                              
              MOVE I                        TO EGV-I-CHCOND1 (I-L)              
           ELSE                                                                 
              MOVE I                        TO EGK-I-CHCOND1 (I-L)              
           END-IF.                                                              
      *-----------------------------------------                                
       INITIALISE-POS-COND2              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-TYPLIGNE  = EGC-TYPLIGNE (I-L)                         
                AND EG10-NOLIGNE   = EGC-NOLIGNE (I-L)                          
                AND EG10-CONTINUER = EGC-CONTINUER (I-L)                        
                AND EG10-POSCHAMP  = EGC-POSCHAMP (I-L)                         
           END-PERFORM.                                                         
           IF W-CLASSE-CHAMP = '2'                                              
              MOVE I                        TO EGV-I-CHCOND2 (I-L)              
           ELSE                                                                 
              MOVE I                        TO EGK-I-CHCOND2 (I-L)              
           END-IF.                                                              
      *-----------------------------------------                                
       INITIALISE-POS-LIGNE              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-TYPLIGNE  = EGL-TYPLIGNE (I-L)                         
                AND EG10-NOLIGNE   = EGL-NOLIGNE (I-L)                          
                AND EG10-CONTINUER = EGL-CONTINUER (I-L)                        
           END-PERFORM.                                                         
           MOVE I                        TO EGL-I-CHVENT (I-L).                 
      *-----------------------------------------                                
       INITIALISE-POS-CONDL              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-TYPLIGNE  = EGL-TYPLIGNE (I-L)                         
                AND EG10-NOLIGNE   = EGL-NOLIGNE (I-L)                          
                AND EG10-CONTINUER = EGL-CONTINUER (I-L)                        
           END-PERFORM.                                                         
           MOVE I                        TO EGL-I-CHTIMP (I-L).                 
      *-----------------------------------------                                
       RECHERCHE-CONSTANTES              SECTION.                               
      *-----------------------------------------                                
           PERFORM OPEN-CURSEUR-KONST.                                          
           PERFORM FETCH-CURSEUR-KONST                                          
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              PERFORM INITIALISE-POS-KONST                                      
              PERFORM FETCH-CURSEUR-KONST                                       
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-KONST.                                         
      *-----------------------------------------                                
       INITIALISE-POS-KONST              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-L FROM 1 BY 1                                      
              UNTIL EG10-TYPLIGNE  = EGC-TYPLIGNE (I-L)                         
                AND EG10-NOLIGNE   = EGC-NOLIGNE (I-L)                          
                AND EG10-CONTINUER = EGC-CONTINUER (I-L)                        
                AND EG10-POSCHAMP  = EGC-POSCHAMP (I-L)                         
           END-PERFORM.                                                         
           PERFORM VARYING I-K FROM I-F BY 1                                    
                   UNTIL EGD-CONTENU (I-K) = EG10-CHCOND2                       
                   OR    EGD-TYPCHAMP (I-K) = SPACES                            
           END-PERFORM.                                                         
           IF EGD-TYPCHAMP (I-K) = SPACES                                       
              ADD  1                     TO I                                   
              MOVE I                     TO I-K                                 
              MOVE 'K'                   TO EGD-TYPCHAMP (I-K)                  
              MOVE ZERO                  TO EGD-LGCHAMP (I-K)                   
              MOVE ZERO                  TO EGD-DECCHAMP (I-K)                  
              MOVE EG10-CHCOND2          TO EGD-CONTENU (I-K)                   
           END-IF                                                               
           IF W-CLASSE-CHAMP = '1'                                              
              MOVE I-K                   TO EGV-I-CHCOND2 (I-L)                 
           ELSE                                                                 
              MOVE I-K                   TO EGK-I-CHCOND2 (I-L)                 
           END-IF.                                                              
      *---------------------------------------                                  
       RECHERCHE-CALCULS               SECTION.                                 
      *---------------------------------------                                  
           PERFORM OPEN-CURSEUR-EG25.                                           
           PERFORM FETCH-CURSEUR-EG25.                                          
           MOVE ZERO                TO I-C.                                     
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              PERFORM RECHERCHER-CALCUL                                         
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-EG25.                                          
           IF W-NOCALCUL > ZERO                                                 
              PERFORM VARYING I-L FROM 1 BY 1                                   
                      UNTIL   I-L > 66                                          
                      OR      EGL-TYPLIGNE (I-L) = ' '                          
                 IF EGL-I-CHVAR (I-L) > ZERO                                    
                    MOVE EGL-I-CHVAR (I-L) TO I-C                               
                    PERFORM VARYING I-C FROM I-C BY 1                           
                            UNTIL   I-C > 255                                   
                            OR      EGC-TYPLIGNE (I-C)  NOT =                   
                                    EGL-TYPLIGNE (I-L)                          
                            OR      EGC-NOLIGNE (I-C)   NOT =                   
                                    EGL-NOLIGNE (I-L)                           
                            OR      EGC-CONTINUER (I-C) NOT =                   
                                    EGL-CONTINUER (I-L)                         
                            OR      EGL-I-CALCUL (I-L) > ZERO                   
                       IF EGC-CLASSE (I-C) = 'V'                                
                          MOVE EGV-I-CHDESC (I-C) TO I                          
                          IF EGD-TYPCHAMP (I) = '*'                             
                          OR EGD-TYPCHAMP (I) = '?'                             
                             IF EGD-NOCALCUL (I) > ZERO                         
                                MOVE I-C        TO EGL-I-CALCUL (I-L)           
                             END-IF                                             
                          END-IF                                                
                       END-IF                                                   
                    END-PERFORM                                                 
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
      *---------------------------------------                                  
       RECHERCHER-CALCUL               SECTION.                                 
      *---------------------------------------                                  
           PERFORM VARYING I-V FROM 1 BY 1                                      
                   UNTIL   I-V > 300                                            
                   OR      EGD-CONTENU (I-V) (1:18) = EG25-NOMCHAMP             
           END-PERFORM.                                                         
           ADD  1                        TO W-NOCALCUL.                         
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
                   OR EG25-NOMCHAMP NOT = EGD-CONTENU (I-V) (1:18)              
              PERFORM INITIALISE-CALCULS                                        
              PERFORM FETCH-CURSEUR-EG25                                        
           END-PERFORM.                                                         
      *-----------------------------------------                                
       INITIALISE-CALCULS                SECTION.                               
      *-----------------------------------------                                
           ADD  1                     TO I-C.                                   
           IF I-C > 150                                                         
              MOVE 'TABLE DES CALCULS TROP PETITE' TO ABEND-MESS                
              PERFORM ABEND-PROGRAMME                                           
           END-IF.                                                              
           IF EGD-NOCALCUL (I-V) = ZERO                                         
              MOVE I-C                TO EGD-NOCALCUL (I-V)                     
           END-IF.                                                              
           MOVE EGD-NOCALCUL (I-V)    TO EGQ-NOCALCUL (I-C).                    
           MOVE EG25-LIGNECALC        TO EGQ-LIGNECALC (I-C).                   
           MOVE EG25-TYPCALCUL        TO EGQ-TYPCALCUL (I-C).                   
           IF EG25-CHCALC1 (1:1) < '0' AND NOT < 'A'                            
              PERFORM INITIALISE-POS-CALC1                                      
           ELSE                                                                 
              IF EG25-CHCALC1 (1:1) NOT = ''''                                  
                 MOVE EG25-CHCALC1 (1:3)   TO W-CHCALCA                         
                 COMPUTE EGQ-I-CHCALC1 (I-C) = W-CHCALC * -1                    
              ELSE                                                              
                 PERFORM VARYING I-K FROM I-F BY 1                              
                         UNTIL EGD-CONTENU (I-K) = EG25-CHCALC1                 
                         OR    EGD-TYPCHAMP (I-K) = SPACES                      
                 END-PERFORM                                                    
                 IF EGD-TYPCHAMP (I-K) = SPACES                                 
                    ADD  1                    TO I                              
                    MOVE I                    TO I-K                            
                    MOVE 'K'                  TO EGD-TYPCHAMP (I-K)             
                    MOVE ZERO                 TO EGD-LGCHAMP (I-K)              
                    MOVE ZERO                 TO EGD-DECCHAMP (I-K)             
                    MOVE EG25-CHCALC1         TO EGD-CONTENU (I-K)              
                 END-IF                                                         
                 MOVE I-K                  TO EGQ-I-CHCALC1 (I-C)               
              END-IF                                                            
           END-IF.                                                              
           IF EG25-CHCALC2 (1:1) < '0' AND NOT < 'A'                            
              PERFORM INITIALISE-POS-CALC2                                      
           ELSE                                                                 
              IF EG25-CHCALC2 (1:1) NOT = ''''                                  
                 MOVE EG25-CHCALC2 (1:3)   TO W-CHCALCA                         
                 COMPUTE EGQ-I-CHCALC2 (I-C) = W-CHCALC * -1                    
              ELSE                                                              
                 PERFORM VARYING I-K FROM I-F BY 1                              
                         UNTIL EGD-CONTENU (I-K) = EG25-CHCALC2                 
                         OR    EGD-TYPCHAMP (I-K) = SPACES                      
                 END-PERFORM                                                    
                 IF EGD-TYPCHAMP (I-K) = SPACES                                 
                    ADD  1                    TO I                              
                    MOVE I                    TO I-K                            
                    MOVE 'K'                  TO EGD-TYPCHAMP (I-K)             
                    MOVE ZERO                 TO EGD-LGCHAMP (I-K)              
                    MOVE ZERO                 TO EGD-DECCHAMP (I-K)             
                    MOVE EG25-CHCALC2         TO EGD-CONTENU (I-K)              
                 END-IF                                                         
                 MOVE I-K                  TO EGQ-I-CHCALC2 (I-C)               
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       INITIALISE-POS-CALC1              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-Q FROM 1 BY 1                                      
                   UNTIL EGD-CONTENU (I-Q) (1:18) = EG25-CHCALC1                
           END-PERFORM.                                                         
           MOVE I-Q             TO EGQ-I-CHCALC1 (I-C).                         
      *-----------------------------------------                                
       INITIALISE-POS-CALC2              SECTION.                               
      *-----------------------------------------                                
           PERFORM VARYING I-Q FROM 1 BY 1                                      
                   UNTIL EGD-CONTENU (I-Q) (1:18) = EG25-CHCALC2                
           END-PERFORM.                                                         
           MOVE I-Q             TO EGQ-I-CHCALC2 (I-C).                         
      *-----------------------------------------                                
       RECHERCHE-TOTALISATIONS           SECTION.                               
      *-----------------------------------------                                
           PERFORM OPEN-CURSEUR-EG15.                                           
           MOVE ZERO                     TO I.                                  
           PERFORM FETCH-CURSEUR-EG15.                                          
           PERFORM UNTIL SQL-FIN-CURSEUR                                        
              ADD  1                     TO I                                   
              IF I > 100                                                        
                 MOVE 'TABLE DES TOTALISATIONS TROP PETITE'                     
                      TO ABEND-MESS                                             
                 PERFORM ABEND-PROGRAMME                                        
              END-IF                                                            
              MOVE EG15-TYPLIGNE         TO EGT-TYPLIGNE (I)                    
              MOVE EG15-NOLIGNE          TO EGT-NOLIGNE (I)                     
              MOVE EG15-CONTINUER        TO EGT-CONTINUER (I)                   
              MOVE EG15-POSCHAMP         TO EGT-POSCHAMP (I)                    
              MOVE EG15-RUPTRAZ          TO EGT-RUPTRAZ (I)                     
              PERFORM VARYING I-T FROM 1 BY 1                                   
                      UNTIL I-T > 300                                           
                      OR EGD-CONTENU (I-T) (1:18) = EG15-NOMCHAMPT              
              END-PERFORM                                                       
              IF I-T NOT > 300                                                  
                 MOVE I-T                TO EGT-I-CHDESC (I)                    
              END-IF                                                            
              IF EG15-RUPTRAZ = ZERO                                            
                 PERFORM VARYING I-L FROM 1 BY 1                                
                         UNTIL   EGL-TYPLIGNE (I-L)  =                          
                                 EGT-TYPLIGNE (I)                               
                           AND   EGL-NOLIGNE (I-L)   =                          
                                 EGT-NOLIGNE (I)                                
                           AND   EGL-CONTINUER (I-L) =                          
                                 EGT-CONTINUER (I)                              
                 END-PERFORM                                                    
                 MOVE EGL-RUPTIMPR (I-L) TO EGT-RUPTRAZ (I)                     
              END-IF                                                            
              PERFORM FETCH-CURSEUR-EG15                                        
           END-PERFORM.                                                         
           PERFORM CLOSE-CURSEUR-EG15.                                          
           PERFORM VARYING I-C FROM 1 BY 1                                      
                   UNTIL   I-C > 255                                            
                   OR      EGC-TYPLIGNE (I-C) = ' '                             
              IF EGC-CLASSE (I-C) = 'V'                                         
                 PERFORM VARYING I-T FROM 1 BY 1                                
                         UNTIL   I-T > 100                                      
                         OR      EGT-TYPLIGNE (I-T) = ' '                       
                    PERFORM VARYING I-L FROM 1 BY 1                             
                       UNTIL EGL-TYPLIGNE (I-L)  = EGT-TYPLIGNE (I-T)           
                         AND EGL-NOLIGNE (I-L)   = EGT-NOLIGNE (I-T)            
                         AND EGL-CONTINUER (I-L) = EGT-CONTINUER (I-T)          
                    END-PERFORM                                                 
                    IF EGL-I-TOTAL (I-L) = ZERO                                 
                       MOVE I-T                 TO EGL-I-TOTAL (I-L)            
                    END-IF                                                      
                    IF EGV-I-CHDESC (I-C) = EGT-I-CHDESC (I-T)                  
                       MOVE I-C                 TO EGT-I-CHTOT (I-T)            
                       IF EGL-I-CHVENT (I-L) = ZERO                             
                          MOVE I-T                 TO EGV-I-CHTOT (I-C)         
                       END-IF                                                   
                    END-IF                                                      
                    IF EGC-TYPLIGNE (I-C)  = EGT-TYPLIGNE (I-T)  AND            
                       EGC-NOLIGNE (I-C)   = EGT-NOLIGNE (I-T)   AND            
                       EGC-CONTINUER (I-C) = EGT-CONTINUER (I-T) AND            
                       EGC-POSCHAMP (I-C)  = EGT-POSCHAMP (I-T)                 
                       MOVE I-C                 TO EGT-I-CHAMP (I-T)            
                    END-IF                                                      
                 END-PERFORM                                                    
              END-IF                                                            
           END-PERFORM.                                                         
      *-----------------------------------------                                
       CALCUL-LONGUEUR-PHYS              SECTION.                               
      *-----------------------------------------                                
           EVALUATE W-TYPCHAMP                                                  
              WHEN 'C'                                                          
                 MOVE W-LGCHAMP               TO W-LGPHYS                       
              WHEN 'Z'                                                          
                 MOVE W-LGCHAMP               TO W-LGPHYS                       
              WHEN 'N'                                                          
                 MOVE W-LGCHAMP               TO W-LGPHYS                       
              WHEN 'H'                                                          
                 MOVE 2                       TO W-LGPHYS                       
              WHEN 'F'                                                          
                 MOVE 4                       TO W-LGPHYS                       
              WHEN 'D'                                                          
                 MOVE 8                       TO W-LGPHYS                       
              WHEN 'S'                                                          
                 MOVE 8                       TO W-LGPHYS                       
              WHEN 'Y'                                                          
                 MOVE 6                       TO W-LGPHYS                       
              WHEN '*'                                                          
                 MOVE 8                       TO W-LGPHYS                       
              WHEN 'P'                                                          
                 PERFORM CALCUL-LONGUEUR-PACKE                                  
              WHEN '?'                                                          
                 PERFORM CALCUL-LONGUEUR-PACKE                                  
              WHEN 'K'                                                          
                 MOVE 18                      TO W-LGPHYS                       
              END-EVALUATE.                                                     
      *-----------------------------------------                                
       CALCUL-LONGUEUR-PACKE             SECTION.                               
      *-----------------------------------------                                
           COMPUTE W-LGPHYS ROUNDED = (W-LGCHAMP + 1) / 2.                      
      *-----------------------------------------                                
       SELECT-RTPT99                SECTION.                                    
      *---------------------------------------                                  
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RVPR9901.                                               
      *                                                                         
           EXEC SQL SELECT CODLANG, CODPIC                                      
                    INTO  :PT9901-CODLANG,                                      
                          :PT9901-CODPIC                                        
                    FROM  RVPT9901                                              
           END-EXEC.                                                            
      *                                                                         
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
      *                                                                         
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           ELSE                                                                 
              IF SQL-NON-TROUVE                                                 
                 STRING 'CODE LANGUE NON TROUVE'                                
                     DELIMITED BY SIZE                                          
                     INTO ABEND-MESS                                            
                     PERFORM ABEND-PROGRAMME                                    
              END-IF                                                            
           END-IF.                                                              
      *                                                                         
           MOVE PT9901-CODLANG              TO W-CODLANG.                       
           MOVE PT9901-CODPIC               TO W-CODPIC.                        
      *                                                                         
       FIN-SELECT-RTPT99.           EXIT.                                       
      *-----------------------------------------                                
       SELECT-RTEG00                     SECTION.                               
      *-----------------------------------------                                
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RTEG00.                                                 
           EXEC SQL SELECT RUPTCHTPAGE,                                         
                           RUPTRAZPAGE,                                         
                           LARGEUR,                                             
                           LONGUEUR,                                            
                           DSECTCREE,                                           
                           LIBETAT                                              
                      INTO :EG00-RUPTCHTPAGE,                                   
                           :EG00-RUPTRAZPAGE,                                   
                           :EG00-LARGEUR,                                       
                           :EG00-LONGUEUR,                                      
                           :EG00-DSECTCREE,                                     
                           :EG00-LIBETAT                                        
                      FROM RVEG0000                                             
                     WHERE NOMETAT = :EG00-NOMETAT                              
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           ELSE                                                                 
              IF SQL-NON-TROUVE                                                 
                 STRING 'ETAT '                                                 
                        EG00-NOMETAT                                            
                        ' NON TROUVE'                                           
                     DELIMITED BY SIZE                                          
                     INTO ABEND-MESS                                            
                     PERFORM ABEND-PROGRAMME                                    
              END-IF                                                            
           END-IF.                                                              
      *-----------------------------------------                                
       SELECT-RTEG10                     SECTION.                               
      *-----------------------------------------                                
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL SELECT CHCOND1,                                             
                           CONDITION,                                           
                           CHCOND2,                                             
                           MASKCHAMP                                            
                      INTO :EG10-CHCOND1,                                       
                           :EG10-CONDITION,                                     
                           :EG10-CHCOND2,                                       
                           :EG10-MASKCHAMP                                      
                      FROM RVEG1000                                             
                     WHERE NOMETAT   = :EG10-NOMETAT                            
                       AND TYPLIGNE  = :EG10-TYPLIGNE                           
                       AND NOLIGNE   = :EG10-NOLIGNE                            
                       AND CONTINUER = :EG10-CONTINUER                          
                       AND POSCHAMP  = :EG10-POSCHAMP                           
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       SELECT-RTEG11                     SECTION.                               
      *-----------------------------------------                                
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RTEG11.                                                 
           EXEC SQL SELECT SOUSTABLE,                                           
                           NOMCHAMPS,                                           
                           CONDITION,                                           
                           LIBCHAMP                                             
                      INTO :EG11-SOUSTABLE,                                     
                           :EG11-NOMCHAMPS,                                     
                           :EG11-CONDITION,                                     
                           :EG11-LIBCHAMP                                       
                      FROM RVEG1100                                             
                     WHERE NOMETAT   = :EG11-NOMETAT                            
                       AND TYPLIGNE  = :EG11-TYPLIGNE                           
                       AND NOLIGNE   = :EG11-NOLIGNE                            
                       AND CONTINUER = :EG11-CONTINUER                          
                       AND POSCHAMP  = :EG11-POSCHAMP                           
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-EG05                 SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG05.                                                 
           EXEC SQL OPEN EG05            END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-EG05                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG05.                                                 
           EXEC SQL FETCH EG05                                                  
                     INTO :EG05-TYPLIGNE,                                       
                          :EG05-NOLIGNE,                                        
                          :EG05-CONTINUER,                                      
                          :EG05-SKIPBEFORE,                                     
                          :EG05-SKIPAFTER,                                      
                          :EG05-RUPTIMPR                                        
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-EG05                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG05.                                                 
           EXEC SQL CLOSE EG05           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-EG15                 SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG15.                                                 
           EXEC SQL OPEN EG15            END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-EG15                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG15.                                                 
           EXEC SQL FETCH EG15                                                  
                     INTO :EG15-TYPLIGNE,                                       
                          :EG15-NOLIGNE,                                        
                          :EG15-CONTINUER,                                      
                          :EG15-POSCHAMP,                                       
                          :EG15-RUPTRAZ,                                        
                          :EG15-NOMCHAMPT                                       
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-EG15                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG15.                                                 
           EXEC SQL CLOSE EG15           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-EG20                 SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG20.                                                 
           EXEC SQL OPEN EG20            END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-EG20                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG20.                                                 
           EXEC SQL FETCH EG20                                                  
                     INTO :EG20-TYPLIGNE,                                       
                          :EG20-NOLIGNE,                                        
                          :EG20-CONTINUER,                                      
                          :EG20-POSCHAMP,                                       
                          :EG20-LIBCHAMP                                        
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-EG20                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG20.                                                 
           EXEC SQL CLOSE EG20           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-RUPT                 SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL OPEN RUPT            END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-RUPT                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL FETCH RUPT                                                  
                     INTO :EG10-POSCHAMP,                                       
                          :EG10-RUPTIMPR                                        
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-RUPT                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL CLOSE RUPT           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-CHAMPS               SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL OPEN CHAMPS          END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-CHAMPS              SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL FETCH CHAMPS                                                
                     INTO :W-CLASSE-CHAMP,                                      
                          :EG10-TYPLIGNE,                                       
                          :EG10-NOLIGNE,                                        
                          :EG10-CONTINUER,                                      
                          :EG10-POSCHAMP,                                       
                          :EG10-RUPTIMPR                                        
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-CHAMPS              SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL CLOSE CHAMPS         END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-POSIT                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL OPEN POSIT           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-POSIT               SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL FETCH POSIT                                                 
                     INTO :W-CLASSE-CHAMP,                                      
                          :EG10-TYPLIGNE,                                       
                          :EG10-NOLIGNE,                                        
                          :EG10-CONTINUER,                                      
                          :EG10-POSCHAMP,                                       
                          :EG10-NOMCHAMP                                        
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
           IF SQL-FIN-CURSEUR                                                   
              MOVE HIGH-VALUE            TO EG10-NOMCHAMP                       
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-POSIT               SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL CLOSE POSIT          END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-KONST                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL OPEN KONST           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-KONST               SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL FETCH KONST                                                 
                     INTO :W-CLASSE-CHAMP,                                      
                          :EG10-TYPLIGNE,                                       
                          :EG10-NOLIGNE,                                        
                          :EG10-CONTINUER,                                      
                          :EG10-POSCHAMP,                                       
                          :EG10-CHCOND2                                         
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-KONST               SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL CLOSE KONST          END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-EG30                 SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG30.                                                 
           EXEC SQL OPEN EG30            END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-EG30                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG30.                                                 
           EXEC SQL FETCH EG30                                                  
                     INTO :EG30-NOMCHAMP,                                       
                          :EG30-TYPCHAMP,                                       
                          :EG30-LGCHAMP,                                        
                          :EG30-DECCHAMP,                                       
                          :EG30-POSDSECT,                                       
                          :EG30-OCCURENCES                                      
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-EG30                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG30.                                                 
           EXEC SQL CLOSE EG30           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       OPEN-CURSEUR-EG25                 SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-OPEN               TO TRUE.                               
           PERFORM CLEF-RTEG25.                                                 
           EXEC SQL OPEN EG25            END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       FETCH-CURSEUR-EG25                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-FETCH              TO TRUE.                               
           PERFORM CLEF-RTEG25.                                                 
           EXEC SQL FETCH EG25                                                  
                     INTO :EG25-NOMCHAMP,                                       
                          :EG25-LIGNECALC,                                      
                          :EG25-CHCALC1,                                        
                          :EG25-TYPCALCUL,                                      
                          :EG25-CHCALC2                                         
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       CLOSE-CURSEUR-EG25                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-CLOSE              TO TRUE.                               
           PERFORM CLEF-RTEG25.                                                 
           EXEC SQL CLOSE EG25           END-EXEC.                              
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       RECHERCHE-NLIEU                   SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-SELECT             TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL SELECT EG10.POSCHAMP                                        
                      INTO :EG10-POSCHAMP                                       
                      FROM RVEG1000 EG10                                        
                     WHERE EG10.NOMETAT  = :EG-NOMETAT                          
                       AND EG10.TYPLIGNE = '7'                                  
                       AND EG10.NOMCHAMP IN ('NLIEU',                           
                                             'NMAG',                            
                                             'MAG')                             
                       AND NOT EXISTS (SELECT 1                                 
                                         FROM RVEG0500                          
                                        WHERE NOMETAT = EG10.NOMETAT            
                                          AND TYPLIGNE = '4'                    
                                          AND RUPTIMPR < EG10.POSCHAMP)         
                END-EXEC.                                                       
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *-----------------------------------------                                
       RECHERCHE-NSOCIETE                SECTION.                               
      *-----------------------------------------                                
           SET  FONCT-SELECT             TO TRUE.                               
           PERFORM CLEF-RTEG10.                                                 
           EXEC SQL SELECT EG10.POSCHAMP                                        
                      INTO :EG10-POSCHAMP                                       
                      FROM RVEG1000 EG10                                        
                     WHERE EG10.NOMETAT  = :EG-NOMETAT                          
                       AND EG10.TYPLIGNE = '7'                                  
                       AND EG10.NOMCHAMP IN ('NSOCIETE',                        
                                             'NSOC',                            
                                             'SOC')                             
                       AND NOT EXISTS (SELECT 1                                 
                                         FROM RVEG0500                          
                                        WHERE NOMETAT = EG10.NOMETAT            
                                          AND TYPLIGNE = '4'                    
                                          AND RUPTIMPR < EG10.POSCHAMP)         
                END-EXEC.                                                       
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *----------------------------------------                                 
       SELECT-RVGA01E6                  SECTION.                                
      *----------------------------------------                                 
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RVGA01E6.                                               
           EXEC SQL SELECT FORMAT                                               
                      INTO :W-TYPEDIT                                           
                      FROM RVGA01E6                                             
                     WHERE SOCIETE  = :MEG36-SOCIETE                            
                       AND ETAT     = :EG-NOMETAT                               
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *----------------------------------------                                 
       SELECT-RVGA01IG                  SECTION.                                
      *----------------------------------------                                 
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RVGA01IG.                                               
           EXEC SQL SELECT WTABLEG                                              
                      INTO :EGIMP-WTABLEG                                       
                      FROM RVGA01IG                                             
                     WHERE CETAT    = :EGIMP-CETAT                              
                       AND DEB_FIN  = :EGIMP-DEB-FIN                            
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *----------------------------------------                                 
       LECTURE-RTGA01                   SECTION.                                
      *----------------------------------------                                 
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RTGA01.                                                 
           EXEC SQL SELECT WTABLEG                                              
                      INTO :GA01-WTABLEG                                        
                      FROM RVGA0100                                             
                     WHERE CTABLEG1 = :GA01-CTABLEG1                            
                       AND CTABLEG2 = :GA01-CTABLEG2                            
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *----------------------------------------                                 
       LECTURE-RTGA71                   SECTION.                                
      *----------------------------------------                                 
           SET FONCT-SELECT              TO TRUE.                               
           PERFORM CLEF-RTGA71.                                                 
           EXEC SQL SELECT CHAMP1,                                              
                           QCHAMP1,                                             
                           WPOSCLE1,                                            
                           CHAMP2,                                              
                           QCHAMP2,                                             
                           WPOSCLE2,                                            
                           CHAMP3,                                              
                           QCHAMP3,                                             
                           WPOSCLE3,                                            
                           CHAMP4,                                              
                           QCHAMP4,                                             
                           WPOSCLE4,                                            
                           CHAMP5,                                              
                           QCHAMP5,                                             
                           WPOSCLE5,                                            
                           CHAMP6,                                              
                           QCHAMP6,                                             
                           WPOSCLE6                                             
                      INTO :GA71-CHAMP1,                                        
                           :GA71-QCHAMP1,                                       
                           :GA71-WPOSCLE1,                                      
                           :GA71-CHAMP2,                                        
                           :GA71-QCHAMP2,                                       
                           :GA71-WPOSCLE2,                                      
                           :GA71-CHAMP3,                                        
                           :GA71-QCHAMP3,                                       
                           :GA71-WPOSCLE3,                                      
                           :GA71-CHAMP4,                                        
                           :GA71-QCHAMP4,                                       
                           :GA71-WPOSCLE4,                                      
                           :GA71-CHAMP5,                                        
                           :GA71-QCHAMP5,                                       
                           :GA71-WPOSCLE5,                                      
                           :GA71-CHAMP6,                                        
                           :GA71-QCHAMP6,                                       
                           :GA71-WPOSCLE6                                       
                      FROM RVGA7100                                             
                     WHERE CTABLE   = 'RTGA01'                                  
                       AND CSTABLE  = :GA71-CSTABLE                             
                    END-EXEC.                                                   
           MOVE SQLCODE                  TO CODE-RETOUR-SQL.                    
           IF NOT SQL-VALIDE                                                    
              PERFORM ABEND-SQL                                                 
           END-IF.                                                              
      *----------------------------------------                                 
       CLEF-RVPR9901                    SECTION.                                
      *----------------------------------------                                 
           MOVE 'RVPT9901'               TO SQL-NOMTABLE.                       
      *----------------------------------------                                 
       CLEF-RVGA01E6                    SECTION.                                
      *----------------------------------------                                 
           MOVE 'RVGA01E6'               TO SQL-NOMTABLE.                       
      *----------------------------------------                                 
       CLEF-RVGA01IG.                                                           
      *----------------------------------------                                 
           MOVE 'RVGA01IG'               TO SQL-NOMTABLE.                       
      *----------------------------------------                                 
       CLEF-RTGA01                      SECTION .                               
      *----------------------------------------                                 
           MOVE 'RVGA0100'               TO SQL-NOMTABLE.                       
      *----------------------------------------                                 
       CLEF-RTGA71                      SECTION.                                
      *----------------------------------------                                 
           MOVE 'RVGA7100'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG00                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG0000'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG05                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG0500'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG10                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG1000'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG11                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG1100'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG15                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG1500'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG20                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG2000'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG25                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG2500'               TO SQL-NOMTABLE.                       
      *-----------------------------------------                                
       CLEF-RTEG30                       SECTION.                               
      *-----------------------------------------                                
           MOVE 'RVEG3000'               TO SQL-NOMTABLE.                       
                                                                                
