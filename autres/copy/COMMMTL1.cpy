      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE TP MTL31  BATCH MTL030         *            
      *     DETERMINATION DU TYPE D'EQUIPAGE                       *            
      **************************************************************            
      **************************************************************            
      *                                                                         
       01 COMM-MTL31-APPLI.                                                     
          02 COMM-MTL31-ENTREE.                                                 
             03 COMM-MTL31-NSOC           PIC X(03).                            
             03 COMM-MTL31-NMAG           PIC X(03).                            
             03 COMM-MTL31-NVENTE         PIC X(07).                            
             03 COMM-MTL31-DLIVR          PIC X(08).                            
           02 COMM-MTL31-SORTIE.                                                
              03 COMM-MTL31-CEQUI         PIC X(03).                            
              03 COMM-MTL31-CODRET        PIC X.                                
                 88 COMM-MTL31-OK               VALUE '0'.                      
                 88 COMM-MTL31-KO               VALUE '1'.                      
              03 COMM-MTL31-MSG           PIC X(70).                            
                                                                                
