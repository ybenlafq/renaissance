      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *****************************************************************         
      * ZONES D'INTERFACE POUR LES ACCES CICS                         *         
      ****************************************************** SYKWCICS *         
       01  CODES-EIBRESP.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EIB-RESP          PIC S9(4)     COMP.                            
      *--                                                                       
           05  EIB-RESP          PIC S9(4) COMP-5.                              
      *}                                                                        
               88  RESP-NORMAL        VALUE +00.                                
               88  RESP-ERROR         VALUE +01.                                
               88  RESP-RDATT         VALUE +02.                                
               88  RESP-WRBRK         VALUE +03.                                
               88  RESP-EOF           VALUE +04.                                
               88  RESP-EODS          VALUE +05.                                
               88  RESP-EOC           VALUE +06.                                
               88  RESP-INBFMH        VALUE +07.                                
               88  RESP-ENDINPT       VALUE +08.                                
               88  RESP-NONVAL        VALUE +09.                                
               88  RESP-NOSTART       VALUE +10.                                
               88  RESP-TERMIDERR     VALUE +11.                                
               88  RESP-DSIDERR       VALUE +12.                                
               88  RESP-NOTFND        VALUE +13.                                
               88  RESP-DUPREC        VALUE +14.                                
               88  RESP-DUPKEY        VALUE +15.                                
               88  RESP-INVREQ        VALUE +16.                                
               88  RESP-IOERR         VALUE +17.                                
               88  RESP-NOSPACE       VALUE +18.                                
               88  RESP-NOTOPEN       VALUE +19.                                
               88  RESP-ENDFILE       VALUE +20.                                
               88  RESP-ILLOGIC       VALUE +21.                                
               88  RESP-LENGERR       VALUE +22.                                
               88  RESP-QZERO         VALUE +23.                                
               88  RESP-SIGNAL        VALUE +24.                                
               88  RESP-QBUSY         VALUE +25.                                
               88  RESP-ITEMERR       VALUE +26.                                
               88  RESP-PGMIDERR      VALUE +27.                                
               88  RESP-TRANSIDERR    VALUE +28.                                
               88  RESP-ENDDATA       VALUE +29.                                
               88  RESP-INVTSREQ      VALUE +30.                                
               88  RESP-EXPIRED       VALUE +31.                                
               88  RESP-RETPAGE       VALUE +32.                                
               88  RESP-RTEFAIL       VALUE +33.                                
               88  RESP-RTESOME       VALUE +34.                                
               88  RESP-TSIOERR       VALUE +35.                                
               88  RESP-MAPFAIL       VALUE +36.                                
               88  RESP-INVERRTERM    VALUE +37.                                
               88  RESP-INVMPSZ       VALUE +38.                                
               88  RESP-IGREQID       VALUE +39.                                
               88  RESP-OVERFLOW      VALUE +40.                                
               88  RESP-INVLDC        VALUE +41.                                
               88  RESP-NOSTG         VALUE +42.                                
               88  RESP-JIDERR        VALUE +43.                                
               88  RESP-QIDERR        VALUE +44.                                
               88  RESP-NOJBUFSP      VALUE +45.                                
               88  RESP-DSSTAT        VALUE +46.                                
               88  RESP-SELNERR       VALUE +47.                                
               88  RESP-FUNCERR       VALUE +48.                                
               88  RESP-UNEXPIN       VALUE +49.                                
               88  RESP-NOPASSBKRD    VALUE +50.                                
               88  RESP-NOPASSBKWR    VALUE +51.                                
               88  RESP-SYSIDERR      VALUE +53.                                
               88  RESP-ISCINVREQ     VALUE +54.                                
               88  RESP-ENQBUSY       VALUE +55.                                
               88  RESP-ENVDEFERR     VALUE +56.                                
               88  RESP-IGCREQCD      VALUE +57.                                
               88  RESP-SESSIONERR    VALUE +58.                                
               88  RESP-SYSBUSY       VALUE +59.                                
               88  RESP-SESSBUSY      VALUE +60.                                
               88  RESP-NOTALLOC      VALUE +61.                                
               88  RESP-CBIDERR       VALUE +62.                                
               88  RESP-INVPARNTSET   VALUE +64.                                
               88  RESP-INVPARTN      VALUE +65.                                
               88  RESP-PARTNFAIL     VALUE +66.                                
               88  RESP-NOTAUTH       VALUE +70.                                
               88  RESP-NOSPOOL       VALUE +80.                                
               88  RESP-TERMERR       VALUE +81.                                
               88  RESP-ROLLEDBACK    VALUE +82.                                
               88  RESP-END           VALUE +83.                                
               88  RESP-DISABLED      VALUE +84.                                
               88  RESP-ALLOCERR      VALUE +85.                                
               88  RESP-STRELERR      VALUE +86.                                
               88  RESP-OPENERR       VALUE +87.                                
               88  RESP-SPOLBUSY      VALUE +88.                                
               88  RESP-SPOLERR       VALUE +89.                                
               88  RESP-NODEIDER      VALUE +90.                                
       01  CODES-EIB-ABEND.                                                     
           05  EIB-ABEND-ERROR        PIC X(4) VALUE 'AEIA'.                    
           05  EIB-ABEND-RDATT        PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-WRBRK        PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-EOF          PIC X(4) VALUE 'AEID'.                    
           05  EIB-ABEND-EODS         PIC X(4) VALUE 'AEIE'.                    
           05  EIB-ABEND-EOC          PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-INBFMH       PIC X(4) VALUE 'AEIG'.                    
           05  EIB-ABEND-ENDINPT      PIC X(4) VALUE 'AEIH'.                    
           05  EIB-ABEND-NONVAL       PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-NOSTART      PIC X(4) VALUE 'AEIJ'.                    
           05  EIB-ABEND-TERMIDERR    PIC X(4) VALUE 'AEIK'.                    
           05  EIB-ABEND-DSIDERR      PIC X(4) VALUE 'AEIL'.                    
           05  EIB-ABEND-NOTFND       PIC X(4) VALUE 'AEIM'.                    
           05  EIB-ABEND-DUPREC       PIC X(4) VALUE 'AEIN'.                    
           05  EIB-ABEND-DUPKEY       PIC X(4) VALUE 'AEIO'.                    
           05  EIB-ABEND-INVREQ       PIC X(4) VALUE 'AEIP'.                    
           05  EIB-ABEND-IOERR        PIC X(4) VALUE 'AEIQ'.                    
           05  EIB-ABEND-NOSPACE      PIC X(4) VALUE 'AEIR'.                    
           05  EIB-ABEND-NOTOPEN      PIC X(4) VALUE 'AEIS'.                    
           05  EIB-ABEND-ENDFILE      PIC X(4) VALUE 'AEIT'.                    
           05  EIB-ABEND-ILLOGIC      PIC X(4) VALUE 'AEIU'.                    
           05  EIB-ABEND-LENGERR      PIC X(4) VALUE 'AEIV'.                    
           05  EIB-ABEND-QZERO        PIC X(4) VALUE 'AEIW'.                    
           05  EIB-ABEND-SIGNAL       PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-QBUSY        PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-ITEMERR      PIC X(4) VALUE 'AEIZ'.                    
           05  EIB-ABEND-PGMIDERR     PIC X(4) VALUE 'AEI0'.                    
           05  EIB-ABEND-TRANSIDERR   PIC X(4) VALUE 'AEI1'.                    
           05  EIB-ABEND-ENDDATA      PIC X(4) VALUE 'AEI2'.                    
           05  EIB-ABEND-INVTSREQ     PIC X(4) VALUE 'AEI3'.                    
           05  EIB-ABEND-EXPIRED      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-RETPAGE      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-RTEFAIL      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-RTESOME      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-TSIOERR      PIC X(4) VALUE 'AEI8'.                    
           05  EIB-ABEND-MAPFAIL      PIC X(4) VALUE 'AEI9'.                    
           05  EIB-ABEND-INVERRTERM   PIC X(4) VALUE 'AEYA'.                    
           05  EIB-ABEND-INVMPSZ      PIC X(4) VALUE 'AEYB'.                    
           05  EIB-ABEND-IGREQID      PIC X(4) VALUE 'AEYC'.                    
           05  EIB-ABEND-OVERFLOW     PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-INVLDC       PIC X(4) VALUE 'AEYE'.                    
           05  EIB-ABEND-NOSTG        PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-JIDERR       PIC X(4) VALUE 'AEYG'.                    
           05  EIB-ABEND-QIDERR       PIC X(4) VALUE 'AEYH'.                    
           05  EIB-ABEND-NOJBUFSP     PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-DSSTAT       PIC X(4) VALUE 'AEYJ'.                    
           05  EIB-ABEND-SELNERR      PIC X(4) VALUE 'AEYK'.                    
           05  EIB-ABEND-FUNCERR      PIC X(4) VALUE 'AEYL'.                    
           05  EIB-ABEND-UNEXPIN      PIC X(4) VALUE 'AEYM'.                    
           05  EIB-ABEND-NOPASSBKRD   PIC X(4) VALUE 'AEYN'.                    
           05  EIB-ABEND-NOPASSBKWR   PIC X(4) VALUE 'AEYO'.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  EIB-ABEND-SYSIDERR     PIC X(4) VALUE 'AEYQ'.                    
           05  EIB-ABEND-ISCINVREQ    PIC X(4) VALUE 'AEYR'.                    
           05  EIB-ABEND-ENQBUSY      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-ENVDEFERR    PIC X(4) VALUE 'AEYT'.                    
           05  EIB-ABEND-IGCREQCD     PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-SESSIONERR   PIC X(4) VALUE 'AEYV'.                    
           05  EIB-ABEND-SYSBUSY      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-SESSBUSY     PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-NOTALLOC     PIC X(4) VALUE 'AEYZ'.                    
           05  EIB-ABEND-CBIDERR      PIC X(4) VALUE '    '.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  EIB-ABEND-INVPARNTSET  PIC X(4) VALUE 'AEY1'.                    
           05  EIB-ABEND-INVPARTN     PIC X(4) VALUE 'AEY2'.                    
           05  EIB-ABEND-PARTNFAIL    PIC X(4) VALUE 'AEY3'.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  EIB-ABEND-NOTAUTH      PIC X(4) VALUE '    '.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  FILLER                 PIC X(4) VALUE SPACES.                    
           05  EIB-ABEND-NOSPOOL      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-TERMERR      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-ROLLEDBACK   PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-END          PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-DISABLED     PIC X(4) VALUE 'AEXL'.                    
           05  EIB-ABEND-ALLOCERR     PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-STRELERR     PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-OPENERR      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-SPOLBUSY     PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-SPOLERR      PIC X(4) VALUE '    '.                    
           05  EIB-ABEND-NODEIDER     PIC X(4) VALUE '    '.                    
       01  FILLER    REDEFINES CODES-EIB-ABEND.                                 
           05  EIB-ABCODE         PIC X(4) OCCURS 90.                           
      *                                                                         
       01  FILLER.                                                              
      *                                                                         
           05  TRACE-CICS-MESSAGE.                                              
      *                                                                         
             10  FILLER         PIC  X(7)  VALUE 'CODE = '.                     
             10  CICS-ABCODE    PIC  X(8)  VALUE SPACES.                        
      *                                                                         
             10  FILLER         PIC  X(12) VALUE ' FONCTION = '.                
             10  CICS-FONCTION  PIC  X(16) VALUE SPACES.                        
      *                                                                         
                 88  CICS-WRITE-DATASET    VALUE 'WRITE DATASET   '.            
                 88  CICS-READ-DATASET     VALUE 'READ DATASET    '.            
                 88  CICS-REWRITE-DATASET  VALUE 'REWRITE DATASET '.            
                 88  CICS-DELETE-DATASET   VALUE 'DELETE DATASET  '.            
                 88  CICS-STARTBR-DATASET  VALUE 'STARTBR DATASET '.            
                 88  CICS-READNEXT-DATASET VALUE 'READNEXT DATASET'.            
                 88  CICS-ENDBR-DATASET    VALUE 'ENDBR DATASET   '.            
                 88  CICS-WRITEQ-TS        VALUE 'WRITEQ TS       '.            
                 88  CICS-READQ-TS         VALUE 'READQ TS        '.            
                 88  CICS-DELETEQ-TS       VALUE 'DELETEQ TS      '.            
                 88  CICS-WRITEQ-TD        VALUE 'WRITEQ TD       '.            
                 88  CICS-READQ-TD         VALUE 'READQ TD        '.            
                 88  CICS-DELETEQ-TD       VALUE 'DELETEQ TD      '.            
                 88  CICS-LINK             VALUE 'LINK            '.            
                 88  CICS-XCTL             VALUE 'XCTL            '.            
                 88  CICS-LOAD             VALUE 'LOAD            '.            
                 88  CICS-RELEASE          VALUE 'RELEASE         '.            
                 88  CICS-RETRIEVE         VALUE 'RETRIEVE        '.            
      *                                                                         
             10  FILLER         PIC  X     VALUE SPACES.                        
             10  CICS-NOMPROG.                                                  
                 15  CICS-DATASET   PIC  X(8)  VALUE SPACES.                    
                                                                                
