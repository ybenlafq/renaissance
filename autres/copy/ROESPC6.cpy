      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION OP ENT�TE SPECIFIQUE (MUTATION)       * 00030000
      * NOM FICHIER.: ROESPC6                                         * 00031000
      *---------------------------------------------------------------* 00032000
      * CR   .......: 27/08/2013                                      * 00033000
      * MODIFIE.....:   /  /                                          * 00034000
      * VERSION N�..: 001                                             * 00035000
      * LONGUEUR....: 482                                             * 00036000
      ***************************************************************** 00037000
      *                                                                 00037100
       01  ROESPC6.                                                     00037200
      * TYPE ENREGISTREMENT                                             00037300
           05      ROESPC6-TYP-ENREG      PIC  X(0006).                 00037400
      * CODE SOCI�T�                                                    00037500
           05      ROESPC6-CSOCIETE       PIC  X(0005).                 00037600
      * NUM�RO D OP                                                     00037700
           05      ROESPC6-NOP            PIC  X(0015).                 00037800
      * TYPE CLIENT                                                     00037900
           05      ROESPC6-TYPE-CLIENT    PIC  X(0006).                 00038000
      * CODE ITIN�RAIRE                                                 00038100
           05      ROESPC6-C-ITINERAIRE   PIC  X(0007).                 00038200
      * FILIALE                                                         00038300
           05      ROESPC6-FILIALE        PIC  9(0003).                 00038400
      * GESTION DES RELIQUATS OUI / NON                                 00038500
           05      ROESPC6-RELIQUAT       PIC  9(0001).                 00038600
      * LANGUE                                                          00038700
           05      ROESPC6-LANGUE         PIC  X(0003).                 00038800
      * OP REGROUPABLE O/N                                              00038900
           05      ROESPC6-OP-REGROUPABLE PIC  9(0001).                 00039000
      * TYPE DE MAGASIN                                                 00040000
           05      ROESPC6-TYPE-MAGASIN   PIC  X(0006).                 00050000
      * ADRESSE CONSOMMATEUR 1                                          00051000
           05      ROESPC6-ADR-CONSO-1    PIC  X(0040).                 00052000
      * ADRESSE CONSOMMATEUR 2                                          00053000
           05      ROESPC6-ADR-CONSO-2    PIC  X(0040).                 00054000
      * ADRESSE CONSOMMATEUR 3                                          00055000
           05      ROESPC6-ADR-CONSO-3    PIC  X(0040).                 00056000
      * ADRESSE CONSOMMATEUR 4                                          00057000
           05      ROESPC6-ADR-CONSO-4    PIC  X(0040).                 00058000
      * ADRESSE CONSOMMATEUR 5                                          00059000
           05      ROESPC6-ADR-CONSO-5    PIC  X(0040).                 00059100
      * ADRESSE CONSOMMATEUR 6                                          00059200
           05      ROESPC6-ADR-CONSO-6    PIC  X(0040).                 00059300
      * ADRESSE CONSOMMATEUR 7                                          00059400
           05      ROESPC6-ADR-CONSO-7    PIC  X(0040).                 00059500
      * ADRESSE CONSOMMATEUR 8                                          00059600
           05      ROESPC6-ADR-CONSO-8    PIC  X(0040).                 00059700
      * ADRESSE CONSOMMATEUR 9                                          00059800
           05      ROESPC6-ADR-CONSO-9    PIC  X(0040).                 00059900
      * TYPE DE FLUX                                                    00060000
           05      ROESPC6-TYPE-FLUX      PIC  X(0002).                 00060100
      * DESTINATION                                                     00060200
           05      ROESPC6-DESTINATION    PIC  X(0012).                 00060300
      * TYPE DE DOSSIER                                                 00060400
           05      ROESPC6-TYPE-DOSSIER   PIC  X(0012).                 00060500
      * ENSEIGNEFLUX                                                    00060600
           05      ROESPC6-ENSEIGNE       PIC  X(0012).                 00060700
      * FILLER 30                                                       00060800
           05      ROESPC6-FILLER         PIC  X(0030).                 00060900
      * FIN                                                             00061000
           05      ROESPC6-FIN            PIC  X(0001).                 00062000
                                                                                
