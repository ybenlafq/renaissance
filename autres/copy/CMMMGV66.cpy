      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * PROJET     : GESTION DES VENTES                                *        
      * TRANSACTION: GV00                                              *        
      * TITRE      : COMMAREA D'APPEL MGV66, MODULE INTERROGATION      *        
      * LONGUEUR   : ????                                              *        
      * -------------------------------------------------------------- *        
        01 COMM-GV66-APPLI.                                                     
           05 COMM-GV66-ZIN.                                                    
               10 COMM-GV66-NSOCIETE         PIC X(03).                         
               10 COMM-GV66-NLIEU            PIC X(03).                         
               10 COMM-GV66-NVENTE           PIC X(07).                         
               10 COMM-GV66-NORDRE           PIC X(05).                         
               10 COMM-GV66-SSAAMMJJ         PIC X(08).                         
               10 COMM-GV66-TOP-VTE          PIC X(01).                         
                  88 COMM-GV66-CREATION                 VALUE 'C'.              
                  88 COMM-GV66-MODIF                    VALUE 'M'.              
      * ->                                                                      
           05 COMM-GV66-ZOUT.                                                   
               10 COMM-GV66-CODRET           PIC X(04).                         
               10 COMM-GV66-LMESSAGE         PIC X(53).                         
                                                                                
