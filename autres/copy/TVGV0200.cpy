      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.TTGV02                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  TVGV0200.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  TVGV0200.                                                            
      *}                                                                        
      *    *************************************************************        
      *                       NSOCIETE                                          
           10 GV02C-NSOCIETE       PIC X(3).                                    
      *    *************************************************************        
      *                       NLIEU                                             
           10 GV02C-NLIEU          PIC X(3).                                    
      *    *************************************************************        
      *                       NORDRE                                            
           10 GV02C-NORDRE         PIC X(5).                                    
      *    *************************************************************        
      *                       NVENTE                                            
           10 GV02C-NVENTE         PIC X(7).                                    
      *    *************************************************************        
      *                       WTYPEADR                                          
           10 GV02C-WTYPEADR       PIC X(1).                                    
      *    *************************************************************        
      *                       CTITRENOM                                         
           10 GV02C-CTITRENOM      PIC X(5).                                    
      *    *************************************************************        
      *                       LNOM                                              
           10 GV02C-LNOM           PIC X(25).                                   
      *    *************************************************************        
      *                       LPRENOM                                           
           10 GV02C-LPRENOM        PIC X(15).                                   
      *    *************************************************************        
      *                       LBATIMENT                                         
           10 GV02C-LBATIMENT      PIC X(3).                                    
      *    *************************************************************        
      *                       LESCALIER                                         
           10 GV02C-LESCALIER      PIC X(3).                                    
      *    *************************************************************        
      *                       LETAGE                                            
           10 GV02C-LETAGE         PIC X(3).                                    
      *    *************************************************************        
      *                       LPORTE                                            
           10 GV02C-LPORTE         PIC X(3).                                    
      *    *************************************************************        
      *                       LCMPAD1                                           
           10 GV02C-LCMPAD1        PIC X(32).                                   
      *    *************************************************************        
      *                       LCMPAD2                                           
           10 GV02C-LCMPAD2        PIC X(32).                                   
      *    *************************************************************        
      *                       CVOIE                                             
           10 GV02C-CVOIE          PIC X(5).                                    
      *    *************************************************************        
      *                       CTVOIE                                            
           10 GV02C-CTVOIE         PIC X(4).                                    
      *    *************************************************************        
      *                       LNOMVOIE                                          
           10 GV02C-LNOMVOIE       PIC X(21).                                   
      *    *************************************************************        
      *                       LCOMMUNE                                          
           10 GV02C-LCOMMUNE       PIC X(32).                                   
      *    *************************************************************        
      *                       CPOSTAL                                           
           10 GV02C-CPOSTAL        PIC X(5).                                    
      *    *************************************************************        
      *                       LBUREAU                                           
           10 GV02C-LBUREAU        PIC X(26).                                   
      *    *************************************************************        
      *                       TELDOM                                            
           10 GV02C-TELDOM         PIC X(10).                                   
      *    *************************************************************        
      *                       TELBUR                                            
           10 GV02C-TELBUR         PIC X(10).                                   
      *    *************************************************************        
      *                       LPOSTEBUR                                         
           10 GV02C-LPOSTEBUR      PIC X(5).                                    
      *    *************************************************************        
      *                       LCOMLIV1                                          
           10 GV02C-LCOMLIV1       PIC X(78).                                   
      *    *************************************************************        
      *                       LCOMLIV2                                          
           10 GV02C-LCOMLIV2       PIC X(78).                                   
      *    *************************************************************        
      *                       WETRANGER                                         
           10 GV02C-WETRANGER      PIC X(1).                                    
      *    *************************************************************        
      *                       CZONLIV                                           
           10 GV02C-CZONLIV        PIC X(5).                                    
      *    *************************************************************        
      *                       CINSEE                                            
           10 GV02C-CINSEE         PIC X(5).                                    
      *    *************************************************************        
      *                       WCONTRA                                           
           10 GV02C-WCONTRA        PIC X(1).                                    
      *    *************************************************************        
      *                       DSYST                                             
           10 GV02C-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       CILOT                                             
           10 GV02C-CILOT          PIC X(8).                                    
      *    *************************************************************        
      *                       IDCLIENT                                          
           10 GV02C-IDCLIENT       PIC X(8).                                    
      *    *************************************************************        
      *                       CPAYS                                             
           10 GV02C-CPAYS          PIC X(3).                                    
      *    *************************************************************        
      *                       NGSM                                              
           10 GV02C-NGSM           PIC X(15).                                   
      *    *************************************************************        
      *                       XNSOCIETE                                         
           10 GV02C-XNSOCIETE      PIC X(3).                                    
      *    *************************************************************        
      *                       XNLIEU                                            
           10 GV02C-XNLIEU         PIC X(3).                                    
      *    *************************************************************        
      *                       XNORDRE                                           
           10 GV02C-XNORDRE        PIC X(5).                                    
      *    *************************************************************        
      *                       XNVENTE                                           
           10 GV02C-XNVENTE        PIC X(7).                                    
      *    *************************************************************        
      *                       XWTYPEADR                                         
           10 GV02C-XWTYPEADR      PIC X(1).                                    
      *    *************************************************************        
      *                       XCTITRENOM                                        
           10 GV02C-XCTITRENOM     PIC X(5).                                    
      *    *************************************************************        
      *                       XLNOM                                             
           10 GV02C-XLNOM          PIC X(25).                                   
      *    *************************************************************        
      *                       XLPRENOM                                          
           10 GV02C-XLPRENOM       PIC X(15).                                   
      *    *************************************************************        
      *                       XLBATIMENT                                        
           10 GV02C-XLBATIMENT     PIC X(3).                                    
      *    *************************************************************        
      *                       XLESCALIER                                        
           10 GV02C-XLESCALIER     PIC X(3).                                    
      *    *************************************************************        
      *                       XLETAGE                                           
           10 GV02C-XLETAGE        PIC X(3).                                    
      *    *************************************************************        
      *                       XLPORTE                                           
           10 GV02C-XLPORTE        PIC X(3).                                    
      *    *************************************************************        
      *                       XLCMPAD1                                          
           10 GV02C-XLCMPAD1       PIC X(32).                                   
      *    *************************************************************        
      *                       XLCMPAD2                                          
           10 GV02C-XLCMPAD2       PIC X(32).                                   
      *    *************************************************************        
      *                       XCVOIE                                            
           10 GV02C-XCVOIE         PIC X(5).                                    
      *    *************************************************************        
      *                       XCTVOIE                                           
           10 GV02C-XCTVOIE        PIC X(4).                                    
      *    *************************************************************        
      *                       XLNOMVOIE                                         
           10 GV02C-XLNOMVOIE      PIC X(21).                                   
      *    *************************************************************        
      *                       XLCOMMUNE                                         
           10 GV02C-XLCOMMUNE      PIC X(32).                                   
      *    *************************************************************        
      *                       XCPOSTAL                                          
           10 GV02C-XCPOSTAL       PIC X(5).                                    
      *    *************************************************************        
      *                       XLBUREAU                                          
           10 GV02C-XLBUREAU       PIC X(26).                                   
      *    *************************************************************        
      *                       XTELDOM                                           
           10 GV02C-XTELDOM        PIC X(10).                                   
      *    *************************************************************        
      *                       XTELBUR                                           
           10 GV02C-XTELBUR        PIC X(10).                                   
      *    *************************************************************        
      *                       XLPOSTEBUR                                        
           10 GV02C-XLPOSTEBUR     PIC X(5).                                    
      *    *************************************************************        
      *                       XLCOMLIV1                                         
           10 GV02C-XLCOMLIV1      PIC X(78).                                   
      *    *************************************************************        
      *                       XLCOMLIV2                                         
           10 GV02C-XLCOMLIV2      PIC X(78).                                   
      *    *************************************************************        
      *                       XWETRANGER                                        
           10 GV02C-XWETRANGER     PIC X(1).                                    
      *    *************************************************************        
      *                       XCZONLIV                                          
           10 GV02C-XCZONLIV       PIC X(5).                                    
      *    *************************************************************        
      *                       XCINSEE                                           
           10 GV02C-XCINSEE        PIC X(5).                                    
      *    *************************************************************        
      *                       XWCONTRA                                          
           10 GV02C-XWCONTRA       PIC X(1).                                    
      *    *************************************************************        
      *                       XDSYST                                            
           10 GV02C-XDSYST         PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       XCILOT                                            
           10 GV02C-XCILOT         PIC X(8).                                    
      *    *************************************************************        
      *                       XIDCLIENT                                         
           10 GV02C-XIDCLIENT      PIC X(8).                                    
      *    *************************************************************        
      *                       XCPAYS                                            
           10 GV02C-XCPAYS         PIC X(3).                                    
      *    *************************************************************        
      *                       XNGSM                                             
           10 GV02C-XNGSM          PIC X(15).                                   
      *    *************************************************************        
      *                       IBMSNAP_COMMITSEQ                                 
           10 GV02C-IBMSNAP-COMMITSEQ  PIC X(10).                               
      *    *************************************************************        
      *                       IBMSNAP_INTENTSEQ                                 
           10 GV02C-IBMSNAP-INTENTSEQ  PIC X(10).                               
      *    *************************************************************        
      *                       IBMSNAP_OPERATION                                 
           10 GV02C-IBMSNAP-OPERATION  PIC X(1).                                
      *    *************************************************************        
      *                       IBMSNAP_LOGMARKER                                 
           10 GV02C-IBMSNAP-LOGMARKER  PIC X(26).                               
      *    *************************************************************        
      *                       DSYSTUPD                                          
           10 GV02C-DSYSTUPD       PIC S9(13)V USAGE COMP-3.                    
      *    *************************************************************        
      *                       CTYPEUPD                                          
           10 GV02C-CTYPEUPD       PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 74      *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  TVGV0200-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  TVGV0200-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-NSOCIETE-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-NSOCIETE-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-NLIEU-F                 PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-NLIEU-F                 PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-NORDRE-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-NORDRE-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-NVENTE-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-NVENTE-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-WTYPEADR-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-WTYPEADR-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CTITRENOM-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CTITRENOM-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LNOM-F                  PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LNOM-F                  PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LPRENOM-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LPRENOM-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LBATIMENT-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LBATIMENT-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LESCALIER-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LESCALIER-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LETAGE-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LETAGE-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LPORTE-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LPORTE-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LCMPAD1-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LCMPAD1-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LCMPAD2-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LCMPAD2-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CVOIE-F                 PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CVOIE-F                 PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CTVOIE-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CTVOIE-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LNOMVOIE-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LNOMVOIE-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LCOMMUNE-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LCOMMUNE-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CPOSTAL-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CPOSTAL-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LBUREAU-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LBUREAU-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-TELDOM-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-TELDOM-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-TELBUR-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-TELBUR-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LPOSTEBUR-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LPOSTEBUR-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LCOMLIV1-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LCOMLIV1-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-LCOMLIV2-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-LCOMLIV2-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-WETRANGER-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-WETRANGER-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CZONLIV-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CZONLIV-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CINSEE-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CINSEE-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-WCONTRA-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-WCONTRA-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-DSYST-F                 PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-DSYST-F                 PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CILOT-F                 PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CILOT-F                 PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-IDCLIENT-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-IDCLIENT-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CPAYS-F                 PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-CPAYS-F                 PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-NGSM-F                  PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-NGSM-F                  PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XNSOCIETE-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XNSOCIETE-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XNLIEU-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XNLIEU-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XNORDRE-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XNORDRE-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XNVENTE-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XNVENTE-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XWTYPEADR-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XWTYPEADR-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCTITRENOM-F            PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCTITRENOM-F            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLNOM-F                 PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLNOM-F                 PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLPRENOM-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLPRENOM-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLBATIMENT-F            PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLBATIMENT-F            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLESCALIER-F            PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLESCALIER-F            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLETAGE-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLETAGE-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLPORTE-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLPORTE-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLCMPAD1-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLCMPAD1-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLCMPAD2-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLCMPAD2-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCVOIE-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCVOIE-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCTVOIE-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCTVOIE-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLNOMVOIE-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLNOMVOIE-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLCOMMUNE-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLCOMMUNE-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCPOSTAL-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCPOSTAL-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLBUREAU-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLBUREAU-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XTELDOM-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XTELDOM-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XTELBUR-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XTELBUR-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLPOSTEBUR-F            PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLPOSTEBUR-F            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLCOMLIV1-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLCOMLIV1-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XLCOMLIV2-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XLCOMLIV2-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XWETRANGER-F            PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XWETRANGER-F            PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCZONLIV-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCZONLIV-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCINSEE-F               PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCINSEE-F               PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XWCONTRA-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XWCONTRA-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XDSYST-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XDSYST-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCILOT-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCILOT-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XIDCLIENT-F             PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XIDCLIENT-F             PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XCPAYS-F                PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XCPAYS-F                PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-XNGSM-F                 PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-XNGSM-F                 PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-IBMSNAP-COMMITSEQ-F     PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-IBMSNAP-COMMITSEQ-F     PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-IBMSNAP-INTENTSEQ-F     PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-IBMSNAP-INTENTSEQ-F     PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-IBMSNAP-OPERATION-F     PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-IBMSNAP-OPERATION-F     PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-IBMSNAP-LOGMARKER-F     PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-IBMSNAP-LOGMARKER-F     PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-DSYSTUPD-F              PIC S9(4) COMP.                     
      *--                                                                       
           10 GV02C-DSYSTUPD-F              PIC S9(4) COMP-5.                   
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GV02C-CTYPEUPD-F              PIC S9(4) COMP.                     
      *                                                                         
      *--                                                                       
           10 GV02C-CTYPEUPD-F              PIC S9(4) COMP-5.                   
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
