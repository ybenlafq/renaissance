      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * COMM DU MEC09: TRANSFO ADRESSE "TEXTE" EN "GV02"                        
      ******************************************************************        
      *--> DONNEES FOURNIES PAR LE PROGRAMME APPELANT.                          
           05  MEC09C-ENTREE.                                                   
               10  MEC09C-ADRESSE-TEXTE            PIC  X(50).                  
      *--> DONNEES RESULTANTES.                                                 
           05  MEC09C-SORTIE.                                                   
               10  MEC09C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC09C-PARFAIT             VALUE '0'.                    
                   88  MEC09C-OUI-MAIS            VALUE '1'.                    
                   88  MEC09C-J-AI-RIEN-PU-FAIRE VALUE '2'.                     
                   88  MEC09C-C-EST-GRAVE         VALUE '4'.                    
               10  MEC09C-CVOIE                    PIC  X(05).                  
               10  MEC09C-CTVOIE                   PIC  X(04).                  
               10  MEC09C-LNOMVOIE                 PIC  X(21).                  
      ********* FIN DE LA ZONE DE COMMUNICATION DU MEC09 *************          
                                                                                
