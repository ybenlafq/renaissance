      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  Z-COMMAREA-MIF0.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  COMM-MIF0-CDRET      PIC S9(4)     COMP.                         
      *--                                                                       
           05  COMM-MIF0-CDRET      PIC S9(4) COMP-5.                           
      *}                                                                        
           05  COMM-MIF0-MESS       PIC X(64).                                  
           05  COMM-MIF0-NOMCHAMP   PIC X(10).                                  
           05  COMM-MIF0-TYPCHAMP   PIC X.                                      
           05  COMM-MIF0-POSCHAMP   PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-LGCHAMP    PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-DECCHAMP   PIC S9(3)     COMP-3.                       
           05  COMM-MIF0-CHAMPS     PIC X(752).                                 
                                                                                
