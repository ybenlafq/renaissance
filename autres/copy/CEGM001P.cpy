      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *---------------------------------------                          24815099
       TRAITEMENT-EGM001               SECTION.                         24816099
      *---------------------------------------                          24817099
           INITIALIZE WS-EGM000.                                                
                                                                        24819399
           MOVE '0'                 TO EGM-ACCORD-1                             
                                       EGM-ACCORD-2.                            
           MOVE ' '                 TO EGM-ARRONDI.                             
           IF ZWM-TYPCHAMP = 'P'  OR '?' OR '*'                                 
              PERFORM TRAIT-P                                                   
           END-IF.                                                              
           IF ZWM-TYPCHAMP = 'Z'                                                
              PERFORM TRAIT-Z                                                   
           END-IF.                                                              
           IF ZWM-TYPCHAMP = 'N'                                                
              PERFORM TRAIT-N                                                   
           END-IF.                                                              
           IF ZWM-TYPCHAMP = 'H'                                                
              PERFORM TRAIT-H                                                   
           END-IF.                                                              
           IF ZWM-TYPCHAMP = 'F'                                                
              PERFORM TRAIT-F                                                   
           END-IF.                                                              
           IF ZWM-TYPCHAMP = 'D'                                                
              PERFORM TRAIT-D                                                   
           END-IF.                                                              
        FIN-TRAITEMENT-EGM-CHAMP. EXIT.                                 24819499
                                                                        24819599
                                                                        24814199
      *---------------------------------------------------------------  24815099
       TRAIT-P      SECTION.                                            24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
           COMPUTE                                                              
                   EGM-LONGUEUR ROUNDED = (ZWM-LGCHAMP + 1) / 2 .               
           COMPUTE EGM-POSIT = (8 - EGM-LONGUEUR) + 1.                          
           IF EGM-POSIT NOT > 0 THEN                                            
              MOVE 'LGCHAMP INCORRECT' TO ABEND-MESS                            
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE ZWM-CONTENU (1:EGM-LONGUEUR) TO                                 
                EGM-CHPAR (EGM-POSIT:EGM-LONGUEUR).                             
           MOVE EGM-CHPR            TO EGM-CHAMPR.                              
           MOVE EGM-CHPR            TO EGM-CHAMPN.                              
           PERFORM TRAIT-NN.                                                    
        FIN-TRAIT-P.     EXIT.                                          24819499
                                                                        24819599
      *---------------------------------------------------------------  24815099
       TRAIT-Z      SECTION.                                            24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
           MOVE ZWM-LGCHAMP TO EGM-LONGUEUR.                                    
           COMPUTE EGM-POSIT = (15 - EGM-LONGUEUR) + 1.                         
           IF EGM-POSIT NOT > 0 THEN                                            
              MOVE 'LGCHAMP INCORRECT' TO ABEND-MESS                            
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE ZWM-CONTENU(1:EGM-LONGUEUR) TO                                  
                EGM-CHR (EGM-POSIT:EGM-LONGUEUR).                               
           MOVE EGM-CHAMPR          TO EGM-CHAMPN.                              
           PERFORM TRAIT-NN.                                                    
       FIN-TRAIT-Z. EXIT.                                                       
      *---------------------------------------------------------------  24815099
       TRAIT-N      SECTION.                                            24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
           MOVE ZWM-LGCHAMP TO EGM-LONGUEUR.                                    
           COMPUTE EGM-POSIT = (15 - EGM-LONGUEUR) + 1.                         
           IF EGM-POSIT NOT > 0 THEN                                            
              MOVE 'LGCHAMP INCORRECT' TO ABEND-MESS                            
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE ZWM-CONTENU (1:EGM-LONGUEUR) TO                                 
                EGM-CHN (EGM-POSIT:EGM-LONGUEUR).                               
           MOVE EGM-CHAMPN          TO EGM-CHAMPR.                              
           PERFORM TRAIT-NN.                                                    
       FIN-TRAIT-N. EXIT.                                                       
      *---------------------------------------------------------------  24815099
       TRAIT-H      SECTION.                                            24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
           MOVE 4           TO EGM-LONGUEUR.                                    
           COMPUTE EGM-POSIT = (15 - EGM-LONGUEUR) + 1.                         
           IF EGM-POSIT NOT > 0 THEN                                            
              MOVE 'LGCHAMP INCORRECT' TO ABEND-MESS                            
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE ZWM-CONTENU(1:2) TO EGM-Z-HALFWORD.                             
           MOVE EGM-HALFWORD TO EGM-CHPR.                                       
           MOVE EGM-CHPR TO EGM-CHAMPR.                                         
           MOVE EGM-CHPR TO EGM-CHAMPN.                                         
           PERFORM TRAIT-NN.                                                    
       FIN-TRAIT-H. EXIT.                                                       
      *---------------------------------------------------------------  24815099
       TRAIT-F      SECTION.                                            24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
           MOVE 9           TO EGM-LONGUEUR.                                    
           COMPUTE EGM-POSIT = (15 - EGM-LONGUEUR) + 1.                         
           IF EGM-POSIT NOT > 0 THEN                                            
              MOVE 'LGCHAMP INCORRECT' TO ABEND-MESS                            
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE ZWM-CONTENU(1:4) TO EGM-Z-FULLWORD.                             
           MOVE EGM-FULLWORD TO EGM-CHPR.                                       
           MOVE EGM-CHPR TO EGM-CHAMPR.                                         
           MOVE EGM-CHPR TO EGM-CHAMPN.                                         
           PERFORM TRAIT-NN.                                                    
       FIN-TRAIT-F. EXIT.                                                       
      *---------------------------------------------------------------  24815099
       TRAIT-D      SECTION.                                            24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
           MOVE 15          TO EGM-LONGUEUR.                                    
           COMPUTE EGM-POSIT = (15 - EGM-LONGUEUR) + 1.                         
           IF EGM-POSIT NOT > 0 THEN                                            
              MOVE 'LGCHAMP INCORRECT' TO ABEND-MESS                            
              PERFORM FIN-ANORMALE                                              
           END-IF.                                                              
           MOVE ZWM-CONTENU(1:8) TO EGM-Z-DOUBLEWORD.                           
           MOVE EGM-DOUBLEWORD TO EGM-CHPR.                                     
           MOVE EGM-CHPR TO EGM-CHAMPR.                                         
           MOVE EGM-CHPR TO EGM-CHAMPN.                                         
           PERFORM TRAIT-NN.                                                    
       FIN-TRAIT-D. EXIT.                                                       
      *---------------------------------------------------------------  24815099
       TRAIT-NN SECTION.                                                24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
              MOVE SPACES TO EGM-CHAMP.                                         
      ***  VERIFICATION  MASQUE                                                 
           PERFORM VARYING EGM-I FROM 1 BY 1                                    
                   UNTIL EGM-I > ZWM-LGMASQUE                                   
              IF ZWM-MASQUE (EGM-I:2) = '/R'                                    
                 SET  EGM-ARRONDIR        TO TRUE                               
                 MOVE '  '                TO ZWM-MASQUE (EGM-I:2)               
                 COMPUTE ZWM-LGMASQUE = ZWM-LGMASQUE - 2                        
              END-IF                                                            
           END-PERFORM.                                                         
           PERFORM VARYING EGM-I FROM 1 BY 1 UNTIL                              
                     ( ZWM-MASQUE(EGM-I:1) NOT = '-'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = '+'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = '.'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = ','                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = 'Z'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = '9'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = 'C'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = 'R'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = 'D'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = 'T'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = ' '                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = '�'                            
                   AND ZWM-MASQUE(EGM-I:1) NOT = '<')                           
                   OR EGM-I > ZWM-LGMASQUE                                      
           END-PERFORM.                                                         
           IF EGM-I NOT > ZWM-LGMASQUE THEN                                     
                MOVE 'MASQUE INCORRECT' TO ABEND-MESS                           
                PERFORM FIN-ANORMALE                                            
           END-IF.                                                              
      **** SI PREMIER CAR EST = '<'                                             
           IF ZWM-MASQUE(1:1) = '<' THEN                                        
              COMPUTE  EGM-LONG = ZWM-LGMASQUE - 1                              
              MOVE ZWM-MASQUE(2:EGM-LONG) TO ZWM-MASQUE(1:EGM-LONG)             
              SUBTRACT 1 FROM ZWM-LGMASQUE                                      
              IF EGM-CHAMPN = 0 THEN                                            
                 MOVE SPACES TO ZWM-MASQUE                                      
                 GO TO FIN-TRAIT-NN                                             
              END-IF                                                            
           END-IF.                                                              
      **** REDEFINITION DU MASQUE SI VIRGULE-FLOTANTE                           
           IF ZWM-MASQUE(1:1) = '�' THEN                                        
      *       COMPUTE XX = ZWM-DECCHAMP - ZWM-DECEDIT                           
      *       COMPUTE EGM-CHAMPN = EGM-CHAMPN / (10 ** XX)                      
              IF ZWM-DECEDIT = ZERO                                             
                 PERFORM VARYING EGM-I FROM 2 BY 1                              
                         UNTIL   ZWM-MASQUE (EGM-I:1) = '9'                     
                         OR      ZWM-MASQUE (EGM-I:1) = 'Z'                     
                         OR      EGM-I > ZWM-LGMASQUE                           
                    COMPUTE EGM-J = EGM-I - 1                                   
                    MOVE ZWM-MASQUE(EGM-I:1) TO ZWM-MASQUE(EGM-J:1)             
                 END-PERFORM                                                    
                 COMPUTE EGM-J = EGM-I - 1                                      
                 MOVE ZWM-MASQUE(EGM-I:1) TO ZWM-MASQUE(EGM-J:1)                
                 COMPUTE EGM-LONG = ZWM-LGMASQUE - 1                            
              ELSE                                                              
                 PERFORM VARYING EGM-LONGUEUR FROM ZWM-LGMASQUE BY -1           
                         UNTIL   ZWM-MASQUE (EGM-LONGUEUR:1) = '9'              
                         OR      ZWM-MASQUE (EGM-LONGUEUR:1) = 'Z'              
                         OR      EGM-LONGUEUR = ZERO                            
                 END-PERFORM                                                    
                 COMPUTE EGM-LONG = EGM-LONGUEUR - ZWM-DECEDIT                  
                 PERFORM VARYING EGM-I FROM 2 BY 1                              
                         UNTIL EGM-I = EGM-LONG                                 
                    COMPUTE EGM-J = EGM-I - 1                                   
                    MOVE ZWM-MASQUE(EGM-I:1) TO ZWM-MASQUE(EGM-J:1)             
                 END-PERFORM                                                    
                 MOVE ','               TO ZWM-MASQUE (EGM-I:1)                 
                 COMPUTE EGM-LONG = ZWM-LGMASQUE - (ZWM-DECEDIT + 2)            
              END-IF                                                            
              PERFORM VARYING EGM-I FROM 1 BY 1                                 
                      UNTIL   EGM-I > EGM-LONG                                  
                 IF ZWM-MASQUE (EGM-I:1) = '9'                                  
                    MOVE 'Z'         TO ZWM-MASQUE (EGM-I:1)                    
                 END-IF                                                         
              END-PERFORM                                                       
           END-IF.                                                              
      **** CALCUL POSITION VIRGULE DANS LE MASQUE                               
           PERFORM VARYING EGM-I FROM 1 BY 1                                    
                   UNTIL ZWM-MASQUE(EGM-I:1) = ','                              
                   OR EGM-I > ZWM-LGMASQUE                                      
           END-PERFORM.                                                         
           IF EGM-I NOT > ZWM-LGMASQUE THEN                                     
              MOVE '0' TO EGM-VIRGULE                                           
              MOVE EGM-I TO EGM-POS-VIR                                         
           ELSE                                                                 
              MOVE '1' TO  EGM-VIRGULE                                          
              MOVE  0  TO  EGM-DECMASQ                                          
           END-IF.                                                              
           IF ZWM-MASQUE(1:1) = '<' THEN                                        
              IF EGM-VIRGULE = '0' THEN                                         
                 SUBTRACT 1 FROM EGM-POS-VIR                                    
              END-IF                                                            
           END-IF.                                                              
      **** CALCUL NOMBRE DE DECIMAL DANS  ZWM-MASQUE                            
           IF EGM-VIRGULE = '0'                                                 
              ADD 1 TO EGM-POS-VIR GIVING EGM-DEB                               
              PERFORM VARYING EGM-I FROM EGM-DEB BY 1                           
                      UNTIL   EGM-I > ZWM-LGMASQUE                              
                      OR     (ZWM-MASQUE(EGM-I:1) NOT = '9' AND                 
                              ZWM-MASQUE(EGM-I:1) NOT = 'Z')                    
                IF EGM-I NOT > ZWM-LGMASQUE AND                                 
                   ZWM-MASQUE(EGM-I:1) = '9' OR 'Z'                             
                  ADD 1 TO EGM-DECMASQ                                          
                ELSE                                                            
                  MOVE  'PAS DE DECIMALES APRES VIRGULE' TO ABEND-MESS          
                  PERFORM FIN-ANORMALE                                          
                END-IF                                                          
              END-PERFORM                                                       
           ELSE                                                                 
              MOVE  0  TO  EGM-DECMASQ                                          
           END-IF.                                                              
           IF EGM-DECMASQ > ZWM-DECCHAMP THEN                                   
             COMPUTE                                                            
                  EGM-IND-DEC = EGM-DECMASQ - ZWM-DECCHAMP                      
                  COMPUTE   EGM-CHAMPN = EGM-CHAMPN                             
                                       * (10 ** EGM-IND-DEC)                    
           END-IF.                                                              
           IF EGM-DECMASQ < ZWM-DECCHAMP THEN                                   
              COMPUTE  EGM-IND-DEC = ZWM-DECCHAMP - EGM-DECMASQ                 
              IF EGM-ARRONDIR                                                   
                 COMPUTE EGM-CHAMPN ROUNDED = EGM-CHAMPN                        
                                            / (10 ** EGM-IND-DEC)               
              ELSE                                                              
                 COMPUTE EGM-CHAMPN = EGM-CHAMPN                                
                                    / (10 ** EGM-IND-DEC)                       
              END-IF                                                            
           END-IF.                                                              
      ***  REMPLISSAGE ZONES-CR DU EGM-CHAMP SELON MASQUE                       
           IF EGM-VIRGULE = '0' THEN                                            
              COMPUTE  EGM-A = EGM-POS-VIR + EGM-DECMASQ                        
           ELSE                                                                 
              COMPUTE EGM-B = ZWM-LGMASQUE - 1                                  
              IF ZWM-MASQUE(EGM-B:2) = 'CR' OR 'DT'                             
                 COMPUTE EGM-A = EGM-B - 1                                      
              ELSE                                                              
                 MOVE ZWM-LGMASQUE TO EGM-B                                     
                 IF ZWM-MASQUE (EGM-B:1) = '-' OR '+' THEN                      
                    COMPUTE EGM-A = EGM-B - 1                                   
                 ELSE                                                           
                    MOVE ZWM-LGMASQUE TO EGM-A                                  
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           IF EGM-A NOT = ZWM-LGMASQUE                                          
              ADD 1 TO EGM-A GIVING EGM-B                                       
              IF ZWM-MASQUE(EGM-B:2) = 'CR' THEN                                
                 IF EGM-CHAMPR < 0 THEN                                         
                    MOVE  'CR' TO EGM-CHAMP(29:2)                               
                    MOVE '0' TO EGM-ACCORD-2                                    
                 ELSE                                                           
                    MOVE '1' TO EGM-ACCORD-2                                    
                 END-IF                                                         
              ELSE                                                              
                 IF ZWM-MASQUE(EGM-B:2) = 'DT' THEN                             
                    IF EGM-CHAMPR < 0 THEN                                      
                       MOVE  'CR' TO EGM-CHAMP(29:2)                            
                    ELSE                                                        
                       MOVE  'DT' TO EGM-CHAMP(29:2)                            
                    END-IF                                                      
                 ELSE                                                           
                   IF ZWM-MASQUE(EGM-B:1) = '-' THEN                            
                      IF EGM-CHAMPR < 0 THEN                                    
                         MOVE  '-' TO EGM-CHAMP(30:1)                           
                         MOVE '0' TO EGM-ACCORD-1                               
                      ELSE                                                      
                         MOVE '1' TO EGM-ACCORD-1                               
                      END-IF                                                    
                   ELSE                                                         
                      IF ZWM-MASQUE(EGM-B:1) = '+' THEN                         
                         IF EGM-CHAMPR < 0 THEN                                 
                            MOVE  '-' TO EGM-CHAMP(30:1)                        
                         ELSE                                                   
                            MOVE '+'  TO EGM-CHAMP(30:1)                        
                         END-IF                                                 
                      END-IF                                                    
                   END-IF                                                       
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
      ***  CALCUL POSITION DU DERNIER CHIFFRE DU EGM-CHAMP                      
           IF EGM-CHAMP(29:1) NOT = SPACES                                      
           OR EGM-ACCORD-2 = '1'                                                
              MOVE 28 TO EGM-C                                                  
           ELSE                                                                 
              IF EGM-CHAMP(30:1) NOT = SPACE                                    
              OR EGM-ACCORD-1 = '1'                                             
                 MOVE  29 TO  EGM-C                                             
              ELSE                                                              
                 MOVE  30 TO  EGM-C                                             
              END-IF                                                            
           END-IF.                                                              
      ***  REMPLISSAGE ZONE DECIMALE DU EGM-CHAMP                               
           IF EGM-VIRGULE = '0' THEN                                            
              COMPUTE  EGM-B = 15 - EGM-DECMASQ                                 
              PERFORM VARYING EGM-I FROM 15 BY -1 UNTIL EGM-I = EGM-B           
                 MOVE  EGM-CHAMPN(EGM-I:1) TO EGM-CHAMP(EGM-C:1)                
                 SUBTRACT 1 FROM EGM-C                                          
              END-PERFORM                                                       
           ELSE                                                                 
              MOVE 15 TO EGM-B                                                  
           END-IF.                                                              
      ***  REMPLISSAGE ZONE VIRGULE DU EGM-CHAMP                                
           IF EGM-VIRGULE = '0' THEN                                            
              MOVE ',' TO EGM-CHAMP(EGM-C:1)                                    
              SUBTRACT 1 FROM EGM-C                                             
           END-IF.                                                              
      ***  CALCUL POSITION DU DEBUT ZONE EGM-CHAMP                              
           MOVE 0 TO EGM-D.                                                     
           PERFORM VARYING EGM-I FROM 1 BY 1                                    
                   UNTIL   EGM-I > ZWM-LGMASQUE                                 
             IF ZWM-MASQUE(EGM-I:1) = '9' OR 'Z' THEN                           
                ADD 1 TO   EGM-M                                                
             END-IF                                                             
             ADD 1 TO EGM-D                                                     
           END-PERFORM.                                                         
           COMPUTE  EGM-DEB-CH = 30 - EGM-D .                                   
      *�������������                                                            
      ***  CALCUL POSITION DU DEBUT ZONE NUM DE EGM-CHAMPN                      
           MOVE ZERO         TO EGM-BEG.                                        
           COMPUTE  EGM-DEB-NN = 15 - EGM-M.                                    
           IF EGM-DEB-NN < 0 THEN                                               
      *       MOVE 'LE MASQUE TROP GRAND' TO ABEND-MESS                         
      *       PERFORM FIN-ANORMALE                                              
              COMPUTE EGM-BEG = EGM-DEB-NN * -1                                 
              MOVE ZERO     TO EGM-DEB-NN                                       
           END-IF.                                                              
      ***  REMPLISSAGE ZONE NUMERIQUE DU EGM-CHAMP                              
           IF ZWM-MASQUE(1:1) = '-' OR '+'                                      
              ADD  2 TO EGM-BEG                                                 
              ADD  1 TO EGM-DEB-CH                                              
           ELSE                                                                 
              ADD  1 TO EGM-BEG                                                 
           END-IF.                                                              
           ADD 1 TO EGM-DEB-NN GIVING EGM-J.                                    
           ADD 1 TO EGM-DEB-CH GIVING EGM-K.                                    
           IF EGM-M > 15                                                        
              COMPUTE EGM-K = EGM-K + (EGM-M - 15)                              
           END-IF.                                                              
           IF EGM-VIRGULE = '0' THEN                                            
             SUBTRACT 1 FROM EGM-POS-VIR                                        
           ELSE                                                                 
             MOVE EGM-A TO EGM-POS-VIR                                          
           END-IF.                                                              
           PERFORM VARYING EGM-I FROM EGM-BEG BY 1                              
                   UNTIL   EGM-I > EGM-POS-VIR                                  
             IF ZWM-MASQUE(EGM-I:1) = ' ' THEN                                  
                MOVE ' ' TO EGM-CHAMP(EGM-K:1)                                  
                ADD 1 TO EGM-K                                                  
             END-IF                                                             
             IF ZWM-MASQUE(EGM-I:1) = '.' THEN                                  
                SUBTRACT 1 FROM EGM-J                                           
                PERFORM VERIFICATION-ZERO                                       
                IF EGM-VERIF = '0' THEN                                         
                   MOVE ' ' TO EGM-CHAMP(EGM-K :1)                              
                   ADD 1 TO  EGM-K EGM-J                                        
                ELSE                                                            
                  MOVE '.' TO EGM-CHAMP(EGM-K:1)                                
                  ADD 1 TO  EGM-K EGM-J                                         
             ELSE                                                               
                IF ZWM-MASQUE(EGM-I:1) = '9' THEN                               
                   MOVE EGM-CHAMPN(EGM-J:1) TO EGM-CHAMP(EGM-K:1)               
                   ADD 1 TO EGM-J EGM-K                                         
                ELSE                                                            
                   IF ZWM-MASQUE(EGM-I:1) = 'Z' THEN                            
                      IF EGM-CHAMPN(EGM-J:1) = '0' THEN                         
                         PERFORM VERIFICATION-ZERO                              
                         IF EGM-VERIF = '0' THEN                                
                            MOVE ' ' TO EGM-CHAMP(EGM-K :1)                     
                            ADD 1 TO EGM-J EGM-K                                
                         ELSE                                                   
                            MOVE EGM-CHAMPN(EGM-J:1) TO                         
                                 EGM-CHAMP(EGM-K:1)                             
                            ADD 1 TO EGM-J EGM-K                                
                         END-IF                                                 
                      ELSE                                                      
                         MOVE EGM-CHAMPN(EGM-J:1) TO                            
                              EGM-CHAMP(EGM-K:1)                                
                         ADD 1 TO EGM-J EGM-K                                   
                      END-IF                                                    
                   ELSE                                                         
                      MOVE 'SIGNE DANS LE MASQUE INCONU ' TO ABEND-MESS         
                      PERFORM FIN-ANORMALE                                      
                   END-IF                                                       
                END-IF                                                          
              END-IF                                                            
           END-PERFORM.                                                         
      ****   VERIFICATION SIGNE DU DEBUT                                        
             PERFORM VARYING EGM-I FROM EGM-DEB-CH BY 1 UNTIL                   
                     EGM-CHAMP(EGM-I:1) NOT = SPACES OR EGM-I > 30              
             END-PERFORM.                                                       
             IF EGM-I NOT > 30 THEN                                             
                COMPUTE   EGM-N = EGM-I - 1                                     
             END-IF.                                                            
             SUBTRACT 1 FROM EGM-DEB-CH GIVING EGM-M.                           
             IF EGM-M > 0 THEN                                                  
                IF ZWM-MASQUE(1:1) = '-' THEN                                   
                   IF EGM-CHAMPR < 0 THEN                                       
                      MOVE '-' TO EGM-CHAMP(EGM-N:1)                            
                   END-IF                                                       
                ELSE                                                            
                   IF ZWM-MASQUE(1:1) = '+'                                     
                      IF EGM-CHAMPR < 0 THEN                                    
                         MOVE '-' TO EGM-CHAMP(EGM-N:1)                         
                      ELSE                                                      
                         MOVE '+' TO EGM-CHAMP(EGM-N:1)                         
                      END-IF                                                    
                   END-IF                                                       
                END-IF                                                          
             ELSE                                                               
                IF ZWM-MASQUE(1:1) = '-' OR '+'                                 
                   MOVE 'PAS LA PLACE POUR SIGNE' TO ABEND-MESS                 
                   PERFORM FIN-ANORMALE                                         
                END-IF                                                          
             END-IF.                                                            
             IF ZWM-MASQUE (1:1) = '-' OR '+'                                   
                MOVE EGM-DEB-CH TO EGM-N                                        
             ELSE                                                               
                COMPUTE EGM-N = EGM-DEB-CH + 1                                  
             END-IF.                                                            
             MOVE SPACES TO ZWM-MASQUE.                                         
             MOVE EGM-CHAMP(EGM-N:ZWM-LGMASQUE) TO                              
                 ZWM-MASQUE(1:ZWM-LGMASQUE).                                    
        FIN-TRAIT-NN. EXIT.                                             24819499
                                                                        24819599
      *---------------------------------------------------------------  24815099
       VERIFICATION-ZERO   SECTION.                                     24816099
      *-----------------------------------------------------------------24817099
                                                                        24819399
            PERFORM VARYING EGM-L FROM 1 BY 1 UNTIL EGM-L > EGM-J               
               IF EGM-CHAMPN(EGM-L:1) NOT = '0' THEN                            
                  ADD 1 TO EGM-COMPT                                            
               END-IF                                                           
            END-PERFORM.                                                        
            IF EGM-COMPT > 0 THEN                                               
               MOVE '1' TO EGM-VERIF                                            
            ELSE                                                                
               MOVE '0' TO EGM-VERIF                                            
            END-IF.                                                             
        FIN-VERIFICATION-ZERO. EXIT.                                    24819499
                                                                                
