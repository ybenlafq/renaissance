      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRAC2 TRACA.: IDREGLE AU MRQ/MAF       *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTRAC2 .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTRAC2 .                                                            
      *}                                                                        
           05  TRAC2-CTABLEG2    PIC X(15).                                     
           05  TRAC2-CTABLEG2-REDEF REDEFINES TRAC2-CTABLEG2.                   
               10  TRAC2-MARQUE          PIC X(05).                             
               10  TRAC2-FAMILLE         PIC X(05).                             
               10  TRAC2-FONCTION        PIC X(03).                             
               10  TRAC2-TCODE           PIC X(02).                             
           05  TRAC2-WTABLEG     PIC X(80).                                     
           05  TRAC2-WTABLEG-REDEF  REDEFINES TRAC2-WTABLEG.                    
               10  TRAC2-IDREGLE         PIC X(10).                             
               10  TRAC2-COMM            PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTRAC2-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTRAC2-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRAC2-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRAC2-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRAC2-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRAC2-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
