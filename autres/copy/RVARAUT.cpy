      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      ******************************************************************        
      * DCLGEN RVARAUT                                                          
      *        LANGUAGE(COBOL)                                                  
       01  RVARAUT.                                                             
           10 ARAUT-CTABLEG2       PIC X(15).                                   
           10 ARAUT-LIDENT         PIC X(10).                                   
           10 ARAUT-WTABLEG        PIC X(80).                                   
           10 ARAUT-LCOMMENT       PIC X(30).                                   
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                            
       01  RVARAUT-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ARAUT-CTABLEG2-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 ARAUT-CTABLEG2-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ARAUT-LIDENT-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 ARAUT-LIDENT-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ARAUT-WTABLEG-F      PIC S9(4) COMP.                              
      *--                                                                       
           10 ARAUT-WTABLEG-F      PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 ARAUT-LCOMMENT-F     PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 ARAUT-LCOMMENT-F     PIC S9(4) COMP-5.                            
                                                                                
      *}                                                                        
