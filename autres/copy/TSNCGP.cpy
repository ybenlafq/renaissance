      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      **************************************************************            
      * TS NCGPLUS POUR PARAMETRE/DONNEES DB2 ACCEDEES SOUVENT                  
       01 TS-NCGP.                                                              
      *    PAR EXEMPLE NOM DE LA VUE (RVGA01BM)                                 
           05 TS-NCGP-CODE   PIC X(20).                                         
      *    PAR EXEMPLE NOM DE LA COLONNE (NSOC)                                 
           05 TS-NCGP-VALEUR PIC X(20).                                         
      *    PAR EXEMPLE VALEUR RETOURNEE                                         
           05 TS-NCGP-PARAM  PIC X(80).                                         
                                                                                
