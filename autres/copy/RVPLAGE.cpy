      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PLAGE CODE PLAGE HORAIRE CHARGEMENT    *        
      *----------------------------------------------------------------*        
       01  RVPLAGE.                                                             
           05  PLAGE-CTABLEG2    PIC X(15).                                     
           05  PLAGE-CTABLEG2-REDEF REDEFINES PLAGE-CTABLEG2.                   
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  PLAGE-CPLAGE          PIC X(05).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
           05  PLAGE-WTABLEG     PIC X(80).                                     
           05  PLAGE-WTABLEG-REDEF  REDEFINES PLAGE-WTABLEG.                    
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
               10  PLAGE-LPLAGE          PIC X(20).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPLAGE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PLAGE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PLAGE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PLAGE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PLAGE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
