      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
       PROCEDURE DIVISION.                                              01530000
      *                                                                 01540000
      ***************************************************************** 01550000
      *              T R A M E   DU   P R O G R A M M E               * 01560000
      ***************************************************************** 01570000
      *                                                                 01580000
      *                                                                 01590000
       MODULE-MGV48                    SECTION.                         01600000
      *----------------------------------------                         01610000
      *                                                                 01620000
           PERFORM MODULE-ENTREE                                        01630000
 1213 *    IF PM00-WSTOCK = 'O'                                         01640000
             PERFORM MODULE-TRAITEMENT                                  01650000
 1213 *    END-IF.                                                      01660000
           PERFORM MODULE-SORTIE.                                       01670000
      *                                                                 01680000
       FIN-MODULE-MGV48. EXIT.                                          01690000
      *                                                                 01700000
      *                                                                 01710000
       MODULE-ENTREE                   SECTION.                         01720000
      *----------------------------------------                         01730000
      *                                                                 01740000
           PERFORM INIT-HANDLE.                                         01750000
           EXEC  CICS  HANDLE  CONDITION                                01770000
                       ITEMERR (ABANDON-CICS)                           01780000
                       QIDERR  (ABANDON-CICS)                           01790000
           END-EXEC.                                                    01800000
      *                                                  *              01810000
           MOVE DFHCOMMAREA            TO COMM-MGV48-APPLI.             01820000
           MOVE 'MGV48' TO NOM-PROG.                                    01830000
           INITIALIZE WS-MESSAGE.                                       01840000
1213       INITIALIZE WS-MESS-R.                                        01841000
      *1213PERFORM SELECT-RTPM00.                                       01850000
PF1   *1213PERFORM SELECT-NCGFC.                                        01860000
 "    *1213STRING COMM-MGV48-NSOCLIVR  COMM-MGV48-NDEPOT                01870000
 "    *1213DELIMITED BY SIZE INTO  WS-SOCLIEU                           01880000
 "    *1213IF (NCG48-WFLAG = 'O'                                        01890000
 "    *1213 OR (NCG48-WFLAG = 'N' AND (NCG48-COMMENT(1:6) = WS-SOCLIEU))01900000
 "    *1213 OR (NCG48-WFLAG = 'N' AND (NCG48-COMMENT(8:6) = WS-SOCLIEU))01910000
 "    *1213   )                                                         01920000
 "    *1213   SET NOUVELLE-VERSION TO TRUE                              01930000
 "    *1213ELSE                                                         01940000
 "    *1213   INITIALIZE      W-TAB-MES-A                               01950000
 "    *1213   SET ANCIENNE-VERSION TO TRUE                              01960000
PF1   *1213END-IF.                                                      01970000
      *                                                                 01980000
       FIN-MODULE-ENTREE. EXIT.                                         01990000
      *                                                                 02000000
      ***************************************************************** 02010000
      * TRAITEMENT :                                                  * 02020000
      *                                                               * 02030000
      ***************************************************************** 02040000
      *                                                                 02050000
       MODULE-TRAITEMENT               SECTION.                         02060000
      *----------------------------------------                         02070000
1213  *                                                                 02080000
 "         MOVE COMM-MGV48-DATAS     TO TAB-DATAS                       02090000
 "         PERFORM TRI-TAB-DATA-LOC                                     02100000
 "         SET AUCUN-MESSAGE-ENVOYER TO TRUE                            02110000
                                                                        02120000
 "         EVALUATE TRUE                                                02130000
 "            WHEN  COMM-MGV48-CONSULTATION                             02140000
 "                  PERFORM CONSULTATION-STOCK                          02150000
 "            WHEN  COMM-MGV48-RESERVATION                              02160000
 "                  PERFORM RESERVATION-STOCK                           02170000
 "            WHEN  COMM-MGV48-VALIDATION                               02180000
 "                  PERFORM VALIDATION-STOCK                            02190000
                                                                        02191000
 "            WHEN  COMM-MGV48-DERESERVATION                            02200000
                    IF COMM-MGV48-NVENTE <= SPACES                      02201000
 "                     PERFORM RESERVATION-STOCK                        02210000
                    END-IF                                              02211000
 "            WHEN  OTHER                                               02220000
 "                  SET  COMM-MGV48-ERR-NON-BLQ  TO TRUE                02230000
 "                  STRING 'CODE TRT ERRON� :' COMM-MGV48-DEMANDE       02240000
 "                  DELIMITED BY SIZE INTO MESSAGE-ERREUR               02250000
 "                  PERFORM ERREUR-GV48                                 02260000
1213       END-EVALUATE.                                                02270000
                                                                        02280000
1213  *                                                                 02290000
      *1213MOVE 'ISL'               TO MES-TYPE                         02300000
      *1213MOVE COMM-MGV48-NSOCLIVR TO MES-NSOCMSG                      02310000
      *1213                            MES-NSOCDST                      02320000
      *1213MOVE COMM-MGV48-NDEPOT   TO MES-NLIEUMSG                     02330000
      *1213                            MES-NLIEUDST                     02340000
      *1213MOVE SPACES              TO MES-WSID                         02350000
      *1213                            MES-USER                         02360000
      *1213MOVE 'MGV48'             TO MES-LPROG                        02370000
      *1213MOVE ZEROES              TO MES-NORD                         02380000
      *1213                            MES-CHRONO                       02390000
      *1213                            MES-NBRMSG                       02400000
      *1213MOVE SPACES              TO MES-FILLER                       02410000
                                                                        02420000
      *1213MOVE 1 TO I                                                  02430000
      *1213PERFORM UNTIL ( COMM-MGV48-E-NCODIC (I) = SPACES             02440000
      *1213         OR LOW-VALUE )  OR ( I > 40 )                       02450000
      *1213   MOVE COMM-MGV48-E-NLIGNE(I) TO MES-GV11-NLIGNE(I)         02460000
PF1   *1213                                  MES-GV11-NLIGNE-A(I)       02470000
      *1213   MOVE COMM-MGV48-E-NCODIC(I) TO MES-GV11-NCODIC(I)         02480000
PF1   *1213                                  MES-GV11-NCODIC-A(I)       02490000
      *1213   MOVE COMM-MGV48-E-QSTOCK(I) TO MES-GV11-QVENDUE(I)        02500000
PF1   *1213                                  MES-GV11-QVENDUE-A(I)      02510000
PF1   *1213   MOVE COMM-MGV48-NVENTE      TO MES-GV11-NVENTE(I)         02520000
 "    *1213   MOVE COMM-MGV48-E-NSEQNQ(I) TO MES-GV11-NSEQNQ(I)         02530000
      *1213   ADD 1 TO I                                                02540000
      *1213   SET MESSAGE-A-ENVOYER TO TRUE                             02550000
      *1213END-PERFORM                                                  02560000
PF1   *1213IF ANCIENNE-VERSION                                          02570000
 "    *1213   INITIALIZE TAB-MES-GV11                                   02580000
 "    *1213   MOVE W-TAB-MES-A TO TAB-MES-GV11                          02590000
PF1   *1213END-IF                                                       02600000
      *1213COMPUTE NB-ENVOYES = I - 1                                   02610000
      *1213IF MESSAGE-A-ENVOYER                                         02620000
      *1213  PERFORM ENVOI-MESSAGE                                      02630000
      *1213ELSE                                                         02640000
      *1213  SET ERREUR-ENTREE-VIDE  TO TRUE                            02650000
      *1213  PERFORM ERREUR-GV48                                        02660000
      *1213END-IF.                                                      02670000
      *1213IF PAS-ERREUR                                                02680000
      *1213   PERFORM RECEPTION-REPONSE                                 02690000
      *1213ELSE                                                         02700000
      *1213   PERFORM ERREUR-GV48                                       02710000
      *1213END-IF.                                                      02720000
      *                                                                 02730000
       FIN-MODULE-TRAITEMENT. EXIT.                                     02740000
      *                                                                 02740100
      *                                                                 02740200
1213   TRI-TAB-DATA-LOC               SECTION.                          02740300
                                                                        02740400
           PERFORM WITH TEST AFTER UNTIL PERMUT-NON                     02740500
              SET PERMUT-NON TO TRUE                                    02740600
              PERFORM VARYING WI FROM 1 BY 1                            02740700
                UNTIL  WI > WI-MAX - 1                                  02740800
                   OR  TAB-LIEUSTK (WI)= SPACE                          02740900
                   IF  TAB-LIEUSTK (WI) > TAB-LIEUSTK (WI + 1)          02741000
                   AND TAB-LIEUSTK (WI + 1) > (SPACE AND LOW-VALUE)     02742000
                      MOVE TAB-DATA-LOC(WI)     TO TAB-DATA-LOC(41)     02743000
                      MOVE TAB-DATA-LOC(WI + 1) TO TAB-DATA-LOC(WI)     02744000
                      MOVE TAB-DATA-LOC(41)     TO TAB-DATA-LOC(WI + 1) 02745000
                      SET PERMUT-OUI TO TRUE                            02746000
                   END-IF                                               02747000
              END-PERFORM                                               02748000
           END-PERFORM.                                                 02749000
                                                                        02750000
1213   FIN-TRI-TAB-DATA-LOC. EXIT.                                      02760000
      *                                                                 02770000
      *                                                                 02780000
1213   CONSULTATION-STOCK             SECTION.                          02790000
                                                                        02791000
           SET NE-PAS-ENVOYER      TO TRUE                              02800000
           SET PAS-ERREUR          TO TRUE                              02820000
           SET WI                  TO 1                                 02820100
           INITIALIZE              WS-NCODIC                            02820200
DE0514     SET PTF-AVEC-STKAV      TO TRUE                              02823000
                                                                        02824000
      *{ reorder-array-condition 1.1                                            
      *    PERFORM UNTIL TAB-NCODIC (WI) <= SPACES                      02830000
      *               OR WI > WI-MAX                                    02840000
      *--                                                                       
           PERFORM UNTIL WI > WI-MAX                                            
                      OR TAB-NCODIC (WI) <= SPACES                              
      *}                                                                        
DE0514                OR PTF-SANS-STKAV                                 02850000
                                                                        02860200
                MOVE SPACE  TO TAB-MES-GV11                             02860400
      *         MOVE SPACES TO COMM-MQ20-MESSAGE                        02860500
                MOVE SPACES TO WS-MESSAGE                               02860600
                               COMM-MQ20-ENTREE                         02860700
                               Z-COMMAREA-LINK                          02860800
                               COMM-MQ12-APPLI                          02860900
                MOVE 0      TO I NB-ENVOYES                             02861000
                MOVE TAB-LIEUSTK(WI) TO W-LIEUSTK                       02861300
                                                                        02861400
DE0514*         LA V�RIF DU PARAM�TRAGE STOCK AVANC� NE SE FAIT         02861500
  "   *         QUE DANS LE CAS DES LIVR�S   PLATEFORME                 02861600
  "             IF TAB-TYPESTOCK-PTF (WI)                               02862100
                   IF TAB-TYPESTOCK-PTF-L (WI)                          02862200
                      PERFORM SELECT-RTLI00                             02862300
                   END-IF                                               02862400
  "                MOVE    'P'       TO TAB-TYPESTOCK(WI)               02862500
DE0514          END-IF                                                  02862600
                                                                        02862700
                PERFORM SELECT-RTPM00                                   02862800
                                                                        02862900
                   PERFORM VARYING WI FROM WI BY 1                      02863000
      *{ reorder-array-condition 1.1                                            
      *               UNTIL TAB-NCODIC (WI) <= SPACES                   02863100
      *                  OR WI > WI-MAX                                 02863200
      *--                                                                       
                      UNTIL WI > WI-MAX                                         
                         OR TAB-NCODIC (WI) <= SPACES                           
      *}                                                                        
                         OR TAB-LIEUSTK(WI) > W-LIEUSTK                 02863300
DE0514                   OR PTF-SANS-STKAV                              02863400
                         IF PM00-WSTOCK NOT = 'N'                       02863500
                            ADD  1 TO I NB-ENVOYES                      02863600
                            MOVE TAB-NLIGNE (WI)                        02863700
                              TO MES-GV11-NLIGNE  (I)                   02863800
                            MOVE TAB-NCODIC (WI)                        02863900
                              TO MES-GV11-NCODIC  (I)                   02864000
                            MOVE TAB-QSTOCK (WI)                        02864100
                              TO MES-GV11-QVENDUE (I)                   02864200
                            MOVE COMM-MGV48-Nsociete                    02864305
                              TO MES-GV11-Nsociete(I)                   02864405
                            MOVE COMM-MGV48-Nlieu                       02864505
                              TO MES-GV11-Nlieu   (I)                   02864605
                            MOVE COMM-MGV48-NVENTE                      02864705
                              TO MES-GV11-NVENTE  (I)                   02864805
                            MOVE TAB-NSEQNQ (WI)                        02864905
                              TO MES-GV11-NSEQNQ  (I)                   02865005
                            MOVE TAB-TYPESTOCK(WI)                      02865105
                              TO MES-GV11-TYPDEM  (I)                   02865205
                         END-IF                                         02865305
                   END-PERFORM                                          02866005
                                                                        02890000
                   IF NB-ENVOYES > 0                                    02900000
                      PERFORM ENVOI-MESSAGE                             02940000
                      PERFORM RECEPTION-REPONSE                         02950000
                   END-IF                                               02951000
                                                                        02960000
                                                                        03031000
           END-PERFORM.                                                 03130100
                                                                        03131000
1213   FIN-CONSULTATION-STOCK. EXIT.                                    03260000
      *                                                                 03270000
1213   VALIDATION-STOCK             SECTION.                            03280000
                                                                        03280100
           SET NE-PAS-ENVOYER      TO TRUE                              03280200
           SET PAS-ERREUR          TO TRUE                              03280300
           SET WI                  TO 1                                 03280400
           INITIALIZE              WS-NCODIC                            03280500
                                                                        03280600
      *{ reorder-array-condition 1.1                                            
      *    PERFORM UNTIL TAB-NCODIC (WI) <= SPACES                      03280700
      *               OR WI > WI-MAX                                    03280800
      *                                                                         
      *                                                                         
      *--                                                                       
           PERFORM UNTIL WI > WI-MAX                                            
                                                                        03280900
                                                                        03281000
                      OR TAB-NCODIC (WI) <= SPACES                              
      *}                                                                        
                MOVE SPACES                    TO COMM-MQ20-MESSAGE     03281100
                INITIALIZE                     MESS-DETAILLE            03281200
                MOVE 0                         TO I NB-ENVOYES          03281400
                SET MDQRPF-CHG-RESA-VENTE-PF   TO TRUE                  03281500
                MOVE COMM-MGV48-NSOCIETE       TO MESS-Q-NSOCVTE        03281600
                MOVE COMM-MGV48-NLIEU          TO MESS-Q-NLIEUVTE       03281700
                MOVE COMM-MGV48-NVENTE         TO MESS-Q-NVENTE         03281800
                MOVE COMM-MGV48-NUMRES         TO MESS-Q-NUMRES         03281900
                                                                        03282000
                MOVE TAB-LIEUSTK(WI)           TO W-LIEUSTK             03282100
                                                                        03282200
                PERFORM VARYING WI FROM WI BY 1                         03282300
      *{ reorder-array-condition 1.1                                            
      *           UNTIL TAB-NCODIC (WI) <= SPACES                       03282400
      *              OR WI > WI-MAX                                     03282500
      *--                                                                       
                  UNTIL WI > WI-MAX                                             
                     OR TAB-NCODIC (WI) <= SPACES                               
      *}                                                                        
                     OR TAB-LIEUSTK(WI) > W-LIEUSTK                     03282600
                        ADD  1 TO I NB-ENVOYES                          03282700
                        MOVE TAB-NCODIC (WI)                            03282800
                          TO MESS-Q-NCODIC    (I)                       03282900
                        MOVE TAB-QSTOCK (WI)                            03283000
                          TO MESS-Q-QTE       (I)                       03283100
                        MOVE TAB-NSEQNQ (WI)                            03283300
                          TO MESS-Q-NSEQNQ    (I)                       03283500
                        MOVE TAB-NSEQNQ-ORIG (WI)                       03283600
                          TO MESS-Q-NSEQNQ-ORIG    (I)                  03283700
                END-PERFORM                                             03283800
                                                                        03283900
                PERFORM ENVOI-MESSAGE-STOCK                             03284000
                                                                        03284200
           END-PERFORM.                                                 03284300
                                                                        03285000
1213   FIN-VALIDATION-STOCK. EXIT.                                      03680000
      *                                                                 03690000
1213   RESERVATION-STOCK              SECTION.                          03700000
           INITIALIZE WS-NCODIC                                         03700100
           INITIALIZE MESS-DETAILLE                                     03701100
           MOVE TAB-LIEUSTK(1) TO W-LIEUSTK                             03701200
                                                                        03701300
           PERFORM SELECT-RTPM00                                        03702000
                                                                        03702100
           IF PM00-WSTOCK NOT ='N'                                      03703000
              MOVE SPACES              TO COMM-MQ20-MESSAGE             03720000
              INITIALIZE                  TAB-MES-RESA                  03721000
              MOVE COMM-MGV48-NSOCIETE TO MESS-Q-NSOCVTE                03730000
              MOVE COMM-MGV48-NLIEU    TO MESS-Q-NLIEUVTE               03740000
              MOVE COMM-MGV48-NVENTE   TO MESS-Q-NVENTE                 03750000
              MOVE COMM-MGV48-NUMRES   TO MESS-Q-NUMRES                 03751000
      *  CREATION DE RESERVATION                                        03760000
              IF COMM-MGV48-DERESERVATION                               03760100
                 SET  MDQRPF-SUPP-RESA-VENTE-PF TO TRUE                 03761000
              ELSE                                                      03762000
                 IF COMM-MGV48-NVENTE  > SPACES                         03763000
                    SET MDQRPF-DEC-RESA-VENTE-PF  TO TRUE               03763100
                 ELSE                                                   03764000
                    SET MDQRPF-CRE-RESA-VENTE-PF  TO TRUE               03770000
                 END-IF                                                 03771000
              END-IF                                                    03780000
              MOVE TAB-NCODIC(1)       TO MESS-Q-NCODIC(1)              03810000
              MOVE TAB-QSTOCK(1)       TO MESS-Q-QTE   (1)              03820000
              MOVE TAB-NSEQNQ(1)       TO MESS-Q-NSEQNQ(1)              03830000
              MOVE 1                   TO NB-ENVOYES                    03840000
              PERFORM ENVOI-MESSAGE-STOCK                               03850000
      * REPONSE QUE DANS LE CAS D'UNE R�SERVATION                       03850100
              IF COMM-MGV48-RESERVATION                                 03851000
                 PERFORM RECEPTION-REPONSE-STOCK                        03860000
      * EN CAS DE PBM DE CR�ATION DE R�SERVATION=> IL FAUT D�RESERVER   03861000
                 IF  TOP-ERREUR >= '06'                                 03862000
                 AND TOP-ERREUR <= '12'                                 03862100
                     SET COMM-MGV48-DERESERVATION   TO TRUE             03862300
                     PERFORM RESERVATION-STOCK                          03862400
                 END-IF                                                 03863000
              END-IF                                                    03864000
           END-IF.                                                      03870000
1213   FIN-RESERVATION-STOCK. EXIT.                                     03880000
      *                                                                 03890000
      *                                                                 04090000
       ENVOI-MESSAGE                  SECTION.                          04100000
      *---------------------------------------                          04110000
      *   ENVOI DANS LE MAG OU ON EMPORTE LE PRODUIT                    04120000
 1213      MOVE 'ISL'               TO MES-TYPE                         04130000
   "       MOVE W-NSOCDEPOT         TO MES-NSOCMSG                      04140000
   "                                   MES-NSOCDST                      04150000
   "       MOVE W-NLIEUDEPOT        TO MES-NLIEUMSG                     04160000
   "                                   MES-NLIEUDST                     04170000
   "       MOVE SPACES              TO MES-WSID                         04180000
   "                                   MES-USER                         04190000
   "       MOVE 'MGV48'             TO MES-LPROG                        04200000
   "       MOVE ZEROES              TO MES-NORD                         04210000
   "                                   MES-CHRONO                       04220000
   "                                   MES-NBRMSG                       04230000
   "       MOVE SPACES              TO MES-FILLER                       04240000
 1213 *    MOVE  COMM-MGV48-NSOCLIVR TO COMM-MQ20-NSOC                  04250000
 1213      MOVE  W-NSOCDEPOT         TO COMM-MQ20-NSOC                  04260000
                                        COMM-MQ12-NSOC                  04270000
 1213 *    MOVE  COMM-MGV48-NDEPOT   TO COMM-MQ20-NLIEU                 04280000
 1213      MOVE  W-NLIEUDEPOT        TO COMM-MQ20-NLIEU                 04290000
AD1                                     COMM-MQ12-NLIEU                 04300000
           MOVE  'ISL'               TO COMM-MQ20-FONCTION              04310000
                                        COMM-MQ12-FONCTION              04320000
           MOVE  'MGV48'             TO COMM-MQ20-NOMPROG               04330000
                                        COMM-MQ12-NOMPROG               04340000
           MOVE  LONGUEUR-MESSAGE    TO COMM-MQ20-LONGMES               04350000
1213       SET MESSAGE-ENVOYER       TO TRUE                            04360000
PF1   *1213IF ANCIENNE-VERSION                                          04370000
      *1213   MOVE LONGUEUR-MESSAGE-A  TO COMM-MQ20-LONGMES             04380000
PF1   *1213END-IF                                                       04390000
           MOVE  '00'                TO COMM-MQ20-CODRET                04400000
           MOVE  SPACES              TO COMM-MQ20-CORRELID              04410000
                                        COMM-MQ20-MSGID                 04420000
           MOVE LENGTH OF COMM-MQ20-APPLI TO LONG-COMMAREA-LINK         04430000
           MOVE COMM-MQ20-APPLI      TO Z-COMMAREA-LINK                 04440000
           MOVE 'MMQ20'              TO NOM-PROG-LINK                   04450000
           PERFORM LINK-PROG                                            04460000
           MOVE Z-COMMAREA-LINK      TO COMM-MQ20-APPLI                 04470000
      *                                                                 04480000
           MOVE COMM-MQ20-CORRELID   TO COMM-MQ12-MSGID                 04490000
      *                                                                 04500000
           IF COMM-MQ20-CODRET NOT = '00'                               04510000
              MOVE COMM-MQ20-CODRET  TO TOP-ERREUR                      04520000
              MOVE COMM-MQ20-ERREUR  TO MESSAGE-ERREUR                  04520200
      *       PERFORM COMM-GV48-ERREUR                                  04520300
              PERFORM ERREUR-GV48                                       04521000
           END-IF.                                                      04530000
      *                                                                 04540000
       FIN-ENVOI-MESSAGE. EXIT.                                         04550000
      *                                                                 04560000
      *                                                                 04570000
      *                                                                 04570100
       RECEPTION-REPONSE    SECTION.                                    04570200
      *                                            *                    04570300
      *                                            *                    04570400
               MOVE LENGTH OF COMM-MQ12-ENTREE TO LONG-COMMAREA-LINK.   04570500
               MOVE COMM-MQ12-ENTREE        TO Z-COMMAREA-LINK.         04570600
               MOVE 'MMQ12'                TO NOM-PROG-LINK.            04570700
               PERFORM LINK-PROG.                                       04570800
               MOVE Z-COMMAREA-LINK TO COMM-MQ12-APPLI                  04570900
                                                                        04571000
               IF COMM-MQ12-CODRET NOT = '00'                           04572000
                  MOVE COMM-MQ12-CODRET  TO TOP-ERREUR                  04573000
                  MOVE COMM-MQ12-MSGID   TO MESSAGE-ERREUR              04573100
      *           PERFORM COMM-GV48-ERREUR                              04573200
                  PERFORM ERREUR-GV48                                   04574000
               ELSE                                                     04575000
                                                                        04575100
               MOVE 1 TO J                                              04576000
               PERFORM VARYING I FROM 1 BY 1                            04577000
                 UNTIL I > NB-ENVOYES                                   04577100
                    OR MESR-NCODIC(I) = SPACES                          04578000
                                                                        04578100
                 PERFORM VARYING K FROM 1 BY 1                          04579000
                     UNTIL K > NB-ENVOYES                               04579100
                        OR (COMM-MGV48-NCODIC(K) = MESR-NCODIC(I)       04580000
                       AND COMM-MGV48-NLIGNE(K) = MESR-NLIGNE(I))       04590000
                 END-PERFORM                                            04600000
                                                                        04601000
                 COMPUTE  MESR-QSTOCK(I) = MESR-QSTOCK(I) * 1           04610000
                                                                        04611000
                 IF MESR-QSTOCK(I) < COMM-MGV48-QSTOCK (K)              04620000
                                                                        04621000
      *             MOVE COMM-MGV48-NLIGNE(K)                           04622000
      *               TO COMM-MGV48-NLIGNE-ERR                          04623000
                    MOVE MESR-NLIGNE(I)                                 04623100
                      TO COMM-MGV48-NLIGNE-ERR                          04623200
                                                                        04623300
                    MOVE K TO COMM-MGV48-INDICE-ERR                     04623400
                                                                        04624000
                    MOVE MESR-QSTOCK-LOCAL(I) TO COMM-MGV48-DATA-LOC(J) 04630000
                    ADD 1               TO J                            04640000
                    MOVE '1'            TO COMM-MGV48-CRET(K)           04642000
                    SET ERREUR-STOCK    TO TRUE                         04650000
                    MOVE MESR-NCODIC(I) TO WS-NCODIC                    04651000
                    PERFORM ERREUR-GV48                                 04660000
                                                                        04661000
                 ELSE                                                   04670000
                                                                        04671000
                   IF MESR-QSTOCK(I) = COMM-MGV48-QSTOCK(K)             04680000
      *               MOVE COMM-MGV48-NLIGNE(K)                         04701000
      *                 TO COMM-MGV48-NLIGNE-ERR                        04702000
                      MOVE MESR-NLIGNE(I)                               04702100
                        TO COMM-MGV48-NLIGNE-ERR                        04702200
                      MOVE K TO COMM-MGV48-INDICE-ERR                   04703000
                      SET ERREUR-EXPO TO TRUE                           04710000
                      PERFORM ERREUR-GV48                               04720000
                   END-IF                                               04730000
                                                                        04731000
                 END-IF                                                 04740000
               END-PERFORM                                              04750000
                                                                        04760000
               END-IF.                                                  04760100
                                                                        04760200
       FIN-RECEPTION-REPONSE. EXIT.                                     04760300
      *                                                                 04760400
      *                                                                 04760500
1213   ENVOI-MESSAGE-STOCK            SECTION.                          04760600
      *---------------------------------------                          04760700
      *   ENVOI DANS LE MAG OU ON EMPORTE LE PRODUIT                    04760800
           MOVE 'RPF'               TO MESS-TYPE                        04760900
           MOVE W-NSOCDEPOT         TO MESS-NSOCMSG                     04761000
                                       MESS-NSOCDST                     04762000
           MOVE W-NLIEUDEPOT        TO MESS-NLIEUMSG                    04763000
                                       MESS-NLIEUDST                    04764000
           MOVE SPACES              TO MESS-WSID                        04765000
                                       MESS-USER                        04766000
           MOVE 'MGV48'             TO MESS-LPROG                       04767000
           MOVE ZEROES              TO MESS-NORD                        04768000
                                       MESS-CHRONO                      04769000
                                       MESS-NBRMSG                      04770000
           MOVE SPACES              TO MESS-FILLER                      04780000
           MOVE  W-NSOCDEPOT        TO COMM-MQ20-NSOC                   04790000
                                       COMM-MQ12-NSOC                   04800000
           MOVE  W-NLIEUDEPOT       TO COMM-MQ20-NLIEU                  04810000
                                       COMM-MQ12-NLIEU                  04820000
           MOVE  'RPF'              TO COMM-MQ20-FONCTION               04830000
                                       COMM-MQ12-FONCTION               04840000
           MOVE  'MGV48'            TO COMM-MQ20-NOMPROG                04850000
                                       COMM-MQ12-NOMPROG                04860000
           MOVE  LONGUEUR-MES-RESA  TO COMM-MQ20-LONGMES                04870000
           MOVE  '00'               TO COMM-MQ20-CODRET                 04880000
           MOVE  SPACES             TO COMM-MQ20-CORRELID               04890000
                                       COMM-MQ20-MSGID                  04900000
           MOVE LENGTH OF COMM-MQ20-APPLI TO LONG-COMMAREA-LINK         04910000
           MOVE COMM-MQ20-APPLI      TO Z-COMMAREA-LINK                 04920000
           MOVE 'MMQ20'              TO NOM-PROG-LINK                   04930000
           PERFORM LINK-PROG                                            04940000
           MOVE Z-COMMAREA-LINK      TO COMM-MQ20-APPLI                 04950000
      *                                                                 04960000
           MOVE COMM-MQ20-CORRELID   TO COMM-MQ12-MSGID                 04970000
      *                                                                 04980000
           IF COMM-MQ20-CODRET NOT = '00'                               04990000
              MOVE COMM-MQ20-CODRET  TO TOP-ERREUR                      05001000
              MOVE COMM-MQ20-ERREUR  TO MESSAGE-ERREUR                  05002000
      *       PERFORM COMM-GV48-ERREUR                                  05003000
              PERFORM ERREUR-GV48                                       05004000
           END-IF.                                                      05010000
      *                                                                 05020000
 1213  FIN-ENVOI-MESSAGE-STOCK. EXIT.                                   05030000
      *                                                                 05030100
 1213  RECEPTION-REPONSE-STOCK    SECTION.                              05030200
           MOVE LENGTH OF COMM-MQ12-ENTREE TO LONG-COMMAREA-LINK.       05030300
           MOVE COMM-MQ12-ENTREE           TO Z-COMMAREA-LINK.          05030400
           MOVE 'MMQ12'                    TO NOM-PROG-LINK.            05030500
           PERFORM LINK-PROG.                                           05030600
           MOVE Z-COMMAREA-LINK            TO COMM-MQ12-APPLI           05030700
                                                                        05030800
           IF COMM-MQ12-CODRET NOT = '00'                               05030900
              MOVE COMM-MQ12-CODRET        TO TOP-ERREUR                05031000
              MOVE COMM-MQ12-MSGID         TO MESSAGE-ERREUR            05031100
              PERFORM ERREUR-GV48                                       05032000
                                                                        05033000
           ELSE                                                         05034000
                                                                        05035000
              MOVE 1 TO J                                               05036000
              PERFORM VARYING I FROM 1 BY 1 UNTIL I > NB-ENVOYES        05037000
                      OR MESR-NCODIC(I) = SPACES                        05038000
                                                                        05038100
                 PERFORM VARYING K FROM 1 BY 1 UNTIL K > NB-ENVOYES     05039000
                         OR ( COMM-MGV48-NCODIC(K) = MESS-R-NCODIC(I)   05040000
                         AND  COMM-MGV48-NLIGNE(K) = MESS-R-NSEQNQ(I) ) 05050000
                 END-PERFORM                                            05060000
      * R�servation VP bien cr��e                                       05061000
      * --- pour l'instant je ne comprends pas ce qui est renvoy�       05062000
      *     en cas d'erreur...                                          05063000
      *          IF  MESS-R-STATUT-KO > SPACE                           05071000
                 if  MESS-R-ERRMSG (i) not = 'R�servation VP bien cr��e'05072000
                    ADD 1                 TO J                          05080000
      *                                                                 05081000
                    MOVE '1'              TO COMM-MGV48-CRET(K)         05082000
                    MOVE MESS-R-ERRMSG(I) TO MESSAGE-ERREUR             05090000
                    SET ERREUR-STOCK      TO TRUE                       05100000
                    MOVE MESS-R-NCODIC(I) TO WS-NCODIC                  05101000
                 END-IF                                                 05120000
               END-PERFORM                                              05130000
               PERFORM ERREUR-GV48                                      05131000
                                                                        05140000
               END-IF.                                                  05150000
                                                                        05150100
 1213  FIN-RECEPTION-REPONSE-STOCK. EXIT.                               05150200
                                                                        05150300
       RECEPTION-REPONSE-MMQ12 SECTION.                                 05150400
           MOVE LENGTH OF COMM-MQ12-ENTREE TO LONG-COMMAREA-LINK.       05150500
           MOVE COMM-MQ12-ENTREE        TO Z-COMMAREA-LINK.             05150600
           MOVE 'MMQ12'                TO NOM-PROG-LINK.                05150700
           PERFORM LINK-PROG.                                           05150800
           MOVE Z-COMMAREA-LINK TO COMM-MQ12-APPLI                      05150900
                                                                        05151000
           IF COMM-MQ12-CODRET NOT = '00'                               05151100
              MOVE COMM-MQ12-CODRET  TO TOP-ERREUR                      05151200
              MOVE COMM-MQ12-MSGID   TO MESSAGE-ERREUR                  05151300
      *       PERFORM COMM-GV48-ERREUR                                  05151400
              PERFORM ERREUR-GV48                                       05151500
           END-IF.                                                      05151600
       F-RECEPTION-REPONSE-MMQ12. EXIT.                                 05151700
                                                                        05151800
      * -> C'EST ICI QUE L'ON VA TRAITER L'ENSEMBLE DES LIGNES          05151900
      *    EMPORT� PLATEFORME SUITE � UN ENCAISSEMENT.                  05152000
      *   ON A TENT� DE TERMINER LA R�SERVATION.                        05152100
      * -> LORS DE LA VALIDATION SI TOUT EST OK POUR LA LIGNE DE PRODUIT05152200
      *    ON TRANSFORME LE MODE DE D�LIVRANCE DE EP � EMR.             05152300
       TRAIT-REPONSE-VALIDATION SECTION.                                05152400
           MOVE FUNC-UPDATE            TO TRACE-SQL-FUNCTION.           05152500
           MOVE 'EMR'                  TO GV11-CMODDEL                  05152600
           MOVE COMM-MGV48-NSOCIETE    TO GV11-NSOCIETE                 05152700
           MOVE COMM-MGV48-NLIEU       TO GV11-NLIEU                    05152800
           MOVE COMM-MGV48-NVENTE      TO GV11-NVENTE                   05152900
           MOVE W-NSOCDEPOT            TO GV11-NSOCLIVR                 05153000
           MOVE W-NLIEUDEPOT           TO GV11-NDEPOT                   05153100
           PERFORM CHARGEMENT-DSYST                                     05153800
           MOVE DATHEUR                TO GV11-DSYST                    05153900
      *                                                                 05154000
           PERFORM VARYING K FROM 1 BY 1                                05154100
                     UNTIL K > WI-MAX                                   05154200
                        OR K > NB-ENVOYES                               05154300
               IF  MESS-R-NCODIC(K) NOT = (SPACE OR LOW-VALUE)          05154400
               AND MESS-R-NSEQNQ(K) > 0                                 05154500
               AND MESS-R-ERRMSG(K) = SPACE                             05154600
                   MOVE MESS-R-NSEQNQ(K) TO GV11-NSEQNQ                 05154700
                   EXEC SQL UPDATE  RVGV1106                            05154800
                        SET                                             05154900
                        CMODDEL   = :GV11-CMODDEL ,                     05155000
                        DSYST     = :GV11-DSYST                         05155100
                        WHERE                                           05155200
                              NSOCIETE  = :GV11-NSOCIETE                05155300
                        AND   NLIEU     = :GV11-NLIEU                   05155400
                        AND   NVENTE    = :GV11-NVENTE                  05155500
                        AND   NSEQNQ    = :GV11-NSEQNQ                  05155600
                        AND   CTYPENREG = '1'                           05155700
                        AND   CENREG    = ' '                           05155800
                        AND   NSOCLIVR  = :GV11-NSOCLIVR                05155900
                        AND   NDEPOT    = :GV11-NDEPOT                  05156000
                    END-EXEC                                            05156100
                    PERFORM TEST-CODE-RETOUR-SQL                        05156200
               END-IF                                                   05156300
           END-PERFORM.                                                 05156400
       F-TRAIT-REP-VAL. EXIT.                                           05156500
      *                                                                 05156600
       CHARGEMENT-DSYST SECTION.                                        05156700
           CALL 'SPDATDB2' USING WRET DATHEUR                           05156800
           IF WRET  NOT = ZEROES                                        05156900
              MOVE  WRET             TO WRET-PIC                        05157000
              MOVE  W-MESS-SPDATDB2  TO MESSAGE-ERREUR                  05157100
              PERFORM ERREUR-GV48                                       05157200
           END-IF.                                                      05157400
       F-CHARGEMENT-DSYST. EXIT.                                        05157500
                                                                        05157600
       TEST-CODE-RETOUR-SQL           SECTION.                          05158400
      *---------------------------------------                          05158500
                                                                        05158600
            MOVE '0'                  TO CODE-RETOUR                    05158700
            MOVE SQLCODE              TO SQL-CODE                       05158800
            MOVE SQLCODE              TO TRACE-SQL-CODE                 05158900
            IF CODE-DECLARE                                             05159000
               MOVE 0                    TO SQL-CODE                    05159100
            END-IF                                                      05159200
            IF CODE-SELECT OR CODE-FETCH                                05160000
               IF SQL-CODE = +100                                       05170000
                  MOVE '1'                  TO CODE-RETOUR              05180000
               END-IF                                                   05190000
            END-IF                                                      05200000
            IF SQL-CODE < 0                                             05210000
               SET  COMM-MGV48-ERR-BLQ TO TRUE                          05220000
               STRING TRACE-SQL-MESSAGE(1:22) TRACE-SQL-MESSAGE(33:31)  05230000
                     '     '                                            05240000
                      DELIMITED BY SIZE INTO COMM-MGV48-LIBERR          05250000
               END-STRING                                               05260000
               PERFORM MODULE-SORTIE                                    05270000
            END-IF.                                                     05280000
                                                                        05290000
       FIN-TEST-CODE-RETOUR-SQL. EXIT.                                  05300000
      *                                                                 05310000
      *                                                                 05320000
      * ------------------------------------------------------------- * 05330000
       MODULE-SORTIE SECTION.                                           05340000
      * ------------------------------------------------------------- * 05350000
      *                                                                 05360000
      **** PASSAGE DE LA COMMAREA - RETOUR                              05370000
           MOVE COMM-MGV48-APPLI            TO DFHCOMMAREA.             05380000
           EXEC CICS RETURN                                             05390000
           END-EXEC.                                                    05400000
                                                                        05410000
      *                                                                 05420000
       FIN-MODULE-SORTIE. EXIT.                                         05430000
                                                                        05520000
      * ------------------------------------------------------------- * 05593000
      * ------------------------------------------------------------- * 05600000
       ERREUR-GV48         SECTION.                                     05610000
      * ------------------------------------------------------------- * 05620000
      *                                                                 05630000
              STRING '00' TOP-ERREUR DELIMITED BY SIZE                  05640000
                           INTO GA99-NSEQERR                            05650000
              PERFORM MLIBERRGEN                                        05660000
      *       INSPECT MESSAGE-ERREUR  CONVERTING '~$�*%@#'              05661000
      *                           TO WS-NCODIC                          05663000
              MOVE MESSAGE-ERREUR     TO COMM-MGV48-LIBERR              05670000
              EVALUATE TOP-ERREUR                                       05680000
                WHEN '06'                                               05720000
                 SET  COMM-MGV48-ERR-BLQ  TO TRUE                       05720100
                WHEN '07'                                               05721000
                 SET  COMM-MGV48-ERR-BLQ  TO TRUE                       05721100
                WHEN '11'                                               05721200
                 SET  COMM-MGV48-ERR-BLQ  TO TRUE                       05722000
                WHEN '12'                                               05730000
                 SET  COMM-MGV48-ERR-STO  TO TRUE                       05740000
                WHEN '13'                                               05750000
                 SET  COMM-MGV48-ERR-NON-BLQ  TO TRUE                   05760000
DE0514          WHEN '14'                                               05761000
  "              SET  COMM-MGV48-ERR-BLQ  TO TRUE                       05762000
                WHEN OTHER                                              05770000
                     IF EIBTRNID NOT = 'GV32'                           05771000
                        SET  COMM-MGV48-OK      TO TRUE                 05780000
                     ELSE                                               05780100
                        SET  COMM-MGV48-ERR-BLQ TO TRUE                 05780200
                     END-IF                                             05781000
              END-EVALUATE.                                             05790000
      *                                                                 05800000
       FIN-ERREUR-GV48-BLQ.     EXIT.                                   05810000
                                                                        05820000
       MLIBERRGEN                     SECTION.                          05830000
      *---------------------------------------                          05840000
      *                                                                 05850000
           MOVE CODE-RETOUR           TO W-CODE-RETOUR-MLIBERRG         05860000
           MOVE NOM-PROG              TO GA99-CNOMPGRM                  05870000
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION             05880000
           MOVE SPACES TO MESSAGE-ERREUR                                05881000
           PERFORM CLEF-GA9900.                                         05890000
           EXEC SQL SELECT   CERRUT,                                    05900000
                             LIBERR                                     05910000
                    INTO    :GA99-CERRUT,                               05920000
                            :GA99-LIBERR                                05930000
                    FROM     RVGA9900                                   05940000
                    WHERE    CNOMPGRM = :GA99-CNOMPGRM                  05950000
                    AND      NSEQERR = :GA99-NSEQERR                    05960000
           END-EXEC.                                                    05970000
           PERFORM TEST-CODE-RETOUR-SQL.                                05980000
           IF NON-TROUVE                                                05990000
           THEN MOVE 'MESSAGE NON REFERENCE' TO LIBERR-MESSAGE          06000000
           ELSE MOVE GA99-CERRUT TO ERREUR-UTILISATEUR                  06010000
               MOVE GA99-LIBERR TO LIBERR-MESSAGE                       06020000
           END-IF.                                                      06030000
           IF MESSAGE-VARIABLE NOT = SPACES                             06040000
              MOVE MESSAGE-VARIABLE TO LIBERR-MESSAGE                   06050000
           END-IF.                                                      06060000
      *                                                                 06070000
       FIN-MLIBERRGEN. EXIT.                                            06080000
                                                                        06090000
       SELECT-RTPM00                  SECTION.                          06100000
      *---------------------------------------                          06110000
      *                                                                 06120000
 1213 *    MOVE COMM-MGV48-NSOCDEPOT  TO PM00-NSOC                      06130000
 1213 *    MOVE COMM-MGV48-NLIEUDEPOT TO PM00-NLIEU                     06140000
           MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION             06150000
           MOVE W-NSOCDEPOT           TO PM00-NSOC                      06151000
           MOVE W-NLIEUDEPOT          TO PM00-NLIEU                     06152000
                                                                        06160000
           EXEC SQL SELECT   WSTOCK                                     06170000
                    INTO    :PM00-WSTOCK                                06180000
                    FROM     RVPM0000                                   06190000
                    WHERE    NSOC     = :PM00-NSOC                      06200000
                    AND      NLIEU    = :PM00-NLIEU                     06210000
           END-EXEC.                                                    06220000
           PERFORM TEST-CODE-RETOUR-SQL.                                06230000
           IF NON-TROUVE                                                06240000
                MOVE 'N' TO PM00-WSTOCK                                 06250000
           END-IF.                                                      06300000
      *                                                                 06310000
       FIN-SELECT-RTPM00. EXIT.                                         06320000
                                                                        06330000
PF1      SELECT-NCGFC                   SECTION.                        06340000
 "    *---------------------------------------                          06350000
 "    *                                                                 06360000
 "    * NOUVELLE LIGNE NCGFC POUR G�RER NOUVELLE/ANCIENNE VERSION.      06370000
 "    *                                                                 06380000
 "          MOVE 'MGV48'   TO NCGFC-CPROG GA01-CTABLEG2                 06390000
 "          MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION            06400000
 "          EXEC SQL SELECT   WTABLEG                                   06410000
 "                   INTO    :NCGFC-WTABLEG                             06420000
 "                   FROM     RVGA0100                                  06430000
 "                   WHERE    CTABLEG1 = 'NCGFC'                        06440000
                     AND      CTABLEG2 = :GA01-CTABLEG2                 06450000
 "          END-EXEC.                                                   06460000
 "          PERFORM TEST-CODE-RETOUR-SQL.                               06470000
 "          IF NON-TROUVE                                               06480000
 "               MOVE 'N' TO NCG48-WFLAG                                06490000
 "               MOVE SPACES TO NCG48-COMMENT                           06500000
             ELSE                                                       06510000
                 MOVE NCGFC-WFLAG TO NCG48-WFLAG                        06520000
                 MOVE NCGFC-COMMENT TO NCG48-COMMENT                    06530000
 "           END-IF.                                                    06540000
 "    *                                                                 06550000
PF1      FIN-SELECT-NCGFC. EXIT.                                        06560000
                                                                        06561000
DE0514 SELECT-RTLI00      SECTION.                                      06570000
      *---------------------------*                                     06580000
           MOVE FUNC-SELECT        TO TRACE-SQL-FUNCTION                06600000
           MOVE 'RVLI0000'         TO TABLE-NAME                        06610000
           MOVE W-NSOCDEPOT        TO LI00-NSOCIETE                     06620000
           MOVE W-NLIEUDEPOT       TO LI00-NLIEU                        06621000
           EXEC SQL                                                     06630000
                SELECT   A.NSOCIETE, A.NLIEU, A.LVPARAM                 06640000
                INTO     :LI00-NSOCIETE,                                06650000
                         :LI00-NLIEU,                                   06651000
                         :LI00-LVPARAM                                  06660000
                FROM     RVLI0000 A                                     06670000
                INNER JOIN RVLI0000 B                                   06680000
                     ON A.NSOCIETE  = B.NSOCIETE                        06690000
                    AND A.NLIEU     = B.NLIEU                           06700000
                    AND A.CPARAM    = 'PTF3'                            06710000
                    AND B.CPARAM    = 'STKAV'                           06720000
                    AND SUBSTR(A.LVPARAM, 1, 1) = 'O'                   06740000
                    AND B.LVPARAM LIKE                                  06740100
                    '______LIV=O_________'                              06741000
                 WHERE A.NSOCIETE  = :LI00-NSOCIETE                     06750000
                 AND   A.NLIEU     = :LI00-NLIEU                        06751000
           END-EXEC.                                                    06760000
                                                                        06830000
           PERFORM TEST-CODE-RETOUR-SQL.                                06840000
                                                                        06841000
           SET PTF-AVEC-STKAV        TO TRUE                            06842000
           IF NON-TROUVE                                                06850000
              SET PTF-SANS-STKAV     TO TRUE                            06850100
              MOVE '14'              TO TOP-ERREUR                      06850200
              MOVE 'PTF NON PARAMETR� DANS LI00/STKAV' TO MESSAGE-ERREUR06850300
              PERFORM ERREUR-GV48                                       06850400
           END-IF.                                                      06854000
DE0514 FIN-SELECT-RTLI00. EXIT.                                         06860000
                                                                                
