      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                               *         
       01  TS-IDSE.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-IDSE-LONG                PIC S9(03) COMP-3                     
                                          VALUE +165.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-IDSE-DONNEES.                                                  
              03 TS-IDSE-LENGTH           PIC 9(03).                            
              03 TS-IDSE-VALUE            PIC X(180).                           
              03 TS-IDSE-NBVAR            PIC 9(02).                            
      *                                                                         
                                                                                
