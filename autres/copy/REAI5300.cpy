      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      **************************************************************    00005000
      * DCLGEN DE LA TABLE FEADTL ETENDUE POUR UTILISATION NCG+         00005050
      **************************************************************    00105000
          01 REAI5300.                                                  00690000
              05 EAI53-FSSKU PIC 9(9).                                          
              05 EAI53-FSCTL PIC 9(5).                                          
              05 EAI53-FSCLAS PIC 9(3).                                         
              05 EAI53-FSFNUM PIC 9(5).                                         
              05 EAI53-FSSTCN PIC 9.                                            
              05 EAI53-FSSTDT PIC 9(6).                                         
              05 EAI53-FSENCN PIC 9.                                            
              05 EAI53-FSENDT PIC 9(6).                                         
              05 EAI53-FSLWLM PIC X(12).                                        
              05 EAI53-FSUPLM PIC X(12).                                        
              05 EAI53-FSUOMN PIC 9(5).                                         
              05 EAI53-FSWEBP PIC 9.                                            
              05 EAI53-FSVAL PIC 9(8).                                          
      * INDICATEURS NULLITE                                             00005050
          01 REAI5300-FLAGS.                                            00690000
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSSKU-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 EAI53-FSSKU-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSCTL-F PIC S9(4) COMP.                                  
      *--                                                                       
              05 EAI53-FSCTL-F PIC S9(4) COMP-5.                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSCLAS-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSCLAS-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSFNUM-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSFNUM-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSSTCN-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSSTCN-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSSTDT-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSSTDT-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSENCN-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSENCN-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSENDT-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSENDT-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSLWLM-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSLWLM-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSUPLM-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSUPLM-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSUOMN-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSUOMN-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSWEBP-F PIC S9(4) COMP.                                 
      *--                                                                       
              05 EAI53-FSWEBP-F PIC S9(4) COMP-5.                               
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 EAI53-FSVAL-F PIC S9(4) COMP.                                  
      *                                                                         
      *--                                                                       
              05 EAI53-FSVAL-F PIC S9(4) COMP-5.                                
                                                                                
      *}                                                                        
