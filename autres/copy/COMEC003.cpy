      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *                                                                *        
      *  PROJET     : ECOMMERCE                                        *        
      *  PGM        : MEC003 (RENVOI Prix pour codic/soc/lieu/date     *        
      *                                                                *        
      *                                                                *        
      ******************************************************************        
      *                                                                         
       01  COMM-EC003.                                                          
      *                                                                         
      *    CHAMPS PASSES POUR EFFECTUER LA DEMANDE                              
      *-----------------------------------------------------------------        
      *                                                                         
           02 COMM-EC003-NSOC           PIC  X(003).                            
           02 COMM-EC003-NLIEU          PIC  X(003).                            
           02 COMM-EC003-NCODIC         PIC  X(007).                            
           02 COMM-EC003-DATE           PIC  X(008).                            
      *                                                                         
      *    Zones de r�ponse                                                     
      *-----------------------------------------------------------------        
           02 COMM-EC003-CODE-RETOUR    PIC  9(01).                             
            88 COMM-EC003-OK            VALUE   0.                              
            88 COMM-EC003-KO            VALUE   1.                              
           02 COMM-EC003-PXVTTC         PIC S9(07)V99 COMP-3.                   
           02 COMM-EC003-LIBERR         PIC  X(50).                             
      *                                                                         
      *    Zones de travail                                                     
           02 COMM-EC003-NZONPRIX       PIC  X(02).                             
           02 COMM-EC003-NSOC-P         PIC  X(003).                            
           02 COMM-EC003-NLIEU-P        PIC  X(003).                            
      *                                                                         
                                                                                
