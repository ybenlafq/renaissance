      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * COMM DU MGV68: ENVOI MSG ENCAISSEMENT � LA DACEM                        
      ******************************************************************        
      *--> DONNEES FOURNIES PAR LE PROGRAMME APPELANT.                          
           05  MGV68C-ENTREE.                                                   
               10  MGV68C-NSOCIETE        PIC  XXX.                             
               10  MGV68C-NLIEU           PIC  XXX.                             
               10  MGV68C-NRESERVATION    PIC  X(7).                            
               10  MGV68C-NVENTE          PIC  X(7).                            
               10  MGV68C-DVENTE          PIC  X(8).                            
               10  MGV68C-DJOUR           PIC  X(8).                            
      *--> DONNEES RESULTANTES.                                                 
           05  MGV68C-SORTIE.                                                   
               10  MGV68C-CODE-RETOUR              PIC  X(01).                  
                   88  MGV68C-OK                  VALUE '0'.                    
                   88  MGV68C-KO                  VALUE '1'.                    
               10  MGV68C-LIBERR          PIC  X(80).                           
      ********* FIN DE LA ZONE DE COMMUNICATION DU MGV68 *************          
                                                                                
