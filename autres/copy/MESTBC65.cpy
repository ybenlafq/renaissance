      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *                             DABORD LA COPY DE L"ENTETE MESSMQ  *      13
      *   MESTBC65 COPY                                                *      13
      * - CREATION / MAJ CLIENT                                        *      13
      *   500 LONGEUR                                                  *      13
      * COPY POUR LES PROGRAMMES MBC60                                 *      13
      ******************************************************************        
      * LONGUEUR DE 34  OCTETS                                                  
              05 MESTBC65-RETOUR.                                               
                 10 MESTBC65-CODE-RETOUR.                                       
                    15  MESTBC65-CODRET  PIC X(1).                              
                         88  MESTBC65-CODRET-OK        VALUE ' '.               
                         88  MESTBC65-CODRET-ERREUR    VALUE '1'.               
                    15  MESTBC65-LIBERR  PIC X(60).                             
                    15  MESTBC65-AS400CR PIC X(08).                             
              05 MESTBC65-DATA.                                                 
      *                               TYPE DE REPONSE                           
                 10  MESTBC65-TYPE            PIC X(03).                        
      *---                         CORRELATION ID                               
                 10  MESTBC65-CORRID          PIC X(50).                        
      * DONNEES CLIENT                                                          
                 10 MESTBC65-DONNES-CLIENT.                                     
      *---                         TYPE DE SELECTION  ' ', 'CARTE'....          
                   15 MESTBC65-TYPSEL     PIC X(10).                            
      *---                               SOCIETE EMETRICE                       
                   15 MESTBC65-NSOC       PIC X(03).                            
      *---                               LIEU EMETEUR                           
                   15 MESTBC65-NLIEU      PIC X(03).                            
      *---                               APPLICATIF SOURCE ; IV, NCG ...        
                   15 MESTBC65-APP-SOURCE PIC X(05).                            
      *---                               CODE TRAITEMENT APPLI SOURCE           
                   15 MESTBC65-CTRT-APPS  PIC X(05).                            
      *---                               PROFIL                                 
                   15 MESTBC65-PROFIL     PIC X(05).                            
      *---                               OPERATEUR                              
                   15 MESTBC65-OPERATEUR  PIC X(20).                            
      *---                               OPERATION CONTACT                      
                   15 MESTBC65-OPECONT    PIC X(06).                            
      *---                               N� CARTE                               
                   15 MESTBC65-NCARTE         PIC X(09).                        
      *---                               ID CLIENT                              
                   15 MESTBC65-IDCLIENT       PIC X(08).                        
      *---                               TITRE CLIENT                           
                   15 MESTBC65-CTITRENOM      PIC X(05).                        
      *---                               LIBELLE TITRE CLIENT                   
                   15 MESTBC65-LTITRENOM      PIC X(30).                        
      *---                               NOM CLIENT                             
                   15 MESTBC65-LNOM           PIC X(50).                        
      *---                               PRENOM CLIENT                          
                   15 MESTBC65-LPRENOM        PIC X(50).                        
      *---                               DATE DE NAISSANCE                      
                   15 MESTBC65-DNAISS         PIC X(08).                        
      *---                               CODE POSTAL DE NAISSANCE               
                   15 MESTBC65-CPNAISS        PIC X(05).                        
      *---                               LOCALITE DE NAISSANCE                  
                   15 MESTBC65-LNAISS         PIC X(32).                        
      *---                               NE SOUHAITE PAS RECEVOIR SMS           
                   15 MESTBC65-RECEPT-SMS    PIC X(01).                         
      *---                             NE SOUHAITE PAS RECEVOIR COURRIER        
                   15 MESTBC65-RECEPT-COURR  PIC X(01).                         
      *---                               NE SOUHAITE PAS RECEVOIR MAIL          
                   15 MESTBC65-RECEPT-MAIL   PIC X(01).                         
      *---                             MOYEN DE COMMUNICATION PRIVILEGIE        
                   15 MESTBC65-MOYEN-COMM    PIC X(05).                         
      *---                               SCORING CLIENT                         
                   15 MESTBC65-SCORING       PIC X(05).                         
      *---                               PROSPECT                               
                   15 MESTBC65-PROSPECT      PIC X(01).                         
      *---                               ESPACE CLIENT ACTIF                    
                   15 MESTBC65-EC-ACTIF      PIC X(01).                         
      *---                              DERNIERE CONNEXION ESPACE CLIENT        
                   15 MESTBC65-DER-EC        PIC X(08).                         
      *---                             INFORMATION COMPLEMENTAIRE CLIENT        
                   15 MESTBC65-ICOMPL-CLI1   PIC X(100).                        
                   15 MESTBC65-ICOMPL-CLI2   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI3   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI4   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI5   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI6   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI7   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI8   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI9   PIC X(20).                         
                   15 MESTBC65-ICOMPL-CLI10  PIC X(20).                         
      *---                              OPERATION                               
                   15 MESTBC65-OPERATION      PIC X(06).                        
      *---                               ID ADRESSE                             
                   15 MESTBC65-NCLIENTADR     PIC X(08).                        
      *---                       RESIDENCE SECONDAIRE, FLAG TYPE ADRESSE        
                   15 MESTBC65-RESSEC         PIC X(01).                        
      *---                               NUMERO DE VOIE                         
                   15 MESTBC65-CVOIE          PIC X(05).                        
      *---                               TYPE DE VOIE                           
                   15 MESTBC65-CTVOIE         PIC X(05).                        
      *---                               LIBELLE DE VOIE                        
                   15 MESTBC65-LCTVOIE        PIC X(30).                        
      *---                               NOM DE VOIE                            
                   15 MESTBC65-LNOMVOIE       PIC X(25).                        
      *---                               BATIMENT                               
                   15 MESTBC65-LBATIMENT      PIC X(03).                        
      *---                               ESCALIER                               
                   15 MESTBC65-LESCALIER      PIC X(03).                        
      *---                               ETAGE                                  
                   15 MESTBC65-LETAGE         PIC X(03).                        
      *---                               PORTE                                  
                   15 MESTBC65-LPORTE         PIC X(03).                        
      *---                               CODE PORTE                             
      *            15 MESTBC65-CPORTE         PIC X(10).                        
      *---                               COMPLEMENT D ADRESSE                   
                   15 MESTBC65-LCMPAD1        PIC X(32).                        
      *---                               CODE POSTAL                            
                   15 MESTBC65-CPOST          PIC X(05).                        
      *---                               CODE PAYS PLAQUES NUMEROLOGIQUE        
                   15 MESTBC65-CPAYS          PIC X(05).                        
      *---                               LOCALITE                               
                   15 MESTBC65-LCOMN          PIC X(32).                        
      *---                               BUREAU DISTRIBUTEUR                    
                   15 MESTBC65-LBUREAU        PIC X(32).                        
      *---                               LONGITUDE                              
                   15 MESTBC65-LONGITUDE      PIC X(22).                        
      *---                               LATITUDE                               
                   15 MESTBC65-LATITUDE       PIC X(22).                        
      *---          INFORMATION COMPLEMENTAIRE CLIENT                           
                   15 MESTBC65-ICOMPL-ADR1    PIC X(100).                       
                   15 MESTBC65-ICOMPL-ADR2    PIC X(20).                        
                   15 MESTBC65-ICOMPL-ADR3    PIC X(20).                        
                   15 MESTBC65-OPETEL1       PIC X(06).                         
                   15 MESTBC65-NUMTEL1       PIC X(20).                         
                   15 MESTBC65-TYPTEL1       PIC X(06).                         
                   15 MESTBC65-STATTEL1      PIC X(05).                         
                   15 MESTBC65-OPTTEL1       PIC X(01).                         
                   15 MESTBC65-OPETEL2       PIC X(06).                         
                   15 MESTBC65-NUMTEL2       PIC X(20).                         
                   15 MESTBC65-TYPTEL2       PIC X(06).                         
                   15 MESTBC65-STATTEL2      PIC X(05).                         
                   15 MESTBC65-OPTTEL2       PIC X(01).                         
                   15 MESTBC65-OPETEL3       PIC X(06).                         
                   15 MESTBC65-NUMTEL3       PIC X(20).                         
                   15 MESTBC65-TYPTEL3       PIC X(06).                         
                   15 MESTBC65-STATTEL3      PIC X(05).                         
                   15 MESTBC65-OPTTEL3       PIC X(01).                         
                   15 MESTBC65-OPETEL4       PIC X(06).                         
                   15 MESTBC65-NUMTEL4       PIC X(20).                         
                   15 MESTBC65-TYPTEL4       PIC X(06).                         
                   15 MESTBC65-STATTEL4      PIC X(05).                         
                   15 MESTBC65-OPTTEL4       PIC X(01).                         
                   15 MESTBC65-OPETEL5       PIC X(06).                         
                   15 MESTBC65-NUMTEL5       PIC X(20).                         
                   15 MESTBC65-TYPTEL5       PIC X(06).                         
                   15 MESTBC65-STATTEL5      PIC X(05).                         
                   15 MESTBC65-OPTTEL5       PIC X(01).                         
                   15 MESTBC65-OPEMEL1       PIC X(06).                         
                   15 MESTBC65-EMAIL1        PIC X(100).                        
                   15 MESTBC65-STATEMAIL1    PIC X(05).                         
                   15 MESTBC65-OPTMAIL1      PIC X(01).                         
                   15 MESTBC65-OPEMEL2       PIC X(06).                         
                   15 MESTBC65-EMAIL2        PIC X(100).                        
                   15 MESTBC65-STATEMAIL2    PIC X(05).                         
                   15 MESTBC65-OPTMAIL2      PIC X(01).                         
                   15 MESTBC65-OPEMEL3       PIC X(06).                         
                   15 MESTBC65-EMAIL3        PIC X(100).                        
                   15 MESTBC65-STATEMAIL3    PIC X(05).                         
                   15 MESTBC65-OPTMAIL3      PIC X(01).                         
      *==========================================================*              
      *            15 MESTBC65-POSTEBUR       PIC X(05).                        
      *            15 MESTBC65-TORG           PIC X(05).                        
      *            15 MESTBC65-LORG           PIC X(32).                        
      *            15 MESTBC65-NFOYERADR      PIC X(08).                        
      *-                                 CODE PAYS AFNOR                        
      *            15 MESTBC65-TPAYS          PIC X(03).                        
      *-                                 DROIT DE MODIFIER L"ORGNISATION        
      *            15 MESTBC65-ORGMODIF       PIC X(01).                        
      *- DEMANDE ENVOI MSG ' ' PAS ENVOI 0 ENVOI CTL 1 ENVOI IMMEDIAT           
JC    *            15 MESTBC65-DATA-FCOURR    PIC X(01).                        
      *-                                 LIBRE POUR PLUS TARD                   
JC    *            15 MESTBC65-CFILLER        PIC X(01).                        
      *                               FLAG DETENTION DONNE REDEVANCE            
      *            15 MESTBC65-EQUEST         PIC X(03).                        
      *-                                 CODE SORTIE TELEPHONIQUE               
      *??????????10 MESTBC65-INDIN            PIC X(03).                        
      *-                                 CRE CLIENT GROUPE                      
      *          10 MESTBC65-NCLIENTGRP       PIC X(08).                        
      *????????  10 MESTBC65-FLAG-MODIF.                                        
      *            15 MESTBC65-F-NCARTE       PIC X(01).                        
      *            15 MESTBC65-F-LNOM         PIC X(01).                        
      *            15 MESTBC65-F-LPRENOM      PIC X(01).                        
      *            15 MESTBC65-F-CTITRENOM    PIC X(01).                        
      *            15 MESTBC65-F-TORG         PIC X(01).                        
      *            15 MESTBC65-F-LORG         PIC X(01).                        
      *            15 MESTBC65-F-DNAISS       PIC X(01).                        
      *            15 MESTBC65-F-CVOIE        PIC X(01).                        
      *            15 MESTBC65-F-CTVOIE       PIC X(01).                        
      *            15 MESTBC65-F-LNOMVOIE     PIC X(01).                        
      *            15 MESTBC65-F-CPOST        PIC X(01).                        
      *            15 MESTBC65-F-LCOMN        PIC X(01).                        
      *            15 MESTBC65-F-NTDOM        PIC X(01).                        
      *            15 MESTBC65-F-NGSM         PIC X(01).                        
      *            15 MESTBC65-F-NTBUR        PIC X(01).                        
      *            15 MESTBC65-F-POSTEBUR     PIC X(01).                        
      *            15 MESTBC65-F-EMAIL        PIC X(01).                        
      *            15 MESTBC65-F-LBATIMENT    PIC X(01).                        
      *            15 MESTBC65-F-LESCALIER    PIC X(01).                        
      *            15 MESTBC65-F-LETAGE       PIC X(01).                        
      *            15 MESTBC65-F-LPORTE       PIC X(01).                        
      *            15 MESTBC65-F-LCMPAD1      PIC X(01).                        
      *            15 MESTBC65-F-LBUREAU      PIC X(01).                        
      *            15 MESTBC65-F-CPAYS        PIC X(01).                        
      *            15 MESTBC65-F-RESSEC       PIC X(01).                        
      *-                                 PAS DE FLAG DE MAJ                     
      *            15 MESTBC65-F-SFLAG        PIC X(01).                        
      *-                                 FILLER FLAG  CLIENT                    
      *            15 MESTBC65-F-FILLEUR      PIC X(04).                        
      *DONNES COMPLEMENTAIRES EN ENTREE DE LA MODULE                            
      *                               CODE TRAITEMENT (MAJ CRE)                 
      *???????   10 MESTBC65-CODETR           PIC X(03).                        
      *                               TYPE DE L'ADRESSE A/B/C           00550009
      *          10 MESTBC65-ADRESSE          PIC X(01).                        
      *                               COMMUNE FORCEE                            
      *???????   10 MESTBC65-COMN-FORCEE      PIC X(01).                        
      * DATA ITILISE PAR LE HOST ACID UTILISATEUR                               
      *          10 MESTBC65-ACID             PIC X(008).                       
      * SI � 'N' ON N'ATTEND PAS LA REPONSE MQ                                  
      *          10 MESTBC65-REPONSE          PIC X(01).                        
PM    *          10 MESTBC65-DATA-INUTILE     PIC X(18).                        
PM    *          10 MESTBC65-DATA-VENTE.                                        
PM    *               15 MESTBC65-NSOCIETEM   PIC X(03).                        
PM    *               15 MESTBC65-NLIEUM      PIC X(03).                        
PM    *               15 MESTBC65-NCVENDEUR   PIC X(06).                        
PM    *               15 MESTBC65-NORDRE      PIC X(05).                        
PM    *               15 MESTBC65-FILLER      PIC X.                            
      *                              RESERVER APPLICATIONS APPELANTES           
      *          10 MESTBC65-DATA-CHANGES     PIC X(20).                        
      *                              RESERVER TRAITEMENT AS/400                 
      *          10 MESTBC65-DATA-AS400D      PIC X(20).                        
      *          10 MESTBC65-DATA-SIEBEL REDEFINES MESTBC65-DATA-AS400D.        
      *             15 MESTBC65-DATA-COMPTEUR    PIC 9(02).                     
      *             15 MESTBC65-DATA-FILLER0     PIC X(18).                     
      *          10 MESTBC65-DATA-PWD    REDEFINES MESTBC65-DATA-AS400D.        
      *             15 MESTBC65-DATA-PSWINIT     PIC X(20).                     
      *          10 MESTBC65-DATA-VENTE  REDEFINES MESTBC65-DATA-AS400D.        
JC0407*             15 MESTBC65-DVENTE        PIC X(08).                        
JCE407*             15 MESTBC65-DATA-FILLER1  PIC X(13).                        
JCE407*             15 MESTBC65-DATA-FILLER1  PIC X(04).                        
JCE407*             15 MESTBC65-DATA-CVENTE   PIC X(01).                        
      *             15 MESTBC65-DATA-NVENTE   PIC X(07).                        
      *          10 MESTBC65-F-LNAISS         PIC X(01).                        
      *                                                                 00550009
      ***************************************************************** 00740000
                                                                                
