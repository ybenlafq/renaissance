      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:05 >
      *****************************************************************
      * 13-03-2012 - DE02074 AJOUT MAJ CLE RTPR00 SI ON VIENT DE
      *            -         L'ARCHIVAGE
      *****************************************************************
      * 13-03-2012 - de02074 ajout code ction dans mec00m
      *****************************************************************
      * 30-03-2012 - FT - VALEURS PAR DEFAUT EN DUR
      *****************************************************************
      * 16-10-2013 - DE02009 Envoi du montant ECOMOB en TTC.
      *****************************************************************
      **28/10/2013. ajout num�ro de pse                               *
      *                                                               *
      *****************************************************************
      *--> GESTION  DB2
       01  DB2-DISP.
      *         LG = 58 ( LONGUEUR DU MESSAGE VERS PGM APPELANT )
           05  FILLER    PIC X(12) VALUE 'PB DB2 PGM: '.
           05  PGM-DISP  PIC X(6) .
           05  FILLER    PIC X(8) VALUE ' ,CODE: '.
           05  RET-DISP  PIC 9(1) .
           05  FILLER    PIC X(6) VALUE ',SQL: '.
           05  SQL-DISP  PIC ++++9 .
           05  FILLER    PIC X(8) VALUE ',ORDRE: '.
           05  ORD-DISP  PIC X(7) .
           05  FILLER    PIC X(1) VALUE '-'.
           05  TAB-DISP  PIC X(4) .
      *--> WORKING  POUR CALCUL PRIX PRODUIT QUAND IL EXISTE 1 REMISE
       01  ZONE-CALCUL .
           05  PRIX-CALCULE  PIC S9(5) COMP-3.
           05  REMISE-UNIT   PIC S9(7)V9(0002) COMP-3 .
           05  REMISE-TOT    PIC S9(7)V9(0002) COMP-3 .
      * FORMATER LE GV11-NSEQ DE X(2) -->  S9(5) COMP-3
       01  NSEQ-X-5.
           05  FILLER    PIC X(3) VALUE '000'.
           05  NSEQ-X-2  PIC X(2) VALUE '00'.
       01  NSEQ-9-5 REDEFINES NSEQ-X-5 PIC 9(5) .
      * DERNIERE POSITION DU TABLEAU VENTE
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  POSMAX    PIC S9(4) BINARY .
      *--
       01  POSMAX    PIC S9(4) COMP-5 .
      *}
      *
      *         RESULTATS REQUETES DB2
      *01  RESULT        PIC X .
      *    88 NORMAL           VALUE '0'.
      *    88 NON-TROUVE       VALUE '1'.
       01  WS-TAB-CLIENT.
         02  WS-TAB-CLIENT-OK OCCURS 2.
           03  WS-TOP-CLIENT        PIC 9                 VALUE 1.
             88 WS-IL-Y-A-CLIENT                     VALUE 1.
             88 WS-PAS-DE-CLIENT                     VALUE 0.
       01  WS-TOP-LECTURE-TS         PIC 9                 VALUE ZERO.
           88 PAS-FIN-TS                           VALUE 1.
           88 FIN-TS                               VALUE 0.
      *         FIN DES CURSEURS
       01  FIN-GV11 PIC X VALUE 'N' .
       01  FIN-VE11 PIC X VALUE 'N' .
       01  FIN-GV14 PIC X VALUE 'N' .
       01  FIN-VE14 PIC X VALUE 'N' .
       01  W-TVA                  PIC ZZV99.
       01  EDIT-GA38-MONTANT      PIC 99999,99.
       01  W-GV02                 PIC 9.
      * POUR DISPLAY DANS LA LOG
       01  W-MESSAGE-S.
           02  FILLER      PIC X(5) VALUE 'ECFC'.
           02  W-MESSAGE-H PIC X(2).
           02  FILLER      PIC X(1) VALUE ':'.
           02  W-MESSAGE-M PIC X(2).
           02  FILLER      PIC X(1) VALUE ' '.
           02  W-MESSAGES  PIC X(69).
       01  W-EIBTIME        PIC 9(7).
       01  FILLER REDEFINES W-EIBTIME.
           02  FILLER    PIC X.
           02  W-HEURE   PIC XX.
           02  W-MINUTE  PIC XX.
           02  W-SECONDE PIC XX.
      * et pour calculer le TTC selon une m�thode � la noix...
       01  6-DECIMALES PIC S9(7)V9(6).
       01  5-DECIMALES PIC S9(7)V9(5).
       01  4-DECIMALES PIC S9(7)V9(4).
       01  3-DECIMALES PIC S9(7)V9(3).
       01  W-TXTVA-TVA PIC S9(04)V9(04) COMP-3.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  M-TXTVA PIC S9(4) BINARY VALUE 10.
      *--
       01  M-TXTVA PIC S9(4) COMP-5 VALUE 10.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  U-TXTVA PIC S9(4) BINARY VALUE 0.
      *--
       01  U-TXTVA PIC S9(4) COMP-5 VALUE 0.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-TXTVA PIC S9(4) BINARY.
      *--
       01  I-TXTVA PIC S9(4) COMP-5.
      *}
       01  T-TXTVA-TAB.
           05 T-TXTVA-OCCURS OCCURS 10.
              10 T-TXTVA-TAUX       PIC X(05).
              10 T-TXTVA-TVA        PIC S9(04)V9(04) COMP-3.
              10 T-TXTVA-ANCTVA     PIC S9(04)V9(04) COMP-3.
       01  W-NOUVELLE-VERSION PIC X.
      * ZONES DIVERSES
      *01  ZONES-AIDA.
      *     02  LONG-TS              PIC S9(4) COMP VALUE +0.
      *     02  RANG-TS              PIC S9(4) COMP VALUE +0.
      *     02  IDENT-TS            PIC X(8)  VALUE SPACE.
      *
      *     02  CODE-RETOUR PIC X(1)           VALUE '0'.
      *       88  TROUVE                     VALUE '0'.
      *       88  NORMAL                     VALUE '0'.
      *       88  DATE-OK                    VALUE '0'.
      *       88  SELECTE                    VALUE '0'.
      *       88  NON-TROUVE                 VALUE '1'.
      *       88  NON-SELECTE                VALUE '1'.
      *       88  ANORMAL                    VALUE '1'.
      *       88  EXISTE-DEJA                VALUE '2'.
      *       88  FIN-FICHIER                VALUE '3'.
      *       88  ERREUR-DATE                VALUE '4'.
      *       88  ERREUR-FORMAT              VALUE '5'.
      *       88  ERREUR-HEURE               VALUE '6'.
      *       88  DOUBLE                     VALUE '7'.
      *  => ZONES DE TRAVAIL UTILES POUR LA GESTION DE TSTENVOI
           COPY WKTENVOI.
      * ...POUR MEC00 (ENVOI A DARTY.COM)
      ***********************************
      *    COPY MEC00C.
           COPY MEC0MC.
      * ...POUR MBS42 (CHERCHER DANS L'ARCHIVAGE SANS LE SORTIR)
      ***********************************
           COPY COMMBS42.
           COPY TSBS42.
      *   COPY SYKWZINO.
      *****************************************************************
      *  LINKAGE
      *****************************************************************
       LINKAGE SECTION.
      *         ZONE DE COMMUNICATION AVEC MECFC .
       01  DFHCOMMAREA.
           COPY COMMECFC.
      *=================================================================
       PROCEDURE DIVISION.
      *=================================================================
           PERFORM ENTREE .
           PERFORM TRAITEMENT-TACHE.
           PERFORM SORTIE .
      *
      *=================================================================
       ENTREE SECTION.
      *****************
           INITIALIZE               RVGV1402   .
           INITIALIZE               RVGV1105   .
           INITIALIZE               RVGV0202   .
           INITIALIZE               RVGV1099   .
           SET  NORMAL              TO TRUE    .
           STRING 'DEMANDE ' W-FC-E-NSOC ' '
                           W-FC-E-NLIEU ' '
                           W-FC-E-NVENTE ' '
                   DELIMITED BY SIZE INTO W-MESSAGES
           PERFORM DISPLAY-W-MESSAGE
           PERFORM RECH-PARAM-VERSION
           INITIALIZE T-TXTVA-TAB
           PERFORM DECLARE-TXTVA
           PERFORM FETCH-TXTVA
           PERFORM UNTIL NON-TROUVE
              PERFORM AJOUT-TXTVA
              PERFORM FETCH-TXTVA
           END-PERFORM
           PERFORM CLOSE-TXTVA
           .
      *
      *=================================================================
       TRAITEMENT-TACHE SECTION.
      *    CODE RETOUR POUR EC00
           MOVE '00' TO W-FACTURE-CLIENT-CRET.
      *    CODE RETOUR DU MESSAGE
           MOVE '00' TO W-FC-E-C-RET.
      * ON RECHERCHE LA VENTE DANS GV10
           PERFORM SELECT-GV10.
      * ON A TROUVE, ON RENSEIGNE LA COMMAREA
           IF NORMAL
              PERFORM MEF-GV10
              PERFORM SELECT-GV02-A
              PERFORM MEF-ADRESSE-FACTURATION
              PERFORM SELECT-GV02-B
              PERFORM MEF-ADRESSE-LIVRAISON
              PERFORM TRAIT-GV11
              PERFORM TRAIT-GV14
           ELSE
      * ON A PAS TROUVE DANS GV10, ON CHERCHE DANS VE10
              PERFORM SELECT-VE10
      * ON A TROUVE, ON RENSEIGNE LA COMMAREA
              IF NORMAL
                 PERFORM MEF-GV10
                 PERFORM SELECT-VE02-A
                 PERFORM MEF-ADRESSE-FACTURATION
                 PERFORM SELECT-VE02-B
                 PERFORM MEF-ADRESSE-LIVRAISON
                 PERFORM TRAIT-VE11
                 PERFORM TRAIT-VE14
              ELSE
      * ON A PAS TROUVE DANS VE10, ON APPEL LE MBS42 POUR CHERCHER
      * SI LA VENTE EST ARCHIVEE
                 STRING 'ARCHIVE ' W-FC-E-NSOC ' '
                                 W-FC-E-NLIEU ' '
                                 W-FC-E-NVENTE ' '
                  DELIMITED BY SIZE INTO W-MESSAGES
                 PERFORM DISPLAY-W-MESSAGE
                 PERFORM LINK-MBS42
                 IF COMM-BS42-CODRET-TS-OK
                   MOVE 1 TO W-GV02
                   MOVE 1 TO RANG-TS
                   MOVE 'N' TO FIN-GV14
                   MOVE 0 TO W-FC-V-U
                             W-FC-R-U
                   PERFORM LECTURE-TS
                   PERFORM UNTIL FIN-TS OR RANG-TS > 999
                     EVALUATE TS42-TABLE
                      WHEN 'GV10'
                         PERFORM ALIM-GV10
                      WHEN 'GV14'
                          PERFORM ALIM-GV14
                      WHEN 'GV11'
                         PERFORM ALIM-GV11
                      WHEN 'GV02'
                          PERFORM ALIM-GV02
                     END-EVALUATE
                     PERFORM LECTURE-TS
                   END-PERFORM
                 ELSE
                   STRING 'PB ARCH ' W-FC-E-NSOC ' '
                                   W-FC-E-NLIEU ' '
                                   W-FC-E-NVENTE ' '
                   DELIMITED BY SIZE INTO W-MESSAGES
                   PERFORM DISPLAY-W-MESSAGE
                   IF COMM-BS42-LIBERR(1:4) = '0001'
                     MOVE '01' TO W-FC-E-C-RET
                   ELSE
                     MOVE '09' TO W-FC-E-C-RET
                   END-IF
                 END-IF
              END-IF
           END-IF.
      * ON APPEL LE PROGRAMME DE MISE EN FORME DU MESSAGE XML
           PERFORM ENVOI-REPONSE
           .
       FIN-TRAITEMENT-TACHE. EXIT.
      *-----------------------------------------------------------------
       LINK-MBS42            SECTION.
           INITIALIZE COMM-BS42-APPLI
           MOVE W-FC-E-NSOC     TO        COMM-BS42-NSOCIETE
           MOVE W-FC-E-NLIEU    TO        COMM-BS42-NLIEU
           MOVE W-FC-E-NVENTE   TO        COMM-BS42-NVENTE
           MOVE 'MECFC'         TO        COMM-BS42-PGAPPEL
           EXEC CICS LINK PROGRAM ('MBS42')
                          COMMAREA (COMM-BS42-APPLI)
           END-EXEC.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-LINK-MBS42. EXIT.
      *-----------------------------------------------------------------
       ENVOI-REPONSE SECTION.
           STRING 'REPONSE ' W-FC-E-NSOC ' '
                                   W-FC-E-NLIEU ' '
                                   W-FC-E-NVENTE ' '
                   DELIMITED BY SIZE INTO W-MESSAGES
           PERFORM DISPLAY-W-MESSAGE
           INITIALIZE              MEC00C-COMMAREA
           MOVE 'FACEC1'        TO MEC00C-FICHIER-SI
           MOVE 'D'             TO MEC00C-CODE-ACTION
      *    MOVE 'D'             TO MEC00C-CODE-MAJ
           MOVE W-FC-VENTE-GV   TO MEC00C-DATA-SI
           MOVE LENGTH OF W-FC-VENTE-GV  TO MEC00C-DATA-SI-L
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
           MOVE 'FACEC2'        TO MEC00C-FICHIER-SI
           MOVE 'T'             TO MEC00C-CODE-ACTION
      *    MOVE 'T'             TO MEC00C-CODE-MAJ
           MOVE W-FC-ENTETE     TO MEC00C-DATA-SI
           MOVE LENGTH OF W-FC-ENTETE    TO MEC00C-DATA-SI-L
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
           IF W-FC-C-NOM(1) > SPACES
           AND WS-IL-Y-A-CLIENT(1)
             MOVE 'FACEC3'        TO MEC00C-FICHIER-SI
             MOVE 'T'             TO MEC00C-CODE-ACTION
      *      MOVE 'T'             TO MEC00C-CODE-MAJ
             MOVE W-FC-CLIENT(1)  TO MEC00C-DATA-SI
             MOVE LENGTH OF W-FC-CLIENT(1) TO MEC00C-DATA-SI-L
             EXEC CICS LINK PROGRAM ('MEC00M')
                           COMMAREA (MEC00C-COMMAREA)
             END-EXEC
           END-IF
           IF W-FC-C-NOM(2) > SPACES
           AND WS-IL-Y-A-CLIENT(2)
             MOVE 'FACEC3'        TO MEC00C-FICHIER-SI
             MOVE 'T'             TO MEC00C-CODE-ACTION
      *      MOVE 'T'             TO MEC00C-CODE-MAJ
             MOVE W-FC-CLIENT(2)  TO MEC00C-DATA-SI
             MOVE LENGTH OF W-FC-CLIENT(2) TO MEC00C-DATA-SI-L
             EXEC CICS LINK PROGRAM ('MEC00M')
                           COMMAREA (MEC00C-COMMAREA)
             END-EXEC
            END-IF
      *
           PERFORM VARYING W-FC-V-I FROM 1 BY 1
           UNTIL W-FC-V-I > W-FC-V-U
           OR W-FC-V-CTYPENREG(W-FC-V-I) = SPACE
              MOVE 'FACEC4  '      TO MEC00C-FICHIER-SI
              MOVE 'T'             TO MEC00C-CODE-ACTION
      *       MOVE 'T'             TO MEC00C-CODE-MAJ
              MOVE W-FC-VENTE(W-FC-V-I) TO MEC00C-DATA-SI
              MOVE LENGTH OF W-FC-VENTE(W-FC-V-I)
                                          TO MEC00C-DATA-SI-L
              EXEC CICS LINK PROGRAM ('MEC00M')
                COMMAREA (MEC00C-COMMAREA)
              END-EXEC
           END-PERFORM
      *
           PERFORM VARYING W-FC-R-I FROM 1 BY 1
           UNTIL W-FC-R-I > W-FC-R-U
           OR W-FC-R-L-PMENT-SI(W-FC-R-I) = SPACE
              MOVE 'FACEC5  '      TO MEC00C-FICHIER-SI
              MOVE 'T'             TO MEC00C-CODE-ACTION
      *       MOVE 'T'             TO MEC00C-CODE-MAJ
              MOVE W-FC-REGLEMENT(W-FC-R-I) TO MEC00C-DATA-SI
              MOVE LENGTH OF W-FC-REGLEMENT(W-FC-R-I)
                                          TO MEC00C-DATA-SI-L
              EXEC CICS LINK PROGRAM ('MEC00M')
                COMMAREA (MEC00C-COMMAREA)
              END-EXEC
           END-PERFORM
           MOVE 'FACEC1'        TO MEC00C-FICHIER-SI
           MOVE 'F'             TO MEC00C-CODE-ACTION
      *    MOVE 'F'             TO MEC00C-CODE-MAJ
           MOVE SPACE           TO MEC00C-DATA-SI
           MOVE LENGTH OF W-FC-VENTE-GV  TO MEC00C-DATA-SI-L
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
           SET MEC00C-FIN-TRAITEMENT TO TRUE
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC.
      *    IF W-MEC31-CODE-RET NOT = '0'
      *       EXEC CICS SYNCPOINT ROLLBACK END-EXEC
      *    END-IF
           PERFORM LOG-LE-MESSAGE.
      *
       FIN-ENVOI-REPONSE. EXIT.
      * CREATION D'UN MOUCHARD MESSAGE EN SORTIE.
       LOG-LE-MESSAGE SECTION.
           IF MEC00C-DATA-XML-L > 4034
              MOVE 4034 TO EC05-XMLDATA-LEN
           ELSE
              MOVE MEC00C-DATA-XML-L TO EC05-XMLDATA-LEN
           END-IF.
           MOVE MEC00C-DATA-XML TO EC05-XMLDATA-TEXT.
           EXEC SQL INSERT INTO RVEC0500 (TIMESTP, WRECENV, XMLDATA)
                         VALUES (CURRENT TIMESTAMP, 'E', :EC05-XMLDATA)
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'INSERT RTEC05:' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-LOG-LE-MESSAGE. EXIT.
       DISPLAY-W-MESSAGE SECTION.
      *--------------------------
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.
      *--
           EXEC CICS ASKTIME NOHANDLE
           END-EXEC.
      *}
           MOVE EIBTIME             TO W-EIBTIME.
           MOVE W-HEURE             TO W-MESSAGE-H.
           MOVE W-MINUTE            TO W-MESSAGE-M.
           EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
                    LENGTH (80) NOHANDLE
           END-EXEC.
           MOVE SPACES TO W-MESSAGES.
       FIN-DISPLAY-W-MESSAGE. EXIT.
      *-----------------------------------------------------------------
       MEF-GV10 SECTION.
      *******************
           MOVE GV10-DVENTE         TO W-FC-E-DVENTE
           STRING GV10-DHVENTE GV10-DMVENTE
           DELIMITED BY SIZE INTO W-FC-E-HVENTE
      *    MOVE GV10-DHVENTE        TO W-FC-E-HVENTE
           MOVE GV10-PTTVENTE       TO W-FC-E-PTTVENTE
           MOVE GV10-PCOMPT         TO W-FC-E-PCOMPT
           MOVE GV10-PLIVR          TO W-FC-E-PLIVR
           MOVE GV10-PDIFFERE       TO W-FC-E-PDIFFERE
           MOVE GV10-PRFACT         TO W-FC-E-PRFACT
           MOVE GV10-LCOMVTE1       TO W-FC-E-LCOMVTE1
           MOVE GV10-LCOMVTE2       TO W-FC-E-LCOMVTE2
           MOVE GV10-LCOMVTE3       TO W-FC-E-LCOMVTE3
           MOVE GV10-LCOMVTE4       TO W-FC-E-LCOMVTE4
           MOVE GV10-DMODIFVTE      TO W-FC-E-DMODIFVTE
           MOVE GV10-WFACTURE       TO W-FC-E-WFACTURE
           MOVE GV10-WEXPORT        TO W-FC-E-WEXPORT
           MOVE GV10-WDETAXEC       TO W-FC-E-WDETAXEC
           MOVE GV10-WDETAXEHC      TO W-FC-E-WDETAXEHC
           MOVE GV10-LDESCRIPTIF1   TO W-FC-E-LDESCRIPTIF1
           MOVE GV10-LDESCRIPTIF2   TO W-FC-E-LDESCRIPTIF2
           MOVE GV10-TYPVTE         TO W-FC-E-TYPVTE.
       FIN-MEF-GV10. EXIT.
       ALIM-GV10 SECTION.
      *******************
           MOVE TS42-E-DVENTE       TO W-FC-E-DVENTE
           MOVE   '0900'       TO W-FC-E-HVENTE
           MOVE TS42-E-PTTVENTE     TO W-FC-E-PTTVENTE
           MOVE TS42-E-PCOMPT       TO W-FC-E-PCOMPT
           MOVE TS42-E-PLIVR        TO W-FC-E-PLIVR
           MOVE TS42-E-PDIFFERE     TO W-FC-E-PDIFFERE
           MOVE TS42-E-PRFACT       TO W-FC-E-PRFACT
           MOVE TS42-E-LCOMVTE1     TO W-FC-E-LCOMVTE1
           MOVE TS42-E-LCOMVTE2     TO W-FC-E-LCOMVTE2
           MOVE TS42-E-LCOMVTE3     TO W-FC-E-LCOMVTE3
           MOVE TS42-E-LCOMVTE4     TO W-FC-E-LCOMVTE4
           MOVE SPACES              TO W-FC-E-DMODIFVTE
           MOVE TS42-E-WFACTURE     TO W-FC-E-WFACTURE
           MOVE TS42-E-WEXPORT      TO W-FC-E-WEXPORT
           MOVE TS42-E-WDETAXEC     TO W-FC-E-WDETAXEC
           MOVE TS42-E-WDETAXEHC    TO W-FC-E-WDETAXEHC
           MOVE SPACES              TO W-FC-E-LDESCRIPTIF1
           MOVE SPACES              TO W-FC-E-LDESCRIPTIF2
           MOVE TS42-E-TYPVTE       TO W-FC-E-TYPVTE.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-ALIM-GV10. EXIT.
       ALIM-GV02 SECTION.
      *******************
           SET WS-IL-Y-A-CLIENT(W-GV02) TO TRUE
           MOVE TS42-WTYPEADR       TO W-FC-C-TYPEADR(W-GV02)
           IF TS42-CTITRENOM = SPACES OR LOW-VALUE
              MOVE 'M MME' TO TS42-CTITRENOM
           END-IF
           MOVE TS42-CTITRENOM      TO W-FC-C-TITRE(W-GV02)
           MOVE TS42-LNOM           TO W-FC-C-NOM(W-GV02)
           MOVE TS42-LPRENOM        TO W-FC-C-PRENOM(W-GV02)
           MOVE TS42-LBATIMENT      TO W-FC-C-BAT(W-GV02)
           MOVE TS42-LESCALIER      TO W-FC-C-ESC(W-GV02)
      *    MOVE TS42-LETAGE         TO W-FC-C-ETAGE(W-GV02)
           MOVE TS42-LPORTE         TO W-FC-C-PORTE(W-GV02)
           MOVE TS42-LCOMMUNE       TO W-FC-C-COMMUNE(W-GV02)
           MOVE TS42-CPOSTAL        TO W-FC-C-CPOSTAL(W-GV02)
           MOVE TS42-TELDOM         TO W-FC-C-TELDOM(W-GV02)
           MOVE TS42-TELBUR         TO W-FC-C-TELBUR(W-GV02)
           MOVE TS42-NGSM           TO W-FC-C-TELMOB(W-GV02)
           MOVE TS42-CVOIE          TO W-FC-C-CVOIE(W-GV02)
           MOVE TS42-CTVOIE         TO W-FC-C-TYPEVOIE(W-GV02)
           MOVE TS42-LNOMVOIE       TO W-FC-C-NOMVOIE(W-GV02)
           MOVE TS42-LCOMPAD1       TO W-FC-C-COMPL1(W-GV02)
           MOVE SPACES              TO W-FC-C-COMPL2(W-GV02)
           MOVE SPACES              TO W-FC-C-COMLIV1(W-GV02)
           MOVE SPACES              TO W-FC-C-COMLIV2(W-GV02)
           MOVE TS42-CPAYS          TO W-FC-C-CPAYS(W-GV02).
           MOVE SPACES              TO W-FC-C-WETRANGER(W-GV02)
           MOVE SPACES              TO W-FC-C-NCLIENT(W-GV02).
           IF (TS42-CVOIE = SPACES OR LOW-VALUE )
           OR (TS42-CTVOIE = SPACES OR LOW-VALUE)
           OR (TS42-LNOMVOIE = SPACES OR LOW-VALUE)
           OR (TS42-LCOMMUNE = SPACES OR LOW-VALUE)
           OR (TS42-CPOSTAL  = SPACES OR LOW-VALUE)
            IF W-NOUVELLE-VERSION = 'O'
              SET WS-PAS-DE-CLIENT(W-GV02) TO TRUE
            END-IF
           END-IF
           ADD 1 TO W-GV02
           .
       FIN-ALIM-GV02. EXIT.
       MEF-ADRESSE-LIVRAISON SECTION.
      ********************************
           SET WS-IL-Y-A-CLIENT(2) TO TRUE
           MOVE 'B'                 TO W-FC-C-TYPEADR(2)
           IF GV02-CTITRENOM = SPACES OR LOW-VALUE
              MOVE 'M MME' TO GV02-CTITRENOM
           END-IF
           MOVE GV02-CTITRENOM      TO W-FC-C-TITRE(2)
           MOVE GV02-LNOM           TO W-FC-C-NOM(2)
           MOVE GV02-LPRENOM        TO W-FC-C-PRENOM(2)
           MOVE GV02-LBATIMENT      TO W-FC-C-BAT(2)
           MOVE GV02-LESCALIER      TO W-FC-C-ESC(2)
      *    MOVE GV02-LETAGE         TO W-FC-C-ETAGE(2)
           MOVE GV02-LPORTE         TO W-FC-C-PORTE(2)
           MOVE GV02-LCOMMUNE       TO W-FC-C-COMMUNE(2)
           MOVE GV02-CPOSTAL        TO W-FC-C-CPOSTAL(2)
           MOVE GV02-TELDOM         TO W-FC-C-TELDOM(2)
           MOVE GV02-TELBUR         TO W-FC-C-TELBUR(2)
           MOVE GV02-NGSM           TO W-FC-C-TELMOB(2)
           MOVE GV02-CVOIE          TO W-FC-C-CVOIE(2)
           MOVE GV02-CTVOIE         TO W-FC-C-TYPEVOIE(2)
           MOVE GV02-LNOMVOIE       TO W-FC-C-NOMVOIE(2)
           MOVE GV02-LCMPAD1        TO W-FC-C-COMPL1(2)
           MOVE GV02-LCMPAD2        TO W-FC-C-COMPL2(2)
           MOVE GV02-LCOMLIV1       TO W-FC-C-COMLIV1(2)
           MOVE GV02-LCOMLIV2       TO W-FC-C-COMLIV2(2)
           MOVE GV02-CPAYS          TO W-FC-C-CPAYS(2).
           MOVE GV02-WETRANGER      TO W-FC-C-WETRANGER(2)
           MOVE GV02-IDCLIENT       TO W-FC-C-NCLIENT(2).
           IF (GV02-CVOIE = SPACES OR LOW-VALUE )
           OR (GV02-CTVOIE = SPACES OR LOW-VALUE)
           OR (GV02-LNOMVOIE = SPACES OR LOW-VALUE)
           OR (GV02-LCOMMUNE = SPACES OR LOW-VALUE)
           OR (GV02-CPOSTAL  = SPACES OR LOW-VALUE)
            IF W-NOUVELLE-VERSION = 'O'
              SET WS-PAS-DE-CLIENT(2) TO TRUE
            END-IF
           END-IF
           .
      *-----------------------------------------------------------------
       MEF-ADRESSE-FACTURATION SECTION.
      **********************************
           SET WS-IL-Y-A-CLIENT(1) TO TRUE
           MOVE 'A'                 TO W-FC-C-TYPEADR(1)
           IF GV02-CTITRENOM = SPACES OR LOW-VALUE
              MOVE 'M MME' TO GV02-CTITRENOM
           END-IF
           MOVE GV02-CTITRENOM      TO W-FC-C-TITRE(1)
           MOVE GV02-LNOM           TO W-FC-C-NOM(1)
           MOVE GV02-LPRENOM        TO W-FC-C-PRENOM(1)
           MOVE GV02-LBATIMENT      TO W-FC-C-BAT(1)
           MOVE GV02-LESCALIER      TO W-FC-C-ESC(1)
      *    MOVE GV02-LETAGE         TO W-FC-C-ETAGE(1)
           MOVE GV02-LPORTE         TO W-FC-C-PORTE(1)
           MOVE GV02-LCOMMUNE       TO W-FC-C-COMMUNE(1)
           MOVE GV02-CPOSTAL        TO W-FC-C-CPOSTAL(1)
           MOVE GV02-TELDOM         TO W-FC-C-TELDOM(1)
           MOVE GV02-TELBUR         TO W-FC-C-TELBUR(1)
           MOVE GV02-NGSM           TO W-FC-C-TELMOB(1)
           MOVE GV02-CVOIE          TO W-FC-C-CVOIE(1)
           MOVE GV02-CTVOIE         TO W-FC-C-TYPEVOIE(1)
           MOVE GV02-LNOMVOIE       TO W-FC-C-NOMVOIE(1)
      *    INSPECT W-FC-C-NOMVOIE(1) REPLACING ALL '''' BY '&apos'.
           MOVE GV02-LCMPAD1        TO W-FC-C-COMPL1(1)
      *    INSPECT W-FC-C-COMPL1(1) REPLACING ALL '''' BY '&apos'.
           MOVE GV02-LCMPAD2        TO W-FC-C-COMPL2(1)
      *    INSPECT W-FC-C-COMPL2(1) REPLACING ALL '''' BY '&apos'.
           MOVE GV02-LCOMLIV1       TO W-FC-C-COMLIV1(1)
           MOVE GV02-LCOMLIV2       TO W-FC-C-COMLIV2(1)
           MOVE GV02-CPAYS          TO W-FC-C-CPAYS(1).
           MOVE GV02-WETRANGER      TO W-FC-C-WETRANGER(1)
           MOVE GV02-IDCLIENT       TO W-FC-C-NCLIENT(1).
           IF (GV02-CVOIE = SPACES OR LOW-VALUE )
           OR (GV02-CTVOIE = SPACES OR LOW-VALUE )
           OR (GV02-LNOMVOIE = SPACES OR LOW-VALUE)
           OR (GV02-LCOMMUNE = SPACES OR LOW-VALUE)
           OR (GV02-CPOSTAL  = SPACES OR LOW-VALUE)
            IF W-NOUVELLE-VERSION = 'O'
              SET WS-PAS-DE-CLIENT(1) TO TRUE
            END-IF
           END-IF
           .
      *-----------------------------------------------------------------
       TRAIT-GV14 SECTION.
      *********************
           PERFORM DECLARE-GV14.
           PERFORM FETCH-GV14.
           PERFORM UNTIL FIN-GV14 = 'O'
              PERFORM MEF-GV14
              PERFORM FETCH-GV14
           END-PERFORM.
           PERFORM CLOSE-GV14.
       TRAIT-VE14 SECTION.
      *********************
           PERFORM DECLARE-VE14.
           PERFORM FETCH-VE14.
           PERFORM UNTIL FIN-VE14 = 'O'
              PERFORM MEF-GV14
              PERFORM FETCH-VE14
           END-PERFORM.
           PERFORM CLOSE-VE14.
      *.................................................................
       MEF-GV14 SECTION.
      *******************
      * ON CHARGE AU PLUS 'W-FC-R-MAX' LIGNES ET ON STOPPE LE CURSEUR
      * EH OUI C'EST PAS BEAU MAIS CE CAS N'EST PAS CENSE SE PRODUIRE
      * EN TOUT CAS C'EST CE QUI A ETE DECIDE POUR L'INSTANT, APRES ...
            ADD 1 TO W-FC-R-I
            IF W-FC-R-I >  10
               MOVE 'O' TO FIN-GV14
            ELSE
              MOVE GV14-NSEQ           TO  W-FC-R-L-PMENT-SI(W-FC-R-I)
              MOVE GV14-CMODPAIMT      TO  W-FC-R-MD-PMENT(W-FC-R-I)
              MOVE GV14-NREGLTVTE      TO  W-FC-R-ID-PMENT(W-FC-R-I)
              MOVE GV14-NTRANS         TO  W-FC-R-NUM-ESSEMT(W-FC-R-I)
              MOVE GV14-DREGLTVTE      TO  W-FC-R-DT-ESSEMT(W-FC-R-I)
              MOVE GV14-PREGLTVTE      TO  W-FC-R-MT(W-FC-R-I)
      *       MOVE SPACES              TO  W-FC-R-STAT(W-FC-R-I)
      *       MOVE '0'                 TO  W-FC-R-C-RET(W-FC-R-I)
      *       MOVE EC02-NSOCIETE       TO  W-FC-R-C-SOC(W-FC-R-I)
      *       MOVE EC02-NLIEU          TO  W-FC-R-C-LIEU(W-FC-R-I)
              ADD 1 TO W-FC-R-U
            END-IF .
      *
       FIN-MEF-GV14. EXIT.
       ALIM-GV14 SECTION.
      ********************
      * ON CHARGE AU PLUS 'W-FC-R-MAX' LIGNES ET ON STOPPE LE CURSEUR
      * EH OUI C'EST PAS BEAU MAIS CE CAS N'EST PAS CENSE SE PRODUIRE
      * EN TOUT CAS C'EST CE QUI A ETE DECIDE POUR L'INSTANT, APRES ...
            ADD 1 TO W-FC-R-I
            IF W-FC-R-I >  10
               MOVE 'O' TO FIN-GV14
            ELSE
              MOVE TS42-NSEQ           TO  W-FC-R-L-PMENT-SI(W-FC-R-I)
              MOVE TS42-CMODPAIMT      TO  W-FC-R-MD-PMENT(W-FC-R-I)
              MOVE TS42-NREGLTVTE      TO  W-FC-R-ID-PMENT(W-FC-R-I)
              MOVE TS42-NTRANS         TO  W-FC-R-NUM-ESSEMT(W-FC-R-I)
              MOVE TS42-DREGLTVTE      TO  W-FC-R-DT-ESSEMT(W-FC-R-I)
              MOVE TS42-PREGLTVTE      TO  W-FC-R-MT(W-FC-R-I)
      *       MOVE SPACES              TO  W-FC-R-STAT(W-FC-R-I)
      *       MOVE '0'                 TO  W-FC-R-C-RET(W-FC-R-I)
      *       MOVE EC02-NSOCIETE       TO  W-FC-R-C-SOC(W-FC-R-I)
      *       MOVE EC02-NLIEU          TO  W-FC-R-C-LIEU(W-FC-R-I)
              ADD 1 TO W-FC-R-U
            END-IF .
      *{ Tr-Empty-Sentence 1.6
      *     .
      *}
       FIN-ALIM-GV14. EXIT.
      *-----------------------------------------------------------------
       TRAIT-GV11 SECTION.
      *********************
           PERFORM DECLARE-GV11.
           PERFORM FETCH-GV11.
           PERFORM UNTIL FIN-GV11 = 'O'
              PERFORM MEF-GV11
              PERFORM FETCH-GV11
           END-PERFORM.
           PERFORM CLOSE-GV11.
      *.................................................................
       TRAIT-VE11 SECTION.
      *********************
           PERFORM DECLARE-VE11.
           PERFORM FETCH-VE11.
           PERFORM UNTIL FIN-VE11 = 'O'
              PERFORM MEF-GV11
              PERFORM FETCH-VE11
           END-PERFORM.
           PERFORM CLOSE-VE11.
      *.................................................................
       MEF-GV11 SECTION.
      ********************
      * ON CHARGE AU PLUS 'W-FC-V-MAX' LIGNES ET ON STOPPE LE CURSEUR
      * EH OUI C'EST PAS BEAU MAIS ... JE ME REPETE
           ADD 1 TO W-FC-V-I
           IF W-FC-V-I >  80
              MOVE 'O' TO FIN-GV11
           ELSE
              MOVE GV11-NCODICGRP   TO  W-FC-V-NCODICGRP(W-FC-V-I)
              MOVE GV11-NCODIC      TO  W-FC-V-NCODIC(W-FC-V-I)
              MOVE GV11-CENREG         TO  W-FC-V-CENREG(W-FC-V-I)
              MOVE GV11-DDELIV         TO  W-FC-V-DDELIV(W-FC-V-I)
              MOVE GV11-QVENDUE        TO  W-FC-V-QVENDUE(W-FC-V-I)
              MOVE GV11-CMODDEL        TO  W-FC-V-CMODDEL(W-FC-V-I)
              MOVE GV11-CPLAGE         TO  W-FC-V-CPLAGE(W-FC-V-I)
              MOVE GV11-NSEQNQ         TO  W-FC-V-NSEQNQ(W-FC-V-I)
      *       MOVE NSEQ-9-5            TO  W-FC-V-L-CMD-SI(W-FC-V-I)
      *       MOVE '0'                 TO W-FC-E-C-RET
              MOVE GV11-CTYPENREG      TO W-FC-V-CTYPENREG(W-FC-V-I)
              if gv11-ctypenreg = '3'
                 MOVE GV11-lcomment (1:8) TO W-FC-V-NPSE(W-FC-V-I)
              end-if
              MOVE GV11-WTOPELIVRE     TO W-FC-V-WTOPELIVRE(W-FC-V-I)
      *       MOVE GV11-NSOCLIVR       TO W-FC-V-NSOCLIVR(W-FC-V-I)
      *       MOVE GV11-NDEPOT         TO W-FC-V-NDEPOT(W-FC-V-I)
              IF GV11-WTOPELIVRE  = 'O'
                 MOVE GV11-DTOPE TO W-FC-V-DTOPE(W-FC-V-I)
              END-IF
              MOVE GV11-WEMPORTE       TO W-FC-V-WEMPORTE(W-FC-V-I)
              MOVE GV11-TAUXTVA        TO W-TVA
              MOVE W-TVA               TO W-FC-V-TAUXTVA(W-FC-V-I)
              MOVE GV11-DCREATION      TO W-FC-V-DCREATION(W-FC-V-I)
              MOVE GV11-HCREATION      TO W-FC-V-HCREATION(W-FC-V-I)
              MOVE GV11-DATENC         TO W-FC-V-DATENC(W-FC-V-I)
              MOVE GV11-DANNULATION    TO W-FC-V-DANNULATION(W-FC-V-I)
              MOVE GV11-NSOCP          TO W-FC-V-NSOCP(W-FC-V-I)
              MOVE GV11-NLIEUP         TO W-FC-V-NLIEUP(W-FC-V-I)
              MOVE GV11-NTRANS         TO W-FC-V-NTRANS(W-FC-V-I)
              MOVE GV11-NSEQREF        TO W-FC-V-NSEQREF(W-FC-V-I)
              MOVE GV11-PVTOTAL        TO W-FC-V-PVTOTAL(W-FC-V-I)
              MOVE GV11-PVUNIT         TO W-FC-V-PVUNIT (W-FC-V-I)
              MOVE GV11-TAUXTVA        TO W-FC-V-TAUXTVA(W-FC-V-I)
              MOVE SPACE               TO GA00-CFAM
              MOVE SPACE               TO GA00-CMARQ
              IF GV11-CTYPENREG = '1'
                 PERFORM SELECT-RTGA00
                 PERFORM SELECT-RTGA38
                 PERFORM SELECT-RTGA82
              END-IF
              IF GV11-CTYPENREG = '4'
                 PERFORM SELECT-RTPR00
              END-IF
              IF GA00-CFAM NOT = SPACE
                 PERFORM SELECT-RTGA14
              END-IF.
              IF GA00-CMARQ NOT = SPACE
                 PERFORM SELECT-RTGA22
              END-IF.
              ADD 1 TO W-FC-V-U.
      *
       FIN-MEF-GV11. EXIT.
       ALIM-GV11 SECTION.
      ********************
      * ON CHARGE AU PLUS 'W-FC-V-MAX' LIGNES ET ON STOPPE LE CURSEUR
      * EH OUI C'EST PAS BEAU MAIS ... JE ME REPETE
           ADD 1 TO W-FC-V-I
           IF W-FC-V-I >  80
              MOVE 'O' TO FIN-GV11
           ELSE
              MOVE TS42-NCODICGRP   TO  W-FC-V-NCODICGRP(W-FC-V-I)
              MOVE TS42-NCODIC      TO  W-FC-V-NCODIC(W-FC-V-I)
              MOVE TS42-CENREG         TO  W-FC-V-CENREG(W-FC-V-I)
              MOVE TS42-DDELIV         TO  W-FC-V-DDELIV(W-FC-V-I)
              MOVE TS42-QVENDUE        TO  W-FC-V-QVENDUE(W-FC-V-I)
              MOVE TS42-CMODDEL        TO  W-FC-V-CMODDEL(W-FC-V-I)
              MOVE SPACES              TO  W-FC-V-CPLAGE(W-FC-V-I)
              MOVE TS42-NSEQNQ         TO  W-FC-V-NSEQNQ(W-FC-V-I)
      *       MOVE NSEQ-9-5            TO  W-FC-V-L-CMD-SI(W-FC-V-I)
      *       MOVE '0'                 TO W-FC-E-C-RET
              MOVE TS42-CTYPENREG      TO W-FC-V-CTYPENREG(W-FC-V-I)
              IF TS42-CTYPENREG = '3'
                 move ts42-lcomment (1:8)
                                       to W-FC-V-NPSE(W-FC-V-I)
              END-IF
              MOVE 'O'                 TO W-FC-V-WTOPELIVRE(W-FC-V-I)
      *       MOVE GV11-NSOCLIVR       TO W-FC-V-NSOCLIVR(W-FC-V-I)
      *       MOVE GV11-NDEPOT         TO W-FC-V-NDEPOT(W-FC-V-I)
              MOVE TS42-DDELIV         TO W-FC-V-DTOPE(W-FC-V-I)
              MOVE SPACES              TO W-FC-V-WEMPORTE(W-FC-V-I)
              MOVE TS42-TAUXTVA        TO W-TVA
              MOVE W-TVA               TO W-FC-V-TAUXTVA(W-FC-V-I)
      *       EN 1ERE APPROXIMATION
              MOVE TS42-DDELIV         TO W-FC-V-DCREATION(W-FC-V-I)
              MOVE SPACES              TO W-FC-V-HCREATION(W-FC-V-I)
              MOVE SPACES              TO W-FC-V-DATENC(W-FC-V-I)
              MOVE TS42-DANNULATION    TO W-FC-V-DANNULATION(W-FC-V-I)
              MOVE SPACES              TO W-FC-V-NSOCP(W-FC-V-I)
              MOVE SPACES              TO W-FC-V-NLIEUP(W-FC-V-I)
              MOVE 0                   TO W-FC-V-NTRANS(W-FC-V-I)
              MOVE TS42-NSEQREF        TO W-FC-V-NSEQREF(W-FC-V-I)
              MOVE TS42-PVTOTAL        TO W-FC-V-PVTOTAL(W-FC-V-I)
              MOVE TS42-PVUNIT         TO W-FC-V-PVUNIT (W-FC-V-I)
              MOVE SPACE               TO GA00-CFAM
              IF TS42-CTYPENREG = '1'
                 MOVE W-FC-V-NCODIC(W-FC-V-I) TO GV11-NCODIC
                 MOVE W-FC-V-DCREATION(W-FC-V-I) TO GV11-DCREATION
                 PERFORM SELECT-RTGA00
                 PERFORM SELECT-RTGA38
              END-IF
              IF TS42-CTYPENREG = '4'
                 MOVE W-FC-V-CENREG(W-FC-V-I) TO GV11-NCODIC
                 MOVE W-FC-V-CENREG(W-FC-V-I) TO GV11-CENREG
                 PERFORM SELECT-RTPR00
              END-IF
              IF GA00-CFAM NOT = SPACE
                 PERFORM SELECT-RTGA14
              END-IF.
              IF GA00-CMARQ NOT = SPACE
                 PERFORM SELECT-RTGA22
              END-IF.
              ADD 1 TO W-FC-V-U.
      *
       FIN-ALIM-GV11. EXIT.
      *=================================================================
       SELECT-RTGA00 SECTION.
      ***********************
           EXEC SQL SELECT CFAM , LREFFOURN, CMARQ,
                           CTAUXTVA
           INTO
               :GA00-CFAM, :GA00-LREFFOURN, :GA00-CMARQ,
               :GA00-CTAUXTVA
                FROM RVGA0000
               WHERE NCODIC   = :GV11-NCODIC
           END-EXEC
           IF SQLCODE = ZERO
              MOVE GA00-CFAM TO W-FC-V-CFAM(W-FC-V-I)
              MOVE GA00-LREFFOURN TO W-FC-V-LREFFOURN(W-FC-V-I)
              MOVE GA00-CMARQ TO W-FC-V-CMARQ(W-FC-V-I)
           ELSE
              MOVE SPACE     TO W-FC-V-CFAM(W-FC-V-I)
              MOVE SPACE     TO W-FC-V-LREFFOURN(W-FC-V-I)
              MOVE SPACE     TO W-FC-V-CMARQ(W-FC-V-I)
              MOVE '02'      TO W-FC-E-C-RET
           END-IF.
       FIN-SELECT-RTGA00. EXIT.
      **************************
      *
       SELECT-RTPR00 SECTION.
      ***********************
           EXEC SQL SELECT CFAM , LPRESTATION, CMARQ
           INTO
               :GA00-CFAM, :PR00-LPRESTATION, :GA00-CMARQ
                FROM RVPR0001
               WHERE CPRESTATION = :GV11-CENREG
           END-EXEC
           IF SQLCODE = ZERO
              MOVE GA00-CFAM TO W-FC-V-CFAM(W-FC-V-I)
              MOVE PR00-LPRESTATION TO W-FC-V-LREFFOURN(W-FC-V-I)
              MOVE GA00-CMARQ TO W-FC-V-CMARQ(W-FC-V-I)
           ELSE
              MOVE SPACE     TO W-FC-V-CFAM(W-FC-V-I)
              MOVE SPACE          TO W-FC-V-LREFFOURN(W-FC-V-I)
              MOVE SPACE      TO W-FC-V-CMARQ(W-FC-V-I)
              MOVE '03'      TO W-FC-E-C-RET
           END-IF.
       FIN-SELECT-RTPR00. EXIT.
      **************************
      *
       SELECT-RTGA14 SECTION.
      ***********************
           EXEC SQL SELECT LFAM
           INTO
               :GA14-LFAM
                FROM RVGA1400
               WHERE CFAM     = :GA00-CFAM
           END-EXEC
           IF SQLCODE = ZERO
              MOVE GA14-LFAM TO W-FC-V-LFAM(W-FC-V-I)
           ELSE
              MOVE SPACE     TO W-FC-V-LFAM(W-FC-V-I)
              MOVE '04'      TO W-FC-E-C-RET
           END-IF.
       FIN-SELECT-RTGA14. EXIT.
      *
       SELECT-RTGA22 SECTION.
      ***********************
           EXEC SQL SELECT LMARQ
           INTO
               :GA22-LMARQ
                FROM RVGA2200
               WHERE CMARQ    = :GA00-CMARQ
           END-EXEC
           IF SQLCODE = ZERO
              MOVE GA22-LMARQ TO W-FC-V-LMARQ(W-FC-V-I)
           ELSE
              MOVE SPACE     TO W-FC-V-LMARQ(W-FC-V-I)
              MOVE '06'      TO W-FC-E-C-RET
           END-IF.
       FIN-SELECT-RTGA22. EXIT.
      **************************
      *
       SELECT-RTGA38 SECTION.
      ***********************
           MOVE GV11-NCODIC       TO GA38-NCODIC
           MOVE 0                TO GA38-PMONTANT
           MOVE GV11-DCREATION   TO GA38-DEFFET
           EXEC SQL
                SELECT PMONTANT
                INTO  :GA38-PMONTANT
                FROM   RVGA3800 A
                WHERE  NCODIC  = :GA38-NCODIC
                AND    WACHVEN = 'V'
                AND    CPAYS   = 'FR'
                AND    WENTCDE = 'R'
                AND    DEFFET  = (SELECT MAX(DEFFET)
                                  FROM   RVGA3800 Z
                                  WHERE  NCODIC = A.NCODIC
                                  AND    WACHVEN = 'V'
                                  AND    CPAYS   = 'FR'
                                  AND    WENTCDE = 'R'
                                  AND    DEFFET <= :GA38-DEFFET)
           END-EXEC
           IF SQLCODE = ZERO
              PERFORM CALCUL-TTC
              MOVE GA38-PMONTANT TO EDIT-GA38-MONTANT
      *       MOVE GA38-PMONTANT TO W-FC-V-PECO(W-FC-V-I)
              MOVE EDIT-GA38-MONTANT TO W-FC-V-PECO(W-FC-V-I)
           ELSE
              MOVE SPACE         TO W-FC-V-PECO(W-FC-V-I)
              MOVE '05'      TO W-FC-E-C-RET
           END-IF.
       FIN-SELECT-RTGA38. EXIT.
      **************************
       CALCUL-TTC   SECTION.
      **********************
           PERFORM VARYING I-TXTVA FROM 1 BY 1
                   UNTIL I-TXTVA > U-TXTVA
                   OR GA00-CTAUXTVA = T-TXTVA-TAUX(I-TXTVA)
           END-PERFORM
           IF GA00-CTAUXTVA = T-TXTVA-TAUX(I-TXTVA)
              IF GV11-DCREATION >= TXTVA-DATE
                 COMPUTE W-TXTVA-TVA   ROUNDED =
                         1 + (T-TXTVA-TVA(I-TXTVA) / 100)
                 COMPUTE 6-DECIMALES = GA38-PMONTANT
                                     * W-TXTVA-TVA
              ELSE
                 COMPUTE W-TXTVA-TVA   ROUNDED =
                         1 + (T-TXTVA-ANCTVA(I-TXTVA) / 100)
                 COMPUTE 6-DECIMALES = GA38-PMONTANT
                                     * W-TXTVA-TVA
              END-IF
              COMPUTE 5-DECIMALES ROUNDED = 6-DECIMALES
              COMPUTE 4-DECIMALES ROUNDED = 5-DECIMALES
              COMPUTE 3-DECIMALES ROUNDED = 4-DECIMALES
              COMPUTE GA38-PMONTANT ROUNDED = 3-DECIMALES
           END-IF.
       FIN-CALCUL-TTC. EXIT.
      **************************
       SELECT-RTGA82 SECTION.
           MOVE GV11-NCODIC      TO GA82-NCODIC
           MOVE 0                TO GA82-PMONTANT
           MOVE GV11-DCREATION   TO GA82-DEFFET
           EXEC SQL
                SELECT GA82.PMONTANT
                   INTO :GA82-PMONTANT
                  FROM RVGA8200 GA82
                  , RVGA5510 GA55
                     WHERE :GA82-DEFFET
                            BETWEEN DEFFET AND DFINEFFET
                     AND GA82.CTAXE = 'MOB'
                     AND GA82.CPAYS = 'FR'
                     AND GA55.WENTCDE = 'R'
                     and GA55.NENTCDE = GA82.NENTCDE
                     AND GA55.NCODIC  = GA82.NCODIC
                     and GA82.ncodic  = :GA82-ncodic
           END-EXEC.
           IF SQLCODE = ZERO
              MOVE GA82-PMONTANT TO GA38-PMONTANT
              PERFORM CALCUL-TTC
              MOVE GA38-PMONTANT TO EDIT-GA38-MONTANT
              MOVE EDIT-GA38-MONTANT TO W-FC-V-ECOMOB(W-FC-V-I)
           ELSE
              MOVE SPACE         TO W-FC-V-ECOMOB(W-FC-V-I)
              MOVE '05'      TO W-FC-E-C-RET
           END-IF.
       F-SELECT-RTGA82. EXIT.
      *
       SORTIE SECTION.
      ****************
      *{ normalize-exec-xx 1.5
      *    EXEC CICS RETURN END-EXEC.
      *--
           EXEC CICS RETURN
           END-EXEC.
      *}
      *
      *=================================================================
      *=================================================================
      * Ent�te de Vente
       SELECT-GV10 SECTION.
           INITIALIZE RVGV1099          .
           SET NORMAL       TO TRUE     .
           MOVE 'GV10'      TO TAB-DISP .
           MOVE 'SELECT '   TO ORD-DISP .
           MOVE W-FC-E-NSOC TO GV10-NSOCIETE
           MOVE W-FC-E-NLIEU TO GV10-NLIEU
           MOVE W-FC-E-NVENTE TO GV10-NVENTE
           EXEC SQL SELECT NSOCIETE, NLIEU, NVENTE, DVENTE, DHVENTE,
                           DMVENTE,
                           PTTVENTE, PCOMPT, PLIVR, PDIFFERE,
                           PRFACT, DMODIFVTE, WFACTURE, WEXPORT,
                           WDETAXEC, WDETAXEHC, LDESCRIPTIF1,
                           LDESCRIPTIF2,
                           LCOMVTE1, LCOMVTE2, LCOMVTE3, LCOMVTE4
                      INTO :GV10-NSOCIETE, :GV10-NLIEU, :GV10-NVENTE,
                           :GV10-DVENTE, :GV10-DHVENTE, :GV10-DMVENTE ,
                           :GV10-PTTVENTE,
                           :GV10-PCOMPT, :GV10-PLIVR, :GV10-PDIFFERE,
                           :GV10-PRFACT, :GV10-DMODIFVTE,
                           :GV10-WFACTURE, :GV10-WEXPORT,
                           :GV10-WDETAXEC, :GV10-WDETAXEHC,
                           :GV10-LDESCRIPTIF1, :GV10-LDESCRIPTIF2,
                                         :GV10-LCOMVTE1,
                           :GV10-LCOMVTE2, :GV10-LCOMVTE3,
                           :GV10-LCOMVTE4
                      FROM RVGV1099
                     WHERE NSOCIETE = :GV10-NSOCIETE
                       AND NLIEU    = :GV10-NLIEU
                       AND NVENTE   = :GV10-NVENTE
           END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       SELECT-VE10 SECTION.
           INITIALIZE RVVE1000          .
           SET NORMAL       TO TRUE     .
           MOVE 'VE10'      TO TAB-DISP .
           MOVE 'SELECT '   TO ORD-DISP .
           MOVE W-FC-E-NSOC TO GV10-NSOCIETE
           MOVE W-FC-E-NLIEU TO GV10-NLIEU
           MOVE W-FC-E-NVENTE TO GV10-NVENTE
           EXEC SQL SELECT NSOCIETE, NLIEU, NVENTE, DVENTE, DHVENTE,
                           DMVENTE ,
                           PTTVENTE, PCOMPT, PLIVR, PDIFFERE,
                           PRFACT, DMODIFVTE, WFACTURE, WEXPORT,
                           WDETAXEC, WDETAXEHC, LDESCRIPTIF1,
                           LDESCRIPTIF2,         NORDRE
                           LCOMVTE1, LCOMVTE2, LCOMVTE3, LCOMVTE4
                      INTO :GV10-NSOCIETE, :GV10-NLIEU, :GV10-NVENTE,
                           :GV10-DVENTE, :GV10-DHVENTE, :GV10-DMVENTE ,
                           :GV10-PTTVENTE,
                           :GV10-PCOMPT, :GV10-PLIVR, :GV10-PDIFFERE,
                           :GV10-PRFACT, :GV10-DMODIFVTE,
                           :GV10-WFACTURE, :GV10-WEXPORT,
                           :GV10-WDETAXEC, :GV10-WDETAXEHC,
                           :GV10-LDESCRIPTIF1, :GV10-LDESCRIPTIF2,
                                         :GV10-NORDRE, :GV10-LCOMVTE1,
                           :GV10-LCOMVTE2, :GV10-LCOMVTE3,
                           :GV10-LCOMVTE4
                      FROM RVVE1000
                     WHERE NSOCIETE = :GV10-NSOCIETE
                       AND NLIEU    = :GV10-NLIEU
                       AND NVENTE   = :GV10-NVENTE
           END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       SELECT-GV02-A SECTION.
           INITIALIZE RVGV0202          .
           MOVE W-FC-E-NSOC   TO GV02-NSOCIETE
           MOVE W-FC-E-NLIEU  TO GV02-NLIEU
           MOVE W-FC-E-NVENTE TO GV02-NVENTE
           SET NORMAL       TO TRUE     .
           MOVE 'GV02'      TO TAB-DISP .
           MOVE 'SELECT1'   TO ORD-DISP .
           EXEC SQL SELECT CTITRENOM, LNOM , LPRENOM, LBATIMENT,
                           LESCALIER, LETAGE, LPORTE, LCOMMUNE,
                           CPOSTAL, TELDOM, TELBUR, NGSM, CVOIE,
                           CTVOIE, LNOMVOIE, LCMPAD1,
                           LCMPAD2, LCOMLIV1, LCOMLIV2, WETRANGER,
                           CINSEE, CPAYS, IDCLIENT
                      INTO :GV02-CTITRENOM, :GV02-LNOM,
                           :GV02-LPRENOM, :GV02-LBATIMENT,
                           :GV02-LESCALIER, :GV02-LETAGE,
                           :GV02-LPORTE, :GV02-LCOMMUNE,
                           :GV02-CPOSTAL, :GV02-TELDOM, :GV02-TELBUR,
                           :GV02-NGSM, :GV02-CVOIE, :GV02-CTVOIE,
                           :GV02-LNOMVOIE, :GV02-LCMPAD1,
                           :GV02-LCMPAD2, :GV02-LCOMLIV1,
                           :GV02-LCOMLIV2, :GV02-WETRANGER,
                           :GV02-CINSEE, :GV02-CPAYS
                           , :GV02-IDCLIENT
                      FROM RVGV0202
                     WHERE NSOCIETE = :GV02-NSOCIETE
                       AND NLIEU    = :GV02-NLIEU
                       AND NVENTE   = :GV02-NVENTE
                       AND WTYPEADR = 'A'
           END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       SELECT-GV02-B SECTION.
           INITIALIZE RVGV0202 .
           MOVE W-FC-E-NSOC   TO GV02-NSOCIETE
           MOVE W-FC-E-NLIEU  TO GV02-NLIEU
           MOVE W-FC-E-NVENTE TO GV02-NVENTE
           SET NORMAL       TO TRUE     .
           MOVE 'GV02'      TO TAB-DISP .
           MOVE 'SELECT2'   TO ORD-DISP .
           EXEC SQL SELECT CTITRENOM, LNOM , LPRENOM, LBATIMENT,
                           LESCALIER, LETAGE, LPORTE, LCOMMUNE,
                           CPOSTAL, TELDOM, TELBUR, NGSM, CVOIE,
                           CTVOIE, LNOMVOIE, LCMPAD1,
                           LCMPAD2, LCOMLIV1, LCOMLIV2, WETRANGER,
                           CINSEE, CPAYS, IDCLIENT
                      INTO :GV02-CTITRENOM, :GV02-LNOM,
                           :GV02-LPRENOM, :GV02-LBATIMENT,
                           :GV02-LESCALIER, :GV02-LETAGE,
                           :GV02-LPORTE, :GV02-LCOMMUNE,
                           :GV02-CPOSTAL, :GV02-TELDOM, :GV02-TELBUR,
                           :GV02-NGSM, :GV02-CVOIE, :GV02-CTVOIE,
                           :GV02-LNOMVOIE, :GV02-LCMPAD1,
                           :GV02-LCMPAD2, :GV02-LCOMLIV1,
                           :GV02-LCOMLIV2, :GV02-WETRANGER,
                           :GV02-CINSEE, :GV02-CPAYS
                           , :GV02-IDCLIENT
                      FROM RVGV0202
                     WHERE NSOCIETE = :GV02-NSOCIETE
                       AND NLIEU    = :GV02-NLIEU
                       AND NVENTE   = :GV02-NVENTE
                       AND WTYPEADR = 'B'
           END-EXEC.
           PERFORM TEST-RET-DB2.
      *
       SELECT-VE02-A SECTION.
           INITIALIZE RVGV0202          .
           MOVE W-FC-E-NSOC   TO GV02-NSOCIETE
           MOVE W-FC-E-NLIEU  TO GV02-NLIEU
           MOVE W-FC-E-NVENTE TO GV02-NVENTE
           SET NORMAL       TO TRUE     .
           MOVE 'VE02'      TO TAB-DISP .
           MOVE 'SELECT1'   TO ORD-DISP .
           EXEC SQL SELECT CTITRENOM, LNOM , LPRENOM, LBATIMENT,
                           LESCALIER, LETAGE, LPORTE, LCOMMUNE,
                           CPOSTAL, TELDOM, TELBUR, NGSM, CVOIE,
                           CTVOIE, LNOMVOIE, LCMPAD1,
                           LCMPAD2, LCOMLIV1, LCOMLIV2, WETRANGER,
                           CINSEE, CPAYS, IDCLIENT
                      INTO :GV02-CTITRENOM, :GV02-LNOM,
                           :GV02-LPRENOM, :GV02-LBATIMENT,
                           :GV02-LESCALIER, :GV02-LETAGE,
                           :GV02-LPORTE, :GV02-LCOMMUNE,
                           :GV02-CPOSTAL, :GV02-TELDOM, :GV02-TELBUR,
                           :GV02-NGSM, :GV02-CVOIE, :GV02-CTVOIE,
                           :GV02-LNOMVOIE, :GV02-LCMPAD1,
                           :GV02-LCMPAD2, :GV02-LCOMLIV1,
                           :GV02-LCOMLIV2, :GV02-WETRANGER,
                           :GV02-CINSEE, :GV02-CPAYS
                           , :GV02-IDCLIENT
                      FROM RVVE0200
                     WHERE NSOCIETE = :GV02-NSOCIETE
                       AND NLIEU    = :GV02-NLIEU
                       AND NVENTE   = :GV02-NVENTE
                       AND WTYPEADR = 'A'
           END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       SELECT-VE02-B SECTION.
           INITIALIZE RVGV0202 .
           MOVE W-FC-E-NSOC   TO GV02-NSOCIETE
           MOVE W-FC-E-NLIEU  TO GV02-NLIEU
           MOVE W-FC-E-NVENTE TO GV02-NVENTE
           SET NORMAL       TO TRUE     .
           MOVE 'VE02'      TO TAB-DISP .
           MOVE 'SELECT2'   TO ORD-DISP .
           EXEC SQL SELECT CTITRENOM, LNOM , LPRENOM, LBATIMENT,
                           LESCALIER, LETAGE, LPORTE, LCOMMUNE,
                           CPOSTAL, TELDOM, TELBUR, NGSM, CVOIE,
                           CTVOIE, LNOMVOIE, LCMPAD1,
                           LCMPAD2, LCOMLIV1, LCOMLIV2, WETRANGER,
                           CINSEE, CPAYS, IDCLIENT
                      INTO :GV02-CTITRENOM, :GV02-LNOM,
                           :GV02-LPRENOM, :GV02-LBATIMENT,
                           :GV02-LESCALIER, :GV02-LETAGE,
                           :GV02-LPORTE, :GV02-LCOMMUNE,
                           :GV02-CPOSTAL, :GV02-TELDOM, :GV02-TELBUR,
                           :GV02-NGSM, :GV02-CVOIE, :GV02-CTVOIE,
                           :GV02-LNOMVOIE, :GV02-LCMPAD1,
                           :GV02-LCMPAD2, :GV02-LCOMLIV1,
                           :GV02-LCOMLIV2, :GV02-WETRANGER,
                           :GV02-CINSEE, :GV02-CPAYS
                           , :GV02-IDCLIENT
                      FROM RVVE0200
                     WHERE NSOCIETE = :GV02-NSOCIETE
                       AND NLIEU    = :GV02-NLIEU
                       AND NVENTE   = :GV02-NVENTE
                       AND WTYPEADR = 'B'
           END-EXEC.
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       DECLARE-GV14 SECTION.
           MOVE W-FC-E-NSOC    TO GV14-NSOCIETE
           MOVE W-FC-E-NLIEU   TO GV14-NLIEU
           MOVE W-FC-E-NVENTE  TO GV14-NVENTE
           EXEC SQL DECLARE GV14 CURSOR FOR
                     SELECT NSEQ, CMODPAIMT, NREGLTVTE, NTRANS,
                            DREGLTVTE, PREGLTVTE
                       FROM RVGV1402
                      WHERE NSOCIETE = :GV14-NSOCIETE
                        AND NLIEU    = :GV14-NLIEU
                        AND NVENTE   = :GV14-NVENTE
           END-EXEC.
           MOVE 'GV14'      TO TAB-DISP .
           MOVE 'OPEN   '   TO ORD-DISP .
           EXEC SQL OPEN GV14 END-EXEC.
           PERFORM TEST-RET-DB2.
      *
       FETCH-GV14 SECTION.
           MOVE 'GV14'      TO TAB-DISP .
           MOVE 'FETCH  '   TO ORD-DISP .
           SET NORMAL       TO TRUE .
           EXEC SQL FETCH   GV14
                    INTO   :GV14-NSEQ           ,
                           :GV14-CMODPAIMT      ,
                           :GV14-NREGLTVTE      ,
                           :GV14-NTRANS         ,
                           :GV14-DREGLTVTE      ,
                           :GV14-PREGLTVTE
           END-EXEC.
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 'O' TO FIN-GV14
           END-IF .
      *
       CLOSE-GV14 SECTION.
           MOVE 'GV14'      TO TAB-DISP .
           MOVE 'CLOSE  '   TO ORD-DISP .
           EXEC SQL CLOSE   GV14 END-EXEC .
           PERFORM TEST-RET-DB2.
      *
       DECLARE-VE14 SECTION.
           MOVE W-FC-E-NSOC    TO GV14-NSOCIETE
           MOVE W-FC-E-NLIEU   TO GV14-NLIEU
           MOVE W-FC-E-NVENTE  TO GV14-NVENTE
           EXEC SQL DECLARE VE14 CURSOR FOR
                     SELECT NSEQ, CMODPAIMT, NREGLTVTE, NTRANS,
                            DREGLTVTE, PREGLTVTE
                       FROM RVVE1400
                      WHERE NSOCIETE = :GV02-NSOCIETE
                        AND NLIEU    = :GV02-NLIEU
                        AND NVENTE   = :GV02-NVENTE
           END-EXEC.
           MOVE 'VE14'      TO TAB-DISP .
           MOVE 'OPEN   '   TO ORD-DISP .
           EXEC SQL OPEN VE14 END-EXEC.
           PERFORM TEST-RET-DB2.
      *
       FETCH-VE14 SECTION.
           MOVE 'VE14'      TO TAB-DISP .
           MOVE 'FETCH  '   TO ORD-DISP .
           SET NORMAL       TO TRUE .
           EXEC SQL FETCH   VE14
                    INTO   :GV14-NSEQ           ,
                           :GV14-CMODPAIMT      ,
                           :GV14-NREGLTVTE      ,
                           :GV14-NTRANS         ,
                           :GV14-DREGLTVTE      ,
                           :GV14-PREGLTVTE
           END-EXEC.
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 'O' TO FIN-VE14
           END-IF .
      *
       CLOSE-VE14 SECTION.
           MOVE 'VE14'      TO TAB-DISP .
           MOVE 'CLOSE  '   TO ORD-DISP .
           EXEC SQL CLOSE   VE14 END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       DECLARE-GV11 SECTION.
           MOVE W-FC-E-NSOC    TO GV11-NSOCIETE
           MOVE W-FC-E-NLIEU   TO GV11-NLIEU
           MOVE W-FC-E-NVENTE  TO GV11-NVENTE
           EXEC SQL DECLARE GV11 CURSOR FOR
                     SELECT NCODIC, CENREG, DDELIV, QVENDUE,
                            PVUNIT, CMODDEL, NSEQ, NSEQNQ,
                            CPLAGE, NCODICGRP, NORDRE, CTYPENREG,
                            WCQERESF, WTOPELIVRE, NSOCLIVR, NDEPOT,
                            DTOPE , PVTOTAL, WEMPORTE, TAUXTVA,
                            DCREATION, HCREATION, DATENC,
                            DANNULATION, NSOCP, NLIEUP, NTRANS,
                            NSEQREF, lcomment
                       FROM RVGV1105
                      WHERE NSOCIETE    = :GV11-NSOCIETE
                        AND NLIEU       = :GV11-NLIEU
                        AND NVENTE      = :GV11-NVENTE
           END-EXEC.
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'OPEN   '   TO ORD-DISP .
           EXEC SQL OPEN   GV11 END-EXEC.
           PERFORM TEST-RET-DB2.
      *
       FETCH-GV11 SECTION.
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'FETCH  '   TO ORD-DISP .
           SET NORMAL       TO TRUE .
           EXEC SQL FETCH GV11 INTO :GV11-NCODIC, :GV11-CENREG,
                         :GV11-DDELIV, :GV11-QVENDUE,
                         :GV11-PVUNIT, :GV11-CMODDEL, :GV11-NSEQ,
                         :GV11-NSEQNQ, :GV11-CPLAGE,
                         :GV11-NCODICGRP, :GV11-NORDRE,
                         :GV11-CTYPENREG, :GV11-WCQERESF,
                         :GV11-WTOPELIVRE, :GV11-NSOCLIVR, :GV11-NDEPOT
                         , :GV11-DTOPE, :GV11-PVTOTAL , :GV11-WEMPORTE
                         , :GV11-TAUXTVA, :GV11-DCREATION,
                         :GV11-HCREATION, :GV11-DATENC,
                         :GV11-DANNULATION, :GV11-NSOCP, :GV11-NLIEUP,
                         :GV11-NTRANS, :GV11-NSEQREF
                         , :gv11-lcomment
           END-EXEC.
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 'O' TO FIN-GV11
           END-IF .
      *
       CLOSE-GV11 SECTION.
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'CLOSE  '   TO ORD-DISP .
           EXEC SQL CLOSE   GV11
           END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       DECLARE-VE11 SECTION.
           MOVE W-FC-E-NSOC    TO GV11-NSOCIETE
           MOVE W-FC-E-NLIEU   TO GV11-NLIEU
           MOVE W-FC-E-NVENTE  TO GV11-NVENTE
           EXEC SQL DECLARE VE11 CURSOR FOR
                     SELECT NCODIC, CENREG, DDELIV, QVENDUE,
                            PVUNIT, CMODDEL, NSEQ, NSEQNQ,
                            CPLAGE, NCODICGRP, NORDRE, CTYPENREG,
                            WCQERESF, WTOPELIVRE, NSOCLIVR, NDEPOT,
                            DTOPE , PVTOTAL, WEMPORTE, TAUXTVA,
                            DCREATION, HCREATION, DATENC,
                            DANNULATION, NSOCP, NLIEUP, NTRANS,
                            NSEQREF
                       FROM RVVE1100
                      WHERE NSOCIETE    = :GV11-NSOCIETE
                        AND NLIEU       = :GV11-NLIEU
                        AND NVENTE      = :GV11-NVENTE
           END-EXEC.
           MOVE 'VE11'      TO TAB-DISP .
           MOVE 'OPEN   '   TO ORD-DISP .
           EXEC SQL OPEN   VE11 END-EXEC.
           PERFORM TEST-RET-DB2.
      *
       FETCH-VE11 SECTION.
           MOVE 'VE11'      TO TAB-DISP .
           MOVE 'FETCH  '   TO ORD-DISP .
           SET NORMAL       TO TRUE .
           EXEC SQL FETCH VE11 INTO :GV11-NCODIC, :GV11-CENREG,
                         :GV11-DDELIV, :GV11-QVENDUE,
                         :GV11-PVUNIT, :GV11-CMODDEL, :GV11-NSEQ,
                         :GV11-NSEQNQ, :GV11-CPLAGE,
                         :GV11-NCODICGRP, :GV11-NORDRE,
                         :GV11-CTYPENREG, :GV11-WCQERESF,
                         :GV11-WTOPELIVRE, :GV11-NSOCLIVR, :GV11-NDEPOT
                         , :GV11-DTOPE, :GV11-PVTOTAL , :GV11-WEMPORTE
                         , :GV11-TAUXTVA, :GV11-DCREATION,
                         :GV11-HCREATION, :GV11-DATENC,
                         :GV11-DANNULATION, :GV11-NSOCP, :GV11-NLIEUP,
                         :GV11-NTRANS, :GV11-NSEQREF
           END-EXEC.
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 'O' TO FIN-VE11
           END-IF .
      *
       CLOSE-VE11 SECTION.
           MOVE 'VE11'      TO TAB-DISP .
           MOVE 'CLOSE  '   TO ORD-DISP .
           EXEC SQL CLOSE   VE11
           END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      ******************************************************************
      * tableau taux de tva Fr
      ******************************************************************
       DECLARE-TXTVA SECTION.
           MOVE 'RVGA01HX'          TO TAB-DISP
           MOVE 'OPEN   '           TO ORD-DISP
           EXEC SQL DECLARE CTXTVA CURSOR FOR
                     SELECT CTABLEG2, WTABLEG
                       FROM RVGA01HX
           END-EXEC
           EXEC SQL OPEN CTXTVA END-EXEC
           PERFORM TEST-RET-DB2.
       FETCH-TXTVA SECTION.
           MOVE 'RVGA01HX'          TO TAB-DISP
           MOVE 'FETCH  '           TO ORD-DISP
           EXEC SQL
              FETCH  CTXTVA
               INTO :TXTVA-CTABLEG2, :TXTVA-WTABLEG
           END-EXEC
           PERFORM TEST-RET-DB2.
       AJOUT-TXTVA SECTION.
           ADD 1 TO U-TXTVA.
           IF U-TXTVA > M-TXTVA
              MOVE 'TABLEAU TXTVA TROP PETIT' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
           MOVE TXTVA-TAUX   TO T-TXTVA-TAUX(U-TXTVA).
           MOVE TXTVA-TVA    TO T-TXTVA-TVA(U-TXTVA).
           MOVE TXTVA-ANCTVA TO T-TXTVA-ANCTVA(U-TXTVA).
      
       CLOSE-TXTVA SECTION.
           MOVE 'RVGA01HX'          TO TAB-DISP
           MOVE 'CLOSE  '           TO ORD-DISP
           EXEC SQL CLOSE CTXTVA END-EXEC
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       LECTURE-TS SECTION.
      *---------------------------------------
      *
           MOVE COMM-BS42-IDENT-TS     TO IDENT-TS
           MOVE LENGTH OF TS42-DONNEES TO LONG-TS
           PERFORM READ-TS
           IF TROUVE
              SET PAS-FIN-TS TO TRUE
              MOVE Z-INOUT               TO TS42-DONNEES
              ADD 1 TO RANG-TS
           ELSE
              SET FIN-TS TO TRUE
           END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-LECTURE-TS. EXIT.
      *
      *
      *-----------------------------------------------------------------
      *-----------------------------------------------------------------
       TEST-RET-DB2 SECTION.
           MOVE SQLCODE     TO SQL-DISP
           IF SQLCODE <  0
              MOVE '-1'     TO W-FC-E-C-RET
              MOVE '-1'     TO W-FACTURE-CLIENT-CRET
      *       MOVE DB2-DISP TO W-FC-MESSAGE
              PERFORM SORTIE
           ELSE
              IF SQLCODE = +100
                 SET NON-TROUVE TO TRUE
              END-IF
           END-IF.
       FIN-TEST-RET-DB2. EXIT.
      *********************************************
      *TEMP01-COPY SECTION. CONTINUE.
      *                       COPY SYKCTSDE.
      *TEMP02-COPY SECTION. CONTINUE. COPY SYKCTSRD.
      *TEMP03-COPY SECTION. CONTINUE. COPY SYKCTSRW.
      *TEMP04-COPY SECTION. CONTINUE. COPY SYKCTSWR.
       DELETE-TS                  SECTION.
           EXEC CICS DELETEQ TS
                              QUEUE    (IDENT-TS)
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   EIB-NORMAL
                MOVE 0 TO CODE-RETOUR
           ELSE
                IF   EIB-QIDERR
                     MOVE 1 TO CODE-RETOUR
                ELSE
                     MOVE '-1'     TO W-FC-E-C-RET
                     MOVE '-1'     TO W-FACTURE-CLIENT-CRET
                END-IF
           END-IF.
       FIN-DELETE-TS. EXIT.
       READ-TS                    SECTION.
           EXEC CICS READQ TS
                              QUEUE    (IDENT-TS)
                              INTO     (Z-INOUT)
                              LENGTH   (LONG-TS)
                              ITEM     (RANG-TS)
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   EIB-NORMAL
                MOVE 0 TO CODE-RETOUR
           ELSE
                IF   EIB-QIDERR OR EIB-ITEMERR
                     MOVE 1 TO CODE-RETOUR
                ELSE
                     MOVE '-1'     TO W-FC-E-C-RET
                     MOVE '-1'     TO W-FACTURE-CLIENT-CRET
                END-IF
           END-IF.
       FIN-READ-TS. EXIT.
       REWRITE-TS                 SECTION.
           EXEC CICS WRITEQ TS
                              QUEUE    (IDENT-TS)
                              FROM     (Z-INOUT)
                              LENGTH   (LONG-TS)
                              ITEM     (RANG-TS)
                              REWRITE
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   NOT EIB-NORMAL
                     MOVE '-1'     TO W-FC-E-C-RET
                     MOVE '-1'     TO W-FACTURE-CLIENT-CRET
           END-IF.
       FIN-REWRITE-TS. EXIT.
       WRITE-TS                   SECTION.
           EXEC CICS WRITEQ TS
                              QUEUE    (IDENT-TS)
                              FROM     (Z-INOUT)
                              LENGTH   (LONG-TS)
                              ITEM     (RANG-TS)
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   NOT EIB-NORMAL
                     MOVE '-1'     TO W-FC-E-C-RET
                     MOVE '-1'     TO W-FACTURE-CLIENT-CRET
           END-IF.
       FIN-WRITE-TS. EXIT.
      *
       RECH-PARAM-VERSION             SECTION.
      ***************************************************************
      * LECTURE DU PARAMETRE NOUVELLE VERSION
      ***************************************************************
      * PAR DEFAUT SI NON TROUVE : NOUVELLE VERSION
      * PAS DE GESTION DE LA TSTENVOI, VERSION TEMPORAIRE
           MOVE  'O' TO W-NOUVELLE-VERSION
           MOVE  LOW-VALUES TO   WS-TYPENVOI
           MOVE  '0' TO WF-TSTENVOI
           PERFORM VARYING WP-RANG-TSTENVOI  FROM 1 BY 1
             UNTIL  WC-TSTENVOI-FIN
              PERFORM LECTURE-TSTENVOI
              IF  WC-TSTENVOI-SUITE
              AND TSTENVOI-NSOCZP    = '00000'
              AND TSTENVOI-CTRL       = 'FACT'
                 MOVE TSTENVOI-WPARAM(1:1) TO W-NOUVELLE-VERSION
              END-IF
           END-PERFORM.
           DISPLAY 'FACTURE : NELLE VERSION = '  W-NOUVELLE-VERSION
           .
       FIN-RECH-PARAM-VERSION. EXIT.
       LECTURE-TSTENVOI SECTION.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS
      *         READQ TS
      *         QUEUE  (WC-ID-TSTENVOI)
      *         INTO   (TS-TSTENVOI-DONNEES)
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
      *         ITEM   (WP-RANG-TSTENVOI)
      *         NOHANDLE
      *    END-EXEC.
      *--
           EXEC CICS READQ TS
                QUEUE  (WC-ID-TSTENVOI)
                INTO   (TS-TSTENVOI-DONNEES)
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
                ITEM   (WP-RANG-TSTENVOI)
                NOHANDLE
           END-EXEC.
      *}
           EVALUATE EIBRESP
           WHEN 0
              SET WC-TSTENVOI-SUITE      TO TRUE
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE
           WHEN 26
           WHEN 44
              SET WC-TSTENVOI-FIN        TO TRUE
           WHEN OTHER
              SET WC-TSTENVOI-ERREUR     TO TRUE
           END-EVALUATE.
       FIN-LECT-TSTENVOI. EXIT.
      *
      
