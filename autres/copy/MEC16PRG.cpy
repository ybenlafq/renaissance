      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:07 >
      *****************************************************************
      *  PROGRAMME ECRIT A PARTIR DU MECFC
      *  CURSEUR GV11 GARDE SOUS LE COUDE AINSI QUE TSTENVOI
      *****************************************************************
      *    CODE CONSIGNE PASSE DE 4 A 5
      *****************************************************************
      *--> GESTION  DB2
       01  DB2-DISP.
      *         LG = 58 ( LONGUEUR DU MESSAGE VERS PGM APPELANT )
           05  FILLER    PIC X(12) VALUE 'PB DB2 PGM: '.
           05  PGM-DISP  PIC X(6) .
           05  FILLER    PIC X(8) VALUE ' ,CODE: '.
           05  RET-DISP  PIC 9(1) .
           05  FILLER    PIC X(6) VALUE ',SQL: '.
           05  SQL-DISP  PIC ++++9 .
           05  FILLER    PIC X(8) VALUE ',ORDRE: '.
           05  ORD-DISP  PIC X(7) .
           05  FILLER    PIC X(1) VALUE '-'.
           05  TAB-DISP  PIC X(4) .
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  W-SPDATDB2-RC                 PIC S9(4)  COMP.
      *--
       01  W-SPDATDB2-RC                 PIC S9(4) COMP-5.
      *}
       01  W-SPDATDB2-DSYST              PIC S9(13) COMP-3 VALUE ZERO.
      *--> WORKING  POUR CALCUL PRIX PRODUIT QUAND IL EXISTE 1 REMISE
       01  ZONE-CALCUL .
           05  PRIX-CALCULE  PIC S9(5) COMP-3.
           05  REMISE-UNIT   PIC S9(7)V9(0002) COMP-3 .
           05  REMISE-TOT    PIC S9(7)V9(0002) COMP-3 .
      * ZONES DE WORKING POUR SOUS-TABLE CONSI
       01  W-CONSIGNE.
           05  W-EVENEMENT   PIC X(15) VALUE SPACES.
           05  W-CHOIX       PIC X(10) VALUE SPACES.
           05  W-UPDEC04     PIC X(01) VALUE SPACES.
           05  W-ENVOIAS400  PIC X(01) VALUE SPACES.
           05  FILLER        PIC X(08) VALUE SPACES.
           05  W-ENVOIDCOM   PIC X(01) VALUE SPACES.
           05  W-ENVOILOGI   PIC X(01) VALUE SPACES.
           05  FILLER        PIC X(43) VALUE SPACES.
      * FORMATER LE GV11-NSEQ DE X(2) -->  S9(5) COMP-3
       01  NSEQ-X-5.
           05  FILLER    PIC X(3) VALUE '000'.
           05  NSEQ-X-2  PIC X(2) VALUE '00'.
       01  NSEQ-9-5 REDEFINES NSEQ-X-5 PIC 9(5) .
      * DERNIERE POSITION DU TABLEAU VENTE
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  POSMAX    PIC S9(4) BINARY .
      *--
       01  POSMAX    PIC S9(4) COMP-5 .
      *}
      *
      *         RESULTATS REQUETES DB2
      *01  RESULT        PIC X .
      *    88 NORMAL           VALUE '0'.
      *    88 NON-TROUVE       VALUE '1'.
       01  WS-TAB-CLIENT.
         02  WS-TAB-CLIENT-OK OCCURS 2.
           03  WS-TOP-CLIENT        PIC 9                 VALUE 1.
             88 WS-IL-Y-A-CLIENT                     VALUE 1.
             88 WS-PAS-DE-CLIENT                     VALUE 0.
       01  WS-TOP-LECTURE-TS         PIC 9                 VALUE ZERO.
           88 PAS-FIN-TS                           VALUE 1.
           88 FIN-TS                               VALUE 0.
      *         FIN DES CURSEURS
       01  FIN-GV11 PIC X VALUE 'N' .
       01  FIN-VE11 PIC X VALUE 'N' .
       01  FIN-GV14 PIC X VALUE 'N' .
       01  FIN-VE14 PIC X VALUE 'N' .
       01  W-TVA                  PIC ZZV99.
       01  EDIT-GA38-MONTANT      PIC 99999,99.
       01  W-GV02                 PIC 9.
      * POUR DISPLAY DANS LA LOG
       01  W-MESSAGE-S.
           02  FILLER      PIC X(5) VALUE 'MEC16'.
           02  W-MESSAGE-H PIC X(2).
           02  FILLER      PIC X(1) VALUE ':'.
           02  W-MESSAGE-M PIC X(2).
           02  FILLER      PIC X(1) VALUE ' '.
           02  W-MESSAGES  PIC X(69).
       01  W-EIBTIME        PIC 9(7).
       01  FILLER REDEFINES W-EIBTIME.
           02  FILLER    PIC X.
           02  W-HEURE   PIC XX.
           02  W-MINUTE  PIC XX.
           02  W-SECONDE PIC XX.
       01  W-NOUVELLE-VERSION PIC X.
      * ZONES DIVERSES
      *01  ZONES-AIDA.
      *     02  LONG-TS              PIC S9(4) COMP VALUE +0.
      *     02  RANG-TS              PIC S9(4) COMP VALUE +0.
      *     02  IDENT-TS            PIC X(8)  VALUE SPACE.
      *
      *     02  CODE-RETOUR PIC X(1)           VALUE '0'.
      *       88  TROUVE                     VALUE '0'.
      *       88  NORMAL                     VALUE '0'.
      *       88  DATE-OK                    VALUE '0'.
      *       88  SELECTE                    VALUE '0'.
      *       88  NON-TROUVE                 VALUE '1'.
      *       88  NON-SELECTE                VALUE '1'.
      *       88  ANORMAL                    VALUE '1'.
      *       88  EXISTE-DEJA                VALUE '2'.
      *       88  FIN-FICHIER                VALUE '3'.
      *       88  ERREUR-DATE                VALUE '4'.
      *       88  ERREUR-FORMAT              VALUE '5'.
      *       88  ERREUR-HEURE               VALUE '6'.
      *       88  DOUBLE                     VALUE '7'.
      *  => ZONES DE TRAVAIL UTILES POUR LA GESTION DE TSTENVOI
           COPY WKTENVOI.
      * ...POUR MEC00 (ENVOI A DARTY.COM)
      ***********************************
      *    COPY MEC00C.
           COPY MEC0MC.
      * ...POUR MECRC (ENVOI A DARTY.COM)
      ***********************************
           COPY MECRCC.
      * ...POUR MBS48 (ENVOI DE DU3)
      ***********************************
           COPY COMMBS48.
      *   COPY SYKWZINO.
      *****************************************************************
      *  LINKAGE
      *****************************************************************
       LINKAGE SECTION.
      *         ZONE DE COMMUNICATION AVEC MEC16 .
       01  DFHCOMMAREA.
           COPY COMMEC16.
      *=================================================================
       PROCEDURE DIVISION.
      *=================================================================
           PERFORM ENTREE .
           PERFORM TRAITEMENT-TACHE.
           PERFORM SORTIE .
      *
      *=================================================================
       ENTREE SECTION.
      *****************
           INITIALIZE               RVEC0400 RVEC0203
           SET  NORMAL              TO TRUE    .
           STRING 'RECU/' W-CS-E-NSOC ' '
                          W-CS-E-NLIEU ' '
                          W-CS-E-NVENTE ' '
                          W-CS-E-CRETRAIT ' '
                          W-CS-E-CASIER ' '
                          W-CS-E-EMPLOYE ' '
                          W-CS-A-EVENEMENT '/'
                   DELIMITED BY SIZE INTO W-MESSAGES
           PERFORM DISPLAY-W-MESSAGE
           PERFORM CALL-SPDATDB2
           MOVE W-CS-E-NSOC       TO EC04-NSOCIETE
           MOVE W-CS-E-NLIEU      TO EC04-NLIEU
           MOVE W-CS-E-NVENTE     TO EC04-NVENTE
           MOVE W-CS-E-CRETRAIT   TO EC04-CPSWDC
           MOVE W-CS-E-CASIER     TO EC04-CCASIER
           MOVE W-CS-E-EMPLOYE    TO EC04-CEMPLOYE
           .
      *
      *=================================================================
       TRAITEMENT-TACHE SECTION.
      *    CODE RETOUR POUR EC00
           MOVE '00' TO W-CONSIGNES-RET
      *    LES EVENEMENTS POSSIBLES SONT
      *   LIVRAISON
      *   LIVRAISON PARTIELLE
      *   RETRAIT
      *   COLLECTE EXPIRE
      *   ANNULATION DE COMMANDE REUSSIE
      *   EVT ANNULATION DE COMMANDE ECHOUEE
      *   EVT D'INSPECTION DE CASIER
      *   EVT DE REMISE EN SERVICE DE CASIER
      *
      *   AUQUEL IL FAUT RAJOUTER L'EVENEMENT ANNULATION
      *   DEPUIS GV VERS LOGIBAG
      *   ON RECHERCHE DANS LA SOUS-TABLE CONSI POUR AVOIR
      *   LA CORRESPONDANCE LOGIBAG / AS400 OU HOST
           INITIALIZE W-CONSIGNE.
           PERFORM SELECT-CONSI.
           IF TROUVE
              MOVE GA01-WTABLEG         TO W-CONSIGNE
              PERFORM TRT-EVENEMENT
           END-IF
           .
       FIN-TRAITEMENT-TACHE. EXIT.
      *-----------------------------------------------------------------
      * TRAITEMENTS
      *-----------------------------------------------------------------
       TRT-EVENEMENT SECTION.
      *  MAJ DONNEES RTEC04
           IF W-UPDEC04 = 'O'
              MOVE W-EVENEMENT       TO EC04-CEVENTC
              MOVE W-CHOIX           TO EC04-CCCHOIX
              PERFORM UPDATE-RTEC04
           END-IF
           IF W-UPDEC04 = 'U'
              MOVE W-EVENEMENT       TO EC04-CEVENTC
              MOVE W-CHOIX           TO EC04-CCCHOIX
              PERFORM UPDATE-RTEC04-2
           END-IF
      *  ENVOI DU3 A L'AS400
           IF W-ENVOIAS400  = 'O'
              PERFORM LINK-MBS48
           END-IF
      *  ENVOI MESSAGE DCOM
           IF W-ENVOIDCOM   = 'O'
              PERFORM ENVOI-DCOM
              PERFORM LINK-MECRC
           END-IF
      *  ENVOI MESSAGE LOGIBAG
           IF W-ENVOILOGI   = 'O'
              PERFORM ENVOI-LOGIBAG
           END-IF
           .
      *-----------------------------------------------------------------
       TRT-ANNULOK SECTION.
      *  ENVOI DU3 A L'AS400 QUI CONFIRME LA BONNE RECEPTION PAR LOGIBAG
      *  DE L'ANNULATION DE VENTE ET DU BLOCAGE DU CAISER
      *  INFO UTILE OU PA COTE AS400 ?
      *  MAJ DONNEES
      *    PAS DE MAJ DONNEES DANS CET EVENEMENT
      *  ENVOI DU3 RETRAIT A L'AS400
      *    MOVE ? TO MBS-WOUTQ
           PERFORM LINK-MBS48
      *  ENVOI MESSAGE DCOM
      *    PAS D'ENVOI DCOM DANS CET EVENEMENT
           .
       FIN-TRT-ANNULOK. EXIT.
      *-----------------------------------------------------------------
       TRT-ANNUL-GV SECTION.
      *  ENVOI DU MESSAGE AU SERVEUR LOGIBAG (XML)
      *  POUR ANNULATION DE LA VENTE (BLOQUER LE CASIER)
      *  MAJ DONNEES
      *    PAS DE MAJ DONNEES DANS CET EVENEMENT
      *  ENVOI MESSAGE LOGIBAG
           PERFORM ENVOI-LOGIBAG
           .
       FIN-TRT-ANNUL-GV. EXIT.
      *-----------------------------------------------------------------
      * FIN TRAITEMENTS
      *-----------------------------------------------------------------
       ENVOI-DCOM    SECTION.
           STRING 'REPONSE ' W-CS-E-NSOC ' '
                                   W-CS-E-NLIEU ' '
                                   W-CS-E-NVENTE ' '
                   DELIMITED BY SIZE INTO W-MESSAGES
           PERFORM DISPLAY-W-MESSAGE
           INITIALIZE              MEC00C-COMMAREA
           PERFORM LOG-LE-MESSAGE.
      *
       FIN-ENVOI-DCOM. EXIT.
       ENVOI-LOGIBAG SECTION.
           PERFORM SELECT-LI00
           STRING 'ANNUL ' W-CS-E-NSOC ' '
                                   W-CS-E-NLIEU ' '
                                   W-CS-E-NVENTE ' '
                   DELIMITED BY SIZE INTO W-MESSAGES
           PERFORM DISPLAY-W-MESSAGE
      *
           INITIALIZE              MEC00C-COMMAREA
           MOVE 'CSANNUL'       TO MEC00C-FICHIER-SI
           MOVE 'D'             TO MEC00C-CODE-ACTION
           MOVE SPACES          TO MEC00C-DATA-SI
           MOVE 0               TO MEC00C-DATA-SI-L
           DISPLAY ' MESS-CSANNUL' MEC00C-COMMAREA(1:80)
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
      *
           MOVE 'CSANNUL2'      TO MEC00C-FICHIER-SI
           MOVE 'T'             TO MEC00C-CODE-ACTION
      *    CODE CONSIGNE PASSE DE 4 A 5
           STRING LI00-LVPARAM(10:5) W-CS-VENTE-GV
             DELIMITED BY SIZE INTO MEC00C-DATA-SI
      *    MOVE 17                       TO MEC00C-DATA-SI-L
           MOVE 18                       TO MEC00C-DATA-SI-L
           DISPLAY ' MESS-CSANNUL' MEC00C-COMMAREA(1:80)
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
           MOVE 'CSANNUL'       TO MEC00C-FICHIER-SI
           MOVE 'F'             TO MEC00C-CODE-ACTION
           MOVE SPACES          TO MEC00C-DATA-SI
           MOVE 0      TO MEC00C-DATA-SI-L
           DISPLAY ' MESS-CSANNUL' MEC00C-COMMAREA(1:80)
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC
           SET MEC00C-FIN-TRAITEMENT TO TRUE
           EXEC CICS LINK PROGRAM ('MEC00M') COMMAREA (MEC00C-COMMAREA)
           END-EXEC.
           IF NOT (MEC00C-CDRET-OK AND MEC00C-NBR-MESSAGE-ENVOYE = 1)
              STRING 'ERREUR MEC00/CSANNUL ' MEC00C-CODE-RETOUR
                      DELIMITED SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              MOVE '-2' TO W-CONSIGNES-RET
              PERFORM SORTIE
           END-IF.
           PERFORM LOG-LE-MESSAGE.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-ENVOI-LOGIBAG. EXIT.
      * CREATION D'UN MOUCHARD MESSAGE EN SORTIE.
       LOG-LE-MESSAGE SECTION.
           IF MEC00C-DATA-XML-L > 4034
              MOVE 4034 TO EC05-XMLDATA-LEN
           ELSE
              MOVE MEC00C-DATA-XML-L TO EC05-XMLDATA-LEN
           END-IF.
           MOVE MEC00C-DATA-XML TO EC05-XMLDATA-TEXT.
           EXEC SQL INSERT INTO RVEC0500 (TIMESTP, WRECENV, XMLDATA)
                         VALUES (CURRENT TIMESTAMP, 'E', :EC05-XMLDATA)
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'INSERT RTEC05:' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
      *       PERFORM ERREUR-SQL
           END-IF.
       FIN-LOG-LE-MESSAGE. EXIT.
       DISPLAY-W-MESSAGE SECTION.
      *--------------------------
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.
      *--
           EXEC CICS ASKTIME NOHANDLE
           END-EXEC.
      *}
           MOVE EIBTIME             TO W-EIBTIME.
           MOVE W-HEURE             TO W-MESSAGE-H.
           MOVE W-MINUTE            TO W-MESSAGE-M.
           EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
                    LENGTH (80) NOHANDLE
           END-EXEC.
           MOVE SPACES TO W-MESSAGES.
       FIN-DISPLAY-W-MESSAGE. EXIT.
       SELECT-CONSI SECTION.
           INITIALIZE RVGA0100.
           MOVE 'CONSI'          TO GA01-CTABLEG1
           MOVE W-CS-A-EVENEMENT TO GA01-CTABLEG2
           EXEC SQL SELECT   WTABLEG
                    INTO    :GA01-WTABLEG
                    FROM     RVGA0100
                    WHERE    CTABLEG1 = :GA01-CTABLEG1
                    AND      CTABLEG2 = :GA01-CTABLEG2
           END-EXEC
           IF SQLCODE NOT = 0
             MOVE SPACES TO W-MESSAGES
             STRING 'SEL S/T CONSI KO  ' GA01-CTABLEG1
                                     ' ' GA01-CTABLEG2
              DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              MOVE W-MESSAGES TO W-CONSIGNES-MESSAGE
              MOVE '-1' TO W-CONSIGNES-RET
              PERFORM SORTIE
           ELSE
              SET TROUVE TO TRUE
           END-IF
           .
       FIN-SELECT-CONSI. EXIT.
      *
       SELECT-LI00 SECTION.
           INITIALIZE RVLI0000.
           MOVE W-CS-E-NSOC     TO LI00-NSOCIETE
           MOVE W-CS-E-NLIEU    TO LI00-NLIEU
           MOVE 'CONSM'         TO LI00-CPARAM
           EXEC SQL SELECT   LVPARAM
                    INTO    :LI00-LVPARAM
                    FROM     RVLI0000
                    WHERE    NSOCIETE = :LI00-NSOCIETE
                    AND      NLIEU    = :LI00-NLIEU
                    AND      CPARAM   = :LI00-CPARAM
           END-EXEC
           IF SQLCODE NOT = 0
             MOVE SPACES TO W-MESSAGES
             STRING 'SEL LI00 KO  ' LI00-NSOCIETE
                                ' ' LI00-NLIEU
              DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              MOVE W-MESSAGES TO W-CONSIGNES-MESSAGE
              MOVE '-1' TO W-CONSIGNES-RET
              PERFORM SORTIE
           ELSE
              SET TROUVE TO TRUE
           END-IF
           .
       FIN-SELECT-LI00. EXIT.
      *
       SORTIE SECTION.
      ****************
      *{ normalize-exec-xx 1.5
      *    EXEC CICS RETURN END-EXEC.
      *--
           EXEC CICS RETURN
           END-EXEC.
      *}
      *
       SELECT-RTEC04                   SECTION.
                EXEC SQL SELECT A.NCDEWC , B.CCEVENT , A.CCCHOIX
                          INTO :EC04-NCDEWC ,
                               :EC02-CCEVENT ,
                               :EC04-CCCHOIX
                          FROM RVEC0400 A , RVEC0203 B
                     WHERE
                                   A.NSOCIETE   = :EC04-NSOCIETE
                            AND    A.NLIEU      = :EC04-NLIEU
                            AND    A.NVENTE     = :EC04-NVENTE
                            AND    A.NSOCIETE   = B.NSOCIETE
                            AND    A.NLIEU      = B.NLIEU
                            AND    A.NVENTE     = B.NVENTE
                   END-EXEC
           IF SQLCODE NOT = 0
             MOVE SPACES TO W-MESSAGES
             STRING 'SEL RTEC04: VENTE ' EC04-NSOCIETE
                                     ' ' EC04-NLIEU
                                     ' ' EC04-NVENTE
              DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              MOVE W-MESSAGES TO W-CONSIGNES-MESSAGE
              MOVE '-1' TO W-CONSIGNES-RET
              PERFORM SORTIE
           END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-SELECT-RTEC04.   EXIT.
      *=================================================================
       UPDATE-RTEC04                   SECTION.
           MOVE W-SPDATDB2-DSYST     TO EC04-DSYST
                EXEC SQL UPDATE   RVEC0400
                     SET      CCCHOIX    = :EC04-CCCHOIX
                            , CPSWDC     = :EC04-CPSWDC
                            , CCASIER    = :EC04-CCASIER
                            , CEMPLOYE   = :EC04-CEMPLOYE
                            , CEVENTC    = :EC04-CEVENTC
                            , DSYST      = :EC04-DSYST
                     WHERE
                                     NSOCIETE   = :EC04-NSOCIETE
                            AND      NLIEU      = :EC04-NLIEU
                            AND      NVENTE     = :EC04-NVENTE
                   END-EXEC
           IF SQLCODE NOT = 0
             MOVE SPACES TO W-MESSAGES
             STRING 'UPD RTEC04: VENTE ' EC04-NSOCIETE
                                     ' ' EC04-NLIEU
                                     ' ' EC04-NVENTE
              DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              MOVE W-MESSAGES TO W-CONSIGNES-MESSAGE
              MOVE '-1' TO W-CONSIGNES-RET
              PERFORM SORTIE
           END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-UPDATE-RTEC04.   EXIT.
       UPDATE-RTEC04-2                 SECTION.
           MOVE W-SPDATDB2-DSYST     TO EC04-DSYST
                EXEC SQL UPDATE   RVEC0400
                     SET      CEVENTC    = :EC04-CEVENTC
                            , DSYST      = :EC04-DSYST
                     WHERE
                                     NSOCIETE   = :EC04-NSOCIETE
                            AND      NLIEU      = :EC04-NLIEU
                            AND      NVENTE     = :EC04-NVENTE
                   END-EXEC
           IF SQLCODE NOT = 0
             MOVE SPACES TO W-MESSAGES
             STRING 'UPD RTEC04: VENTE ' EC04-NSOCIETE
                                     ' ' EC04-NLIEU
                                     ' ' EC04-NVENTE
              DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              MOVE W-MESSAGES TO W-CONSIGNES-MESSAGE
              MOVE '-1' TO W-CONSIGNES-RET
              PERFORM SORTIE
           END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-UPDATE-RTEC04-2.   EXIT.
      *
       CALL-SPDATDB2 SECTION.
           CALL 'SPDATDB2' USING W-SPDATDB2-RC W-SPDATDB2-DSYST.
           IF W-SPDATDB2-RC NOT = ZERO
              MOVE 'PBM DATE SYSTEME SPDATDB2' TO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
           END-IF.
       FIN-CALL-SPDATDB2. EXIT.
      *
      * EN CAS DE BESOIN : PAVE DE LECTURE TS
      *-----------------------------------------------------------------
       LECTURE-TS SECTION.
      *---------------------------------------
      *
           MOVE 'TOTO'                 TO IDENT-TS
      *    MOVE LENGTH OF TS42-DONNEES TO LONG-TS
           PERFORM READ-TS
           IF TROUVE
              SET PAS-FIN-TS TO TRUE
      *       MOVE Z-INOUT               TO TS42-DONNEES
              ADD 1 TO RANG-TS
           ELSE
              SET FIN-TS TO TRUE
           END-IF.
      *{ Tr-Empty-Sentence 1.6
      *    .
      *}
       FIN-LECTURE-TS. EXIT.
      *
      *
      *-----------------------------------------------------------------
      *-----------------------------------------------------------------
       TEST-RET-DB2 SECTION.
           MOVE SQLCODE     TO SQL-DISP
           IF SQLCODE <  0
              MOVE '-1'     TO W-CONSIGNES-RET
              PERFORM SORTIE
           ELSE
              IF SQLCODE = +100
                 SET NON-TROUVE TO TRUE
              END-IF
           END-IF.
       FIN-TEST-RET-DB2. EXIT.
      *********************************************
      *TEMP01-COPY SECTION. CONTINUE.
      *                       COPY SYKSTRAC.
      *TEMP02-COPY SECTION. CONTINUE. COPY SYKCTSRD.
      *TEMP03-COPY SECTION. CONTINUE. COPY SYKCTSRW.
      *TEMP04-COPY SECTION. CONTINUE. COPY SYKCTSWR.
       DELETE-TS                  SECTION.
           EXEC CICS DELETEQ TS
                              QUEUE    (IDENT-TS)
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   EIB-NORMAL
                MOVE 0 TO CODE-RETOUR
           ELSE
                IF   EIB-QIDERR
                     MOVE 1 TO CODE-RETOUR
                ELSE
                     MOVE '-1'     TO W-CONSIGNES-RET
                END-IF
           END-IF.
       FIN-DELETE-TS. EXIT.
       READ-TS                    SECTION.
           EXEC CICS READQ TS
                              QUEUE    (IDENT-TS)
                              INTO     (Z-INOUT)
                              LENGTH   (LONG-TS)
                              ITEM     (RANG-TS)
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   EIB-NORMAL
                MOVE 0 TO CODE-RETOUR
           ELSE
                IF   EIB-QIDERR OR EIB-ITEMERR
                     MOVE 1 TO CODE-RETOUR
                ELSE
                     MOVE '-1'     TO W-CONSIGNES-RET
                END-IF
           END-IF.
       FIN-READ-TS. EXIT.
       REWRITE-TS                 SECTION.
           EXEC CICS WRITEQ TS
                              QUEUE    (IDENT-TS)
                              FROM     (Z-INOUT)
                              LENGTH   (LONG-TS)
                              ITEM     (RANG-TS)
                              REWRITE
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   NOT EIB-NORMAL
                     MOVE '-1'     TO W-CONSIGNES-RET
           END-IF.
       FIN-REWRITE-TS. EXIT.
       WRITE-TS                   SECTION.
           EXEC CICS WRITEQ TS
                              QUEUE    (IDENT-TS)
                              FROM     (Z-INOUT)
                              LENGTH   (LONG-TS)
                              ITEM     (RANG-TS)
                              NOHANDLE
           END-EXEC.
           MOVE EIBRCODE TO EIB-RCODE.
           IF   NOT EIB-NORMAL
                     MOVE '-1'     TO W-CONSIGNES-RET
           END-IF.
       FIN-WRITE-TS. EXIT.
      *
       LECTURE-TSTENVOI SECTION.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS
      *         READQ TS
      *         QUEUE  (WC-ID-TSTENVOI)
      *         INTO   (TS-TSTENVOI-DONNEES)
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
      *         ITEM   (WP-RANG-TSTENVOI)
      *         NOHANDLE
      *    END-EXEC.
      *--
           EXEC CICS READQ TS
                QUEUE  (WC-ID-TSTENVOI)
                INTO   (TS-TSTENVOI-DONNEES)
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)
                ITEM   (WP-RANG-TSTENVOI)
                NOHANDLE
           END-EXEC.
      *}
           EVALUATE EIBRESP
           WHEN 0
              SET WC-TSTENVOI-SUITE      TO TRUE
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE
           WHEN 26
           WHEN 44
              SET WC-TSTENVOI-FIN        TO TRUE
           WHEN OTHER
              SET WC-TSTENVOI-ERREUR     TO TRUE
           END-EVALUATE.
       FIN-LECT-TSTENVOI. EXIT.
      *
       LINK-MBS48                     SECTION.
      * -> MINI DUPLI NEM
           INITIALIZE COMM-MBS48-APPLI
           MOVE  EC04-CEVENTC            TO COMM-MBS48-EVENT
           MOVE  W-CS-E-NSOC             TO COMM-MBS48-NSOCIETE
                                              COMM-MBS48-NSOCMODIF
           MOVE  W-CS-E-NLIEU              TO COMM-MBS48-NLIEU
                                              COMM-MBS48-NLIEUMODIF
           MOVE  W-CS-E-NVENTE             TO COMM-MBS48-NVENTE
           MOVE  'G'                       TO COMM-MBS48-TYPVTE.
           MOVE  W-CS-E-NSOC               TO COMM-MBS48-NSOCP
           MOVE  W-CS-E-NLIEU              TO COMM-MBS48-NLIEUP
           MOVE  SPACES                    TO COMM-MBS48-CIMPRIM
           SET COMM-MBS48-MAGASIN          TO TRUE
           EXEC CICS LINK PROGRAM ('MBS48')
                          COMMAREA (COMM-MBS48-APPLI)
           END-EXEC.
           IF EIBRCODE > SPACES
              STRING 'MEC16 > PB APPEL MBS48' EIBRCODE DELIMITED BY SIZE
              INTO W-CONSIGNES-MESSAGE
              MOVE '01' TO W-CONSIGNES-RET
              PERFORM SORTIE
           ELSE
              IF COMM-MBS48-CODRET      NOT = ' '
                 MOVE '01' TO W-CONSIGNES-RET
                 STRING 'MEC16 > RETOUR MBS48' COMM-MBS48-CODE-RETOUR
                                               COMM-MBS48-LIBERR
                 DELIMITED BY SIZE INTO W-CONSIGNES-MESSAGE
                 PERFORM SORTIE
           END-IF
           .
       FIN-LINK-MBS48. EXIT.
      *************************************************************
      * ENVOI VERS DCOM
       LINK-MECRC SECTION.
           MOVE W-CS-E-NSOC       TO EC04-NSOCIETE
           MOVE W-CS-E-NLIEU      TO EC04-NLIEU
           MOVE W-CS-E-NVENTE     TO EC04-NVENTE
           PERFORM SELECT-RTEC04.
           MOVE EC04-NCDEWC TO MECRC-NCDEWC.
      ** CODE �V�NEMENT CLICK & COLLECT
      *    MOVE EC02-CCEVENT TO MECRC-CCEVENT.
           MOVE 'DEPL'       TO MECRC-CCEVENT.
      ** CHOIX EMPLACEMENT CLICK & COLLECT
           MOVE EC04-CCCHOIX TO MECRC-CCCHOIX.
           EXEC CICS LINK PROGRAM ('MECRC' ) COMMAREA (COMM-MECRC)
           END-EXEC.
           IF NOT MECRC-OK
              MOVE SPACES TO W-MESSAGES
              STRING 'MEC16: LINK � MECRC: '
                          MECRC-CODE-RET ' ' MECRC-MESSAGE
              DELIMITED BY SIZE INTO W-MESSAGES
              PERFORM DISPLAY-W-MESSAGE
              SET MECRC-KO TO TRUE
           END-IF.
       FIN-LINK-MECRC. EXIT.
      *************************************************************
      
