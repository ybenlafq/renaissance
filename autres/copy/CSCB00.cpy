      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * COMMUNICATION GESTION PRIX PRIMES                                       
E0265 ******************************************************************        
      * DSA057 21/03/06 SUPPORT EVOLUTION CBN                                   
      *                 MODULE GENERIQUE CALCUL PRIMES CBN                      
      * !!!!! LES LIGNES MARQUEES PAR H!!!! CONTIENNENT                         
      * !!!!! DES CARACTERES NON VISUALISABLES                                  
      * !!!!! NE PAS Y TOUCHER                                                  
E0499 ******************************************************************        
      * DE02005 01/10/09 SUPPORT EVOLUTION D004416-17                           
      *                  PRIX ET PRIME AUTO CODIC GROUPE                        
************************************************************************        
      * TS PRIX                                                                 
      ******************************************************************        
       EFFACE-TS-CB-PP SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-PP-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-PP-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-PP-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-PP-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB-PP SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-PP-IDENT)                                        
      *        FROM     (TS-CB-PP-ENR)                                          
      *        NUMITEMS (TB-CB-PP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-PP-IDENT)                                        
               FROM     (TS-CB-PP-ENR)                                          
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-PP-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB-PP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-PP-IDENT TS-CB-PP-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-PP-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-PP-IDENT)
                  NUMITEMS (TB-CB-PP-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-PP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-PP-IDENT)                                        
      *        INTO     (TS-CB-PP-ENR)                                          
      *        LENGTH   (LENGTH OF TS-CB-PP-ENR)                                
      *        ITEM     (TB-CB-PP-POS)                                          
      *        NUMITEMS (TB-CB-PP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-PP-IDENT)                                        
               INTO     (TS-CB-PP-ENR)                                          
               LENGTH   (LENGTH OF TS-CB-PP-ENR)                                
               ITEM     (TB-CB-PP-POS)                                          
               NUMITEMS (TB-CB-PP-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-PP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-PP-IDENT TS-CB-PP-E        
      *dfhei*     DFHB0020 TB-CB-PP-NB TB-CB-PP-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB-PP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-PP-IDENT)                                        
      *        FROM     (TS-CB-PP-ENR)                                          
      *        ITEM     (TB-CB-PP-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-PP-IDENT)                                        
               FROM     (TS-CB-PP-ENR)                                          
               ITEM     (TB-CB-PP-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-PP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-PP-IDENT TS-CB-PP-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-PP-POS                                
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      ******************************************************************        
      * SELECTION/DESELECTION D'UN LIEU                                         
       SELECT-TS-CB-LI SECTION.                                                 
           MOVE TB-CB-LI-POS TO WB-SEL-POS                                      
           SET WC-SEL-NONE TO TRUE                                              
      *    LIGNE DEPENDANTE D'UNE AUTRE                                         
           IF TB-CB-LI-REF > 0                                                  
      *        DESELECTION                                                      
               IF TB-CB-LI-SEL = TC-CB-STAT-SEL                                 
                   MOVE TC-CB-STAT-INI TO TB-CB-LI-SEL                          
                   PERFORM MODIFIE-TS-CB-LI                                     
                   IF CB-CB-SEL-LIE > 0                                         
                       SUBTRACT 1 FROM CB-CB-SEL-LIE                            
                   END-IF                                                       
      *            MISE A JOUR LIGNE DE REF                                     
                   MOVE TB-CB-LI-REF TO TB-CB-LI-POS                            
                   PERFORM LECTURE-TS-CB-LI                                     
                   IF TB-CB-LI-ACT > 0                                          
                       SUBTRACT 1 FROM TB-CB-LI-ACT                             
                       PERFORM MODIFIE-TS-CB-LI                                 
                   END-IF                                                       
                   MOVE WB-SEL-POS TO TB-CB-LI-POS                              
                   PERFORM LECTURE-TS-CB-LI                                     
      *        SELECTION                                                        
               ELSE                                                             
      *            VERIFIE REF                                                  
                   MOVE TB-CB-LI-REF TO TB-CB-LI-POS                            
                   PERFORM LECTURE-TS-CB-LI                                     
                   IF TB-CB-LI-SEL = TC-CB-STAT-SEL                             
                       SET WC-SEL-REFSEL TO TRUE                                
      *            MISE A JOUR REF                                              
                   ELSE                                                         
      **               DERNIERE LIEE SELECTIONNEE                               
      **               EN MODE ZONE OU SOC SEULEMENT                            
      *                IF (TB-CB-LI-ACT >= TB-CB-LI-MAX - TB-CB-LI-MIN)         
      *                AND (CC-CB-DRT-SOC OR CC-CB-DRT-ZONE)                    
      **                   SELECTION DE LA REF                                  
      *                    MOVE TC-CB-STAT-SEL TO TB-CB-LI-SEL                  
      *                    ADD 1 TO CB-CB-SEL-REF                               
      **                   DESELECTION DE TOUTES LES LIEES                      
      *                    IF TB-CB-LI-ACT > 1                                  
      *                        MOVE 0 TO TB-CB-LI-ACT                           
      *                        PERFORM MODIFIE-TS-CB-LI                         
      *                        COMPUTE WB-NB =                                  
      *                            TB-CB-LI-MAX - TB-CB-LI-MIN + 1              
      *                        PERFORM WB-NB TIMES                              
      *                            ADD 1 TO TB-CB-LI-POS                        
      *                            PERFORM LECTURE-TS-CB-LI                     
      *                            IF TB-CB-LI-SEL = TC-CB-STAT-SEL             
      *                                MOVE TC-CB-STAT-INI TO                   
      *                                    TB-CB-LI-SEL                         
      *                                PERFORM MODIFIE-TS-CB-LI                 
      *                                IF CB-CB-SEL-LIE > 0                     
      *                                   SUBTRACT 1 FROM                       
      *                                       CB-CB-SEL-LIE                     
      *                                END-IF                                   
      *                            END-IF                                       
      *                        END-PERFORM                                      
      *                    ELSE                                                 
      *                        PERFORM MODIFIE-TS-CB-LI                         
      *                    END-IF                                               
      *                    SET WC-SEL-REFSEL TO TRUE                            
      **               INCREMENT NB DE LIEE SELECTIONNEE                        
      *                ELSE                                                     
                           ADD 1 TO TB-CB-LI-ACT                                
                           PERFORM MODIFIE-TS-CB-LI                             
      *                END-IF                                                   
                       MOVE WB-SEL-POS TO TB-CB-LI-POS                          
                       PERFORM LECTURE-TS-CB-LI                                 
                   END-IF                                                       
      *            SELECTION                                                    
                   IF WC-SEL-NONE                                               
                       MOVE TC-CB-STAT-SEL TO TB-CB-LI-SEL                      
                       PERFORM MODIFIE-TS-CB-LI                                 
                       ADD 1 TO CB-CB-SEL-LIE                                   
                   END-IF                                                       
               END-IF                                                           
      *    LIGNE REFERENCE                                                      
           ELSE                                                                 
      *        DESELECTION                                                      
               IF TB-CB-LI-SEL = TC-CB-STAT-SEL                                 
                   MOVE TC-CB-STAT-INI TO TB-CB-LI-SEL                          
                   PERFORM MODIFIE-TS-CB-LI                                     
                   IF CB-CB-SEL-REF > 0                                         
                      SUBTRACT 1 FROM CB-CB-SEL-REF                             
                   END-IF                                                       
      *        SELECTION                                                        
               ELSE                                                             
      *            LIGNES LIEES A DESELECTIONNER                                
                   IF TB-CB-LI-ACT > 0                                          
                       MOVE TB-CB-LI-ACT TO WB-SEL-NB                           
                       PERFORM UNTIL WB-SEL-NB = 0                              
                       OR (TB-CB-LI-POS > TB-CB-LI-NB)                          
                           ADD 1 TO TB-CB-LI-POS                                
                           PERFORM LECTURE-TS-CB-LI                             
                           IF TB-CB-LI-SEL = TC-CB-STAT-SEL                     
                               COMPUTE TB-CB-LI-SEL = TC-CB-STAT-INI            
                               PERFORM MODIFIE-TS-CB-LI                         
                               SUBTRACT 1 FROM WB-SEL-NB                        
                               IF CB-CB-SEL-LIE > 0                             
                                  SUBTRACT 1 FROM CB-CB-SEL-LIE                 
                               END-IF                                           
                           END-IF                                               
                       END-PERFORM                                              
                       MOVE WB-SEL-POS TO TB-CB-LI-POS                          
                       PERFORM LECTURE-TS-CB-LI                                 
                       MOVE 0 TO TB-CB-LI-ACT                                   
                       SET WC-SEL-LIEDES TO TRUE                                
                   END-IF                                                       
                   COMPUTE TB-CB-LI-SEL = TC-CB-STAT-SEL                        
                   PERFORM MODIFIE-TS-CB-LI                                     
                   ADD 1 TO CB-CB-SEL-REF                                       
               END-IF                                                           
           END-IF                                                               
           .                                                                    
************************************************************************        
      * TS ZONE DE PRIX                                                         
      ******************************************************************        
       EFFACE-TS-CB-ZP SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-ZP-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-ZP-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-ZP-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-ZP-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB-ZP SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-ZP-IDENT)                                        
      *        FROM     (TS-CB-ZP-ENR)                                          
      *        NUMITEMS (TB-CB-ZP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-ZP-IDENT)                                        
               FROM     (TS-CB-ZP-ENR)                                          
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-ZP-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB-ZP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-ZP-IDENT TS-CB-ZP-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-ZP-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-ZP-IDENT)
                  NUMITEMS (TB-CB-ZP-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-ZP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-ZP-IDENT)                                        
      *        INTO     (TS-CB-ZP-ENR)                                          
      *        LENGTH   (LENGTH OF TS-CB-ZP-ENR)                                
      *        ITEM     (TB-CB-ZP-POS)                                          
      *        NUMITEMS (TB-CB-ZP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-ZP-IDENT)                                        
               INTO     (TS-CB-ZP-ENR)                                          
               LENGTH   (LENGTH OF TS-CB-ZP-ENR)                                
               ITEM     (TB-CB-ZP-POS)                                          
               NUMITEMS (TB-CB-ZP-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-ZP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-ZP-IDENT TS-CB-ZP-E        
      *dfhei*     DFHB0020 TB-CB-ZP-NB TB-CB-ZP-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB-ZP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-ZP-IDENT)                                        
      *        FROM     (TS-CB-ZP-ENR)                                          
      *        ITEM     (TB-CB-ZP-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-ZP-IDENT)                                        
               FROM     (TS-CB-ZP-ENR)                                          
               ITEM     (TB-CB-ZP-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-ZP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-ZP-IDENT TS-CB-ZP-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-ZP-POS                                
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
************************************************************************        
      * TS LIEUX                                                                
      ******************************************************************        
       EFFACE-TS-CB-LI SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-LI-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-LI-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-LI-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-LI-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB-LI SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-LI-IDENT)                                        
      *        FROM     (TS-CB-LI-ENR)                                          
      *        NUMITEMS (TB-CB-LI-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-LI-IDENT)                                        
               FROM     (TS-CB-LI-ENR)                                          
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-LI-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB-LI-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-LI-IDENT TS-CB-LI-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-LI-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-LI-IDENT)
                  NUMITEMS (TB-CB-LI-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-LI SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-LI-IDENT)                                        
      *        INTO     (TS-CB-LI-ENR)                                          
      *        LENGTH   (LENGTH OF TS-CB-LI-ENR)                                
      *        ITEM     (TB-CB-LI-POS)                                          
      *        NUMITEMS (TB-CB-LI-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-LI-IDENT)                                        
               INTO     (TS-CB-LI-ENR)                                          
               LENGTH   (LENGTH OF TS-CB-LI-ENR)                                
               ITEM     (TB-CB-LI-POS)                                          
               NUMITEMS (TB-CB-LI-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-LI-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-LI-IDENT TS-CB-LI-E        
      *dfhei*     DFHB0020 TB-CB-LI-NB TB-CB-LI-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB-LI SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-LI-IDENT)                                        
      *        FROM     (TS-CB-LI-ENR)                                          
      *        ITEM     (TB-CB-LI-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-LI-IDENT)                                        
               FROM     (TS-CB-LI-ENR)                                          
               ITEM     (TB-CB-LI-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-LI-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-LI-IDENT TS-CB-LI-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-LI-POS                                
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
************************************************************************        
      * TS DEPOTS LIES                                                          
      ******************************************************************        
       EFFACE-TS-CB-DP SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-DP-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-DP-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-DP-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-DP-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB-DP SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-DP-IDENT)                                        
      *        FROM     (TS-CB-DP-ENR)                                          
      *        NUMITEMS (TB-CB-DP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-DP-IDENT)                                        
               FROM     (TS-CB-DP-ENR)                                          
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-DP-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB-DP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-DP-IDENT TS-CB-DP-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-DP-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-DP-IDENT)
                  NUMITEMS (TB-CB-DP-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-DP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-DP-IDENT)                                        
      *        INTO     (TS-CB-DP-ENR)                                          
      *        LENGTH   (LENGTH OF TS-CB-DP-ENR)                                
      *        ITEM     (TB-CB-DP-POS)                                          
      *        NUMITEMS (TB-CB-DP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-DP-IDENT)                                        
               INTO     (TS-CB-DP-ENR)                                          
               LENGTH   (LENGTH OF TS-CB-DP-ENR)                                
               ITEM     (TB-CB-DP-POS)                                          
               NUMITEMS (TB-CB-DP-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-DP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-DP-IDENT TS-CB-DP-E        
      *dfhei*     DFHB0020 TB-CB-DP-NB TB-CB-DP-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB-DP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-DP-IDENT)                                        
      *        FROM     (TS-CB-DP-ENR)                                          
      *        ITEM     (TB-CB-DP-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-DP-IDENT)                                        
               FROM     (TS-CB-DP-ENR)                                          
               ITEM     (TB-CB-DP-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-DP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-DP-IDENT TS-CB-DP-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-DP-POS                                
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
************************************************************************        
      * TS COPR1 PAS DE PRIME                                                   
      ******************************************************************        
       EFFACE-TS-CB-CO SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-CO-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-CO-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-CO-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-CO-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB-CO SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-CO-IDENT)                                        
      *        FROM     (TS-CB-CO-ENR)                                          
      *        NUMITEMS (TB-CB-CO-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-CO-IDENT)                                        
               FROM     (TS-CB-CO-ENR)                                          
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-CO-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB-CO-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-CO-IDENT TS-CB-CO-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-CO-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-CO-IDENT)
                  NUMITEMS (TB-CB-CO-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-CO SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-CO-IDENT)                                        
      *        INTO     (TS-CB-CO-ENR)                                          
      *        LENGTH   (LENGTH OF TS-CB-CO-ENR)                                
      *        ITEM     (TB-CB-CO-POS)                                          
      *        NUMITEMS (TB-CB-CO-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-CO-IDENT)                                        
               INTO     (TS-CB-CO-ENR)                                          
               LENGTH   (LENGTH OF TS-CB-CO-ENR)                                
               ITEM     (TB-CB-CO-POS)                                          
               NUMITEMS (TB-CB-CO-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-CO-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-CO-IDENT TS-CB-CO-E        
      *dfhei*     DFHB0020 TB-CB-CO-NB TB-CB-CO-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
E0499-******************************************************************        
      * TS GROUPE                                                               
      ******************************************************************        
       EFFACE-TS-CB-GR SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-GR-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-GR-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GR-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-GR-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB-GR SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-GR-IDENT)                                        
      *        FROM     (TS-CB-GR-ENR)                                          
      *        NUMITEMS (TB-CB-GR-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-GR-IDENT)                                        
               FROM     (TS-CB-GR-ENR)                                          
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-GR-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB-GR-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GR-IDENT TS-CB-GR-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-GR-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-GR-IDENT)
                  NUMITEMS (TB-CB-GR-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-GR SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-GR-IDENT)                                        
      *        INTO     (TS-CB-GR-ENR)                                          
      *        LENGTH   (LENGTH OF TS-CB-GR-ENR)                                
      *        ITEM     (TB-CB-GR-POS)                                          
      *        NUMITEMS (TB-CB-GR-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-GR-IDENT)                                        
               INTO     (TS-CB-GR-ENR)                                          
               LENGTH   (LENGTH OF TS-CB-GR-ENR)                                
               ITEM     (TB-CB-GR-POS)                                          
               NUMITEMS (TB-CB-GR-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-GR-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GR-IDENT TS-CB-GR-E        
      *dfhei*     DFHB0020 TB-CB-GR-NB TB-CB-GR-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB-GR SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-GR-IDENT)                                        
      *        FROM     (TS-CB-GR-ENR)                                          
      *        ITEM     (TB-CB-GR-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-GR-IDENT)                                        
               FROM     (TS-CB-GR-ENR)                                          
               ITEM     (TB-CB-GR-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-GR-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GR-IDENT TS-CB-GR-E        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-GR-POS                                
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       EFFACE-TS-CB-GC SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-GC-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-GC-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GC-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-GC-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      * DEPUIS ET VERS DONNEES COMMCB00                                         
       ECRIT-TS-CB-GC SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-GC-IDENT)                                        
      *        FROM     (CS-CB-DATA)                                            
      *        NUMITEMS (TB-CB-GC-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-GC-IDENT)                                        
               FROM     (CS-CB-DATA)                                            
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-GC-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF CS-CB-DATA TO DFHB0020                         
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GC-IDENT CS-CB-DATA        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-GC-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-GC-IDENT)
                  NUMITEMS (TB-CB-GC-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-GC SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-GC-IDENT)                                        
      *        INTO     (CS-CB-DATA)                                            
      *        LENGTH   (LENGTH OF CS-CB-DATA)                                  
      *        ITEM     (TB-CB-GC-POS)                                          
      *        NUMITEMS (TB-CB-GC-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-GC-IDENT)                                        
               INTO     (CS-CB-DATA)                                            
               LENGTH   (LENGTH OF CS-CB-DATA)                                  
               ITEM     (TB-CB-GC-POS)                                          
               NUMITEMS (TB-CB-GC-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF CS-CB-DATA TO DFHB0020                         
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GC-IDENT CS-CB-DATA        
      *dfhei*     DFHB0020 TB-CB-GC-NB TB-CB-GC-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB-GC SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-GC-IDENT)                                        
      *        FROM     (CS-CB-DATA)                                            
      *        ITEM     (TB-CB-GC-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-GC-IDENT)                                        
               FROM     (CS-CB-DATA)                                            
               ITEM     (TB-CB-GC-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF CS-CB-DATA TO DFHB0020                         
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GC-IDENT CS-CB-DATA        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-GC-POS                                
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
       EFFACE-TS-CB-GP SECTION.                                                 
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB-GP-IDENT)                                        
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB-GP-IDENT)                                        
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB-GP-IDENT                   
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB-GP-NB                                            
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB-GP SECTION.                                                  
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-GP-IDENT)                                        
      *        FROM     (TS-CB-GP-ENR)                                          
      *        NUMITEMS (TB-CB-GP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-GP-IDENT)                                        
               FROM     (TS-CB-GP-ENR)                                          
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB-GP-NB)                                           
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB-GP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0                                   
      *dfhei*     TS-CB-GP-IDENT TS-CB-GP-ENR                                   
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-GP-NB                                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB-GP-IDENT)
                  NUMITEMS (TB-CB-GP-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB-GP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB-GP-IDENT)                                        
      *        INTO     (TS-CB-GP-ENR)                                          
      *        LENGTH   (LENGTH OF TS-CB-GP-ENR)                                
      *        ITEM     (TB-CB-GP-POS)                                          
      *        NUMITEMS (TB-CB-GP-NB)                                           
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB-GP-IDENT)                                        
               INTO     (TS-CB-GP-ENR)                                          
               LENGTH   (LENGTH OF TS-CB-GP-ENR)                                
               ITEM     (TB-CB-GP-POS)                                          
               NUMITEMS (TB-CB-GP-NB)                                           
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-GP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0                                   
      *dfhei*     TS-CB-GP-IDENT TS-CB-GP-ENR                                   
      *dfhei*     DFHB0020 TB-CB-GP-NB TB-CB-GP-POS                             
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB-GP SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB-GP-IDENT)                                        
      *        FROM     (TS-CB-GP-ENR)                                          
      *        ITEM     (TB-CB-GP-POS)                                          
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB-GP-IDENT)                                        
               FROM     (TS-CB-GP-ENR)                                          
               ITEM     (TB-CB-GP-POS)                                          
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB-GP-ENR TO DFHB0020                       
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0                                   
      *dfhei*     TS-CB-GP-IDENT TS-CB-GP-ENR                                   
      *dfhei*     DFHB0020 DFHDUMMY TB-CB-GP-POS                                
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
-E0499     .                                                                    
                                                                                
                                                                                
