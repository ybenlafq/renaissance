      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC01                             *         
      *****************************************************************         
      *--> LONGUEUR MAX D'UNE COMMAREA 32 767.                                  
       01  MEC01C-COMMAREA.                                                     
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           05  MEC01C-DATA-ENTREE.                                              
               10  MEC01C-MESSAGE-XML              PIC  X(30720).               
               10  MEC01C-MESSAGE-XML-L            PIC  9(05).                  
               10  MEC01C-PARAMETRES-MQ.                                        
                   15  MEC01C-PARAMETRES-MQ-OBLIGA.                             
                       20  MEC01C-NOM-PG-APPELANT  PIC  X(06).                  
                       20  MEC01C-CODE-FONCTION    PIC  X(03).                  
                       20  MEC01C-TYPE-ENVOI-MQ    PIC  X(01).                  
                           88  MEC01C-TYPE-ENVOI-MQ-UNIQUE VALUE '1'.           
                           88  MEC01C-TYPE-ENVOI-MQ-MULTI  VALUE '2'.           
                   15  MEC01C-PARAMETRES-MQ-OPTION.                             
                       20  MEC01C-ALIAS            PIC  X(18).                  
                       20  MEC01C-MSGID            PIC  X(24).                  
                       20  MEC01C-DUREE            PIC S9(05).                  
                       20  MEC01C-PERSISTANCE      PIC  X(01).                  
                       20  MEC01C-PRIORITE         PIC S9(01).                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *                20  MEC01C-SYNCPOINT        PIC S9(09) BINARY.           
      *--                                                                       
                       20  MEC01C-SYNCPOINT        PIC S9(09) COMP-5.           
      *}                                                                        
      *--> DONNEES RESULTANTES.                                                 
           05  MEC01C-DATA-SORTIE.                                              
               10  MEC01C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC01C-CDRET-OK                  VALUE '0'.              
                   88  MEC01C-CDRET-ERR-NON-BLQ         VALUE '1'.              
                   88  MEC01C-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.              
                   88  MEC01C-CDRET-ERR-BLQ             VALUE '8'.              
                   88  MEC01C-CDRET-ERR-DB2-BLQ         VALUE '9'.              
               10  MEC01C-CODE-DB2                 PIC ++++9.                   
               10  MEC01C-CODE-DB2-DISPLAY         PIC  X(05).                  
               10  MEC01C-MESSAGE                  PIC  X(500).                 
           05  FILLER                              PIC  X(1468).                
      ********* FIN DE LA ZONE DE COMMUNICATION DU MEC01 *************          
                                                                                
