      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  FRB120-RECORD.                                               00000480
      *--------------------------------------- CONSTANTE '0020'         00000480
           05  FRB120-CGRP            PIC X(04).                        00000480
      *-------------------------------- N� DE DOSSIER = N� DE HS        00000480
           05  FRB120-NDOS            PIC X(20).                        00000480
      *-------------------------- CODE PRODUIT = NUMERO DE CODIC        00000480
           05  FRB120-NPRD            PIC X(10).                        00000480
      *--------------------------------- NUMERO DE SERIE ORIGINE        00000480
           05  FRB120-NSERIEE         PIC X(20).                        00000480
      *------------------------------ NUMERO DE SERIE RECU SAGEM        00000480
           05  FRB120-NSERIER         PIC X(20).                        00000480
      *----------------------------------------- DATE EXPEDITION        00000480
           05  FRB120-DEXPED          PIC X(08).                        00000480
      *------------------------------------ DATE RECEPTION SAGEM        00000480
           05  FRB120-DRECEP          PIC X(08).                        00000480
      *------------------------------------ CODE STATUT GARANTIE        00000480
           05  FRB120-CSTTGAR         PIC X(02).                        00000480
      *------------------------------------ DATE MISE EN SERVICE        00000480
           05  FRB120-DSERV           PIC X(08).                        00000480
      *------------------------------------------------ DATE SAV        00000480
           05  FRB120-DSAV            PIC X(08).                        00000480
      *-------------------------------- STATUT GARANTIE CONSTATE        00000480
           05  FRB120-CSTTGARC        PIC X(02).                        00000480
      *------------------------------------- CODE SYMPTOME PANNE        00000480
           05  FRB120-CSYMPT          PIC X(01).                        00000480
      *------------------------------------- CODE PANNE                 00000480
           05  FRB120-CPANNE          PIC X(04).                        00000480
      *----------------------------------- NUMERO SERIE RESTITUE        00000480
           05  FRB120-NSERIEREPRIS    PIC X(20).                        00000480
      *------------------------------------- MONTANT FACTURATION        00000480
           05  FRB120-MTFACT          PIC 9(13),99.                     00000480
      *---------------------------------- ZONE PIECES MANQUANTES        00000480
           05  FRB120-NPIECE          PIC X(50).                        00000480
      *-------------------------- ZONE MONTANT PIECES MANQUANTES        00000480
           05  FRB120-MTPIECE         PIC X(80).                        00000480
      *----------------------------------------- DATE REPARATION        00000480
           05  FRB120-DREPAR          PIC X(08).                        00000480
      *--------------------------------------------- DATE RETOUR        00000480
           05  FRB120-DRETOUR         PIC X(08).                        00000480
      *--------------------------------------- COMMENTAIRE DARTY        00000480
           05  FRB120-LCOMMENT        PIC X(70).                        00000480
      *--------------------------------------- COMMENTAIRE SAGEM        00000480
           05  FRB120-LCOMMENT1       PIC X(70).                        00000480
      *------------------------------------ NUMERO PRODUIT SAGEM        00000480
           05  FRB120-NPRDSAG         PIC X(16).                        00000480
      *------------------------------------- N� EXPEDITION DARTY        00000480
           05  FRB120-NEXPED          PIC X(10).                        00000480
      *------------------------------------- N� EXPEDITION SAGEM        00000480
           05  FRB120-NEXPEDSAG       PIC X(10).                        00000480
      *----------------------- CODIC PRODUIT DARTY ENVOYE A SAGEM               
           05  FRB120-NPRDENV         PIC X(10).                                
      *-------------------- CODIC PRODUIT DARTY EXPEDIE PAR SAGEM               
           05  FRB120-NPRDEXP         PIC X(10).                                
      *-------------------------------------- ZONE CONTROLE = '*'       00000480
           05  FRB120-CCTRL           PIC X(01).                        00000480
      *                                                                 00000600
      *-------------------------------------- ZONE FIN DE LIGNE         00000480
           05  FRB120-FILLER          PIC X(01).                        00000480
      *                                                                 00000600
                                                                                
