      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQUMK PARAM. P/ EXCEPT  MONOEQUIPAGE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUMK .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUMK .                                                            
      *}                                                                        
           05  EQUMK-CTABLEG2    PIC X(15).                                     
           05  EQUMK-CTABLEG2-REDEF REDEFINES EQUMK-CTABLEG2.                   
               10  EQUMK-NSOCLIEU        PIC X(06).                             
               10  EQUMK-CFAM            PIC X(05).                             
               10  EQUMK-NSEQ            PIC X(03).                             
           05  EQUMK-WTABLEG     PIC X(80).                                     
           05  EQUMK-WTABLEG-REDEF  REDEFINES EQUMK-WTABLEG.                    
               10  EQUMK-CMARK           PIC X(05).                             
               10  EQUMK-CVMARK          PIC X(05).                             
               10  EQUMK-TYPEQU          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUMK-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUMK-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUMK-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQUMK-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUMK-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQUMK-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
