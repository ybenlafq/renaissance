      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQUPD PARAMETRAGE POIDS MONOEQUIPAGE   *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUPD .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUPD .                                                            
      *}                                                                        
           05  EQUPD-CTABLEG2    PIC X(15).                                     
           05  EQUPD-CTABLEG2-REDEF REDEFINES EQUPD-CTABLEG2.                   
               10  EQUPD-NSOCLIEU        PIC X(06).                             
           05  EQUPD-WTABLEG     PIC X(80).                                     
           05  EQUPD-WTABLEG-REDEF  REDEFINES EQUPD-WTABLEG.                    
               10  EQUPD-POIDS0          PIC S9(07)       COMP-3.               
               10  EQUPD-TYPEQU          PIC X(03).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUPD-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUPD-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUPD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQUPD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUPD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQUPD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
