      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  Z-COMMAREA-MGV60.                                                    
           05 COMM-MGV60-NB-CADRE PIC X(01).                                    
              88 COMM-MGV60-PLUSIEURS-CADRES VALUE 'N'.                         
              88 COMM-MGV60-UN-CADRE     VALUE 'O'.                             
           05 COMM-MGV60-FAMILLE  PIC X(01).                                    
              88 COMM-MGV60-FAMILLE-PAS-BONNE      VALUE 'N'.                   
              88 COMM-MGV60-FAMILLE-BONNE     VALUE 'O'.                        
           05 COMM-MGV60-CADRE-CADRE    PIC X(5).                               
           05 COMM-MGV60-NOM-TS              PIC X(8).                          
           05 COMM-MGV60-DATES    PIC X(01).                                    
              88 COMM-MGV60-DATES-DIFFERENTES      VALUE 'N'.                   
              88 COMM-MGV60-DATES-EGALES      VALUE 'O'.                        
           05 COMM-MGV60-CADRE-EQUIPE   PIC X(5).                               
           05 COMM-MGV60-EQUIPES  PIC X(01).                                    
              88 COMM-MGV60-EQUIPES-DIFFERENTES      VALUE 'N'.                 
              88 COMM-MGV60-EQUIPES-EGALES      VALUE 'O'.                      
           05 COMM-MGV60-DDELIV              PIC X(08).                         
           05 COMM-MGV60-ERR-AUTRE           PIC X(02).                         
           05 COMM-MGV60-CTYPPREST           PIC X(05).                         
      *    05 COMM-MGV60-FILLER              PIC X(57).                         
           05 COMM-MGV60-FILLER              PIC X(54).                         
           05 COMM-MGV60-CMODDEL             PIC X(03).                         
      *    05 COMM-MGV60-FILLER              PIC X(64).                         
           05 COMM-MGV60-POS-ERREUR.                                            
              10 COMM-MGV60-INDICE1             PIC 9(02).                      
              10 COMM-MGV60-INDICE2             PIC 9(02).                      
                                                                                
