      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EXEC SQL                                                             
       declare OC cursor for                                                    
       --codif_offer                                                            
       with                                                                     
       --Prix Nationaux                                                         
       GN59 as (                                                                
       select                                                                   
       GN59.NCODIC                                                              
       , GN59.PREFTTC                                                           
       , GN59.DEFFET                                                            
       from RVGN5901 as GN59                                                    
       where                                                                    
       GN59.CREF = '1'                                                          
       and GN59.DEFFET <= :W-DJOUR-SUIV                                         
       )                                                                        
       --Primes Nationales                                                      
       , GN75 as (                                                              
       select                                                                   
       GN75.NCODIC                                                              
       , GN75.PCOMMREF                                                          
       , GN75.DEFFET                                                            
       from RVGN7501 as GN75                                                    
       where                                                                    
       GN75.DEFFET <= :W-DJOUR-SUIV                                             
       )                                                                        
       --Assortiments futurs                                                    
       , GA65 as (                                                              
       select                                                                   
       GA65.NCODIC                                                              
       , GA65.LSTATUT                                                           
       , GA65.DEFFET                                                            
       from RVGA6500 as GA65                                                    
       where                                                                    
       GA65.CSTATUT = 'ASSOR'                                                   
       and GA65.DEFFET > :W-DJOUR-SUIV                                          
       )                                                                        
       --Fragments XML                                                          
       --Attributes                                                             
       , attrs as (                                                             
       select                                                                   
       GA53.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "attr_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/offer')                   
       , xmlelement(name "code", rtrim(GA53.CDESCRIPTIF))                       
       , xmlelement(name "value", rtrim(GA53.CVDESCRIPT))                       
       )                                                                        
       ) as xattr_NCG                                                           
       from RVGA5300 as GA53                                                    
       group by                                                                 
       GA53.NCODIC                                                              
       )                                                                        
       --Destinations                                                           
       , dests as (                                                             
       select                                                                   
       GA33.NCODIC                                                              
       , xmlagg(                                                                
       xmlelement(name "saleabl"                                                
       , xmlnamespaces(default 'http://codif.darty.fr/offer')                   
       --mapping country vers ISO-3166-1-2                                      
       , rtrim(                                                                 
       case when substr(GA01.WTABLEG, 21, 1) = 'N'                              
       then substr(GA01.WTABLEG, 22, 2)                                         
       else GA01.CTABLEG2 end)                                                  
       )                                                                        
       ) as xsaleabl                                                            
       from RVGA3300 as GA33                                                    
       --mapping country vers ISO-3166-1-2                                      
       inner join RVGA0100 as GA01 on                                           
       GA01.CTABLEG1 = 'ORIGP'                                                  
       and substr(GA01.CTABLEG2, 1, 5) = GA33.CDEST                             
       group by                                                                 
       GA33.NCODIC                                                              
       )                                                                        
       --Dossiers                                                               
       , fileNcg as (                                                           
       select                                                                   
       BS25.NENTITE                                                             
       , xmlagg(                                                                
       xmlelement(name "file_NCG"                                               
       , xmlnamespaces(default 'http://codif.darty.fr/offer')                   
       , xmlelement(name "code", rtrim(BS25.CDOSSIER))                          
       , xmlelement(name "seq", int(BS25.NSEQDOS))                              
       , xmlelement(name "from",                                                
       timestamp_format(BS25.DEFFET, 'YYYYMMDD'))                               
       , case when BS25.DFINEFFET = '99999999' then null                        
       else xmlelement(name "to",                                               
       timestamp_format(BS25.DFINEFFET, 'YYYYMMDD'))                            
       end                                                                      
       )                                                                        
       ) as xfile_NCG                                                           
       from RVBS2500 as BS25                                                    
       group by                                                                 
       BS25.NENTITE                                                             
       )                                                                        
       --Select                                                                 
       select                                                                   
       GA00.NCODIC                                                              
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' concat                                               
      *  lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' ||                                                   
      *--                                                                       
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         || '_BXM101_' ||                                                       
      *}                                                                        
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *}                                                                        
       --Ent�te codif                                                           
       , xmlserialize (                                                         
       xmldocument(                                                             
       xmlelement(                                                              
       name "codif"                                                             
       , xmlnamespaces(default 'http://codif.darty.fr',                         
       'http://www.w3.org/2001/XMLSchema-instance' as "xsi")                    
       , xmlattributes(                                                         
       'http://codif.darty.fr http://codif.darty.fr/schemas/codif.xsd'          
       as "xsi:schemaLocation"                                                  
       , 'NCG' as "source"                                                      
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         concat '_BXM101_' concat                                               
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
         as "reference"                                                         
       , current timestamp as "issued"                                          
       )                                                                        
       --                                                                       
       , xmlelement(name "product"                                              
       , xmlnamespaces(default 'http://codif.darty.fr/offer'                    
       , 'http://www.w3.org/2001/XMLSchema-instance' as "xsi"                   
       )                                                                        
       , xmlattributes('907' as "context")                                      
       , xmlelement(name "codicInt", GA03.NCODICK)                              
       , xmlelement(name "lnkOpc"                                               
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "codicOpc", GA00.NCODIC)                               
       , xmlelement(name "creatd",                                              
       timestamp_format(GA00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "modifd",                                              
       timestamp_format(GA00.DMAJ, 'YYYYMMDD'))                                 
       --Specifique NCG                                                         
       , xmlelement(name "specOpc"                                              
       , xmlelement(name "type_NCG", rtrim(GA14.CTYPENT))                       
       , xmlelement(name "ref_NCG", rtrim(GA00.LREFFOURN))                      
       , xmlelement(name "smmr_NCG", rtrim(GA00.LREFDARTY))                     
       , xmlelement(name "categ_NCG", rtrim(GA00.CFAM))                         
       , (                                                                      
       select                                                                   
       xattr_NCG                                                                
       from attrs                                                               
       where                                                                    
       attrs.NCODIC = GA00.NCODIC                                               
       )                                                                        
       , xmlelement(name "mngr_NCG", rtrim(GA00.CHEFPROD))                      
       , (                                                                      
       select                                                                   
       xfile_NCG                                                                
       from fileNcg                                                             
       where                                                                    
       fileNcg.NENTITE = GA00.NCODIC                                            
       )                                                                        
       , xmlelement(name "assor_NCG", rtrim(GA00.CASSORT))                      
       , (                                                                      
       select                                                                   
       xmlconcat(                                                               
       xmlelement(name "assorNxt_NCG", rtrim(GA65.LSTATUT))                     
       , xmlelement(name "assorNDt_NCG"                                         
       , timestamp_format(GA65.DEFFET, 'YYYYMMDD')                              
       )                                                                        
       )                                                                        
       from GA65                                                                
       where                                                                    
       GA65.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GA65.DEFFET asc                                                          
       fetch first 1 rows only                                                  
       )                                                                        
       --INFO:DARTY-3004 consid�rer GA00.LSTATCOMP au lieu du CEXPO             
       --, xmlelement(name "expo_NCG", rtrim(GA00.CEXPO))                       
       , xmlelement(name "comp_NCG", rtrim(GA00.LSTATCOMP))                     
       , xmlelement(name "vat_NCG", rtrim(GA00.CTAUXTVA))                       
       , xmlelement(name "priceNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(GN59.PREFTTC as char(8))),',','.')                    
       from GN59                                                                
       where                                                                    
       GN59.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GN59.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       , xmlelement(name "bonusNat_NCG"                                         
       , value(                                                                 
       (                                                                        
       select                                                                   
       replace(rtrim(cast(GN75.PCOMMREF as char(8))),',','.')                   
       from GN75                                                                
       where                                                                    
       GN75.NCODIC = GA00.NCODIC                                                
       order by                                                                 
       GN75.DEFFET desc                                                         
       fetch first 1 rows only                                                  
       )                                                                        
       , '0.00')                                                                
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "valord_NCG", int(1))                                  
       , xmlelement(name "profOfr_NCG", rtrim(BS00.CPROFASS))                   
       , xmlelement(name "profOfr_NCG_calcul", rtrim(BS30.LVCCALCUL))           
       , xmlelement(name "profPrnt_NCG", rtrim(GA73.CODPROF))                   
       , xmlelement(name "profPrnt_NCG_label"                                   
       , rtrim(substr(PROGE.WTABLEG, 2, 30)))                                   
       --donn�es Innovente � minima                                             
       --non g�r�                                                               
       , xmlelement(name "ref_INO", '')                                         
       , xmlelement(name "categ_INO", '')                                       
       , xmlelement(name "short_INO", '')                                       
       , xmlelement(name "srcEngn_INO", '')                                     
       , xmlelement(name "lblMrkt_INO", '')                                     
       , xmlelement(name "saleabl_INO", int(1))                                 
       --                                                                       
       , xmlelement(name "client_INO"                                           
       , case when CLIRQ.WPARAM = 'O' then int(1) else int(0) end)              
       , xmlelement(name "multipl_INO"                                          
       , case when QTEMT.WPARAM = 'O' then int(1) else int(0) end)              
       , xmlelement(name "discount_INO"                                         
       , case when REMIS.WPARAM = 'O' then int(1) else int(0) end)              
       , xmlelement(name "receipt_INO"                                          
       , case when IMPRI.WPARAM = 'O' then int(1) else int(0) end)              
       --non g�r�                                                               
       , xmlelement(name "clntDob_INO", int(0))                                 
       )                                                                        
       )                                                                        
       , xmlelement(name "creatd",                                              
       timestamp_format(GA00.DCREATION, 'YYYYMMDD'))                            
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "modifd",                                              
       timestamp_format(GA00.DMAJ, 'YYYYMMDD'))                                 
       --INFO:non g�r�                                                          
       , xmlelement(name "userMod",                                             
       xmlattributes('true' as "xsi:nil"), '')                                  
       --INFO:non g�r�                                                          
       , xmlelement(name "nature", 'offer')                                     
       , xmlelement(name "brand_NCG", rtrim(GA00.CMARQ))                        
       , xmlelement(name "refMnf", rtrim(GA00.LREFO))                           
       --INFO:DARTY-2986 ne pas alimenter la zone                               
       --, xmlelement(name "lblMrkt", xmlattributes('fr' as "locale"),          
       --rtrim(GA00.LREFFOURN))                                                 
       , xmlelement(name "lblMrkt",                                             
       xmlattributes('fr' as "locale"), '')                                     
       , xmlelement(name "refSale"                                              
       , xmlattributes('fr' as "locale")                                        
       , rtrim(GA00.LREFFOURN)                                                  
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "categInt",                                            
       xmlattributes('true' as "xsi:nil"), '')                                  
       , (                                                                      
       select                                                                   
       xsaleabl                                                                 
       from dests                                                               
       where                                                                    
       dests.NCODIC = GA00.NCODIC                                               
       )                                                                        
       --INFO:non g�r�                                                          
       , xmlelement(name "tax"                                                  
       , xmlelement(name "country", 'FR')                                       
       , xmlelement(name "code", 'TVA')                                         
       )                                                                        
       --INFO:launch occurs: inconnu = 0                                        
       -- ) as XPRODUCT                                                         
       )                                                                        
       )) as clob(50K) including xmldeclaration) as XCODIF                      
       from RVGA0012 as GA00                                                    
       --limit� aux offres                                                      
       inner join RVGA1401 as GA14 on                                           
       GA14.CFAM = GA00.CFAM                                                    
       and GA14.CTYPENT = 'OC'                                                  
       inner join RVGA0300 as GA03 on                                           
       GA03.NCODIC = GA00.NCODIC                                                
       and GA03.CSOC = 'DAR'                                                    
       --mapping country vers ISO-3166-1-2                                      
       inner join RVGA0100 as GA01 on                                           
       GA01.CTABLEG1 = 'ORIGP'                                                  
       and substr(GA01.CTABLEG2, 1, 5) = GA00.CORIGPROD                         
       inner join RVBS0000 as BS00 on                                           
       BS00.NENTITE = GA00.NCODIC                                               
       inner join RVBS3000 as BS30 on                                           
       BS30.CPROFASS = BS00.CPROFASS                                            
       inner join RVGA7300 as GA73 on                                           
       GA73.NCODIC = GA00.NCODIC                                                
       inner join RVGA0100 PROGE on                                             
       PROGE.CTABLEG1 = 'PROGE'                                                 
       and substr(PROGE.CTABLEG2, 1, 10) = GA73.CODPROF                         
       and substr(PROGE.WTABLEG, 1, 1) = 'O'                                    
       --donn�es Innovente                                                      
       left join RVNV1400 QTEMT on                                              
       QTEMT.NENTITE = GA00.NCODIC                                              
       and QTEMT.CPARAM = 'QTEMT'                                               
       left join RVNV1400 CLIRQ on                                              
       CLIRQ.NENTITE = GA00.NCODIC                                              
       and CLIRQ.CPARAM = 'CLIRQ'                                               
       left join RVNV1400 REMIS on                                              
       REMIS.NENTITE = GA00.NCODIC                                              
       and REMIS.CPARAM = 'REMIS'                                               
       left join RVNV1400 IMPRI on                                              
       IMPRI.NENTITE = GA00.NCODIC                                              
       and IMPRI.CPARAM = 'IMPRI'                                               
       --                                                                       
       inner join rvad0500 ad05 on                                              
        ga00.ncodic = ad05.ncodic                                               
       and ad05.copco = 'DAR'                                                   
       and ad05.cdata = 'BXM101-OC'                                             
       and ad05.ddata = :W-DJOUR                                                
           END-EXEC                                                             
                                                                                
