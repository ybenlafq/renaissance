      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE D'EDITION DES PRESTATIONS      *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MTL20                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MTL2-LONG-COMMAREA PIC S9(4) COMP VALUE +112.                   
      *--                                                                       
       01  COMM-MTL2-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +112.                 
      *}                                                                        
       01  Z-COMMAREA-MTL2.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MTL2-I.                                                      
      *                                                                         
              03 COMM-MTL2-NSOCIETE        PIC    X(3).                         
              03 COMM-MTL2-LSOCIETE        PIC    X(20).                        
              03 COMM-MTL2-DDELIV          PIC    X(8).                         
              03 COMM-MTL2-DJOUR           PIC    X(8).                         
              03 COMM-MTL2-CTYPPREST       PIC    X(5).                         
              03 COMM-MTL2-LTYPPREST       PIC    X(20).                        
              03 COMM-MTL2-CZONLIV         PIC    X(5).                         
              03 COMM-MTL2-CPOSTAL         PIC    X(2).                         
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MTL2-O.                                                      
      *---------------------------------------CODE RETOUR                       
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MTL2-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MTL2-OK           VALUE '0'.                           
                 88 COMM-MTL2-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MTL2-MSG             PIC    X(40).                        
                                                                                
