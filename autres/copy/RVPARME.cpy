      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE PARME PARAMETRAGE PAR ETAT/CFAM/ENTR   *        
      *----------------------------------------------------------------*        
       01  RVPARME.                                                             
           05  PARME-CTABLEG2    PIC X(15).                                     
           05  PARME-CTABLEG2-REDEF REDEFINES PARME-CTABLEG2.                   
               10  PARME-CPARAM          PIC X(03).                             
               10  PARME-NSOCLIEU        PIC X(06).                             
               10  PARME-CFAM            PIC X(05).                             
           05  PARME-WTABLEG     PIC X(80).                                     
           05  PARME-WTABLEG-REDEF  REDEFINES PARME-WTABLEG.                    
               10  PARME-CETAT           PIC X(06).                             
               10  PARME-WFLAG           PIC X(01).                             
               10  PARME-LCOMMENT        PIC X(40).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVPARME-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PARME-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  PARME-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  PARME-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  PARME-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
