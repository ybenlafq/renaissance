      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION ARTICLE UNIT� LOGISTIQUE STANDARD     * 00031000
      * NOM FICHIER.: RAUSTD                                          * 00032017
      *---------------------------------------------------------------* 00034000
      * CR   .......: 12/10/2011  17:46:00                            * 00035000
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR0...: 302                                             * 00037103
      ***************************************************************** 00039000
      *                                                                 00040000
       01  RAUSTD.                                                      00050000
      * TYPE ENREG :R�CEPTION ARTICLE UNIT� LOGISTIQUE STANDARD         00060008
           05      RAUSTD-TYP-ENREG       PIC  X(0006).                 00070000
      * CODE ACTION                                                     00071006
           05      RAUSTD-CODE-ACTION     PIC  X(0001).                 00080013
      * CODE SOCI�T�                                                    00081006
           05      RAUSTD-CSOCIETE        PIC  X(0005).                 00090004
      * CODE ARTICLE                                                    00091006
           05      RAUSTD-CARTICLE        PIC  X(0018).                 00100004
      * CODE UNIT� LOGISTIQUE                                           00100108
           05      RAUSTD-CODE-UL         PIC  X(0005).                 00101001
      * TYPE UNIT� LOGISTIQUE                                           00102008
           05      RAUSTD-TYPE-UL         PIC  9(0003).                 00110001
      * CODE UNIT� LOGISTIQUE FILLE                                     00111008
           05      RAUSTD-CODE-UL-FILLE   PIC  X(0005).                 00120001
      * LIBELL�                                                         00121008
           05      RAUSTD-LIB-UL          PIC  X(0035).                 00130001
      * QUANTIT�                                                        00131008
           05      RAUSTD-QTE             PIC  9(0009).                 00140004
      * COEFFICIENT CONVERSION QUANTIT� POUR AFFICHAGE                  00141008
           05      RAUSTD-COEF-CONV-QTE-A PIC  X(0020).                 00150014
      * LIBELL� AFFICHAGE QUANTIT�                                      00151008
           05      RAUSTD-LAFF-QTE        PIC  X(0010).                 00160004
      * LONGUEUR                                                        00160108
           05      RAUSTD-LONGUEUR        PIC  9(0009).                 00161001
      * LARGEUR                                                         00161108
           05      RAUSTD-LARGEUR         PIC  9(0009).                 00162001
      * HAUTEUR                                                         00162108
           05      RAUSTD-HAUTEUR         PIC  9(0009).                 00163001
      * POIDS                                                           00163108
           05      RAUSTD-POIDS           PIC  9(0009).                 00164001
      * ROTATION                                                        00164108
           05      RAUSTD-ROTATION        PIC  X(0001).                 00165015
      * CODE-�-BARRES                                                   00165108
           05      RAUSTD-CODE-BARRES     PIC  X(0020).                 00166001
      * DISPONIBLE                                                      00166108
           05      RAUSTD-DISPONIBLE      PIC  9(0001).                 00167001
      * CODE FOURNISSEUR PRINCIPAL                                      00167108
           05      RAUSTD-CFOURNISSEUR-PPAL PIC X(015).                 00168005
      * NOM FOURNISSEUR PRINCIPAL                                       00168108
           05      RAUSTD-NOM-FOURN-PPAL  PIC  X(0035).                 00169003
      * COMPACTAGE R�CEPTION (AUTORISATION)                             00169108
           05      RAUSTD-COMPACT-RECEPT  PIC  9(0001).                 00169201
      * NOMBRE DE COUCHES SUR LE SUPPORT                                00169308
           05      RAUSTD-NB-COUCHES-SUP  PIC  9(0009).                 00169403
      * COEFFICIENT DE FOISONNEMENT                                     00169508
           05      RAUSTD-COEF-FOISONNE   PIC  9(0001)V9(2).            00169616
      * COEFFICIENT DE VIDE                                             00169708
           05      RAUSTD-COEF-VIDE       PIC  9(0001)V9(2).            00169816
      * CARACT�RISTIQUE 1                                               00169908
           05      RAUSTD-CARACT1         PIC  X(0020).                 00170001
      * CARACT�RISTIQUE 2                                               00170108
           05      RAUSTD-CARACT2         PIC  X(0020).                 00170201
      * CARACT�RISTIQUE 3                                               00170308
           05      RAUSTD-CARACT3         PIC  X(0020).                 00171001
      * FILLER                                                          00172009
           05      RAUSTD-FILLER          PIC  X(0001).                 00180001
                                                                                
