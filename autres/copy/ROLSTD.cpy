      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION OP LIGNE STANDARD (MUTATION)          * 00031000
      * NOM FICHIER.: ROLSTD                                          * 00032004
      *---------------------------------------------------------------* 00034000
      * CR   .......: 09/01/2012  17:46:00                            * 00035002
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR....: 291                                             * 00037100
      ***************************************************************** 00037300
      *                                                                 00037400
       01  ROLSTD.                                                      00037500
      * TYPE ENREGISTREMENT                                             00037604
           05      ROLSTD-TYP-ENREG       PIC  X(0006).                 00037700
      * CODE SOCI�T�                                                    00037804
           05      ROLSTD-CSOCIETE        PIC  X(0005).                 00037901
      * NUM�RO D OP                                                     00038004
           05      ROLSTD-NOP             PIC  X(0015).                 00038101
      * NUM�RO DE LIGNE                                                 00038204
           05      ROLSTD-NLIGNE          PIC  9(0005).                 00038301
      * NUM�RO DE SOUS-LIGNE                                            00038404
           05      ROLSTD-NSSLIGNE        PIC  9(0005).                 00039001
      * TYPE DE LIGNE                                                   00039104
           05      ROLSTD-TYPE-LIGNE      PIC  9(0001).                 00040000
      * CODE ARTICLE                                                    00041004
           05      ROLSTD-CARTICLE        PIC  X(0018).                 00050001
      * LOT IMPOS�                                                      00060004
           05      ROLSTD-LOT-IMPOSE      PIC  X(0015).                 00070000
      * DLV (D�LAI DE S�CURIT� EN JOURS)                                00071004
           05      ROLSTD-DLV             PIC  9(0005).                 00080000
      * DLC (D�LAI DE S�CURIT� EN JOURS)                                00081004
           05      ROLSTD-DLC             PIC  9(0005).                 00090000
      * LIBELL� ARTICLE                                                 00091004
           05      ROLSTD-LARTICLE        PIC  X(0035).                 00100001
      * CODE ARTICLE CLIENT                                             00101004
           05      ROLSTD-CARTICLE-CLIENT PIC  X(0018).                 00110000
      * QUANTIT� � PR�LEVER                                             00111004
           05      ROLSTD-QTE-A-PRLV      PIC  9(0009).                 00120000
      * QUANTIT� COMMAND�E                                              00120104
           05      ROLSTD-QTE-CDEE        PIC  9(0009).                 00121004
      * QUANTIT� RESTANTE                                               00121104
           05      ROLSTD-QTE-RESTANTE    PIC  9(0009).                 00122004
      * MONTANT DU CONTRE-REMBOURSEMENT                                 00122104
           05      ROLSTD-MT-CONTRE-REMB  PIC  9(0015).                 00123004
      * IDENTIFIANT FAMILLE DE COLISAGE                                 00124004
           05      ROLSTD-ID-FAM-COLISAGE PIC  X(0003).                 00130000
      * COLISAGE KIT                                                    00131004
           05      ROLSTD-COLISAGE-KIT    PIC  9(0003).                 00140000
      * SORTIE SP�CIALE                                                 00141004
           05      ROLSTD-SORTIE-SPECIALE PIC  9(0001).                 00150000
      * �TAT STOCK NIVEAU 3                                             00160004
           05      ROLSTD-ETAT-STOCK-N3   PIC  9(0003).                 00161000
      * R�SERVATION                                                     00161104
           05      ROLSTD-RESERVATION     PIC  X(0015).                 00162000
      * NUM�RO DE COMMANDE CLIENT                                       00162104
           05      ROLSTD-NCDE-CLIENT     PIC  X(0015).                 00163000
      * NUM�RO DE COMMANDE NIVEAU3                                      00163104
           05      ROLSTD-NCDE-CLIENT-N3  PIC  X(0015).                 00163200
      * CARACT�RISTIQUE 1                                               00163304
           05      ROLSTD-CARACT1         PIC  X(0020).                 00170400
      * CARACT�RISTIQUE 2                                               00170504
           05      ROLSTD-CARACT2         PIC  X(0020).                 00170600
      * CARACT�RISTIQUE 3                                               00170704
           05      ROLSTD-CARACT3         PIC  X(0020).                 00170800
      *                                                                 00170904
           05      ROLSTD-FIN             PIC  X(0001).                 00171000
                                                                                
