      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:11 >
      
      ******************************************************************
      *                             DABORD LA COPY DE L"ENTETE MESSMQ  *
      *   MESTSV34 COPY                                                *
      * - DEMANDE ENVOI VENTE � SIEBEL OU SAV                          *
      *   500 LONGUEUR                                                 *
      * COPY POUR LES PROGRAMMES TSV91 ET MSV34                        *
      ******************************************************************
      * LONGUEUR DE 34  OCTETS
              05 MESTSV34-RETOUR.
                 10 MESTSV34-CODE-RETOUR.
                    15  MESTSV34-CODRET  PIC X(1).
                         88  MESTSV34-CODRET-OK        VALUE ' '.
                         88  MESTSV34-CODRET-ERREUR    VALUE '1'.
                    15  MESTSV34-LIBERR  PIC X(60).
              05 MESTSV34-DATA.
      * NUMERO DE CLIENT
                 10 MESTSV34-DONNES-CLIENT.
                   15 MESTSV34-NCLIENTADR     PIC X(08).
      * TYPE DE VENTE
                   15 MESTSV34-WTYPVTE          PIC X(01).
                      88 MESTSV34-ADR VALUE 'X'.
                      88 MESTSV34-NAD VALUE 'N'.
                      88 MESTSV34-ALL VALUE 'A'.
      * PRESTATION
                   15 MESTSV34-WPREST           PIC X(01).
                      88 MESTSV34-PRE VALUE 'O'.
                      88 MESTSV34-NPR VALUE 'N'.
      * TYPE DE REPONSE
                 10 MESTSV34-TYPE             PIC X(03).
                 10 MESTSV34-FILLER           PIC X(24).
      *
      *****************************************************************
      
