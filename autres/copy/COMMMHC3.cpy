      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE EDITION ENCOURS PRODUITS 020          *            
      *  PROJET CHS                                                *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MHC30                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MHC3-LONG-COMMAREA PIC S9(4) COMP VALUE +100.                   
      *--                                                                       
       01  COMM-MHC3-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +100.                 
      *}                                                                        
       01  Z-COMMAREA-MHC3.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MHC3-I.                                                      
      *                                                                         
              03 COMM-MHC3-NLIEUHC         PIC    X(03).                        
              03 COMM-MHC3-TEBRB           PIC    X(02).                        
              03 COMM-MHC3-TRI             PIC    X(01).                        
              03 COMM-MHC3-CIMP            PIC    X(04).                        
              03 COMM-MHC3-CMARQ           PIC    X(05).                        
              03 FILLER                    PIC    X(44).                        
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MHC3-O.                                                      
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MHC3-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MHC3-OK           VALUE '0'.                           
                 88 COMM-MHC3-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MHC3-MSG             PIC    X(40).                        
                                                                                
