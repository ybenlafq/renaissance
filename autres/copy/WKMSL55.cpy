      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *---------------------------------------------------------------*         
      *--COMBINAISONS CRITERES DE RECHERCHE                                     
      *---------------------------------------------------------------*         
      * 1ER  CHIFFRE = SOCIETE 1 RENSEIGNE, 0 PAS RENSEIGNE                     
      * 2EME CHIFFRE = LIEU    1 RENSEIGNE, 0 PAS RENSEIGNE                     
      * 3EME CHIFFRE = CODIC   1 RENSEIGNE, 0 PAS RENSEIGNE                     
      * 4EME CHIFFRE = NVENTE  1 RENSEIGNE, 0 PAS RENSEIGNE                     
      * 5EME CHIFFRE = CODACT  1 RENSEIGNE, 0 PAS RENSEIGNE                     
       01  B-CRITERES-RECH  PIC X(5)      VALUE '00000'.                        
           88  RECH-LIEU-SOC-VENTE        VALUE '11010'.                        
           88  RECH-LIEU-SOC-VENTE-CODACT VALUE '11011'.                        
           88  RECH-SOC-LIEU-CODIC        VALUE '11100'.                        
           88  RECH-SOC-LIEU-CODIC-CODACT VALUE '11101'.                        
           88  RECH-SOC-LIEU              VALUE '11000'.                        
           88  RECH-SOC-LIEU-CODACT       VALUE '11001'.                        
           88  RECH-LIEU                  VALUE '01000'.                        
           88  RECH-LIEU-CODACT           VALUE '01001'.                        
           88  RECH-SOC                   VALUE '10000'.                        
           88  RECH-SOC-CODACT            VALUE '10001'.                        
           88  RECH-CODIC                 VALUE '00100'.                        
           88  RECH-CODIC-CODACT          VALUE '00101'.                        
           88  RECH-SOC-CODIC             VALUE '10100'.                        
           88  RECH-SOC-CODIC-CODACT      VALUE '10101'.                        
           88  RECH-LIEU-CODIC            VALUE '01100'.                        
           88  RECH-LIEU-CODIC-CODACT     VALUE '01101'.                        
           88  RECH-CODACT                VALUE '00001'.                        
           88  RECH-NULL                  VALUE '00000'.                        
      *01  B-CRITERES-RECH  PIC X        VALUE '0'.                             
      *    88  RECH-LIEU-SOC-VENTE       VALUE '1'.                             
      *    88  RECH-CODACT               VALUE '2'.                             
      *    88  RECH-DATES                VALUE '3'.                             
      *    88  RECH-CODIC-CODACT         VALUE '4'.                             
      *    88  RECH-CODIC                VALUE '5'.                             
      *    88  RECH-CODIC-DATES          VALUE '6'.                             
      *    88  RECH-CODIC-CODACT-DATES   VALUE '7'.                             
      *    88  RECH-CODACT-DATES         VALUE '8'.                             
      *    88  RECH-NULL                 VALUE '9'.                             
      *    88  RECH-CODACTX              VALUE 'X'.                             
      *    88  RECH-CODIC-CODACTX        VALUE 'X'.                             
      *    88  RECH-SOC-LIEU             VALUE 'A'.                             
      *    88  RECH-SOC                  VALUE 'B'.                             
      *    88  RECH-LIEU                 VALUE 'C'.                             
AP----*    88  RECH-SOC-LIEU-DATES       VALUE 'D'.                             
AP----*    88  RECH-SOC-LIEU-CODIC       VALUE 'D'.                             
AP----*    88  RECH-SOC-DATES            VALUE 'E'.                             
AP----*    88  RECH-SOC-CODIC            VALUE 'E'.                             
AP----*    88  RECH-LIEU-DATES           VALUE 'F'.                             
AP----*    88  RECH-LIEU-CODIC           VALUE 'F'.                             
AP----*    88  RECH-LIEU-CODACT          VALUE 'G'.                             
                                                                                
