      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT IFEMDA AU 31/03/2000  *        
      *                                                                *        
      *          CRITERES DE TRI  07,21,BI,A,                          *        
      *                           28,08,BI,A,                          *        
      *                           36,02,BI,A,                          *        
      *                                                                *        
      *----------------------------------------------------------------*        
       01  DSECT-IFEMDA.                                                        
            05 NOMETAT-IFEMDA           PIC X(6) VALUE 'IFEMDA'.                
            05 RUPTURES-IFEMDA.                                                 
           10 IFEMDA-LIBELLE            PIC X(21).                      007  021
           10 IFEMDA-NSOC               PIC X(08).                      028  008
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 IFEMDA-SEQUENCE           PIC S9(04) COMP.                036  002
      *--                                                                       
           10 IFEMDA-SEQUENCE           PIC S9(04) COMP-5.                      
      *}                                                                        
            05 CHAMPS-IFEMDA.                                                   
           10 IFEMDA-CFAM               PIC X(05).                      038  005
           10 IFEMDA-CMARQ              PIC X(05).                      043  005
           10 IFEMDA-CSELART            PIC X(05).                      048  005
           10 IFEMDA-DDELIV             PIC X(08).                      053  008
           10 IFEMDA-DMUTATION          PIC X(09).                      061  009
           10 IFEMDA-LREFFOURN          PIC X(20).                      070  020
           10 IFEMDA-NCODIC             PIC X(07).                      090  007
           10 IFEMDA-NMUTATION          PIC X(09).                      097  009
           10 IFEMDA-NVENTE             PIC X(07).                      106  007
           10 IFEMDA-WSEQFAM            PIC X(28).                      113  028
           10 IFEMDA-QUANTITE           PIC S9(07)      COMP-3.         141  004
            05 FILLER                      PIC X(368).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      * 01  DSECT-IFEMDA-LONG           PIC S9(4)   COMP  VALUE +144.           
      *                                                                         
      *--                                                                       
        01  DSECT-IFEMDA-LONG           PIC S9(4) COMP-5  VALUE +144.           
                                                                                
      *}                                                                        
