      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************* 14-09-94          
      * BTIARCW0 : WORKING BATCH COBOL                                          
      *          : ZONES DE COMMUNICATION AVEC PROGRAMME DSNTIAR                
      ****************************************************************          
       01  TIAR.                                                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  TIAR-LENGTH     PIC  S9(04) COMP VALUE +800.                     
      *--                                                                       
           02  TIAR-LENGTH     PIC  S9(04) COMP-5 VALUE +800.                   
      *}                                                                        
           02  TIAR-MSG        PIC   X(80) OCCURS 10.                           
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  TIAR-LRECL          PIC  S9(09)  COMP VALUE +80.                     
      *--                                                                       
       77  TIAR-LRECL          PIC  S9(09) COMP-5 VALUE +80.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77  TIAR-I              PIC  S9(04)  BINARY.                             
      *--                                                                       
       77  TIAR-I              PIC  S9(04) COMP-5.                              
      *}                                                                        
       EJECT                                                                    
                                                                                
