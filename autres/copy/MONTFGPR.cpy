      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 09/04/2016 1        
                                                                                
      *                                                                         
      *==> DARTY ******************************************************         
      *          * TRAITEMENT MONTANT                                 *         
      *   SPECIFIQUE NETTING ---> WORKING SWMONTFG                              
      ****************************************************** SYKPMONT *         
      *                                                                         
       TRAITEMENT-MONTANT         SECTION.                                      
      *                                                                         
           MOVE 5 TO CODE-RETOUR.                                               
      *                                                                         
      * CONTROLE DE TOUS LES CARACTERES                                         
      *                                                                         
           MOVE 0      TO W-MONT-POS-DEB                                        
                          W-MONT-POS-VIR                                        
                          W-MONT-NB-DEC                                         
                          W-MONT-NB-ENT                                         
0795                      W-MONT-FILE-FG.                                       
           MOVE 1      TO W-MONT-POIDS.                                         
           MOVE SPACES TO W-MONT-SIGNE.                                         
           MOVE SPACES TO W-MONT-NUMERIC.                                       
      *                                                                         
           PERFORM VARYING W-MONT-I FROM 1 BY 1                                 
0795                       UNTIL W-MONT-I > W-MONT-LONG-FG                      
      * BLANCS OU LOW-VALUE INTERCALES                                          
              IF W-MONT-USER-CAR(W-MONT-I) = SPACES OR LOW-VALUE                
                 COMPUTE W-MONT-J = W-MONT-I + 1                                
                 IF W-MONT-POS-DEB NOT = 0                                      
0795             AND W-MONT-I < W-MONT-LONG-FG                                  
                 AND W-MONT-USER-CAR(W-MONT-J)                                  
                                              NOT = SPACES AND LOW-VALUE        
                    GO TO FIN-TRAITEMENT                                        
                 END-IF                                                         
      * CARACTERES NON-NUMERIQUES                                               
              ELSE                                                              
                 IF W-MONT-USER-CAR(W-MONT-I) NOT NUMERIC                       
                 AND W-MONT-USER-CAR(W-MONT-I) NOT = '+' AND '-' AND ','        
                    MOVE 'N' TO W-MONT-NUMERIC                                  
                    GO TO FIN-TRAITEMENT                                        
      * POSITION DEBUT                                                          
                 ELSE                                                           
                    IF W-MONT-POS-DEB = 0                                       
                       MOVE W-MONT-I TO W-MONT-POS-DEB                          
                    END-IF                                                      
      * SIGNE                                                                   
                    IF W-MONT-USER-CAR(W-MONT-I) = '+' OR '-'                   
                       IF W-MONT-SIGNE NOT = SPACES                             
                          GO TO FIN-TRAITEMENT                                  
                       ELSE                                                     
                          MOVE W-MONT-USER-CAR(W-MONT-I) TO W-MONT-SIGNE        
                       END-IF                                                   
      * VIRGULE                                                                 
                    ELSE                                                        
                       IF W-MONT-USER-CAR(W-MONT-I) = ','                       
                          IF W-MONT-POS-VIR NOT = 0                             
                             GO TO FIN-TRAITEMENT                               
                          ELSE                                                  
                             MOVE W-MONT-I TO W-MONT-POS-VIR                    
                          END-IF                                                
                       ELSE                                                     
      * CARACTERE NUMERIQUE                                                     
                          IF W-MONT-POS-VIR = 0                                 
                             IF W-MONT-USER-CAR(W-MONT-I) NOT = '0'             
                             OR W-MONT-NB-ENT NOT = 0                           
                                ADD 1 TO W-MONT-NB-ENT                          
                             END-IF                                             
                          ELSE                                                  
                             ADD 1 TO W-MONT-NB-DEC                             
                          END-IF                                                
                       END-IF                                                   
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-PERFORM.                                                         
      *                                                                         
      * CONTROLE NOMBRES DE DECIMAUX ET D'ENTIERS                               
      *                                                                         
           IF ( W-MONT-NB-ENT-USER NOT = 0                                      
            AND W-MONT-NB-ENT > W-MONT-NB-ENT-USER )                            
           OR ( W-MONT-NB-DEC-USER NOT = 0                                      
            AND W-MONT-NB-DEC > W-MONT-NB-DEC-USER )                            
           OR W-MONT-POS-DEB = 0                                                
              GO TO FIN-TRAITEMENT                                              
           END-IF.                                                              
      *                                                                         
           MOVE 0 TO CODE-RETOUR.                                               
      *                                                                         
      * CONVERSION EN NUMERIQUE                                                 
      *                                                                         
           PERFORM VARYING W-MONT-I FROM W-MONT-POS-DEB BY 1                    
                   UNTIL W-MONT-USER-CAR(W-MONT-I) = SPACES OR LOW-VALUE        
      *                                                                         
              IF W-MONT-USER-CAR(W-MONT-I) NUMERIC                              
                 IF W-MONT-I < W-MONT-POS-VIR                                   
                 OR W-MONT-POS-VIR = 0                                          
0795                COMPUTE W-MONT-FILE-FG = W-MONT-FILE-FG * 10                
                                        + W-MONT-USER-CAR9(W-MONT-I)            
                 ELSE                                                           
                    COMPUTE W-MONT-POIDS = W-MONT-POIDS / 10                    
0795                COMPUTE W-MONT-FILE-FG = W-MONT-FILE-FG                     
                                        + ( W-MONT-USER-CAR9(W-MONT-I)          
                                          * W-MONT-POIDS )                      
                 END-IF                                                         
              END-IF                                                            
      *                                                                         
           END-PERFORM.                                                         
      *                                                                         
           IF W-MONT-SIGNE = '-'                                                
0795          COMPUTE W-MONT-FILE-FG = 0 - W-MONT-FILE-FG                       
           END-IF.                                                              
      *                                                                         
       FIN-TRAITEMENT.                                                          
      *                                                                         
           MOVE 0 TO W-MONT-NB-ENT-USER                                         
                     W-MONT-NB-DEC-USER.                                        
      *                                                                         
       FIN-TRAITEMENT-MONTANT.                                                  
         EXIT.                                                                  
           EJECT                                                                
                                                                                
