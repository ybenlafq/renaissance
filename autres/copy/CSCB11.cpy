      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * ITSSC                                                                   
      * GESTION PRIX PRIMES                                                     
E0452 ******************************************************************        
      * DE02005 26/09/08 SUPPORT EVOLUTION CBN                                  
      *                  TS TRANS CB11                                          
      * !!!!! LES LIGNES MARQUEES PAR H!!!! CONTIENNENT                         
      * !!!!! DES CARACTERES NON VISUALISABLES                                  
      * !!!!! NE PAS Y TOUCHER                                                  
      ******************************************************************        
      * TS FILTRE                                                               
      ******************************************************************        
       EFFACE-TS-CB11-FL SECTION.                                               
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB11-FL-IDENT)                                      
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB11-FL-IDENT)                                      
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-FL-IDENT                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB11-FL-NB                                          
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB11-FL SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-FL-IDENT)                                      
      *        FROM     (TS-CB11-FL-ENR)                                        
      *        NUMITEMS (TB-CB11-FL-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-FL-IDENT)                                      
               FROM     (TS-CB11-FL-ENR)                                        
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB11-FL-NB)                                         
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB11-FL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-FL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-FL-NB                               
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB11-FL-IDENT)
                  NUMITEMS (TB-CB11-FL-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB11-FL SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB11-FL-IDENT)                                      
      *        INTO     (TS-CB11-FL-ENR)                                        
      *        LENGTH   (LENGTH OF TS-CB11-FL-ENR)                              
      *        ITEM     (TB-CB11-FL-POS)                                        
      *        NUMITEMS (TB-CB11-FL-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB11-FL-IDENT)                                      
               INTO     (TS-CB11-FL-ENR)                                        
               LENGTH   (LENGTH OF TS-CB11-FL-ENR)                              
               ITEM     (TB-CB11-FL-POS)                                        
               NUMITEMS (TB-CB11-FL-NB)                                         
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-FL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-FL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 TB-CB11-FL-NB TB-CB11-FL-POS                         
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB11-FL SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-FL-IDENT)                                      
      *        FROM     (TS-CB11-FL-ENR)                                        
      *        ITEM     (TB-CB11-FL-POS)                                        
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-FL-IDENT)                                      
               FROM     (TS-CB11-FL-ENR)                                        
               ITEM     (TB-CB11-FL-POS)                                        
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-FL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-FL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-FL-POS                              
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      ******************************************************************        
      * TS RELEVE                                                               
      ******************************************************************        
       EFFACE-TS-CB11-RL SECTION.                                               
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB11-RL-IDENT)                                      
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB11-RL-IDENT)                                      
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-RL-IDENT                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB11-RL-NB                                          
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB11-RL SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-RL-IDENT)                                      
      *        FROM     (TS-CB11-RL-ENR)                                        
      *        NUMITEMS (TB-CB11-RL-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-RL-IDENT)                                      
               FROM     (TS-CB11-RL-ENR)                                        
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB11-RL-NB)                                         
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB11-RL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-RL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-RL-NB                               
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB11-RL-IDENT)
                  NUMITEMS (TB-CB11-RL-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB11-RL SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB11-RL-IDENT)                                      
      *        INTO     (TS-CB11-RL-ENR)                                        
      *        LENGTH   (LENGTH OF TS-CB11-RL-ENR)                              
      *        ITEM     (TB-CB11-RL-POS)                                        
      *        NUMITEMS (TB-CB11-RL-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB11-RL-IDENT)                                      
               INTO     (TS-CB11-RL-ENR)                                        
               LENGTH   (LENGTH OF TS-CB11-RL-ENR)                              
               ITEM     (TB-CB11-RL-POS)                                        
               NUMITEMS (TB-CB11-RL-NB)                                         
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-RL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-RL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 TB-CB11-RL-NB TB-CB11-RL-POS                         
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB11-RL SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-RL-IDENT)                                      
      *        FROM     (TS-CB11-RL-ENR)                                        
      *        ITEM     (TB-CB11-RL-POS)                                        
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-RL-IDENT)                                      
               FROM     (TS-CB11-RL-ENR)                                        
               ITEM     (TB-CB11-RL-POS)                                        
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-RL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-RL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-RL-POS                              
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      ******************************************************************        
      * TS AFFICHAGE                                                            
      ******************************************************************        
       EFFACE-TS-CB11-AF SECTION.                                               
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB11-AF-IDENT)                                      
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB11-AF-IDENT)                                      
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-AF-IDENT                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB11-AF-NB                                          
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB11-AF SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-AF-IDENT)                                      
      *        FROM     (TS-CB11-AF-ENR)                                        
      *        NUMITEMS (TB-CB11-AF-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-AF-IDENT)                                      
               FROM     (TS-CB11-AF-ENR)                                        
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB11-AF-NB)                                         
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB11-AF-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-AF-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-AF-NB                               
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB11-AF-IDENT)
                  NUMITEMS (TB-CB11-AF-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB11-AF SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB11-AF-IDENT)                                      
      *        INTO     (TS-CB11-AF-ENR)                                        
      *        LENGTH   (LENGTH OF TS-CB11-AF-ENR)                              
      *        ITEM     (TB-CB11-AF-POS)                                        
      *        NUMITEMS (TB-CB11-AF-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB11-AF-IDENT)                                      
               INTO     (TS-CB11-AF-ENR)                                        
               LENGTH   (LENGTH OF TS-CB11-AF-ENR)                              
               ITEM     (TB-CB11-AF-POS)                                        
               NUMITEMS (TB-CB11-AF-NB)                                         
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-AF-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-AF-IDENT TS-CB11-        
      *dfhei*     DFHB0020 TB-CB11-AF-NB TB-CB11-AF-POS                         
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB11-AF SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-AF-IDENT)                                      
      *        FROM     (TS-CB11-AF-ENR)                                        
      *        ITEM     (TB-CB11-AF-POS)                                        
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-AF-IDENT)                                      
               FROM     (TS-CB11-AF-ENR)                                        
               ITEM     (TB-CB11-AF-POS)                                        
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-AF-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-AF-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-AF-POS                              
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      ******************************************************************        
      * TS CONCURRENT                                                           
      ******************************************************************        
       EFFACE-TS-CB11-NC SECTION.                                               
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB11-NC-IDENT)                                      
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB11-NC-IDENT)                                      
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NC-IDENT                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB11-NC-NB                                          
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB11-NC SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-NC-IDENT)                                      
      *        FROM     (TS-CB11-NC-ENR)                                        
      *        NUMITEMS (TB-CB11-NC-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-NC-IDENT)                                      
               FROM     (TS-CB11-NC-ENR)                                        
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB11-NC-NB)                                         
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB11-NC-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NC-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-NC-NB                               
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB11-NC-IDENT)
                  NUMITEMS (TB-CB11-NC-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB11-NC SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB11-NC-IDENT)                                      
      *        INTO     (TS-CB11-NC-ENR)                                        
      *        LENGTH   (LENGTH OF TS-CB11-NC-ENR)                              
      *        ITEM     (TB-CB11-NC-POS)                                        
      *        NUMITEMS (TB-CB11-NC-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB11-NC-IDENT)                                      
               INTO     (TS-CB11-NC-ENR)                                        
               LENGTH   (LENGTH OF TS-CB11-NC-ENR)                              
               ITEM     (TB-CB11-NC-POS)                                        
               NUMITEMS (TB-CB11-NC-NB)                                         
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-NC-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NC-IDENT TS-CB11-        
      *dfhei*     DFHB0020 TB-CB11-NC-NB TB-CB11-NC-POS                         
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB11-NC SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-NC-IDENT)                                      
      *        FROM     (TS-CB11-NC-ENR)                                        
      *        ITEM     (TB-CB11-NC-POS)                                        
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-NC-IDENT)                                      
               FROM     (TS-CB11-NC-ENR)                                        
               ITEM     (TB-CB11-NC-POS)                                        
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-NC-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NC-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-NC-POS                              
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      ******************************************************************        
      * TS CONCURRENT - MAGASINS LIES                                           
      ******************************************************************        
       EFFACE-TS-CB11-NL SECTION.                                               
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB11-NL-IDENT)                                      
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB11-NL-IDENT)                                      
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NL-IDENT                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB11-NL-NB                                          
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB11-NL SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-NL-IDENT)                                      
      *        FROM     (TS-CB11-NL-ENR)                                        
      *        NUMITEMS (TB-CB11-NL-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-NL-IDENT)                                      
               FROM     (TS-CB11-NL-ENR)                                        
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB11-NL-NB)                                         
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB11-NL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-NL-NB                               
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB11-NL-IDENT)
                  NUMITEMS (TB-CB11-NL-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB11-NL SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB11-NL-IDENT)                                      
      *        INTO     (TS-CB11-NL-ENR)                                        
      *        LENGTH   (LENGTH OF TS-CB11-NL-ENR)                              
      *        ITEM     (TB-CB11-NL-POS)                                        
      *        NUMITEMS (TB-CB11-NL-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB11-NL-IDENT)                                      
               INTO     (TS-CB11-NL-ENR)                                        
               LENGTH   (LENGTH OF TS-CB11-NL-ENR)                              
               ITEM     (TB-CB11-NL-POS)                                        
               NUMITEMS (TB-CB11-NL-NB)                                         
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-NL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 TB-CB11-NL-NB TB-CB11-NL-POS                         
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB11-NL SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-NL-IDENT)                                      
      *        FROM     (TS-CB11-NL-ENR)                                        
      *        ITEM     (TB-CB11-NL-POS)                                        
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-NL-IDENT)                                      
               FROM     (TS-CB11-NL-ENR)                                        
               ITEM     (TB-CB11-NL-POS)                                        
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-NL-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-NL-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-NL-POS                              
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      ******************************************************************        
      * TS MAGASINS                                                             
      ******************************************************************        
       EFFACE-TS-CB11-MG SECTION.                                               
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        DELETEQ TS                                                       
      *        QUEUE    (TS-CB11-MG-IDENT)                                      
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS DELETEQ TS                                                 
               QUEUE    (TS-CB11-MG-IDENT)                                      
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '����05557   ' TO DFHEIV0                               
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-MG-IDENT                 
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
               MOVE 0 TO TB-CB11-MG-NB                                          
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       ECRIT-TS-CB11-MG SECTION.                                                
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-MG-IDENT)                                      
      *        FROM     (TS-CB11-MG-ENR)                                        
      *        NUMITEMS (TB-CB11-MG-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-MG-IDENT)                                      
               FROM     (TS-CB11-MG-ENR)                                        
      *{Post-translation Correct-Temp-Cics
      *         NUMITEMS (TB-CB11-MG-NB)                                         
      *}
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y��05571   ' TO DFHEIV0                              
      *dfhei*     MOVE LENGTH OF TS-CB11-MG-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-MG-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-MG-NB                               
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET ANORMAL TO TRUE                                              
           END-EVALUATE                                                         
           .                                                                    
      *{Post-translation Correct-Temp-Cics
           EXEC CICS
             INQUIRE TSQUEUE(TS-CB11-MG-IDENT)
                  NUMITEMS (TB-CB11-MG-NB)
                  NOHANDLE
            END-EXEC
            EVALUATE EIBRESP
            WHEN 0
                 SET NORMAL TO TRUE
            WHEN OTHER
                 SET ANORMAL TO TRUE
            END-EVALUATE.
      *}
      *                                                                         
       LECTURE-TS-CB11-MG SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        READQ TS                                                         
      *        QUEUE    (TS-CB11-MG-IDENT)                                      
      *        INTO     (TS-CB11-MG-ENR)                                        
      *        LENGTH   (LENGTH OF TS-CB11-MG-ENR)                              
      *        ITEM     (TB-CB11-MG-POS)                                        
      *        NUMITEMS (TB-CB11-MG-NB)                                         
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS READQ TS                                                   
               QUEUE    (TS-CB11-MG-IDENT)                                      
               INTO     (TS-CB11-MG-ENR)                                        
               LENGTH   (LENGTH OF TS-CB11-MG-ENR)                              
               ITEM     (TB-CB11-MG-POS)                                        
               NUMITEMS (TB-CB11-MG-NB)                                         
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '��8�05587   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-MG-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-MG-IDENT TS-CB11-        
      *dfhei*     DFHB0020 TB-CB11-MG-NB TB-CB11-MG-POS                         
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET TROUVE TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
      *                                                                         
       MODIFIE-TS-CB11-MG SECTION.                                              
      *dfhei*{ decommente EXEC                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *        WRITEQ TS                                                        
      *        QUEUE    (TS-CB11-MG-IDENT)                                      
      *        FROM     (TS-CB11-MG-ENR)                                        
      *        ITEM     (TB-CB11-MG-POS)                                        
      *        REWRITE                                                          
      *        NOHANDLE                                                         
      *    END-EXEC                                                             
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
               QUEUE    (TS-CB11-MG-IDENT)                                      
               FROM     (TS-CB11-MG-ENR)                                        
               ITEM     (TB-CB11-MG-POS)                                        
               REWRITE                                                          
               NOHANDLE                                                         
           END-EXEC                                                             
      *}                                                                        
      *dfhei*- commente DFHEI1                                                  
      *dfhei*     MOVE '�Y�05605   ' TO DFHEIV0                               
      *dfhei*     MOVE LENGTH OF TS-CB11-MG-ENR TO DFHB0020                     
      *dfhei*     CALL 'DFHEI1' USING DFHEIV0  TS-CB11-MG-IDENT TS-CB11-        
      *dfhei*     DFHB0020 DFHDUMMY TB-CB11-MG-POS                              
      *dfhei*} decommente EXEC                                                  
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
               SET NORMAL TO TRUE                                               
           WHEN OTHER                                                           
               SET NON-TROUVE TO TRUE                                           
           END-EVALUATE                                                         
           .                                                                    
                                                                                
                                                                                
