      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      * -------------------------------------------------------------- *        
      * CETTE COPIE EST UN PEU PLUS COMPLETTE QUE LE CSTENVOI,         *        
      * LORS DE LA LECTURE DE LA TS, IL VERIFIE QU'IL N'Y A PAS DE     *        
      * DOUBLONS                                                       *        
      * POUR EVITER DE DELETER UNE TS EN COURS DE CREATION, ON LIT     *        
      * PLUSIEURS FOIS LA TS AFIN DE S'ASSURER QU'ELLE EST COMPLETE    *        
      * (ENREG DE FIN TROUVE) OU QU'ELLE N'EST PLUS EN COURS DE        *        
      * CREATION(LE DERNIER ENREG LU EST IDENTIQUE A 1 SECONDE         *        
      * D'INTERVAL)                                                    *        
      * TSTENVOI: ** CREATION SI CETTE DERNIERE N'EXISTE PAS           *        
      *           ** LECTURE DE CETTE TS                               *        
      * LES ZONES DE TRAVAIL SONT DECLAREES DANS WKTENVOI              *        
      *                                                                *        
      * CREE LE 03/11/2011 PAR DE02074 (EG)                            *        
      *                                                                *        
      * -------------------------------------------------------------- *        
       GESTION-TSTENVOI  SECTION.                                               
      ****************************                                              
           MOVE 1                            TO WP-RANG-TSTENVOI                
           PERFORM VARYING IND-TS FROM 1 BY 1 UNTIL IND-TS > 500                
              MOVE SPACE TO POSTE-TSTENVOI(IND-TS)                              
           END-PERFORM                                                          
           MOVE ZERO TO IND-TS                                                  
      * PREMIERE LECTURE                                                        
           PERFORM LECTURE-TSTENV                                               
           IF WC-TSTENVOI-FIN                                                   
      * ON NE TROUVE PAS DE TS --> ON LA CREE                                   
              PERFORM CREATION-TSTENVOI                                         
           ELSE                                                                 
      * SI NON, ON CONTROLE QU'IL N'Y A PAS DE DOUBLON                          
      * INTI DE LA TABLE INTERNE                                                
              PERFORM VARYING IND-TS FROM 1 BY 1 UNTIL IND-TS > 500             
                 MOVE SPACE TO POSTE-TSTENVOI(IND-TS)                           
              END-PERFORM                                                       
              MOVE ZERO TO IND-TS                                               
      * ON CHERCHE LE DERNIER ENREG DE LA TSTENVOI                              
             PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                       
             UNTIL WC-TSTENVOI-FIN OR WC-TSTENVOI-ERREUR                        
                PERFORM LECTURE-TSTENV                                          
             END-PERFORM                                                        
             IF WC-TSTENVOI-ERREUR                                              
                PERFORM DELETE-TSTENVOI                                         
                PERFORM CREATION-TSTENVOI                                       
                SET WC-TSTENVOI-SUITE TO TRUE                                   
             ELSE                                                               
      * SI ON TROUVE LA FIN, C'EST BON, SI NON, ON REFAIS 1 LECTURE             
      * APRES 1 SECONDE D'ATTENTE                                               
             IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '                         
               MOVE TSTENVOI-LIBELLE TO DER-ENREG                               
               PERFORM TRT-TSTENVOI                                             
      * SI ON TROUVE LA FIN, C'EST BON, SI NON, ON COMPARE LES 2                
      * DERNIERS ENREG DE FIN LU, S'ILS SONT IDENTIQUES, ON DELETE LA TS        
      * ET ON LA RECREE, SI NON, ON REFAIS 1 LECTURE APRES 1 SECONDE            
      * D'ATTENTE                                                               
               IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '                       
                  MOVE DER-ENREG TO AVANT-DER-ENREG                             
                  MOVE TSTENVOI-LIBELLE TO DER-ENREG                            
                  IF DER-ENREG = AVANT-DER-ENREG                                
                     PERFORM DELETE-TSTENVOI                                    
                     PERFORM CREATION-TSTENVOI                                  
                  ELSE                                                          
                     PERFORM TRT-TSTENVOI                                       
      * SI ON TROUVE LA FIN, C'EST BON, SI NON, ON COMPARE LES 2                
      * DERNIERS ENREG DE FIN LU, S'ILS SONT IDENTIQUES, ON DELETE LA TS        
      * ET ON LA RECREE, SI NON, ON PLANTE                                      
                     IF TSTENVOI-LIBELLE  NOT = 'FIN TSTENVOI '                 
                        MOVE DER-ENREG TO AVANT-DER-ENREG                       
                        MOVE TSTENVOI-LIBELLE TO DER-ENREG                      
                        IF DER-ENREG = AVANT-DER-ENREG                          
                           PERFORM DELETE-TSTENVOI                              
                           PERFORM CREATION-TSTENVOI                            
                        ELSE                                                    
                           SET WC-TSTENVOI-ERREUR TO TRUE                       
                        END-IF                                                  
                     END-IF                                                     
                  END-IF                                                        
               END-IF                                                           
             END-IF                                                             
             END-IF                                                             
           END-IF.                                                              
       FIN-GEST-TSTENVOI.  EXIT.                                                
      *                                                                         
      *-----------------------------------------------------------------        
       CREATION-TSTENVOI SECTION.                                               
              MOVE '00000'                   TO TSTENVOI-NSOCZP                 
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM                 
              MOVE 'NOM PROGRAMME'           TO TSTENVOI-LIBELLE                
              PERFORM ECRITURE-TSTENVOI                                         
              SET WC-XCTRL-EXPDEL-NON-TROUVE TO TRUE                            
              INITIALIZE WZ-XCTRL-EXPDEL-SQLCODE                                
              PERFORM DECLARE-OPEN-CXCTRL                                       
              IF WC-XCTRL-EXPDEL-PB-DB2                                         
                 SET WC-TSTENVOI-ERREUR TO TRUE                                 
                 GO TO FIN-CREATION-TSTENVOI                                    
              END-IF                                                            
              PERFORM FETCH-CXCTRL                                              
              IF WC-XCTRL-EXPDEL-PB-DB2                                         
                 SET WC-TSTENVOI-ERREUR TO TRUE                                 
                 GO TO FIN-CREATION-TSTENVOI                                    
              END-IF                                                            
              PERFORM UNTIL WC-XCTRL-EXPDEL-FIN                                 
                 IF  XCTRL-WPARAM   > SPACES                                    
                 AND XCTRL-LIBELLE  > SPACES                                    
                    MOVE XCTRL-SOCZP         TO TSTENVOI-NSOCZP                 
                    MOVE XCTRL-CTRL          TO TSTENVOI-CTRL                   
                    MOVE XCTRL-WPARAM        TO TSTENVOI-WPARAM                 
                    MOVE XCTRL-LIBELLE       TO TSTENVOI-LIBELLE                
                    PERFORM ECRITURE-TSTENVOI                                   
                 END-IF                                                         
                 PERFORM FETCH-CXCTRL                                           
                 IF WC-XCTRL-EXPDEL-PB-DB2                                      
                    SET WC-TSTENVOI-ERREUR TO TRUE                              
                    GO TO FIN-CREATION-TSTENVOI                                 
                 END-IF                                                         
              END-PERFORM                                                       
              PERFORM CLOSE-CXCTRL                                              
      * AJOUT D'UN ENREG DE FIN                                                 
              MOVE '99999'                   TO TSTENVOI-NSOCZP                 
              MOVE 'ZZZZ'                    TO TSTENVOI-CTRL                   
              MOVE WS-TSTENVOI-NOMPGM        TO TSTENVOI-WPARAM                 
              MOVE 'FIN TSTENVOI '           TO TSTENVOI-LIBELLE                
              PERFORM ECRITURE-TSTENVOI.                                        
       FIN-CREATION-TSTENVOI.  EXIT.                                            
      *                                                                         
       TRT-TSTENVOI SECTION.                                                    
      ***********************                                                   
      * ON ATTENT 1 SECONDE AVANT DE RELIRE LA TS                               
           INITIALIZE WN-TIME WN-TIME2 WN-DELTATIME                             
           ACCEPT WN-TIME  FROM TIME                                            
           PERFORM UNTIL WN-DELTATIME > 00000100                                
             ACCEPT WN-TIME2 FROM TIME                                          
             COMPUTE WN-DELTATIME = WN-TIME2 - WN-TIME                          
           END-PERFORM                                                          
           SET WC-TSTENVOI-SUITE      TO TRUE                                   
           PERFORM VARYING WP-RANG-TSTENVOI FROM 1 BY 1                         
           UNTIL WC-TSTENVOI-FIN                                                
              PERFORM LECTURE-TSTENV                                            
           END-PERFORM                                                          
           .                                                                    
       FIN-TRT-TSTENVOI. EXIT.                                                  
      *                                                                         
      *-----------------------------------------------------------------        
       LECTURE-TSTENV SECTION.                                                  
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         READQ TS                                                        
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         INTO   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         ITEM   (WP-RANG-TSTENVOI)                                       
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
              SET WC-TSTENVOI-SUITE      TO TRUE                                
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
      * POUR CHAQUE ENREG LU, ON VERIFIE DANS LA TABLE INTERNE SI               
      * ON A PAS DEJA LU LE MEME ENREG                                          
      *                                                                         
              PERFORM VARYING IND-TS FROM 1 BY 1 UNTIL                          
              POSTE-TSTENVOI(IND-TS) = SPACE                                    
              OR WC-TSTENVOI-ERREUR                                             
                IF POSTE-TSTENVOI(IND-TS) = TS-TSTENVOI-DONNEES                 
                   SET WC-TSTENVOI-ERREUR     TO TRUE                           
                END-IF                                                          
              END-PERFORM                                                       
      * ON A PAS TROUVE L'ENREG --> ON L'AJOUTE DANS LA TABLE                   
              IF NOT WC-TSTENVOI-ERREUR                                         
      *{ reorder-array-condition 1.1                                            
      *         IF POSTE-TSTENVOI(IND-TS) = SPACE AND IND-TS < 500              
      *--                                                                       
                IF IND-TS < 500 AND POSTE-TSTENVOI(IND-TS) = SPACE              
      *}                                                                        
                   MOVE TS-TSTENVOI-DONNEES TO POSTE-TSTENVOI(IND-TS)           
                ELSE                                                            
                   SET WC-TSTENVOI-ERREUR     TO TRUE                           
                END-IF                                                          
              END-IF                                                            
           WHEN 26                                                              
           WHEN 44                                                              
              SET WC-TSTENVOI-FIN        TO TRUE                                
           WHEN OTHER                                                           
              SET WC-TSTENVOI-ERREUR     TO TRUE                                
           END-EVALUATE.                                                        
       FIN-LECT-TSTENV. EXIT.                                                   
      *                                                                         
       LECTURE-TSTENVOI SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         READQ TS                                                        
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         INTO   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         ITEM   (WP-RANG-TSTENVOI)                                       
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTENVOI)                                         
                INTO   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                ITEM   (WP-RANG-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           EVALUATE EIBRESP                                                     
           WHEN 0                                                               
              SET WC-TSTENVOI-SUITE      TO TRUE                                
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
           WHEN 26                                                              
           WHEN 44                                                              
              SET WC-TSTENVOI-FIN        TO TRUE                                
           WHEN OTHER                                                           
              SET WC-TSTENVOI-ERREUR     TO TRUE                                
           END-EVALUATE.                                                        
       FIN-LECT-TSTENVOI. EXIT.                                                 
      *                                                                         
      *-----------------------------------------------------------------        
       ECRITURE-TSTENVOI SECTION.                                               
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         WRITEQ TS                                                       
      *         QUEUE  (WC-ID-TSTENVOI)                                         
      *         FROM   (TS-TSTENVOI-DONNEES)                                    
      *         LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
                QUEUE  (WC-ID-TSTENVOI)                                         
                FROM   (TS-TSTENVOI-DONNEES)                                    
                LENGTH (LENGTH OF TS-TSTENVOI-DONNEES)                          
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           INITIALIZE TS-TSTENVOI-DONNEES.                                      
       FIN-ECRIT-TSTENVOI. EXIT.                                                
      *                                                                         
      *-----------------------------------------------------------------        
       DECLARE-OPEN-CXCTRL SECTION.                                             
           MOVE FUNC-DECLARE             TO TRACE-SQL-FUNCTION.                 
           MOVE 'RVGA01ZZ'               TO TABLE-NAME.                         
           MOVE 'EXPDEL'                 TO MODEL-NAME.                         
           EXEC SQL                                                             
                DECLARE CXCTRL CURSOR FOR                                       
                SELECT SOCZP, CTRL, WPARAM, LIBELLE                             
                FROM RVGA01ZZ                                                   
                WHERE TRANS = 'EXPDEL'                                          
                AND   WFLAG = 'O'                                               
                ORDER BY SOCZP, CTRL                                            
           END-EXEC.                                                            
           PERFORM TEST-SQLCODE.                                                
           MOVE FUNC-OPEN                TO TRACE-SQL-FUNCTION.                 
           EXEC SQL OPEN CXCTRL END-EXEC.                                       
           PERFORM TEST-SQLCODE.                                                
      *                                                                         
       FETCH-CXCTRL SECTION.                                                    
           MOVE FUNC-SELECT              TO TRACE-SQL-FUNCTION.                 
           EXEC SQL                                                             
                FETCH CXCTRL                                                    
                INTO :XCTRL-SOCZP                                               
                   , :XCTRL-CTRL                                                
                   , :XCTRL-WPARAM                                              
                   , :XCTRL-LIBELLE                                             
           END-EXEC.                                                            
           PERFORM TEST-SQLCODE.                                                
           IF WC-XCTRL-EXPDEL-SUITE                                             
              SET WC-XCTRL-EXPDEL-TROUVE TO TRUE                                
           END-IF.                                                              
      *                                                                         
       CLOSE-CXCTRL SECTION.                                                    
           MOVE FUNC-CLOSE               TO TRACE-SQL-FUNCTION.                 
           EXEC SQL CLOSE CXCTRL  END-EXEC.                                     
           PERFORM TEST-SQLCODE.                                                
      *                                                                         
       DELETE-TSTENVOI SECTION.                                                 
      ******************************                                            
           EXEC CICS DELETEQ TS                                                 
                QUEUE    (WC-ID-TSTENVOI)                                       
                NOHANDLE                                                        
           END-EXEC.                                                            
       FIN-DELETE-TSTENVOI. EXIT.                                               
      ********************************                                          
      *-----------------------------------------------------------------        
       TEST-SQLCODE SECTION.                                                    
            EVALUATE TRUE                                                       
            WHEN SQLCODE = 0                                                    
                 SET WC-XCTRL-EXPDEL-SUITE    TO TRUE                           
            WHEN SQLCODE = +100                                                 
                 SET WC-XCTRL-EXPDEL-FIN      TO TRUE                           
            WHEN OTHER                                                          
                 SET WC-XCTRL-EXPDEL-PB-DB2   TO TRUE                           
                 SET WC-XCTRL-EXPDEL-FIN      TO TRUE                           
                 MOVE SQLCODE                 TO WZ-XCTRL-EXPDEL-SQLCODE        
           END-EVALUATE.                                                        
                                                                                
