      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MMVEN EXCLUSION VENDEUR                *        
      *----------------------------------------------------------------*        
       01  RVMMVEN .                                                            
           05  MMVEN-CTABLEG2    PIC X(15).                                     
           05  MMVEN-CTABLEG2-REDEF REDEFINES MMVEN-CTABLEG2.                   
               10  MMVEN-CVENDEUR        PIC X(06).                             
           05  MMVEN-WTABLEG     PIC X(80).                                     
           05  MMVEN-WTABLEG-REDEF  REDEFINES MMVEN-WTABLEG.                    
               10  MMVEN-WACTIF          PIC X(01).                             
               10  MMVEN-COMMENT         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMMVEN-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMVEN-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MMVEN-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMVEN-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MMVEN-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
