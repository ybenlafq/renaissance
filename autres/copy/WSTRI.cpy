      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *=====================================================*                   
      *                                                     *                   
      *           MODULE DE TRI INTERNE D'UNE TABLE         *                   
      *           ---------------------------------         *                   
      *                                                     *                   
      *   EXEMPLE D'UTILISATION :                           *                   
      *                                                     *                   
      *   POUR UN TRI, EN ASCENDANT, SUR UNE CLE DE 20      *                   
      *   OCTETS D'UNE TABLE DE 240 POSTES                  *                   
      *                                                     *                   
      *   1�) DEFINIR EN WORKING :                          *                   
      *                                                     *                   
      *       COPY WSTRI REPLACING :NB-OCCURS: BY ==240==   *                   
      *                            :LG-CLETRI: BY ==020==.  *                   
      *                                                     *                   
      *   2�) INTEGRER EN PROCEDURE DIVISION :              *                   
      *                                                     *                   
      *       COPY PRTRI.                                   *                   
      *                                                     *                   
      *=====================================================*                   
      *                                                                         
       77     WTI         PIC  9(05)          VALUE 0.                          
       77     WTJ         PIC  9(05)          VALUE 0.                          
       77     WTK         PIC  9(05)          VALUE 0.                          
      *                                                                         
       77     WNBLTSMAX   PIC  9(07)          VALUE 0.                          
       77     WNBPMAX     PIC  9(05)          VALUE 0.                          
       77     WNBLMAX     PIC  9(02)          VALUE 0.                          
      *                                                                         
       77     WTCLETRI    PIC  X(:LG-CLETRI:) VALUE SPACES.                     
       77     WTNBITEM    PIC  9(03)          VALUE 0.                          
       77     WTNLIGNE    PIC  9(03)          VALUE 0.                          
      *                                                                         
      *       TYPE DE TRI (CROISSANT/DECROISSANT)                               
      *                                                                         
       77     WTYPTRI     PIC  X(01)          VALUE 'C'.                        
         88   CROISSANT                       VALUE 'C'.                        
         88   DECROISSANT                     VALUE 'D'.                        
      *                                                                         
      *       DEBUT DU NOM DE LA TS RECEPTRICE                                  
      *                                                                         
       77     WNPRG       PIC  X(04)          VALUE SPACES.                     
      *                                                                         
      *       TABLE INTERNE DE TRI                                              
      *                                                                         
       1      WTABLETRI.                                                        
        2     WTABLTRI.                                                         
          4   WTABTRI                OCCURS         :NB-OCCURS:.                
            8 WCLETRI     PIC  X(:LG-CLETRI:).                                  
            8 WNBITEM     PIC  9(03).                                           
            8 WNLIGNE     PIC  9(03).                                           
        2     WTIMAX      PIC  9(05)          VALUE :NB-OCCURS:.                
      *                                                                         
                                                                                
