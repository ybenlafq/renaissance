      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD1401.                                                            
      *}                                                                        
           10 AD14-CFAMK           PIC X(5).                                    
           10 AD14-CFAM            PIC X(5).                                    
           10 AD14-LFAM            PIC X(20).                                   
           10 AD14-WSEQFAM         PIC S9(05) COMP-3.                           
           10 AD14-WMULTIFAM       PIC X.                                       
           10 AD14-CTYPENT         PIC X(2).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ************************************************                          
      * INDICATOR VARIABLE STRUCTURE                                            
      ************************************************                          
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD1401-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD1401-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-CFAMK-F         PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-CFAMK-F         PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-CFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-CFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-LFAM-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-LFAM-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-WSEQFAM-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-WSEQFAM-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-WMULTIFAM-F     PIC S9(4) COMP.                              
      *--                                                                       
           10 AD14-WMULTIFAM-F     PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD14-CTYPENT-F       PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 AD14-CTYPENT-F       PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
