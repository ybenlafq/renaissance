      * Traitements de gestion des tables DB2 RTIGS*
      *
      *
       DECLARE-ALL-DIGS-CURSORS SECTION.
           PERFORM DECLARE-CURS-DIGSA.
           PERFORM DECLARE-CURS-DIGSC.
           PERFORM DECLARE-CURS-DIGSF.
       F-DECLARE-ALL-DIGS-CURSORS. EXIT.

       DECLARE-CURS-DIGSA  SECTION.
           EXEC SQL DECLARE CURS_DIGSA CURSOR FOR
              SELECT DIGFA0, COL01001
              FROM RTIGSA
              WHERE DIGFA0 > :H-DIGFA0
           END-EXEC.
       F-DECLARE-CURS-DIGSA. EXIT.

       DECLARE-CURS-DIGSF  SECTION.
           EXEC SQL DECLARE CURS_DIGSF CURSOR FOR
              SELECT DIGFF0, COL04001
              FROM RTIGSF
              WHERE DIGFA0 = :H-DIGFA0
               AND  DIGFF0 >= :H-DIGFF0
           END-EXEC.
       F-DECLARE-CURS-DIGSF. EXIT.

       DECLARE-CURS-DIGSC  SECTION.
           EXEC SQL DECLARE CURS_DIGSC CURSOR FOR
              SELECT DIGFC0, SM_DIGSC, COL06001, COL06002
              FROM RTIGSC
              WHERE DIGFA0 = :H-DIGFA0
               AND  ( DIGFC0 > :H-DIGFC0
               OR   ( DIGFC0 = :H-DIGFC0 AND
                      SM_DIGSC > :H-SM-DIGSC ))
           END-EXEC.
       F-DECLARE-CURS-DIGSC. EXIT.

       OPEN-CURS-DIGSA SECTION.
           EXEC SQL OPEN CURS_DIGSA
           END-EXEC.
           IF SQLCODE = 0 
              SET CURS-DIGSA-OPEN TO TRUE
           END-IF.
       F-OPEN-CURS-DIGSA. EXIT.

       FETCH-CURS-DIGSA SECTION.
           EXEC SQL FETCH CURS_DIGSA
             INTO :H-DIGFA0,
                  :H-COL01001
           END-EXEC.
       F-FETCH-CURS-DIGSA. EXIT.    

       CLOSE-CURS-DIGSA SECTION.
           EXEC SQL CLOSE CURS_DIGSA
           END-EXEC.
           IF SQLCODE = 0
              SET CURS-DIGSA-CLOSE TO TRUE
           END-IF.
       F-CLOSE-CURS-DIGSA. EXIT.

       OPEN-CURS-DIGSF SECTION.
           EXEC SQL OPEN CURS_DIGSF
           END-EXEC.
           IF SQLCODE = 0
              SET CURS-DIGSF-OPEN TO TRUE
           END-IF.
       F-OPEN-CURS-DIGSF. EXIT.

       FETCH-CURS-DIGSF SECTION.
           EXEC SQL FETCH CURS_DIGSF
             INTO :H-DIGFF0,
                  :H-COL04001
           END-EXEC.
       F-FETCH-CURS-DIGSF. EXIT.

       CLOSE-CURS-DIGSF SECTION.
           EXEC SQL CLOSE CURS_DIGSF
           END-EXEC.
           IF SQLCODE = 0
              SET CURS-DIGSF-CLOSE TO TRUE
           END-IF.
       F-CLOSE-CURS-DIGSF. EXIT.
        
       OPEN-CURS-DIGSC SECTION.
           EXEC SQL OPEN CURS_DIGSC
           END-EXEC.
           IF SQLCODE = 0 
              SET CURS-DIGSC-OPEN TO TRUE
           END-IF.
       F-OPEN-CURS-DIGSC. EXIT.

       FETCH-CURS-DIGSC SECTION.
             EXEC SQL FETCH CURS_DIGSC
               INTO :H-DIGFC0,
                    :H-SM-DIGSC,
                    :H-COL06001,
                    :H-COL06002
             END-EXEC.
       F-FETCH-CURS-DIGSC. EXIT.

       CLOSE-CURS-DIGSC SECTION.
           EXEC SQL CLOSE CURS_DIGSC
           END-EXEC.
           IF SQLCODE = 0
              SET CURS-DIGSC-CLOSE TO TRUE
           END-IF.
       F-CLOSE-CURS-DIGSA. EXIT.

       TRT-SQLCODE-DL1 SECTION.
           EVALUATE SQLCODE 
           WHEN 0
                 MOVE '  ' TO DIBSTAT
           WHEN 100 
                 IF GN 
                   MOVE 'GB' TO DIBSTAT
                 ELSE
                   MOVE 'GE' TO DIBSTAT
                 END-IF
           WHEN 803
                 MOVE 'II' TO DIBSTAT
           WHEN OTHER
                 MOVE 'NI' TO DIBSTAT
           END-EVALUATE.
       F-TRT-SQLCODE-DL1. EXIT.

           
