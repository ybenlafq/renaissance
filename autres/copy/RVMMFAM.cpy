      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MMFAM FAMILLES EXCLUES 1000 MERCIS     *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMMFAM .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMMFAM .                                                            
      *}                                                                        
           05  MMFAM-CTABLEG2    PIC X(15).                                     
           05  MMFAM-CTABLEG2-REDEF REDEFINES MMFAM-CTABLEG2.                   
               10  MMFAM-CFAM            PIC X(05).                             
               10  MMFAM-CODE            PIC X(07).                             
           05  MMFAM-WTABLEG     PIC X(80).                                     
           05  MMFAM-WTABLEG-REDEF  REDEFINES MMFAM-WTABLEG.                    
               10  MMFAM-TYPEX           PIC X(04).                             
               10  MMFAM-WFLAG           PIC X(01).                             
               10  MMFAM-COMMENT         PIC X(10).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVMMFAM-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVMMFAM-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMFAM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MMFAM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMFAM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MMFAM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
