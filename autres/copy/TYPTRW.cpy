      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      * DEBUT WORKING TYPTR                                             00010001
       01  W-TYPTR-CODE                     PIC X(01) VALUE SPACES.     00020002
      * ACCES A CES CODES PAR LA SOUS TABLE TYPTR                       00030000
       01 WS-FLAG-TYPE-DE-CODE              PIC X VALUE SPACE.          00040000
           88 W-TYPE-INCONNU                   VALUE 'I'.               00050000
       01 WS-FLAG-TYPE-DE-VENTE             PIC X VALUE SPACE.          00060000
           88 W-VTE-TYPE-INCONNU               VALUE 'I'.               00070000
      *                                                               * 00080000
      *---------------------------------------------------------------* 00090000
      *    DESCRIPTION DES COPIES DE LA TABLE GENERALISEE             * 00100000
      *---------------------------------------------------------------* 00110000
      *                                                               * 00120000
      *    CLE D'ACCES A LA TABLE GENERALISEE                         * 00130000
      *                                                               * 00140000
       01  WS-GA01-CTABLEG1                PIC X(05) VALUE SPACES.      00150000
       01  WS-GA01-CTABLEG2                PIC X(15) VALUE SPACES.      00160000
       01  WS-GA01-CTABLEG2-REDEF REDEFINES WS-GA01-CTABLEG2.           00170000
           10  WS-GA01-CTABLEG2-CODE.                                   00180000
               15  WS-GA01-CODE-VENTE      PIC X(01).                   00190000
               15  FILLER                  PIC X(02).                   00200000
           10  FILLER                      PIC X(12).                   00210000
      *                                                               * 00220000
      *------- ASSOCIATION CODE VENTE - TYPE DE VENTE : TYPTR --------* 00230000
      *                                                               * 00240000
       01  WS-GA01-TYPTR.                                               00250000
           10 WS-TYPTR-LIBELLE                  PIC X(20).              00260002
           10 WS-TYPTR-TYPE-TRANS               PIC X(1).               00270000
              88 W-TYPE-VENTE                VALUE 'V'.                 00280002
              88 W-TYPE-TRANS                VALUE 'T'.                 00290002
              88 W-TYPE-GENERAL              VALUE 'G'.                 00300002
              88 W-TYPE-NATURE               VALUE 'N'.                 00310002
           10 WS-TYPTR-APVF                     PIC X(4).               00320000
           10 WS-TYPTR-ACCES-VENTE              PIC X(2).               00330000
              88 W-ACCES-VE                  VALUE 'VE'.                00340002
              88 W-ACCES-CC                  VALUE 'CC'.                00350002
              88 W-ACCES-CP                  VALUE 'CP'.                00360002
              88 W-ACCES-DP                  VALUE 'DP'.                00370002
           10 WS-TYPTR-TYPE-VENTE               PIC X.                  00380000
              88 W-VTE-HOST                  VALUE 'H'.                 00390002
              88 W-VTE-ACOMPTE               VALUE 'A'.                 00400002
              88 W-VTE-DACEM                 VALUE 'D'.                 00410002
              88 W-VTE-LOC-AVEC-ADR          VALUE 'L'.                 00420002
              88 W-VTE-LOC-SANS-ADR          VALUE 'S'.                 00430002
              88 W-VTE-INTER-MAG             VALUE 'X'.                 00440002
           10 WS-TYPTR-FLAGS.                                           00450000
      * FLAG  1 : VENTE INVERSE AUTO. AUTORISEE O/N                     00460002
              15 WS-TYPTR-FLAG1                 PIC X.                  00470000
                 88 W-VIA-AUTORISE           VALUE 'O'.                 00480002
                 88 W-VIA-AUTORISE-N         VALUE 'N'.                 00490002
      * FLAG  2 : APPEL AU RECAP ENCAISSEMENTS AUTORISE O/N             00500002
              15 WS-TYPTR-FLAG2                 PIC X.                  00510000
                 88 W-RECAP-AUTORISE         VALUE 'O'.                 00520002
                 88 W-RECAP-AUTORISE-N       VALUE 'N'.                 00530002
      * FLAG  3 : RAPPEL DE VENTE AUTORISE O/N                          00540002
              15 WS-TYPTR-FLAG3                 PIC X.                  00550000
                 88 W-RAPPEL-AUTORISE        VALUE 'O'.                 00560002
                 88 W-RAPPEL-AUTORISE-N      VALUE 'N'.                 00570002
      * FLAG  4 : FACTURE AUTORISEE O/N                                 00580002
              15 WS-TYPTR-FLAG4                 PIC X.                  00590000
                 88 W-FACTURE-AUTORISE       VALUE 'O'.                 00600002
                 88 W-FACTURE-AUTORISE-N     VALUE 'N'.                 00610002
      * FLAG  5 : GV00 OPTION 2 AUTORISEE O/N                           00620002
              15 WS-TYPTR-FLAG5                 PIC X.                  00630000
                 88 W-GVOPT2-AUTORISE        VALUE 'O'.                 00640002
                 88 W-GVOPT2-AUTORISE-N      VALUE 'N'.                 00650002
      * FLAG  6 : SORTIE D'ARCHIVAGE AUTORISEE O/N                      00660002
              15 WS-TYPTR-FLAG6                 PIC X.                  00670000
                 88 W-ARCHIV-AUTORISE        VALUE 'O'.                 00680002
                 88 W-ARCHIV-AUTORISE-N      VALUE 'N'.                 00690002
      * FLAG  7 : AUTORISATION MODIF MODE RGLT AU RECAP AS400           00700003
              15 WS-TYPTR-FLAG7                 PIC X.                  00710000
                 88 W-AUTMMRGL-AUTORISE      VALUE 'O'.                 00711002
                 88 W-AUTMMRGL-AUTORISE-N    VALUE 'N'.                 00712002
      * FLAG  8 : GV00 OPTION 1 AUTORISEE O/N                           00720003
              15 WS-TYPTR-FLAG8                 PIC X.                  00730000
                 88 W-GVOPT1-AUTORISE        VALUE 'O'.                 00731002
                 88 W-GVOPT1-AUTORISE-N      VALUE 'N'.                 00732002
      * FLAG  9 : LIBRE                                                 00740002
              15 WS-TYPTR-FLAG9                 PIC X.                  00750000
      * FLAG 10 : LIBRE                                                 00760002
              15 WS-TYPTR-FLAG10                PIC X.                  00770000
      * FLAG 11 : LIBRE                                                 00780002
              15 WS-TYPTR-FLAG11                PIC X.                  00790000
      * FLAG 12 : LIBRE                                                 00800002
              15 WS-TYPTR-FLAG12                PIC X.                  00810000
      * FLAG 13 : LIBRE                                                 00820002
              15 WS-TYPTR-FLAG13                PIC X.                  00830000
      * FLAG 14 : LIBRE                                                 00840002
              15 WS-TYPTR-FLAG14                PIC X.                  00850000
      * FLAG 15 : LIBRE                                                 00860002
              15 WS-TYPTR-FLAG15                PIC X.                  00870000
      * FLAG 16 : LIBRE                                                 00880002
              15 WS-TYPTR-FLAG16                PIC X.                  00890000
      * FLAG 17 : LIBRE                                                 00900002
              15 WS-TYPTR-FLAG17                PIC X.                  00910000
           10 WS-GA01-TYPTR-WACTIF              PIC X.                  00920000
           10 FILLER                            PIC X(34).              00930000
      * FIN WORKING TYPTR                                               00940001
                                                                                
