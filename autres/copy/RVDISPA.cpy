      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE DISPA DISPATCH DES ETATS BATCH         *        
      *----------------------------------------------------------------*        
       01  RVDISPA.                                                             
           05  DISPA-CTABLEG2    PIC X(15).                                     
           05  DISPA-CTABLEG2-REDEF REDEFINES DISPA-CTABLEG2.                   
               10  DISPA-CETAT           PIC X(06).                             
               10  DISPA-CBOITE          PIC X(07).                             
               10  DISPA-NSEQ            PIC S9(03)       COMP-3.               
           05  DISPA-WTABLEG     PIC X(80).                                     
           05  DISPA-WTABLEG-REDEF  REDEFINES DISPA-WTABLEG.                    
               10  DISPA-NSOCIETE        PIC X(03).                             
               10  DISPA-CTYPE           PIC X(05).                             
               10  DISPA-LIBELLEC        PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVDISPA-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DISPA-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  DISPA-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  DISPA-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  DISPA-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
