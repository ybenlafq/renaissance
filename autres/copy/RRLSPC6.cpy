      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION R�CEPTION LIGNE SPECIFIQUE (MUTATION) * 00030000
      * NOM FICHIER.: RRLSPC6                                         * 00031000
      *---------------------------------------------------------------* 00032000
      * CR   .......: 27/08/2013                                      * 00033000
      * MODIFIE.....:   /  /                                          * 00034000
      * VERSION N�..: 001                                             * 00035000
      * LONGUEUR....: 282                                             * 00036000
      ***************************************************************** 00037000
      *                                                                 00037100
       01  RRLSPC6.                                                     00037200
      * TYPE ENREGISTREMENT                                             00037300
           05      RRLSPC6-TYP-ENREG      PIC  X(0006).                 00037400
      * CODE SOCI�T�                                                    00037500
           05      RRLSPC6-CSOCIETE       PIC  X(0005).                 00037600
      * NUM�RO DE DOSSIER DE R�CEPTION                                  00037700
           05      RRLSPC6-NDOS-REC       PIC  X(0015).                 00037800
      * NUM�RO DE LIGNE DE R�CEPTION                                    00037900
           05      RRLSPC6-NLIGNE-REC     PIC  9(0005).                 00038000
      * NUM�RO DE SOUS-LIGNE DE R�CEPTION                               00038100
           05      RRLSPC6-NSSLIGNE-REC   PIC  9(0005).                 00038200
      * R�F�RENCE FOURNISSEUR                                           00038300
           05      RRLSPC6-CARTICLE       PIC  X(0018).                 00038400
      * NOMBRE DE LIGNES DU DOSSIER DE R�CEPTION                        00038500
           05      RRLSPC6-NBLIG-NDOS-REC PIC  9(0005).                 00038600
      * R�CEPTION URGENTE                                               00038700
           05      RRLSPC6-REC-URGENTE    PIC  9(0001).                 00038800
      * NOMBRE DE JOURS DE BLOCAGE                                      00038900
           05      RRLSPC6-NBJOURS-BLOCAGE PIC 9(0003).                 00039000
      * INCOTERM                                                        00039100
           05      RRLSPC6-INCOTERM       PIC  X(0004).                 00039200
      * ADRESSE NOM                                                     00039300
           05      RRLSPC6-NOM            PIC  X(0030).                 00039400
      * ADRESSE 1                                                       00039500
           05      RRLSPC6-ADR1           PIC  X(0030).                 00039600
      * ADRESSE 2                                                       00039700
           05      RRLSPC6-ADR2           PIC  X(0030).                 00039800
      * ADRESSE 3                                                       00039900
           05      RRLSPC6-ADR3           PIC  X(0030).                 00040000
      * ADRESSE PAYS                                                    00040100
           05      RRLSPC6-PAYS           PIC  X(0004).                 00040200
      * ADRESSE CODE POSTAL                                             00040300
           05      RRLSPC6-CP             PIC  X(0009).                 00040400
      * D�CLARATIF                                                      00040500
           05      RRLSPC6-DECLARATIF     PIC  9(0001).                 00040600
      * TYPE DE FLUX                                                    00040700
           05      RRLSPC6-TYPE-FLUX      PIC  X(0002).                 00040800
      * DESTINATION                                                     00040900
           05      RRLSPC6-DESTINATION    PIC  X(0012).                 00041000
      * TYPE DE DOSSIER                                                 00041100
           05      RRLSPC6-TYPE-DOSSIER   PIC  X(0012).                 00041200
      * N� DOSSIER VENTE                                                00041300
           05      RRLSPC6-NDOS-VENTE     PIC  X(0012).                 00041400
      * INFO MAGASIN                                                    00041500
           05      RRLSPC6-INFO-MAGASIN   PIC  X(0012).                 00041600
      * FILLER 30                                                       00041700
           05      RRLSPC6-FILLER         PIC  X(0030).                 00041800
      * FIN                                                             00041900
           05      RRLSPC6-FIN            PIC  X(0001).                 00042000
                                                                                
