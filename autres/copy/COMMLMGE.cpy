      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01   ZONES-MGE.                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 COMM-LMGE-LONG  PIC S9(04) COMP VALUE +1900.                       
      *--                                                                       
          02 COMM-LMGE-LONG  PIC S9(04) COMP-5 VALUE +1900.                     
      *}                                                                        
          02 COMM-LMGE-DATA.                                                    
              03 COMM-LMGE-SOCIETE         PIC  X(03).                          
              03 COMM-LMGE-DEPOT           PIC  X(03).                          
              03 COMM-LMGE-NCODIC          PIC  X(07).                          
              03 COMM-LMGE-NBR-LOT         PIC  9(03).                          
              03 COMM-LMGE-QTE-LOT         PIC S9(05) COMP-3.                   
              03 COMM-LMGE-QTE-TOT         PIC S9(05) COMP-3.                   
              03 COMM-LMGE-ZA              PIC  X(01).                          
              03 COMM-LMGE-CO              PIC  X(01).                          
              03 COMM-LMGE-SP              PIC  X(01).                          
              03 COMM-LMGE-QCOTEHOLD       PIC S9(03) COMP-3.                   
              03 COMM-LMGE-QNBRANMAIL      PIC S9(03) COMP-3.                   
              03 COMM-LMGE-QNBNIVGERB      PIC S9(03) COMP-3.                   
              03 COMM-LMGE-SORTIE.                                              
                 04 ZONES-SORTIE OCCURS 100.                                    
                     05 COMM-LMGE-EMPLACEMENT.                                  
                        07 COMM-LMGE-EMP1     PIC  X(02).                       
                        07 COMM-LMGE-EMP2     PIC  X(02).                       
                        07 COMM-LMGE-EMP3     PIC  9(03).                       
                     05 COMM-LMGE-FOND-EMP    PIC S9(03) COMP-3.                
                     05 COMM-LMGE-NB-RGPROF   PIC S9(03) COMP-3.                
                     05 COMM-LMGE-FETIQUETTE  PIC  X(01).                       
                     05 COMM-LMGE-QTE-STOCK   PIC S9(05) COMP-3.                
                     05 COMM-LMGE-QTAUXOCC    PIC S9(03) COMP-3.                
                 04 COMM-LMGE-QTE-RES         PIC S9(05) COMP-3.                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *          04 COMM-LMGE-SQLCODE         PIC S9(09) COMP.                  
      *--                                                                       
                 04 COMM-LMGE-SQLCODE         PIC S9(09) COMP-5.                
      *}                                                                        
                 04 ZONES-EMPI OCCURS 13.                                       
                     05 COMM-LMGE-EMPI.                                         
                        07 COMM-LMGE-EMPI1    PIC  X(02).                       
                        07 COMM-LMGE-EMPI2    PIC  X(02).                       
                        07 COMM-LMGE-EMPI3    PIC  9(03).                       
                        07 COMM-LMGE-QTEI     PIC  S9(05) COMP-3.               
              03 COMM-LMGE-ZA-ART          PIC  X(01).                          
              03 COMM-LMGE-WRP             PIC  X(01).                          
              03 COMM-LMGE-CLASSE          PIC  X(01).                          
              03 COMM-LMGE-CMODSTORIG      PIC  X(05).                          
                                                                                
