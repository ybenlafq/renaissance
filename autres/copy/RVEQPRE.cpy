      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQPRE PARAMETRAGE PRESTATION 360       *        
      *----------------------------------------------------------------*        
       01  RVEQPRE .                                                            
           05  EQPRE-CTABLEG2    PIC X(15).                                     
           05  EQPRE-CTABLEG2-REDEF REDEFINES EQPRE-CTABLEG2.                   
               10  EQPRE-PREST           PIC X(05).                             
           05  EQPRE-WTABLEG     PIC X(80).                                     
           05  EQPRE-WTABLEG-REDEF  REDEFINES EQPRE-WTABLEG.                    
               10  EQPRE-AREALISE        PIC X(01).                             
               10  EQPRE-PL              PIC X(02).                             
               10  EQPRE-CLIENT          PIC X(01).                             
               10  EQPRE-DARTY           PIC X(01).                             
               10  EQPRE-DUREE           PIC X(03).                             
               10  EQPRE-DUREE-N        REDEFINES EQPRE-DUREE                   
                                         PIC 9(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVEQPRE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQPRE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQPRE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQPRE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQPRE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
