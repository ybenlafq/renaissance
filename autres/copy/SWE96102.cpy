      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *---------------------------------------------------------*               
      *    COPY DU FICHIER CSV D'EXTRACTION DE L'ETAT "QHS96102"*               
      *    LONGUEUR : 66                                        *               
      *---------------------------------------------------------*               
       01  H102-ENREG.                                                          
           05  H102-NOM              PIC X(08)  VALUE 'QHS96102'.               
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H102-CLIEUHET         PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H102-NLIEUHED         PIC X(03).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H102-CTRAIT           PIC X(05).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H102-NBRENR           PIC Z(8)9.                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H102-PRMP             PIC -(8)9,99.                              
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H102-DATED.                                                      
               10  H102-DEB-SA       PIC X(04).                                 
               10  H102-DEB-MM       PIC X(02).                                 
               10  H102-DEB-JJ       PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
           05  H102-DATEF.                                                      
               10  H102-FIN-SA       PIC X(04).                                 
               10  H102-FIN-MM       PIC X(02).                                 
               10  H102-FIN-JJ       PIC X(02).                                 
           05  FILLER                PIC X(01)  VALUE ';'.                      
      *                                                                         
      *---------------------------------------------------------*               
                                                                                
