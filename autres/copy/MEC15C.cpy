      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * LINK � MEC15 - M�J du "mouchard" commandes eCommerce                    
      ******************************************************************        
      *01  MEC15C-COMMAREA <== niveau 1 d�fini par le programme appelant        
INPUT ******************************************************************        
   1       10 MEC15C-NCDEWC   PIC S9(15)V   PACKED-DECIMAL.                     
   9       10 MEC15C-NSOCIETE PIC X(3).                                         
  12       10 MEC15C-NLIEU    PIC X(3).                                         
  15       10 MEC15C-NVENTE   PIC X(7).                                         
  22       10 MEC15C-NSEQNQ   PIC S9(5)V    PACKED-DECIMAL.                     
  25       10 MEC15C-STAT     PIC X(1).                                         
  26       10 MEC15C-NSEQERR  PIC X(0004).                                      
  30       10 MEC15C-NUM      PIC S9(13)V99 PACKED-DECIMAL OCCURS 5.            
  70       10 MEC15C-ALP      PIC X(20)                    OCCURS 5.            
 170       10 MEC15C-TEXTE    PIC X(80).                                        
OUTPUT******************************************************************        
 250       10 MEC15C-RC       PIC X.                                            
           88 MEC15C-OK           VALUE 'O'.                                    
           88 MEC15C-KO           VALUE 'K'.                                    
 251       10 MEC15C-MSG      PIC X(80).                                        
 331  * LENGTH=330                                                              
                                                                                
