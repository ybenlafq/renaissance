      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      *           MAQUETTE COMMAREA STANDARD AIDA COBOL2               *        
      ******************************************************************        
      *                                                                *        
      *  PROJET     : GESTION DES VENTES                               *        
      *  PROGRAMME  : MGVXX                                            *        
      *  TITRE      : COMMAREA DES MODULES DE LA VENTE                 *        
      *  LONGUEUR   : 150 C                                            *        
      *                                                                *        
      *  UTILISE PAR: MGV16 - DETERMINATION DU LIEU DEMANDEUR          *        
      *                                                                *        
      ******************************************************************        
       01  COMM-MGV00-APPLI.                                                    
      **** DONNEES COMMUNES ***************************************** 60        
           05 COMM-MGV00-CODRET         PIC X(01).                              
              88 COMM-MGV00-CODRET-OK                         VALUE ' '.        
              88 COMM-MGV00-CODRET-ERREUR                     VALUE '1'.        
           05 COMM-MGV00-LIBERR.                                                
              10 COMM-MGV00-NSEQERR        PIC X(04).                           
              10 COMM-MGV00-ERRFIL         PIC X(01).                           
              10 COMM-MGV00-LMESSAGE       PIC X(54).                           
      *                                                                 00960000
           05 COMM-MGV00-ZONES-ENTREE      PIC X(45).                           
      **** ZONES EN ENTREE DU MGV16 - SOCIETE VENTE / CINSEE ***********00730000
           05 COMM-MGV16-ENTREE  REDEFINES COMM-MGV00-ZONES-ENTREE.             
              10 COMM-MGV16-NSOCIETE       PIC X(03).                           
              10 COMM-MGV16-CINSEE         PIC X(05).                           
300709        10 COMM-MGV16-DDELIV         PIC X(08).                           
      *                                                                 00960000
           05 COMM-MGV00-ZONES-SORTIE      PIC X(45).                           
      **** ZONES EN SORTIE DU MGV16 - LIEU DEPART LIVRAISON ************00730000
           05 COMM-MGV16-SORTIE  REDEFINES COMM-MGV00-ZONES-SORTIE.             
              10 COMM-MGV16-LIEUGEST.                                           
                 15 COMM-MGV16-NSOCGEST    PIC X(03).                           
                 15 COMM-MGV16-NLIEUGEST   PIC X(03).                           
              10 COMM-MGV16-LDEPLIV.                                            
                 15 COMM-MGV16-NSOCDEPLIV  PIC X(03).                           
                 15 COMM-MGV16-NLIEUDEPLIV PIC X(03).                           
                                                                                
