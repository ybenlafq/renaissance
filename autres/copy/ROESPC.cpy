      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 27/07/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION OP ENT�TE SPECIFIQUE (MUTATION)       * 00031000
      * NOM FICHIER.: ROESPC                                          * 00032004
      *---------------------------------------------------------------* 00034000
      * CR   .......: 09/01/2012  17:46:00                            * 00035002
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR....: 056                                             * 00037102
      ***************************************************************** 00037300
      *                                                                 00037400
       01  ROESPC.                                                      00037500
      * TYPE ENREGISTREMENT                                             00037604
           05      ROESPC-TYP-ENREG       PIC  X(0006).                 00037700
      * CODE SOCI�T�                                                    00037804
           05      ROESPC-CSOCIETE        PIC  X(0005).                 00037901
      * NUM�RO D OP                                                     00038004
           05      ROESPC-NOP             PIC  X(0015).                 00038101
      * S�LECTION ARTICLE                                               00038204
           05      ROESPC-CSELART         PIC  X(0005).                 00038300
      * DATE DE DESTOCKAGE                                              00038404
           05      ROESPC-DDESTOCK        PIC  X(0008).                 00039000
      * DATE DE CHARGEMENT                                              00039104
           05      ROESPC-DCHARGT         PIC  X(0008).                 00040000
      * HEURE DE CHARGEMENT                                             00041004
           05      ROESPC-HCHARGT         PIC  X(0005).                 00050000
      * TYPE DE LIVRAISON / MORPHEUS                                    00051004
           05      ROESPC-TYPLIVR         PIC  X(0001).                 00060002
      * FLAG COMPTE CLIENT /MORPH�US                                    00061004
           05      ROESPC-WCPTCLIENT      PIC  X(0001).                 00070002
      * FLAG ENVOI MORPH�US                                             00071004
           05      ROESPC-WENVOI-MORPH    PIC  9(0001).                 00080002
      *                                                                 00090004
           05      ROESPC-FIN             PIC  X(0001).                 00170700
                                                                                
