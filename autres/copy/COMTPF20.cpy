      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************    00020000
      *  COMMAREA POUR LA TRANSACTION PF20                         *    00030000
      *      LONG = 4096                                           *    00030000
      **************************************************************    00040000
      *                                                                 00050000
       01  Z-COMMAREA.                                                  00260000
      * ZONES RESERVEES A AIDA ----------------------------------- 100  00280000
           02 FILLER-COM-AIDA                   PIC X(100).             00290000
      * ZONES RESERVEES EN PROVENANCE DE CICS -------------------- 020  00310000
           02 COMM-CICS-APPLID                  PIC X(8).               00320000
           02 COMM-CICS-NETNAM                  PIC X(8).               00330000
           02 COMM-CICS-TRANSA                  PIC X(4).               00340000
      * ZONES RESERVEES A LA DATE DE TRAITEMENT TRANSACTION ------ 100  00360000
           02 COMM-DATE-SIECLE                  PIC XX.                 00370000
           02 COMM-DATE-ANNEE                   PIC XX.                 00380000
           02 COMM-DATE-MOIS                    PIC XX.                 00390000
           02 COMM-DATE-JOUR                    PIC 99.                 00400000
      *    QUANTIEMES CALENDAIRE ET STANDARD                            00410000
           02 COMM-DATE-QNTA                    PIC 999.                00420000
           02 COMM-DATE-QNT0                    PIC 99999.              00430000
      *    ANNEE BISSEXTILE 1=OUI 0=NON                                 00440000
           02 COMM-DATE-BISX                    PIC 9.                  00450000
      *    JOUR SEMAINE 1=LUNDI ... 7=DIMANCHE                          00460000
           02 COMM-DATE-JSM                     PIC 9.                  00470000
      *    LIBELLES DU JOUR COURT - LONG                                00480000
           02 COMM-DATE-JSM-LC                  PIC XXX.                00490000
           02 COMM-DATE-JSM-LL                  PIC XXXXXXXX.           00500000
      *    LIBELLES DU MOIS COURT - LONG                                00510000
           02 COMM-DATE-MOIS-LC                 PIC XXX.                00520000
           02 COMM-DATE-MOIS-LL                 PIC XXXXXXXX.           00530000
      *    DIFFERENTES FORMES DE DATE                                   00540000
           02 COMM-DATE-SSAAMMJJ                PIC X(8).               00550000
           02 COMM-DATE-AAMMJJ                  PIC X(6).               00560000
           02 COMM-DATE-JJMMSSAA                PIC X(8).               00570000
           02 COMM-DATE-JJMMAA                  PIC X(6).               00580000
           02 COMM-DATE-JJ-MM-AA                PIC X(8).               00590000
           02 COMM-DATE-JJ-MM-SSAA              PIC X(10).              00600000
      *    TRAITEMENT DU NUMERO DE SEMAINE                              00610000
           02 COMM-DATE-WEEK.                                           00620000
              05 COMM-DATE-SEMSS                PIC 99.                 00630000
              05 COMM-DATE-SEMAA                PIC 99.                 00640000
              05 COMM-DATE-SEMNU                PIC 99.                 00650000
           02 COMM-DATE-FILLER                  PIC X(08).              00670000
      * ZONES RESERVEES APPLICATIVES COMMUNES----------------------001  00730000
           02 COMM-ERREUR-PAGE                  PIC X.                          
      * ZONES RESERVEES APPLICATIVES -MENU-------------------------668  00730000
           02 COMM-PF20-APPLI.                                          00740000
              03 COMM-PF20-AUTOR                PIC X(1).               00741002
              03 COMM-PF20-CACID                PIC X(8).                       
              03 COMM-PF20-ACID-SOC-LIEU.                                       
                 04 COMM-PF20-ACID-SOC          PIC X(3).                       
                 04 COMM-PF20-ACID-LIEU         PIC X(3).                       
              03 COMM-PF20-ENTREPOT-PFORME.                             00741002
                 04 COMM-PF20-TAB-ENTREPOT-PFORME OCCURS 10.            00741002
                    05 COMM-PF20-ENT-SOC        PIC X(3).                       
                    05 COMM-PF20-ENT-LIEU       PIC X(3).                       
                    05 COMM-PF20-PF-SOC         PIC X(3).                       
                    05 COMM-PF20-PF-LIEU        PIC X(3).                       
              03 COMM-PF20-DATA-SAISIES.                                        
                 04 COMM-PF20-SAISIE-ENTREPOT.                                  
                    05 COMM-PF20-SAISI-E-SOC    PIC X(3).                       
                    05 COMM-PF20-SAISI-E-LIEU   PIC X(3).                       
                 04 COMM-PF20-SAISIE-PLATEFORME.                                
                    05 COMM-PF20-SAISI-P-SOC    PIC X(3).                       
                    05 COMM-PF20-SAISI-P-LIEU   PIC X(3).                       
                 04 COMM-PF20-SAISI-SELART      PIC X(5).                       
                 04 COMM-PF20-SAISI-NMUT        PIC X(7).               00741002
                 04 COMM-PF20-SAISI-DDESTOCK    PIC X(8).               00741002
              03 COMM-PF20-CRIT-RECH            PIC X(1).               00741002
              03 COMM-PF20-FILLER               PIC X(500).             00741002
      * ZONES RESERVEES APPLICATIVES --TPF21---------------------558            
           02 COMM-PF21-APPLI.                                                  
              03 COMM-PF21-ENTETE.                                              
                 04 COMM-PF21-NPAGE             PIC 9(4).                       
                 04 COMM-PF21-NBPAGE            PIC 9(4).                       
              03 COMM-PF21-MUTATION-TROUVEE     PIC X(1).                       
              03 COMM-PF21-DETAIL.                                              
                 04 COMM-PF21-LIGNE-SELECTIONNEE.                               
                    05 COMM-PF21-SEL-PLATEFORME.                                
                       06 COMM-PF21-SEL-PF-SOC  PIC X(3).                       
                       06 COMM-PF21-SEL-PF-LIEU PIC X(3).                       
                    05 COMM-PF21-SEL-NMUTATION  PIC X(7).                       
                    05 COMM-PF21-SEL-SELART     PIC X(5).                       
                    05 COMM-PF21-SEL-DMUTATION  PIC X(8).                       
                    05 COMM-PF21-SEL-QVOL       PIC S9(3)V9(6) COMP-3.          
                    05 COMM-PF21-SEL-QTE        PIC S9(5)      COMP-3.          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             05 COMM-PF21-SEL-NLIGNE     PIC 9(3)       COMP.            
      *--                                                                       
                    05 COMM-PF21-SEL-NLIGNE     PIC 9(3) COMP-5.                
      *}                                                                        
              03 FILLER                         PIC X(500).                     
      * ZONES RESERVEES APPLICATIVES --TPF22---------------------- 575  00730000
           02 COMM-PF22-APPLI.                                          00800001
              03 COMM-PF22-ENTETE.                                              
                 04 COMM-PF22-NPAGE             PIC 9(4).                       
                 04 COMM-PF22-NBPAGE            PIC 9(4).                       
                 04 COMM-PF22-LENTREPOT         PIC X(20).                      
                 04 COMM-PF22-LPLATEFORME       PIC X(20).                      
              03 COMM-PF22-MUTATION-TROUVEE     PIC X(1).                       
              03 COMM-PF22-DETAIL.                                              
                 04 COMM-PF22-LIGNE-SELECTIONNEE.                               
                    05 COMM-PF22-SEL-NMUTATION  PIC X(7).                       
                    05 COMM-PF22-SEL-QVOLINIT   PIC S9(3)V9(6) COMP-3.          
                    05 COMM-PF22-SEL-QVOLPRIS   PIC S9(3)V9(6) COMP-3.          
                    05 COMM-PF22-SEL-QPINIT     PIC S9(5)      COMP-3.          
                    05 COMM-PF22-SEL-QPPRIS     PIC S9(5)      COMP-3.          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *             05 COMM-PF22-SEL-NLIGNE     PIC 9(3)       COMP.            
      *--                                                                       
                    05 COMM-PF22-SEL-NLIGNE     PIC 9(3) COMP-5.                
      *}                                                                        
                 04 COMM-PF22-TRANSF-OK         PIC X(1).                       
              03 FILLER                         PIC X(500).             00890001
      * ZONES LIBRES ---------------------------------------------      00730000
          02 COMM-PF20-FILLER                   PIC X(2070).            00970003
                                                                                
