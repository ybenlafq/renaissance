      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           EJECT                                                        00000100
      *==> DARTY ****************************************************** 00010001
      *          * COMMAREA TRAITEMENT BGD092 EN TP = MGD092          * 00020001
      *          * LE MODULE MGD092 EST APPELE PAR TGD30              * 00020002
      ****************************************************** COMMD092 * 00030001
      *                                                                 00040001
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-GD092-LONG-COMMAREA PIC S9(4) COMP VALUE +80.           00050001
      *--                                                                       
       01  COMM-GD092-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +80.                 
      *}                                                                        
      *                                                                 00060001
       01  Z-COMMAREA-MGD092.                                           00070001
      *                                                                 00080001
          02 MGD092-NSOCDEPOT            PIC XXX.                       00090001
          02 MGD092-NDEPOT               PIC XXX.                       00090002
          02 MGD092-DRAFALE              PIC X(8).                      00090003
          02 MGD092-CODE-RETOUR          PIC 99.                        00090006
          02 MGD092-MESSAGE-ERREUR       PIC X(64).                     00090007
          02 MGD092-MESSAGE REDEFINES MGD092-MESSAGE-ERREUR PIC X(64).  00090008
      *                                                                 00100001
                                                                                
