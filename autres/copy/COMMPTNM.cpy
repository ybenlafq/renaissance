      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * COMMAREA pour module de d�codage num�rique international MPTNM          
      ******************************************************************        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-PTNM-LONG-COMMAREA PIC S9(4)   COMP VALUE +200.                 
      *--                                                                       
       01  COMM-PTNM-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +200.                 
      *}                                                                        
       01  Z-COMMAREA-MPTNM.                                                    
      ** '01': XXX XXX XXX,XX                                                   
      ** '02': XXX,XXX,XXX.XX                                                   
      ** '03': XXX.XXX.XXX,XX                                                   
  1        02  PTNM-CODPIC         PIC XX VALUE '03'.                           
      ** Longueur de la zone                                                    
  3 2      02  PTNM-LONG           PIC S9(3) COMP-3.                            
      ** Zone d'�dition                                                         
      ** ==> En INPUT c'est la zone � d�coder                                   
      ** ==> Si cette zone est � blanc, on veut la mise en forme de             
      **     la zone num�rique PTNM-ZNUM                                        
      ** ==> En OUTPUT c'est la zone d�cod�e cadr�e � droite                    
      **     (sur le nombre de caract�res sp�cifi� par PTNM-LONG)               
  5        02  PTNM-ZEDIT          PIC X(20).                                   
      ** Zone d'Edition (= ZEDIT OUTPUT mais cadr�e � gauche)                   
 25        02  PTNM-ZEDIT-GAUCHE   PIC X(20).                                   
      ** Zones Num�riques                                                       
      **     Les zones PTNM-ZNUM-0 � 7 seront utilis�es pour les                
      **     d�cimales correspondantes                                          
 45 8      02  PTNM-ZNUM           PIC S9(15) COMP-3.                           
           02  PTNM-ZNUM-0 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(15) COMP-3.                           
           02  PTNM-ZNUM-1 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(14)V9 COMP-3.                         
           02  PTNM-ZNUM-2 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(13)V99 COMP-3.                        
           02  PTNM-ZNUM-3 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(12)V999 COMP-3.                       
           02  PTNM-ZNUM-4 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(11)V9(4) COMP-3.                      
           02  PTNM-ZNUM-5 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(10)V9(5) COMP-3.                      
           02  PTNM-ZNUM-6 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(9)V9(6) COMP-3.                       
           02  PTNM-ZNUM-7 REDEFINES PTNM-ZNUM                                  
                                   PIC S9(8)V9(7) COMP-3.                       
      ** Code Retour -> Si probl�me, voir NSEQERR                               
 53        02  PTNM-CDRET          PIC X.                                       
               88  PTNM-OK             VALUE '0'.                               
               88  PTNM-KO             VALUE '1'.                               
      ** Code MOVE -> Si Zone Zedit Valide                                      
 54        02  PTNM-CDMOVE         PIC X.                                       
               88  PTNM-MV             VALUE '0'.                               
               88  PTNM-NM             VALUE '1'.                               
      ** NSEQERR -> � utiliser directement par MLIBERRGEN                       
 55        02  PTNM-NSEQERR        PIC X(4).                                    
      ** Param�tres d'appel                                                     
      **     Table de v�rit� des variables:                                     
      **     - nombre de d�cimales (de 0 � 7)                                   
      **     - valeurs admises (toutes, <>0, <=0, <0, >=0, >0, =0)              
      **     - s�parateur des milliers (oui ou non)                             
      **     - signe si valeur positive (oui ou non)                            
 59        02  PTNM-PARAMETRES     PIC XXXX VALUE '    '.                       
      ** sans s�parateur, sans signe +, toutes valeurs admises                  
               88  PTNM-0                     VALUE '00 -'.                     
               88  PTNM-1                     VALUE '10 -'.                     
               88  PTNM-2                     VALUE '20 -'.                     
               88  PTNM-3                     VALUE '30 -'.                     
               88  PTNM-4                     VALUE '40 -'.                     
               88  PTNM-5                     VALUE '50 -'.                     
               88  PTNM-6                     VALUE '60 -'.                     
               88  PTNM-7                     VALUE '70 -'.                     
      ** sans s�parateur, sans signe +, contr�le des valeurs admises            
               88  PTNM-0-NON-NUL             VALUE '01 -'.                     
               88  PTNM-0-NEG-OU-NUL          VALUE '02 -'.                     
               88  PTNM-0-NEGATIF             VALUE '03 -'.                     
               88  PTNM-0-POS-OU-NUL          VALUE '04 -'.                     
               88  PTNM-0-POSITIF             VALUE '05 -'.                     
               88  PTNM-0-NUL                 VALUE '06 -'.                     
               88  PTNM-1-NON-NUL             VALUE '11 -'.                     
               88  PTNM-1-NEG-OU-NUL          VALUE '12 -'.                     
               88  PTNM-1-NEGATIF             VALUE '13 -'.                     
               88  PTNM-1-POS-OU-NUL          VALUE '14 -'.                     
               88  PTNM-1-POSITIF             VALUE '15 -'.                     
               88  PTNM-1-NUL                 VALUE '16 -'.                     
               88  PTNM-2-NON-NUL             VALUE '21 -'.                     
               88  PTNM-2-NEG-OU-NUL          VALUE '22 -'.                     
               88  PTNM-2-NEGATIF             VALUE '23 -'.                     
               88  PTNM-2-POS-OU-NUL          VALUE '24 -'.                     
               88  PTNM-2-POSITIF             VALUE '25 -'.                     
               88  PTNM-2-NUL                 VALUE '26 -'.                     
               88  PTNM-3-NON-NUL             VALUE '31 -'.                     
               88  PTNM-3-NEG-OU-NUL          VALUE '32 -'.                     
               88  PTNM-3-NEGATIF             VALUE '33 -'.                     
               88  PTNM-3-POS-OU-NUL          VALUE '34 -'.                     
               88  PTNM-3-POSITIF             VALUE '35 -'.                     
               88  PTNM-3-NUL                 VALUE '36 -'.                     
               88  PTNM-4-NON-NUL             VALUE '41 -'.                     
               88  PTNM-4-NEG-OU-NUL          VALUE '42 -'.                     
               88  PTNM-4-NEGATIF             VALUE '43 -'.                     
               88  PTNM-4-POS-OU-NUL          VALUE '44 -'.                     
               88  PTNM-4-POSITIF             VALUE '45 -'.                     
               88  PTNM-4-NUL                 VALUE '46 -'.                     
               88  PTNM-5-NON-NUL             VALUE '51 -'.                     
               88  PTNM-5-NEG-OU-NUL          VALUE '52 -'.                     
               88  PTNM-5-NEGATIF             VALUE '53 -'.                     
               88  PTNM-5-POS-OU-NUL          VALUE '54 -'.                     
               88  PTNM-5-POSITIF             VALUE '55 -'.                     
               88  PTNM-5-NUL                 VALUE '56 -'.                     
               88  PTNM-6-NON-NUL             VALUE '61 -'.                     
               88  PTNM-6-NEG-OU-NUL          VALUE '62 -'.                     
               88  PTNM-6-NEGATIF             VALUE '63 -'.                     
               88  PTNM-6-POS-OU-NUL          VALUE '64 -'.                     
               88  PTNM-6-POSITIF             VALUE '65 -'.                     
               88  PTNM-6-NUL                 VALUE '66 -'.                     
               88  PTNM-7-NON-NUL             VALUE '71 -'.                     
               88  PTNM-7-NEG-OU-NUL          VALUE '72 -'.                     
               88  PTNM-7-NEGATIF             VALUE '73 -'.                     
               88  PTNM-7-POS-OU-NUL          VALUE '74 -'.                     
               88  PTNM-7-POSITIF             VALUE '75 -'.                     
               88  PTNM-7-NUL                 VALUE '76 -'.                     
      ** avec s�parateur, sans signe +, toutes valeurs admises                  
               88  PTNM-0-MIL                 VALUE '00M-'.                     
               88  PTNM-1-MIL                 VALUE '10M-'.                     
               88  PTNM-2-MIL                 VALUE '20M-'.                     
               88  PTNM-3-MIL                 VALUE '30M-'.                     
               88  PTNM-4-MIL                 VALUE '40M-'.                     
               88  PTNM-5-MIL                 VALUE '50M-'.                     
               88  PTNM-6-MIL                 VALUE '60M-'.                     
               88  PTNM-7-MIL                 VALUE '70M-'.                     
      ** avec s�parateur, sans signe +, contr�le des valeurs admises            
               88  PTNM-0-MIL-NON-NUL         VALUE '01M-'.                     
               88  PTNM-0-MIL-NEG-OU-NUL      VALUE '02M-'.                     
               88  PTNM-0-MIL-NEGATIF         VALUE '03M-'.                     
               88  PTNM-0-MIL-POS-OU-NUL      VALUE '04M-'.                     
               88  PTNM-0-MIL-POSITIF         VALUE '05M-'.                     
               88  PTNM-0-MIL-NUL             VALUE '06M-'.                     
               88  PTNM-1-MIL-NON-NUL         VALUE '11M-'.                     
               88  PTNM-1-MIL-NEG-OU-NUL      VALUE '12M-'.                     
               88  PTNM-1-MIL-NEGATIF         VALUE '13M-'.                     
               88  PTNM-1-MIL-POS-OU-NUL      VALUE '14M-'.                     
               88  PTNM-1-MIL-POSITIF         VALUE '15M-'.                     
               88  PTNM-1-MIL-NUL             VALUE '16M-'.                     
               88  PTNM-2-MIL-NON-NUL         VALUE '21M-'.                     
               88  PTNM-2-MIL-NEG-OU-NUL      VALUE '22M-'.                     
               88  PTNM-2-MIL-NEGATIF         VALUE '23M-'.                     
               88  PTNM-2-MIL-POS-OU-NUL      VALUE '24M-'.                     
               88  PTNM-2-MIL-POSITIF         VALUE '25M-'.                     
               88  PTNM-2-MIL-NUL             VALUE '26M-'.                     
               88  PTNM-3-MIL-NON-NUL         VALUE '31M-'.                     
               88  PTNM-3-MIL-NEG-OU-NUL      VALUE '32M-'.                     
               88  PTNM-3-MIL-NEGATIF         VALUE '33M-'.                     
               88  PTNM-3-MIL-POS-OU-NUL      VALUE '34M-'.                     
               88  PTNM-3-MIL-POSITIF         VALUE '35M-'.                     
               88  PTNM-3-MIL-NUL             VALUE '36M-'.                     
               88  PTNM-4-MIL-NON-NUL         VALUE '41M-'.                     
               88  PTNM-4-MIL-NEG-OU-NUL      VALUE '42M-'.                     
               88  PTNM-4-MIL-NEGATIF         VALUE '43M-'.                     
               88  PTNM-4-MIL-POS-OU-NUL      VALUE '44M-'.                     
               88  PTNM-4-MIL-POSITIF         VALUE '45M-'.                     
               88  PTNM-4-MIL-NUL             VALUE '46M-'.                     
               88  PTNM-5-MIL-NON-NUL         VALUE '51M-'.                     
               88  PTNM-5-MIL-NEG-OU-NUL      VALUE '52M-'.                     
               88  PTNM-5-MIL-NEGATIF         VALUE '53M-'.                     
               88  PTNM-5-MIL-POS-OU-NUL      VALUE '54M-'.                     
               88  PTNM-5-MIL-POSITIF         VALUE '55M-'.                     
               88  PTNM-5-MIL-NUL             VALUE '56M-'.                     
               88  PTNM-6-MIL-NON-NUL         VALUE '61M-'.                     
               88  PTNM-6-MIL-NEG-OU-NUL      VALUE '62M-'.                     
               88  PTNM-6-MIL-NEGATIF         VALUE '63M-'.                     
               88  PTNM-6-MIL-POS-OU-NUL      VALUE '64M-'.                     
               88  PTNM-6-MIL-POSITIF         VALUE '65M-'.                     
               88  PTNM-6-MIL-NUL             VALUE '66M-'.                     
               88  PTNM-7-MIL-NON-NUL         VALUE '71M-'.                     
               88  PTNM-7-MIL-NEG-OU-NUL      VALUE '72M-'.                     
               88  PTNM-7-MIL-NEGATIF         VALUE '73M-'.                     
               88  PTNM-7-MIL-POS-OU-NUL      VALUE '74M-'.                     
               88  PTNM-7-MIL-POSITIF         VALUE '75M-'.                     
               88  PTNM-7-MIL-NUL             VALUE '76M-'.                     
      ** sans s�parateur, avec signe +, toutes valeurs admises                  
               88  PTNM-0-SPOS                VALUE '00 +'.                     
               88  PTNM-1-SPOS                VALUE '10 +'.                     
               88  PTNM-2-SPOS                VALUE '20 +'.                     
               88  PTNM-3-SPOS                VALUE '30 +'.                     
               88  PTNM-4-SPOS                VALUE '40 +'.                     
               88  PTNM-5-SPOS                VALUE '50 +'.                     
               88  PTNM-6-SPOS                VALUE '60 +'.                     
               88  PTNM-7-SPOS                VALUE '70 +'.                     
      ** sans s�parateur, avec signe +, contr�le des valeurs admises            
               88  PTNM-0-NON-NUL-SPOS        VALUE '01 +'.                     
               88  PTNM-0-NEG-OU-NUL-SPOS     VALUE '02 +'.                     
               88  PTNM-0-NEGATIF-SPOS        VALUE '03 +'.                     
               88  PTNM-0-POS-OU-NUL-SPOS     VALUE '04 +'.                     
               88  PTNM-0-POSITIF-SPOS        VALUE '05 +'.                     
               88  PTNM-0-NUL-SPOS            VALUE '06 +'.                     
               88  PTNM-1-NON-NUL-SPOS        VALUE '11 +'.                     
               88  PTNM-1-NEG-OU-NUL-SPOS     VALUE '12 +'.                     
               88  PTNM-1-NEGATIF-SPOS        VALUE '13 +'.                     
               88  PTNM-1-POS-OU-NUL-SPOS     VALUE '14 +'.                     
               88  PTNM-1-POSITIF-SPOS        VALUE '15 +'.                     
               88  PTNM-1-NUL-SPOS            VALUE '16 +'.                     
               88  PTNM-2-NON-NUL-SPOS        VALUE '21 +'.                     
               88  PTNM-2-NEG-OU-NUL-SPOS     VALUE '22 +'.                     
               88  PTNM-2-NEGATIF-SPOS        VALUE '23 +'.                     
               88  PTNM-2-POS-OU-NUL-SPOS     VALUE '24 +'.                     
               88  PTNM-2-POSITIF-SPOS        VALUE '25 +'.                     
               88  PTNM-2-NUL-SPOS            VALUE '26 +'.                     
               88  PTNM-3-NON-NUL-SPOS        VALUE '31 +'.                     
               88  PTNM-3-NEG-OU-NUL-SPOS     VALUE '32 +'.                     
               88  PTNM-3-NEGATIF-SPOS        VALUE '33 +'.                     
               88  PTNM-3-POS-OU-NUL-SPOS     VALUE '34 +'.                     
               88  PTNM-3-POSITIF-SPOS        VALUE '35 +'.                     
               88  PTNM-3-NUL-SPOS            VALUE '36 +'.                     
               88  PTNM-4-NON-NUL-SPOS        VALUE '41 +'.                     
               88  PTNM-4-NEG-OU-NUL-SPOS     VALUE '42 +'.                     
               88  PTNM-4-NEGATIF-SPOS        VALUE '43 +'.                     
               88  PTNM-4-POS-OU-NUL-SPOS     VALUE '44 +'.                     
               88  PTNM-4-POSITIF-SPOS        VALUE '45 +'.                     
               88  PTNM-4-NUL-SPOS            VALUE '46 +'.                     
               88  PTNM-5-NON-NUL-SPOS        VALUE '51 +'.                     
               88  PTNM-5-NEG-OU-NUL-SPOS     VALUE '52 +'.                     
               88  PTNM-5-NEGATIF-SPOS        VALUE '53 +'.                     
               88  PTNM-5-POS-OU-NUL-SPOS     VALUE '54 +'.                     
               88  PTNM-5-POSITIF-SPOS        VALUE '55 +'.                     
               88  PTNM-5-NUL-SPOS            VALUE '56 +'.                     
               88  PTNM-6-NON-NUL-SPOS        VALUE '61 +'.                     
               88  PTNM-6-NEG-OU-NUL-SPOS     VALUE '62 +'.                     
               88  PTNM-6-NEGATIF-SPOS        VALUE '63 +'.                     
               88  PTNM-6-POS-OU-NUL-SPOS     VALUE '64 +'.                     
               88  PTNM-6-POSITIF-SPOS        VALUE '65 +'.                     
               88  PTNM-6-NUL-SPOS            VALUE '66 +'.                     
               88  PTNM-7-NON-NUL-SPOS        VALUE '71 +'.                     
               88  PTNM-7-NEG-OU-NUL-SPOS     VALUE '72 +'.                     
               88  PTNM-7-NEGATIF-SPOS        VALUE '73 +'.                     
               88  PTNM-7-POS-OU-NUL-SPOS     VALUE '74 +'.                     
               88  PTNM-7-POSITIF-SPOS        VALUE '75 +'.                     
               88  PTNM-7-NUL-SPOS            VALUE '76 +'.                     
      ** avec s�parateur, avec signe +, toutes valeurs admises                  
               88  PTNM-0-MIL-SPOS            VALUE '00M+'.                     
               88  PTNM-1-MIL-SPOS            VALUE '10M+'.                     
               88  PTNM-2-MIL-SPOS            VALUE '20M+'.                     
               88  PTNM-3-MIL-SPOS            VALUE '30M+'.                     
               88  PTNM-4-MIL-SPOS            VALUE '40M+'.                     
               88  PTNM-5-MIL-SPOS            VALUE '50M+'.                     
               88  PTNM-6-MIL-SPOS            VALUE '60M+'.                     
               88  PTNM-7-MIL-SPOS            VALUE '70M+'.                     
      ** avec s�parateur, avec signe +, contr�le des valeurs admises            
               88  PTNM-0-MIL-NON-NUL-SPOS    VALUE '01M+'.                     
               88  PTNM-0-MIL-NEG-OU-NUL-SPOS VALUE '02M+'.                     
               88  PTNM-0-MIL-NEGATIF-SPOS    VALUE '03M+'.                     
               88  PTNM-0-MIL-POS-OU-NUL-SPOS VALUE '04M+'.                     
               88  PTNM-0-MIL-POSITIF-SPOS    VALUE '05M+'.                     
               88  PTNM-0-MIL-NUL-SPOS        VALUE '06M+'.                     
               88  PTNM-1-MIL-NON-NUL-SPOS    VALUE '11M+'.                     
               88  PTNM-1-MIL-NEG-OU-NUL-SPOS VALUE '12M+'.                     
               88  PTNM-1-MIL-NEGATIF-SPOS    VALUE '13M+'.                     
               88  PTNM-1-MIL-POS-OU-NUL-SPOS VALUE '14M+'.                     
               88  PTNM-1-MIL-POSITIF-SPOS    VALUE '15M+'.                     
               88  PTNM-1-MIL-NUL-SPOS        VALUE '16M+'.                     
               88  PTNM-2-MIL-NON-NUL-SPOS    VALUE '21M+'.                     
               88  PTNM-2-MIL-NEG-OU-NUL-SPOS VALUE '22M+'.                     
               88  PTNM-2-MIL-NEGATIF-SPOS    VALUE '23M+'.                     
               88  PTNM-2-MIL-POS-OU-NUL-SPOS VALUE '24M+'.                     
               88  PTNM-2-MIL-POSITIF-SPOS    VALUE '25M+'.                     
               88  PTNM-2-MIL-NUL-SPOS        VALUE '26M+'.                     
               88  PTNM-3-MIL-NON-NUL-SPOS    VALUE '31M+'.                     
               88  PTNM-3-MIL-NEG-OU-NUL-SPOS VALUE '32M+'.                     
               88  PTNM-3-MIL-NEGATIF-SPOS    VALUE '33M+'.                     
               88  PTNM-3-MIL-POS-OU-NUL-SPOS VALUE '34M+'.                     
               88  PTNM-3-MIL-POSITIF-SPOS    VALUE '35M+'.                     
               88  PTNM-3-MIL-NUL-SPOS        VALUE '36M+'.                     
               88  PTNM-4-MIL-NON-NUL-SPOS    VALUE '41M+'.                     
               88  PTNM-4-MIL-NEG-OU-NUL-SPOS VALUE '42M+'.                     
               88  PTNM-4-MIL-NEGATIF-SPOS    VALUE '43M+'.                     
               88  PTNM-4-MIL-POS-OU-NUL-SPOS VALUE '44M+'.                     
               88  PTNM-4-MIL-POSITIF-SPOS    VALUE '45M+'.                     
               88  PTNM-4-MIL-NUL-SPOS        VALUE '46M+'.                     
               88  PTNM-5-MIL-NON-NUL-SPOS    VALUE '51M+'.                     
               88  PTNM-5-MIL-NEG-OU-NUL-SPOS VALUE '52M+'.                     
               88  PTNM-5-MIL-NEGATIF-SPOS    VALUE '53M+'.                     
               88  PTNM-5-MIL-POS-OU-NUL-SPOS VALUE '54M+'.                     
               88  PTNM-5-MIL-POSITIF-SPOS    VALUE '55M+'.                     
               88  PTNM-5-MIL-NUL-SPOS        VALUE '56M+'.                     
               88  PTNM-6-MIL-NON-NUL-SPOS    VALUE '61M+'.                     
               88  PTNM-6-MIL-NEG-OU-NUL-SPOS VALUE '62M+'.                     
               88  PTNM-6-MIL-NEGATIF-SPOS    VALUE '63M+'.                     
               88  PTNM-6-MIL-POS-OU-NUL-SPOS VALUE '64M+'.                     
               88  PTNM-6-MIL-POSITIF-SPOS    VALUE '65M+'.                     
               88  PTNM-6-MIL-NUL-SPOS        VALUE '66M+'.                     
               88  PTNM-7-MIL-NON-NUL-SPOS    VALUE '71M+'.                     
               88  PTNM-7-MIL-NEG-OU-NUL-SPOS VALUE '72M+'.                     
               88  PTNM-7-MIL-NEGATIF-SPOS    VALUE '73M+'.                     
               88  PTNM-7-MIL-POS-OU-NUL-SPOS VALUE '74M+'.                     
               88  PTNM-7-MIL-POSITIF-SPOS    VALUE '75M+'.                     
               88  PTNM-7-MIL-NUL-SPOS        VALUE '76M+'.                     
 63 *** (longueur=62)                                                           
                                                                                
