      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *                             DABORD LA COPY DE L"ENTETE MESSMQ  *      13
      *   MESTSV32 COPY                                                *      13
      * - DEMANDE ENVOI VENTE � SIEBEL OU SAV                          *      13
      *   500 LONGEUR                                                  *      13
      * COPY POUR LES PROGRAMMES TSV90 ET MSV32                        *      13
      ******************************************************************        
      * LONGUEUR DE 34  OCTETS                                                  
              05 MESTSV32-RETOUR.                                               
                 10 MESTSV32-CODE-RETOUR.                                       
                    15  MESTSV32-CODRET  PIC X(1).                              
                         88  MESTSV32-CODRET-OK        VALUE ' '.               
                         88  MESTSV32-CODRET-ERREUR    VALUE '1'.               
                    15  MESTSV32-LIBERR  PIC X(60).                             
              05 MESTSV32-DATA.                                                 
      * NUMERO DE VENTE                                                         
                 10 MESTSV32-DONNES-VENTE.                                      
                   15 MESTSV32-NSOCIETE       PIC X(03).                        
                   15 MESTSV32-NLIEU          PIC X(03).                        
                   15 MESTSV32-NVENTE         PIC X(07).                        
      * TYPE DE LIEU RETOUR SIEBEL (XML) NASC (NORMAL)                          
                   15 MESTSV32-FLAG             PIC X(01).                      
                      88 MESTSV32-XML VALUE 'X'.                                
                      88 MESTSV32-NOR VALUE 'N'.                                
                      88 MESTSV32-ALL VALUE 'A'.                                
      * TYPE DE REPONSE                                                         
                 10 MESTSV32-TYPE             PIC X(03).                        
                 10 MESTSV32-FILLER           PIC X(20).                        
      *                                                                 00550009
      ***************************************************************** 00740000
                                                                                
