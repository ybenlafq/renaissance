      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *   COPY ZONE DE TRAVAIL INTERNE TGA17                       *            
      *                      EN CAS DE MODIF :ATTENTION AU FILLER  *            
      *    LG = 100      PWD LIBRARIAN =GA17                       *            
      **************************************************************            
      *                                                                         
       01  W-TGA17.                                                             
      *                                                                         
          02 TS-CLEF.                                                           
             03 ID-TRANS                 PIC X(4).                              
             03 ID-TERM                  PIC X(4).                              
      * MAXI DE CODES DESCRIPTIFS  TROUVES POUR LA FAMILLE.                     
          02 MAX-CDESC                  PIC S9(4) VALUE +0.                     
          02 FILLER   PIC X(8) VALUE '***IPAGE'.                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 IPAGE                      PIC S9(4) COMP VALUE +0.                
      *--                                                                       
          02 IPAGE                      PIC S9(4) COMP-5 VALUE +0.              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *   02 ICDES                      PIC S9(4) COMP VALUE +0.                
      *--                                                                       
          02 ICDES                      PIC S9(4) COMP-5 VALUE +0.              
      *}                                                                        
      *                                                                         
          02 FILLER                     PIC X(88).                              
      *****************************************************************         
                                                                                
