      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  COMM-RM15-APPLI.                                                     
           05  COMM-RM15-CGROUP                PIC  X(5).                       
           05  COMM-RM15-CFAM                  PIC  X(5).                       
           05  COMM-RM15-LAGREGATED            PIC  X(20).                      
           05  COMM-RM15-NCODIC                PIC  X(7).                       
           05  COMM-RM15-QPV                   PIC  S9(7) COMP-3.               
           05  COMM-RM15-NATURE                PIC  X(1).                       
           05  COMM-RM15-DATES OCCURS 8.                                        
               10  COMM-RM15-SEMAINE           PIC  X(6).                       
           05  COMM-RM15-DATEJOUR              PIC  X(8).                       
           05  COMM-RM15-CODRET                PIC  S9(4).                      
           05  COMM-RM15-MESS                  PIC  X(78).                      
           05  COMM-RM15-INDTS                 PIC  9(4)  COMP-3.               
           05  COMM-RM15-QSEUILSEGMENT         PIC  9(3)  COMP-3.               
           05  COMM-RM15-QCSTLISSAGE           PIC  9V99  COMP-3.               
           05  COMM-RM15-QNBSEM                PIC  9(3)  COMP-3.               
           05  COMM-RM15-GROUPE                PIC  X.                          
           05  COMM-RM15-TABLEAU.                                               
               10  COMM-RM15-TABTS OCCURS 500.                                  
                   15  COMM-RM15-TAB-NCODIC    PIC  X(7).                       
           05  COMM-RM15-NSOCIETE              PIC  X(3).                       
SR         05  COMM-RM15-NSOC                  PIC  X(3).                       
SR         05  COMM-RM15-NSOCCICS              PIC  X(3).                       
SR         05  COMM-RM15-TAB-SOC.                                               
SR             10  COMM-RM15-OCC-SOC           PIC X(3) OCCURS 5.               
SR             10  COMM-RM15-OCC-DEP           PIC X(3) OCCURS 5.               
SR         05  COMM-RM15-NBSOC                 PIC S9(3) COMP-3.                
           05  COMM-RM15-S-1                   PIC X(06).                       
           05  COMM-RM15-S-2                   PIC X(06).                       
           05  COMM-RM15-S-3                   PIC X(06).                       
           05  COMM-RM15-S-4                   PIC X(06).                       
           05  COMM-RM15-S-5                   PIC X(06).                       
           05  COMM-RM15-S-6                   PIC X(06).                       
           05  COMM-RM15-S-7                   PIC X(06).                       
           05  COMM-RM15-S-8                   PIC X(06).                       
           05  FILLER                          PIC  X(317).                     
                                                                                
