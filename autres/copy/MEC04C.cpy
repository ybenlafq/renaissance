      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC04                             *         
      *****************************************************************         
      *--> LONGUEUR MAX ADMIS POUR UNE COMMAREA 32 767.                         
       01  MEC04C-COMMAREA.                                                     
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           05  MEC04C-DATA-ENTREE.                                              
               10  MEC04C-TABLE             PIC  X(6).                          
               10  MEC04C-ORDRE-DB2         PIC  X(6).                          
               10  MEC04C-SOCIETE           PIC  X(3).                          
               10  MEC04C-LIEU              PIC  X(3).                          
               10  MEC04C-ZONE-PRIX         PIC  X(2).                          
               10  MEC04C-CODIC             PIC  X(7).                          
               10  MEC04C-DATE-EFFET        PIC  X(8).                          
               10  MEC04C-DATE-JOUR         PIC  X(8).                          
               10  MEC04C-PRIX              PIC  S9(7)V9(2) COMP-3.             
               10  FILLER                   PIC  X(100).                        
      *--> DONNEES RESULTANTES.                                                 
           05  MEC04C-DATA-SORTIE.                                              
      * NBR MESSSAGE ENVOY� VIA MQS PAR MEC004                                  
               10  MEC04C-NBR-MESSAGE-ENVOYE       PIC  9(09).                  
               10  MEC04C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC04C-CDRET-OK                  VALUE '0'.              
                   88  MEC04C-CDRET-ERR-NON-BLQ         VALUE '1'.              
                   88  MEC04C-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.              
                   88  MEC04C-CDRET-ERR-BLQ             VALUE '8'.              
                   88  MEC04C-CDRET-ERR-DB2-BLQ         VALUE '9'.              
               10  MEC04C-CODE-DB2                 PIC ++++9.                   
               10  MEC04C-CODE-DB2-DISPLAY         PIC  X(05).                  
      * LE PRIX DOIT ETRE ENVOY� 'O' OU 'N'                                     
               10  MEC04C-MAJ-PRIX                 PIC  X(1).                   
      * LE PRIX A �T� ENVOY�     'O' OU 'N'                                     
               10  MEC04C-MAJ-ENV                  PIC  X(1).                   
               10  MEC04C-MESSAGE                  PIC  X(500).                 
           05  FILLER                              PIC  X(100).                 
      ********* FIN DE LA ZONE DE COMMUNICATION DU MEC007 *************         
                                                                                
