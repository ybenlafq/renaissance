      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:03 >
      * ----------------------------------------------------------- *
      * - envoi quotas lors de la reception d'une commande dcom   - *
      * -                                                         - *
      * ----------------------------------------------------------- *
      * 16/08/2012 : EVOLUTION - GESTION DES MUTATIONS J0 POUR DCOM *
      * AUTEUR     : DE02001                                        *
      ***************************************************************
       01  W-CC-TIMESTP        PIC S9(15) PACKED-DECIMAL.
       01  W-CC-E-TIMESTP-DEC.
           05  W-CC-E-TIMESTP-2    PIC  9(02) PACKED-DECIMAL VALUE 0.
           05  W-CC-E-TIMESTP-9    PIC  9(13) PACKED-DECIMAL VALUE 0.
       01  W-QPVUNIT-9     PIC 999.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  W-QPVUNIT-BIN   PIC S9(08) BINARY.
      *--
       01  W-QPVUNIT-BIN   PIC S9(08) COMP-5.
      *}
       01  W-NBJOURS-9     PIC 999    VALUE 10.
       01  GQ03-DJOUR-PLUS-N PIC X(8).
       01  B-GQ03-DJOUR      PIC X(08).
       01  B-GQ03-CZONLIV    PIC X(05).
       01  B-GQ03-CPLAGE     PIC X(02).
       01  B-GQ03-QQUOTA     PIC S9(5) PACKED-DECIMAL.
           COPY COMMDATC.
       01  W-EIBTIME        PIC 9(7).
       01  FILLER REDEFINES W-EIBTIME.
           02  FILLER      PIC X.
           02  W-HEURE     PIC XX.
           02  W-MINUTE    PIC XX.
           02  W-SECONDE   PIC XX.
       01  MSG-SQL.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    02 MSG-SQL-LENGTH PIC S9(4) COMP VALUE +240.
      *--
           02 MSG-SQL-LENGTH PIC S9(4) COMP-5 VALUE +240.
      *}
           02 MSG-SQL-MSG    PIC X(80) OCCURS 3.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  MSG-SQL-LRECL     PIC S9(8) COMP VALUE +80.
      *--
       01  MSG-SQL-LRECL     PIC S9(8) COMP-5 VALUE +80.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-SQL             PIC S9(4) BINARY.
      *--
       01  I-SQL             PIC S9(4) COMP-5.
      *}
       01  DSNTIAR           PIC X(8)  VALUE 'DSNTIAR'.
       01  W-MESSAGE-S.
           02  w-nom-prog          PIC X(6).
           02  W-MESSAGE-H         PIC X(2).
           02  FILLER              PIC X(1) VALUE ':'.
           02  W-MESSAGE-M         PIC X(2).
           02  FILLER              PIC X(1) VALUE ' '.
           02  W-MESSAGE           PIC X(68).
       01  CPT-RTGQ03  PIC S9(05) PACKED-DECIMAL VALUE ZERO.
       01  FILLER PIC X.
           88  DEBUT-RTGQ03    VALUE 'D'.
           88  FIN-RTGQ03      VALUE 'F'.
       01  FILLER PIC X.
       88  DEBUT-RTEC06    VALUE 'D'.
       88  FIN-RTEC06      VALUE 'F'.
       01  FILLER PIC X.
       88  DEBUT-XCTRL     VALUE 'D'.
       88  FIN-XCTRL       VALUE 'F'.
       01  FILLER PIC X.
       88  DEBUT-GA01      VALUE 'D'.
       88  FIN-GA01        VALUE 'F'.
       01 STATUT-TRT          PIC X VALUE '1'.
           88 TRT-KO                VALUE '1'.
           88 TRT-OK                VALUE '0'.
       01 W-INFO.
          05 W-CPLAGE      PIC X(02).
          05 W-CMODDEL     PIC X(03).
          05 W-CSERVICE    PIC X(05).
          05 W-NSOCLIV     PIC X(03).
          05 W-NLIEULIV    PIC X(03).
       01 WINFO            PIC 9(01) VALUE 0.
          88 TROUVE-INFO             VALUE 1.
          88 NON-TROUVE-INFO         VALUE 0.
       01 STATUT-DISPLAY      PIC X VALUE '1'.
           88 AFFICHER-NON          VALUE '1'.
           88 AFFICHER-OUI          VALUE '0'.
       01  W1-RTEC06.
           05    W1-MAX     PIC 9(5) VALUE  1000.
           05    W1-ZONE.
            10   W1-NBP    PIC 9(5) VALUE    0.
            10   W1-POSTE     OCCURS 1000 INDEXED IW1.
             15  W1-NSOC      PIC X(3).
             15  W1-CSERVICE  PIC X(5).
             15  W1-CMODDEL   PIC X(3).
             15  W1-CELEMEN   PIC X(5).
             15  W1-NSOCLIV   PIC X(3).
             15  W1-NLIEULIV  PIC X(3).
       01  W2-XCTRL.
           05 W2-MAX     PIC 9(5) VALUE  1000.
           05 W2-ZONE.
            10 W2-NBP    PIC 9(5) VALUE    0.
            10 W2-POSTE     OCCURS 1000 INDEXED IW2.
               15 W2-CMODDEL    PIC X(03).
               15 W2-CPLAGE     PIC X(02).
               15 W2-CSERVICE   PIC X(05).
       01  W3-LPLGE.
           05 W3-MAX     PIC 9(5) VALUE  20.
           05 W3-ZONE.
            10 W3-NBP    PIC 9(5) VALUE    0.
            10 W3-POSTE     OCCURS 20 INDEXED IW3.
               15 W3-CPLAGE     PIC X(03).
               15 W3-HDEBUT     PIC X(04).
               15 W3-HFIN       PIC X(04).
       01  W-PARAMETRES-NCGFC.
           05  W-PARAMETRES-NSSLIEU  PIC X(03) VALUE SPACES.
           05  W-PARAMETRES-CRSL     PIC X(05) VALUE SPACES.
           05  W-PARAMETRES-STOCK    PIC 9(03) VALUE 0.
           05  W-PARAMETRES-NBRJRS   PIC 9(02) VALUE 0.
           05  W-PARAMETRES-FILLER   PIC X(03) VALUE SPACES.
           05  W-PARAMETRES-DISPLAY  PIC X(01) VALUE SPACES.
       01  W-CODE-DB2               PIC ++++9.
       01  W-CODE-DB2-DISPLAY       PIC  X(05)   VALUE SPACES.
       01 I                      PIC S9(05) COMP-3.
      * -> FILTRAGE DES SELECTIONS ARTICLES PAR MODE DE D�LIVRANCE
       01 WS-SEL-ARTICLES.
          05 WS-NB-SEL           PIC 9(03) VALUE 0.
          05 WS-NB-MAX           PIC 9(02) VALUE 50.
          05 WS-TABLE OCCURS 50.
             15 WS-CMODDEL       PIC X(03).
             15 WS-TYPE-MUT      PIC X(03).
      ******************************************************************
      * D�passements de quota
      ******************************************************************
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-EC6     PIC S9(4) BINARY.
      *--
       01  I-EC6     PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-EC6-U   PIC S9(4) BINARY VALUE 0.
      *--
       01  I-EC6-U   PIC S9(4) COMP-5 VALUE 0.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  I-EC6-MAX PIC S9(4) BINARY VALUE 500.
      *--
       01  I-EC6-MAX PIC S9(4) COMP-5 VALUE 500.
      *}
       01  W-EC6-TABLE.
           02  W-EC6-FILLER   OCCURS 500.
               07  W-EC6-NSOC      PIC X(3).
               07  W-EC6-NLIEU     PIC X(3).
               07  W-EC6-CZONLIV   PIC X(5).
               07  W-EC6-CPLAGE    PIC X(2).
               07  W-EC6-NJOUR     PIC X(1).
               07  W-EC6-QQUOTA-N  PIC 9(3).
      ******************************************************************
      * DSect GQ03 pour envoi XML MQ...
      ******************************************************************
       01  W-XML-RTGQ03.
           02  W-XML-RTGQ03-C-SOC  PIC XXX.
           02  W-XML-RTGQ03-C-LIEU PIC XXX.
           02  W-XML-RTGQ03-C-ZONE PIC X(5).
           02  W-XML-RTGQ03-DT     PIC X(8).
           02  W-XML-RTGQ03-C-CREN PIC XX.
           02  W-XML-RTGQ03-QUOTA  PIC 9(5).
           02  W-XML-HDEBUT        pic x(04).
           02  W-XML-HFIN          pic x(04).
           02  W-XML-CMODDEL       pic x(03).
           02  W-XML-CSERVICE      pic x(05).
           02  W-XML-TP-MUTATION   pic x(05).
           02  W-XML-TIMESTP       PIC 9(13)  VALUE 0.
           COPY MEC0MC.
      *    COPY MEC00C.
           COPY MEC24C.
       LINKAGE SECTION.
       01  DFHCOMMAREA.
           05  FILLER PIC X OCCURS 1 DEPENDING ON EIBCALEN.
       PROCEDURE DIVISION.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL) END-EXEC.
      *--
           EXEC CICS HANDLE ABEND LABEL (ABEND-LABEL)
           END-EXEC.
      *}
      *{ normalize-exec-xx 1.5
      *    EXEC CICS
      *         ASSIGN PROGRAM (W-NOM-PROG)
      *    END-EXEC.
      *--
           EXEC CICS ASSIGN PROGRAM (W-NOM-PROG)
           END-EXEC.
      *}
           PERFORM SELECT-NCGFC.
           IF TRT-OK
               PERFORM MODULE-ENTREE
               PERFORM MODULE-TRAITEMENT
           end-if.
           PERFORM MODULE-SORTIE.
       MODULE-ENTREE SECTION.
           MOVE DFHCOMMAREA         TO MEC24C-COMMAREA.
           MOVE '0'                 TO MEC24C-CODE-RETOUR .
           MOVE '0'                 TO MEC24C-NBR-MESSAGE-ENVOYE.
           PERFORM SELECT-CISOC.
           PERFORM CHARGE-DEPASSEMENT-QUOTAS.
           MOVE '4'      TO GFDATA.
           PERFORM FAIS-UN-LINK-A-TETDATC.
           MOVE GFSAMJ-0              TO GQ03-DJOUR-PLUS-N.
           INITIALIZE MEC00C-COMMAREA.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ASKTIME NOHANDLE END-EXEC.
      *--
           EXEC CICS ASKTIME NOHANDLE
           END-EXEC.
      *}
           MOVE EIBTIME             TO W-EIBTIME.
           MOVE W-HEURE             TO W-MESSAGE-H.
           MOVE W-MINUTE            TO W-MESSAGE-M.
      * CHARGEMENT TABLEAU MUT J0
           PERFORM CHARGEMENT-TAB-MUT-J0
           perform CHARGEMENT-AXE-SERVICE.
       MODULE-TRAITEMENT SECTION.
           INITIALIZE RVGQ0300.
           SET DEBUT-RTGQ03 TO TRUE.
           MOVE MEC24C-DATE-ENT          TO GQ03-DJOUR  B-GQ03-DJOUR
           MOVE MEC24C-SOCIETE-ENT       TO EC06-NSOCLIV
           MOVE MEC24C-CZONLIV-ENT       TO GQ03-CZONLIV
           MOVE MEC24C-CPLAGE-ENT        TO GQ03-CPLAGE
           MOVE MEC24C-CEQUIP-ENT        TO GQ03-CEQUIP
           MOVE MEC24C-CMODDEL-ENT       TO GQ03-CMODDEL
           MOVE 0                        TO GQ03-QQUOTA B-GQ03-QQUOTA
      *  > SELECTION DU QUOTA SUR LA ZONE DE LIVRAISON
      *  > CODE EQUIPE... LE TEC04 PREND TOUS LES CODES �QUIPES.
      *  > NORMALEMENT IL N'Y EN A QU'UN SEUL ASSOCI� AU P�RIM�TRE.
      *  > DONC TOUT VA BIEN.
           PERFORM DECLARE-RTGQ03
           PERFORM FETCH-RTGQ03
           PERFORM UNTIL FIN-RTGQ03
              IF GQ03-WILLIM = 'O'
                 MOVE 9999        TO B-GQ03-QQUOTA
              ELSE
                 ADD  GQ03-QQUOTA TO B-GQ03-QQUOTA
              END-IF
              MOVE GQ03-CZONLIV    TO B-GQ03-CZONLIV
              MOVE GQ03-DJOUR      TO B-GQ03-DJOUR
              MOVE GQ03-CPLAGE     TO B-GQ03-CPLAGE
              COMPUTE B-GQ03-QQUOTA   =  (GQ03-QQUOTA - GQ03-QPRIS)
      *       PERFORM SELECT-RTEC06
              PERFORM INFO-LIVRAISON
              IF TROUVE-INFO
                 PERFORM RECHERCHE-DEPASSEMENT-QUOTAS
                 PERFORM ALIMENTATION-TYPE-MUT
                 PERFORM ENVOI-QUOTA-A-WEBSPHERE
              END-IF
              PERFORM FETCH-RTGQ03
           END-PERFORM.
           IF MEC00C-FICHIER-SI > ' '
              SET MEC00C-FIN-TRAITEMENT TO TRUE
              MOVE SPACES TO MEC00C-DATA-SI
              MOVE 1 TO MEC00C-DATA-SI-L
              PERFORM FAIS-UN-LINK-A-MEC00
           END-IF.
       MODULE-SORTIE SECTION.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS RETURN END-EXEC.
      *--
           EXEC CICS RETURN
           END-EXEC.
      *}
       SELECT-CISOC SECTION.
           MOVE SPACES TO CISOC-CTABLEG2.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ASSIGN APPLID (CISOC-CTABLEG2) NOHANDLE END-EXEC.
      *--
           EXEC CICS ASSIGN APPLID (CISOC-CTABLEG2) NOHANDLE
           END-EXEC.
      *}
           EXEC SQL SELECT WTABLEG
                      INTO :CISOC-WTABLEG
                      FROM RVGA01BM
                     WHERE CCICS = :CISOC-CTABLEG2
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'SELECT CISOC' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
       CHARGE-DEPASSEMENT-QUOTAS SECTION.
           PERFORM DECLARE-OPEN-EC006.
           PERFORM FETCH-EC006
           PERFORM UNTIL SQLCODE = 100
              IF EC006-QQUOTA NUMERIC
                 ADD 1 TO I-EC6-U
                 IF I-EC6-U > I-EC6-MAX
                    MOVE 'mec24: Tableau EC006 trop petit ' TO W-MESSAGE
                    PERFORM ERREUR-PRG
                 END-IF
                 MOVE EC006-NSOC     TO W-EC6-NSOC(I-EC6-U)
                 MOVE EC006-NLIEU    TO W-EC6-NLIEU(I-EC6-U)
                 MOVE EC006-CZONLIV  TO W-EC6-CZONLIV(I-EC6-U)
                 MOVE EC006-CPLAGE   TO W-EC6-CPLAGE(I-EC6-U)
                 MOVE EC006-NJOUR    TO W-EC6-NJOUR(I-EC6-U)
                 MOVE EC006-QQUOTA-N TO W-EC6-QQUOTA-N(I-EC6-U)
              END-IF
              PERFORM FETCH-EC006
           END-PERFORM.
           PERFORM CLOSE-EC006.
      ******************************************************************
      * Recherche dans le tableau des d�passements de quota, qui est
      * par jour de la semaine, ce qui implique de le calculer,
      * si besoin est (le jour de la semaine).
      * Dans le tableau (chargement de la TG EC006), on recherche:
      * - d'abord, soci�t�, lieu de la plateforme plus zone de livraison
      * - ensuite:
      *   - soit la plage sp�cifique (MA, AM, JO) soit "**"=toutes
      *   - soit le jour de la semaine soit "*"=tous
      ******************************************************************
       RECHERCHE-DEPASSEMENT-QUOTAS SECTION.
           IF B-GQ03-DJOUR NOT = GFSAMJ-0
              MOVE B-GQ03-DJOUR TO GFSAMJ-0
              MOVE '5'     TO GFDATA
              PERFORM FAIS-UN-LINK-A-TETDATC
           END-IF.
           PERFORM VARYING I-EC6 FROM 1 BY 1 UNTIL I-EC6 > I-EC6-U
      *       IF    (W-EC6-NSOC(I-EC6)    = EC06-NSOCLIV)
      *         AND (W-EC6-NLIEU(I-EC6)   = EC06-NLIEULIV)
              IF    (W-EC6-NSOC(I-EC6)    = W-NSOCLIV)
                AND (W-EC6-NLIEU(I-EC6)   = W-NLIEULIV)
                AND (W-EC6-CZONLIV(I-EC6) = B-GQ03-CZONLIV)
                AND (   (W-EC6-CPLAGE(I-EC6) = '**')
                     OR (W-EC6-CPLAGE(I-EC6) = B-GQ03-CPLAGE))
                AND (   (W-EC6-NJOUR(I-EC6)  = '*')
                     OR (W-EC6-NJOUR(I-EC6)  = GFSMN))
                 ADD W-EC6-QQUOTA-N(I-EC6) TO B-GQ03-QQUOTA
              END-IF
           END-PERFORM.
       DECLARE-RTGQ03 SECTION.
           SET DEBUT-RTGQ03 TO TRUE
           EXEC SQL DECLARE CGQ03  CURSOR FOR
                     SELECT GQ03.DJOUR, GQ03.CZONLIV, GQ03.CPLAGE,
                           (GQ03.QQUOTA - GQ03.QPRIS), GQ03.WILLIM,
                            GQ03.DSYST ,
                            GQ03.CPLAGE
                       FROM RVGQ0300 GQ03
                      WHERE GQ03.WZONACT =  'O'
                        AND GQ03.DJOUR   = :GQ03-DJOUR
                        AND GQ03.CZONLIV = :GQ03-CZONLIV
                        AND GQ03.CMODDEL = :GQ03-CMODDEL
                        AND GQ03.CPLAGE  = :GQ03-CPLAGE
                        AND GQ03.CZONLIV IN
                            (SELECT MAX(ZLIV) FROM RVGA01ES
                              WHERE CELEM >  ' '
                              GROUP BY CELEM
                             HAVING COUNT(DISTINCT ZLIV) = 1)
           UNION ALL
                     SELECT GQ03.DJOUR, LIVRE.CELEM, GQ03.CPLAGE,
                           (GQ03.QQUOTA - GQ03.QPRIS), GQ03.WILLIM,
                            GQ03.DSYST ,
                            GQ03.CPLAGE
                       FROM RVGQ0300 GQ03, RVGA01ES LIVRE
                      WHERE GQ03.WZONACT =  'O'
                        AND GQ03.DJOUR   = :GQ03-DJOUR
                        AND GQ03.CZONLIV = :GQ03-CZONLIV
                        AND GQ03.CMODDEL = :GQ03-CMODDEL
                        AND GQ03.CPLAGE  = :GQ03-CPLAGE
                        AND GQ03.CZONLIV = LIVRE.ZLIV
                        AND LIVRE.CELEM IN
                            (SELECT CELEM FROM RVGA01ES
                              WHERE CELEM >  ' '
                              GROUP BY CELEM
                             HAVING COUNT(DISTINCT ZLIV) > 1)
           END-EXEC.
           EXEC SQL
                OPEN CGQ03
           END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'OPEN CGQ03' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
       FETCH-RTGQ03 SECTION.
           EXEC SQL FETCH CGQ03 INTO :GQ03-DJOUR, :GQ03-CZONLIV,
                          :GQ03-CPLAGE, :GQ03-QQUOTA, :GQ03-WILLIM,
                          :GQ03-DSYST
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE 'FETCH CGQ03' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
           IF SQLCODE = 100
              SET FIN-RTGQ03 TO TRUE
           END-IF.
       CLOSE-RTGQ03             SECTION.
           EXEC SQL CLOSE CGQ03 END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'CLOSE CGQ03' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
       DECLARE-OPEN-EC006 SECTION.
           EXEC SQL DECLARE CEC006 CURSOR FOR
                     SELECT CTABLEG2, WTABLEG
                       FROM RVEC006
                   ORDER BY CTABLEG2
           END-EXEC.
           EXEC SQL OPEN CEC006 END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'OPEN CEC006' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
       FETCH-EC006 SECTION.
           EXEC SQL FETCH  CEC006
                     INTO :EC006-CTABLEG2, :EC006-WTABLEG
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE 'FETCH CEC006' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
       CLOSE-EC006 SECTION.
           EXEC SQL CLOSE CEC006 END-EXEC.
           IF SQLCODE NOT = 0
              MOVE 'CLOSE CEC006' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
       SELECT-RTEC06 SECTION.
           MOVE 'FR'               TO EC06-CPAYS.
           MOVE MEC24C-SOCIETE-ENT TO EC06-NSOC.
           MOVE B-GQ03-CZONLIV     TO EC06-CELEMEN.
           EXEC SQL SELECT NSOCLIV, NLIEULIV
                      INTO :EC06-NSOCLIV, :EC06-NLIEULIV
                      FROM RVEC0601
                     WHERE CPAYS   = :EC06-CPAYS
                       AND NSOC    = :EC06-NSOC
                       AND CELEMEN = :EC06-CELEMEN
           END-EXEC.
           IF SQLCODE NOT = 0 AND 100
              MOVE 'SELECT RTEC06' TO W-MESSAGE
              PERFORM ERREUR-SQL
           END-IF.
       ENVOI-QUOTA-A-WEBSPHERE SECTION.
           PERFORM RECUP-TIMESP.
           MOVE 'RTGQ03'          TO MEC00C-FICHIER-SI.
           MOVE 'M'               TO MEC00C-CODE-MAJ.
      *    MOVE EC06-NSOCLIV      TO W-XML-RTGQ03-C-SOC.
      *    MOVE EC06-NLIEULIV     TO W-XML-RTGQ03-C-LIEU.
           MOVE W-NSOCLIV         TO W-XML-RTGQ03-C-SOC.
           MOVE W-NLIEULIV        TO W-XML-RTGQ03-C-LIEU.
           MOVE W-CMODDEL         TO W-XML-CMODDEL
           MOVE W-CSERVICE        TO W-XML-CSERVICE
           MOVE B-GQ03-CZONLIV    TO W-XML-RTGQ03-C-ZONE.
           MOVE B-GQ03-DJOUR      TO W-XML-RTGQ03-DT.
           MOVE B-GQ03-CPLAGE     TO W-XML-RTGQ03-C-CREN.
           MOVE B-GQ03-QQUOTA     TO W-XML-RTGQ03-QUOTA.
      * > RECHERCHE DES HEURES D'OUVERTURE ET FERMETURE DE LA PLAGE
           PERFORM VARYING IW3 FROM 1 BY 1
             UNTIL IW3 > W3-NBP
                OR B-GQ03-CPLAGE = W3-CPLAGE (IW3)
           END-PERFORM
           IF IW3 > W3-NBP
              move spaces to w-message
              STRING 'plage : ' B-GQ03-CPLAGE ' NON TROUVE'
              DELIMITED BY SIZE INTO w-message
              PERFORM ERREUR-PRG
           END-IF
           MOVE W3-HDEBUT (IW3)   TO W-XML-HDEBUT.
           MOVE W3-HFIN   (IW3)   TO W-XML-HFIN.
           MOVE W-CC-E-TIMESTP-9  TO W-XML-TIMESTP.
           MOVE 'T'           TO MEC00C-CODE-ACTION.
           MOVE W-XML-RTGQ03      TO MEC00C-DATA-SI.
           IF AFFICHER-OUI
              MOVE W-XML-RTGQ03   TO W-MESSAGE
              PERFORM DISPLAY-W-MESSAGE
           END-IF
           MOVE LENGTH OF W-XML-RTGQ03 TO MEC00C-DATA-SI-L.
           PERFORM FAIS-UN-LINK-A-MEC00.
       FAIS-UN-LINK-A-MEC00 SECTION.
      *    EXEC CICS LINK PROGRAM ('MEC0M')
           EXEC CICS LINK PROGRAM ('MEC00M')
                     COMMAREA (MEC00C-COMMAREA) NOHANDLE
           END-EXEC.
           IF MEC00C-CDRET-ERR-BLQ
      *       STRING 'MEC24: ERREUR MEC0M: '
              STRING 'MEC24: ERREUR MEC00M: '
                      MEC00C-MESSAGE DELIMITED BY SIZE INTO W-MESSAGE
              PERFORM ERREUR-PRG
           END-IF.
           IF MEC00C-CDRET-ERR-DB2-BLQ
              STRING 'mec24: Erreur MEC00 DB2: '
                      MEC00C-CODE-DB2-DISPLAY ' '
                      MEC00C-MESSAGE DELIMITED BY SIZE INTO W-MESSAGE
              PERFORM ERREUR-PRG
           END-IF.
       SELECT-NCGFC SECTION.
           MOVE  0                  TO W-CODE-DB2.
           MOVE '0'                 TO W-CODE-DB2-DISPLAY.
           INITIALIZE RVGA0100
                      W-PARAMETRES-NCGFC.
           MOVE 'NCGFC'             TO GA01-CTABLEG1.
           MOVE 'MEC24'             TO GA01-CTABLEG2.
           EXEC SQL SELECT WTABLEG INTO :GA01-WTABLEG
                      FROM RVGA0100
                     WHERE CTABLEG1 =:GA01-CTABLEG1
                       AND CTABLEG2 =:GA01-CTABLEG2
           END-EXEC.
           IF SQLCODE NOT = 0  and +100
              MOVE SQLCODE          TO W-CODE-DB2
              MOVE W-CODE-DB2       TO W-CODE-DB2-DISPLAY
              MOVE SPACES           TO W-MESSAGE
              STRING '- DB2 SEL RVGA01/NCGFC SQL='
                     W-CODE-DB2-DISPLAY
                DELIMITED BY SIZE
                INTO W-MESSAGE
              END-STRING
           END-IF.
           if sqlcode = +100
              move 'pas de NCGFC arr�t imm�diat sans plantage'
              to w-message
              perform DISPLAY-W-MESSAGE
           else
              MOVE GA01-WTABLEG        TO NCGFC-WTABLEG
              IF ( NCGFC-COMMENT > SPACES )
                 MOVE NCGFC-COMMENT(15:1) TO W-PARAMETRES-DISPLAY
              END-IF
              IF W-PARAMETRES-DISPLAY  = 'O'
                 SET AFFICHER-OUI TO TRUE
              END-IF
              IF ( NCGFC-WFLAG2        = 'O'    )
                 SET TRT-OK            TO TRUE
              END-IF
           END-IF.
       DISPLAY-W-MESSAGE SECTION.
           EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
                     LENGTH (80) NOHANDLE
           END-EXEC.
       FAIS-UN-LINK-A-TETDATC SECTION.
           EXEC CICS LINK PROGRAM ('TETDATC')
                     COMMAREA (Z-COMMAREA-TETDATC)
           END-EXEC.
           IF GFVDAT NOT = '1'
              MOVE 'EC00: Code retour TETDATC: ' TO W-MESSAGE
              MOVE GFVDAT                        TO W-MESSAGE(30:)
              PERFORM DISPLAY-W-MESSAGE
      *{ normalize-exec-xx 1.5
      *       EXEC CICS ABEND ABCODE ('DATE') END-EXEC
      *--
              EXEC CICS ABEND ABCODE ('DATE')
              END-EXEC
      *}
           END-IF.
       ERREUR-PRG SECTION.
           PERFORM DISPLAY-W-MESSAGE.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ABEND ABCODE ('EC03') END-EXEC.
      *--
           EXEC CICS ABEND ABCODE ('EC03')
           END-EXEC.
      *}
       ERREUR-SQL SECTION.
           PERFORM DISPLAY-W-MESSAGE.
           CALL DSNTIAR USING SQLCA, MSG-SQL, MSG-SQL-LRECL.
           PERFORM VARYING I-SQL FROM 1 BY 1 UNTIL I-SQL > 3
              MOVE MSG-SQL-MSG (I-SQL) TO W-MESSAGE
              PERFORM DISPLAY-W-MESSAGE
           END-PERFORM.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS ABEND ABCODE ('EC3S') END-EXEC.
      *--
           EXEC CICS ABEND ABCODE ('EC3S')
           END-EXEC.
      *}
       RECUP-TIMESP SECTION.
           EXEC SQL SELECT DEC(
                   (  DAYS (CURRENT TIMESTAMP - CURRENT TIMEZONE)
                    - DAYS (TIMESTAMP ('1970-01-01-00.00.00.000000'))
                    ) * 86400 + MIDNIGHT_SECONDS (CURRENT TIMESTAMP
                    - CURRENT TIMEZONE)
                    , 12 ) * 1000
                    + (MICROSECOND (CURRENT TIMESTAMP
                                     - CURRENT TIMEZONE) / 1000)
                      INTO :W-CC-TIMESTP
                      FROM SYSIBM.SYSDUMMY1
           END-EXEC.
            MOVE W-CC-TIMESTP   TO W-CC-E-TIMESTP-9.
       ABEND-LABEL SECTION.
           MOVE 'Handle Abend effectu�' TO W-MESSAGE.
           PERFORM DISPLAY-W-MESSAGE.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS RETURN END-EXEC.
      *--
           EXEC CICS RETURN
           END-EXEC.
      *}
       CHARGEMENT-AXE-SERVICE SECTION.
      * > CHARGEMENT TABLE RTEC06 POUR LA SOCI�T�
      * > CHARGEMENT DE LA SOUS-TABLE XCTRL POUR LIEN
      * >  - MODE DE D�LIVRANCE
      * >  - PLAGE
      * >  - SERVICE DE LIVRAISON
           MOVE 0 TO W1-NBP
           PERFORM DECLARE-OPEN-EC06.
           PERFORM FETCH-EC06.
           PERFORM UNTIL FIN-RTEC06
                   ADD 1   TO W1-NBP
                   IF W1-NBP > W1-MAX
                      MOVE 'TABLEAU W1 TROP PETIT'
                        TO W-MESSAGE
                      perform ERREUR-PRG
                   END-IF
                   SET IW1 TO W1-NBP
                   MOVE EC06-NSOC     TO    W1-NSOC     (IW1)
                   MOVE EC06-CSERVICE TO    W1-CSERVICE (IW1)
                   MOVE EC06-CMODDEL  TO    W1-CMODDEL  (IW1)
                   MOVE EC06-CELEMEN  TO    W1-CELEMEN  (IW1)
                   MOVE EC06-NSOCLIV  TO    W1-NSOCLIV  (IW1)
                   MOVE EC06-NLIEULIV TO    W1-NLIEULIV (IW1)
                   display 'RTEC06 : '  W1-POSTE (iw1)
                   PERFORM FETCH-EC06
           END-PERFORM.
           PERFORM CLOSE-EC06.
           IF IW1 = 0
              MOVE 'RTCE06 VIDE !!!' TO W-MESSAGE
              PERFORM ERREUR-PRG
           END-IF.
           MOVE 0 TO W2-NBP
           PERFORM DECLARE-OPEN-XCTRL.
           PERFORM FETCH-XCTRL.
           PERFORM UNTIL FIN-XCTRL
                   IF  GA01-WTABLEG (1:1) = 'O'
                   AND (GA01-CTABLEG2 (1:3) = CISOC-NSOC OR '000')
                   ADD 1   TO W2-NBP
                   IF W2-NBP > W2-MAX
                      MOVE 'TABLEAU W2 TROP PETIT'
                        TO W-MESSAGE
                      PERFORM ERREUR-PRG
                   END-IF
                   SET IW2 TO W2-NBP
                   MOVE GA01-WTABLEG (2:3)     TO W2-CMODDEL  (IW2)
                   MOVE GA01-WTABLEG (5:2)     TO W2-CPLAGE   (IW2)
                   MOVE GA01-WTABLEG (18:5)    TO W2-CSERVICE (IW2)
                   display 'XCTRL : '  W2-POSTE (iw2)
                   END-IF
                   PERFORM FETCH-XCTRL
           END-PERFORM.
           PERFORM CLOSE-XCTRL.
           IF IW2 = 0
              MOVE 'XCTRL VIDE !!!' TO W-MESSAGE
              PERFORM ERREUR-PRG
           END-IF.
           MOVE 'LPLGE' TO GA01-CTABLEG1
           PERFORM DECLARE-GA01.
           PERFORM FETCH-GA01
           MOVE 0 TO W3-NBP
           PERFORM UNTIL FIN-GA01
              ADD 1   TO W3-NBP
              IF W3-NBP > W3-MAX
                 MOVE 'TABLEAU W3 TROP PETIT'
                   TO W-MESSAGE
                 PERFORM ERREUR-PRG
              END-IF
              SET IW3 TO W3-NBP
              MOVE GA01-CTABLEG2(1:2)  TO W3-CPLAGE (IW3)
              MOVE GA01-WTABLEG (15:4) TO W3-HDEBUT (IW3)
              MOVE GA01-WTABLEG (19:4) TO W3-HFIN   (IW3)
              PERFORM FETCH-GA01
           END-PERFORM.
           PERFORM CLOSE-GA01.
           IF IW3 = 0
              MOVE 'LPLGE VIDE !!!' TO W-MESSAGE
              PERFORM ERREUR-PRG
           END-IF.
       F-CHARGEMENT-AXE-SERVICE. EXIT.
       INFO-LIVRAISON SECTION.
      * > ON PART D'UN QUOTA D�FINI SUR UNE ZONE DE LIVRAISON/ELEM
      * > D�FINI SUR UNE PLAGE DONN�E.
      * > ON CHERCHE � D�TERMINER QUEL SERA LE SERVICE DE LIVRAISON
      * > ASSOCI� EN PLUS DE LA PLATEFORME COMME ON LE FAISAIT DANS
      * > LA RTEC06.
      * >
      * > RECHERCHE DANS LE TABLEAU W1 POUR  D�TERMINER LES
      * > SERVICES ASSOCI�S � LA ZONE DE LIVRAISON.
      * > SI TROUV� ON OBTIENT ALORS UNE OU PLUSIEURS ASSOCIATIONS
      * > SERVICES / MODE DE D�LIVRANCE CLIENT EN PLUS DE LA PLAGE
      * > HORAIRE DE LA GQ03.
      * > C'EST ALORS QUE L'ON PEUT ALLER CONTROLER DANS LE TABLEAU W2
      * > QU'IL EXISTE UNE RELATION �TABLIE ENTRE LE CODE PLAGE
      * > ET L'UN DES SERVICES DE LA TABLE W1.
      * > LA RELATION DANS LA TABLE W2 PEUT �TRE DU TYPE :
      * > LD2 ** SERVICE1
      * > LD2 S1 SERVICE2
      * > IL VA DONC FALLOIR CONTROLER QUE LA PLAGE NE SE TROUVE
      * > PAS SP�CIFIQUEMENT D�FINIE ET SI �A N'EST PAS LE CAS
      * > PRENDRE LA RELATION G�N�RIQUE DE TYPE LD2 **.
           MOVE CISOC-NSOC      TO EC06-NSOC.
           MOVE B-GQ03-CZONLIV  TO EC06-CELEMEN.
           MOVE SPACES          TO W-INFO.
           SET NON-TROUVE-INFO TO TRUE.
      * > RECHERCHE SUR LA ZONE DE LIVRAISON TOUS LES SERVICES.
           PERFORM VARYING IW1 FROM 1 BY 1
             UNTIL IW1 > W1-NBP
                IF EC06-CELEMEN = W1-CELEMEN (IW1)
      * > RECHERCHE SI LA RELATION SE V�RIFIE AVEC LE CODE PLAGE.
                   MOVE W1-NSOCLIV  (IW1)   TO W-NSOCLIV
                   MOVE W1-NLIEULIV (IW1)   TO W-NLIEULIV
                   PERFORM VARYING IW2 FROM 1 BY 1
                     UNTIL IW2 > W2-NBP OR TROUVE-INFO
                           IF W2-CSERVICE (IW2) = W1-CSERVICE (IW1)
                           AND (W2-CPLAGE (IW2) = B-GQ03-CPLAGE
                            OR W2-CPLAGE (IW2) = '**')
                               IF W-CPLAGE = '  ' OR '**'
                                  MOVE W2-CPLAGE (IW2)
                                    TO W-CPLAGE
                                  MOVE W2-CSERVICE (IW2)
                                    TO W-CSERVICE
                                  MOVE W2-CMODDEL  (IW2)
                                     TO W-CMODDEL
                                  IF W-CPLAGE NOT = '**'
                                     SET TROUVE-INFO TO TRUE
                                  END-IF
                               END-IF
                           END-IF
                    END-PERFORM
                END-IF
           END-PERFORM.
           IF W-CPLAGE  = '**'
              SET TROUVE-INFO TO TRUE
           END-IF.
       F-INFO-LIVRAISON. EXIT.
       DECLARE-OPEN-EC06 SECTION.
           SET DEBUT-RTEC06 TO TRUE
           MOVE CISOC-NSOC  TO EC06-NSOC
           EXEC SQL DECLARE EC06 CURSOR FOR
                 SELECT  CPAYS
                        ,NSOC
                        ,CSERVICE
                        ,CMODDEL
                        ,CELEMEN
                        ,NSOCLIV
                        ,NLIEULIV
               FROM RVEC0601
               WHERE NSOC = :EC06-NSOC
           END-EXEC
           EXEC SQL OPEN EC06
           END-EXEC.
       FIN-DECLARE-EC06. EXIT.
       FETCH-EC06 SECTION.
           EXEC SQL FETCH  EC06
                     INTO :EC06-CPAYS
                         ,:EC06-NSOC
                         ,:EC06-CSERVICE
                         ,:EC06-CMODDEL
                         ,:EC06-CELEMEN
                         ,:EC06-NSOCLIV
                         ,:EC06-NLIEULIV
           END-EXEC
           IF SQLCODE NOT = 0 AND 100
              MOVE 'FETCH  RTEC06' TO W-MESSAGE
              PERFORM ERREUR-SQL
           ELSE
              IF SQLCODE = 100
                 SET FIN-RTEC06 TO TRUE
              END-IF
           END-IF.
       FIN-FETCH-EC06. EXIT.
       CLOSE-EC06 SECTION.
           EXEC SQL CLOSE EC06
           END-EXEC.
       FIN-CLOSE-EC06. EXIT.
       DECLARE-OPEN-XCTRL SECTION.
           SET DEBUT-XCTRL TO TRUE
           EXEC SQL DECLARE XCTRL CURSOR FOR
                    SELECT  CTABLEG2
                           ,WTABLEG
                    FROM RVGA0100
                    WHERE CTABLEG1 = 'XCTRL'
                      AND CTABLEG2 LIKE '%EXPDELES%'
           END-EXEC
           EXEC SQL OPEN XCTRL
           END-EXEC.
       FIN-DECLARE-XCTRL. EXIT.
       FETCH-XCTRL SECTION.
           EXEC SQL FETCH  XCTRL
                     INTO :GA01-CTABLEG2
                         ,:GA01-WTABLEG
           END-EXEC
           IF SQLCODE NOT = 0 AND 100
              MOVE 'FETCH  XCTRL' TO W-MESSAGE
              PERFORM ERREUR-SQL
           ELSE
              IF SQLCODE = 100
                 SET FIN-XCTRL TO TRUE
              END-IF
           END-IF.
       FIN-FETCH-XCTRL. EXIT.
       CLOSE-XCTRL SECTION.
           EXEC SQL CLOSE XCTRL
           END-EXEC.
       FIN-CLOSE-XCTRL. EXIT.
       DECLARE-GA01 SECTION.
           SET DEBUT-GA01 TO TRUE
           EXEC SQL DECLARE GA01 CURSOR FOR SELECT
                 CTABLEG2,
                 WTABLEG
               FROM RVGA0100
               WHERE CTABLEG1 = :GA01-CTABLEG1
               ORDER BY CTABLEG2
           END-EXEC
           EXEC SQL OPEN GA01
           END-EXEC.
       FIN-DECLARE-GA01. EXIT.
       FETCH-GA01 SECTION.
           EXEC SQL FETCH  GA01
                     INTO :GA01-CTABLEG2,
                          :GA01-WTABLEG
           END-EXEC
           IF SQLCODE NOT = 0 AND 100
              MOVE 'FETCH RTGA01' TO W-MESSAGE
              PERFORM ERREUR-SQL
           ELSE
              IF SQLCODE = 100
                 SET FIN-GA01 TO TRUE
              END-IF
           END-IF.
       FIN-FETCH-GA01. EXIT.
       CLOSE-GA01 SECTION.
           EXEC SQL CLOSE GA01
           END-EXEC.
       FIN-CLOSE-GA01. EXIT.
       CHARGEMENT-TAB-MUT-J0  SECTION.
           PERFORM DECLARE-OPEN-XCTRL-MU
           PERFORM FETCH-XCTRL-MU
           PERFORM UNTIL FIN-XCTRL OR (WS-NB-SEL = WS-NB-MAX)
              IF GA01-WTABLEG(17:1) = 'O'
                 ADD  1  TO WS-NB-SEL
                 MOVE GA01-WTABLEG(2:3)  TO WS-CMODDEL(WS-NB-SEL)
                 MOVE GA01-WTABLEG(18:3) TO WS-TYPE-MUT(WS-NB-SEL)
              END-IF
              PERFORM FETCH-XCTRL-MU
           END-PERFORM
           PERFORM CLOSE-XCTRL-MU
           .
       F-CHARGEMENT-TAB-MUT-J0.  EXIT.
       ALIMENTATION-TYPE-MUT SECTION.
           MOVE SPACE    TO W-XML-TP-MUTATION
           PERFORM VARYING I FROM 1 BY 1 UNTIL I > WS-NB-SEL
           OR (W-XML-TP-MUTATION NOT = SPACE)
              IF WS-CMODDEL(I) = W-CMODDEL
                 MOVE WS-TYPE-MUT(I)  TO W-XML-TP-MUTATION
              END-IF
           END-PERFORM
           .
       F-ALIMENTATION-TYPE-MUT. EXIT.
       DECLARE-OPEN-XCTRL-MU SECTION.
           SET DEBUT-XCTRL TO TRUE
           EXEC SQL DECLARE XCTRL-MU CURSOR FOR
                    SELECT WTABLEG
                    FROM RVGA0100
                    WHERE CTABLEG1 = 'XCTRL'
                      AND CTABLEG2 LIKE '%EXPDELMU%'
                      AND WTABLEG  LIKE 'O%'
           END-EXEC
           EXEC SQL OPEN XCTRL-MU
           END-EXEC
           .
       FIN-DECLARE-XCTRL-MU. EXIT.
      
       FETCH-XCTRL-MU SECTION.
           EXEC SQL FETCH  XCTRL-MU
                     INTO :GA01-WTABLEG
           END-EXEC
           IF SQLCODE NOT = 0 AND 100
              MOVE 'FETCH  XCTRL' TO W-MESSAGE
              PERFORM ERREUR-SQL
           ELSE
              IF SQLCODE = 100
                 SET FIN-XCTRL TO TRUE
              END-IF
           END-IF
           .
       FIN-FETCH-XCTRL-MU. EXIT.
      
       CLOSE-XCTRL-MU SECTION.
           EXEC SQL CLOSE XCTRL-MU
           END-EXEC
           .
       FIN-CLOSE-XCTRL-MU. EXIT.
      
