      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVDC1000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVDC1000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVDC1000.                                                    00000090
           02  DC10-NSOCIETE                                            00000100
               PIC X(0003).                                             00000110
           02  DC10-NDOSSIER                                            00000120
               PIC X(0007).                                             00000130
           02  DC10-NLIEU                                               00000140
               PIC X(0003).                                             00000150
           02  DC10-NVENTE                                              00000160
               PIC X(0007).                                             00000170
           02  DC10-DECHEANCE                                           00000180
               PIC X(0008).                                             00000190
           02  DC10-NECHEANCE                                           00000200
               PIC S9(1) COMP-3.                                        00000210
           02  DC10-MECHEANCE                                           00000220
               PIC S9(7)V9(0002) COMP-3.                                00000230
           02  DC10-DPREMPRES                                           00000240
               PIC X(0008).                                             00000250
           02  DC10-NBPRES                                              00000260
               PIC S9(1) COMP-3.                                        00000270
           02  DC10-STATUT                                              00000280
               PIC X(0002).                                             00000290
           02  DC10-DSYST                                               00000300
               PIC S9(13) COMP-3.                                       00000310
      *                                                                 00000320
      *---------------------------------------------------------        00000330
      *   LISTE DES FLAGS DE LA TABLE RVDC1000                          00000340
      *---------------------------------------------------------        00000350
      *                                                                 00000360
       01  RVDC1000-FLAGS.                                              00000370
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-NSOCIETE-F                                          00000380
      *        PIC S9(4) COMP.                                          00000390
      *--                                                                       
           02  DC10-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-NDOSSIER-F                                          00000400
      *        PIC S9(4) COMP.                                          00000410
      *--                                                                       
           02  DC10-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-NLIEU-F                                             00000420
      *        PIC S9(4) COMP.                                          00000430
      *--                                                                       
           02  DC10-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-NVENTE-F                                            00000440
      *        PIC S9(4) COMP.                                          00000450
      *--                                                                       
           02  DC10-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-DECHEANCE-F                                         00000460
      *        PIC S9(4) COMP.                                          00000470
      *--                                                                       
           02  DC10-DECHEANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-NECHEANCE-F                                         00000480
      *        PIC S9(4) COMP.                                          00000490
      *--                                                                       
           02  DC10-NECHEANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-MECHEANCE-F                                         00000500
      *        PIC S9(4) COMP.                                          00000510
      *--                                                                       
           02  DC10-MECHEANCE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-DPREMPRES-F                                         00000520
      *        PIC S9(4) COMP.                                          00000530
      *--                                                                       
           02  DC10-DPREMPRES-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-NBPRES-F                                            00000540
      *        PIC S9(4) COMP.                                          00000550
      *--                                                                       
           02  DC10-NBPRES-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-STATUT-F                                            00000560
      *        PIC S9(4) COMP.                                          00000570
      *--                                                                       
           02  DC10-STATUT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC10-DSYST-F                                             00000580
      *        PIC S9(4) COMP.                                          00000590
      *--                                                                       
           02  DC10-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00000600
                                                                                
