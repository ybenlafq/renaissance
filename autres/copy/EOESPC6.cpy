      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        *
      * FICHIER.....:     : DECLARATION PRODUIT          (OBLIGATOIRE)*
      * FICHIER.....: EMISSION OP ENT�TE SPECIFIQUE (MUTATION)        *
      * NOM FICHIER.: EOESPC6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 098                                             *
      *****************************************************************
      *
       01  EOESPC6.
      * CODE IDENTIFICATEUR
           05      EOESPC6-TYP-ENREG      PIC  X(0006).
      * CODE SOCI�T�
           05      EOESPC6-CSOCIETE       PIC  X(0005).
      * NUM�RO D'OP
           05      EOESPC6-NOP            PIC  X(0015).
      * NUM�RO DE BL
           05      EOESPC6-NBL            PIC  9(0007).
      * FILLER 30
           05      EOESPC6-FILLER         PIC  X(0030).
      * FIN
           05      EOESPC6-FIN            PIC  X(0001).
      
