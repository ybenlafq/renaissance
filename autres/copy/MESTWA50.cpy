      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ******************************************************************        
      * - MESTWA50 COPY GENERALISES  49974 OCTETS                      *      13
      *   = 105 DE DEFINITION D ENTETE                                 *        
      *   + 34  DE DEFINITION DE CODE RETOUR                           *        
      *   + 49835 DE MESSAGE REEL                                      *        
      * COPY POUR LA SERIE DE TRANSACTION TGA50                        *      13
      ******************************************************************        
              05 MESTWA50-ENTETE.                                               
                 10 MESTWA50-TYPE        PIC X(03).                             
                 10 MESTWA50-NSOCMSG     PIC X(03).                             
                 10 MESTWA50-NLIEUMSG    PIC X(03).                             
                 10 MESTWA50-NSOCDST     PIC X(03).                             
                 10 MESTWA50-NLIEUDST    PIC X(03).                             
                 10 MESTWA50-NORD        PIC 9(08).                             
                 10 MESTWA50-LPROG       PIC X(10).                             
                 10 MESTWA50-DJOUR       PIC X(08).                             
                 10 MESTWA50-WSID        PIC X(10).                             
                 10 MESTWA50-USER        PIC X(10).                             
                 10 MESTWA50-CHRONO      PIC 9(07).                             
                 10 MESTWA50-NBRMSG      PIC 9(07).                             
                 10 MESTWA50-NBRENR      PIC 9(5).                              
                 10 MESTWA50-TAILLE      PIC 9(5).                              
                 10 MESTWA50-VERSION     PIC X(2).                              
                 10 MESTWA50-FILLER      PIC X(18).                             
              05 MESTWA50-RETOUR.                                               
                 10 MESTWA50-CODE-RETOUR.                                       
                    15  MESTWA50-CODRET  PIC X(1).                              
                         88  MESTWA50-CODRET-OK        VALUE ' '.               
                         88  MESTWA50-CODRET-ERREUR    VALUE '1'.               
                    15  MESTWA50-LIBERR  PIC X(33).                             
              05 MESTWA50-DATA           PIC X(49835).                          
                                                                                
