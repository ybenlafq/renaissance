      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  TS-IDFM.                                                             
      *----------------------------------  LONGUEUR TS                          
           02 TS-IDFM-LONG                 PIC S9(03) COMP-3                    
                                           VALUE +40.                           
      *----------------------------------  DONNEES  TS                          
           02 TS-IDFM-DONNEES.                                                  
              03 TS-IDFM-NUMSEG            PIC 9(02).                           
              03 TS-IDFM-NSEG-SAP          PIC X(30).                           
              03 TS-IDFM-NSEG-PT9903       PIC X(08).                           
                                                                                
