      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * -------------------------------------------------------------- *        
      * -------------------------------------------------------------- *        
      * -------------------------------------------------------------- *        
      * -------------------------------------------------------------- *        
      * -------------------------------------------------------------- *        
      *01 LG-TSMIQ00-TS                     PIC S9(04) COMP VALUE +4846.        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01 LG-TSMIQ00-TS                     PIC S9(04) COMP VALUE +8026.        
      *--                                                                       
       01 LG-TSMIQ00-TS                     PIC S9(04) COMP-5 VALUE             
                                                                  +8026.        
      *}                                                                        
       01 TSMIQ00-TS.                                                           
          05 TSMIQ00-NBLMAX                  PIC S9(02) COMP-3 VALUE 50.        
          05 TSMIQ00-DATA.                                                      
                10 TS-MIQ00-NBLIGNE          PIC S9(03) COMP-3 VALUE 0.         
                10 TS-MIQ00-LIGNE  OCCURS 50.                                   
39                 15 TS-MIQ00-LIBELLE.                                         
                      20 TS-MIQ00-CPLAGE     PIC X(02) VALUE SPACES.            
                      20 TS-MIQ00-LPLAGE     PIC X(19) VALUE SPACES.            
                      20 TS-MIQ00-SRVCE      PIC X(05) VALUE SPACES.            
                      20 TS-MIQ00-CPERIM     PIC X(05) VALUE SPACES.            
                      20 TS-MIQ00-CEQUIP     PIC X(05) VALUE SPACES.            
                   15 TS-MIQ00-PRIX          PIC X(06) VALUE SPACES.            
                15 TS-MIQ00-TAB1   OCCURS 9.                                    
                   20 TS-MIQ00-DATE          PIC X(08) VALUE SPACES.            
                   20 TS-MIQ00-QQUOTA        PIC 9(05) VALUE 0.                 
          05 TSTAB-DATE OCCURS 9.                                               
              10 TS-DATE  PIC X(08).                                            
                                                                                
