      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE MRM20                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  RM91-LONG                 PIC S9(3) COMP-3 VALUE 156.        00010000
       01  RM91-DONNEES.                                                00020000
           02  RM91-NCODIC           PIC X(0007).                               
           02  RM91-NSOCIETE         PIC X(0003).                               
           02  RM91-NLIEU            PIC X(0003).                               
           02  RM91-QV8SR            PIC S9(7) COMP-3.                          
           02  RM91-QSTOCKOBJR       PIC S9(7) COMP-3.                          
           02  RM91-QV8SD            PIC S9(7) COMP-3.                          
           02  RM91-QSTOCKOBJD       PIC S9(7) COMP-3.                          
           02  RM91-QV8SC            PIC S9(7) COMP-3.                          
           02  RM91-QSTOCKOBJC       PIC S9(7) COMP-3.                          
           02  RM91-QV8SP            PIC S9(7) COMP-3.                          
           02  RM91-QSTOCKOBJP       PIC S9(7) COMP-3.                          
           02  RM91-QSO              PIC S9(7) COMP-3.                          
           02  RM91-QSA              PIC S9(7) COMP-3.                          
           02  RM91-QFREQUENCE       PIC S9(1)V9(0002) COMP-3.                  
           02  RM91-QINDICER         PIC S9(1)V9(0006) COMP-3.                  
           02  RM91-QINDICEP         PIC S9(1)V9(0006) COMP-3.                  
           02  RM91-QPOIDSMAG        PIC S9(1)V9(0006) COMP-3.                  
           02  RM91-QTE              PIC S9(1)V9(0006) COMP-3.                  
           02  RM91-NLOI             PIC X(0002).                               
           02  RM91-NLOIP            PIC X(0002).                               
           02  RM91-QV8SE-NON-AJUSTE PIC S9(7) COMP-3.                          
           02  RM91-V8SA             PIC S9(7) COMP-3.                          
           02  RM91-WASSORT          PIC X.                                     
           02  RM91-QSO-SAVE         PIC S9(7) COMP-3.                          
           02  RM91-QSA-SAVE         PIC S9(7) COMP-3.                          
           02  RM91-QSOTROUVE        PIC X.                                     
           02  RM91-QSATROUVE        PIC X.                                     
           02  RM91-QSOMIN           PIC S9(7) COMP-3.                          
           02  RM91-QSAMIN           PIC S9(7) COMP-3.                          
           02  RM91-QSOMAX           PIC S9(7) COMP-3.                          
           02  RM91-QSAMAX           PIC S9(7) COMP-3.                          
           02  RM91-W80              PIC X.                                     
           02  RM91-EXISTENCE        PIC X.                                     
           02  RM91-QSOS             PIC S9(7) COMP-3.                          
           02  RM91-QSAS             PIC S9(7) COMP-3.                          
           02  RM91-QSOAS            PIC S9(7) COMP-3.                          
           02  RM91-QSAAS            PIC S9(7) COMP-3.                          
           02  RM91-S-1              PIC S9(5) COMP-3.                          
           02  RM91-S-2              PIC S9(5) COMP-3.                          
           02  RM91-S-3              PIC S9(5) COMP-3.                          
           02  RM91-S-4              PIC S9(5) COMP-3.                          
           02  RM91-QEXPO            PIC S9(5) COMP-3.                          
           02  RM91-QLS              PIC S9(5) COMP-3.                          
           02  RM91-STOCK-FORCE      PIC X.                                     
           02  RM91-QSO-FORCE        PIC S9(7) COMP-3.                          
           02  RM91-QSA-FORCE        PIC S9(7) COMP-3.                          
           02  RM91-VALIDATION       PIC X.                                     
                                                                                
