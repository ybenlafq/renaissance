      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * - MESTGR32 COPY MESSAGE POUR L ENVOI ET LA RECEPTION LONGUEUR  *      13
      *  49835                                                         *      13
      * COPY POUR LES PROGRAMMES TGR30 MGR30                           *      13
      ******************************************************************        
              05 MESTGR32-DATA REDEFINES MESTGR30-DATA.                         
      * LONGUEUR DE 100 OCTETS * 495                                            
                 10 MESTGR32-DATA-X            OCCURS  495.                     
                    15 MESTGR32-NSOCLIVR       PIC X(3).                        
                    15 MESTGR32-NDEPOT         PIC X(3).                        
                    15 MESTGR32-NREC           PIC X(7).                        
                    15 MESTGR32-NRECQUAI       PIC X(7).                        
                    15 MESTGR32-DJRECQUAI      PIC X(8).                        
                    15 MESTGR32-TRANSFERT      PIC X(1).                        
                    15 MESTGR32-FILLER         PIC X(71).                       
                 10 MESTGR32-DATA-INUTILE      PIC X(335).                      
                                                                                
