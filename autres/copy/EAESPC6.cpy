      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE ENTETE SPECIFIQUE              *
      * NOM FICHIER.: EAESPC6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 074                                             *
      *****************************************************************
      *
       01  EAESPC6.
      * TYPE ENREGISTREMENT
           05      EAESPC6-TYP-ENREG      PIC  X(0006).
      * CODE SOCIETE
           05      EAESPC6-CSOCIETE       PIC  X(0005).
      * CODE ARTICLE
           05      EAESPC6-CARTICLE       PIC  X(0018).
      * DATE DERNIERE MODIF
           05      EAESPC6-DMODIF         PIC  X(0008).
      * HEURE DERNIERE MODIF
           05      EAESPC6-HMODIF         PIC  X(0006).
      * FILL 30
           05      EAESPC6-FILLER         PIC  X(0030).
      * FIN
           05      EAESPC6-FIN            PIC  X(0001).
      
