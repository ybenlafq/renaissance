      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
           EJECT                                                                
      *==> DARTY ******************************************************         
      *    ZONE DE COMMUNICATION DU MEC08                             *         
      *****************************************************************         
LD0410*   MODIFICATION TAILLE DU DATA-SI POUR POUVOIR RECEVOIR                  
LD0410*   L'ENTETE D.COM (AJOUT D'ASSEZ DE PLUS POUR POUVOIR PRENDRE            
LD0410*   ENCORE DES ZONES)                                                     
      *****************************************************************         
      *--> LONGUEUR MAX ADMIS POUR UNE COMMAREA 32 767.                         
       01  MEC08C-COMMAREA.                                                     
      *--> DONNEES FOURNIS PAR LE PROGRAMME APPELANT.                           
           05  MEC08C-DATA-ENTREE.                                              
               10  MEC08C-FICHIER-SI               PIC  X(10).                  
LD0410*        10  MEC08C-DATA-SI                  PIC  X(1024).                
LD0410         10  MEC08C-DATA-SI                  PIC  X(1500).                
               10  MEC08C-DATA-SI-L                PIC  9(05).                  
               10  MEC08C-CODE-MAJ                 PIC  X(01).                  
      *--> DONNEES RESULTANTES.                                                 
           05  MEC08C-DATA-SORTIE.                                              
               10  MEC08C-DATA-XML-COMPLET         PIC  X(15000).               
               10  MEC08C-DATA-XML-COMPLET-L       PIC  9(05).                  
               10  MEC08C-LIGNE-ENTETE-XML         PIC  X(60).                  
               10  MEC08C-LIGNE-ENTETE-XML-L       PIC  9(03).                  
               10  MEC08C-LIGNE-DTD                PIC  X(60).                  
               10  MEC08C-LIGNE-DTD-L              PIC  9(02).                  
               10  MEC08C-BALISE-DEBUT             PIC  X(50).                  
               10  MEC08C-BALISE-DEBUT-L           PIC  9(02).                  
               10  MEC08C-DATA-XML                 PIC  X(12000).               
               10  MEC08C-DATA-XML-L               PIC  9(05).                  
               10  MEC08C-BALISE-FIN-ENREG         PIC  X(19).                  
               10  MEC08C-BALISE-FIN-ENREG-L       PIC  9(02).                  
               10  MEC08C-BALISE-FIN               PIC  X(50).                  
               10  MEC08C-BALISE-FIN-L             PIC  9(02).                  
               10  MEC08C-CODE-RETOUR              PIC  X(01).                  
                   88  MEC08C-CDRET-OK                  VALUE '0'.              
                   88  MEC08C-CDRET-ERR-NON-BLQ         VALUE '1'.              
                   88  MEC08C-CDRET-ERR-DB2-NON-BLQ     VALUE '2'.              
                   88  MEC08C-CDRET-ERR-BLQ             VALUE '8'.              
                   88  MEC08C-CDRET-ERR-DB2-BLQ         VALUE '9'.              
               10  MEC08C-CODE-DB2                 PIC ++++9.                   
               10  MEC08C-CODE-DB2-DISPLAY         PIC  X(05).                  
               10  MEC08C-MESSAGE                  PIC  X(500).                 
      *--> DONNEES DE TRAVAIL A CONSERVE ENTRE CHACUN DES APPELS.               
           05  MEC08C-DATA-TRAVAIL.                                             
      *-->     PARAMETRES LU DE LA SOUS-TABLE EC001 (RVEC001)                   
               10  MEC08C-TABLE-SI                 PIC  X(10).                  
               10  MEC08C-TABLE-ECOMM              PIC  X(16).                  
               10  MEC08C-TABLE-ECOMM-L            PIC  9(02).                  
               10  MEC08C-ROOT-ECOMM               PIC  X(17).                  
               10  MEC08C-ROOT-ECOMM-L             PIC  9(02).                  
               10  MEC08C-DTD-ECOMM                PIC  X(20).                  
               10  MEC08C-DTD-ECOMM-L              PIC  9(02).                  
               10  MEC08C-WTRT                     PIC  X(01).                  
                   88  MEC08C-WTRT-MQ                   VALUE '1'.              
                   88  MEC08C-WTRT-FICHIER              VALUE '2'.              
                   88  MEC08C-WTRT-MQ-ET-FICHIER        VALUE '3'.              
      *{ remove-comma-in-dde 1.5                                                
      *            88  MEC08C-WTRT-ENVOIE-MQ            VALUE '1', '3'.         
      *--                                                                       
                   88  MEC08C-WTRT-ENVOIE-MQ            VALUE '1'  '3'.         
      *}                                                                        
      *{ remove-comma-in-dde 1.5                                                
      *            88  MEC08C-WTRT-FICHIER-REQUIS       VALUE '2', '3'.         
      *--                                                                       
                   88  MEC08C-WTRT-FICHIER-REQUIS       VALUE '2'  '3'.         
      *}                                                                        
      *-->     PARAMETRES LU DE LA SOUS-TABLE EC002 (RVEC002)                   
               10  MEC08C-NBR-OCCURENCE            PIC  9(03).                  
               10  MEC08C-NBR-OCCURENCE-MAX        PIC  9(03) VALUE 100.        
               10  MEC08C-TABLE-ATTRIBUT OCCURS 100 INDEXED I-ATTR.             
                   15  MEC08C-CMASQ                PIC  X(07).                  
                   15  MEC08C-CMASQ-REDEF REDEFINES MEC08C-CMASQ.               
                       20  MEC08C-CMASQ-LONG       PIC  9(05).                  
                       20  MEC08C-CMASQ-TYPE       PIC  X(01).                  
                           88  MEC08C-TYPE-ALPHA        VALUE 'A'.              
                           88  MEC08C-TYPE-CODE         VALUE 'C'.              
                           88  MEC08C-TYPE-HTML         VALUE 'H'.              
                           88  MEC08C-TYPE-NUMERIC      VALUE 'N'.              
                           88  MEC08C-TYPE-PACKED       VALUE 'P'.              
                       20  MEC08C-CMASQ-DEC        PIC  9(01).                  
                   15  MEC08C-ATTRIBUT             PIC  X(20).                  
                   15  MEC08C-ATTRIBUT-L           PIC  9(02).                  
LD0410*    05  FILLER                              PIC  X(980).                 
LD0410     05  FILLER                              PIC  X(504).                 
      ********* FIN DE LA ZONE DE COMMUNICATION DU MEC08  *************         
                                                                                
