      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * COPY POUR CONTROLE D'UN LIEU DANS GA01/HEGSA                            
      * UTILISE POUR APLLICATION GEF                                            
LG=702*                                                                         
       01  W-HEGSA-ZONE.                                                        
      *                                                                         
           03 W-TAB-HEGSA.                                                      
              05 W-HEGSA-ENREG     OCCURS 50.                                   
                 07 W-HEGSA-NSOC       PIC X(3).                                
                 07 W-HEGSA-NLIEU      PIC X(3).                                
                 07 W-HEGSA-NSSLIEU    PIC X(3).                                
                 07 W-HEGSA-CLIEUTRT   PIC X(5).                                
      *                                                                         
           03 W-NB-HEGSA               PIC 9(2).                                
                                                                                
