      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *   MESTBS76 COPY                                                *      13
      * - RECEPTION RESA DE BIZ                                        *      13
      * COPY POUR LES PROGRAMMES TBS76 ET MBS76                        *      13
      ******************************************************************        
              05 MESTBS76-DATA.                                                 
                 10 MESTBS76-ENTETE-VENTE.                                      
                   15 MESTBS76-TRAN           PIC X(04).                        
                   15 MESTBS76-REFCMDEXT.                                       
                     30 MESTBS76-IDCLIENTA    PIC X(08).                        
                     30 MESTBS76-IDCLIENTB    PIC X(08).                        
                     30 MESTBS76-IDCLIENTFIL  PIC X(14).                        
                   15 MESTBS76-CVENDEUR       PIC X(06).                        
                   15 MESTBS76-IDBIZ          PIC X(30).                        
      *---            MISE EN SERVICE                                           
                   15 MESTBS76-MSERVICE       PIC X(01).                        
      *---            DEPOSE UNIQUEMENT                                         
                   15 MESTBS76-DEPOSE         PIC X(01).                        
      *---            REPRISE ANCIEN APPAREIL                                   
                   15 MESTBS76-REPRISEANC     PIC X(01).                        
      *---            SOCIETE - LIEU PAIEMENT                                   
                   15 MESTBS76-SOCLIEUP       PIC X(06).                        
      * ADRESSES                                                                
                 10 MESTBS76-LIVRAISON.                                         
                  12 MESTBS76-LIVRAISON2 OCCURS 2.                              
                   15 MESTBS76-NOM            PIC X(25).                        
                   15 MESTBS76-PRENOM         PIC X(15).                        
                   15 MESTBS76-CIVILITE       PIC X(15).                        
                   15 MESTBS76-LTELBUR        PIC X(10).                        
                   15 MESTBS76-EMAIL          PIC X(63).                        
                   15 MESTBS76-TELGSM         PIC X(10).                        
                   15 MESTBS76-TELDOM         PIC X(10).                        
      *---            FLAG ADRESSE DE LIVRAISON                                 
                   15 MESTBS76-FADRLIVR       PIC X.                            
                   15 MESTBS76-CVOIE          PIC X(5).                         
                   15 MESTBS76-CTVOIE         PIC X(15).                        
                   15 MESTBS76-LVOIE          PIC X(21).                        
                   15 MESTBS76-LBATIMENT      PIC X(3).                         
                   15 MESTBS76-LESCALIER      PIC X(3).                         
                   15 MESTBS76-LETAGE         PIC X(3).                         
                   15 MESTBS76-LPORTE         PIC X(3).                         
                   15 MESTBS76-LCPOSTAL       PIC X(5).                         
                   15 MESTBS76-LCOMMUNE       PIC X(38).                        
                   15 MESTBS76-LCMPAD         PIC X(64).                        
                   15 MESTBS76-LCPAYS         PIC X(05).                        
      * LIGNE DE VENTE                                                          
                 10 MESTBS76-LG-VENTE OCCURS 40.                                
      * CODIC = R 12345 REMISE                                                  
      * CODIC = G 12345 GARANTIE                                                
      * CODIC = P 12345 PRESTATION                                              
      * CODIC = 1234567 CODIC                                                   
      * CODIC = 12345   PRESTATION                                              
                   15 MESTBS76-CODIC          PIC X(07).                        
                   15 MESTBS76-QTE            PIC 9(3).                         
                   15 MESTBS76-PVUNIT         PIC X(10).                        
                   15 MESTBS76-DDELIV         PIC X(08).                        
                   15 MESTBS76-CMODDEL        PIC X(03).                        
      *                                                                 00550009
      ***************************************************************** 00740000
                                                                                
