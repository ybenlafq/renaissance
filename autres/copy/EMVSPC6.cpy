      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION MOUVEMENTS SPECIFIQUE                  *
      * NOM FICHIER.: EMVSPC6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 50                                              *
      *****************************************************************
      *
       01  EMVSPC6.
      * TYPE ENREGISTREMENT : EMISSION IMAGE STANDARD
           05      EMVSPC6-TYP-ENREG      PIC  X(0006).
      * NUMERO DE MOUVEMENT
           05      EMVSPC6-NMVT           PIC  9(0009).
      * FLAG DERNIER SUPPORT DE LA LIGNE DE R�CEPTION
           05      EMVSPC6-E-DERN-LIGREC  PIC  X(0001).
      * FLAG LIGNE LITIGE OUI/ NON
           05      EMVSPC6-E-LITIGE       PIC  X(0001).
      * NATURE DE R�CEPTION
           05      EMVSPC6-NAT-RECEPTION  PIC  X(0002).
      * FILLER 30
           05      EMVSPC6-FILLER         PIC  X(0030).
      * FIN DE L'ENREGISTREMENT
           05      EMVSPC6-FIN            PIC  X(0001).
      
