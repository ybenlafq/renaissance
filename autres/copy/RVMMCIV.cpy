      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MMCIV TITRE CLIENT 1000 MERCIS         *        
      *----------------------------------------------------------------*        
       01  RVMMCIV .                                                            
           05  MMCIV-CTABLEG2    PIC X(15).                                     
           05  MMCIV-CTABLEG2-REDEF REDEFINES MMCIV-CTABLEG2.                   
               10  MMCIV-TITRE           PIC X(05).                             
           05  MMCIV-WTABLEG     PIC X(80).                                     
           05  MMCIV-WTABLEG-REDEF  REDEFINES MMCIV-WTABLEG.                    
               10  MMCIV-WFLAG           PIC X(01).                             
               10  MMCIV-COMMENT         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMMCIV-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMCIV-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MMCIV-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMCIV-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MMCIV-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
