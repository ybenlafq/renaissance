      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       ABEND-SQL-STD.                                                           
      *-----------------------------------------                                
           CALL 'DSNTIAR' USING SQLCA W-SQL-MSGS-STD                            
                                      W-SQL-LRECL-STD.                          
           IF W-SQL-MSG-STD (1) NOT = SPACES                                    
              DISPLAY W-SQL-MSG-STD (1)                                         
              IF W-SQL-MSG-STD (2) NOT = SPACES                                 
                 DISPLAY W-SQL-MSG-STD (2)                                      
                 IF W-SQL-MSG-STD (3) NOT = SPACES                              
                    DISPLAY W-SQL-MSG-STD (3)                                   
                    IF W-SQL-MSG-STD (4) NOT = SPACES                           
                       DISPLAY W-SQL-MSG-STD (4)                                
                    END-IF                                                      
                 END-IF                                                         
              END-IF                                                            
           END-IF.                                                              
           MOVE  'PROBLEME DB2            ' TO ABEND-MESS-STD.                  
           MOVE   SQLCODE                   TO ABEND-CODE-STD.                  
           PERFORM ABEND-PROGRAMME-STD.                                         
      *-----------------------------------------                                
       ABEND-PROGRAMME-STD.                                                     
      *-----------------------------------------                                
           CALL 'MW-ABEND' USING   ABEND-PROG-STD  ABEND-MESS-STD.                 
                                                                                
