      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION OP ENT�TE STANDARD (MUTATION)         * 00030000
      * NOM FICHIER.: ROESTD6                                         * 00031000
      *---------------------------------------------------------------* 00032000
      * CR   .......: 27/08/2013                                      * 00033000
      * MODIFIE.....:   /  /                                          * 00034000
      * VERSION N�..: 001                                             * 00035000
      * LONGUEUR....: 1043                                            * 00036000
      ***************************************************************** 00037000
      *                                                                 00038000
       01  ROESTD6.                                                     00039000
      * TYPE ENREGISTREMENT                                             00040000
           05      ROESTD6-TYP-ENREG      PIC  X(0006).                 00050000
      * CODE SOCI�T�                                                    00060000
           05      ROESTD6-CSOCIETE       PIC  X(0005).                 00070000
      * NUM�RO D OP                                                     00080000
           05      ROESTD6-NOP            PIC  X(0015).                 00090000
      * NB DE LIGNES DE L OP                                            00100000
           05      ROESTD6-NBLIG-OP       PIC  9(0005).                 00110000
      * TYPE D OP                                                       00111000
           05      ROESTD6-TYPE-OP        PIC  9(0002).                 00112000
      * CODE CLIENT LIVR�                                               00113000
           05      ROESTD6-CCLIENT        PIC  X(0013).                 00114000
      * ADRESSE DE LIVRAISON                                            00115000
           05      ROESTD6-ADR-LIVRAISON.                               00116000
      * RAISON SOCIALE - ADRESSE DE LIVRAISON                           00117000
              10   ROESTD6-RAISOC-LIVR    PIC  X(0035).                 00118000
      * ADRESSE 1- ADRESSE DE LIVRAISON                                 00119000
              10   ROESTD6-ADR1-LIVR      PIC  X(0035).                 00120000
      * ADRESSE 2 - ADRESSE DE LIVRAISON                                00130000
              10   ROESTD6-ADR2-LIVR      PIC  X(0035).                 00140000
      * ADRESSE 3 - ADRESSE DE LIVRAISON                                00150000
              10   ROESTD6-ADR3-LIVR      PIC  X(0035).                 00160000
      * ADRESSE 4 - ADRESSE DE LIVRAISON                                00161000
              10   ROESTD6-ADR4-LIVR      PIC  X(0035).                 00162000
      * CODE POSTAL - ADRESSE DE LIVRAISON                              00162100
              10   ROESTD6-CPOSTAL-LIVR   PIC  X(0009).                 00162200
      *  VILLE - ADRESSE DE LIVRAISON                                   00162300
              10   ROESTD6-VILLE-LIVR     PIC  X(0035).                 00162400
      *  CODE PAYS - ADRESSE DE LIVRAISON                               00162500
              10   ROESTD6-CPAYS-LIVR     PIC  X(0003).                 00162600
      * LIBELL� PAYS LIVRAISON                                          00162700
              10   ROESTD6-LPAYS-LIVR     PIC  X(0035).                 00162800
      * ADRESSE DU CLIENT EXP�DI�                                       00162900
           05      ROESTD6-ADR-CLI-EXP.                                 00163000
      * RAISON SOCIALE - ADRESSE DU CLIENT EXP�DI�                      00164000
              10   ROESTD6-RAISOC-CLI-EXP PIC  X(0035).                 00165000
      * ADRESSE 1 - ADRESSE DU CLIENT EXP�DI�                           00166000
              10   ROESTD6-ADR1-CLI-EXP   PIC  X(0035).                 00167000
      * ADRESSE 2 - ADRESSE DU CLIENT EXP�DI�                           00167100
              10   ROESTD6-ADR2-CLI-EXP   PIC  X(0035).                 00167200
      * ADRESSE 3 - ADRESSE DU CLIENT EXP�DI�                           00167300
              10   ROESTD6-ADR3-CLI-EXP   PIC  X(0035).                 00167400
      * ADRESSE 4 - ADRESSE DU CLIENT EXP�DI�                           00167500
              10   ROESTD6-ADR4-CLI-EXP   PIC  X(0035).                 00167600
      * CODE POSTAL - ADRESSE DU CLIENT EXP�DI�                         00167700
              10   ROESTD6-CPOSTAL-CLI-EXP PIC X(0009).                 00167800
      * VILLE - ADRESSE DU CLIENT EXP�DI�                               00167900
              10   ROESTD6-VILLE-CLI-EXP  PIC  X(0035).                 00168000
      * CODE PAYS - ADRESSE DU CLIENT EXP�DI�                           00168100
              10   ROESTD6-CPAYS-CLI-EXP  PIC  X(0003).                 00168200
      * LIBELL� PAYS - ADRESSE DU CLIENT EXP�DI�                        00168300
              10   ROESTD6-LPAYS-CLI-EXP  PIC  X(0035).                 00168400
      * ADRESSE FACTURATION                                             00168500
           05      ROESTD6-ADR-FACTURATION.                             00168600
      * RAISON SOCIALE - ADRESSE FACTURATION                            00168700
              10   ROESTD6-RAISOC-FACT    PIC  X(0035).                 00168800
      * ADRESSE 1 - ADRESSE FACTURATION                                 00168900
              10   ROESTD6-ADR1-FACT      PIC  X(0035).                 00169000
      * ADRESSE 2 - ADRESSE FACTURATION                                 00169100
              10   ROESTD6-ADR2-FACT      PIC  X(0035).                 00169200
      * ADRESSE 3 - ADRESSE FACTURATION                                 00169300
              10   ROESTD6-ADR3-FACT      PIC  X(0035).                 00169400
      * ADRESSE 4 - ADRESSE FACTURATION                                 00169500
              10   ROESTD6-ADR4-FACT      PIC  X(0035).                 00169600
      * CODE POSTAL - ADRESSE FACTURATION                               00169700
              10   ROESTD6-CPOSTAL-FACT   PIC  X(0009).                 00169800
      * VILLE - ADRESSE FACTURATION                                     00169900
              10   ROESTD6-VILLE-FACT     PIC  X(0035).                 00170000
      * CODE PAYS - ADRESSE FACTURATION                                 00170100
              10   ROESTD6-CPAYS-FACT     PIC  X(0003).                 00170200
      * LIBELL� PAYS - ADRESSE FACTURATION                              00170300
              10   ROESTD6-LPAYS-FACT     PIC  X(0035).                 00170400
      * ZONE G�OGRAPHIQUE                                               00170500
           05      ROESTD6-ZONE-GEO       PIC  X(0020).                 00170600
      * CODE TRANSPORTEUR                                               00170700
           05      ROESTD6-CTRANSPORTEUR  PIC  X(0013).                 00170800
      * MODE DE TRANSPORT                                               00170900
           05      ROESTD6-MODE-TRANSPORT PIC  X(0003).                 00171000
      * MODE EXP�DITION                                                 00171100
           05      ROESTD6-MODE-EXPED     PIC  X(0003).                 00171200
      * � PALETTISER                                                    00171300
           05      ROESTD6-A-PALETTISER   PIC  9(0001).                 00171400
      * EMPLACEMENT D EXP�DITION                                        00171500
           05      ROESTD6-EMPL-EXPED     PIC  X(0020).                 00171600
      * DATE DE LIVRAISON SOUHAIT�E                                     00171700
           05      ROESTD6-DLIVRAISON     PIC  X(0008).                 00171800
      * DATE DE PRISE DE COMMANDE                                       00171900
           05      ROESTD6-DPRISE-CDE     PIC  X(0008).                 00172000
      * HEURE DE PRISE DE COMMANDE                                      00172100
           05      ROESTD6-HPRISE-CDE     PIC  X(0006).                 00172200
      * MONOR�F�RENCE                                                   00172300
           05      ROESTD6-MONOREF        PIC  9(0001).                 00172400
      * URGENCE                                                         00172500
           05      ROESTD6-URGENCE        PIC  9(0001).                 00172600
      * CONTRE-REMBOURSEMENT                                            00172700
           05      ROESTD6-CONTRE-REMB    PIC  9(0001).                 00172800
      * LANGUE                                                          00172900
           05      ROESTD6-LANGUE         PIC  X(0003).                 00173000
      * NUM�RO DE COMMANDE CLIENT                                       00173100
           05      ROESTD6-NCDE-CLIENT    PIC  X(0015).                 00173200
      * NUM�RO DE COMMANDE N3                                           00173300
           05      ROESTD6-NCDE-N3        PIC  X(0015).                 00173400
      * NUM�RO CONTRAINTE COLISAGE                                      00173500
           05      ROESTD6-NCONTRAINTE-COL PIC 9(0003).                 00173600
      * NOMBRE DE BON DE LIVRAISON                                      00173700
           05      ROESTD6-NB-BL          PIC  9(0002).                 00173800
      * NOMBRE DE LISTE DE COLISAGE                                     00173900
           05      ROESTD6-NB-LST-COLISAGE PIC 9(0002).                 00174000
      * COLISAGE AUTOMATIQUE                                            00174100
           05      ROESTD6-COLISAGE-AUTOM PIC  9(0001).                 00174200
      * POIDS LIMITE COLIS                                              00174300
           05      ROESTD6-POIDS-LIM-COLIS PIC 9(0009).                 00174400
      * CARACT�RISTIQUE 1                                               00174500
           05      ROESTD6-CARACT1        PIC  X(0020).                 00174600
      * CARACT�RISTIQUE 2                                               00174700
           05      ROESTD6-CARACT2        PIC  X(0020).                 00174800
      * CARACT�RISTIQUE 3                                               00174900
           05      ROESTD6-CARACT3        PIC  X(0020).                 00175000
      * FILLER 30                                                       00175100
           05      ROESTD6-FILLER         PIC  X(0030).                 00175200
      * FIN                                                             00175300
           05      ROESTD6-FIN            PIC  X(0001).                 00175400
                                                                                
