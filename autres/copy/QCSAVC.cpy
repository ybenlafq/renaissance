      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************          
      * DSECT DU FICHIER FQCSAV                                      *          
      * FICHIER REMONTE DU 36 : INTERVENTIONS SAV TLM                *          
      * APPLICATION CARTES QUALITE                                   *          
      * 16/09/96 : TELDOM X(8) -> X(10)                              *          
      *            NUDOS  X(9) -> X(10)                              *          
      * LONGUEUR : 221                                               *          
      * MODIF : LE 14/09/05 : AJOUT CTYPSERV                         *          
      *         LONGUEUR ==> 226                                     *          
CG2907* PASSE A LONGUEUR ==> 231                                     *          
PA1012* PASSE A LONGUEUR ==> 250                                     *          
PA0413* PASSE A LONGUEUR ==> 300                                     *          
      ****************************************************************          
       01 FQCSAV-ENREG.                                                         
          04 FQCSAV-CTCARTE          PIC X.                                     
          04 FQCSAV-NSOCIETE         PIC 9(3).                                  
          04 FQCSAV-NLIEU            PIC 9(3).                                  
          04 FQCSAV-NUDOS            PIC X(10).                                 
          04 FQCSAV-CFAM             PIC X(5).                                  
          04 FQCSAV-CMARQ            PIC X(5).                                  
          04 FQCSAV-LREF             PIC X(10).                                 
          04 FQCSAV-CDINTH           PIC X(3).                                  
          04 FQCSAV-CDINTT           PIC X(3).                                  
          04 FQCSAV-REGGAR           PIC X(3).                                  
          04 FQCSAV-CPANNE           PIC X(2).                                  
          04 FQCSAV-DELAI            PIC 9(3).                                  
          04 FQCSAV-CTITRE           PIC X(5).                                  
          04 FQCSAV-LNOM             PIC X(25).                                 
          04 FQCSAV-LPRENOM          PIC X(15).                                 
          04 FQCSAV-LADR1            PIC X(32).                                 
          04 FQCSAV-LADR2            PIC X(32).                                 
          04 FQCSAV-CPOSTAL          PIC X(5).                                  
          04 FQCSAV-LCOMMUNE         PIC X(32).                                 
          04 FQCSAV-TELDOM           PIC X(10).                                 
          04 FQCSAV-DTERM            PIC 9(8).                                  
          04 FQCSAV-CDSOA            PIC X(6).                                  
          04 FQCSAV-CTYPSERV         PIC X(5).                                  
          04 FQCSAV-CENREG           PIC X(5).                                  
          04 FQCSAV-TOP-MAIL         PIC X(01).                                 
          02  FILLER                PIC X(09).                                  
          02  FQCSAV-WEMPORTE       PIC X(1).                                   
          02  FQCSAV-PVTOTAL        PIC S9(7)V9(2) USAGE COMP-3.                
          02  FQCSAV-NSEQNQ         PIC S9(5)V USAGE COMP-3.                    
          02  FQCSAV-EMAIL          PIC X(50).                                  
          02  FQCSAV-NGSM           PIC X(10).                                  
          02  FQCSAV-IDCLIENT       PIC X(08).                                  
          02 FILLER                 PIC X(32).                                  
                                                                                
