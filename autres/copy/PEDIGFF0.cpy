      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      ************************************************** EXEC DLI ******        
      * PREPARATION DE L'ACCES AU SEGMENT DIGSFC PAR LE PATH DIGFF0             
      ******************************************************************        
      *                                                                         
       CLEF-DIGFF0             SECTION.                                         
      *                                                                         
      ************************************* PREPARATION CLEF-1                  
      *                                                                         
           MOVE 'DIGSF'  TO SEG-NAME-1.                                         
           MOVE 'DIGFF0'  TO KEY-NAME-1.                                        
           MOVE '64'  TO SEG-LONG-1.                                            
           MOVE '4'  TO KEY-LONG-1.                                             
           MOVE DIGSF-DIGFF0                                                    
                         TO KEY-VAL-1.                                          
      *                                                                         
           MOVE 'DIGFF0'  TO PATH-NAME.                                         
      *                                                                         
           IF PSB-NOT-OK THEN                                                   
                         PERFORM SCHEDULING-EXEC                                
           END-IF.                                                              
      *                                                                         
       FIN-CLEF-DIGFF0.    EXIT.                                                
                EJECT                                                           
                                                                                
EMOD                                                                            
