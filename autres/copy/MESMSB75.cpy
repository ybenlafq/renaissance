      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      *   COPY MESSAGE MQ REPONSE                                               
      *   VERS SIEBEL                                                           
      *****************************************************************         
      *                                                                         
      *                                                                         
       01 MESMSB75-DATA.                                                        
      *                                                                         
      *                                                                         
      *   CODE RETOUR : 0->KO, 1->OK                                            
          05 MESMSB75-CRETOUR      PIC    X(01).                                
      *   CODIC                                                                 
          05 MESMSB75-LRETOUR      PIC    X(30).                                
      *   CODE EAN                                                              
          05 MESMSB75-IDSIEBEL     PIC    X(30).                                
      *   N� D ORDRE                                                            
          05 MESMSB75-NORDRE       PIC    X(05).                                
      *   SOCIETE                                                               
          05 MESMSB75-NSOCIETE     PIC    X(03).                                
      *   LIEU                                                                  
          05 MESMSB75-NLIEU        PIC    X(03).                                
      *   DVENTE                                                                
          05 MESMSB75-DVENTE       PIC    X(10).                                
                                                                                
