      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
000100 01  COMM-MTL0-APPLI.                                                     
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000200*    02  COMM-MTL0-CODRET          PIC S9(0004) COMP.                     
      *--                                                                       
           02  COMM-MTL0-CODRET          PIC S9(0004) COMP-5.                   
      *}                                                                        
000300     02  COMM-MTL0-CMDE            PIC  X(0009).                          
000400     02  COMM-MTL0-WRO             PIC  X(0001).                          
000500     02  COMM-MTL0-DLIVR           PIC  X(0008).                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
000600*    02  COMM-MTL0-SQLCODE         PIC S9(0009) COMP.                     
      *--                                                                       
           02  COMM-MTL0-SQLCODE         PIC S9(0009) COMP-5.                   
      *}                                                                        
000700     02  COMM-MTL0-TABNAME         PIC  X(0008).                          
000800     02  FILLER                    PIC  X(4064).                          
                                                                                
