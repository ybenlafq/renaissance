      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                 00000010
      *-----------------------------------------------------------------00000020
      * TSLP08   :        PROGRAMMES BLP125 ET MLP008                   00000030
      *-----------------------------------------------------------------00000050
      *                                                                 00000060
      *                                                                 00000080
       01  STRUCT-TS-LINK.                                              00000090
           05  TS-NSOCENTRE-LINK       PIC  X(03).                      00000110
           05  TS-NDEPOT-LINK          PIC  X(03).                      00000120
           05  TS-NMUTATION-LINK       PIC  X(07).                      00000100
           05  TS-NSOCIETE-LINK        PIC  X(03).                      00000110
           05  TS-NLIEU-LINK           PIC  X(03).                      00000120
           05  TS-CSELART-LINK         PIC  X(05).                      00000130
           05  TS-DMUTATION-LINK       PIC  X(08).                      00000140
           05  TS-DATEJOUR             PIC  X(08).                      00000140
           05  TS-DATE-MIN-M           PIC  X(08).                      00000140
           05  TS-DATE-MIN-S           PIC  X(08).                      00000140
           05  TS-CODRET               PIC  X.                          00000140
           05  TS-MESS                 PIC  X(80).                      00000140
      *                                                                 00000150
                                                                                
