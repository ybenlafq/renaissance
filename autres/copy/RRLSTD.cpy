      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION R�CEPTION LIGNE STANDARD (MUTATION)   * 00031000
      * NOM FICHIER.: RRLSTD                                          * 00032003
      *---------------------------------------------------------------* 00034000
      * CR   .......: 12/10/2011  17:46:00                            * 00035000
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR....: 742                                             * 00037100
      ***************************************************************** 00037300
      *                                                                 00037400
       01  RRLSTD.                                                      00037500
      * TYPE ENREGISTREMENT                                             00037605
           05      RRLSTD-TYP-ENREG       PIC  X(0006).                 00037700
      * CODE ACTION                                                     00037805
           05      RRLSTD-CACTION         PIC  X(0001).                 00037901
      * CODE SOCI�T�                                                    00038005
           05      RRLSTD-CSOCIETE        PIC  X(0005).                 00038101
      * NUM�RO DE DOSSIER DE R�CEPTION                                  00038205
           05      RRLSTD-NDOS-REC        PIC  X(0015).                 00038301
      * NUM�RO DE LIGNE DE R�CEPTION                                    00038405
           05      RRLSTD-NLIGNE-REC      PIC  9(0005).                 00038501
      * NUM�RO DE SOUS-LIGNE DE R�CEPTION                               00038605
           05      RRLSTD-NSSLIGNE-REC    PIC  9(0005).                 00039001
      * NUM�RO DE COMMANDE N3                                           00039105
           05      RRLSTD-NCDE-N3         PIC  X(0015).                 00040001
      * FABRICATION                                                     00041005
           05      RRLSTD-FABRICATION     PIC  9(0001).                 00050000
      * ADRESSE FABRICATION                                             00051005
           05      RRLSTD-ADR-FABRICATION PIC  X(0020).                 00060000
      * TYPE DE LIGNE DE R�CEPTION                                      00061005
           05      RRLSTD-TYPE-LIG-REC    PIC  9(0002).                 00070000
      * NUM�RO DE SITE                                                  00071005
           05      RRLSTD-NO-SITE         PIC  9(0003).                 00080000
      * CODE ARTICLE                                                    00081005
           05      RRLSTD-ARTICLE         PIC  X(0018).                 00090000
      * NUM�RO DE LOT                                                   00091005
           05      RRLSTD-NLOT            PIC  X(0015).                 00100001
      * NUM�RO DE CARTON                                                00101005
           05      RRLSTD-NCARTON         PIC  X(0018).                 00110001
      * DATE LIMITE DE VENTE                                            00111005
           05      RRLSTD-DLV             PIC  X(0008).                 00120000
      * DATE LIMITE DE CONSOMMATION                                     00120105
           05      RRLSTD-DLC             PIC  X(0008).                 00120200
      * INFORMATION 1                                                   00120305
           05      RRLSTD-INFO1           PIC  X(0010).                 00121000
      * INFORMATION 2                                                   00121105
           05      RRLSTD-INFO2           PIC  X(0010).                 00121200
      * INFORMATION 3                                                   00121305
           05      RRLSTD-INFO3           PIC  X(0010).                 00121400
      * QUANTIT� PR�VUE                                                 00121505
           05      RRLSTD-QTE-PREVUE      PIC  9(0009).                 00122000
      * QUANTIT� PR�VUE MAXIMUM                                         00122105
           05      RRLSTD-QTE-PREVUE-MAX  PIC  9(0009).                 00122200
      * CODE FOURNISSEUR                                                00122305
           05      RRLSTD-CFOURNISSEUR    PIC  X(0015).                 00123000
      * LIBELL� FOURNISSEUR                                             00123105
           05      RRLSTD-LFOURNISSEUR    PIC  X(0035).                 00123200
      * ORIGINE                                                         00123305
           05      RRLSTD-ORIGINE         PIC  X(0010).                 00124000
      * PROPRI�TAIRE                                                    00124105
           05      RRLSTD-PROPRIETAIRE    PIC  X(0020).                 00125000
      * �TAT BLOCAGE                                                    00125105
           05      RRLSTD-ETAT-BLOCAGE    PIC  9(0003).                 00126000
      * �TAT STOCK N3                                                   00126105
           05      RRLSTD-ETAT-STOCK-N3   PIC  9(0003).                 00126200
      * R�SERVATION                                                     00126305
           05      RRLSTD-RESERVATION     PIC  X(0015).                 00126400
      * DESCRIPTION SUPPORT                                             00126505
           05      RRLSTD-DESCR-SUPPORT   PIC  9(0001).                 00126600
      * NUM�RO SUPPORT FOURNISSEUR                                      00126705
           05      RRLSTD-NSUPPORT-FOURN  PIC  X(0018).                 00126802
      * CODE UNIT� LOGISTIQUE                                           00126905
           05      RRLSTD-CODE-UL         PIC  X(0005).                 00127000
      * ADRESSE DE STOCKAGE                                             00127105
           05      RRLSTD-ADR-STOCKAGE    PIC  X(0020).                 00127200
      * HAUTEUR                                                         00127305
           05      RRLSTD-HAUTEUR         PIC  9(0009).                 00127400
      * CODE EMBALLAGE                                                  00127505
           05      RRLSTD-CEMBALLAGE      PIC  X(0010).                 00127600
      * POIDS                                                           00127705
           05      RRLSTD-POIDS           PIC  9(0009).                 00127800
      * DATE DE LIVRAISON PR�VUE                                        00127905
           05      RRLSTD-DLIVRAISON      PIC  X(0008).                 00128000
      * DATE DE COMMANDE                                                00128105
           05      RRLSTD-DCDE            PIC  X(0008).                 00128200
      * DATE DE FABRICATION                                             00128305
           05      RRLSTD-DFABRICATION    PIC  X(0008).                 00128400
      * NIVEAU DE CONTR�LE                                              00128505
           05      RRLSTD-NIV-CTRL        PIC  9(0001).                 00128600
      * COMMENTAIRE                                                     00128705
           05      RRLSTD-COMMENTAIRE     PIC  X(0035).                 00128800
      * INFORMATION COMPL�MENTAIRE                                      00128905
           05      RRLSTD-INFO-CMPL       PIC  X(0255).                 00129002
      * CARACT�RISTIQUE 1                                               00130005
           05      RRLSTD-CARACT1         PIC  X(0020).                 00139000
      * CARACT�RISTIQUE 2                                               00139105
           05      RRLSTD-CARACT2         PIC  X(0020).                 00140000
      * CARACT�RISTIQUE 3                                               00141005
           05      RRLSTD-CARACT3         PIC  X(0020).                 00150000
      * FILLER                                                          00151005
           05      RRLSTD-FIN             PIC  X(0001).                 00160000
                                                                                
