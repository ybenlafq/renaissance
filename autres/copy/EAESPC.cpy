      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION ARTICLE ENTETE SPECIFIQUE              *
      * NOM FICHIER.: EAESPC                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 162                                             *
      *****************************************************************
      *
       01  EAESPC.
      * TYPE ENREGISTREMENT
           05      EAESPC-TYP-ENREG       PIC  X(0006).
      * CODE SOCIETE
           05      EAESPC-CSOCIETE        PIC  X(0005).
      * CODE ARTICLE
           05      EAESPC-CARTICLE        PIC  X(0018).
      * CODE FAMILLE NCG
           05      EAESPC-CFAM            PIC  X(0005).
      * CODE MARQUE NCG
           05      EAESPC-CMARQ           PIC  X(0005).
      * DACEM O/N
           05      EAESPC-WDACEM          PIC  X(0001).
      * TYPE COLISAGE DESTOCK A OU I
           05      EAESPC-CFETEMPL        PIC  X(0001).
      * COLISAGE DE RECEPTION
           05      EAESPC-QCOLIRECEPT     PIC  9(0005).
      * COLISAGE DE DESTOCKAGE
           05      EAESPC-QCOLIDSTOCK     PIC  9(0005).
      * COTE DE PREHENSION
           05      EAESPC-CCOTEHOLD       PIC  X(0001).
      * NOMBRE DE PIECES PAR FRONT
           05      EAESPC-QNBRANMAIL      PIC  9(0003).
      * NOMBRE DE NIVEAUX DE GERBAGE
           05      EAESPC-QNBNIVGERB      PIC  9(0003).
      * SPECIFICITE DE STOCKAGE
           05      EAESPC-CSPECIFSTK      PIC  X(0001).
      * NOMBRE DE PRODUITS PAR PINCEES STOCKAGE
           05      EAESPC-QNBPVSOL-STK    PIC  9(0005).
      * NOMBRE DE PRODUITS PAR PINCEES PREPARATION
           05      EAESPC-QNBPVSOL-PREP   PIC  9(0005).
      * MODE DE STOCKAGE (S OU R : SOL OU RACK)
           05      EAESPC-CMODSTOCK       PIC  X(0001).
      * DATE PREMIERE RECEPTION
           05      EAESPC-D1RECEPT        PIC  X(0008).
      * STATUT COMPACTE
           05      EAESPC-LSTATCOMP       PIC  X(0003).
      * CODE QUOTA DE RECEPTION
           05      EAESPC-CQUOTA          PIC  X(0005).
      * CODE APPROVISIONNEMENT
           05      EAESPC-CAPPRO          PIC  X(0005).
      * CODE FOURNISSEUR REGULIER
           05      EAESPC-NENTCDE         PIC  X(0005).
      * CODE EAN 2
           05      EAESPC-EAN2            PIC  X(0013).
      * CODE EAN 3
           05      EAESPC-EAN3            PIC  X(0013).
      * CODE EAN 4
           05      EAESPC-EAN4            PIC  X(0013).
      * CODE EAN 5
           05      EAESPC-EAN5            PIC  X(0013).
      * CODE EAN 6
           05      EAESPC-EAN6            PIC  X(0013).
      * FILLER
           05      EAESPC-FILLER          PIC  X(0001).
      
