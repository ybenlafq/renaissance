      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:06 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION OP LIGNE STANDARD (MUTATION)           *
      * NOM FICHIER.: EOLSTD6                                         *
      *---------------------------------------------------------------*
      * CR   .......: 27/08/2013                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 95                                              *
      *****************************************************************
      *
       01  EOLSTD6.
      * TYPE ENREGISTREMENT
           05      EOLSTD6-TYP-ENREG      PIC  X(0006).
      * CODE SOCIETE
           05      EOLSTD6-CSOCIETE       PIC  X(0005).
      * NUMERO D OP
           05      EOLSTD6-NOP            PIC  X(0015).
      * NUMERO DE LIGNE
           05      EOLSTD6-NLIGNE         PIC  9(0005).
      * NUMERO DE SOUS-LIGNE
           05      EOLSTD6-NSSLIGNE       PIC  9(0005).
      * TYPE DE LIGNE
           05      EOLSTD6-TYPE-LIGNE     PIC  9(0001).
      * CODE ARTICLE
           05      EOLSTD6-CARTICLE       PIC  X(0018).
      * QUANTITE PREPAREE
           05      EOLSTD6-QTE-PREP       PIC  X(0009).
      * FILLER 30
           05      EOLSTD6-FILLER         PIC  X(0030).
      * CODE FIN
           05      EOLSTD6-FIN            PIC  X(0001).
      
