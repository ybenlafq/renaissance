      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION OP LIGNE STANDARD (MUTATION)          * 00030000
      * NOM FICHIER.: ROLSTD6                                         * 00031000
      *---------------------------------------------------------------* 00032000
      * CR   .......: 27/08/2013                                      * 00033000
      * MODIFIE.....:   /  /                                          * 00034000
      * VERSION N�..: 001                                             * 00035000
      * LONGUEUR....: 303                                             * 00036000
      ***************************************************************** 00037000
      *                                                                 00037100
       01  ROLSTD6.                                                     00037200
      * TYPE ENREGISTREMENT                                             00037300
           05      ROLSTD6-TYP-ENREG      PIC  X(0006).                 00037400
      * CODE SOCI�T�                                                    00037500
           05      ROLSTD6-CSOCIETE       PIC  X(0005).                 00037600
      * NUM�RO D OP                                                     00037700
           05      ROLSTD6-NOP            PIC  X(0015).                 00037800
      * NUM�RO DE LIGNE                                                 00037900
           05      ROLSTD6-NLIGNE         PIC  9(0005).                 00038000
      * NUM�RO DE SOUS-LIGNE                                            00038100
           05      ROLSTD6-NSSLIGNE       PIC  9(0005).                 00038200
      * TYPE DE LIGNE                                                   00038300
           05      ROLSTD6-TYPE-LIGNE     PIC  9(0001).                 00038400
      * CODE ARTICLE                                                    00038500
           05      ROLSTD6-CARTICLE       PIC  X(0018).                 00038600
      * LOT IMPOS�                                                      00038700
           05      ROLSTD6-LOT-IMPOSE     PIC  X(0015).                 00038800
      * LIBELL� ARTICLE                                                 00038900
           05      ROLSTD6-LARTICLE       PIC  X(0035).                 00039000
      * CODE ARTICLE CLIENT                                             00040000
           05      ROLSTD6-CARTICLE-CLIENT PIC X(0018).                 00050000
      * QUANTIT� � PR�LEVER                                             00060000
           05      ROLSTD6-QTE-A-PRLV     PIC  9(0009).                 00070000
      * QUANTIT� COMMAND�E                                              00080000
           05      ROLSTD6-QTE-CDEE       PIC  9(0009).                 00090000
      * QUANTIT� RESTANTE                                               00100000
           05      ROLSTD6-QTE-RESTANTE   PIC  9(0009).                 00110000
      * MONTANT DU CONTRE-REMBOURSEMENT                                 00120000
           05      ROLSTD6-MT-CONTRE-REMB PIC  9(0015).                 00121000
      * IDENTIFIANT FAMILLE DE COLISAGE                                 00121100
           05      ROLSTD6-ID-FAM-COLISAGE PIC X(0005).                 00121200
      * COLISAGE KIT                                                    00121300
           05      ROLSTD6-COLISAGE-KIT   PIC  9(0003).                 00121400
      * SORTIE SP�CIALE                                                 00121500
           05      ROLSTD6-SORTIE-SPECIALE PIC 9(0001).                 00121600
      * �TAT BLOCAGE                                                    00121700
           05      ROLSTD6-ETAT-BLOCAGE   PIC  9(0003).                 00121800
      * DLV                                                             00121900
           05      ROLSTD6-DLV            PIC  9(0005).                 00122000
      * NUM�RO DE COMMANDE CLIENT                                       00123000
           05      ROLSTD6-NCDE-CLIENT    PIC  X(0015).                 00124000
      * NUM�RO DE COMMANDE NIVEAU3                                      00125000
           05      ROLSTD6-NCDE-CLIENT-N3 PIC  X(0015).                 00126000
      * CARACT�RISTIQUE 1                                               00127000
           05      ROLSTD6-CARACT1        PIC  X(0020).                 00128000
      * CARACT�RISTIQUE 2                                               00129000
           05      ROLSTD6-CARACT2        PIC  X(0020).                 00130000
      * CARACT�RISTIQUE 3                                               00140000
           05      ROLSTD6-CARACT3        PIC  X(0020).                 00150000
      * FILLER 30                                                       00160000
           05      ROLSTD6-FILLER         PIC  X(0030).                 00170000
      * FIN                                                             00170100
           05      ROLSTD6-FIN            PIC  X(0001).                 00170200
                                                                                
