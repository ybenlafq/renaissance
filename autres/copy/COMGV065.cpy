      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * -------------------------------------------------------------- *        
      * PROJET     : GESTION DES VENTES                                *        
      * TITRE      : COMMAREA D'APPEL DU BSL300                        *        
      *                                                                *        
      * -------------------------------------------------------------- *        
        01 COMM-GV65-APPLI.                                                     
           05 COMM-GV65-ZIN.                                                    
      * -> CONTROLE ET AUTRES                                                   
               10 COMM-GV65-NSOCIETE         PIC X(03).                         
               10 COMM-GV65-NLIEU            PIC X(03).                         
               10 COMM-GV65-NVENTE           PIC X(07).                         
               10 COMM-GV65-NSEQNQ           PIC S9(5) COMP-3.                  
               10 COMM-GV65-NCODIC           PIC X(07).                         
               10 COMM-GV65-WCQERESF         PIC X(01).                         
                  88 COMM-GV65-CMD-FNR                 VALUE 'F'.               
               10 COMM-GV65-DDELIV           PIC X(08).                         
               10 COMM-GV65-DELAI-SL10       PIC X(08).                         
               10 COMM-GV65-SSAAMMJJ         PIC X(08).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10 COMM-GV65-QTE              PIC S9(06) COMP VALUE 0.           
      *--                                                                       
               10 COMM-GV65-QTE              PIC S9(06) COMP-5 VALUE 0.         
      *}                                                                        
               10 COMM-GV65-TYPE-VENTE       PIC  X(01).                        
                  88 VENTE-INDETERMINE       VALUE '0'.                         
                  88 VENTE-B2B               VALUE '1'.                         
                  88 VENTE-INTERNET          VALUE '2'.                         
                  88 VENTE-MAGASIN           VALUE '3'.                         
               10 COMM-GV65-NUM-DACEM        PIC  X(06).                        
               10 COMM-GV65-B2B-MUT-MAG      PIC  X(01).                        
                  88 COMM-B2B-MUT-MAG-NON    VALUE '0'.                         
                  88 COMM-B2B-MUT-MAG-OUI    VALUE '1'.                         
               10 COMM-GV65-LOG-DACEM-B2B    PIC  X(01).                        
                  88 COMM-B2B-LOG-DACEM-NON  VALUE '0'.                         
                  88 COMM-B2B-LOG-DACEM-OUI  VALUE '1'.                         
               10 COMM-GV65-B2B-DACEM        PIC  X(01).                        
                  88 COMM-B2B-DACEM-NON      VALUE '0'.                         
                  88 COMM-B2B-DACEM-OUI      VALUE '1'.                         
           05 COMM-GV65-ZMUTATION.                                              
               10 COMM-GV65-MUTATION   OCCURS 100.                              
                  15 COMM-GV65-CTRL.                                            
                     20 COMM-GV65-CTRL12.                                       
                        25 COMM-GV65-CTRL1    PIC X(01).                        
                        25 COMM-GV65-CTRL2    PIC X(01).                        
                     20 FILLER                PIC X(02).                        
                  15 COMM-GV65-PARAM.                                           
                     20 COMM-GV65-SOC         PIC X(03).                        
                     20 COMM-GV65-LIEU.                                         
                        25 COMM-GV65-CTENVOI  PIC X(01).                        
                        25 FILLER             PIC X(02).                        
                     20 FILLER                PIC X(04).                        
                  15 COMM-GV65-LIBELLE.                                         
                     20 COMM-GV65-MUT         PIC X(07).                        
                     20 FILLER                PIC X(05).                        
                     20 FILLER     OCCURS 7.                                    
                        25 COMM-WJOURMUTAUTOR   PIC X.                          
                     20 FILLER                PIC X(11).                        
      * -> RESERVATION DACEM OU DARTY                                           
               10 COMM-GV65-TYPCDE           PIC X(05).                         
                  88 COMM-GV65-CDEDARTY                 VALUE 'DARTY'.          
                  88 COMM-GV65-CDEDACEM                 VALUE 'DACEM'.          
      * -> PARAM�TRES SP�CIAUX                                                  
               10 COMM-GV65-TYPMUT          PIC X(01).                          
                  88 COMM-GV65-MUT-UNIQUE               VALUE 'U'.              
                  88 COMM-GV65-MUT-MULTI                VALUE 'M'.              
               10 COMM-GV65-WEMPORTE        PIC X(01).                          
V43R0          10 COMM-GV65-TOPDEJAMUTE     PIC 9(01).                          
                  88 COMM-TRAIT-DEJAMUTE-NON                 VALUE 0.           
                  88 COMM-TRAIT-DEJAMUTE-OUI                 VALUE 1.           
 "             10 COMM-GV65-NMUTCOMM        PIC X(07).                          
               10 FILLER                    PIC X(73).                          
      * ->                                                                      
           05 COMM-GV65-ZOUT.                                                   
               10 COMM-GV65-CODRET           PIC X(04).                         
               10 COMM-GV65-PB-400           PIC X(03).                         
               10 COMM-GV65-LMESSAGE         PIC X(53).                         
               10 COMM-GV65-NMUTATION        PIC X(07).                         
               10 COMM-GV65-LIEU-STOCK.                                         
                  15 COMM-GV65-NSOCDEPOT     PIC X(03).                         
                  15 COMM-GV65-NDEPOT        PIC X(03).                         
               10 COMM-GV65-ETAT-RESERVATION PIC 9(01).                         
                  88 COMM-GV65-AUCUN-STOCK-DISPO       VALUE 0.                 
                  88 COMM-GV65-STOCK-DISPONIBLE        VALUE 1.                 
                  88 COMM-GV65-STOCK-DACEM             VALUE 2.                 
               10 COMM-GV65-ETAT-FOURNISSEUR PIC 9(01).                         
                  88 COMM-GV65-STOCK-FOURN-NON         VALUE 0.                 
                  88 COMM-GV65-STOCK-FOURN-OUI         VALUE 1.                 
               10 COMM-GV65-FILIERE          PIC X(002).                        
               10 FILLER                     PIC X(096).                        
                                                                                
