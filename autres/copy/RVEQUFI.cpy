      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQUFI PARAM. FILIALE MONOEQUIPAGE      *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUFI .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUFI .                                                            
      *}                                                                        
           05  EQUFI-CTABLEG2    PIC X(15).                                     
           05  EQUFI-CTABLEG2-REDEF REDEFINES EQUFI-CTABLEG2.                   
               10  EQUFI-NSOCLIEU        PIC X(06).                             
           05  EQUFI-WTABLEG     PIC X(80).                                     
           05  EQUFI-WTABLEG-REDEF  REDEFINES EQUFI-WTABLEG.                    
               10  EQUFI-POIDS1          PIC S9(07)       COMP-3.               
               10  EQUFI-POIDS2          PIC S9(07)       COMP-3.               
               10  EQUFI-DIM             PIC S9(07)       COMP-3.               
               10  EQUFI-HTEUR           PIC S9(07)       COMP-3.               
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUFI-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUFI-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUFI-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQUFI-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUFI-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQUFI-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
