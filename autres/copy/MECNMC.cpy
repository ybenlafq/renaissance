      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * COMM pour module de formatage num�rique MECNM                           
      ******************************************************************        
       01  Z-COMM-MECNM.                                                        
      ** Zone num�rique en entr�e                                               
  1>>>     02  MECNM-INPUT         PIC X(18).                                   
      ** Longueur de la zone en Input                                           
 19>>>     02  MECNM-LONG-IN       PIC S9(3) PACKED-DECIMAL.                    
      ** Num�rique ou Pack�                                                     
 21>>>     02  MECNM-NUM-PACK      PIC X.                                       
               88  MECNM-NUMERIQUE     VALUE 'N'.                               
               88  MECNM-PACKE         VALUE 'P'.                               
      ** Nombre de d�cimales                                                    
 22>>>     02  MECNM-NBDEC         PIC S9 PACKED-DECIMAL.                       
      ** Zone num�rique en sortie d�cod�e                                       
 23        02  MECNM-OUTPUT        PIC X(20).                                   
      ** Longueur de la zone en Output                                          
 43        02  MECNM-LONG-OUT      PIC S9(3) PACKED-DECIMAL.                    
      ** Longueur de la zone Fichier                                            
 45        02  MECNM-LONG-FIC      PIC S9(3) PACKED-DECIMAL.                    
      ** Code Retour                                                            
 47        02  MECNM-RCODE         PIC X.                                       
               88  MECNM-OK            VALUE 'O'.                               
               88  MECNM-KO            VALUE 'K'.                               
 ******* Longueur = 46                                                          
                                                                                
