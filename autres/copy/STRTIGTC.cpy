      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *==> DARTY ****************************************** 23-06-89 *          
      *          * ZONE START-RETRIEVE DU PROGRAMME TIGA6            *          
      ************************************************ COPY STRTIGTC *          
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  STRT-IG6A-LONG-AREA PIC S9(4) COMP VALUE +103.                       
      *--                                                                       
       01  STRT-IG6A-LONG-AREA PIC S9(4) COMP-5 VALUE +103.                     
      *}                                                                        
      *                                                                         
       01  Z-STRTAREA-IG6A.                                                     
      *---CONFIGURATION ETAT A IMPRIMER                                         
           02 STRT-IG6A-CLE.                                                    
              05 STRT-IG6A-ETA        PIC X(06).                                
              05 STRT-IG6A-DAT        PIC X(06).                                
              05 STRT-IG6A-DST        PIC X(09).                                
              05 STRT-IG6A-DOC        PIC X(15).                                
      *---CARACTERISTIQUES ETAT                                                 
      *      . NUMERO DE CONFIGURATION                                          
      *      . TYPE IMPRESSION                                                  
           02 STRT-IG6A-CFG           PIC X(01).                                
           02 STRT-IG6A-IMP           PIC X(01).                                
      *---CARACTERISTIQUES IMPRIMANTE                                           
      *      . CODE IMPRIMANTE TCT                                              
      *      . TYPE IMPRIMANTE (SYRD)                                           
      *      . MARQUE IMPRIMANTE                                                
      *      . MODELE IMPRIMANTE                                                
           02 STRT-IG6A-PRT           PIC X(04).                                
           02 STRT-IG6A-TYP           PIC X(01).                                
           02 STRT-IG6A-MRQ           PIC X(20).                                
           02 STRT-IG6A-MDL           PIC X(08).                                
      *---CARACTERISTIQUES PROTOCOLE                                            
      *       . LONGUEUR ZONE COMMANDES                                         
      *       . LONGUEUR ZONE DONNEES                                           
      *       . HAUTEUR PAGE GRAPHIQUE                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IG6A-HTR           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IG6A-HTR           PIC 9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IG6A-LNG           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IG6A-LNG           PIC 9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IG6A-COL           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IG6A-COL           PIC 9(04) COMP-5.                         
      *}                                                                        
      *---PROVENANCE                                                            
      *       . ORIGINE PROGRAMME                                               
      *       . NOM DE LA TS                                                    
      *       . LONGUEUR DE LA TS                                               
      *       . NOMBRE ITEM DE LA TS                                            
           02 STRT-IG6A-PGM           PIC X(08).                                
           02 STRT-IG6A-TSN           PIC X(08).                                
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IG6A-TSL           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IG6A-TSL           PIC 9(04) COMP-5.                         
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IG6A-TSI           PIC 9(04) COMP.                           
      *--                                                                       
           02 STRT-IG6A-TSI           PIC 9(04) COMP-5.                         
      *}                                                                        
      *---DATE DU JOUR AAMMJJ           */                                      
           02 STRT-IG6A-AMJ           PIC X(06).                                
      *-------------------------------------------------------------*           
      *  ===> ATTENTION <==                                         *           
      *  CETTE DESCRIPTION COBOL-STRTIGTC A UNE CORRESPONDANCE EN   *           
      *  PL1-STRTIGTP .                                             *           
      *  LES MODIFICATIONS DOIVENT ETRE RECIPROQUES .               *           
      *-------------------------------------------------------------*           
       EJECT                                                                    
                                                                                
