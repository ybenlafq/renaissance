      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE TP MTL30  BATCH MTL030         *            
      *     DETERMINATION DU TYPE D'EQUIPAGE                       *            
      **************************************************************            
      * CETTE COMM N'EST PLUS UTILISEE QUE PAR LES PROGRAMMES      *            
      * TP TGV17 ET MBS45  ET TRAITE TOUTE LA VENTE                *            
      **************************************************************            
      **************************************************************            
      *                                                                         
       01 COMM-MTL30-APPLI.                                                     
          02 COMM-MTL30-ENTREE.                                                 
             03 COMM-MTL30-NSOC           PIC X(03).                            
             03 COMM-MTL30-NMAG           PIC X(03).                            
             03 COMM-MTL30-NVENTE         PIC X(07).                            
AD01  *      03 COMM-MTL30-DLIVR          PIC X(08).                            
           02 COMM-MTL30-SORTIE.                                                
      *       03 COMM-MTL30-CEQUI         PIC X(03).                            
              03 COMM-MTL30-CODRET        PIC X.                                
                 88 COMM-MTL30-OK               VALUE '0'.                      
                 88 COMM-MTL30-KO               VALUE '1'.                      
              03 COMM-MTL30-MSG           PIC X(70).                            
                                                                                
