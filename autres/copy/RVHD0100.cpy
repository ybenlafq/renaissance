      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVHD0100  = FICHIER SEQUENTIEL                       
      * A L'IMAGE DE LA TABLE RTHV01 MAIS POUR ARTICLES DACEM                   
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVHD0100                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVHD0100.                                                            
           02  HD01-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  HD01-NCODIC                                                      
               PIC X(0007).                                                     
           02  HD01-NLIEU                                                       
               PIC X(0003).                                                     
           02  HD01-DVENTELIVREE                                                
               PIC X(0006).                                                     
           02  HD01-QPIECES                                                     
               PIC S9(5) COMP-3.                                                
           02  HD01-PCA                                                         
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HD01-PMTACHATS                                                   
               PIC S9(7)V9(0002) COMP-3.                                        
           02  HD01-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVHD0100                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVHD0100-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-NLIEU-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-DVENTELIVREE-F                                              
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-DVENTELIVREE-F                                              
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-QPIECES-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-QPIECES-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-PCA-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-PCA-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-PMTACHATS-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-PMTACHATS-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  HD01-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  HD01-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
