      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE INT3.RTAD07                        *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD0700.                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD0700.                                                            
      *}                                                                        
           10 AD07-NENTCDEK        PIC X(5).                                    
           10 AD07-CSOC            PIC X(3).                                    
           10 AD07-NENTCDE         PIC X(5).                                    
           10 AD07-AUTO            PIC X(1).                                    
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVAD0700-FLAGS.                                                      
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVAD0700-FLAGS.                                                      
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD07-NENTCDEK-F       PIC S9(4) COMP.                             
      *--                                                                       
           10 AD07-NENTCDEK-F       PIC S9(4) COMP-5.                           
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD07-CSOC-F          PIC S9(4) COMP.                              
      *--                                                                       
           10 AD07-CSOC-F          PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD07-NENTCDE-F       PIC S9(4) COMP.                              
      *--                                                                       
           10 AD07-NENTCDE-F       PIC S9(4) COMP-5.                            
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 AD07-AUTO-F          PIC S9(4) COMP.                              
      *                                                                         
      *--                                                                       
           10 AD07-AUTO-F          PIC S9(4) COMP-5.                            
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
