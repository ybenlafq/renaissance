      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
           EXEC SQL                                                             
       declare PS cursor for                                                    
       --codif_service PSE                                                      
       with                                                                     
       -- Prix et prime PSE actuel national                                     
       GA40 as (                                                                
       select                                                                   
       GA40.CGARANTIE                                                           
       , GA40.CFAM                                                              
       , GA40.CTARIF                                                            
       , GA40.PGARANTIE                                                         
       , GA40.PPRIME                                                            
       from RVGA4000 GA40                                                       
       left join RVGA4000 as GA40Z on                                           
       GA40Z.CGARANTIE = GA40.CGARANTIE                                         
       and GA40Z.CFAM = GA40.CFAM                                               
       and GA40Z.CTARIF = GA40.CTARIF                                           
       and GA40Z.DEFFET <= :W-DJOUR-SUIV                                        
       and GA40Z.DEFFET > GA40.DEFFET                                           
       where                                                                    
       GA40.DEFFET <= :W-DJOUR-SUIV                                             
       and GA40Z.CGARANTIE is null                                              
       )                                                                        
       , GA41 as (                                                              
       select                                                                   
       GA41.CGCPLT                                                              
       , GA41.DUGSTD                                                            
       , GA41.DUGCPLT                                                           
       , GA41.PFRAIS                                                            
       --MGD                                                                    
       , GA41.CMARQ                                                             
       from RVGA4101 GA41                                                       
       left join RVGA4100 as GA41Z on                                           
       GA41Z.CGCPLT = GA41.CGCPLT                                               
       and GA41Z.DEFFET <= :W-DJOUR-SUIV                                        
       and GA41Z.DEFFET > GA41.DEFFET                                           
       where                                                                    
       GA41.DEFFET <= :W-DJOUR-SUIV                                             
       and GA41Z.CGCPLT is null                                                 
       )                                                                        
       --Select                                                                 
       select                                                                   
       PSEHY.CSERVHY as CPRESTATION                                             
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' concat                                               
      *  lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *--                                                                       
      *{ SQL-concatenation-operators 1.4                                        
      *, varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
      *  concat '_BXM101_' ||                                                   
      *--                                                                       
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         || '_BXM101_' ||                                                       
      *}                                                                        
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
      *}                                                                        
       --Ent�te codif                                                           
       , xmlserialize (                                                         
       xmldocument(                                                             
       xmlelement(                                                              
       name "codif"                                                             
       , xmlnamespaces(default 'http://codif.darty.fr',                         
       'http://www.w3.org/2001/XMLSchema-instance' as "xsi")                    
       , xmlattributes(                                                         
       'http://codif.darty.fr http://codif.darty.fr/schemas/codif.xsd'          
       as "xsi:schemaLocation"                                                  
       , 'NCG' as "source"                                                      
       , varchar_format(current timestamp,'YYYYMMDDHH24MISSFF4')                
         concat '_BXM101_' concat                                               
         lpad(cast(row_number() over() as varchar(6)), 6, '0')                  
         as "reference"                                                         
       , current timestamp as "issued"                                          
       )                                                                        
       --                                                                       
       , xmlelement(name "product"                                              
       , xmlnamespaces(default 'http://codif.darty.fr/service'                  
       , 'http://www.w3.org/2001/XMLSchema-instance' as "xsi")                  
       , xmlattributes('907' as "context")                                      
       , xmlelement(name "codicInt", GA03.NCODICK)                              
       , xmlelement(name "lnkOpc"                                               
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "codicOpc", PSEHY.CSERVHY)                             
       --INFO: non g�r�                                                         
       , xmlelement(name "creatd",                                              
       timestamp_format('20010101', 'YYYYMMDD'))                                
       , xmlelement(name "modifd", current timestamp)                           
       , xmlelement(name "specOpc"                                              
       , xmlelement(name "type_NCG", rtrim(GA14.CTYPENT))                       
       , xmlelement(name "ref_NCG", rtrim(GCPLT.LGCPLT))                        
       , xmlelement(name "categ_NCG", rtrim(GA14.CFAM))                         
       --INFO: non g�r� Attribut par defaut de SEDIV                            
       , xmlelement(name "attr_NCG"                                             
       , xmlelement(name "code", 'DIVER')                                       
       , xmlelement(name "value", 'INIT')                                       
       )                                                                        
       , xmlelement(name "mngr_NCG", 'PREST')                                   
       , xmlelement(name "gstSe_NCG", 'PSE')                                    
       , xmlelement(name "typeSe_NCG", 'PSE')                                   
       --Service attributes                                                     
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'CGCPLT')                                      
       , xmlelement(name "value", rtrim(PSEHY.CGARANT))                         
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'PSEINO')                                      
       , xmlelement(name "value", rtrim(PSEIN.CGCPLTNU))                        
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'DUGSTD')                                      
       , xmlelement(name "value", int(GA41.DUGSTD))                             
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'DUGCPLT')                                     
       , xmlelement(name "value", int(GA41.DUGCPLT))                            
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'PFRAIS')                                      
       , xmlelement(name "value"                                                
       , replace(rtrim(cast(GA41.PFRAIS as char(6))), ',', '.'))                
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'PRDRQ')                                       
       , xmlelement(name "value", int(1))                                       
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'CATNCG')                                      
       , xmlelement(name "value", rtrim(PSEHY.CFAM))                            
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'CTARIF')                                      
       , xmlelement(name "value", rtrim(PSEHY.CTARIF))                          
       )                                                                        
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'CCTRT')                                       
       , xmlelement(name "value", substr(PSEIN.LIBCPLT, 26, 5))                 
       )                                                                        
       --INFO: non g�r�                                                         
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'NBPRD')                                       
       , xmlelement(name "value", int(1))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , xmlelement(name "attrSe_NCG"                                           
       , xmlelement(name "code", 'QTEMT')                                       
       , xmlelement(name "value", int(0))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'PAIDF')                                       
       , XMLELEMENT(NAME "value", INT(1))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'PVMOD')                                       
       , XMLELEMENT(NAME "value", INT(0))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'REGEO')                                       
       , XMLELEMENT(NAME "value", INT(0))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'WCTRT')                                       
       , XMLELEMENT(NAME "value", INT(1))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'IMPRI')                                       
       , XMLELEMENT(NAME "value", INT(1))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'DESUE')                                       
       , XMLELEMENT(NAME "value", INT(0))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'REMIS')                                       
       , XMLELEMENT(NAME "value", INT(0))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'WACTI')                                       
       , XMLELEMENT(NAME "value", INT(0))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'DATRQ')                                       
       , XMLELEMENT(NAME "value", INT(0))                                       
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'PRALP')                                       
       , XMLELEMENT(NAME "value", '')                                           
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'CACTI')                                       
       , XMLELEMENT(NAME "value", '')                                           
       )                                                                        
       --INFO: non g�r�                                                         
       , XMLELEMENT(NAME "attrSe_NCG"                                           
       , XMLELEMENT(NAME "code", 'CPTRS')                                       
       , XMLELEMENT(NAME "value", int(0))                                       
       )                                                                        
       , xmlelement(name "supplr_NCG"                                           
       , xmlelement(name "code", '90700')                                       
       , xmlelement(name "main", int(1))                                        
       , xmlelement(name "lead", int(0))                                        
       )                                                                        
       , xmlelement(name "assor_NCG", 'G')                                      
       , xmlelement(name "vat_NCG", rtrim(GCPLT.CTVA))                          
       --INFO: non g�r�                                                         
       , xmlelement(name "priceInv_NCG", '0.00' )                               
       , xmlelement(name "priceSgn_NCG", 'P')                                   
       , xmlelement(name "priceNat_NCG"                                         
       , replace(rtrim(cast(GA40.PGARANTIE as char(8))), ',', '.'))             
       , xmlelement(name "bonusNat_NCG"                                         
       , replace(rtrim(cast(GA40.PPRIME as char(8))), ',', '.'))                
       , xmlelement(name "valord_NCG", int(1))                                  
       , xmlelement(name "comment_account", '')                                 
       )                                                                        
       )                                                                        
       --INFO: non g�r�                                                         
       , xmlelement(name "creatd",                                              
       timestamp_format('20010101', 'YYYYMMDD'))                                
       , xmlelement(name "opco", '907')                                         
       , xmlelement(name "modifd", current timestamp)                           
       , xmlelement(name "userMod",                                             
       xmlattributes('true' as "xsi:nil"), '')                                  
       , xmlelement(name "nature", 'Service')                                   
       --, xmlelement(name "brand_NCG", 'DARTY')                                
       , xmlelement(name "brand_NCG", GA41.CMARQ)                               
       , xmlelement(name "refMnf"                                               
       , rtrim(PSEHY.CGARANT concat PSEHY.CFAM concat PSEHY.CTARIF))            
       , xmlelement(name "lblMrkt"                                              
       , xmlattributes('fr' as "locale"), rtrim(GCPLT.LGCPLT))                  
       , xmlelement(name "refSale"                                              
       , xmlattributes('fr' as "locale"), rtrim(GCPLT.LGCPLT))                  
       , xmlelement(name "categInt"                                             
       , xmlattributes('true' as "xsi:nil"), '')                                
       , xmlelement(name "tax"                                                  
       , xmlelement(name "country", 'FR'),                                      
       xmlelement(name "code", 'TVA'))                                          
       -- ) as XPRODUCT                                                         
       )                                                                        
       )) as clob(50K) including xmldeclaration) as XCODIF                      
       from RVPSEHY PSEHY                                                       
       --Famille NCG 'SEDIV'                                                    
       inner join RVGA1401 GA14 on                                              
       GA14.CFAM = 'SEDIV'                                                      
       inner join RVGA0300 GA03 on                                              
       GA03.NCODIC = PSEHY.CSERVHY                                              
       and GA03.CSOC = 'DAR'                                                    
       inner join GA40 on                                                       
       GA40.CGARANTIE = PSEHY.CGARANT                                           
       and GA40.CFAM = PSEHY.CFAM                                               
       and GA40.CTARIF = PSEHY.CTARIF                                           
       -- D�finition du code PSE                                                
       inner join RVGA01DI GCPLT on                                             
       GCPLT.CGCPLT = PSEHY.CGARANT                                             
       -- Lien Innovente                                                        
       inner join RVPSEIN PSEIN on                                              
       PSEIN.CGCPLT = PSEHY.CGARANT                                             
       -- Donn�es comptables                                                    
       inner join GA41 on                                                       
       GA41.CGCPLT = PSEHY.CGARANT                                              
       --                                                                       
       inner join rvad0500 ad05 on                                              
        PSEHY.CSERVHY = ad05.ncodic                                             
       and ad05.copco = 'DAR'                                                   
       and ad05.cdata = 'BXM101-PS'                                             
       and ad05.ddata = :W-DJOUR                                                
           END-EXEC                                                             
                                                                                
