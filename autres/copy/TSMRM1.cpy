      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ****************************************************************  00010000
      * TS SPECIFIQUE MRM15                                          *  00020000
      ****************************************************************  00030000
      *                                                                 00040000
      *------------------------------ LONGUEUR                          00050000
       01  TS-LONG              PIC S9(3) COMP-3 VALUE 150.             00060015
       01  TS-DONNEES.                                                  00070000
              10 TS-NCODIC          PIC X(7).                           00080012
              10 TS-RANGP           PIC S9(5)    COMP-3.                00090012
              10 TS-POIDSP          PIC S9(3)V99 COMP-3.                00100012
              10 TS-NBREFP          PIC S9(5)    COMP-3.                00110012
              10 TS-QPV             PIC S9(7)    COMP-3.                00120012
              10 TS-QRANGHER        PIC S9(5)    COMP-3.                00130012
              10 TS-QPOIDSR         PIC S9(3)V99 COMP-3.                00140012
              10 TS-QNBREFR         PIC S9(5)    COMP-3.                00150012
              10 TS-QRANG           PIC S9(5)    COMP-3.                00160012
              10 TS-QNBREF          PIC S9(5)    COMP-3.                00170012
              10 TS-PREV-CODIC      PIC X.                              00180012
              10 TS-QPREV-CODIC     PIC S9(7)    COMP-3.                00190012
              10 TS-S-1             PIC S9(5)    COMP-3.                00200012
              10 TS-S-2             PIC S9(5)    COMP-3.                00210012
              10 TS-S-3             PIC S9(5)    COMP-3.                00220012
              10 TS-S-4             PIC S9(5)    COMP-3.                00230012
              10 TS-QSOSIMU         PIC S9(7)    COMP-3.                00240012
              10 TS-QSASIMU         PIC S9(7)    COMP-3.                00250012
              10 TS-QNBMAG          PIC S9(3)    COMP-3.                00260012
              10 TS-LSTATCOMP       PIC X(3).                           00270012
              10 TS-CFAM            PIC X(5).                           00280012
              10 TS-CMARQ           PIC X(5).                           00290012
              10 TS-LREFFOURN       PIC X(20).                          00300012
              10 TS-CEXPO           PIC X(5).                           00310012
              10 TS-QSO-DIFF        PIC S9(7)    COMP-3.                00320013
              10 TS-QSA-DIFF        PIC S9(7)    COMP-3.                00330013
              10 TS-VALIDATION      PIC X.                              00340014
              10 TS-FILLER          PIC X(41).                          00350015
                                                                                
