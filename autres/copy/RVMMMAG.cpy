      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MMMAG MAGASINS EXCLUS 1000 MERCIS      *        
      *----------------------------------------------------------------*        
       01  RVMMMAG .                                                            
           05  MMMAG-CTABLEG2    PIC X(15).                                     
           05  MMMAG-CTABLEG2-REDEF REDEFINES MMMAG-CTABLEG2.                   
               10  MMMAG-SOCIETE         PIC X(03).                             
               10  MMMAG-LIEU            PIC X(03).                             
           05  MMMAG-WTABLEG     PIC X(80).                                     
           05  MMMAG-WTABLEG-REDEF  REDEFINES MMMAG-WTABLEG.                    
               10  MMMAG-WFLAG           PIC X(01).                             
               10  MMMAG-COMMENT         PIC X(10).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMMMAG-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMMAG-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MMMAG-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MMMAG-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MMMAG-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
