      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE D'EDITION BON DE MUTATION      *            
      *  PROJET CHS                                                *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MHC70                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MHC7-LONG-COMMAREA PIC S9(4) COMP VALUE +66.                    
      *--                                                                       
       01  COMM-MHC7-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +66.                  
      *}                                                                        
       01  Z-COMMAREA-MHC7.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MHC7-I.                                                      
      *                                                                         
              03 COMM-MHC7-CIMP            PIC    X(04).                        
              03 COMM-MHC7-NENVOI          PIC    X(07).                        
              03 COMM-MHC7-NSOCIETE        PIC    X(03).                        
              03 COMM-MHC7-NLIEU           PIC    X(03).                        
              03 COMM-MHC7-NSSLIEU         PIC    X(03).                        
              03 COMM-MHC7-CLIEUTRT        PIC    X(05).                        
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MHC7-O.                                                      
      *---------------------------------------CODE RETOUR                       
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MHC7-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MHC7-OK           VALUE '0'.                           
                 88 COMM-MHC7-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MHC7-MSG             PIC    X(40).                        
                                                                                
