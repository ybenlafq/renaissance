      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:35 >
      *----------------------------------------------------------------*
      *    VUE DE LA SOUS-TABLE NCGPT NCG PLUS - TRANSACTIONS          *
      *----------------------------------------------------------------*
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  RVNCGPT.
           05  NCGPT-CTABLEG2    PIC X(15).
           05  NCGPT-CTABLEG2-REDEF REDEFINES NCGPT-CTABLEG2.
               10  NCGPT-TRAN            PIC X(04).
           05  NCGPT-WTABLEG     PIC X(80).
           05  NCGPT-WTABLEG-REDEF  REDEFINES NCGPT-WTABLEG.
               10  NCGPT-LTRAN           PIC X(40).
      *----------------------------------------------------------------*
      *    FLAGS DE LA VUE                                             *
      *----------------------------------------------------------------*
       01  RVNCGPT-FLAGS.
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    05  NCGPT-CTABLEG2-F  PIC S9(4)  COMP.
      *--
           05  NCGPT-CTABLEG2-F  PIC S9(4) COMP-5.
      *}
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *    05  NCGPT-WTABLEG-F   PIC S9(4)  COMP.
      *
      *--
           05  NCGPT-WTABLEG-F   PIC S9(4) COMP-5.
       EXEC SQL END DECLARE SECTION END-EXEC.
      
      *}
