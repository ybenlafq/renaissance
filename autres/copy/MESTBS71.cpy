      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:18 >
      
      ******************************************************************
      *                            D'ABORD LA COPY DE L"ENTETE MESSMQ  *
      *   MESTBS71 COPY                                                *
      * - SUIVI DES DECONNECTIONS DU LIEN NEM / MON�TIQUE              *
      *   500 LONGUEUR                                                 *
      * COPY POUR LES PROGRAMMES MBS71                                 *
      ******************************************************************
      *
              06 MESTBS71-DATA.
      *            SOCIETE
                   07 MESTBS71-NSOCIETE       PIC X(03).
      *            LIEU
                   07 MESTBS71-NLIEU          PIC X(03).
      *            DATE DE L'OPERATION
                   07 MESTBS71-DATEOP         PIC X(08).
      *            HEURE DE L'OPERATION
                   07 MESTBS71-HEUREOP        PIC X(08).
      *            OPERATION ( OUV / FER / CON / DEC )
                   07 MESTBS71-OPERATION      PIC X(03).
      *            ETAT ( C / D )
                   07 MESTBS71-ETAT           PIC X(01).
      *            ACID
                   07 MESTBS71-CACID          PIC X(08).
      *            ECRAN
                   07 MESTBS71-ECRAN          PIC X(10).
      *            DSYST
                   07 MESTBS71-DSYST          PIC S9(13) COMP-3.
      *            REPONSE SI � 'N' PAS DE REPONSE
                   07 MESTBS71-REPONSE          PIC X(01).
      *
              06 MESTBS71-RETOUR.
                 10 MESTBS71-CODE-RETOUR.
                    15  MESTBS71-CODRET  PIC X(1).
                         88  MESTBS71-CODRET-OK        VALUE ' '.
                         88  MESTBS71-CODRET-ERREUR    VALUE '1'.
                    15  MESTBS71-LIBERR  PIC X(60).
                    15  MESTBS71-AS400CR PIC X(08).
      *
      *****************************************************************
      
