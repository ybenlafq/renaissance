      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *                                                                         
      *==> DARTY ******************************************************         
      *          * ZONE DE START DU PROGRAMME TIGAL                   *         
      ************************************************* COPY STRTIGAL *         
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  STRT-IGAL-LONG-AREA PIC S9(4) COMP VALUE +103.                       
      *--                                                                       
       01  STRT-IGAL-LONG-AREA PIC S9(4) COMP-5 VALUE +103.                     
      *}                                                                        
       01  Z-STRTAREA-IGAL.                                                     
      *                                                                         
      *----CONFIGURATION ETAT A IMPRIMER                             36         
           02 STRT-IGAL-CLE.                                                    
              05 STRT-IGAL-ETA               PIC X(06).                         
              05 STRT-IGAL-DAT               PIC X(06).                         
              05 STRT-IGAL-DST               PIC X(09).                         
              05 STRT-IGAL-DOC               PIC X(15).                         
      *                                                                         
      *----CARACTERISTIQUES ETAT                                     02         
      *       . NUMERO DE CONFIGURATION                                         
      *       . TYPE IMPRESSION                                                 
           02 STRT-IGAL-CFG                  PIC X(01).                         
           02 STRT-IGAL-IMP                  PIC X(01).                         
      *                                                                         
      *----CARACTERISTIQUES IMPRIMANTE                               33         
      *       . CODE IMPRIMANTE TCT                                             
      *       . TYPE IMPRIMANTE (SYRD)                                          
      *       . MARQUE IMPRIMANTE                                               
      *       . MODELE IMPRIMANTE                                               
           02 STRT-IGAL-PRT                  PIC X(04).                         
           02 STRT-IGAL-TYP                  PIC X(01).                         
           02 STRT-IGAL-MRQ                  PIC X(20).                         
           02 STRT-IGAL-MDL                  PIC X(08).                         
      *                                                                         
      *----CARACTERISTIQUES PROTOCOLE                                06         
      *       . LONGUEUR ZONE COMMANDES                                         
      *       . LONGUEUR ZONE DONNEES                                           
      *       . FORMAT                                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAL-CMD                  PIC 9(04) COMP.                    
      *--                                                                       
           02 STRT-IGAL-CMD                  PIC 9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAL-DON                  PIC 9(04) COMP.                    
      *--                                                                       
           02 STRT-IGAL-DON                  PIC 9(04) COMP-5.                  
      *}                                                                        
           02 STRT-IGAL-FOR                  PIC X(02).                         
      *                                                                         
      *----PROVENANCE                                                20         
      *       . ORIGINE PROGRAMME                                               
      *       . NOM DE LA TS                                                    
      *       . LONGUEUR DE LA TS                                               
      *       . NOMBRE ITEM DE LA TS                                            
           02 STRT-IGAL-PGM                  PIC X(08).                         
           02 STRT-IGAL-TSN                  PIC X(08).                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAL-TSL                  PIC 9(04) COMP.                    
      *--                                                                       
           02 STRT-IGAL-TSL                  PIC 9(04) COMP-5.                  
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02 STRT-IGAL-TSI                  PIC 9(04) COMP.                    
      *--                                                                       
           02 STRT-IGAL-TSI                  PIC 9(04) COMP-5.                  
      *}                                                                        
      *                                                                         
      *----DATE DU JOUR AAMMJJ                                       06         
           02 STRT-IGAL-AMJ                  PIC X(06).                         
      *--------------------------------------------------------------*          
      * ==> ATTENTION <==                                            *          
      * CETTE DESCRIPTION COBOL-STRTIGLC A UNE CORRESPONDANCE EN     *          
      * PL1-STRTIGLP .                                               *          
      * LES MODIFICATIONS DOIVENT ETRE RECIPROQUES .                 *          
      *--------------------------------------------------------------*          
             EJECT                                                              
                                                                                
