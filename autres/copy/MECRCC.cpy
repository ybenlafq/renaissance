      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * COMM pour MECRC                                                         
      ******************************************************************        
M21388* OBJET  : AJOUT DE LA DATE DE LIVRAISON ANTICIPEE (MANTIS 21388)*        
      * AUTEUR : DE02001                                               *        
      * DATE   : 05/03/2013                                            *        
      ******************************************************************        
CONSI * OBJET  : AJOUT DU CHOIX DE PREPARATION : CONSIGNE OU COMPTOIR  *        
      * AUTEUR : DE02013                                               *        
      * DATE   : 19/08/2013                                            *        
      ******************************************************************        
       01  COMM-MECRC.                                                          
      ** Num�ro de commande DARTY.COM                                           
           02  MECRC-NCDEWC    PIC S9(15)V USAGE COMP-3.                        
      ** Code retour                                                            
           02  MECRC-CODE-RET  PIC X(2) .                                       
               88  MECRC-OK        VALUE '00' .                                 
               88  MECRC-PBVENTE   VALUE '01' .                                 
               88  MECRC-PBDB2     VALUE '02' .                                 
      *{ remove-comma-in-dde 1.5                                                
      *        88  MECRC-KO        VALUE '01','02'.                             
      *--                                                                       
               88  MECRC-KO        VALUE '01' '02'.                             
      *}                                                                        
      ** Message retour                                                         
           02  MECRC-MESSAGE   PIC X(58).                                       
      ** Code �v�nement Click & Collect                                         
           02  MECRC-CCEVENT   PIC X(4).                                        
M21388     02  MECRC-NEW-DATE  PIC X(8).                                        
V6.5       02  MECRC-CCCHOIX   PIC X(10).                                       
M21388     02  FILLER          PIC X(40).                                       
                                                                                
