      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM6           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION ARTICLE ENT�TE STANDARD               * 00030000
      * NOM FICHIER.: RAESTD6                                         * 00040000
      *---------------------------------------------------------------* 00050000
      * CR   .......: 27/08/2013                                      * 00060000
      * MODIFIE.....:   /  /                                          * 00070000
      * VERSION N�..: 001                                             * 00080000
      * LONGUEUR....: 301                                             * 00090000
      ***************************************************************** 00100000
      *                                                                 00110000
       01  RAESTD6.                                                     00120000
      * TYPE ENREGISTREMENT                                             00130000
           05      RAESTD6-TYP-ENREG      PIC  X(0006).                 00140000
      * ETAT ARTICLE                                                    00150000
           05      RAESTD6-ETAT-ARTICLE   PIC  X(0001).                 00160000
      * CODE SOCIETE                                                    00160100
           05      RAESTD6-CSOCIETE       PIC  X(0005).                 00160200
      * CODE ARTICLE (CODIC)                                            00160300
           05      RAESTD6-CARTICLE       PIC  X(0018).                 00160400
      * LIBELL� ARTICLE (COMMERCIAL)                                    00160500
           05      RAESTD6-LARTICLE       PIC  X(0035).                 00160600
      * LIBELL� COMPL�MENTAIRE (LOGISTIQUE)                             00160700
           05      RAESTD6-LCOMPL         PIC  X(0035).                 00160800
      * CODE RACINE                                                     00160900
           05      RAESTD6-CRACINE        PIC  X(0018).                 00161000
      * TYPE D ARTICLE                                                  00162000
           05      RAESTD6-TYPE-ARTICLE   PIC  9(0002).                 00163000
      * DISPONIBLE                                                      00164000
           05      RAESTD6-DISPONIBLE     PIC  9(0001).                 00165000
      * VERTICALITE                                                     00166000
           05      RAESTD6-VERTICAL       PIC  9(0001).                 00167000
      * DANGEREUX                                                       00167100
           05      RAESTD6-DANGEREUX      PIC  9(0001).                 00167200
      * A�ROSOL                                                         00167300
           05      RAESTD6-AEROSOL        PIC  9(0001).                 00167400
      * FRAGILE                                                         00167500
           05      RAESTD6-FRAGILE        PIC  9(0001).                 00167600
      * RELEV� LOT                                                      00167700
           05      RAESTD6-REL-LOT        PIC  9(0001).                 00167800
      * RELEV� NUM�RO DE S�RIE                                          00167900
           05      RAESTD6-REL-NSERIE     PIC  9(0001).                 00168000
      * ROTATION                                                        00169000
           05      RAESTD6-ROTATION       PIC  X(0001).                 00170000
      * PRIX UNITAIRE MOYEN POND�R� (PUMP)                              00171000
           05      RAESTD6-PUMP           PIC  9(0010)V9(5).            00171100
      * TYPE DE SORTIE                                                  00171200
           05      RAESTD6-TYPE-SORTIE    PIC  9(0003).                 00171300
      * FEN�TRE TYPE DE SORTIE                                          00171400
           05      RAESTD6-FEN-TYPE-SORTIE PIC 9(0005).                 00171500
      * POURCENTAGE D ALCOOL                                            00171600
           05      RAESTD6-PER-ALCOOL     PIC  9(0003)V9(2).            00171700
      * VOLUME LIQUIDE                                                  00171800
           05      RAESTD6-VOL-LIQUIDE    PIC  9(0010).                 00171900
      * CODE-�-BARRES                                                   00172000
           05      RAESTD6-CODE-BARRES    PIC  X(0020).                 00172100
      * IDENTIFIANT FAMILLE DE COLISAGE                                 00172200
           05      RAESTD6-ID-FAM-COLISAGE PIC X(0005).                 00172300
      * IDENTIFIANT FAMILLE DE STOCKAGE                                 00172400
           05      RAESTD6-ID-FAM-STOCKAGE PIC 9(0003).                 00172500
      * SEUIL DE PR�PARATION PALETTE                                    00172600
           05      RAESTD6-SEUIL-PREP-PAL PIC  9(0009).                 00172700
      * PREMI�RE R�CEPTION                                              00172800
           05      RAESTD6-1ERE-RECEPTION PIC  9(0001).                 00172900
      * QT� TOL�RANCE INVENTAIRE                                        00173000
           05      RAESTD6-QTE-TOL-INVENT PIC  9(0003).                 00173100
      * D�LAI INTER-INVENTAIRE                                          00173200
           05      RAESTD6-DELAI-INTER-INV PIC 9(0003).                 00173300
      * CARACT�RISTIQUE 1                                               00173400
           05      RAESTD6-CARACT1        PIC  X(0020).                 00173500
      * CARACT�RISTIQUE 2                                               00173600
           05      RAESTD6-CARACT2        PIC  X(0020).                 00173700
      * CARACT�RISTIQUE 3                                               00173800
           05      RAESTD6-CARACT3        PIC  X(0020).                 00173900
      * FILLER 30 CARACT�RES                                            00174000
           05      RAESTD6-FILLER         PIC  X(0030).                 00174100
      * FIN                                                             00174200
           05      RAESTD6-FIN            PIC  X(0001).                 00174300
                                                                                
