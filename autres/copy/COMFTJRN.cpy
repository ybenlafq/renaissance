      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *****************************************************************         
      * COMMAREA POUR MODULE DE CONTROLE DES AUTORISARIONS ASSOCIEES  *         
      * AUX JOURNAUX                                                  *         
      *****************************************************************         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-FTJRN-LONG-COMMAREA PIC S9(2)  COMP VALUE +63.                  
      *--                                                                       
       01  COMM-FTJRN-LONG-COMMAREA PIC S9(2) COMP-5 VALUE +63.                 
      *}                                                                        
       01  Z-COMMAREA-MFTJRN.                                                   
           05  FTJRN-NSOC          PIC X(5).                                    
           05  FTJRN-NETAB         PIC X(3).                                    
           05  FTJRN-NJRN          PIC X(3).                                    
           05  FTJRN-CACID         PIC X(8).                                    
           05  FTJRN-COMPTE        PIC X(6).                                    
           05  FTJRN-WAUT          PIC X.                                       
           05  FTJRN-MSG           PIC X(30).                                   
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  FTJRN-SQLCODE       PIC S9(4) COMP.                              
      *--                                                                       
           05  FTJRN-SQLCODE       PIC S9(4) COMP-5.                            
      *}                                                                        
           05  FTJRN-SQLTABLE      PIC X(8).                                    
                                                                                
