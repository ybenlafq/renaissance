      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      * SE REFERER � CCBMDSL105 SUR AS400/NMD                                   
      ******************************************************************        
LD0410* MODIFICATION POUR CHRONOPOST MEC14-DERCHRONO INDICATEUR DE              
LD0410* DERNIER ENVOIE DE COLISIMO DU JOUR POUR LE CHRONOPOST                   
      ******************************************************************        
           10 MEC14-DONNEES.                                                    
      *          DATE D'EXP�DITION                                              
              15 MEC14-DEXPED          PIC X(08).                               
      *          HEURE D'EXPEDITION                                             
              15 MEC14-HEXPED          PIC X(06).                               
      *          TABLEAU DES LIGNES                                             
              15 MEC14-TABLEAU-COLIS.                                           
      *             D�TAIL DE L'EXP�DITION                                      
LD0810*          20 MEC14-DETAIL OCCURS 747.                                    
LD0810           20 MEC14-DETAIL OCCURS 700.                                    
      *               SOCI�T� DE CR�ATION.                                      
                   25 MEC14-NSOCVTE      PIC X(03).                             
      *               LIEU DE CR�ATION.                                         
                   25 MEC14-NLIEUVTE     PIC X(03).                             
      *               NUM�RO DE VENTE                                           
                   25 MEC14-NVENTE       PIC 9(07).                             
      *               LIGNE DE VENTE                                            
                   25 MEC14-NSEQNQ   PIC 9(05).                                 
      *               NUM�RO DE COLISSIMO                                       
LD0410*            25 MEC14-NCOLISSIMO   PIC X(13).                             
LD0410             25 MEC14-NCOLISSIMO   PIC X(26).                             
      *                                                                         
      *       FILLER                                                            
LD0410*       15 MEC14-FILLER          PIC X(28).                               
LD0410        15 MEC14-DEREXPED        PIC X(01).                               
LD0410        15 MEC14-FILLER          PIC X(27).                               
      *                                                                         
      *                                                                         
      *==>   FIN COPY  MEC14C                                                   
                                                                                
