      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01 TAB-CPTAB.                                                    00000010
          04 TAB-CLE.                                                   00000020
             06 TAB-CLE1          PIC X(02).                            00000030
             06 TAB-CLE2          PIC X(10).                            00000040
          04 TAB-DATA             PIC X(108).                           00000050
          04 TAB-DATA-56 REDEFINES TAB-DATA.                            00000060
             06 TAB-DEBZONES      PIC X(15).                            00000070
             06 TAB-SUITZONE      PIC X(17).                            00000080
             06 TAB-NBDECIM       PIC S9(1) COMP-3.                     00000090
             06 TAB-PSEURO        PIC X(01).                            00000100
             06 TAB-ARRONDI       PIC S9(1)V99 COMP-3.                  00000110
             06 FILLER            PIC X(72).                            00000120
          04 TAB-DATA-57 REDEFINES TAB-DATA.                            00000130
             06 TAB-DEFFET        PIC X(08).                            00000140
             06 TAB-OPERATOR      PIC X(01).                            00000150
             06 TAB-TAUX          PIC S9(4)V9(5) COMP-3.                00000160
             06 TAB-WZONES        PIC X(30).                            00000170
             06 FILLER            PIC X(64).                            00000180
                                                                                
