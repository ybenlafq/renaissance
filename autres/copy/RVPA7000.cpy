      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVPA7000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVPA7000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVPA7000.                                                            
           02  PA70-NSOCIETE                                                    
               PIC X(0003).                                                     
           02  PA70-CPTSAV                                                      
               PIC X(0002).                                                     
           02  PA70-CAUTOR                                                      
               PIC X(0001).                                                     
           02  PA70-LCPSAV                                                      
               PIC X(0030).                                                     
           02  PA70-CSNTRN                                                      
               PIC X(0001).                                                     
           02  PA70-CEXCLU                                                      
               PIC X(0001).                                                     
           02  PA70-LCTCRI                                                      
               PIC X(0010).                                                     
           02  PA70-CEDEMP                                                      
               PIC X(0001).                                                     
           02  PA70-CEDCHQ                                                      
               PIC X(0001).                                                     
           02  PA70-DSYST                                                       
               PIC S9(13) COMP-3.                                               
           02  PA70-WIC                                                         
               PIC X(0001).                                                     
           02  PA70-CTRCRI                                                      
               PIC X(0001).                                                     
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVPA7000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVPA7000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-NSOCIETE-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-CPTSAV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-CPTSAV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-CAUTOR-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-CAUTOR-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-LCPSAV-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-LCPSAV-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-CSNTRN-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-CSNTRN-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-CEXCLU-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-CEXCLU-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-LCTCRI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-LCTCRI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-CEDEMP-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-CEDEMP-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-CEDCHQ-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-CEDCHQ-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-WIC-F                                                       
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-WIC-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  PA70-CTRCRI-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  PA70-CTRCRI-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
