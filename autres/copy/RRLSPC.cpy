      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION R�CEPTION LIGNE SPECIFIQUE (MUTATION) * 00031000
      * NOM FICHIER.: RRLSPC                                          * 00032002
      *---------------------------------------------------------------* 00034000
      * CR   .......: 12/10/2011  17:46:00                            * 00035000
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
      * LONGUEUR....: 093                                             * 00037102
      ***************************************************************** 00037300
      *                                                                 00037400
       01  RRLSPC.                                                      00037500
      * TYPE ENREGISTREMENT                                             00037604
           05      RRLSPC-TYP-ENREG       PIC  X(0006).                 00037700
      * CODE SOCI�T�                                                    00037804
           05      RRLSPC-CSOCIETE        PIC  X(0005).                 00037901
      * NUM�RO DE DOSSIER DE R�CEPTION                                  00038004
           05      RRLSPC-NDOS-REC        PIC  X(0015).                 00038101
      * NUM�RO DE LIGNE DE R�CEPTION                                    00038204
           05      RRLSPC-NLIGNE-REC      PIC  9(0005).                 00038301
      * NUM�RO DE SOUS-LIGNE DE R�CEPTION                               00038404
           05      RRLSPC-NSSLIGNE-REC    PIC  9(0005).                 00038501
      * NUM�RO DE COMMANDE N3                                           00038604
           05      RRLSPC-NCDE-N3         PIC  X(0015).                 00038702
      * CODE ARTICLE                                                    00038804
           05      RRLSPC-CARTICLE        PIC  X(0018).                 00038903
      * DATE DE LIVRAISON PR�VUE                                        00039004
           05      RRLSPC-DLIVR-PREVUE    PIC  X(0008).                 00039102
      * MUTATION D'ORIGINE                                              00039204
           05      RRLSPC-NMUTATION-ORIG  PIC  X(0015).                 00040002
      * FILLER                                                          00050004
           05      RRLSPC-FIN             PIC  X(0001).                 00150000
                                                                                
