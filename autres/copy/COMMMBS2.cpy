      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *-----------------------------------------------------------------00000010
      *  F I C H E   D E S C R I P T I V E   D E   C O M M A R E A      00000020
      *-----------------------------------------------------------------00000030
      *                                                                 00000040
      *  PROJET     : BS20                                              00000050
      *  PROGRAMME  : MBS25                                             00000060
      *  TITRE      :                                                   00000070
      *  FONCTION   : COMMAREA LINK DU MODULE MBS25                     00000080
      *               LONGUEUR = 100                                    00000090
      *-----------------------------------------------------------------00000100
       01  COMM-MBS25.                                                  00000110
           03 COMM-MBS25-CFAM                 PIC X(5).                 00000120
           03 COMM-MBS25-CODE-RETOUR          PIC X(1).                 00000130
              88 COMM-MBS25-OK                         VALUE ' '.       00000140
              88 COMM-MBS25-KO                         VALUE '1'.       00000150
           03 COMM-MBS25-LIBERR               PIC X(50).                00000160
           03 COMM-MBS25-FILLER               PIC X(44).                00000170
                                                                                
