      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 17:16 >
      *****************************************************************
      * 17/03/2010 ! DSA029 !  SUPPRESSION DE LA RECONSTITUTION DES   *
      *            !        !  BUDDLES  + AJOUT ZONE                  *
      *****************************************************************
      * 17/03/2010 ! DSA029 !  CORRECTION BUG SUR PVUNIT PRESTATION   *
      *****************************************************************
      * 17/03/2010 ! DSA029 !  MODIFICATION CHRONOPOST                *
      *****************************************************************
      * 01/02/2011  FT ENVOI DE L'ID CLIENT (ADRESSE)                  *
      * VERS DARTY.COM EN MODIF DE VENTE                               *
      ******************************************************************
      * 09/03/2011 - De02003 C.LAVAURE - KIALA
      ******************************************************************
      * 07/07/2011 - De02074 e.gabetty - ajout date de reprise
      ******************************************************************
      * 22/08/2011 - De02074 e.gabetty - message d'alerte
      ******************************************************************
      * 19/01/2012 - De02074 e.gabetty - ajout code pse post achat
      ******************************************************************
      * m23862 ajout identifiant pse
      *
      ******************************************************************
      * MGD15 correctif envoi montant pvtotal pour service au lieu de
      *       pvunit... GV ne modifie que ce montant...(pvtotal)
      ******************************************************************
      * 21/10/2015 carte cadeau moteur de promo
      ******************************************************************
      *--> GESTION  DB2
       01  DB2-DISP.
      *         LG = 58 ( LONGUEUR DU MESSAGE VERS PGM APPELANT )
           05  FILLER    PIC X(12) VALUE 'PB DB2 PGM: '.
           05  PGM-DISP  PIC X(6) .
           05  FILLER    PIC X(8) VALUE ' ,CODE: '.
           05  RET-DISP  PIC 9(1) .
           05  FILLER    PIC X(6) VALUE ',SQL: '.
           05  SQL-DISP  PIC ++++9 .
           05  FILLER    PIC X(8) VALUE ',ORDRE: '.
           05  ORD-DISP  PIC X(7) .
           05  FILLER    PIC X(1) VALUE '-'.
           05  TAB-DISP  PIC X(4) .
      *--> WORKING  POUR CALCUL PRIX PRODUIT QUAND IL EXISTE 1 REMISE
       01  ZONE-CALCUL .
           05  PRIX-CALCULE  PIC S9(7)V9(0002) COMP-3.
           05  REMISE-UNIT   PIC S9(7)V9(0002) COMP-3 .
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  REMISE-TOT    PIC S9(7)V9(0002) COMP-3 .
           EXEC SQL END DECLARE SECTION END-EXEC.
       01 W-DATE-REPRISE              PIC X(08) VALUE SPACE.
       01 W-ed-NSEQNQ                 PIC 9(03).
       01  W-MESSAGE-S.
           02  FILLER      PIC X(5) VALUE 'MECRC'.
           02  W-MESSAGE-H PIC X(2).
           02  FILLER      PIC X(1) VALUE ':'.
           02  W-MESSAGE-M PIC X(2).
           02  FILLER      PIC X(1) VALUE ' '.
           02  W-MESSAGE   PIC X(69).
      * FORMATER LE GV11-NSEQ DE X(2) -->  S9(5) COMP-3
       01  NSEQ-X-5.
           05  FILLER    PIC X(3) VALUE '000'.
           05  NSEQ-X-2  PIC X(2) VALUE '00'.
       01  NSEQ-9-5 REDEFINES NSEQ-X-5 PIC 9(5) .
      * DERNIERE POSITION DU TABLEAU VENTE
      *{ convert-comp-comp4-binary-to-comp5 1.8
      *01  POSMAX    PIC S9(4) BINARY .
      *--
       01  POSMAX    PIC S9(4) COMP-5 .
      *}
      * LONGUEUR MAX DES TABLEAU DE LA COMMAREA 'COMMECCC'
           COPY COMMECCL.
      *
      *         RESULTATS REQUETES DB2
       01  RESULT        PIC X .
           88 NORMAL           VALUE '0'.
           88 NON-TROUVE       VALUE '1'.
      *         FIN DES CURSEURS
       01  FIN-GV11 PIC X VALUE 'N' .
       01  FIN-GV08 PIC X VALUE 'N' .
       01  FIN-GV14 PIC X VALUE 'N' .
       01  W-TVA                  PIC ZZV99.
           EXEC SQL BEGIN DECLARE SECTION END-EXEC.
       01  w-cdossier             pic x(05).
           EXEC SQL END DECLARE SECTION END-EXEC.
       01 wi pic 9.
       01 liste-cctrl.
          02 filler pic x(05) value 'VENTE'.
          02 filler pic x(05) value 'PROMO'.
          02 filler pic x(05) value 'MONCC'.
          02 filler pic x(05) value 'NUMCC'.
       01 filler redefines liste-cctrl.
          02 itab pic x(05) occurs 4.
       01 zright pic x(11) just right value spaces.
       01 zspaces pic x(11) value spaces.
       01 wentier pic x(11) value spaces.
       01 wdec pic x(02) value spaces.
       01 ix pic 99 value 0.
      *****************************************************************
      *  LINKAGE
      *****************************************************************
       LINKAGE SECTION.
      *         ZONE DE COMMUNICATION AVEC MECRC .
       01  DFHCOMMAREA.
           COPY COMMECCC.
      *=================================================================
       PROCEDURE DIVISION.
      *=================================================================
           PERFORM ENTREE .
           PERFORM TRAITEMENT .
           PERFORM SORTIE .
      *
      *=================================================================
       ENTREE SECTION.
           INITIALIZE               RVGV1402   .
           INITIALIZE               RVGV1106   .
           SET  NORMAL              TO TRUE    .
           MOVE 0                   TO W-CC-RETOUR .
      *  ON RECUPERE ICI LE NOM DU PROG EN COURS
           MOVE W-CC-MESSAGE(1:6)   TO PGM-DISP    .
           MOVE SPACES              TO W-CC-MESSAGE.
      *            RECEPTION NSOCIETE NLIEU NVENTE
      *      QUI SE SITUE DANS LA DERNIERE POSITION DU TAB VENTE
           MOVE 0  TO POSMAX                                .
           COMPUTE POSMAX   = W-CC-V-MAX + 1                .
           INITIALIZE               RVEC0203   .
           MOVE    W-CC-V-C-SOC(POSMAX)         TO EC02-NSOCIETE .
           MOVE    W-CC-V-C-LIEU(POSMAX)        TO EC02-NLIEU   .
           MOVE    W-CC-V-NUM-CMD-SI(POSMAX)    TO EC02-NVENTE  .
      *
      *=================================================================
       TRAITEMENT SECTION.
           PERFORM SELECT-GV10.
           PERFORM SELECT-GV02-B.
           IF NORMAL
              PERFORM RENSEIGNE-ADRESSE-LIVRAISON
              PERFORM SELECT-GV02-A
              IF NORMAL
                 PERFORM RENSEIGNE-ADRESSE-FACTURATION
              END-IF
           ELSE
              PERFORM SELECT-GV02-A
              IF NORMAL
                 PERFORM RENSEIGNE-ADRESSE-LIVRAISON
              END-IF
           END-IF.
           PERFORM TRAIT-GV14.
           PERFORM TRAIT-GV11.
      *
      *-----------------------------------------------------------------
       RENSEIGNE-ADRESSE-LIVRAISON SECTION.
           MOVE GV02-CTITRENOM      TO W-CC-E-CIVIL
           MOVE GV02-LNOM           TO W-CC-E-NOM-LIV
           MOVE GV02-LPRENOM        TO W-CC-E-PNOM-LIV
           MOVE GV02-LBATIMENT      TO W-CC-E-BMENT
           MOVE GV02-LESCALIER      TO W-CC-E-ESCAL
           MOVE GV02-LETAGE         TO W-CC-E-ETAGE
           MOVE GV02-LPORTE         TO W-CC-E-PTE
           MOVE GV02-LCOMMUNE       TO W-CC-E-CMUNE-LIV
           MOVE GV02-CPOSTAL        TO W-CC-E-C-POST-LIV
           MOVE GV02-TELDOM         TO W-CC-E-TEL-FIXE
           MOVE GV02-NGSM           TO W-CC-E-TEL-PORT
           MOVE GV02-IDCLIENT       TO W-CC-E-IDENT-CLIENT
           IF   GV02-CVOIE NOT = SPACES
                     STRING GV02-CVOIE GV02-CTVOIE GV02-LNOMVOIE
                     DELIMITED BY SIZE        INTO W-CC-E-ADR1-LIV
           ELSE
                IF   GV02-CTVOIE NOT = SPACES
                     STRING            GV02-CTVOIE GV02-LNOMVOIE
                     DELIMITED BY SIZE        INTO W-CC-E-ADR1-LIV
                ELSE
                     MOVE GV02-LNOMVOIE         TO W-CC-E-ADR1-LIV
                END-IF
           END-IF.
           MOVE GV02-LCMPAD1        TO W-CC-E-ADR2-LIV.
           MOVE GV02-LCMPAD2        TO W-CC-E-CPT-ADR-LIV.
           MOVE GV02-LCOMLIV1       TO W-CC-E-CMENT-LIV.
           MOVE GV02-CINSEE         TO W-CC-E-C-INSEE-LIV.
           MOVE GV02-CPAYS          TO W-CC-E-C-PAYS-LIV.
      *
      *-----------------------------------------------------------------
       RENSEIGNE-ADRESSE-FACTURATION SECTION.
           MOVE GV02-LNOM           TO W-CC-E-NOM-FACT
           MOVE GV02-LPRENOM        TO W-CC-E-PNOM-FACT
           IF GV02-CVOIE NOT = SPACES
              STRING GV02-CVOIE GV02-CTVOIE GV02-LNOMVOIE
                 DELIMITED BY SIZE INTO W-CC-E-ADR1-FACT
           ELSE
              IF GV02-CTVOIE NOT = SPACES
                 STRING         GV02-CTVOIE GV02-LNOMVOIE
                    DELIMITED BY SIZE  INTO W-CC-E-ADR1-FACT
              ELSE
                 MOVE GV02-LNOMVOIE   TO W-CC-E-ADR1-FACT
              END-IF
           END-IF.
           MOVE GV02-LCMPAD1        TO W-CC-E-ADR2-FACT.
           MOVE GV02-LCMPAD2        TO W-CC-E-CPT-ADR-FACT.
           MOVE GV02-CPOSTAL        TO W-CC-E-C-POST-FACT.
           MOVE GV02-CPAYS          TO W-CC-E-C-PAYS-FACT.
           MOVE GV02-CINSEE         TO W-CC-E-C-INSEE-FACT.
           MOVE GV02-LCOMMUNE       TO W-CC-E-CMUNE-FACT.
           MOVE GV02-IDCLIENT       TO W-CC-E-IDENT-CLIENT
           .
      *
      *-----------------------------------------------------------------
       TRAIT-GV14 SECTION.
           PERFORM DECLARE-GV14.
           PERFORM FETCH-GV14.
           PERFORM UNTIL FIN-GV14 = 'O'
              PERFORM ALIM-GV14
              PERFORM FETCH-GV14
           END-PERFORM.
           PERFORM CLOSE-GV14.
      *.................................................................
       ALIM-GV14 SECTION.
      * ON CHARGE AU PLUS 'W-CC-R-MAX' LIGNES ET ON STOPPE LE CURSEUR
      * EH OUI C'EST PAS BEAU MAIS CE CAS N'EST PAS CENSE SE PRODUIRE
      * EN TOUT CAS C'EST CE QUI A ETE DECIDE POUR L'INSTANT, APRES ...
            ADD 1 TO W-CC-R-I
            IF W-CC-R-I >  W-CC-R-MAX
               MOVE 'O' TO FIN-GV14
            ELSE
              MOVE EC02-NVENTE         TO  W-CC-R-NUM-CMD-SI(W-CC-R-I)
              MOVE GV14-NSEQ           TO  W-CC-R-L-PMENT-SI(W-CC-R-I)
              MOVE GV14-CMODPAIMT      TO  W-CC-R-MD-PMENT(W-CC-R-I)
              MOVE GV14-NREGLTVTE      TO  W-CC-R-ID-PMENT(W-CC-R-I)
      * attente MOVE GV14-               TO  W-CC-R-NUM-CAISSE(W-CC-R-I)
              MOVE GV14-NTRANS         TO  W-CC-R-NUM-ESSEMT(W-CC-R-I)
              MOVE GV14-DREGLTVTE      TO  W-CC-R-DT-ESSEMT(W-CC-R-I)
              MOVE GV14-PREGLTVTE      TO  W-CC-R-MT(W-CC-R-I)
              MOVE SPACES              TO  W-CC-R-STAT(W-CC-R-I)
              MOVE '0'                 TO  W-CC-R-C-RET(W-CC-R-I)
              MOVE EC02-NSOCIETE       TO  W-CC-R-C-SOC(W-CC-R-I)
              MOVE EC02-NLIEU          TO  W-CC-R-C-LIEU(W-CC-R-I)
              ADD 1 TO W-CC-R-U
            END-IF .
      *
      *-----------------------------------------------------------------
       TRAIT-GV11 SECTION.
      *    STRING 'trait gv11   '
      *    DELIMITED BY SIZE INTO W-MESSAGE
      *
      *    EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *         LENGTH (80) NOHANDLE
      *    END-EXEC
           PERFORM DECLARE-GV11.
           PERFORM FETCH-GV11.
           PERFORM UNTIL FIN-GV11 = 'O'
              PERFORM ALIM-GV11
              PERFORM FETCH-GV11
           END-PERFORM.
           PERFORM CLOSE-GV11.
      *.................................................................
       ALIM-GV11 SECTION.
      * ON CHARGE AU PLUS 'W-CC-V-MAX' LIGNES ET ON STOPPE LE CURSEUR
      * EH OUI C'EST PAS BEAU MAIS ... JE ME REPETE
      *    IF GV11-CMODDEL       = 'RD2'
           IF GV11-CMODDEL (1:2) = 'RD'
              MOVE GV11-DDELIV TO W-DATE-REPRISE
           ELSE
             IF ((GV11-CMODDEL(1 : 1) = 'L' OR 'E' )
              AND GV11-DANNULATION = ' ' AND W-DATE-REPRISE = SPACE)
             OR
               W-DATE-REPRISE NOT = SPACE
      * a voir AND (GV11-DANNULATION <= ' ' )
                ADD 1 TO W-CC-V-I
                IF W-CC-V-I >  W-CC-V-MAX
                   MOVE 'O' TO FIN-GV11
                ELSE
                  MOVE EC02-NSOCIETE  TO  W-CC-V-C-SOC(W-CC-V-I)
                  MOVE EC02-NLIEU     TO  W-CC-V-C-LIEU(W-CC-V-I)
                  MOVE EC02-NVENTE    TO  W-CC-V-NUM-CMD-SI(W-CC-V-I)
      *       IF (GV11-CTYPENREG = '1') AND (GV11NCODICGRP > ' ')
      *          MOVE GV11-NCODICGRP   TO  W-CC-V-CODIC(W-CC-V-I)
                  MOVE GV11-NCODICGRP TO  W-CC-V-NCODICGRP(W-CC-V-I)
      *       ELSE
                  MOVE GV11-NCODIC    TO  W-CC-V-CODIC(W-CC-V-I)
      *       END-IF
                  MOVE GV11-CENREG    TO  W-CC-V-CENREG(W-CC-V-I)
                  MOVE GV11-DDELIV    TO  W-CC-V-DT-LIV(W-CC-V-I)
                  MOVE GV11-QVENDUE   TO  W-CC-V-QTY(W-CC-V-I)
                  MOVE GV11-CMODDEL   TO  W-CC-V-MODE-DELIV(W-CC-V-I)
                  MOVE GV11-CPLAGE    TO  W-CC-V-CREN-LIV(W-CC-V-I)
                  if gv11-ctypenreg = '3'
                     move gv11-lcomment(1:8) to W-CC-V-NUM-PS (w-cc-v-i)
                  end-if
                  if gv11-cmoddel (1:2) = 'LD' or 'RD'
                  and gv11-ctypenreg = '1'
                     perform recherche-heures
                     MOVE GA01-WTABLEG (15:4)
                                      TO  W-CC-CREN-HDEB (W-CC-V-I)
                     MOVE GA01-WTABLEG (19:4)
                                      TO  W-CC-CREN-HFIN (W-CC-V-I)
                  end-if
                  MOVE GV11-NSEQ      TO  NSEQ-X-2
                  MOVE NSEQ-9-5       TO  W-CC-V-L-CMD-SI(W-CC-V-I)
                  MOVE '0'            TO W-CC-V-C-RET(W-CC-V-I)
                  MOVE SPACES         TO W-CC-V-REASON (W-CC-V-I)
                  MOVE GV11-CTYPENREG TO W-CC-V-CTYPENREG(W-CC-V-I)
                  MOVE GV11-WTOPELIVRE TO W-CC-V-STAT(W-CC-V-I)
                  MOVE GV11-NSOCLIVR  TO W-CC-V-NSOCLIVR(W-CC-V-I)
                  MOVE GV11-NDEPOT    TO W-CC-V-NDEPOT(W-CC-V-I)
                  MOVE W-DATE-REPRISE TO W-CC-V-DT-REPRISE(W-CC-V-I)
                  IF   GV11-CTYPENT = 'PA'
                     MOVE 'O'   TO W-CC-V-POST-PSE(W-CC-V-I)
                  ELSE
                     MOVE ' '   TO W-CC-V-POST-PSE(W-CC-V-I)
                  END-IF
      *    STRING 'date reprise '
      *       W-DATE-REPRISE
      *    DELIMITED BY SIZE INTO W-MESSAGE
      *
      *    EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *         LENGTH (80) NOHANDLE
      *    END-EXEC
                  MOVE SPACE TO W-DATE-REPRISE
                  IF GV11-WTOPELIVRE  = 'O'
                    MOVE GV11-DTOPE TO W-CC-V-DT-TOPE(W-CC-V-I)
                  END-IF
                  MOVE GV11-WEMPORTE   TO W-CC-V-WEMPORTE(W-CC-V-I)
                  MOVE GV11-TAUXTVA    TO W-TVA
                  MOVE W-TVA           TO W-CC-V-T-TVA(W-CC-V-I)
                  ADD 1 TO W-CC-V-U
                  IF GV11-CTYPENREG = '1'
                     IF   GV11-WTOPELIVRE   NOT = 'O'
                        MOVE SPACE TO W-CC-E-STAT
                     END-IF
      *            NUMERO DE COLIS
                     PERFORM SELECT-EC03
      *            CARACTERISTIQUES SPECIFIQUES
                     PERFORM SELECT-GV15
      *            REMISE SUR LE PRODUIT ?
                     PERFORM Y-A-T-IL-UNE-REMISE
                     IF NORMAL
                       MOVE PRIX-CALCULE TO W-CC-V-PX-VT(W-CC-V-I)
                     ELSE
                       MOVE GV11-PVUNIT TO W-CC-V-PX-VT(W-CC-V-I)
                     END-IF
                     IF GV11-CTYPENREG = '4'
                        MOVE GV11-PVTOTAL TO W-CC-V-PX-VT(W-CC-V-I)
                     END-IF
      *              IF NORMAL
      *                MOVE PRIX-CALCULE TO W-CC-V-PX-VT(W-CC-V-I)
      *                IF GV11-PVUNIT = ZERO AND GV11-CTYPENREG = '4'
      *                  MOVE GV11-PVTOTAL TO W-CC-V-PX-VT(W-CC-V-I)
      *                END-IF
      *              ELSE
      *                MOVE GV11-PVUNIT TO W-CC-V-PX-VT(W-CC-V-I)
      *                IF GV11-PVUNIT = ZERO AND GV11-CTYPENREG = '4'
      *                  MOVE GV11-PVTOTAL TO W-CC-V-PX-VT(W-CC-V-I)
      *                END-IF
      *              END-IF
                     PERFORM SELECT-GV22
                     IF NON-TROUVE
                        MOVE SPACE TO GV22-CSTATUT
                     END-IF
      *    STRING 'ericcc ' 'mod ' GV11-CMODDEL
      *    ' cstatut ' GV22-CSTATUT
      *    ' cequipe ' GV11-CEQUIPE
      *    ' wcqeresf ' GV11-WCQERESF
      *    DELIMITED BY SIZE INTO W-MESSAGE
      *
      *    EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *         LENGTH (80) NOHANDLE
      *    END-EXEC
                     IF GV11-WCQERESF = 'Z' OR
                       (GV11-CMODDEL = 'EMR' AND GV22-CSTATUT = 'A'
                       AND GV11-WCQERESF = ' ' AND
                         (GV11-CEQUIPE = '66VGM' OR 'MGV64'))
                       MOVE '1' TO W-CC-E-C-RET W-CC-V-C-RET(W-CC-V-I)
      *    STRING 'ericcc ' 'code ret = 1 '
      *    DELIMITED BY SIZE INTO W-MESSAGE
      *
      *    EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *         LENGTH (80) NOHANDLE
      *    END-EXEC
                       IF GV11-CMODDEL = 'EMR'
                          PERFORM UPDATE-GV11
                       END-IF
                     END-IF
                     IF GV11-WCQERESF = 'F'
                       MOVE '2' TO W-CC-E-C-RET W-CC-V-C-RET(W-CC-V-I)
                       PERFORM Q-DATE-CALEE
      *                STRING 'ERICCC ' 'CODE RET = 2 '
      *                DELIMITED BY SIZE INTO W-MESSAGE
      *
      *                EXEC CICS WRITEQ TD QUEUE ('CESO')
      *                     FROM (W-MESSAGE-S)
      *                     LENGTH (80) NOHANDLE
      *                END-EXEC
                     END-IF
                  ELSE
                    if GV11-CTYPENREG = '4'
                       MOVE GV11-PVTOTAL TO W-CC-V-PX-VT(W-CC-V-I)
                    else
                       MOVE GV11-PVUNIT TO W-CC-V-PX-VT(W-CC-V-I)
                    end-if
      *             MOVE GV11-PVUNIT TO W-CC-V-PX-VT(W-CC-V-I)
      *             IF GV11-PVUNIT = ZERO   AND GV11-CTYPENREG = '4'
      *                MOVE GV11-PVTOTAL TO W-CC-V-PX-VT(W-CC-V-I)
      *             END-IF
                  END-IF
                  if w-cdossier > spaces
                     move '5' TO W-CC-V-CTYPENREG(W-CC-V-I)
                     perform cherche-gv08
                  end-if
                END-IF
             ELSE
                MOVE SPACE TO W-DATE-REPRISE
             END-IF
           END-IF.
      *
      *=================================================================
       SORTIE SECTION.
      *{ normalize-exec-xx 1.5
      *    EXEC CICS RETURN END-EXEC.
      *--
           EXEC CICS RETURN
           END-EXEC.
      *}
      *
      *=================================================================
      *=================================================================
      * Ent�te de Vente
       SELECT-GV10 SECTION.
           INITIALIZE RVGV1004          .
           SET NORMAL       TO TRUE     .
           MOVE 'GV10'      TO TAB-DISP .
           MOVE 'SELECT '   TO ORD-DISP .
           EXEC SQL SELECT NSOCIETE, NLIEU, NVENTE, NORDRE,
                           LCOMVTE1, LCOMVTE2, LCOMVTE3, LCOMVTE4
                      INTO :GV10-NSOCIETE, :GV10-NLIEU, :GV10-NVENTE,
                           :GV10-NORDRE, :GV10-LCOMVTE1, :GV10-LCOMVTE2,
                           :GV10-LCOMVTE3, :GV10-LCOMVTE4
                      FROM RVGV1004
                     WHERE NSOCIETE = :EC02-NSOCIETE
                       AND NLIEU    = :EC02-NLIEU
                       AND NVENTE   = :EC02-NVENTE
           END-EXEC .
      *    if sqlcode = 0 or +100
      *       continue
      *    else
      *      move sqlcode to SQL-DISP
      *      STRING 'sel gv10 ' sql-disp ' cle: '
      *           EC02-NSOCIETE ' '
      *           EC02-NLIEU       ' '
      *           EC02-NVENTE
      *      DELIMITED BY SIZE INTO W-MESSAGE
      *
      *      EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *           LENGTH (80) NOHANDLE
      *      END-EXEC
      *    end-if
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 1             TO   W-CC-RETOUR
              STRING 'AUCUNE VENTE DANS GV10,NSOC=' EC02-NSOCIETE
              ' NLIEU=' EC02-NLIEU ' NVENTE=' EC02-NVENTE
              DELIMITED BY SIZE INTO  W-CC-MESSAGE
              PERFORM SORTIE
           ELSE
              MOVE GV10-LCOMVTE1          TO W-CC-E-CMENT-VT(1:30)
              MOVE GV10-LCOMVTE2          TO W-CC-E-CMENT-VT(31:30)
              MOVE GV10-LCOMVTE3          TO W-CC-E-CMENT-VT(61:30)
              MOVE GV10-LCOMVTE4          TO W-CC-E-CMENT-VT(91:30)
           END-IF.
      *
      *-----------------------------------------------------------------
       SELECT-GV02-A SECTION.
           INITIALIZE RVGV0202          .
           SET NORMAL       TO TRUE     .
           MOVE 'GV02'      TO TAB-DISP .
           MOVE 'SELECT1'   TO ORD-DISP .
           EXEC SQL SELECT CTITRENOM, LNOM , LPRENOM, LBATIMENT,
                           LESCALIER, LETAGE, LPORTE, LCOMMUNE,
                           CPOSTAL, TELDOM, NGSM, CVOIE, CTVOIE,
                           LNOMVOIE, LCMPAD1,
                           LCMPAD2, LCOMLIV1, CINSEE, CPAYS
                           , IDCLIENT
                      INTO :GV02-CTITRENOM, :GV02-LNOM,
                           :GV02-LPRENOM, :GV02-LBATIMENT,
                           :GV02-LESCALIER, :GV02-LETAGE,
                           :GV02-LPORTE, :GV02-LCOMMUNE,
                           :GV02-CPOSTAL, :GV02-TELDOM,
                           :GV02-NGSM, :GV02-CVOIE, :GV02-CTVOIE,
                           :GV02-LNOMVOIE, :GV02-LCMPAD1,
                           :GV02-LCMPAD2, :GV02-LCOMLIV1,
                           :GV02-CINSEE, :GV02-CPAYS
                           , :GV02-IDCLIENT
                      FROM RVGV0202
                     WHERE NSOCIETE = :EC02-NSOCIETE
                       AND NLIEU    = :EC02-NLIEU
                       AND NVENTE   = :EC02-NVENTE
                       AND NORDRE   = :GV10-NORDRE
                       AND WTYPEADR = 'A'
           END-EXEC .
      *    if sqlcode = 0 or +100
      *       continue
      *    else
      *      move sqlcode to SQL-DISP
      *      STRING 'sel gv02a ' sql-disp ' cle: '
      *           EC02-NSOCIETE   ' '
      *           EC02-NLIEU       ' '
      *           EC02-NVENTE ' '
      *           GV10-NORDRE
      *      DELIMITED BY SIZE INTO W-MESSAGE
      *
      *      EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *           LENGTH (80) NOHANDLE
      *      END-EXEC
      *    end-if
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       SELECT-GV02-B SECTION.
           INITIALIZE RVGV0202 .
           SET NORMAL       TO TRUE     .
           MOVE 'GV02'      TO TAB-DISP .
           MOVE 'SELECT2'   TO ORD-DISP .
           EXEC SQL SELECT CTITRENOM, LNOM , LPRENOM, LBATIMENT,
                           LESCALIER, LETAGE, LPORTE, LCOMMUNE,
                           CPOSTAL, TELDOM, NGSM, CVOIE, CTVOIE,
                           LNOMVOIE, LCMPAD1,
                           LCMPAD2, LCOMLIV1, CINSEE, CPAYS
                           , IDCLIENT
                      INTO :GV02-CTITRENOM, :GV02-LNOM,
                           :GV02-LPRENOM, :GV02-LBATIMENT,
                           :GV02-LESCALIER, :GV02-LETAGE,
                           :GV02-LPORTE, :GV02-LCOMMUNE,
                           :GV02-CPOSTAL, :GV02-TELDOM,
                           :GV02-NGSM, :GV02-CVOIE, :GV02-CTVOIE,
                           :GV02-LNOMVOIE, :GV02-LCMPAD1,
                           :GV02-LCMPAD2, :GV02-LCOMLIV1,
                           :GV02-CINSEE, :GV02-CPAYS
                           , :GV02-IDCLIENT
                      FROM RVGV0202
                     WHERE NSOCIETE = :EC02-NSOCIETE
                       AND NLIEU    = :EC02-NLIEU
                       AND NVENTE   = :EC02-NVENTE
                       AND NORDRE   = :GV10-NORDRE
                       AND WTYPEADR = 'B'
           END-EXEC.
      *    if sqlcode = 0 or +100
      *       continue
      *    else
      *      move sqlcode to SQL-DISP
      *      STRING 'sel gv02b ' sql-disp ' cle: '
      *           EC02-NSOCIETE   ' '
      *           EC02-NLIEU       ' '
      *           EC02-NVENTE ' '
      *           GV10-NORDRE
      *      DELIMITED BY SIZE INTO W-MESSAGE
      *
      *      EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *           LENGTH (80) NOHANDLE
      *      END-EXEC
      *    end-if
           PERFORM TEST-RET-DB2.
      *
       SELECT-GV22 SECTION.
           INITIALIZE RVGV2204 .
           SET NORMAL       TO TRUE     .
           MOVE 'GV22'      TO TAB-DISP .
           MOVE 'SELECT2'   TO ORD-DISP .
           EXEC SQL SELECT CSTATUT
                      INTO :GV22-CSTATUT
                      FROM RVGV2204
                     WHERE NSOCIETE   = :EC02-NSOCIETE
                       AND NLIEU      = :EC02-NLIEU
                       AND NVENTE     = :EC02-NVENTE
                       AND NCODIC     = :GV11-NCODIC
                       AND NCODICGRP  = :GV11-NCODICGRP
                       AND NSEQ       = :GV11-NSEQ
           END-EXEC.
      *    if sqlcode = 0 or +100
      *       continue
      *    else
      *      move sqlcode to SQL-DISP
      *      STRING 'sel gv22 ' sql-disp ' cle: '
      *           EC02-NSOCIETE   ' '
      *           EC02-NLIEU       ' '
      *           EC02-NVENTE    ' '
      *           GV11-NCODIC    ' '
      *           GV11-NCODICGRP      ' '
      *           GV11-NSEQ
      *      DELIMITED BY SIZE INTO W-MESSAGE
      *
      *      EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *           LENGTH (80) NOHANDLE
      *      END-EXEC
      *    end-if
           PERFORM TEST-RET-DB2.
      *-----------------------------------------------------------------
       DECLARE-GV14 SECTION.
           EXEC SQL DECLARE GV14 CURSOR FOR
                     SELECT NSEQ, CMODPAIMT, NREGLTVTE, NTRANS,
                            DREGLTVTE, PREGLTVTE
                       FROM RVGV1402
                      WHERE NSOCIETE = :EC02-NSOCIETE
                        AND NLIEU    = :EC02-NLIEU
                        AND NVENTE   = :EC02-NVENTE
           END-EXEC.
           MOVE 'GV14'      TO TAB-DISP .
           MOVE 'OPEN   '   TO ORD-DISP .
           EXEC SQL OPEN GV14 END-EXEC.
           PERFORM TEST-RET-DB2.
      *
       FETCH-GV14 SECTION.
           MOVE 'GV14'      TO TAB-DISP .
           MOVE 'FETCH  '   TO ORD-DISP .
           SET NORMAL       TO TRUE .
           EXEC SQL FETCH   GV14
                    INTO   :GV14-NSEQ           ,
                           :GV14-CMODPAIMT      ,
                           :GV14-NREGLTVTE      ,
                           :GV14-NTRANS         ,
                           :GV14-DREGLTVTE      ,
                           :GV14-PREGLTVTE
           END-EXEC.
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 'O' TO FIN-GV14
           END-IF .
      *
       CLOSE-GV14 SECTION.
           MOVE 'GV14'      TO TAB-DISP .
           MOVE 'CLOSE  '   TO ORD-DISP .
           EXEC SQL CLOSE   GV14 END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       DECLARE-GV11 SECTION.
           EXEC SQL DECLARE GV11 CURSOR FOR
                     SELECT NCODIC, CENREG, DDELIV, QVENDUE,
                            PVUNIT, CMODDEL, NSEQ, NSEQNQ,
                            CPLAGE, NCODICGRP, NORDRE, CTYPENREG,
                            WCQERESF, WTOPELIVRE, NSOCLIVR, NDEPOT
                            , DTOPE , PVTOTAL, WEMPORTE, TAUXTVA
                            , NLIGNE, CEQUIPE , DANNULATION, CTYPENT
                            , lcomment , '     '
                            , nseqens
                       FROM RVGV1106
                      WHERE NSOCIETE   = :EC02-NSOCIETE
                        AND NLIEU      = :EC02-NLIEU
                        AND NVENTE     = :EC02-NVENTE
                        AND  CTYPENREG IN ('1','3')
                     UNION
                     SELECT NCODIC, CENREG, DDELIV, QVENDUE,
                            PVUNIT, CMODDEL, NSEQ, NSEQNQ,
                            CPLAGE, NCODICGRP, NORDRE, CTYPENREG,
                            WCQERESF, WTOPELIVRE, NSOCLIVR, NDEPOT
                            , DTOPE , PVTOTAL, WEMPORTE, TAUXTVA
                            , NLIGNE, CEQUIPE , DANNULATION, CTYPENT
                            , lcomment
                            , case when cdossier is null
                                   then '     '
                                   else cdossier
                              end
                            , a.nseqens
                     from
                     (
                     SELECT NCODIC, CENREG, DDELIV, QVENDUE,
                            PVUNIT, CMODDEL, NSEQ, NSEQNQ,
                            CPLAGE, NCODICGRP, NORDRE, CTYPENREG,
                            WCQERESF, WTOPELIVRE, NSOCLIVR, NDEPOT
                            , DTOPE , PVTOTAL, WEMPORTE, TAUXTVA
                            , NLIGNE, CEQUIPE , DANNULATION, CTYPENT
                            , lcomment
                            , nsociete
                            , nlieu
                            , nvente
                            , nseqens
                       FROM RVGV1106 a
                       WHERE NSOCIETE  = :EC02-NSOCIETE
                        AND NLIEU      = :EC02-NLIEU
                        AND NVENTE     = :EC02-NVENTE
                        AND  CTYPENREG IN ('4')
                     ) A
                       LEFT OUTER JOIN
                        (
                          SELECT
                                   NSOCIETE
                                 , NLIEU
                                 , NVENTE
                                 , NSEQENS
                                 , CDOSSIER
                            FROM RVGV0500 A
                            inner join RVGA0100 B
                              on  B.CTABLEG1 = 'VALCT'
                              AND B.CTABLEG2 LIKE 'MPDOS__________'
                              AND SUBSTR(B.WTABLEG, 4, 5) = A.CDOSSIER
                              AND A.NSOCIETE   = :EC02-NSOCIETE
                              AND A.NLIEU      = :EC02-NLIEU
                              AND A.NVENTE     = :EC02-NVENTE
                        ) b
                        on a.nsociete = b.nsociete
                        and a.nlieu   = b.nlieu
                        and a.nvente  = b.nvente
                        and a.nseqens = b.nseqens
                   ORDER BY NLIGNE DESC
           END-EXEC.
      *    POUR CTYPENREG = '2' IL S'AGIT D'UNE REMISE
      *    TRAITEE A PART
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'OPEN   '   TO ORD-DISP .
           EXEC SQL OPEN   GV11 END-EXEC.
           PERFORM TEST-RET-DB2.
      *
       FETCH-GV11 SECTION.
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'FETCH  '   TO ORD-DISP .
           SET NORMAL       TO TRUE .
           EXEC SQL FETCH GV11 INTO :GV11-NCODIC, :GV11-CENREG,
                         :GV11-DDELIV, :GV11-QVENDUE,
                         :GV11-PVUNIT, :GV11-CMODDEL, :GV11-NSEQ,
                         :GV11-NSEQNQ, :GV11-CPLAGE,
                         :GV11-NCODICGRP, :GV11-NORDRE,
                         :GV11-CTYPENREG, :GV11-WCQERESF,
                         :GV11-WTOPELIVRE, :GV11-NSOCLIVR, :GV11-NDEPOT
                         , :GV11-DTOPE, :GV11-PVTOTAL , :GV11-WEMPORTE
                         , :GV11-TAUXTVA , :GV11-NLIGNE , :GV11-CEQUIPE
                         , :GV11-DANNULATION , :GV11-CTYPENT
                         , :gv11-lcomment
                         , :w-cdossier
                         , :gv11-nseqens
           END-EXEC.
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 'O' TO FIN-GV11
           END-IF .
      *
       CLOSE-GV11 SECTION.
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'CLOSE  '   TO ORD-DISP .
           EXEC SQL CLOSE   GV11
           END-EXEC .
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       UPDATE-GV11 SECTION.
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'UPDATE '   TO ORD-DISP .
           SET NORMAL       TO TRUE .
           EXEC SQL UPDATE RVGV1106
                       SET WCQERESF = ' '
                    WHERE    NSOCIETE = :EC02-NSOCIETE AND
                             NLIEU    = :EC02-NLIEU    AND
                             NVENTE   = :EC02-NVENTE   AND
                             NCODIC   = :GV11-NCODIC   AND
                             NSEQNQ   = :GV11-NSEQNQ   AND
                             CTYPENREG = '1'
           END-EXEC.
           PERFORM TEST-RET-DB2.
      *
      *-----------------------------------------------------------------
       SELECT-EC03 SECTION.
      *    INITIALIZE RVEC0301 .
      *    INITIALIZE RVEC0302 .
           INITIALIZE RVEC0303 .
           MOVE GV11-NSEQNQ    TO  EC03-NSEQNQ  .
           MOVE 'EC03'         TO TAB-DISP .
           MOVE 'SELECT '      TO ORD-DISP .
           SET NORMAL          TO TRUE     .
           EXEC SQL
                SELECT CCOLIS, CCDDEL, CCHDEL
                    ,  NRELAISID
                INTO  :EC03-CCOLIS, :EC03-CCDDEL, :EC03-CCHDEL
                    , :EC03-NRELAISID
      *               FROM RVEC0301
      *               FROM RVEC0302
                FROM RVEC0303
                WHERE NSOCIETE = :EC02-NSOCIETE
                AND   NLIEU    = :EC02-NLIEU
                AND   NVENTE   = :EC02-NVENTE
                AND   NSEQNQ   = :EC03-NSEQNQ
           END-EXEC.
      *    if sqlcode = 0 or +100
      *       continue
      *    else
      *      move sqlcode to SQL-DISP
      *      move EC03-NSEQNQ to w-ed-NSEQNQ
      *      STRING 'sel ec03 ' sql-disp ' cle: '
      *           EC02-NSOCIETE   ' '
      *           EC02-NLIEU       ' '
      *           EC02-NVENTE    ' '
      *           w-ed-NSEQNQ    ' '
      *      DELIMITED BY SIZE INTO W-MESSAGE
      *
      *      EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *           LENGTH (80) NOHANDLE
      *      END-EXEC
      *    end-if
           PERFORM TEST-RET-DB2.
           IF NORMAL
              MOVE EC03-CCOLIS         TO  W-CC-V-NUM-COLIS(W-CC-V-I)
              MOVE EC03-CCDDEL         TO  W-CC-V-DELIVERY(W-CC-V-I)
              MOVE EC03-CCHDEL         TO  W-CC-V-DELIVERY(W-CC-V-I)(9:)
              MOVE EC03-NRELAISID      TO  W-CC-V-RELAIS-ID(W-CC-V-I)
           END-IF.
      *
      *-----------------------------------------------------------------
       SELECT-GV15 SECTION.
           INITIALIZE RVGV1500 .
           MOVE 'GV15'      TO TAB-DISP .
           MOVE 'SELECT '   TO ORD-DISP .
           SET NORMAL       TO TRUE     .
           EXEC SQL SELECT  CARACTSPE1 , CVCARACTSPE1
                      INTO :GV15-CARACTSPE1 , :GV15-CVCARACTSPE1
                      FROM RVGV1500
                     WHERE NSOCIETE  = :EC02-NSOCIETE
                       AND NLIEU     = :EC02-NLIEU
                       AND NVENTE    = :EC02-NVENTE
                       AND NORDRE    = :GV11-NORDRE
                       AND NCODIC    = :GV11-NCODIC
                       AND NCODICGRP = :GV11-NCODICGRP
                       AND NSEQ      = :GV11-NSEQ
           END-EXEC.
      *    if sqlcode = 0 or +100
      *       continue
      *    else
      *      move sqlcode to SQL-DISP
      *      STRING 'sel ec03 ' sql-disp ' cle: '
      *           EC02-NSOCIETE   ' '
      *           EC02-NLIEU       ' '
      *           EC02-NVENTE    ' '
      *           GV11-NORDRE    ' '
      *           GV11-NCODIC     ' '
      *           GV11-NCODICGRP      ' '
      *           GV11-NSEQ
      *      DELIMITED BY SIZE INTO W-MESSAGE
      *
      *      EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *           LENGTH (80) NOHANDLE
      *      END-EXEC
      *    end-if
           PERFORM TEST-RET-DB2.
           IF NORMAL
              STRING GV15-CARACTSPE1 DELIMITED BY SPACE
                                 '=' DELIMITED BY SIZE
                   GV15-CVCARACTSPE1 DELIMITED BY SIZE
                INTO W-CC-V-CARSPEC(W-CC-V-I)
           END-IF.
      *
      *-----------------------------------------------------------------
       Y-A-T-IL-UNE-REMISE SECTION.
           MOVE 0           TO PRIX-CALCULE.
           MOVE 0           TO REMISE-UNIT REMISE-TOT .
           MOVE 'GV11'      TO TAB-DISP .
           MOVE 'SELECT '   TO ORD-DISP .
           SET NORMAL       TO TRUE     .
           EXEC SQL SELECT PVTOTAL
                      INTO :REMISE-TOT
                      FROM RVGV1106
                     WHERE NSOCIETE  = :EC02-NSOCIETE
                       AND NLIEU     = :EC02-NLIEU
                       AND NVENTE    = :EC02-NVENTE
                       AND NORDRE    = :GV11-NORDRE
                       AND NCODIC    = :GV11-NCODIC
                       AND NSEQREF   = :GV11-NSEQNQ
                       AND CTYPENREG = '2'
           END-EXEC.
           PERFORM TEST-RET-DB2.
           IF NORMAL
              COMPUTE REMISE-UNIT  ROUNDED = REMISE-TOT / GV11-QVENDUE
              END-COMPUTE
              COMPUTE PRIX-CALCULE = GV11-PVUNIT - REMISE-UNIT
              END-COMPUTE
           END-IF.
      *
       recherche-heures section.
           initialize rvga0100
           MOVE SPACES      TO GA01-CTABLEG2
           MOVE GV11-CPLAGE TO GA01-CTABLEG2
           SET NORMAL       TO TRUE     .
           MOVE 'LPLG'      TO TAB-DISP .
           MOVE 'SELECT '   TO ORD-DISP .
           EXEC SQL
                SELECT  WTABLEG
                  into :ga01-wtableg
                  from rvga0100
                 WHERE CTABLEG1 = 'LPLGE'
                 AND   CTABLEG2 = :GA01-CTABLEG2
           END-EXEC
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE ALL '?' TO GA01-WTABLEG
           else
           if  GA01-WTABLEG (15:4) <= SPACES
               MOVE spaces TO GA01-WTABLEG (15:4)
           end-if
           if  GA01-WTABLEG (19:4) <= SPACES
               MOVE spaces TO GA01-WTABLEG (19:4)
           end-if
           END-IF.
       f-recherche-heures. exit.
       Q-DATE-CALEE SECTION.
           SET NORMAL       TO TRUE     .
           MOVE 'EC11'      TO TAB-DISP .
           MOVE 'SELECT '   TO ORD-DISP .
           EXEC SQL
                SELECT nsociete
                  INTO :EC02-NSOCIETE
                 FROM  RVEC1100
                 WHERE NSOCIETE = :EC02-NSOCIETE
                   AND NLIEU    = :EC02-NLIEU
                   AND NVENTE   = :EC02-NVENTE
                   AND NSEQNQ   = :GV11-NSEQNQ
                   AND NSEQERR  = '0026'
           end-exec.
      *    if sqlcode = 0 or +100
      *       continue
      *    else
      *      move sqlcode to SQL-DISP
      *      STRING 'SEL EC11 ' SQL-DISP ' CLE: '
      *           EC02-NSOCIETE ' '
      *           EC02-NLIEU       ' '
      *           EC02-NVENTE
      *      DELIMITED BY SIZE INTO W-MESSAGE
      *
      *      EXEC CICS WRITEQ TD QUEUE ('CESO') FROM (W-MESSAGE-S)
      *           LENGTH (80) NOHANDLE
      *      END-EXEC
      *    end-if
           PERFORM TEST-RET-DB2.
           IF NON-TROUVE
              MOVE 'CALEE'     TO W-CC-V-REASON (W-CC-V-I)
           ELSE
              MOVE 'NON_CALEE' TO W-CC-V-REASON (W-CC-V-I)
           END-IF.
       F-Q-DATE-CALEE. EXIT.
      *-----------------------------------------------------------------
      *-----------------------------------------------------------------
       TEST-RET-DB2 SECTION.
           MOVE SQLCODE     TO SQL-DISP
           IF SQLCODE <  0
              MOVE 2        TO W-CC-RETOUR
              MOVE DB2-DISP TO W-CC-MESSAGE
              PERFORM SORTIE
           ELSE
              IF SQLCODE = +100
                 SET NON-TROUVE TO TRUE
              END-IF
           END-IF.
       cherche-gv08 section.
           move 0 to W-CC-V-P2
           MOVE W-CC-V-C-SOC  (W-CC-V-MAX + 1)     TO GV08-NSOCIETE
           MOVE W-CC-V-C-LIEU (W-CC-V-MAX + 1)     TO GV08-NLIEU
           MOVE W-CC-V-NUM-CMD-SI (W-CC-V-MAX + 1) TO GV08-NVENTE
           PERFORM VARYING WI FROM 1 BY 1
              UNTIL WI > 5
                 MOVE ITAB (WI) TO GV08-CCTRL
                 MOVE SPACEs TO GV08-VALCTRL
                 EXEC SQL
                     SELECT VALCTRL
                      INTO :GV08-VALCTRL
                     FROM RVGV0800 a
                     inner join rvgv0500 B
                     on a.nsociete  = b.nsociete
                     and a.nlieu    = b.nlieu
                     and a.nvente   = b.nvente
                     and a.ndossier = b.ndossier
                     and b.NSOCIETE   = :GV08-NSOCIETE
                     AND b.NLIEU      = :GV08-NLIEU
                     AND b.NVENTE     = :GV08-NVENTE
                     AND a.CCTRL      = :GV08-CCTRL
                     and b.nseqens    = :gv11-nseqens
                 END-EXEC
                 ADD 1 TO W-CC-V-P2
                 move itab(wi)
                   to W-CC-V-NOM  (W-CC-V-I, W-CC-V-P2)
                 if itab(wi) = 'MONCC'
                    unstring gv08-valctrl (1:11) delimited
                    by all space into zright
                    move zright(10:2) to wdec
                    unstring zright (1:9) delimited
                    by all space into zspaces wentier
                    string wentier '.' wdec delimited by space
                    intO W-CC-V-VALEUR  (W-CC-V-I, W-CC-V-P2)
                 else
                    move gv08-valctrl
                      TO W-CC-V-VALEUR  (W-CC-V-I, W-CC-V-P2)
                 end-if
           END-PERFORM.
       f-cherche-gv08. exit.
      
