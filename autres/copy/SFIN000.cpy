      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
           03   FIN000-CODENR             PIC  X.                       00450000
           03   FIN000-CLEF.                                            00460000
                05   FIN000-TYPE          PIC  X(3).                    00470000
                05   FIN000-LIEU          PIC  X(3).                    00480000
                05   FIN000-RAYON         PIC  X(5).                    00490000
                05   FIN000-LIEUSTK       PIC  X(3).                    00500000
                05   FIN000-SEQFAM        PIC  9(5) COMP-3.             00510000
           03   FIN000-MARQUE             PIC  X(5).                    00520000
           03   FIN000-NCODIC             PIC  X(07).                   00530000
           03   FIN000-CFAM               PIC  X(5).                    00540000
           03   FIN000-LIBFAM             PIC  X(30).                   00550000
           03   FIN000-REFERENCE          PIC  X(20).                   00560000
           03   FIN000-NBETIQ             PIC  9(5) COMP-3.             00570000
           03   FIN000-DATEINV            PIC  X(8).                    00580000
           03   FIN000-LDESTO1            PIC  X(5).                    00581000
           03   FIN000-LDESTO2            PIC  X(5).                    00582000
           03   FIN000-LDESTO3            PIC  X(5).                    00583000
           03   FIN000-LDESTO4            PIC  X(5).                    00584000
           03   FIN000-LDESTO5            PIC  X(5).                    00585000
           03   FIN000-NEAN               PIC  X(13).                   00586000
           03   FILLER                    PIC  X(1).                    00590000
                                                                                
