      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
       01  COMM-MTL45-APPLI.                                                    
           05  COMM-MTL45-NSOCDEPART           PIC  X(3).                       
           05  COMM-MTL45-NLIEUDEPART          PIC  X(3).                       
           05  COMM-MTL45-NSOCARRIVEE          PIC  X(3).                       
           05  COMM-MTL45-NLIEUARRIVEE         PIC  X(3).                       
           05  COMM-MTL45-GESTION-GV21         PIC  X(1).                       
           05  COMM-MTL45-DETAIL               OCCURS 100.                      
               10  COMM-MTL45-NSOCIETE         PIC  X(3).                       
               10  COMM-MTL45-NLIEU            PIC  X(3).                       
               10  COMM-MTL45-NVENTE           PIC  X(7).                       
               10  COMM-MTL45-NSEQ             PIC  X(2).                       
               10  COMM-MTL45-NCODICGRP        PIC  X(7).                       
               10  COMM-MTL45-NCODIC           PIC  X(7).                       
               10  COMM-MTL45-QUANTITE         PIC  S9(5) COMP-3.               
               10  COMM-MTL45-FILLER           PIC  X(8).                       
           05  COMM-MTL45-CODRET               PIC  X(02).                      
           05  COMM-MTL45-LIBRET               PIC  X(60).                      
           05  FILLER                          PIC  X(21).                      
                                                                                
