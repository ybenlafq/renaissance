      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      * -------------------------------------------------------------- *        
      * CETTE COPIE GERE TOUT CE QUI TOUCHE LA GESTION DE LA TS        *        
      * TSTEC02 : ** CREATION SI CETTE DERNERE N'EXISTE PAS            *        
      *           ** LECTURE DE CETTE TS                               *        
      * LES ZONES DE TRAVAIL SONT DECLAREES DANS WKTENVOI              *        
      *                                                                *        
      * CREE LE 01/03/2011 PAR DE02003 (CL)                            *        
      *                                                                *        
      * -------------------------------------------------------------- *        
      *                                                                         
      *-----------------------------------------------------------------        
       LECTURE-TSTEC02 SECTION.                                                 
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         READQ TS                                                        
      *         QUEUE  (WC-ID-TSTEC02)                                          
      *         INTO   (TS-TSTEC02-DONNEES)                                     
      *         LENGTH (LENGTH OF TS-TSTEC02-DONNEES)                           
      *         ITEM   (WP-RANG-TSTEC02)                                        
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS READQ TS                                                   
                QUEUE  (WC-ID-TSTEC02)                                          
                INTO   (TS-TSTEC02-DONNEES)                                     
                LENGTH (LENGTH OF TS-TSTEC02-DONNEES)                           
                ITEM   (WP-RANG-TSTEC02)                                        
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           MOVE '9' TO WF-COD-RET-EC02.                                         
           IF EIBRESP = 0                                                       
              SET WC-EC02-OK TO TRUE                                            
           END-IF.                                                              
      *    EVALUATE EIBRESP                                                     
      *    WHEN 0                                                               
      *       SET WC-TSTEC02-OK         TO TRUE                                 
      *    WHEN 26                                                              
      *    WHEN 44                                                              
      *       SET WC-TSTEC02-FIN        TO TRUE                                 
      *    WHEN OTHER                                                           
      *       SET WC-TSTEC02-ERREUR     TO TRUE                                 
      *    END-EVALUATE.                                                        
       FIN-LECT-TSTEC02. EXIT.                                                  
      *                                                                         
      *-----------------------------------------------------------------        
       ECRITURE-TSTEC02 SECTION.                                                
      *{ normalize-exec-xx 1.5                                                  
      *    EXEC CICS                                                            
      *         WRITEQ TS                                                       
      *         QUEUE  (WC-ID-TSTEC02)                                          
      *         FROM   (TS-TSTEC02-DONNEES)                                     
      *         LENGTH (LENGTH OF TS-TSTEC02-DONNEES)                           
      *         NOHANDLE                                                        
      *    END-EXEC.                                                            
      *--                                                                       
           EXEC CICS WRITEQ TS                                                  
                QUEUE  (WC-ID-TSTEC02)                                          
                FROM   (TS-TSTEC02-DONNEES)                                     
                LENGTH (LENGTH OF TS-TSTEC02-DONNEES)                           
                NOHANDLE                                                        
           END-EXEC.                                                            
      *}                                                                        
           INITIALIZE TS-TSTEC02-DONNEES.                                       
       FIN-ECRIT-TSTEC02. EXIT.                                                 
      *                                                                         
      *-----------------------------------------------------------------        
      *                                                                         
       DELETE-TSTEC02 SECTION.                                                  
      ******************************                                            
           EXEC CICS DELETEQ TS                                                 
                QUEUE    (WC-ID-TSTEC02)                                        
                NOHANDLE                                                        
           END-EXEC.                                                            
       FIN-DELETE-TSTEC02. EXIT.                                                
      ********************************                                          
      *-----------------------------------------------------------------        
                                                                                
