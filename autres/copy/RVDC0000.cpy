      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      **********************************************************        00000010
      *   COPY DE LA TABLE RVDC0000                                     00000020
      **********************************************************        00000030
      *                                                                 00000040
      *---------------------------------------------------------        00000050
      *   LISTE DES HOST VARIABLES DE LA TABLE RVDC0000                 00000060
      *---------------------------------------------------------        00000070
      *                                                                 00000080
       01  RVDC0000.                                                    00000090
           02  DC00-NSOCIETE                                            00000100
               PIC X(0003).                                             00000110
           02  DC00-NDOSSIER                                            00000120
               PIC X(0007).                                             00000130
           02  DC00-NLIEU                                               00000140
               PIC X(0003).                                             00000150
           02  DC00-NVENTE                                              00000160
               PIC X(0007).                                             00000170
           02  DC00-RIB                                                 00000180
               PIC X(0023).                                             00000190
           02  DC00-DOMBANQ                                             00000200
               PIC X(0035).                                             00000210
           02  DC00-AGENCE                                              00000220
               PIC X(0035).                                             00000230
           02  DC00-LBATIMB                                             00000240
               PIC X(0003).                                             00000250
           02  DC00-LESCALB                                             00000260
               PIC X(0003).                                             00000270
           02  DC00-LETAGEB                                             00000280
               PIC X(0003).                                             00000290
           02  DC00-LPORTEB                                             00000300
               PIC X(0003).                                             00000310
           02  DC00-CVOIEB                                              00000320
               PIC X(0005).                                             00000330
           02  DC00-CTVOIEB                                             00000340
               PIC X(0004).                                             00000350
           02  DC00-LNVOIEB                                             00000360
               PIC X(0021).                                             00000370
           02  DC00-LCOMMUNEB                                           00000380
               PIC X(0032).                                             00000390
           02  DC00-CPOSTALB                                            00000400
               PIC X(0005).                                             00000410
           02  DC00-LBUREAUB                                            00000420
               PIC X(0026).                                             00000430
           02  DC00-CTITRENOM                                           00000440
               PIC X(0005).                                             00000450
           02  DC00-LNOM                                                00000460
               PIC X(0025).                                             00000470
           02  DC00-LPRENOM                                             00000480
               PIC X(0015).                                             00000490
           02  DC00-LBATIMENT                                           00000500
               PIC X(0003).                                             00000510
           02  DC00-LESCALIER                                           00000520
               PIC X(0003).                                             00000530
           02  DC00-LETAGE                                              00000540
               PIC X(0003).                                             00000550
           02  DC00-LPORTE                                              00000560
               PIC X(0003).                                             00000570
           02  DC00-CVOIE                                               00000580
               PIC X(0005).                                             00000590
           02  DC00-CTVOIE                                              00000600
               PIC X(0004).                                             00000610
           02  DC00-LNOMVOIE                                            00000620
               PIC X(0021).                                             00000630
           02  DC00-LCOMMUNE                                            00000640
               PIC X(0032).                                             00000650
           02  DC00-CPOSTAL                                             00000660
               PIC X(0005).                                             00000670
           02  DC00-LBUREAU                                             00000680
               PIC X(0026).                                             00000690
           02  DC00-NUMTEL                                              00000700
               PIC X(0008).                                             00000710
           02  DC00-NBECHEANCE                                          00000720
               PIC S9(1) COMP-3.                                        00000730
           02  DC00-MACCOMPTE                                           00000740
               PIC S9(7)V9(0002) COMP-3.                                00000750
           02  DC00-MENCAISSE                                           00000760
               PIC S9(7)V9(0002) COMP-3.                                00000770
           02  DC00-MENCOURS                                            00000780
               PIC S9(7)V9(0002) COMP-3.                                00000790
           02  DC00-NOMACCORD                                           00000800
               PIC X(0020).                                             00000810
           02  DC00-DECHSUIV                                            00000820
               PIC X(0008).                                             00000830
           02  DC00-ETAT                                                00000840
               PIC X(0001).                                             00000850
           02  DC00-NOMOPER                                             00000860
               PIC X(0020).                                             00000870
           02  DC00-DSAISIE                                             00000880
               PIC X(0008).                                             00000890
           02  DC00-DCOMPTA                                             00000900
               PIC X(0008).                                             00000910
           02  DC00-DSYST                                               00000920
               PIC S9(13) COMP-3.                                       00000930
           02  DC00-CHRONO                                              00000940
               PIC X(0005).                                             00000950
      *                                                                 00000960
      *---------------------------------------------------------        00000970
      *   LISTE DES FLAGS DE LA TABLE RVDC0000                          00000980
      *---------------------------------------------------------        00000990
      *                                                                 00001000
       01  RVDC0000-FLAGS.                                              00001010
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NSOCIETE-F                                          00001020
      *        PIC S9(4) COMP.                                          00001030
      *--                                                                       
           02  DC00-NSOCIETE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NDOSSIER-F                                          00001040
      *        PIC S9(4) COMP.                                          00001050
      *--                                                                       
           02  DC00-NDOSSIER-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NLIEU-F                                             00001060
      *        PIC S9(4) COMP.                                          00001070
      *--                                                                       
           02  DC00-NLIEU-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NVENTE-F                                            00001080
      *        PIC S9(4) COMP.                                          00001090
      *--                                                                       
           02  DC00-NVENTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-RIB-F                                               00001100
      *        PIC S9(4) COMP.                                          00001110
      *--                                                                       
           02  DC00-RIB-F                                                       
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-DOMBANQ-F                                           00001120
      *        PIC S9(4) COMP.                                          00001130
      *--                                                                       
           02  DC00-DOMBANQ-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-AGENCE-F                                            00001140
      *        PIC S9(4) COMP.                                          00001150
      *--                                                                       
           02  DC00-AGENCE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LBATIMB-F                                           00001160
      *        PIC S9(4) COMP.                                          00001170
      *--                                                                       
           02  DC00-LBATIMB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LESCALB-F                                           00001180
      *        PIC S9(4) COMP.                                          00001190
      *--                                                                       
           02  DC00-LESCALB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LETAGEB-F                                           00001200
      *        PIC S9(4) COMP.                                          00001210
      *--                                                                       
           02  DC00-LETAGEB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LPORTEB-F                                           00001220
      *        PIC S9(4) COMP.                                          00001230
      *--                                                                       
           02  DC00-LPORTEB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CVOIEB-F                                            00001240
      *        PIC S9(4) COMP.                                          00001250
      *--                                                                       
           02  DC00-CVOIEB-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CTVOIEB-F                                           00001260
      *        PIC S9(4) COMP.                                          00001270
      *--                                                                       
           02  DC00-CTVOIEB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LNVOIEB-F                                           00001280
      *        PIC S9(4) COMP.                                          00001290
      *--                                                                       
           02  DC00-LNVOIEB-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LCOMMUNEB-F                                         00001300
      *        PIC S9(4) COMP.                                          00001310
      *--                                                                       
           02  DC00-LCOMMUNEB-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CPOSTALB-F                                          00001320
      *        PIC S9(4) COMP.                                          00001330
      *--                                                                       
           02  DC00-CPOSTALB-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LBUREAUB-F                                          00001340
      *        PIC S9(4) COMP.                                          00001350
      *--                                                                       
           02  DC00-LBUREAUB-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CTITRENOM-F                                         00001360
      *        PIC S9(4) COMP.                                          00001370
      *--                                                                       
           02  DC00-CTITRENOM-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LNOM-F                                              00001380
      *        PIC S9(4) COMP.                                          00001390
      *--                                                                       
           02  DC00-LNOM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LPRENOM-F                                           00001400
      *        PIC S9(4) COMP.                                          00001410
      *--                                                                       
           02  DC00-LPRENOM-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LBATIMENT-F                                         00001420
      *        PIC S9(4) COMP.                                          00001430
      *--                                                                       
           02  DC00-LBATIMENT-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LESCALIER-F                                         00001440
      *        PIC S9(4) COMP.                                          00001450
      *--                                                                       
           02  DC00-LESCALIER-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LETAGE-F                                            00001460
      *        PIC S9(4) COMP.                                          00001470
      *--                                                                       
           02  DC00-LETAGE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LPORTE-F                                            00001480
      *        PIC S9(4) COMP.                                          00001490
      *--                                                                       
           02  DC00-LPORTE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CVOIE-F                                             00001500
      *        PIC S9(4) COMP.                                          00001510
      *--                                                                       
           02  DC00-CVOIE-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CTVOIE-F                                            00001520
      *        PIC S9(4) COMP.                                          00001530
      *--                                                                       
           02  DC00-CTVOIE-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LNOMVOIE-F                                          00001540
      *        PIC S9(4) COMP.                                          00001550
      *--                                                                       
           02  DC00-LNOMVOIE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LCOMMUNE-F                                          00001560
      *        PIC S9(4) COMP.                                          00001570
      *--                                                                       
           02  DC00-LCOMMUNE-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CPOSTAL-F                                           00001580
      *        PIC S9(4) COMP.                                          00001590
      *--                                                                       
           02  DC00-CPOSTAL-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-LBUREAU-F                                           00001600
      *        PIC S9(4) COMP.                                          00001610
      *--                                                                       
           02  DC00-LBUREAU-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NUMTEL-F                                            00001620
      *        PIC S9(4) COMP.                                          00001630
      *--                                                                       
           02  DC00-NUMTEL-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NBECHEANCE-F                                        00001640
      *        PIC S9(4) COMP.                                          00001650
      *--                                                                       
           02  DC00-NBECHEANCE-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-MACCOMPTE-F                                         00001660
      *        PIC S9(4) COMP.                                          00001670
      *--                                                                       
           02  DC00-MACCOMPTE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-MENCAISSE-F                                         00001680
      *        PIC S9(4) COMP.                                          00001690
      *--                                                                       
           02  DC00-MENCAISSE-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-MENCOURS-F                                          00001700
      *        PIC S9(4) COMP.                                          00001710
      *--                                                                       
           02  DC00-MENCOURS-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NOMACCORD-F                                         00001720
      *        PIC S9(4) COMP.                                          00001730
      *--                                                                       
           02  DC00-NOMACCORD-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-DECHSUIV-F                                          00001740
      *        PIC S9(4) COMP.                                          00001750
      *--                                                                       
           02  DC00-DECHSUIV-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-ETAT-F                                              00001760
      *        PIC S9(4) COMP.                                          00001770
      *--                                                                       
           02  DC00-ETAT-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-NOMOPER-F                                           00001780
      *        PIC S9(4) COMP.                                          00001790
      *--                                                                       
           02  DC00-NOMOPER-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-DSAISIE-F                                           00001800
      *        PIC S9(4) COMP.                                          00001810
      *--                                                                       
           02  DC00-DSAISIE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-DCOMPTA-F                                           00001820
      *        PIC S9(4) COMP.                                          00001830
      *--                                                                       
           02  DC00-DCOMPTA-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-DSYST-F                                             00001840
      *        PIC S9(4) COMP.                                          00001850
      *--                                                                       
           02  DC00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  DC00-CHRONO-F                                            00001860
      *        PIC S9(4) COMP.                                          00001870
      *--                                                                       
           02  DC00-CHRONO-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                            00001880
                                                                                
