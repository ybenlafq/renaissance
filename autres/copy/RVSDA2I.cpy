      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SDA2I LIEN SOCIETE DARTY / SOC DA2I    *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSDA2I.                                                             
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSDA2I.                                                             
      *}                                                                        
           05  SDA2I-CTABLEG2    PIC X(15).                                     
           05  SDA2I-CTABLEG2-REDEF REDEFINES SDA2I-CTABLEG2.                   
               10  SDA2I-NSOCIETE        PIC X(03).                             
               10  SDA2I-DEFFET          PIC X(08).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
               10  SDA2I-DEFFET-N       REDEFINES SDA2I-DEFFET                  
                                         PIC 9(08).                             
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
           05  SDA2I-WTABLEG     PIC X(80).                                     
           05  SDA2I-WTABLEG-REDEF  REDEFINES SDA2I-WTABLEG.                    
               10  SDA2I-NSOCDA2I        PIC X(03).                             
               10  SDA2I-LSOCDA2I        PIC X(50).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVSDA2I-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVSDA2I-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SDA2I-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SDA2I-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SDA2I-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SDA2I-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
