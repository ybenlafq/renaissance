      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
                                                                                
      *                                                               *         
      *               PARAMETRE MODULE DEGROUPAGE                     *         
       01  W-DEGROUP-PARAM.                                                     
           02  W-CODIC-GROUPE             PIC X(7).                             
           02  W-CTYPLIEN                 PIC X(5).                             
           02  W-TAB-DEGROUPE             OCCURS 20.                            
              03  W-CODIC-DEGROUPE        PIC X(7).                             
              03  W-CODIC-DEGRELIB        PIC X(7).                             
              03  W-CODIC-QTYPLIEN        PIC 9(3) COMP-3.                      
       01  W-TYPE-ARTICLE                 PIC X.                                
           88 W-ARTICLE-GROUPE            VALUE '0'.                            
           88 W-ARTICLE-SIMPLE            VALUE '1'.                            
       01  W-IND-DEGROUP                  PIC 9(5) COMP-3.                      
      *                                                               *         
                                                                                
