      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      * ZONES DE TRAVAIL UTILISEES POUR LA GESTION DE LA TS TSTENVOI            
       77 WC-ID-TSTEC02          PIC  X(08)      VALUE 'TSTEC02 '.              
       77 IND-TS                     PIC 9(05)         VALUE 0.                 
      *77 WN-TIME                    PIC 9(08)         VALUE 0.                 
      *77 WN-TIME2                   PIC 9(08)         VALUE 0.                 
      *77 WN-DELTATIME               PIC 9(08)         VALUE 0.                 
      *77 DER-ENREG                  PIC X(30) VALUE SPACE.                     
      *77 AVANT-DER-ENREG            PIC X(30) VALUE SPACE.                     
      *77 WZ-XCTRL-EXPDEL-SQLCODE PIC -Z(7)9      VALUE ZEROS .                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WP-RANG-TSTEC02         PIC S9(04) COMP VALUE +0.                     
      *--                                                                       
       77 WP-RANG-TSTEC02         PIC S9(04) COMP-5 VALUE +0.                   
      *}                                                                        
      *77 WS-TSTENVOI-NOMPGM      PIC  X(10)      VALUE SPACE.                  
      *                                                                         
      *                                                                         
       01 WF-COD-RET-EC02         PIC X.                                        
          88 WC-EC02-OK                  VALUE '0'.                             
      *   88 WC-XCTRL-EXPDEL-NON-TROUVE  VALUE '1'.                             
      *   88 WC-XCTRL-EXPDEL-PB-DB2      VALUE '2'.                             
      *01 WF-CURS-XCTRL-EXPDEL    PIC X.                                        
      *   88 WC-XCTRL-EXPDEL-SUITE       VALUE '0'.                             
      *   88 WC-XCTRL-EXPDEL-FIN         VALUE '1'.                             
      *01 WF-TSTENVOI             PIC X.                                        
      *   88 WC-TSTENVOI-SUITE           VALUE '0'.                             
      *   88 WC-TSTENVOI-FIN             VALUE '1'.                             
      *   88 WC-TSTENVOI-ERREUR          VALUE '2'.                             
      * DESCRIPTION DE LA TS.                                                   
       01 TS-TSTEC02-DONNEES.                                                   
          05 TSTEC02-CODIC                        PIC X(07).                    
      *01 W-TSTENVOI-DONNEES.                                                   
      *   05 TABLE-POSTE-TSTENVOI OCCURS 500.                                   
      *      10 POSTE-TSTENVOI                     PIC X(50).                   
                                                                                
