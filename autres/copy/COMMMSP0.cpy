      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  COMM-MSP00-APPLI.                                                    
           02 COMM-MSP00-ZONES-ENTREE.                                          
              05 COMM-MSP00-NCODIC             PIC X(07).                       
              05 COMM-MSP00-DATE               PIC X(08).                       
              05 COMM-MSP00-NCDE               PIC X(07).                       
              05 COMM-MSP00-NREC               PIC X(07).                       
              05 COMM-MSP00-NBL                PIC X(10).                       
              05 COMM-MSP00-CFAM               PIC X(05).                       
           02 COMM-MSP00-ZONES-SORTIE.                                          
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *       05 COMM-MSP00-CODRET      COMP   PIC  S9(4).                      
      *--                                                                       
              05 COMM-MSP00-CODRET COMP-5   PIC  S9(4).                         
      *}                                                                        
              05 COMM-MSP00-SRPREC             PIC S9(7)V9(6) COMP-3.           
              05 COMM-MSP00-SRPCDE             PIC S9(7)V9(6) COMP-3.           
              05 COMM-MSP00-PABASEFACT         PIC S9(7)V9(2) COMP-3.           
              05 COMM-MSP00-PROMOFACT          PIC S9(7)V9(2) COMP-3.           
              05 COMM-MSP00-QCOEFF1            PIC S9(1)V9(4) COMP-3.           
              05 COMM-MSP00-QCOEFF2            PIC S9(1)V9(4) COMP-3.           
              05 COMM-MSP00-TABLE              PIC X(06).                       
                                                                                
