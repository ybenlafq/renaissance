      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION OP LIGNE STANDARD (MUTATION)           *
      * NOM FICHIER.: EOLSTD                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 65                                              *
      *****************************************************************
      *
       01  EOLSTD.
      * TYPE ENREGISTREMENT
           05      EOLSTD-TYP-ENREG       PIC  X(0006).
      * CODE SOCIETT
           05      EOLSTD-CSOCIETE        PIC  X(0005).
      * NUMERO D OP
           05      EOLSTD-NOP             PIC  X(0015).
      * NUMERO DE LIGNE
           05      EOLSTD-NLIGNE          PIC  9(0005).
      * NUMERO DE SOUS-LIGNE
           05      EOLSTD-NSSLIGNE        PIC  9(0005).
      * TYPE DE LIGNE
           05      EOLSTD-TYPE-LIGNE      PIC  9(0001).
      * CODE ARTICLE
           05      EOLSTD-CARTICLE        PIC  X(0018).
      * QUANTITE PREPAREE
           05      EOLSTD-QTE-PREP        PIC  X(0009).
      * CODE FIN
           05      EOLSTD-FIN             PIC  X(0001).
      
