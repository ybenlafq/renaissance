      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      ******************************************************************        
      *   COPY D'UN MESSAGE STANDARD ENTRE APPLI NMD ET LE HOST.                
      *-----------------------------------------------------------------        
      *   (105 caract�res d'ent�te standard et 30000 de donn�es).               
      ******************************************************************        
      *                                                                         
       01  MESS-NMD.                                                            
      *    ENTETE STANDARD.                                 LG=105C             
           05  MESS-ENTETE.                                                     
               10  MESS-TYPE        PIC  X(03).                                 
               10  MESS-NSOCMSG     PIC  X(03).                                 
               10  MESS-NLIEUMSG    PIC  X(03).                                 
               10  MESS-NSOCDST     PIC  X(03).                                 
               10  MESS-NLIEUDST    PIC  X(03).                                 
               10  MESS-NORD        PIC  9(08).                                 
               10  MESS-LPROG       PIC  X(10).                                 
               10  MESS-DATEJ       PIC  X(08).                                 
               10  MESS-WSID        PIC  X(10).                                 
               10  MESS-USER        PIC  X(10).                                 
               10  MESS-CHRONO      PIC  9(07).                                 
               10  MESS-NBRMSG      PIC  9(07).                                 
               10  MESS-NBROCC      PIC  9(05).                                 
               10  MESS-LNGOCC      PIC  9(05).                                 
               10  MESS-VERSION     PIC  X(02).                                 
               10  MESS-FILLER      PIC  X(18).                                 
      *    DONNEES.                                         LG=30000C.          
           05  MESS-DONNEES         PIC  X(30000).                              
                                                                                
