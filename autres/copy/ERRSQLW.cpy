      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
       01  ABEND-ZONE-STD.                                                      
           05 ABEND-PROG-STD           PIC  X(8).                               
           05 ABEND-MESS-STD.                                                   
              10  FILLER               PIC  X(35).                              
              10  ABEND-NCODE-STD.                                              
                  15  ABEND-CODE-STD   PIC  -ZZZ9.                              
       01  W-SQL-MESS-STD.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  W-SQL-LRECL-STD         PIC S9(8) COMP  VALUE +72.               
      *--                                                                       
           05  W-SQL-LRECL-STD         PIC S9(8) COMP-5  VALUE +72.             
      *}                                                                        
           05  W-SQL-MSGS-STD.                                                  
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *        10  W-SQL-LGTH-STD      PIC S9(4)   COMP   VALUE +288.           
      *--                                                                       
               10  W-SQL-LGTH-STD      PIC S9(4) COMP-5   VALUE +288.           
      *}                                                                        
               10  W-SQL-MSG-STD       PIC X(72)   OCCURS 4.                    
                                                                                
