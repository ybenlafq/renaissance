      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE ADMCH ADMINISTRATION MENU CHASSIS      *        
      *----------------------------------------------------------------*        
       01  RVADMCH.                                                             
           05  ADMCH-CTABLEG2    PIC X(15).                                     
           05  ADMCH-CTABLEG2-REDEF REDEFINES ADMCH-CTABLEG2.                   
               10  ADMCH-CACID           PIC X(08).                             
           05  ADMCH-WTABLEG     PIC X(80).                                     
           05  ADMCH-WTABLEG-REDEF  REDEFINES ADMCH-WTABLEG.                    
               10  ADMCH-A               PIC X(01).                             
               10  ADMCH-GRP             PIC X(03).                             
               10  ADMCH-CHA             PIC X(03).                             
               10  ADMCH-COD             PIC X(03).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVADMCH-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ADMCH-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  ADMCH-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  ADMCH-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  ADMCH-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
