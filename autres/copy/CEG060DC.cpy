      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES LIGNES                *         
      *===============================================================*         
           EXEC SQL DECLARE EG05   CURSOR FOR                                   
                     SELECT TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            SKIPBEFORE,                                         
                            SKIPAFTER,                                          
                            RUPTIMPR                                            
                       FROM RVEG0500                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND TYPLIGNE ^= '7'                                     
                        AND TYPLIGNE <> '7'                                     
      *}
                      ORDER BY TYPLIGNE, NOLIGNE, CONTINUER                     
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 66 ROWS                                         
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES CHAMPS A TOTALISER    *         
      *===============================================================*         
           EXEC SQL DECLARE EG15   CURSOR FOR                                   
                     SELECT TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            RUPTRAZ,                                            
                            NOMCHAMPT                                           
                       FROM RVEG1500                                            
                      WHERE NOMETAT   = :EG15-NOMETAT                           
                      ORDER BY TYPLIGNE,                                        
                               NOLIGNE,                                         
                               CONTINUER,                                       
                               POSCHAMP                                         
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 256 ROWS                                        
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES CHAMPS FIXES          *         
      *===============================================================*         
           EXEC SQL DECLARE EG20   CURSOR FOR                                   
                     SELECT TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            LIBCHAMP                                            
                       FROM RVEG2000                                            
                      WHERE NOMETAT   = :EG20-NOMETAT                           
                        AND TYPLIGNE  = :EG20-TYPLIGNE                          
                        AND NOLIGNE   = :EG20-NOLIGNE                           
                        AND CONTINUER = :EG20-CONTINUER                         
                      ORDER BY TYPLIGNE, NOLIGNE, CONTINUER, POSCHAMP           
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 264 ROWS                                        
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES RUPTURES              *         
      *===============================================================*         
           EXEC SQL DECLARE RUPT   CURSOR FOR                                   
                     SELECT POSCHAMP,                                           
                            RUPTIMPR                                            
                       FROM RVEG1000                                            
                      WHERE TYPLIGNE = '7'                                      
                        AND NOMETAT  = :EG10-NOMETAT                            
                      ORDER BY POSCHAMP                                         
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 15 ROWS                                         
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES CHAMPS D'UNE LIGNE    *         
      *===============================================================*         
           EXEC SQL DECLARE CHAMPS CURSOR FOR                                   
                     SELECT 'V',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            RUPTIMPR                                            
                       FROM RVEG1000                                            
      *{Post-translation Correct-Sign-not
      *               WHERE TYPLIGNE ^= '7'                                     
                      WHERE TYPLIGNE <> '7'                                     
      *}
                        AND NOMETAT   = :EG10-NOMETAT                           
                    UNION                                                       
                     SELECT 'C',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            RUPTIMPR                                            
                       FROM RVEG1100                                            
      *{Post-translation Correct-Sign-not
      *               WHERE TYPLIGNE ^= '7'                                     
                      WHERE TYPLIGNE <> '7'                                     
      *}
                        AND NOMETAT = :EG11-NOMETAT                             
                      ORDER BY 2, 3, 4, 5                                       
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 256 ROWS                                        
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES POSITIONS DES CHAMPS  *         
      *===============================================================*         
           EXEC SQL DECLARE POSIT  CURSOR FOR                                   
                     SELECT 'R',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            NOMCHAMP                                            
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
                        AND TYPLIGNE  = '7'                                     
                    UNION                                                       
                     SELECT 'V',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            NOMCHAMP                                            
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND TYPLIGNE ^= '7'                                     
                        AND TYPLIGNE <> '7'                                     
      *}
                    UNION                                                       
                     SELECT '1',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND1                                             
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND CHCOND1  ^= '                  '                    
                        AND CHCOND1  <> '                  '                    
      *}
                    UNION                                                       
                     SELECT '2',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND2                                             
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND CHCOND2  ^= '                  '                    
                        AND CHCOND2  <> '                  '                    
      *}
                    UNION                                                       
                     SELECT '5',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND1                                             
                       FROM RVEG1100                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND CHCOND1  ^= '                  '                    
                        AND CHCOND1  <> '                  '                    
      *}
                    UNION                                                       
                     SELECT '6',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND2                                             
                       FROM RVEG1100                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND CHCOND2  ^= '                  '                    
                        AND CHCOND2  <> '                  '                    
      *}
                    UNION                                                       
                     SELECT 'C',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            NOMCHAMPC                                           
                       FROM RVEG1100                                            
                      WHERE NOMETAT    = :EG10-NOMETAT                          
      *{Post-translation Correct-Sign-not
      *                 AND NOMCHAMPC ^= '                  '                   
                        AND NOMCHAMPC <> '                  '                   
      *}
                    UNION                                                       
                     SELECT 'L',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            0 ,                                                 
                            NOMCHAMPC                                           
                       FROM RVEG0500                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND NOMCHAMPC ^= '                  '                   
                        AND NOMCHAMPC <> '                  '                   
      *}
                    UNION                                                       
                     SELECT 'T',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            0 ,                                                 
                            NOMCHAMP                                            
                       FROM RVEG4000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
                      ORDER BY 6, 2, 3, 4, 5                                    
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 500 ROWS                                        
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES CONSTANTES            *         
      *===============================================================*         
           EXEC SQL DECLARE KONST  CURSOR FOR                                   
                     SELECT '1',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND2                                             
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND TYPLIGNE ^= '7'                                     
                        AND TYPLIGNE <> '7'                                     
      *}
      *{Post-translation Correct-Sign-not
      *                 AND CHCOND2  ^= ' '                                     
                        AND CHCOND2  <> ' '                                     
      *}
                        AND SUBSTR (CHCOND2, 1, 1) = ''''                       
                    UNION                                                       
                     SELECT '2',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND2                                             
                       FROM RVEG1100                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND CHCOND2  ^= ' '                                     
                        AND CHCOND2  <> ' '                                     
      *}
                        AND SUBSTR (CHCOND2, 1, 1) = ''''                       
                      ORDER BY 2, 3, 4, 5                                       
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 256 ROWS                                        
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES COMPTEUR CONDITIONS   *         
      *===============================================================*         
           EXEC SQL DECLARE CCTR   CURSOR FOR                                   
                     SELECT '1',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND1                                             
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND TYPLIGNE ^= '7'                                     
                        AND TYPLIGNE <> '7'                                     
      *}
                        AND SUBSTR (CHCOND1, 1, 1) = '&'                        
                    UNION                                                       
                     SELECT '2',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND2                                             
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
      *{Post-translation Correct-Sign-not
      *                 AND TYPLIGNE ^= '7'                                     
                        AND TYPLIGNE <> '7'                                     
      *}
                        AND SUBSTR (CHCOND2, 1, 1) = '&'                        
                    UNION                                                       
                     SELECT '3',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND1                                             
                       FROM RVEG1100                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
                        AND SUBSTR (CHCOND1, 1, 1) = '&'                        
                    UNION                                                       
                     SELECT '4',                                                
                            TYPLIGNE,                                           
                            NOLIGNE,                                            
                            CONTINUER,                                          
                            POSCHAMP,                                           
                            CHCOND2                                             
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG10-NOMETAT                           
                        AND SUBSTR (CHCOND2, 1, 1) = '&'                        
                      ORDER BY 2, 3, 4, 5                                       
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 30 ROWS                                         
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES CHAMPS DE L'ETAT      *         
      *===============================================================*         
           EXEC SQL DECLARE EG30   CURSOR FOR                                   
                     SELECT NOMCHAMP,                                           
                            TYPCHAMP,                                           
                            LGCHAMP,                                            
                            DECCHAMP,                                           
                            POSDSECT,                                           
                            OCCURENCES                                          
                       FROM RVEG3000                                            
                      WHERE NOMETAT   = :EG30-NOMETAT                           
                    UNION                                                       
                     SELECT NOMCHAMP,                                           
                            'C',                                                
                            0 ,                                                 
                            0 ,                                                 
                            0 ,                                                 
                            0                                                   
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG30-NOMETAT                           
                        AND NOMCHAMP  LIKE '$$________________'                 
                    UNION                                                       
                     SELECT NOMCHAMP,                                           
                            '&',                                                
                            0 ,                                                 
                            0 ,                                                 
                            0 ,                                                 
                            0                                                   
                       FROM RVEG1000                                            
                      WHERE NOMETAT   = :EG30-NOMETAT                           
                        AND NOMCHAMP  LIKE '&_________________'                 
                      ORDER BY 1                                                
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 500 ROWS                                        
                END-EXEC.                                                       
      *===============================================================*         
      *     DECLARE DU CURSEUR DE RECHERCHE DES CALCULS DE L'ETAT     *         
      *===============================================================*         
           EXEC SQL DECLARE EG25   CURSOR FOR                                   
                     SELECT NOMCHAMP,                                           
                            LIGNECALC,                                          
                            CHCALC1,                                            
                            TYPCALCUL,                                          
                            CHCALC2                                             
                       FROM RVEG2500                                            
                      WHERE NOMETAT   = :EG30-NOMETAT                           
                      ORDER BY NOMCHAMP,LIGNECALC                               
                        FOR FETCH ONLY                                          
                   OPTIMIZE FOR 150 ROWS                                        
                END-EXEC.                                                       
      *================================================================*        
                                                                                
