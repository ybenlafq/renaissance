      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE EQUPF PARAM. P/ PF MONOEQUIPAGE        *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUPF .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUPF .                                                            
      *}                                                                        
           05  EQUPF-CTABLEG2    PIC X(15).                                     
           05  EQUPF-CTABLEG2-REDEF REDEFINES EQUPF-CTABLEG2.                   
               10  EQUPF-NSOCLIEU        PIC X(06).                             
           05  EQUPF-WTABLEG     PIC X(80).                                     
           05  EQUPF-WTABLEG-REDEF  REDEFINES EQUPF-WTABLEG.                    
               10  EQUPF-ELIGIB          PIC X(01).                             
               10  EQUPF-REPRISE         PIC X(01).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVEQUPF-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVEQUPF-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUPF-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  EQUPF-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  EQUPF-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  EQUPF-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
