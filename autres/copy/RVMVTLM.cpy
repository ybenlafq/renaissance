      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE MVTLM NATURE MVT LM7 - MVT NCG         *        
      *----------------------------------------------------------------*        
       01  RVMVTLM .                                                            
           05  MVTLM-CTABLEG2    PIC X(15).                                     
           05  MVTLM-CTABLEG2-REDEF REDEFINES MVTLM-CTABLEG2.                   
               10  MVTLM-NSOCDEP         PIC X(06).                             
               10  MVTLM-NSOCDEP-N      REDEFINES MVTLM-NSOCDEP                 
                                         PIC 9(06).                             
               10  MVTLM-CNATMVT         PIC X(09).                             
           05  MVTLM-WTABLEG     PIC X(80).                                     
           05  MVTLM-WTABLEG-REDEF  REDEFINES MVTLM-WTABLEG.                    
               10  MVTLM-ACTIF           PIC X(01).                             
               10  MVTLM-NSSLIEUO        PIC X(08).                             
               10  MVTLM-NSSLIEUD        PIC X(08).                             
               10  MVTLM-COMMENT         PIC X(20).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVMVTLM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MVTLM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  MVTLM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  MVTLM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  MVTLM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
