      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      **************************************************************            
      *  COMMAREA SPECIFIQUE MODULE D'EDITION BON DE MUTATION      *            
      *  PROJET CHS                                                *            
      **************************************************************            
      *                                                                         
      * PROGRAMME  MHC20                                                        
      *                                                                         
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  COMM-MHC2-LONG-COMMAREA PIC S9(4) COMP VALUE +111.                   
      *--                                                                       
       01  COMM-MHC2-LONG-COMMAREA PIC S9(4) COMP-5 VALUE +111.                 
      *}                                                                        
       01  Z-COMMAREA-MHC2.                                                     
      *---------------------------------------ZONES EN ENTREE DU MODULE         
           02 COMM-MHC2-I.                                                      
      *                                                                         
              03 COMM-MHC2-CIMP            PIC    X(04).                        
              03 COMM-MHC2-NLIEUHC         PIC    X(03).                        
              03 COMM-MHC2-LTRAIT          PIC    X(20).                        
              03 COMM-MHC2-DENVOI          PIC    X(08).                        
              03 COMM-MHC2-NENVOI          PIC    X(07).                        
              03 COMM-MHC2-NSOCORIG        PIC    X(03).                        
              03 COMM-MHC2-NLIEUORIG       PIC    X(03).                        
              03 COMM-MHC2-NSSLIEUORIG     PIC    X(03).                        
              03 COMM-MHC2-CLIEUTRTORIG    PIC    X(05).                        
              03 COMM-MHC2-NSOCDEST        PIC    X(03).                        
              03 COMM-MHC2-NLIEUDEST       PIC    X(03).                        
              03 COMM-MHC2-NSSLIEUDEST     PIC    X(03).                        
              03 COMM-MHC2-CLIEUTRTDEST    PIC    X(05).                        
      *---------------------------------------ZONES EN SORTIE DU MODULE         
           02 COMM-MHC2-O.                                                      
      *---------------------------------------CODE RETOUR                       
      * ERREUR --> ABANDON PROGRAMME                                            
              03 COMM-MHC2-CODRET          PIC    X VALUE '0'.                  
                 88 COMM-MHC2-OK           VALUE '0'.                           
                 88 COMM-MHC2-KO           VALUE '1'.                           
      * MESSAGE D'ERREUR                                                        
              03 COMM-MHC2-MSG             PIC    X(40).                        
                                                                                
