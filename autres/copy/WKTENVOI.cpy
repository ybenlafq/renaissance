      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
      * ZONES DE TRAVAIL UTILISEES POUR LA GESTION DE LA TS TSTENVOI            
       77 WC-ID-TSTENVOI          PIC  X(08)      VALUE 'TSTENVOI'.             
V43R1  77 IND-TS                     PIC 9(03)         VALUE 0.                 
V43R1  77 WN-TIME                    PIC 9(08)         VALUE 0.                 
V43R1  77 WN-TIME2                   PIC 9(08)         VALUE 0.                 
V43R1  77 WN-DELTATIME               PIC 9(08)         VALUE 0.                 
V43R1  77 DER-ENREG                  PIC X(30) VALUE SPACE.                     
V43R1  77 AVANT-DER-ENREG            PIC X(30) VALUE SPACE.                     
       77 WZ-XCTRL-EXPDEL-SQLCODE PIC -Z(7)9      VALUE ZEROS .                 
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *77 WP-RANG-TSTENVOI        PIC S9(04) COMP VALUE +0.                     
      *--                                                                       
       77 WP-RANG-TSTENVOI        PIC S9(04) COMP-5 VALUE +0.                   
      *}                                                                        
       77 WS-TSTENVOI-NOMPGM      PIC  X(10)      VALUE SPACE.                  
      *                                                                         
       01 WS-TYPENVOI.                                                          
          05 WS-TYPENVOI1         PIC X.                                        
          05 WS-TYPENVOI2         PIC X.                                        
          05 WS-TYPENVOI3         PIC X.                                        
          05 WS-TYPENVOI4         PIC X.                                        
          05 WS-TYPENVOI5         PIC X.                                        
          05 WS-TYPENVOI6         PIC X.                                        
          05 WS-TYPENVOI7         PIC X.                                        
          05 WS-TYPENVOI8         PIC X.                                        
          05 WS-TYPENVOI9         PIC X.                                        
          05 WS-TYPENVOI10        PIC X.                                        
       01 TAB-TYPENVOI REDEFINES WS-TYPENVOI.                                   
          05 WS-TAB-TENVOI        PIC X    OCCURS 10.                           
      *                                                                         
       01 WF-XCTRL-EXPDEL         PIC X.                                        
          88 WC-XCTRL-EXPDEL-TROUVE      VALUE '0'.                             
          88 WC-XCTRL-EXPDEL-NON-TROUVE  VALUE '1'.                             
          88 WC-XCTRL-EXPDEL-PB-DB2      VALUE '2'.                             
       01 WF-CURS-XCTRL-EXPDEL    PIC X.                                        
          88 WC-XCTRL-EXPDEL-SUITE       VALUE '0'.                             
          88 WC-XCTRL-EXPDEL-FIN         VALUE '1'.                             
       01 WF-TSTENVOI             PIC X.                                        
          88 WC-TSTENVOI-SUITE           VALUE '0'.                             
          88 WC-TSTENVOI-FIN             VALUE '1'.                             
          88 WC-TSTENVOI-ERREUR          VALUE '2'.                             
      * DESCRIPTION DE LA TS.                                                   
       01 TS-TSTENVOI-DONNEES.                                                  
          05 TSTENVOI-NSOCZP.                                                   
             10 TSTENVOI-NSOC                      PIC X(03).                   
             10 FILLER                             PIC X(02).                   
          05 TSTENVOI-CTRL                         PIC X(04).                   
          05 TSTENVOI-WPARAM                       PIC X(10).                   
          05 TSTENVOI-LIBELLE                      PIC X(30).                   
       01 W-TSTENVOI-DONNEES.                                                   
          05 TABLE-POSTE-TSTENVOI OCCURS 500.                                   
             10 POSTE-TSTENVOI                     PIC X(50).                   
                                                                                
