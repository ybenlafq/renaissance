      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
jk01  * Trouver les ventes faisant partie de DARTY.COM                  03580008
 "    * Appel du sous-module MECRC qui va                               03580008
 "    * orienter vers la(les) filiale(s) en liaison avec la table SUFIX         
 "     TRT-MECRC                   SECTION.                             03580008
 "    *---------------------------------------                          03590008
 "         INITIALIZE                    COMM-MECRC                     01660008
 "         INITIALIZE                    RVEC0203                       01660008
 "         MOVE COMM-MGV51-NSOCIETE     TO EC02-NSOCIETE                01660008
 "         MOVE COMM-MGV51-NLIEU        TO EC02-NLIEU                   01660008
 "         MOVE COMM-MGV51-NVENTE       TO EC02-NVENTE                  01660008
 "                                                                      01660008
 "         PERFORM SELECT-EC02                                          01660008
 "                                                                              
 "         IF TROUVE                                                    01660008
 "            MOVE EC02-NCDEWC             TO MECRC-NCDEWC              01660008
AL0507        MOVE W-CCEVENT               TO MECRC-CCEVENT                     
V6.5          MOVE W-CCCHOIX               TO MECRC-CCCHOIX                     
 "            MOVE COMM-MECRC              TO Z-COMMAREA-LINK           01660008
 "            MOVE LENGTH OF COMM-MECRC    TO LONG-COMMAREA-LINK        01660008
 "            MOVE 'MECRC'                 TO NOM-PROG-LINK             01590008
 "                                                                      01590008
 "            PERFORM LINK-PROG                                         01670008
 "                                                                      01680008
 "            MOVE Z-COMMAREA-LINK            TO COMM-MECRC             01680008
 "            IF  MECRC-CODE-RET NOT = '00'                             01690008
                  move 'M'              to COMM-MGV51-CODRET                    
 "                MOVE MECRC-MESSAGE    TO COMM-MGV51-LIBERR                    
 "                PERFORM MODULE-SORTIE                                         
 "            END-IF                                                    01750008
 "          END-IF.                                                     03643008
 "                                                                      03644008
 "    *---------------------------------------                          03590008
 "     FIN-TRT-MECRC. EXIT.                                             03647008
 "    *------------------                                               03590008
 "     SELECT-EC02 SECTION.                                             03580008
 "    *------------------                                               03590008
 "         MOVE FUNC-SELECT           TO TRACE-SQL-FUNCTION                     
 "         EXEC SQL                                                     02060008
 "            SELECT        NCDEWC                                      01850008
 "              INTO  :EC02-NCDEWC                                      01660008
 "              FROM  RVEC0203                                          01660008
 "              WHERE NSOCIETE = :EC02-NSOCIETE                         01660008
 "                AND NLIEU    = :EC02-NLIEU                            01660008
 "                AND NVENTE   = :EC02-NVENTE                           01660008
                fetch first 1 row only                                          
 "         END-EXEC                                                     01870008
 "         PERFORM TEST-CODE-RETOUR-SQL.                                02150008
 "                                                                      01660008
 "    *--------------------                                             03590008
 "     FIN-SELECT-EC02. EXIT.                                           03647008
                                                                                
