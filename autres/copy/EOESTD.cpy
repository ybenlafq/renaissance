      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 16:58 >
      
      *****************************************************************
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREPOT        *
      * FICHIER.....: EMISSION OP ENTETE STANDARD (MUTATION)          *
      * NOM FICHIER.: EOESTD                                          *
      *---------------------------------------------------------------*
      * CR   .......: 29/02/2012                                      *
      * MODIFIE.....:   /  /                                          *
      * VERSION N�..: 001                                             *
      * LONGUEUR....: 98                                              *
      *****************************************************************
      *
       01  EOESTD.
      * TYPE ENREGISTREMENT
           05      EOESTD-TYP-ENREG       PIC  X(0006).
      * CODE SOCIETE
           05      EOESTD-CSOCIETE        PIC  X(0005).
      * NUMERO D OP
           05      EOESTD-NOP             PIC  X(0015).
      * NUMERO DE SITE
           05      EOESTD-NSITE           PIC  9(0003).
      * CODE TRANSPORTEUR
           05      EOESTD-CTRANSPORTEUR   PIC  X(0013).
      * MODE EXPEDITION
           05      EOESTD-MODE-EXPED      PIC  X(0003).
      * ETAT
           05      EOESTD-CETAT           PIC  X(0001).
      * DATE DU PASSAGE A L'ETAT
           05      EOESTD-DETAT           PIC  X(0008).
      * HEURE DU PASSAGE A L'ETAT
           05      EOESTD-HETAT           PIC  9(0006).
      * NUMERO D'EXPEDITION
           05      EOESTD-NEXPED          PIC  X(0005).
      * NUMERO DE LA VAGUE
           05      EOESTD-NVAGUE          PIC  X(0006).
      * POIDS
           05      EOESTD-POIDS           PIC  9(0009).
      * VOLUME
           05      EOESTD-VOLUME          PIC  9(0012).
      * NOMBRE DE COLIS
           05      EOESTD-NCOLIS          PIC  9(0005).
      * CODE FIN
           05      EOESTD-FIN             PIC  X(0001).
      
