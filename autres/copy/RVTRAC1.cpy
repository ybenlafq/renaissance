      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 21/10/2016 2        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE TRAC1 TRACA.: IDREGLE AU CODIC         *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTRAC1 .                                                            
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTRAC1 .                                                            
      *}                                                                        
           05  TRAC1-CTABLEG2    PIC X(15).                                     
           05  TRAC1-CTABLEG2-REDEF REDEFINES TRAC1-CTABLEG2.                   
               10  TRAC1-CODIC           PIC X(07).                             
               10  TRAC1-FONCTION        PIC X(03).                             
               10  TRAC1-TCODE           PIC X(02).                             
           05  TRAC1-WTABLEG     PIC X(80).                                     
           05  TRAC1-WTABLEG-REDEF  REDEFINES TRAC1-WTABLEG.                    
               10  TRAC1-IDREGLE         PIC X(10).                             
               10  TRAC1-COMM            PIC X(20).                             
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
      *{ Sql-Host-Variable-Add-Declare-Section 1.0                              
      *01  RVTRAC1-FLAGS.                                                       
      *--                                                                       
       EXEC SQL                                                                 
        BEGIN DECLARE SECTION                                                   
       END-EXEC.                                                                
       01  RVTRAC1-FLAGS.                                                       
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRAC1-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  TRAC1-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  TRAC1-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  TRAC1-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
       EXEC SQL                                                                 
        END DECLARE SECTION                                                     
       END-EXEC.                                                                
      *}                                                                        
