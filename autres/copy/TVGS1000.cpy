      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ******************************************************************        
      * COBOL DECLARATION FOR TABLE PNMD.TTGS10                        *        
      ******************************************************************        
       01  TVGS1000.                                                            
      *                       NSOCDEPOT                                         
           10 GS10C-NSOCDEPOT      PIC X(3).                                    
      *                       NDEPOT                                            
           10 GS10C-NDEPOT         PIC X(3).                                    
      *                       NCODIC                                            
           10 GS10C-NCODIC         PIC X(7).                                    
      *                       WSTOCKMASQ                                        
           10 GS10C-WSTOCKMASQ     PIC X(1).                                    
      *                       NSSLIEU                                           
           10 GS10C-NSSLIEU        PIC X(3).                                    
      *                       QSTOCK                                            
           10 GS10C-QSTOCK         PIC S9(5)V USAGE COMP-3.                     
      *                       QSTOCKRES                                         
           10 GS10C-QSTOCKRES      PIC S9(5)V USAGE COMP-3.                     
      *                       DMAJSTOCK                                         
           10 GS10C-DMAJSTOCK      PIC X(8).                                    
      *                       WARTINC                                           
           10 GS10C-WARTINC        PIC X(1).                                    
      *                       DSYST                                             
           10 GS10C-DSYST          PIC S9(13)V USAGE COMP-3.                    
      *                       XNSOCDEPOT                                        
           10 GS10C-XNSOCDEPOT     PIC X(3).                                    
      *                       XNDEPOT                                           
           10 GS10C-XNDEPOT        PIC X(3).                                    
      *                       XNCODIC                                           
           10 GS10C-XNCODIC        PIC X(7).                                    
      *                       XWSTOCKMASQ                                       
           10 GS10C-XWSTOCKMASQ    PIC X(1).                                    
      *                       XNSSLIEU                                          
           10 GS10C-XNSSLIEU       PIC X(3).                                    
      *                       XQSTOCK                                           
           10 GS10C-XQSTOCK        PIC S9(5)V USAGE COMP-3.                     
      *                       XQSTOCKRES                                        
           10 GS10C-XQSTOCKRES     PIC S9(5)V USAGE COMP-3.                     
      *                       XDMAJSTOCK                                        
           10 GS10C-XDMAJSTOCK     PIC X(8).                                    
      *                       XWARTINC                                          
           10 GS10C-XWARTINC       PIC X(1).                                    
      *                       XDSYST                                            
           10 GS10C-XDSYST         PIC S9(13)V USAGE COMP-3.                    
      *                       IBMSNAP_COMMITSEQ                                 
           10 GS10C-IBMSNAP-COMMITSEQ  PIC X(10).                               
      *                       IBMSNAP_INTENTSEQ                                 
           10 GS10C-IBMSNAP-INTENTSEQ  PIC X(10).                               
      *                       IBMSNAP_OPERATION                                 
           10 GS10C-IBMSNAP-OPERATION  PIC X(1).                                
      *                       IBMSNAP_LOGMARKER                                 
           10 GS10C-IBMSNAP-LOGMARKER  PIC X(26).                               
      *                       NSEQ                                              
           10 GS10C-NSEQ           PIC S9(18)V USAGE COMP-3.                    
      *                       STATUT                                            
           10 GS10C-STATUT         PIC X(10).                                   
      *                       IDENTIFIANT                                       
           10 GS10C-IDENTIFIANT    PIC X(12).                                   
      ******************************************************************        
      * INDICATOR VARIABLE STRUCTURE                                   *        
      ******************************************************************        
       01  TVGS1000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-NSOCDEPOT-F            PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-NSOCDEPOT-F            PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-NDEPOT-F               PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-NDEPOT-F               PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-NCODIC-F               PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-NCODIC-F               PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-WSTOCKMASQ-F           PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-WSTOCKMASQ-F           PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-NSSLIEU-F              PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-NSSLIEU-F              PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-QSTOCK-F               PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-QSTOCK-F               PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-QSTOCKRES-F            PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-QSTOCKRES-F            PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-DMAJSTOCK-F            PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-DMAJSTOCK-F            PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-WARTINC-F              PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-WARTINC-F              PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-DSYST-F                PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-DSYST-F                PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XNSOCDEPOT-F           PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XNSOCDEPOT-F           PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XNDEPOT-F              PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XNDEPOT-F              PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XNCODIC-F              PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XNCODIC-F              PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XWSTOCKMASQ-F          PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XWSTOCKMASQ-F          PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XNSSLIEU-F             PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XNSSLIEU-F             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XQSTOCK-F              PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XQSTOCK-F              PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XQSTOCKRES-F           PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XQSTOCKRES-F           PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XDMAJSTOCK-F           PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XDMAJSTOCK-F           PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XWARTINC-F             PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XWARTINC-F             PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-XDSYST-F               PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-XDSYST-F               PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-IBMSNAP-COMMITSEQ-F    PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-IBMSNAP-COMMITSEQ-F    PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-IBMSNAP-INTENTSEQ-F    PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-IBMSNAP-INTENTSEQ-F    PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-IBMSNAP-OPERATION-F    PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-IBMSNAP-OPERATION-F    PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-IBMSNAP-LOGMARKER-F    PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-IBMSNAP-LOGMARKER-F    PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-NSEQ-F                 PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-NSEQ-F                 PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-STATUT-F               PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-STATUT-F               PIC S9(4) COMP-5.                    
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    10 GS10C-IDENTIFIANT-F          PIC S9(4) COMP.                      
      *--                                                                       
           10 GS10C-IDENTIFIANT-F          PIC S9(4) COMP-5.                    
      *}                                                                        
      ******************************************************************        
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 27      *        
      ******************************************************************        
                                                                                
