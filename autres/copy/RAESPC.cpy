      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      ***************************************************************** 00010000
      * APPLICATION.: WMS - LM7           - GESTION D'ENTREP�T        * 00020000
      * FICHIER.....: R�CEPTION ARTICLE ENT�TE SPECIFIQUE             * 00031000
      * NOM FICHIER.: RAESPC                                          * 00032013
      *---------------------------------------------------------------* 00034000
      * CR   .......: 12/10/2011  17:46:00                            * 00035000
      * MODIFIE.....:   /  /                                          * 00036000
      * VERSION N�..: 001                                             * 00037000
AA0912* LONGUEUR....: 162                                             * 00037117
      ***************************************************************** 00039000
      *                                                                 00040000
       01  RAESPC.                                                      00050000
      * TYPE ENREGISTREMENT : R�CEPTION ARTICLE ENT�TE SP�CIFIQUE       00060006
           05      RAESPC-TYP-ENREG       PIC  X(0006).                 00070000
      * CODE SOCI�T�                                                    00080006
           05      RAESPC-CSOCIETE        PIC  X(0005).                 00090003
      * CODE ARTICLE                                                    00091006
           05      RAESPC-CARTICLE        PIC  X(0018).                 00100003
      * CODE FAMILLE NCG                                                00101006
           05      RAESPC-CFAM            PIC  X(0005).                 00110001
      * CODE MARQUE NCG                                                 00111006
           05      RAESPC-CMARQ           PIC  X(0005).                 00120001
      * DACEM O/N                                                       00121006
           05      RAESPC-WDACEM          PIC  X(0001).                 00130010
      * TYPE COLISAGE DESTOCK A OU I                                    00131006
           05      RAESPC-CFETEMPL        PIC  X(0001).                 00140009
AA0912* COLISAGE DE RECEPTION                                           00140118
AA0912     05      RAESPC-QCOLIRECEPT     PIC  9(0005).                 00140218
      * COLISAGE DE DESTOCKAGE                                          00141006
           05      RAESPC-QCOLIDSTOCK     PIC  9(0005).                 00150001
      * C�T� DE PR�HENSION                                              00151006
           05      RAESPC-CCOTEHOLD       PIC  X(0001).                 00160001
      * NOMBRE DE PI�CES PAR FRONT                                      00160106
           05      RAESPC-QNBRANMAIL      PIC  9(0003).                 00161001
      * NOMBRE DE NIVEAUX DE GERBAGE                                    00161106
           05      RAESPC-QNBNIVGERB      PIC  9(0003).                 00162001
      * SP�CIFICIT� DE STOCKAGE                                         00162106
           05      RAESPC-CSPECIFSTK      PIC  X(0001).                 00163011
      * NOMBRE DE PRODUITS PAR PINC�ES STOCKAGE                         00163106
           05      RAESPC-QNBPVSOL-STK    PIC  9(0005).                 00164002
      * NOMBRE DE PRODUITS PAR PINC�ES PR�PARATION                      00164106
           05      RAESPC-QNBPVSOL-PREP   PIC  9(0005).                 00165002
      * MODE DE STOCKAGE (S OU R : SOL OU RACK)                         00165106
           05      RAESPC-CMODSTOCK       PIC  X(0001).                 00165205
      * DATE PREMI�RE R�CEPTION                                         00165306
           05      RAESPC-D1RECEPT        PIC  X(0008).                 00166002
      * STATUT COMPACTE                                                 00167006
           05      RAESPC-LSTATCOMP       PIC  X(0003).                 00169101
      * CODE QUOTA DE R�CEPTION                                         00169206
           05      RAESPC-CQUOTA          PIC  X(0005).                 00169301
      * CODE APPROVISIONNEMENT                                          00169406
           05      RAESPC-CAPPRO          PIC  X(0005).                 00169501
      * CODE FOURNISSEUR R�GULIER                                       00169606
           05      RAESPC-NENTCDE         PIC  X(0005).                 00170001
      * CODE EAN 2                                                      00170106
           05      RAESPC-EAN2            PIC  X(0013).                 00170314
      * CODE EAN 3                                                      00170406
           05      RAESPC-EAN3            PIC  X(0013).                 00170514
      * CODE EAN 4                                                      00170606
           05      RAESPC-EAN4            PIC  X(0013).                 00170714
      * CODE EAN 5                                                      00170806
           05      RAESPC-EAN5            PIC  X(0013).                 00170914
      * CODE EAN 6                                                      00171006
           05      RAESPC-EAN6            PIC  X(0013).                 00171114
      * FILLER                                                          00171407
           05      RAESPC-FILLER          PIC  X(0001).                 00172008
                                                                                
