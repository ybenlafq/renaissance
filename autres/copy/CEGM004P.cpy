      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------                                 
       TRAITEMENT-EGM004                SECTION.                                
      *----------------------------------------                                 
           INITIALIZE WS-EGM000.                                                
           MOVE ZWC-TYPCHAMP1       TO EGM-TYPC1.                               
           MOVE ZWC-TYPCHAMP2       TO EGM-TYPC2.                               
      *                                                                 00010000
      *----> TRAITEMENT PREALABLE DE STANDARDISATION                    00010000
      *                                                                 00010000
           PERFORM RECUP-LG-PHY.                                                
           PERFORM STANDARDISATION-NUMERIQUE.                                   
           PERFORM STANDARDISATION-ALPHA.                                       
           PERFORM STANDARDISATION-DATE.                                        
           PERFORM STANDARDISATION-K.                                           
      *                                                                 00010000
      *----> COMPARAISON                                                00010000
      *                                                                 00010000
           MOVE '0' TO ZWC-RETCODE                                              
      *                                                                 00010000
      *=========== LES DEUX ZONES SONT ALPHABETIQUES ===========        00010000
      *                                                                 00010000
           IF ( EGM-TYPC1 = 'C' ) AND ( EGM-TYPC2 = 'C' )                       
      *                                                                 00010000
               IF EGM-CONT1(1:EGM-LG-PHY1) = EGM-CONT2(1:EGM-LG-PHY2)           
                   IF ZWC-COMPARAISON = '= ' OR '<=' OR '>='                    
                       MOVE '1' TO ZWC-RETCODE                                  
                   END-IF                                                       
               ELSE                                                             
                   IF EGM-CONT1(1:EGM-LG-PHY1) >                                
                      EGM-CONT2(1:EGM-LG-PHY2)                                  
      *{Post-translation Correct-Sign-not
      *                IF ZWC-COMPARAISON = '^=' OR '> ' OR '>='                
                       IF ZWC-COMPARAISON<> '<>' OR '> ' OR '<>'                
      *}
                           MOVE '1' TO ZWC-RETCODE                              
                       END-IF                                                   
                   ELSE                                                         
      *{Post-translation Correct-Sign-not
      *                IF ZWC-COMPARAISON = '^=' OR '< ' OR '<='                
                       IF ZWC-COMPARAISON<> '<>' OR '< ' OR '<>'                
      *}
                           MOVE '1' TO ZWC-RETCODE                              
                       END-IF                                                   
                   END-IF                                                       
               END-IF                                                           
      *                                                                 00010000
           END-IF.                                                              
      *                                                                 00010000
      *=========== LES DEUX ZONES SONT NUMERIQUES ==============        00010000
      *                                                                 00010000
           IF ( EGM-TYPC1 NOT = 'C' ) AND                                       
                         ( EGM-TYPC2 NOT = 'C' )                                
      *                                                                 00010000
               IF EGM-CHAMP1 = EGM-CHAMP2                                       
                   IF ZWC-COMPARAISON = '= ' OR '<=' OR '>='                    
                       MOVE '1' TO ZWC-RETCODE                                  
                   END-IF                                                       
               ELSE                                                             
                   IF EGM-CHAMP1 > EGM-CHAMP2                                   
      *{Post-translation Correct-Sign-not
      *                IF ZWC-COMPARAISON = '^=' OR '> ' OR '>='                
                       IF ZWC-COMPARAISON<> '<>' OR '> ' OR '<>'                
      *}
                           MOVE '1' TO ZWC-RETCODE                              
                       END-IF                                                   
                   ELSE                                                         
      *{Post-translation Correct-Sign-not
      *                IF ZWC-COMPARAISON = '^=' OR '< ' OR '<='                
                       IF ZWC-COMPARAISON<> '<>' OR '< ' OR '<>'                
      *}
                           MOVE '1' TO ZWC-RETCODE                              
                       END-IF                                                   
                   END-IF                                                       
               END-IF                                                           
      *                                                                 00010000
           END-IF.                                                              
      *                                                                 00010000
      *======= LES DEUX ZONES SONT DE TYPE DIFFERENT =======            00010000
      *                                                                 00010000
           IF ( EGM-TYPC1 = 'C' ) AND ( EGM-TYPC2 NOT = 'C' )                   
               MOVE SPACES TO ZWC-RETCODE                                       
               PERFORM VARYING EGM-I FROM 1 BY 1                                
                       UNTIL   EGM-I > 15                                       
                                OR ZWC-RETCODE > SPACES                         
                   IF EGM-CONT1(EGM-I:1) NOT = EGM-CHAMP2(EGM-I:1)              
                       IF EGM-CONT1(EGM-I:1) > EGM-CHAMP2(EGM-I:1)              
      *{Post-translation Correct-Sign-not
      *                    IF ZWC-COMPARAISON = '^=' OR '> ' OR '>='            
                           IF ZWC-COMPARAISON<> '<>' OR '> ' OR '<>'            
      *}
                               MOVE '1' TO ZWC-RETCODE                          
                           ELSE                                                 
                               MOVE '0' TO ZWC-RETCODE                          
                           END-IF                                               
                       ELSE                                                     
      *{Post-translation Correct-Sign-not
      *                    IF ZWC-COMPARAISON = '^=' OR '< ' OR '<='            
                           IF ZWC-COMPARAISON<> '<>' OR '< ' OR '<>'            
      *}
                               MOVE '1' TO ZWC-RETCODE                          
                           ELSE                                                 
                               MOVE '0' TO ZWC-RETCODE                          
                           END-IF                                               
                       END-IF                                                   
                   END-IF                                                       
               END-PERFORM                                                      
               IF ZWC-RETCODE = SPACES                                          
                   PERFORM VARYING EGM-I FROM 16 BY 1                           
                           UNTIL   EGM-I > 64                                   
                           OR      EGM-CONT1(EGM-I:1) NOT = SPACES              
                   END-PERFORM                                                  
                   IF EGM-I > 64                                                
                       IF ZWC-COMPARAISON = '= ' OR '<=' OR '>='                
                           MOVE '1' TO ZWC-RETCODE                              
                       ELSE                                                     
                           MOVE '0' TO ZWC-RETCODE                              
                       END-IF                                                   
                   ELSE                                                         
                       IF ZWC-COMPARAISON = '= ' OR '< ' OR '<='                
                           MOVE '0' TO ZWC-RETCODE                              
                       ELSE                                                     
                           MOVE '1' TO ZWC-RETCODE                              
                       END-IF                                                   
                   END-IF                                                       
               END-IF                                                           
           END-IF.                                                              
           IF ( EGM-TYPC1      NOT = 'C' ) AND ( EGM-TYPC2 = 'C' )              
               MOVE SPACES TO ZWC-RETCODE                                       
               PERFORM VARYING EGM-I FROM 1 BY 1                                
                       UNTIL   EGM-I > 15                                       
                                OR ZWC-RETCODE > SPACES                         
                   IF EGM-CONT2(EGM-I:1) NOT = EGM-CHAMP1(EGM-I:1)              
                       IF EGM-CONT2(EGM-I:1) > EGM-CHAMP1(EGM-I:1)              
      *{Post-translation Correct-Sign-not
      *                    IF ZWC-COMPARAISON = '^=' OR '< ' OR '<='            
                           IF ZWC-COMPARAISON<> '<>' OR '< ' OR '<>'            
      *}
                               MOVE '1' TO ZWC-RETCODE                          
                           ELSE                                                 
                               MOVE '0' TO ZWC-RETCODE                          
                           END-IF                                               
                       ELSE                                                     
      *{Post-translation Correct-Sign-not
      *                    IF ZWC-COMPARAISON = '^=' OR '> ' OR '>='            
                           IF ZWC-COMPARAISON<> '<>' OR '> ' OR '<>'            
      *}
                               MOVE '1' TO ZWC-RETCODE                          
                           ELSE                                                 
                               MOVE '0' TO ZWC-RETCODE                          
                           END-IF                                               
                       END-IF                                                   
                   END-IF                                                       
               END-PERFORM                                                      
               IF ZWC-RETCODE = SPACES                                          
                   PERFORM VARYING EGM-I FROM 16 BY 1                           
                           UNTIL   EGM-I > 64                                   
                           OR      EGM-CONT2(EGM-I:1) NOT = SPACES              
                   END-PERFORM                                                  
                   IF EGM-I > 64                                                
                       IF ZWC-COMPARAISON = '= ' OR '<=' OR '>='                
                           MOVE '1' TO ZWC-RETCODE                              
                       ELSE                                                     
                           MOVE '0' TO ZWC-RETCODE                              
                       END-IF                                                   
                   ELSE                                                         
                       IF ZWC-COMPARAISON = '= ' OR '> ' OR '>='                
                           MOVE '0' TO ZWC-RETCODE                              
                       ELSE                                                     
                           MOVE '1' TO ZWC-RETCODE                              
                       END-IF                                                   
                   END-IF                                                       
               END-IF                                                           
           END-IF.                                                              
      *                                                                 00010000
      *===============================================================* 00020000
       STANDARDISATION-ALPHA         SECTION.                           00030000
      *===============================================================* 00040000
           IF EGM-TYPC1 = 'C'                                                   
               MOVE ZWC-CONTENU1(1:EGM-LG-PHY1) TO EGM-CONT1                    
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC2 = 'C'                                                   
               MOVE ZWC-CONTENU2(1:EGM-LG-PHY2) TO EGM-CONT2                    
           END-IF.                                                              
      *===============================================================* 00020000
       STANDARDISATION-NUMERIQUE     SECTION.                           00030000
      *===============================================================* 00040000
      *                                                                 00050000
      *-------> STANDARDISATION DU PREMIER CHAMP                        00050000
      *                                                                 00010000
           IF EGM-TYPC1 =      'Z' OR 'N'                                       
               MOVE ZWC-CONTENU1(1:EGM-LG-PHY1) TO EGM-CHAMP1                   
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC1 = 'D'                                                   
               MOVE ZWC-CONTENU1(1:EGM-LG-PHY1) TO EGM-Z-DOUBLEWORD             
               MOVE EGM-DOUBLEWORD          TO EGM-CHAMP1                       
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC1 = 'F'                                                   
               MOVE ZWC-CONTENU1(1:EGM-LG-PHY1) TO EGM-Z-FULLWORD               
               MOVE EGM-FULLWORD            TO EGM-CHAMP1                       
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC1 = 'H'                                                   
               MOVE ZWC-CONTENU1(1:EGM-LG-PHY1) TO EGM-Z-HALFWORD               
               MOVE EGM-HALFWORD            TO EGM-CHAMP1                       
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC1 = 'P' OR '?'                                            
               MOVE 0 TO EGM-CHPR                                               
               COMPUTE EGM-K = 9 - EGM-LG-PHY1                                  
               MOVE ZWC-CONTENU1(1:EGM-LG-PHY1) TO                              
                    EGM-CHPAR(EGM-K:EGM-LG-PHY1)                                
               MOVE EGM-CHPR                TO EGM-CHAMP1                       
           END-IF.                                                              
      *                                                                 00050000
      *-------> STANDARDISATION DU SECOND CHAMP                         00050000
      *                                                                 00010000
           IF EGM-TYPC2 =      'Z' OR 'N'                                       
               MOVE ZWC-CONTENU2(1:EGM-LG-PHY2) TO EGM-CHAMP2                   
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC2 = 'D'                                                   
               MOVE ZWC-CONTENU2(1:EGM-LG-PHY1) TO EGM-Z-DOUBLEWORD             
               MOVE EGM-DOUBLEWORD          TO EGM-CHAMP2                       
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC2 = 'F'                                                   
               MOVE ZWC-CONTENU2(1:EGM-LG-PHY1) TO EGM-Z-FULLWORD               
               MOVE EGM-FULLWORD            TO EGM-CHAMP2                       
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC2 = 'H'                                                   
               MOVE ZWC-CONTENU2(1:EGM-LG-PHY1) TO EGM-Z-HALFWORD               
               MOVE EGM-HALFWORD            TO EGM-CHAMP2                       
           END-IF.                                                              
      *                                                                 00010000
           IF EGM-TYPC2 = 'P' OR '?'                                            
               MOVE 0 TO EGM-CHPR                                               
               COMPUTE EGM-K = 9 - EGM-LG-PHY2                                  
               MOVE ZWC-CONTENU2(1:EGM-LG-PHY2) TO                              
                    EGM-CHPAR(EGM-K:EGM-LG-PHY2)                                
               MOVE EGM-CHPR                TO EGM-CHAMP2                       
           END-IF.                                                              
      *                                                                 00050000
      *-------> ON MET LE MEME NOMBRE DE CHIFFRES APRES LES VIRGULES    00050000
      *                                                                 00010000
           IF ZWC-DECCHAMP1 > ZWC-DECCHAMP2                                     
               COMPUTE EGM-ECART = ZWC-DECCHAMP1 - ZWC-DECCHAMP2                
               COMPUTE EGM-CHAMP2 = EGM-CHAMP2 * (10 ** EGM-ECART)              
           ELSE                                                                 
               COMPUTE EGM-ECART = ZWC-DECCHAMP2 - ZWC-DECCHAMP1                
               COMPUTE EGM-CHAMP1 = EGM-CHAMP1 * (10 ** EGM-ECART)              
           END-IF.                                                              
      *                                                                 00010000
      *===============================================================* 00020000
       STANDARDISATION-DATE          SECTION.                           00030000
      *===============================================================* 00040000
      *                                                                 00050000
           IF EGM-TYPC1 = 'S'                                                   
           OR EGM-TYPC1 = '*'                                                   
              MOVE ZWC-CONTENU1(1:8) TO EGM-CONT1                               
              MOVE 'C'               TO EGM-TYPC1                               
           END-IF.                                                              
      *                                                                 00050000
           IF EGM-TYPC2 = 'S'                                                   
           OR EGM-TYPC2 = '*'                                                   
              MOVE ZWC-CONTENU2(1:8) TO EGM-CONT2                               
              MOVE 'C'               TO EGM-TYPC2                               
           END-IF.                                                              
      *                                                                 00050000
           IF EGM-TYPC1 = 'Y'                                                   
              MOVE ZWC-CONTENU1(1:2) TO EGM-I                                   
              IF EGM-I > 50                                                     
                 MOVE '19'              TO EGM-Z-DATE(1:2)                      
              ELSE                                                              
                 MOVE '20'              TO EGM-Z-DATE(1:2)                      
              END-IF                                                            
              MOVE ZWC-CONTENU1(1:6) TO EGM-Z-DATE(3:6)                         
              MOVE EGM-Z-DATE        TO EGM-CONT1                               
              MOVE 8                 TO EGM-LG-PHY1                             
              MOVE 'C'               TO EGM-TYPC1                               
           END-IF.                                                              
      *                                                                 00050000
           IF EGM-TYPC2 = 'Y'                                                   
              MOVE ZWC-CONTENU2(1:2) TO EGM-I                                   
              IF EGM-I > 50                                                     
                 MOVE '19'              TO EGM-Z-DATE(1:2)                      
              ELSE                                                              
                 MOVE '20'              TO EGM-Z-DATE(1:2)                      
              END-IF                                                            
              MOVE ZWC-CONTENU2(1:6) TO EGM-Z-DATE(3:6)                         
              MOVE EGM-Z-DATE        TO EGM-CONT2                               
              MOVE 8                 TO EGM-LG-PHY2                             
              MOVE 'C'               TO EGM-TYPC2                               
           END-IF.                                                              
      *                                                                 00010000
      *===============================================================* 00020000
       STANDARDISATION-K       SECTION.                                 00030000
      *===============================================================* 00040000
      *                                                                 00050000
           IF EGM-TYPC1 = 'K'                                                   
               MOVE ZWC-CONTENU1(2:EGM-LG-PHY1) TO EGM-CONT1                    
               IF EGM-TYPC2 = 'C'                                               
                   MOVE 'C'                  TO EGM-TYPC1                       
               ELSE                                                             
                   MOVE 'Z'                  TO EGM-TYPC1                       
                   MOVE 0                    TO EGM-CHAMP1                      
                   COMPUTE EGM-K = 16 - EGM-LG-PHY1                             
                   MOVE EGM-CONT1 (1:EGM-LG-PHY1) TO                            
                        EGM-CH1 (EGM-K:EGM-LG-PHY1)                             
                   COMPUTE EGM-CHAMP1 = EGM-CHAMP1 * +1                         
               END-IF                                                           
           END-IF.                                                              
      *                                                                 00050000
           IF EGM-TYPC2 = 'K'                                                   
               MOVE ZWC-CONTENU2(2:EGM-LG-PHY2) TO EGM-CONT2                    
               IF EGM-TYPC1 = 'C'                                               
                   MOVE 'C'                  TO EGM-TYPC2                       
               ELSE                                                             
                   MOVE 'Z'                  TO EGM-TYPC2                       
                   MOVE 0                    TO EGM-CHAMP2                      
                   COMPUTE EGM-K = 16 - EGM-LG-PHY2                             
                   MOVE EGM-CONT2 (1:EGM-LG-PHY2) TO                            
                        EGM-CH2 (EGM-K:EGM-LG-PHY2)                             
                   COMPUTE EGM-CHAMP2 = EGM-CHAMP2 * +1                         
               END-IF                                                           
           END-IF.                                                              
      *                                                                 00010000
      *===============================================================* 00020000
       RECUP-LG-PHY            SECTION.                                 00030000
      *===============================================================* 00040000
      *                                                                 00050000
           MOVE ZWC-CONTENU1  TO EGM-CONT-CH.                                   
           MOVE EGM-TYPC1 TO EGM-TYPE-CH.                                       
           MOVE ZWC-LGCHAMP1  TO EGM-LONG-CH.                                   
           PERFORM CALCUL-LG-PHY.                                               
           MOVE EGM-LG-PHY    TO EGM-LG-PHY1.                                   
      *                                                                 00050000
           MOVE ZWC-CONTENU2  TO EGM-CONT-CH.                                   
           MOVE EGM-TYPC2 TO EGM-TYPE-CH.                                       
           MOVE ZWC-LGCHAMP2  TO EGM-LONG-CH.                                   
           PERFORM CALCUL-LG-PHY.                                               
           MOVE EGM-LG-PHY    TO EGM-LG-PHY2.                                   
      *                                                                 00010000
      *===============================================================* 00020000
       CALCUL-LG-PHY           SECTION.                                 00030000
      *===============================================================* 00040000
      *                                                                 00050000
           IF EGM-TYPE-CH = '?' OR 'P' OR '*'                           00060000
               COMPUTE EGM-LG-PHY ROUNDED = ( EGM-LONG-CH + 1 ) / 2     00070000
           END-IF.                                                      00080000
      *                                                                 00090000
           IF EGM-TYPE-CH = 'C' OR 'Z' OR 'N'                           00100000
               MOVE EGM-LONG-CH TO EGM-LG-PHY                           00110000
           END-IF.                                                      00120000
      *                                                                 00130000
           IF EGM-TYPE-CH = 'H'                                         00140000
               MOVE 2             TO EGM-LG-PHY                         00150000
           END-IF.                                                      00160000
      *                                                                 00170000
           IF EGM-TYPE-CH = 'F'                                         00180000
               MOVE 4             TO EGM-LG-PHY                         00190000
           END-IF.                                                      00200000
      *                                                                 00210000
           IF EGM-TYPE-CH = 'D' OR 'S'                                  00220000
               MOVE 8             TO EGM-LG-PHY                         00230000
           END-IF.                                                      00240000
      *                                                                 00250000
           IF EGM-TYPE-CH = 'Y'                                         00260000
               MOVE 6             TO EGM-LG-PHY                         00270000
           END-IF.                                                      00280000
      *                                                                 00250000
           IF EGM-TYPE-CH = 'K'                                         00260000
               PERFORM VARYING EGM-I FROM 2 BY 1                                
                       UNTIL   EGM-CONT-CH(EGM-I:1) = ''''                      
               END-PERFORM                                                      
               COMPUTE  EGM-LG-PHY = EGM-I - 2                          00270000
           END-IF.                                                      00280000
       FIN-CALCUL-LG-PHY.                                               00290000
      *                                                                 00300000
                                                                                
