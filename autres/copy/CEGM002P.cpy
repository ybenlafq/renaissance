      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 13/06/2016 1        
                                                                                
      *----------------------------------------                                 
       TRAITEMENT-EGM002                SECTION.                                
      *----------------------------------------                                 
           INITIALIZE WS-EGM000.                                                
           IF ZWM-TYPCHAMP = 'Y'                                                
              MOVE ZWM-CONTENU (1:6)  TO GFAMJ-1                                
              MOVE '7'                TO GFDATA                                 
              PERFORM APPEL-CALCUL-DATE                                         
              MOVE GFSAMJ-0           TO ZWM-CONTENU                            
           ELSE                                                                 
              MOVE ZWM-CONTENU (1:8)  TO GFSAMJ-0                               
              MOVE '5'                TO GFDATA                                 
              PERFORM APPEL-CALCUL-DATE                                         
           END-IF.                                                              
           IF GFVDAT = '1'                                                      
              PERFORM TRAITEMENT-DU-MASQUE                                      
              MOVE EGM-CHAMP      TO ZWM-MASQUE                                 
           ELSE                                                                 
              MOVE SPACES         TO ZWM-MASQUE                                 
           END-IF.                                                              
      *----------------------------------------                                 
       TRAITEMENT-DU-MASQUE             SECTION.                                
      *----------------------------------------                                 
           MOVE 1              TO EGM-I.                                        
           MOVE SPACES         TO EGM-CHAMP.                                    
           PERFORM VARYING EGM-K FROM 1 BY 1                                    
                   UNTIL   EGM-K > ZWM-LGMASQUE                                 
              EVALUATE ZWM-MASQUE (EGM-K:1)                                     
                 WHEN 'J'                                                       
                    PERFORM TRAITEMENT-JOUR                                     
                 WHEN 'M'                                                       
                    PERFORM TRAITEMENT-MOIS                                     
                 WHEN 'S'                                                       
                    PERFORM TRAITEMENT-SIECLE                                   
                 WHEN 'A'                                                       
                    PERFORM TRAITEMENT-ANNEE                                    
                 WHEN OTHER                                                     
                    PERFORM TRAITEMENT-CARACTERES                               
              END-EVALUATE                                                      
           END-PERFORM.                                                         
      *----------------------------------------                                 
       TRAITEMENT-JOUR                  SECTION.                                
      *----------------------------------------                                 
           IF ZWM-MASQUE (EGM-K:3) = 'JJJ'                                      
              MOVE 1             TO EGM-J                                       
              IF ZWM-MASQUE (EGM-K - 1:1) = '<'                                 
                 PERFORM VARYING EGM-K FROM EGM-K BY 1                          
                         UNTIL   EGM-K > ZWM-LGMASQUE                           
                         OR ZWM-MASQUE (EGM-K:1) NOT = 'J'                      
                         OR EGM-J > 8                                           
                         OR GFSMN-LIB-L (EGM-J:1) = ' '                         
                    ADD  1          TO EGM-J                                    
                 END-PERFORM                                                    
              ELSE                                                              
                 PERFORM VARYING EGM-K FROM EGM-K BY 1                          
                         UNTIL   EGM-K > ZWM-LGMASQUE                           
                         OR ZWM-MASQUE (EGM-K:1) NOT = 'J'                      
                         OR EGM-J > 8                                           
                    ADD  1          TO EGM-J                                    
                 END-PERFORM                                                    
              END-IF                                                            
              COMPUTE EGM-J         = EGM-J - 1                                 
              MOVE GFSMN-LIB-L (1:EGM-J) TO EGM-CHAMP (EGM-I:EGM-J)             
              ADD  EGM-J            TO EGM-I                                    
           ELSE                                                                 
              IF ZWM-CONTENU (7:1) = '0'                                        
                 IF ZWM-MASQUE (EGM-K - 1:1) NOT = '<'                          
                    MOVE ZWM-CONTENU (7:1) TO EGM-CHAMP (EGM-I:1)               
                    ADD  1             TO EGM-I                                 
                 END-IF                                                         
              ELSE                                                              
                 MOVE ZWM-CONTENU (7:1) TO EGM-CHAMP (EGM-I:1)                  
                 ADD  1             TO EGM-I                                    
              END-IF                                                            
              MOVE ZWM-CONTENU (8:1) TO EGM-CHAMP (EGM-I:1)                     
              ADD  1             TO EGM-I                                       
           END-IF.                                                              
           PERFORM VARYING EGM-K FROM EGM-K BY 1                                
                   UNTIL   EGM-K > ZWM-LGMASQUE                                 
                   OR ZWM-MASQUE (EGM-K:1) NOT = 'J'                            
           END-PERFORM.                                                         
           SUBTRACT 1           FROM EGM-K.                                     
      *----------------------------------------                                 
       TRAITEMENT-MOIS                  SECTION.                                
      *----------------------------------------                                 
           IF ZWM-MASQUE (EGM-K:3) = 'MMM'                                      
              MOVE 1             TO EGM-J                                       
              IF ZWM-MASQUE (EGM-K - 1:1) = '<'                                 
                 PERFORM VARYING EGM-K FROM EGM-K BY 1                          
                         UNTIL EGM-K > ZWM-LGMASQUE                             
                         OR ZWM-MASQUE (EGM-K:1) NOT = 'M'                      
                         OR EGM-J > 9                                           
                         OR GFMOI-LIB-L (EGM-J:1) = ' '                         
                    ADD  1          TO EGM-J                                    
                 END-PERFORM                                                    
              ELSE                                                              
                 PERFORM VARYING EGM-K FROM EGM-K BY 1                          
                         UNTIL EGM-K > ZWM-LGMASQUE                             
                         OR ZWM-MASQUE (EGM-K:1) NOT = 'M'                      
                         OR EGM-J > 9                                           
                    ADD  1          TO EGM-J                                    
                 END-PERFORM                                                    
              END-IF                                                            
              COMPUTE EGM-J         = EGM-J - 1                                 
              IF EGM-J = 3                                                      
                 MOVE GFMOI-LIB-C (1:EGM-J) TO EGM-CHAMP (EGM-I:EGM-J)          
              ELSE                                                              
                 MOVE GFMOI-LIB-L (1:EGM-J) TO EGM-CHAMP (EGM-I:EGM-J)          
              END-IF                                                            
              ADD  EGM-J         TO EGM-I                                       
           ELSE                                                                 
              IF ZWM-CONTENU (5:1) = '0'                                        
                 IF ZWM-MASQUE (EGM-K - 1:1) NOT = '<'                          
                    MOVE ZWM-CONTENU (5:1) TO EGM-CHAMP (EGM-I:1)               
                    ADD  1             TO EGM-I                                 
                 END-IF                                                         
              ELSE                                                              
                 MOVE ZWM-CONTENU (5:1) TO EGM-CHAMP (EGM-I:1)                  
                 ADD  1             TO EGM-I                                    
              END-IF                                                            
              MOVE ZWM-CONTENU (6:1) TO EGM-CHAMP (EGM-I:1)                     
              ADD  1             TO EGM-I                                       
           END-IF.                                                              
           PERFORM VARYING EGM-K FROM EGM-K BY 1                                
                   UNTIL EGM-K > ZWM-LGMASQUE                                   
                   OR ZWM-MASQUE (EGM-K:1) NOT = 'M'                            
           END-PERFORM.                                                         
           SUBTRACT 1           FROM EGM-K.                                     
      *----------------------------------------                                 
       TRAITEMENT-ANNEE                 SECTION.                                
      *----------------------------------------                                 
           MOVE ZWM-CONTENU (3:2) TO EGM-CHAMP (EGM-I:2)                        
           ADD  2             TO EGM-I                                          
           PERFORM VARYING EGM-K FROM EGM-K BY 1                                
                   UNTIL EGM-K > ZWM-LGMASQUE                                   
                   OR ZWM-MASQUE (EGM-K:1) NOT = 'A'                            
           END-PERFORM.                                                         
           SUBTRACT 1           FROM EGM-K.                                     
      *----------------------------------------                                 
       TRAITEMENT-SIECLE                SECTION.                                
      *----------------------------------------                                 
           MOVE ZWM-CONTENU (1:2) TO EGM-CHAMP (EGM-I:2)                        
           ADD  2             TO EGM-I                                          
           PERFORM VARYING EGM-K FROM EGM-K BY 1                                
                   UNTIL EGM-K > ZWM-LGMASQUE                                   
                   OR ZWM-MASQUE (EGM-K:1) NOT = 'S'                            
           END-PERFORM.                                                         
           SUBTRACT 1           FROM EGM-K.                                     
      *----------------------------------------                                 
       TRAITEMENT-CARACTERES            SECTION.                                
      *----------------------------------------                                 
           IF ZWM-MASQUE (EGM-K:1) NOT = '<'                                    
              MOVE ZWM-MASQUE (EGM-K:1) TO EGM-CHAMP (EGM-I:1)                  
              ADD  1             TO EGM-I                                       
           END-IF.                                                              
                                                                                
