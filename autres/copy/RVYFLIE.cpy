      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE YFLIE MIGRATION FILIALE: LIEUX         *        
      *----------------------------------------------------------------*        
       01  RVYFLIE.                                                             
           05  YFLIE-CTABLEG2    PIC X(15).                                     
           05  YFLIE-CTABLEG2-REDEF REDEFINES YFLIE-CTABLEG2.                   
               10  YFLIE-TYPL            PIC X(01).                             
               10  YFLIE-AFL             PIC X(07).                             
               10  YFLIE-ATRT            PIC X(05).                             
           05  YFLIE-WTABLEG     PIC X(80).                                     
           05  YFLIE-WTABLEG-REDEF  REDEFINES YFLIE-WTABLEG.                    
               10  YFLIE-NFL             PIC X(07).                             
               10  YFLIE-NTRT            PIC X(05).                             
               10  YFLIE-DACTIF          PIC X(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVYFLIE-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  YFLIE-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  YFLIE-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  YFLIE-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  YFLIE-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
