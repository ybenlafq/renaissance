      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      **********************************************************                
      *   COPY DE LA TABLE RVRF0000                                             
      **********************************************************                
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES HOST VARIABLES DE LA TABLE RVRF0000                         
      *---------------------------------------------------------                
      *                                                                         
       01  RVRF0000.                                                            
           02  RF00-NSOC                                                        
               PIC X(0003).                                                     
           02  RF00-NDEPOT                                                      
               PIC X(0003).                                                     
           02  RF00-NENTCDE                                                     
               PIC X(0005).                                                     
           02  RF00-CFAM                                                        
               PIC X(0005).                                                     
           02  RF00-NCODIC                                                      
               PIC X(0007).                                                     
           02  RF00-QNDELAPP                                                    
               PIC S9(3) COMP-3.                                                
           02  RF00-FBACKORDERS                                                 
               PIC X(0001).                                                     
           02  RF00-QNSTKSECU                                                   
               PIC S9(3) COMP-3.                                                
           02  RF00-QNWEEKREAPP                                                 
               PIC S9(2) COMP-3.                                                
           02  RF00-FREAPPAUTO                                                  
               PIC X(0001).                                                     
           02  RF00-DSYST                                                       
               PIC S9(13) COMP-3.                                               
      *                                                                         
      *---------------------------------------------------------                
      *   LISTE DES FLAGS DE LA TABLE RVRF0000                                  
      *---------------------------------------------------------                
      *                                                                         
       01  RVRF0000-FLAGS.                                                      
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-NSOC-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-NSOC-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-NDEPOT-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-NDEPOT-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-NENTCDE-F                                                   
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-NENTCDE-F                                                   
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-CFAM-F                                                      
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-CFAM-F                                                      
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-NCODIC-F                                                    
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-NCODIC-F                                                    
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-QNDELAPP-F                                                  
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-QNDELAPP-F                                                  
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-FBACKORDERS-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-FBACKORDERS-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-QNSTKSECU-F                                                 
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-QNSTKSECU-F                                                 
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-QNWEEKREAPP-F                                               
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-QNWEEKREAPP-F                                               
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-FREAPPAUTO-F                                                
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-FREAPPAUTO-F                                                
               PIC S9(4) COMP-5.                                                
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    02  RF00-DSYST-F                                                     
      *        PIC S9(4) COMP.                                                  
      *--                                                                       
           02  RF00-DSYST-F                                                     
               PIC S9(4) COMP-5.                                                
      *}                                                                        
       EJECT                                                                    
                                                                                
