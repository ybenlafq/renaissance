      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
                                                                                
      *----------------------------------------------------------------*        
      *  DSECT DU FICHIER D'EXTRACTION DE L'ETAT B3E003                *        
      *             ECOTAXE D3E - EXTRACTION MENSUELLE                 *        
      *                                                                *        
      *  SUIVI DES VENTES TYPE IMPORT FILIALE DIF                      *        
      *  SUIVI DES MUTATIONS VERS FILIALES ETRANGERES                  *        
      *          CRITERES DE TRI  WSEQFAM                              *        
      *                           CMARQ                                *        
      *                           REF                                  *        
      *                           NCODIC                               *        
      *----------------------------------------------------------------*        
       01  DSECT-J3E003.                                                        
         05 CHAMPS-J3E003.                                                      
           10 J3E003-CFAM               PIC X(05).                      122  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-CMARQ              PIC X(06).                      094  006
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-LREFFOURN          PIC X(20).                      100  020
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-NCODIC             PIC X(07).                      197  007
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-CNOMDOU            PIC X(14).                      127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-D3EHT              PIC 999,9999.                   207  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-QVENDUE            PIC -9(05).                     048  003
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-TOTD3EHT           PIC -99999,999.                 207  005
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-PERIODE            PIC X(6).                       127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-TYPE               PIC X(5).                       127  015
           10 FILLER                    PIC X VALUE ';'.                007  002
           10 J3E003-WSEQFAM            PIC 9(05).                      048  003
         05 FILLER                      PIC X(028).                             
       01 J3E003-LIBELLES.                                                      
         05 FILLER                      PIC X(05) VALUE 'CFAM'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(06) VALUE 'CMARQ'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(20) VALUE 'LREFFOURN'.            
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'NCODIC'.               
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(14) VALUE 'CNOMDOU'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'D3EHT'.                
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'QVENDUE'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(08) VALUE 'TOTD3EHT'.             
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'PERIODE'.              
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(05) VALUE 'TYPE'.                 
         05 FILLER                      PIC X(01) VALUE ';'.                    
         05 FILLER                      PIC X(07) VALUE 'WSEQFAM'.              
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *01  DSECT-J3E003-LONG           PIC S9(4)   COMP  VALUE +102.            
      *                                                                         
      *--                                                                       
       01  DSECT-J3E003-LONG           PIC S9(4) COMP-5  VALUE +102.            
                                                                                
      *}                                                                        
