      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 23/08/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE SOCRD ACTIV. BAR. CR�DIT NIV. GROUPE   *        
      *----------------------------------------------------------------*        
       01  RVSOCRD.                                                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  SOCRD-CTABLEG2    PIC X(15).                                     
           05  SOCRD-CTABLEG2-REDEF REDEFINES SOCRD-CTABLEG2.                   
               10  SOCRD-CBAREME         PIC X(07).                             
               10  SOCRD-NSOC            PIC X(03).                             
               10  SOCRD-NSEQ            PIC X(02).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  SOCRD-NSEQ-N         REDEFINES SOCRD-NSEQ                    
                                         PIC 9(02).                             
       EXEC SQL BEGIN DECLARE SECTION END-EXEC.
           05  SOCRD-WTABLEG     PIC X(80).                                     
           05  SOCRD-WTABLEG-REDEF  REDEFINES SOCRD-WTABLEG.                    
               10  SOCRD-DEFFET          PIC X(08).                             
       EXEC SQL END DECLARE SECTION END-EXEC.
               10  SOCRD-DEFFET-N       REDEFINES SOCRD-DEFFET                  
                                         PIC 9(08).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVSOCRD-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SOCRD-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  SOCRD-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  SOCRD-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  SOCRD-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
