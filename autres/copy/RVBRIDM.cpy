      *@ @(#) MetaWare Technologies Cobol-Translater 0.9.16 26/07/2016 1        
      *----------------------------------------------------------------*        
      *    VUE DE LA SOUS-TABLE BRIDM BRIDAGE TRANSACTION SAISIE MUT   *        
      *----------------------------------------------------------------*        
       01  RVBRIDM .                                                            
           05  BRIDM-CTABLEG2    PIC X(15).                                     
           05  BRIDM-CTABLEG2-REDEF REDEFINES BRIDM-CTABLEG2.                   
               10  BRIDM-DEPOT           PIC X(06).                             
               10  BRIDM-CSELART         PIC X(05).                             
               10  BRIDM-JOUR            PIC X(03).                             
           05  BRIDM-WTABLEG     PIC X(80).                                     
           05  BRIDM-WTABLEG-REDEF  REDEFINES BRIDM-WTABLEG.                    
               10  BRIDM-QVOLUM          PIC X(03).                             
               10  BRIDM-QPIECE          PIC X(03).                             
               10  BRIDM-WACTIF          PIC X(01).                             
      *----------------------------------------------------------------*        
      *    FLAGS DE LA VUE                                             *        
      *----------------------------------------------------------------*        
       01  RVBRIDM-FLAGS.                                                       
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BRIDM-CTABLEG2-F  PIC S9(4)  COMP.                               
      *--                                                                       
           05  BRIDM-CTABLEG2-F  PIC S9(4) COMP-5.                              
      *}                                                                        
      *{ convert-comp-comp4-binary-to-comp5 1.8                                 
      *    05  BRIDM-WTABLEG-F   PIC S9(4)  COMP.                               
      *                                                                         
      *--                                                                       
           05  BRIDM-WTABLEG-F   PIC S9(4) COMP-5.                              
                                                                                
      *}                                                                        
