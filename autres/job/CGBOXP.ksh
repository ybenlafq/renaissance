#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CGBOXP.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCGBOX -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/09/19 AT 11.34.50 BY BURTECA                      
#    STANDARDS: P  JOBSET: CGBOXP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='FILIALES'                                                          
# ********************************************************************         
#  TRI DES FICHIERS ICS KAPRO                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CGBOXPA
       ;;
(CGBOXPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RAAK=${RAAK:-CGBOXPAG}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/09/19 AT 11.34.50 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CGBOXP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'CONVERGENCE'                           
# *                           APPL...: REPCAPRO                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CGBOXPAA
       ;;
(CGBOXPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# *********************************                                            
# *****   FICHIER ISSU DE FV001K                                               
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F44.FV001K1
# *****   FICHIER ISSU DE FTI01K                                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.FTI01K2
# ******* FICHIER DE RECYCLAGE                                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F44.CGICSP0K
# ******* FICHIER DE REPRISE                                                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F44.CGICSPRK.REPRISE
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CGBOXPAA.CGICSP1K
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_235_1 235 CH 1
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_186_3 186 CH 3
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_235_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGBOXPAB
       ;;
(CGBOXPAB)
       m_CondExec 00,EQ,CGBOXPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG010                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGBOXPAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPAD
       ;;
(CGBOXPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FIC ISSU DU TRI                                                       
       m_FileAssign -d SHR -g ${G_A1} FFTV02 ${DATA}/PTEM/CGBOXPAA.CGICSP1K
# ******  TABLE EN LECTURE                                                     
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSFM99   : NAME=RSFM99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFM99 /dev/null
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
# ****** FICHIERS EN SORTIE POUR LE BCG020 ET LE BCG030                        
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FICANO ${DATA}/PTEM/CGBOXPAD.CGICSP2K
# ****** FICHIERS EN SORTIE POUR LE PGM BCG020                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FCG010 ${DATA}/PTEM/CGBOXPAD.CGICSP3K
# ------  ETAT DES ANOMALIES                                                   
       m_OutputAssign -c 9 -w ICG010 ICG010
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG010 
       JUMP_LABEL=CGBOXPAE
       ;;
(CGBOXPAE)
       m_CondExec 04,GE,CGBOXPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BCG020                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGBOXPAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPAG
       ;;
(CGBOXPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FICANO ${DATA}/PTEM/CGBOXPAD.CGICSP2K
       m_FileAssign -d SHR -g ${G_A3} FCG010 ${DATA}/PTEM/CGBOXPAD.CGICSP3K
# ****** FICHIER  EN SORTIE VER XFB GATEWAY                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 FCG020 ${DATA}/PXX0/F44.CGBOXP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG020 
       JUMP_LABEL=CGBOXPAH
       ;;
(CGBOXPAH)
       m_CondExec 04,GE,CGBOXPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG030                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGBOXPAJ PGM=BCG030     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPAJ
       ;;
(CGBOXPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A4} FFTV02 ${DATA}/PTEM/CGBOXPAA.CGICSP1K
       m_FileAssign -d SHR -g ${G_A5} FICANO ${DATA}/PTEM/CGBOXPAD.CGICSP2K
# ****** FICHIER  EN SORTIE POUR RECYCLAGE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FREPRIS ${DATA}/PXX0/F44.CGICSP0K
       m_ProgramExec BCG030 
#                                                                              
# ********************************************************************         
#  PGM : EASYTRIEVE                                                            
#  -----------------                                                           
#  FICHIER _A ENVOYER _A BT                                                      
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGBOXPAM PGM=EZTPA00    ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPAM
       ;;
(CGBOXPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d NEW EZTVFM ${MT_TMP}/EZTVFM_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A6} FILEA ${DATA}/PXX0/F44.CGBOXP
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F44.CGICKP1
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/CGBOXPAM
       m_ProgramExec CGBOXPAM
# ********************************************************************         
#  ENVOI DU FICHIER VERS GATEWAY POUR ROUTAGE VERS MACHINE SAP DARTY           
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=IEFBR14,PATTERN=CFT,COND1=(03,GT,_AAAK)                    
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SAPICEP,                                                            
#      FNAME=":CGBOXP""(0),                                                    
#      NFNAME=CGBOXP                                                           
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI DU FICHIER VERS GATEWAY POUR ROUTAGE VERS MACHINE SAP BOUYGUE         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABE      STEP  PGM=IEFBR14,PATTERN=CFT,COND1=(03,GT,_AAAK)                    
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=CGICKP1,                                                            
#      FNAME=":CGICKP1""(0)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCGBOXP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAZ      STEP  PGM=FTP,PARM='(EXIT',COND1=(03,GT,_AAAK)                       
#                                                                              
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# IPO1GTW                                                                      
# IPO1GTW                                                                      
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
# PUT 'PXX0.F44.CGBOXP(0)' CGBOXP                                              
# QUIT                                                                         
#          DATAEND                                                             
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
# ABE      STEP  PGM=EZACFSM1,COND1=(03,GT,_AAAK)                               
# SYSOUT   FILE  NAME=FLOGFTP,MODE=N                                           
# SYSIN    DATA  *                                                             
# &LYYMMDD &LHR:&LMIN:&LSEC &JOBNAME CGBOXP                                    
#         DATAEND                                                              
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCGBOXP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGBOXPAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPAQ
       ;;
(CGBOXPAQ)
       m_CondExec ${EXAAZ},NE,YES 03,GT,$[RAAK] 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/CGBOXPAQ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP CGBOXPAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPAT
       ;;
(CGBOXPAT)
       m_CondExec ${EXABE},NE,YES 03,GT,$[RAAK] 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGBOXPAT.sysin
       m_ProgramExec EZACFSM1
# ******************************************************************           
#  REMISE A ZERO DU FICHIER DE REPRISE                                         
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP CGBOXPAX PGM=IDCAMS     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPAX
       ;;
(CGBOXPAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F44.CGICSPRK.REPRISE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGBOXPAX.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CGBOXPAY
       ;;
(CGBOXPAY)
       m_CondExec 16,NE,CGBOXPAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CGBOXPZA
       ;;
(CGBOXPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGBOXPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
