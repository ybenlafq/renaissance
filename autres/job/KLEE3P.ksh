#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  KLEE3P.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPKLEE3 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/04/17 AT 11.20.34 BY BURTEC7                      
#    STANDARDS: P  JOBSET: KLEE3P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DES TABLES RTCE50 & RTCE52                                           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=KLEE3PA
       ;;
(KLEE3PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=KLEE3PAA
       ;;
(KLEE3PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#    RSCE50   : NAME=RSCE50,MODE=I - DYNAM=YES                                 
#    RSCE52   : NAME=RSCE52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE3PAA.BUNRTCAP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE3PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  TRI-SUM DU FICHIER D'UNLOAD                                                 
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE3PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=KLEE3PAD
       ;;
(KLEE3PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/KLEE3PAA.BUNRTCAP
       m_FileAssign -d NEW,CATLG,DELETE -r 34 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEE3PAD.BUNRTCBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_14_6 14 CH 6
 /FIELDS FLD_CH_9_5 9 CH 5
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_14_6 ASCENDING,
   FLD_CH_9_5 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 34 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE3PAE
       ;;
(KLEE3PAE)
       m_CondExec 00,EQ,KLEE3PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BCE652 : CONSOLIDATION DE L'EXTRACTION CE50                                 
#           PAR LES STOCKS MAGASINS                                            
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE3PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=KLEE3PAG
       ;;
(KLEE3PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **      TABLES DB2 EN LECTURE                                                
#    RSCM00   : NAME=RSCM00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSCM00 /dev/null
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA73   : NAME=RSGA73,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA73 /dev/null
#    RSGB05   : NAME=RSGB05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB05 /dev/null
#    RSGB15   : NAME=RSGB15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGB15 /dev/null
#    RSGS36   : NAME=RSGS36,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS36 /dev/null
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSPT01   : NAME=RSPT01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT01 /dev/null
#    RSPT03   : NAME=RSPT03,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSPT03 /dev/null
#    RSSL11   : NAME=RSSL11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSSL11 /dev/null
# **      FICHIER DATE                                                         
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FCE652I ${DATA}/PTEM/KLEE3PAD.BUNRTCBP
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 FCE652O ${DATA}/PXX0/F07.BCE652AP
# **      FICHIER ASSORTIMENT                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 FCE652B ${DATA}/PTEM/KLEE3PAG.FCE652BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCE652 
       JUMP_LABEL=KLEE3PAH
       ;;
(KLEE3PAH)
       m_CondExec 04,GE,KLEE3PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI-SUM DU FICHIER FCE652B AVEC FICHIER PROVENANT DE KLEE2                  
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE3PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=KLEE3PAJ
       ;;
(KLEE3PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/KLEE3PAG.FCE652BP
       m_FileAssign -d SHR -C ${DATA}/PXX0.F07.KLEE2P
# **      FICHIER ALLANT VERS KLEE7                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FCE652CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 14 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE3PAK
       ;;
(KLEE3PAK)
       m_CondExec 00,EQ,KLEE3PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI-SUM DU FICHIER FCE652O AVEC FICHIER PROVENANT DE KLEE3                  
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE3PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=KLEE3PAM
       ;;
(KLEE3PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F07.BCE652AP
# **      FICHIER ALLANT VERS KLEE6                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BCE652OP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_92 1 CH 92
 /KEYS
   FLD_CH_1_92 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 92 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE3PAN
       ;;
(KLEE3PAN)
       m_CondExec 00,EQ,KLEE3PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=KLEE3PZA
       ;;
(KLEE3PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE3PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
