#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  DEP00L.ksh                       --- VERSION DU 09/10/2016 05:25
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLDEP00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.29.48 BY BURTEC6                      
#    STANDARDS: P  JOBSET: DEP00L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BDEPDA  ==> QHRDEPDACXXX                                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=DEP00LA
       ;;
(DEP00LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+2'}
       G_A3=${G_A3:-'+2'}
       RUN=${RUN}
       JUMP_LABEL=DEP00LAA
       ;;
(DEP00LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS36L  : NAME=RSGS36L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS36L /dev/null
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER SORTIE LONG=200                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FDEPDA ${DATA}/PXX0/F61.DEP000AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDEPDA 
       JUMP_LABEL=DEP00LAB
       ;;
(DEP00LAB)
       m_CondExec 04,GE,DEP00LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DBDS001C ==> QHRDS001HRCXXX                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DEP00LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LAD
       ;;
(DEP00LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS10 /dev/null
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
#    RSGA69   : NAME=RSGA69,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA69 /dev/null
#    RSGA67   : NAME=RSGA67,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA67 /dev/null
#    RSVA25L  : NAME=RSVA25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSVA25L /dev/null
#    RSGS36L  : NAME=RSGS36L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS36L /dev/null
# ******  PARAMETRE SOCIETE : 961                                              
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  DATE JJMMSSAA                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER SORTIE LONG=200                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 FDS001 ${DATA}/PXX0/F61.DEP001AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDS001C 
       JUMP_LABEL=DEP00LAE
       ;;
(DEP00LAE)
       m_CondExec 04,GE,DEP00LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FICHIER DEP000AL VERS GATEWAY SMTP                                    
#  (FARID.CHOHRA@DARTY.FR)                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=DEP000AL                                                            
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FICHIER DEP001AL VERS GATEWAY SMTP                                    
#  (FARID.CHOHRA@DARTY.FR)                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=DEP001AL                                                            
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTDEP00L                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DEP00LAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LAG
       ;;
(DEP00LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DEP00LAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/DEP00LAG.FTDEP00L
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTDEP00L                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DEP00LAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LAJ
       ;;
(DEP00LAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/DEP00LAJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.DEP00LAG.FTDEP00L(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP DEP00LAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LAM
       ;;
(DEP00LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DEP00LAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTDEP00L                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DEP00LAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LAQ
       ;;
(DEP00LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DEP00LAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A2} SYSOUT ${DATA}/PTEM/DEP00LAG.FTDEP00L
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTDEP00L                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DEP00LAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LAT
       ;;
(DEP00LAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/DEP00LAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.DEP00LAG.FTDEP00L(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP DEP00LAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LAX
       ;;
(DEP00LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DEP00LAX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=DEP00LZA
       ;;
(DEP00LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DEP00LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
