#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GRAZPT.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PTGRAZP -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/04/04 AT 16.34.08 BY BURTEC2                      
#    STANDARDS: P  JOBSET: GRAZPT                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  CREATION D'UNE GENERATION A VIDE POUR LE LENDEMAIN                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=GRAZPTA
       ;;
(GRAZPTA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2008/04/04 AT 16.34.08 BY BURTEC2                
# *    JOBSET INFORMATION:    NAME...: GRAZPT                                  
# *                           FREQ...: 5W                                      
# *                           TITLE..: 'RAZ DES FICS'                          
# *                           APPL...: REPRECET                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=GRAZPTAA
       ;;
(GRAZPTAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# ***********************************                                          
# *****   FICHIER EN ENTREE                                                    
#                                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
#                                                                              
#                                                                              
# *****   FICHIER REMIS A ZERO POUR LES PCL DU LENDEMAIN                       
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT1 ${DATA}/PNCGT/F61.FFG140GL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT2 ${DATA}/PNCGT/F07.FFG140GP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT3 ${DATA}/PNCGT/REC.BPS225AT.PSE00T
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT4 ${DATA}/PNCGT/REC.BPS225LT.PS00LT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT5 ${DATA}/PNCGT/SEM.GBA210AT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT6 ${DATA}/PNCGT/SEM.GBA210AL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT7 ${DATA}/PNCGT/SEM.GBA211AT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT8 ${DATA}/PNCGT/SEM.GBA211AL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT9 ${DATA}/PNCGT/SEM.GBA212AT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT10 ${DATA}/PNCGT/SEM.GBA212AL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT11 ${DATA}/PNCGT/REC.BSA105AT.GRA01T
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT12 ${DATA}/PNCGT/REC.BAV000AT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT13 ${DATA}/PNCGT/REC.BAV000LT.AV00LT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT14 ${DATA}/PNCGT/REC.BAV201ET
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT15 ${DATA}/PNCGT/REC.FTV002.AV00LT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT16 ${DATA}/PNCGT/REC.FGT961AT.IF50FT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT17 ${DATA}/PNCGT/REC.FGT907AT.IF50FT
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 OUT18 ${DATA}/PNCGT/REC.FTICSGCT
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -g +1 OUT19 ${DATA}/PNCGT/REC.LTICSGCT.FTICLT
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 OUT20 ${DATA}/PNCGT/REC.BSA105LT.RA00LT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/GRAZPTAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=GRAZPTAB
       ;;
(GRAZPTAB)
       m_CondExec 16,NE,GRAZPTAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# **************************                                                   
#   DEPENDANCE POUR PLAN                                                       
# **************************                                                   
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
