#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM26P.ksh                       --- VERSION DU 09/10/2016 05:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIVM26 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/07 AT 13.20.31 BY BURTECC                      
#    STANDARDS: P  JOBSET: IVM26P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIV125   MAJ DE LA TABLE STOCK MAGASINS RTGS30 IMPUTATIONS DES ECAR         
#                      D'INVENTAIRE                                            
# ********************************************************************         
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM26PA
       ;;
(IVM26PA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXA98=${EXA98:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2003/03/07 AT 13.20.31 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: IVM26P                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK INVT'                        
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM26PAA
       ;;
(IVM26PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  INVENTAIRE MAGASIN                                                   
#    RSIN00   : NAME=RSIN00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIN00 /dev/null
# ******  LIENS INTER ARTICLES                                                 
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
#                                                                              
# ******  STOCKS MAGASINS                                                      
#    RSGS30   : NAME=RSGS30,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS30 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ******  SOCIETE A TRAITER   = 907                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#                                                                              
# ******  FICHIER DES MAJ AYANT RENDU LE STOCK DISPO NEGATIF                   
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 FER125 ${DATA}/PGI0/F07.BIV125AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV125 
       JUMP_LABEL=IVM26PAB
       ;;
(IVM26PAB)
       m_CondExec 04,GE,IVM26PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM26PZA
       ;;
(IVM26PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM26PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
