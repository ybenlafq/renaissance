#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PPSTAT0.ksh                       --- VERSION DU 19/10/2016 19:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSTAT0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/08/21 AT 10.23.23 BY BURTECC                      
#    STANDARDS: P  JOBSET: STAT0P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  QUIESCE DES TABLES RTGS40-RTHV06-RTVA05-RTGG50                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=STAT0PA
       ;;
(STAT0PA)
#
#STAT0PAA
#STAT0PAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#STAT0PAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       STAT0PA=${BHV031}
       JUMP_LABEL=STAT0PAD
       ;;
(STAT0PAD)
       m_CondExec ${EXAAF},NE,YES 1,NE,$[STAT0PA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE SOUS TABLES GENERALISEES                                       
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  MOUVEMENTS DE STOCKS MAG                                             
#    RSGS40   : NAME=RSGS40,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  MOUVEMENTS DE STOCKS MGI                                             
#    RSGS42   : NAME=RSGS42,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS42 /dev/null
#                                                                              
# ******  FMOIS : MMSSAA                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER PARAMETRE M POUR MENSUEL H POUR HEBDO                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0PAD
#                                                                              
# ******  FICHIER SEQ CONTENANT LES MVTS DE LA TABLE RTGS40                    
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 FGS40 ${DATA}/PXX0/F07.BHV030FP
# ******  FICHIER DES VENTES ET REPRISES DU MOIS (GROUPE DE PRODUITS)          
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 FHV100 ${DATA}/PXX0/F07.BHV100FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV030 
       JUMP_LABEL=STAT0PAE
       ;;
(STAT0PAE)
       m_CondExec 04,GE,STAT0PAD ${EXAAF},NE,YES 1,NE,$[STAT0PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHV031  === TYPE MAG === PROGRAMME DE RATTRAPAGE LORSQUE              
#                                 DES MAGS NON PAS ETE RECUPERES               
# ********************************************************************         
#  1) EXTRACTION DES MVTS NON TRAITES DE RTGS40 DONT LES LIEUX                 
#     NE SONT PAS A EXCLURE DU TRAITEMENT(RTGA01 SS-TABLE:EXMAG)               
#  2) ECRITURE DE CES MVTS DANS FGS40                                          
#  3) MAJ DE LA 'DSTAT' DE RTGS40 ON DU FICH SEQ DU MOIS                       
#  4) ECRITURE DES MVTS NON TRAITES DONT LA DSTAT = '    '                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PAG
       ;;
(STAT0PAG)
       m_CondExec ${EXAAK},NE,YES 1,EQ,$[STAT0PA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE SOUS TABLES GENERALISEES                                       
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  MOUVEMENTS DE STOCKS MAG                                             
#    RSGS40   : NAME=RSGS40,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS40 /dev/null
# ******  MOUVEMENTS DE STOCKS MGI                                             
#    RSGS42   : NAME=RSGS42,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGS42 /dev/null
#                                                                              
# ******  FMOIS : MMSSAA                                                       
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER PARAMETRE M POUR MENSUEL H POUR HEBDO                        
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0PAG
#                                                                              
# ******  FICHIER SEQ CONTENANT LES MVTS DE LA TABLE RTGS40                    
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g ${G_A1} FGS40 ${DATA}/PXX0/F07.BHV030FP
# ******  FICHIER DES VENTES ET REPRISES DU MOIS (GROUPE DE PRODUITS)          
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g ${G_A2} FHV100 ${DATA}/PXX0/F07.BHV100FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV031 
       JUMP_LABEL=STAT0PAH
       ;;
(STAT0PAH)
       m_CondExec 04,GE,STAT0PAG ${EXAAK},NE,YES 1,EQ,$[STAT0PA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 907                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PAJ
       ;;
(STAT0PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PAJ.BHV030XP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "907"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PAK
       ;;
(STAT0PAK)
       m_CondExec 00,EQ,STAT0PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 907                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PAM
       ;;
(STAT0PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/STAT0PAJ.BHV030XP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BHV030AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "000")
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_1_3 01 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PAN
       ;;
(STAT0PAN)
       m_CondExec 00,EQ,STAT0PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 991                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PAQ
       ;;
(STAT0PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PAQ.BHV030XD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "991"
 /DERIVEDFIELD CST_1_4 "991"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PAR
       ;;
(STAT0PAR)
       m_CondExec 00,EQ,STAT0PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 991                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PAT
       ;;
(STAT0PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/STAT0PAQ.BHV030XD
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.BHV030AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "991"
 /DERIVEDFIELD CST_3_8 "000")
 /FIELDS FLD_CH_1_3 01 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_4_3 04 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PAU
       ;;
(STAT0PAU)
       m_CondExec 00,EQ,STAT0PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 989                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PAX
       ;;
(STAT0PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PAX.BHV030XM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "989"
 /DERIVEDFIELD CST_3_8 "989"
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PAY
       ;;
(STAT0PAY)
       m_CondExec 00,EQ,STAT0PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 989                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBA
       ;;
(STAT0PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/STAT0PAX.BHV030XM
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.BHV030AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "000")
 /DERIVEDFIELD CST_1_4 "989"
 /FIELDS FLD_CH_1_3 01 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBB
       ;;
(STAT0PBB)
       m_CondExec 00,EQ,STAT0PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 961                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBD
       ;;
(STAT0PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PBD.BHV030XL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "961"
 /DERIVEDFIELD CST_1_4 "961"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBE
       ;;
(STAT0PBE)
       m_CondExec 00,EQ,STAT0PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 961                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBG
       ;;
(STAT0PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/STAT0PBD.BHV030XL
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.BHV030AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "961"
 /DERIVEDFIELD CST_3_8 "000")
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_CH_1_3 01 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBH
       ;;
(STAT0PBH)
       m_CondExec 00,EQ,STAT0PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 945                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBJ
       ;;
(STAT0PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PBJ.BHV030XY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "945"
 /DERIVEDFIELD CST_3_8 "945"
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBK
       ;;
(STAT0PBK)
       m_CondExec 00,EQ,STAT0PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 945                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBM
       ;;
(STAT0PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/STAT0PBJ.BHV030XY
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.BHV030AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "945"
 /DERIVEDFIELD CST_3_8 "000")
 /FIELDS FLD_CH_1_3 01 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBN
       ;;
(STAT0PBN)
       m_CondExec 00,EQ,STAT0PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 996                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBQ
       ;;
(STAT0PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PBQ.BHV030XB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "996"
 /DERIVEDFIELD CST_3_8 "996"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBR
       ;;
(STAT0PBR)
       m_CondExec 00,EQ,STAT0PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 996                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBT
       ;;
(STAT0PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/STAT0PBQ.BHV030XB
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F96.BHV030AB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "996"
 /DERIVEDFIELD CST_3_8 "000")
 /FIELDS FLD_CH_1_3 01 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBU
       ;;
(STAT0PBU)
       m_CondExec 00,EQ,STAT0PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 916                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PBX
       ;;
(STAT0PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PBX.BHV030XO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "916"
 /DERIVEDFIELD CST_3_8 "916"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PBY
       ;;
(STAT0PBY)
       m_CondExec 00,EQ,STAT0PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 916                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCA
       ;;
(STAT0PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PTEM/STAT0PBX.BHV030XO
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.BHV030AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "916"
 /DERIVEDFIELD CST_3_8 "000")
 /FIELDS FLD_CH_1_3 01 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCB
       ;;
(STAT0PCB)
       m_CondExec 00,EQ,STAT0PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 908                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCD
       ;;
(STAT0PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PCD.BHV030XX
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "908"
 /DERIVEDFIELD CST_1_4 "908"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCE
       ;;
(STAT0PCE)
       m_CondExec 00,EQ,STAT0PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 908                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCG PGM=SORT       ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCG
       ;;
(STAT0PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PTEM/STAT0PCD.BHV030XX
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/P908/SEM.BHV030AX
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "000")
 /DERIVEDFIELD CST_1_4 "908"
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_1_3 01 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCH
       ;;
(STAT0PCH)
       m_CondExec 00,EQ,STAT0PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 969                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCJ
       ;;
(STAT0PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PCJ.BHV030XE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "961"
 /DERIVEDFIELD CST_3_8 "961"
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCK
       ;;
(STAT0PCK)
       m_CondExec 00,EQ,STAT0PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 969                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCM
       ;;
(STAT0PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A20} SORTIN ${DATA}/PTEM/STAT0PCJ.BHV030XE
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F69.BHV030AE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "000")
 /DERIVEDFIELD CST_1_4 "961"
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_CH_1_3 01 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCN
       ;;
(STAT0PCN)
       m_CondExec 00,EQ,STAT0PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 911 (GRAND OUEST)                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCQ
       ;;
(STAT0PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PCQ.BHV030P1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "961"
 /DERIVEDFIELD CST_1_4 "961"
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCR
       ;;
(STAT0PCR)
       m_CondExec 00,EQ,STAT0PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 911                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCT
       ;;
(STAT0PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/STAT0PCQ.BHV030P1
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.BHV030GO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "961"
 /DERIVEDFIELD CST_3_8 "000")
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /FIELDS FLD_CH_1_3 01 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCU
       ;;
(STAT0PCU)
       m_CondExec 00,EQ,STAT0PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 905 (GRAND EST)                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PCX
       ;;
(STAT0PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PXX0/F07.BHV030FP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PCX.BHV030P2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "945")
 /DERIVEDFIELD CST_1_4 "945"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_10_3 10 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 OR FLD_CH_10_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PCY
       ;;
(STAT0PCY)
       m_CondExec 00,EQ,STAT0PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER MVT STOCK POUR SOCIETE 905                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDA
       ;;
(STAT0PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PTEM/STAT0PCX.BHV030P2
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.BHV030GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "000")
 /DERIVEDFIELD CST_1_4 "945"
 /FIELDS FLD_CH_1_3 01 CH 3
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_BI_1_25 1 CH 25
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 AND FLD_CH_4_3 EQ CST_3_8 OR 
 /KEYS
   FLD_BI_1_25 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDB
       ;;
(STAT0PDB)
       m_CondExec 00,EQ,STAT0PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 907                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDD
       ;;
(STAT0PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BHV100AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "907"
 /FIELDS FLD_BI_1_38 1 CH 38
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDE
       ;;
(STAT0PDE)
       m_CondExec 00,EQ,STAT0PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 991                                    
#  REPRISE : OUI                                                               
#  LGT     : A DECOMMENTARISER LORS BASCULE DPM  DANS LGT                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDG PGM=SORT       ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDG
       ;;
(STAT0PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A26} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.BHV100AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "991"
 /FIELDS FLD_BI_1_38 1 CH 38
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDH
       ;;
(STAT0PDH)
       m_CondExec 00,EQ,STAT0PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 961                                    
#  REPRISE : OUI                                                               
#  LGT     : A DECOMMENTARISER LORS BASCULE DNPC DANS LGT                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDJ PGM=SORT       ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDJ
       ;;
(STAT0PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.BHV100AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "961"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_1_38 1 CH 38
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDK
       ;;
(STAT0PDK)
       m_CondExec 00,EQ,STAT0PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 989                                    
#  REPRISE : OUI                                                               
#  LGT     : A DECOMMENTARISER LORS BASCULE DAL  DANS LGT                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDM
       ;;
(STAT0PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.BHV100AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "989"
 /FIELDS FLD_BI_1_38 1 CH 38
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDN
       ;;
(STAT0PDN)
       m_CondExec 00,EQ,STAT0PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 945                                    
#  REPRISE : OUI                                                               
#  LGT     : A DECOMMENTARISER LORS BASCULE DRA  DANS LGT                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDQ
       ;;
(STAT0PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.BHV100AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "945"
 /FIELDS FLD_BI_1_38 1 CH 38
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDR
       ;;
(STAT0PDR)
       m_CondExec 00,EQ,STAT0PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 996                                    
#  REPRISE : OUI                                                               
#  LGT     : A DECOMMENTARISER LORS BASCULE DRA  DANS LGT                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDT
       ;;
(STAT0PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PDT.BHV100AB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "996"
 /FIELDS FLD_BI_1_38 1 CH 38
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDU
       ;;
(STAT0PDU)
       m_CondExec 00,EQ,STAT0PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 916                                    
#  REPRISE : OUI                                                               
#  LGT     : A DECOMMENTARISER LORS BASCULE DRA  DANS LGT                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PDX
       ;;
(STAT0PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.BHV100AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "916"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_1_38 1 CH 38
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PDY
       ;;
(STAT0PDY)
       m_CondExec 00,EQ,STAT0PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION  FICHIER VENTE POUR SOCIETE 908                                    
#  REPRISE : OUI                                                               
#  LGT     : A DECOMMENTARISER LORS BASCULE DRA  DANS LGT                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PEA PGM=SORT       ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PEA
       ;;
(STAT0PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PXX0/F07.BHV100FP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/P908/SEM.BHV100AX
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "908"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_BI_1_38 1 CH 38
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_4 
 /KEYS
   FLD_BI_1_38 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PEB
       ;;
(STAT0PEB)
       m_CondExec 00,EQ,STAT0PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
#                                                                              
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PZA
       ;;
(STAT0PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0PZA.sysin
       m_UtilityExec
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
