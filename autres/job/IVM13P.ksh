#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM13P.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIVM13 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/03/24 AT 15.24.00 BY PREPA2                       
#    STANDARDS: P  JOBSET: IVM13P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BIV600                                                                
#  ------------                                                                
#                                                                              
#  CONTROLE DE TOP 'HSPRET' POUR TRAITER LA FIN D'INVENTAIRE (EDITIONS         
#                                                                              
#      - SI LE TOP HSPRET EST A 'OO', ON PEUT TRAITER LA FIN                   
#        D'INVENTAIRE ; EDITIONS CI-DESSOUS.                                   
#      - SI LE TOP HSPRET N'EST PAS A 'OO', IL Y A UN ABEND PROVOQU�           
#        DU PGM BIV600.                                                        
#                                                                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM13PA
       ;;
(IVM13PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVM13PAA
       ;;
(IVM13PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
       m_FileAssign -d SHR RSIE10 ${DATA}/PNGI.DSNDBC.PPDGI00.RSIE10.I0001.A001
# ------  CONTROLE DU TOP HS DANS RTIE10                                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PAA
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV600 
       JUMP_LABEL=IVM13PAB
       ;;
(IVM13PAB)
       m_CondExec 04,GE,IVM13PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE540                                                                      
# ********************************************************************         
#  CREATION DU FICHIER DES ECARTS D'INVENTAIRE ENTREPOT                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PAD
       ;;
(IVM13PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE D'INVENTAIRE                                                          
#    RSIE05   : NAME=RSIE05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE05 /dev/null
#  TABLE D'AVANCEMENT D'INVENTAIRE                                             
#    RSIE10   : NAME=RSIE10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIE10 /dev/null
#  TABLE DES LIEUX                                                             
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#  TABLE GENERALISEE                                                           
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#  TABLE DES ETATS                                                             
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#  TABLE DES PRMP DARTY                                                        
#    RSGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG50 /dev/null
#  TABLE DES PRMP DACEM                                                        
#    RSGG55   : NAME=RSGG55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE LOGISTIQUE                                                     
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#  FICHIER DES ECARTS D'INVENTAIRE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FIE540 ${DATA}/PGI0/F07.BIE540AP
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PAD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE540 
       JUMP_LABEL=IVM13PAE
       ;;
(IVM13PAE)
       m_CondExec 04,GE,IVM13PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV130                                                                      
# ********************************************************************         
#  BIV130  CREE FIC DES ECARTS D INVENTAIRE MAGS                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PAG
       ;;
(IVM13PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********                                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#  TABLE GENERALISE (INVDA)                                                    
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#  TABLE DES SEQUENCES FAMILLES EDITIONS (PARAMETRAGES ETATS)                  
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#  TABLE INVENTAIRE MAGASIN                                                    
#    RSIN00   : NAME=RSIN00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIN00 /dev/null
#  TABLE DES ANOMALIES                                                         
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
# -X-RSAN00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER DES ECARTS D'INVENTAIRE MAGASIN                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FIN130 ${DATA}/PGI0/F07.BIV130AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV130 
       JUMP_LABEL=IVM13PAH
       ;;
(IVM13PAH)
       m_CondExec 04,GE,IVM13PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI SUR SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON              
#  SEQ FAMILLE                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PAJ
       ;;
(IVM13PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIER DES ECARTS SOCIETE TRIES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PAJ.BIV135AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_92_5 92 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_55_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PAK
       ;;
(IVM13PAK)
       m_CondExec 00,EQ,IVM13PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV135                                                                      
# ********************************************************************         
#  EDITION DES ECARTS FAMILLES ET SOUS-LIEU                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PAM
       ;;
(IVM13PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIEUX                                                             
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                                
       m_FileAssign -d SHR -g ${G_A3} FIV130 ${DATA}/PTEM/IVM13PAJ.BIV135AP
# *   FIC D IMPRESSION ECARTS FAMILLES SOUS-LIEU                               
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV135 ${DATA}/PXX0.FICHEML.IVM13P.BIV135BP
# *   ETAT NON VALORISE                                                        
       m_OutputAssign -c Z IIV135B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV135 
       JUMP_LABEL=IVM13PAN
       ;;
(IVM13PAN)
       m_CondExec 04,GE,IVM13PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI SUR SOCIETE MAGASIN SEQUENCE RAYON SEQUENCE FAMILLE                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PAQ
       ;;
(IVM13PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIER DES ECARTS SOCIETE TRIES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PAQ.BIV130BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PAR
       ;;
(IVM13PAR)
       m_CondExec 00,EQ,IVM13PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV136                                                                      
# ********************************************************************         
#  EDITION DES ECARTS FAMILLES PAR LIEU                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PAT
       ;;
(IVM13PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIEUX                                                             
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                                
       m_FileAssign -d SHR -g ${G_A6} FIV130 ${DATA}/PTEM/IVM13PAQ.BIV130BP
# *   FIC D IMPRESSION LIEU RAYON FAMILLE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV136 ${DATA}/PXX0.FICHEML.IVM13P.BIV136BP
# *   ETAT NON VALORISE                                                        
       m_OutputAssign -c Z IIV136B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV136 
       JUMP_LABEL=IVM13PAU
       ;;
(IVM13PAU)
       m_CondExec 04,GE,IVM13PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SUR SOCIETE SEQUENCE RAYON SEQ FAMILLE            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PAX
       ;;
(IVM13PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS MAGASINS                                                 
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PGI0/F07.BIV130AP
#  FICHIER DES ECARTS MAGASINS TRIES                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PAX.BIV137AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PAY
       ;;
(IVM13PAY)
       m_CondExec 00,EQ,IVM13PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137                                                                      
# ********************************************************************         
#  EDITION PAR FAMILLE TOUS MAGASINS                                           
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBA PGM=BIV137     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBA
       ;;
(IVM13PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS TRIEES                                                   
       m_FileAssign -d SHR -g ${G_A8} FIV130 ${DATA}/PTEM/IVM13PAX.BIV137AP
# *   EDITION TOUS MAGASINS RAYON FAMILLE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV137 ${DATA}/PXX0.FICHEML.IVM13P.BIV137CP
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PBA
       m_ProgramExec BIV137 
# ********************************************************************         
#  SORT                                                                        
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR SOCIETE SEQUENCE RAYON SEQ FAM          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBD
       ;;
(IVM13PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIERS DES ECARTS SOCIETE                                                 
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIER DES ECARTS SOCIETE TRIE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PBD.BIV137BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PBE
       ;;
(IVM13PBE)
       m_CondExec 00,EQ,IVM13PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE PAR FAMILLE                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBG PGM=BIV137     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBG
       ;;
(IVM13PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A11} FIV130 ${DATA}/PTEM/IVM13PBD.BIV137BP
#    EDITION DES ECARTS SOCIETE PAR FAMILLE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV137 ${DATA}/PXX0.FICHEML.IVM13P.BIV137DP
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PBG
       m_ProgramExec BIV137 
# ********************************************************************         
#  SORT                                                                        
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR:                                        
#  SOCIETE SEQ FAM MARQUE CODIC LIEU SOUS-LIEU                                 
#  ET JE GARDE QUE LES ECARTS > 0                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBJ
       ;;
(IVM13PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIERS DES ECARTS SOCIETE                                                 
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIER DES ECARTS SOCIETE TRIE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PXX0/F07.BIV139AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 X'00000C'
 /DERIVEDFIELD CST_3_13 X'00000D'
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_85_3 85 CH 3
 /FIELDS FLD_CH_68_3 68 CH 3
 /FIELDS FLD_CH_72_3 72 CH 3
 /CONDITION CND_1 FLD_CH_85_3 EQ CST_1_9 OR FLD_CH_85_3 EQ CST_3_13 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_68_3 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_72_3 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PBK
       ;;
(IVM13PBK)
       m_CondExec 00,EQ,IVM13PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV139                                                                      
# ********************************************************************         
#  EDITION DES ECARTS FAMILLE CODIC TOTAL CODIC                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBM PGM=BIV139     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBM
       ;;
(IVM13PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS SOCIETE TRIE                                             
       m_FileAssign -d SHR -g ${G_A14} FIV130 ${DATA}/PXX0/F07.BIV139AP
#    EDITION DES ECARTS SOCIETE PAR FAMILLE CODIC TOTAL CODIC                  
#  IIV139   REPORT SYSOUT=(9,IIV139),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV139 ${DATA}/PXX0.FICHEML.IVM13P.BIV139BP
       m_ProgramExec BIV139 
# ********************************************************************         
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON SEQ FAMILLE          
#  LIB RAYON LIB FAMILLE LIB MARQUE ARTICLE                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBQ
       ;;
(IVM13PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIER DES ECARTS TRIEES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PBQ.BIV140AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_92_5 92 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PBR
       ;;
(IVM13PBR)
       m_CondExec 00,EQ,IVM13PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU ET SOUS-LIEU                  
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBT
       ;;
(IVM13PBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIEUX                                                             
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#  FICHIER DES ECARTS SOCIETE TRIES                                            
       m_FileAssign -d SHR -g ${G_A17} FIV140 ${DATA}/PTEM/IVM13PBQ.BIV140AP
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#      EDITON                                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV140 ${DATA}/PXX0.FICHEML.IVM13P.BIV140BP
       m_OutputAssign -c Z IIV140M
#      LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                            
       m_OutputAssign -c Z IIV140A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=IVM13PBU
       ;;
(IVM13PBU)
       m_CondExec 04,GE,IVM13PBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SEQ RAYON SEQ FAM LIB RAYON LIB FAMILLE LIB MARQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PBX
       ;;
(IVM13PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIER DES ECARTS TRIEES                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PBX.BIV130CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PBY
       ;;
(IVM13PBY)
       m_CondExec 00,EQ,IVM13PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV141                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCA
       ;;
(IVM13PCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIEUX                                                             
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#  FICHIER DES ECARTS SOCIETE TRIES                                            
       m_FileAssign -d SHR -g ${G_A20} FIV141 ${DATA}/PTEM/IVM13PBX.BIV130CP
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#      EDITON PAR LIEU/RAYON/FAMILLE/CODIC                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV141 ${DATA}/PXX0.FICHEML.IVM13P.BIV141BP
#      MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                           
       m_OutputAssign -c Z IIV141M
#      LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                            
       m_OutputAssign -c Z IIV141A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV141 
       JUMP_LABEL=IVM13PCB
       ;;
(IVM13PCB)
       m_CondExec 04,GE,IVM13PCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SUR:                                              
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCD
       ;;
(IVM13PCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS MAGASINS                                                 
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PGI0/F07.BIV130AP
#  FICHIER DES ECARTS MAGASINS TRIES                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PCD.BIV130DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PCE
       ;;
(IVM13PCE)
       m_CondExec 00,EQ,IVM13PCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142                                                                      
# ********************************************************************         
#  EDITION DES ECARTS MAGASINS PRODUITS                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCG PGM=BIV142     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCG
       ;;
(IVM13PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS MAGASINS TRIES                                           
       m_FileAssign -d SHR -g ${G_A22} FIV142 ${DATA}/PTEM/IVM13PCD.BIV130DP
# * EDITION RAYON/FAMILLE/CODIC                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142 ${DATA}/PXX0.FICHEML.IVM13P.BIV142BP
# *   LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                             
       m_OutputAssign -c Z -w IIV142A IIV142A
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PCG
       m_ProgramExec BIV142 
# ********************************************************************         
#  BIV142                                                                      
# ********************************************************************         
#  EDITION DES ECARTS MAGASINS PRODUITS                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCJ PGM=BIV142     ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCJ
       ;;
(IVM13PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS MAGASINS TRIES                                           
       m_FileAssign -d SHR -g ${G_A23} FIV142 ${DATA}/PTEM/IVM13PCD.BIV130DP
# * EDITION RAYON/FAMILLE/CODIC                                                
       m_OutputAssign -c Z -w IIV142 IIV142
# *   LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142A ${DATA}/PXX0.FICHEML.IVM13P.BIV142CP
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PCJ
       m_ProgramExec BIV142 
# ********************************************************************         
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCM
       ;;
(IVM13PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIERS DES ECARTS SOCIETE                                                 
       m_FileAssign -d SHR -g ${G_A24} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A25} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIERS DES ECARTS SOCIETE TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PCM.BIV130EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PCN
       ;;
(IVM13PCN)
       m_CondExec 00,EQ,IVM13PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE PRODUITS                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCQ PGM=BIV142     ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCQ
       ;;
(IVM13PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A26} FIV142 ${DATA}/PTEM/IVM13PCM.BIV130EP
#    EDITION DES ECARTS SOCIETE RAYON/FAMILLE/CODIC                            
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142 ${DATA}/PXX0.FICHEML.IVM13P.BIV142DP
# *  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                              
       m_OutputAssign -c Z -w IIV142SA IIV142A
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PCQ
       m_ProgramExec BIV142 
# ********************************************************************         
#  BIV142                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE PRODUITS                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCT PGM=BIV142     ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCT
       ;;
(IVM13PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A27} FIV142 ${DATA}/PTEM/IVM13PCM.BIV130EP
#    EDITION DES ECARTS SOCIETE RAYON/FAMILLE/CODIC                            
       m_OutputAssign -c Z -w IIV142S IIV142
# *  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                              
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142A ${DATA}/PXX0.FICHEML.IVM13P.BIV142EP
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PCT
       m_ProgramExec BIV142 
# ********************************************************************         
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETEE SUR:                                     
#  SOCIETE MAG SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PCX
       ;;
(IVM13PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A29} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIERS DES ECARTS SOCIETE TRIE                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PCX.BIV143AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_23_7 23 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PCY
       ;;
(IVM13PCY)
       m_CondExec 00,EQ,IVM13PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV143                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE PAR LIEU                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDA PGM=IKJEFT01   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDA
       ;;
(IVM13PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIEUX                                                             
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A30} FIV143 ${DATA}/PTEM/IVM13PCX.BIV143AP
#    EDITION DES ECARTS SOCIETE PAR LIEU                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV143 ${DATA}/PXX0.FICHEML.IVM13P.BIV143BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV143 
       JUMP_LABEL=IVM13PDB
       ;;
(IVM13PDB)
       m_CondExec 04,GE,IVM13PDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON LIB RAYON MAGASIN                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDD
       ;;
(IVM13PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A31} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A32} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIERS DES ECARTS MAGASINS TRIE                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PDD.BIV144AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_3 65 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PDE
       ;;
(IVM13PDE)
       m_CondExec 00,EQ,IVM13PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV144                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE PAR RAYON                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDG PGM=IKJEFT01   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDG
       ;;
(IVM13PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE DES LIEUX                                                             
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A33} FIV144 ${DATA}/PTEM/IVM13PDD.BIV144AP
#    EDITION DES ECARTS SOCIETE PAR RAYON MAGASINS                             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV144 ${DATA}/PXX0.FICHEML.IVM13P.BIV144BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV144 
       JUMP_LABEL=IVM13PDH
       ;;
(IVM13PDH)
       m_CondExec 04,GE,IVM13PDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV170                                                                      
# ********************************************************************         
#  CREATION DU FICHIER DE DEPRECIATION                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDJ PGM=IKJEFT01   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDJ
       ;;
(IVM13PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ********                                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
#  TABLE DES ARTICLES                                                          
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#  TABLE GENERALISEE (INVDA)                                                   
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#  TABLE EDITION DES ETATS SEQUENCE EDITION DES FAMILLES                       
#    RSGA11   : NAME=RSGA11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA11 /dev/null
#  TABLE D'INVENTAIRE MAGASINS                                                 
#    RSIN00   : NAME=RSIN00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSIN00 /dev/null
#                                                                              
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#                                                                              
#  TABLE D'ANIMALIE DE TRAITEMENT                                              
#    RSAN00   : NAME=RSAN00,MODE=U - DYNAM=YES                                 
# -X-RSAN00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00 /dev/null
#  FICHIER DE DEPRECIATION                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FIV170 ${DATA}/PTEM/IVM13PDJ.BIV170AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV170 
       JUMP_LABEL=IVM13PDK
       ;;
(IVM13PDK)
       m_CondExec 04,GE,IVM13PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI DU FICHIER DE DEPRECIATION SUR :                                        
#  SOCIETE MAGASIN SEQ RAYON SEQ FAMILLE MARQUE CODIC STATUS                   
#  SUM SUR LA ZONE 73,3                                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDM PGM=SORT       ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDM
       ;;
(IVM13PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DE DEPRECIATION                                                     
       m_FileAssign -d SHR -g ${G_A34} SORTIN ${DATA}/PTEM/IVM13PDJ.BIV170AP
#  FICHIER DE DEPRECIATION TRIE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PGI0/F07.BIV170BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_91_1 91 CH 1
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_PD_73_3 73 PD 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING,
   FLD_CH_91_1 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_73_3
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PDN
       ;;
(IVM13PDN)
       m_CondExec 00,EQ,IVM13PDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV180                                                                      
# ********************************************************************         
#  EDITION DES PROVISIONS DE DEPRECIATIONS                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDQ PGM=IKJEFT01   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDQ
       ;;
(IVM13PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE (INVDA)                                                   
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DE DEPRECIATION TRIE                                                
       m_FileAssign -d SHR -g ${G_A35} FIV180 ${DATA}/PGI0/F07.BIV170BP
# * EDITION DES PROVISIONS DE DEPRECIATION                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV180 ${DATA}/PXX0.FICHEML.IVM13P.BIV180BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV180 
       JUMP_LABEL=IVM13PDR
       ;;
(IVM13PDR)
       m_CondExec 04,GE,IVM13PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDT PGM=SORT       ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDT
       ;;
(IVM13PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#  FICHIERS DES ECARTS SOCIETE                                                 
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PGI0/F07.BIV130AP
       m_FileAssign -d SHR -g ${G_A37} -C ${DATA}/PGI0/F07.BIE540AP
#  FICHIER DES ECARTS SOCIETE TRIE                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13PDT.BIV137EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "095"
 /DERIVEDFIELD CST_3_12 "100"
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_23_7 23 CH 07
 /FIELDS FLD_CH_4_3 04 CH 3
 /FIELDS FLD_PD_75_3 75 PD 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /CONDITION CND_2 FLD_CH_4_3 EQ CST_1_8 OR FLD_CH_4_3 EQ CST_3_12 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_75_3,
    TOTAL FLD_PD_85_3
 /OMIT CND_2
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13PDU
       ;;
(IVM13PDU)
       m_CondExec 00,EQ,IVM13PDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137                                                                      
# ********************************************************************         
#  EDITION DES ECARTS SOCIETE SANS LES ENTREPOTS NATIONAUX                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PDX PGM=BIV137     ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PDX
       ;;
(IVM13PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                                      
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER DES ECARTS SOCIETE                                                  
       m_FileAssign -d SHR -g ${G_A38} FIV130 ${DATA}/PTEM/IVM13PDT.BIV137EP
#    EDITION DES ECARTS SOCIETE PAR FAMILLE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV137 ${DATA}/PXX0.FICHEML.IVM13P.BIV137FP
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13PDX
       m_ProgramExec BIV137 
# ********************************************************************         
#  REPRISE :OUI     (ETAT IIV135)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PEA
       ;;
(IVM13PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV135BP
       m_OutputAssign -c 9 -w IIV135 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PEB
       ;;
(IVM13PEB)
       m_CondExec 00,EQ,IVM13PEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV136)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PED PGM=IEBGENER   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PED
       ;;
(IVM13PED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV136BP
       m_OutputAssign -c 9 -w IIV136 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PEE
       ;;
(IVM13PEE)
       m_CondExec 00,EQ,IVM13PED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PEG PGM=IEBGENER   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PEG
       ;;
(IVM13PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV137CP
       m_OutputAssign -c 9 -w IIV137 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PEH
       ;;
(IVM13PEH)
       m_CondExec 00,EQ,IVM13PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PEJ PGM=IEBGENER   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PEJ
       ;;
(IVM13PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV137DP
       m_OutputAssign -c 9 -w IIV137S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PEK
       ;;
(IVM13PEK)
       m_CondExec 00,EQ,IVM13PEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV140)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PEM PGM=IEBGENER   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PEM
       ;;
(IVM13PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV140BP
       m_OutputAssign -c 9 -w IIV140 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PEN
       ;;
(IVM13PEN)
       m_CondExec 00,EQ,IVM13PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV141)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PEQ PGM=IEBGENER   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PEQ
       ;;
(IVM13PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV141BP
       m_OutputAssign -c 9 -w IIV141 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PER
       ;;
(IVM13PER)
       m_CondExec 00,EQ,IVM13PEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PET PGM=IEBGENER   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PET
       ;;
(IVM13PET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV142BP
       m_OutputAssign -c 9 -w IIV142 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PEU
       ;;
(IVM13PEU)
       m_CondExec 00,EQ,IVM13PET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142A)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PEX PGM=IEBGENER   ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PEX
       ;;
(IVM13PEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV142CP
       m_OutputAssign -c 9 -w IIV142A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PEY
       ;;
(IVM13PEY)
       m_CondExec 00,EQ,IVM13PEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PFA PGM=IEBGENER   ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PFA
       ;;
(IVM13PFA)
       m_CondExec ${EXAHS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV142DP
       m_OutputAssign -c 9 -w IIV142S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PFB
       ;;
(IVM13PFB)
       m_CondExec 00,EQ,IVM13PFA ${EXAHS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142SA)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PFD PGM=IEBGENER   ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PFD
       ;;
(IVM13PFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV142EP
       m_OutputAssign -c 9 -w IIV142SA SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PFE
       ;;
(IVM13PFE)
       m_CondExec 00,EQ,IVM13PFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV143)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PFG PGM=IEBGENER   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PFG
       ;;
(IVM13PFG)
       m_CondExec ${EXAIC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV143BP
       m_OutputAssign -c 9 -w IIV143 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PFH
       ;;
(IVM13PFH)
       m_CondExec 00,EQ,IVM13PFG ${EXAIC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV144)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PFJ PGM=IEBGENER   ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PFJ
       ;;
(IVM13PFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV144BP
       m_OutputAssign -c 9 -w IIV144 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PFK
       ;;
(IVM13PFK)
       m_CondExec 00,EQ,IVM13PFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV180)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PFM PGM=IEBGENER   ** ID=AIM                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PFM
       ;;
(IVM13PFM)
       m_CondExec ${EXAIM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV180BP
       m_OutputAssign -c 9 -w IIV180 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PFN
       ;;
(IVM13PFN)
       m_CondExec 00,EQ,IVM13PFM ${EXAIM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137H)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PFQ PGM=IEBGENER   ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PFQ
       ;;
(IVM13PFQ)
       m_CondExec ${EXAIR},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV137FP
       m_OutputAssign -c 9 -w IIV137H SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PFR
       ;;
(IVM13PFR)
       m_CondExec 00,EQ,IVM13PFQ ${EXAIR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV139)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13PFT PGM=IEBGENER   ** ID=AIW                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PFT
       ;;
(IVM13PFT)
       m_CondExec ${EXAIW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13P.BIV139BP
       m_OutputAssign -c 9 -w IIV139 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13PFU
       ;;
(IVM13PFU)
       m_CondExec 00,EQ,IVM13PFT ${EXAIW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM13PZA
       ;;
(IVM13PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM13PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
