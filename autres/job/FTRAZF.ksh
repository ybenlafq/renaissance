#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTRAZF.ksh                       --- VERSION DU 09/10/2016 00:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFFTRAZ -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/12 AT 10.40.58 BY BURTEC3                      
#    STANDARDS: P  JOBSET: FTRAZF                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  SAUVEGARDE DES FICHIERS D'INTERFACES                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FTRAZFA
       ;;
(FTRAZFA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       G_A1=${G_A1:-'+2'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2015/08/12 AT 10.40.54 BY BURTEC3                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FTRAZF                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'RAZ DES FICHIERS'                      
# *                           APPL...: REPFILIA                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FTRAZFAA
       ;;
(FTRAZFAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# *********************************                                            
# ******** FICHIER FTI01* / CGSAP* ET FV001K/P/B/X                             
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/F91.FFTI00GD
       m_FileAssign -d SHR -g +0 IN2 ${DATA}/PXX0/F61.FFTI00EL
       m_FileAssign -d SHR -g +0 IN3 ${DATA}/PXX0/F89.FFTI00EM
       m_FileAssign -d SHR -g +0 IN4 ${DATA}/PXX0/F16.FFTI00EO
       m_FileAssign -d SHR -g +0 IN5 ${DATA}/PXX0/F07.FFTI00EP
       m_FileAssign -d SHR -g +0 IN6 ${DATA}/PXX0/F45.FFTI00EY
       m_FileAssign -d SHR -g +0 IN7 ${DATA}/PXX0/F91.FTI01D
       m_FileAssign -d SHR -g +0 IN8 ${DATA}/PXX0/F61.FTI01L
       m_FileAssign -d SHR -g +0 IN9 ${DATA}/PXX0/F89.FTI01M
       m_FileAssign -d SHR -g +0 IN10 ${DATA}/PXX0/F16.FTI01O
       m_FileAssign -d SHR -g +0 IN11 ${DATA}/PXX0/F07.FTI01P
       m_FileAssign -d SHR -g +0 IN12 ${DATA}/PXX0/F45.FTI01Y
       m_FileAssign -d SHR -g +0 IN13 ${DATA}/PXX0/F91.FV001D
       m_FileAssign -d SHR -g +0 IN14 ${DATA}/PXX0/F61.FV001L
       m_FileAssign -d SHR -g +0 IN15 ${DATA}/PXX0/F89.FV001M
       m_FileAssign -d SHR -g +0 IN16 ${DATA}/PXX0/F16.FV001O
       m_FileAssign -d SHR -g +0 IN17 ${DATA}/PXX0/F07.FV001P
       m_FileAssign -d SHR -g +0 IN18 ${DATA}/PXX0/F45.FV001Y
       m_FileAssign -d SHR -g +0 IN19 ${DATA}/PNCGK/F97.BFTV01AK
       m_FileAssign -d SHR -g +0 IN20 ${DATA}/PNCGP/F07.BFTV01AP
       m_FileAssign -d SHR -g +0 IN21 ${DATA}/PNCGB/F96.BFTV01AB
       m_FileAssign -d SHR -g +0 IN22 ${DATA}/P908/SEM.BFTV01AX
       m_FileAssign -d SHR -g +0 IN23 ${DATA}/PXX0/F96.BGS105AB
       m_FileAssign -d SHR -g +0 IN24 ${DATA}/PXX0/F91.BGS105AD
       m_FileAssign -d SHR -g +0 IN25 ${DATA}/PXX0/F61.BGS105AL
       m_FileAssign -d SHR -g +0 IN26 ${DATA}/PXX0/F89.BGS105AM
       m_FileAssign -d SHR -g +0 IN27 ${DATA}/PXX0/F07.BGS105AP
       m_FileAssign -d SHR -g +0 IN28 ${DATA}/PXX0/F45.BGS105AY
       m_FileAssign -d SHR -g +0 IN29 ${DATA}/PNCGL/F61.BNM155AL
       m_FileAssign -d SHR -g +0 IN30 ${DATA}/PNCGP/F07.BNM155AP
       m_FileAssign -d SHR -g +0 IN31 ${DATA}/PNCGR/F94.BNM155AR
       m_FileAssign -d SHR -g +0 IN32 ${DATA}/PNCGY/F45.BNM155AY
       m_FileAssign -d SHR -g +0 IN33 ${DATA}/PNCGM/F89.BNM155AM
       m_FileAssign -d SHR -g +0 IN34 ${DATA}/PNCGD/F91.BNM155AD
       m_FileAssign -d SHR -g +0 IN35 ${DATA}/PNCGB/F96.BNM155AB
       m_FileAssign -d SHR -g +0 IN36 ${DATA}/PNCGO/F16.BNM155AO
       m_FileAssign -d SHR -g +0 IN37 ${DATA}/P908/SEM.BGS105AX
       m_FileAssign -d SHR -g +0 IN38 ${DATA}/P908/SEM.BNM155AX
# ******** FICHIER S54 DE GS100X                                               
       m_FileAssign -d SHR -g +0 IN39 ${DATA}/PXX0/F07.BNM154AP
       m_FileAssign -d SHR -g +0 IN40 ${DATA}/PXX0/F45.BNM154AY
       m_FileAssign -d SHR -g +0 IN41 ${DATA}/PXX0/F89.BNM154AM
       m_FileAssign -d SHR -g +0 IN42 ${DATA}/PXX0/F16.BNM154AO
       m_FileAssign -d SHR -g +0 IN43 ${DATA}/PXX0/F91.BNM154AD
       m_FileAssign -d SHR -g +0 IN44 ${DATA}/PXX0/F61.BNM154AL
       m_FileAssign -d SHR -g +0 IN45 ${DATA}/PXX0/F96.BNM154AB
# ******** FICHIER S56 DE GS100X                                               
       m_FileAssign -d SHR -g +0 IN46 ${DATA}/PXX0/F07.BNM156AP
       m_FileAssign -d SHR -g +0 IN47 ${DATA}/PXX0/F45.BNM156AY
       m_FileAssign -d SHR -g +0 IN48 ${DATA}/PXX0/F89.BNM156AM
       m_FileAssign -d SHR -g +0 IN49 ${DATA}/PXX0/F16.BNM156AO
       m_FileAssign -d SHR -g +0 IN50 ${DATA}/PXX0/F91.BNM156AD
       m_FileAssign -d SHR -g +0 IN51 ${DATA}/PXX0/F61.BNM156AL
       m_FileAssign -d SHR -g +0 IN52 ${DATA}/PXX0/F96.BNM156AB
# ******** FICHIER S57 DE RE000X                                               
       m_FileAssign -d SHR -g +0 IN53 ${DATA}/PXX0/F07.BNM157AP
       m_FileAssign -d SHR -g +0 IN54 ${DATA}/PXX0/F45.BNM157AY
       m_FileAssign -d SHR -g +0 IN55 ${DATA}/PXX0/F89.BNM157AM
       m_FileAssign -d SHR -g +0 IN56 ${DATA}/PXX0/F16.BNM157AO
       m_FileAssign -d SHR -g +0 IN57 ${DATA}/PXX0/F91.BNM157AD
       m_FileAssign -d SHR -g +0 IN58 ${DATA}/PXX0/F61.BNM157AL
       m_FileAssign -d SHR -g +0 IN59 ${DATA}/PXX0/F96.BNM157AB
# ******** FICHIER S58 DE DA012B                                               
       m_FileAssign -d SHR -g +0 IN60 ${DATA}/PXX0/F96.BNM158AB
# ******** FICHIER S59 DE GCDG0B                                               
       m_FileAssign -d SHR -g +0 IN61 ${DATA}/PXX0/F07.BNM159AP
       m_FileAssign -d SHR -g +0 IN62 ${DATA}/PXX0/F96.BNM159AB
# ******** FICHIER S60 DE NM001B                                               
       m_FileAssign -d SHR -g +0 IN63 ${DATA}/PNCGP/F07.BNM160AP
       m_FileAssign -d SHR -g +0 IN64 ${DATA}/PNCGY/F45.BNM160AY
       m_FileAssign -d SHR -g +0 IN65 ${DATA}/PNCGM/F89.BNM160AM
       m_FileAssign -d SHR -g +0 IN66 ${DATA}/PNCGO/F16.BNM160AO
       m_FileAssign -d SHR -g +0 IN67 ${DATA}/PNCGD/F91.BNM160AD
       m_FileAssign -d SHR -g +0 IN68 ${DATA}/PNCGL/F61.BNM160AL
       m_FileAssign -d SHR -g +0 IN69 ${DATA}/PNCGB/F96.BNM160AB
# ******** FICHIER S75 DE FC150X                                               
       m_FileAssign -d SHR -g +0 IN70 ${DATA}/PXX0/F07.BNM175AP
       m_FileAssign -d SHR -g +0 IN71 ${DATA}/PXX0/F45.BNM175AY
       m_FileAssign -d SHR -g +0 IN72 ${DATA}/PXX0/F89.BNM175AM
       m_FileAssign -d SHR -g +0 IN73 ${DATA}/PXX0/F16.BNM175AO
       m_FileAssign -d SHR -g +0 IN74 ${DATA}/PXX0/F91.BNM175AD
       m_FileAssign -d SHR -g +0 IN75 ${DATA}/PXX0/F61.BNM175AL
       m_FileAssign -d SHR -g +0 IN76 ${DATA}/PXX0/F96.BNM175AB
# ******** FICHIER S76 DE FC150X                                               
       m_FileAssign -d SHR -g +0 IN77 ${DATA}/PXX0/F07.BNM176AP
       m_FileAssign -d SHR -g +0 IN78 ${DATA}/PXX0/F45.BNM176AY
       m_FileAssign -d SHR -g +0 IN79 ${DATA}/PXX0/F89.BNM176AM
       m_FileAssign -d SHR -g +0 IN80 ${DATA}/PXX0/F16.BNM176AO
       m_FileAssign -d SHR -g +0 IN81 ${DATA}/PXX0/F91.BNM176AD
       m_FileAssign -d SHR -g +0 IN82 ${DATA}/PXX0/F61.BNM176AL
       m_FileAssign -d SHR -g +0 IN83 ${DATA}/PXX0/F96.BNM176AB
# ******** FICHIER S61 DES CHAINES NM061*                                      
       m_FileAssign -d SHR -g +0 IN84 ${DATA}/PXX0/F91.BNM161AD
       m_FileAssign -d SHR -g +0 IN85 ${DATA}/PXX0/F61.BNM161AL
       m_FileAssign -d SHR -g +0 IN86 ${DATA}/PXX0/F89.BNM161AM
       m_FileAssign -d SHR -g +0 IN87 ${DATA}/PXX0/F16.BNM161AO
       m_FileAssign -d SHR -g +0 IN88 ${DATA}/PXX0/F07.BNM161AP
       m_FileAssign -d SHR -g +0 IN89 ${DATA}/PXX0/F45.BNM161AY
# ******** FICHIER INTERFACE BBS052                                            
       m_FileAssign -d SHR -g +0 IN90 ${DATA}/PNCGD/F91.BBS052AD
       m_FileAssign -d SHR -g +0 IN91 ${DATA}/PNCGL/F61.BBS052AL
       m_FileAssign -d SHR -g +0 IN92 ${DATA}/PNCGM/F89.BBS052AM
       m_FileAssign -d SHR -g +0 IN93 ${DATA}/PNCGO/F16.BBS052AO
       m_FileAssign -d SHR -g +0 IN94 ${DATA}/PNCGP/F07.BBS052AP
       m_FileAssign -d SHR -g +0 IN95 ${DATA}/P908/SEM.BBS052AX
       m_FileAssign -d SHR -g +0 IN96 ${DATA}/PNCGY/F45.BBS052AY
# ******** FICHIER INTERFACE CAPRO (PETITES SOCIETES)                          
       m_FileAssign -d SHR -g +0 IN97 ${DATA}/PXX0/F44.FV001K
       m_FileAssign -d SHR -g +0 IN98 ${DATA}/PXX0/F99.FTI01K
# ******** FICHIER ASSURANCE PAR ABONNEMENT (HD001P)                           
       m_FileAssign -d SHR -g +0 IN99 ${DATA}/PXX0/FTP.F07.HD0001AP
# ******** FICHIER RACHAT PRODUITS ELECTRONIQUES CORDON (IR010P)               
       m_FileAssign -d SHR -g +0 IN100 ${DATA}/PXX0/FTP.F07.BIR010AP
#                                                                              
       m_FileAssign -d SHR -g +0 IN101 ${DATA}/PXX0/F44.FV001K1
       m_FileAssign -d SHR -g +0 IN102 ${DATA}/PXX0/F44.FV001K2
# ******** FICHIERS DE SAUVEGARDE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PNCGD/F91.SAUVE.FFTI00GD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT2 ${DATA}/PNCGL/F61.SAUVE.FFTI00EL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT3 ${DATA}/PNCGM/F89.SAUVE.FFTI00EM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT4 ${DATA}/PNCGO/F16.SAUVE.FFTI00EO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT5 ${DATA}/PNCGP/F07.SAUVE.FFTI00EP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT6 ${DATA}/PNCGY/F45.SAUVE.FFTI00EY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT7 ${DATA}/PNCGD/F91.SAUVE.FTI01D
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT8 ${DATA}/PNCGL/F61.SAUVE.FTI01L
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT9 ${DATA}/PNCGM/F89.SAUVE.FTI01M
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT10 ${DATA}/PNCGO/F16.SAUVE.FTI01O
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT11 ${DATA}/PNCGP/F07.SAUVE.FTI01P
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT12 ${DATA}/PNCGY/F45.SAUVE.FTI01Y
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT13 ${DATA}/PNCGD/F91.SAUVE.FV001D
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT14 ${DATA}/PNCGL/F61.SAUVE.FV001L
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT15 ${DATA}/PNCGM/F89.SAUVE.FV001M
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT16 ${DATA}/PNCGO/F16.SAUVE.FV001O
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT17 ${DATA}/PNCGP/F07.SAUVE.FV001P
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT18 ${DATA}/PNCGY/F45.SAUVE.FV001Y
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT19 ${DATA}/PNCGK/F97.SAUVE.INTERFAK
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT20 ${DATA}/PNCGP/F07.SAUVE.INTERFAP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT21 ${DATA}/PNCGB/F96.SAUVE.INTERFAB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT22 ${DATA}/P908/SEM.SAUVE.INTERFAX
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -t LSEQ -g +1 OUT23 ${DATA}/PNCGB/F96.SAUVE.BGS105AB
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -t LSEQ -g +1 OUT24 ${DATA}/PNCGD/F91.SAUVE.BGS105AD
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -t LSEQ -g +1 OUT25 ${DATA}/PNCGL/F61.SAUVE.BGS105AL
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -t LSEQ -g +1 OUT26 ${DATA}/PNCGM/F89.SAUVE.BGS105AM
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -t LSEQ -g +1 OUT27 ${DATA}/PNCGP/F07.SAUVE.BGS105AP
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -t LSEQ -g +1 OUT28 ${DATA}/PNCGY/F45.SAUVE.BGS105AY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT29 ${DATA}/PNCGL/F61.SAUVE.BNM155AL
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT30 ${DATA}/PNCGP/F07.SAUVE.BNM155AP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT31 ${DATA}/PNCGR/F94.SAUVE.BNM155AR
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT32 ${DATA}/PNCGY/F45.SAUVE.BNM155AY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT33 ${DATA}/PNCGM/F89.SAUVE.BNM155AM
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT34 ${DATA}/PNCGD/F91.SAUVE.BNM155AD
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT35 ${DATA}/PNCGB/F96.SAUVE.BNM155AB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT36 ${DATA}/PNCGO/F16.SAUVE.BNM155AO
       m_FileAssign -d NEW,CATLG,DELETE -r 630 -t LSEQ -g +1 OUT37 ${DATA}/P908/SEM.SAUVE.BGS105AX
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT38 ${DATA}/P908/SEM.SAUVE.BNM155AX
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 OUT39 ${DATA}/PNCGP/F07.SAUVE.BNM154AP
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 OUT40 ${DATA}/PNCGY/F45.SAUVE.BNM154AY
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 OUT41 ${DATA}/PNCGM/F89.SAUVE.BNM154AM
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 OUT42 ${DATA}/PNCGO/F16.SAUVE.BNM154AO
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 OUT43 ${DATA}/PNCGD/F91.SAUVE.BNM154AD
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 OUT44 ${DATA}/PNCGL/F61.SAUVE.BNM154AL
       m_FileAssign -d NEW,CATLG,DELETE -r 150 -t LSEQ -g +1 OUT45 ${DATA}/PNCGB/F96.SAUVE.BNM154AB
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT46 ${DATA}/PNCGP/F07.SAUVE.SAU156AP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT47 ${DATA}/PNCGY/F45.SAUVE.SAU156AY
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT48 ${DATA}/PNCGM/F89.SAUVE.SAU156AM
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT49 ${DATA}/PNCGO/F16.SAUVE.SAU156AO
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT50 ${DATA}/PNCGD/F91.SAUVE.SAU156AD
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT51 ${DATA}/PNCGL/F61.SAUVE.SAU156AL
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT52 ${DATA}/PNCGB/F96.SAUVE.SAU156AB
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -t LSEQ -g +1 OUT53 ${DATA}/PNCGP/F07.SAUVE.SAU157AP
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -t LSEQ -g +1 OUT54 ${DATA}/PNCGY/F45.SAUVE.SAU157AY
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -t LSEQ -g +1 OUT55 ${DATA}/PNCGM/F89.SAUVE.SAU157AM
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -t LSEQ -g +1 OUT56 ${DATA}/PNCGO/F16.SAUVE.SAU157AO
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -t LSEQ -g +1 OUT57 ${DATA}/PNCGD/F91.SAUVE.SAU157AD
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -t LSEQ -g +1 OUT58 ${DATA}/PNCGL/F61.SAUVE.SAU157AL
       m_FileAssign -d NEW,CATLG,DELETE -r 107 -t LSEQ -g +1 OUT59 ${DATA}/PNCGB/F96.SAUVE.SAU157AB
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 OUT60 ${DATA}/PNCGB/F96.SAUVE.SAU158AB
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 OUT61 ${DATA}/PNCGB/F07.SAUVE.BNM159AP
       m_FileAssign -d NEW,CATLG,DELETE -r 49 -t LSEQ -g +1 OUT62 ${DATA}/PNCGB/F96.SAUVE.BNM159AB
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 OUT63 ${DATA}/PNCGP/F07.SAUVE.SAU160AP
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 OUT64 ${DATA}/PNCGY/F45.SAUVE.SAU160AY
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 OUT65 ${DATA}/PNCGM/F89.SAUVE.SAU160AM
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 OUT66 ${DATA}/PNCGO/F16.SAUVE.SAU160AO
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 OUT67 ${DATA}/PNCGD/F91.SAUVE.SAU160AD
       m_FileAssign -d NEW,CATLG,DELETE -r 120 -t LSEQ -g +1 OUT68 ${DATA}/PNCGL/F61.SAUVE.SAU160AL
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT69 ${DATA}/PNCGB/F96.SAUVE.SAU160AB
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT70 ${DATA}/PNCGP/F07.SAUVE.SAU175AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT71 ${DATA}/PNCGY/F45.SAUVE.SAU175AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT72 ${DATA}/PNCGM/F89.SAUVE.SAU175AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT73 ${DATA}/PNCGO/F16.SAUVE.SAU175AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT74 ${DATA}/PNCGD/F91.SAUVE.SAU175AD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT75 ${DATA}/PNCGL/F61.SAUVE.SAU175AL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT76 ${DATA}/PNCGB/F96.SAUVE.SAU175AB
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT77 ${DATA}/PNCGP/F07.SAUVE.SAU176AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT78 ${DATA}/PNCGY/F45.SAUVE.SAU176AY
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT79 ${DATA}/PNCGM/F89.SAUVE.SAU176AM
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT80 ${DATA}/PNCGO/F16.SAUVE.SAU176AO
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT81 ${DATA}/PNCGD/F91.SAUVE.SAU176AD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT82 ${DATA}/PNCGL/F61.SAUVE.SAU176AL
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT83 ${DATA}/PNCGB/F96.SAUVE.SAU176AB
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 OUT84 ${DATA}/PNCGD/F91.SAUVE.SAU161AD
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 OUT85 ${DATA}/PNCGL/F61.SAUVE.SAU161AL
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 OUT86 ${DATA}/PNCGM/F89.SAUVE.SAU161AM
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 OUT87 ${DATA}/PNCGO/F16.SAUVE.SAU161AO
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 OUT88 ${DATA}/PNCGP/F07.SAUVE.SAU161AP
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 OUT89 ${DATA}/PNCGY/F45.SAUVE.SAU161AY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT90 ${DATA}/PNCGD/F91.SAUVE.SAU052AD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT91 ${DATA}/PNCGL/F61.SAUVE.SAU052AL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT92 ${DATA}/PNCGM/F89.SAUVE.SAU052AM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT93 ${DATA}/PNCGO/F16.SAUVE.SAU052AO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT94 ${DATA}/PNCGP/F07.SAUVE.SAU052AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT95 ${DATA}/P908/SEM.SAUVE.SAU052AX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT96 ${DATA}/PNCGL/F45.SAUVE.SAU052AY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT97 ${DATA}/PNCGK/F44.SAUVE.FV001K
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT98 ${DATA}/PXX0/F99.SAUVE.FTI01K
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT99 ${DATA}/PXX0/F99.SAUVE.HD001P
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT100 ${DATA}/PXX0/F99.SAUVE.IR010P
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT101 ${DATA}/PNCGK/F44.SAUVE.FV001K1
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT102 ${DATA}/PNCGK/F44.SAUVE.FV001K2
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTRAZFAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTRAZFAB
       ;;
(FTRAZFAB)
       m_CondExec 16,NE,FTRAZFAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE DES FICHIERS FINANCEMENTS                                        
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTRAZFAD PGM=FDRABR     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTRAZFAG
       ;;
(FTRAZFAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
       m_FileAssign -d SHR IN21 /dev/null
       m_FileAssign -d SHR IN22 /dev/null
       m_FileAssign -d SHR IN23 /dev/null
       m_FileAssign -d SHR IN24 /dev/null
       m_FileAssign -d SHR IN25 /dev/null
       m_FileAssign -d SHR IN26 /dev/null
       m_FileAssign -d SHR IN27 /dev/null
       m_FileAssign -d SHR IN28 /dev/null
       m_FileAssign -d SHR IN29 /dev/null
       m_FileAssign -d SHR IN30 /dev/null
       m_FileAssign -d SHR IN31 /dev/null
       m_FileAssign -d SHR IN32 /dev/null
       m_FileAssign -d SHR IN33 /dev/null
       m_FileAssign -d SHR IN34 /dev/null
       m_FileAssign -d SHR IN35 /dev/null
       m_FileAssign -d SHR IN36 /dev/null
       m_FileAssign -d SHR IN37 /dev/null
       m_FileAssign -d SHR IN38 /dev/null
       m_FileAssign -d SHR IN39 /dev/null
       m_FileAssign -d SHR IN40 /dev/null
       m_FileAssign -d SHR IN41 /dev/null
       m_FileAssign -d SHR IN42 /dev/null
       m_FileAssign -d SHR IN43 /dev/null
       m_FileAssign -d SHR IN44 /dev/null
       m_FileAssign -d SHR IN45 /dev/null
       m_FileAssign -d SHR IN46 /dev/null
       m_FileAssign -d SHR IN47 /dev/null
       m_FileAssign -d SHR IN48 /dev/null
       m_FileAssign -d SHR IN49 /dev/null
       m_FileAssign -d SHR IN50 /dev/null
       m_FileAssign -d SHR IN51 /dev/null
       m_FileAssign -d SHR IN52 /dev/null
       m_FileAssign -d SHR IN53 /dev/null
       m_FileAssign -d SHR IN54 /dev/null
       m_FileAssign -d SHR IN55 /dev/null
       m_FileAssign -d SHR IN56 /dev/null
       m_FileAssign -d SHR IN57 /dev/null
       m_FileAssign -d SHR IN58 /dev/null
       m_FileAssign -d SHR IN59 /dev/null
       m_FileAssign -d SHR IN60 /dev/null
       m_FileAssign -d SHR IN61 /dev/null
       m_FileAssign -d SHR IN62 /dev/null
       m_FileAssign -d SHR IN63 /dev/null
       m_FileAssign -d SHR IN64 /dev/null
       m_FileAssign -d SHR IN65 /dev/null
       m_FileAssign -d SHR IN66 /dev/null
       m_FileAssign -d SHR IN67 /dev/null
       m_FileAssign -d SHR IN68 /dev/null
       m_FileAssign -d SHR IN69 /dev/null
       m_FileAssign -d SHR IN70 /dev/null
       m_FileAssign -d SHR IN71 /dev/null
       m_FileAssign -d SHR IN72 /dev/null
       m_FileAssign -d SHR IN73 /dev/null
       m_FileAssign -d SHR IN74 /dev/null
       m_FileAssign -d SHR IN75 /dev/null
       m_FileAssign -d SHR IN76 /dev/null
       m_FileAssign -d SHR IN77 /dev/null
       m_FileAssign -d SHR IN78 /dev/null
       m_FileAssign -d SHR IN79 /dev/null
       m_FileAssign -d SHR IN80 /dev/null
       m_FileAssign -d SHR IN81 /dev/null
       m_FileAssign -d SHR IN82 /dev/null
       m_FileAssign -d SHR IN83 /dev/null
       m_FileAssign -d SHR IN84 /dev/null
       m_FileAssign -d SHR IN85 /dev/null
       m_FileAssign -d SHR IN86 /dev/null
       m_FileAssign -d SHR IN87 /dev/null
       m_FileAssign -d SHR IN88 /dev/null
       m_FileAssign -d SHR IN89 /dev/null
       m_FileAssign -d SHR IN90 /dev/null
       m_FileAssign -d SHR IN91 /dev/null
       m_FileAssign -d SHR IN92 /dev/null
       m_FileAssign -d SHR IN93 /dev/null
       m_FileAssign -d SHR IN94 /dev/null
       m_FileAssign -d SHR IN95 /dev/null
       m_FileAssign -d SHR IN96 /dev/null
       m_FileAssign -d SHR IN97 /dev/null
       m_FileAssign -d SHR IN98 /dev/null
       m_FileAssign -d SHR IN99 /dev/null
       m_FileAssign -d SHR IN100 /dev/null
       m_FileAssign -d SHR IN101 /dev/null
       m_FileAssign -d SHR IN102 /dev/null
       m_FileAssign -d SHR IN103 /dev/null
       m_FileAssign -d SHR IN104 /dev/null
       m_FileAssign -d SHR IN105 /dev/null
       m_FileAssign -d SHR IN106 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F91.BBS001AD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F61.BBS001AL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F89.BBS001AM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/F16.BBS001AO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT5 ${DATA}/PXX0/F07.BBS001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT6 ${DATA}/P908/SEM.BBS001AX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT7 ${DATA}/PXX0/F45.BBS001AY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT8 ${DATA}/PNCGD/F91.BBS052AD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT9 ${DATA}/PNCGL/F61.BBS052AL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT10 ${DATA}/PNCGM/F89.BBS052AM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT11 ${DATA}/PNCGO/F16.BBS052AO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT12 ${DATA}/PNCGP/F07.BBS052AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT13 ${DATA}/P908/SEM.BBS052AX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT14 ${DATA}/PNCGY/F45.BBS052AY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT15 ${DATA}/PXX0/F91.BMD002CD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT16 ${DATA}/PXX0/F61.BMD002CL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT17 ${DATA}/PXX0/F89.BMD002CM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT18 ${DATA}/PXX0/F16.BMD002CO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT19 ${DATA}/PXX0/F07.BMD002CP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT20 ${DATA}/P908/SEM.BMD002CX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT21 ${DATA}/PXX0/F45.BMD002CY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT22 ${DATA}/PNCGB/F96.BNM001CB
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT23 ${DATA}/PNCGD/F91.BNM001CD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT24 ${DATA}/PNCGL/F61.BNM001CL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT25 ${DATA}/PNCGM/F89.BNM001CM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT26 ${DATA}/PNCGO/F16.BNM001CO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT27 ${DATA}/PNCGP/F07.BNM001CP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT28 ${DATA}/PNCGV/F75.BNM001CV
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT29 ${DATA}/P908/SEM.BNM001CX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT30 ${DATA}/PNCGY/F45.BNM001CY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT31 ${DATA}/PNCGB/F96.BNM001DB
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT32 ${DATA}/PNCGD/F91.BNM001DD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT33 ${DATA}/PNCGL/F61.BNM001DL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT34 ${DATA}/PNCGM/F89.BNM001DM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT35 ${DATA}/PNCGO/F16.BNM001DO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT36 ${DATA}/PNCGP/F07.BNM001DP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT37 ${DATA}/PNCGR/F94.BNM001DR
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT38 ${DATA}/PNCGY/F45.BNM001DY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT39 ${DATA}/PNCGD/F91.BNM003AD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT40 ${DATA}/PNCGL/F61.BNM003AL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT41 ${DATA}/PNCGM/F89.BNM003AM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT42 ${DATA}/PNCGO/F16.BNM003AO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT43 ${DATA}/PNCGP/F07.BNM003AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT44 ${DATA}/P908/SEM.BNM003AX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT45 ${DATA}/PNCGY/F45.BNM003AY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT46 ${DATA}/PXX0/F07.BPC002AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT47 ${DATA}/PXX0/F91.CREDORBD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT48 ${DATA}/PXX0/F61.CREDORBL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT49 ${DATA}/PXX0/F89.CREDORBM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT50 ${DATA}/PXX0/F16.CREDORBO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT51 ${DATA}/PNCGP/F07.CREDORBP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT52 ${DATA}/PXX0/F45.CREDORBY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT53 ${DATA}/PNCGB/F96.FFTI20AB
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT54 ${DATA}/PNCGD/F91.FFTI20AD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT55 ${DATA}/PNCGL/F61.FFTI20AL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT56 ${DATA}/PNCGM/F89.FFTI20AM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT57 ${DATA}/PNCGO/F16.FFTI20AO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT58 ${DATA}/P908/SEM.FFTI20AX
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT59 ${DATA}/PNCGY/F45.FFTI20AY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT60 ${DATA}/PXX0/F97.HIDEALBK
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT61 ${DATA}/PNCGD/F91.NASGCTD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT62 ${DATA}/PNCGL/F61.NASGCTL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT63 ${DATA}/PNCGM/F89.NASGCTM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT64 ${DATA}/PNCGO/F16.NASGCTO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT65 ${DATA}/PNCGP/F07.NASGCTP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT66 ${DATA}/PNCGY/F45.NASGCTY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT67 ${DATA}/PXX0/F89.SAFIDALG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT68 ${DATA}/PXX0/F07.SAFIDIFG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT69 ${DATA}/PXX0/F61.SAFIDNNG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT70 ${DATA}/PXX0/F16.SAFIDOG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT71 ${DATA}/PXX0/F91.SAFIDPMG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT72 ${DATA}/PXX0/F45.SAFIDRAG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT73 ${DATA}/PXX0/F08.SAFILUXG
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT74 ${DATA}/PXX0/F07.CREDORAP
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT75 ${DATA}/PXX0/F07.SAFIGKDO
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT76 ${DATA}/PXX0/FTP.F97.HIDEALAK
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT77 ${DATA}/PXX0/F07.FFT100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT78 ${DATA}/PXX0/F07.FPC049AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT79 ${DATA}/PXX0/F07.FPC050AP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT80 ${DATA}/PNCGK/F97.BKF002AK.INTERGCT
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT81 ${DATA}/PXX0/F07.BPC002BP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT82 ${DATA}/PXX0/F07.FFT100BP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT83 ${DATA}/PXX0/F07.FPC049BP
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT84 ${DATA}/PXX0/FTP.F07.EXTLIKDO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT85 ${DATA}/PXX0/F07.HD001PP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT86 ${DATA}/PXX0/F91.HD001PD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT87 ${DATA}/PXX0/F61.HD001PL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT88 ${DATA}/PXX0/F89.HD001PM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT89 ${DATA}/PXX0/F16.HD001PO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT90 ${DATA}/PXX0/F20.HD001PK
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT91 ${DATA}/PXX0/F45.HD001PY
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT92 ${DATA}/PXX0/FTP.F07.HD0001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT93 ${DATA}/PXX0/F07.BIR010PP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT94 ${DATA}/PXX0/F16.BIR010PO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT95 ${DATA}/PXX0/F45.BIR010PY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT96 ${DATA}/PXX0/F61.BIR010PL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT97 ${DATA}/PXX0/F89.BIR010PM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT98 ${DATA}/PXX0/F91.BIR010PD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT99 ${DATA}/PXX0/F07.FPC050BP
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT100 ${DATA}/PXX0/FTP.F97.DM1000AK
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT101 ${DATA}/PXX0/F91.PSE050CD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT102 ${DATA}/PXX0/F61.PSE050CL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT103 ${DATA}/PXX0/F89.PSE050CM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT104 ${DATA}/PXX0/F16.PSE050CO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT105 ${DATA}/PXX0/F07.PSE050CP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT106 ${DATA}/PXX0/F45.PSE050CY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTRAZFAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTRAZFAH
       ;;
(FTRAZFAH)
       m_CondExec 16,NE,FTRAZFAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES GDG DE L'ECS    CREE UNE GENERATION  A VIDE               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTRAZFAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FTRAZFAJ
       ;;
(FTRAZFAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
       m_FileAssign -d SHR IN21 /dev/null
       m_FileAssign -d SHR IN22 /dev/null
       m_FileAssign -d SHR IN23 /dev/null
       m_FileAssign -d SHR IN24 /dev/null
       m_FileAssign -d SHR IN25 /dev/null
       m_FileAssign -d SHR IN26 /dev/null
       m_FileAssign -d SHR IN27 /dev/null
       m_FileAssign -d SHR IN28 /dev/null
       m_FileAssign -d SHR IN29 /dev/null
       m_FileAssign -d SHR IN30 /dev/null
       m_FileAssign -d SHR IN31 /dev/null
       m_FileAssign -d SHR IN32 /dev/null
       m_FileAssign -d SHR IN33 /dev/null
       m_FileAssign -d SHR IN34 /dev/null
       m_FileAssign -d SHR IN35 /dev/null
       m_FileAssign -d SHR IN36 /dev/null
       m_FileAssign -d SHR IN37 /dev/null
       m_FileAssign -d SHR IN38 /dev/null
       m_FileAssign -d SHR IN39 /dev/null
       m_FileAssign -d SHR IN40 /dev/null
       m_FileAssign -d SHR IN41 /dev/null
       m_FileAssign -d SHR IN42 /dev/null
       m_FileAssign -d SHR IN43 /dev/null
       m_FileAssign -d SHR IN44 /dev/null
       m_FileAssign -d SHR IN45 /dev/null
       m_FileAssign -d SHR IN46 /dev/null
       m_FileAssign -d SHR IN47 /dev/null
       m_FileAssign -d SHR IN48 /dev/null
       m_FileAssign -d SHR IN49 /dev/null
       m_FileAssign -d SHR IN50 /dev/null
       m_FileAssign -d SHR IN51 /dev/null
       m_FileAssign -d SHR IN52 /dev/null
       m_FileAssign -d SHR IN53 /dev/null
       m_FileAssign -d SHR IN54 /dev/null
       m_FileAssign -d SHR IN55 /dev/null
       m_FileAssign -d SHR IN56 /dev/null
       m_FileAssign -d SHR IN57 /dev/null
       m_FileAssign -d SHR IN58 /dev/null
       m_FileAssign -d SHR IN59 /dev/null
       m_FileAssign -d SHR IN60 /dev/null
       m_FileAssign -d SHR IN61 /dev/null
       m_FileAssign -d SHR IN62 /dev/null
       m_FileAssign -d SHR IN63 /dev/null
       m_FileAssign -d SHR IN64 /dev/null
       m_FileAssign -d SHR IN65 /dev/null
       m_FileAssign -d SHR IN66 /dev/null
       m_FileAssign -d SHR IN67 /dev/null
       m_FileAssign -d SHR IN68 /dev/null
       m_FileAssign -d SHR IN69 /dev/null
       m_FileAssign -d SHR IN70 /dev/null
       m_FileAssign -d SHR IN71 /dev/null
       m_FileAssign -d SHR IN72 /dev/null
       m_FileAssign -d SHR IN73 /dev/null
       m_FileAssign -d SHR IN74 /dev/null
       m_FileAssign -d SHR IN75 /dev/null
       m_FileAssign -d SHR IN76 /dev/null
       m_FileAssign -d SHR IN77 /dev/null
       m_FileAssign -d SHR IN78 /dev/null
       m_FileAssign -d SHR IN79 /dev/null
       m_FileAssign -d SHR IN80 /dev/null
       m_FileAssign -d SHR IN81 /dev/null
       m_FileAssign -d SHR IN82 /dev/null
       m_FileAssign -d SHR IN83 /dev/null
       m_FileAssign -d SHR IN84 /dev/null
       m_FileAssign -d SHR IN85 /dev/null
       m_FileAssign -d SHR IN86 /dev/null
       m_FileAssign -d SHR IN87 /dev/null
       m_FileAssign -d SHR IN88 /dev/null
       m_FileAssign -d SHR IN89 /dev/null
       m_FileAssign -d SHR IN90 /dev/null
       m_FileAssign -d SHR IN91 /dev/null
       m_FileAssign -d SHR IN92 /dev/null
       m_FileAssign -d SHR IN93 /dev/null
       m_FileAssign -d SHR IN94 /dev/null
       m_FileAssign -d SHR IN95 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT1 ${DATA}/PAB0/F96.ABELGCTB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT2 ${DATA}/PAB0/F91.ABELGCTD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT3 ${DATA}/PAB0/F97.ABELGCTK
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT4 ${DATA}/PAB0/F61.ABELGCTL
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT5 ${DATA}/PAB0/F89.ABELGCTM
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT6 ${DATA}/PAB0/F16.ABELGCTO
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT7 ${DATA}/PAB0/F07.ABELGCTP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT8 ${DATA}/P908/SEM.ABELGCTX
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g ${G_A1} OUT9 ${DATA}/P908/SEM.ABELGCTX
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT10 ${DATA}/PXX0/F91.AV001D
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT11 ${DATA}/PXX0/F61.AV001L
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT12 ${DATA}/PXX0/F89.AV001M
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT13 ${DATA}/PXX0/F16.AV001O
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT14 ${DATA}/PXX0/F07.AV001P
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT15 ${DATA}/PXX0/F08.AV001X
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT16 ${DATA}/PXX0/F45.AV001Y
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT17 ${DATA}/PXX0/F91.AV201D
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT18 ${DATA}/PXX0/F61.AV201L
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT19 ${DATA}/PXX0/F89.AV201M
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT20 ${DATA}/PXX0/F16.AV201O
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT21 ${DATA}/PXX0/F07.AV201P
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT22 ${DATA}/PXX0/F08.AV201X
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT23 ${DATA}/PXX0/F45.AV201Y
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT24 ${DATA}/PXX0/F96.FG14FPB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT25 ${DATA}/PXX0/F91.FG14FPD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT26 ${DATA}/PXX0/F44.FG14FPK
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT27 ${DATA}/PXX0/F61.FG14FPL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT28 ${DATA}/PXX0/F89.FG14FPM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT29 ${DATA}/PXX0/F16.FG14FPO
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT30 ${DATA}/PXX0/F07.FG14FPC
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT31 ${DATA}/PXX0/F08.FG14FPX
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT32 ${DATA}/PXX0/F45.FG14FPY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT33 ${DATA}/PNCGB/F96.FTICSGCB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT34 ${DATA}/PNCGK/F97.FTICSGCK
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT35 ${DATA}/P908/SEM.FTICSGCX
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT36 ${DATA}/PNCGD/F91.FTV05DB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT37 ${DATA}/PNCGL/F61.FTV05LB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT38 ${DATA}/PNCGM/F89.FTV05MB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT39 ${DATA}/PNCGO/F16.FTV05OB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT40 ${DATA}/PNCGP/F07.FTV05PB
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT41 ${DATA}/PNCGY/F45.FTV05YB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT42 ${DATA}/PXX0/F91.GBA00D
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT43 ${DATA}/PXX0/F61.GBA00L
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT44 ${DATA}/PXX0/F89.GBA00M
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT45 ${DATA}/PXX0/F16.GBA00O
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT46 ${DATA}/PXX0/F07.GBA00P
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT47 ${DATA}/PXX0/F08.GBA00X
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT48 ${DATA}/PXX0/F45.GBA00Y
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT49 ${DATA}/PXX0/F91.GBA011AD
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT50 ${DATA}/PNCGL/F61.GBA011AL
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT51 ${DATA}/PNCGM/F89.GBA011AM
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT52 ${DATA}/PNCGO/F16.GBA011AO
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT53 ${DATA}/PXX0/F07.GBA011AP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT54 ${DATA}/P908/SEM.GBA011AX
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT55 ${DATA}/PXX0/F45.GBA011AY
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT56 ${DATA}/PXX0/F91.GBA012AD
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT57 ${DATA}/PNCGL/F61.GBA012AL
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT58 ${DATA}/PNCGM/F89.GBA012AM
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT59 ${DATA}/PNCGO/F16.GBA012AO
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT60 ${DATA}/PXX0/F07.GBA012AP
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT61 ${DATA}/P908/SEM.GBA012AX
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT62 ${DATA}/PXX0/F45.GBA012AY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT63 ${DATA}/PXX0/F91.GBA02D
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT64 ${DATA}/PXX0/F61.GBA02L
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT65 ${DATA}/PXX0/F89.GBA02M
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT66 ${DATA}/PXX0/F16.GBA02O
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT67 ${DATA}/PXX0/F07.GBA02P
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT68 ${DATA}/PXX0/F08.GBA02X
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT69 ${DATA}/PXX0/F45.GBA02Y
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT70 ${DATA}/PXX0/F96.GRA00B
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT71 ${DATA}/PXX0/F91.GRA00D
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT72 ${DATA}/PXX0/F44.GRA00K
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT73 ${DATA}/PXX0/F61.GRA00L
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT74 ${DATA}/PXX0/F89.GRA00M
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT75 ${DATA}/PXX0/F16.GRA00O
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT76 ${DATA}/PXX0/F07.GRA00P
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT77 ${DATA}/PXX0/F08.GRA00X
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT78 ${DATA}/PXX0/F45.GRA00Y
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT79 ${DATA}/PXX0/F96.IF00FPB
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT80 ${DATA}/PXX0/F91.IF00FPD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT81 ${DATA}/PXX0/F44.IF00FPK
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT82 ${DATA}/PXX0/F61.IF00FPL
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT83 ${DATA}/PXX0/F89.IF00FPM
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT84 ${DATA}/PXX0/F16.IF00FPO
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT85 ${DATA}/PXX0/F07.IF00FPP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT86 ${DATA}/PXX0/F08.IF00FPX
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT87 ${DATA}/PXX0/F45.IF00FPY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT88 ${DATA}/PXX0/F91.PSE00D
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT89 ${DATA}/PXX0/F61.PSE00L
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT90 ${DATA}/PXX0/F89.PSE00M
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT91 ${DATA}/PXX0/F16.PSE00O
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT92 ${DATA}/PXX0/F07.PSE00P
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT93 ${DATA}/PXX0/F08.PSE00X
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT94 ${DATA}/PXX0/F45.PSE00Y
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT95 ${DATA}/PXX0/FTP.F07.HIDECLAP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTRAZFAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTRAZFAK
       ;;
(FTRAZFAK)
       m_CondExec 16,NE,FTRAZFAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DES GDG DES FICHIER POUR SAP (CGECS ET CGICS)                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTRAZFAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FTRAZFAM
       ;;
(FTRAZFAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
       m_FileAssign -d SHR IN4 /dev/null
       m_FileAssign -d SHR IN5 /dev/null
       m_FileAssign -d SHR IN6 /dev/null
       m_FileAssign -d SHR IN7 /dev/null
       m_FileAssign -d SHR IN8 /dev/null
       m_FileAssign -d SHR IN9 /dev/null
       m_FileAssign -d SHR IN10 /dev/null
       m_FileAssign -d SHR IN11 /dev/null
       m_FileAssign -d SHR IN12 /dev/null
       m_FileAssign -d SHR IN13 /dev/null
       m_FileAssign -d SHR IN14 /dev/null
       m_FileAssign -d SHR IN15 /dev/null
       m_FileAssign -d SHR IN16 /dev/null
       m_FileAssign -d SHR IN17 /dev/null
       m_FileAssign -d SHR IN18 /dev/null
       m_FileAssign -d SHR IN19 /dev/null
       m_FileAssign -d SHR IN20 /dev/null
       m_FileAssign -d SHR IN21 /dev/null
       m_FileAssign -d SHR IN22 /dev/null
       m_FileAssign -d SHR IN23 /dev/null
       m_FileAssign -d SHR IN24 /dev/null
       m_FileAssign -d SHR IN25 /dev/null
       m_FileAssign -d SHR IN26 /dev/null
       m_FileAssign -d SHR IN27 /dev/null
       m_FileAssign -d SHR IN28 /dev/null
       m_FileAssign -d SHR IN29 /dev/null
       m_FileAssign -d SHR IN30 /dev/null
       m_FileAssign -d SHR IN31 /dev/null
       m_FileAssign -d SHR IN32 /dev/null
       m_FileAssign -d SHR IN33 /dev/null
       m_FileAssign -d SHR IN34 /dev/null
       m_FileAssign -d SHR IN35 /dev/null
       m_FileAssign -d SHR IN36 /dev/null
       m_FileAssign -d SHR IN37 /dev/null
       m_FileAssign -d SHR IN38 /dev/null
       m_FileAssign -d SHR IN39 /dev/null
       m_FileAssign -d SHR IN40 /dev/null
       m_FileAssign -d SHR IN41 /dev/null
       m_FileAssign -d SHR IN42 /dev/null
       m_FileAssign -d SHR IN43 /dev/null
       m_FileAssign -d SHR IN44 /dev/null
       m_FileAssign -d SHR IN45 /dev/null
       m_FileAssign -d SHR IN46 /dev/null
       m_FileAssign -d SHR IN47 /dev/null
       m_FileAssign -d SHR IN48 /dev/null
       m_FileAssign -d SHR IN49 /dev/null
       m_FileAssign -d SHR IN50 /dev/null
       m_FileAssign -d SHR IN51 /dev/null
       m_FileAssign -d SHR IN52 /dev/null
       m_FileAssign -d SHR IN53 /dev/null
       m_FileAssign -d SHR IN54 /dev/null
       m_FileAssign -d SHR IN55 /dev/null
       m_FileAssign -d SHR IN56 /dev/null
       m_FileAssign -d SHR IN57 /dev/null
       m_FileAssign -d SHR IN58 /dev/null
       m_FileAssign -d SHR IN59 /dev/null
       m_FileAssign -d SHR IN60 /dev/null
       m_FileAssign -d SHR IN61 /dev/null
       m_FileAssign -d SHR IN62 /dev/null
       m_FileAssign -d SHR IN63 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F91.FV001D
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F61.FV001L
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F89.FV001M
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT4 ${DATA}/PXX0/F16.FV001O
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT5 ${DATA}/PXX0/F45.FV001Y
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT6 ${DATA}/PXX0/F07.FV001P
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT7 ${DATA}/PXX0/F91.CC200D
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT8 ${DATA}/PXX0/F61.CC200L
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT9 ${DATA}/PXX0/F89.CC200M
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT10 ${DATA}/PXX0/F16.CC200O
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT11 ${DATA}/PXX0/F45.CC200Y
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT12 ${DATA}/PXX0/F07.CC200P
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT13 ${DATA}/PXX0/F91.FTI01D
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT14 ${DATA}/PXX0/F61.FTI01L
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT15 ${DATA}/PXX0/F89.FTI01M
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT16 ${DATA}/PXX0/F16.FTI01O
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT17 ${DATA}/PXX0/F45.FTI01Y
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT18 ${DATA}/PXX0/F07.FTI01P
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT19 ${DATA}/PXX0/F99.CGECSF
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT20 ${DATA}/PXX0/F07.CGECSP
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT21 ${DATA}/PXX0/F99.CGICSF
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT22 ${DATA}/PXX0/F07.CGICSP
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT23 ${DATA}/PXX0/F07.CGSIGP
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT24 ${DATA}/PXX0/F16.CGSIGO
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT25 ${DATA}/PXX0/F45.CGSIGY
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT26 ${DATA}/PXX0/F61.CGSIGL
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT27 ${DATA}/PXX0/F89.CGSIGM
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT28 ${DATA}/PXX0/F91.CGSIGD
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT29 ${DATA}/PXX0/F07.FG14FPA
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT30 ${DATA}/PXX0/F91.FFTI00GD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT31 ${DATA}/PXX0/F61.FFTI00EL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT32 ${DATA}/PXX0/F89.FFTI00EM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT33 ${DATA}/PXX0/F16.FFTI00EO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT34 ${DATA}/PXX0/F07.FFTI00EP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT35 ${DATA}/PXX0/F45.FFTI00EY
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT36 ${DATA}/PXX0/F07.FG14FPP
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT37 ${DATA}/PXX0/F44.FV001K
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT38 ${DATA}/PXX0/F99.FTI01K
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT39 ${DATA}/PXX0/F91.BBS002AD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT40 ${DATA}/PXX0/F61.BBS002AL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT41 ${DATA}/PXX0/F89.BBS002AM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT42 ${DATA}/PXX0/F07.BBS002AP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT43 ${DATA}/PXX0/F16.BBS002AO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT44 ${DATA}/PXX0/F45.BBS002AY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT45 ${DATA}/P908/SEM.BBS002AX
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT46 ${DATA}/PXX0/F91.BMO000BD
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT47 ${DATA}/PXX0/F61.BMO000BL
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT48 ${DATA}/PXX0/F89.BMO000BM
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT49 ${DATA}/PXX0/F07.BMO000BP
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT50 ${DATA}/PXX0/F16.BMO000BO
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 OUT51 ${DATA}/PXX0/F45.BMO000BY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT52 ${DATA}/PXX0/F91.BMO020AD.FTI01D
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT53 ${DATA}/PXX0/F61.BMO020AL.FTI01L
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT54 ${DATA}/PXX0/F89.BMO020AM.FTI01M
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT55 ${DATA}/PXX0/F07.BMO020AP.FTI01P
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT56 ${DATA}/PXX0/F16.BMO020AO.FTI01O
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT57 ${DATA}/PXX0/F45.BMO020AY.FTI01Y
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT58 ${DATA}/PXX0/F44.FV001K1
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT59 ${DATA}/PXX0/F44.FV001K2
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 OUT60 ${DATA}/PXX0/F44.CGBOXP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT61 ${DATA}/PXX0/F07.FTI01K1
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT62 ${DATA}/PXX0/F99.FTI01K2
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT63 ${DATA}/PXX0/F97.SAFIKAPG.NOT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTRAZFAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTRAZFAN
       ;;
(FTRAZFAN)
       m_CondExec 16,NE,FTRAZFAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
