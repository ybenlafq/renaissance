#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQCPIP.ksh                       --- VERSION DU 08/10/2016 22:21
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMQCPI -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 07/03/07 AT 11.37.37 BY BURTEC6                      
#    STANDARDS: P  JOBSET: MQCPIP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BMQCPI                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MQCPIPA
       ;;
(MQCPIPA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2007/03/07 AT 11.37.37 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: MQCPIP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'COPIE QUEUE 0015'                      
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MQCPIPAA
       ;;
(MQCPIPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP1
# ========================================                                     
#  FQUEIN   : QUEUE EN ENTREE = Q 0015 - MESSAGES DE CTL EN ANOMALIE           
# ========================================                                     
       m_FileAssign -d SHR FQUEIN ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP2
# ========================================                                     
#  FQUEOUT  : QUEUE EN SORTIE = Q 0002                                         
# ========================================                                     
       m_FileAssign -d SHR FQUEOUT ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP3
# ========================================                                     
#  FBROW    : B = BROWSE / D = DESCRIPTIF                                      
# ========================================                                     
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP4
# ========================================                                     
#  FSYNC    : CODE SYNCPOINT / NOSYNCPOINT                                     
# ========================================                                     
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP5
# ========================================                                     
#  FDEAD    : CODE DEAD LETTER O/N - ON LIT DEPUIS UNE DEAD LETTER Q           
# ========================================                                     
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP6
# ========================================                                     
# --  PARAMETRES FACULTATIFS                                                   
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP7
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP8
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP9
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP10
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP11
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP12
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP13
# -- FBACK - METTRE O POUR REMETTRE LE BACKOUT COUNT A 0                       
       m_FileAssign -d SHR FBACK ${DATA}/CORTEX4.P.MTXTFIX1/MQCPIP14
       m_ProgramExec BMQCPI 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
