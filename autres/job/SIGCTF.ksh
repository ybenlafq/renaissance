#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SIGCTF.ksh                       --- VERSION DU 08/10/2016 22:13
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFSIGCT -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/09/30 AT 09.38.25 BY BURTECA                      
#    STANDARDS: P  JOBSET: SIGCTF                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='SIGA'                                                              
# ********************************************************************         
#  EXTRACTION 991                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=SIGCTFA
       ;;
(SIGCTFA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2014/09/30 AT 09.38.25 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: SIGCTF                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'INTERFACE SIGA/GCT'                    
# *                           APPL...: REPPARIS                                
# *                           WAIT...: GCT02                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=SIGCTFAA
       ;;
(SIGCTFAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
#  REPRISE: OUI                                                                
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.SXGCTD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "991"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAB
       ;;
(SIGCTFAB)
       m_CondExec 00,EQ,SIGCTFAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 828                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFAD
       ;;
(SIGCTFAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F828.SXGCTD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "828"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAE
       ;;
(SIGCTFAE)
       m_CondExec 00,EQ,SIGCTFAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 997                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFAG
       ;;
(SIGCTFAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F97.SXGCTK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "997"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAH
       ;;
(SIGCTFAH)
       m_CondExec 00,EQ,SIGCTFAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 511                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFAJ
       ;;
(SIGCTFAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F511.SXGCTK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "511"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAK
       ;;
(SIGCTFAK)
       m_CondExec 00,EQ,SIGCTFAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 802                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFAM
       ;;
(SIGCTFAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F802.SXGCTK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "802"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAN
       ;;
(SIGCTFAN)
       m_CondExec 00,EQ,SIGCTFAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 901                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFAQ
       ;;
(SIGCTFAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F901.SXGCTK
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "901"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAR
       ;;
(SIGCTFAR)
       m_CondExec 00,EQ,SIGCTFAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 961                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFAT
       ;;
(SIGCTFAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.SXGCTL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "961"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAU
       ;;
(SIGCTFAU)
       m_CondExec 00,EQ,SIGCTFAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 824                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFAX
       ;;
(SIGCTFAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F824.SXGCTL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "824"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFAY
       ;;
(SIGCTFAY)
       m_CondExec 00,EQ,SIGCTFAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 916                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBA
       ;;
(SIGCTFBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.SXGCTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "916"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFBB
       ;;
(SIGCTFBB)
       m_CondExec 00,EQ,SIGCTFBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 821                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBD
       ;;
(SIGCTFBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F821.SXGCTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "821"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFBE
       ;;
(SIGCTFBE)
       m_CondExec 00,EQ,SIGCTFBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 907                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBG
       ;;
(SIGCTFBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.SXGCTP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "907"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFBH
       ;;
(SIGCTFBH)
       m_CondExec 00,EQ,SIGCTFBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 829                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBJ
       ;;
(SIGCTFBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F829.SXGCTP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "829"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFBK
       ;;
(SIGCTFBK)
       m_CondExec 00,EQ,SIGCTFBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 945                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBM
       ;;
(SIGCTFBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.SXGCTY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "945"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFBN
       ;;
(SIGCTFBN)
       m_CondExec 00,EQ,SIGCTFBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 822                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBQ
       ;;
(SIGCTFBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F822.SXGCTY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "822"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFBR
       ;;
(SIGCTFBR)
       m_CondExec 00,EQ,SIGCTFBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION 513 NEW LE 30.09.204                                             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBT
       ;;
(SIGCTFBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F513.SX513K1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "513"
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTFBU
       ;;
(SIGCTFBU)
       m_CondExec 00,EQ,SIGCTFBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER GCT02 CREE UNE GENERATION A VIDE                   
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SIGCTFBX PGM=IDCAMS     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTFBX
       ;;
(SIGCTFBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.GCT02
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGCTFBX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SIGCTFBY
       ;;
(SIGCTFBY)
       m_CondExec 16,NE,SIGCTFBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
