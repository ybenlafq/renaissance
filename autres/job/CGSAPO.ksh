#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CGSAPO.ksh                       --- VERSION DU 08/10/2016 23:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POCGSAP -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/10 AT 15.37.14 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CGSAPO                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DE TOUS LES FICHIERS D'INTERFACE ECS                                    
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CGSAPOA
       ;;
(CGSAPOA)
       EXAAA=${EXAAA:-0}
       EXAAZ=${EXAAZ:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/07/10 AT 15.37.14 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CGSAPO                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'INTERFACE SAP'                         
# *                           APPL...: REPMGIO                                 
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CGSAPOAA
       ;;
(CGSAPOAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.FG14FPO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.IF00FPO
# ******* FIC DE REPRISE                                                       
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BCG000O.REPRISE
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.PSE00O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.GRA00O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.GBA00O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.GBA02O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.AV001O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.AV201O
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.CC200O
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FV001O
# ******************************************************************           
#  REMISE A ZERO DU FICHIER DE REPRISE                                         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_98_25 98 CH 25
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_18_10 18 CH 10
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_98_25 ASCENDING
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGSAPOAB
       ;;
(CGSAPOAB)
       m_CondExec 00,EQ,CGSAPOAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP CGSAPOAD PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CGSAPOAD
       ;;
(CGSAPOAD)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F16.BCG000O.REPRISE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSAPOAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CGSAPOAE
       ;;
(CGSAPOAE)
       m_CondExec 16,NE,CGSAPOAD ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
