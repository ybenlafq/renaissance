#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SX104P.ksh                       --- VERSION DU 09/10/2016 05:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSX104 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/02 AT 07.58.11 BY BURTEC2                      
#    STANDARDS: P  JOBSET: SX104P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='SIGA'                                                              
# *******************************************************************          
#  PKUNZIP DU FICHIER HRV7                                                     
#  REPRISE : OUI                                                               
# *******************************************************************          
# AAA      STEP  PGM=PKUNZIP                                                   
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# PRED     LINK  NAME=$EXPAIP,MODE=I                                           
# **************************************                                       
# //STEPLIB  DD  DSN=SYS2.ZIP390.D21ZTAB,DISP=SHR                              
# //         DD  DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                              
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# TARGET   FILE  NAME=BSX104EP,MODE=O                                          
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ARCHIVE(PXX0.FTP.F07.SIG02AP.G0001V00)                                      
# -EXTRACT                                                                     
# -OUTFILE(TARGET)                                                             
# -TRAN(ASCII850)                                                              
#          DATAEND                                                             
# *******************************************************************          
#  JAVAUNZIP  FICHIER HRV7                                                     
#  REPRISE : OUI                                                               
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=SX104PA
       ;;
(SX104PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2015/07/02 AT 07.58.10 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: SX104P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'ENVOI SIG02'                           
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=SX104PAA
       ;;
(SX104PAA)
       m_CondExec ${EXAAA},NE,YES 
# *************************************                                        
# DEPENDANCES POUR PLAN :             *                                        
# ----------------------              *                                        
# *************************************                                        
# ***** FICHIER EN SORTIE DE ZIP                                               
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SX104PAA.sysin
       m_ProgramExec JVMLDM76_UNZIP.ksh
# ********************************************************************         
#  PERMET DE FORMATTER L ETAT                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SX104PAD PGM=IRXJCL     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SX104PAD
       ;;
(SX104PAD)
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g +0  IN ${DATA}/PTEM/SX104PAA.BSX104EP
       m_FileAssign -d NEW,CATLG,DELETE -r 132 -t LSEQ -g +1 OUT ${DATA}/PXX0/F07.BSX104AP
       m_FileRepro -i IN -o OUT
# ********************************************************************         
#  PGM : EASYTRIEVE                                                            
#  -----------------                                                           
#  EDITION DU SIG02 : AJOUT DU SAUT DE PAGE                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SX104PAG PGM=EZTPA00    ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SX104PAG
       ;;
(SX104PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d NEW EZTVFM ${MT_TMP}/EZTVFM_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d SHR -g ${G_A2} FILEAI ${DATA}/PXX0/F07.BSX104AP
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ -g +1 FILEAO ${DATA}/PTEM/SX104PAG.SIG02BP
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/SX104PAG
       m_ProgramExec SX104PAG
# ********************************************************************         
#  EDITION DU SIG02 : ENVOI SOUS EOS POUR L'�DITION EN MAGASIN                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SX104PAJ PGM=IEBGENER   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SX104PAJ
       ;;
(SX104PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSTSPRT
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} SYSUT1 ${DATA}/PTEM/SX104PAG.SIG02BP
       m_OutputAssign -c 9 -w SIG02 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# ******************************************************************           
#   DELETE DES FICHIERS SEQUENTIELS NON GDG                                    
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP SX104PAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SX104PAM
       ;;
(SX104PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SX104PAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SX104PAN
       ;;
(SX104PAN)
       m_CondExec 16,NE,SX104PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SX104PZA
       ;;
(SX104PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SX104PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
