#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QDEP5P.ksh                       --- VERSION DU 08/10/2016 22:12
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPQDEP5 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/06/23 AT 11.26.39 BY BURTECM                      
#    STANDARDS: P  JOBSET: QDEP5P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM QQF QDEP05 HIT PARADE DES ENVOIS FOURNISSEURS EN ELA                    
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QDEP5PA
       ;;
(QDEP5PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=QDEP5PAA
       ;;
(QDEP5PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w QDEP05 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -i HOSTIN
FMOIS=$FMOIS_3_4$FMOIS_1_2
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b QDEP05 
       JUMP_LABEL=QDEP5PAB
       ;;
(QDEP5PAB)
       m_CondExec 04,GE,QDEP5PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM QQF QDEP06 HIT PARADE DES ENVOIS FOURNISSEURS EN TLM                    
# ********************************************************************         
# AF      STEP  PGM=IKJEFT01,PATTERN=EO3,PATKW=(L=QDEP06,F=,P=QDEP06,          
#               R=RDAR)                                                        
# OSTIN   DATA  CLASS=VAR,PARMS=QQFFMOIS,MBR=QDEP5P2                           
# ********************************************************************         
#   LISTE STATS LIVREE DETAIL  QMF120D                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QDEP5PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=QDEP5PAD
       ;;
(QDEP5PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QDEP06 DSQPRINT
       m_FileAssign -i QMFPARM
RUN PDARTY.QDEP06 (&&FMOIS='$FMOISJ_5_4$FMOISJ_3_2' &&SOC='$DIFDEP_1_3' FORM=PDARTY.FDEP06
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QDEP5P3 -a QMFPARM DSQPRINT
       JUMP_LABEL=QDEP5PAE
       ;;
(QDEP5PAE)
       m_CondExec 04,GE,QDEP5PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
