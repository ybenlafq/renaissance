#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM01Y.ksh                       --- VERSION DU 13/10/2016 17:32
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYIVM01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/09/16 AT 16.39.30 BY PREPA2                       
#    STANDARDS: P  JOBSET: IVM01Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BIV120 : PREPARATION DU FICHIER SERVANT A LOADER LA TABLE RTIN00            
#           IMAGE DES STOCKS MAGASINS AVANT INVENTAIRE                         
#  NOTE   : ON EXCLU LES STOCK DES MAGASINS PRESENTS DANS LA SOUS TABL         
#              EXMAG DE LA RTGA01 ET CE POUR TOUTES LES SOCIETES               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM01YA
       ;;
(IVM01YA)
#
#IVM01YAJ
#IVM01YAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVM01YAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2008/09/16 AT 16.39.30 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: IVM01Y                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'INVT STOCK MAG'                        
# *                           APPL...: IMPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM01YAA
       ;;
(IVM01YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  SOCIETEE A TRAITER                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******  TABLE GENERALISEE : SS TABLE EXMAG                                   
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  STOCK S/LIEU MAGASIN                                                 
#    RSGS30Y  : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30Y /dev/null
# ******  ARTICLES                                                             
#    RTGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00Y /dev/null
# ******  PRMP                                                                 
#    RTGG50Y  : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG50Y /dev/null
# ******  PRMP DACEM                                                           
#    RTGG55Y  : NAME=RSGG55Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGG55Y /dev/null
# ******  FAMILLES                                                             
#    RTGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14Y /dev/null
#                                                                              
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 FIN120 ${DATA}/PTEM/IVM01YAA.BIV120AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV120 
       JUMP_LABEL=IVM01YAB
       ;;
(IVM01YAB)
       m_CondExec 04,GE,IVM01YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC SERVANT A LOADER LA TABLE RTIN00 SUR L'INDEX CLUSTER             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM01YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM01YAD
       ;;
(IVM01YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IVM01YAA.BIV120AY
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 SORTOUT ${DATA}/PGI945/F45.RELOAD.IN00RY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_64_1 64 CH 1
 /FIELDS FLD_CH_37_3 37 CH 3
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_1_14 1 CH 14
 /FIELDS FLD_CH_36_1 36 CH 1
 /KEYS
   FLD_CH_1_14 ASCENDING,
   FLD_CH_16_7 ASCENDING,
   FLD_CH_37_3 ASCENDING,
   FLD_CH_36_1 ASCENDING,
   FLD_CH_64_1 ASCENDING
 /* Record Type = F  Record Length = 64 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM01YAE
       ;;
(IVM01YAE)
       m_CondExec 00,EQ,IVM01YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTIN00                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM01YAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM01YAG
       ;;
(IVM01YAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******  FICHIER DE LOAD                                                      
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PGI945/F45.RELOAD.IN00RY
# ******  TABLE INVENTAIRE                                                     
#    RSIN00Y  : NAME=RSIN00Y,MODE=U - DYNAM=YES                                
# -X-RSIN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSIN00Y /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM01YAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IVM01Y_IVM01YAG_RTIN00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IVM01YAH
       ;;
(IVM01YAH)
       m_CondExec 04,GE,IVM01YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REPAIR NOCOPY TABLESPACE RSIN00Y  DE LA D BASE PYDGI00                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM01YAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM01YZA
       ;;
(IVM01YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM01YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
