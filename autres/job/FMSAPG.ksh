#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FMSAPG.ksh                       --- VERSION DU 13/10/2016 16:20
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGFMSAP -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/06/27 AT 15.40.15 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FMSAPG                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# * RELOAD DE LA TABLE RTFM99 A PARTIR DU FICHIER SAPCPT                       
# * REPRISE .OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FMSAPGA
       ;;
(FMSAPGA)
#
#FMSAPGAD
#FMSAPGAD Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#FMSAPGAD
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/06/27 AT 15.40.15 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FMSAPG                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'LOAD DE TABLE'                         
# *                           APPL...: REPGROUP                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
#{ Post-translation ADAPT_LOAD_REPLACE
#       JUMP_LABEL=FMSAPGAA
#       ;;
#(FMSAPGAA)
#       m_CondExec ${EXAAA},NE,YES 
#       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
#       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
#       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
#       m_OutputAssign -c "*" ST01MSG
#       m_OutputAssign -c "*" PTIMSG01
#       m_OutputAssign -c "*" ST02MSG
#       m_OutputAssign -c "*" PTIMSG03
#       m_OutputAssign -c "*" ST03MSG
#       m_OutputAssign -c "*" PTIMSG02
#       m_OutputAssign -c "*" PTIMSG
## **************************************                                       
##  DEPENDANCES POUR PLAN :             *                                       
##    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
## **************************************                                       
##                                                                              
#       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSREC ${MT_TMP}/SYSREC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
## ************* TABLE A RELOADER ******                                        
##    RSFM99   : NAME=RSFM99G,MODE=I - DYNAM=YES                                
#       m_FileAssign -d SHR RSFM99 /dev/null
##                                                                              
## ******* FIC DE LOAD DE RTFM99 EN PROVENANCE DE SAP                           
#       m_FileAssign -d SHR -g +0 SYSULD ${DATA}/PXX0/FTP.SAP.SAPPCG
#       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FMSAPGAA.sysin
#       
#       #Untranslated utility: PTLDRIVM
     JUMP_LABEL=FMSAPGAA_SORT
       ;;
(FMSAPGAA_SORT)
     m_CondExec ${EXAAA},NE,YES
     m_OutputAssign -c "*" SYSOUT
     m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.SAP.SAPPCG
     m_FileAssign -d NEW,CATLG,DELETE SORTOUT ${MT_TMP}/FMSAPG_FMSAPGAA_RTFM99_${MT_JOB_NAME}_${MT_JOB_PID}
     m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS FLD_CH_1_8 1 CH 8
/FIELDS FLD_CH_9_1 9 CH 1
/FIELDS FLD_CH_10_6 10 CH 6
/FIELDS FLD_CH_16_6 16 CH 6
/KEYS
   FLD_CH_1_8  ASCENDING,
   FLD_CH_9_1  ASCENDING,
   FLD_CH_10_6  ASCENDING,
   FLD_CH_16_6  ASCENDING
_end
     m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FMSAPGAA
       ;;
(FMSAPGAA)
       m_CondExec ${EXAAA},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSREC ${MT_TMP}/SYSREC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ************* TABLE A RELOADER ******                                        
#    RSFM99   : NAME=RSFM99G,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSFM99 /dev/null
#                                                                              
# ******* FIC DE LOAD DE RTFM99 EN PROVENANCE DE SAP                           
       m_FileAssign -d SHR SYSULD ${MT_TMP}/FMSAPG_FMSAPGAA_RTFM99_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FMSAPGAA.sysin
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/FMSAPG_FMSAPGAA_RTFM99.sysload
       m_UtilityExec
#} Post-translation
# ********************************************************************         
#   REPAIR NOCOPYPEND DU TABLESPACE                                            
#       (REMISE DU STATUS EN READ WRITE DU TABLESPACE)                         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FMSAPGAD PGM=DSNUTILB   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
