#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ADO02P.ksh                       --- VERSION DU 08/10/2016 14:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPADO02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 08.37.57 BY BURTEC6                      
#    STANDARDS: P  JOBSET: ADO02P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EXTRACTION RTGA00                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=ADO02PA
       ;;
(ADO02PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXAQE=${EXAQE:-0}
       EXAQJ=${EXAQJ:-0}
       EXAQO=${EXAQO:-0}
       EXAQT=${EXAQT:-0}
       EXAQY=${EXAQY:-0}
       EXARD=${EXARD:-0}
       EXARI=${EXARI:-0}
       EXARN=${EXARN:-0}
       EXARS=${EXARS:-0}
       EXARX=${EXARX:-0}
       EXASC=${EXASC:-0}
       EXASH=${EXASH:-0}
       EXASM=${EXASM:-0}
       EXASR=${EXASR:-0}
       EXASW=${EXASW:-0}
       EXATB=${EXATB:-0}
       EXATG=${EXATG:-0}
       EXATL=${EXATL:-0}
       EXATQ=${EXATQ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A49=${G_A49:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A50=${G_A50:-'+1'}
       G_A51=${G_A51:-'+1'}
       G_A52=${G_A52:-'+1'}
       G_A53=${G_A53:-'+1'}
       G_A54=${G_A54:-'+2'}
       G_A55=${G_A55:-'+1'}
       G_A56=${G_A56:-'+2'}
       G_A57=${G_A57:-'+1'}
       G_A58=${G_A58:-'+1'}
       G_A59=${G_A59:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A60=${G_A60:-'+1'}
       G_A61=${G_A61:-'+1'}
       G_A62=${G_A62:-'+1'}
       G_A63=${G_A63:-'+1'}
       G_A64=${G_A64:-'+1'}
       G_A65=${G_A65:-'+1'}
       G_A66=${G_A66:-'+1'}
       G_A67=${G_A67:-'+1'}
       G_A68=${G_A68:-'+1'}
       G_A69=${G_A69:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A70=${G_A70:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       PROCSTEP=${PROCSTEP:-ADO02PA}
       RUN=${RUN}
       JUMP_LABEL=ADO02PAA
       ;;
(ADO02PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PAA.HDGA00AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PAD
       ;;
(ADO02PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/ADO02PAA.HDGA00AP
       m_FileAssign -d NEW,CATLG,DELETE -r 777 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA00BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AAK      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA00BP.*,+                                            
# RTGA00_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01AP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AAP      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA00BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01BP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01AP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_777 3 CH 777
 /FIELDS FLD_CH_1_780 1 CH 780
 /KEYS
   FLD_CH_1_780 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_777
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PAG
       ;;
(ADO02PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01AP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PAE
       ;;
(ADO02PAE)
       m_CondExec 00,EQ,ADO02PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PAJ PROC=JVZIP     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PAJ
       ;;
(ADO02PAJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAAP},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A2} DD1 ${DATA}/PXX0/F07.HDGA00BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01BP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PAJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01AP(+1),DISP=SHR                    ~         
#
       JUMP_LABEL=ADO02PAM
       ;;
(ADO02PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PAM.FADO01BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PAQ
       ;;
(ADO02PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PAM.FADO01BP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGA10                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PAT PGM=PTLDRIVM   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PAT
       ;;
(ADO02PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PAT.HDGA10AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PAT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PAX
       ;;
(ADO02PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/ADO02PAT.HDGA10AP
       m_FileAssign -d NEW,CATLG,DELETE -r 450 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA10BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABO      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA10BP.*,+                                            
# RTGA10_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01CP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABT      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA10BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01DP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01CP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_450 3 CH 450
 /FIELDS FLD_CH_1_453 1 CH 453
 /KEYS
   FLD_CH_1_453 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_450
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PBA
       ;;
(ADO02PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PBA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01CP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PAY
       ;;
(ADO02PAY)
       m_CondExec 00,EQ,ADO02PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PBD PROC=JVZIP     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PBD
       ;;
(ADO02PBD)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXABT},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A6} DD1 ${DATA}/PXX0/F07.HDGA10BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01DP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PBD.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01CP(+1),DISP=SHR                    ~         
#
       JUMP_LABEL=ADO02PBG
       ;;
(ADO02PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PBG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PBG.FADO01DP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PBJ PGM=FTP        ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PBJ
       ;;
(ADO02PBJ)
       m_CondExec ${EXACD},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PBJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PBG.FADO01DP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGV31                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PBM PGM=PTLDRIVM   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PBM
       ;;
(ADO02PBM)
       m_CondExec ${EXACI},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PBM.HDGV31AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PBM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PBQ
       ;;
(ADO02PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/ADO02PBM.HDGV31AP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGV31BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ACS      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGV31BP.*,+                                            
# RTGV31_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01EP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ACX      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGV31BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01FP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01EP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PBT PGM=EZACFSM1   ** ID=ACS                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_80 3 CH 80
 /FIELDS FLD_CH_1_82 1 CH 82
 /KEYS
   FLD_CH_1_82 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_80
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PBT
       ;;
(ADO02PBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PBT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01EP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PBR
       ;;
(ADO02PBR)
       m_CondExec 00,EQ,ADO02PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PBX PROC=JVZIP     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PBX
       ;;
(ADO02PBX)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXACX},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A10} DD1 ${DATA}/PXX0/F07.HDGV31BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01FP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PBX.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01EP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PCA
       ;;
(ADO02PCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PCA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PCA.FADO01FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PCD PGM=FTP        ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PCD
       ;;
(ADO02PCD)
       m_CondExec ${EXADH},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PCD.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PCA.FADO01FP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGA58                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PCG PGM=PTLDRIVM   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PCG
       ;;
(ADO02PCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PCG.HDGA58AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PCG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PCJ
       ;;
(ADO02PCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/ADO02PCG.HDGA58AP
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA58BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ADW      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA58BP.*,+                                            
# RTGA58_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01GP,MODE=O                                          
# ***************************************************************              
# *                                                                            
# ***************************************************************              
# AEB      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA58BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01HP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01GP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PCM PGM=EZACFSM1   ** ID=ADW                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_52 3 CH 52
 /FIELDS FLD_CH_1_54 1 CH 54
 /KEYS
   FLD_CH_1_54 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_52
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PCM
       ;;
(ADO02PCM)
       m_CondExec ${EXADW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PCM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01GP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PCK
       ;;
(ADO02PCK)
       m_CondExec 00,EQ,ADO02PCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PCQ PROC=JVZIP     ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PCQ
       ;;
(ADO02PCQ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAEB},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A14} DD1 ${DATA}/PXX0/F07.HDGA58BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01HP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PCQ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01GP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PCT
       ;;
(ADO02PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PCT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PCT.FADO01HP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01HP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PCX PGM=FTP        ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PCX
       ;;
(ADO02PCX)
       m_CondExec ${EXAEL},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PCX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PCT.FADO01HP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGA22                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PDA PGM=PTLDRIVM   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PDA
       ;;
(ADO02PDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PDA.HDGA22AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PDA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PDD
       ;;
(ADO02PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/ADO02PDA.HDGA22AP
       m_FileAssign -d NEW,CATLG,DELETE -r 55 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA22BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AFA      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA22BP.*,+                                            
# RTGA22_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01IP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AFF      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA22BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01JP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01IP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PDG PGM=EZACFSM1   ** ID=AFA                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_57 1 CH 57
 /FIELDS FLD_CH_3_55 3 CH 55
 /KEYS
   FLD_CH_1_57 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_55
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PDG
       ;;
(ADO02PDG)
       m_CondExec ${EXAFA},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PDG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01IP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PDE
       ;;
(ADO02PDE)
       m_CondExec 00,EQ,ADO02PDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PDJ PROC=JVZIP     ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PDJ
       ;;
(ADO02PDJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAFF},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A18} DD1 ${DATA}/PXX0/F07.HDGA22BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01JP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PDJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01IP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PDM
       ;;
(ADO02PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PDM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PDM.FADO01JP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01JP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PDQ PGM=FTP        ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PDQ
       ;;
(ADO02PDQ)
       m_CondExec ${EXAFP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PDQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PDM.FADO01JP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGA14                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PDT PGM=PTLDRIVM   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PDT
       ;;
(ADO02PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PDT.HDGA14AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PDT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PDX PGM=SORT       ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PDX
       ;;
(ADO02PDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/ADO02PDT.HDGA14AP
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA14BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AGE      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA14BP.*,+                                            
# RTGA14_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01KP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AGJ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA14BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01LP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01KP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PEA PGM=EZACFSM1   ** ID=AGE                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_99 1 CH 99
 /FIELDS FLD_CH_3_97 3 CH 97
 /KEYS
   FLD_CH_1_99 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_97
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PEA
       ;;
(ADO02PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PEA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01KP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PDY
       ;;
(ADO02PDY)
       m_CondExec 00,EQ,ADO02PDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PED PROC=JVZIP     ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PED
       ;;
(ADO02PED)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAGJ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A22} DD1 ${DATA}/PXX0/F07.HDGA14BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01LP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PED.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01KP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PEG
       ;;
(ADO02PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PEG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PEG.FADO01LP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01LP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PEJ PGM=FTP        ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PEJ
       ;;
(ADO02PEJ)
       m_CondExec ${EXAGT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PEJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PEG.FADO01LP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGG40                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# AGY      STEP  PGM=PTLDRIVM,PATTERN=FSL,PATKW=(SYS=RDAR,UT=ADO02P.U7         
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# //SYSPUNCH DD DUMMY                                                          
# SYSABEND REPORT SYSOUT=*                                                     
# SYSREC01 FILE  NAME=HDGG40AP,MODE=O,DISP=(NEW,CATLG,CATLG)                   
# SYSIN    DATA  *                                                             
#   FASTUNLOAD                                                                 
#   EXCP YES                                                                   
#   SQL-ACCESS EXTENSION                                                       
#   VALIDATE-HEADER NO                                                         
#   OUTPUT-FORMAT D                                                            
#   SELECT                                                                     
#   'RTGG40    '!!'              '!!NSOCIETE!!'  '                             
#   !!NLIEU!!' '                                                               
#   !!NSOCIETE!!NLIEU!!NCODIC!!DEFFET                                          
#   !!'                                                  '                     
#   !!'                             ' !!                                       
#   '"'!!NCODIC!!'";"'!!NCONC!!'";"'                                           
#   !! LEFT(CHAR(PEXPCOMMART), 6) !! RIGHT(CHAR(PEXPCOMMART), 2)               
#   !!'";"'!!NSOCIETE!!'";"' !!NLIEU!!'";"' !!DEFFET!!'";"'                    
#   !!DFINEFFET!!'";"' !! LEFT(CHAR(DSYST), 14)                                
#   !!'";"'!! WFINEFFET !!'";"'!! CORIG                                        
#   !!'";"'!! LEFT(CHAR(PCOMMADP), 6)                                          
#   !! RIGHT(CHAR(PCOMMADP), 2) !!'"'                                          
#   FROM PDARTY.RTGG40                                                         
#   ORDER BY NCODIC ;                                                          
#          DATAEND                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AHD      SORT                                                                
# SORTIN   FILE  NAME=HDGG40AP,MODE=I                                          
# SORTOUT  FILE  NAME=HDGG40ZP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,235,A),FORMAT=CH                                             
#       OUTREC FIELDS=(3,232)                                                  
#          DATAEND                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AHI      SORT                                                                
# SORTIN   FILE  NAME=HDGG40ZP,MODE=I                                          
# SORTOUT  FILE  NAME=HDGG40RP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,100,CH,A)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# AHN      STEP  PGM=BTF900,LANG=CBL                                           
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# *                                                                            
# ******** FICHIER EN ENTRE DU JOUR                                            
# FTF600   FILE  NAME=HDGG40RP,MODE=I                                          
# ******** DUMMY POUR 1ER PASSAGE HDF601AP                                     
# //FTF601   DD DUMMY                                                          
# *******  JJMMSSAA                                                            
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# ******** PARAMETRE SOCIETE                                                   
# FNSOC    DATA  CLASS=FIX1,MBR=SOCDIF                                         
# ******** FICHIER DES MISES A JOUR                                            
# FTF602   FILE  NAME=HDF900KP,MODE=O                                          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PEM
       ;;
(ADO02PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BTF840BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BTF840BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BTF840BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BTF840BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BTF840BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BTF840BY
       m_FileAssign -d NEW,CATLG,DELETE -r 97 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGG40BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AHD      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGG40BP.*,+                                            
# RTGG40_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01OP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AHI      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGG40BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01PP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01OP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PEQ PGM=EZACFSM1   ** ID=AHD                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_97 1 CH 97
 /KEYS
   FLD_CH_1_97 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=ADO02PEQ
       ;;
(ADO02PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PEQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01OP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PEN
       ;;
(ADO02PEN)
       m_CondExec 00,EQ,ADO02PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PET PROC=JVZIP     ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PET
       ;;
(ADO02PET)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAHI},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A25} DD1 ${DATA}/PXX0/F07.HDGG40BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01PP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PET.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01OP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PEX
       ;;
(ADO02PEX)
       m_CondExec ${EXAHN},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PEX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PEX.FADO01PP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01PP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PFA PGM=FTP        ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PFA
       ;;
(ADO02PFA)
       m_CondExec ${EXAHS},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PFA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PEX.FADO01PP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PFD PGM=SORT       ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PFD
       ;;
(ADO02PFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.HDGG40RP
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDF601AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=ADO02PFE
       ;;
(ADO02PFE)
       m_CondExec 00,EQ,ADO02PFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTGA40                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PFG PGM=PTLDRIVM   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PFG
       ;;
(ADO02PFG)
       m_CondExec ${EXAIC},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PFG.HDGA40AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PFG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PFJ PGM=SORT       ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PFJ
       ;;
(ADO02PFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A28} SORTIN ${DATA}/PTEM/ADO02PFG.HDGA40AP
       m_FileAssign -d NEW,CATLG,DELETE -r 73 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA40BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AIM      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA40BP.*,+                                            
# RTGA40_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01QP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AIR      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA40BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01RP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01QP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PFM PGM=EZACFSM1   ** ID=AIM                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_75 1 CH 75
 /FIELDS FLD_CH_3_73 3 CH 73
 /KEYS
   FLD_CH_1_75 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_73
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PFM
       ;;
(ADO02PFM)
       m_CondExec ${EXAIM},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PFM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01QP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PFK
       ;;
(ADO02PFK)
       m_CondExec 00,EQ,ADO02PFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PFQ PROC=JVZIP     ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PFQ
       ;;
(ADO02PFQ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAIR},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A29} DD1 ${DATA}/PXX0/F07.HDGA40BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01RP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PFQ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01QP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PFT
       ;;
(ADO02PFT)
       m_CondExec ${EXAIW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PFT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PFT.FADO01RP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01RP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PFX PGM=FTP        ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PFX
       ;;
(ADO02PFX)
       m_CondExec ${EXAJB},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PFX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PFT.FADO01RP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGA52                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PGA PGM=PTLDRIVM   ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PGA
       ;;
(ADO02PGA)
       m_CondExec ${EXAJG},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PGA.HDGA52AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PGA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PGD PGM=SORT       ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PGD
       ;;
(ADO02PGD)
       m_CondExec ${EXAJL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A32} SORTIN ${DATA}/PTEM/ADO02PGA.HDGA52AP
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA52BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AJQ      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA52BP.*,+                                            
# RTGA52_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01SP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AJV      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA52BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01TP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01SP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PGG PGM=EZACFSM1   ** ID=AJQ                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_23 3 CH 23
 /FIELDS FLD_CH_1_25 1 CH 25
 /KEYS
   FLD_CH_1_25 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_23
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PGG
       ;;
(ADO02PGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PGG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01SP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PGE
       ;;
(ADO02PGE)
       m_CondExec 00,EQ,ADO02PGD ${EXAJL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PGJ PROC=JVZIP     ** ID=AJV                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PGJ
       ;;
(ADO02PGJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAJV},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A33} DD1 ${DATA}/PXX0/F07.HDGA52BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01TP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PGJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01SP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PGM
       ;;
(ADO02PGM)
       m_CondExec ${EXAKA},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PGM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PGM.FADO01TP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01TP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PGQ PGM=FTP        ** ID=AKF                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PGQ
       ;;
(ADO02PGQ)
       m_CondExec ${EXAKF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PGQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PGM.FADO01TP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGA67                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PGT PGM=PTLDRIVM   ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PGT
       ;;
(ADO02PGT)
       m_CondExec ${EXAKK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PGT.HDGA67AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PGT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PGX PGM=SORT       ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PGX
       ;;
(ADO02PGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A36} SORTIN ${DATA}/PTEM/ADO02PGT.HDGA67AP
       m_FileAssign -d NEW,CATLG,DELETE -r 79 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA67BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AKU      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA67BP.*,+                                            
# RTGA67_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01UP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AKZ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA67BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01VP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01UP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PHA PGM=EZACFSM1   ** ID=AKU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_81 1 CH 81
 /FIELDS FLD_CH_3_79 3 CH 79
 /KEYS
   FLD_CH_1_81 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_79
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PHA
       ;;
(ADO02PHA)
       m_CondExec ${EXAKU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PHA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01UP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PGY
       ;;
(ADO02PGY)
       m_CondExec 00,EQ,ADO02PGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PHD PROC=JVZIP     ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PHD
       ;;
(ADO02PHD)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAKZ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A37} DD1 ${DATA}/PXX0/F07.HDGA67BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01VP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PHD.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01UP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PHG
       ;;
(ADO02PHG)
       m_CondExec ${EXALE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PHG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PHG.FADO01VP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01VP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PHJ PGM=FTP        ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PHJ
       ;;
(ADO02PHJ)
       m_CondExec ${EXALJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PHJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PHG.FADO01VP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTGA01                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PHM PGM=PTLDRIVM   ** ID=ALO                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PHM
       ;;
(ADO02PHM)
       m_CondExec ${EXALO},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PHM.HDGA01AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PHM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PHQ PGM=SORT       ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PHQ
       ;;
(ADO02PHQ)
       m_CondExec ${EXALT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A40} SORTIN ${DATA}/PTEM/ADO02PHM.HDGA01AP
       m_FileAssign -d NEW,CATLG,DELETE -r 108 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA01BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ALY      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA01BP.*,+                                            
# RTGA01_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01WP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AMD      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA01BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01XP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01WP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PHT PGM=EZACFSM1   ** ID=ALY                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_108 3 CH 108
 /FIELDS FLD_CH_1_110 1 CH 110
 /KEYS
   FLD_CH_1_110 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_108
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PHT
       ;;
(ADO02PHT)
       m_CondExec ${EXALY},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PHT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01WP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PHR
       ;;
(ADO02PHR)
       m_CondExec 00,EQ,ADO02PHQ ${EXALT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PHX PROC=JVZIP     ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PHX
       ;;
(ADO02PHX)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAMD},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A41} DD1 ${DATA}/PXX0/F07.HDGA01BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01XP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PHX.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01WP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PIA
       ;;
(ADO02PIA)
       m_CondExec ${EXAMI},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PIA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PIA.FADO01XP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01XP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PID PGM=FTP        ** ID=AMN                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PID
       ;;
(ADO02PID)
       m_CondExec ${EXAMN},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PID.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PIA.FADO01XP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTNV15                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PIG PGM=PTLDRIVM   ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PIG
       ;;
(ADO02PIG)
       m_CondExec ${EXAMS},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PIG.HDNV15AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PIG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PIJ PGM=SORT       ** ID=AMX                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PIJ
       ;;
(ADO02PIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A44} SORTIN ${DATA}/PTEM/ADO02PIG.HDNV15AP
       m_FileAssign -d NEW,CATLG,DELETE -r 78 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDNV15BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ANC      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDNV15BP.*,+                                            
# RTNV15_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO01YP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ANH      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDNV15BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO01ZP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO01YP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PIM PGM=EZACFSM1   ** ID=ANC                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_80 1 CH 80
 /FIELDS FLD_CH_3_78 3 CH 78
 /KEYS
   FLD_CH_1_80 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_78
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PIM
       ;;
(ADO02PIM)
       m_CondExec ${EXANC},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PIM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO01YP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PIK
       ;;
(ADO02PIK)
       m_CondExec 00,EQ,ADO02PIJ ${EXAMX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PIQ PROC=JVZIP     ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PIQ
       ;;
(ADO02PIQ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXANH},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A45} DD1 ${DATA}/PXX0/F07.HDNV15BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO01ZP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PIQ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01YP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PIT
       ;;
(ADO02PIT)
       m_CondExec ${EXANM},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PIT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PIT.FADO01ZP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO01ZP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PIX PGM=FTP        ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PIX
       ;;
(ADO02PIX)
       m_CondExec ${EXANR},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PIX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PIT.FADO01ZP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  FICHIER PROVENANT DE GNMD1*                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PJA PGM=SORT       ** ID=ANW                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PJA
       ;;
(ADO02PJA)
       m_CondExec ${EXANW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BTF867BP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BTF867BY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BTF867BM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BTF867BD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BTF867BL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BTF867BO
       m_FileAssign -d NEW,CATLG,DELETE -r 81 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGA75BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AOB      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDGA75BP.*,+                                            
# RTGA75_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HAD001AP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AOG      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDGA75BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HAD001BP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HAD001AP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PJD PGM=EZACFSM1   ** ID=AOB                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_81 1 CH 81
 /KEYS
   FLD_CH_1_81 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=ADO02PJD
       ;;
(ADO02PJD)
       m_CondExec ${EXAOB},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PJD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HAD001AP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PJB
       ;;
(ADO02PJB)
       m_CondExec 00,EQ,ADO02PJA ${EXANW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PJG PROC=JVZIP     ** ID=AOG                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PJG
       ;;
(ADO02PJG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAOG},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A48} DD1 ${DATA}/PXX0/F07.HDGA75BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HAD001BP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PJG.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HAD001AP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PJJ
       ;;
(ADO02PJJ)
       m_CondExec ${EXAOL},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PJJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PJJ.FAD001BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FAD001BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PJM PGM=FTP        ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PJM
       ;;
(ADO02PJM)
       m_CondExec ${EXAOQ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PJM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PJJ.FAD001BP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  EXTRACTION RTPC01                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PJQ PGM=PTLDRIVM   ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PJQ
       ;;
(ADO02PJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PJQ.HDPC01AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PJQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PJT
       ;;
(ADO02PJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A51} SORTIN ${DATA}/PTEM/ADO02PJQ.HDPC01AP
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/ADO02PJT.HDPC01HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_463 3 CH 463
 /KEYS
   FLD_CH_3_463 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_463
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PJU
       ;;
(ADO02PJU)
       m_CondExec 00,EQ,ADO02PJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPC900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PJX PGM=BPC900     ** ID=APF                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PJX
       ;;
(ADO02PJX)
       m_CondExec ${EXAPF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A52} FPC900 ${DATA}/PTEM/ADO02PJT.HDPC01HP
#  FICHIER DE LA VEILLE                                                        
       m_FileAssign -d SHR -g +0 FPC901 ${DATA}/PXX0/F07.BPC900AP
#  FICHIER DES MAJ                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 FPC902 ${DATA}/PTEM/ADO02PJX.BPC900BP
       m_ProgramExec BPC900 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PKA PGM=SORT       ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PKA
       ;;
(ADO02PKA)
       m_CondExec ${EXAPK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A53} SORTIN ${DATA}/PTEM/ADO02PJX.BPC900BP
       m_FileAssign -d NEW,CATLG,DELETE -r 328 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDPC01BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# APP      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDPC01BP.*,+                                            
# RTPC01_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HAD001CP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# APU      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDPC01BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HAD001DP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HAD001CP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PKD PGM=EZACFSM1   ** ID=APP                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_633 1 CH 633
 /FIELDS FLD_CH_134_461 134 CH 461
 /KEYS
   FLD_CH_1_633 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_134_461
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PKD
       ;;
(ADO02PKD)
       m_CondExec ${EXAPP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PKD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A54} SYSOUT ${DATA}/PXX0/F07.HADO01CP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PKB
       ;;
(ADO02PKB)
       m_CondExec 00,EQ,ADO02PKA ${EXAPK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PKG PROC=JVZIP     ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PKG
       ;;
(ADO02PKG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAPU},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A55} DD1 ${DATA}/PXX0/F07.HDPC01BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HAD001DP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PKG.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO01CP(+2),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PKJ
       ;;
(ADO02PKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PKJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PKJ.FAD001DP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FAD001DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PKM PGM=FTP        ** ID=AQE                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PKM
       ;;
(ADO02PKM)
       m_CondExec ${EXAQE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PKM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PKJ.FAD001DP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  RECONSTRUTION DU FICHIER POUR LE LENDEMAIN                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PKQ PGM=SORT       ** ID=AQJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PKQ
       ;;
(ADO02PKQ)
       m_CondExec ${EXAQJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A58} SORTIN ${DATA}/PTEM/ADO02PJT.HDPC01HP
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BPC900AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=ADO02PKR
       ;;
(ADO02PKR)
       m_CondExec 00,EQ,ADO02PKQ ${EXAQJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTPC02                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PKT PGM=PTLDRIVM   ** ID=AQO                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PKT
       ;;
(ADO02PKT)
       m_CondExec ${EXAQO},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PKT.HDPC02AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PKT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PKX PGM=SORT       ** ID=AQT                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PKX
       ;;
(ADO02PKX)
       m_CondExec ${EXAQT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A59} SORTIN ${DATA}/PTEM/ADO02PKT.HDPC02AP
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/ADO02PKX.HDPC02HP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_463 3 CH 463
 /KEYS
   FLD_CH_3_463 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_463
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PKY
       ;;
(ADO02PKY)
       m_CondExec 00,EQ,ADO02PKX ${EXAQT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPC900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PLA PGM=BPC900     ** ID=AQY                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PLA
       ;;
(ADO02PLA)
       m_CondExec ${EXAQY},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A60} FPC900 ${DATA}/PTEM/ADO02PKX.HDPC02HP
#  FICHIER DE LA VEILLE                                                        
       m_FileAssign -d OLD -g +0 FPC901 ${DATA}/PXX0/F07.BPC900DP
#  FICHIER DES MAJ                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 FPC902 ${DATA}/PTEM/ADO02PLA.BPC900EP
       m_ProgramExec BPC900 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PLD PGM=SORT       ** ID=ARD                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PLD
       ;;
(ADO02PLD)
       m_CondExec ${EXARD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A61} SORTIN ${DATA}/PTEM/ADO02PLA.BPC900EP
       m_FileAssign -d NEW,CATLG,DELETE -r 314 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDPC02BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ARI      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.HDPC02BP.*,+                                            
# RTPC02_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HAD001EP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ARN      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=HDPC02BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HAD001FP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HAD001EP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO02PLG PGM=EZACFSM1   ** ID=ARI                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_447 1 CH 447
 /FIELDS FLD_CH_134_447 134 CH 447
 /KEYS
   FLD_CH_1_447 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_134_447
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PLG
       ;;
(ADO02PLG)
       m_CondExec ${EXARI},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PLG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HAD001EP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PLE
       ;;
(ADO02PLE)
       m_CondExec 00,EQ,ADO02PLD ${EXARD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PLJ PROC=JVZIP     ** ID=ARN                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PLJ
       ;;
(ADO02PLJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXARN},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A62} DD1 ${DATA}/PXX0/F07.HDPC02BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HAD001FP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PLJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HAD001EP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO02PLM
       ;;
(ADO02PLM)
       m_CondExec ${EXARS},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PLM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PLM.FAD001FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FAD001FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PLQ PGM=FTP        ** ID=ARX                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PLQ
       ;;
(ADO02PLQ)
       m_CondExec ${EXARX},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PLQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PLM.FAD001FP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  REPRISE : OUI       ********                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PLT PGM=SORT       ** ID=ASC                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PLT
       ;;
(ADO02PLT)
       m_CondExec ${EXASC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A65} SORTIN ${DATA}/PTEM/ADO02PKX.HDPC02HP
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BPC900DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=ADO02PLU
       ;;
(ADO02PLU)
       m_CondExec 00,EQ,ADO02PLT ${EXASC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTAC01                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# ASH      STEP  PGM=PTLDRIVM,PATTERN=FSL,PATKW=(SYS=RMAC,UT=ADO02P.U1         
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# //SYSPUNCH DD DUMMY                                                          
# SYSABEND REPORT SYSOUT=*                                                     
# SYSREC01 FILE  NAME=HDAC01AP,MODE=O,DISP=(NEW,CATLG,CATLG)                   
# SYSIN    DATA  *                                                             
#   FASTUNLOAD                                                                 
#   EXCP YES                                                                   
#   SQL-ACCESS EXTENSION                                                       
#   VALIDATE-HEADER NO                                                         
#   OUTPUT-FORMAT D                                                            
#   SELECT                                                                     
#   '"'!!DVENTE!!'";"'!!NSOCIETE!!'";"'!!   NLIEU                              
#   !!'";"'!! LEFT(CHAR(QTLM), 5) !!'";"'!!  LEFT(CHAR(QELA), 5)               
#   !!'";"'!! LEFT(CHAR(QDACEM), 5)                                            
#   !!'";"'!! LEFT(CHAR(CATLM), 8) !!'";"'!!  LEFT(CHAR(CAELA), 8)             
#   !!'";"'!! LEFT(CHAR(CADACEM), 8)                                           
#   !!'";"'!! LEFT(CHAR(QTRDARTY), 5) !!'";"'!! LEFT(CHAR(QTRDACEM), 5         
#   !!'";"'!! LEFT(CHAR(QENTRE), 6)                                            
#   !!'";"'!! LEFT(CHAR(QVENDEUR), 3) !!  RIGHT(CHAR(QVENDEUR), 1)             
#   !!'";"'!! COUVERTN1 !!'";"'!!   COUVERTJ1                                  
#   !!'";"'!! LEFT(CHAR(DSYST), 14) !!'"'                                      
#   FROM M907.RTAC01;                                                          
#          DATAEND                                                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# ASM      SORT                                                                
# SORTIN   FILE  NAME=HDAC01AP,MODE=I                                          
# SORTOUT  FILE  NAME=HDAC01BP,MODE=O                                          
# SYSIN    DATA  *                                                             
#  SORT FIELDS=(1,138,A),FORMAT=CH                                             
#   OUTREC FIELDS=(3,136)                                                      
#          DATAEND                                                             
# ***************************************************************              
# ASR      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# //DD:DD1 -n                                                                  
# RTAC01_&YR4.&LMON.&LDAY..TXT                                                 
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HAD001GP,MODE=O                                          
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# ASW      STEP  PGM=*,PATTERN=JVZIP                                           
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=HDAC01BP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=HAD001HP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
# -x ANSI850                                                                   
# //DD:FICZIP                                                                  
#          DATAEND                                                             
#          FILE  NAME=HAD001GP,MODE=I                                          
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FADO01HP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# ATB      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# PUT 'PXX0.F07.HAD001HP(0)' RTAC01_&YR4.&LMON.&LDAY..ZIP                      
# QUIT                                                                         
#          DATAEND                                                             
# SYSOUT   FILE  NAME=FAD001HP,MODE=O                                          
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FAD001HP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# ATG      STEP  PGM=FTP,PARM='(EXIT'                                          
# *                                                                            
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    DATA  *                                                             
# 10.135.2.114                                                                 
# HADOOP                                                                       
# HADOOP                                                                       
# BIN                                                                          
# LOCSITE SBD=TCPIP1.XLATE2.TCPXLBIN                                           
#          DATAEND                                                             
#          FILE  NAME=FAD001HP,MODE=I                                          
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO02PLX PGM=IDCAMS     ** ID=ASH                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PLX
       ;;
(ADO02PLX)
       m_CondExec ${EXASH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PLX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO02PLY
       ;;
(ADO02PLY)
       m_CondExec 16,NE,ADO02PLX ${EXASH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTGN59                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PMA PGM=PTLDRIVM   ** ID=ASM                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PMA
       ;;
(ADO02PMA)
       m_CondExec ${EXASM},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO02PMA.HDGN59AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PMA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PMD PGM=SORT       ** ID=ASR                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PMD
       ;;
(ADO02PMD)
       m_CondExec ${EXASR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A66} SORTIN ${DATA}/PTEM/ADO02PMA.HDGN59AP
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/ADO02PMD.HDGN59BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_223 3 CH 223
 /FIELDS FLD_CH_1_223 1 CH 223
 /KEYS
   FLD_CH_1_223 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_223
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PME
       ;;
(ADO02PME)
       m_CondExec 00,EQ,ADO02PMD ${EXASR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BPC900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PMG PGM=BPC900     ** ID=ASW                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PMG
       ;;
(ADO02PMG)
       m_CondExec ${EXASW},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A67} FPC900 ${DATA}/PTEM/ADO02PMD.HDGN59BP
#  FICHIER DE LA VEILLE                                                        
       m_FileAssign -d SHR -g +0 FPC901 ${DATA}/PXX0/F07.BPC900FP
#  FICHIER DES MAJ                                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 FPC902 ${DATA}/PTEM/ADO02PMG.BPC900CP
       m_ProgramExec BPC900 
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PMJ PGM=SORT       ** ID=ATB                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PMJ
       ;;
(ADO02PMJ)
       m_CondExec ${EXATB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A68} SORTIN ${DATA}/PTEM/ADO02PMG.BPC900CP
       m_FileAssign -d NEW,CATLG,DELETE -r 328 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HDGN59CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_134_461 134 CH 461
 /FIELDS FLD_CH_1_633 1 CH 633
 /KEYS
   FLD_CH_1_633 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_134_461
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO02PMK
       ;;
(ADO02PMK)
       m_CondExec 00,EQ,ADO02PMJ ${EXATB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FADO01CP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PMM PGM=EZACFSM1   ** ID=ATG                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PMM
       ;;
(ADO02PMM)
       m_CondExec ${EXATG},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PMM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO02PMM.FADO01AP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FAD001AP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PMQ PGM=FTP        ** ID=ATL                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PMQ
       ;;
(ADO02PMQ)
       m_CondExec ${EXATL},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PMQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO02PMM.FADO01AP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#  RECONSTRUTION DU FICHIER POUR LE LENDEMAIN                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO02PMT PGM=SORT       ** ID=ATQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PMT
       ;;
(ADO02PMT)
       m_CondExec ${EXATQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A70} SORTIN ${DATA}/PTEM/ADO02PMD.HDGN59BP
       m_FileAssign -d NEW,CATLG,DELETE -r 633 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BPC900FP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=ADO02PMU
       ;;
(ADO02PMU)
       m_CondExec 00,EQ,ADO02PMT ${EXATQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=ADO02PZA
       ;;
(ADO02PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO02PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
