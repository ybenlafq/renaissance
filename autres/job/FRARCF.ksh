#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FRARCF.ksh                       --- VERSION DU 08/10/2016 22:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFFRARC -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/18 AT 15.04.03 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FRARCF                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EXTRACTION MOUVEMENT DU MOIS PRéCéDENT DPM                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FRARCFA
       ;;
(FRARCFA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       PERIO=${PERIO}
       RUN=${RUN}
       JUMP_LABEL=FRARCFAA
       ;;
(FRARCFAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ---------------------               *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FFTI50CD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ SORTOUT ${DATA}/PXX0.F91.FFTI50CD.ARCH$[PERIO]
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
INCLUDE COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAB
       ;;
(FRARCFAB)
       m_CondExec 00,EQ,FRARCFAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS EN COURS DPM                         *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFAD
       ;;
(FRARCFAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FFTI50CD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FFTI50CD
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
OMIT COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAE
       ;;
(FRARCFAE)
       m_CondExec 00,EQ,FRARCFAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS PRéCéDENT DNPC                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFAG
       ;;
(FRARCFAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.FFTI50CL
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ SORTOUT ${DATA}/PXX0.F61.FFTI50CL.ARCH$[PERIO]
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
INCLUDE COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAH
       ;;
(FRARCFAH)
       m_CondExec 00,EQ,FRARCFAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS EN COURS DNPC                        *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFAJ
       ;;
(FRARCFAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.FFTI50CL
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.FFTI50CL
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
OMIT COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAK
       ;;
(FRARCFAK)
       m_CondExec 00,EQ,FRARCFAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS PRéCéDENT DAL                        *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFAM
       ;;
(FRARCFAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.FFTI50CM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ SORTOUT ${DATA}/PXX0.F89.FFTI50CM.ARCH$[PERIO]
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
INCLUDE COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAN
       ;;
(FRARCFAN)
       m_CondExec 00,EQ,FRARCFAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS EN COURS DAL                         *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFAQ
       ;;
(FRARCFAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.FFTI50CM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FFTI50CM
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
OMIT COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAR
       ;;
(FRARCFAR)
       m_CondExec 00,EQ,FRARCFAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS PRéCéDENT DO                         *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFAT
       ;;
(FRARCFAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.FFTI50CO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ SORTOUT ${DATA}/PXX0.F16.FFTI50CO.ARCH$[PERIO]
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
INCLUDE COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAU
       ;;
(FRARCFAU)
       m_CondExec 00,EQ,FRARCFAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS EN COURS DO                          *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFAX
       ;;
(FRARCFAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.FFTI50CO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FFTI50CO
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
OMIT COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFAY
       ;;
(FRARCFAY)
       m_CondExec 00,EQ,FRARCFAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS PRéCéDENT DIF                        *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFBA
       ;;
(FRARCFBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FFTI50CP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ SORTOUT ${DATA}/PXX0.F07.FFTI50CP.ARCH$[PERIO]
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
INCLUDE COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFBB
       ;;
(FRARCFBB)
       m_CondExec 00,EQ,FRARCFBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS EN COURS DIF                         *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFBD
       ;;
(FRARCFBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FFTI50CP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FFTI50CP
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
OMIT COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFBE
       ;;
(FRARCFBE)
       m_CondExec 00,EQ,FRARCFBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS PRéCéDENT DRA                        *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFBG
       ;;
(FRARCFBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FFTI50CY
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ SORTOUT ${DATA}/PXX0.F45.FFTI50CY.ARCH$[PERIO]
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
INCLUDE COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFBH
       ;;
(FRARCFBH)
       m_CondExec 00,EQ,FRARCFBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION MOUVEMENT DU MOIS EN COURS DRA                         *         
#  REPRISE : OUI                                                     *         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FRARCFBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FRARCFBJ
       ;;
(FRARCFBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.FFTI50CY
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.FFTI50CY
       m_FileAssign -i SYSIN
SORT FIELDS=(01,18,BI,A)
OMIT COND=(144,6,CH,EQ,C'20$PERIO')
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=FRARCFBK
       ;;
(FRARCFBK)
       m_CondExec 00,EQ,FRARCFBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
