#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM01P.ksh                       --- VERSION DU 14/10/2016 10:01
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPIVM01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/09/16 AT 16.39.11 BY PREPA2                       
#    STANDARDS: P  JOBSET: IVM01P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BIV120                                                                
# ********************************************************************         
#  PREPARATION DU FICHIER SERVANT A LOADER LA TABLE RTIN00                     
#  IMAGE DES STOCKS MAGASINS AVANT INVENTAIRE                                  
#  NOTE   : ON EXCLU LE STOCK DES MAGASINS PRESENTS DANS LA SOUS TABLE         
#              EXMAG DE LA RTGA01 ET CE POUR TOUTES LES SOCIETES               
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM01PA
       ;;
(IVM01PA)
#
#IVM01PAJ
#IVM01PAJ Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#IVM01PAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2008/09/16 AT 16.39.11 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: IVM01P                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'INVT STOCK MAG'                        
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM01PAA
       ;;
(IVM01PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  SOCIETEE A TRAITER                                                          
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *****   TABLE GENERALISEE : SS TABLE EXMAG                                   
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ************** TABLE STOCK S/LIEU MAGASIN ***                                
#    RSGS30   : NAME=RSGS30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS30 /dev/null
# ************** TABLE GENERALITE ARTICLE *****                                
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# ************** TABLE DES PRMP ***************                                
#    RTGG50   : NAME=RSGG50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG50 /dev/null
# ************** TABLE DES PRMP DACEM *********                                
#    RTGG55   : NAME=RSGG55,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGG55 /dev/null
# ************** TABLE DES FAMILLES ***********                                
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
# ************** FICHIER D'EXTRACTION *********                                
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 FIN120 ${DATA}/PTEM/IVM01PAA.BIV120AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV120 
       JUMP_LABEL=IVM01PAB
       ;;
(IVM01PAB)
       m_CondExec 04,GE,IVM01PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC SERVANT A LOADER LA TABLE RTIN00 SUR L'INDEX CLUSTER             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM01PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM01PAD
       ;;
(IVM01PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IVM01PAA.BIV120AP
       m_FileAssign -d NEW,CATLG,DELETE -r 64 -g +1 SORTOUT ${DATA}/PGI0/F07.RELOAD.IN00RP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_14 1 CH 14
 /FIELDS FLD_CH_36_1 36 CH 1
 /FIELDS FLD_CH_16_7 16 CH 7
 /FIELDS FLD_CH_64_1 64 CH 1
 /FIELDS FLD_CH_37_3 37 CH 3
 /KEYS
   FLD_CH_1_14 ASCENDING,
   FLD_CH_16_7 ASCENDING,
   FLD_CH_37_3 ASCENDING,
   FLD_CH_36_1 ASCENDING,
   FLD_CH_64_1 ASCENDING
 /* Record Type = F  Record Length = 64 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM01PAE
       ;;
(IVM01PAE)
       m_CondExec 00,EQ,IVM01PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   D S N U T I L B                                                            
# ********************************************************************         
#   LOAD DE LA TABLE RTIN00                                                    
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM01PAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM01PAG
       ;;
(IVM01PAG)
       m_CondExec ${EXAAK},NE,YES 
#  FICHIERS DE TRAVAIL                                                         
#  SORTOUT AU MOINS = TAILLE DU + GROS INDEX X NBR INDEXS                      
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
# ************** TABLE INVENTAIRE ************                                 
#    RSIN00   : NAME=RSIN00,MODE=U - DYNAM=YES                                 
# -X-RSIN00   - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSIN00 /dev/null
# ************** FICHIER DE LOAD *************                                 
       m_FileAssign -d SHR -g ${G_A2} SYSREC ${DATA}/PGI0/F07.RELOAD.IN00RP
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM01PAG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/IVM01P_IVM01PAG_RTIN00.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=IVM01PAH
       ;;
(IVM01PAH)
       m_CondExec 04,GE,IVM01PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  D S N U T I L B                                                             
# ********************************************************************         
#   REPAIR NOCOPY TABLESPACE RSIN00   DE LA D BASE PPDGI00                     
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM01PAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM01PZA
       ;;
(IVM01PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM01PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
