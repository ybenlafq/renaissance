#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  STA0EY.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYSTA0E -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/06 AT 08.20.58 BY BURTEC6                      
#    STANDARDS: P  JOBSET: STA0EY                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTVA06-RTGG51                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=STA0EYA
       ;;
(STA0EYA)
#
#STA0EYAA
#STA0EYAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#STA0EYAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=STA0EYAD
       ;;
(STA0EYAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F45.BHV030GE
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STA0EYAD.BVA400GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_70_8 70 CH 8
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_70_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STA0EYAE
       ;;
(STA0EYAE)
       m_CondExec 00,EQ,STA0EYAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA400                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYAG
       ;;
(STA0EYAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA01   : NAME=RSVA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA01 /dev/null
#    RTGG70   : NAME=RSGG70Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40 /dev/null
# *****   FICHIER FNSOC XXX                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PTEM/STA0EYAD.BVA400GE
# *****   FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -t LSEQ -g +1 FVA00 ${DATA}/PTEM/STA0EYAG.FVA000GE
# *****   FICHIER DES ANOMALIES ENTRANT DANS LE GENERATEUR D'ETAT              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA500 ${DATA}/PXX0/F45.FVA500GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA400 
       JUMP_LABEL=STA0EYAH
       ;;
(STA0EYAH)
       m_CondExec 04,GE,STA0EYAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA000  POUR CREATION FICHIER ENTRANT DANS BGG500  *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYAJ
       ;;
(STA0EYAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/STA0EYAG.FVA000GE
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STA0EYAJ.FVA000YE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_8 "945"
 /DERIVEDFIELD CST_3_12 "991")
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_PD_36_6 36 PD 6
 /CONDITION CND_2 FLD_CH_8_3 EQ CST_1_8 OR FLD_CH_8_3 EQ CST_3_12 OR 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_48_8,FLD_CH_36_6,FLD_CH_42_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STA0EYAK
       ;;
(STA0EYAK)
       m_CondExec 00,EQ,STA0EYAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG600                                                                
# ********************************************************************         
#  RECALCUL DES PRMPS DU MOIS                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYAM
       ;;
(STA0EYAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FVA00 ${DATA}/PTEM/STA0EYAJ.FVA000YE
# ******  TABLES EN ENTREE                                                     
#    RTVA06   : NAME=RSVA06Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA06 /dev/null
# ******  TABLES EN UPDATE (INSERT)                                            
#    RTGG51   : NAME=RSGG51Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RTGG51 /dev/null
# *****   FICHIER FNSOC XXX                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FGG500 ${DATA}/PTEM/STA0EYAM.FGG500GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG600 
       JUMP_LABEL=STA0EYAN
       ;;
(STA0EYAN)
       m_CondExec 04,GE,STA0EYAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FGG500                                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYAQ
       ;;
(STA0EYAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/STA0EYAM.FGG500GE
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STA0EYAQ.FGG500YE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_12 19 CH 12
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_8 11 CH 8
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_4_7,FLD_CH_11_8,FLD_CH_19_12
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STA0EYAR
       ;;
(STA0EYAR)
       m_CondExec 00,EQ,STA0EYAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG605                                                                
# ********************************************************************         
#  MAJ DES PRMPS APRES RECALCUL                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYAT
       ;;
(STA0EYAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG70   : NAME=RSGG70Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGA10   : NAME=RSGA10Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTGG51   : NAME=RSGG51Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG51 /dev/null
# ******  FICHIER EN ENTREE ISSU DU TRI PRECEDENT                              
       m_FileAssign -d SHR -g ${G_A5} RTVA00I ${DATA}/PTEM/STA0EYAG.FVA000GE
# *****   FICHIER FNSOC XXX                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# ******  FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER DES PRA ENTRANT DANS LE GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGG505 ${DATA}/PXX0/F45.FGG505GE
# ******  FICHIER D'EXTRACTION ISSU DU BGG600                                  
       m_FileAssign -d SHR -g ${G_A6} FGG500 ${DATA}/PTEM/STA0EYAQ.FGG500YE
# ******  FICHIER ENTRANT DANS ,BVA505 ET SERVANT A LOADER RTVA01              
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -t LSEQ -g +1 RTVA00S ${DATA}/PXX0/F45.FVA01GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG605 
       JUMP_LABEL=STA0EYAU
       ;;
(STA0EYAU)
       m_CondExec 04,GE,STA0EYAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA00A POUR CREATION FICHIER BVA505A ENTRANT                 
#  DANS LE PROG BVA505                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYAX
       ;;
(STA0EYAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F45.FVA01GE
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STA0EYAX.BVA405GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "P"
 /FIELDS FLD_PD_76_8 76 PD 8
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_68_8 68 CH 8
 /FIELDS FLD_PD_68_8 68 PD 8
 /FIELDS FLD_CH_76_8 76 CH 8
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_42_6 42 CH 6
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_11 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6,
    TOTAL FLD_PD_68_8,
    TOTAL FLD_PD_76_8
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_11_3,FLD_CH_8_3,FLD_CH_36_6,FLD_CH_68_8,FLD_CH_42_6,FLD_CH_76_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STA0EYAY
       ;;
(STA0EYAY)
       m_CondExec 00,EQ,STA0EYAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA405                                                                
# ********************************************************************         
#  MAJ DE LA RTVA06 POUR METTRE A JOUR EN QUANTITE ET EN VALEUR LES            
#  STOCKS,ET CREATION D'UN FICHIER BVA406A POUR MAJ DE LA TABLE                
#  RTGG51 DANS LA PGM BVA406                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYBA
       ;;
(STA0EYBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTVA16   : NAME=RSVA16Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA16 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTVA06   : NAME=RSVA06Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA06 /dev/null
#    RTGG51   : NAME=RSGG51Y,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG51 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A8} FVA505 ${DATA}/PTEM/STA0EYAX.BVA405GE
# ******  FICHIER SERVANT A LOADER LE TABLE RTVA15                             
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 FVA515 ${DATA}/PXX0/F45.FVA16GE
# ******  FICHIER ENTRANT DANS VA001R ET SERVANT A LOADER RTVA31               
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 RTVA30 ${DATA}/PXX0/F45.FVA31GE
# ******  FICHIER SERVANT A LA MAJ DE LA RTGG51 DANS LE BVA406                 
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 FGG500 ${DATA}/PTEM/STA0EYBA.BVA506GE
# ******  FICHIER A HISTORISER QUI PERMET DE LOADER LA TABLE RTVA10            
# *****   FICHIER PARAMETRE 'M' POUR TRAIT. MENSUEL                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STA0EYBA
# *****   FICHIER FNSOC XXX                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDGE
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA405 
       JUMP_LABEL=STA0EYBB
       ;;
(STA0EYBB)
       m_CondExec 04,GE,STA0EYBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA406                                                                
# ********************************************************************         
#  MAJ DE LA RTGG51 POUR RECALCUL DES PRMS A PARTIR DU FICHIER                 
#  BVA406AE ISSU DU BVA405                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STA0EYBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYBD
       ;;
(STA0EYBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN MAJ                                                        
#    RTGG51   : NAME=RSGG51Y,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG51 /dev/null
# ******  FICHIER ISSU DU BVA405                                               
       m_FileAssign -d SHR -g ${G_A9} FGG500 ${DATA}/PTEM/STA0EYBA.BVA506GE
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA406 
       JUMP_LABEL=STA0EYBE
       ;;
(STA0EYBE)
       m_CondExec 04,GE,STA0EYBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=STA0EYZA
       ;;
(STA0EYZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STA0EYZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
