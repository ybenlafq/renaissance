#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTI02M.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMFTI02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/29 AT 10.44.32 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FTI02M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   SORT DES FICHIERS ISSU DES DIFFERENTS TRAITEMENTS DE GESTION               
#   PLUS LE FICHIER           DE REPRISE DE LA VEILLE.                         
#   REPRISEE NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FTI02MA
       ;;
(FTI02MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FTI02MAA
       ;;
(FTI02MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# *** FICHIER CAISSES NEM DACEM ISSU DE NM002D                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.FFTI49MM
# *** FICHIER DE REPRISE EN CAS DE PLANTAGE LA VEILLE                          
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FTI02REM
# *** FICHIER DE RECYCLAGE                                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FTRECYFM
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FFTI02GM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MAB
       ;;
(FTI02MAB)
       m_CondExec 00,EQ,FTI02MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   HISTORISATION DES FICHIERS MGD EN ENTREE                                   
#   REPRISE  NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MAD
       ;;
(FTI02MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER DU JOUR                                                          
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.FFTI49MM
# *** FICHIER HISTO                                                            
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FFTI49MM.HISTO
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FFTI49MM.HISTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MAE
       ;;
(FTI02MAE)
       m_CondExec 00,EQ,FTI02MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI50 .GENERATIONS DES MVTS COMPTABLES FRANCHISE                      
#   REPRISE: NON                                                               
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01,RSTRT=SAME                                       
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *** CARTE PARAM�TRE                                                          
# FPARAM   DATA  *,CLASS=FIX1,MBR=FTI01D01                                     
# F91                                                                          
#          DATAEND                                                             
# *** FDATE                                                                    
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# *** TABLE  EN ENTREE SANS MAJ                                                
# RTLI00   FILE  DYNAM=YES,NAME=RSLI00,MODE=(I,U)                              
# RTFM95   FILE  DYNAM=YES,NAME=RSFM95,MODE=(I,U)                              
# RTGA00   FILE  DYNAM=YES,NAME=RSGA00,MODE=(I,U)                              
# **** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                 
# FICICS   FILE  NAME=FFTI49AD,MODE=I                                          
# **** FICHIER EN SORTIE  (LRECL 400)                                          
# FICICD   FILE  NAME=FFTI50AD,MODE=O                                          
# **** FICHIER EN SORTIE  (LRECL 400)                                          
# FICICF   FILE  NAME=FFTI50BD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BFTI50) PLAN(BFTI50M)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#   PGM BFTI01 .GENERATIONS DES MVTS COMPTABLES                                
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MAG
       ;;
(FTI02MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTFM55   : NAME=RSFM55M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM55 /dev/null
#    RTFM80   : NAME=RSFM80M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM80 /dev/null
#    RTFM81   : NAME=RSFM81M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM81 /dev/null
#    RTFM82   : NAME=RSFM82M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM82 /dev/null
#    RTFM83   : NAME=RSFM83M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM83 /dev/null
#    RTFM89   : NAME=RSFM89M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM89 /dev/null
#    RTFM90   : NAME=RSFM90M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM90 /dev/null
#    RTFM91   : NAME=RSFM91M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM91 /dev/null
#    RTFM92   : NAME=RSFM92M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM92 /dev/null
#    RTFM97   : NAME=RSFM97,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM97 /dev/null
#    RTFM98   : NAME=RSFM98,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM98 /dev/null
#    RTFT85   : NAME=RSFT85M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT85 /dev/null
#    RTFT86   : NAME=RSFT86M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT86 /dev/null
#    RTFT88   : NAME=RSFT88M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT88 /dev/null
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# *********************************** PARAMETRE DATE / SOC                     
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  TABLE GENERALISEE: SOUS TABLES(TXTVA ENTST FEURO SDA2I)              
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                  
       m_FileAssign -d SHR -g +0 FFTI00 ${DATA}/PXX0/F89.FFTI49MM
# *** FICHIER EN SORTIE  (LRECL 200)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFV002 ${DATA}/PTEM/FTI02MAG.FFTI02AM
# *** FICHIER DES ANOMALIES POUR TRI                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 FREPRIS ${DATA}/PTEM/FTI02MAG.FFTI02BM
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IFTI00 ${DATA}/PTEM/FTI02MAG.IFTI02AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTI01 
       JUMP_LABEL=FTI02MAH
       ;;
(FTI02MAH)
       m_CondExec 04,GE,FTI02MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FTI02MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MAJ
       ;;
(FTI02MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER REPRISE                                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FTI02MAG.FFTI02BM
# *** FICHIER REPRISE TRIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MAJ.FFTI02CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 01 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MAK
       ;;
(FTI02MAK)
       m_CondExec 00,EQ,FTI02MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01AO: ISSU DU PGM BFTI01                               
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MAM
       ;;
(FTI02MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FTI02MAG.FFTI02AM
#  SORTIE                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MAM.FFTI02DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_169_8 169 CH 08
 /FIELDS FLD_CH_13_15 13 CH 15
 /KEYS
   FLD_CH_169_8 ASCENDING,
   FLD_CH_13_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MAN
       ;;
(FTI02MAN)
       m_CondExec 00,EQ,FTI02MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI06 : CONSTITUTION DU FICHIER ANOMALIES POUR LE LENDEMAIN           
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MAQ PGM=BFTI06     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MAQ
       ;;
(FTI02MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU PGM BFTI00 ET TRIE PRECEDEMMENT                             
# *** FICHIER DU FFTI00 TRIE                                                   
       m_FileAssign -d SHR -g +0 FFTI00 ${DATA}/PXX0/F89.FFTI49MM
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A3} FFTV02 ${DATA}/PTEM/FTI02MAM.FFTI02DM
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A4} FREPRIS ${DATA}/PTEM/FTI02MAJ.FFTI02CM
#                                                                              
#  FICHIER DES ANOMALIES REPRIS LE LENDEMAIN                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI05 ${DATA}/PXX0/F89.FTRECYFM
#                                                                              
#  FICHIER EN SORTIE POUR TRI                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV05 ${DATA}/PTEM/FTI02MAQ.FFTI06EM
#                                                                              
#  FICHIER EN SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PTEM/FTI02MAQ.FFTI02EM
       m_ProgramExec BFTI06 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER FFTV05 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MAT
       ;;
(FTI02MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/FTI02MAQ.FFTI06EM
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MAT.FFTI06FM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_177_1 177 CH 01
 /FIELDS FLD_CH_1_27 01 CH 27
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MAU
       ;;
(FTI02MAU)
       m_CondExec 00,EQ,FTI02MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FTI02MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MAX
       ;;
(FTI02MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FTI02MAT.FFTI06FM
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MAX.FFTI06GM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_123_8 123 PD 08
 /FIELDS FLD_CH_1_50 01 CH 50
 /FIELDS FLD_CH_75_48 75 CH 48
 /FIELDS FLD_CH_201_199 201 CH 199
 /FIELDS FLD_CH_57_12 57 CH 12
 /KEYS
   FLD_CH_1_50 ASCENDING,
   FLD_CH_201_199 ASCENDING,
   FLD_CH_57_12 ASCENDING,
   FLD_CH_75_48 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_123_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MAY
       ;;
(FTI02MAY)
       m_CondExec 00,EQ,FTI02MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI11 .TRAITEMENT DU CALCUL DES SOLDES ET CUMULS                      
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBA PGM=BFTI11     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBA
       ;;
(FTI02MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A7} FFTV06 ${DATA}/PTEM/FTI02MAX.FFTI06GM
#                                                                              
#  FICHIER POUR TRI ET POUR SAP                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV06B ${DATA}/PTEM/FTI02MBA.FFTI11BM
       m_ProgramExec BFTI11 
#                                                                              
# ********************************************************************         
#   TRI DU FICHIER FFTV05 ISSU DU PGM BFTI06                                   
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBD
       ;;
(FTI02MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/FTI02MAQ.FFTI06EM
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MBD.FFTI06HM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_1_27 01 CH 27
 /FIELDS FLD_CH_177_1 177 CH 01
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MBE
       ;;
(FTI02MBE)
       m_CondExec 00,EQ,FTI02MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER DES DEUX FICHIERS PRECEDENTS POUR FICHIER SAP              
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBG
       ;;
(FTI02MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/FTI02MBD.FFTI06HM
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/FTI02MBA.FFTI11BM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FFTI02M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_27 01 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MBH
       ;;
(FTI02MBH)
       m_CondExec 00,EQ,FTI02MBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   HISTORISATION DES FICHIERS VERS SAP                                        
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBJ
       ;;
(FTI02MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/F89.FFTI02M
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FTI02M
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FTI02M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MBK
       ;;
(FTI02MBK)
       m_CondExec 00,EQ,FTI02MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC1 (IFTI02AM)                                          
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBM
       ;;
(FTI02MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/FTI02MAG.IFTI02AM
# ********* FICHIER FEXTRAC1 TRIE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MBM.IFTI02BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_143_4 143 CH 04
 /FIELDS FLD_PD_139_4 139 PD 04
 /FIELDS FLD_BI_1_138 01 CH 138
 /KEYS
   FLD_BI_1_138 ASCENDING,
   FLD_BI_143_4 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_139_4
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MBN
       ;;
(FTI02MBN)
       m_CondExec 00,EQ,FTI02MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS IFTI02CM                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBQ
       ;;
(FTI02MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/FTI02MBM.IFTI02BM
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/FTI02MBQ.IFTI02CM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=FTI02MBR
       ;;
(FTI02MBR)
       m_CondExec 04,GE,FTI02MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBT
       ;;
(FTI02MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/FTI02MBQ.IFTI02CM
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MBT.IFTI02DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MBU
       ;;
(FTI02MBU)
       m_CondExec 00,EQ,FTI02MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IFTI01                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MBX
       ;;
(FTI02MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/FTI02MBM.IFTI02BM
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/FTI02MBT.IFTI02DM
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IFTI01 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=FTI02MBY
       ;;
(FTI02MBY)
       m_CondExec 04,GE,FTI02MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DES FICHIERS EN ENTREE DE L'ICS POUR EVITER LES TRAITEMENTS          
#         EN DOUBLE.                                                           
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MCA PGM=IDCAMS     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MCA
       ;;
(FTI02MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***   FIC DE CUMUL DES INTERFACES (EN CAS DE PLANTAGE DE L'ICS)              
       m_FileAssign -d SHR IN1 /dev/null
# ***   FICHIER CUMUL REMIS A ZERO                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F89.FTI02REM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02MCA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTI02MCB
       ;;
(FTI02MCB)
       m_CondExec 16,NE,FTI02MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MCD
       ;;
(FTI02MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/FTI02MAQ.FFTI02EM
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PXX0/F89.FTRECYFM
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02MCD.FFTI02FM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_19_381 19 CH 381
 /FIELDS FLD_CH_1_15 01 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_BI_19_381 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02MCE
       ;;
(FTI02MCE)
       m_CondExec 00,EQ,FTI02MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTRIEVE                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MCG PGM=EZTPA00    ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MCG
       ;;
(FTI02MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} FICHI ${DATA}/PXX0/F89.FFTI02M
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FICHO ${DATA}/PXX0/F89.FTI02SM
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FTI02MCG
       m_ProgramExec FTI02MCG
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FT??????                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MCJ PGM=EZACFSM1   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MCJ
       ;;
(FTI02MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02MCJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FTI02MCJ.FTFTI02M
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FT??????                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02MCM PGM=FTP        ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MCM
       ;;
(FTI02MCM)
       m_CondExec ${EXADW},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02MCM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FTI02MCJ.FTFTI02M(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#   DEPENDANCE POUR PLAN                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FTI02MZA
       ;;
(FTI02MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
