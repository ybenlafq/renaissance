#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTIA1M.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMFTIA1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/06 AT 10.28.41 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FTIA1M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#              CUMUL EN CAS DE PLANTAGE DE FTI01M                              
#   SORT DES FICHIERS ISSU DES DIFFERENTS TRAITEMENTS DE GESTION               
#   PLUS LE FICHIER RECYCLAGE DE LA VEILLE.                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FTIA1MA
       ;;
(FTIA1MA)
#
#FTIA1MAJ
#FTIA1MAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#FTIA1MAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/11/06 AT 10.28.41 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FTIA1M                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-FTI01M'                            
# *                           APPL...: IMPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FTIA1MAA
       ;;
(FTIA1MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER CAISSES NEM DACEM ISSU DE NM002M                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGM/F89.BNM001CM
# *** FICHIER CAISSES NEM DACEM ISSU DE NM030M                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BNM003AM
# *** FICHIER COMPTA NASL ISSU DE NASFTF   (SITE CENTRALIS�)                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.NASGCTM
# *** FICHIER CONTENTIEUX CREDOR ISSU DE FTCREP                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.CREDORBM
# *** FICHIER MVTS SAFIG ISSU DE LA FTCREG                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.SAFIDALG
# *** FICHIER COMPTABILISATION DES MOUVEMENTS DE STOCK INTRA-SOCI�T�           
# *** FICHIER NMD (CHAINE MD0020)                                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BMD002CM
# **** FICHIER VENANT DE BS001M                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BBS002AM
# **** FICHIER VENANT DE FS052M                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGM/F89.BBS052AM
# *** FICHIER DE REPRISE EN CAS DE PLANTAGE LA VEILLE                          
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FTI01REM
# **** FICHIER VENANT DE HD001P (ASSURANCE PAR ABONNEMENT)                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.HD001PM
# *** FICHIER DE REPRISE POUR (J+1)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.FTI01REM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTIA1MAB
       ;;
(FTIA1MAB)
       m_CondExec 00,EQ,FTIA1MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UNE GENERATION A VIDE POUR DEBLOCAGE CGICSM                      
#  EN CAS DE PLANTAGE DE FTI01M                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTIA1MAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTIA1MAD
       ;;
(FTIA1MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC A DESTINATION DU CGICSM                                          
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER REMIS A ZERO A DESTINATION DE CGICSM                         
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F89.FTI01M
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTIA1MAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTIA1MAE
       ;;
(FTIA1MAE)
       m_CondExec 16,NE,FTIA1MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMINE DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTIA1MAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
