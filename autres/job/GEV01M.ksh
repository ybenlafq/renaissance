#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  GEV01M.ksh                       --- VERSION DU 08/10/2016 22:00
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMGEV01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/06/02 AT 15.39.20 BY BURTECA                      
#    STANDARDS: P  JOBSET: GEV01M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PROGRAM : BEM004 EXTRACTION DES DONN�ES ET MAJ DE LA TABLE RTEM56          
#   REPRISE : OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=GEV01MA
       ;;
(GEV01MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       RUN=${RUN}
       JUMP_LABEL=GEV01MAA
       ;;
(GEV01MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#  TABLE DES LIEUX                                                             
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE RTEM51 GROUPE                                                         
#    RTEM51   : NAME=RSEM51,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM51 /dev/null
#  TABLE RTEM56 GROUPE                                                         
#    RTEM56   : NAME=RSEM56,MODE=U - DYNAM=YES                                 
       m_FileAssign -d SHR RTEM56 /dev/null
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEM004 
       JUMP_LABEL=GEV01MAB
       ;;
(GEV01MAB)
       m_CondExec 04,GE,GEV01MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PROGRAM : BEM005                                                           
#   REPRISE : OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP GEV01MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=GEV01MAD
       ;;
(GEV01MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  TABLE GENERALISEE                                                           
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#  TABLE RTEM56                                                                
#    RTEM56   : NAME=RSEM56,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTEM56 /dev/null
#  FICHIER FDATE                                                               
       m_FileAssign -i FDATE
$FDATE
_end
#  FICHIER FNSOC                                                               
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#  FICHIER FMOIS                                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
#  FICHIER EDITION                                                             
       m_OutputAssign -c 9 -w IEM005 IEM005
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEM005 
       JUMP_LABEL=GEV01MAE
       ;;
(GEV01MAE)
       m_CondExec 04,GE,GEV01MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
