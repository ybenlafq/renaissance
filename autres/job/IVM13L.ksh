#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM13L.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLIVM13 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/02/23 AT 12.03.18 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVM13L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BIV600                                                                
#  ------------                                                                
#                                                                              
#  CONTROLE DE TOP 'HSPRET' POUR TRAITER LA FIN D'INVENTAIRE (EDITIONS         
#                                                                              
#      - SI LE TOP HSPRET EST A 'OO', ON PEUT TRAITER LA FIN                   
#        D'INVENTAIRE ; EDITIONS CI-DESSOUS.                                   
#      - SI LE TOP HSPRET N'EST PAS A 'OO', IL Y A UN ABEND PROVOQU�           
#        DU PGM BIV600.                                                        
#                                                                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM13LA
       ;;
(IVM13LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVM13LAA
       ;;
(IVM13LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSIE10   : NAME=RSIE10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ------  CONTROLE DU TOP HS DANS RTIE10                                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13LAA
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV600 
       JUMP_LABEL=IVM13LAB
       ;;
(IVM13LAB)
       m_CondExec 04,GE,IVM13LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE540  : CREATION DU FICHIER DES ECARTS D'INVENTAIRE MAGASINS              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LAD
       ;;
(IVM13LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE D'INVENTAIRE                                                   
#    RSIE05   : NAME=RSIE05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05 /dev/null
# ******  TABLE D'AVANCEMENT D'INVENTAIRE                                      
#    RSIE10   : NAME=RSIE10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES ETATS                                                      
#    RSGA11   : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******  TABLE DES PRMP LILLE                                                 
#    RSGG50   : NAME=RSGG50L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  TABLE DES PRMP DACEM                                                 
#    RSGG55   : NAME=RSGG55L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  TABLE LOGISTIQUE                                                     
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FIE540 ${DATA}/PGI961/F61.BIE540AL
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE540 
       JUMP_LABEL=IVM13LAE
       ;;
(IVM13LAE)
       m_CondExec 04,GE,IVM13LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV130 :  CREE FIC DES ECARTS D INVENTAIRE MAGS                             
#  REPRISE:  OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LAG
       ;;
(IVM13LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISE (INVDA)                                             
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES SEQUENCES FAMILLES EDITIONS (PARAMETRAGES ETATS)           
#    RSGA11   : NAME=RSGA11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******  TABLE INVENTAIRE MAGASIN                                             
#    RSIN00   : NAME=RSIN00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00 /dev/null
#                                                                              
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00   : NAME=RSAN00L,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE MAGASIN                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FIN130 ${DATA}/PGI961/F61.BIV130AL
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV130 
       JUMP_LABEL=IVM13LAH
       ;;
(IVM13LAH)
       m_CondExec 04,GE,IVM13LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON              
#  SEQ FAMILLE                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LAJ
       ;;
(IVM13LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI961/F61.BIV130AL
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PGI961/F61.BIE540AL
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LAJ.BIV135AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_55_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LAK
       ;;
(IVM13LAK)
       m_CondExec 00,EQ,IVM13LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV135  : EDITION DES ECARTS FAMILLES ET SOUS-LIEU                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LAM
       ;;
(IVM13LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                         
       m_FileAssign -d SHR -g ${G_A3} FIV130 ${DATA}/PTEM/IVM13LAJ.BIV135AL
# ******  FIC D IMPRESSION ECARTS FAMILLES SOUS-LIEU                           
#  IIV135   REPORT SYSOUT=(9,IIV135),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV135 ${DATA}/PXX0.FICHEML.IVM13L.BIV135BL
# ******  ETAT NON VALORISE                                                    
       m_OutputAssign -c Z IIV135B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV135 
       JUMP_LABEL=IVM13LAN
       ;;
(IVM13LAN)
       m_CondExec 04,GE,IVM13LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR SOCIETE MAGASIN SEQUENCE RAYON SEQUENCE FAMILLE                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LAQ
       ;;
(IVM13LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LAQ.BIV130BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LAR
       ;;
(IVM13LAR)
       m_CondExec 00,EQ,IVM13LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV136  : EDITION DES ECARTS FAMILLES PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LAT
       ;;
(IVM13LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                         
       m_FileAssign -d SHR -g ${G_A5} FIV130 ${DATA}/PTEM/IVM13LAQ.BIV130BL
# ******  FIC D IMPRESSION LIEU RAYON FAMILLE                                  
#  IIV136   REPORT SYSOUT=(9,IIV136),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV136 ${DATA}/PXX0.FICHEML.IVM13L.BIV136BL
# ******  ETAT NON VALORISE                                                    
       m_OutputAssign -c Z IIV136B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV136 
       JUMP_LABEL=IVM13LAU
       ;;
(IVM13LAU)
       m_CondExec 04,GE,IVM13LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SUR SOCIETE SEQUENCE RAYON SEQ FAMILLE            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LAX
       ;;
(IVM13LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LAX.BIV137AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LAY
       ;;
(IVM13LAY)
       m_CondExec 00,EQ,IVM13LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137  : EDITION PAR FAMILLE TOUS MAGASINS                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBA PGM=BIV137     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBA
       ;;
(IVM13LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d SHR -g ${G_A7} FIV130 ${DATA}/PTEM/IVM13LAX.BIV137AL
# ******  EDITION TOUS MAGASINS RAYON FAMILLE                                  
#  IIV137   REPORT SYSOUT=(9,IIV137),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV137 ${DATA}/PXX0.FICHEML.IVM13L.BIV137DL
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13LBA
       m_ProgramExec BIV137 
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR SOCIETE SEQUENCE RAYON SEQ FAM          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBD
       ;;
(IVM13LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LBD.BIV137BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LBE
       ;;
(IVM13LBE)
       m_CondExec 00,EQ,IVM13LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137  : EDITION DES ECARTS SOCIETE PAR FAMILLE                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBG PGM=BIV137     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBG
       ;;
(IVM13LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A9} FIV130 ${DATA}/PTEM/IVM13LBD.BIV137BL
# ******  EDITION DES ECARTS SOCIETE PAR FAMILLE                               
#  IIV137   REPORT SYSOUT=(9,IIV137S),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV137 ${DATA}/PXX0.FICHEML.IVM13L.BIV137CL
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13LBG
       m_ProgramExec BIV137 
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR:                                        
#  SOCIETE SEQ FAM MARQUE CODIC LIEU SOUS-LIEU                                 
#  ET JE GARDE QUE LES ECARTS > 0                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBJ
       ;;
(IVM13LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LBJ.BIV139AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 X'00000C'
 /DERIVEDFIELD CST_3_13 X'00000D'
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_68_3 68 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_85_3 85 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_85_3 EQ CST_1_9 OR FLD_CH_85_3 EQ CST_3_13 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_68_3 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_72_3 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LBK
       ;;
(IVM13LBK)
       m_CondExec 00,EQ,IVM13LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV139  : EDITION DES ECARTS FAMILLE CODIC TOTAL CODIC                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBM PGM=BIV139     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBM
       ;;
(IVM13LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d SHR -g ${G_A11} FIV130 ${DATA}/PTEM/IVM13LBJ.BIV139AL
# ******  EDITION DES ECARTS SOCIETE PAR FAMILLE CODIC TOTAL CODIC             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV139 ${DATA}/PXX0.FICHEML.IVM13L.BIV139BL
       m_ProgramExec BIV139 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON SEQ FAMILLE          
#  LIB RAYON LIB FAMILLE LIB MAYQUE ARTICLE                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBQ
       ;;
(IVM13LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LBQ.BIV140AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LBR
       ;;
(IVM13LBR)
       m_CondExec 00,EQ,IVM13LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140  : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU ET SOUS-LIE         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBT
       ;;
(IVM13LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A13} FIV140 ${DATA}/PTEM/IVM13LBQ.BIV140AL
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITON                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV140 ${DATA}/PXX0.FICHEML.IVM13L.BIV140CL
# ******  MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                        
       m_OutputAssign -c Z IIV140M
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV140A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=IVM13LBU
       ;;
(IVM13LBU)
       m_CondExec 04,GE,IVM13LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SEQ RAYON SEQ FAM LIB RAYON LIB FAMILLE LIB MAYQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LBX
       ;;
(IVM13LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LBX.BIV130CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LBY
       ;;
(IVM13LBY)
       m_CondExec 00,EQ,IVM13LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV141 : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCA
       ;;
(IVM13LCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A15} FIV141 ${DATA}/PTEM/IVM13LBX.BIV130CL
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITON PAR LIEU/RAYON/FAMILLE/CODIC                                  
#  IIV141   REPORT SYSOUT=(9,IIV141),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV141 ${DATA}/PXX0.FICHEML.IVM13L.BIV141BL
# ******  MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                        
       m_OutputAssign -c Z IIV141M
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV141A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV141 
       JUMP_LABEL=IVM13LCB
       ;;
(IVM13LCB)
       m_CondExec 04,GE,IVM13LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SUR:                                              
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCD
       ;;
(IVM13LCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LCD.BIV130DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LCE
       ;;
(IVM13LCE)
       m_CondExec 00,EQ,IVM13LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142  : EDITION DES ECARTS MAGASINS PRODUITS                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCG PGM=BIV142     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCG
       ;;
(IVM13LCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d SHR -g ${G_A17} FIV142 ${DATA}/PTEM/IVM13LCD.BIV130DL
# ******  EDITION RAYON/FAMILLE/CODIC                                          
#  IIV142   REPORT SYSOUT=(9,IIV142),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142 ${DATA}/PXX0.FICHEML.IVM13L.BIV142BL
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
#  IIV142A  REPORT SYSOUT=(9,IIV142A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142A ${DATA}/PXX0.FICHEML.IVM13L.BIV142EL
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13LCG
       m_ProgramExec BIV142 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCJ
       ;;
(IVM13LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIERS DES ECARTS SOCIETE TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LCJ.BIV130EL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_60_5 60 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LCK
       ;;
(IVM13LCK)
       m_CondExec 00,EQ,IVM13LCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142  : EDITION DES ECARTS SOCIETE PRODUITS                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCM PGM=BIV142     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCM
       ;;
(IVM13LCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A19} FIV142 ${DATA}/PTEM/IVM13LCJ.BIV130EL
# ******  EDITION DES ECARTS SOCIETE RAYON/FAMILLE/CODIC                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142 ${DATA}/PXX0.FICHEML.IVM13L.BIV142CL
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV142A
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13LCM
       m_ProgramExec BIV142 
# ********************************************************************         
#  BIV142  : EDITION DES ECARTS SOCIETE PRODUITS                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCQ PGM=BIV142     ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCQ
       ;;
(IVM13LCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A20} FIV142 ${DATA}/PTEM/IVM13LCJ.BIV130EL
# ******  EDITION DES ECARTS SOCIETE RAYON/FAMILLE/CODIC                       
       m_OutputAssign -c Z IIV142
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142A ${DATA}/PXX0.FICHEML.IVM13L.BIV142DL
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13LCQ
       m_ProgramExec BIV142 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETEE SUR:                                     
#  SOCIETE MAG SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCT
       ;;
(IVM13LCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIERS DES ECARTS SOCIETE TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LCT.BIV143AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LCU
       ;;
(IVM13LCU)
       m_CondExec 00,EQ,IVM13LCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV143  :  EDITION DES ECARTS SOCIETE PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LCX
       ;;
(IVM13LCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A22} FIV143 ${DATA}/PTEM/IVM13LCT.BIV143AL
# ******  EDITION DES ECARTS SOCIETE PAR LIEU                                  
#  IIV143   REPORT SYSOUT=(9,IIV143),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV143 ${DATA}/PXX0.FICHEML.IVM13L.BIV143BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV143 
       JUMP_LABEL=IVM13LCY
       ;;
(IVM13LCY)
       m_CondExec 04,GE,IVM13LCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON LIB RAYON MAGASIN                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDA
       ;;
(IVM13LDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PGI961/F61.BIV130AL
# ******  FICHIERS DES ECARTS MAGASINS TRIE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM13LDA.BIV144AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_65_3 65 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13LDB
       ;;
(IVM13LDB)
       m_CondExec 00,EQ,IVM13LDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV144  : EDITION DES ECARTS SOCIETE PAR RAYON                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDD
       ;;
(IVM13LDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A24} FIV144 ${DATA}/PTEM/IVM13LDA.BIV144AL
# ******  EDITION DES ECARTS SOCIETE PAR RAYON MAGASINS                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV144 ${DATA}/PXX0.FICHEML.IVM13L.BIV144BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV144 
       JUMP_LABEL=IVM13LDE
       ;;
(IVM13LDE)
       m_CondExec 04,GE,IVM13LDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV135)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDG PGM=IEBGENER   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDG
       ;;
(IVM13LDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV135BL
       m_OutputAssign -c 9 -w IIV135 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LDH
       ;;
(IVM13LDH)
       m_CondExec 00,EQ,IVM13LDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV136)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDJ PGM=IEBGENER   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDJ
       ;;
(IVM13LDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV136BL
       m_OutputAssign -c 9 -w IIV136 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LDK
       ;;
(IVM13LDK)
       m_CondExec 00,EQ,IVM13LDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDM PGM=IEBGENER   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDM
       ;;
(IVM13LDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV137DL
       m_OutputAssign -c 9 -w IIV137 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LDN
       ;;
(IVM13LDN)
       m_CondExec 00,EQ,IVM13LDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDQ PGM=IEBGENER   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDQ
       ;;
(IVM13LDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV137CL
       m_OutputAssign -c 9 -w IIV137S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LDR
       ;;
(IVM13LDR)
       m_CondExec 00,EQ,IVM13LDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV139)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDT PGM=IEBGENER   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDT
       ;;
(IVM13LDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV139BL
       m_OutputAssign -c 9 -w IIV139 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LDU
       ;;
(IVM13LDU)
       m_CondExec 00,EQ,IVM13LDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV140)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LDX PGM=IEBGENER   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LDX
       ;;
(IVM13LDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV140CL
       m_OutputAssign -c 9 -w IIV140 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LDY
       ;;
(IVM13LDY)
       m_CondExec 00,EQ,IVM13LDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV141)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LEA
       ;;
(IVM13LEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV141BL
       m_OutputAssign -c 9 -w IIV141 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LEB
       ;;
(IVM13LEB)
       m_CondExec 00,EQ,IVM13LEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LED PGM=IEBGENER   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LED
       ;;
(IVM13LED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV142BL
       m_OutputAssign -c 9 -w IIV142 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LEE
       ;;
(IVM13LEE)
       m_CondExec 00,EQ,IVM13LED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142A)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LEG PGM=IEBGENER   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LEG
       ;;
(IVM13LEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV142EL
       m_OutputAssign -c 9 -w IIV142A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LEH
       ;;
(IVM13LEH)
       m_CondExec 00,EQ,IVM13LEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LEJ PGM=IEBGENER   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LEJ
       ;;
(IVM13LEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV142CL
       m_OutputAssign -c 9 -w IIV142S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LEK
       ;;
(IVM13LEK)
       m_CondExec 00,EQ,IVM13LEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142SA)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LEM PGM=IEBGENER   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LEM
       ;;
(IVM13LEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV142DL
       m_OutputAssign -c 9 -w IIV142SA SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LEN
       ;;
(IVM13LEN)
       m_CondExec 00,EQ,IVM13LEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV143)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LEQ PGM=IEBGENER   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LEQ
       ;;
(IVM13LEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV143BL
       m_OutputAssign -c 9 -w IIV143 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LER
       ;;
(IVM13LER)
       m_CondExec 00,EQ,IVM13LEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV144)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13LET PGM=IEBGENER   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LET
       ;;
(IVM13LET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13L.BIV144BL
       m_OutputAssign -c 9 -w IIV144 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13LEU
       ;;
(IVM13LEU)
       m_CondExec 00,EQ,IVM13LET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM13LZA
       ;;
(IVM13LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM13LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
