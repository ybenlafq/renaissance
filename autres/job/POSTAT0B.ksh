#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  POSTAT0B.ksh                       --- VERSION DU 19/10/2016 19:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POSTAT0B -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    STANDARDS: P  JOBSET: STAT0O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030A ISSU DU PGM BHV030                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=STAT0OB
       ;;
(STAT0OB)
       EXCAA=${EXCAA:-0}
       EXCAF=${EXCAF:-0}
       EXCAK=${EXCAK:-0}
       EXCAP=${EXCAP:-0}
       EXCAU=${EXCAU:-0}
       EXCAZ=${EXCAZ:-0}
       EXCBE=${EXCBE:-0}
       EXCBJ=${EXCBJ:-0}
       EXCBO=${EXCBO:-0}
       EXCBT=${EXCBT:-0}
       EXCBY=${EXCBY:-0}
       EXCCD=${EXCCD:-0}
       EXCCI=${EXCCI:-0}
       EXCCN=${EXCCN:-0}
       EXCCS=${EXCCS:-0}
       EXCCX=${EXCCX:-0}
       EXC98=${EXC98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2009/12/08 AT 15.50.41 BY PREPA2                 
# *    JOBSET INFORMATION:    NAME...: STAT0O                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'MENSUELLE STAT/VALO'                   
# *                           APPL...: REPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT0OBA
       ;;
(STAT0OBA)
       m_CondExec ${EXCAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BHV030AO
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0OBA.BVA500AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_70_8 70 CH 8
 /FIELDS FLD_CH_19_7 19 CH 7
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_70_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0OBB
       ;;
(STAT0OBB)
       m_CondExec 00,EQ,STAT0OBA ${EXCAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA500                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OBD PGM=IKJEFT01   ** ID=CAF                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OBD
       ;;
(STAT0OBD)
       m_CondExec ${EXCAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA00   : NAME=RSVA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTGG70   : NAME=RSGG70O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PTEM/STAT0OBA.BVA500AO
# *****   FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 FVA00 ${DATA}/PTEM/STAT0OBD.FVA000AO
# *****   FICHIER DES ANOMALIES ENTRANT DANS LE GENERATEUR D'ETAT              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FVA500 ${DATA}/PXX0/F16.FVA500AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA500 
       JUMP_LABEL=STAT0OBE
       ;;
(STAT0OBE)
       m_CondExec 04,GE,STAT0OBD ${EXCAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA000A POUR CREATION FICHIER ENTRANT DANS BGG500  *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OBG PGM=SORT       ** ID=CAK                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OBG
       ;;
(STAT0OBG)
       m_CondExec ${EXCAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/STAT0OBD.FVA000AO
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/STAT0OBG.FVA000BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "916"
 /DERIVEDFIELD CST_3_13 "P"
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_1_7 1 CH 7
 /CONDITION CND_2 FLD_CH_8_3 EQ CST_1_9 AND FLD_CH_14_1 EQ CST_3_13 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_48_8,FLD_CH_36_6,FLD_CH_42_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0OBH
       ;;
(STAT0OBH)
       m_CondExec 00,EQ,STAT0OBG ${EXCAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG500                                                                
# ********************************************************************         
#  RECALCUL DES PRMPS DU MOIS                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OBJ PGM=IKJEFT01   ** ID=CAP                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OBJ
       ;;
(STAT0OBJ)
       m_CondExec ${EXCAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FVA00 ${DATA}/PTEM/STAT0OBG.FVA000BO
# ******  TABLES EN ENTREE                                                     
#    RTVA05   : NAME=RSVA05O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FGG500 ${DATA}/PTEM/STAT0OBJ.FGG500AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG500 
       JUMP_LABEL=STAT0OBK
       ;;
(STAT0OBK)
       m_CondExec 04,GE,STAT0OBJ ${EXCAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FGG500AO                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OBM PGM=SORT       ** ID=CAU                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OBM
       ;;
(STAT0OBM)
       m_CondExec ${EXCAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/STAT0OBJ.FGG500AO
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PTEM/STAT0OBM.FGG500BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_12 19 CH 12
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_4_7 4 CH 7
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_4_7,FLD_CH_11_8,FLD_CH_19_12
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0OBN
       ;;
(STAT0OBN)
       m_CondExec 00,EQ,STAT0OBM ${EXCAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA000A POUR CREATION FICHIER ENTRANT DANS BGG505  *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OBQ PGM=SORT       ** ID=CAZ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OBQ
       ;;
(STAT0OBQ)
       m_CondExec ${EXCAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/STAT0OBD.FVA000AO
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 SORTOUT ${DATA}/PTEM/STAT0OBQ.FVA000CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_48_8 48 CH 8
 /KEYS
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0OBR
       ;;
(STAT0OBR)
       m_CondExec 00,EQ,STAT0OBQ ${EXCAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG505                                                                
# ********************************************************************         
#  MAJ DES PRMPS APRES RECALCUL                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OBT PGM=IKJEFT01   ** ID=CBE                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OBT
       ;;
(STAT0OBT)
       m_CondExec ${EXCBE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG70   : NAME=RSGG70O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER EN ENTREE ISSU DU TRI PRECEDENT                              
       m_FileAssign -d SHR -g ${G_A6} RTVA00I ${DATA}/PTEM/STAT0OBQ.FVA000CO
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER DES PRA ENTRANT DANS LE GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGG505 ${DATA}/PXX0/F16.FGG505AO
# ******  FICHIER D'EXTRACTION ISSU DU BGG500                                  
       m_FileAssign -d SHR -g ${G_A7} FGG500 ${DATA}/PTEM/STAT0OBM.FGG500BO
# ******  FICHIER POUR MAJ DE RTGA67 (GG506P)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGA67 ${DATA}/PNCGO/F16.FGA67AO
# ******  FICHIER ENTRANT DANS ,VA505 ET SERVANT A LOADER RTVA00               
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 RTVA00S ${DATA}/PXX0/F16.FVA00AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG505 
       JUMP_LABEL=STAT0OBU
       ;;
(STAT0OBU)
       m_CondExec 04,GE,STAT0OBT ${EXCBE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA00A POUR CREATION FICHIER BVA505A ENTRANT                 
#  DANS LE PROG BVA505                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OBX PGM=SORT       ** ID=CBJ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OBX
       ;;
(STAT0OBX)
       m_CondExec ${EXCBJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F16.FVA00AO
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PTEM/STAT0OBX.BVA505AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "P"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_76_8 76 CH 8
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_PD_76_8 76 PD 8
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_PD_68_8 68 PD 8
 /FIELDS FLD_CH_68_8 68 CH 8
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_11 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6,
    TOTAL FLD_PD_68_8,
    TOTAL FLD_PD_76_8
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_11_3,FLD_CH_8_3,FLD_CH_36_6,FLD_CH_68_8,FLD_CH_42_6,FLD_CH_76_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0OBY
       ;;
(STAT0OBY)
       m_CondExec 00,EQ,STAT0OBX ${EXCBJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA505                                                                
# ********************************************************************         
#  MAJ DE LA RTVA05 POUR METTRE A JOUR EN QUANTITE ET EN VALEUR LES            
#  STOCKS,ET CREATION D'UN FICHIER BVA506A POUR MAJ DE LA TABLE                
#  RTGG50 DANS LA PGM BVA506                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCA PGM=IKJEFT01   ** ID=CBO                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCA
       ;;
(STAT0OCA)
       m_CondExec ${EXCBO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG55   : NAME=RSGG55O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTVA10   : NAME=RSVA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA10 /dev/null
#    RTVA15   : NAME=RSVA15O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA15 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTVA05   : NAME=RSVA05O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGG50   : NAME=RSGG50O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A9} FVA505 ${DATA}/PTEM/STAT0OBX.BVA505AO
# ******  FICHIER SERVANT A LOADER LE TABLE RTVA15                             
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -g +1 FVA515 ${DATA}/PXX0/F16.FVA15AO
# ******  FICHIER ENTRANT DANS VA001R ET SERVANT A LOADER RTVA30               
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 RTVA30 ${DATA}/PXX0/F16.FVA30AO
# ******  FICHIER SERVANT A LA MAJ DE LA RTGG50 DANS LE BVA506                 
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -g +1 FGG500 ${DATA}/PTEM/STAT0OCA.BVA506AO
# ******  FICHIER A HISTORISER QUI PERMET DE LOADER LA TABLE RTVA10            
# *****   FICHIER PARAMETRE 'M' POUR TRAIT. MENSUEL                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0OCA
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA505 
       JUMP_LABEL=STAT0OCB
       ;;
(STAT0OCB)
       m_CondExec 04,GE,STAT0OCA ${EXCBO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA506                                                                
# ********************************************************************         
#  MAJ DE LA RTGG50 POUR RECALCUL DES PRMS A PARTIR DU FICHIER                 
#  BVA506AR ISSU DU BVA505                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCD PGM=IKJEFT01   ** ID=CBT                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCD
       ;;
(STAT0OCD)
       m_CondExec ${EXCBT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU BVA505                                               
       m_FileAssign -d SHR -g ${G_A10} FGG500 ${DATA}/PTEM/STAT0OCA.BVA506AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA506 
       JUMP_LABEL=STAT0OCE
       ;;
(STAT0OCE)
       m_CondExec 04,GE,STAT0OCD ${EXCBT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LGT 15.01.2001 : EVITE RELABEL TT FILIALES                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCG PGM=IEFBR14    ** ID=CBY                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCG
       ;;
(STAT0OCG)
       m_CondExec ${EXCBY},NE,YES 
       m_ProgramExec IEFBR14 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030A                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCJ PGM=SORT       ** ID=CCD                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCJ
       ;;
(STAT0OCJ)
       m_CondExec ${EXCCD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BHV030AO
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0OCJ.HV0000BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "VEN"
 /DERIVEDFIELD CST_3_9 "VEN"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_30_3 30 CH 3
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_5 OR FLD_CH_13_3 EQ CST_3_9 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_30_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0OCK
       ;;
(STAT0OCK)
       m_CondExec 00,EQ,STAT0OCJ ${EXCCD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV150                                                                
# ********************************************************************         
#  CREATION DES FICHIERS DE LOAD DE LA RTHV09 ET RTHV10 ET DES FICHIER         
#  CUMULS FHV01                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCM PGM=IKJEFT01   ** ID=CCI                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCM
       ;;
(STAT0OCM)
       m_CondExec ${EXCCI},NE,YES 
#  SYSPRINT REPORT SYSOUT=(9,BGV150)                                           
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN ENTREE                                                      
#    RTGA00   : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG50   : NAME=RSGG50O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGG55   : NAME=RSGG55O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
# ******* FICHIER HISTO FHV01 (ARTICLE DARTY)                                  
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PGV0/F16.HV01AO
# ******* FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A11} SV0404 ${DATA}/PTEM/STAT0OCJ.HV0000BO
# ******* FIC DES VTES DU MOIS PAR ARTICLES AYANT PU ETRE RECYCLE              
# *******               (LOAD DE LA RTHV10)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGV150 ${DATA}/PGV0/F16.GV0150AO
# ******* FIC DES VTES DU MOIS PAR FAMILLE AYANT PU ETRE RECYCLE               
# *******               (LOAD DE LA RTHV09)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FGV158 ${DATA}/PGV0/F16.GV0158AO
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV152 ${DATA}/PGV0/F16.GV0152AO
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV153 ${DATA}/PGV0/F16.GV0153AO
# ******* CARTE PARAMETRE MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******   PARAMETRE SOCIETE : 916                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV150 
       JUMP_LABEL=STAT0OCN
       ;;
(STAT0OCN)
       m_CondExec 04,GE,STAT0OCM ${EXCCI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER DES VENTES ET REPRISES NON TRAITEES PAR BHV030               
#  NSOCORIG,NLIEU,NORIGINE,CODIC-GROUP,CMODDEL,NCODIC                          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCQ PGM=SORT       ** ID=CCN                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCQ
       ;;
(STAT0OCQ)
       m_CondExec ${EXCCN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F16.BHV100AO
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -g +1 SORTOUT ${DATA}/PTEM/STAT0OCQ.BHV100BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_21_3 ASCENDING,
   FLD_CH_24_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0OCR
       ;;
(STAT0OCR)
       m_CondExec 00,EQ,STAT0OCQ ${EXCCN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHV100                                                                
# ********************************************************************         
#  MAJ DE LA TABLE HISTO VENTES ET REPRISE DU MOIS                             
#  POUR LES GROUPES DE PRODUITS                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCT PGM=IKJEFT01   ** ID=CCS                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCT
       ;;
(STAT0OCT)
       m_CondExec ${EXCCS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00O  : NAME=RSGA00O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00O /dev/null
# ******  TABLE SOUS TABLES GENERALISEES                                       
#    RSGA01O  : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01O /dev/null
# ******  DETAILS DE VENTES                                                    
#    RSGV11O  : NAME=RSGV11O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11O /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50O  : NAME=RSGG50O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50O /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55O  : NAME=RSGG55O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG55O /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58O  : NAME=RSGA58O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA58O /dev/null
# ******  VENTES ET REPRISES DU MOIS/GROUP-PRODUITS                            
       m_FileAssign -d SHR -g ${G_A12} FHV100 ${DATA}/PTEM/STAT0OCQ.BHV100BO
#                                                                              
# ******  HISTO VENTES ET REPRISES DE VENTES DU MOIS PAR GROUP-ARTICLE         
#    RSHV06O  : NAME=RSHV06O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV06O /dev/null
# ******  ANOMALIES                                                            
#    RSAN00O  : NAME=RSAN00O,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00O /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV100 
       JUMP_LABEL=STAT0OCU
       ;;
(STAT0OCU)
       m_CondExec 04,GE,STAT0OCT ${EXCCS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA DATE POUR L'HITO DES PSE (GCX55O)                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0OCX PGM=SORT       ** ID=CCX                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OCX
       ;;
(STAT0OCX)
       m_CondExec ${EXCCX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU MOIS A TRAITER                                               
       m_FileAssign -i SORTIN
$FMOIS
_end
# ******  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F16.DATPSE0O
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0OCY
       ;;
(STAT0OCY)
       m_CondExec 00,EQ,STAT0OCX ${EXCCX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=C98                                   
# ***********************************                                          
       JUMP_LABEL=STAT0OZB
       ;;
(STAT0OZB)
       m_CondExec ${EXC98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0OZB.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
# *********************************************************************        
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
