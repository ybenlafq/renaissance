#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SYNT0G.ksh                       --- VERSION DU 09/10/2016 05:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGSYNT0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/05 AT 08.11.33 BY BURTEC2                      
#    STANDARDS: P  JOBSET: SYNT0G                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  CUMUL DES FICHIERS SYNTH�SE TYPE "CUISINE"                                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=SYNT0GA
       ;;
(SYNT0GA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+2'}
       G_A3=${G_A3:-'+2'}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2013/07/05 AT 08.11.33 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: SYNT0G                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'CUMUL SYNT VTES'                       
# *                           APPL...: REPGROUP                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=SYNT0GAA
       ;;
(SYNT0GAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BSYNT2AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BSYNT2AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BSYNT2AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BSYNT2AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BSYNT2AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F08.BSYNT2AX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BSYNT2AY
       m_FileAssign -d NEW,CATLG,DELETE -r 84 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.BSYNT2AG
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_30 01 CH 30
 /KEYS
   FLD_CH_1_30 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SYNT0GAB
       ;;
(SYNT0GAB)
       m_CondExec 00,EQ,SYNT0GAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CUMUL DES FICHIERS SYNTH�SE TYPE "DETAIL"                                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0GAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GAD
       ;;
(SYNT0GAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BSYNT3AD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.BSYNT3AL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.BSYNT3AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BSYNT3AO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BSYNT3AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F08.BSYNT3AX
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.BSYNT3AY
       m_FileAssign -d NEW,CATLG,DELETE -r 84 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.BSYNT3AG
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_30 01 CH 30
 /KEYS
   FLD_CH_1_30 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SYNT0GAE
       ;;
(SYNT0GAE)
       m_CondExec 00,EQ,SYNT0GAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ENVOI FICHIER VERS GATEWAY SYNTHESE TYPE "CUISINE"                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAK      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BSYNTCUI,                                                           
#      FNAME=":BSYNT2AG""(0),                                                  
#      NFNAME=COMPTES_CLIENTS_CUISINES_&FDATE.CSV                              
#          DATAEND                                                             
# ********************************************************************         
#  ENVOI FICHIER VERS GATEWAY SYNTHESE TYPES "DETAIL"                          
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BSYNTDET,                                                           
#      FNAME=":BSYNT3AG""(0),                                                  
#      NFNAME=COMPTES_CLIENTS_DETAIL_&FDATE.CSV                                
#          DATAEND                                                             
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTSYNT0G                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0GAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GAG
       ;;
(SYNT0GAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0GAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/SYNT0GAG.FTSYNT0G
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTSYNT0G                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0GAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GAJ
       ;;
(SYNT0GAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0GAJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.SYNT0GAG.FTSYNT0G(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP SYNT0GAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GAM
       ;;
(SYNT0GAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0GAM.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTSYNT0G                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0GAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GAQ
       ;;
(SYNT0GAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0GAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A2} SYSOUT ${DATA}/PTEM/SYNT0GAG.FTSYNT0G
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTSYNT0G                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0GAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GAT
       ;;
(SYNT0GAT)
       m_CondExec ${EXABE},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0GAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.SYNT0GAG.FTSYNT0G(+2),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP SYNT0GAX PGM=EZACFSM1   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GAX
       ;;
(SYNT0GAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0GAX.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0GZA
       ;;
(SYNT0GZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0GZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
