#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM26Y.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYIVM26 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/03/20 AT 14.30.33 BY BURTEC6                      
#    STANDARDS: P  JOBSET: IVM26Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIV125   MAJ DE LA TABLE STOCK MAGASINS RTGS30 IMPUTATIONS DES ECAR         
#                      D'INVENTAIRE                                            
# ********************************************************************         
#  REPRISE: NON APRES UNE FIN NORMALE                                          
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM26YA
       ;;
(IVM26YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXA98=${EXA98:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2001/03/20 AT 14.30.33 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: IVM26Y                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK INVT'                        
# *                           APPL...: IMPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM26YAA
       ;;
(IVM26YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******  INVENTAIRE MAGASIN                                                   
#    RSIN00Y  : NAME=RSIN00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00Y /dev/null
# ******  LIENS INTER ARTICLES                                                 
#    RSGA58Y  : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58Y /dev/null
# ******  STOCKS MAGASINS                                                      
#    RSGS30Y  : NAME=RSGS30Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSGS30Y /dev/null
# ******  ANOMALIES                                                            
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00Y /dev/null
# ******  SOCIETE A TRAITER   = 945                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  FICHIER DES MAJ AYANT RENDU LE STOCK DISPO NEGATIF                   
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 FER125 ${DATA}/PGI945/F45.BIV125AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV125 
       JUMP_LABEL=IVM26YAB
       ;;
(IVM26YAB)
       m_CondExec 04,GE,IVM26YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM26YZA
       ;;
(IVM26YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM26YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
