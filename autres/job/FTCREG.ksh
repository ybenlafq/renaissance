#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTCREG.ksh                       --- VERSION DU 08/10/2016 23:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGFTCRE -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/12 AT 18.01.40 BY BURTECA                      
#    STANDARDS: P  JOBSET: FTCREG                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EASYTREAVE POUR TRAITEMENT LES FICHIERS D'INTERFACE - SAFIG - CARTE         
#  CADEAUX - NOTE DE FRAIS (NOTILUS)                                           
#  FICHIERS PROVENANT DE LA GATEWAY                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FTCREGA
       ;;
(FTCREGA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+2'}
       G_A12=${G_A12:-'+3'}
       G_A13=${G_A13:-'+3'}
       G_A14=${G_A14:-'+4'}
       G_A15=${G_A15:-'+4'}
       G_A16=${G_A16:-'+5'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+2'}
       G_A19=${G_A19:-'+2'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+3'}
       G_A21=${G_A21:-'+3'}
       G_A22=${G_A22:-'+4'}
       G_A23=${G_A23:-'+4'}
       G_A24=${G_A24:-'+5'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+2'}
       G_A27=${G_A27:-'+2'}
       G_A28=${G_A28:-'+3'}
       G_A29=${G_A29:-'+3'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+4'}
       G_A31=${G_A31:-'+4'}
       G_A32=${G_A32:-'+5'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+2'}
       G_A35=${G_A35:-'+2'}
       G_A36=${G_A36:-'+3'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2015/08/12 AT 18.01.40 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FTCREG                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'INTEGR ICS BAT SAFIG'                  
# *                           APPL...: REPGROUP                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FTCREGAA
       ;;
(FTCREGAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c X SYSPRINT
       m_FileAssign -d SHR -g +0 FILEA ${DATA}/PXX0/F07.SAFIGKDO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.SAFIREPR
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/FTP.F07.EXTLIKDO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.EXTLREPR
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d SHR -C ${DATA}/PXX0/FTP.F99.DUE000AG.DUE
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F99.NOTEREPR
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 SORTIE ${DATA}/PXX0/F07.SAFIEASG
       m_OutputAssign -c T IMPRIM
       m_OutputAssign -c T SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FTCREGAA
       m_ProgramExec FTCREGAA
# ********************************************************************         
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DIF          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGAD
       ;;
(FTCREGAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.SAFIDIFG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "907"
 /DERIVEDFIELD CST_3_10 "829"
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_16_3 16 CH 3
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 OR FLD_CH_102_3 EQ CST_3_10 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGAE
       ;;
(FTCREGAE)
       m_CondExec 00,EQ,FTCREGAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DNN          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGAG
       ;;
(FTCREGAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.SAFIDNNG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "961"
 /DERIVEDFIELD CST_3_10 "824"
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_16_3 16 CH 3
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 OR FLD_CH_102_3 EQ CST_3_10 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGAH
       ;;
(FTCREGAH)
       m_CondExec 00,EQ,FTCREGAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DAL          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGAJ
       ;;
(FTCREGAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.SAFIDALG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "989"
 /DERIVEDFIELD CST_3_10 "827"
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_16_3 16 CH 3
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 OR FLD_CH_102_3 EQ CST_3_10 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGAK
       ;;
(FTCREGAK)
       m_CondExec 00,EQ,FTCREGAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DRA          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGAM
       ;;
(FTCREGAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.SAFIDRAG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_10 "822"
 /DERIVEDFIELD CST_1_6 "945"
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_CH_102_3 102 CH 3
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 OR FLD_CH_102_3 EQ CST_3_10 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGAN
       ;;
(FTCREGAN)
       m_CondExec 00,EQ,FTCREGAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DPM          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGAQ
       ;;
(FTCREGAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.SAFIDPMG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_10 "828"
 /DERIVEDFIELD CST_1_6 "991"
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_16_3 16 CH 3
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 OR FLD_CH_102_3 EQ CST_3_10 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGAR
       ;;
(FTCREGAR)
       m_CondExec 00,EQ,FTCREGAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DO           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGAT
       ;;
(FTCREGAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.SAFIDOG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "916"
 /DERIVEDFIELD CST_3_10 "821"
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_144_8 144 CH 8
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 OR FLD_CH_102_3 EQ CST_3_10 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGAU
       ;;
(FTCREGAU)
       m_CondExec 00,EQ,FTCREGAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DLUX         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGAX
       ;;
(FTCREGAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F08.SAFILUXG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "908"
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_144_8 144 CH 8
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGAY
       ;;
(FTCREGAY)
       m_CondExec 00,EQ,FTCREGAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE CAPR         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBA
       ;;
(FTCREGBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/F07.SAFIEASG
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F97.SAFIKAPG.NOT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_7_18 "513"
 /DERIVEDFIELD CST_5_14 "901"
 /DERIVEDFIELD CST_3_10 "802"
 /DERIVEDFIELD CST_1_6 "511"
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_144_8 144 CH 8
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 OR FLD_CH_102_3 EQ CST_3_10 OR FLD_CH_102_3 EQ CST_5_14 OR FLD_CH_102_3 EQ CST_7_18 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREGBB
       ;;
(FTCREGBB)
       m_CondExec 00,EQ,FTCREGBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DU  FICHIER REPRISE POUR �VITER DES DOUBLONS                         
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBD PGM=IDCAMS     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBD
       ;;
(FTCREGBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***   FIC REPRISE DE L'INTERFACE SAFIGKDO                                    
       m_FileAssign -d SHR IN1 /dev/null
# ***   FIC REPRISE DE L'INTERFACE EXTLIKDO                                    
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d SHR IN3 /dev/null
# ***   FICHIER CUMUL REMIS A ZERO                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F99.SAFIREPR
# ***   FICHIER CUMUL REMIS A ZERO                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT2 ${DATA}/PXX0/F99.EXTLREPR
# ***   FICHIER CUMUL REMIS A ZERO                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT3 ${DATA}/PXX0/F99.NOTEREPR
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTCREGBD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGBE
       ;;
(FTCREGBE)
       m_CondExec 16,NE,FTCREGBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SAUVEGARDE DES FICHIERS                                                     
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBG PGM=IDCAMS     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBG
       ;;
(FTCREGBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUTPUT ${DATA}/PXX0/F99.FTCR99AP.HISTO
       m_FileAssign -d SHR INPUT ${DATA}/PXX0.F99.NDF000AG.NDF
       m_FileAssign -d SHR -C ${DATA}/PXX0.F99.PRV000AG.PRV
       m_FileAssign -d SHR -C ${DATA}/PXX0.F99.AVA000AG.AVA
       m_FileAssign -d SHR -C ${DATA}/PXX0.F99.DUE000AG.DUE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGBH
       ;;
(FTCREGBH)
       m_CondExec 16,NE,FTCREGBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A VIDE                                                               
#  REPRISE : NON                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBJ PGM=IDCAMS     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBJ
       ;;
(FTCREGBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FICHIER DUMMY                                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DESTINATION                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTCREGBJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGBK
       ;;
(FTCREGBK)
       m_CondExec 16,NE,FTCREGBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBM PGM=IDCAMS     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBM
       ;;
(FTCREGBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A9} INPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A10} OUTPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBQ PGM=WAITSS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBQ
       ;;
(FTCREGBQ)
       m_CondExec ${EXACN},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGBN
       ;;
(FTCREGBN)
       m_CondExec 16,NE,FTCREGBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBT PGM=IDCAMS     ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBT
       ;;
(FTCREGBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A11} INPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A12} OUTPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGBX PGM=WAITSS     ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGBX
       ;;
(FTCREGBX)
       m_CondExec ${EXACX},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGBU
       ;;
(FTCREGBU)
       m_CondExec 16,NE,FTCREGBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCA PGM=IDCAMS     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCA
       ;;
(FTCREGCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A13} INPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A14} OUTPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCD PGM=WAITSS     ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCD
       ;;
(FTCREGCD)
       m_CondExec ${EXADH},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGCB
       ;;
(FTCREGCB)
       m_CondExec 16,NE,FTCREGCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCG PGM=IDCAMS     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCG
       ;;
(FTCREGCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A15} INPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A16} OUTPUT ${DATA}/PXX0/FTP.F99.NDF000AG.NDF
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCJ PGM=WAITSS     ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCJ
       ;;
(FTCREGCJ)
       m_CondExec ${EXADR},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGCH
       ;;
(FTCREGCH)
       m_CondExec 16,NE,FTCREGCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A VIDE                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCM PGM=IDCAMS     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCM
       ;;
(FTCREGCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FICHIER DUMMY                                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DESTINATION                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTCREGCM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGCN
       ;;
(FTCREGCN)
       m_CondExec 16,NE,FTCREGCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCQ PGM=IDCAMS     ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCQ
       ;;
(FTCREGCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A17} INPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A18} OUTPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCT PGM=WAITSS     ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCT
       ;;
(FTCREGCT)
       m_CondExec ${EXAEG},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGCR
       ;;
(FTCREGCR)
       m_CondExec 16,NE,FTCREGCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGCX PGM=IDCAMS     ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGCX
       ;;
(FTCREGCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A19} INPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A20} OUTPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDA PGM=WAITSS     ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDA
       ;;
(FTCREGDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGCY
       ;;
(FTCREGCY)
       m_CondExec 16,NE,FTCREGCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDD PGM=IDCAMS     ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDD
       ;;
(FTCREGDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A21} INPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A22} OUTPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDG PGM=WAITSS     ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDG
       ;;
(FTCREGDG)
       m_CondExec ${EXAFA},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGDE
       ;;
(FTCREGDE)
       m_CondExec 16,NE,FTCREGDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDJ PGM=IDCAMS     ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDJ
       ;;
(FTCREGDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A23} INPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A24} OUTPUT ${DATA}/PXX0/FTP.F99.PRV000AG.PRV
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDM PGM=WAITSS     ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDM
       ;;
(FTCREGDM)
       m_CondExec ${EXAFK},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGDK
       ;;
(FTCREGDK)
       m_CondExec 16,NE,FTCREGDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A VIDE                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDQ PGM=IDCAMS     ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDQ
       ;;
(FTCREGDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FICHIER DUMMY                                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DESTINATION                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTCREGDQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGDR
       ;;
(FTCREGDR)
       m_CondExec 16,NE,FTCREGDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDT PGM=IDCAMS     ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDT
       ;;
(FTCREGDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A25} INPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A26} OUTPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGDX PGM=WAITSS     ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGDX
       ;;
(FTCREGDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGDU
       ;;
(FTCREGDU)
       m_CondExec 16,NE,FTCREGDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGEA PGM=IDCAMS     ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGEA
       ;;
(FTCREGEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A27} INPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A28} OUTPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGED PGM=WAITSS     ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGED
       ;;
(FTCREGED)
       m_CondExec ${EXAGJ},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGEB
       ;;
(FTCREGEB)
       m_CondExec 16,NE,FTCREGEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGEG PGM=IDCAMS     ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGEG
       ;;
(FTCREGEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A29} INPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A30} OUTPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGEJ PGM=WAITSS     ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGEJ
       ;;
(FTCREGEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGEH
       ;;
(FTCREGEH)
       m_CondExec 16,NE,FTCREGEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGEM PGM=IDCAMS     ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGEM
       ;;
(FTCREGEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A31} INPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A32} OUTPUT ${DATA}/PXX0/FTP.F99.AVA000AG.AVA
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGEQ PGM=WAITSS     ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGEQ
       ;;
(FTCREGEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGEN
       ;;
(FTCREGEN)
       m_CondExec 16,NE,FTCREGEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A VIDE                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP FTCREGET PGM=IDCAMS     ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGET
       ;;
(FTCREGET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FICHIER DUMMY                                                        
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER DESTINATION                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F99.DUE000AG.DUE
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTCREGET.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGEU
       ;;
(FTCREGEU)
       m_CondExec 16,NE,FTCREGET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGEX PGM=IDCAMS     ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGEX
       ;;
(FTCREGEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A33} INPUT ${DATA}/PXX0/FTP.F99.DUE000AG.DUE
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A34} OUTPUT ${DATA}/PXX0/FTP.F99.DUE000AG.DUE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGFA PGM=WAITSS     ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGFA
       ;;
(FTCREGFA)
       m_CondExec ${EXAHS},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGEY
       ;;
(FTCREGEY)
       m_CondExec 16,NE,FTCREGEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRO                                                                       
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGFD PGM=IDCAMS     ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGFD
       ;;
(FTCREGFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A35} INPUT ${DATA}/PXX0/FTP.F99.DUE000AG.DUE
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g ${G_A36} OUTPUT ${DATA}/PXX0/FTP.F99.DUE000AG.DUE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/REPRO.sysin
       m_UtilityExec
# ****************************************************                         
#   WAIT POUR ATTENDRE CREATION FICHIER STEP PRECEDENT                         
# ****************************************************                         
#                                                                              
# ***********************************                                          
# *   STEP FTCREGFG PGM=WAITSS     ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=FTCREGFG
       ;;
(FTCREGFG)
       m_CondExec ${EXAIC},NE,YES 
       m_ProgramExec WAITSS "0002"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTCREGFE
       ;;
(FTCREGFE)
       m_CondExec 16,NE,FTCREGFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
