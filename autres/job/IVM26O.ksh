#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM26O.ksh                       --- VERSION DU 09/10/2016 05:43
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POIVM26 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/09/22 AT 00.10.09 BY BURTEC6                      
#    STANDARDS: P  JOBSET: IVM26O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIV125   MAJ DE LA TABLE STOCK MAGASINS RTGS30 IMPUTATIONS DES ECAR         
#                      D'INVENTAIRE                                            
# ********************************************************************         
#  REPRISE: NON APRES UNE FIN NORMALE                                          
#           RECOVER DE LA TABLE RTGS30 A PARTIR DU QUIESCE PRECEDANT           
#           ET DE LA TABLE RTAN00                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM26OA
       ;;
(IVM26OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXA98=${EXA98:-0}
# ********************************************************************         
# *    GENERATED ON MONDAY    2003/09/22 AT 00.10.09 BY BURTEC6                
# *    JOBSET INFORMATION:    NAME...: IVM26O                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK INVT'                        
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM26OAA
       ;;
(IVM26OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******  INVENTAIRE MAGASIN                                                   
#    RSIN00O  : NAME=RSIN00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00O /dev/null
# ******  LIENS INTER ARTICLES                                                 
#    RSGA58O  : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58O /dev/null
# ******  STOCKS MAGASINS                                                      
#    RSGS30O  : NAME=RSGS30O,MODE=U - DYNAM=YES                                
# -X-RSGS30O  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSGS30O /dev/null
# ******  ANOMALIES                                                            
#    RSAN00O  : NAME=RSAN00O,MODE=U - DYNAM=YES                                
# -X-RSAN00O  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00O /dev/null
# ******  SOCIETE A TRAITER   = 916                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  FICHIER DES MAJ AYANT RENDU LE STOCK DISPO NEGATIF                   
       m_FileAssign -d NEW,CATLG,DELETE -r 35 -g +1 FER125 ${DATA}/PXX0/F16.BIV125AO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV125 
       JUMP_LABEL=IVM26OAB
       ;;
(IVM26OAB)
       m_CondExec 04,GE,IVM26OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM26OZA
       ;;
(IVM26OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM26OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
