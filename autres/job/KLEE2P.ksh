#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  KLEE2P.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPKLEE2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/12/04 AT 09.27.32 BY BURTECA                      
#    STANDARDS: P  JOBSET: KLEE2P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ******************************************************************           
#  DELETE DU FICHIER PXX0.F07.KLEE2P                                           
#  REPRISE : OUI                                                               
# ******************************************************************           
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=KLEE2PA
       ;;
(KLEE2PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=KLEE2PAA
       ;;
(KLEE2PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE2PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=KLEE2PAB
       ;;
(KLEE2PAB)
       m_CondExec 16,NE,KLEE2PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BCE610 : GENERATION D'ORDRE D'UNLOAD N�1                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE2PAD PGM=BCE610     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=KLEE2PAD
       ;;
(KLEE2PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#    RSGB05   : NAME=RSGB05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSGB05 /dev/null
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FCE610O ${DATA}/PTEM/KLEE2PAD.FCE610P1
# ******  FICHIER SYSIN                                                        
       m_FileAssign -d SHR FSYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE2PAD.sysin
       m_ProgramExec BCE610 
# ********************************************************************         
#  UNLOAD N�1                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE2PAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=KLEE2PAG
       ;;
(KLEE2PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE2PAG.KLEE01P1
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SYSREC02 ${DATA}/PTEM/KLEE2PAG.KLEE02P1
       m_FileAssign -d SHR -g ${G_A1} SYSIN ${DATA}/PTEM/KLEE2PAD.FCE610P1
       
       #Untranslated utility: PTLDRIVM##Untranslate PTLDRIVM SYSIN

# ********************************************************************         
#  UNLOAD N�2                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE2PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=KLEE2PAJ
       ;;
(KLEE2PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE2PAJ.KLEE01P2
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE2PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  UNLOAD N�3                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE2PAM PGM=PTLDRIVM   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=KLEE2PAM
       ;;
(KLEE2PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 16 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE2PAM.KLEE03P1
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE2PAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  TRI POUR CREATION DU FICHER A ENVOYER                                       
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE2PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=KLEE2PAQ
       ;;
(KLEE2PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/KLEE2PAG.KLEE01P1
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/KLEE2PAG.KLEE02P1
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/KLEE2PAJ.KLEE01P2
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/KLEE2PAM.KLEE03P1
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ SORTOUT ${DATA}/PXX0.F07.KLEE2P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /FIELDS FLD_CH_16_1 16 CH 1
 /MT_INFILE_SORT SORTIN
 /REFORMAT FLD_CH_1_13,FLD_CH_16_1
 /KEYS
   FLD_CH_1_13 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 14 */
_end
       m_FileSort -s SYSIN -o SORTOUT
       JUMP_LABEL=KLEE2PAR
       ;;
(KLEE2PAR)
       m_CondExec 00,EQ,KLEE2PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   ATTENTION CE STEP DOIT TOUJOURS ETRE LE DERNIER STEP DU JOBSET             
#   AJOUT STEP POUR JALON ATOS                                                 
#   PERMET DE VERIFIER QUE LES JOBS NE SONT PAS EN RETARD                      
#   REPRISE : OUI SI PROBLEME DANS CE STEP METTRE TERMIN� SOUS PLAN _A          
#   CONDITION QUE CE SOIT LE DERNIER STEP DE LA CHAINE                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE2PAT PGM=IEBGENER   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=KLEE2PZA
       ;;
(KLEE2PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE2PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
