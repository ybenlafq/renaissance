#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM25Y.ksh                       --- VERSION DU 08/10/2016 17:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYIVM25 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/11 AT 14.29.16 BY BURTECC                      
#    STANDARDS: P  JOBSET: IVM25Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIV122 : CONTROLE ET ENRICHISSEMENT DU FICHIER ECART D'INVENTAIRE           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM25YA
       ;;
(IVM25YA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2003/03/11 AT 14.29.16 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: IVM25Y                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK INVT'                        
# *                           APPL...: IMPLYON                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM25YAA
       ;;
(IVM25YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  TABLE GENERALISEE (MAGTY)                                            
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
#                                                                              
# ******  TABLE DES RéSULTATS D'INVENTAIRES                                    
#    RSIN01   : NAME=RSIN01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN01 /dev/null
#                                                                              
# ******  SOCIETEE A TRAITER = 945                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# ******  TYPE DE TRAITEMENT = MGC CHS (MAGASINS)                              
       m_FileAssign -d SHR FTYPE ${DATA}/CORTEX4.P.MTXTFIX1/IVM25YAA
#                                                                              
# ******  EDITION DE CONTROLE                                                  
       m_OutputAssign -c X IIV122
# ******  FICHIER DES ECARTS D INVENTAIRE ENRICHI                              
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -g +1 FIV123 ${DATA}/PTEM/IVM25YAA.BIV123AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV122 
       JUMP_LABEL=IVM25YAB
       ;;
(IVM25YAB)
       m_CondExec 04,GE,IVM25YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS D'INVENTAIRE A IMPUTER                            
#           SUR CODIC SOCIETE MAGASIN S/LIEU STATUT                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM25YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM25YAD
       ;;
(IVM25YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IVM25YAA.BIV123AY
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -g +1 SORTOUT ${DATA}/PTEM/IVM25YAD.BIV123BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_11_7 11 CH 7
 /KEYS
   FLD_CH_11_7 ASCENDING,
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 25 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM25YAE
       ;;
(IVM25YAE)
       m_CondExec 00,EQ,IVM25YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV123                                                                      
# ********************************************************************         
#  MAJ DE LA TABLE D INVENTAIRE RTIN00 IMPUTATION DES ECARTS                   
#  REPRISE:  NON APRES UNE FIN NORMALE                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM25YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM25YAG
       ;;
(IVM25YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE GENERALISEE (INVDA)                                            
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  FAMILLES                                                             
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******  LIENS ARTICLES                                                       
#    RSGA58Y  : NAME=RSGA58Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58Y /dev/null
# ******  PRMP                                                                 
#    RSGG50Y  : NAME=RSGG50Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50Y /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55Y  : NAME=RSGG55Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55Y /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D INVENTAIRE PROVENANT DES 36 MAGS                
       m_FileAssign -d SHR -g ${G_A2} FIV123 ${DATA}/PTEM/IVM25YAD.BIV123BY
#                                                                              
# ******  TABLE D'INVENTAIRE MAGASINS                                          
#    RSIN00Y  : NAME=RSIN00Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00Y /dev/null
#                                                                              
# ******  SOCIETEE A TRAITER  = 945                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV123 
       JUMP_LABEL=IVM25YAH
       ;;
(IVM25YAH)
       m_CondExec 04,GE,IVM25YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM25YZA
       ;;
(IVM25YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM25YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
