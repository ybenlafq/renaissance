#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  STAT1P.ksh                       --- VERSION DU 08/10/2016 22:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSTAT1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 95/09/26 AT 10.56.50 BY BURTEC5                      
#    STANDARDS: P  JOBSET: STAT1P                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#                       EXECUTION BHV500                                       
#                       ****************                                       
#             FORMATAGE DES FICHIERS FEXTRAC POUR EDITION                      
#            *********************************************                     
# ********************************************************************         
#      PGM POUR ETAT SYNTHETIQUE :                                             
#      ETAT IHV500 DONNANT LE VOLUME + LE CA + LA MARGE POUR LE MOIS           
#     REPRISE OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=STAT1PA
       ;;
(STAT1PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+1'}
       EXAAK=${EXAAK:-0}
       G_A2=${G_A2:-'+1'}
       EXAAP=${EXAAP:-0}
       G_A3=${G_A3:-'+1'}
       EXAAU=${EXAAU:-0}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       EXAAZ=${EXAAZ:-0}
       G_A6=${G_A6:-'+1'}
       EXABE=${EXABE:-0}
       G_A7=${G_A7:-'+1'}
       EXABJ=${EXABJ:-0}
       G_A8=${G_A8:-'+1'}
       EXABO=${EXABO:-0}
       G_A9=${G_A9:-'+1'}
       G_A10=${G_A10:-'+1'}
       EXABT=${EXABT:-0}
       G_A11=${G_A11:-'+1'}
       EXABY=${EXABY:-0}
       G_A12=${G_A12:-'+1'}
       EXACD=${EXACD:-0}
       G_A13=${G_A13:-'+1'}
       EXACI=${EXACI:-0}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       EXA99=${EXA99:-0}
       JUMP_LABEL=STAT1PAA
       ;;
(STAT1PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
#    APRES LES STATS MENSUELLES                                                
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **  TABLE ARTICLES   ***                                                     
#    RTGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA00 /dev/null
# **  TABLE DES FAMILLES   ***                                                 
#    RTGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA14 /dev/null
#                                                                              
# **  FICHIER FEXTRAC : LRECL 512 :                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FHV500 ${DATA}/PXX0/STAT1PAA.BHV500AP
# **  FICHIER FEXTRAC : LRECL 512 :                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FHV505 ${DATA}/PXX0/STAT1PAA.BHV505AP
# **  FICHIER FEXTRAC : LRECL 512 :                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FHV510 ${DATA}/PXX0/STAT1PAA.BHV510AP
# ** PARAMETRE DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
# ** PARAMETRE SOCIETE                                                         
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# ******* FIC ISSU DU PGM BGV150 (CHAINE STAT0P)                               
       m_FileAssign -d SHR -g +0 FGV152 ${DATA}/PGV0/F07.GV0152AP
# ******* FIC ISSU DU PGM BGV150 (CHAINE STAT0P)                               
       m_FileAssign -d SHR -g +0 FGV153 ${DATA}/PGV0/F07.GV0153AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV500 
       JUMP_LABEL=STAT1PAB
       ;;
(STAT1PAB)
       m_CondExec 04,GE,STAT1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC (FHV500AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PAD PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PAD
       ;;
(STAT1PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/STAT1PAA.BHV500AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/STAT1PAD.BHV500BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_50_5 50 PD 5
 /FIELDS FLD_PD_29_7 29 PD 7
 /FIELDS FLD_PD_55_5 55 PD 5
 /FIELDS FLD_PD_43_7 43 PD 7
 /FIELDS FLD_PD_36_7 36 PD 7
 /FIELDS FLD_BI_7_15 7 CH 15
 /FIELDS FLD_PD_22_7 22 PD 7
 /KEYS
   FLD_BI_7_15 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_22_7,
    TOTAL FLD_PD_29_7,
    TOTAL FLD_PD_36_7,
    TOTAL FLD_PD_43_7,
    TOTAL FLD_PD_50_5,
    TOTAL FLD_PD_55_5
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT1PAE
       ;;
(STAT1PAE)
       m_CondExec 00,EQ,STAT1PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS BHV500CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PAG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PAG
       ;;
(STAT1PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A2} FEXTRAC ${DATA}/PXX0/STAT1PAD.BHV500BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/STAT1PAG.BHV500CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=STAT1PAH
       ;;
(STAT1PAH)
       m_CondExec 04,GE,STAT1PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PAJ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PAJ
       ;;
(STAT1PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/STAT1PAG.BHV500CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/STAT1PAJ.FHV500AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT1PAK
       ;;
(STAT1PAK)
       m_CondExec 00,EQ,STAT1PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IHV500                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PAM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PAM
       ;;
(STAT1PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A4} FEXTRAC ${DATA}/PXX0/STAT1PAD.BHV500BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A5} FCUMULS ${DATA}/PXX0/STAT1PAJ.FHV500AP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IHV500 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=STAT1PAN
       ;;
(STAT1PAN)
       m_CondExec 04,GE,STAT1PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  OPERATION POUR ETAT I H V 5 0 5                                           
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BHV505AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PAQ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PAQ
       ;;
(STAT1PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PXX0/STAT1PAA.BHV505AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/STAT1PAQ.BHV505BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_55_5 55 PD 5
 /FIELDS FLD_BI_15_12 15 CH 12
 /FIELDS FLD_PD_34_7 34 PD 7
 /FIELDS FLD_BI_7_3 7 CH 3
 /FIELDS FLD_PD_60_5 60 PD 5
 /FIELDS FLD_PD_27_7 27 PD 7
 /FIELDS FLD_PD_48_7 48 PD 7
 /FIELDS FLD_BI_10_5 10 CH 5
 /FIELDS FLD_PD_41_7 41 PD 7
 /KEYS
   FLD_BI_7_3 ASCENDING,
   FLD_BI_10_5 DESCENDING,
   FLD_BI_15_12 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_27_7,
    TOTAL FLD_PD_34_7,
    TOTAL FLD_PD_41_7,
    TOTAL FLD_PD_48_7,
    TOTAL FLD_PD_55_5,
    TOTAL FLD_PD_60_5
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT1PAR
       ;;
(STAT1PAR)
       m_CondExec 00,EQ,STAT1PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS BHV505CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PAT PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PAT
       ;;
(STAT1PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A7} FEXTRAC ${DATA}/PXX0/STAT1PAQ.BHV505BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/STAT1PAT.BHV505CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=STAT1PAU
       ;;
(STAT1PAU)
       m_CondExec 04,GE,STAT1PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PAX PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PAX
       ;;
(STAT1PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PXX0/STAT1PAT.BHV505CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/STAT1PAX.FHV505AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT1PAY
       ;;
(STAT1PAY)
       m_CondExec 00,EQ,STAT1PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IHV505                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PBA PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PBA
       ;;
(STAT1PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A9} FEXTRAC ${DATA}/PXX0/STAT1PAQ.BHV505BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A10} FCUMULS ${DATA}/PXX0/STAT1PAX.FHV505AP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IHV505 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=STAT1PBB
       ;;
(STAT1PBB)
       m_CondExec 04,GE,STAT1PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *  OPERATION POUR ETAT I H V 5 1 0                                           
# ********************************************************************         
#  TRI DU FICHIER FEXTRAC (BHV510AP)                                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PBD PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PBD
       ;;
(STAT1PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ********************************* FICHIER FEXTRAC                            
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/STAT1PAA.BHV510AP
# ********************************* FICHIER FEXTRAC TRIE                       
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/STAT1PBD.BHV510BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_55_5 55 PD 5
 /FIELDS FLD_PD_29_7 29 PD 7
 /FIELDS FLD_BI_7_15 7 CH 15
 /FIELDS FLD_PD_22_7 22 PD 7
 /FIELDS FLD_PD_36_7 36 PD 7
 /FIELDS FLD_PD_50_5 50 PD 5
 /FIELDS FLD_PD_43_7 43 PD 7
 /KEYS
   FLD_BI_7_15 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_22_7,
    TOTAL FLD_PD_29_7,
    TOTAL FLD_PD_36_7,
    TOTAL FLD_PD_43_7,
    TOTAL FLD_PD_50_5,
    TOTAL FLD_PD_55_5
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT1PBE
       ;;
(STAT1PBE)
       m_CondExec 00,EQ,STAT1PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
#  CREATION D'UN FICHIER FCUMULS BHV510CP                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PBG PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PBG
       ;;
(STAT1PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A12} FEXTRAC ${DATA}/PXX0/STAT1PBD.BHV510BP
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FCUMULS ${DATA}/PXX0/STAT1PBG.BHV510CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=STAT1PBH
       ;;
(STAT1PBH)
       m_CondExec 04,GE,STAT1PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PBJ PGM=SORT       **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PBJ
       ;;
(STAT1PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PXX0/STAT1PBG.BHV510CP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 SORTOUT ${DATA}/PXX0/STAT1PBJ.FHV510AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT1PBK
       ;;
(STAT1PBK)
       m_CondExec 00,EQ,STAT1PBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  CREATION DE L'ETAT : IHV510                                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT1PBM PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PBM
       ;;
(STAT1PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A14} FEXTRAC ${DATA}/PXX0/STAT1PBD.BHV510BP
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FCUMULS ${DATA}/PXX0/STAT1PBJ.FHV510AP
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IHV510 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=STAT1PBN
       ;;
(STAT1PBN)
       m_CondExec 04,GE,STAT1PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=STAT1PZA
       ;;
(STAT1PZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT1PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
