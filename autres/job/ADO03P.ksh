#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ADO03P.ksh                       --- VERSION DU 17/10/2016 18:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPADO03 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/03/01 AT 13.54.14 BY BURTECA                      
#    STANDARDS: P  JOBSET: ADO03P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='CONTROLE'                                                          
#                                                                              
# ********************************************************************         
#  EXTRACTION RTSL07                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=ADO03PA
       ;;
(ADO03PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+2'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+2'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2016/03/01 AT 13.54.14 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: ADO03P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'HADOOP '                               
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=ADO03PAA
       ;;
(ADO03PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES SAUVEGARDES DU SOIR         *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO03PAA.UNSL07AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  EXTRACTION RTGS10                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO03PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PAD
       ;;
(ADO03PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO03PAD.ZUGS10AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# **************************************************************               
#  SORT                                                                        
#  REPRISE : OUI                                                               
# **************************************************************               
#                                                                              
# ***********************************                                          
# *   STEP ADO03PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PAG
       ;;
(ADO03PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/ADO03PAA.UNSL07AP
       m_FileAssign -d NEW,CATLG,DELETE -r 88 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/ADO03PAG.UNSL07BP
# **************************************************************               
#  SORT                                                                        
#  REPRISE : OUI                                                               
# **************************************************************               
#                                                                              
# ***********************************                                          
# *   STEP ADO03PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_90 1 CH 90
 /FIELDS FLD_CH_3_88 3 CH 88
 /KEYS
   FLD_CH_1_90 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_88
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO03PAJ
       ;;
(ADO03PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/ADO03PAD.ZUGS10AP
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/ADO03PAJ.ZUGS10BP
# ***************************************************************              
# POUR INFO //DD:DD1 -n RTSL07 ET RTGS10                                       
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO03PAM PGM=EZACFSM1   ** ID=AAU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_60 3 CH 60
 /FIELDS FLD_CH_1_62 1 CH 62
 /KEYS
   FLD_CH_1_62 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_60
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO03PAM
       ;;
(ADO03PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.FTADO3AP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO03PAK
       ;;
(ADO03PAK)
       m_CondExec 00,EQ,ADO03PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO03PAQ PGM=JVMLDM76   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PAQ
       ;;
(ADO03PAQ)
       m_CondExec ${EXAAZ},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A3} DD1 ${DATA}/PTEM/ADO03PAG.UNSL07BP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 27998 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.ADO003AP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PAQ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTADO3BP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO03PAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PAT
       ;;
(ADO03PAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.FTADO3BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTADO3BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO03PAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PAX
       ;;
(ADO03PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PXX0.F07.FTADO3BP(+1),DISP=SHR                    ~         
#
       m_UtilityExec INPUT
# ***************************************************************              
# POUR INFO //DD:DD1 -n RTSL07 ET RTGS10                                       
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO03PBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PBA
       ;;
(ADO03PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PBA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A6} SYSOUT ${DATA}/PXX0/F07.FTADO3AP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO03PBD PGM=JVMLDM76   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PBD
       ;;
(ADO03PBD)
       m_CondExec ${EXABT},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A7} DD1 ${DATA}/PTEM/ADO03PAJ.ZUGS10BP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 27998 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.ADO003BP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PBD.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTADO3BP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO03PBG PGM=EZACFSM1   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PBG
       ;;
(ADO03PBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PBG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g ${G_A9} SYSOUT ${DATA}/PXX0/F07.FTADO3BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTADO3BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO03PBJ PGM=FTP        ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PBJ
       ;;
(ADO03PBJ)
       m_CondExec ${EXACD},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PBJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PXX0.F07.FTADO3BP(+2),DISP=SHR                   ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=ADO03PZA
       ;;
(ADO03PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO03PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
