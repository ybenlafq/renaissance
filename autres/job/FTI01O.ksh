#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTI01O.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POFTI01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/13 AT 10.20.18 BY BURTECA                      
#    STANDARDS: P  JOBSET: FTI01O                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   SORT DES FICHIERS ISSU DES DIFFERENTS TRAITEMENTS DE GESTION               
#   PLUS LE FICHIER           DE REPRISE DE LA VEILLE.                         
#   REPRISEE NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FTI01OA
       ;;
(FTI01OA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FTI01OAA
       ;;
(FTI01OAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# *** FICHIER CAISSES NEM DACEM ISSU DE NM002O                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGO/F16.BNM001CO
# *** FICHIER CAISSES NEM DACEM ISSU DE NM030O                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.BNM003AO
# *** FICHIER COMPTA NASL ISSU DE NASFTF   (SITE CENTRALIS�)                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.NASGCTO
# *** FICHIER MVTS SAFIG ISSU DE LA FTCREG                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.SAFIDOG
# *** FICHIER COMPTABILISATION DES MOUVEMENTS DE STOCK INTRA-SOCI�T�           
# *** FICHIER NMD (CHAINE MD0020)                                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BMD002CO
# **** FICHIER VENANT DE BS001O                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BBS002AO
# **** FICHIER VENANT DE FS052O                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGO/F16.BBS052AO
# *** FICHIER DE REPRISE EN CAS DE PLANTAGE LA VEILLE                          
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FTI01REO
# **** FICHIER VENANT DE HD001P (ASSURANCE PAR ABONNEMENT)                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.HD001PO
# **** FICHIER VENANT DE IR010P (RACHAT PRODUITS ELECTRONIQUES CORDON)         
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.BIR010PO
# **** FICHIER VENANT DE PSE50O                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.PSE050CO
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FFTI00EO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OAB
       ;;
(FTI01OAB)
       m_CondExec 00,EQ,FTI01OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI49 : DETOURAGE MGD                                                 
#   REPRISE: OUI                                                               
# ********************************************************************         
# AAF      STEP  PGM=BFTI49,LANG=CBL,RSTRT=SAME                                
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *                                                                            
# **** FICHIER DU FFTI00 TRIE                                                  
# DATMGD   FILE  NAME=FDATMGDO,MODE=I                                          
# **** FICHIER DU FFTI00 TRIE                                                  
# FICICS   FILE  NAME=FFTI00EO,MODE=I                                          
# *                                                                            
# * FICHIER EN SORTIE POUR TRI                                                 
# FICDTY   FILE  NAME=FFTI49AO,MODE=O                                          
# *                                                                            
# FICMGD   FILE  NAME=FFTI49MO,MODE=O                                          
# ********************************************************************         
#   PGM BFTI50 .GENERATIONS DES MVTS COMPTABLES FRANCHISE                      
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OAD
       ;;
(FTI01OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ** CARTE PARAM�TRE                                                           
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/FTI01O01
# ** FDATE                                                                     
       m_FileAssign -i FDATE
$FDATE
_end
# ** TABLE  EN ENTREE SANS MAJ                                                 
#    RTLI00   : NAME=RSLI00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTFM95   : NAME=RSFM95,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM95 /dev/null
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
# *** FICHIER EN ENTREE ISSU DU TRI PRECEDENT A LA PLACE DE FFTI49AO           
       m_FileAssign -d SHR -g ${G_A1} FICICS ${DATA}/PXX0/F16.FFTI00EO
# *** FICHIER EN SORTIE  (LRECL 400)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FICICD ${DATA}/PTEM/FTI01OAD.FFTI50AO
# *** FICHIER EN SORTIE  (LRECL 400)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FICICF ${DATA}/PTEM/FTI01OAD.FFTI50BO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTI50 
       JUMP_LABEL=FTI01OAE
       ;;
(FTI01OAE)
       m_CondExec 04,GE,FTI01OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER DARTY SORTANT DU BFTI50                                    
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OAG
       ;;
(FTI01OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER DU BFTI50 FRANCHISE FICICF                                       
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FTI01OAD.FFTI50AO
# *** FICHIER CONTENTIEUX CREDOR + HIDEAL ISSU DE FTCREP                       
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.CREDORBO
# *** FICHIER DE RECYCLAGE           (FTI01O J-1)                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FTRECYDO
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FFTI50DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OAH
       ;;
(FTI01OAH)
       m_CondExec 00,EQ,FTI01OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI01 .GENERATIONS DES MVTS COMPTABLES                                
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OAJ
       ;;
(FTI01OAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
       m_FileAssign -i FDATE
$FDATE
_end
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTFM55   : NAME=RSFM55O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM55 /dev/null
#    RTFM80   : NAME=RSFM80O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM80 /dev/null
#    RTFM81   : NAME=RSFM81O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM81 /dev/null
#    RTFM82   : NAME=RSFM82O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM82 /dev/null
#    RTFM83   : NAME=RSFM83O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM83 /dev/null
#    RTFM89   : NAME=RSFM89O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM89 /dev/null
#    RTFM90   : NAME=RSFM90O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM90 /dev/null
#    RTFM91   : NAME=RSFM91O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM91 /dev/null
#    RTFM92   : NAME=RSFM92O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM92 /dev/null
#    RTFM97   : NAME=RSFM97,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM97 /dev/null
#    RTFM98   : NAME=RSFM98,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM98 /dev/null
#    RTFT85   : NAME=RSFT85O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT85 /dev/null
#    RTFT86   : NAME=RSFT86O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT86 /dev/null
#    RTFT88   : NAME=RSFT88O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT88 /dev/null
#    RTGA10   : NAME=RSGA10O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  TABLE GENERALISEE: SOUS TABLES(TXTVA ENTST FEURO SDA2I)              
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                  
       m_FileAssign -d SHR -g ${G_A3} FFTI00 ${DATA}/PXX0/F16.FFTI50DO
# *** FICHIER EN SORTIE  (LRECL 200)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFV002 ${DATA}/PTEM/FTI01OAJ.FFTI01AO
# *** FICHIER DES ANOMALIES POUR TRI                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 FREPRIS ${DATA}/PTEM/FTI01OAJ.FFTI01BO
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IFTI00 ${DATA}/PTEM/FTI01OAJ.IFTI01AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTI01 
       JUMP_LABEL=FTI01OAK
       ;;
(FTI01OAK)
       m_CondExec 04,GE,FTI01OAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MERGE DU FICHIER DU BFTI50 FRANCHISE J & HISTO                             
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OAM
       ;;
(FTI01OAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER DU BFTI50 FRANCHISE FICICF                                       
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/FTI01OAD.FFTI50BO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FFTI50CO
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FFTI50CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OAN
       ;;
(FTI01OAN)
       m_CondExec 00,EQ,FTI01OAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER FFTI00CO                                                    
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OAQ
       ;;
(FTI01OAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER DU FFTI00EO                                                      
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/FTI01OAD.FFTI50AO
# *** FICHIER DU FFTI00DO TRIE                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OAQ.FFTI00DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 01 CH 15
 /FIELDS FLD_CH_233_8 233 CH 08
 /KEYS
   FLD_CH_233_8 ASCENDING,
   FLD_CH_1_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OAR
       ;;
(FTI01OAR)
       m_CondExec 00,EQ,FTI01OAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TR DU FICHIER FREPRIS ISSU DU BFTI01                                       
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OAT
       ;;
(FTI01OAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER REPRISE                                                          
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FTI01OAJ.FFTI01BO
# *** FICHIER REPRISE TRIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OAT.FFTI01CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 01 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OAU
       ;;
(FTI01OAU)
       m_CondExec 00,EQ,FTI01OAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01AO: ISSU DU PGM BFTI01                               
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OAX
       ;;
(FTI01OAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PTEM/FTI01OAJ.FFTI01AO
#  SORTIE                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OAX.FFTI01DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_15 13 CH 15
 /FIELDS FLD_CH_169_8 169 CH 08
 /KEYS
   FLD_CH_169_8 ASCENDING,
   FLD_CH_13_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OAY
       ;;
(FTI01OAY)
       m_CondExec 00,EQ,FTI01OAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI06 : CONSTITUTION DU FICHIER ANOMALIES POUR LE LENDEMAIN           
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBA PGM=BFTI06     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBA
       ;;
(FTI01OBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU PGM BFTI00 ET TRIE PRECEDEMMENT                             
# *** FICHIER DU FFTI00 TRIE                                                   
       m_FileAssign -d SHR -g ${G_A8} FFTI00 ${DATA}/PXX0/F16.FFTI50DO
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A9} FFTV02 ${DATA}/PTEM/FTI01OAX.FFTI01DO
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A10} FREPRIS ${DATA}/PTEM/FTI01OAT.FFTI01CO
#                                                                              
#  FICHIER DES ANOMALIES REPRIS LE LENDEMAIN                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI05 ${DATA}/PXX0/F16.FTRECYDO
#                                                                              
#  FICHIER EN SORTIE POUR TRI                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV05 ${DATA}/PTEM/FTI01OBA.FFTI06AO
#                                                                              
#  FICHIER EN SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PTEM/FTI01OBA.FFTI01EO
       m_ProgramExec BFTI06 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER FFTV05 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBD
       ;;
(FTI01OBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/FTI01OBA.FFTI06AO
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OBD.FFTI06BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_177_1 177 CH 01
 /FIELDS FLD_CH_1_27 01 CH 27
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OBE
       ;;
(FTI01OBE)
       m_CondExec 00,EQ,FTI01OBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBG
       ;;
(FTI01OBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/FTI01OBD.FFTI06BO
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OBG.FFTI06CO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_50 01 CH 50
 /FIELDS FLD_PD_123_8 123 PD 08
 /FIELDS FLD_CH_75_48 75 CH 48
 /FIELDS FLD_CH_57_12 57 CH 12
 /FIELDS FLD_CH_201_199 201 CH 199
 /KEYS
   FLD_CH_1_50 ASCENDING,
   FLD_CH_201_199 ASCENDING,
   FLD_CH_57_12 ASCENDING,
   FLD_CH_75_48 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_123_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OBH
       ;;
(FTI01OBH)
       m_CondExec 00,EQ,FTI01OBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI11 .TRAITEMENT DU CALCUL DES SOLDES ET CUMULS                      
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBJ PGM=BFTI11     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBJ
       ;;
(FTI01OBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A13} FFTV06 ${DATA}/PTEM/FTI01OBG.FFTI06CO
#                                                                              
#  FICHIER POUR TRI ET POUR SAP                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV06B ${DATA}/PTEM/FTI01OBJ.FFTI11AO
       m_ProgramExec BFTI11 
#                                                                              
# ********************************************************************         
#   TRI DU FICHIER FFTV05 ISSU DU PGM BFTI06                                   
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBM
       ;;
(FTI01OBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/FTI01OBA.FFTI06AO
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OBM.FFTI06DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_1_27 01 CH 27
 /FIELDS FLD_CH_177_1 177 CH 01
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OBN
       ;;
(FTI01OBN)
       m_CondExec 00,EQ,FTI01OBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER DES DEUX FICHIERS PRECEDENTS POUR FICHIER SAP              
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBQ
       ;;
(FTI01OBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PTEM/FTI01OBM.FFTI06DO
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PTEM/FTI01OBJ.FFTI11AO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.FTI01O
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_27 01 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OBR
       ;;
(FTI01OBR)
       m_CondExec 00,EQ,FTI01OBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC1 (IFTI01AO)                                          
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBT
       ;;
(FTI01OBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/FTI01OAJ.IFTI01AO
# ********* FICHIER FEXTRAC1 TRIE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OBT.IFTI01BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_143_4 143 CH 04
 /FIELDS FLD_PD_139_4 139 PD 04
 /FIELDS FLD_BI_1_138 01 CH 138
 /KEYS
   FLD_BI_1_138 ASCENDING,
   FLD_BI_143_4 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_139_4
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OBU
       ;;
(FTI01OBU)
       m_CondExec 00,EQ,FTI01OBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS IFTI01CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OBX
       ;;
(FTI01OBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A18} FEXTRAC ${DATA}/PTEM/FTI01OBT.IFTI01BO
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/FTI01OBX.IFTI01CO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=FTI01OBY
       ;;
(FTI01OBY)
       m_CondExec 04,GE,FTI01OBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OCA
       ;;
(FTI01OCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PTEM/FTI01OBX.IFTI01CO
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OCA.IFTI01DO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OCB
       ;;
(FTI01OCB)
       m_CondExec 00,EQ,FTI01OCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IFTI01                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OCD
       ;;
(FTI01OCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A20} FEXTRAC ${DATA}/PTEM/FTI01OBT.IFTI01BO
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A21} FCUMULS ${DATA}/PTEM/FTI01OCA.IFTI01DO
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDO
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IFTI01 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=FTI01OCE
       ;;
(FTI01OCE)
       m_CondExec 04,GE,FTI01OCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DES FICHIERS EN ENTREE DE L'ICS POUR EVITER LES TRAITEMENTS          
#         EN DOUBLE.                                                           
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OCG PGM=IDCAMS     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OCG
       ;;
(FTI01OCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***   FIC DE CUMUL DES INTERFACES (EN CAS DE PLANTAGE DE L'ICS)              
       m_FileAssign -d SHR IN1 /dev/null
# ***   FICHIER CUMUL REMIS A ZERO                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F16.FTI01REO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI01OCG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTI01OCH
       ;;
(FTI01OCH)
       m_CondExec 16,NE,FTI01OCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OCJ
       ;;
(FTI01OCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A22} SORTIN ${DATA}/PTEM/FTI01OBA.FFTI01EO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FTRECYEO
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI01OCJ.FFTI01FO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_59_1 59 CH 1
 /FIELDS FLD_BI_60_341 60 CH 341
 /FIELDS FLD_CH_1_15 01 CH 15
 /FIELDS FLD_CH_19_40 19 CH 40
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_19_40 ASCENDING,
   FLD_CH_59_1 DESCENDING,
   FLD_BI_60_341 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI01OCK
       ;;
(FTI01OCK)
       m_CondExec 00,EQ,FTI01OCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI15  ETALEMENT DES CHARGES                                          
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI01OCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OCM
       ;;
(FTI01OCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** PARAMETRE DATE                                                           
       m_FileAssign -i FDATE
$FDATE
_end
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTFM97   : NAME=RSFM97,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM97 /dev/null
#    RTFM94   : NAME=RSFM94,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM94 /dev/null
#    RTPS04   : NAME=RSPS04O,MODE=(U) - DYNAM=YES                              
       m_FileAssign -d SHR RTPS04 /dev/null
# ******  TABLE GENERALISEE: SOUS TABLES(TXTVA ENTST FEURO SDA2I)              
#    RTGA01   : NAME=RSGA01O,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                  
       m_FileAssign -d SHR -g ${G_A23} FFTI00 ${DATA}/PTEM/FTI01OCJ.FFTI01FO
# *** FICHIER DES ANOMALIES POUR TRI                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI05 ${DATA}/PXX0/F16.FTRECYEO
# ***                                                                          
       m_OutputAssign -c 9 -w IFTI15 IFTI15
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTI15 
       JUMP_LABEL=FTI01OCN
       ;;
(FTI01OCN)
       m_CondExec 04,GE,FTI01OCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   DEPENDANCE POUR PLAN                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FTI01OZA
       ;;
(FTI01OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI01OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
