#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  COPOKP.ksh                       --- VERSION DU 08/10/2016 22:51
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCOPOK -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 18.26.42 BY BURTEC6                      
#    STANDARDS: P  JOBSET: COPOKP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY U4I351                                           
# ********************************************************************         
# AAA      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BSIG25XX,                                                           
#      FNAME=":BSIG25DP""(0),                                                  
#      NFNAME=BSIG25DP                                                         
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY U7I351                                           
# ********************************************************************         
# AAF      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPPRO,                                                           
#      IDF=BSIG25XX,                                                           
#      FNAME=":BSIG25DP""(0),                                                  
#      NFNAME=BSIG25DP                                                         
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCOPOKP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=COPOKPA
       ;;
(COPOKPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/07/04 AT 18.26.42 BY BURTEC6                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: COPOKP                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'FIN DE PAYES'                          
# *                           APPL...: REPEXPL                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=COPOKPAA
       ;;
(COPOKPAA)
       m_CondExec ${EXAAA},NE,YES 
# ***************************************                                      
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/COPOKPAA.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP COPOKPAD PGM=EZACFSM1   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=COPOKPAD
       ;;
(COPOKPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/COPOKPAD.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY RE7 DU FTCOPOKP                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP COPOKPAG PGM=FTP        ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=COPOKPAG
       ;;
(COPOKPAG)
       m_CondExec ${EXAAK},NE,YES 
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/COPOKPAG.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE � JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP COPOKPAJ PGM=EZACFSM1   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=COPOKPAJ
       ;;
(COPOKPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/COPOKPAJ.sysin
       m_ProgramExec EZACFSM1
# ******************************************************************           
#  REMISE A ZERO DU FICHIER SIGGCTP DDNAME=FGCT                                
#                   FICHIER SIGGC2P DDNAME=FGCT                                
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP COPOKPAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=COPOKPAM
       ;;
(COPOKPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT1 ${DATA}/PNCGP/F07.SIGGCTP
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT2 ${DATA}/PNCGP/F07.SIGGC2P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/COPOKPAM.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=COPOKPAN
       ;;
(COPOKPAN)
       m_CondExec 16,NE,COPOKPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
