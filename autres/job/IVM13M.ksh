#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM13M.ksh                       --- VERSION DU 08/10/2016 15:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMIVM13 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/03/22 AT 16.47.35 BY PREPA2                       
#    STANDARDS: P  JOBSET: IVM13M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BIV600                                                                
#  ------------                                                                
#                                                                              
#  CONTROLE DE TOP 'HSPRET' POUR TRAITER LA FIN D'INVENTAIRE (EDITIONS         
#                                                                              
#      - SI LE TOP HSPRET EST A 'OO', ON PEUT TRAITER LA FIN                   
#        D'INVENTAIRE ; EDITIONS CI-DESSOUS.                                   
#      - SI LE TOP HSPRET N'EST PAS A 'OO', IL Y A UN ABEND PROVOQU�           
#        DU PGM BIV600.                                                        
#                                                                              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM13MA
       ;;
(IVM13MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVM13MAA
       ;;
(IVM13MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSIE10   : NAME=RSIE10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10 /dev/null
# ------  CONTROLE DU TOP HS DANS RTIE10                                       
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13MAA
# ------  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV600 
       JUMP_LABEL=IVM13MAB
       ;;
(IVM13MAB)
       m_CondExec 04,GE,IVM13MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIE540  : CREATION DU FICHIER DES ECARTS D'INVENTAIRE MAGASINS              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MAD
       ;;
(IVM13MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE D'INVENTAIRE                                                   
#    RSIE05M  : NAME=RSIE05M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE05M /dev/null
# ******  TABLE D'AVANCEMENT D'INVENTAIRE                                      
#    RSIE10M  : NAME=RSIE10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIE10M /dev/null
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE GENERALISEE                                                    
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  TABLE DES ETATS                                                      
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
# ******  TABLE DES PRMP LYON                                                  
#    RSGG50M  : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50M /dev/null
# ******  TABLE DES PRMP DACEM                                                 
#    RSGG55M  : NAME=RSGG55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55M /dev/null
# ******  TABLE LOGISTIQUE                                                     
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
# ******  FICHIER DES ECARTS D'INVENTAIRE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FIE540 ${DATA}/PGI989/F89.BIE540AM
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIE540 
       JUMP_LABEL=IVM13MAE
       ;;
(IVM13MAE)
       m_CondExec 04,GE,IVM13MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV130 :  CREE FIC DES ECARTS D INVENTAIRE MAGS                             
#  REPRISE:  OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MAG
       ;;
(IVM13MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE GENERALISE (INVDA)                                             
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  TABLE DES SEQUENCES FAMILLES EDITIONS (PARAMETRAGES ETATS)           
#    RSGA11M  : NAME=RSGA11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11M /dev/null
# ******  TABLE INVENTAIRE MAGASIN                                             
#    RSIN00M  : NAME=RSIN00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00M /dev/null
#                                                                              
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00M  : NAME=RSAN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSAN00M /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE MAGASIN                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 FIN130 ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV130 
       JUMP_LABEL=IVM13MAH
       ;;
(IVM13MAH)
       m_CondExec 04,GE,IVM13MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON              
#  SEQ FAMILLE                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MAJ
       ;;
(IVM13MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MAJ.BIV135AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_55_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MAK
       ;;
(IVM13MAK)
       m_CondExec 00,EQ,IVM13MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV135  : EDITION DES ECARTS FAMILLES ET SOUS-LIEU                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MAM
       ;;
(IVM13MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                         
       m_FileAssign -d SHR -g ${G_A3} FIV130 ${DATA}/PTEM/IVM13MAJ.BIV135AM
# ******  FIC D IMPRESSION ECARTS FAMILLES SOUS-LIEU                           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV135 ${DATA}/PXX0.FICHEML.IVM13M.BIV135BM
       m_OutputAssign -c Z IIV135B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV135 
       JUMP_LABEL=IVM13MAN
       ;;
(IVM13MAN)
       m_CondExec 04,GE,IVM13MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR SOCIETE MAGASIN SEQUENCE RAYON SEQUENCE FAMILLE                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MAQ
       ;;
(IVM13MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MAQ.BIV130BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MAR
       ;;
(IVM13MAR)
       m_CondExec 00,EQ,IVM13MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV136  : EDITION DES ECARTS FAMILLES PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MAT
       ;;
(IVM13MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                         
       m_FileAssign -d SHR -g ${G_A6} FIV130 ${DATA}/PTEM/IVM13MAQ.BIV130BM
# ******  FIC D IMPRESSION LIEU RAYON FAMILLE                                  
#  IIV136   REPORT SYSOUT=(9,IIV136),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV136 ${DATA}/PXX0.FICHEML.IVM13M.BIV136BM
# ******  ETAT NON VALORISE                                                    
       m_OutputAssign -c Z IIV136B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV136 
       JUMP_LABEL=IVM13MAU
       ;;
(IVM13MAU)
       m_CondExec 04,GE,IVM13MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SUR SOCIETE SEQUENCE RAYON SEQ FAMILLE            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MAX
       ;;
(IVM13MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PGI989/F89.BIV130AM
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MAX.BIV137AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MAY
       ;;
(IVM13MAY)
       m_CondExec 00,EQ,IVM13MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137  : EDITION PAR FAMILLE TOUS MAGASINS                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBA PGM=BIV137     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBA
       ;;
(IVM13MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d SHR -g ${G_A8} FIV130 ${DATA}/PTEM/IVM13MAX.BIV137AM
# ******  EDITION TOUS MAGASINS RAYON FAMILLE                                  
#  IIV137   REPORT SYSOUT=(9,IIV137),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV137 ${DATA}/PXX0.FICHEML.IVM13M.BIV137DM
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13MBA
       m_ProgramExec BIV137 
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR SOCIETE SEQUENCE RAYON SEQ FAM          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBD
       ;;
(IVM13MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MBD.BIV137BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MBE
       ;;
(IVM13MBE)
       m_CondExec 00,EQ,IVM13MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137  : EDITION DES ECARTS SOCIETE PAR FAMILLE                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBG PGM=BIV137     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBG
       ;;
(IVM13MBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A11} FIV130 ${DATA}/PTEM/IVM13MBD.BIV137BM
# ******  EDITION DES ECARTS SOCIETE PAR FAMILLE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV137 ${DATA}/PXX0.FICHEML.IVM13M.BIV137CM
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13MBG
       m_ProgramExec BIV137 
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR:                                        
#  SOCIETE SEQ FAM MARQUE CODIC LIEU SOUS-LIEU                                 
#  ET JE GARDE QUE LES ECARTS > 0                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBJ
       ;;
(IVM13MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A13} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MBJ.BIV139AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 X'00000C'
 /DERIVEDFIELD CST_3_13 X'00000D'
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_85_3 85 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_68_3 68 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_85_3 EQ CST_1_9 OR FLD_CH_85_3 EQ CST_3_13 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_68_3 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_72_3 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MBK
       ;;
(IVM13MBK)
       m_CondExec 00,EQ,IVM13MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV139  : EDITION DES ECARTS FAMILLE CODIC TOTAL CODIC                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBM PGM=BIV139     ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBM
       ;;
(IVM13MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d SHR -g ${G_A14} FIV130 ${DATA}/PTEM/IVM13MBJ.BIV139AM
# ******  EDITION DES ECARTS SOCIETE PAR FAMILLE CODIC TOTAL CODIC             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV139 ${DATA}/PXX0.FICHEML.IVM13M.BIV139BM
       m_ProgramExec BIV139 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON SEQ FAMILLE          
#  LIB RAYON LIB FAMILLE LIB MAYQUE ARTICLE                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBQ
       ;;
(IVM13MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A16} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MBQ.BIV140AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MBR
       ;;
(IVM13MBR)
       m_CondExec 00,EQ,IVM13MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140  : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU ET SOUS-LIE         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBT
       ;;
(IVM13MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A17} FIV140 ${DATA}/PTEM/IVM13MBQ.BIV140AM
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITON                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV140 ${DATA}/PXX0.FICHEML.IVM13M.BIV140BM
# ******  MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                        
       m_OutputAssign -c Z IIV140M
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV140A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=IVM13MBU
       ;;
(IVM13MBU)
       m_CondExec 04,GE,IVM13MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SEQ RAYON SEQ FAM LIB RAYON LIB FAMILLE LIB MAYQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MBX
       ;;
(IVM13MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A19} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MBX.BIV130CM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MBY
       ;;
(IVM13MBY)
       m_CondExec 00,EQ,IVM13MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV141 : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCA
       ;;
(IVM13MCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A20} FIV141 ${DATA}/PTEM/IVM13MBX.BIV130CM
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITON PAR LIEU/RAYON/FAMILLE/CODIC                                  
#  IIV141   REPORT SYSOUT=(9,IIV141),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV141 ${DATA}/PXX0.FICHEML.IVM13M.BIV141BM
# ******  MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                        
       m_OutputAssign -c Z IIV141M
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV141A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV141 
       JUMP_LABEL=IVM13MCB
       ;;
(IVM13MCB)
       m_CondExec 04,GE,IVM13MCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SUR:                                              
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCD
       ;;
(IVM13MCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PGI989/F89.BIV130AM
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MCD.BIV130DM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MCE
       ;;
(IVM13MCE)
       m_CondExec 00,EQ,IVM13MCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142  : EDITION DES ECARTS MAGASINS PRODUITS                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCG PGM=BIV142     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCG
       ;;
(IVM13MCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d SHR -g ${G_A22} FIV142 ${DATA}/PTEM/IVM13MCD.BIV130DM
# ******  EDITION RAYON/FAMILLE/CODIC                                          
#  IIV142   REPORT SYSOUT=(9,IIV142),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142 ${DATA}/PXX0.FICHEML.IVM13M.BIV142BM
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
#  IIV142A  REPORT SYSOUT=(9,IIV142A),RECFM=FBA,LRECL=133,BLKSIZE=1330         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142A ${DATA}/PXX0.FICHEML.IVM13M.BIV142EM
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13MCG
       m_ProgramExec BIV142 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCJ
       ;;
(IVM13MCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A24} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIERS DES ECARTS SOCIETE TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MCJ.BIV130EM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MCK
       ;;
(IVM13MCK)
       m_CondExec 00,EQ,IVM13MCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142  : EDITION DES ECARTS SOCIETE PRODUITS                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCM PGM=BIV142     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCM
       ;;
(IVM13MCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A25} FIV142 ${DATA}/PTEM/IVM13MCJ.BIV130EM
# ******  EDITION DES ECARTS SOCIETE RAYON/FAMILLE/CODIC                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142 ${DATA}/PXX0.FICHEML.IVM13M.BIV142CM
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV142A
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13MCM
       m_ProgramExec BIV142 
# ********************************************************************         
#  BIV142  : EDITION DES ECARTS SOCIETE PRODUITS                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCQ PGM=BIV142     ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCQ
       ;;
(IVM13MCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A26} FIV142 ${DATA}/PTEM/IVM13MCJ.BIV130EM
# ******  EDITION DES ECARTS SOCIETE RAYON/FAMILLE/CODIC                       
       m_OutputAssign -c Z IIV142
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV142A ${DATA}/PXX0.FICHEML.IVM13M.BIV142DM
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM13MCQ
       m_ProgramExec BIV142 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETEE SUR:                                     
#  SOCIETE MAG SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCT
       ;;
(IVM13MCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A27} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A28} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIERS DES ECARTS SOCIETE TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MCT.BIV143AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_55_5 55 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MCU
       ;;
(IVM13MCU)
       m_CondExec 00,EQ,IVM13MCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV143  :  EDITION DES ECARTS SOCIETE PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MCX
       ;;
(IVM13MCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A29} FIV143 ${DATA}/PTEM/IVM13MCT.BIV143AM
# ******  EDITION DES ECARTS SOCIETE PAR LIEU                                  
#  IIV143   REPORT SYSOUT=(9,IIV143),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV143 ${DATA}/PXX0.FICHEML.IVM13M.BIV143BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV143 
       JUMP_LABEL=IVM13MCY
       ;;
(IVM13MCY)
       m_CondExec 04,GE,IVM13MCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON LIB RAYON MAGASIN                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDA
       ;;
(IVM13MDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A30} SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g ${G_A31} -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIERS DES ECARTS MAGASINS TRIE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM13MDA.BIV144AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_65_3 65 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM13MDB
       ;;
(IVM13MDB)
       m_CondExec 00,EQ,IVM13MDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV144  : EDITION DES ECARTS SOCIETE PAR RAYON                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDD PGM=IKJEFT01   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDD
       ;;
(IVM13MDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A32} FIV144 ${DATA}/PTEM/IVM13MDA.BIV144AM
# ******  EDITION DES ECARTS SOCIETE PAR RAYON MAGASINS                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV144 ${DATA}/PXX0.FICHEML.IVM13M.BIV144BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV144 
       JUMP_LABEL=IVM13MDE
       ;;
(IVM13MDE)
       m_CondExec 04,GE,IVM13MDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV135)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDG PGM=IEBGENER   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDG
       ;;
(IVM13MDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV135BM
       m_OutputAssign -c 9 -w IIV135 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MDH
       ;;
(IVM13MDH)
       m_CondExec 00,EQ,IVM13MDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDJ PGM=IEBGENER   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDJ
       ;;
(IVM13MDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV137DM
       m_OutputAssign -c 9 -w IIV137 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MDK
       ;;
(IVM13MDK)
       m_CondExec 00,EQ,IVM13MDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDM PGM=IEBGENER   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDM
       ;;
(IVM13MDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV137CM
       m_OutputAssign -c 9 -w IIV137S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MDN
       ;;
(IVM13MDN)
       m_CondExec 00,EQ,IVM13MDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV139)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDQ PGM=IEBGENER   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDQ
       ;;
(IVM13MDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV139BM
       m_OutputAssign -c 9 -w IIV139 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MDR
       ;;
(IVM13MDR)
       m_CondExec 00,EQ,IVM13MDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV140)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDT PGM=IEBGENER   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDT
       ;;
(IVM13MDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV140BM
       m_OutputAssign -c 9 -w IIV140 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MDU
       ;;
(IVM13MDU)
       m_CondExec 00,EQ,IVM13MDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MDX PGM=IEBGENER   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MDX
       ;;
(IVM13MDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV142CM
       m_OutputAssign -c 9 -w IIV142S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MDY
       ;;
(IVM13MDY)
       m_CondExec 00,EQ,IVM13MDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142SA)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MEA
       ;;
(IVM13MEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV142DM
       m_OutputAssign -c 9 -w IIV142SA SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MEB
       ;;
(IVM13MEB)
       m_CondExec 00,EQ,IVM13MEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV144)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MED PGM=IEBGENER   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MED
       ;;
(IVM13MED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV144BM
       m_OutputAssign -c 9 -w IIV144 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MEE
       ;;
(IVM13MEE)
       m_CondExec 00,EQ,IVM13MED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV136)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MEG PGM=IEBGENER   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MEG
       ;;
(IVM13MEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV136BM
       m_OutputAssign -c 9 -w IIV136 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MEH
       ;;
(IVM13MEH)
       m_CondExec 00,EQ,IVM13MEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV141)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MEJ PGM=IEBGENER   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MEJ
       ;;
(IVM13MEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV141BM
       m_OutputAssign -c 9 -w IIV141 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MEK
       ;;
(IVM13MEK)
       m_CondExec 00,EQ,IVM13MEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MEM PGM=IEBGENER   ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MEM
       ;;
(IVM13MEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV142BM
       m_OutputAssign -c 9 -w IIV142 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MEN
       ;;
(IVM13MEN)
       m_CondExec 00,EQ,IVM13MEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142A)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MEQ PGM=IEBGENER   ** ID=AHD                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MEQ
       ;;
(IVM13MEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV142EM
       m_OutputAssign -c 9 -w IIV142A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MER
       ;;
(IVM13MER)
       m_CondExec 00,EQ,IVM13MEQ ${EXAHD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV143)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MET PGM=IEBGENER   ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MET
       ;;
(IVM13MET)
       m_CondExec ${EXAHI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM13M.BIV143BM
       m_OutputAssign -c 9 -w IIV143 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MEU
       ;;
(IVM13MEU)
       m_CondExec 00,EQ,IVM13MET ${EXAHI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  DELETE DU FICHIER DES REMONTEES PXX0.F89.BIE501AM                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM13MEX PGM=IDCAMS     ** ID=AHN                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MEX
       ;;
(IVM13MEX)
       m_CondExec ${EXAHN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/F89.BIE501AM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM13MEX.sysin
       m_UtilityExec
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=IVM13MEY
       ;;
(IVM13MEY)
       m_CondExec 16,NE,IVM13MEX ${EXAHN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM13MZA
       ;;
(IVM13MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM13MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
