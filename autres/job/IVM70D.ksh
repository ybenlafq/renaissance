#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM70D.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDIVM70 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/17 AT 14.57.47 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVM70D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BIV730  : CREE FIC DES ECARTS D INVENTAIRE MAGS                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM70DA
       ;;
(IVM70DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVM70DAA
       ;;
(IVM70DAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
#    FACULTATIF POUR OPTIMISATION PLAN *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ********                                                                     
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  TABLE DES ARTICLES                                                   
#    RSGA00   : NAME=RSGA00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISE (INVDA)                                             
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  TABLE DES SEQUENCES FAMILLES EDITIONS (PARAMETRAGES ETATS)           
#    RSGA11   : NAME=RSGA11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11 /dev/null
# ******  TABLE INVENTAIRE MAGASIN                                             
#    RSIN00   : NAME=RSIN00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00 /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE MAGASIN                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FIN130 ${DATA}/PTEM/IVM70DAA.TIV130FD
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00Y  : NAME=RSAN00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00Y /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV730 
       JUMP_LABEL=IVM70DAB
       ;;
(IVM70DAB)
       m_CondExec 04,GE,IVM70DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS MAGS SUR :                                        
#  SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON SEQ FAMILLE          
#  LIB RAYON LIB FAMILLE LIB MARQUE ARTICLE                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DAD
       ;;
(IVM70DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IVM70DAA.TIV130FD
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM70DAD.TIV140BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_75_3 75 PD 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_75_3,
    TOTAL FLD_PD_85_3
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM70DAE
       ;;
(IVM70DAE)
       m_CondExec 00,EQ,IVM70DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS MAGS SUR :                                        
#  SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON SEQ FAMILLE          
#  LIB RAYON LIB FAMILLE LIB MARQUE ARTICLE                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DAG
       ;;
(IVM70DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/IVM70DAD.TIV140BD
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM70DAG.TIV140CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 0
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /CONDITION CND_1 FLD_PD_85_3 EQ CST_1_9 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM70DAH
       ;;
(IVM70DAH)
       m_CondExec 00,EQ,IVM70DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140  : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU ET SOUS-LIE         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DAJ
       ;;
(IVM70DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A3} FIV140 ${DATA}/PTEM/IVM70DAG.TIV140CD
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION                                                              
       m_OutputAssign -c Z IIV140
# ******  MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                        
       m_OutputAssign -c 9 -w IIV140M IIV140M
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV140A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=IVM70DAK
       ;;
(IVM70DAK)
       m_CondExec 04,GE,IVM70DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR SOCIETE MAGASIN SEQUENCE RAYON SEQUENCE FAMILLE                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DAM
       ;;
(IVM70DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/IVM70DAA.TIV130FD
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM70DAM.TV0136BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM70DAN
       ;;
(IVM70DAN)
       m_CondExec 00,EQ,IVM70DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV136  : EDITION DES ECARTS FAMILLES PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DAQ
       ;;
(IVM70DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                         
       m_FileAssign -d SHR -g ${G_A5} FIV130 ${DATA}/PTEM/IVM70DAM.TV0136BD
# ******  FIC D IMPRESSION LIEU RAYON FAMILLE                                  
       m_OutputAssign -c 9 -w IIV136 IIV136
# ******  ETAT NON VALORISE                                                    
       m_OutputAssign -c Z IIV136B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV136 
       JUMP_LABEL=IVM70DAR
       ;;
(IVM70DAR)
       m_CondExec 04,GE,IVM70DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SUR SOCIETE SEQUENCE RAYON SEQ FAMILLE            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DAT
       ;;
(IVM70DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/IVM70DAA.TIV130FD
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM70DAT.TIV137XD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM70DAU
       ;;
(IVM70DAU)
       m_CondExec 00,EQ,IVM70DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137  : EDITION PAR FAMILLE TOUS MAGASINS                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DAX PGM=BIV137     ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DAX
       ;;
(IVM70DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d SHR -g ${G_A7} FIV130 ${DATA}/PTEM/IVM70DAT.TIV137XD
# ******  EDITION TOUS MAGASINS RAYON FAMILLE                                  
       m_OutputAssign -c 9 -w IIV137 IIV137
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM70DAX
       m_ProgramExec BIV137 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON LIB RAYON MAGASIN                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DBA
       ;;
(IVM70DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/IVM70DAA.TIV130FD
# ******  FICHIERS DES ECARTS MAGASINS TRIE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM70DBA.TIV144XD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_65_3 65 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM70DBB
       ;;
(IVM70DBB)
       m_CondExec 00,EQ,IVM70DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV144  : EDITION DES ECARTS SOCIETE PAR RAYON                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DBD
       ;;
(IVM70DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A9} FIV144 ${DATA}/PTEM/IVM70DBA.TIV144XD
# ******  EDITION DES ECARTS SOCIETE PAR RAYON MAGASINS                        
       m_OutputAssign -c 9 -w IIV144 IIV144
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV144 
       JUMP_LABEL=IVM70DBE
       ;;
(IVM70DBE)
       m_CondExec 04,GE,IVM70DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS MAGASINS                                          
#  SOCIETE MAGASIN SEQ RAYON SEQ FAM LIB RAYON LIB FAMILLE LIB MAYQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DBG
       ;;
(IVM70DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/IVM70DAA.TIV130FD
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM70DBG.TIV141XD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_PD_75_3 75 PD 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_75_3,
    TOTAL FLD_PD_85_3
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM70DBH
       ;;
(IVM70DBH)
       m_CondExec 00,EQ,IVM70DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS MAGASINS                                          
#  SOCIETE MAGASIN SEQ RAYON SEQ FAM LIB RAYON LIB FAMILLE LIB MAYQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DBJ
       ;;
(IVM70DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/IVM70DBG.TIV141XD
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM70DBJ.TIV141WD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 0
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_PD_85_3 85 PD 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /CONDITION CND_1 FLD_PD_85_3 EQ CST_1_9 
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM70DBK
       ;;
(IVM70DBK)
       m_CondExec 00,EQ,IVM70DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV141 : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM70DBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DBM
       ;;
(IVM70DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A12} FIV141 ${DATA}/PTEM/IVM70DBJ.TIV141WD
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITON PAR LIEU/RAYON/FAMILLE/CODIC                                  
       m_OutputAssign -c 9 -w IIV141M IIV141M
# ******  MEME ETAT MAIS VALORISE POUR LES MAGASINS                            
       m_OutputAssign -c Z IIV141
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV141A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV141 
       JUMP_LABEL=IVM70DBN
       ;;
(IVM70DBN)
       m_CondExec 04,GE,IVM70DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM70DZA
       ;;
(IVM70DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM70DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
