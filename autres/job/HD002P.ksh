#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HD002P.ksh                       --- VERSION DU 17/10/2016 18:29
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPHD002 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/06 AT 11.24.26 BY BURTEC2                      
#    STANDARDS: P  JOBSET: HD002P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TEST DU FICHIER VIDE SI FICHIER VIDE LE RESTE NE S'EX�CUTE PAS              
#          FICHIER PLEIN UNE FOIS DANS LE MOIS                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HD002PA
       ;;
(HD002PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       RAAA=${RAAA:-HD002PAA}
       RUN=${RUN}
       JUMP_LABEL=HD002PAA
       ;;
(HD002PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/FTP.F07.HDECHEAN
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HD002PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=HD002PAB
       ;;
(HD002PAB)
       m_CondExec 16,NE,HD002PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORCAGE DU STATUT SOUS PLAN . MISE TERMINE LA CHAINE EN CAS DE              
#  FICHIER VIDE                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD002PAD PGM=CZX2PTRT   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=HD002PAG
       ;;
(HD002PAG)
       m_CondExec ${EXAAK},NE,YES 3,EQ,$[RAAA] 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE                                                                
#    RSRB20   : NAME=RSRB20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB20 /dev/null
#                                                                              
# ******* FDATE SOUS LA FORM JJMMSSAA                                          
       m_FileAssign -i FDATE
$FDATE
_end
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g +0 FHDEAL ${DATA}/PXX0/FTP.F07.HDECHEAN
# ******* UN FICHIER POUR CHAQUE SOCIETE A TRAITER                             
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FBHD10 ${DATA}/PXX0/F07.BHD010PP
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FBHD11 ${DATA}/PXX0/F16.BHD010PO
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FBHD12 ${DATA}/PXX0/F45.BHD010PY
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FBHD13 ${DATA}/PXX0/F61.BHD010PL
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FBHD14 ${DATA}/PXX0/F91.BHD010PD
# *****   ETAT D'ANOMALIE                                                      
       m_OutputAssign -c 9 -w BHD010 FANO01
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHD010 
       JUMP_LABEL=HD002PAH
       ;;
(HD002PAH)
       m_CondExec 04,GE,HD002PAG ${EXAAK},NE,YES 3,EQ,$[RAAA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES FICHIERS                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAP      STEP  PGM=PKZIP,COND1=(3,EQ,_AAAA)                                   
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=BHD010PP,MODE=I                                          
# DD2      FILE  NAME=BHD010PO,MODE=I                                          
# DD3      FILE  NAME=BHD010PY,MODE=I                                          
# DD4      FILE  NAME=BHD010PL,MODE=I                                          
# DD5      FILE  NAME=BHD010PD,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=BHD010AP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -INFILE(DD2)                                                                
#  -INFILE(DD3)                                                                
#  -INFILE(DD4)                                                                
#  -INFILE(DD5)                                                                
#  -ZIPPED_DSN(PXX0.F07.BHD010PP.*,ECHEANCIER_PAR_ASS_NOMADE_907.CSV)          
#  -ZIPPED_DSN(PXX0.F16.BHD010PO.*,ECHEANCIER_PAR_ASS_NOMADE_916.CSV)          
#  -ZIPPED_DSN(PXX0.F45.BHD010PY.*,ECHEANCIER_PAR_ASS_NOMADE_945.CSV)          
#  -ZIPPED_DSN(PXX0.F61.BHD010PL.*,ECHEANCIER_PAR_ASS_NOMADE_961.CSV)          
#  -ZIPPED_DSN(PXX0.F91.BHD010PD.*,ECHEANCIER_PAR_ASS_NOMADE_991.CSV)          
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD002PAJ PGM=JVMLDM76   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=HD002PAJ
       ;;
(HD002PAJ)
       m_CondExec ${EXAAP},NE,YES 3,EQ,$[RAAA] 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F07.BHD010PP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 230 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.BHD010AP.ZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HD002PAJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  TRANSFERT VERS XFB-GATEWAY VERS LE GROUPE ABELBOX                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=IEFBR14,PATTERN=CFT,COND1=(3,EQ,_AAAA)                     
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=BHD010AP                                                            
#          DATAEND                                                             
#                                                                              
# ********************************************************************         
#  REMISE A ZERO                                                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD002PAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=HD002PAM
       ;;
(HD002PAM)
       m_CondExec ${EXAAU},NE,YES 3,EQ,$[RAAA] 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 200 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/FTP.F07.HDECHEAN
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HD002PAM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=HD002PAN
       ;;
(HD002PAN)
       m_CondExec 16,NE,HD002PAM ${EXAAU},NE,YES 3,EQ,$[RAAA] 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTHD002P                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD002PAQ PGM=EZACFSM1   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=HD002PAQ
       ;;
(HD002PAQ)
       m_CondExec ${EXAAZ},NE,YES 3,EQ,$[RAAA] 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HD002PAQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/HD002PAQ.FTHD002P
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTHD002P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD002PAT PGM=FTP        ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=HD002PAT
       ;;
(HD002PAT)
       m_CondExec ${EXABE},NE,YES 3,EQ,$[RAAA] 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/HD002PAT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.HD002PAQ.FTHD002P(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=HD002PZA
       ;;
(HD002PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HD002PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
