#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  KLEE5P.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPKLEE5 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/05/07 AT 10.33.49 BY BURTECA                      
#    STANDARDS: P  JOBSET: KLEE5P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *******************************************************************          
#  TRI DU FICHIER BCE652AP ISSU DE KLEE3P                                      
#  REPRISE : OUI                                                               
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=KLEE5PA
       ;;
(KLEE5PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=KLEE5PAA
       ;;
(KLEE5PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BCE652AP
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEEBPAA.BCE652JP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 07 CH 07
 /KEYS
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE5PAB
       ;;
(KLEE5PAB)
       m_CondExec 00,EQ,KLEE5PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BCE613 : COMPARAISON DU REFERENTIEL                                         
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PAD
       ;;
(KLEE5PAD)
       m_CondExec ${EXAAF},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#                                                                              
# *   FICHIER ISSU DE KLEE3P TRIE                                              
       m_FileAssign -d SHR -g ${G_A1} FSTRUCT ${DATA}/PTEM/KLEEBPAA.BCE652JP
# *   FICHIER ISSU DE KLEE4P                                                   
       m_FileAssign -d SHR -g +0 FCODICS ${DATA}/PXX0/F07.BCE611BP
# *   FICHIER ISSU DE KLEE4P (-1)                                              
       m_FileAssign -d SHR -g -1 FREFJ1 ${DATA}/PXX0/F07.BCE612CP
# *   FICHIER ISSU DE KLEE4P                                                   
       m_FileAssign -d SHR -g +0 FREFJ ${DATA}/PXX0/F07.BCE612CP
# *   FICHIER EN SORTIE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FCE613 ${DATA}/PTEM/KLEEBPAD.BCE613AP
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FCE613F ${DATA}/PTEM/KLEEBPAD.BCE613FP
# *   FICHIER DATE                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCE613 
       JUMP_LABEL=KLEE5PAE
       ;;
(KLEE5PAE)
       m_CondExec 04,GE,KLEE5PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BCE652AP ISSU DE KLEE3P                                      
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PAG
       ;;
(KLEE5PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BCE652AP
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEEBPAG.BCE652BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 01 CH 06
 /FIELDS FLD_CH_55_7 55 CH 07
 /FIELDS FLD_CH_7_7 07 CH 07
 /KEYS
   FLD_CH_55_7 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE5PAH
       ;;
(KLEE5PAH)
       m_CondExec 00,EQ,KLEE5PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER BCE652AP J-1 ISSU DE KLEE3P                                  
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PAJ
       ;;
(KLEE5PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g -1 SORTIN ${DATA}/PXX0/F07.BCE652AP
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEEBPAJ.BCE652CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_7 55 CH 07
 /FIELDS FLD_CH_1_6 01 CH 06
 /FIELDS FLD_CH_7_7 07 CH 07
 /KEYS
   FLD_CH_55_7 ASCENDING,
   FLD_CH_1_6 ASCENDING,
   FLD_CH_7_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE5PAK
       ;;
(KLEE5PAK)
       m_CondExec 00,EQ,KLEE5PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCE613                                                       
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PAM
       ;;
(KLEE5PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/KLEEBPAD.BCE613FP
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEEBPAM.BCE613BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE5PAN
       ;;
(KLEE5PAN)
       m_CondExec 00,EQ,KLEE5PAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BCE614 : TRAITEMENT DES CODICS GSM                                          
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PAQ
       ;;
(KLEE5PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A3} FCE613I ${DATA}/PTEM/KLEEBPAM.BCE613BP
# *   FICHIER J-1 TRIE ISSU DE KLEE3P                                          
       m_FileAssign -d SHR -g ${G_A4} FSTRUCJ1 ${DATA}/PTEM/KLEEBPAJ.BCE652CP
# *   FICHIER TRIE ISSU DE KLEE3P                                              
       m_FileAssign -d SHR -g ${G_A5} FSTRUCJ ${DATA}/PTEM/KLEEBPAG.BCE652BP
# *                                                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 FCE613O ${DATA}/PTEM/KLEEBPAQ.BCE614AP
# *   FICHIER DATE                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCE614 
       JUMP_LABEL=KLEE5PAR
       ;;
(KLEE5PAR)
       m_CondExec 04,GE,KLEE5PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER PRIX                                                         
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PAT
       ;;
(KLEE5PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *                                                                            
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/KLEEBPAQ.BCE614AP
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/KLEEBPAD.BCE613AP
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEEBPAT.BCE614BP
# *                                                                            
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 01 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE5PAU
       ;;
(KLEE5PAU)
       m_CondExec 00,EQ,KLEE5PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BCE615 : CONSTITUTION DU PANIER                                             
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PAX
       ;;
(KLEE5PAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA30 /dev/null
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FCE614 ${DATA}/PTEM/KLEEBPAT.BCE614BP
# *   FICHIER ISSU DE KLEE4P                                                   
       m_FileAssign -d SHR -g +0 FCODICS ${DATA}/PXX0/F07.BCE611BP
# *   FICHIER ALLANT VERS KLEE6P POUR ENVOI VIA LA GATEWAY                     
       m_FileAssign -d NEW,CATLG,DELETE -r 29 -t LSEQ -g +1 FETQPAN ${DATA}/PXX0/F07.BCE615AP
# *   FICHIER DATE                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******************************************************************           
#  DELETE DES FICHIERS PXX0.F07.KLEEZIP-KLEE7P-KLEE4P                          
#  REPRISE : OUI                                                               
# ******************************************************************           
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PBA PGM=IDCAMS     ** ID=ABO                                   
# ***********************************                                          

       m_ProgramExec -b BCE615 
       JUMP_LABEL=KLEE5PBA
       ;;
(KLEE5PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE5PBA.sysin
       m_UtilityExec
# ******************************************************************           
#  COPIE FICHIER KLEE3P AVANT ZIP                                              
#  REPRISE : NON IL FAUT REPRENDRE AU 1ER STEP                                 
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=KLEE5PBB
       ;;
(KLEE5PBB)
       m_CondExec 16,NE,KLEE5PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PBD PGM=IDCAMS     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PBD
       ;;
(KLEE5PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g +0 IN1 ${DATA}/PXX0/F07.BCE652OP
       m_FileAssign -d SHR -g +0 IN2 ${DATA}/PXX0/F07.BCE612CP
       m_FileAssign -d SHR -g ${G_A9} IN3 ${DATA}/PXX0/F07.BCE615AP
       m_FileAssign -d SHR -g +0 IN4 ${DATA}/PXX0/F07.FCE652DP
# **      FICHIER ALLANT VERS KLEE6P POUR ZIP                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 92 -t LSEQ OUT1 ${DATA}/PXX0.F07.KLEE3P
       m_FileAssign -d NEW,CATLG,DELETE -r 91 -t LSEQ OUT2 ${DATA}/PXX0.F07.KLEE4P
       m_FileAssign -d NEW,CATLG,DELETE -r 29 -t LSEQ OUT3 ${DATA}/PXX0.F07.KLEE5P
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ OUT4 ${DATA}/PXX0.F07.KLEE7P1
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE5PBD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=KLEE5PBE
       ;;
(KLEE5PBE)
       m_CondExec 16,NE,KLEE5PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP DES FICHIER KLEE0 KLEE1 KLEE2 KLEE7 KLEE4                               
#  REPRISE : NON IL FAUT REPRENDRE AU 1ER STEP                                 
#  SAVE ANCIEN PKZIP SI PB                                                     
# ******************************************************************           
# ABY      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -ARCHIVE(":KLEEZIPS"")                                                      
#  -ARCHIVE_SPACE_PRIMARY(00001000)                                            
#  -ARCHIVE_SPACE_SECONDARY(00000100)                                          
#  -ARCHUNIT(TEM350)                                                           
#  -FILE_TERMINATOR()                                                          
#  -ZIPPED_DSN(":KLEE0P"",KLEE0_MARQUE.TXT)                                    
#  -ZIPPED_DSN(":KLEE1P"",KLEE1_MAGASIN.TXT)                                   
#  -ZIPPED_DSN(":KLEE3P"",KLEE3_STOCK.TXT)                                     
#  -ZIPPED_DSN(":KLEE7P1"",KLEE2_ASSORT.TXT)                                   
#  -ZIPPED_DSN(":KLEE4P"",KLEE4_REFPRIX.TXT)                                   
#  -ZIPPED_DSN(":KLEE5P"",KLEE5_PANPRIX.TXT)                                   
#  ":KLEE0P""                                                                  
#  ":KLEE1P""                                                                  
#  ":KLEE3P""                                                                  
#  ":KLEE7P1""                                                                 
#  ":KLEE4P""                                                                  
#  ":KLEE5P""                                                                  
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PBG PGM=JVMLDM76   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PBG
       ;;
(KLEE5PBG)
       m_CondExec ${EXABY},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 256 -t LSEQ FICZIP ${DATA}/PXX0.F07.KLEEZIPS
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE5PBG.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ZIP DES FICHIER KLEE0 KLEE1 KLEE2 KLEE7 KLEE4                               
#  REPRISE : NON IL FAUT REPRENDRE AU 1ER STEP                                 
# ******************************************************************           
# ACD      STEP  PGM=PKZIP                                                     
# //STEPLIB  DD DSN=SYS2.ZIP390.LOADLIB,DISP=SHR                               
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -ARCHIVE(":KLEEZIP"")                                                       
#  -ARCHIVE_SPACE_PRIMARY(00001000)                                            
#  -ARCHIVE_SPACE_SECONDARY(00000100)                                          
#  -ARCHUNIT(TEM350)                                                           
#  -FILE_TERMINATOR()                                                          
#  -ZIPPED_DSN(":KLEE0P"",KLEE0_MARQUE.TXT)                                    
#  -ZIPPED_DSN(":KLEE1P"",KLEE1_MAGASIN.TXT)                                   
#  -ZIPPED_DSN(":KLEE3P"",KLEE3_STOCK.TXT)                                     
#  -ZIPPED_DSN(":KLEE7P1"",KLEE2_ASSORT.TXT)                                   
#  -ZIPPED_DSN(":KLEE4P"",KLEE4_REFPRIX.TXT)                                   
#  -ZIPPED_DSN(":KLEE5P"",KLEE5_PANPRIX.TXT)                                   
#  ":KLEE0P""                                                                  
#  ":KLEE1P""                                                                  
#  ":KLEE3P""                                                                  
#  ":KLEE7P1""                                                                 
#  ":KLEE4P""                                                                  
#  ":KLEE5P""                                                                  
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PBJ PGM=JVMLDM76   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PBJ
       ;;
(KLEE5PBJ)
       m_CondExec ${EXACD},NE,YES 
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 256 -t LSEQ FICZIP ${DATA}/PXX0.F07.KLEEZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE5PBJ.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTKLEE6P                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE5PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PBM
       ;;
(KLEE5PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE5PBM.sysin
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=KLEE5PZA
       ;;
(KLEE5PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE5PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
