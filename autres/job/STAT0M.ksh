#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  STAT0M.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMSTAT0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/07/05 AT 10.46.06 BY BURTEC6                      
#    STANDARDS: P  JOBSET: STAT0M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLES RTHV06-RTVA05-RTGG50                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=STAT0MA
       ;;
(STAT0MA)
#
#STAT0MAA
#STAT0MAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#STAT0MAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=STAT0MAD
       ;;
(STAT0MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BHV030AM
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0MAD.BVA500AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_70_8 70 CH 8
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_70_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0MAE
       ;;
(STAT0MAE)
       m_CondExec 00,EQ,STAT0MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA500                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MAG
       ;;
(STAT0MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA00   : NAME=RSVA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTGG70   : NAME=RSGG70M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PTEM/STAT0MAD.BVA500AM
# *****   FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 FVA00 ${DATA}/PTEM/STAT0MAG.FVA000AM
# *****   FICHIER DES ANOMALIES ENTRANT DANS LE GENERATEUR D'ETAT              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FVA500 ${DATA}/PXX0/F89.FVA500AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA500 
       JUMP_LABEL=STAT0MAH
       ;;
(STAT0MAH)
       m_CondExec 04,GE,STAT0MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA000A POUR CREATION FICHIER ENTRANT DANS BGG500  *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MAJ
       ;;
(STAT0MAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/STAT0MAG.FVA000AM
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/STAT0MAJ.FVA000BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "989"
 /DERIVEDFIELD CST_3_13 "P"
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /CONDITION CND_2 FLD_CH_8_3 EQ CST_1_9 AND FLD_CH_14_1 EQ CST_3_13 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_48_8,FLD_CH_36_6,FLD_CH_42_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0MAK
       ;;
(STAT0MAK)
       m_CondExec 00,EQ,STAT0MAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG500                                                                
# ********************************************************************         
#  RECALCUL DES PRMPS DU MOIS                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MAM
       ;;
(STAT0MAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FVA00 ${DATA}/PTEM/STAT0MAJ.FVA000BM
# ******  TABLES EN ENTREE                                                     
#    RTVA05   : NAME=RSVA05M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FGG500 ${DATA}/PTEM/STAT0MAM.FGG500AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG500 
       JUMP_LABEL=STAT0MAN
       ;;
(STAT0MAN)
       m_CondExec 04,GE,STAT0MAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FGG500AM                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MAQ
       ;;
(STAT0MAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/STAT0MAM.FGG500AM
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PTEM/STAT0MAQ.FGG500BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_19_12 19 CH 12
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_4_7,FLD_CH_11_8,FLD_CH_19_12
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0MAR
       ;;
(STAT0MAR)
       m_CondExec 00,EQ,STAT0MAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG505                                                                
# ********************************************************************         
#  MAJ DES PRMPS APRES RECALCUL                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MAT
       ;;
(STAT0MAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG70   : NAME=RSGG70M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGA10   : NAME=RSGA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER EN ENTREE ISSU DU TRI PRECEDENT                              
       m_FileAssign -d SHR -g ${G_A5} RTVA00I ${DATA}/PTEM/STAT0MAG.FVA000AM
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER DES PRA ENTRANT DANS LE GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGG505 ${DATA}/PXX0/F89.FGG505AM
# ******  FICHIER D'EXTRACTION ISSU DU BGG500                                  
       m_FileAssign -d SHR -g ${G_A6} FGG500 ${DATA}/PTEM/STAT0MAQ.FGG500BM
# ******  FICHIER POUR MAJ DE RTGA67 (GG506P)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGA67 ${DATA}/PNCGM/F89.FGA67AM
# ******  FICHIER ENTRANT DANS ,VA505 ET SERVANT A LOADER RTVA00               
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 RTVA00S ${DATA}/PXX0/F89.FVA00AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG505 
       JUMP_LABEL=STAT0MAU
       ;;
(STAT0MAU)
       m_CondExec 04,GE,STAT0MAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA00A POUR CREATION FICHIER BVA505A ENTRANT                 
#  DANS LE PROG BVA505                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MAX
       ;;
(STAT0MAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F89.FVA00AM
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PTEM/STAT0MAX.BVA505AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "P"
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_76_8 76 CH 8
 /FIELDS FLD_PD_76_8 76 PD 8
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_68_8 68 CH 8
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_PD_68_8 68 PD 8
 /FIELDS FLD_PD_36_6 36 PD 6
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_11 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6,
    TOTAL FLD_PD_68_8,
    TOTAL FLD_PD_76_8
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_11_3,FLD_CH_8_3,FLD_CH_36_6,FLD_CH_68_8,FLD_CH_42_6,FLD_CH_76_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0MAY
       ;;
(STAT0MAY)
       m_CondExec 00,EQ,STAT0MAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA505                                                                
# ********************************************************************         
#  MAJ DE LA RTVA05 POUR METTRE A JOUR EN QUANTITE ET EN VALEUR LES            
#  STOCKS,ET CREATION D'UN FICHIER BVA506A POUR MAJ DE LA TABLE                
#  RTGG50 DANS LA PGM BVA506                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBA
       ;;
(STAT0MBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG55   : NAME=RSGG55M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTVA10   : NAME=RSVA10M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA10 /dev/null
#    RTVA15   : NAME=RSVA15M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA15 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTVA05   : NAME=RSVA05M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGG50   : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A8} FVA505 ${DATA}/PTEM/STAT0MAX.BVA505AM
# ******  FICHIER SERVANT A LOADER LE TABLE RTVA15                             
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -g +1 FVA515 ${DATA}/PXX0/F89.FVA15AM
# ******  FICHIER ENTRANT DANS VA001R ET SERVANT A LOADER RTVA30               
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 RTVA30 ${DATA}/PXX0/F89.FVA30AM
# ******  FICHIER SERVANT A LA MAJ DE LA RTGG50 DANS LE BVA506                 
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -g +1 FGG500 ${DATA}/PTEM/STAT0MBA.BVA506AM
# ******  FICHIER A HISTORISER QUI PERMET DE LOADER LA TABLE RTVA10            
# *****   FICHIER PARAMETRE 'M' POUR TRAIT. MENSUEL                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0MBA
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA505 
       JUMP_LABEL=STAT0MBB
       ;;
(STAT0MBB)
       m_CondExec 04,GE,STAT0MBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA506                                                                
# ********************************************************************         
#  MAJ DE LA RTGG50 POUR RECALCUL DES PRMS A PARTIR DU FICHIER                 
#  BVA506AR ISSU DU BVA505                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBD
       ;;
(STAT0MBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU BVA505                                               
       m_FileAssign -d SHR -g ${G_A9} FGG500 ${DATA}/PTEM/STAT0MBA.BVA506AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA506 
       JUMP_LABEL=STAT0MBE
       ;;
(STAT0MBE)
       m_CondExec 04,GE,STAT0MBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LGT 15.01.2001 : EVITE RELABEL TT FILIALES                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBG PGM=IEFBR14    ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBG
       ;;
(STAT0MBG)
       m_CondExec ${EXABY},NE,YES 
       m_ProgramExec IEFBR14 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030A                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBJ
       ;;
(STAT0MBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BHV030AM
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0MBJ.HV0000BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_9 "VEN"
 /DERIVEDFIELD CST_1_5 "VEN"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_5 OR FLD_CH_13_3 EQ CST_3_9 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_30_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0MBK
       ;;
(STAT0MBK)
       m_CondExec 00,EQ,STAT0MBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV150                                                                
# ********************************************************************         
#  CREATION DES FICHIERS DE LOAD DE LA RTHV09 ET RTHV10 ET DES FICHIER         
#  CUMULS FHV01                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBM
       ;;
(STAT0MBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c 9 -w BGV150 SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN ENTREE                                                      
#    RTGA00   : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG50   : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGG55   : NAME=RSGG55M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
# ******* FICHIER HISTO FHV01 (ARTICLE DARTY)                                  
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PGV0/F89.HV01AM
# ******* FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A10} SV0404 ${DATA}/PTEM/STAT0MBJ.HV0000BM
# ******* FIC DES VTES DU MOIS PAR ARTICLES AYANT PU ETRE RECYCLE              
# *******               (LOAD DE LA RTHV10)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGV150 ${DATA}/PGV0/F89.GV0150AM
# ******* FIC DES VTES DU MOIS PAR FAMILLE AYANT PU ETRE RECYCLE               
# *******               (LOAD DE LA RTHV09)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FGV158 ${DATA}/PGV0/F89.GV0158AM
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV152 ${DATA}/PGV0/F89.GV0152AM
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV153 ${DATA}/PGV0/F89.GV0153AM
# ******* CARTE PARAMETRE MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******   PARAMETRE SOCIETE : 989                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV150 
       JUMP_LABEL=STAT0MBN
       ;;
(STAT0MBN)
       m_CondExec 04,GE,STAT0MBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER DES VENTES ET REPRISES NON TRAITEES PAR BHV030               
#  NSOCORIG,NLIEU,NORIGINE,CODIC-GROUP,CMODDEL,NCODIC                          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBQ
       ;;
(STAT0MBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F89.BHV100AM
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -g +1 SORTOUT ${DATA}/PTEM/STAT0MBQ.BHV100BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_21_3 ASCENDING,
   FLD_CH_24_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0MBR
       ;;
(STAT0MBR)
       m_CondExec 00,EQ,STAT0MBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHV100                                                                
# ********************************************************************         
#  MAJ DE LA TABLE HISTO VENTES ET REPRISE DU MOIS                             
#  POUR LES GROUPES DE PRODUITS                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBT
       ;;
(STAT0MBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE SOUS TABLES GENERALISEES                                       
#    RSGA01   : NAME=RSGA01M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  DETAILS DE VENTES                                                    
#    RSGV11   : NAME=RSGV11M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58   : NAME=RSGA58M,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  VENTES ET REPRISES DU MOIS/GROUP-PRODUITS                            
       m_FileAssign -d SHR -g ${G_A11} FHV100 ${DATA}/PTEM/STAT0MBQ.BHV100BM
#                                                                              
# ******  HISTO VENTES ET REPRISES DE VENTES DU MOIS PAR GROUP-ARTICLE         
#    RSHV06   : NAME=RSHV06M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV06 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00M,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV100 
       JUMP_LABEL=STAT0MBU
       ;;
(STAT0MBU)
       m_CondExec 04,GE,STAT0MBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA DATE POUR L'HITO DES PSE (GCX55M)                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0MBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MBX
       ;;
(STAT0MBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU MOIS A TRAITER                                               
       m_FileAssign -i SORTIN
$FMOIS
_end
# ******  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F89.DATPSE0M
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0MBY
       ;;
(STAT0MBY)
       m_CondExec 00,EQ,STAT0MBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=STAT0MZA
       ;;
(STAT0MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
