#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TLPURD.ksh                       --- VERSION DU 08/10/2016 17:16
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDTLPUR -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/07/21 AT 10.36.45 BY BURTEC6                      
#    STANDARDS: P  JOBSET: TLPURD                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#  QUIESCE DES  TABLES LOGISTIQUES : RSTL01-05                                 
#  QUIESCE DES  TABLES MARSEILLE   : RSTL02D-04D-06D-09D           *           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TLPURDA
       ;;
(TLPURDA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=TLPURDAA
       ;;
(TLPURDAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PTL0/F91.QUIESCE.QTLPURAD
# *****   TABLES LOGISTIQUES                                                   
#    RSTL01   : NAME=RSTL01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL01 /dev/null
#    RSTL05   : NAME=RSTL05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05 /dev/null
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/TLPURDAA
       m_ProgramExec IEFBR14 "RDAR,TLPURD.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=TLPURDAD
       ;;
(TLPURDAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PTL0/F91.QUIESCE.QTLPURAD
       m_OutputAssign -c X SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=TLPURDAE
       ;;
(TLPURDAE)
       m_CondExec 00,EQ,TLPURDAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL909                                                                
#  EPURATION DE LA TABLE RTTL09R                                               
#  REPRISE: NON BACKOUT = RECOVER                                              
#     POUR LES  TABLES LOGISTIQUES : RSTL01-05  ===> DB2RBP                    
#     POUR LES  TABLES MARSEILLE   : RSTL02D-04D-06D-09D ==>DB2RBD             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TLPURDAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TLPURDAG
       ;;
(TLPURDAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE                                                                       
#    RSTL09   : NAME=RSTL09D,MODE=(U,N) - DYNAM=YES                            
# -X-TLPURDR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSTL09 /dev/null
#  TABLE GENERALISEE:SS-TABLE(DELAI,BTL901,BTL904,BTL905) EN NBR DE MO         
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  NUMERO DE SOCIETE                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/TLPURDAG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL909 
       JUMP_LABEL=TLPURDAH
       ;;
(TLPURDAH)
       m_CondExec 04,GE,TLPURDAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSTL09R                                       
#  REPRISE: NON BACKOUT = RECOVER                                              
#     POUR LES  TABLES LOGISTIQUES : RSTL01-05  ===> DB2RBP                    
#     POUR LES  TABLES MARSEILLE   : RSTL02D-04D-06D-09D ==>DB2RBD             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TLPURDAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TLPURDAM
       ;;
(TLPURDAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLES LOGISTIQUES                                                   
#    RSTL01   : NAME=RSTL01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL01 /dev/null
#    RSTL05   : NAME=RSTL05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05 /dev/null
#                                                                              
#    RSTL02   : NAME=RSTL02D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL02 /dev/null
#    RSTL04   : NAME=RSTL04D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL04 /dev/null
#    RSTL06   : NAME=RSTL06D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL06 /dev/null
#    RSTL09   : NAME=RSTL09D,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL09 /dev/null
#  TABLE GENERALISEE:SS-TABLE(DELAI,BTL901,BTL904,BTL905) EN NBR DE MO         
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  TABLE DES VENTES                                                            
#    RSGV11   : NAME=RSGV11D,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#  NUMERO DE SOCIETE                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/TLPURDAM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL900 
       JUMP_LABEL=TLPURDAN
       ;;
(TLPURDAN)
       m_CondExec 04,GE,TLPURDAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSTL02R                                       
#  REPRISE: NON BACKOUT = RECOVER                                              
#     POUR LES  TABLES LOGISTIQUES : RSTL01-05  ===> DB2RBP                    
#     POUR LES  TABLES MARSEILLE   : RSTL02D-04D-06D-09D ==>DB2RBD             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TLPURDAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
