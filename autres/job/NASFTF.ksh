#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NASFTF.ksh                       --- VERSION DU 17/10/2016 18:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFNASFT -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/06/13 AT 09.18.07 BY BURTECA                      
#    STANDARDS: P  JOBSET: NASFTF                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ******************************************************************           
#  CRE GDG � VIDE.  FTP SITE CENTRALIS� AVANT �CLATEMENT PAR FILIALE           
#  PAS DE CR�ATION � VIDE POSSIBLE DANS EASYTREV. SI AS400 N'A RIEN            
#  ENVOY�.                                                                     
#  REPRISE : OUI                                                               
# *******************************************************************          
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NASFTFA
       ;;
(NASFTFA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       G_A1=${G_A1:-'+2'}
       G_A10=${G_A10:-'+2'}
       G_A11=${G_A11:-'+2'}
       G_A12=${G_A12:-'+2'}
       G_A13=${G_A13:-'+2'}
       G_A2=${G_A2:-'+2'}
       G_A3=${G_A3:-'+2'}
       G_A4=${G_A4:-'+2'}
       G_A5=${G_A5:-'+2'}
       G_A6=${G_A6:-'+2'}
       G_A7=${G_A7:-'+2'}
       G_A8=${G_A8:-'+2'}
       G_A9=${G_A9:-'+2'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2012/06/13 AT 09.18.07 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: NASFTF                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'D�COMP NASLFTP'                        
# *                           APPL...: REPAPPL                                 
# *                           WAIT...: NASFTF                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NASFTFAA
       ;;
(NASFTFAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR NASCGCT /dev/null
       m_FileAssign -d SHR NASCKT0 /dev/null
       m_FileAssign -d SHR NASCA2I /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 NASGCTD ${DATA}/PNCGD/F91.NASGCTD
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g +1 NASKT0D ${DATA}/PXX0/F91.NASKT0D
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 NASGCTL ${DATA}/PNCGL/F61.NASGCTL
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g +1 NASKT0L ${DATA}/PXX0/F61.NASKT0L
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 NASGCTM ${DATA}/PNCGM/F89.NASGCTM
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g +1 NASKT0M ${DATA}/PXX0/F89.NASKT0M
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 NASGCTO ${DATA}/PNCGO/F16.NASGCTO
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g +1 NASKT0O ${DATA}/PXX0/F16.NASKT0O
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 NASGCTP ${DATA}/PNCGP/F07.NASGCTP
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g +1 NASKT0P ${DATA}/PXX0/F07.NASKT0P
       m_FileAssign -d NEW,CATLG,DELETE -r 277 -t LSEQ -g +1 FSLA2IP ${DATA}/PNCGP/F07.FSLA2IP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 NASGCTY ${DATA}/PNCGY/F45.NASGCTY
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g +1 NASKT0Y ${DATA}/PXX0/F45.NASKT0Y
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 NASCGCTF ${DATA}/PNCGF/F99.M5I351.NASCGCTF
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g +1 NASCKT0F ${DATA}/PNCGF/F99.M5I351.NASCKT0F
       m_FileAssign -d NEW,CATLG,DELETE -r 277 -t LSEQ -g +1 NASCA2IF ${DATA}/PNCGF/F99.M5I351.NASCA2IF
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NASFTFAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NASFTFAB
       ;;
(NASFTFAB)
       m_CondExec 16,NE,NASFTFAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ECLATEMENT FICHIERS PAR FILIALES. NASGCTX,NASKT0X, NASA2IX                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP NASFTFAD PGM=EZTPA00    ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=NASFTFAD
       ;;
(NASFTFAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d OLD,DELETE,KEEP NASCA2I ${DATA}/PNCGF/F99.M5I351.NASCA2IF
       m_FileAssign -d OLD,DELETE,KEEP NASCGCT ${DATA}/PNCGF/F99.M5I351.NASCGCTF
       m_FileAssign -d OLD,DELETE,KEEP NASCKT0 ${DATA}/PNCGF/F99.M5I351.NASCKT0F
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g ${G_A1} NASGCTD ${DATA}/PNCGD/F91.NASGCTD
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g ${G_A2} NASKT0D ${DATA}/PXX0/F91.NASKT0D
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g ${G_A3} NASGCTL ${DATA}/PNCGL/F61.NASGCTL
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g ${G_A4} NASKT0L ${DATA}/PXX0/F61.NASKT0L
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g ${G_A5} NASGCTM ${DATA}/PNCGM/F89.NASGCTM
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g ${G_A6} NASKT0M ${DATA}/PXX0/F89.NASKT0M
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g ${G_A7} NASGCTO ${DATA}/PNCGO/F16.NASGCTO
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g ${G_A8} NASKT0O ${DATA}/PXX0/F16.NASKT0O
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g ${G_A9} NASGCTP ${DATA}/PNCGP/F07.NASGCTP
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g ${G_A10} NASKT0P ${DATA}/PXX0/F07.NASKT0P
       m_FileAssign -d NEW,CATLG,DELETE -r 277 -t LSEQ -g ${G_A11} FSLA2IP ${DATA}/PNCGP/F07.FSLA2IP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g ${G_A12} NASGCTY ${DATA}/PNCGY/F45.NASGCTY
       m_FileAssign -d NEW,CATLG,DELETE -r 221 -t LSEQ -g ${G_A13} NASKT0Y ${DATA}/PXX0/F45.NASKT0Y
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/NASFTFAD
       m_ProgramExec NASFTFAD
# ********************************************************************         
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
