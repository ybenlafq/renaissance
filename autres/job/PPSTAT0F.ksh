#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PPSTAT0F.ksh                       --- VERSION DU 19/10/2016 19:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSTAT0F -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    STANDARDS: P  JOBSET: STAT0P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
#                                                                              
# ********************************************************************         
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030AP ISSU DU PGM BHV030                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=STAT0PF
       ;;
(STAT0PF)
       EXCAA=${EXCAA:-0}
       EXCAF=${EXCAF:-0}
       EXCAK=${EXCAK:-0}
       EXCAP=${EXCAP:-0}
       EXCAU=${EXCAU:-0}
       EXCAZ=${EXCAZ:-0}
       EXCBE=${EXCBE:-0}
       EXCBJ=${EXCBJ:-0}
       EXCBO=${EXCBO:-0}
       EXCBT=${EXCBT:-0}
       EXCBY=${EXCBY:-0}
       EXCCD=${EXCCD:-0}
       EXCCI=${EXCCI:-0}
       EXCCN=${EXCCN:-0}
       EXC98=${EXC98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2014/08/21 AT 10.23.23 BY BURTECC                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: STAT0P                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'MENSUELLE STAT/VALO'                   
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT0PFA
       ;;
(STAT0PFA)
       m_CondExec ${EXCAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHV030AP
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PFA.BVA500AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_70_8 70 CH 8
 /FIELDS FLD_CH_19_7 19 CH 7
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_70_8 ASCENDING
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PFB
       ;;
(STAT0PFB)
       m_CondExec 00,EQ,STAT0PFA ${EXCAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA500                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PFD PGM=IKJEFT01   ** ID=CAF                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PFD
       ;;
(STAT0PFD)
       m_CondExec ${EXCAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA00   : NAME=RSVA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTGG70   : NAME=RSGG70,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PTEM/STAT0PFA.BVA500AP
# *****   FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -t LSEQ -g +1 FVA00 ${DATA}/PTEM/STAT0PFD.FVA000AP
# *****   FICHIER DES ANOMALIES ENTRANT DANS LE GENERATEUR D'ETAT              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FVA500 ${DATA}/PXX0/F07.FVA500AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA500 
       JUMP_LABEL=STAT0PFE
       ;;
(STAT0PFE)
       m_CondExec 04,GE,STAT0PFD ${EXCAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA000AP POUR CREATION FICHIER ENTRANT DANS BGG500           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PFG PGM=SORT       ** ID=CAK                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PFG
       ;;
(STAT0PFG)
       m_CondExec ${EXCAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/STAT0PFD.FVA000AP
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PFG.FVA000BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "907"
 /DERIVEDFIELD CST_3_13 "P"
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_2 FLD_CH_8_3 EQ CST_1_9 AND FLD_CH_14_1 EQ CST_3_13 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 30 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_48_8,FLD_CH_36_6,FLD_CH_42_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0PFH
       ;;
(STAT0PFH)
       m_CondExec 00,EQ,STAT0PFG ${EXCAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG500                                                                
# ********************************************************************         
#  RECALCUL DES PRMPS DU MOIS                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PFJ PGM=IKJEFT01   ** ID=CAP                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PFJ
       ;;
(STAT0PFJ)
       m_CondExec ${EXCAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FVA00 ${DATA}/PTEM/STAT0PFG.FVA000BP
# ******  TABLES EN ENTREE                                                     
#    RTVA05   : NAME=RSVA05,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTVA05 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 FGG500 ${DATA}/PTEM/STAT0PFJ.FGG500AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG500 
       JUMP_LABEL=STAT0PFK
       ;;
(STAT0PFK)
       m_CondExec 04,GE,STAT0PFJ ${EXCAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FGG500AP                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PFM PGM=SORT       ** ID=CAU                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PFM
       ;;
(STAT0PFM)
       m_CondExec ${EXCAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/STAT0PFJ.FGG500AP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PFM.FGG500BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_19_12 19 CH 12
 /FIELDS FLD_CH_4_7 4 CH 7
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_4_7,FLD_CH_11_8,FLD_CH_19_12
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0PFN
       ;;
(STAT0PFN)
       m_CondExec 00,EQ,STAT0PFM ${EXCAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG505                                                                
# ********************************************************************         
#  MAJ DES PRMPS APRES RECALCUL                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PFQ PGM=IKJEFT01   ** ID=CAZ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PFQ
       ;;
(STAT0PFQ)
       m_CondExec ${EXCAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG70   : NAME=RSGG70,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGA10   : NAME=RSGA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER EN ENTREE ISSU DU TRI PRECEDENT                              
       m_FileAssign -d SHR -g ${G_A5} RTVA00I ${DATA}/PTEM/STAT0PFD.FVA000AP
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER DES PRA ENTRANT DANS LE GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IGG505 ${DATA}/PXX0/F07.FGG505AP
# ******  FICHIER D'EXTRACTION ISSU DU BGG500                                  
       m_FileAssign -d SHR -g ${G_A6} FGG500 ${DATA}/PTEM/STAT0PFM.FGG500BP
# ******  FICHIER POUR MAJ DE RTGA67 (GG506P)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -t LSEQ -g +1 FGA67 ${DATA}/PNCGP/F07.FGA67AP
# ******  FICHIER ENTRANT DANS BVA505 ET SERVANT A LOADER RTVA00               
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -t LSEQ -g +1 RTVA00S ${DATA}/PXX0/F07.FVA00AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG505 
       JUMP_LABEL=STAT0PFR
       ;;
(STAT0PFR)
       m_CondExec 04,GE,STAT0PFQ ${EXCAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA00AP POUR CREATION FICHIER BVA505AP ENTRANT               
#  DANS LE PROG BVA505                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PFT PGM=SORT       ** ID=CBE                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PFT
       ;;
(STAT0PFT)
       m_CondExec ${EXCBE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/F07.FVA00AP
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PFT.BVA505AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "P"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_76_8 76 CH 8
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_CH_68_8 68 CH 8
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_PD_76_8 76 PD 8
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_PD_68_8 68 PD 8
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_36_6 36 CH 6
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_11 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6,
    TOTAL FLD_PD_68_8,
    TOTAL FLD_PD_76_8
 /INCLUDE CND_2
 /* Record Type = F  Record Length = 44 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_11_3,FLD_CH_8_3,FLD_CH_36_6,FLD_CH_68_8,FLD_CH_42_6,FLD_CH_76_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0PFU
       ;;
(STAT0PFU)
       m_CondExec 00,EQ,STAT0PFT ${EXCBE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA505                                                                
# ********************************************************************         
#  MAJ DE LA RTVA05 POUR METTRE A JOUR EN QUANTITE ET EN VALEUR LES            
#  STOCKS,ET CREATION D'UN FICHIER BVA506AP POUR MAJ DE LA TABLE               
#  RTGG50 DANS LA PGM BVA506                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PFX PGM=IKJEFT01   ** ID=CBJ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PFX
       ;;
(STAT0PFX)
       m_CondExec ${EXCBJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG55   : NAME=RSGG55,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTVA10   : NAME=RSVA10,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTVA10 /dev/null
#    RTVA15   : NAME=RSVA15,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTVA15 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTVA05   : NAME=RSVA05,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGG50   : NAME=RSGG50,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A8} FVA505 ${DATA}/PTEM/STAT0PFT.BVA505AP
# ******  FICHIER SERVANT A LOADER LE TABLE RTVA15 DANS VA001P                 
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 FVA515 ${DATA}/PXX0/F07.FVA15AP
# ******  FICHIER ENTRANT DANS VA001P ET SERVANT A LOADER RTVA30               
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -t LSEQ -g +1 RTVA30 ${DATA}/PXX0/F07.FVA30AP
# ******  FICHIER SERVANT A LA MAJ DE LA RTGG50 DANS LE BVA506                 
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 FGG500 ${DATA}/PTEM/STAT0PFX.BVA506AP
# ******  FICHIER A HISTORISER QUI PERMET DE LOADER LA TABLE RTVA10            
# *****   FICHIER PARAMETRE 'M' POUR TRAIT. MENSUEL                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0PFX
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA505 
       JUMP_LABEL=STAT0PFY
       ;;
(STAT0PFY)
       m_CondExec 04,GE,STAT0PFX ${EXCBJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA506                                                                
# ********************************************************************         
#  MAJ DE LA RTGG50 POUR RECALCUL DES PRMS A PARTIR DU FICHIER                 
#  BVA506AP ISSU DU BVA505                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PGA PGM=IKJEFT01   ** ID=CBO                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PGA
       ;;
(STAT0PGA)
       m_CondExec ${EXCBO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU BVA505                                               
       m_FileAssign -d SHR -g ${G_A9} FGG500 ${DATA}/PTEM/STAT0PFX.BVA506AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA506 
       JUMP_LABEL=STAT0PGB
       ;;
(STAT0PGB)
       m_CondExec 04,GE,STAT0PGA ${EXCBO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030AP ISSU DU BHV030                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PGD PGM=SORT       ** ID=CBT                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PGD
       ;;
(STAT0PGD)
       m_CondExec ${EXCBT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHV030AP
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PGD.HV0000BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_9 "VEN"
 /DERIVEDFIELD CST_1_5 "VEN"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_5 OR FLD_CH_13_3 EQ CST_3_9 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_30_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 141 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PGE
       ;;
(STAT0PGE)
       m_CondExec 00,EQ,STAT0PGD ${EXCBT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV150                                                                
# ********************************************************************         
#  CREATION DES FICHIERS DE LOAD DE LA RTHV09 ET RTHV10 ET DES FICHIER         
#  CUMULS FHV01                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PGG PGM=IKJEFT01   ** ID=CBY                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PGG
       ;;
(STAT0PGG)
       m_CondExec ${EXCBY},NE,YES 
       m_OutputAssign -c 9 -w BGV150 SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN ENTREE                                                      
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG50   : NAME=RSGG50,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGG55   : NAME=RSGG55,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGG55 /dev/null
# ******* FICHIER HISTO FHV01 (ARTICLE DARTY)                                  
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PGV0/F07.HV01RP
# ******* FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A10} SV0404 ${DATA}/PTEM/STAT0PGD.HV0000BP
# ******* FIC DES VTES DU MOIS PAR ARTICLES AYANT PU ETRE RECYCLE              
# *******               (LOAD DE LA RTHV10)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -t LSEQ -g +1 FGV150 ${DATA}/PGV0/F07.GV0150AP
# ******* FIC DES VTES DU MOIS PAR FAMILLE AYANT PU ETRE RECYCLE               
# *******               (LOAD DE LA RTHV09)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -t LSEQ -g +1 FGV158 ${DATA}/PGV0/F07.GV0158AP
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -t LSEQ -g +1 FGV152 ${DATA}/PGV0/F07.GV0152AP
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -t LSEQ -g +1 FGV153 ${DATA}/PGV0/F07.GV0153AP
# ******* CARTE PARAMETRE MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******   PARAMETRE SOCIETE : 907                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV150 
       JUMP_LABEL=STAT0PGH
       ;;
(STAT0PGH)
       m_CondExec 04,GE,STAT0PGG ${EXCBY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER DES VENTES ET REPRISES NON TRAITEES PAR BHV030               
#  NSOCORIG,NLIEU,NORIGINE,CODIC-GROUP,CMODDEL,NCODIC                          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PGJ PGM=SORT       ** ID=CCD                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PGJ
       ;;
(STAT0PGJ)
       m_CondExec ${EXCCD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHV100AP
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/STAT0PGJ.BHV100BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_21_3 ASCENDING,
   FLD_CH_24_7 ASCENDING
 /* Record Type = F  Record Length = 70 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PGK
       ;;
(STAT0PGK)
       m_CondExec 00,EQ,STAT0PGJ ${EXCCD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHV100                                                                
# ********************************************************************         
#  MAJ DE LA TABLE HISTO VENTES ET REPRISE DU MOIS                             
#  POUR LES GROUPES DE PRODUITS                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PGM PGM=IKJEFT01   ** ID=CCI                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PGM
       ;;
(STAT0PGM)
       m_CondExec ${EXCCI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE SOUS TABLES GENERALISEES                                       
#    RSGA01   : NAME=RSGA01,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  DETAILS DE VENTES                                                    
#    RSGV11   : NAME=RSGV11,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58   : NAME=RSGA58,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  VENTES ET REPRISES DU MOIS/GROUP-PRODUITS                            
       m_FileAssign -d SHR -g ${G_A11} FHV100 ${DATA}/PTEM/STAT0PGJ.BHV100BP
#                                                                              
# ******  HISTO VENTES ET REPRISES DE VENTES DU MOIS PAR GROUP-ARTICLE         
#    RSHV06   : NAME=RSHV06,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSHV06 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00,MODE=(U,U) - DYNAM=YES                             
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV100 
       JUMP_LABEL=STAT0PGN
       ;;
(STAT0PGN)
       m_CondExec 04,GE,STAT0PGM ${EXCCI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA DATE POUR L'HITO DES PSE (GCX55O)                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0PGQ PGM=SORT       ** ID=CCN                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PGQ
       ;;
(STAT0PGQ)
       m_CondExec ${EXCCN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU MOIS A TRAITER                                               
       m_FileAssign -i SORTIN
$FMOIS
_end
# ******  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PEX0/F07.DATPSE0P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0PGR
       ;;
(STAT0PGR)
       m_CondExec 00,EQ,STAT0PGQ ${EXCCN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=C98                                   
# ***********************************                                          
       JUMP_LABEL=STAT0PZB
       ;;
(STAT0PZB)
       m_CondExec ${EXC98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0PZB.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
# *********************************************************************        
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
