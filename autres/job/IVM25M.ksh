#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM25M.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMIVM25 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/11 AT 14.27.25 BY BURTECC                      
#    STANDARDS: P  JOBSET: IVM25M                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIV122 : CONTROLE ET ENRICHISSEMENT DU FICHIER ECART D'INVENTAIRE           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM25MA
       ;;
(IVM25MA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2003/03/11 AT 14.27.25 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: IVM25M                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK INVT'                        
# *                           APPL...: IMPMETZ                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM25MAA
       ;;
(IVM25MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10M  : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10M /dev/null
# ******  TABLE GENERALISEE (MAGTM)                                            
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
#                                                                              
# ******  TABLE DES RéSULTATS D'INVENTAIRES                                    
#    RSIN01   : NAME=RSIN01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN01 /dev/null
#                                                                              
# ******  SOCIETEE A TRAITER = 989                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  TYPE DE TRAITEMENT = MGC CHS (MAGASINS)                              
       m_FileAssign -d SHR FTYPE ${DATA}/CORTEX4.P.MTXTFIX1/IVM25MAA
#                                                                              
# ******  EDITION DE CONTROLE                                                  
       m_OutputAssign -c X IIV122
# ******  FICHIER DES ECARTS D INVENTAIRE ENRICHI                              
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -g +1 FIV123 ${DATA}/PTEM/IVM25MAA.BIV123AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV122 
       JUMP_LABEL=IVM25MAB
       ;;
(IVM25MAB)
       m_CondExec 04,GE,IVM25MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS D'INVENTAIRE A IMPUTER                            
#           SUR CODIC SOCIETE MAGASIN S/LIEU STATUT                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM25MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM25MAD
       ;;
(IVM25MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IVM25MAA.BIV123AM
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -g +1 SORTOUT ${DATA}/PTEM/IVM25MAD.BIV123BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_10 1 CH 10
 /FIELDS FLD_CH_11_7 11 CH 7
 /KEYS
   FLD_CH_11_7 ASCENDING,
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 25 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM25MAE
       ;;
(IVM25MAE)
       m_CondExec 00,EQ,IVM25MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV123                                                                      
# ********************************************************************         
#  MAJ DE LA TABLE D INVENTAIRE RTIN00 IMPUTATION DES ECARTS                   
#  REPRISE:  NON APRES UNE FIN NORMALE                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM25MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM25MAG
       ;;
(IVM25MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00M  : NAME=RSGA00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00M /dev/null
# ******  TABLE GENERALISEE (INVDA)                                            
#    RSGA01M  : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01M /dev/null
# ******  FAMILLES                                                             
#    RSGA14M  : NAME=RSGA14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14M /dev/null
# ******  LIENS ARTICLES                                                       
#    RSGA58M  : NAME=RSGA58M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58M /dev/null
# ******  PRMP                                                                 
#    RSGG50M  : NAME=RSGG50M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50M /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55M  : NAME=RSGG55M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55M /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D INVENTAIRE PROVENANT DES 36 MAGS                
       m_FileAssign -d SHR -g ${G_A2} FIV123 ${DATA}/PTEM/IVM25MAD.BIV123BM
#                                                                              
# ******  TABLE D'INVENTAIRE MAGASINS                                          
#    RSIN00M  : NAME=RSIN00M,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00M /dev/null
#                                                                              
# ******  SOCIETEE A TRAITER  = 989                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV123 
       JUMP_LABEL=IVM25MAH
       ;;
(IVM25MAH)
       m_CondExec 04,GE,IVM25MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM25MZA
       ;;
(IVM25MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM25MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
