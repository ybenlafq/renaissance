#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  PRO01P.ksh                       --- VERSION DU 08/10/2016 12:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPPRO01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/04/14 AT 11.55.44 BY BURTECA                      
#    STANDARDS: P  JOBSET: PRO01P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  PGM : BPRO01                                                                
# ********************************************************************         
#  PURGE PAR PROGRAMME DES TABLES SYSTEMES DU PRODUIT DPROPR                   
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=PRO01PA
       ;;
(PRO01PA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=PRO01PAA
       ;;
(PRO01PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ENTREE : FICHIER DATE                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******* ENTREE : PARAMETRAGE DELAI DE PURGE (NNN EN JOURS)                   
# ******* MIN = 000 JOURS / MAX = 020 JOURS                                    
       m_FileAssign -d SHR DELAIS ${DATA}/CORTEX4.P.MTXTFIX1/PRO01PAA
#                                                                              
# ******  SORTIE : TABLES EN MAJ                                               
# ******  A T T E N T I O N ! MAJ UNIQUEMENT DES TABLE APPLY_TRACE             
# ******                                               APPLY_TRAIL             
#                                                                              
#    DTSTSROW : NAME=DTSTSROW,MODE=(U,N) - DYNAM=YES                           
       m_FileAssign -d SHR DTSTSROW /dev/null
#                                                                              
# ******  APPEL DU PGM                                                         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BPRO01 
       JUMP_LABEL=PRO01PAB
       ;;
(PRO01PAB)
       m_CondExec 04,GE,PRO01PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
