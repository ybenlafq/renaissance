#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EMMAJL.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLEMMAJ -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/28 AT 10.34.57 BY BURTECA                      
#    STANDARDS: P  JOBSET: EMMAJL                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTF600 : EXTRACTION DE LA TABLE GENERALISEE (NEMAT) RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EMMAJLA
       ;;
(EMMAJLA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EMMAJLAA
       ;;
(EMMAJLAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF600 ${DATA}/PTEM/EMMAJLAA.MTF600AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF600 
       JUMP_LABEL=EMMAJLAB
       ;;
(EMMAJLAB)
       m_CondExec 04,GE,EMMAJLAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF730 : EXTRACTION TABLE PARAMETRES MAGASIN RTPM00                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJLAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLAD
       ;;
(EMMAJLAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10L  : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10L /dev/null
# ******* TABLE PARAMETRE MAGASIN NEM                                          
#    RSPM00L  : NAME=RSPM00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPM00L /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* N� MAGASIN A INITIALISER                                             
       m_FileAssign -d SHR FNMAG ${DATA}/CORTEX4.P.MTXTFIX1/EMMAJLAD
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF730 ${DATA}/PTEM/EMMAJLAD.MTF730AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF730 
       JUMP_LABEL=EMMAJLAE
       ;;
(EMMAJLAE)
       m_CondExec 04,GE,EMMAJLAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DE TOUS LES FICHIERS POUR ENVOI DES MAJ                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJLAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLAG
       ;;
(EMMAJLAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EMMAJLAA.MTF600AL
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/EMMAJLAD.MTF730AL
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 SORTOUT ${DATA}/PTEM/EMMAJLAG.MTF000XL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EMMAJLAH
       ;;
(EMMAJLAH)
       m_CondExec 00,EQ,EMMAJLAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJLAJ PGM=BTF900     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLAJ
       ;;
(EMMAJLAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A3} FTF600 ${DATA}/PTEM/EMMAJLAG.MTF000XL
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/PXX0/F61.MTF000AL
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF602 ${DATA}/PXX0/F61.MTF900AL
       m_ProgramExec BTF900 
# ********************************************************************         
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  DUPLI AVEC FICHIER DE LA VEILLE EN DUMMY                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJLAM PGM=BTF900     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLAM
       ;;
(EMMAJLAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A4} FTF600 ${DATA}/PTEM/EMMAJLAG.MTF000XL
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/PXX0/F61.MTF000AL
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF602 ${DATA}/PTEM/EMMAJLAM.MTF900XL
       m_ProgramExec BTF900 
# ********************************************************************         
#  BTF910 : ENVOI DES MAJ PAR MQSERIES                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJLAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLAQ
       ;;
(EMMAJLAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A5} FTF910 ${DATA}/PXX0/F61.MTF900AL
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/EMMAJL1
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/EMMAJLAQ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF910 
       JUMP_LABEL=EMMAJLAR
       ;;
(EMMAJLAR)
       m_CondExec 04,GE,EMMAJLAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF915 : ENVOI DES MAJ PAR MQSERIES POUR LA MACHINE CENTRALISEE             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJLAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLAT
       ;;
(EMMAJLAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A6} FTF915 ${DATA}/PTEM/EMMAJLAM.MTF900XL
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/EMMAJL1
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/EMMAJLAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF915 
       JUMP_LABEL=EMMAJLAU
       ;;
(EMMAJLAU)
       m_CondExec 04,GE,EMMAJLAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DU FICHIER MTF0000 POUR COMPARAISON DU LENDEMAIN                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJLAX PGM=IEBGENER   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLAX
       ;;
(EMMAJLAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A7} SYSUT1 ${DATA}/PTEM/EMMAJLAG.MTF000XL
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 SYSUT2 ${DATA}/PXX0/F61.MTF000AL
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=EMMAJLAY
       ;;
(EMMAJLAY)
       m_CondExec 00,EQ,EMMAJLAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJLZA
       ;;
(EMMAJLZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EMMAJLZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
