#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM85M.ksh                       --- VERSION DU 08/10/2016 22:24
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMIVM85 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/03/22 AT 16.47.59 BY PREPA2                       
#    STANDARDS: P  JOBSET: IVM85M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DES FICHIERS ISSU DES PGMS BIV130 ET BIE540                             
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM85MA
       ;;
(IVM85MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVM85MAA
       ;;
(IVM85MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PGI989/F89.BIV130AM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PGI989/F89.BIE540AM
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -g +1 SORTOUT ${DATA}/PTEM/IVM85MAA.BIV185AM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM85MAB
       ;;
(IVM85MAB)
       m_CondExec 00,EQ,IVM85MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV185                                                                      
# ********************************************************************         
#  EDITION DES ECARTS FAMILLES ET SOUS-LIEU                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM85MAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM85MAD
       ;;
(IVM85MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN ENTREE                                                      
#    RSGA01   : NAME=RSGA01M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA10   : NAME=RSGA10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA30   : NAME=RSGA30M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30 /dev/null
# ******  PARAMETRE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
# ******  FICHIER ISSU DU TRI PRECEDENTS                                       
       m_FileAssign -d SHR -g ${G_A1} FIV185 ${DATA}/PTEM/IVM85MAA.BIV185AM
# ******  EDITION DE L'ETAT IIV185                                             
#  IIV185   REPORT SYSOUT=(9,IIV185),RECFM=FBA,LRECL=133,BLKSIZE=1330          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 IIV185 ${DATA}/PXX0.FICHEML.IVM85M.BIV185BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV185 
       JUMP_LABEL=IVM85MAE
       ;;
(IVM85MAE)
       m_CondExec 04,GE,IVM85MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV185)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM85MAG PGM=IEBGENER   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM85MAG
       ;;
(IVM85MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM85M.BIV185BM
       m_OutputAssign -c 9 -w IIV185 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM85MAH
       ;;
(IVM85MAH)
       m_CondExec 00,EQ,IVM85MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM85MZA
       ;;
(IVM85MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM85MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
