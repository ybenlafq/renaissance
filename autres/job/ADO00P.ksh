#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ADO00P.ksh                       --- VERSION DU 17/10/2016 18:02
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPADO00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 08.35.49 BY BURTEC6                      
#    STANDARDS: P  JOBSET: ADO00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EXTRACTION RTEM51                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=ADO00PA
       ;;
(ADO00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       PROCSTEP=${PROCSTEP:-ADO00PA}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2016/02/24 AT 08.35.49 BY BURTEC6                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: ADO00P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'JOB HADOOP'                            
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=ADO00PAA
       ;;
(ADO00PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/REORG/F07.UNEM51AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PAD
       ;;
(ADO00PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/REORG/F07.UNEM51AP
       m_FileAssign -d NEW,CATLG,DELETE -r 335 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNEM51BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AAK      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNEM51BP.*,+                                            
# RTEM51_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO00AP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AAP      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNEM51BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO00BP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO00AP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO00PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_337 1 CH 337
 /FIELDS FLD_CH_3_337 3 CH 337
 /KEYS
   FLD_CH_1_337 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_337
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO00PAG
       ;;
(ADO00PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO00AP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PAE
       ;;
(ADO00PAE)
       m_CondExec 00,EQ,ADO00PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PAJ PROC=JVZIP     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PAJ
       ;;
(ADO00PAJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAAP},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A2} DD1 ${DATA}/PXX0/F07.UNEM51BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO00BP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PAJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO00AP(+1),DISP=SHR                    ~         
#
       JUMP_LABEL=ADO00PAM
       ;;
(ADO00PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO00PAM.FADO00BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO00BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PAQ
       ;;
(ADO00PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO00PAM.FADO00BP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO00PAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PAT
       ;;
(ADO00PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PAU
       ;;
(ADO00PAU)
       m_CondExec 16,NE,ADO00PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTEM52                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PAX PGM=PTLDRIVM   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PAX
       ;;
(ADO00PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/REORG/F07.UNEM52AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PBA
       ;;
(ADO00PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/REORG/F07.UNEM52AP
       m_FileAssign -d NEW,CATLG,DELETE -r 305 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNEM52BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABT      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNEM52BP.*,+                                            
# RTEM52_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO00CP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABY      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNEM52BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO00DP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO00CP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO00PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_307 1 CH 307
 /FIELDS FLD_CH_3_307 3 CH 307
 /KEYS
   FLD_CH_1_307 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_307
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO00PBD
       ;;
(ADO00PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO00CP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PBB
       ;;
(ADO00PBB)
       m_CondExec 00,EQ,ADO00PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PBG PROC=JVZIP     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PBG
       ;;
(ADO00PBG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXABY},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A6} DD1 ${DATA}/PXX0/F07.UNEM52BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO00DP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PBG.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO00CP(+1),DISP=SHR                    ~         
#
       JUMP_LABEL=ADO00PBJ
       ;;
(ADO00PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PBJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO00PBJ.FADO00DP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO00DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PBM
       ;;
(ADO00PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PBM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO00PBJ.FADO00DP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO00PBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PBQ
       ;;
(ADO00PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PBQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PBR
       ;;
(ADO00PBR)
       m_CondExec 16,NE,ADO00PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTEM53                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PBT PGM=PTLDRIVM   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PBT
       ;;
(ADO00PBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/REORG/F07.UNEM53AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PBT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PBX
       ;;
(ADO00PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/REORG/F07.UNEM53AP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNEM53BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ADC      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNEM53BP.*,+                                            
# RTEM53_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO00EP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ADH      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNEM53BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO00FP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO00EP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO00PCA PGM=EZACFSM1   ** ID=ADC                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_148 3 CH 148
 /FIELDS FLD_CH_1_148 1 CH 148
 /KEYS
   FLD_CH_1_148 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_148
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO00PCA
       ;;
(ADO00PCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PCA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO00EP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PBY
       ;;
(ADO00PBY)
       m_CondExec 00,EQ,ADO00PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PCD PROC=JVZIP     ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PCD
       ;;
(ADO00PCD)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXADH},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A10} DD1 ${DATA}/PXX0/F07.UNEM53BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO00FP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PCD.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO00EP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO00PCG
       ;;
(ADO00PCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PCG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO00PCG.FADO00FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO00FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PCJ PGM=FTP        ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PCJ
       ;;
(ADO00PCJ)
       m_CondExec ${EXADR},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PCJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO00PCG.FADO00FP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO00PCM PGM=IDCAMS     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PCM
       ;;
(ADO00PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PCM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PCN
       ;;
(ADO00PCN)
       m_CondExec 16,NE,ADO00PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTEM54                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PCQ PGM=PTLDRIVM   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PCQ
       ;;
(ADO00PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/REORG/F07.UNEM54AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PCQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PCT
       ;;
(ADO00PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/REORG/F07.UNEM54AP
       m_FileAssign -d NEW,CATLG,DELETE -r 154 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNEM54BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AEL      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNEM54BP.*,+                                            
# RTEM54_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO00GP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AEQ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNEM54BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO00HP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO00GP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO00PCX PGM=EZACFSM1   ** ID=AEL                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_156 3 CH 156
 /FIELDS FLD_CH_1_156 1 CH 156
 /KEYS
   FLD_CH_1_156 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_156
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO00PCX
       ;;
(ADO00PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PCX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO00GP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PCU
       ;;
(ADO00PCU)
       m_CondExec 00,EQ,ADO00PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PDA PROC=JVZIP     ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PDA
       ;;
(ADO00PDA)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAEQ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A14} DD1 ${DATA}/PXX0/F07.UNEM54BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO00HP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PDA.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO00GP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO00PDD
       ;;
(ADO00PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PDD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO00PDD.FADO00HP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO00HP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PDG PGM=FTP        ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PDG
       ;;
(ADO00PDG)
       m_CondExec ${EXAFA},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PDG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO00PDD.FADO00HP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO00PDJ PGM=IDCAMS     ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PDJ
       ;;
(ADO00PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PDJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PDK
       ;;
(ADO00PDK)
       m_CondExec 16,NE,ADO00PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTEM55                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PDM PGM=PTLDRIVM   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PDM
       ;;
(ADO00PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC01 ${DATA}/REORG/F07.UNEM55AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PDM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PDQ
       ;;
(ADO00PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/REORG/F07.UNEM55AP
       m_FileAssign -d NEW,CATLG,DELETE -r 127 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNEM55BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AFU      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNEM55BP.*,+                                            
# RTEM55_&YR4.&LMON.&LDAY..TXT)                                                
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=HADO00IP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AFZ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNEM55BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=HADO00JP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=HADO00IP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO00PDT PGM=EZACFSM1   ** ID=AFU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_129 1 CH 129
 /FIELDS FLD_CH_3_129 3 CH 129
 /KEYS
   FLD_CH_1_129 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_129
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO00PDT
       ;;
(ADO00PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PDT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PXX0/F07.HADO00IP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PDR
       ;;
(ADO00PDR)
       m_CondExec 00,EQ,ADO00PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PDX PROC=JVZIP     ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PDX
       ;;
(ADO00PDX)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAFZ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A18} DD1 ${DATA}/PXX0/F07.UNEM55BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.HADO00JP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PDX.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PXX0.F07.HADO00IP(+1),DISP=SHR                   ~         
#
       JUMP_LABEL=ADO00PEA
       ;;
(ADO00PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PEA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO00PEA.FADO00JP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FADO00JP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO00PED PGM=FTP        ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PED
       ;;
(ADO00PED)
       m_CondExec ${EXAGJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PED.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO00PEA.FADO00JP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO00PEG PGM=IDCAMS     ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PEG
       ;;
(ADO00PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PEG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO00PEH
       ;;
(ADO00PEH)
       m_CondExec 16,NE,ADO00PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=ADO00PZA
       ;;
(ADO00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
