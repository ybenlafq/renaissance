#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTCREP.ksh                       --- VERSION DU 17/10/2016 18:10
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFTCRE -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/05 AT 10.57.38 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FTCREP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  EASYTREAVE POUR METTRE LE FICHIER CREDOR EN 400                             
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FTCREPA
       ;;
(FTCREPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2015/03/05 AT 10.57.38 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FTCREP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'CREDOR'                                
# *                           APPL...: REPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FTCREPAA
       ;;
(FTCREPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
# **************************************                                       
       m_OutputAssign -c X SYSPRINT
       m_FileAssign -d SHR -g +0 FILEA ${DATA}/PXX0/F07.CREDORAP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/FTP.F07.HIDECLAP
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 SORTIE ${DATA}/PTEM/FTCREPAA.CREDORXP
       m_OutputAssign -c T IMPRIM
       m_OutputAssign -c T SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FTCREPAA
       m_ProgramExec FTCREPAA
# ********************************************************************         
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DIF          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREPAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTCREPAD
       ;;
(FTCREPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FTCREPAA.CREDORXP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGP/F07.CREDORBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "907"
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREPAE
       ;;
(FTCREPAE)
       m_CondExec 00,EQ,FTCREPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DNN          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREPAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FTCREPAG
       ;;
(FTCREPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FTCREPAA.CREDORXP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.CREDORBL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "961"
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREPAH
       ;;
(FTCREPAH)
       m_CondExec 00,EQ,FTCREPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DAL          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREPAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FTCREPAJ
       ;;
(FTCREPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/FTCREPAA.CREDORXP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F89.CREDORBM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "989"
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_144_8 144 CH 8
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREPAK
       ;;
(FTCREPAK)
       m_CondExec 00,EQ,FTCREPAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DO           
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREPAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FTCREPAM
       ;;
(FTCREPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/FTCREPAA.CREDORXP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F16.CREDORBO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "916"
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_BI_16_3 16 CH 3
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREPAN
       ;;
(FTCREPAN)
       m_CondExec 00,EQ,FTCREPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DPM          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREPAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FTCREPAQ
       ;;
(FTCREPAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/FTCREPAA.CREDORXP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.CREDORBD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "991"
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_144_8 144 CH 8
 /FIELDS FLD_BI_16_3 16 CH 3
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREPAR
       ;;
(FTCREPAR)
       m_CondExec 00,EQ,FTCREPAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#    TRI DU FICHIER ISSU DE L'EASYTRIEVE POUR CR�ER LE FICHIER DE DRA          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTCREPAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FTCREPAT
       ;;
(FTCREPAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FIC EN ENTREE                                                        
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FTCREPAA.CREDORXP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F45.CREDORBY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "945"
 /FIELDS FLD_CH_102_3 102 CH 3
 /FIELDS FLD_BI_16_3 16 CH 3
 /FIELDS FLD_BI_1_15 1 CH 15
 /FIELDS FLD_BI_144_8 144 CH 8
 /CONDITION CND_1 FLD_CH_102_3 EQ CST_1_6 
 /KEYS
   FLD_BI_1_15 ASCENDING,
   FLD_BI_144_8 ASCENDING,
   FLD_BI_16_3 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTCREPAU
       ;;
(FTCREPAU)
       m_CondExec 00,EQ,FTCREPAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FTCREPZA
       ;;
(FTCREPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTCREPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
