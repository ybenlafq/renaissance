#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM25O.ksh                       --- VERSION DU 08/10/2016 12:50
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POIVM25 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/03/11 AT 14.27.51 BY BURTECC                      
#    STANDARDS: P  JOBSET: IVM25O                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIV122 : CONTROLE ET ENRICHISSEMENT DU FICHIER ECART D'INVENTAIRE           
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=IVM25OA
       ;;
(IVM25OA)
       C_A1=${C_A1:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON TUESDAY   2003/03/11 AT 14.27.51 BY BURTECC                
# *    JOBSET INFORMATION:    NAME...: IVM25O                                  
# *                           FREQ...: 2Y                                      
# *                           TITLE..: 'MAJ STOCK INVT'                        
# *                           APPL...: IMPMGIO                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=IVM25OAA
       ;;
(IVM25OAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#                                                                              
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  TABLE DES LIEUX                                                      
#    RSGA10   : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
# ******  TABLE GENERALISEE (MAGTY)                                            
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#                                                                              
# ******  TABLE DES RéSULTATS D'INVENTAIRES                                    
#    RSIN01   : NAME=RSIN01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN01 /dev/null
#                                                                              
# ******  SOCIETEE A TRAITER = 916                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  TYPE DE TRAITEMENT = MGC CHS (MAGASINS)                              
       m_FileAssign -d SHR FTYPE ${DATA}/CORTEX4.P.MTXTFIX1/IVM25OAA
#                                                                              
# ******  EDITION DE CONTROLE                                                  
       m_OutputAssign -c X IIV122
# ******  FICHIER DES ECARTS D INVENTAIRE ENRICHI                              
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -g +1 FIV123 ${DATA}/PTEM/IVM25OAA.BIV123AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV122 
       JUMP_LABEL=IVM25OAB
       ;;
(IVM25OAB)
       m_CondExec 04,GE,IVM25OAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS D'INVENTAIRE A IMPUTER                            
#           SUR CODIC SOCIETE MAGASIN S/LIEU STATUT                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM25OAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM25OAD
       ;;
(IVM25OAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IVM25OAA.BIV123AO
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -g +1 SORTOUT ${DATA}/PTEM/IVM25OAD.BIV123BO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_7 11 CH 7
 /FIELDS FLD_CH_1_10 1 CH 10
 /KEYS
   FLD_CH_11_7 ASCENDING,
   FLD_CH_1_10 ASCENDING
 /* Record Type = F  Record Length = 25 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM25OAE
       ;;
(IVM25OAE)
       m_CondExec 00,EQ,IVM25OAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV123                                                                      
# ********************************************************************         
#  MAJ DE LA TABLE D INVENTAIRE RTIN00 IMPUTATION DES ECARTS                   
#  REPRISE:  NON APRES UNE FIN NORMALE                                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM25OAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM25OAG
       ;;
(IVM25OAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE GENERALISEE (INVDA)                                            
#    RSGA01   : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  FAMILLES                                                             
#    RSGA14   : NAME=RSGA14O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14 /dev/null
# ******  LIENS ARTICLES                                                       
#    RSGA58   : NAME=RSGA58O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  PRMP                                                                 
#    RSGG50   : NAME=RSGG50O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  FICHIER DES ECARTS D INVENTAIRE PROVENANT DES 36 MAGS                
       m_FileAssign -d SHR -g ${G_A2} FIV123 ${DATA}/PTEM/IVM25OAD.BIV123BO
# ******  TABLE D'INVENTAIRE MAGASINS                                          
#    RSIN00   : NAME=RSIN00O,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00 /dev/null
# ******  SOCIETEE A TRAITER  = 916                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV123 
       JUMP_LABEL=IVM25OAH
       ;;
(IVM25OAH)
       m_CondExec 04,GE,IVM25OAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM25OZA
       ;;
(IVM25OZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM25OZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
