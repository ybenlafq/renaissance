#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  STAT2P.ksh                       --- VERSION DU 08/10/2016 15:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPSTAT2 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/07/21 AT 14.44.20 BY BURTEC6                      
#    STANDARDS: P  JOBSET: STAT2P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030AP ISSU DU PGM BHV030 (CHAINE STAT0P)                  
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=STAT2PA
       ;;
(STAT2PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXA99=${EXA99:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=STAT2PAA
       ;;
(STAT2PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :                                                     
#  ----------------------                                                      
#  APRES CREATION DU FICHIER BHV030AP                                          
# **************************************                                       
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.BHV030AP
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 SORTOUT ${DATA}/PXX0/STAT2PAA.BGV112AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 7
 /KEYS
   FLD_CH_19_7 ASCENDING
 /* Record Type = F  Record Length = 146 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT2PAB
       ;;
(STAT2PAB)
       m_CondExec 00,EQ,STAT2PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV112                                                                
#  ------------                                                                
#  EXTRACTION D'UN FICHIER DES VENTES TOPEES                                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT2PAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=STAT2PAD
       ;;
(STAT2PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA00   : NAME=RSGA00,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA30   : NAME=RSGA30,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGV02   : NAME=RSGV02,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGV02 /dev/null
# *****   FICHIER PARAMETRE DES COMMUNES A TRAITER                             
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT2PAD
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PXX0/STAT2PAA.BGV112AP
# *****   FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 62 -g +1 FGV112 ${DATA}/PAS0/F07.FGV112P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV112 
       JUMP_LABEL=STAT2PAE
       ;;
(STAT2PAE)
       m_CondExec 04,GE,STAT2PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     **                                          
# ***********************************                                          
       JUMP_LABEL=STAT2PZA
       ;;
(STAT2PZA)
       m_CondExec ${EXA99},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT2PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
