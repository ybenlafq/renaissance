#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MQPURP.ksh                       --- VERSION DU 08/10/2016 13:18
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPMQPUR -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/03/31 AT 15.32.58 BY BURTECN                      
#    STANDARDS: P  JOBSET: MQPURP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   PGM : BMQ032                                                               
#   ------------                                                               
#   RECOPIE D'UNE QUEUE DANS UNE AUTRE                                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MQPURPA
       ;;
(MQPURPA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2004/03/31 AT 15.32.58 BY BURTECN                
# *    JOBSET INFORMATION:    NAME...: MQPURP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'PURGE MSG S59'                         
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MQPURPAA
       ;;
(MQPURPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  --------------------- :             *                                       
# **************************************                                       
       m_StepLibSet NEM.DEVL.LOAD:DSA.TEST.LOAD
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSPRINT
#  FICHIER QUI PERMET DE RECHARGER DANS UNE QUEUE                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FMQOUT ${DATA}/PXX0/F07.MQPURPA
# ========================================                                     
#  FQMGR    : QUEUE MANAGER                                                    
# ========================================                                     
       m_FileAssign -d SHR FQMGR ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP1
       m_FileAssign -d SHR FQUEUE ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP2
       m_FileAssign -d SHR FMSGID ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP3
       m_FileAssign -d SHR FCORR ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP4
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP5
       m_FileAssign -d SHR FHEURE ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP6
       m_FileAssign -d SHR FNSOCE ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP7
       m_FileAssign -d SHR FNSOCD ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP8
       m_FileAssign -d SHR FCFONC ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP9
       m_FileAssign -d SHR FBROW ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP10
       m_FileAssign -d SHR FSYNC ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP11
       m_FileAssign -d SHR FDEAD ${DATA}/CORTEX4.P.MTXTFIX1/MQPURP12
       m_ProgramExec BMQ032 
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
