#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  RQDGEP.ksh                       --- VERSION DU 08/10/2016 13:53
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPRQDGE -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/06/15 AT 10.45.11 BY BURTEC2                      
#    STANDARDS: P  JOBSET: RQDGEP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **                                                                           
# ********************************************************************         
#  UNLOAD SL10                                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=RQDGEPA
       ;;
(RQDGEPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON MONDAY    2015/06/15 AT 10.45.11 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: RQDGEP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'EXTRACDGE SL10'                        
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=RQDGEPAA
       ;;
(RQDGEPAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -t LSEQ -g +1 SYSREC ${DATA}/PXX0/F07.UNRQDGEP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RQDGEPAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC
# ********************************************************************         
#  ZIP DU FICHIER UNLOAD� POUR ENVOI GTW                                       
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAF      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# ******* FICHIER EN ENTREE DE ZIP                                             
# DD1      FILE  NAME=UNRQDGEP,MODE=I                                          
# ******* FICHIER EN SORTIE DE ZIP                                             
# FICZIP   FILE  NAME=RQDGEZIP,MODE=O                                          
# **                                                                           
# SYSIN    DATA  *                                                             
#  -ECHO                                                                       
#  -ACTION(ADD)                                                                
#  -COMPRESSION_LEVEL(NORMAL)                                                  
#  -TRAN(ANSI850)                                                              
#  -TEXT                                                                       
#  -FILE_TERMINATOR()                                                          
#  -INFILE(DD1)                                                                
#  -ZIPPED_DSN(PXX0.F07.UNRQDGEP.*,EXTRACTION_SL10.CSV)                        
#  -ARCHIVE_OUTFILE(FICZIP)                                                    
#          DATAEND                                                             
# ********************************************************************         
#  ZIP AVEC JAVAZIP 040                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RQDGEPAD PGM=JVMLDM76   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=RQDGEPAD
       ;;
(RQDGEPAD)
       m_CondExec ${EXAAF},NE,YES 
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileAssign -d SHR -g ${G_A1} DD1 ${DATA}/PXX0/F07.UNRQDGEP
# ****** FICHIER EN SORTIE DE ZIP                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 FICZIP ${DATA}/PXX0/F07.RQDGEZIP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RQDGEPAD.sysin
       m_ProgramExec JVMLDM76_ZIP.ksh
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTRQDGEP                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RQDGEPAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=RQDGEPAG
       ;;
(RQDGEPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RQDGEPAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/RQDGEPAG.FTRQDGEP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTRQDGEP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP RQDGEPAJ PGM=FTP        ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=RQDGEPAJ
       ;;
(RQDGEPAJ)
       m_CondExec ${EXAAP},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/RQDGEPAJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.RQDGEPAG.FTRQDGEP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=RQDGEPZA
       ;;
(RQDGEPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/RQDGEPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
