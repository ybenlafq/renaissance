#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CFQ11Y.ksh                       --- VERSION DU 08/10/2016 23:06
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYCFQ11 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/04/04 AT 14.27.26 BY BURTECR                      
#    STANDARDS: P  JOBSET: CFQ11Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# **--USER='LYON'                                                              
# ********************************************************************         
#   LISTE POUR LA GESTION DU HS POUR LA COMPTA CFQ11                           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CFQ11YA
       ;;
(CFQ11YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       RUN=${RUN}
       JUMP_LABEL=CFQ11YAA
       ;;
(CFQ11YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" QQFERR
# ***********************                                                      
# ** FICHIER POUR QQF ***                                                      
# ***********************                                                      
       m_FileAssign -d SHR SYSEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d SHR QQFEXEC ${DATA}/SYS2.KRYSTAL.CLIST
       m_FileAssign -d NEW,DELETE QQFSPILL ${MT_TMP}/SPILL_${MT_JOB_NAME}_${MT_JOB_PID}
       m_FileAssign -d NEW,DELETE QQFSORTN ${MT_TMP}/SORTN_${MT_JOB_NAME}_${MT_JOB_PID}
       m_OutputAssign -c 9 -w CFQ11 QQFRPT
       m_OutputAssign -c "*" QQFRPT2
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_FileAssign -d SHR HOSTIN /dev/null
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b CFQ11 
       JUMP_LABEL=CFQ11YAB
       ;;
(CFQ11YAB)
       m_CondExec 04,GE,CFQ11YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE POUR LA GESTION DU HS POUR LA COMPTA CFQ12                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CFQ11YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CFQ11YAD
       ;;
(CFQ11YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w CFQ12 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.CFQ12 (&&DATE='$DATEDIT_ANNMMDD' FORM=ADMFIL.CFF12
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p CFQ12Y1 -a QMFPARM DSQPRINT
       JUMP_LABEL=CFQ11YAE
       ;;
(CFQ11YAE)
       m_CondExec 04,GE,CFQ11YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE POUR LA GESTION DU HS POUR LA COMPTA QCF13                           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CFQ11YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CFQ11YAG
       ;;
(CFQ11YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w CFQ13 DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QCF13 (&&FMOIS='$FINMOIS_ANNMM' &&SOC='$DRADEP' FORM=ADMFIL.FCF13
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QCF13Y1 -a QMFPARM DSQPRINT
       JUMP_LABEL=CFQ11YAH
       ;;
(CFQ11YAH)
       m_CondExec 04,GE,CFQ11YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE POUR LA VALEUR DES STOCKS FOURNITURE QFOURD                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CFQ11YAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CFQ11YAJ
       ;;
(CFQ11YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QFOURD DSQPRINT
       m_FileAssign -i QMFPARM
RUN ADMFIL.QFOURD (&&SOC='$DRADEP' FORM=ADMFIL.FFOURD
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QFOURDY1 -a QMFPARM DSQPRINT
       JUMP_LABEL=CFQ11YAK
       ;;
(CFQ11YAK)
       m_CondExec 04,GE,CFQ11YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LISTE POUR LA VALEUR DES STOCKS FOURNITURE QFOURM                          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CFQ11YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CFQ11YAM
       ;;
(CFQ11YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QFOURM DSQPRINT
       m_FileAssign -d SHR QMFPARM ${DATA}/CORTEX4.P.MTXTFIX1/QFOURMY1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QFOURMY1 -a QMFPARM DSQPRINT
       JUMP_LABEL=CFQ11YAN
       ;;
(CFQ11YAN)
       m_CondExec 04,GE,CFQ11YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
