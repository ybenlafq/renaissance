#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  KLEE4P.ksh                       --- VERSION DU 17/10/2016 18:26
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPKLEE4 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/10/15 AT 10.28.57 BY BURTECA                      
#    STANDARDS: P  JOBSET: KLEE4P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DE LA TABLE DES STOCKS RTSL11 & RTMU06                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=KLEE4PA
       ;;
(KLEE4PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=KLEE4PAA
       ;;
(KLEE4PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
#    RSSL11   : NAME=RSSL11,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE4PAA.BUNSTOAP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE4PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  TRI-SUM DU FICHIER D'UNLOAD                                                 
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PAD
       ;;
(KLEE4PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/KLEE4PAA.BUNSTOAP
       m_FileAssign -d NEW,CATLG,DELETE -r 7 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEE4PAD.BUNSTOBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE4PAE
       ;;
(KLEE4PAE)
       m_CondExec 00,EQ,KLEE4PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BCE611 : EXTRACTION DES CODICS                                              
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PAG
       ;;
(KLEE4PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSCE52   : NAME=RSCE52,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSCE52 /dev/null
# *   FICHIER DATE                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FSTOCKS ${DATA}/PTEM/KLEE4PAD.BUNSTOBP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 FCODICS ${DATA}/PTEM/KLEE4PAG.BCE611AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCE611 
       JUMP_LABEL=KLEE4PAH
       ;;
(KLEE4PAH)
       m_CondExec 04,GE,KLEE4PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI-SUM DU FICHIER CODICS                                                   
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PAJ
       ;;
(KLEE4PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/KLEE4PAG.BCE611AP
# * FICHIER CODICS TRIE ALLANT VERS KLEE5P                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BCE611BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /KEYS
   FLD_CH_1_7 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE4PAK
       ;;
(KLEE4PAK)
       m_CondExec 00,EQ,KLEE4PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE DES PRIX/PRIMES/ECOTAXE                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PAM PGM=PTLDRIVM   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PAM
       ;;
(KLEE4PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE4PAM.BUNPRXAP
# *                                                                            
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE4PAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FICHIER PRIX                                                         
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PAQ
       ;;
(KLEE4PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/KLEE4PAM.BUNPRXAP
# * FICHIER CODICS TRIE ALLANT VERS KLEE5?                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEE4PAQ.BUNPRXBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "PX"
 /FIELDS FLD_CH_14_2 14 CH 02
 /FIELDS FLD_CH_1_13 01 CH 13
 /CONDITION CND_1 FLD_CH_14_2 EQ CST_1_4 
 /KEYS
   FLD_CH_1_13 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE4PAR
       ;;
(KLEE4PAR)
       m_CondExec 00,EQ,KLEE4PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER PRIME                                                        
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PAT
       ;;
(KLEE4PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/KLEE4PAM.BUNPRXAP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEE4PAT.BUNPRMBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "PM"
 /FIELDS FLD_CH_14_2 14 CH 02
 /FIELDS FLD_CH_1_13 01 CH 13
 /CONDITION CND_1 FLD_CH_14_2 EQ CST_1_4 
 /KEYS
   FLD_CH_1_13 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE4PAU
       ;;
(KLEE4PAU)
       m_CondExec 00,EQ,KLEE4PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER ECO TAXE                                                     
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PAX
       ;;
(KLEE4PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/KLEE4PAM.BUNPRXAP
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/KLEE4PAX.BUNECTBP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_8 "PM"
 /DERIVEDFIELD CST_1_4 "PX"
 /FIELDS FLD_CH_1_13 01 CH 13
 /FIELDS FLD_CH_14_2 14 CH 02
 /CONDITION CND_1 FLD_CH_14_2 EQ CST_1_4 OR FLD_CH_14_2 EQ CST_3_8 
 /KEYS
   FLD_CH_1_13 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE4PAY
       ;;
(KLEE4PAY)
       m_CondExec 00,EQ,KLEE4PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTNV15                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PBA PGM=PTLDRIVM   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PBA
       ;;
(KLEE4PBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d NEW,CATLG,DELETE -r 31 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE4PBA.BUNV15AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE4PBA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  BCE612 : CONSTITUTION DE REFERENTIEL PRIX                                   
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PBD
       ;;
(KLEE4PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
# *   FICHIER DATE                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# *                                                                            
# ***    FICHIER ALLANT VERS KLEE5R                                            
       m_FileAssign -d SHR -g ${G_A7} FCODICS ${DATA}/PXX0/F07.BCE611BP
       m_FileAssign -d SHR -g ${G_A8} FPRIX ${DATA}/PTEM/KLEE4PAQ.BUNPRXBP
       m_FileAssign -d SHR -g ${G_A9} FPRIME ${DATA}/PTEM/KLEE4PAT.BUNPRMBP
       m_FileAssign -d SHR -g ${G_A10} FECO ${DATA}/PTEM/KLEE4PAX.BUNECTBP
       m_FileAssign -d SHR -g ${G_A11} FSTATUT ${DATA}/PTEM/KLEE4PBA.BUNV15AP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 91 -t LSEQ -g +1 FETQREF ${DATA}/PXX0/F07.BCE612BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCE612 
       JUMP_LABEL=KLEE4PBE
       ;;
(KLEE4PBE)
       m_CondExec 04,GE,KLEE4PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER                                                              
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE4PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PBG
       ;;
(KLEE4PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PXX0/F07.BCE612BP
# ***    FICHIER ENVOI VIA KLEE6P & KLEE5P                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 91 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BCE612CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_7_7 07 CH 07
 /FIELDS FLD_CH_1_6 01 CH 06
 /KEYS
   FLD_CH_7_7 ASCENDING,
   FLD_CH_1_6 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE4PBH
       ;;
(KLEE4PBH)
       m_CondExec 00,EQ,KLEE4PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=KLEE4PZA
       ;;
(KLEE4PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE4PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
