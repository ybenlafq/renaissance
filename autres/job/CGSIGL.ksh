#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CGSIGL.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLCGSIG -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/07 AT 16.02.04 BY BURTECA                      
#    STANDARDS: P  JOBSET: CGSIGL                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DES FICHIERS ECS                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CGSIGLA
       ;;
(CGSIGLA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/08/07 AT 16.02.04 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CGSIGL                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'CONVERGENCE'                           
# *                           APPL...: REPLILLE                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CGSIGLAA
       ;;
(CGSIGLAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# *********************************                                            
# ------  FICHIER GCT ISSU DE SIGCTL DDNAME=FGCT (LONGUEUR 250)                
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGL/F61.SIGGCTL
# ******  FICHIER GCT ISSU DE SIGC2L DDNAME=FGCT (LONGUEUR 250)                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGL/F61.SIGGC2L
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CGSIGLAA.CGSIGDL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGSIGLAB
       ;;
(CGSIGLAB)
       m_CondExec 00,EQ,CGSIGLAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS ECS                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLAD
       ;;
(CGSIGLAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------                                                                       
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/CGSIGLAA.CGSIGDL
# ******  FICHIER DE RECYCLAGE                                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.CGSIGL0
# ******  FICHIER DE REPRISE                                                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.CGSIGL.REPRISE
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CGSIGLAD.CGSIGL1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_235_1 235 CH 1
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_235_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGSIGLAE
       ;;
(CGSIGLAE)
       m_CondExec 00,EQ,CGSIGLAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG010                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLAG
       ;;
(CGSIGLAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FIC ISSU DU TRI                                                       
       m_FileAssign -d SHR -g ${G_A2} FFTV02 ${DATA}/PTEM/CGSIGLAD.CGSIGL1
# ******  TABLE EN LECTURE                                                     
#    RSGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSFM99   : NAME=RSFM99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFM99 /dev/null
# ****** FICHIERS EN SORTIE POUR LE BCG020 ET LE BCG030                        
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FICANO ${DATA}/PTEM/CGSIGLAG.CGSIGL2
# ****** FICHIERS EN SORTIE POUR LE PGM BCG020                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FCG010 ${DATA}/PTEM/CGSIGLAG.CGSIGL3
# ------  ETAT DES ANOMALIES                                                   
       m_OutputAssign -c 9 -w ICG010 ICG010
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG010 
       JUMP_LABEL=CGSIGLAH
       ;;
(CGSIGLAH)
       m_CondExec 04,GE,CGSIGLAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BCG020                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLAJ
       ;;
(CGSIGLAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FICANO ${DATA}/PTEM/CGSIGLAG.CGSIGL2
       m_FileAssign -d SHR -g ${G_A4} FCG010 ${DATA}/PTEM/CGSIGLAG.CGSIGL3
# ****** FICHIER  EN SORTIE VER XFB GATEWAY                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 FCG020 ${DATA}/PXX0/F61.CGSIGL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG020 
       JUMP_LABEL=CGSIGLAK
       ;;
(CGSIGLAK)
       m_CondExec 04,GE,CGSIGLAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG030                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLAM PGM=BCG030     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLAM
       ;;
(CGSIGLAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A5} FFTV02 ${DATA}/PTEM/CGSIGLAD.CGSIGL1
       m_FileAssign -d SHR -g ${G_A6} FICANO ${DATA}/PTEM/CGSIGLAG.CGSIGL2
# ****** FICHIER  EN SORTIE POUR RECYCLAGE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FREPRIS ${DATA}/PXX0/F61.CGSIGL0
       m_ProgramExec BCG030 
#                                                                              
# ********************************************************************         
#  ENVOI DU FICHIER VERS GATEWAY POUR ROUTAGE VERS SAP                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SAPXXXP,                                                            
#      FNAME=":CGSIGL""(0),                                                    
#      NFNAME=CGSIGL_&FDATE&FTIME                                              
#          DATAEND                                                             
# ******************************************************************           
#  REMISE A ZERO DU FICHIER DE REPRISE CGSIGLR                                 
#                   FICHIER BSIG25DL DDNAME=FFDC                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLAQ
       ;;
(CGSIGLAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F61.CGSIGL.REPRISE
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 OUT2 ${DATA}/PNCGL/F61.SIGAFDC
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGLAQ.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CGSIGLAR
       ;;
(CGSIGLAR)
       m_CondExec 16,NE,CGSIGLAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHANGEMENT PCL => CGSIGL <= CHANGEMENT PCL                                  
# ###############################################################              
# ###########           DCLF _A CR��R                    #########              
# ###############################################################              
# FTCGSIGL  DCLF LRECL=080,RECNB=5,CLASS=XP,RECFM=FB                           
# ###############################################################              
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FTCGSIGL                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLAT
       ;;
(CGSIGLAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGLAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/CGSIGLAT.FTCGSIGL
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCGSIGL                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLAX
       ;;
(CGSIGLAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGLAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.CGSIGLAT.FTCGSIGL(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP CGSIGLBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLBA
       ;;
(CGSIGLBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGLBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGLZA
       ;;
(CGSIGLZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGLZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
