#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  KLEE7P.ksh                       --- VERSION DU 13/10/2016 16:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPKLEE7 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/04/18 AT 09.02.36 BY BURTECA                      
#    STANDARDS: P  JOBSET: KLEE7P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BCE617 : EXTRACTION AFIN D ALIMENTER LA TABLE RTCE13                        
#                                                                              
#   REPRISE: NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=KLEE7PA
       ;;
(KLEE7PA)
#
#KLEE7PAG
#KLEE7PAG Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#KLEE7PAG
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2012/04/18 AT 09.02.36 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: KLEE7P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALIM DE RTCE13'                        
# *                           APPL...: REPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=KLEE7PAA
       ;;
(KLEE7PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# **      TABLES DB2 EN LECTURE                                                
#    RSLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSLI00 /dev/null
#    RSGA58   : NAME=RSGA58,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA58 /dev/null
#    RSRM85   : NAME=RSRM85,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRM85 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -t LSEQ -g +1 FRTCE13 ${DATA}/PTEM/KLEE7PAA.LOACE13P
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCE617 
       JUMP_LABEL=KLEE7PAB
       ;;
(KLEE7PAB)
       m_CondExec 04,GE,KLEE7PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP KLEE7PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
#{ Post-translation ADAPT_LOAD_REPLACE
#       JUMP_LABEL=KLEE7PAD
#       ;;
#(KLEE7PAD)
#       m_CondExec ${EXAAF},NE,YES 
#       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
#       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
#       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
#       m_OutputAssign -c "*" ST01MSG
#       m_OutputAssign -c "*" PTIMSG01
#       m_OutputAssign -c "*" ST02MSG
#       m_OutputAssign -c "*" PTIMSG03
#       m_OutputAssign -c "*" ST03MSG
#       m_OutputAssign -c "*" PTIMSG02
#       m_OutputAssign -c "*" PTIMSG
#       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
##                                                                              
## ******* FIC DE LOAD DE RTCE13                                                
##                                                                              
#       m_FileAssign -d SHR -g ${G_A1} SYSULD ${DATA}/PTEM/KLEE7PAA.LOACE13P
##    RSCE13   : NAME=RSCE13,MODE=(U,N) - DYNAM=YES                             
#       m_FileAssign -d SHR RSCE13 /dev/null
##                                                                              
#       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE7PAD.sysin
#       
#       #Untranslated utility: PTLDRIVM
     JUMP_LABEL=KLEE7PAD_SORT
       ;;
(KLEE7PAD_SORT)
     m_CondExec ${EXAAF},NE,YES 
     m_OutputAssign -c "*" SYSOUT
     m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/KLEE7PAA.LOACE13P
     m_FileAssign -d NEW,CATLG,DELETE SORTOUT ${MT_TMP}/KLEE7P_KLEE7PAD_RTCE13_${MT_JOB_NAME}_${MT_JOB_PID}
     m_FileAssign -i SYSIN
/PADBYTE x"20"
/FIELDS FLD_CH_1_3 1 CH 3
/FIELDS FLD_CH_4_3 4 CH 3
/FIELDS FLD_CH_7_5 7 CH 5 
/FIELDS FLD_CH_12_7 12 CH 7 
/KEYS
   FLD_CH_1_3  ASCENDING,
   FLD_CH_4_3  ASCENDING,
   FLD_CH_7_5  ASCENDING,
   FLD_CH_12_7 ASCENDING
_end
     m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=KLEE7PAD
       ;;
(KLEE7PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_FileAssign -d SHR PTILIB ${DATA}/SYS2.KRYSTAL.LOADLIB
       m_FileAssign -d SHR PTIPARM ${DATA}/SYS2.KRYSTAL.PARMLIB
       m_FileAssign -d SHR PTIXMSG ${DATA}/SYS2.KRYSTAL.XMESSAGE
       m_OutputAssign -c "*" ST01MSG
       m_OutputAssign -c "*" PTIMSG01
       m_OutputAssign -c "*" ST02MSG
       m_OutputAssign -c "*" PTIMSG03
       m_OutputAssign -c "*" ST03MSG
       m_OutputAssign -c "*" PTIMSG02
       m_OutputAssign -c "*" PTIMSG
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT101 ${MT_TMP}/SYSUT101_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT102 ${MT_TMP}/SYSUT102_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT103 ${MT_TMP}/SYSUT103_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT104 ${MT_TMP}/SYSUT104_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SORTWK01 ${MT_TMP}/SORTWK01_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSERR ${MT_TMP}/SYSERR_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSDISC ${MT_TMP}/SYSDISC_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
#                                                                              
# ******* FIC DE LOAD DE RTCE13                                                
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE7PAD.sysin
       m_FileAssign -d SHR SYSULD ${MT_TMP}/KLEE7P_KLEE7PAD_RTCE13_${MT_JOB_NAME}_${MT_JOB_PID}
#    RSCE13   : NAME=RSCE13,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSCE13 /dev/null
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/KLEE7P_KLEE7PAD_RTCE13.sysload
       m_UtilityExec
#} Post-translation
# ********************************************************************         
#   REPAIR NOCOPY DU PREMIER TABLESPACE                                        
#       (REMISE DU STATUS EN READ WRITE DU TABLESPACE)                         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE7PAG PGM=DSNUTILB   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=KLEE7PAJ
       ;;
(KLEE7PAJ)
       m_CondExec ${EXAAP},NE,YES 
#    RSCE13   : NAME=RSCE13,MODE=I - DYNAM=YES                                 
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
#    RSGA30   : NAME=RSGA30,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 14 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE7PAJ.UNLCE13P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE7PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  BCE618 :  CREATION DU FICHIER D ASSORTIMENT                                 
#                                                                              
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE7PAM PGM=BCE618     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=KLEE7PAM
       ;;
(KLEE7PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# *                                                                            
       m_FileAssign -d SHR -g ${G_A2} FCE618D ${DATA}/PTEM/KLEE7PAJ.UNLCE13P
       m_FileAssign -d SHR -g +0 FCE618S ${DATA}/PXX0/F07.FCE652CP
# ***           C EST CE FICHIER QUI PASSE DANS LA CHAINE KLEE6P               
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -t LSEQ -g +1 FCE618 ${DATA}/PXX0/F07.FCE652DP
       m_ProgramExec BCE618 
# *******************************************************************          
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=KLEE7PZA
       ;;
(KLEE7PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE7PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
