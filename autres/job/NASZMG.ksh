#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NASZMG.ksh                       --- VERSION DU 08/10/2016 23:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGNASZM -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/11/24 AT 10.00.22 BY BURTEC9                      
#    STANDARDS: P  JOBSET: NASZMG                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  REMISE A ZERO DES  GDG DE R�CUP CUMUL�S MENSUEL  NASL                       
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NASZMGA
       ;;
(NASZMGA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2004/11/24 AT 10.00.22 BY BURTEC9                
# *    JOBSET INFORMATION:    NAME...: NASZMG                                  
# *                           FREQ...: M                                       
# *                           TITLE..: 'RAZ MENSUEL NASL'                      
# *                           APPL...: REPEXPL                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NASZMGAA
       ;;
(NASZMGAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
       m_FileAssign -d SHR INCT1 /dev/null
       m_FileAssign -d SHR INIDG /dev/null
       m_FileAssign -d SHR INFDC /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUTCT1MD ${DATA}/PXX0/F91.FSLST1MD
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUTCT1ML ${DATA}/PXX0/F61.FSLST1ML
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUTCT1MM ${DATA}/PXX0/F89.FSLST1MM
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUTCT1MO ${DATA}/PXX0/F16.FSLST1MO
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUTCT1MP ${DATA}/PXX0/F07.FSLST1MP
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUTCT1MY ${DATA}/PXX0/F45.FSLST1MY
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -g +1 OUTIDGD ${DATA}/PXX0/F91.NASCIDGD
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -g +1 OUTIDGL ${DATA}/PXX0/F61.NASCIDGL
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -g +1 OUTIDGM ${DATA}/PXX0/F89.NASCIDGM
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -g +1 OUTIDGO ${DATA}/PXX0/F16.NASCIDGO
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -g +1 OUTIDGP ${DATA}/PXX0/F07.NASCIDGP
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -g +1 OUTIDGY ${DATA}/PXX0/F45.NASCIDGY
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 OUTFDCD ${DATA}/PXX0/F91.NASCFDCD
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 OUTFDCL ${DATA}/PXX0/F61.NASCFDCL
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 OUTFDCM ${DATA}/PXX0/F89.NASCFDCM
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 OUTFDCO ${DATA}/PXX0/F16.NASCFDCO
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 OUTFDCP ${DATA}/PXX0/F07.NASCFDCP
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 OUTFDCY ${DATA}/PXX0/F45.NASCFDCY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NASZMGAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NASZMGAB
       ;;
(NASZMGAB)
       m_CondExec 16,NE,NASZMGAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
