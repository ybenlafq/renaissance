#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM00Y.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYIVM00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/09/11 AT 10.43.29 BY PREPA3                       
#    STANDARDS: P  JOBSET: IVM00Y                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  BIV101 : EXTRACTION DES CODICS DEMANDES                                     
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM00YA
       ;;
(IVM00YA)
       DEPOT=${DEPOT}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       MAGASIN=${MAGASIN}
       RUN=${RUN}
       JUMP_LABEL=IVM00YAA
       ;;
(IVM00YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RTGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00Y /dev/null
#    RTGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01Y /dev/null
#    RTGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10Y /dev/null
#    RTGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14Y /dev/null
#    RTGA30Y  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30Y /dev/null
#    RTGS10Y  : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS10Y /dev/null
#    RTGS30Y  : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGS30Y /dev/null
#    RTGD31Y  : NAME=RSGD31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGD31Y /dev/null
#                                                                              
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  FICHIERS PARAMETRES                                                  
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FIN100 ${DATA}/PBI0/IVM00YAA.BIV100AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV101 
       JUMP_LABEL=IVM00YAB
       ;;
(IVM00YAB)
       m_CondExec 04,GE,IVM00YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV102 : EDITION ETIQUETTES INVENTAIRE                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00YAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM00YAD
       ;;
(IVM00YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE GENERALISEE (SS TABLES:INVED VUE RVGA01S7)                     
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  LIBELLES DES FAMILLES                                                
#    RSGA14Y  : NAME=RSGA14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14Y /dev/null
# ******  FAMILLES                                                             
#    RSGA30Y  : NAME=RSGA30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30Y /dev/null
# ******  STOCK MAG                                                            
#    RSGS30Y  : NAME=RSGS30Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30Y /dev/null
# ******  STOCK                                                                
#    RSGS60Y  : NAME=RSGS60Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60Y /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FIC SOCIETE                                                          
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 FIN100 ${DATA}/PXX0/F45.BIV102AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV102 
       JUMP_LABEL=IVM00YAE
       ;;
(IVM00YAE)
       m_CondExec 04,GE,IVM00YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC D'EXTRACTION                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00YAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM00YAG
       ;;
(IVM00YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PBI0/IVM00YAA.BIV100AY
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F45.BIV102AY
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PBI0/IVM00YAG.BIV105AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "1"
 /FIELDS FLD_CH_16_15 16 CH 15
 /FIELDS FLD_CH_2_14 2 CH 14
 /FIELDS FLD_CH_97_25 97 CH 25
 /FIELDS FLD_CH_1_1 01 CH 01
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_6 
 /KEYS
   FLD_CH_2_14 ASCENDING,
   FLD_CH_97_25 ASCENDING,
   FLD_CH_16_15 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM00YAH
       ;;
(IVM00YAH)
       m_CondExec 00,EQ,IVM00YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV105 : EDITION DES ETIQUETTES AUTOCOLLANTES QUE LE DEPOT                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00YAJ PGM=BIV105     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM00YAJ
       ;;
(IVM00YAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[DEPOT] 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DES ETIQUETTES AUTOCOLLANTES                                 
       m_FileAssign -d SHR -g ${G_A3} FIN105 ${DATA}/PBI0/IVM00YAG.BIV105AY
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV105 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  BIV105 : EDITION DES ETIQUETTES AUTOCOLLANTES QUE LES MAGASINS              
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00YAM PGM=BIV105     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVM00YAM
       ;;
(IVM00YAM)
       m_CondExec ${EXAAU},NE,YES 1,EQ,$[MAGASIN] 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  FICHIER DES ETIQUETTES AUTOCOLLANTES                                 
       m_FileAssign -d SHR -g ${G_A4} FIN105 ${DATA}/PBI0/IVM00YAG.BIV105AY
# ******  FICHIER FDATE                                                        
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV106 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  TRI DU FICHIER D'EXTRACTION                                                 
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM00YAQ
       ;;
(IVM00YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PBI0/IVM00YAA.BIV100AY
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PXX0/F45.BIV102AY
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -g +1 SORTOUT ${DATA}/PGI945/F45.BIV110AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "2"
 /FIELDS FLD_CH_1_1 01 CH 01
 /FIELDS FLD_CH_2_26 02 CH 26
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_4 
 /KEYS
   FLD_CH_2_26 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM00YAR
       ;;
(IVM00YAR)
       m_CondExec 00,EQ,IVM00YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV110 : EDITION DES ETATS PREREMPLIS PAR MAG/RAYON                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00YAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVM00YAT
       ;;
(IVM00YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RTGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10Y /dev/null
# ******  FICHIER D'EXTRACTION TRI�                                            
       m_FileAssign -d SHR -g ${G_A7} FIN110 ${DATA}/PGI945/F45.BIV110AY
# ******  FICHIER PARAMETRE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITION DES ETATS PRE-REMPLIS PAR MAG ET RAYON                       
       m_OutputAssign -c 9 -w IIV110 IIV110
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV110 
       JUMP_LABEL=IVM00YAU
       ;;
(IVM00YAU)
       m_CondExec 04,GE,IVM00YAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM00YZA
       ;;
(IVM00YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM00YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
