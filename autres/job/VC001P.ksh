#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  VC001P.ksh                       --- VERSION DU 17/10/2016 18:27
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPVC001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 01/01/22 AT 15.46.52 BY BURTEC7                      
#    STANDARDS: P  JOBSET: VC001P                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  TRAITEMENT JOURNALIER                                                       
#  EDITION DES DONNEES DU JOUR                                                 
# ********************************************************************         
#  BVC001 : REQUETES SUR LES TABLES RTHV15, RTGS40, RTFT12 ET RTFT21           
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=VC001PA
       ;;
(VC001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=VC001PAA
       ;;
(VC001PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -d SHR FPER ${DATA}/CORTEX4.P.MTXTFIX1/VC001PAA
#                                                                              
#    RSHV15   : NAME=RSHV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSFT12   : NAME=RSFT12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT12 /dev/null
#    RSFT21   : NAME=RSFT21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT21 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 FVC001 ${DATA}/PXX0/VC001PAA.BVC001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -g +1 FVC002 ${DATA}/PXX0/VC001PAA.BVC002AP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 FVC003 ${DATA}/PXX0/VC001PAA.BVC003AP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVC001 
       JUMP_LABEL=VC001PAB
       ;;
(VC001PAB)
       m_CondExec 04,GE,VC001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVC001                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=VC001PAD
       ;;
(VC001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PXX0/VC001PAA.BVC001AP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PXX0/VC001PAD.BVC001BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_PD_18_3 18 PD 3
 /FIELDS FLD_PD_15_3 15 PD 3
 /KEYS
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_15_3,
    TOTAL FLD_PD_18_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PAE
       ;;
(VC001PAE)
       m_CondExec 00,EQ,VC001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVC002                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=VC001PAG
       ;;
(VC001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PXX0/VC001PAA.BVC002AP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -g +1 SORTOUT ${DATA}/PXX0/VC001PAG.BVC002BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_18_4 18 PD 4
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_PD_34_8 34 PD 8
 /FIELDS FLD_PD_22_4 22 PD 4
 /FIELDS FLD_PD_26_8 26 PD 8
 /KEYS
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_18_4,
    TOTAL FLD_PD_22_4,
    TOTAL FLD_PD_26_8,
    TOTAL FLD_PD_34_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PAH
       ;;
(VC001PAH)
       m_CondExec 00,EQ,VC001PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVC003                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=VC001PAJ
       ;;
(VC001PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PXX0/VC001PAA.BVC003AP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 SORTOUT ${DATA}/PXX0/VC001PAJ.BVC003BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_40_7 40 PD 7
 /FIELDS FLD_PD_19_7 19 PD 7
 /FIELDS FLD_PD_26_7 26 PD 7
 /FIELDS FLD_CH_1_9 1 CH 9
 /FIELDS FLD_PD_33_7 33 PD 7
 /KEYS
   FLD_CH_1_9 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_19_7,
    TOTAL FLD_PD_26_7,
    TOTAL FLD_PD_33_7,
    TOTAL FLD_PD_40_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PAK
       ;;
(VC001PAK)
       m_CondExec 00,EQ,VC001PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVC002                                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PAM PGM=BVC002     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=VC001PAM
       ;;
(VC001PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A4} FVC001 ${DATA}/PXX0/VC001PAD.BVC001BP
       m_FileAssign -d SHR -g ${G_A5} FVC002 ${DATA}/PXX0/VC001PAG.BVC002BP
       m_FileAssign -d SHR -g ${G_A6} FVC003 ${DATA}/PXX0/VC001PAJ.BVC003BP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 FVC004 ${DATA}/PXX0/VC001PAM.BVC004AP
       m_ProgramExec BVC002 
# ********************************************************************         
#  TRI DU FICHIER FVC004                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=VC001PAQ
       ;;
(VC001PAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PXX0/VC001PAM.BVC004AP
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PXX0/VC001PAQ.BVC004BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_5 17 CH 5
 /FIELDS FLD_CH_27_5 27 CH 5
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_CH_56_8 56 CH 8
 /FIELDS FLD_CH_32_8 32 CH 8
 /FIELDS FLD_CH_64_8 64 CH 8
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_22_5 22 CH 5
 /FIELDS FLD_CH_40_8 40 CH 8
 /FIELDS FLD_CH_7_5 7 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_7_5,
    TOTAL FLD_CH_12_5,
    TOTAL FLD_CH_17_5,
    TOTAL FLD_CH_22_5,
    TOTAL FLD_CH_27_5,
    TOTAL FLD_CH_32_8,
    TOTAL FLD_CH_40_8,
    TOTAL FLD_CH_48_8,
    TOTAL FLD_CH_56_8,
    TOTAL FLD_CH_64_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PAR
       ;;
(VC001PAR)
       m_CondExec 00,EQ,VC001PAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVC003 : EDITION DES MAGASINS EN �CART                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PAT PGM=BVC003     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=VC001PAT
       ;;
(VC001PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPER ${DATA}/CORTEX4.P.MTXTFIX1/VC001PAT
#                                                                              
       m_FileAssign -d SHR -g ${G_A8} FVC004 ${DATA}/PXX0/VC001PAM.BVC004AP
       m_FileAssign -d SHR -g ${G_A9} FVC005 ${DATA}/PXX0/VC001PAQ.BVC004BP
#                                                                              
       m_OutputAssign -c 9 -w BVC003 EDITION
       m_ProgramExec BVC003 
# ********************************************************************         
#  TRAITEMENT JOURNALIER                                                       
#  EDITION DES DONN�ES CUMUL�ES                                                
# ********************************************************************         
#  BVC001 : REQUETES SUR LES TABLES RTHV15, RTGS40, RTFT12 ET RTFT21           
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=VC001PAX
       ;;
(VC001PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDIF
       m_FileAssign -d SHR FPER ${DATA}/CORTEX4.P.MTXTFIX1/VC001PAX
#                                                                              
#    RSHV15   : NAME=RSHV15,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSHV15 /dev/null
#    RSGS40   : NAME=RSGS40,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGS40 /dev/null
#    RSFT12   : NAME=RSFT12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT12 /dev/null
#    RSFT21   : NAME=RSFT21,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFT21 /dev/null
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 FVC001 ${DATA}/PXX0/VC001PAX.BVC001CP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -g +1 FVC002 ${DATA}/PXX0/VC001PAX.BVC002CP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 FVC003 ${DATA}/PXX0/VC001PAX.BVC003CP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVC001 
       JUMP_LABEL=VC001PAY
       ;;
(VC001PAY)
       m_CondExec 04,GE,VC001PAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVC001                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=VC001PBA
       ;;
(VC001PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PXX0/VC001PAX.BVC001CP
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -g +1 SORTOUT ${DATA}/PXX0/VC001PBA.BVC001DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_18_3 18 PD 3
 /FIELDS FLD_PD_15_3 15 PD 3
 /FIELDS FLD_CH_1_6 1 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_15_3,
    TOTAL FLD_PD_18_3
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PBB
       ;;
(VC001PBB)
       m_CondExec 00,EQ,VC001PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVC002                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=VC001PBD
       ;;
(VC001PBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/VC001PAX.BVC002CP
       m_FileAssign -d NEW,CATLG,DELETE -r 41 -g +1 SORTOUT ${DATA}/PXX0/VC001PBD.BVC002DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_26_8 26 PD 8
 /FIELDS FLD_PD_18_4 18 PD 4
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_PD_34_8 34 PD 8
 /FIELDS FLD_PD_22_4 22 PD 4
 /KEYS
   FLD_CH_1_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_18_4,
    TOTAL FLD_PD_22_4,
    TOTAL FLD_PD_26_8,
    TOTAL FLD_PD_34_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PBE
       ;;
(VC001PBE)
       m_CondExec 00,EQ,VC001PBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FVC003                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=VC001PBG
       ;;
(VC001PBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PXX0/VC001PAX.BVC003CP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 SORTOUT ${DATA}/PXX0/VC001PBG.BVC003DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_19_7 19 PD 7
 /FIELDS FLD_PD_26_7 26 PD 7
 /FIELDS FLD_CH_1_9 1 CH 9
 /FIELDS FLD_PD_33_7 33 PD 7
 /FIELDS FLD_PD_40_7 40 PD 7
 /KEYS
   FLD_CH_1_9 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_19_7,
    TOTAL FLD_PD_26_7,
    TOTAL FLD_PD_33_7,
    TOTAL FLD_PD_40_7
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PBH
       ;;
(VC001PBH)
       m_CondExec 00,EQ,VC001PBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVC002                                                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PBJ PGM=BVC002     ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=VC001PBJ
       ;;
(VC001PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A13} FVC001 ${DATA}/PXX0/VC001PBA.BVC001DP
       m_FileAssign -d SHR -g ${G_A14} FVC002 ${DATA}/PXX0/VC001PBD.BVC002DP
       m_FileAssign -d SHR -g ${G_A15} FVC003 ${DATA}/PXX0/VC001PBG.BVC003DP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 FVC004 ${DATA}/PXX0/VC001PBJ.BVC004CP
       m_ProgramExec BVC002 
# ********************************************************************         
#  TRI DU FICHIER FVC004                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=VC001PBM
       ;;
(VC001PBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/VC001PBJ.BVC004CP
       m_FileAssign -d NEW,CATLG,DELETE -r 71 -g +1 SORTOUT ${DATA}/PXX0/VC001PBM.BVC004DP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_17_5 17 CH 5
 /FIELDS FLD_CH_12_5 12 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_22_5 22 CH 5
 /FIELDS FLD_CH_32_8 32 CH 8
 /FIELDS FLD_CH_27_5 27 CH 5
 /FIELDS FLD_CH_64_8 64 CH 8
 /FIELDS FLD_CH_56_8 56 CH 8
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_7_5 7 CH 5
 /FIELDS FLD_CH_40_8 40 CH 8
 /KEYS
   FLD_CH_1_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_CH_7_5,
    TOTAL FLD_CH_12_5,
    TOTAL FLD_CH_17_5,
    TOTAL FLD_CH_22_5,
    TOTAL FLD_CH_27_5,
    TOTAL FLD_CH_32_8,
    TOTAL FLD_CH_40_8,
    TOTAL FLD_CH_48_8,
    TOTAL FLD_CH_56_8,
    TOTAL FLD_CH_64_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=VC001PBN
       ;;
(VC001PBN)
       m_CondExec 00,EQ,VC001PBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BVC003 : EDITION DES MAGASINS EN �CART                                      
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP VC001PBQ PGM=BVC003     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=VC001PBQ
       ;;
(VC001PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FPER ${DATA}/CORTEX4.P.MTXTFIX1/VC001PBQ
#                                                                              
       m_FileAssign -d SHR -g ${G_A17} FVC004 ${DATA}/PXX0/VC001PBJ.BVC004CP
       m_FileAssign -d SHR -g ${G_A18} FVC005 ${DATA}/PXX0/VC001PBM.BVC004DP
#                                                                              
       m_OutputAssign -c 9 -w BVC003M EDITION
       m_ProgramExec BVC003 
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=VC001PZA
       ;;
(VC001PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/VC001PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
