#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  DXFTPF.ksh                       --- VERSION DU 08/10/2016 12:43
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFDXFTP -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 12/06/13 AT 11.24.20 BY BURTEC9                      
#    STANDARDS: P  JOBSET: DXFTPF                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# *******************************************************************          
#  ELIMINATION DES DOUBLONS                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=DXFTPFA
       ;;
(DXFTPFA)
       EXAAA=${EXAAA:-0}
       EXAAP=${EXAAP:-0}
       EXABY=${EXABY:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2012/06/13 AT 11.24.20 BY BURTEC9                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: DXFTPF                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'MERGE NDM/FTPNAS'                      
# *                           APPL...: REPAPPL                                 
# *                           WAIT...: DXFTPF                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=DXFTPFAA
       ;;
(DXFTPFAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# **************************************                                       
#                                                                              
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGF/F99.M5I351.NASCFDCF
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGF/F99.M5I351.NASCFDCF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_54 1 CH 54
 /KEYS
   FLD_CH_1_54 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 54 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DXFTPFAB
       ;;
(DXFTPFAB)
       m_CondExec 00,EQ,DXFTPFAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ELIMINATION DES DOUBLONS                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DXFTPFAD PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=DXFTPFAD
       ;;
(DXFTPFAD)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGF/F99.M5I351.NASCIDGF
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -t LSEQ -g +1 SORTOUT ${DATA}/PNCGF/F99.M5I351.NASCIDGF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_104 1 CH 104
 /KEYS
   FLD_CH_1_104 ASCENDING
 /SUMMARIZE
 /* Record Type = F  Record Length = 104 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DXFTPFAE
       ;;
(DXFTPFAE)
       m_CondExec 00,EQ,DXFTPFAD ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ECLATEMENT PAR FILIALES SITE CENTRALISť + CUMUL DE LA VEILLE                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DXFTPFAG PGM=EZTPA00    ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=DXFTPFAG
       ;;
(DXFTPFAG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g +0 NASCFDCF ${DATA}/PXX0/F91.NASCFDCD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.NASCFDCL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.NASCFDCM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.NASCFDCO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.NASCFDCP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.NASCFDCY
       m_FileAssign -d SHR -g ${G_A1} -C ${DATA}/PNCGF/F99.M5I351.NASCFDCF
       m_FileAssign -d SHR -g +0 NASCIDGF ${DATA}/PXX0/F91.NASCIDGD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.NASCIDGL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.NASCIDGM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.NASCIDGO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.NASCIDGP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.NASCIDGY
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PNCGF/F99.M5I351.NASCIDGF
       m_FileAssign -d SHR -g +0 NASCT1SF ${DATA}/PXX0/F91.FSLST1SD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FSLST1SL
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FSLST1SM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FSLST1SO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FSLST1SP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FSLST1SY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGF/F99.M5I351.NASCT1SF
       m_FileAssign -d SHR -g +0 NASCT2SF ${DATA}/PNCGF/F99.M5I351.NASCT2SF
       m_FileAssign -d SHR -g +0 NASCT1MF ${DATA}/PXX0/F91.FSLST1MD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FSLST1ML
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F89.FSLST1MM
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F16.FSLST1MO
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FSLST1MP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.FSLST1MY
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGF/F99.M5I351.NASCT1MF
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 NASCFDCD ${DATA}/PXX0/F91.NASCFDCD
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -t LSEQ -g +1 NASCIDGD ${DATA}/PXX0/F91.NASCIDGD
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1SD ${DATA}/PXX0/F91.FSLST1SD
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 NASCT2SD ${DATA}/PXX0/F91.FSLST2SD
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1MD ${DATA}/PXX0/F91.FSLST1MD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 NASCFDCL ${DATA}/PXX0/F61.NASCFDCL
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -t LSEQ -g +1 NASCIDGL ${DATA}/PXX0/F61.NASCIDGL
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1SL ${DATA}/PXX0/F61.FSLST1SL
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 NASCT2SL ${DATA}/PXX0/F61.FSLST2SL
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1ML ${DATA}/PXX0/F61.FSLST1ML
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 NASCFDCM ${DATA}/PXX0/F89.NASCFDCM
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -t LSEQ -g +1 NASCIDGM ${DATA}/PXX0/F89.NASCIDGM
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1SM ${DATA}/PXX0/F89.FSLST1SM
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 NASCT2SM ${DATA}/PXX0/F89.FSLST2SM
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1MM ${DATA}/PXX0/F89.FSLST1MM
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 NASCFDCO ${DATA}/PXX0/F16.NASCFDCO
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -t LSEQ -g +1 NASCIDGO ${DATA}/PXX0/F16.NASCIDGO
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1SO ${DATA}/PXX0/F16.FSLST1SO
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 NASCT2SO ${DATA}/PXX0/F16.FSLST2SO
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1MO ${DATA}/PXX0/F16.FSLST1MO
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 NASCFDCP ${DATA}/PXX0/F07.NASCFDCP
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -t LSEQ -g +1 NASCIDGP ${DATA}/PXX0/F07.NASCIDGP
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1SP ${DATA}/PXX0/F07.FSLST1SP
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 NASCT2SP ${DATA}/PXX0/F07.FSLST2SP
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1MP ${DATA}/PXX0/F07.FSLST1MP
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 NASCFDCY ${DATA}/PXX0/F45.NASCFDCY
       m_FileAssign -d NEW,CATLG,DELETE -r 104 -t LSEQ -g +1 NASCIDGY ${DATA}/PXX0/F45.NASCIDGY
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1SY ${DATA}/PXX0/F45.FSLST1SY
       m_FileAssign -d NEW,CATLG,DELETE -r 21 -t LSEQ -g +1 NASCT2SY ${DATA}/PXX0/F45.FSLST2SY
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -t LSEQ -g +1 NASCT1MY ${DATA}/PXX0/F45.FSLST1MY
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/DXFTPFAG
       m_ProgramExec DXFTPFAG
# ********************************************************************         
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
