#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  EMMAJO.ksh                       --- VERSION DU 17/10/2016 18:05
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j POEMMAJ -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/10/28 AT 10.35.28 BY BURTECA                      
#    STANDARDS: P  JOBSET: EMMAJO                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BTF600 : EXTRACTION DE LA TABLE GENERALISEE (NEMAT) RTGA01                  
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=EMMAJOA
       ;;
(EMMAJOA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=EMMAJOAA
       ;;
(EMMAJOAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE                                                    
#    RSGA01O  : NAME=RSGA01O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01O /dev/null
# ******* TABLE LIEUX                                                          
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
#                                                                              
# ******* FICHIER D'EXTRACTION DES ARTICLES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF600 ${DATA}/PTEM/EMMAJOAA.MTF600AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF600 
       JUMP_LABEL=EMMAJOAB
       ;;
(EMMAJOAB)
       m_CondExec 04,GE,EMMAJOAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF730 : EXTRACTION TABLE PARAMETRES MAGASIN RTPM00                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJOAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOAD
       ;;
(EMMAJOAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE LIEUX                                                          
#    RSGA10O  : NAME=RSGA10O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10O /dev/null
# ******* TABLE PARAMETRE MAGASIN NEM                                          
#    RSPM00O  : NAME=RSPM00O,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPM00O /dev/null
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* N� MAGASIN A INITIALISER                                             
       m_FileAssign -d SHR FNMAG ${DATA}/CORTEX4.P.MTXTFIX1/EMMAJOAD
#                                                                              
# ******* FICHIER EN SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF730 ${DATA}/PTEM/EMMAJOAD.MTF730AO
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF730 
       JUMP_LABEL=EMMAJOAE
       ;;
(EMMAJOAE)
       m_CondExec 04,GE,EMMAJOAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DE TOUS LES FICHIERS POUR ENVOI DES MAJ                                
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJOAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOAG
       ;;
(EMMAJOAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/EMMAJOAA.MTF600AO
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PTEM/EMMAJOAD.MTF730AO
# ****    MODIF                                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 SORTOUT ${DATA}/PTEM/EMMAJOAG.MTF000XO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_100 1 CH 100
 /KEYS
   FLD_CH_1_100 ASCENDING
 /* Record Type = F  Record Length = 385 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=EMMAJOAH
       ;;
(EMMAJOAH)
       m_CondExec 00,EQ,EMMAJOAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJOAJ PGM=BTF900     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOAJ
       ;;
(EMMAJOAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A3} FTF600 ${DATA}/PTEM/EMMAJOAG.MTF000XO
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/PXX0/F16.MTF000AO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF602 ${DATA}/PXX0/F16.MTF900AO
       m_ProgramExec BTF900 
#                                                                              
# ********************************************************************         
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF900 : COMPARAISON DES FICHIERS J ET J-1 POUR ENVOI MAJ                   
#  DUPLI AVEC FICHIER DE LA VEILLE EN DUMMY                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJOAM PGM=BTF900     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOAM
       ;;
(EMMAJOAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE DU JOUR                                             
       m_FileAssign -d SHR -g ${G_A4} FTF600 ${DATA}/PTEM/EMMAJOAG.MTF000XO
# ******* FICHIER EN ENTRE DE LA VEILLE                                        
       m_FileAssign -d SHR -g +0 FTF601 ${DATA}/PXX0/F16.MTF000AO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******* FICHIER DES MISES A JOUR                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 FTF602 ${DATA}/PTEM/EMMAJOAM.MTF900XO
       m_ProgramExec BTF900 
# ********************************************************************         
#  BTF910 : ENVOI DES MAJ PAR MQSERIES                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJOAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOAQ
       ;;
(EMMAJOAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A5} FTF910 ${DATA}/PXX0/F16.MTF900AO
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/EMMAJO1
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/EMMAJOAQ
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF910 
       JUMP_LABEL=EMMAJOAR
       ;;
(EMMAJOAR)
       m_CondExec 04,GE,EMMAJOAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CENTRALISATION AS400                                                        
# ********************************************************************         
#  BTF915 : ENVOI DES MAJ PAR MQSERIES POUR LA MACHINE CENTRALISEE             
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJOAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOAT
       ;;
(EMMAJOAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER EN ENTRE                                                     
       m_FileAssign -d SHR -g ${G_A6} FTF915 ${DATA}/PTEM/EMMAJOAM.MTF900XO
# ******* PARAMETRE SOCIETE                                                    
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCMGIO
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FPARAM                                                               
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX5/EMMAJO1
# ******* POUR LES MAJ LAISSER A BLANC                                         
       m_FileAssign -d SHR FINIT ${DATA}/CORTEX4.P.MTXTFIX1/EMMAJOAT
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTF915 
       JUMP_LABEL=EMMAJOAU
       ;;
(EMMAJOAU)
       m_CondExec 04,GE,EMMAJOAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  IEBGENER DU FICHIER MTF0000 POUR COMPARAISON DU LENDEMAIN                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP EMMAJOAX PGM=IEBGENER   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOAX
       ;;
(EMMAJOAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
#                                                                              
       m_FileAssign -d SHR -g ${G_A7} SYSUT1 ${DATA}/PTEM/EMMAJOAG.MTF000XO
       m_FileAssign -d NEW,CATLG,DELETE -r 385 -g +1 SYSUT2 ${DATA}/PXX0/F16.MTF000AO
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=EMMAJOAY
       ;;
(EMMAJOAY)
       m_CondExec 00,EQ,EMMAJOAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=EMMAJOZA
       ;;
(EMMAJOZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/EMMAJOZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
