#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CRMAGP.ksh                       --- VERSION DU 09/10/2016 05:39
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCRMAG -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/08/05 AT 18.11.15 BY BURTECC                      
#    STANDARDS: P  JOBSET: CRMAGP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   BCRMAG : EXTRACTION DES CODICS BRUNS                                       
#   REPRISE:OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=CRMAGPA
       ;;
(CRMAGPA)
       EXAAA=${EXAAA:-0}
       RUN=${RUN}
       JUMP_LABEL=CRMAGPAA
       ;;
(CRMAGPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE GENERALISE ( MAGVE )                                           
#    RTGA00   : NAME=RSGA00,MODE=(U,I) - DYNAM=YES                             
# -X-RSGA00   - IS UPDATED IN PLACE WITH MODE=(U,I)                            
       m_FileAssign -d SHR RTGA00 /dev/null
# ******  TABLE DES LIEUX                                                      
#    RTLI00   : NAME=RSLI00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI00 /dev/null
#    RTLI01   : NAME=RSLI01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTLI01 /dev/null
#                                                                              
# ******  FDATE JJMMSSAA ******DATE DU JOUR DE TRAITEMENT                      
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   PARAMETRE MAGASIN REFERENCE                                          
       m_FileAssign -i FMAGREF
$MAGREF
_end
#                                                                              
# *****   PARAMETRE MAGASIN � CR�ER                                            
       m_FileAssign -i FNEWMAG
$FRANCH
$MAG1
_end
#                                                                              
#                                                                              
# ********************************************                                 
# ********************************************                                 
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCRMAG 
       JUMP_LABEL=CRMAGPAB
       ;;
(CRMAGPAB)
       m_CondExec 04,GE,CRMAGPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
