#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  QDAL0M.ksh                       --- VERSION DU 08/10/2016 15:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMQDAL0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 98/01/05 AT 17.22.47 BY PREPA3                       
#    STANDARDS: P  JOBSET: QDAL0M                                              
# --------------------------------------------------------------------         
# ********************************************************************         
#  QMFBATCH : QDAL2 LISTE DES ECHANGES                                         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=QDAL0MA
       ;;
(QDAL0MA)
       EXAAA=${EXAAA:-0}
       EXAAK=${EXAAK:-0}
       RUN=${RUN}
       JUMP_LABEL=QDAL0MAA
       ;;
(QDAL0MAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QDAL0M DSQPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVE MATIN (SA202P)        *                                       
# **************************************                                       
       m_FileAssign -i QMFPARM
RUN ADMFIL.QDAL2 (&&DEBMOIS='$DEBMOIS_ANNMMDD' &&FINMOIS='$FINMOIS_ANNMMDD' FORM=ADMFIL.FDAL2
PRINT REPORT
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QDAL0M1 -a QMFPARM DSQPRINT
       JUMP_LABEL=QDAL0MAB
       ;;
(QDAL0MAB)
       m_CondExec 04,GE,QDAL0MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  QMFBATCH : QDS2 TRI LISTE DE DESTOCKAGE                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
# *AAF      STEP  PGM=IKJEFT01,PATTERN=EO4,PARMS=(L=QDAL0M,S=RDAR,             
# *               M=QDAL0M2)                                                   
# *QMFPARM  DATA  *,CLASS=FIX1,MBR=QDAL0M2                                     
# * RUN ADMFIL.QDS2  (FORM=ADMFIL.FDS2                                         
# * PRINT REPORT                                                               
# *         DATAEND                                                            
# ********************************************************************         
#  QMFBATCH : QDAL3 LISTE DES ECHANGES RM                                      
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP QDAL0MAD PGM=IKJEFT01   **                                          
# ***********************************                                          
       JUMP_LABEL=QDAL0MAD
       ;;
(QDAL0MAD)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ***********************                                                      
# ** FICHIER POUR QMF ***                                                      
# ***********************                                                      
# ***********************                                                      
# ** SORTIE EDITION   ***                                                      
# ***********************                                                      
       m_OutputAssign -c 9 -w QDAL0M DSQPRINT
# **************************************                                       
       m_FileAssign -i QMFPARM
RUN ADMFIL.QDAL3 (&&DEBMOIS='$DEBMOIS_ANNMMDD' &&FINMOIS='$FINMOIS_ANNMMDD' FORM=ADMFIL.FDAL3
PRINT REPORT
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       m_DataQuantCall -p QDAL0M2 -a QMFPARM DSQPRINT
       JUMP_LABEL=QDAL0MAE
       ;;
(QDAL0MAE)
       m_CondExec 04,GE,QDAL0MAD ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
