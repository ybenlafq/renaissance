#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM00L.ksh                       --- VERSION DU 17/10/2016 18:28
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLIVM00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/03/03 AT 14.15.12 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVM00L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BIV100 : EXTRACTION DES CODICS DEMANDES                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM00LA
       ;;
(IVM00LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       MAGASIN=${MAGASIN}
       RUN=${RUN}
       JUMP_LABEL=IVM00LAA
       ;;
(IVM00LAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE EN LECTURE                                                     
#    RTGA00   : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA14   : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA14 /dev/null
#    RTGA30   : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA30 /dev/null
#    RTGS10   : NAME=RSGS10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGS10 /dev/null
#    RTGD31   : NAME=RSGD31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RTGD31 /dev/null
#                                                                              
#    RSFL50   : NAME=RSFL50,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL50 /dev/null
#    RSFL90   : NAME=RSFL90,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFL90 /dev/null
# ******  PARAMETRE                                                            
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 FIN100 ${DATA}/PTEM/IVM00LAA.BIV100AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV101 
       JUMP_LABEL=IVM00LAB
       ;;
(IVM00LAB)
       m_CondExec 04,GE,IVM00LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV102 : EDITION ETIQUETTES INVENTAIRE                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00LAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM00LAD
       ;;
(IVM00LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  ARTICLES                                                             
#    RSGA00L  : NAME=RSGA00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00L /dev/null
# ******  TABLE GENERALISEE (SS TABLES:INVED VUE RVGA01S7)                     
#    RSGA01L  : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01L /dev/null
# ******  LIBELLES DES FAMILLES                                                
#    RSGA14L  : NAME=RSGA14L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA14L /dev/null
# ******  FAMILLES                                                             
#    RSGA30L  : NAME=RSGA30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA30L /dev/null
# ******  STOCK MAG                                                            
#    RSGS30L  : NAME=RSGS30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS30L /dev/null
# ******  STOCK                                                                
#    RSGS60L  : NAME=RSGS60L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGS60L /dev/null
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FIC SOCIETE                                                          
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 FIN100 ${DATA}/PXX0/F61.BIV102AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV102 
       JUMP_LABEL=IVM00LAE
       ;;
(IVM00LAE)
       m_CondExec 04,GE,IVM00LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FIC D'EXTRACTION                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00LAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM00LAG
       ;;
(IVM00LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/IVM00LAA.BIV100AL
       m_FileAssign -d SHR -g ${G_A2} -C ${DATA}/PXX0/F61.BIV102AL
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM00LAG.BIV105AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "1"
 /FIELDS FLD_CH_97_25 97 CH 25
 /FIELDS FLD_CH_2_14 2 CH 14
 /FIELDS FLD_CH_1_1 01 CH 01
 /FIELDS FLD_CH_16_15 16 CH 15
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_6 
 /KEYS
   FLD_CH_2_14 ASCENDING,
   FLD_CH_97_25 ASCENDING,
   FLD_CH_16_15 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM00LAH
       ;;
(IVM00LAH)
       m_CondExec 00,EQ,IVM00LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV105 : EDITION DES ETIQUETTES AUTOCOLLANTES POUR LES MAGASINS             
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00LAJ PGM=BIV105     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM00LAJ
       ;;
(IVM00LAJ)
       m_CondExec ${EXAAP},NE,YES 1,EQ,$[MAGASIN] 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
# ******  FICHIER DES ETIQUETTES AUTOCOLLANTES                                 
       m_FileAssign -d SHR -g ${G_A3} FIN105 ${DATA}/PTEM/IVM00LAG.BIV105AL
#                                                                              
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  EDITION DES ETIQUETTES AUTOCOLLANTES                                 
       m_OutputAssign -c 9 -w IIV106 IIV105
       m_ProgramExec BIV105 
# ********************************************************************         
#  TRI DU FICHIER D'EXTRACTION                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVM00LAM
       ;;
(IVM00LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/IVM00LAA.BIV100AL
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PXX0/F61.BIV102AL
       m_FileAssign -d NEW,CATLG,DELETE -r 135 -t LSEQ -g +1 SORTOUT ${DATA}/PGI961/F61.BIV110AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "2"
 /FIELDS FLD_CH_1_1 01 CH 01
 /FIELDS FLD_CH_2_26 02 CH 26
 /CONDITION CND_1 FLD_CH_1_1 EQ CST_1_4 
 /KEYS
   FLD_CH_2_26 ASCENDING
 /INCLUDE CND_1
 /* Record Type = F  Record Length = 135 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM00LAN
       ;;
(IVM00LAN)
       m_CondExec 00,EQ,IVM00LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV110 : EDITION DES ETATS PREREMPLIS PAR MAG/RAYON                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM00LAQ PGM=IKJEFT01   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM00LAQ
       ;;
(IVM00LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******  LIEUX                                                                
#    RTGA10   : NAME=RSGA10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA10 /dev/null
# ******  FICHIER D'EXTRACTION TRI�                                            
       m_FileAssign -d SHR -g ${G_A6} FIN110 ${DATA}/PGI961/F61.BIV110AL
#                                                                              
# ******  SOCIETE TRAITE  = 961                                                
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  JJMMSSAA                                                             
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ******  EDITION DES ETATS PRE-REMPLIS PAR MAG ET RAYON                       
# IIV110   REPORT SYSOUT=(9,IIV110),RECFM=FBA,LRECL=133,BLKSIZE=1330           
       m_OutputAssign -c "*" IIV110
#                                                                              
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV110 
       JUMP_LABEL=IVM00LAR
       ;;
(IVM00LAR)
       m_CondExec 04,GE,IVM00LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM00LZA
       ;;
(IVM00LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM00LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
