#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  MGOK1F.ksh                       --- VERSION DU 17/10/2016 18:36
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PFMGOK1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/09/18 AT 17.55.00 BY BURTEC2                      
#    STANDARDS: P  JOBSET: MGOK1F                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   UNLOAD TABLE RTGA10                                                        
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=MGOK1FA
       ;;
(MGOK1FA)
       C_A1=${C_A1:-0}
       C_A2=${C_A2:-0}
       C_A3=${C_A3:-0}
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/09/18 AT 17.55.00 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: MGOK1F                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'Contr�le MAGOUV'                       
# *                           APPL...: REPAPPL                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=MGOK1FAA
       ;;
(MGOK1FAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#                                                                              
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F99.RTGA10
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MGOK1FAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  Eclatement par filiales et mag040(internet) toutes filiales                 
#  Cr�ation d'un script FTP conforme pour l'envoi de courriel.                 
#  Nombre de mag ouverts dans le nom du fichier .txt                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGOK1FAD PGM=EZTPA00    ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=MGOK1FAD
       ;;
(MGOK1FAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ____________________________________________________________________         
#  Fichier script FTP mod�le- MGOK1MEL pour envoi courriel                     
#  Zones fixes inchang�es, et variables modifi�es par eazitrev                 
# ____________________________________________________________________         
       m_FileAssign -d SHR MGOK1MEL ${DATA}/CORTEX4.P.MTXTFIX4/MGOK1MEL
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} RTGA10F ${DATA}/PXX0/F99.RTGA10
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK040F ${DATA}/PTEM/MGOK1FAD.MGOK040F
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK907P ${DATA}/PTEM/MGOK1FAD.MGOK907P
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK908X ${DATA}/PTEM/MGOK1FAD.MGOK908X
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK916O ${DATA}/PTEM/MGOK1FAD.MGOK916O
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK961L ${DATA}/PTEM/MGOK1FAD.MGOK961L
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK989M ${DATA}/PTEM/MGOK1FAD.MGOK989M
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK945Y ${DATA}/PTEM/MGOK1FAD.MGOK945Y
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK991D ${DATA}/PTEM/MGOK1FAD.MGOK991D
#                                                                              
#  Fichier script FTP modifi� par eazitrev  envoi courriel                     
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOS ${DATA}/PTEM/MGOK1FAD.MGOS
#                                                                              
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/MGOK1FAD
       m_ProgramExec MGOK1FAD
# *******************************************m************************         
#    Print 7 enregistrements MGOK040F contr�les si tous les magasins           
#    INTERNET sont ouverts.Sinon RC 04 d�clenche le step AAZ ALERTE SM         
# ____________________________________________________________________         
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGOK1FAG PGM=IDCAMS     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=MGOK1FAG
       ;;
(MGOK1FAG)
       m_CondExec ${EXAAK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A2} MGOK040F ${DATA}/PTEM/MGOK1FAD.MGOK040F
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MGOK1FAG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MGOK1FAH
       ;;
(MGOK1FAH)
       m_CondExec 16,NE,MGOK1FAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : IDCAMS                                                                
#  ------------                                                                
#  REPRO Des fichiers mags toutes filiales sur un seul                         
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGOK1FAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=MGOK1FAJ
       ;;
(MGOK1FAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR -g ${G_A3} INPUT ${DATA}/PTEM/MGOK1FAD.MGOK040F
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/MGOK1FAD.MGOK907P
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/MGOK1FAD.MGOK908X
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/MGOK1FAD.MGOK916O
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/MGOK1FAD.MGOK961L
       m_FileAssign -d SHR -g ${G_A8} -C ${DATA}/PTEM/MGOK1FAD.MGOK989M
       m_FileAssign -d SHR -g ${G_A9} -C ${DATA}/PTEM/MGOK1FAD.MGOK945Y
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/MGOK1FAD.MGOK991D
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 OUTPUT ${DATA}/PTEM/MGOK1FAJ.MGOK
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MGOK1FAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=MGOK1FAK
       ;;
(MGOK1FAK)
       m_CondExec 16,NE,MGOK1FAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  Cr�ation du script FTP pour envoi ALERTE SMS via Gateway compatible         
#  CVF : S{t�l�phone_du_destinataire}{jjmmaahhmmss00}                          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGOK1FAM PGM=EZTPA00    ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=MGOK1FAM
       ;;
(MGOK1FAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ____________________________________________________________________         
#  Fichier script FTP mod�le- MGOK1SMS pour envoi SMS en entr�e.               
#  Zones fixes inchang�es, et variables modifi�es par eazitrev                 
# ____________________________________________________________________         
       m_FileAssign -d SHR MGOK1SMS ${DATA}/CORTEX4.P.MTXTFIX4/MGOK1SMS
#                                                                              
# ____________________________________________________________________         
#   Fichier script FTP via gateway pour envoi SMS. Modifi� par eazitre         
# ____________________________________________________________________         
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 MGOK1FSC ${DATA}/PTEM/MGOK1FAM.MGOK1FSC
#                                                                              
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX4/MGOK1FAM
       m_ProgramExec MGOK1FAM
# ********************************************************************         
#  ENVOI FTP SUR.LA.GATEWAY pour courriels vers    meteomag@darty.fr           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP MGOK1FAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=MGOK1FAQ
       ;;
(MGOK1FAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR -g ${G_A11} INPUT ${DATA}/PTEM/MGOK1FAD.MGOS
       m_UtilityExec
# ********************************************************************         
#  ENVOI FTP SUR.LA.GATEWAY pour SMS vers responsable d'astreinte.             
#  Si moins de 7 magasins Internet ouverts. Si step AAK code 4                 
#  REPRISE : OUI                                                               
# ********************************************************************         
# ABE      STEP  PGM=FTP,PARM='(EXIT',COND1=(00,EQ,_AAAK)                       
# SYSPRINT REPORT SYSOUT=*                                                     
# OUTPUT   REPORT SYSOUT=*                                                     
# //SYSFTPD  DD DSN=IPOX.PARMLIB(TCPFTPSD),DISP=SHR                            
# INPUT    FILE  NAME=MGOK1FSC,MODE=I                                          
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
# ABJ      STEP  PGM=EZACFSM1                                                  
# SYSOUT   FILE  NAME=FLOGFTP,MODE=N                                           
# SYSIN    DATA  *                                                             
# &LYYMMDD &LHR:&LMIN:&LSEC &JOBNAME MGOK1F.TXT                                
#         DATAEND                                                              
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=MGOK1FZA
       ;;
(MGOK1FZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/MGOK1FZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
