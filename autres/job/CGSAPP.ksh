#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CGSAPP.ksh                       --- VERSION DU 08/10/2016 14:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCGSAP -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 14/10/16 AT 16.05.53 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CGSAPP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  TRI DE TOUS LES FICHIERS D'INTERFACE ECS                                    
#  REPRISE : NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CGSAPPA
       ;;
(CGSAPPA)
       EXAAA=${EXAAA:-0}
       EXAAZ=${EXAAZ:-0}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2014/10/16 AT 16.05.53 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CGSAPP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'INTERFACE SAP'                         
# *                           APPL...: REPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CGSAPPAA
       ;;
(CGSAPPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#                                                                              
# ******* FIC D'INTERFACES                                                     
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FG14FPP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F96.FG14FPB
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BCG000P.REPRISE
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.PSE00P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.GRA00P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.GBA00P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.GBA02P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.IF00FPP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.AV001P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.AV201P
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CC200P
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FV001P
# ******************************************************************           
#  REMISE A ZERO DU FICHIER DE REPRISE                                         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_98_25 98 CH 25
 /FIELDS FLD_CH_18_10 18 CH 10
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_98_25 ASCENDING
 /* Record Type = F  Record Length = 400 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGSAPPAB
       ;;
(CGSAPPAB)
       m_CondExec 00,EQ,CGSAPPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP CGSAPPAD PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CGSAPPAD
       ;;
(CGSAPPAD)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.BCG000P.REPRISE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSAPPAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CGSAPPAE
       ;;
(CGSAPPAE)
       m_CondExec 16,NE,CGSAPPAD ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
