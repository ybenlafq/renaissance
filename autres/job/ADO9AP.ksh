#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ADO9AP.ksh                       --- VERSION DU 08/10/2016 21:59
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPADO9A -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/24 AT 14.38.44 BY BURTEC2                      
#    STANDARDS: P  JOBSET: ADO9AP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  EXTRACTION RTVT01                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=ADO9APA
       ;;
(ADO9APA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       PROCSTEP=${PROCSTEP:-ADO9APA}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2015/07/24 AT 14.38.44 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: ADO9AP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'REPRISE HISTO'                         
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=ADO9APAA
       ;;
(ADO9APAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES SAUVEGARDES DU SOIR         *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PAX.UNVT02AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APAD
       ;;
(ADO9APAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/ADO01PAX.UNVT02AP
       m_FileAssign -d NEW,CATLG,DELETE -r 297 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT02BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_299 1 CH 299
 /FIELDS FLD_CH_3_297 3 CH 297
 /KEYS
   FLD_CH_1_299 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_297
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APAG
       ;;
(ADO9APAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PBD.ADO001CP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APAE
       ;;
(ADO9APAE)
       m_CondExec 00,EQ,ADO9APAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APAJ PROC=JVZIP     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APAJ
       ;;
(ADO9APAJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAAP},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A2} DD1 ${DATA}/PXX0/F07.UNVT02BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001DP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APAJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PBD.ADO001CP(+1),DISP=SHR               ~         
#
       JUMP_LABEL=ADO9APAM
       ;;
(ADO9APAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PBJ.TADO01DP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APAQ
       ;;
(ADO9APAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PBJ.TADO01DP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APAT
       ;;
(ADO9APAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APAU
       ;;
(ADO9APAU)
       m_CondExec 16,NE,ADO9APAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT05                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APAX PGM=PTLDRIVM   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APAX
       ;;
(ADO9APAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PBT.UNVT05AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APBA
       ;;
(ADO9APBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/ADO01PBT.UNVT05AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT05BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_104 1 CH 104
 /FIELDS FLD_CH_3_100 3 CH 100
 /KEYS
   FLD_CH_1_104 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_100
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APBD
       ;;
(ADO9APBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PCA.ADO001EP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APBB
       ;;
(ADO9APBB)
       m_CondExec 00,EQ,ADO9APBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APBG PROC=JVZIP     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APBG
       ;;
(ADO9APBG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXABY},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A6} DD1 ${DATA}/PXX0/F07.UNVT05BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001FP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APBG.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PCA.ADO001EP(+1),DISP=SHR               ~         
#
       JUMP_LABEL=ADO9APBJ
       ;;
(ADO9APBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APBJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PCG.TADO01FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APBM
       ;;
(ADO9APBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APBM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PCG.TADO01FP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APBQ
       ;;
(ADO9APBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APBQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APBR
       ;;
(ADO9APBR)
       m_CondExec 16,NE,ADO9APBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT06                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APBT PGM=PTLDRIVM   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APBT
       ;;
(ADO9APBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PCQ.UNVT06AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APBT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APBX
       ;;
(ADO9APBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/ADO01PCQ.UNVT06AP
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT06BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APCA PGM=EZACFSM1   ** ID=ADC                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_48 3 CH 48
 /FIELDS FLD_CH_1_53 1 CH 53
 /KEYS
   FLD_CH_1_53 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_48
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APCA
       ;;
(ADO9APCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APCA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PCX.ADO001GP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APBY
       ;;
(ADO9APBY)
       m_CondExec 00,EQ,ADO9APBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APCD PROC=JVZIP     ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APCD
       ;;
(ADO9APCD)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXADH},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A10} DD1 ${DATA}/PXX0/F07.UNVT06BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001HP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APCD.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PCX.ADO001GP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APCG
       ;;
(ADO9APCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APCG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PDD.TADO01HP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01HP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APCJ PGM=FTP        ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APCJ
       ;;
(ADO9APCJ)
       m_CondExec ${EXADR},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APCJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PDD.TADO01HP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APCM PGM=IDCAMS     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APCM
       ;;
(ADO9APCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APCM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APCN
       ;;
(ADO9APCN)
       m_CondExec 16,NE,ADO9APCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT08                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APCQ PGM=PTLDRIVM   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APCQ
       ;;
(ADO9APCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PDM.UNVT08AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APCQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APCT
       ;;
(ADO9APCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/ADO01PDM.UNVT08AP
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT08BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APCX PGM=EZACFSM1   ** ID=AEL                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_134 3 CH 134
 /FIELDS FLD_CH_1_136 1 CH 136
 /KEYS
   FLD_CH_1_136 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_134
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APCX
       ;;
(ADO9APCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APCX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PDT.ADO001IP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APCU
       ;;
(ADO9APCU)
       m_CondExec 00,EQ,ADO9APCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APDA PROC=JVZIP     ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APDA
       ;;
(ADO9APDA)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAEQ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A14} DD1 ${DATA}/PXX0/F07.UNVT08BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001JP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APDA.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PDT.ADO001IP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APDD
       ;;
(ADO9APDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APDD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PEA.TADO01JP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01JP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APDG PGM=FTP        ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APDG
       ;;
(ADO9APDG)
       m_CondExec ${EXAFA},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APDG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PEA.TADO01JP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APDJ PGM=IDCAMS     ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APDJ
       ;;
(ADO9APDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APDJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APDK
       ;;
(ADO9APDK)
       m_CondExec 16,NE,ADO9APDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT11                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APDM PGM=PTLDRIVM   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APDM
       ;;
(ADO9APDM)
       m_CondExec ${EXAFK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PEJ.UNVT11AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APDM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APDQ
       ;;
(ADO9APDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/ADO01PEJ.UNVT11AP
       m_FileAssign -d NEW,CATLG,DELETE -r 228 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT11BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APDT PGM=EZACFSM1   ** ID=AFU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_228 3 CH 228
 /FIELDS FLD_CH_1_230 1 CH 230
 /KEYS
   FLD_CH_1_230 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_228
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APDT
       ;;
(ADO9APDT)
       m_CondExec ${EXAFU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APDT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PEQ.ADO001KP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APDR
       ;;
(ADO9APDR)
       m_CondExec 00,EQ,ADO9APDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APDX PROC=JVZIP     ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APDX
       ;;
(ADO9APDX)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAFZ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A18} DD1 ${DATA}/PXX0/F07.UNVT11BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001LP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APDX.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PEQ.ADO001KP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APEA
       ;;
(ADO9APEA)
       m_CondExec ${EXAGE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APEA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PEX.TADO01LP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01LP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APED PGM=FTP        ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APED
       ;;
(ADO9APED)
       m_CondExec ${EXAGJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APED.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PEX.TADO01LP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APEG PGM=IDCAMS     ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APEG
       ;;
(ADO9APEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APEG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APEH
       ;;
(ADO9APEH)
       m_CondExec 16,NE,ADO9APEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT12                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APEJ PGM=PTLDRIVM   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APEJ
       ;;
(ADO9APEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PFG.UNVT12AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APEJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APEM
       ;;
(ADO9APEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/ADO01PFG.UNVT12AP
       m_FileAssign -d NEW,CATLG,DELETE -r 176 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT12BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APEQ PGM=EZACFSM1   ** ID=AHD                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_176 3 CH 176
 /FIELDS FLD_CH_1_227 1 CH 227
 /KEYS
   FLD_CH_1_227 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_176
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APEQ
       ;;
(ADO9APEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APEQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PFM.ADO001MP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APEN
       ;;
(ADO9APEN)
       m_CondExec 00,EQ,ADO9APEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APET PROC=JVZIP     ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APET
       ;;
(ADO9APET)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAHI},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A22} DD1 ${DATA}/PXX0/F07.UNVT12BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001NP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APET.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PFM.ADO001MP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APEX
       ;;
(ADO9APEX)
       m_CondExec ${EXAHN},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APEX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PFT.TADO01NP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01NP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APFA PGM=FTP        ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APFA
       ;;
(ADO9APFA)
       m_CondExec ${EXAHS},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APFA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PFT.TADO01NP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APFD PGM=IDCAMS     ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APFD
       ;;
(ADO9APFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APFD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APFE
       ;;
(ADO9APFE)
       m_CondExec 16,NE,ADO9APFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT13                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APFG PGM=PTLDRIVM   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APFG
       ;;
(ADO9APFG)
       m_CondExec ${EXAIC},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PGD.UNVT13AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APFG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APFJ PGM=SORT       ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APFJ
       ;;
(ADO9APFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/ADO01PGD.UNVT13AP
       m_FileAssign -d NEW,CATLG,DELETE -r 198 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT13BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APFM PGM=EZACFSM1   ** ID=AIM                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_250 1 CH 250
 /FIELDS FLD_CH_3_198 3 CH 198
 /KEYS
   FLD_CH_1_250 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_198
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APFM
       ;;
(ADO9APFM)
       m_CondExec ${EXAIM},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APFM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PGJ.ADO001OP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APFK
       ;;
(ADO9APFK)
       m_CondExec 00,EQ,ADO9APFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APFQ PROC=JVZIP     ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APFQ
       ;;
(ADO9APFQ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAIR},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A26} DD1 ${DATA}/PXX0/F07.UNVT13BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001QP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APFQ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PGJ.ADO001OP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APFT
       ;;
(ADO9APFT)
       m_CondExec ${EXAIW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APFT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PGQ.TADO01QP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01QP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APFX PGM=FTP        ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APFX
       ;;
(ADO9APFX)
       m_CondExec ${EXAJB},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APFX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PGQ.TADO01QP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APGA PGM=IDCAMS     ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APGA
       ;;
(ADO9APGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APGA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APGB
       ;;
(ADO9APGB)
       m_CondExec 16,NE,ADO9APGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT14                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APGD PGM=PTLDRIVM   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APGD
       ;;
(ADO9APGD)
       m_CondExec ${EXAJL},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PHA.UNVT14AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APGD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APGG PGM=SORT       ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APGG
       ;;
(ADO9APGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/ADO01PHA.UNVT14AP
       m_FileAssign -d NEW,CATLG,DELETE -r 151 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT14BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APGJ PGM=EZACFSM1   ** ID=AJV                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_151 3 CH 151
 /FIELDS FLD_CH_1_166 1 CH 166
 /KEYS
   FLD_CH_1_166 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_151
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APGJ
       ;;
(ADO9APGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APGJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PHG.ADO001RP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APGH
       ;;
(ADO9APGH)
       m_CondExec 00,EQ,ADO9APGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APGM PROC=JVZIP     ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APGM
       ;;
(ADO9APGM)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAKA},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A30} DD1 ${DATA}/PXX0/F07.UNVT14BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001SP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APGM.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PHG.ADO001RP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APGQ
       ;;
(ADO9APGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APGQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PHM.TADO01SP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01SP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APGT PGM=FTP        ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APGT
       ;;
(ADO9APGT)
       m_CondExec ${EXAKK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APGT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PHM.TADO01SP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APGX PGM=IDCAMS     ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APGX
       ;;
(ADO9APGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APGX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APGY
       ;;
(ADO9APGY)
       m_CondExec 16,NE,ADO9APGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT15                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APHA PGM=PTLDRIVM   ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APHA
       ;;
(ADO9APHA)
       m_CondExec ${EXAKU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PHX.UNVT15AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APHA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APHD PGM=SORT       ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APHD
       ;;
(ADO9APHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/ADO01PHX.UNVT15AP
       m_FileAssign -d NEW,CATLG,DELETE -r 176 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT15BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APHG PGM=EZACFSM1   ** ID=ALE                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_176 3 CH 176
 /FIELDS FLD_CH_1_215 1 CH 215
 /KEYS
   FLD_CH_1_215 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_176
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APHG
       ;;
(ADO9APHG)
       m_CondExec ${EXALE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APHG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PID.ADO001TP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APHE
       ;;
(ADO9APHE)
       m_CondExec 00,EQ,ADO9APHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APHJ PROC=JVZIP     ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APHJ
       ;;
(ADO9APHJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXALJ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A34} DD1 ${DATA}/PXX0/F07.UNVT15BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001UP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APHJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PID.ADO001TP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APHM
       ;;
(ADO9APHM)
       m_CondExec ${EXALO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APHM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PIJ.TADO01UP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01UP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APHQ PGM=FTP        ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APHQ
       ;;
(ADO9APHQ)
       m_CondExec ${EXALT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APHQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PIJ.TADO01UP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APHT PGM=IDCAMS     ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APHT
       ;;
(ADO9APHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APHT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APHU
       ;;
(ADO9APHU)
       m_CondExec 16,NE,ADO9APHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT16                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APHX PGM=PTLDRIVM   ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APHX
       ;;
(ADO9APHX)
       m_CondExec ${EXAMD},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PIT.UNVT16AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APHX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APIA PGM=SORT       ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APIA
       ;;
(ADO9APIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/ADO01PIT.UNVT16AP
       m_FileAssign -d NEW,CATLG,DELETE -r 174 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT16BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APID PGM=EZACFSM1   ** ID=AMN                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_213 1 CH 213
 /FIELDS FLD_CH_3_174 3 CH 174
 /KEYS
   FLD_CH_1_213 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_174
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APID
       ;;
(ADO9APID)
       m_CondExec ${EXAMN},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APID.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PJA.ADO001VP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APIB
       ;;
(ADO9APIB)
       m_CondExec 00,EQ,ADO9APIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APIG PROC=JVZIP     ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APIG
       ;;
(ADO9APIG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAMS},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A38} DD1 ${DATA}/PXX0/F07.UNVT16BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001WP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APIG.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PJA.ADO001VP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APIJ
       ;;
(ADO9APIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APIJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PJG.TADO01WP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01WP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APIM PGM=FTP        ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APIM
       ;;
(ADO9APIM)
       m_CondExec ${EXANC},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APIM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PJG.TADO01WP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APIQ PGM=IDCAMS     ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APIQ
       ;;
(ADO9APIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APIQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APIR
       ;;
(ADO9APIR)
       m_CondExec 16,NE,ADO9APIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT17                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APIT PGM=PTLDRIVM   ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APIT
       ;;
(ADO9APIT)
       m_CondExec ${EXANM},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PJQ.UNVT17AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APIT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APIX PGM=SORT       ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APIX
       ;;
(ADO9APIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/ADO01PJQ.UNVT17AP
       m_FileAssign -d NEW,CATLG,DELETE -r 212 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT17BP
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO9APJA PGM=EZACFSM1   ** ID=ANW                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_212 3 CH 212
 /FIELDS FLD_CH_1_251 1 CH 251
 /KEYS
   FLD_CH_1_251 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_212
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO9APJA
       ;;
(ADO9APJA)
       m_CondExec ${EXANW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APJA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PJX.ADO001XP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APIY
       ;;
(ADO9APIY)
       m_CondExec 00,EQ,ADO9APIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APJD PROC=JVZIP     ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APJD
       ;;
(ADO9APJD)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAOB},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A42} DD1 ${DATA}/PXX0/F07.UNVT17BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001YP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APJD.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PJX.ADO001XP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO9APJG
       ;;
(ADO9APJG)
       m_CondExec ${EXAOG},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APJG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PKD.TADO01YP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01YP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO9APJJ PGM=FTP        ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APJJ
       ;;
(ADO9APJJ)
       m_CondExec ${EXAOL},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APJJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PKD.TADO01YP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO9APJM PGM=IDCAMS     ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APJM
       ;;
(ADO9APJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APJM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO9APJN
       ;;
(ADO9APJN)
       m_CondExec 16,NE,ADO9APJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=ADO9APZA
       ;;
(ADO9APZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO9APZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
