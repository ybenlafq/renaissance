#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  NASZHG.ksh                       --- VERSION DU 08/10/2016 23:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PGNASZH -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/01/30 AT 12.41.12 BY BURTEC9                      
#    STANDARDS: P  JOBSET: NASZHG                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  REMISE A ZERO DES  GDG DE R�CUP CUMUL�S HEBDO    NASL                       
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=NASZHGA
       ;;
(NASZHGA)
       EXAAA=${EXAAA:-0}
# ********************************************************************         
# *    GENERATED ON FRIDAY    2004/01/30 AT 12.41.12 BY BURTEC9                
# *    JOBSET INFORMATION:    NAME...: NASZHG                                  
# *                           FREQ...: W                                       
# *                           TITLE..: 'RAZ HEBDO NASL'                        
# *                           APPL...: REPEXPL                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=NASZHGAA
       ;;
(NASZHGAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUT1 ${DATA}/PXX0/F07.FSLST1SP
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUT2 ${DATA}/PXX0/F91.FSLST1SD
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUT3 ${DATA}/PXX0/F61.FSLST1SL
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUT4 ${DATA}/PXX0/F89.FSLST1SM
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUT5 ${DATA}/PXX0/F94.FSLST1SR
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUT6 ${DATA}/PXX0/F45.FSLST1SY
       m_FileAssign -d NEW,CATLG,DELETE -r 331 -g +1 OUT7 ${DATA}/PXX0/F16.FSLST1SO
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/NASZHGAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=NASZHGAB
       ;;
(NASZHGAB)
       m_CondExec 16,NE,NASZHGAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
# ********************************************************************         
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
