#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTI02D.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDFTI02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/29 AT 10.43.25 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FTI02D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   SORT DES FICHIERS ISSU DES DIFFERENTS TRAITEMENTS DE GESTION               
#   PLUS LE FICHIER           DE REPRISE DE LA VEILLE.                         
#   REPRISEE NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FTI02DA
       ;;
(FTI02DA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FTI02DAA
       ;;
(FTI02DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# *** FICHIER CAISSES NEM DACEM ISSU DE NM002D                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FFTI49MD
# *** FICHIER DE REPRISE EN CAS DE PLANTAGE LA VEILLE                          
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FTI02RED
# *** FICHIER DE RECYCLAGE                                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FTRECYFD
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FFTI02GD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DAB
       ;;
(FTI02DAB)
       m_CondExec 00,EQ,FTI02DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   HISTORISATION DES FICHIERS MGD EN ENTREE                                   
#   REPRISE  NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DAD
       ;;
(FTI02DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER DU JOUR                                                          
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.FFTI49MD
# *** FICHIER HISTO                                                            
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FFTI49MD.HISTO
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FFTI49MD.HISTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DAE
       ;;
(FTI02DAE)
       m_CondExec 00,EQ,FTI02DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI50 .GENERATIONS DES MVTS COMPTABLES FRANCHISE                      
#   REPRISE: NON                                                               
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01,RSTRT=SAME                                       
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *** CARTE PARAM�TRE                                                          
# FPARAM   DATA  *,CLASS=FIX1,MBR=FTI01D01                                     
# F91                                                                          
#          DATAEND                                                             
# *** FDATE                                                                    
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# *** TABLE  EN ENTREE SANS MAJ                                                
# RTLI00   FILE  DYNAM=YES,NAME=RSLI00,MODE=(I,U)                              
# RTFM95   FILE  DYNAM=YES,NAME=RSFM95,MODE=(I,U)                              
# RTGA00   FILE  DYNAM=YES,NAME=RSGA00,MODE=(I,U)                              
# **** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                 
# FICICS   FILE  NAME=FFTI49AD,MODE=I                                          
# **** FICHIER EN SORTIE  (LRECL 400)                                          
# FICICD   FILE  NAME=FFTI50AD,MODE=O                                          
# **** FICHIER EN SORTIE  (LRECL 400)                                          
# FICICF   FILE  NAME=FFTI50BD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BFTI50) PLAN(BTDARO)                                            
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#   PGM BFTI01 .GENERATIONS DES MVTS COMPTABLES                                
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DAG
       ;;
(FTI02DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTFM55   : NAME=RSFM55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM55 /dev/null
#    RTFM80   : NAME=RSFM80D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM80 /dev/null
#    RTFM81   : NAME=RSFM81D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM81 /dev/null
#    RTFM82   : NAME=RSFM82D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM82 /dev/null
#    RTFM83   : NAME=RSFM83D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM83 /dev/null
#    RTFM89   : NAME=RSFM89D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM89 /dev/null
#    RTFM90   : NAME=RSFM90D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM90 /dev/null
#    RTFM91   : NAME=RSFM91D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM91 /dev/null
#    RTFM92   : NAME=RSFM92D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM92 /dev/null
#    RTFM97   : NAME=RSFM97,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM97 /dev/null
#    RTFM98   : NAME=RSFM98,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM98 /dev/null
#    RTFT85   : NAME=RSFT85D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT85 /dev/null
#    RTFT86   : NAME=RSFT86D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT86 /dev/null
#    RTFT88   : NAME=RSFT88D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT88 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# *********************************** PARAMETRE DATE / SOC                     
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  TABLE GENERALISEE: SOUS TABLES(TXTVA ENTST FEURO SDA2I)              
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                  
       m_FileAssign -d SHR -g +0 FFTI00 ${DATA}/PXX0/F91.FFTI49MD
# *** FICHIER EN SORTIE  (LRECL 200)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFV002 ${DATA}/PTEM/FTI02DAG.FFTI02AD
# *** FICHIER DES ANOMALIES POUR TRI                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 FREPRIS ${DATA}/PTEM/FTI02DAG.FFTI02BD
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IFTI00 ${DATA}/PTEM/FTI02DAG.IFTI02AD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTI01 
       JUMP_LABEL=FTI02DAH
       ;;
(FTI02DAH)
       m_CondExec 04,GE,FTI02DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FTI02DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DAJ
       ;;
(FTI02DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER REPRISE                                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FTI02DAG.FFTI02BD
# *** FICHIER REPRISE TRIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DAJ.FFTI02CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 01 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DAK
       ;;
(FTI02DAK)
       m_CondExec 00,EQ,FTI02DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01AO: ISSU DU PGM BFTI01                               
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DAM
       ;;
(FTI02DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FTI02DAG.FFTI02AD
#  SORTIE                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DAM.FFTI02DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_169_8 169 CH 08
 /FIELDS FLD_CH_13_15 13 CH 15
 /KEYS
   FLD_CH_169_8 ASCENDING,
   FLD_CH_13_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DAN
       ;;
(FTI02DAN)
       m_CondExec 00,EQ,FTI02DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI06 : CONSTITUTION DU FICHIER ANOMALIES POUR LE LENDEMAIN           
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DAQ PGM=BFTI06     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DAQ
       ;;
(FTI02DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU PGM BFTI00 ET TRIE PRECEDEMMENT                             
# *** FICHIER DU FFTI00 TRIE                                                   
       m_FileAssign -d SHR -g +0 FFTI00 ${DATA}/PXX0/F91.FFTI49MD
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A3} FFTV02 ${DATA}/PTEM/FTI02DAM.FFTI02DD
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A4} FREPRIS ${DATA}/PTEM/FTI02DAJ.FFTI02CD
#                                                                              
#  FICHIER DES ANOMALIES REPRIS LE LENDEMAIN                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI05 ${DATA}/PXX0/F91.FTRECYFD
#                                                                              
#  FICHIER EN SORTIE POUR TRI                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV05 ${DATA}/PTEM/FTI02DAQ.FFTI06ED
#                                                                              
#  FICHIER EN SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PTEM/FTI02DAQ.FFTI02ED
       m_ProgramExec BFTI06 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER FFTV05 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DAT
       ;;
(FTI02DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/FTI02DAQ.FFTI06ED
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DAT.FFTI06FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_177_1 177 CH 01
 /FIELDS FLD_CH_1_27 01 CH 27
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DAU
       ;;
(FTI02DAU)
       m_CondExec 00,EQ,FTI02DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FTI02DAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DAX
       ;;
(FTI02DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FTI02DAT.FFTI06FD
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DAX.FFTI06GD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_75_48 75 CH 48
 /FIELDS FLD_CH_57_12 57 CH 12
 /FIELDS FLD_PD_123_8 123 PD 08
 /FIELDS FLD_CH_1_50 01 CH 50
 /FIELDS FLD_CH_201_199 201 CH 199
 /KEYS
   FLD_CH_1_50 ASCENDING,
   FLD_CH_201_199 ASCENDING,
   FLD_CH_57_12 ASCENDING,
   FLD_CH_75_48 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_123_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DAY
       ;;
(FTI02DAY)
       m_CondExec 00,EQ,FTI02DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI11 .TRAITEMENT DU CALCUL DES SOLDES ET CUMULS                      
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBA PGM=BFTI11     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBA
       ;;
(FTI02DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A7} FFTV06 ${DATA}/PTEM/FTI02DAX.FFTI06GD
#                                                                              
#  FICHIER POUR TRI ET POUR SAP                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV06B ${DATA}/PTEM/FTI02DBA.FFTI11BD
       m_ProgramExec BFTI11 
#                                                                              
# ********************************************************************         
#   TRI DU FICHIER FFTV05 ISSU DU PGM BFTI06                                   
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBD
       ;;
(FTI02DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/FTI02DAQ.FFTI06ED
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DBD.FFTI06HD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_1_27 01 CH 27
 /FIELDS FLD_CH_177_1 177 CH 01
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DBE
       ;;
(FTI02DBE)
       m_CondExec 00,EQ,FTI02DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER DES DEUX FICHIERS PRECEDENTS POUR FICHIER SAP              
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBG
       ;;
(FTI02DBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/FTI02DBD.FFTI06HD
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/FTI02DBA.FFTI11BD
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FFTI02D
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_27 01 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DBH
       ;;
(FTI02DBH)
       m_CondExec 00,EQ,FTI02DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER DES DEUX FICHIERS PRECEDENTS POUR FICHIER SAP              
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBJ
       ;;
(FTI02DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/F91.FFTI02D
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FTI02D
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FTI02D
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DBK
       ;;
(FTI02DBK)
       m_CondExec 00,EQ,FTI02DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC1 (IFTI02AD)                                          
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBM
       ;;
(FTI02DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/FTI02DAG.IFTI02AD
# ********* FICHIER FEXTRAC1 TRIE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DBM.IFTI02BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_139_4 139 PD 04
 /FIELDS FLD_BI_1_138 01 CH 138
 /FIELDS FLD_BI_143_4 143 CH 04
 /KEYS
   FLD_BI_1_138 ASCENDING,
   FLD_BI_143_4 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_139_4
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DBN
       ;;
(FTI02DBN)
       m_CondExec 00,EQ,FTI02DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS IFTI02CD                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBQ
       ;;
(FTI02DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/FTI02DBM.IFTI02BD
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/FTI02DBQ.IFTI02CD
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=FTI02DBR
       ;;
(FTI02DBR)
       m_CondExec 04,GE,FTI02DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBT
       ;;
(FTI02DBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/FTI02DBQ.IFTI02CD
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DBT.IFTI02DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DBU
       ;;
(FTI02DBU)
       m_CondExec 00,EQ,FTI02DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IFTI01                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DBX
       ;;
(FTI02DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/FTI02DBM.IFTI02BD
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/FTI02DBT.IFTI02DD
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IFTI01 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=FTI02DBY
       ;;
(FTI02DBY)
       m_CondExec 04,GE,FTI02DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DES FICHIERS EN ENTREE DE L'ICS POUR EVITER LES TRAITEMENTS          
#         EN DOUBLE.                                                           
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DCA PGM=IDCAMS     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DCA
       ;;
(FTI02DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***   FIC DE CUMUL DES INTERFACES (EN CAS DE PLANTAGE DE L'ICS)              
       m_FileAssign -d SHR IN1 /dev/null
# ***   FICHIER CUMUL REMIS A ZERO                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F91.FTI02RED
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02DCA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTI02DCB
       ;;
(FTI02DCB)
       m_CondExec 16,NE,FTI02DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DCD
       ;;
(FTI02DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/FTI02DAQ.FFTI02ED
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PXX0/F91.FTRECYFD
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02DCD.FFTI02FD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_15 01 CH 15
 /FIELDS FLD_BI_19_381 19 CH 381
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_BI_19_381 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02DCE
       ;;
(FTI02DCE)
       m_CondExec 00,EQ,FTI02DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTRIEVE                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DCG PGM=EZTPA00    ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DCG
       ;;
(FTI02DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} FICHI ${DATA}/PXX0/F91.FFTI02D
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FICHO ${DATA}/PXX0/F91.FTI02SD
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FTI02DCG
       m_ProgramExec FTI02DCG
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FT??????                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DCJ PGM=EZACFSM1   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DCJ
       ;;
(FTI02DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02DCJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FTI02DCJ.FTFTI02D
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FT??????                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02DCM PGM=FTP        ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DCM
       ;;
(FTI02DCM)
       m_CondExec ${EXADW},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02DCM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FTI02DCJ.FTFTI02D(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#   DEPENDANCE POUR PLAN                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FTI02DZA
       ;;
(FTI02DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
