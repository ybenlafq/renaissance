#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  KLEE1P.ksh                       --- VERSION DU 09/10/2016 05:35
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPKLEE1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/04 AT 11.34.35 BY BURTEC6                      
#    STANDARDS: P  JOBSET: KLEE1P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ******************************************************************           
#  DELETE DU FICHIER PXX0.F07.KLEE1P                                           
#  REPRISE : OUI                                                               
# ******************************************************************           
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=KLEE1PA
       ;;
(KLEE1PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
# ********************************************************************         
# *    GENERATED ON THURSDAY  2013/07/04 AT 11.34.34 BY BURTEC6                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: KLEE1P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'KLEE INFO'                             
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=KLEE1PAA
       ;;
(KLEE1PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE1PAA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=KLEE1PAB
       ;;
(KLEE1PAB)
       m_CondExec 16,NE,KLEE1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DE LA TABLE RTGA10                                                   
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP KLEE1PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=KLEE1PAD
       ;;
(KLEE1PAD)
       m_CondExec ${EXAAF},NE,YES 
#    RTGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 128 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/KLEE1PAD.UNGA10P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE1PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# *******************************************************************          
#  TRI DU FICHIER D'UNLOAD ET MISE EN FORME AVANT ENVOI                        
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP KLEE1PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=KLEE1PAG
       ;;
(KLEE1PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/KLEE1PAD.UNGA10P
       m_FileAssign -d NEW,CATLG,DELETE -r 68 -t LSEQ SORTOUT ${DATA}/PXX0.F07.KLEE1P
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_77_1 77 CH 1
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_29_1 29 CH 1
 /FIELDS FLD_CH_1_26 1 CH 26
 /FIELDS FLD_CH_65_10 65 CH 10
 /FIELDS FLD_CH_32_30 32 CH 30
 /KEYS
   FLD_CH_1_6 ASCENDING
 /* Record Type = F  Record Length = 68 */
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_26,FLD_CH_29_1,FLD_CH_32_30,FLD_CH_65_10,FLD_CH_77_1
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=KLEE1PAH
       ;;
(KLEE1PAH)
       m_CondExec 00,EQ,KLEE1PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=KLEE1PZA
       ;;
(KLEE1PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/KLEE1PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
