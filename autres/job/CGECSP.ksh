#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CGECSP.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPCGECS -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/07/10 AT 15.38.31 BY BURTEC2                      
#    STANDARDS: P  JOBSET: CGECSP                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='FILIALES'                                                          
# ********************************************************************         
#  TRI DES FICHIERS ECS DIF                                                    
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CGECSPA
       ;;
(CGECSPA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/07/10 AT 15.38.31 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CGECSP                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'CONVERGENCE'                           
# *                           APPL...: REPPARIS                                
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CGECSPAA
       ;;
(CGECSPAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# *********************************                                            
# *****   FICHIER ISSU DE LA CHA�NE CGSAPP                                     
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F07.FV001P
# ******* FICHIER DE RECYCLAGE                                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CGECSP0
# ******* FICHIER DE REPRISE                                                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.CGECSP.REPRISE
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CGECSPAA.CGECSP1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_1_17 1 CH 17
 /FIELDS FLD_CH_235_1 235 CH 1
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_235_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGECSPAB
       ;;
(CGECSPAB)
       m_CondExec 00,EQ,CGECSPAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG010                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGECSPAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CGECSPAD
       ;;
(CGECSPAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FIC ISSU DU TRI                                                       
       m_FileAssign -d SHR -g ${G_A1} FFTV02 ${DATA}/PTEM/CGECSPAA.CGECSP1
# ******  TABLE EN LECTURE                                                     
#    RSGA10   : NAME=RSGA10,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSFM99   : NAME=RSFM99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFM99 /dev/null
# ****** FICHIERS EN SORTIE POUR LE BCG020 ET LE BCG030                        
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FICANO ${DATA}/PTEM/CGECSPAD.CGECSP2
# ****** FICHIERS EN SORTIE POUR LE PGM BCG020                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FCG010 ${DATA}/PTEM/CGECSPAD.CGECSP3
# ------  ETAT DES ANOMALIES                                                   
       m_OutputAssign -c 9 -w ICG010 ICG010
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG010 
       JUMP_LABEL=CGECSPAE
       ;;
(CGECSPAE)
       m_CondExec 04,GE,CGECSPAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BCG020                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGECSPAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CGECSPAG
       ;;
(CGECSPAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A2} FICANO ${DATA}/PTEM/CGECSPAD.CGECSP2
       m_FileAssign -d SHR -g ${G_A3} FCG010 ${DATA}/PTEM/CGECSPAD.CGECSP3
# ****** FICHIER  EN SORTIE VER XFB GATEWAY                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 FCG020 ${DATA}/PXX0/F07.CGECSP
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG020 
       JUMP_LABEL=CGECSPAH
       ;;
(CGECSPAH)
       m_CondExec 04,GE,CGECSPAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG030                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGECSPAJ PGM=BCG030     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CGECSPAJ
       ;;
(CGECSPAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A4} FFTV02 ${DATA}/PTEM/CGECSPAA.CGECSP1
       m_FileAssign -d SHR -g ${G_A5} FICANO ${DATA}/PTEM/CGECSPAD.CGECSP2
# ****** FICHIER  EN SORTIE POUR RECYCLAGE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FREPRIS ${DATA}/PXX0/F07.CGECSP0
       m_ProgramExec BCG030 
# ********************************************************************         
#  ENVOI DU FICHIER VERS GATEWAY POUR ROUTAGE VERS SAP                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=IEFBR14,PATTERN=CFT,COND1=(03,GT,_AAAK)                    
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SAPICEP,                                                            
#      FNAME=":CGECSP""(0),                                                    
#      NFNAME=CGECSP                                                           
#          DATAEND                                                             
# ******************************************************************           
#  REMISE A ZERO DU FICHIER DE REPRISE                                         
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP CGECSPAM PGM=IDCAMS     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CGECSPAM
       ;;
(CGECSPAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.CGECSP.REPRISE
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGECSPAM.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CGECSPAN
       ;;
(CGECSPAN)
       m_CondExec 16,NE,CGECSPAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHANGEMENT PCL => CGECSP <= CHANGEMENT PCL                                  
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCGECSP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGECSPAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CGECSPAQ
       ;;
(CGECSPAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/CGECSPAQ.sysin
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP CGECSPAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=CGECSPAT
       ;;
(CGECSPAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGECSPAT.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CGECSPZA
       ;;
(CGECSPZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGECSPZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
