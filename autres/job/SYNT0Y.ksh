#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SYNT0Y.ksh                       --- VERSION DU 08/10/2016 17:21
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYSYNT0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/02/05 AT 14.58.45 BY BURTECC                      
#    STANDARDS: P  JOBSET: SYNT0Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BSYNT01 : TRAITEMENT BSYNT01                                                
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SYNT0YA
       ;;
(SYNT0YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SYNT0YAA
       ;;
(SYNT0YAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSPR00   : NAME=RSPR00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#                                                                              
# ****** CARTE DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
# ****** FICHIER SORTIE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -g +1 FSYNT1 ${DATA}/PTEM/SYNT0YAA.BSYNT1AY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSYNT01 
       JUMP_LABEL=SYNT0YAB
       ;;
(SYNT0YAB)
       m_CondExec 04,GE,SYNT0YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0YAD
       ;;
(SYNT0YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SYNT0YAA.BSYNT1AY
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -g +1 SORTOUT ${DATA}/PTEM/SYNT0YAD.BSYNT1BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SYNT0YAE
       ;;
(SYNT0YAE)
       m_CondExec 00,EQ,SYNT0YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSYNT05 : TRAITEMENT BSYNT05                                                
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0YAG
       ;;
(SYNT0YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV10   : NAME=RSGV10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FSYNT1 ${DATA}/PTEM/SYNT0YAD.BSYNT1BY
#                                                                              
# ****** FICHIERS SYNTHESES VENTES TYPE CUISINE                                
       m_FileAssign -d NEW,CATLG,DELETE -r 84 -g +1 FSYNT2 ${DATA}/PXX0/F45.BSYNT2AY
# ****** FICHIERS SYNTHESES VENTES TYPE DETAIL                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 84 -g +1 FSYNT3 ${DATA}/PXX0/F45.BSYNT3AY
#                                                                              
# ******************************************************                       
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          

       m_ProgramExec -b BSYNT05 
       JUMP_LABEL=SYNT0YZA
       ;;
(SYNT0YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=SYNT0YAH
       ;;
(SYNT0YAH)
       m_CondExec 04,GE,SYNT0YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
