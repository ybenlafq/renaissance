#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTI02L.ksh                       --- VERSION DU 17/10/2016 18:11
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLFTI02 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/04/29 AT 10.43.59 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FTI02L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   SORT DES FICHIERS ISSU DES DIFFERENTS TRAITEMENTS DE GESTION               
#   PLUS LE FICHIER           DE REPRISE DE LA VEILLE.                         
#   REPRISEE NON                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=FTI02LA
       ;;
(FTI02LA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=FTI02LAA
       ;;
(FTI02LAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
# **************************************                                       
# *** FICHIER CAISSES NEM DACEM ISSU DE NM002D                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.FFTI49ML
# *** FICHIER DE REPRISE EN CAS DE PLANTAGE LA VEILLE                          
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FTI02REL
# *** FICHIER DE RECYCLAGE                                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FTRECYFL
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.FFTI02GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LAB
       ;;
(FTI02LAB)
       m_CondExec 00,EQ,FTI02LAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   HISTORISATION DES FICHIERS MGD EN ENTREE                                   
#   REPRISE  NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LAD
       ;;
(FTI02LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER DU JOUR                                                          
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.FFTI49ML
# *** FICHIER HISTO                                                            
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FFTI49ML.HISTO
# *** SORTIE POUR PGM BFTI01                                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.FFTI49ML.HISTO
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LAE
       ;;
(FTI02LAE)
       m_CondExec 00,EQ,FTI02LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI50 .GENERATIONS DES MVTS COMPTABLES FRANCHISE                      
#   REPRISE: NON                                                               
# ********************************************************************         
# AAK      STEP  PGM=IKJEFT01,RSTRT=SAME                                       
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=H                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSABOUT REPORT SYSOUT=X                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# *** CARTE PARAM�TRE                                                          
# FPARAM   DATA  *,CLASS=FIX1,MBR=FTI01D01                                     
# F91                                                                          
#          DATAEND                                                             
# *** FDATE                                                                    
# FDATE    DATA  CLASS=VAR,PARMS=FDATE,MBR=FDATE                               
# *** TABLE  EN ENTREE SANS MAJ                                                
# RTLI00   FILE  DYNAM=YES,NAME=RSLI00,MODE=(I,U)                              
# RTFM95   FILE  DYNAM=YES,NAME=RSFM95,MODE=(I,U)                              
# RTGA00   FILE  DYNAM=YES,NAME=RSGA00,MODE=(I,U)                              
# **** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                 
# FICICS   FILE  NAME=FFTI49AD,MODE=I                                          
# **** FICHIER EN SORTIE  (LRECL 400)                                          
# FICICD   FILE  NAME=FFTI50AD,MODE=O                                          
# **** FICHIER EN SORTIE  (LRECL 400)                                          
# FICICF   FILE  NAME=FFTI50BD,MODE=O                                          
# *                                                                            
# SYSTSIN  DATA  *,CLASS=FIX2                                                  
#  DSN SYSTEM(RDAR)                                                            
#  RUN PROGRAM(BFTI50) PLAN(BFTI50L)                                           
#  END                                                                         
#          DATAEND                                                             
# ********************************************************************         
#   PGM BFTI01 .GENERATIONS DES MVTS COMPTABLES                                
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LAG
       ;;
(FTI02LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *** TABLE  EN ENTREE SANS MAJ                                                
#    RTFM55   : NAME=RSFM55L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM55 /dev/null
#    RTFM80   : NAME=RSFM80L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM80 /dev/null
#    RTFM81   : NAME=RSFM81L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM81 /dev/null
#    RTFM82   : NAME=RSFM82L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM82 /dev/null
#    RTFM83   : NAME=RSFM83L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM83 /dev/null
#    RTFM89   : NAME=RSFM89L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM89 /dev/null
#    RTFM90   : NAME=RSFM90L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM90 /dev/null
#    RTFM91   : NAME=RSFM91L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM91 /dev/null
#    RTFM92   : NAME=RSFM92L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFM92 /dev/null
#    RTFM97   : NAME=RSFM97,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM97 /dev/null
#    RTFM98   : NAME=RSFM98,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTFM98 /dev/null
#    RTFT85   : NAME=RSFT85L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT85 /dev/null
#    RTFT86   : NAME=RSFT86L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT86 /dev/null
#    RTFT88   : NAME=RSFT88L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTFT88 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
# *********************************** PARAMETRE DATE / SOC                     
       m_FileAssign -i FDATE
$FDATE
_end
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# ******  TABLE GENERALISEE: SOUS TABLES(TXTVA ENTST FEURO SDA2I)              
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# *** FICHIER EN ENTREE ISSU DU TRI PRECEDENT                                  
       m_FileAssign -d SHR -g +0 FFTI00 ${DATA}/PXX0/F61.FFTI49ML
# *** FICHIER EN SORTIE  (LRECL 200)                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFV002 ${DATA}/PTEM/FTI02LAG.FFTI02AL
# *** FICHIER DES ANOMALIES POUR TRI                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 FREPRIS ${DATA}/PTEM/FTI02LAG.FFTI02BL
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 IFTI00 ${DATA}/PTEM/FTI02LAG.IFTI02AL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BFTI01 
       JUMP_LABEL=FTI02LAH
       ;;
(FTI02LAH)
       m_CondExec 04,GE,FTI02LAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FTI02LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LAJ
       ;;
(FTI02LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER REPRISE                                                          
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/FTI02LAG.FFTI02BL
# *** FICHIER REPRISE TRIE                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 23 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LAJ.FFTI02CL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_23 01 CH 23
 /KEYS
   FLD_CH_1_23 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LAK
       ;;
(FTI02LAK)
       m_CondExec 00,EQ,FTI02LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01AO: ISSU DU PGM BFTI01                               
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LAM PGM=SORT       ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LAM
       ;;
(FTI02LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/FTI02LAG.FFTI02AL
#  SORTIE                                                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LAM.FFTI02DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_13_15 13 CH 15
 /FIELDS FLD_CH_169_8 169 CH 08
 /KEYS
   FLD_CH_169_8 ASCENDING,
   FLD_CH_13_15 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LAN
       ;;
(FTI02LAN)
       m_CondExec 00,EQ,FTI02LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI06 : CONSTITUTION DU FICHIER ANOMALIES POUR LE LENDEMAIN           
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LAQ PGM=BFTI06     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LAQ
       ;;
(FTI02LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU PGM BFTI00 ET TRIE PRECEDEMMENT                             
# *** FICHIER DU FFTI00 TRIE                                                   
       m_FileAssign -d SHR -g +0 FFTI00 ${DATA}/PXX0/F61.FFTI49ML
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A3} FFTV02 ${DATA}/PTEM/FTI02LAM.FFTI02DL
#                                                                              
#  FICHIER ISSU DU PGM BFTI01 ET TRIE PRECEDEMMENT                             
       m_FileAssign -d SHR -g ${G_A4} FREPRIS ${DATA}/PTEM/FTI02LAJ.FFTI02CL
#                                                                              
#  FICHIER DES ANOMALIES REPRIS LE LENDEMAIN                                   
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI05 ${DATA}/PXX0/F61.FTRECYFL
#                                                                              
#  FICHIER EN SORTIE POUR TRI                                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV05 ${DATA}/PTEM/FTI02LAQ.FFTI06EL
#                                                                              
#  FICHIER EN SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTI01 ${DATA}/PTEM/FTI02LAQ.FFTI02EL
       m_ProgramExec BFTI06 
#                                                                              
# ********************************************************************         
#   SORT DU FICHIER FFTV05 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LAT
       ;;
(FTI02LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/FTI02LAQ.FFTI06EL
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LAT.FFTI06FL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_1_27 01 CH 27
 /FIELDS FLD_CH_177_1 177 CH 01
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LAU
       ;;
(FTI02LAU)
       m_CondExec 00,EQ,FTI02LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#                                                                              
# ***********************************                                          
# *   STEP FTI02LAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LAX
       ;;
(FTI02LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/FTI02LAT.FFTI06FL
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LAX.FFTI06GL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_57_12 57 CH 12
 /FIELDS FLD_CH_201_199 201 CH 199
 /FIELDS FLD_PD_123_8 123 PD 08
 /FIELDS FLD_CH_75_48 75 CH 48
 /FIELDS FLD_CH_1_50 01 CH 50
 /KEYS
   FLD_CH_1_50 ASCENDING,
   FLD_CH_201_199 ASCENDING,
   FLD_CH_57_12 ASCENDING,
   FLD_CH_75_48 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_123_8
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LAY
       ;;
(FTI02LAY)
       m_CondExec 00,EQ,FTI02LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   PGM BFTI11 .TRAITEMENT DU CALCUL DES SOLDES ET CUMULS                      
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBA PGM=BFTI11     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBA
       ;;
(FTI02LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER ISSU DU TRI PRECEDENT                                               
       m_FileAssign -d SHR -g ${G_A7} FFTV06 ${DATA}/PTEM/FTI02LAX.FFTI06GL
#                                                                              
#  FICHIER POUR TRI ET POUR SAP                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FFTV06B ${DATA}/PTEM/FTI02LBA.FFTI11BL
       m_ProgramExec BFTI11 
#                                                                              
# ********************************************************************         
#   TRI DU FICHIER FFTV05 ISSU DU PGM BFTI06                                   
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBD
       ;;
(FTI02LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/FTI02LAQ.FFTI06EL
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LBD.FFTI06HL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_4 "N"
 /FIELDS FLD_CH_177_1 177 CH 01
 /FIELDS FLD_CH_1_27 01 CH 27
 /CONDITION CND_1 FLD_CH_177_1 EQ CST_1_4 
 /KEYS
   FLD_CH_1_27 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LBE
       ;;
(FTI02LBE)
       m_CondExec 00,EQ,FTI02LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER DES DEUX FICHIERS PRECEDENTS POUR FICHIER SAP              
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBG PGM=SORT       ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBG
       ;;
(FTI02LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/FTI02LBD.FFTI06HL
       m_FileAssign -d SHR -g ${G_A10} -C ${DATA}/PTEM/FTI02LBA.FFTI11BL
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.FFTI02L
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_27 01 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LBH
       ;;
(FTI02LBH)
       m_CondExec 00,EQ,FTI02LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   HISTORISATION DES FICHIERS VERS SAP                                        
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBJ
       ;;
(FTI02LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
#                                                                              
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PXX0/F61.FFTI02L
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F61.FTI02L
#                                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F61.FTI02L
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LBK
       ;;
(FTI02LBK)
       m_CondExec 00,EQ,FTI02LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FEXTRAC1 (IFTI02AL)                                          
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBM
       ;;
(FTI02LBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER  POUR GENERATEUR D'ETAT (LREC 512)                               
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/FTI02LAG.IFTI02AL
# ********* FICHIER FEXTRAC1 TRIE                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LBM.IFTI02BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_138 01 CH 138
 /FIELDS FLD_BI_143_4 143 CH 04
 /FIELDS FLD_PD_139_4 139 PD 04
 /KEYS
   FLD_BI_1_138 ASCENDING,
   FLD_BI_143_4 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_139_4
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LBN
       ;;
(FTI02LBN)
       m_CondExec 00,EQ,FTI02LBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG050                                                                
# ********************************************************************         
#  CREATION D'UN FICHIER FCUMULS IFTI02CL                                      
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBQ
       ;;
(FTI02LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A13} FEXTRAC ${DATA}/PTEM/FTI02LBM.IFTI02BL
# *********************************** FICHIER FCUMULS  RECL=512                
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 FCUMULS ${DATA}/PTEM/FTI02LBQ.IFTI02CL
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG050 
       JUMP_LABEL=FTI02LBR
       ;;
(FTI02LBR)
       m_CondExec 04,GE,FTI02LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCUMULS ISSU DU BEG050                                       
# ********************************************************************         
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBT PGM=SORT       ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBT
       ;;
(FTI02LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** FICHIER FCUMULS ISSU DU BEG050           
       m_FileAssign -d SHR -g ${G_A14} SORTIN ${DATA}/PTEM/FTI02LBQ.IFTI02CL
# ********************************** FICHIER FCUMULS TRIE                      
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LBT.IFTI02DL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_512 1 CH 512
 /KEYS
   FLD_BI_1_512 ASCENDING
 /* Record Type = F  Record Length = 512 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LBU
       ;;
(FTI02LBU)
       m_CondExec 00,EQ,FTI02LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BEG060                                                                
#  MODIFICATION EN FILIALE .LE PGM BEG060 UTILISE LA S/TABLE MEG36             
#  SI FORMAT =A LE FEG132 EST ECRIT (DANS LE CAS D IMPRESSION A 132 CH         
#  SI FORMAT =B LE FEG198 EST ECRIT (DANS LE CAS D IMPRESSION A 198 CH         
# ********************************************************************         
#  CREATION DE L'ETAT : IFTI01                                                 
#  REPRISE: NON                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LBX PGM=IKJEFT01   ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LBX
       ;;
(FTI02LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *********************************** TABLE GENERALISEE                        
#    RTGA01   : NAME=RSGA01L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA01 /dev/null
# *********************************** TABLE DES SOUS/TABLES                    
#    RTGA71   : NAME=RSGA71L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTGA71 /dev/null
# *********************************** TABLE DU GENERATEUR D'ETATS              
#    RTEG00   : NAME=RSEG00L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG00 /dev/null
#    RTEG05   : NAME=RSEG05L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG05 /dev/null
#    RTEG10   : NAME=RSEG10L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG10 /dev/null
#    RTEG11   : NAME=RSEG11L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG11 /dev/null
#    RTEG15   : NAME=RSEG15L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG15 /dev/null
#    RTEG20   : NAME=RSEG20L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG20 /dev/null
#    RTEG25   : NAME=RSEG25L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG25 /dev/null
#    RTEG30   : NAME=RSEG30L,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RTEG30 /dev/null
# *********************************** FICHIER FEXTRAC TRIE                     
       m_FileAssign -d SHR -g ${G_A15} FEXTRAC ${DATA}/PTEM/FTI02LBM.IFTI02BL
# *********************************** FICHIER FCUMULS TRIE                     
       m_FileAssign -d SHR -g ${G_A16} FCUMULS ${DATA}/PTEM/FTI02LBT.IFTI02DL
# *********************************** PARAMETRE DATE                           
       m_FileAssign -i FDATE
$FDATE
_end
# *********************************** PARAMETRE SOCIETE                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************** PARAMETRE  MOIS                          
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************** PARAMETRE  FICHIER S/36(156)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** PARAMETRE  FICHIER S/36(222)             
       m_FileAssign -d SHR FEG198 /dev/null
# *********************************** FICHIER D'IMPRESSION                     
       m_OutputAssign -c 9 -w IFTI01 FEDITION
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEG060 
       JUMP_LABEL=FTI02LBY
       ;;
(FTI02LBY)
       m_CondExec 04,GE,FTI02LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  R A Z  DES FICHIERS EN ENTREE DE L'ICS POUR EVITER LES TRAITEMENTS          
#         EN DOUBLE.                                                           
#  REPRISE : NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LCA PGM=IDCAMS     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LCA
       ;;
(FTI02LCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ***   FIC DE CUMUL DES INTERFACES (EN CAS DE PLANTAGE DE L'ICS)              
       m_FileAssign -d SHR IN1 /dev/null
# ***   FICHIER CUMUL REMIS A ZERO                                             
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F61.FTI02REL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02LCA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTI02LCB
       ;;
(FTI02LCB)
       m_CondExec 16,NE,FTI02LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   SORT DU FICHIER FFTI01 ISSU DU PGM BFTI06                                  
#   REPRISE: NON                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LCD
       ;;
(FTI02LCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/FTI02LAQ.FFTI02EL
       m_FileAssign -d SHR -g ${G_A18} -C ${DATA}/PXX0/F61.FTRECYFL
#  SORTIE POUR CONSO                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/FTI02LCD.FFTI02FL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_19_381 19 CH 381
 /FIELDS FLD_CH_1_15 01 CH 15
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_BI_19_381 ASCENDING
 /SUMMARIZE
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTI02LCE
       ;;
(FTI02LCE)
       m_CondExec 00,EQ,FTI02LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EASYTRIEVE                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LCG PGM=EZTPA00    ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LCG
       ;;
(FTI02LCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" IMPRIM
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A19} FICHI ${DATA}/PXX0/F61.FFTI02L
       m_FileAssign -d NEW,CATLG,DELETE -r 500 -t LSEQ -g +1 FICHO ${DATA}/PXX0/F61.FTI02SL
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/FTI02LCG
       m_ProgramExec FTI02LCG
# ********************************************************************         
#  FORMATAGE SYSIN PUT FTP FT??????                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LCJ PGM=EZACFSM1   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LCJ
       ;;
(FTI02LCJ)
       m_CondExec ${EXADR},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02LCJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/FTI02LCJ.FTFTI02L
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FT??????                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTI02LCM PGM=FTP        ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LCM
       ;;
(FTI02LCM)
       m_CondExec ${EXADW},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02LCM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.FTI02LCJ.FTFTI02L(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# ********************************************************************         
#   DEPENDANCE POUR PLAN                                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=FTI02LZA
       ;;
(FTI02LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTI02LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
