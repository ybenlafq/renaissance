#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  ADO01P.ksh                       --- VERSION DU 08/10/2016 13:22
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPADO01 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/08/29 AT 14.53.06 BY BURTECA                      
#    STANDARDS: P  JOBSET: ADO01P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
#                                                                              
# ********************************************************************         
#  EXTRACTION RTVT01                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=ADO01PA
       ;;
(ADO01PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXAGY=${EXAGY:-0}
       EXAHD=${EXAHD:-0}
       EXAHI=${EXAHI:-0}
       EXAHN=${EXAHN:-0}
       EXAHS=${EXAHS:-0}
       EXAHX=${EXAHX:-0}
       EXAIC=${EXAIC:-0}
       EXAIH=${EXAIH:-0}
       EXAIM=${EXAIM:-0}
       EXAIR=${EXAIR:-0}
       EXAIW=${EXAIW:-0}
       EXAJB=${EXAJB:-0}
       EXAJG=${EXAJG:-0}
       EXAJL=${EXAJL:-0}
       EXAJQ=${EXAJQ:-0}
       EXAJV=${EXAJV:-0}
       EXAKA=${EXAKA:-0}
       EXAKF=${EXAKF:-0}
       EXAKK=${EXAKK:-0}
       EXAKP=${EXAKP:-0}
       EXAKU=${EXAKU:-0}
       EXAKZ=${EXAKZ:-0}
       EXALE=${EXALE:-0}
       EXALJ=${EXALJ:-0}
       EXALO=${EXALO:-0}
       EXALT=${EXALT:-0}
       EXALY=${EXALY:-0}
       EXAMD=${EXAMD:-0}
       EXAMI=${EXAMI:-0}
       EXAMN=${EXAMN:-0}
       EXAMS=${EXAMS:-0}
       EXAMX=${EXAMX:-0}
       EXANC=${EXANC:-0}
       EXANH=${EXANH:-0}
       EXANM=${EXANM:-0}
       EXANR=${EXANR:-0}
       EXANW=${EXANW:-0}
       EXAOB=${EXAOB:-0}
       EXAOG=${EXAOG:-0}
       EXAOL=${EXAOL:-0}
       EXAOQ=${EXAOQ:-0}
       EXAOV=${EXAOV:-0}
       EXAPA=${EXAPA:-0}
       EXAPF=${EXAPF:-0}
       EXAPK=${EXAPK:-0}
       EXAPP=${EXAPP:-0}
       EXAPU=${EXAPU:-0}
       EXAPZ=${EXAPZ:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A26=${G_A26:-'+1'}
       G_A27=${G_A27:-'+1'}
       G_A28=${G_A28:-'+1'}
       G_A29=${G_A29:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A30=${G_A30:-'+1'}
       G_A31=${G_A31:-'+1'}
       G_A32=${G_A32:-'+1'}
       G_A33=${G_A33:-'+1'}
       G_A34=${G_A34:-'+1'}
       G_A35=${G_A35:-'+1'}
       G_A36=${G_A36:-'+1'}
       G_A37=${G_A37:-'+1'}
       G_A38=${G_A38:-'+1'}
       G_A39=${G_A39:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A40=${G_A40:-'+1'}
       G_A41=${G_A41:-'+1'}
       G_A42=${G_A42:-'+1'}
       G_A43=${G_A43:-'+1'}
       G_A44=${G_A44:-'+1'}
       G_A45=${G_A45:-'+1'}
       G_A46=${G_A46:-'+1'}
       G_A47=${G_A47:-'+1'}
       G_A48=${G_A48:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       PROCSTEP=${PROCSTEP:-ADO01PA}
# ********************************************************************         
# *    GENERATED ON SATURDAY  2015/08/29 AT 14.53.06 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: ADO01P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'XXXXXXXXXXXX'                          
# *                           APPL...: REPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=ADO01PAA
       ;;
(ADO01PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    APRES SAUVEGARDES DU SOIR         *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PAA.UNVT01AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PAD
       ;;
(ADO01PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/ADO01PAA.UNVT01AP
       m_FileAssign -d NEW,CATLG,DELETE -r 154 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT01BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AAK      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT01BP.*,+                                            
# RTVT01_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001AP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AAP      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT01BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001BP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001AP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PAG PGM=EZACFSM1   ** ID=AAK                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_156 1 CH 156
 /FIELDS FLD_CH_3_154 3 CH 154
 /KEYS
   FLD_CH_1_156 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_154
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PAG
       ;;
(ADO01PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PAG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PAG.ADO001AP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PAE
       ;;
(ADO01PAE)
       m_CondExec 00,EQ,ADO01PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PAJ PROC=JVZIP     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PAJ
       ;;
(ADO01PAJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAAP},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A2} DD1 ${DATA}/PXX0/F07.UNVT01BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001BP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PAJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PAG.ADO001AP(+1),DISP=SHR               ~         
#
       JUMP_LABEL=ADO01PAM
       ;;
(ADO01PAM)
       m_CondExec ${EXAAU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PAM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PAM.TADO01BP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01BP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PAQ PGM=FTP        ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PAQ
       ;;
(ADO01PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PAQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PAM.TADO01BP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PAT PGM=IDCAMS     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PAT
       ;;
(ADO01PAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PAT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PAU
       ;;
(ADO01PAU)
       m_CondExec 16,NE,ADO01PAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT02                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PAX PGM=PTLDRIVM   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PAX
       ;;
(ADO01PAX)
       m_CondExec ${EXABJ},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PAX.UNVT02AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PBA
       ;;
(ADO01PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/ADO01PAX.UNVT02AP
       m_FileAssign -d NEW,CATLG,DELETE -r 297 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT02BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABT      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT02BP.*,+                                            
# RTVT02_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001CP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ABY      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT02BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001DP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001CP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PBD PGM=EZACFSM1   ** ID=ABT                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_299 1 CH 299
 /FIELDS FLD_CH_3_297 3 CH 297
 /KEYS
   FLD_CH_1_299 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_297
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PBD
       ;;
(ADO01PBD)
       m_CondExec ${EXABT},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PBD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PBD.ADO001CP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PBB
       ;;
(ADO01PBB)
       m_CondExec 00,EQ,ADO01PBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PBG PROC=JVZIP     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PBG
       ;;
(ADO01PBG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXABY},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A6} DD1 ${DATA}/PXX0/F07.UNVT02BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001DP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PBG.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PBD.ADO001CP(+1),DISP=SHR               ~         
#
       JUMP_LABEL=ADO01PBJ
       ;;
(ADO01PBJ)
       m_CondExec ${EXACD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PBJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PBJ.TADO01DP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01DP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PBM PGM=FTP        ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PBM
       ;;
(ADO01PBM)
       m_CondExec ${EXACI},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PBM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PBJ.TADO01DP(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PBQ PGM=IDCAMS     ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PBQ
       ;;
(ADO01PBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PBQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PBR
       ;;
(ADO01PBR)
       m_CondExec 16,NE,ADO01PBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT05                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PBT PGM=PTLDRIVM   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PBT
       ;;
(ADO01PBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PBT.UNVT05AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PBT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PBX
       ;;
(ADO01PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PTEM/ADO01PBT.UNVT05AP
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT05BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ADC      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT05BP.*,+                                            
# RTVT05_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001EP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ADH      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT05BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001FP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001EP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PCA PGM=EZACFSM1   ** ID=ADC                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_104 1 CH 104
 /FIELDS FLD_CH_3_100 3 CH 100
 /KEYS
   FLD_CH_1_104 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_100
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PCA
       ;;
(ADO01PCA)
       m_CondExec ${EXADC},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PCA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PCA.ADO001EP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PBY
       ;;
(ADO01PBY)
       m_CondExec 00,EQ,ADO01PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PCD PROC=JVZIP     ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PCD
       ;;
(ADO01PCD)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXADH},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A10} DD1 ${DATA}/PXX0/F07.UNVT05BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001FP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PCD.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PCA.ADO001EP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PCG
       ;;
(ADO01PCG)
       m_CondExec ${EXADM},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PCG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PCG.TADO01FP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01FP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PCJ PGM=FTP        ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PCJ
       ;;
(ADO01PCJ)
       m_CondExec ${EXADR},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PCJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PCG.TADO01FP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PCM PGM=IDCAMS     ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PCM
       ;;
(ADO01PCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PCM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PCN
       ;;
(ADO01PCN)
       m_CondExec 16,NE,ADO01PCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT06                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PCQ PGM=PTLDRIVM   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PCQ
       ;;
(ADO01PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PCQ.UNVT06AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PCQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PCT
       ;;
(ADO01PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PTEM/ADO01PCQ.UNVT06AP
       m_FileAssign -d NEW,CATLG,DELETE -r 48 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT06BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AEL      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT06BP.*,+                                            
# RTVT06_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001GP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AEQ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT06BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001HP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001GP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PCX PGM=EZACFSM1   ** ID=AEL                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_53 1 CH 53
 /FIELDS FLD_CH_3_48 3 CH 48
 /KEYS
   FLD_CH_1_53 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_48
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PCX
       ;;
(ADO01PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PCX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PCX.ADO001GP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PCU
       ;;
(ADO01PCU)
       m_CondExec 00,EQ,ADO01PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PDA PROC=JVZIP     ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PDA
       ;;
(ADO01PDA)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAEQ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A14} DD1 ${DATA}/PXX0/F07.UNVT06BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001HP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PDA.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PCX.ADO001GP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PDD
       ;;
(ADO01PDD)
       m_CondExec ${EXAEV},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PDD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PDD.TADO01HP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01HP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PDG PGM=FTP        ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PDG
       ;;
(ADO01PDG)
       m_CondExec ${EXAFA},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PDG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PDD.TADO01HP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PDJ PGM=IDCAMS     ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PDJ
       ;;
(ADO01PDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PDJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PDK
       ;;
(ADO01PDK)
       m_CondExec 16,NE,ADO01PDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT08                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PDM PGM=PTLDRIVM   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PDM
       ;;
(ADO01PDM)
       m_CondExec ${EXAFK},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PDM.UNVT08AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PDM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PDQ PGM=SORT       ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PDQ
       ;;
(ADO01PDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PTEM/ADO01PDM.UNVT08AP
       m_FileAssign -d NEW,CATLG,DELETE -r 134 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT08BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AFU      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT08BP.*,+                                            
# RTVT08_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001IP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AFZ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT08BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001JP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001IP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PDT PGM=EZACFSM1   ** ID=AFU                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_136 1 CH 136
 /FIELDS FLD_CH_3_134 3 CH 134
 /KEYS
   FLD_CH_1_136 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_134
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PDT
       ;;
(ADO01PDT)
       m_CondExec ${EXAFU},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PDT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PDT.ADO001IP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PDR
       ;;
(ADO01PDR)
       m_CondExec 00,EQ,ADO01PDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PDX PROC=JVZIP     ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PDX
       ;;
(ADO01PDX)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAFZ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A18} DD1 ${DATA}/PXX0/F07.UNVT08BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001JP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PDX.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PDT.ADO001IP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PEA
       ;;
(ADO01PEA)
       m_CondExec ${EXAGE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PEA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PEA.TADO01JP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01JP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PED PGM=FTP        ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PED
       ;;
(ADO01PED)
       m_CondExec ${EXAGJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PED.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PEA.TADO01JP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PEG PGM=IDCAMS     ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PEG
       ;;
(ADO01PEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PEG.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PEH
       ;;
(ADO01PEH)
       m_CondExec 16,NE,ADO01PEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT11                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PEJ PGM=PTLDRIVM   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PEJ
       ;;
(ADO01PEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PEJ.UNVT11AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PEJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PEM PGM=SORT       ** ID=AGY                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PEM
       ;;
(ADO01PEM)
       m_CondExec ${EXAGY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PTEM/ADO01PEJ.UNVT11AP
       m_FileAssign -d NEW,CATLG,DELETE -r 228 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT11BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AHD      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT11BP.*,+                                            
# RTVT11_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001KP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AHI      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT11BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001LP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001KP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PEQ PGM=EZACFSM1   ** ID=AHD                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_230 1 CH 230
 /FIELDS FLD_CH_3_228 3 CH 228
 /KEYS
   FLD_CH_1_230 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_228
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PEQ
       ;;
(ADO01PEQ)
       m_CondExec ${EXAHD},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PEQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PEQ.ADO001KP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PEN
       ;;
(ADO01PEN)
       m_CondExec 00,EQ,ADO01PEM ${EXAGY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PET PROC=JVZIP     ** ID=AHI                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PET
       ;;
(ADO01PET)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAHI},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A22} DD1 ${DATA}/PXX0/F07.UNVT11BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001LP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PET.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PEQ.ADO001KP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PEX
       ;;
(ADO01PEX)
       m_CondExec ${EXAHN},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PEX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PEX.TADO01LP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01LP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PFA PGM=FTP        ** ID=AHS                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PFA
       ;;
(ADO01PFA)
       m_CondExec ${EXAHS},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PFA.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PEX.TADO01LP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PFD PGM=IDCAMS     ** ID=AHX                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PFD
       ;;
(ADO01PFD)
       m_CondExec ${EXAHX},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PFD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PFE
       ;;
(ADO01PFE)
       m_CondExec 16,NE,ADO01PFD ${EXAHX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT12                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PFG PGM=PTLDRIVM   ** ID=AIC                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PFG
       ;;
(ADO01PFG)
       m_CondExec ${EXAIC},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PFG.UNVT12AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PFG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PFJ PGM=SORT       ** ID=AIH                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PFJ
       ;;
(ADO01PFJ)
       m_CondExec ${EXAIH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A25} SORTIN ${DATA}/PTEM/ADO01PFG.UNVT12AP
       m_FileAssign -d NEW,CATLG,DELETE -r 176 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT12BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AIM      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT12BP.*,+                                            
# RTVT12_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001MP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AIR      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT12BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001NP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001MP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PFM PGM=EZACFSM1   ** ID=AIM                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_227 1 CH 227
 /FIELDS FLD_CH_3_176 3 CH 176
 /KEYS
   FLD_CH_1_227 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_176
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PFM
       ;;
(ADO01PFM)
       m_CondExec ${EXAIM},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PFM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PFM.ADO001MP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PFK
       ;;
(ADO01PFK)
       m_CondExec 00,EQ,ADO01PFJ ${EXAIH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PFQ PROC=JVZIP     ** ID=AIR                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PFQ
       ;;
(ADO01PFQ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAIR},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A26} DD1 ${DATA}/PXX0/F07.UNVT12BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001NP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PFQ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PFM.ADO001MP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PFT
       ;;
(ADO01PFT)
       m_CondExec ${EXAIW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PFT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PFT.TADO01NP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01NP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PFX PGM=FTP        ** ID=AJB                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PFX
       ;;
(ADO01PFX)
       m_CondExec ${EXAJB},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PFX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PFT.TADO01NP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PGA PGM=IDCAMS     ** ID=AJG                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PGA
       ;;
(ADO01PGA)
       m_CondExec ${EXAJG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PGA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PGB
       ;;
(ADO01PGB)
       m_CondExec 16,NE,ADO01PGA ${EXAJG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT13                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PGD PGM=PTLDRIVM   ** ID=AJL                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PGD
       ;;
(ADO01PGD)
       m_CondExec ${EXAJL},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PGD.UNVT13AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PGD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PGG PGM=SORT       ** ID=AJQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PGG
       ;;
(ADO01PGG)
       m_CondExec ${EXAJQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A29} SORTIN ${DATA}/PTEM/ADO01PGD.UNVT13AP
       m_FileAssign -d NEW,CATLG,DELETE -r 198 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT13BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AJV      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT13BP.*,+                                            
# RTVT13_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001OP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AKA      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT13BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001QP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001OP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PGJ PGM=EZACFSM1   ** ID=AJV                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_198 3 CH 198
 /FIELDS FLD_CH_1_250 1 CH 250
 /KEYS
   FLD_CH_1_250 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_198
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PGJ
       ;;
(ADO01PGJ)
       m_CondExec ${EXAJV},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PGJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PGJ.ADO001OP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PGH
       ;;
(ADO01PGH)
       m_CondExec 00,EQ,ADO01PGG ${EXAJQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PGM PROC=JVZIP     ** ID=AKA                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PGM
       ;;
(ADO01PGM)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAKA},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A30} DD1 ${DATA}/PXX0/F07.UNVT13BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001QP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PGM.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PGJ.ADO001OP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PGQ
       ;;
(ADO01PGQ)
       m_CondExec ${EXAKF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PGQ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PGQ.TADO01QP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01QP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PGT PGM=FTP        ** ID=AKK                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PGT
       ;;
(ADO01PGT)
       m_CondExec ${EXAKK},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PGT.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PGQ.TADO01QP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PGX PGM=IDCAMS     ** ID=AKP                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PGX
       ;;
(ADO01PGX)
       m_CondExec ${EXAKP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PGX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PGY
       ;;
(ADO01PGY)
       m_CondExec 16,NE,ADO01PGX ${EXAKP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT14                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PHA PGM=PTLDRIVM   ** ID=AKU                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PHA
       ;;
(ADO01PHA)
       m_CondExec ${EXAKU},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PHA.UNVT14AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PHA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PHD PGM=SORT       ** ID=AKZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PHD
       ;;
(ADO01PHD)
       m_CondExec ${EXAKZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A33} SORTIN ${DATA}/PTEM/ADO01PHA.UNVT14AP
       m_FileAssign -d NEW,CATLG,DELETE -r 151 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT14BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ALE      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT14BP.*,+                                            
# RTVT14_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001RP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ALJ      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT14BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001SP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001RP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PHG PGM=EZACFSM1   ** ID=ALE                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_151 3 CH 151
 /FIELDS FLD_CH_1_166 1 CH 166
 /KEYS
   FLD_CH_1_166 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_151
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PHG
       ;;
(ADO01PHG)
       m_CondExec ${EXALE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PHG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PHG.ADO001RP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PHE
       ;;
(ADO01PHE)
       m_CondExec 00,EQ,ADO01PHD ${EXAKZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PHJ PROC=JVZIP     ** ID=ALJ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PHJ
       ;;
(ADO01PHJ)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXALJ},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A34} DD1 ${DATA}/PXX0/F07.UNVT14BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001SP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PHJ.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PHG.ADO001RP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PHM
       ;;
(ADO01PHM)
       m_CondExec ${EXALO},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PHM.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PHM.TADO01SP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01SP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PHQ PGM=FTP        ** ID=ALT                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PHQ
       ;;
(ADO01PHQ)
       m_CondExec ${EXALT},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PHQ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PHM.TADO01SP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PHT PGM=IDCAMS     ** ID=ALY                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PHT
       ;;
(ADO01PHT)
       m_CondExec ${EXALY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PHT.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PHU
       ;;
(ADO01PHU)
       m_CondExec 16,NE,ADO01PHT ${EXALY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT15                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PHX PGM=PTLDRIVM   ** ID=AMD                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PHX
       ;;
(ADO01PHX)
       m_CondExec ${EXAMD},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PHX.UNVT15AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PHX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PIA PGM=SORT       ** ID=AMI                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PIA
       ;;
(ADO01PIA)
       m_CondExec ${EXAMI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A37} SORTIN ${DATA}/PTEM/ADO01PHX.UNVT15AP
       m_FileAssign -d NEW,CATLG,DELETE -r 176 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT15BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AMN      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT15BP.*,+                                            
# RTVT15_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001TP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AMS      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT15BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001UP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001TP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PID PGM=EZACFSM1   ** ID=AMN                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_176 3 CH 176
 /FIELDS FLD_CH_1_215 1 CH 215
 /KEYS
   FLD_CH_1_215 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_176
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PID
       ;;
(ADO01PID)
       m_CondExec ${EXAMN},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PID.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PID.ADO001TP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PIB
       ;;
(ADO01PIB)
       m_CondExec 00,EQ,ADO01PIA ${EXAMI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PIG PROC=JVZIP     ** ID=AMS                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PIG
       ;;
(ADO01PIG)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAMS},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A38} DD1 ${DATA}/PXX0/F07.UNVT15BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001UP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PIG.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PID.ADO001TP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PIJ
       ;;
(ADO01PIJ)
       m_CondExec ${EXAMX},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PIJ.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PIJ.TADO01UP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01UP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PIM PGM=FTP        ** ID=ANC                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PIM
       ;;
(ADO01PIM)
       m_CondExec ${EXANC},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PIM.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PIJ.TADO01UP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PIQ PGM=IDCAMS     ** ID=ANH                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PIQ
       ;;
(ADO01PIQ)
       m_CondExec ${EXANH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PIQ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PIR
       ;;
(ADO01PIR)
       m_CondExec 16,NE,ADO01PIQ ${EXANH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT16                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PIT PGM=PTLDRIVM   ** ID=ANM                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PIT
       ;;
(ADO01PIT)
       m_CondExec ${EXANM},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PIT.UNVT16AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PIT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PIX PGM=SORT       ** ID=ANR                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PIX
       ;;
(ADO01PIX)
       m_CondExec ${EXANR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A41} SORTIN ${DATA}/PTEM/ADO01PIT.UNVT16AP
       m_FileAssign -d NEW,CATLG,DELETE -r 174 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT16BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# ANW      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT16BP.*,+                                            
# RTVT16_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001VP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# AOB      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT16BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001WP,MODE=O                                          
#                                                                              
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001VP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PJA PGM=EZACFSM1   ** ID=ANW                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_213 1 CH 213
 /FIELDS FLD_CH_3_174 3 CH 174
 /KEYS
   FLD_CH_1_213 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_174
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PJA
       ;;
(ADO01PJA)
       m_CondExec ${EXANW},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PJA.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PJA.ADO001VP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PIY
       ;;
(ADO01PIY)
       m_CondExec 00,EQ,ADO01PIX ${EXANR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PJD PROC=JVZIP     ** ID=AOB                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PJD
       ;;
(ADO01PJD)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAOB},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A42} DD1 ${DATA}/PXX0/F07.UNVT16BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001WP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PJD.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PJA.ADO001VP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PJG
       ;;
(ADO01PJG)
       m_CondExec ${EXAOG},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PJG.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PJG.TADO01WP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01WP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PJJ PGM=FTP        ** ID=AOL                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PJJ
       ;;
(ADO01PJJ)
       m_CondExec ${EXAOL},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PJJ.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PJG.TADO01WP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PJM PGM=IDCAMS     ** ID=AOQ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PJM
       ;;
(ADO01PJM)
       m_CondExec ${EXAOQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PJM.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PJN
       ;;
(ADO01PJN)
       m_CondExec 16,NE,ADO01PJM ${EXAOQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  EXTRACTION RTVT17                                                           
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PJQ PGM=PTLDRIVM   ** ID=AOV                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PJQ
       ;;
(ADO01PJQ)
       m_CondExec ${EXAOV},NE,YES 
       m_FileAssign -d NEW,CATLG,CATLG -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/ADO01PJQ.UNVT17AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PJQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PJT PGM=SORT       ** ID=APA                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PJT
       ;;
(ADO01PJT)
       m_CondExec ${EXAPA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A45} SORTIN ${DATA}/PTEM/ADO01PJQ.UNVT17AP
       m_FileAssign -d NEW,CATLG,DELETE -r 212 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.UNVT17BP
# ***************************************************************              
#                                                                              
# ***************************************************************              
# APF      STEP  PGM=EZACFSM1                                                  
# SYSIN    DATA  *                                                             
# -ZIPPED_DSN(PXX0.F07.UNVT17BP.*,+                                            
# RTVT17_&YR4.&LMON.&LDAY_PART23.TXT)                                          
# -ARCHIVE_OUTFILE(FICZIP)                                                     
#          DATAEND                                                             
# SYSOUT   FILE  NAME=ADO001XP,MODE=O                                          
# ***************************************************************              
#                                                                              
# ***************************************************************              
# APK      STEP  PGM=PKZIP                                                     
# SYSPRINT REPORT SYSOUT=*                                                     
# SYSUDUMP REPORT SYSOUT=*                                                     
# SYSOUT   REPORT SYSOUT=*                                                     
# SYSTSPRT REPORT SYSOUT=*                                                     
# SYSABEND REPORT SYSOUT=*                                                     
# *                                                                            
# DD1      FILE  NAME=UNVT17BP,MODE=I                                          
# *                                                                            
# FICZIP   FILE  NAME=ADO001YP,MODE=O                                          
# *                                                                            
# SYSIN    DATA  *                                                             
# -ECHO                                                                        
# -ACTION(ADD)                                                                 
# -COMPRESSION_LEVEL(NORMAL)                                                   
# -TRAN(ANSI850)                                                               
# -TEXT                                                                        
# -FILE_TERMINATOR()                                                           
# -INFILE(DD1)                                                                 
#        DATAEND                                                               
#          FILE  NAME=ADO001XP,MODE=I                                          
# ***************************************************************              
# POUR INFO //DD:DD1 -N RTEM51_20141022.TXT                                    
# ***************************************************************              
#                                                                              
# ***********************************                                          
# *   STEP ADO01PJX PGM=EZACFSM1   ** ID=APF                                   
# ***********************************                                          
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_3_212 3 CH 212
 /FIELDS FLD_CH_1_251 1 CH 251
 /KEYS
   FLD_CH_1_251 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_3_212
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=ADO01PJX
       ;;
(ADO01PJX)
       m_CondExec ${EXAPF},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PJX.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PJX.ADO001XP
       m_ProgramExec EZACFSM1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PJU
       ;;
(ADO01PJU)
       m_CondExec 00,EQ,ADO01PJT ${EXAPA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  ZIP AVEC JAVAZIP                                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PKA PROC=JVZIP     ** ID=APK                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PKA
       ;;
(ADO01PKA)
       m_ProcInclude JVZIP COND="${PROCSTEP}.${EXAPK},NE,YES "
# ****** FICHIER EN ENTREE DE ZIP                                              
       m_FileOverride -d SHR -s JVZIPU -g ${G_A46} DD1 ${DATA}/PXX0/F07.UNVT17BP
       m_FileOverride -d NEW,CATLG,DELETE -r 27998 -t LSEQ -s JVZIPU -g +1 FICZIP ${DATA}/PXX0/F07.ADO001YP
       m_FileOverride -d SHR -s JVZIPU SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PKA.sysin
# File translated and concatened in SYSIN
# //         DD  DSN=PTEM.ADO01PJX.ADO001XP(+1),DISP=SHR              ~         
#
       JUMP_LABEL=ADO01PKD
       ;;
(ADO01PKD)
       m_CondExec ${EXAPP},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PKD.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/ADO01PKD.TADO01YP
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU TADO01YP                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP ADO01PKG PGM=FTP        ** ID=APU                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PKG
       ;;
(ADO01PKG)
       m_CondExec ${EXAPU},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PKG.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.ADO01PKD.TADO01YP(+1),DISP=SHR              ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
#  SUPPRESSION DES GENERATIONS DE FICHIERS SI TRAITEMENT OK                    
#  REPRISE: OUI                                                                
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP ADO01PKJ PGM=IDCAMS     ** ID=APZ                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PKJ
       ;;
(ADO01PKJ)
       m_CondExec ${EXAPZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PKJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=ADO01PKK
       ;;
(ADO01PKK)
       m_CondExec 16,NE,ADO01PKJ ${EXAPZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=ADO01PZA
       ;;
(ADO01PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/ADO01PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
