#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  CGSIGY.ksh                       --- VERSION DU 17/10/2016 18:03
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYCGSIG -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/08/07 AT 16.03.11 BY BURTECA                      
#    STANDARDS: P  JOBSET: CGSIGY                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  TRI DES FICHIERS ECS                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=CGSIGYA
       ;;
(CGSIGYA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/08/07 AT 16.03.11 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: CGSIGY                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'CONVERGENCE'                           
# *                           APPL...: REPLYON                                 
# *                           BACKOUT: JOBSET                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=CGSIGYAA
       ;;
(CGSIGYAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# *********************************                                            
# ------  FICHIER GCT ISSU DE SIGCTY DDNAME=FGCT (LONGUEUR 250)                
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGY/F45.SIGGCTY
# ******  FICHIER GCT ISSU DE SIGC2Y DDNAME=FGCT (LONGUEUR 250)                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGY/F45.SIGGC2Y
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CGSIGYAA.CGSIGDY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGSIGYAB
       ;;
(CGSIGYAB)
       m_CondExec 00,EQ,CGSIGYAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DES FICHIERS ECS                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYAD
       ;;
(CGSIGYAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIER GCT ISSU DE SIGCTY                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/CGSIGYAA.CGSIGDY
# ******  FICHIER DE RECYCLAGE                                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.CGSIGY0
# ******  FICHIER DE REPRISE                                                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F45.CGSIGY.REPRISE
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/CGSIGYAD.CGSIGY1
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_235_1 235 CH 1
 /FIELDS FLD_CH_18_10 18 CH 10
 /FIELDS FLD_CH_186_3 186 CH 3
 /FIELDS FLD_CH_1_17 1 CH 17
 /KEYS
   FLD_CH_1_17 ASCENDING,
   FLD_CH_186_3 ASCENDING,
   FLD_CH_18_10 ASCENDING,
   FLD_CH_235_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=CGSIGYAE
       ;;
(CGSIGYAE)
       m_CondExec 00,EQ,CGSIGYAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG010                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYAG
       ;;
(CGSIGYAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FIC ISSU DU TRI                                                       
       m_FileAssign -d SHR -g ${G_A2} FFTV02 ${DATA}/PTEM/CGSIGYAD.CGSIGY1
# ******  TABLE EN LECTURE                                                     
#    RSGA10   : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10 /dev/null
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSFM99   : NAME=RSFM99,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSFM99 /dev/null
# ****** FICHIERS EN SORTIE POUR LE BCG020 ET LE BCG030                        
       m_FileAssign -d NEW,CATLG,DELETE -r 50 -t LSEQ -g +1 FICANO ${DATA}/PTEM/CGSIGYAG.CGSIGY2
# ****** FICHIERS EN SORTIE POUR LE PGM BCG020                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 350 -t LSEQ -g +1 FCG010 ${DATA}/PTEM/CGSIGYAG.CGSIGY3
# ------  ETAT DES ANOMALIES                                                   
       m_OutputAssign -c 9 -w ICG010 ICG010
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG010 
       JUMP_LABEL=CGSIGYAH
       ;;
(CGSIGYAH)
       m_CondExec 04,GE,CGSIGYAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BCG020                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYAJ PGM=IKJEFT01   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYAJ
       ;;
(CGSIGYAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FICANO ${DATA}/PTEM/CGSIGYAG.CGSIGY2
       m_FileAssign -d SHR -g ${G_A4} FCG010 ${DATA}/PTEM/CGSIGYAG.CGSIGY3
# ****** FICHIER  EN SORTIE VER XFB GATEWAY                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 1100 -t LSEQ -g +1 FCG020 ${DATA}/PXX0/F45.CGSIGY
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BCG020 
       JUMP_LABEL=CGSIGYAK
       ;;
(CGSIGYAK)
       m_CondExec 04,GE,CGSIGYAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BCG030                                                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYAM PGM=BCG030     ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYAM
       ;;
(CGSIGYAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSOUA
       m_OutputAssign -c "*" SYSOUT
# ****** FICHIERS EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A5} FFTV02 ${DATA}/PTEM/CGSIGYAD.CGSIGY1
       m_FileAssign -d SHR -g ${G_A6} FICANO ${DATA}/PTEM/CGSIGYAG.CGSIGY2
# ****** FICHIER  EN SORTIE POUR RECYCLAGE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FREPRIS ${DATA}/PXX0/F45.CGSIGY0
       m_ProgramExec BCG030 
#                                                                              
# ********************************************************************         
#  ENVOI DU FICHIER VERS GATEWAY POUR ROUTAGE VERS SAP                         
#  REPRISE : OUI                                                               
# ********************************************************************         
# AAU      STEP  PGM=IEFBR14,PATTERN=CFT                                       
# CFTIN    DATA  *                                                             
# SEND PART=XFBPRO,                                                            
#      IDF=SAPXXXP,                                                            
#      FNAME=":CGSIGY""(0),                                                    
#      NFNAME=CGSIGY_&FDATE&FTIME                                              
#          DATAEND                                                             
# ******************************************************************           
#  REMISE A ZERO DU FICHIER DE REPRISE CGSIGYR                                 
#                   FICHIER BSIG25DY DDNAME=FFDC                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYAQ PGM=IDCAMS     ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYAQ
       ;;
(CGSIGYAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# ******   ENTREE                                                              
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F45.CGSIGY.REPRISE
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -t LSEQ -g +1 OUT2 ${DATA}/PNCGY/F45.SIGAFDC
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGYAQ.sysin
       m_UtilityExec
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=CGSIGYAR
       ;;
(CGSIGYAR)
       m_CondExec 16,NE,CGSIGYAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  FORMATAGE SYSIN PUT FTP FTCGSIGY                                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYAT PGM=EZACFSM1   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYAT
       ;;
(CGSIGYAT)
       m_CondExec ${EXABE},NE,YES 
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGYAT.sysin
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SYSOUT ${DATA}/PTEM/CGSIGYAT.FTCGSIGY
       m_ProgramExec EZACFSM1
# ********************************************************************         
#  ENVOI FTP SUR LA GATEWAY DU FTCGSIGY                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYAX PGM=FTP        ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYAX
       ;;
(CGSIGYAX)
       m_CondExec ${EXABJ},NE,YES 
#                                                                              
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" OUTPUT
       m_FileAssign -d SHR SYSFTPD ${DATA}/IPOX.PARMLIB/TCPFTPSD
       m_FileAssign -d SHR INPUT ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGYAX.sysin
# File translated and concatened in INPUT
# //         DD  DSN=PTEM.CGSIGYAT.FTCGSIGY(+1),DISP=SHR               ~         
#
       m_UtilityExec INPUT
# *********************************************************                    
# ** MISE _A JOUR DU FICHIER LOG SEND FTP DES FICHIERS  ****                    
# *********************************************************                    
#                                                                              
# ***********************************                                          
# *   STEP CGSIGYBA PGM=EZACFSM1   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYBA
       ;;
(CGSIGYBA)
       m_CondExec ${EXABO},NE,YES 
       m_FileAssign -d SHR SYSOUT /dev/null
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGYBA.sysin
       m_ProgramExec EZACFSM1
# ********************************************************************         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=CGSIGYZA
       ;;
(CGSIGYZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/CGSIGYZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
