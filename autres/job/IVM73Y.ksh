#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  IVM73Y.ksh                       --- VERSION DU 08/10/2016 12:47
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYIVM73 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/07/08 AT 15.52.03 BY BURTECA                      
#    STANDARDS: P  JOBSET: IVM73Y                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BIV730 :  CREE FIC DES ECARTS D INVENTAIRE MAGS                             
#  REPRISE:  OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=IVM73YA
       ;;
(IVM73YA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXAFF=${EXAFF:-0}
       EXAFK=${EXAFK:-0}
       EXAFP=${EXAFP:-0}
       EXAFU=${EXAFU:-0}
       EXAFZ=${EXAFZ:-0}
       EXAGE=${EXAGE:-0}
       EXAGJ=${EXAGJ:-0}
       EXAGO=${EXAGO:-0}
       EXAGT=${EXAGT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=IVM73YAA
       ;;
(IVM73YAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES ARTICLES                                                   
#    RSGA00Y  : NAME=RSGA00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA00Y /dev/null
# ******  TABLE GENERALISE (INVDA)                                             
#    RSGA01Y  : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01Y /dev/null
# ******  TABLE DES SEQUENCES FAMILLES EDITIONS (PARAMETRAGES ETATS)           
#    RSGA11Y  : NAME=RSGA11Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA11Y /dev/null
# ******  TABLE INVENTAIRE MAGASIN                                             
#    RSIN00Y  : NAME=RSIN00Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSIN00Y /dev/null
#                                                                              
# ******  TABLE DES ANOMALIES                                                  
#    RSAN00Y  : NAME=RSAN00Y,MODE=U - DYNAM=YES                                
# -X-RSAN00Y  - IS UPDATED IN PLACE WITH MODE=U                                
       m_FileAssign -d SHR RSAN00Y /dev/null
#                                                                              
# ******  FICHIER DES ECARTS D'INVENTAIRE MAGASIN                              
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 FIN130 ${DATA}/PGI945/F45.TIV130AY
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDRA
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV730 
       JUMP_LABEL=IVM73YAB
       ;;
(IVM73YAB)
       m_CondExec 04,GE,IVM73YAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON              
#  SEQ FAMILLE                                                                 
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YAD
       ;;
(IVM73YAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YAD.TIV135AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_92_5 92 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_72_3 72 CH 3
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_55_5 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YAE
       ;;
(IVM73YAE)
       m_CondExec 00,EQ,IVM73YAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV135  : EDITION DES ECARTS FAMILLES ET SOUS-LIEU                          
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YAG
       ;;
(IVM73YAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                         
       m_FileAssign -d SHR -g ${G_A2} FIV130 ${DATA}/PTEM/IVM73YAD.TIV135AY
# ******  FIC D IMPRESSION ECARTS FAMILLES SOUS-LIEU                           
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV135 ${DATA}/PXX0.FICHEML.IVM73Y.TIV135BY
# ******  ETAT NON VALORISE                                                    
       m_OutputAssign -c Z IIV135B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV135 
       JUMP_LABEL=IVM73YAH
       ;;
(IVM73YAH)
       m_CondExec 04,GE,IVM73YAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI SUR SOCIETE MAGASIN SEQUENCE RAYON SEQUENCE FAMILLE                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YAJ
       ;;
(IVM73YAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YAJ.TIV130BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YAK
       ;;
(IVM73YAK)
       m_CondExec 00,EQ,IVM73YAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV136  : EDITION DES ECARTS FAMILLES PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YAM
       ;;
(IVM73YAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS D'INVENTAIRE SOCIETE TRIE                         
       m_FileAssign -d SHR -g ${G_A4} FIV130 ${DATA}/PTEM/IVM73YAJ.TIV130BY
# ******  FIC D IMPRESSION LIEU RAYON FAMILLE                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV136 ${DATA}/PXX0.FICHEML.IVM73Y.TIV136BY
# ******  ETAT NON VALORISE                                                    
       m_OutputAssign -c Z IIV136B
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV136 
       JUMP_LABEL=IVM73YAN
       ;;
(IVM73YAN)
       m_CondExec 04,GE,IVM73YAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SUR SOCIETE SEQUENCE RAYON SEQ FAMILLE            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YAQ
       ;;
(IVM73YAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YAQ.TIV137AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YAR
       ;;
(IVM73YAR)
       m_CondExec 00,EQ,IVM73YAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137  : EDITION PAR FAMILLE TOUS MAGASINS                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YAT PGM=BIV137     ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YAT
       ;;
(IVM73YAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d SHR -g ${G_A6} FIV130 ${DATA}/PTEM/IVM73YAQ.TIV137AY
# ******  EDITION TOUS MAGASINS RAYON FAMILLE                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV137 ${DATA}/PXX0.FICHEML.IVM73Y.TIV137CY
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM73YAT
       m_ProgramExec BIV137 
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR SOCIETE SEQUENCE RAYON SEQ FAM          
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YAX
       ;;
(IVM73YAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A7} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YAX.TIV137BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YAY
       ;;
(IVM73YAY)
       m_CondExec 00,EQ,IVM73YAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV137  : EDITION DES ECARTS SOCIETE PAR FAMILLE                            
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBA PGM=BIV137     ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBA
       ;;
(IVM73YBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A8} FIV130 ${DATA}/PTEM/IVM73YAX.TIV137BY
# ******  EDITION DES ECARTS SOCIETE PAR FAMILLE                               
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV137 ${DATA}/PXX0.FICHEML.IVM73Y.TIV137SY
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/IVM73YBA
       m_ProgramExec BIV137 
# ********************************************************************         
#  TRI DES FICHIERS ECARTS SOCIETE SUR:                                        
#  SOCIETE SEQ FAM MARQUE CODIC LIEU SOUS-LIEU                                 
#  ET JE GARDE QUE LES ECARTS > 0                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBD
       ;;
(IVM73YBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YBD.TIV139AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 X'00000C'
 /DERIVEDFIELD CST_3_13 X'00000D'
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_68_3 68 CH 3
 /FIELDS FLD_CH_85_3 85 CH 3
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_85_3 EQ CST_1_9 OR FLD_CH_85_3 EQ CST_3_13 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_68_3 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_72_3 ASCENDING
 /OMIT CND_1
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YBE
       ;;
(IVM73YBE)
       m_CondExec 00,EQ,IVM73YBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV139  : EDITION DES ECARTS FAMILLE CODIC TOTAL CODIC                      
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBG PGM=BIV139     ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBG
       ;;
(IVM73YBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE TRIE                                      
       m_FileAssign -d SHR -g ${G_A10} FIV130 ${DATA}/PTEM/IVM73YBD.TIV139AY
# ******  EDITION DES ECARTS SOCIETE PAR FAMILLE CODIC TOTAL CODIC             
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV139 ${DATA}/PXX0.FICHEML.IVM73Y.TIV139BY
       m_ProgramExec BIV139 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SOUS-LIEU LIEU DE TRAITEMENT SEQ RAYON SEQ FAMILLE          
#  LIB RAYON LIB FAMILLE LIB MAYQUE ARTICLE                                    
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBJ
       ;;
(IVM73YBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YBJ.TIV140AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_72_3 72 CH 3
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_92_5 92 CH 5
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_72_3 ASCENDING,
   FLD_CH_92_5 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YBK
       ;;
(IVM73YBK)
       m_CondExec 00,EQ,IVM73YBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV140  : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU ET SOUS-LIE         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBM PGM=IKJEFT01   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBM
       ;;
(IVM73YBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A12} FIV140 ${DATA}/PTEM/IVM73YBJ.TIV140AY
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITON                                                               
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV140 ${DATA}/PXX0.FICHEML.IVM73Y.TIV140VY
# ******  MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                        
       m_OutputAssign -c Z IIV140M
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV140A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV140 
       JUMP_LABEL=IVM73YBN
       ;;
(IVM73YBN)
       m_CondExec 04,GE,IVM73YBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SOCIETE SUR :                                     
#  SOCIETE MAGASIN SEQ RAYON SEQ FAM LIB RAYON LIB FAMILLE LIB MAYQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBQ
       ;;
(IVM73YBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A13} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS TRIEES                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YBQ.TIV130CY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YBR
       ;;
(IVM73YBR)
       m_CondExec 00,EQ,IVM73YBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV141 : EDITION DES ECARTS SOCIETE  PRODUITS PAR LIEU                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBT
       ;;
(IVM73YBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  FICHIER DES ECARTS SOCIETE TRIES                                     
       m_FileAssign -d SHR -g ${G_A14} FIV141 ${DATA}/PTEM/IVM73YBQ.TIV130CY
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  EDITON PAR LIEU/RAYON/FAMILLE/CODIC                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV141 ${DATA}/PXX0.FICHEML.IVM73Y.TIV141VY
# ******  MEME ETAT MAIS NON VALORISE POUR LES MAGASINS                        
       m_OutputAssign -c Z IIV141M
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_OutputAssign -c Z IIV141A
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV141 
       JUMP_LABEL=IVM73YBU
       ;;
(IVM73YBU)
       m_CondExec 04,GE,IVM73YBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SUR:                                              
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YBX
       ;;
(IVM73YBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS MAGASINS                                          
       m_FileAssign -d SHR -g ${G_A15} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YBX.TIV130DY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_1_3 1 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YBY
       ;;
(IVM73YBY)
       m_CondExec 00,EQ,IVM73YBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142  : EDITION DES ECARTS MAGASINS PRODUITS                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCA PGM=BIV142     ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCA
       ;;
(IVM73YCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS MAGASINS TRIES                                    
       m_FileAssign -d SHR -g ${G_A16} FIV142 ${DATA}/PTEM/IVM73YBX.TIV130DY
# ******  EDITION RAYON/FAMILLE/CODIC                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142 ${DATA}/PXX0.FICHEML.IVM73Y.TIV142BY
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142A ${DATA}/PXX0.FICHEML.IVM73Y.TIV142AY
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM73YCA
       m_ProgramExec BIV142 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE              
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCD
       ;;
(IVM73YCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIERS DES ECARTS SOCIETE                                          
       m_FileAssign -d SHR -g ${G_A17} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIERS DES ECARTS SOCIETE TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YCD.TIV130EY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YCE
       ;;
(IVM73YCE)
       m_CondExec 00,EQ,IVM73YCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV142  : EDITION DES ECARTS SOCIETE PRODUITS                               
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCG PGM=BIV142     ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCG
       ;;
(IVM73YCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A18} FIV142 ${DATA}/PTEM/IVM73YCD.TIV130EY
# ******  EDITION DES ECARTS SOCIETE RAYON/FAMILLE/CODIC                       
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142 ${DATA}/PXX0.FICHEML.IVM73Y.TIV142SY
# ******  LISTE DES ECARTS NON APPLIQUES INTEGRALEMENT                         
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV142A ${DATA}/PXX0.FICHEML.IVM73Y.TIV142RY
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/IVM73YCG
       m_ProgramExec BIV142 
# ********************************************************************         
#  TRI DU FICHIER DES ECARTS SOCIETEE SUR:                                     
#  SOCIETE MAG SEQ RAYON SEQ FAMILLE LIB RAYON LIB FAMILLE LIB MARQUE          
#  ARTICLE                                                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCJ PGM=SORT       ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCJ
       ;;
(IVM73YCJ)
       m_CondExec ${EXADR},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A19} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIERS DES ECARTS SOCIETE TRIE                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YCJ.TIV143AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_60_5 60 CH 5
 /FIELDS FLD_CH_1_6 1 CH 6
 /FIELDS FLD_CH_18_5 18 CH 5
 /FIELDS FLD_CH_65_6 65 CH 6
 /FIELDS FLD_CH_55_5 55 CH 5
 /FIELDS FLD_CH_23_7 23 CH 7
 /KEYS
   FLD_CH_1_6 ASCENDING,
   FLD_CH_65_6 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_60_5 ASCENDING,
   FLD_CH_55_5 ASCENDING,
   FLD_CH_23_7 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YCK
       ;;
(IVM73YCK)
       m_CondExec 00,EQ,IVM73YCJ ${EXADR},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV143  :  EDITION DES ECARTS SOCIETE PAR LIEU                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCM PGM=IKJEFT01   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCM
       ;;
(IVM73YCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A20} FIV143 ${DATA}/PTEM/IVM73YCJ.TIV143AY
# ******  EDITION DES ECARTS SOCIETE PAR LIEU                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV143 ${DATA}/PXX0.FICHEML.IVM73Y.TIV143BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV143 
       JUMP_LABEL=IVM73YCN
       ;;
(IVM73YCN)
       m_CondExec 04,GE,IVM73YCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER DES ECARTS SOCIETE SUR:                                      
#  SOCIETE SEQ RAYON LIB RAYON MAGASIN                                         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCQ
       ;;
(IVM73YCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A21} SORTIN ${DATA}/PGI945/F45.TIV130AY
# ******  FICHIERS DES ECARTS MAGASINS TRIE                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/IVM73YCQ.TIV144AY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_65_3 65 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_18_5 18 CH 5
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_65_3 ASCENDING,
   FLD_CH_18_5 ASCENDING,
   FLD_CH_4_3 ASCENDING
 /* Record Type = F  Record Length = 100 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=IVM73YCR
       ;;
(IVM73YCR)
       m_CondExec 00,EQ,IVM73YCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BIV144  : EDITION DES ECARTS SOCIETE PAR RAYON                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCT
       ;;
(IVM73YCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLE DES LIEUX                                                      
#    RSGA10Y  : NAME=RSGA10Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA10Y /dev/null
# ******  DATE DE PASSAGE SOUS LA FORME JJMMSSAA                               
       m_FileAssign -i FDATE
$FDATE
_end
# ******  FICHIER DES ECARTS SOCIETE                                           
       m_FileAssign -d SHR -g ${G_A22} FIV144 ${DATA}/PTEM/IVM73YCQ.TIV144AY
# ******  EDITION DES ECARTS SOCIETE PAR RAYON MAGASINS                        
       m_FileAssign -d NEW,CATLG,DELETE -r 133 -t LSEQ IIV144 ${DATA}/PXX0.FICHEML.IVM73Y.TIV144BY
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BIV144 
       JUMP_LABEL=IVM73YCU
       ;;
(IVM73YCU)
       m_CondExec 04,GE,IVM73YCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV135)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YCX PGM=IEBGENER   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YCX
       ;;
(IVM73YCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV135BY
       m_OutputAssign -c 9 -w IIV135 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YCY
       ;;
(IVM73YCY)
       m_CondExec 00,EQ,IVM73YCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV136)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDA PGM=IEBGENER   ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDA
       ;;
(IVM73YDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV136BY
       m_OutputAssign -c 9 -w IIV136 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDB
       ;;
(IVM73YDB)
       m_CondExec 00,EQ,IVM73YDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDD PGM=IEBGENER   ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDD
       ;;
(IVM73YDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV137CY
       m_OutputAssign -c 9 -w IIV137 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDE
       ;;
(IVM73YDE)
       m_CondExec 00,EQ,IVM73YDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV137S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDG PGM=IEBGENER   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDG
       ;;
(IVM73YDG)
       m_CondExec ${EXAFA},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV137SY
       m_OutputAssign -c 9 -w IIV137S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDH
       ;;
(IVM73YDH)
       m_CondExec 00,EQ,IVM73YDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV139)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDJ PGM=IEBGENER   ** ID=AFF                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDJ
       ;;
(IVM73YDJ)
       m_CondExec ${EXAFF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV139BY
       m_OutputAssign -c 9 -w IIV139 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDK
       ;;
(IVM73YDK)
       m_CondExec 00,EQ,IVM73YDJ ${EXAFF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV140)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDM PGM=IEBGENER   ** ID=AFK                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDM
       ;;
(IVM73YDM)
       m_CondExec ${EXAFK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV140VY
       m_OutputAssign -c 9 -w IIV140 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDN
       ;;
(IVM73YDN)
       m_CondExec 00,EQ,IVM73YDM ${EXAFK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV141)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDQ PGM=IEBGENER   ** ID=AFP                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDQ
       ;;
(IVM73YDQ)
       m_CondExec ${EXAFP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV141VY
       m_OutputAssign -c 9 -w IIV141 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDR
       ;;
(IVM73YDR)
       m_CondExec 00,EQ,IVM73YDQ ${EXAFP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDT PGM=IEBGENER   ** ID=AFU                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDT
       ;;
(IVM73YDT)
       m_CondExec ${EXAFU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV142BY
       m_OutputAssign -c 9 -w IIV142 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDU
       ;;
(IVM73YDU)
       m_CondExec 00,EQ,IVM73YDT ${EXAFU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142A)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YDX PGM=IEBGENER   ** ID=AFZ                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YDX
       ;;
(IVM73YDX)
       m_CondExec ${EXAFZ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV142AY
       m_OutputAssign -c 9 -w IIV142A SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YDY
       ;;
(IVM73YDY)
       m_CondExec 00,EQ,IVM73YDX ${EXAFZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142S)                                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YEA PGM=IEBGENER   ** ID=AGE                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YEA
       ;;
(IVM73YEA)
       m_CondExec ${EXAGE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV142SY
       m_OutputAssign -c 9 -w IIV142S SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YEB
       ;;
(IVM73YEB)
       m_CondExec 00,EQ,IVM73YEA ${EXAGE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV142SA)                                            
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YED PGM=IEBGENER   ** ID=AGJ                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YED
       ;;
(IVM73YED)
       m_CondExec ${EXAGJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV142RY
       m_OutputAssign -c 9 -w IIV142SA SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YEE
       ;;
(IVM73YEE)
       m_CondExec 00,EQ,IVM73YED ${EXAGJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV143)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YEG PGM=IEBGENER   ** ID=AGO                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YEG
       ;;
(IVM73YEG)
       m_CondExec ${EXAGO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV143BY
       m_OutputAssign -c 9 -w IIV143 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YEH
       ;;
(IVM73YEH)
       m_CondExec 00,EQ,IVM73YEG ${EXAGO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REPRISE :OUI     (ETAT IIV144)                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP IVM73YEJ PGM=IEBGENER   ** ID=AGT                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YEJ
       ;;
(IVM73YEJ)
       m_CondExec ${EXAGT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR SYSUT1 ${DATA}/PXX0.FICHEML.IVM73Y.TIV144BY
       m_OutputAssign -c 9 -w IIV144 SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=IVM73YEK
       ;;
(IVM73YEK)
       m_CondExec 00,EQ,IVM73YEJ ${EXAGT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=IVM73YZA
       ;;
(IVM73YZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/IVM73YZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
