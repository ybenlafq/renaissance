#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  STAT0D.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDSTAT0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 03/07/05 AT 10.47.22 BY BURTEC6                      
#    STANDARDS: P  JOBSET: STAT0D                                              
# --------------------------------------------------------------------         
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
# ********************************************************************         
#  QUIESCE DES TABLES RTHV06-RTVA05-RTGG50                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=STAT0DA
       ;;
(STAT0DA)
#
#STAT0DAA
#STAT0DAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#STAT0DAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXAEQ=${EXAEQ:-0}
       EXAEV=${EXAEV:-0}
       EXAFA=${EXAFA:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A14=${G_A14:-'+1'}
       G_A15=${G_A15:-'+1'}
       G_A16=${G_A16:-'+1'}
       G_A17=${G_A17:-'+1'}
       G_A18=${G_A18:-'+1'}
       G_A19=${G_A19:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A20=${G_A20:-'+1'}
       G_A21=${G_A21:-'+1'}
       G_A22=${G_A22:-'+1'}
       G_A23=${G_A23:-'+1'}
       G_A24=${G_A24:-'+1'}
       G_A25=${G_A25:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=STAT0DAD
       ;;
(STAT0DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BHV030AD
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0DAD.BVA500AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_70_8 70 CH 8
 /FIELDS FLD_CH_19_7 19 CH 7
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_70_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DAE
       ;;
(STAT0DAE)
       m_CondExec 00,EQ,STAT0DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA500 NEW                                                            
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DAG
       ;;
(STAT0DAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA00   : NAME=RSVA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTGG70   : NAME=RSGG70D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A1} FGS40 ${DATA}/PTEM/STAT0DAD.BVA500AD
# *****   FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 FVA00 ${DATA}/PTEM/STAT0DAG.FVA000AD
# *****   FICHIER DES ANOMALIES ENTRANT DANS LE GENERATEUR D'ETAT              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FVA500 ${DATA}/PXX0/F91.FVA500AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA500 
       JUMP_LABEL=STAT0DAH
       ;;
(STAT0DAH)
       m_CondExec 04,GE,STAT0DAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA000A POUR CREATION FICHIER ENTRANT DANS BGG500  *         
#  REPRISE : OUI              NEW                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DAJ
       ;;
(STAT0DAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/STAT0DAG.FVA000AD
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/STAT0DAJ.FVA000BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_13 "P"
 /DERIVEDFIELD CST_1_9 "991"
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /CONDITION CND_2 FLD_CH_8_3 EQ CST_1_9 AND FLD_CH_14_1 EQ CST_3_13 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_48_8,FLD_CH_36_6,FLD_CH_42_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0DAK
       ;;
(STAT0DAK)
       m_CondExec 00,EQ,STAT0DAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG500                                                                
# ********************************************************************         
#  RECALCUL DES PRMPS DU MOIS                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DAM
       ;;
(STAT0DAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A3} FVA00 ${DATA}/PTEM/STAT0DAJ.FVA000BD
# ******  TABLES EN ENTREE                                                     
#    RTVA05   : NAME=RSVA05D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FGG500 ${DATA}/PTEM/STAT0DAM.FGG500AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG500 
       JUMP_LABEL=STAT0DAN
       ;;
(STAT0DAN)
       m_CondExec 04,GE,STAT0DAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FGG500AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DAQ
       ;;
(STAT0DAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/STAT0DAM.FGG500AD
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PTEM/STAT0DAQ.FGG500BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "991"
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_19_12 19 CH 12
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_5 
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_4_7,FLD_CH_11_8,FLD_CH_19_12
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0DAR
       ;;
(STAT0DAR)
       m_CondExec 00,EQ,STAT0DAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FVA000AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DAT PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DAT
       ;;
(STAT0DAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/STAT0DAG.FVA000AD
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 SORTOUT ${DATA}/PTEM/STAT0DAT.FVA000GD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_3 "991"
 /FIELDS FLD_CH_104_3 104 CH 3
 /CONDITION CND_1 FLD_CH_104_3 EQ CST_1_3 
 /COPY
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DAU
       ;;
(STAT0DAU)
       m_CondExec 00,EQ,STAT0DAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG505                                                                
# ********************************************************************         
#  MAJ DES PRMPS APRES RECALCUL                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DAX PGM=IKJEFT01   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DAX
       ;;
(STAT0DAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG70   : NAME=RSGG70D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGA10   : NAME=RSGA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER EN ENTREE ISSU DU TRI PRECEDENT                              
       m_FileAssign -d SHR -g ${G_A6} RTVA00I ${DATA}/PTEM/STAT0DAT.FVA000GD
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER DES PRA ENTRANT DANS LE GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGG505 ${DATA}/PXX0/F91.FGG505AD
# ******  FICHIER D'EXTRACTION ISSU DU BGG500                                  
       m_FileAssign -d SHR -g ${G_A7} FGG500 ${DATA}/PTEM/STAT0DAQ.FGG500BD
# ******  FICHIER POUR MAJ DE RTGA67 (GG506P)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGA67 ${DATA}/PNCGD/F91.FGA67AD
# ******  FICHIER ENTRANT DANS ,VA505 ET SERVANT A LOADER RTVA00               
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 RTVA00S ${DATA}/PXX0/F91.FVA00AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG505 
       JUMP_LABEL=STAT0DAY
       ;;
(STAT0DAY)
       m_CondExec 04,GE,STAT0DAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FVA000AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBA PGM=SORT       ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBA
       ;;
(STAT0DBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/STAT0DAG.FVA000AD
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PTEM/STAT0DBA.FVA000HD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "991"
 /DERIVEDFIELD CST_3_13 "P")
 /FIELDS FLD_CH_8_6 8 CH 6
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_91_3 91 CH 3
 /FIELDS FLD_CH_84_13 84 CH 13
 /FIELDS FLD_CH_35_1 35 CH 1
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_94_3 94 CH 3
 /FIELDS FLD_CH_84_7 84 CH 7
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_14_1 14 CH 1
 /CONDITION CND_1 FLD_CH_104_3 NE CST_1_9 AND FLD_CH_14_1 EQ CST_3_13 AND 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING,
   FLD_CH_84_13 ASCENDING,
   FLD_CH_35_1 ASCENDING,
   FLD_CH_8_6 ASCENDING
 /INCLUDE CND_1
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_48_8,FLD_CH_84_7,FLD_CH_91_3,FLD_CH_94_3,FLD_CH_36_6,FLD_CH_35_1,FLD_CH_8_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0DBB
       ;;
(STAT0DBB)
       m_CondExec 00,EQ,STAT0DBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG570    NOUVEAU                                                     
# ********************************************************************         
#  CONSTITUTION DE LA TABLE RTVA70     NOUVEAU                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBD PGM=IKJEFT01   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBD
       ;;
(STAT0DBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
# ******  FICHIER EN ENTREE ISSU DU TRI PRECEDENT                              
       m_FileAssign -d SHR -g ${G_A9} FGG570 ${DATA}/PTEM/STAT0DBA.FVA000HD
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -g +1 RTVA70 ${DATA}/PTEM/STAT0DBD.BGG570AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG570 
       JUMP_LABEL=STAT0DBE
       ;;
(STAT0DBE)
       m_CondExec 04,GE,STAT0DBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTVA70     NOUVEAU                                        
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBG PGM=DSNUTILB   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBG
       ;;
(STAT0DBG)
       m_CondExec ${EXABY},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTVA70                                                
       m_FileAssign -d SHR -g ${G_A10} SYSREC ${DATA}/PTEM/STAT0DBD.BGG570AD
#    RSVA70   : NAME=RSVA70D,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA70D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSVA70 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0DBG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/STAT0D_STAT0DBG_RTVA70.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=STAT0DBH
       ;;
(STAT0DBH)
       m_CondExec 04,GE,STAT0DBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FVA000AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBJ PGM=SORT       ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBJ
       ;;
(STAT0DBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A11} SORTIN ${DATA}/PTEM/STAT0DAG.FVA000AD
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 SORTOUT ${DATA}/PTEM/STAT0DBJ.FVA000ID
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "991"
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_48_8 48 CH 8
 /CONDITION CND_1 FLD_CH_104_3 EQ CST_1_6 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DBK
       ;;
(STAT0DBK)
       m_CondExec 00,EQ,STAT0DBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FGG500AD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBM PGM=SORT       ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBM
       ;;
(STAT0DBM)
       m_CondExec ${EXACI},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A12} SORTIN ${DATA}/PTEM/STAT0DAM.FGG500AD
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/STAT0DBM.FGG500CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_6 "991"
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_1_3 1 CH 3
 /CONDITION CND_1 FLD_CH_1_3 EQ CST_1_6 
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /OMIT CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DBN
       ;;
(STAT0DBN)
       m_CondExec 00,EQ,STAT0DBM ${EXACI},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG550    NOUVEAU                                                     
# ********************************************************************         
#  CALCUL DES PRMP DE DPMO                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBQ PGM=IKJEFT01   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBQ
       ;;
(STAT0DBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RSVA70   : NAME=RSVA70D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSVA70 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A13} RTVA00I ${DATA}/PTEM/STAT0DBJ.FVA000ID
# ******  FICHIER ENTRE                                                        
       m_FileAssign -d SHR -g ${G_A14} FGG500 ${DATA}/PTEM/STAT0DBM.FGG500CD
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -g +1 RTVA50 ${DATA}/PTEM/STAT0DBQ.BGG550AD
# ******  FICHIER SORTIE                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 RTVA00S ${DATA}/PTEM/STAT0DBQ.BGG550BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG550 
       JUMP_LABEL=STAT0DBR
       ;;
(STAT0DBR)
       m_CondExec 04,GE,STAT0DBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTVA50     NOUVEAU                                        
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBT PGM=DSNUTILB   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBT
       ;;
(STAT0DBT)
       m_CondExec ${EXACS},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTVA50                                                
       m_FileAssign -d SHR -g ${G_A15} SYSREC ${DATA}/PTEM/STAT0DBQ.BGG550AD
#    RSVA50   : NAME=RSVA50D,MODE=(U,N) - DYNAM=YES                            
# -X-RSVA50D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSVA50 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0DBT.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/STAT0D_STAT0DBT_RTVA50.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=STAT0DBU
       ;;
(STAT0DBU)
       m_CondExec 04,GE,STAT0DBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER BGG550BD                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DBX
       ;;
(STAT0DBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A16} SORTIN ${DATA}/PXX0/F91.FVA00AD
       m_FileAssign -d SHR -g ${G_A17} -C ${DATA}/PTEM/STAT0DBQ.BGG550BD
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 SORTOUT ${DATA}/PXX0/F91.BGG550DD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_35_1 35 CH 1
 /FIELDS FLD_CH_11_3 11 CH 3
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_35_1 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DBY
       ;;
(STAT0DBY)
       m_CondExec 00,EQ,STAT0DBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT NEW REMPLACE FVA00AD PAR BGG550DD                                
# ********************************************************************         
#  TRI DU FICHIER FVA00A POUR CREATION FICHIER BVA505A ENTRANT                 
#  DANS LE PROG BVA505                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCA PGM=SORT       ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCA
       ;;
(STAT0DCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A18} SORTIN ${DATA}/PXX0/F91.BGG550DD
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PTEM/STAT0DCA.BVA505AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "P"
 /FIELDS FLD_PD_76_8 76 PD 8
 /FIELDS FLD_CH_76_8 76 CH 8
 /FIELDS FLD_CH_68_8 68 CH 8
 /FIELDS FLD_PD_68_8 68 PD 8
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_CH_14_1 14 CH 1
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_11 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6,
    TOTAL FLD_PD_68_8,
    TOTAL FLD_PD_76_8
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_11_3,FLD_CH_8_3,FLD_CH_36_6,FLD_CH_68_8,FLD_CH_42_6,FLD_CH_76_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0DCB
       ;;
(STAT0DCB)
       m_CondExec 00,EQ,STAT0DCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA505                                                                
# ********************************************************************         
#  MAJ DE LA RTVA05 POUR METTRE A JOUR EN QUANTITE ET EN VALEUR LES            
#  STOCKS,ET CREATION D'UN FICHIER BVA506A POUR MAJ DE LA TABLE                
#  RTGG50 DANS LA PGM BVA506                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCD PGM=IKJEFT01   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCD
       ;;
(STAT0DCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG55   : NAME=RSGG55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTVA10   : NAME=RSVA10D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA10 /dev/null
#    RTVA15   : NAME=RSVA15D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA15 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTVA05   : NAME=RSVA05D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A19} FVA505 ${DATA}/PTEM/STAT0DCA.BVA505AD
# ******  FICHIER SERVANT A LOADER LE TABLE RTVA15                             
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -g +1 FVA515 ${DATA}/PXX0/F91.FVA15AD
# ******  FICHIER ENTRANT DANS VA001R ET SERVANT A LOADER RTVA30               
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 RTVA30 ${DATA}/PXX0/F91.FVA30AD
# ******  FICHIER SERVANT A LA MAJ DE LA RTGG50 DANS LE BVA506                 
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -g +1 FGG500 ${DATA}/PTEM/STAT0DCD.BVA506AD
# ******  FICHIER A HISTORISER QUI PERMET DE LOADER LA TABLE RTVA10            
# *****   FICHIER PARAMETRE 'M' POUR TRAIT. MENSUEL                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0DCD
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA505 
       JUMP_LABEL=STAT0DCE
       ;;
(STAT0DCE)
       m_CondExec 04,GE,STAT0DCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA506                                                                
# ********************************************************************         
#  MAJ DE LA RTGG50 POUR RECALCUL DES PRMS A PARTIR DU FICHIER                 
#  BVA506AR ISSU DU BVA505                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCG
       ;;
(STAT0DCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU BVA505                                               
       m_FileAssign -d SHR -g ${G_A20} FGG500 ${DATA}/PTEM/STAT0DCD.BVA506AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA506 
       JUMP_LABEL=STAT0DCH
       ;;
(STAT0DCH)
       m_CondExec 04,GE,STAT0DCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LGT 15.01.2001 : EVITE RELABEL TT FILIALES                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCJ PGM=IEFBR14    ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCJ
       ;;
(STAT0DCJ)
       m_CondExec ${EXADR},NE,YES 
       m_ProgramExec IEFBR14 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030A                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCM PGM=SORT       ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCM
       ;;
(STAT0DCM)
       m_CondExec ${EXADW},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BHV030AD
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0DCM.HV0000BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_5 "VEN"
 /DERIVEDFIELD CST_3_9 "VEN"
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_13_3 13 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_5 OR FLD_CH_13_3 EQ CST_3_9 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_30_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DCN
       ;;
(STAT0DCN)
       m_CondExec 00,EQ,STAT0DCM ${EXADW},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV150                                                                
# ********************************************************************         
#  CREATION DES FICHIERS DE LOAD DE LA RTHV09 ET RTHV10 ET DES FICHIER         
#  CUMULS FHV01                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCQ PGM=IKJEFT01   ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCQ
       ;;
(STAT0DCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c 9 -w BGV150 SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN ENTREE                                                      
#    RTGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG50   : NAME=RSGG50D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGG55   : NAME=RSGG55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
# ******* FICHIER HISTO FHV01 (ARTICLE DARTY)                                  
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PXX0/F91.HV01AD
# ******* FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A21} SV0404 ${DATA}/PTEM/STAT0DCM.HV0000BD
# ******* FIC DES VTES DU MOIS PAR ARTICLES AYANT PU ETRE RECYCLE              
# *******               (LOAD DE LA RTHV10)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGV150 ${DATA}/PGV0/F91.GV0150AD
# ******* FIC DES VTES DU MOIS PAR FAMILLE AYANT PU ETRE RECYCLE               
# *******               (LOAD DE LA RTHV09)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FGV158 ${DATA}/PGV0/F91.GV0158AD
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV152 ${DATA}/PGV0/F91.GV0152AD
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV153 ${DATA}/PGV0/F91.GV0153AD
# ******* CARTE PARAMETRE MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******   PARAMETRE SOCIETE : 991                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV150 
       JUMP_LABEL=STAT0DCR
       ;;
(STAT0DCR)
       m_CondExec 04,GE,STAT0DCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER DES VENTES ET REPRISES NON TRAITEES PAR BHV030               
#  NSOCORIG,NLIEU,NORIGINE,CODIC-GROUP,CMODDEL,NCODIC                          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCT PGM=SORT       ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCT
       ;;
(STAT0DCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F91.BHV100AD
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -g +1 SORTOUT ${DATA}/PTEM/STAT0DCT.BHV100BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_7_7 7 CH 7
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_21_3 ASCENDING,
   FLD_CH_24_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DCU
       ;;
(STAT0DCU)
       m_CondExec 00,EQ,STAT0DCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHV100                                                                
# ********************************************************************         
#  MAJ DE LA TABLE HISTO VENTES ET REPRISE DU MOIS                             
#  POUR LES GROUPES DE PRODUITS                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DCX PGM=IKJEFT01   ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DCX
       ;;
(STAT0DCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE SOUS TABLES GENERALISEES                                       
#    RSGA01   : NAME=RSGA01D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  DETAILS DE VENTES                                                    
#    RSGV11   : NAME=RSGV11D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58   : NAME=RSGA58D,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  VENTES ET REPRISES DU MOIS/GROUP-PRODUITS                            
       m_FileAssign -d SHR -g ${G_A22} FHV100 ${DATA}/PTEM/STAT0DCT.BHV100BD
#                                                                              
# ******  HISTO VENTES ET REPRISES DE VENTES DU MOIS PAR GROUP-ARTICLE         
#    RSHV06   : NAME=RSHV06D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV06 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00D,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV100 
       JUMP_LABEL=STAT0DCY
       ;;
(STAT0DCY)
       m_CondExec 04,GE,STAT0DCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA DATE POUR L'HITO DES PSE (GCX55D)                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DDA PGM=SORT       ** ID=AEQ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DDA
       ;;
(STAT0DDA)
       m_CondExec ${EXAEQ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU MOIS A TRAITER                                               
       m_FileAssign -i SORTIN
$FMOIS
_end
# ******  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F91.DATPSE0D
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DDB
       ;;
(STAT0DDB)
       m_CondExec 00,EQ,STAT0DDA ${EXAEQ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DES FICHIERS GV0152AD ET GV0153AD SUR L'INDEX CLUSTER DE LA RTH         
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DDD PGM=SORT       ** ID=AEV                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DDD
       ;;
(STAT0DDD)
       m_CondExec ${EXAEV},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A23} SORTIN ${DATA}/PGV0/F91.GV0152AD
       m_FileAssign -d SHR -g ${G_A24} -C ${DATA}/PGV0/F91.GV0153AD
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 SORTOUT ${DATA}/PGV0/F91.HV01RD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_19 1 CH 19
 /KEYS
   FLD_CH_1_19 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0DDE
       ;;
(STAT0DDE)
       m_CondExec 00,EQ,STAT0DDD ${EXAEV},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   LOAD DE LA TABLE RTHV01 POUR DPMO                                          
# ********************************************************************         
#   REPRISE OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0DDG PGM=DSNUTILB   ** ID=AFA                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DDG
       ;;
(STAT0DDG)
       m_CondExec ${EXAFA},NE,YES 
       m_FileAssign -d NEW SORTOUT ${MT_TMP}/SORTOUT_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSMAP ${MT_TMP}/SYSMAP_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_FileAssign -d NEW SYSUT1 ${MT_TMP}/SYSUT1_${MT_JOB_NAME}_${MT_JOB_PID} # DD avec UNIT, UNTRANSLATED
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" UTPRINT
       m_OutputAssign -c "*" DSNTRACE
#                                                                              
# ******* FIC DE LOAD DE RTVA70                                                
       m_FileAssign -d SHR -g ${G_A25} SYSREC ${DATA}/PGV0/F91.HV01RD
#    RSHV01   : NAME=RSHV01D,MODE=(U,N) - DYNAM=YES                            
# -X-RSHV01D  - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSHV01 /dev/null
#                                                                              
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0DDG.sysin
       m_OutputAssign -c "*" SYSERR01
       m_FileAssign -d SHR MT_CTL ${MT_CTL_FILES}/STAT0D_STAT0DDG_RTHV01.sysload
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=STAT0DDH
       ;;
(STAT0DDH)
       m_CondExec 04,GE,STAT0DDG ${EXAFA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=STAT0DZA
       ;;
(STAT0DZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0DZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
