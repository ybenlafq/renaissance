#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  DWA00P.ksh                       --- VERSION DU 19/10/2016 16:04
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPDWA00 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 16/02/24 AT 09.49.59 BY BURTEC6                      
#    STANDARDS: P  JOBSET: DWA00P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGA01 S/TABLE EGV18                         
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=DWA00PA
       ;;
(DWA00PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXADM=${EXADM:-0}
       EXADR=${EXADR:-0}
       EXADW=${EXADW:-0}
       EXAEB=${EXAEB:-0}
       EXAEG=${EXAEG:-0}
       EXAEL=${EXAEL:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=DWA00PAA
       ;;
(DWA00PAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  PALNIFIEE A 19 H 30                                                         
# **************************************                                       
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 86 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA01AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGA01 S/TABLE VTREM                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PAD PGM=PTLDRIVM   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PAD
       ;;
(DWA00PAD)
       m_CondExec ${EXAAF},NE,YES 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA01BP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGA01 S/TABLE GCPLT                         
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PAG PGM=PTLDRIVM   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PAG
       ;;
(DWA00PAG)
       m_CondExec ${EXAAK},NE,YES 
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA01CP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTPM06                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PAJ PGM=PTLDRIVM   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PAJ
       ;;
(DWA00PAJ)
       m_CondExec ${EXAAP},NE,YES 
#    RSPM06   : NAME=RSPM06,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNPM06AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGA25                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PAM PGM=PTLDRIVM   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PAM
       ;;
(DWA00PAM)
       m_CondExec ${EXAAU},NE,YES 
#    RSGA25   : NAME=RSGA25,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 20 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA25AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGA23                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PAQ PGM=PTLDRIVM   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PAQ
       ;;
(DWA00PAQ)
       m_CondExec ${EXAAZ},NE,YES 
#    RSGA23   : NAME=RSGA23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA23AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGA26                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PAT PGM=PTLDRIVM   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PAT
       ;;
(DWA00PAT)
       m_CondExec ${EXABE},NE,YES 
#    RSGA26   : NAME=RSGA26,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA26AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGF01                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PAX PGM=PTLDRIVM   ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PAX
       ;;
(DWA00PAX)
       m_CondExec ${EXABJ},NE,YES 
#    RSGF01   : NAME=RSGF01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 42 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/DWA00PAX.UNGF01AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PAX.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  EASYTRIEVE                                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBA PGM=EZTPA00    ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBA
       ;;
(DWA00PBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} FILEA ${DATA}/PTEM/DWA00PAX.UNGF01AP
       m_FileAssign -d NEW,CATLG,DELETE -r 46 -t LSEQ -g +1 FILEB ${DATA}/PXX0/F07.UNGF01BP
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/DWA00PBA
       m_ProgramExec DWA00PBA
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGV31                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBD PGM=PTLDRIVM   ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBD
       ;;
(DWA00PBD)
       m_CondExec ${EXABT},NE,YES 
#    RSGV31   : NAME=RSGV31,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/DWA00PBD.UNGV31AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PBD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGV31 LYON                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBG PGM=PTLDRIVM   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBG
       ;;
(DWA00PBG)
       m_CondExec ${EXABY},NE,YES 
#    RSGV31   : NAME=RSGV31Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/DWA00PBG.UNGV31AY
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PBG.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGV31 METZ                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBJ PGM=PTLDRIVM   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBJ
       ;;
(DWA00PBJ)
       m_CondExec ${EXACD},NE,YES 
#    RSGV31   : NAME=RSGV31M,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/DWA00PBJ.UNGV31AM
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PBJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGV31 MARSEILLE                             
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBM PGM=PTLDRIVM   ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBM
       ;;
(DWA00PBM)
       m_CondExec ${EXACI},NE,YES 
#    RSGV31   : NAME=RSGV31D,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/DWA00PBM.UNGV31AD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PBM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGV31 LILLE                                 
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBQ PGM=PTLDRIVM   ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBQ
       ;;
(DWA00PBQ)
       m_CondExec ${EXACN},NE,YES 
#    RSGV31   : NAME=RSGV31L,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/DWA00PBQ.UNGV31AL
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PBQ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGV31 MGIO                                  
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBT PGM=PTLDRIVM   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBT
       ;;
(DWA00PBT)
       m_CondExec ${EXACS},NE,YES 
#    RSGV31   : NAME=RSGV31O,MODE=I - DYNAM=YES                                
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SYSREC01 ${DATA}/PTEM/DWA00PBT.UNGV31AO
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PBT.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#   MERGE DES FICHIERS                                                         
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PBX
       ;;
(DWA00PBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/DWA00PBD.UNGV31AP
       m_FileAssign -d SHR -g ${G_A3} -C ${DATA}/PTEM/DWA00PBT.UNGV31AO
       m_FileAssign -d SHR -g ${G_A4} -C ${DATA}/PTEM/DWA00PBG.UNGV31AY
       m_FileAssign -d SHR -g ${G_A5} -C ${DATA}/PTEM/DWA00PBQ.UNGV31AL
       m_FileAssign -d SHR -g ${G_A6} -C ${DATA}/PTEM/DWA00PBJ.UNGV31AM
       m_FileAssign -d SHR -g ${G_A7} -C ${DATA}/PTEM/DWA00PBM.UNGV31AD
# ****                                                                         
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F99.UNGV31AF
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
 /* Record Type = F  Record Length = 27 */
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DWA00PBY
       ;;
(DWA00PBY)
       m_CondExec 00,EQ,DWA00PBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES DE LA TABLE RTGA24                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCA PGM=PTLDRIVM   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCA
       ;;
(DWA00PCA)
       m_CondExec ${EXADC},NE,YES 
#    RSGA24   : NAME=RSGA24,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 25 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA24AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PCA.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGA27                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCD PGM=PTLDRIVM   ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCD
       ;;
(DWA00PCD)
       m_CondExec ${EXADH},NE,YES 
#    RSGA27   : NAME=RSGA27,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA27AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PCD.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  BEX001 : EXTRACT VTE                                                        
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCG PGM=IKJEFT01   ** ID=ADM                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCG
       ;;
(DWA00PCG)
       m_CondExec ${EXADM},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN LECTURE                                                    
#    RSGA00   : NAME=RSGA00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA00 /dev/null
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
#    RSGA12   : NAME=RSGA12,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA12 /dev/null
#    RSGA14   : NAME=RSGA14,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA14 /dev/null
#    RSGA20   : NAME=RSGA20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA20 /dev/null
#    RSGA22   : NAME=RSGA22,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA22 /dev/null
#    RSGA23   : NAME=RSGA23,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA23 /dev/null
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA53 /dev/null
#    RSAN00   : NAME=RSAN00,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSAN00 /dev/null
# ****** FICHIERS PARAMETRES                                                   
       m_FileAssign -d SHR FPARM ${DATA}/CORTEX4.P.MTXTFIX1/DWA00PCG
       m_FileAssign -i FDATE
$FDATE
_end
# ******                                                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 FEX001 ${DATA}/PTEM/DWA00PCG.BDWV20AP
# ******                                                                       
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BEX001 
       JUMP_LABEL=DWA00PCH
       ;;
(DWA00PCH)
       m_CondExec 04,GE,DWA00PCG ${EXADM},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  UNLOAD DES DONNEES DE LA TABLE RTGA53                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCJ PGM=PTLDRIVM   ** ID=ADR                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCJ
       ;;
(DWA00PCJ)
       m_CondExec ${EXADR},NE,YES 
#    RSGA53   : NAME=RSGA53,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 17 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGA53AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PCJ.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  UNLOAD DES DONNEES DE LA TABLE RTGN75                                       
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCM PGM=PTLDRIVM   ** ID=ADW                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCM
       ;;
(DWA00PCM)
       m_CondExec ${EXADW},NE,YES 
#    RSGN75   : NAME=RSGN75,MODE=I - DYNAM=YES                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -t LSEQ -g +1 SYSREC01 ${DATA}/PXX0/F07.UNGN75AP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PCM.sysin
       
       m_DBHpuUnload -f SYSIN -o SYSREC01
# ********************************************************************         
#  TRI DU FICHER POUR LE PGM BDWV20                                            
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCQ PGM=SORT       ** ID=AEB                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCQ
       ;;
(DWA00PCQ)
       m_CondExec ${EXAEB},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A8} SORTIN ${DATA}/PTEM/DWA00PCG.BDWV20AP
       m_FileAssign -d NEW,CATLG,DELETE -r 600 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/DWA00PCQ.BDWV20BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_559_7 559 CH 7
 /KEYS
   FLD_CH_559_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DWA00PCR
       ;;
(DWA00PCR)
       m_CondExec 00,EQ,DWA00PCQ ${EXAEB},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM BDWV20                                                                  
#  SI PLANTAGE _A CE STEP ET QUE VOUS NE SAVEZ PAS REPRENDRE VOUS POUVE         
#  METTRE LA CHAINE TERMINER SOUS PLAN AVEC ENVOI DE MAIL _A LA PROD PO         
#  PREVENIR                                                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCT PGM=IKJEFT01   ** ID=AEG                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCT
       ;;
(DWA00PCT)
       m_CondExec ${EXAEG},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A9} BDWV20I ${DATA}/PTEM/DWA00PCQ.BDWV20BP
# ******  FICHIER DSYST DE LA VEILLE                                           
       m_FileAssign -d SHR -g +0 BDWV20C ${DATA}/PXX0/F07.BDWV20CP
# ******  FICHIERS PARAMETRE                                                   
       m_FileAssign -d SHR FDATE ${DATA}/CORTEX4.P.PARAM.DWA00P.R$[RUN]/FDATE
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/DWA00PCT
# ******  NOUVEAU FICHIER DSYST                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 BDWV20K ${DATA}/PTEM/DWA00PCT.BDWV20DP
# ******  FICHIER POUR AXE BIRD                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 100 -t LSEQ -g +1 BDWV20O ${DATA}/PXX0/F07.BDWV20EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BDWV20 
       JUMP_LABEL=DWA00PCU
       ;;
(DWA00PCU)
       m_CondExec 04,GE,DWA00PCT ${EXAEG},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  RECOPIE DU FICHIER BDWV20CP POUR LE LENDEMAIN                               
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP DWA00PCX PGM=SORT       ** ID=AEL                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PCX
       ;;
(DWA00PCX)
       m_CondExec ${EXAEL},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A10} SORTIN ${DATA}/PTEM/DWA00PCT.BDWV20DP
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.BDWV20CP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=DWA00PCY
       ;;
(DWA00PCY)
       m_CondExec 00,EQ,DWA00PCX ${EXAEL},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=DWA00PZA
       ;;
(DWA00PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/DWA00PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
