#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTIA1D.ksh                       --- VERSION DU 09/10/2016 05:23
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDFTIA1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/11/06 AT 10.27.50 BY BURTEC2                      
#    STANDARDS: P  JOBSET: FTIA1D                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#              CUMUL EN CAS DE PLANTAGE DE FTI01D                              
#   SORT DES FICHIERS ISSU DES DIFFERENTS TRAITEMENTS DE GESTION               
#   PLUS LE FICHIER RECYCLAGE DE LA VEILLE.                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FTIA1DA
       ;;
(FTIA1DA)
#
#FTIA1DAJ
#FTIA1DAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#FTIA1DAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON WEDNSDAY  2013/11/06 AT 10.27.50 BY BURTEC2                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FTIA1D                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-FTI01D'                            
# *                           APPL...: IMPMARSE                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FTIA1DAA
       ;;
(FTIA1DAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER CAISSES NEM DACEM ISSU DE NM002D                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGD/F91.BNM001CD
# *** FICHIER CAISSES NEM DACEM ISSU DE NM030D                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BNM003AD
# *** FICHIER COMPTA NASL ISSU DE NASFTF   (SITE CENTRALIS�)                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.NASGCTD
# *** FICHIER CONTENTIEUX CREDOR ISSU DE FTCREP                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.CREDORBD
# *** FICHIER MVTS SAFIG ISSU DE LA FTCREG                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.SAFIDPMG
# *** FICHIER COMPTABILISATION DES MOUVEMENTS DE STOCK INTRA-SOCI�T�           
# *** FICHIER NMD (CHAINE MD0020)                                              
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BMD002CD
# **** FICHIER VENANT DE BS001D                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.BBS002AD
# **** FICHIER VENANT DE FS052D                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGD/F91.BBS052AD
# *** FICHIER DE REPRISE EN CAS DE PLANTAGE LA VEILLE                          
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.FTI01RED
# **** FICHIER VENANT DE HD001P (ASSURANCE PAR ABONNEMENT)                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F91.HD001PD
# *** FICHIER DE REPRISE POUR (J+1)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F91.FTI01RED
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTIA1DAB
       ;;
(FTIA1DAB)
       m_CondExec 00,EQ,FTIA1DAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UNE GENERATION A VIDE POUR DEBLOCAGE CGICSD                      
#  EN CAS DE PLANTAGE DE FTI01D                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTIA1DAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTIA1DAD
       ;;
(FTIA1DAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC A DESTINATION DU CGICSD                                          
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER REMIS A ZERO A DESTINATION DE CGICSD                         
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F91.FTI01D
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTIA1DAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTIA1DAE
       ;;
(FTIA1DAE)
       m_CondExec 16,NE,FTIA1DAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMINE DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTIA1DAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
