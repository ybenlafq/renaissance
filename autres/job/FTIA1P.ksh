#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  FTIA1P.ksh                       --- VERSION DU 08/10/2016 13:14
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPFTIA1 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 13/12/09 AT 16.29.22 BY BURTECA                      
#    STANDARDS: P  JOBSET: FTIA1P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#              CUMUL EN CAS DE PLANTAGE DE FTI01P                              
#   SORT DES FICHIERS ISSU DES DIFFERENTS TRAITEMENTS DE GESTION               
#   PLUS LE FICHIER RECYCLAGE DE LA VEILLE.                                    
#   REPRISE: OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
       JUMP_LABEL=FTIA1PA
       ;;
(FTIA1PA)
#
#FTIA1PAJ
#FTIA1PAJ Delete a faire manuellement, PGMSVC34 avec instructions non port�e dans PROC
#FTIA1PAJ
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       LOT=${CZXSRUNB}
# ********************************************************************         
# *    GENERATED ON MONDAY    2013/12/09 AT 16.29.21 BY BURTECA                
# *    ENVIRONMENT: IPO1      STD....: P                                       
# *    JOBSET INFORMATION:    NAME...: FTIA1P                                  
# *                           FREQ...: D                                       
# *                           TITLE..: 'ALT-FTI01P'                            
# *                           APPL...: IMPPARIS                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP YES      PGM=CZX3PSRC   **                                          
# ***********************************                                          
       JUMP_LABEL=FTIA1PAA
       ;;
(FTIA1PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# *** FICHIER CAISSES NEM DACEM ISSU DE NM002P                                 
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PNCGP/F07.BNM001CP
# *** FICHIER CAISSES NEM DACEM ISSU DE NM003P                                 
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BNM003AP
# *** FICHIER COMPTA NASL 907(M5I351)      (SITE CENTRALIS�)                   
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.NASGCTP
# *** FICHIER CONTENTIEUX CREDOR ISSU DE FTCREP                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.CREDORBP
# *** FICHIER MVTS SAFIG ISSU DE LA FTCREG                                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.SAFIDIFG
# *** FICHIER DE REPRISE EN CAS DE PLANTAGE LA VEILLE                          
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FTI01REP
# *** FICHIER COMPTABILISATION DES MOUVEMENTS DE STOCK INTRA-SOCI�T�           
# *** FICHIER NMD (CHAINE MD002)                                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BMD002CP
# **** FICHIER VENANT DE FS052P                                                
       m_FileAssign -d SHR -g +0 -C ${DATA}/PNCGP/F07.BBS052AP
# **** FICHIER VENANT DE GCC00P (CARTES CADEAUX)                               
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.BPC002AP
# **** FICHIER VENANT DE HD001P (ASSURANCE PAR ABONNEMENT)                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.HD001PP
# **** AJOUT LE 09122013 DEMANDE DE MANUEL POUR CORRECTION                     
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FFT100AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FPC049AP
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.FPC050AP
# *** FICHIER DE REPRISE POUR (J+1)                                            
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.FTI01REP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_18 01 CH 18
 /KEYS
   FLD_BI_1_18 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=FTIA1PAB
       ;;
(FTIA1PAB)
       m_CondExec 00,EQ,FTIA1PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CREATION D'UNE GENERATION A VIDE POUR DEBLOCAGE CGICSP                      
#  EN CAS DE PLANTAGE DE FTI01P                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTIA1PAD PGM=IDCAMS     ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=FTIA1PAD
       ;;
(FTIA1PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
# *****   FIC A DESTINATION DU CGICSP                                          
       m_FileAssign -d SHR IN1 /dev/null
# *****   FICHIER REMIS A ZERO A DESTINATION DE CGICSP                         
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.FTI01P
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/FTIA1PAD.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=FTIA1PAE
       ;;
(FTIA1PAE)
       m_CondExec 16,NE,FTIA1PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   MISE TERMINE DES JOBS SOUS PLAN PAR JOB UTILITAIRE DE PLAN                 
#   REPRISE: OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP FTIA1PAG PGM=CZX2PTRT   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
