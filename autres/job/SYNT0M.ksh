#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SYNT0M.ksh                       --- VERSION DU 08/10/2016 18:08
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PMSYNT0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/02/05 AT 14.58.20 BY BURTECC                      
#    STANDARDS: P  JOBSET: SYNT0M                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  BSYNT01 : TRAITEMENT BSYNT01                                                
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SYNT0MA
       ;;
(SYNT0MA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SYNT0MAA
       ;;
(SYNT0MAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSPR00   : NAME=RSPR00M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSPR00 /dev/null
#                                                                              
# ****** CARTE DATE                                                            
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
# ****** FICHIER SORTIE                                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -g +1 FSYNT1 ${DATA}/PTEM/SYNT0MAA.BSYNT1AM
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSYNT01 
       JUMP_LABEL=SYNT0MAB
       ;;
(SYNT0MAB)
       m_CondExec 04,GE,SYNT0MAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  SORT                                                                        
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0MAD PGM=SORT       ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0MAD
       ;;
(SYNT0MAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SYNT0MAA.BSYNT1AM
       m_FileAssign -d NEW,CATLG,DELETE -r 39 -g +1 SORTOUT ${DATA}/PTEM/SYNT0MAD.BSYNT1BM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_1_13 1 CH 13
 /KEYS
   FLD_CH_1_13 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SYNT0MAE
       ;;
(SYNT0MAE)
       m_CondExec 00,EQ,SYNT0MAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BSYNT05 : TRAITEMENT BSYNT05                                                
#                                                                              
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SYNT0MAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=SYNT0MAG
       ;;
(SYNT0MAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLES EN LECTURE                                                    
#    RSGV10   : NAME=RSGV10M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV10 /dev/null
#    RSGV11   : NAME=RSGV11M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#    RSGV14   : NAME=RSGV14M,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGV14 /dev/null
#                                                                              
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# ****** CARTE PARAMETRE.                                                      
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/SOCDAL
#                                                                              
#                                                                              
       m_FileAssign -d SHR -g ${G_A2} FSYNT1 ${DATA}/PTEM/SYNT0MAD.BSYNT1BM
#                                                                              
# ****** FICHIERS SYNTHESES VENTES TYPE CUISINE                                
       m_FileAssign -d NEW,CATLG,DELETE -r 84 -g +1 FSYNT2 ${DATA}/PXX0/F89.BSYNT2AM
# ****** FICHIERS SYNTHESES VENTES TYPE DETAIL                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 84 -g +1 FSYNT3 ${DATA}/PXX0/F89.BSYNT3AM
#                                                                              
# ******************************************************                       
# ******************************************************                       
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          

       m_ProgramExec -b BSYNT05 
       JUMP_LABEL=SYNT0MZA
       ;;
(SYNT0MZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SYNT0MZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=SYNT0MAH
       ;;
(SYNT0MAH)
       m_CondExec 04,GE,SYNT0MAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
