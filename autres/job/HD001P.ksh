#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  HD001P.ksh                       --- VERSION DU 09/10/2016 05:31
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PPHD001 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 15/12/26 AT 11.54.09 BY OPERAT1                      
#    STANDARDS: P  JOBSET: HD001P                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#   TRI DES MVTS VENANT DU FICHIER VENANT DE LA GATEWAY HD0001AP               
#    REPRISE: OUI                                                              
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=HD001PA
       ;;
(HD001PA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=HD001PAA
       ;;
(HD001PAA)
       m_CondExec ${EXAAA},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES SAUVEGARDES CICS            *                                       
#  DéMARRAGE _A 23H PAR PLAN                                                    
# **************************************                                       
# *****   FICHIER STANDARD VENANT DE LA GATEWAY                                
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/FTP.F07.HD0001AP
# *****   FICHIER DE RECYCLAGE ANOMALIES                                       
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.HD0001CP
# *****   FICHIER REPRISE                                                      
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F07.HDREPRIP
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 SORTOUT ${DATA}/PTEM/HD001PAA.HD0001BP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_250_25 250 CH 25
 /KEYS
   FLD_CH_250_25 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HD001PAB
       ;;
(HD001PAB)
       m_CondExec 00,EQ,HD001PAA ${EXAAA},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  BHD001 : ECLATEMENT DU FICHIER HD0001BP POUR TOUTES LES FILIALES            
#                                                                              
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD001PAD PGM=IKJEFT01   ** ID=AAF                                   
# ***********************************                                          
       JUMP_LABEL=HD001PAD
       ;;
(HD001PAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#                                                                              
# ******* TABLE GENERALISEE RMAC(COPIE DE LA NCP TS LES SOIR)                  
#    RSGA01   : NAME=RSGA01,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSGA01 /dev/null
# ******* TABLE                                                                
#    RSRB20   : NAME=RSRB20,MODE=I - DYNAM=YES                                 
       m_FileAssign -d SHR RSRB20 /dev/null
#                                                                              
# ******* DATE JOUR JJMMSSAA                                                   
       m_FileAssign -i FDATE
$FDATE
_end
#                                                                              
# *****   FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A1} FTICSE ${DATA}/PTEM/HD001PAA.HD0001BP
# *****   FICHIER RECYCLAGE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 FRECYCL ${DATA}/PXX0/F07.HD0001CP
# ******* UN FICHIER POUR CHAQUE SOCIETE A TRAITER                             
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS1 ${DATA}/PXX0/F07.HD001PP
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS2 ${DATA}/PXX0/F16.HD001PO
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS3 ${DATA}/PXX0/F45.HD001PY
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS4 ${DATA}/PXX0/F61.HD001PL
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS5 ${DATA}/PXX0/F89.HD001PM
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS6 ${DATA}/PXX0/F91.HD001PD
       m_FileAssign -d NEW,CATLG,DELETE -r 400 -t LSEQ -g +1 FTICS7 ${DATA}/PXX0/F20.HD001PK
# ******* UN FICHIER JUSTIF POUR CHAQUE SOCIETE A TRAITER                      
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -t LSEQ -g +1 FJUSTIF1 ${DATA}/PXX0/F07.HDJUSTP
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -t LSEQ -g +1 FJUSTIF2 ${DATA}/PXX0/F16.HDJUSTO
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -t LSEQ -g +1 FJUSTIF3 ${DATA}/PXX0/F45.HDJUSTY
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -t LSEQ -g +1 FJUSTIF4 ${DATA}/PXX0/F61.HDJUSTL
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -t LSEQ -g +1 FJUSTIF5 ${DATA}/PXX0/F89.HDJUSTM
       m_FileAssign -d NEW,CATLG,DELETE -r 87 -t LSEQ -g +1 FJUSTIF6 ${DATA}/PXX0/F91.HDJUSTD
       m_FileAssign -d NEW,CATLG,DELETE -r 105 -t LSEQ -g +1 FJUSTIF7 ${DATA}/PXX0/F20.HDJUSTK
# ******* FICHIER                                                              
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 FFMICRO ${DATA}/PTEM/HD001PAD.HD0001DP
# ******  ETAT D'ANOMALIE                                                      
       m_OutputAssign -c 9 -w IHD001 IHD001
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHD001 
       JUMP_LABEL=HD001PAE
       ;;
(HD001PAE)
       m_CondExec 04,GE,HD001PAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   TRI DU FICHIER HD0001DP                                                    
#    REPRISE: OUI                                                              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD001PAG PGM=SORT       ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=HD001PAG
       ;;
(HD001PAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******* FICHIER                                                              
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/HD001PAD.HD0001DP
# *****   FICHIER DE SORTIE                                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 116 -t LSEQ -g +1 SORTOUT ${DATA}/PXX0/F07.HD0001EP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_ZD_28_13 28 ZD 13
 /FIELDS FLD_CH_1_27 1 CH 27
 /KEYS
   FLD_CH_1_27 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_ZD_28_13
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=HD001PAH
       ;;
(HD001PAH)
       m_CondExec 00,EQ,HD001PAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER HDREPRIP                                           
#  CREE UNE GENERATION  A VIDE                                                 
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP HD001PAJ PGM=IDCAMS     ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=HD001PAJ
       ;;
(HD001PAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 410 -t LSEQ -g +1 OUT1 ${DATA}/PXX0/F07.HDREPRIP
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HD001PAJ.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=HD001PAK
       ;;
(HD001PAK)
       m_CondExec 16,NE,HD001PAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=HD001PZA
       ;;
(HD001PZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/HD001PZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
