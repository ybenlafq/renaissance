#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  STAT0L.ksh                       --- VERSION DU 17/10/2016 18:30
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PLSTAT0 -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 04/10/21 AT 14.30.45 BY BURTECA                      
#    STANDARDS: P  JOBSET: STAT0L                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
#  QUIESCE DES TABLES RTHV06-RTVA05-RTGG50                                     
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=STAT0LA
       ;;
(STAT0LA)
#
#STAT0LAA
#STAT0LAA Delete a faire manuellement, DSNUTILB avec QUIESCE ou REPAIR dans PROC
#STAT0LAA
#
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXABY=${EXABY:-0}
       EXACD=${EXACD:-0}
       EXACI=${EXACI:-0}
       EXACN=${EXACN:-0}
       EXACS=${EXACS:-0}
       EXACX=${EXACX:-0}
       EXADC=${EXADC:-0}
       EXADH=${EXADH:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A10=${G_A10:-'+1'}
       G_A11=${G_A11:-'+1'}
       G_A12=${G_A12:-'+1'}
       G_A13=${G_A13:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=STAT0LAD
       ;;
(STAT0LAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BHV030AL
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 SORTOUT ${DATA}/PTEM/STAT0LAD.BYHV36AP
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_BI_1_80 1 CH 80
 /FIELDS FLD_BI_88_59 88 CH 59
 /KEYS
   FLD_BI_1_80 ASCENDING,
   FLD_BI_88_59 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0LAE
       ;;
(STAT0LAE)
       m_CondExec 00,EQ,STAT0LAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BYHV36                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LAG PGM=BYHV36     ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LAG
       ;;
(STAT0LAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_OutputAssign -c "*" SYSOUA
#                                                                              
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0LAG
#                                                                              
#  FIC FNSOC                                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
#                                                                              
       m_FileAssign -d SHR -g ${G_A1} FHV030 ${DATA}/PTEM/STAT0LAD.BYHV36AP
#                                                                              
       m_FileAssign -d SHR -g +0 FHV031 ${DATA}/PXX0/F07.BYHV39HP
#  FICHIER DE SORTIE                                                           
       m_FileAssign -d NEW,CATLG,DELETE -r 146 -g +1 FGS40 ${DATA}/PTEM/STAT0LAG.BYHV36BP
       m_ProgramExec BYHV36 
#                                                                              
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030A ISSU DU PGM BHV030                                   
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LAJ PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LAJ
       ;;
(STAT0LAJ)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A2} SORTIN ${DATA}/PTEM/STAT0LAG.BYHV36BP
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0LAJ.BVA500AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_70_8 70 CH 8
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_70_8 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0LAK
       ;;
(STAT0LAK)
       m_CondExec 00,EQ,STAT0LAJ ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA500                                                                
# ********************************************************************         
#  GENERATION DES MOUVEMENTS DE STOCK ENTRANT DANS LA VALO                     
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LAM PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LAM
       ;;
(STAT0LAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# *****   TABLES EN ENTREE                                                     
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTVA00   : NAME=RSVA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA00 /dev/null
#    RTGG70   : NAME=RSGG70L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGS40   : NAME=RSGS40,MODE=(I,U) - DYNAM=YES                             
       m_FileAssign -d SHR RTGS40 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *****   FICHIER ISSU DU TRI                                                  
       m_FileAssign -d SHR -g ${G_A3} FGS40 ${DATA}/PTEM/STAT0LAJ.BVA500AL
# *****   FICHIER DES MVTS                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 FVA00 ${DATA}/PTEM/STAT0LAM.FVA000AL
# *****   FICHIER DES ANOMALIES ENTRANT DANS LE GENERATEUR D'ETAT              
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 FVA500 ${DATA}/PXX0/F61.FVA500AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA500 
       JUMP_LABEL=STAT0LAN
       ;;
(STAT0LAN)
       m_CondExec 04,GE,STAT0LAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA000A POUR CREATION FICHIER ENTRANT DANS BGG500  *         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LAQ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LAQ
       ;;
(STAT0LAQ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/STAT0LAM.FVA000AL
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 SORTOUT ${DATA}/PTEM/STAT0LAQ.FVA000BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_9 "961"
 /DERIVEDFIELD CST_3_13 "P"
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_CH_104_3 104 CH 3
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_48_8 48 CH 8
 /FIELDS FLD_CH_42_6 42 CH 6
 /CONDITION CND_2 FLD_CH_8_3 EQ CST_1_9 AND FLD_CH_14_1 EQ CST_3_13 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_8_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_48_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_48_8,FLD_CH_36_6,FLD_CH_42_6
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0LAR
       ;;
(STAT0LAR)
       m_CondExec 00,EQ,STAT0LAQ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG500                                                                
# ********************************************************************         
#  RECALCUL DES PRMPS DU MOIS                                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LAT PGM=IKJEFT01   ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LAT
       ;;
(STAT0LAT)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  FICHIER EN ENTREE                                                    
       m_FileAssign -d SHR -g ${G_A5} FVA00 ${DATA}/PTEM/STAT0LAQ.FVA000BL
# ******  TABLES EN ENTREE                                                     
#    RTVA05   : NAME=RSVA05L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
# *****   FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER D'EXTRACTION                                                 
       m_FileAssign -d NEW,CATLG,DELETE -r 30 -g +1 FGG500 ${DATA}/PTEM/STAT0LAT.FGG500AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG500 
       JUMP_LABEL=STAT0LAU
       ;;
(STAT0LAU)
       m_CondExec 04,GE,STAT0LAT ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT    NOUVEAU                                                       
# ********************************************************************         
#  TRI DU FICHIER FGG500AL                                                     
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LAX PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LAX
       ;;
(STAT0LAX)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A6} SORTIN ${DATA}/PTEM/STAT0LAT.FGG500AL
       m_FileAssign -d NEW,CATLG,DELETE -r 27 -g +1 SORTOUT ${DATA}/PTEM/STAT0LAX.FGG500BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_11_8 11 CH 8
 /FIELDS FLD_CH_4_7 4 CH 7
 /FIELDS FLD_CH_19_12 19 CH 12
 /KEYS
   FLD_CH_4_7 ASCENDING,
   FLD_CH_11_8 ASCENDING
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_4_7,FLD_CH_11_8,FLD_CH_19_12
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0LAY
       ;;
(STAT0LAY)
       m_CondExec 00,EQ,STAT0LAX ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGG505                                                                
# ********************************************************************         
#  MAJ DES PRMPS APRES RECALCUL                                                
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBA PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBA
       ;;
(STAT0LBA)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG70   : NAME=RSGG70L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG70 /dev/null
#    RTGA10   : NAME=RSGA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA10 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER EN ENTREE ISSU DU TRI PRECEDENT                              
       m_FileAssign -d SHR -g ${G_A7} RTVA00I ${DATA}/PTEM/STAT0LAM.FVA000AL
# ******  FICHIER FNSOC                                                        
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# ******  FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******  FICHIER DES PRA ENTRANT DANS LE GENERATEUR D'ETAT                    
       m_FileAssign -d NEW,CATLG,DELETE -r 512 -g +1 IGG505 ${DATA}/PXX0/F61.FGG505AL
# ******  FICHIER D'EXTRACTION ISSU DU BGG500                                  
       m_FileAssign -d SHR -g ${G_A8} FGG500 ${DATA}/PTEM/STAT0LAX.FGG500BL
# ******  FICHIER POUR MAJ DE RTGA67 (GG506P)                                  
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGA67 ${DATA}/PNCGL/F61.FGA67AL
# ******  FICHIER ENTRANT DANS ,VA505 ET SERVANT A LOADER RTVA00               
       m_FileAssign -d NEW,CATLG,DELETE -r 106 -g +1 RTVA00S ${DATA}/PXX0/F61.FVA00AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGG505 
       JUMP_LABEL=STAT0LBB
       ;;
(STAT0LBB)
       m_CondExec 04,GE,STAT0LBA ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER FVA00A POUR CREATION FICHIER BVA505A ENTRANT                 
#  DANS LE PROG BVA505                                                         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBD PGM=SORT       ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBD
       ;;
(STAT0LBD)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A9} SORTIN ${DATA}/PXX0/F61.FVA00AL
       m_FileAssign -d NEW,CATLG,DELETE -r 44 -g +1 SORTOUT ${DATA}/PTEM/STAT0LBD.BVA505AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_1_11 "P"
 /FIELDS FLD_CH_1_7 1 CH 7
 /FIELDS FLD_CH_8_3 8 CH 3
 /FIELDS FLD_PD_36_6 36 PD 6
 /FIELDS FLD_CH_68_8 68 CH 8
 /FIELDS FLD_CH_76_8 76 CH 8
 /FIELDS FLD_PD_76_8 76 PD 8
 /FIELDS FLD_CH_11_3 11 CH 3
 /FIELDS FLD_CH_14_1 14 CH 1
 /FIELDS FLD_CH_42_6 42 CH 6
 /FIELDS FLD_PD_42_6 42 PD 6
 /FIELDS FLD_PD_68_8 68 PD 8
 /FIELDS FLD_CH_36_6 36 CH 6
 /FIELDS FLD_CH_104_3 104 CH 3
 /CONDITION CND_2 FLD_CH_14_1 EQ CST_1_11 
 /KEYS
   FLD_CH_104_3 ASCENDING,
   FLD_CH_1_7 ASCENDING,
   FLD_CH_11_3 ASCENDING,
   FLD_CH_8_3 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_36_6,
    TOTAL FLD_PD_42_6,
    TOTAL FLD_PD_68_8,
    TOTAL FLD_PD_76_8
 /INCLUDE CND_2
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_104_3,FLD_CH_1_7,FLD_CH_11_3,FLD_CH_8_3,FLD_CH_36_6,FLD_CH_68_8,FLD_CH_42_6,FLD_CH_76_8
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=STAT0LBE
       ;;
(STAT0LBE)
       m_CondExec 00,EQ,STAT0LBD ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA505                                                                
# ********************************************************************         
#  MAJ DE LA RTVA05 POUR METTRE A JOUR EN QUANTITE ET EN VALEUR LES            
#  STOCKS,ET CREATION D'UN FICHIER BVA506A POUR MAJ DE LA TABLE                
#  RTGG50 DANS LA PGM BVA506                                                   
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBG PGM=IKJEFT01   ** ID=ABY                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBG
       ;;
(STAT0LBG)
       m_CondExec ${EXABY},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN ENTREE                                                     
#    RTGG55   : NAME=RSGG55L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
#    RTVA10   : NAME=RSVA10L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA10 /dev/null
#    RTVA15   : NAME=RSVA15L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA15 /dev/null
# ******  TABLES EN MAJ                                                        
#    RTVA05   : NAME=RSVA05L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTVA05 /dev/null
#    RTGG50   : NAME=RSGG50L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A10} FVA505 ${DATA}/PTEM/STAT0LBD.BVA505AL
# ******  FICHIER SERVANT A LOADER LE TABLE RTVA15                             
       m_FileAssign -d NEW,CATLG,DELETE -r 28 -g +1 FVA515 ${DATA}/PXX0/F61.FVA15AL
# ******  FICHIER ENTRANT DANS VA001R ET SERVANT A LOADER RTVA30               
       m_FileAssign -d NEW,CATLG,DELETE -r 19 -g +1 RTVA30 ${DATA}/PXX0/F61.FVA30AL
# ******  FICHIER SERVANT A LA MAJ DE LA RTGG50 DANS LE BVA506                 
       m_FileAssign -d NEW,CATLG,DELETE -r 15 -g +1 FGG500 ${DATA}/PTEM/STAT0LBG.BVA506AL
# ******  FICHIER A HISTORISER QUI PERMET DE LOADER LA TABLE RTVA10            
# *****   FICHIER PARAMETRE 'M' POUR TRAIT. MENSUEL                            
       m_FileAssign -d SHR FPARAM ${DATA}/CORTEX4.P.MTXTFIX1/STAT0LBG
# *****   FICHIER FMOIS                                                        
       m_FileAssign -i FMOIS
$FMOIS
_end
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA505 
       JUMP_LABEL=STAT0LBH
       ;;
(STAT0LBH)
       m_CondExec 04,GE,STAT0LBG ${EXABY},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BVA506                                                                
# ********************************************************************         
#  MAJ DE LA RTGG50 POUR RECALCUL DES PRMS A PARTIR DU FICHIER                 
#  BVA506AR ISSU DU BVA505                                                     
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBJ PGM=IKJEFT01   ** ID=ACD                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBJ
       ;;
(STAT0LBJ)
       m_CondExec ${EXACD},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  TABLES EN MAJ                                                        
#    RTGG50   : NAME=RSGG50L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
# ******  FICHIER ISSU DU BVA505                                               
       m_FileAssign -d SHR -g ${G_A11} FGG500 ${DATA}/PTEM/STAT0LBG.BVA506AL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BVA506 
       JUMP_LABEL=STAT0LBK
       ;;
(STAT0LBK)
       m_CondExec 04,GE,STAT0LBJ ${EXACD},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  LGT 15.01.2001 : EVITE RELABEL TT FILIALES                                  
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBM PGM=IEFBR14    ** ID=ACI                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBM
       ;;
(STAT0LBM)
       m_CondExec ${EXACI},NE,YES 
       m_ProgramExec IEFBR14 
# ********************************************************************         
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER BHV030A                                                      
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBQ PGM=SORT       ** ID=ACN                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBQ
       ;;
(STAT0LBQ)
       m_CondExec ${EXACN},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BHV030AL
       m_FileAssign -d NEW,CATLG,DELETE -r 141 -g +1 SORTOUT ${DATA}/PTEM/STAT0LBQ.HV0000BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /DERIVEDFIELD CST_3_9 "VEN"
 /DERIVEDFIELD CST_1_5 "VEN"
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_30_3 30 CH 3
 /FIELDS FLD_CH_19_7 19 CH 7
 /FIELDS FLD_CH_13_3 13 CH 3
 /CONDITION CND_1 FLD_CH_4_3 EQ CST_1_5 OR FLD_CH_13_3 EQ CST_3_9 
 /KEYS
   FLD_CH_19_7 ASCENDING,
   FLD_CH_30_3 ASCENDING
 /INCLUDE CND_1
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0LBR
       ;;
(STAT0LBR)
       m_CondExec 00,EQ,STAT0LBQ ${EXACN},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BGV150                                                                
# ********************************************************************         
#  CREATION DES FICHIERS DE LOAD DE LA RTHV09 ET RTHV10 ET DES FICHIER         
#  CUMULS FHV01                                                                
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBT PGM=IKJEFT01   ** ID=ACS                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBT
       ;;
(STAT0LBT)
       m_CondExec ${EXACS},NE,YES 
       m_OutputAssign -c 9 -w BGV150 SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******* TABLE EN ENTREE                                                      
#    RTGA00   : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA00 /dev/null
#    RTGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGA01 /dev/null
#    RTGG50   : NAME=RSGG50L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG50 /dev/null
#    RTGG55   : NAME=RSGG55L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RTGG55 /dev/null
# ******* FICHIER HISTO FHV01 (ARTICLE DARTY)                                  
       m_FileAssign -d SHR -g +0 FHV01 ${DATA}/PXX0/F61.HV01AL
# ******* FICHIER ISSU DU TRI PRECEDENT                                        
       m_FileAssign -d SHR -g ${G_A12} SV0404 ${DATA}/PTEM/STAT0LBQ.HV0000BL
# ******* FIC DES VTES DU MOIS PAR ARTICLES AYANT PU ETRE RECYCLE              
# *******               (LOAD DE LA RTHV10)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 36 -g +1 FGV150 ${DATA}/PGV0/F61.GV0150AL
# ******* FIC DES VTES DU MOIS PAR FAMILLE AYANT PU ETRE RECYCLE               
# *******               (LOAD DE LA RTHV09)                                    
       m_FileAssign -d NEW,CATLG,DELETE -r 37 -g +1 FGV158 ${DATA}/PGV0/F61.GV0158AL
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV152 ${DATA}/PGV0/F61.GV0152AL
# ******* FIC A CUMULER AVEC LE FHV01                                          
       m_FileAssign -d NEW,CATLG,DELETE -r 52 -g +1 FGV153 ${DATA}/PGV0/F61.GV0153AL
# ******* CARTE PARAMETRE MMSSAA                                               
       m_FileAssign -i FMOIS
$FMOIS
_end
# ******   PARAMETRE SOCIETE : 961                                             
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDNPC
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BGV150 
       JUMP_LABEL=STAT0LBU
       ;;
(STAT0LBU)
       m_CondExec 04,GE,STAT0LBT ${EXACS},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : SORT                                                                  
# ********************************************************************         
#  TRI DU FICHIER DES VENTES ET REPRISES NON TRAITEES PAR BHV030               
#  NSOCORIG,NLIEU,NORIGINE,CODIC-GROUP,CMODDEL,NCODIC                          
# ********************************************************************         
#  REPRISE : OUI                                                               
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LBX PGM=SORT       ** ID=ACX                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LBX
       ;;
(STAT0LBX)
       m_CondExec ${EXACX},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 SORTIN ${DATA}/PXX0/F61.BHV100AL
       m_FileAssign -d NEW,CATLG,DELETE -r 70 -g +1 SORTOUT ${DATA}/PTEM/STAT0LBX.BHV100BL
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_14_7 14 CH 7
 /FIELDS FLD_CH_21_3 21 CH 3
 /FIELDS FLD_CH_1_3 1 CH 3
 /FIELDS FLD_CH_4_3 4 CH 3
 /FIELDS FLD_CH_24_7 24 CH 7
 /FIELDS FLD_CH_7_7 7 CH 7
 /KEYS
   FLD_CH_1_3 ASCENDING,
   FLD_CH_4_3 ASCENDING,
   FLD_CH_7_7 ASCENDING,
   FLD_CH_14_7 ASCENDING,
   FLD_CH_21_3 ASCENDING,
   FLD_CH_24_7 ASCENDING
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0LBY
       ;;
(STAT0LBY)
       m_CondExec 00,EQ,STAT0LBX ${EXACX},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BHV100                                                                
# ********************************************************************         
#  MAJ DE LA TABLE HISTO VENTES ET REPRISE DU MOIS                             
#  POUR LES GROUPES DE PRODUITS                                                
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LCA PGM=IKJEFT01   ** ID=ADC                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LCA
       ;;
(STAT0LCA)
       m_CondExec ${EXADC},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ******  ARTICLES                                                             
#    RSGA00   : NAME=RSGA00L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA00 /dev/null
# ******  TABLE SOUS TABLES GENERALISEES                                       
#    RSGA01   : NAME=RSGA01L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA01 /dev/null
# ******  DETAILS DE VENTES                                                    
#    RSGV11   : NAME=RSGV11L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGV11 /dev/null
# ******  PRMP DU JOUR                                                         
#    RSGG50   : NAME=RSGG50L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG50 /dev/null
# ******  PRMP DACEM                                                           
#    RSGG55   : NAME=RSGG55L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGG55 /dev/null
# ******  LIENS ENTRE ARTICLES                                                 
#    RSGA58   : NAME=RSGA58L,MODE=(I,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSGA58 /dev/null
# ******  VENTES ET REPRISES DU MOIS/GROUP-PRODUITS                            
       m_FileAssign -d SHR -g ${G_A13} FHV100 ${DATA}/PTEM/STAT0LBX.BHV100BL
#                                                                              
# ******  HISTO VENTES ET REPRISES DE VENTES DU MOIS PAR GROUP-ARTICLE         
#    RSHV06   : NAME=RSHV06L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSHV06 /dev/null
# ******  ANOMALIES                                                            
#    RSAN00   : NAME=RSAN00L,MODE=(U,U) - DYNAM=YES                            
       m_FileAssign -d SHR RSAN00 /dev/null
#                                                                              
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BHV100 
       JUMP_LABEL=STAT0LCB
       ;;
(STAT0LCB)
       m_CondExec 04,GE,STAT0LCA ${EXADC},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  CHARGEMENT DE LA DATE POUR L'HITO DES PSE (GCX55L)                          
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP STAT0LCD PGM=SORT       ** ID=ADH                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LCD
       ;;
(STAT0LCD)
       m_CondExec ${EXADH},NE,YES 
       m_OutputAssign -c "*" SYSOUT
# ******  DATE DU MOIS A TRAITER                                               
       m_FileAssign -i SORTIN
$FMOIS
_end
# ******  DATE JJMMSSAA POUR LE PROCHAIN PASSAGE DE LA CHAINE                  
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PEX0/F61.DATPSE0L
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /COPY
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=STAT0LCE
       ;;
(STAT0LCE)
       m_CondExec 00,EQ,STAT0LCD ${EXADH},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=STAT0LZA
       ;;
(STAT0LZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/STAT0LZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
