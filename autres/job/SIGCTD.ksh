#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  SIGCTD.ksh                       --- VERSION DU 17/10/2016 18:33
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PDSIGCT -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 10/09/07 AT 09.54.28 BY PREPA2                       
#    STANDARDS: P  JOBSET: SIGCTD                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# **--USER='SIGA'                                                              
# ********************************************************************         
#  PGM : BSIG20                                                                
#  ------------                                                                
#  REPRISE DU FICHIER SXGCTD ISSU D'UNE EXPLO HRV3 (UNIX) AFIN DE              
#  PACKER LES ZONES DECIMALES                                                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=SIGCTDA
       ;;
(SIGCTDA)
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       EXABO=${EXABO:-0}
       EXABT=${EXABT:-0}
       EXA98=${EXA98:-0}
       G_A1=${G_A1:-'+1'}
       G_A2=${G_A2:-'+1'}
       G_A3=${G_A3:-'+1'}
       G_A4=${G_A4:-'+1'}
       G_A5=${G_A5:-'+1'}
       G_A6=${G_A6:-'+1'}
       G_A7=${G_A7:-'+1'}
       G_A8=${G_A8:-'+1'}
       G_A9=${G_A9:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=SIGCTDAA
       ;;
(SIGCTDAA)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSOUA
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#  ----------------------              *                                       
# **************************************                                       
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g +0 FEXPLO ${DATA}/PXX0/F91.SXGCTD
       m_FileAssign -d SHR -g +0 -C ${DATA}/PXX0/F828.SXGCTD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FSIG20 ${DATA}/PTEM/SIGCTDAA.BSIG20AD
       m_ProgramExec BSIG20 
# ********************************************************************         
#  TRI DU FICHIER ISSU DU PGM BSIG20                                           
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTDAD PGM=SORT       ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDAD
       ;;
(SIGCTDAD)
       m_CondExec ${EXAAP},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A1} SORTIN ${DATA}/PTEM/SIGCTDAA.BSIG20AD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 SORTOUT ${DATA}/PTEM/SIGCTDAD.BSIG20BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_16_25 16 CH 25
 /FIELDS FLD_CH_1_15 01 CH 15
 /FIELDS FLD_CH_61_7 61 CH 7
 /FIELDS FLD_PD_41_9 41 PD 9
 /FIELDS FLD_CH_59_2 59 CH 2
 /FIELDS FLD_PD_50_9 50 PD 9
 /KEYS
   FLD_CH_1_15 ASCENDING,
   FLD_CH_59_2 ASCENDING,
   FLD_CH_16_25 ASCENDING,
   FLD_CH_61_7 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_41_9,
    TOTAL FLD_PD_50_9
_end
       m_FileSort -s SYSIN -i SORTIN -o SORTOUT
       JUMP_LABEL=SIGCTDAE
       ;;
(SIGCTDAE)
       m_CondExec 00,EQ,SIGCTDAD ${EXAAP},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG25                                                                
#  ------------                                                                
#  CREATION DU FICHIER D'INTERFACE GCT PNCGM.F91.SIGAGCT REPRIS DANS L         
#  CHAINE GCA50M PGM BFTV02                                                    
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTDAG PGM=IKJEFT01   ** ID=AAU                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDAG
       ;;
(SIGCTDAG)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  TABLES EN LECTURE                                                    
#    RSGA01   : NAME=RSGA01D,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/SOCDPM
       m_FileAssign -d SHR FTYPPAIE ${DATA}/CORTEX4.P.MTXTFIX1/PAYDEFD
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DU TRI PRECEDANT                                        
       m_FileAssign -d SHR -g ${G_A2} FSIG20 ${DATA}/PTEM/SIGCTDAD.BSIG20BD
# ------  FICHIER POUR CONTROLE                                                
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 FPARAM ${DATA}/PTEM/SIGCTDAG.SIGAPRMD
# ------  FICHIER REPRIS DANS LE BSIG26                                        
       m_FileAssign -d NEW,CATLG,DELETE -r 60 -g +1 FCTL ${DATA}/PTEM/SIGCTDAG.SIGACTLD
# ------  FICHIER POUR GCT                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 250 -g +1 FGCT ${DATA}/PNCGD/F91.SIGGCTD
# ------  FICHIER POUR FDC                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 54 -g +1 FFDC ${DATA}/PXX0/F91.BSIG25ED
# ------  FICHIER POUR FDC                                                     
       m_FileAssign -d NEW,CATLG,DELETE -r 61 -g +1 FREMUN ${DATA}/PXX0/F91.BSIG25GD
# ------  ETATS DE COMPTE RENDUS                                               
       m_OutputAssign -c 9 -w ISG010 FANOSYNT
       m_OutputAssign -c 9 -w ISG020 FANODET
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG25 
       JUMP_LABEL=SIGCTDAH
       ;;
(SIGCTDAH)
       m_CondExec 04,GE,SIGCTDAG ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25  (CONTROLE ANALYTIQUE)               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTDAJ PGM=SORT       ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDAJ
       ;;
(SIGCTDAJ)
       m_CondExec ${EXAAZ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A3} SORTIN ${DATA}/PTEM/SIGCTDAG.SIGACTLD
       m_FileAssign -d NEW,CATLG,DELETE -r 51 -g +1 SORTOUT ${DATA}/PTEM/SIGCTDAJ.BSIG26AD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_1_51 1 CH 51
 /FIELDS FLD_CH_1_33 1 CH 33
 /FIELDS FLD_PD_34_9 34 PD 9
 /KEYS
   FLD_CH_1_33 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_51
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGCTDAK
       ;;
(SIGCTDAK)
       m_CondExec 00,EQ,SIGCTDAJ ${EXAAZ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE GENERAL)                   
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTDAM PGM=SORT       ** ID=ABE                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDAM
       ;;
(SIGCTDAM)
       m_CondExec ${EXABE},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A4} SORTIN ${DATA}/PTEM/SIGCTDAG.SIGACTLD
       m_FileAssign -d NEW,CATLG,DELETE -r 47 -g +1 SORTOUT ${DATA}/PTEM/SIGCTDAM.BSIG26BD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /FIELDS FLD_CH_1_21 1 CH 21
 /FIELDS FLD_CH_43_9 43 CH 9
 /FIELDS FLD_CH_52_8 52 CH 8
 /FIELDS FLD_CH_34_9 34 CH 9
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_52_8 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_52_8,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGCTDAN
       ;;
(SIGCTDAN)
       m_CondExec 00,EQ,SIGCTDAM ${EXABE},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  TRI DU FICHIER FCTL ISSU DU PGM BSIG25 (CONTROLE RUBRIQUE)                  
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTDAQ PGM=SORT       ** ID=ABJ                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDAQ
       ;;
(SIGCTDAQ)
       m_CondExec ${EXABJ},NE,YES 
       m_OutputAssign -c "*" SYSOUT
       m_FileAssign -d SHR -g ${G_A5} SORTIN ${DATA}/PTEM/SIGCTDAG.SIGACTLD
       m_FileAssign -d NEW,CATLG,DELETE -r 45 -g +1 SORTOUT ${DATA}/PTEM/SIGCTDAQ.BSIG26CD
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM SORT IS NOT ZERO        ***                             
# ************************************************                             
       m_FileAssign -i SYSIN
 /PADBYTE x"20"
 /FIELDS FLD_CH_34_9 34 CH 9
 /FIELDS FLD_CH_28_6 28 CH 6
 /FIELDS FLD_CH_1_21 1 CH 21
 /FIELDS FLD_PD_34_9 34 PD 9
 /FIELDS FLD_CH_43_9 43 CH 9
 /FIELDS FLD_PD_43_9 43 PD 9
 /KEYS
   FLD_CH_1_21 ASCENDING,
   FLD_CH_28_6 ASCENDING
 /SUMMARIZE 
    TOTAL FLD_PD_34_9,
    TOTAL FLD_PD_43_9
 /MT_OUTFILE_ASG SORTOUT
 /REFORMAT FLD_CH_1_21,FLD_CH_28_6,FLD_CH_34_9,FLD_CH_43_9
_end
       m_FileSort -s SYSIN -i SORTIN
       JUMP_LABEL=SIGCTDAR
       ;;
(SIGCTDAR)
       m_CondExec 00,EQ,SIGCTDAQ ${EXABJ},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BSIG26                                                                
#  ------------                                                                
#  EDITION DES ETATS DE CONTROLE                                               
# ********************************************************************         
#  REPRISE: OUI                                                                
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SIGCTDAT PGM=IKJEFT01   ** ID=ABO                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDAT
       ;;
(SIGCTDAT)
       m_CondExec ${EXABO},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
# ------  FICHIERS PARAMETRE                                                   
       m_FileAssign -i FDATE
$FDATE
_end
# ------  FICHIER ISSU DES TRIS PRECEDANT                                      
       m_FileAssign -d SHR -g ${G_A6} FCTLANA ${DATA}/PTEM/SIGCTDAJ.BSIG26AD
       m_FileAssign -d SHR -g ${G_A7} FCTLGEN ${DATA}/PTEM/SIGCTDAM.BSIG26BD
       m_FileAssign -d SHR -g ${G_A8} FCTLRUB ${DATA}/PTEM/SIGCTDAQ.BSIG26CD
# ------  FICHIER DE CONTROLE SI OK OU ANO                                     
       m_FileAssign -d SHR -g ${G_A9} FPARAM ${DATA}/PTEM/SIGCTDAG.SIGAPRMD
# ------  ETATS DE CONTROLE                                                    
       m_OutputAssign -c 9 -w ISG025 FGENE
       m_OutputAssign -c 9 -w ISG026 FANA
       m_OutputAssign -c 9 -w ISG027 FRUB
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BSIG26 
       JUMP_LABEL=SIGCTDAU
       ;;
(SIGCTDAU)
       m_CondExec 04,GE,SIGCTDAT ${EXABO},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  REMISE A ZERO DU FICHIER SXGCTD,CREE UNE GENERATION A VIDE                  
# *******************************************************************          
#  REPRISE : OUI                                                               
# *******************************************************************          
#                                                                              
# ***********************************                                          
# *   STEP SIGCTDAX PGM=IDCAMS     ** ID=ABT                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDAX
       ;;
(SIGCTDAX)
       m_CondExec ${EXABT},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR IN1 /dev/null
       m_FileAssign -d SHR IN2 /dev/null
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT1 ${DATA}/PXX0/F91.SXGCTD
       m_FileAssign -d NEW,CATLG,DELETE -r 80 -g +1 OUT2 ${DATA}/PXX0/F828.SXGCTD
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGCTDAX.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RETURN CODE FROM IDCAMS IS 16   ***                             
# ************************************************                             
       JUMP_LABEL=SIGCTDAY
       ;;
(SIGCTDAY)
       m_CondExec 16,NE,SIGCTDAX ${EXABT},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP SCRATCH  PGM=IDCAMS     ** ID=A98                                   
# ***********************************                                          
       JUMP_LABEL=SIGCTDZA
       ;;
(SIGCTDZA)
       m_CondExec ${EXA98},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_FileAssign -d SHR SYSIN ${SYSIN}/CORTEX4.P.MTXTFIX1/SIGCTDZA.sysin
       m_UtilityExec
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************                                         
# *      STEP ZABCOND/ZABEND      ****                                         
# ************************************                                         
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
