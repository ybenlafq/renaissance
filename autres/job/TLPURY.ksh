#!/usr/bin/ksh
#@(#)---------------------------------------------------------------------------------------------
#@(#)-                                                                                           -
#@(#)- SCRIPT NAME    ==  TLPURY.ksh                       --- VERSION DU 08/10/2016 15:19
#@(#)-                                                                                           -
#@(#)- AUTHOR         ==                                                                         -
#@(#)-                                                                                           -
#@(#)- TREATMENT      ==                                                                         -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)- OBSERVATIONS   ==  MAINFRAME MIGRATION                                                    -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------
#@(#)- NDLR         ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)-              ==                                                                           -
#@(#)- ..                                                                                        -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)-                                                                                           -
#@(#)---------------------------------------------------------------------------------------------

m_JobBegin -j PYTLPUR -s START -v 2.0 -c A -t HOLD
while true ;
do
       m_PhaseBegin
       case ${CURRENT_LABEL} in
(START)
#    RUN JCL GENERATED ON 08/07/21 AT 10.41.27 BY BURTEC6                      
#    STANDARDS: P  JOBSET: TLPURY                                              
#  MODIF JOJO 02/03/98:DUPLIQUE 1E JOBLIB POUR GENERER LA SPECIFIQUE           
#  FRED 10.03.2004 ://*IMSDB OVERRIDEE DS CZXG0JCP SI APLLI WMS (P+Y)          
# --------------------------------------------------------------------         
# IMSDB   SERT A GENERER CARTE ALIAS IMS/DBCTL                                 
# ********************************************************************         
# ********************************************************************         
#  QUIESCE DES  TABLES LOGISTIQUES : RSTL01-05                                 
#  QUIESCE DES  TABLES LYON       : RSTL02Y-04Y-06Y-09Y           *            
#  REPRISE: OUI                                                                
# ********************************************************************         
# --------------------------------------------------------------------         
#                                                                              
#                                                                              
       JUMP_LABEL=TLPURYA
       ;;
(TLPURYA)
       EXAAA=${EXAAA:-0}
       EXAAF=${EXAAF:-0}
       EXAAK=${EXAAK:-0}
       EXAAP=${EXAAP:-0}
       EXAAU=${EXAAU:-0}
       EXAAZ=${EXAAZ:-0}
       EXABE=${EXABE:-0}
       EXABJ=${EXABJ:-0}
       G_A1=${G_A1:-'+1'}
       RUN=${RUN}
       JUMP_LABEL=TLPURYAA
       ;;
(TLPURYAA)
       m_CondExec ${EXAAA},NE,YES 
# **************************************                                       
#  DEPENDANCES POUR PLAN :             *                                       
#    OBLIGATOIRE POUR LOGIQUE APPL     *                                       
#    APRES JOB TETE DE NUIT            *                                       
# **************************************                                       
       m_FileAssign -d NEW,CATLG,DELETE -r 1-128 -g +1 SYSPRINT ${DATA}/PTL0/F45.QUIESCE.QTLPURAY
# *****   TABLES LOGISTIQUES                                                   
#    RSTL01   : NAME=RSTL01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL01 /dev/null
#    RSTL05   : NAME=RSTL05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05 /dev/null
       m_FileAssign -d SHR SYSIN ${DATA}/CORTEX4.P.MTXTFIX1/TLPURYAA
       m_ProgramExec IEFBR14 "RDAR,TLPURY.U0"
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DSN IS GT 4        ***                             
# ************************************************                             
       JUMP_LABEL=TLPURYAD
       ;;
(TLPURYAD)
       m_CondExec ${EXAAF},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c H SYSUDUMP
       m_FileAssign -d SHR -g ${G_A1} SYSUT1 ${DATA}/PTL0/F45.QUIESCE.QTLPURAY
       m_OutputAssign -c X SYSUT2
       m_FileRepro -i SYSUT1 -o SYSUT2
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM IEBGENER IS NOT ZERO    ***                             
# ************************************************                             
       JUMP_LABEL=TLPURYAE
       ;;
(TLPURYAE)
       m_CondExec 00,EQ,TLPURYAD ${EXAAF},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#  PGM : BTL909                                                                
#  EPURATION DE LA TABLE RTTL09R                                               
#  REPRISE: NON BACKOUT = RECOVER                                              
#     POUR LES  TABLES LOGISTIQUES : RSTL01-05  ===> DB2RBP                    
#     POUR LES  TABLES LYON       : RSTL02Y-04Y-06Y-09Y ==>DB2RBY              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TLPURYAG PGM=IKJEFT01   ** ID=AAK                                   
# ***********************************                                          
       JUMP_LABEL=TLPURYAG
       ;;
(TLPURYAG)
       m_CondExec ${EXAAK},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
#  TABLE                                                                       
#    RSTL09   : NAME=RSTL09Y,MODE=(U,N) - DYNAM=YES                            
# -X-TLPURYR1 - IS UPDATED IN PLACE WITH MODE=(U,N)                            
       m_FileAssign -d SHR RSTL09 /dev/null
#  TABLE GENERALISEE:SS-TABLE(DELAI,BTL901,BTL904,BTL905) EN NBR DE MO         
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  NUMERO DE SOCIETE                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/TLPURYAG
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL909 
       JUMP_LABEL=TLPURYAH
       ;;
(TLPURYAH)
       m_CondExec 04,GE,TLPURYAG ${EXAAK},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSTL09R                                       
#  REPRISE: NON BACKOUT = RECOVER                                              
#     POUR LES  TABLES LOGISTIQUES : RSTL01-05  ===> DB2RBP                    
#     POUR LES  TABLES LYON       : RSTL02Y-04Y-06Y-09Y ==>DB2RBY              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TLPURYAJ PGM=DSNUTILB   ** ID=AAP                                   
# ***********************************                                          
       JUMP_LABEL=TLPURYAM
       ;;
(TLPURYAM)
       m_CondExec ${EXAAU},NE,YES 
       m_OutputAssign -c "*" SYSPRINT
       m_OutputAssign -c "*" SYSOUT
#  FICHIER DATE                                                                
       m_FileAssign -i FDATE
$FDATE
_end
# ******  TABLES LOGISTIQUES                                                   
#    RSTL01   : NAME=RSTL01,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL01 /dev/null
#    RSTL05   : NAME=RSTL05,MODE=(U,N) - DYNAM=YES                             
       m_FileAssign -d SHR RSTL05 /dev/null
#                                                                              
#    RSTL02   : NAME=RSTL02Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL02 /dev/null
#    RSTL04   : NAME=RSTL04Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL04 /dev/null
#    RSTL06   : NAME=RSTL06Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL06 /dev/null
#    RSTL09   : NAME=RSTL09Y,MODE=(U,N) - DYNAM=YES                            
       m_FileAssign -d SHR RSTL09 /dev/null
#  TABLE GENERALISEE:SS-TABLE(DELAI,BTL901,BTL904,BTL905) EN NBR DE MO         
#    RSGA01   : NAME=RSGA01Y,MODE=I - DYNAM=YES                                
       m_FileAssign -d SHR RSGA01 /dev/null
#  TABLE DES VENTES                                                            
#    RSGV11   : NAME=RSGV11Y,MODE=U - DYNAM=YES                                
       m_FileAssign -d SHR RSGV11 /dev/null
#  NUMERO DE SOCIETE                                                           
       m_FileAssign -d SHR FNSOC ${DATA}/CORTEX4.P.MTXTFIX1/TLPURYAM
# *********************************************************************        
# -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*                           
# ************************************************                             
# **  ABEND IF RC FROM STEP DB2 IS GT 4        ***                             
# ************************************************                             

       m_ProgramExec -b BTL900 
       JUMP_LABEL=TLPURYAN
       ;;
(TLPURYAN)
       m_CondExec 04,GE,TLPURYAM ${EXAAU},NE,YES 
       m_RcSet ${MT_RC_ABORT:-S999} "ZUTABEND : ${CURRENT_LABEL}"
# *********************************************************************        
#   REORGANISATION DU TABLESPACE RSTL02R                                       
#  REPRISE: NON BACKOUT = RECOVER                                              
#     POUR LES  TABLES LOGISTIQUES : RSTL01-05  ===> DB2RBP                    
#     POUR LES  TABLES LYON       : RSTL02Y-04Y-06Y-09Y ==>DB2RBY              
# ********************************************************************         
#                                                                              
# ***********************************                                          
# *   STEP TLPURYAQ PGM=DSNUTILB   ** ID=AAZ                                   
# ***********************************                                          
       JUMP_LABEL=END_JOB
       ;;
(END_JOB)
       break
       ;;
(*)
       m_RcSet ${MT_RC_ABORT:-S999} "Label inconnu : ${CURRENT_LABEL}"
       break
       ;;
esac
m_PhaseEnd
done
m_JobEnd
#@(#)---------------------------------------------------------------------------------------------
