       IDENTIFICATION DIVISION.
       PROGRAM-ID. SPDATDB2.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 DAT.
          02 DAT-1  PIC 9(5) VALUE 0.
          02 DAT-2  PIC 9(8) VALUE 0.
       01 DAT-R  PIC S9(13).
       77 DAT-S1 PIC S9(5) VALUE 0.
       77 DAT-S2 PIC S9(8) VALUE 0.

       LINKAGE SECTION.
       01 SCODER    PIC S9(4)  COMP.
       01 SDATE-S   PIC S9(13) COMP-3.
       01 SDATE-S1  PIC S9(5)  COMP-3.
       01 SDATE-S2  PIC S9(8)  COMP-3.

       PROCEDURE DIVISION USING SCODER SDATE-S SDATE-S1 SDATE-S2.
       START-PARA.
         
       IF  ADDRESS OF SCODER = NULL              
           MOVE 4 TO SCODER
       ELSE
          IF ADDRESS OF SDATE-S = NULL  
             IF ADDRESS OF SDATE-S1 = NULL OR
                ADDRESS OF SDATE-S2 = NULL 
                MOVE  4 TO SCODER
             ELSE 
                ACCEPT DAT-S1 FROM DAY
                ACCEPT DAT-S2 FROM TIME
                MOVE   DAT-S1 TO SDATE-S1
                MOVE   DAT-S2 TO SDATE-S2
                MOVE   0      TO SCODER
             END-IF   
          ELSE   
             ACCEPT DAT-1 FROM DAY
             ACCEPT DAT-2 FROM TIME
             MOVE DAT   TO DAT-R
             MOVE DAT-R TO SDATE-S
             MOVE   0   TO SCODER
          END-IF   

        END-IF.    
               
        GOBACK.
