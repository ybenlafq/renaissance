       IDENTIFICATION DIVISION.
       PROGRAM-ID. VA004PAX.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-ENTREE
              ASSIGN TO "ENTREE"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-SORTIE
              ASSIGN TO "SORTIE"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-ENTREE
           LABEL RECORD STANDARD
           DATA RECORD E-ENTREE.
       01 E-ENTREE.      
         04 FILL1            PIC X(15).                 
         04 FIL1             PIC S9(7) COMP-3.       
         04 FIL2             PIC S9(4) COMP-5.       
         04 FILL2            PIC X(52).       
         04 FIL3             PIC S9(9) COMP-3.       
         04 FIL4             PIC S9(9) COMP-3.       
         04 FIL5             PIC S9(15) COMP-3.       
         04 FIL6             PIC S9(15) COMP-3.       
         04 FIL7             PIC S9(15) COMP-3.       
         04 FIL8             PIC S9(15) COMP-3.       
         04 FIL9             PIC S9(11) COMP-3.       
         04 FIL10            PIC S9(11) COMP-3.       
         04 FIL11            PIC S9(11) COMP-3.       
         04 FIL12            PIC S9(11) COMP-3.       
         
       FD  MW-SORTIE
           LABEL RECORD STANDARD
           DATA RECORD S-SORTIE.
       01 S-SORTIE.
          04 FILL1            PIC X(15).                 
          04 FIL1             PIC S9(7).
          04 FIL2             PIC S9(4).
          04 FILL2            PIC X(52).       
          04 FIL3             PIC S9(9).
          04 FIL4             PIC S9(9).
          04 FIL5             PIC S9(15).
          04 FIL6             PIC S9(15).
          04 FIL7             PIC S9(15).
          04 FIL8             PIC S9(15).
          04 FIL9             PIC S9(11).
          04 FIL10            PIC S9(11).
          04 FIL11            PIC S9(11).
          04 FIL12            PIC S9(11).
       
       WORKING-STORAGE SECTION.
       
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
           OPEN INPUT MW-ENTREE
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN FILE ENTREE:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
           
           OPEN OUTPUT MW-SORTIE
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN FILE SORTIE:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.  
       READ-LOOP.
           READ MW-ENTREE NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.  
       
           GO TO READ-LOOP.	
                                                                
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
        
           CLOSE MW-ENTREE.
           CLOSE MW-SORTIE.
       
       MOVE-FIELD-TO-FIELD.
           MOVE CORRESPONDING E-ENTREE TO S-SORTIE
        
           WRITE S-SORTIE                     
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                                                      
