       IDENTIFICATION DIVISION.
       PROGRAM-ID. SYRD.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       CONFIGURATION SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 TWALENG PIC S9(4) COMP  VALUE ZERO.
       77 DEV     PIC S9(8) COMP. 
       77 STATUTS PIC S9(8) COMP.
       77 ACQ     PIC S9(8) COMP.
       77 RESP    PIC S9(8) COMP.
       LINKAGE SECTION.
       01  DFHCOMMAREA.
           05  FILLER           PIC X
                                    OCCURS 4096.
       01 LINK-TWA          PIC X(500).   
       01 TWAPTR REDEFINES LINK-TWA.
          02 TERMID  PIC X(4).
          02 REQUETE PIC X.
          02 REPONSE PIC X.
       PROCEDURE DIVISION .
       SYRD SECTION.
           EXEC CICS IGNORE CONDITION ERROR    END-EXEC.
           EXEC CICS ASSIGN TWALENG(TWALENG)   END-EXEC.
           IF TWALENG <= ZERO
              GO TO RETOUR
           ELSE
              EXEC CICS ADDRESS TWA (ADDRESS OF TWAPTR) END-EXEC
           END-IF.

       CONTINU.
           EXEC CICS INQUIRE TERMINAL(TERMID)          
              SERVSTATUS(STATUTS) RESP(RESP) DEVICE(DEV)
           END-EXEC.

           EXEC  CICS ENTER TRACEID(1) END-EXEC.
           IF RESP = DFHRESP(TERMIDERR)    
               GO TO INCONNU                                
           ELSE 
                EXEC CICS ENTER TRACEID(2) END-EXEC                
                IF RESP = DFHRESP(NORMAL)
                   GO TO    TERMOK                                 
                END-IF
           END-IF.

       INCONNU.
           MOVE "1" TO REPONSE.
           EXEC  CICS ENTER TRACEID(3) END-EXEC.
           GO TO RETOUR.

       TERMOK.    
           EXEC  CICS ENTER TRACEID(0) END-EXEC.
           IF DEV = DFHVALUE(T3284R)
              GO TO TERMOK1.
           EXEC  CICS ENTER TRACEID(5) END-EXEC.
           IF DEV = DFHVALUE(T3286R)
              GO TO TERMOK1.
           EXEC  CICS ENTER TRACEID(6) END-EXEC.
           IF DEV = DFHVALUE(T3284L)
              GO TO TERMOK1.
           EXEC  CICS ENTER TRACEID(7) END-EXEC.
           IF DEV = DFHVALUE(T3286L)
              GO TO TERMOK1.
           EXEC  CICS ENTER TRACEID(8) END-EXEC.
           IF DEV = DFHVALUE(T3790SCSP)
              GO TO TERMOK1.
           EXEC  CICS ENTER TRACEID(9) END-EXEC.
           MOVE  "2" TO REPONSE.
           GO TO RETOUR.

       TERMOK1.
           EXEC  CICS ENTER TRACEID(10) END-EXEC.
           IF STATUTS = DFHVALUE(INSERVICE)
              GO TO TERMOK2.                  
           EXEC  CICS ENTER TRACEID(11) END-EXEC.
           MOVE  "4" TO REPONSE.
           GO TO RETOUR.

       TERMOK2.
           EXEC  CICS ENTER TRACEID(12) END-EXEC.
           MOVE "0" TO REPONSE                         
           MOVE DFHVALUE(ACQUIRED) TO ACQ                   
           EXEC CICS SET TERMINAL(TERMID)                  
                RESP(RESP) ACQSTATUS(ACQ)                    
           END-EXEC.     
           IF RESP = DFHRESP(NORMAL)                       
              GO TO RETOUR.
           EXEC  CICS ENTER TRACEID(13) END-EXEC.
           MOVE "7" TO  REPONSE.
       RETOUR.
            EXEC  CICS ENTER TRACEID(14) END-EXEC.
            EXEC CICS RETURN             END-EXEC. 
            GOBACK.
