       Identification Division.
       Program-Id. XRUBPAIE.
      *
       Environment Division.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       Input-Output Section.
       File-Control.
           Select RUBPAIE assign SYSREC
                  organization is line sequential.
      *
           Select ALLDUPS assign ALLDUPS
                  organization is line sequential.
           Select NODUPS  assign NODUPS
                  organization is sequential.
      *
           Select TRI assign "TRI".
      *
       Data Division.
       File Section.
      *
       FD  RUBPAIE.
       01  RUBPAIE-RCD.
           03 NRUBPAIE            pic 9(3).
           03 filler              pic x(48).
      *
       FD  ALLDUPS.
       01  LIG1   pic x(51).
      *
       FD  NODUPS.
       01  LIG2   pic x(51).
      *
       SD  TRI.
       01  TRI-RCD.
           03 NRUBPAIE            pic 9(3).
           03 filler              pic x(48).
      *
       Working-Storage Section.
       77  WNOMBRE            pic 9(8)  value 0.
       01  FIN-FICHIER        pic 9.
           88 EOF     value 1.
           88 NOTEOF  value 0.
       01  DUP.
           03 WMLE1      pic 9(3).
           03 filler     pic x(48).
       Procedure Division.
       TRI-TRAIT section.
       TRI0.
           sort TRI on ascending key NRUBPAIE OF TRI-RCD 
                input  procedure ENTREE 
                output procedure SORTIE.
           stop run.  
       ENTREE Section.
       ENTREE0.
           open input RUBPAIE.
           move 0 to FIN-FICHIER.
           perform ENTREE-LECTURE until EOF.
           close RUBPAIE.
       ENTREE-TRAITEMENT section.
       ENTREE-LECTURE.
           read RUBPAIE at end move 1 to FIN-FICHIER.
           if NOTEOF perform ENTREE-TRAIT.
       ENTREE-TRAIT.
           move RUBPAIE-RCD to TRI-RCD.
           release TRI-RCD.
       SORTIE Section.
       SORTIE0.
           move 0 to FIN-FICHIER.
           open output ALLDUPS.
           open output NODUPS.
           Return TRI at end move 1 to FIN-FICHIER.
           if not EOF perform TRAITEMENT.
       FIN-TRAIT.    
           close ALLDUPS.
           close NODUPS.
       TRAITEMENT Section.
       DEBUT-MLE.
           move TRI-RCD to DUP.
       TRAIT-SUIVANT.
           perform TRAIT-LECTURE until EOF.
       TRAIT-MLE section.   
       TRAIT-LECTURE.    
           Return TRI at end move 1 to FIN-FICHIER.
           if EOF 
              perform FIN-MLE
           else 
                if NRUBPAIE of TRI-RCD not = WMLE1
                   perform FIN-MLE
                 else 
                   add 1 to WNOMBRE
                   write LIG1 from DUP.
           move TRI-RCD to DUP.
       FIN-MLE.    
           if WNOMBRE = 0 write LIG2 from DUP
           else write LIG1 from DUP.
           move 0 to WNOMBRE .
