       IDENTIFICATION DIVISION.
       PROGRAM-ID. WAITSS.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
                
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  Seconds-To-Sleep       USAGE COMP-2.
                                      
       PROCEDURE DIVISION.
       ACCEPT Seconds-To-Sleep FROM COMMAND-LINE
                                                   
       CALL "C$SLEEP" USING BY CONTENT Seconds-To-Sleep
       GOBACK.
