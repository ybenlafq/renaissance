       IDENTIFICATION DIVISION.
       PROGRAM-ID. HWAIT.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  Seconds-To-Sleep       USAGE COMP-2.
       01  Minutes-To-Sleep       USAGE COMP-2.
                                      
       PROCEDURE DIVISION.
       ACCEPT Minutes-To-Sleep FROM COMMAND-LINE
       Compute Seconds-To-Sleep = Minutes-To-Sleep * 60
                                                   
       CALL "C$SLEEP" USING BY CONTENT Seconds-To-Sleep
       GOBACK.
