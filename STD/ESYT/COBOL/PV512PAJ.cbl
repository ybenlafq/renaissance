       IDENTIFICATION DIVISION.
       PROGRAM-ID. PV512PAJ.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FILEB
              ASSIGN TO "FILEB"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
         02 IPV512.
         04 FILLER           PIC X(06).                 
         04 NSOCIETE         PIC X(03).                 
         04 NLIEU            PIC X(03).  
         04 FILLER           PIC X(07).                 
         04 RUBRIQUE         PIC X(20).                 
         04 COMM             PIC S9(7)V99 COMP-3.       
         04 MBU              PIC S9(7)V99 COMP-3.       
         04 MBUGRP           PIC S9(7)V99 COMP-3.       
         04 PCA              PIC S9(7)V99 COMP-3.       
         04 PCAGRP           PIC S9(7)V99 COMP-3.       
         04 PMT              PIC S9(7)V99 COMP-3.       
         04 PMTADD           PIC S9(7)V99 COMP-3.       
         04 PMTEP            PIC S9(7)V99 COMP-3.       
         04 PMTPSE           PIC S9(7)V99 COMP-3.       
         04 PMTTOT           PIC S9(7)V99 COMP-3.       
         04 PMTVOL           PIC S9(7)V99 COMP-3.       
         04 PRIMECLI         PIC S9(7)V99 COMP-3.       
         04 QVENDUE          PIC S9(5) COMP-3.          
         04 QVENDUEG         PIC S9(5) COMP-3.          
         04 RECETTE          PIC S9(7)V99 COMP-3.       
         04 DEBPERIODE       PIC X(8).                  
         04 FINPERIODE       PIC X(8).      
         04 FILLER           PIC X(385).
         
       FD  MW-FILEB
           LABEL RECORD STANDARD
           DATA RECORD S-FILEB.
       01 S-FILEB.
       02 IPV512B.                              
       04 NSOCIETEB        PIC X(03).           
       04 FIL1             PIC X(01).           
       04 NLIEUB           PIC X(03).           
       04 FIL2             PIC X(01).           
       04 RUBRIQUEB        PIC X(20).           
       04 FIL3             PIC X(01).           
       04 QVENDUEB         PIC S9(5) COMP-3.    
       04 FIL4             PIC X(01).           
       04 QVENDUEGB        PIC S9(5) COMP-3.    
       04 FIL5             PIC X(01).           
       04 PCAB             PIC S9(7)V99 COMP-3. 
       04 FIL6             PIC X(01).           
       04 PRIMECLIB        PIC S9(7)V99 COMP-3. 
       04 FIL7             PIC X(01).           
       04 PCAGRPB          PIC S9(7)V99 COMP-3. 
       04 FIL8             PIC X(01).           
       04 COMMB            PIC S9(7)V99 COMP-3. 
       04 FIL9             PIC X(01).           
       04 RECETTEB         PIC S9(7)V99 COMP-3. 
       04 FIL10            PIC X(01).           
       04 MBUB             PIC S9(7)V99 COMP-3. 
       04 FIL11            PIC X(01).           
       04 TXMBUB           PIC S9(3)V99 COMP-3. 
       04 FIL12            PIC X(01).           
       04 MBUGRPB          PIC S9(7)V99 COMP-3. 
       04 FIL13            PIC X(01).           
       04 TXMBUGRPB        PIC S9(3)V99 COMP-3. 
       04 FIL14            PIC X(01).           
       04 PMTB             PIC S9(7)V99 COMP-3. 
       04 FIL15            PIC X(01).           
       04 PMTADDB          PIC S9(7)V99 COMP-3. 
       04 FIL16            PIC X(01).           
       04 PMTEPB           PIC S9(7)V99 COMP-3. 
       04 FIL17            PIC X(01).           
       04 PMTPSEB          PIC S9(7)V99 COMP-3. 
       04 FIL18            PIC X(01).           
       04 PMTVOLB          PIC S9(7)V99 COMP-3. 
       04 FIL19            PIC X(01).           
       04 PMTTOTB          PIC S9(7)V99 COMP-3. 
       04 FIL20            PIC X(01).           
       04 TXEURVB          PIC S9(5)V99 COMP-3. 
       04 FIL21            PIC X(01).           
       04 TXEURMB          PIC S9(5)V99 COMP-3. 
       
       WORKING-STORAGE SECTION.
       01 TXMBU    PIC S9(3)V99 COMP-3.                         
       01 TXMBUGRP PIC S9(3)V99 COMP-3.                      
       01 TXEURV   PIC S9(5)V99 COMP-3.                            
       01 TXEURM   PIC S9(5)V99 COMP-3.                      
       01 GRPCOMM  PIC S9(7)V99 COMP-3.                    
                                        
       
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
           OPEN INPUT MW-FILEA
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
           
           OPEN OUTPUT MW-FILEB
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.  

       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.  
       
           GO TO READ-LOOP.	
                                                                
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
        
           CLOSE MW-FILEA.
           CLOSE MW-FILEB.
       
       MOVE-FIELD-TO-FIELD.
       
           IF PCA = 0                                              
              MOVE 1 TO PCA                                  
           END-IF                                             
           IF PCAGRP = 0                            
              MOVE 1 TO PCAGRP                                 
           END-IF                                             
           IF MBUGRP = 0                                       
              MOVE 1 TO MBUGRP               
           END-IF                                                    
           COMPUTE TXMBU = (MBU * 100 ) / PCA       
           COMPUTE GRPCOMM = (PCAGRP + COMM)                    
           IF GRPCOMM = 0                                     
              MOVE 1 TO GRPCOMM                                     
           END-IF                                             
           COMPUTE TXMBUGRP = (MBUGRP * 100 ) / ( GRPCOMM )   
           COMPUTE TXEURV   = (PMTTOT * 10000 ) / ( GRPCOMM )
           COMPUTE TXEURM   = (PMTTOT * 10000 ) / ( MBUGRP )       
           MOVE NSOCIETE  TO NSOCIETEB                         
           MOVE NLIEU     TO NLIEUB                        
           MOVE RUBRIQUE  TO RUBRIQUEB             
           MOVE QVENDUE   TO QVENDUEB                        
           MOVE QVENDUEG  TO QVENDUEGB                         
           MOVE PCA       TO PCAB                             
           MOVE PRIMECLI  TO PRIMECLIB                          
           MOVE PCAGRP    TO PCAGRPB                         
           MOVE COMM      TO COMMB                     
           MOVE RECETTE   TO RECETTEB                         
           MOVE MBU       TO MBUB                           
           MOVE TXMBU     TO TXMBUB                            
           MOVE MBUGRP    TO MBUGRPB                          
           MOVE TXMBUGRP  TO TXMBUGRPB                          
           MOVE PMT       TO PMTB                               
           MOVE PMTADD    TO PMTADDB                            
           MOVE PMTEP     TO PMTEPB                                
           MOVE PMTPSE    TO PMTPSEB                                
           MOVE PMTVOL    TO PMTVOLB                             
           MOVE PMTTOT    TO PMTTOTB                               
           MOVE TXEURV    TO TXEURVB                               
           MOVE TXEURM    TO  TXEURMB 
           MOVE  ';' TO                                             
           FIL1                                                  
           FIL2                                                  
           FIL3                                                 
           FIL4                                                
           FIL5                                                
           FIL6                                              
           FIL7                                                
           FIL8                                                
           FIL9                                               
           FIL10                                            
           FIL11                                           
           FIL12                                         
           FIL13                                          
           FIL14                                              
           FIL15                                        
           FIL16                                               
           FIL17                                            
           FIL18                                     
           FIL19                                     
           FIL20                                           
           FIL21                         
        
           WRITE S-FILEB                      
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                                                      
