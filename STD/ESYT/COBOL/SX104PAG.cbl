       IDENTIFICATION DIVISION.
       PROGRAM-ID. SX104PAG.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEAI
              ASSIGN TO "FILEAI"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FILEAO
              ASSIGN TO "FILEAO"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEAI
           LABEL RECORD STANDARD
           DATA RECORD E-FILEAI.
       01 E-FILEAI.      
          02 COL01A47  PIC X(47).  
          02 COL48A57  PIC X(10).  
          02 COL58A132 PIC X(75).  
               	    
       FD  MW-FILEAO
           LABEL RECORD STANDARD
           DATA RECORD S-FILEAO.
       01 S-FILEAO.   
          02 COL01A01    PIC X(01).  
          02 COL01A47    PIC X(47).  
          02 COL48A57    PIC X(10).  
          02 COL58A132   PIC X(76).  
          
       	  
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
            
           OPEN INPUT MW-FILEAI
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
           
           OPEN OUTPUT MW-FILEAO
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.  

       READ-LOOP.
           READ MW-FILEAI NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD. 
       
           GO TO READ-LOOP.	
                                                    
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
         
        CLOSE MW-FILEAI.
        CLOSE MW-FILEAO.
       
       MOVE-FIELD-TO-FIELD.
       
           IF COL48A57 OF MW-FILEAI = 'Etat SIG02' 
              MOVE '1' TO COL01A01
           END-IF.	
           MOVE CORRESPONDING  E-FILEAI TO S-FILEAO         
       
           WRITE S-FILEAO   
           MOVE  ' '  TO COL01A01
       	.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                   
