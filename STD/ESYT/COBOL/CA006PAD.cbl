       IDENTIFICATION DIVISION.
       PROGRAM-ID. CA006PAD.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FILES
              ASSIGN TO "FILES"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
          02 FILLER      PIC X(6). 
          02 COL07A80.
             04 FILLER   PIC X(54).
             04 COL61A66 PIC X(6).
             04 FILLER   PIC X(14).
         
       FD  MW-FILES
           LABEL RECORD STANDARD
           DATA RECORD S-FILES.
       01 S-FILES.   
          02 COL01A06 PIC X(6).
          02 COL07A80 PIC X(74).
                                                                
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
            
          OPEN INPUT MW-FILEA
              IF IO-STATUS NOT = "00"
                 DISPLAY "ERROR OPEN:"
                 DISPLAY "IO-STATUS =" IO-STATUS
                 GO TO FIN-ERREUR
              END-IF.
           
              OPEN OUTPUT MW-FILES
              IF IO-STATUS NOT = "00"
                 DISPLAY "ERROR OPEN:"
                 DISPLAY "IO-STATUS =" IO-STATUS
                 GO TO FIN-ERREUR
              END-IF.  
       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.
                  
           GO TO READ-LOOP.                   
       
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
        
             
        CLOSE MW-FILEA.
        CLOSE MW-FILES.
       
       MOVE-FIELD-TO-FIELD.
       
         MOVE COL61A66 TO COL01A06  
         MOVE CORRESPONDING E-FILEA TO S-FILES  
       
         WRITE S-FILES                                               
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT. 
