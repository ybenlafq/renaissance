       IDENTIFICATION DIVISION.
       PROGRAM-ID. MGOK1FAM.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-MGOK1SMS
              ASSIGN TO "MGOK1SMS"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
      
       SELECT MW-MGOK1FSC
              ASSIGN TO "MGOK1FSC"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
      
       DATA DIVISION.
       FILE SECTION.
       FD  MW-MGOK1SMS
           LABEL RECORD STANDARD
           DATA RECORD E-MGOK1SMS.
       01  E-MGOK1SMS.
           02 LIGNE.
              04 COL1A4      PIC X(4). 
              04 FILLER      PIC X(76).
      
       FD  MW-MGOK1FSC
           LABEL RECORD STANDARD
           DATA RECORD S-MGOK1FSC.
       01  S-MGOK1FSC.
           02 LIGNES.
              04 FILLER      PIC X(40).
              04 JJS         PIC X(2).
              04 MMS         PIC X(2).      
              04 AAS         PIC X(2).   
              04 HHS         PIC X(2). 
              04 MNS         PIC X(2).   
              04 SSS         PIC X(2). 
              04 SC          PIC X(2). 
              04 FILLER      PIC X(26).

       WORKING-STORAGE SECTION.
       01 DATES.
          02 SAA         PIC 9(2).
          02 SMM         PIC 9(2).
          02 SJJ         PIC 9(2).
          02 SHH         PIC 9(2).
          02 SMN         PIC 9(2).
          02 SST         PIC 9(2).
          02 SSC         PIC 9(2).

       01  IO-STATUS            PIC XX.
      
       PROCEDURE DIVISION.
       START-PARA.
           OPEN INPUT MW-MGOK1SMS
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.
      
           OPEN OUTPUT MW-MGOK1FSC
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.

       READ-MGOK1SMS.
           READ MW-MGOK1SMS NEXT
            AT END GO TO FIN-REL
           END-READ.

           PERFORM MOVE-FIELD-TO-FIELD
            THRU E-MOVE-FIELD-TO-FIELD.
      
           GO TO READ-MGOK1SMS.
      
      
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
      
           EXIT PROGRAM.
           STOP RUN.
      
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.

           EXIT PROGRAM.
           STOP RUN RETURNING 16.

       END-COMMON-DISPLAY.
           CLOSE MW-MGOK1SMS.
           CLOSE MW-MGOK1FSC.

       MOVE-FIELD-TO-FIELD.
           IF COL1A4 = 'PUT '
              MOVE LIGNE TO LIGNES
           ELSE
              MOVE LIGNE TO LIGNES
              MOVE FUNCTION CURRENT-DATE (3:14) TO DATES
              MOVE SJJ      TO JJS
              MOVE SMM      TO MMS
              MOVE SAA      TO AAS
              MOVE SHH      TO HHS
              MOVE SMN      TO MNS
              MOVE SST      TO SSS
              MOVE '99'     TO SC
           END-IF
           WRITE S-MGOK1FSC
           .
       E-MOVE-FIELD-TO-FIELD.
           EXIT.
