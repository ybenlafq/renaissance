       IDENTIFICATION DIVISION.
       PROGRAM-ID. FTCREPAA.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-SORTIE
              ASSIGN TO "SORTIE"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
         02 FIL1       PIC X(15).  
         02 Z1P        PIC S9(5). 
         02 FIL2       PIC X(41).
         02 Z2P        PIC S9(4).
         02 FIL3       PIC X(1).
         02 Z3P        PIC S9(14).
         02 FIL4       PIC X(329).
         
       FD  MW-SORTIE
           LABEL RECORD STANDARD
           DATA RECORD S-SORTIE.
       01 S-SORTIE.   
         02 FIL1        PIC X(15).
         02 R1P         PIC S9(5) COMP-3.
         02 FIL2        PIC X(41).     
         02 R2P         PIC S9(4) COMP-3. 
         02 FIL3        PIC X(1).  
         02 R3P         PIC S9(14) COMP-3. 
         02 FIL4        PIC X(328). 
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
            
           OPEN INPUT MW-FILEA
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
           
           OPEN OUTPUT MW-SORTIE
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.  
       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.

           GO TO READ-LOOP.
                                                                     
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
             
          CLOSE MW-FILEA.
          CLOSE MW-SORTIE.
       
       MOVE-FIELD-TO-FIELD.
       
          MOVE CORRESPONDING E-FILEA TO S-SORTIE 
          MOVE Z1P TO R1P   
          MOVE Z2P TO R2P
          MOVE Z3P TO R3P  
                                                            
          WRITE S-SORTIE   
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT. 
                                                                       
          
                                                                      
