       IDENTIFICATION DIVISION.
       PROGRAM-ID. DWA00PBA.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FILEB
              ASSIGN TO "FILEB"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
          02 TEXTA       PIC X(36).                 
          02 DECI1A      PIC S9(5) COMP-3.                         
          02 DECI2A      PIC S9(5) COMP-3. 	  
         
       FD  MW-FILEB
           LABEL RECORD STANDARD
           DATA RECORD S-FILEB.
       01 S-FILEB.   
          02 TEXTB   PIC X(36).    
          02 NUM1B   PIC S9(5).                               
          02 NUM2B   PIC S9(5). 	  
       	  
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
            
          OPEN INPUT MW-FILEA
              IF IO-STATUS NOT = "00"
                 DISPLAY "ERROR OPEN:"
                 DISPLAY "IO-STATUS =" IO-STATUS
                 GO TO FIN-ERREUR
              END-IF.
           
              OPEN OUTPUT MW-FILEB
              IF IO-STATUS NOT = "00"
                 DISPLAY "ERROR OPEN:"
                 DISPLAY "IO-STATUS =" IO-STATUS
                 GO TO FIN-ERREUR
              END-IF.  
       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.
                  
           GO TO READ-LOOP.                   

       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
             
           CLOSE MW-FILEA.
           CLOSE MW-FILEB.
       
       MOVE-FIELD-TO-FIELD.
       
           MOVE TEXTA  TO TEXTB                     
           MOVE DECI1A TO NUM1B                
           MOVE DECI2A TO NUM2B
       
           WRITE S-FILEB                 
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                                                  
