       IDENTIFICATION DIVISION.
       PROGRAM-ID. FG00FPBD.
      **********************************************************
      * Ce programme permet de lire un fichier et sortir un etat
      * avec insertion des separateurs.
      * *********************************************************
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-ENTREE
              ASSIGN TO "FICHI"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
      
       SELECT MW-SORTIE
              ASSIGN TO "FICHO"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
      
       DATA DIVISION.
       FILE SECTION.
       FD  MW-ENTREE
           LABEL RECORD STANDARD
           DATA RECORD E-INPUT.
       01  E-INPUT.
           04 CAPPLI      PIC X(3). 
           04 NSOCORIG    PIC X(3).
           04 NLIEUORIG   PIC X(3).
           04 NSOCDEST    PIC X(3).
           04 NLIEUDEST   PIC X(3).
           04 CTYPFACT    PIC X(2).
           04 NATFACT     PIC X(5).
           04 CVENT       PIC X(5).
           04 MONTHT      PIC 9(13).
           04 CTAUXTVA    PIC X(5).
           04 DMOUV       PIC X(8).
           04 QNBPIECES   PIC 9(5).
           04 LIBELLE     PIC X(22).
           04 NTIERSE     PIC X(8).
           04 NLETTRE     PIC X(10).
           04 NTIERSR     PIC X(8).
           04 NLETTRR     PIC X(10).
      
       FD  MW-SORTIE
           LABEL RECORD STANDARD
           DATA RECORD S-OUTPUT.
       01  S-OUTPUT.
           04 CAPPLI            PIC X(3).  
           04 SEP01             PIC X(1).  
           04 NSOCORIG          PIC X(3).  
           04 SEP02             PIC X(1).  
           04 NLIEUORIG         PIC X(3).  
           04 SEP03             PIC X(1).  
           04 NSOCDEST          PIC X(3).  
           04 SEP04             PIC X(1).  
           04 NLIEUDEST         PIC X(3).  
           04 SEP05             PIC X(1).  
           04 CTYPFACT          PIC X(2).  
           04 SEP06             PIC X(1).  
           04 NATFACT           PIC X(5).  
           04 SEP07             PIC X(1).  
           04 CVENT             PIC X(5).  
           04 SEP08             PIC X(1).  
           04 MONTHT            PIC 9(13).  
           04 SEP09             PIC X(1).  
           04 CTAUXTVA          PIC X(5).  
           04 SEP10             PIC X(1).  
           04 DMOUV             PIC X(8).  
           04 SEP11             PIC X(1).  
           04 QNBPIECES         PIC 9(5).  
           04 SEP12             PIC X(1).  
           04 LIBELLE           PIC X(22).  
           04 SEP13             PIC X(1).  
           04 NTIERSE           PIC X(8).  
           04 SEP14             PIC X(1).  
           04 NLETTRE           PIC X(10).  
           04 SEP15             PIC X(1).  
           04 NTIERSR           PIC X(8).  
           04 SEP16             PIC X(1).  
           04 NLETTRR           PIC X(10).  
           04 SEP17             PIC X(1).  
           04 FIL1              PIC X(367).  

       WORKING-STORAGE SECTION.
       77 NBREAD   PIC 9(5) VALUE 0.
       77 NBPUT    PIC 9(5) VALUE 0.

       01  WTITRE.
           02 ENREG1 PIC X(230).
           02 ENREG2 PIC X(270).
       
       01 WTITR1.
          02 FILLER PIC X(29) VALUE "CODE APPLICATION;STE ORIGINE;".
          02 FILLER PIC X(30) VALUE "LIEU ORIGINE;STE DESTINATAIRE;".
          02 FILLER PIC X(31) VALUE "LIEU DESTINATAIRE;TYPE FACTURE;".
          02 FILLER PIC X(23) VALUE "NATURE FACTURE;CRITERE ".
          02 FILLER PIC X(30) VALUE "VENTILATION;MONTANT  H T;CODE ".
          02 FILLER PIC X(29) VALUE "TAUX TVA;DATE MOUVEMENT;NBRE ".
          02 FILLER PIC X(31) VALUE "PIECES;LIBELLE OU N� FACTURE + ".
          02 FILLER PIC X(27) VALUE "DATE VALEUR;TIERS EMETTEUR;".
                
       01 WTITR2.
          02 FILLER  PIC X(24) VALUE "LETTRAGE EMETTEUR;TIERS ".
          02 FILLER  PIC X(206) VALUE "RECEPTEUR;LETTRAGE RECEPTEUR;".

       01  IO-STATUS            PIC XX.
      
       PROCEDURE DIVISION.
       START-PARA.
           DISPLAY  '***** DEBUT EZFMICRO ***********'
           OPEN INPUT MW-ENTREE
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.
      
           OPEN OUTPUT MW-SORTIE
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.

       WRITE-TOP.    
           MOVE WTITR1 TO ENREG1.
           MOVE WTITR2 TO ENREG2.
           MOVE WTITRE TO S-OUTPUT.
           WRITE S-OUTPUT.
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.
           MOVE ';' TO SEP01 SEP02 SEP03 SEP04 SEP05 SEP06
           SEP07 SEP08 SEP09 SEP10 SEP11 SEP12 SEP13 SEP14
           SEP15 SEP16 SEP17.

       READ-LOOP.
           READ MW-ENTREE NEXT
            AT END GO TO FIN-REL
           END-READ.
           ADD 1 TO NBREAD.
      
           PERFORM MOVE-FIELD-TO-FIELD
            THRU E-MOVE-FIELD-TO-FIELD.
      
           WRITE S-OUTPUT.
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.
      
           ADD 1 TO NBPUT.
           GO TO READ-LOOP.
      
       FIN-REL.
           DISPLAY "INSERTION TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
      
           EXIT PROGRAM.
           STOP RUN.
      
       FIN-ERREUR.
           DISPLAY "INSERTION FAILED".
           PERFORM END-COMMON-DISPLAY.

           EXIT PROGRAM.
           STOP RUN RETURNING 16.

       END-COMMON-DISPLAY.
           DISPLAY "Nb rows reloaded: " NBREAD.
           DISPLAY "Nb rows insered:  " NBPUT.
      
           CLOSE MW-ENTREE.
           CLOSE MW-SORTIE.
           DISPLAY  '***** FIN   EZFMICRO ***********'.

       MOVE-FIELD-TO-FIELD.
           MOVE SPACES TO FIL1.
           MOVE CORRESPONDING E-INPUT TO S-OUTPUT
           .
       E-MOVE-FIELD-TO-FIELD.
           EXIT.
