       IDENTIFICATION DIVISION.
       PROGRAM-ID. NASFTFAD.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-NASCGCT
         ASSIGN TO "NASCGCT"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-NASCKT0
         ASSIGN TO "NASCKT0"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-NASCA2I
         ASSIGN TO "NASCA2I"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-NASGCTD
         ASSIGN TO "NASGCTD"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
       
       SELECT MW-NASKT0D
         ASSIGN TO "NASKT0D"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
        
       SELECT MW-NASGCTL
         ASSIGN TO "NASGCTL"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-NASKT0L
         ASSIGN TO "NASKT0L"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-NASGCTM
         ASSIGN TO "NASGCTM"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.  
         
       SELECT MW-NASKT0M
         ASSIGN TO "NASKT0M"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS. 
       
       SELECT MW-NASGCTO
         ASSIGN TO "NASGCTO"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
       
       SELECT MW-NASKT0O
         ASSIGN TO "NASKT0O"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
        
       SELECT MW-NASGCTP
         ASSIGN TO "NASGCTP"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-NASKT0P
         ASSIGN TO "NASKT0P"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-FSLA2IP
         ASSIGN TO "FSLA2IP"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.  
         
       SELECT MW-NASGCTY
         ASSIGN TO "NASGCTY"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
       
       SELECT MW-NASKT0Y
         ASSIGN TO "NASKT0Y"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.  
         
       DATA DIVISION.
       FILE SECTION.
       FD  MW-NASCGCT
          LABEL RECORD STANDARD
          DATA RECORD E-NASCGCT.
       01 E-NASCGCT.
          02 TOUT.
           04 FILLER PIC X(101).
           04 FILI   PIC X(3).
           04 FILLER PIC X(296).
         
       FD  MW-NASCKT0
          LABEL RECORD STANDARD
          DATA RECORD E-NASCKT0.
       01 E-NASCKT0.
          02 TOUT.
           04 FILLER PIC X.
           04 FILI   PIC X(3).    
           04 FILLER PIC X(217).
          
       
       FD  MW-NASCA2I
       LABEL RECORD STANDARD
       DATA RECORD E-NASCA2I.
       01 E-NASCA2I.
          02 TOUT PIC X(277).
         
       FD MW-NASGCTD
       LABEL RECORD STANDARD
       DATA RECORD S-NASGCTD.
       01 S-NASGCTD.
          02 TOUT PIC X(400).
       
       FD MW-NASKT0D
       LABEL RECORD STANDARD
       DATA RECORD S-NASKT0D.
       01 S-NASKT0D.
          02 TOUT  PIC X(221).

       FD MW-NASGCTL
       LABEL RECORD STANDARD
       DATA RECORD S-NASGCTL.
       01 S-NASGCTL.
          02 TOUT  PIC X(400).

       FD MW-NASKT0L
       LABEL RECORD STANDARD
       DATA RECORD S-NASKT0L.
       01 S-NASKT0L.
          02 TOUT  PIC X(221).

       FD MW-NASGCTM
       LABEL RECORD STANDARD
       DATA RECORD S-NASGCTM.
       01 S-NASGCTM.
          02 TOUT  PIC X(400).

       FD MW-NASKT0M
       LABEL RECORD STANDARD
       DATA RECORD S-NASKT0M.
       01 S-NASKT0M.
          02 TOUT  PIC X(221).

       FD MW-NASGCTO
       LABEL RECORD STANDARD
       DATA RECORD S-NASGCTO.
       01 S-NASGCTO.
          02 TOUT  PIC X(400).

       FD MW-NASKT0O
       LABEL RECORD STANDARD
       DATA RECORD S-NASKT0O.
       01 S-NASKT0O.
          02 TOUT  PIC X(221).

       FD MW-NASGCTP
       LABEL RECORD STANDARD
       DATA RECORD S-NASGCTP.
       01 S-NASGCTP.
          02 TOUT  PIC X(400).

       FD MW-NASKT0P
       LABEL RECORD STANDARD
       DATA RECORD S-NASKT0P.
       01 S-NASKT0P.
          02 TOUT  PIC X(221).

       FD MW-FSLA2IP
       LABEL RECORD STANDARD
       DATA RECORD S-FSLA2IP.
       01 S-FSLA2IP.
          02 TOUT  PIC X(277).

       FD MW-NASGCTY
       LABEL RECORD STANDARD
       DATA RECORD S-NASGCTY.
       01 S-NASGCTY.
          02 TOUT  PIC X(400).

       FD MW-NASKT0Y
       LABEL RECORD STANDARD
       DATA RECORD S-NASKT0Y.
       01 S-NASKT0Y.
          02 TOUT  PIC X(221).
       
       WORKING-STORAGE SECTION.
       01  IO-STATUS PIC XX.
       
       PROCEDURE DIVISION.
       START-PARA.
          OPEN INPUT MW-NASCGCT
          PERFORM VERIFY-OPEN.
          
          OPEN INPUT MW-NASCKT0
          PERFORM VERIFY-OPEN.
          
          OPEN INPUT MW-NASCA2I
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASGCTD
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASKT0D
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASGCTL
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASKT0L
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASGCTM
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASKT0M
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASGCTO
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASKT0O
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASGCTP
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASKT0P
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-FSLA2IP
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASGCTY
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-NASKT0Y
          PERFORM VERIFY-OPEN.

       READ-NASCGCT.   

          READ MW-NASCGCT NEXT
               AT END GO TO READ-NASCKT0
          END-READ.
          
          IF FILI OF MW-NASCGCT = '991' OR '828'
             MOVE CORRESPONDING E-NASCGCT TO S-NASGCTD 
             WRITE S-NASGCTD
          END-IF
          IF FILI OF MW-NASCGCT = '961' OR '824'
             MOVE CORRESPONDING E-NASCGCT TO S-NASGCTL 
             WRITE S-NASGCTL
          END-IF
          IF FILI OF MW-NASCGCT = '989' OR '827'
             MOVE CORRESPONDING E-NASCGCT TO S-NASGCTM 
             WRITE S-NASGCTM
          END-IF
          IF FILI OF MW-NASCGCT = '916' OR '821'
             MOVE CORRESPONDING E-NASCGCT TO S-NASGCTO 
             WRITE S-NASGCTO
          END-IF
          IF FILI OF MW-NASCGCT = '907' OR '820' OR '829'
             MOVE CORRESPONDING E-NASCGCT TO S-NASGCTP 
             WRITE S-NASGCTP
          END-IF
          IF FILI OF MW-NASCGCT = '945' OR '822'
             MOVE CORRESPONDING E-NASCGCT TO S-NASGCTY 
             WRITE S-NASGCTY
          END-IF

          GO TO READ-NASCGCT.

       READ-NASCKT0.

          READ MW-NASCKT0 NEXT
               AT END GO TO READ-NASCA2I
          END-READ.
          
          IF FILI OF MW-NASCKT0 = '991' 
             MOVE CORRESPONDING E-NASCKT0 TO S-NASKT0D
             WRITE S-NASKT0D 
          END-IF
          IF FILI OF MW-NASCKT0 = '961' 
             MOVE CORRESPONDING E-NASCKT0 TO S-NASKT0L  
             WRITE S-NASKT0L 
          END-IF
          IF FILI OF MW-NASCKT0 = '989' 
             MOVE CORRESPONDING E-NASCKT0 TO S-NASKT0M  
             WRITE S-NASKT0M 
          END-IF
          IF FILI OF MW-NASCKT0 = '916' 
             MOVE CORRESPONDING E-NASCKT0 TO S-NASKT0O  
             WRITE S-NASKT0O 
          END-IF
          IF FILI OF MW-NASCKT0 = '907' 
             MOVE CORRESPONDING E-NASCKT0 TO S-NASKT0P  
             WRITE S-NASKT0P 
          END-IF
          IF FILI OF MW-NASCKT0 = '945' 
             MOVE CORRESPONDING E-NASCKT0 TO S-NASKT0Y  
             WRITE S-NASKT0Y
          END-IF

          GO TO READ-NASCKT0.
      *
       READ-NASCA2I.

          READ MW-NASCA2I NEXT
               AT END GO TO FIN-REL
          END-READ.
          
          MOVE CORRESPONDING E-NASCA2I TO S-FSLA2IP  WRITE S-FSLA2IP.

          GO TO READ-NASCA2I.
      *
       
       FIN-REL.
          DISPLAY "PROGRAM TERMINATED OK".
          PERFORM END-COMMON-DISPLAY.
       
          EXIT PROGRAM.
          STOP RUN.
          
       VERIFY-OPEN.
          IF IO-STATUS NOT = "00"
             DISPLAY "ERROR: OPEN FILE " 
             DISPLAY "IO-STATUS =" IO-STATUS
             PERFORM FIN-ERREUR
          END-IF.
       
       FIN-ERREUR.
          PERFORM END-COMMON-DISPLAY.
       
          EXIT PROGRAM.
          STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
          CLOSE MW-NASCGCT
          CLOSE MW-NASCKT0
          CLOSE MW-NASCA2I
          CLOSE MW-NASGCTD
          CLOSE MW-NASKT0D
          CLOSE MW-NASGCTL
          CLOSE MW-NASKT0L 
          CLOSE MW-NASGCTM 
          CLOSE MW-NASKT0M
          CLOSE MW-NASGCTO 
          CLOSE MW-NASKT0O  
          CLOSE MW-NASGCTP
          CLOSE MW-NASKT0P 
          CLOSE MW-FSLA2IP
          CLOSE MW-NASGCTY 
          CLOSE MW-NASKT0Y. 
