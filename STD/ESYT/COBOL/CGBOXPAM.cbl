       IDENTIFICATION DIVISION.
       PROGRAM-ID. CGBOXPAM.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FILEB
              ASSIGN TO "FILEB"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
          02 AFILE.                                           
             04 ATABNAM PIC X(10).
             04 FILLER  PIC X(1090).	  
         
       FD  MW-FILEB
           LABEL RECORD STANDARD
           DATA RECORD S-FILEB.
       01 S-FILEB.   
          02 BFILE.  
             04 BTABNAM     PIC X(10).
             04 FILLER      PIC X(119).
             04 BMESCOD     PIC X(3).
             04 FILLER      PIC X(131).	  
             04 BRCVPOR     PIC X(10).
             04 FILLER      PIC X(4).  
             04 BRCVPRN     PIC X(10). 
             04 FILLER      PIC X(813).	  
       	  
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
          OPEN INPUT MW-FILEA
          IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
          END-IF.
           
          OPEN OUTPUT MW-FILEB
          IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
          END-IF.  
       READ-LOOP.
          READ MW-FILEA NEXT
             AT END GO TO FIN-REL
          END-READ.
                  
          PERFORM MOVE-FIELD-TO-FIELD
          THRU E-MOVE-FIELD-TO-FIELD.
                  
          GO TO READ-LOOP.                   

       FIN-REL.
          DISPLAY "PROGRAM TERMINATED OK".
          PERFORM END-COMMON-DISPLAY.
             
          EXIT PROGRAM.
          STOP RUN.
             
       FIN-ERREUR.
          PERFORM END-COMMON-DISPLAY.
       
          EXIT PROGRAM.
          STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
        
             
          CLOSE MW-FILEA.
          CLOSE MW-FILEB.
       
       MOVE-FIELD-TO-FIELD.
       
          MOVE AFILE TO BFILE                             
          IF ATABNAM = 'EDI_DC40'                        
             MOVE 'SAPPRD'  TO BRCVPOR                  
             MOVE 'PRD_020' TO BRCVPRN              
             MOVE 'DAT'     TO BMESCOD                 
          END-IF                
       
          WRITE S-FILEB                             
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                                              
