       IDENTIFICATION DIVISION.
       PROGRAM-ID. FTI02YCG.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FICHI
              ASSIGN TO "FICHI"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FICHO
              ASSIGN TO "FICHO"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FICHI
           LABEL RECORD STANDARD
           DATA RECORD E-FICHI.
       01 E-FICHI.                                              
            02 NSOC                     PIC X(05).
            02 NETAB                    PIC X(03).
            02 NJRN                     PIC X(03).
            02 FILLER1                  PIC X(01).
            02 CINTERFACE               PIC X(05).
            02 NPIECE                   PIC X(10).
            02 NATURE                   PIC X(03).
            02 REFDOC                   PIC X(12).
            02 DFTI00                   PIC X(08).
            02 NCOMPTE                  PIC X(06).
            02 NSSCOMPTE                PIC X(06).
            02 NSECTION                 PIC X(06).
            02 NRUBR                    PIC X(06).
            02 NSTEAPP                  PIC X(05).
            02 NTIERSCV                 PIC X(08).
            02 NLETTRAGE                PIC X(10).
            02 LMVT                     PIC X(25).
            02 PMONTMVT-P               PIC X(08).
            02 CMONTMVT                 PIC X(01).
            02 DMVT                     PIC X(08).
            02 NOECS                    PIC X(05).
            02 CATCH                    PIC X(01).
            02 NATCH                    PIC X(06).
            02 DECH                     PIC X(08).
            02 WBAP                     PIC X(01).
            02 CNOMPROG                 PIC X(08).
            02 DTRAITEMENT              PIC X(08).
            02 WCUMUL                   PIC X(01).
            02 DDOC                     PIC X(08).
            02 CDEVISE                  PIC X(03).
            02 FILLER2                  PIC X(12).
            02 CPTSAP                   PIC X(08).
            02 TIERSAP                  PIC X(10).
            02 CCGS                     PIC X(01).
            02 TYPTIERS                 PIC X(01).
            02 CPROFITSAP               PIC X(10).
            02 TAUXTVA-P                PIC X(03).
            02 NATTVA                   PIC X(01).
            02 NATMT                    PIC X(01).
            02 FILLER3                  PIC X(15).
       
       FD  MW-FICHO
           LABEL RECORD STANDARD
           DATA RECORD S-FICHO.  
       	
       01 S-FICHO.                                                    
            02 NSOC                    PIC X(05).
            02 SEPAR01                 PIC X(01).
            02 NETAB                   PIC X(03).
            02 SEPAR02                 PIC X(01).
            02 NJRN                    PIC X(03).
            02 SEPAR03                 PIC X(01).
            02 CINTERFACE              PIC X(05).
            02 SEPAR04                 PIC X(01).
            02 NPIECE                  PIC X(10).
            02 SEPAR05                 PIC X(01).
            02 NATURE                  PIC X(03).
            02 SEPAR06                 PIC X(01).
            02 REFDOC                  PIC X(12).
            02 SEPAR07                 PIC X(01).
            02 DFTI00                  PIC X(08).
            02 SEPAR08                 PIC X(01).
            02 NCOMPTE                 PIC X(06).
            02 SEPAR09                 PIC X(01).
            02 NSSCOMPTE               PIC X(06).
            02 SEPAR10                 PIC X(01).
            02 NSECTION                PIC X(06).
            02 SEPAR11                 PIC X(01).
            02 NRUBR                   PIC X(06).
            02 SEPAR12                 PIC X(01).
            02 NSTEAPP                 PIC X(05).
            02 SEPAR13                 PIC X(01).
            02 NTIERSCV                PIC X(08).
            02 SEPAR14                 PIC X(01).
            02 NLETTRAGE               PIC X(10).
            02 SEPAR15                 PIC X(01).
            02 LMVT                    PIC X(25).
            02 SEPAR16                 PIC X(01).
            02 PMONTMVT-N              PIC X(16).
            02 SEPAR17                 PIC X(01).
            02 CMONTMVT                PIC X(01).
            02 SEPAR18                 PIC X(01).
            02 DMVT                    PIC X(08).
            02 SEPAR19                 PIC X(01).
            02 NOECS                   PIC X(05).
            02 SEPAR20                 PIC X(01).
            02 CATCH                   PIC X(01).
            02 SEPAR21                 PIC X(01).
            02 NATCH                   PIC X(06).
            02 SEPAR22                 PIC X(01).
            02 DECH                    PIC X(08).
            02 SEPAR23                 PIC X(01).
            02 WBAP                    PIC X(01).
            02 SEPAR24                 PIC X(01).
            02 CNOMPROG                PIC X(08).
            02 SEPAR25                 PIC X(01).
            02 DTRAITEMENT             PIC X(08).
            02 SEPAR26                 PIC X(01).
            02 WCUMUL                  PIC X(01).
            02 SEPAR27                 PIC X(01).
            02 DDOC                    PIC X(08).
            02 SEPAR28                 PIC X(01).
            02 CDEVISE                 PIC X(03).
            02 SEPAR29                 PIC X(01).
            02 CPTSAP                  PIC X(08).
            02 SEPAR30                 PIC X(01).
            02 TIERSAP                 PIC X(10).
            02 SEPAR31                 PIC X(01).
            02 CCGS                    PIC X(01).
            02 SEPAR32                 PIC X(01).
            02 TYPTIERS                PIC X(01).
            02 SEPAR33                 PIC X(01).
            02 CPROFITSAP              PIC X(10).
            02 SEPAR34                 PIC X(01).
            02 TAUXTVA-N               PIC X(06).
            02 SEPAR35                 PIC X(01).
            02 NATTVA                  PIC X(01).
            02 SEPAR36                 PIC X(01).
            02 NATMT                   PIC X(01).
            02 SEPAR37                 PIC X(01).
            02 FIL1                    PIC X(271).                     
      *  
       WORKING-STORAGE SECTION.
       77 NBREAD   PIC 9(5) VALUE 0.
       77 NBPUT    PIC 9(5) VALUE 0.
       	   
       01 WTITRE.
          02 ENREG1 PIC X(227).
          02 ENREG2 PIC X(273).
              
       01 WTITR1.
          02 FILLER PIC X(32) VALUE "SOCIETE;NETAB;JOURNAL;INTERFACE;".
          02 FILLER PIC X(28) VALUE "PIECE;NATURE;REFERENCE;DATE ".
          02 FILLER PIC X(27) VALUE "INTERFACE;COMPTE; ;SECTION;".
          02 FILLER PIC X(31) VALUE "RUBRIQUE;STEAPP;TIERS;LETTRAGE;".
          02 FILLER PIC X(26) VALUE "LIBELLE;MONTANT;SENS;DATE ".
          02 FILLER PIC X(26) VALUE "COMPTABLE;ECS;;;ECHEANCE;;".
          02 FILLER PIC X(32) VALUE "PROGRAMME;DATE TRAITEMENT;CUMUL;".
          02 FILLER PIC X(25) VALUE "DATE PIECE;DEVISE;COMPTE;".
                       
       01 WTITR2.
          02 FILLER  PIC X(28) VALUE "TIERS;CCGS;TYPE TIERS;CENTRE".
          02 FILLER  PIC X(24) VALUE " PROFIT;TAUX TVA;NATURE ".
          02 FILLER  PIC X(10) VALUE "TVA;NATMT;".
       		 
       
       01  IO-STATUS            PIC XX.                       
       PROCEDURE DIVISION.
       START-PARA.
           DISPLAY '********************************' 
           DISPLAY '***** DEBUT EZECS   ************' 
           OPEN INPUT MW-FICHI
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
           
           OPEN OUTPUT MW-FICHO
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.

       WRITE-TOP.                                                      
                           
           MOVE WTITR1 TO ENREG1                                     
           MOVE WTITR2 TO ENREG2                                    
           MOVE WTITRE TO S-FICHO                                      
           WRITE S-FICHO                                  
           ADD 1 TO NBPUT  
       
           MOVE ';' TO SEPAR01 SEPAR02 SEPAR03 SEPAR04 SEPAR05 SEPAR06
           SEPAR07 SEPAR08 SEPAR09 SEPAR10 SEPAR11 SEPAR12 SEPAR13
           SEPAR14 SEPAR15 SEPAR16 SEPAR17 SEPAR18 SEPAR19 
           SEPAR20 SEPAR21 SEPAR22 SEPAR23 SEPAR24 SEPAR25 
           SEPAR26 SEPAR27 SEPAR28 SEPAR29 SEPAR30 SEPAR31 
           SEPAR32 SEPAR33 SEPAR34 SEPAR35 SEPAR36 SEPAR37.
        
       READ-LOOP.
           READ MW-FICHI NEXT
              AT END GO TO FIN-REL
           END-READ.
           ADD 1 TO NBREAD.		   
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.
                  
           GO TO READ-LOOP.                                      
                                                                     
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
           DISPLAY 'NBRE LECTURES  FICHI : ' NBREAD                     
           DISPLAY 'NBRE ECRITURES FICHO : ' NBPUT                      
           DISPLAY '***** FIN   EZECS   ************'                   
           DISPLAY '********************************' 
             
           CLOSE MW-FICHI.
           CLOSE MW-FICHO.
       
       MOVE-FIELD-TO-FIELD.
       	   
           MOVE SPACES TO FIL1                                   
           MOVE CORRESPONDING E-FICHI TO S-FICHO                       
           MOVE PMONTMVT-P TO PMONTMVT-N
           MOVE TAUXTVA-P TO TAUXTVA-N
                                                                      
           WRITE S-FICHO                                          
           ADD 1 TO NBPUT                                              
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.
