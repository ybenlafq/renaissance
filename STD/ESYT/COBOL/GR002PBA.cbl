       IDENTIFICATION DIVISION.
       PROGRAM-ID. GR002PBA.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FILEB
              ASSIGN TO "FILEB"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
          02 ANCODIC           PIC X(7).                             
          02 ANREC             PIC X(7).                           
          02 ADREC             PIC X(8).                           
          02 ANCDE             PIC X(7).                            
          02 AQTE              PIC X(3).                        
          02 APRA              PIC X(5).                           
          02 APRMP             PIC X(5).                            
          02 APRMPA            PIC X(5).                            
          02 APRAA             PIC X(5).                          
          02 ANENTCDE          PIC X(5).                            
          02 ACTYPREC          PIC X(2).                          
          02 APERIODE          PIC X(1).                             
          02 ALREFFO           PIC X(20).                             
          02 ALENTCDE          PIC X(20). 	  
         
       FD  MW-FILEB
           LABEL RECORD STANDARD
           DATA RECORD S-FILEB.
       01 S-FILEB.   
          02 BNOMETAT          PIC X(6).  
          02 BCTYREC           PIC X(2).  
          02 BPERIODE          PIC X(1).  
          02 BNENTCDE          PIC X(5).  
          02 BNCODIC           PIC X(7).  
          02 BSEQ              PIC X(2).  
          02 BLENTCDE          PIC X(20). 
          02 BLREFFO           PIC X(20). 
          02 BNCDE             PIC X(7).  
          02 BNREC             PIC X(7).  
          02 BPRA              PIC X(5).  
          02 BPRAA             PIC X(5).  
          02 BPRMP             PIC X(5).  
          02 BPRMPA            PIC X(5).  
          02 BQTE              PIC X(3).  
          02 BDREC             PIC X(8).  
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
          OPEN INPUT MW-FILEA
          IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
          GO TO FIN-ERREUR
          END-IF.

          OPEN OUTPUT MW-FILEB
          IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
          END-IF.  

       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.
                  
           GO TO READ-LOOP.                   

       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
        
             
        CLOSE MW-FILEA.
        CLOSE MW-FILEB.
       
       MOVE-FIELD-TO-FIELD.
       
          MOVE 'IGR016'  TO BNOMETAT            
          MOVE ACTYPREC  TO BCTYREC             
          MOVE APERIODE  TO BPERIODE            
          MOVE ANENTCDE  TO BNENTCDE            
          MOVE ANCODIC   TO BNCODIC             
          MOVE X'0000'   TO BSEQ                
          MOVE ALENTCDE  TO BLENTCDE            
          MOVE ALREFFO   TO BLREFFO             
          MOVE ANCDE     TO BNCDE               
          MOVE ANREC     TO BNREC               
          MOVE APRA      TO BPRA                
          MOVE APRAA     TO BPRAA               
          MOVE APRMP     TO BPRMP               
          MOVE APRMPA    TO BPRMPA              
          MOVE AQTE      TO BQTE                
          MOVE ADREC     TO BDREC
       
          WRITE S-FILEB                                                
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                                          
