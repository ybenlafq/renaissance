       IDENTIFICATION DIVISION.
       PROGRAM-ID. SK010PAQ.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
         ASSIGN TO "FILEA"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-FILEB
         ASSIGN TO "FILEB"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-FILEC
         ASSIGN TO "FILEC"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
      
       DATA DIVISION.
       FILE SECTION.
       FD  MW-FILEA
       LABEL RECORD STANDARD
       DATA RECORD E-FILEA.
       01  E-FILEA.
           02 ANCODICK  PIC X(07).
           02 ACMARQK   PIC X(05).
           02 ALREFO    PIC X(20).
           02 ACFAMK    PIC X(05).
           02 ADCREA    PIC X(08).
           02 ADMAJ     PIC X(08).
           02 ACOPCO    PIC X(03).
           02 ANCODICO  PIC X(07).
           02 ACOPCOD   PIC X(03).
           02 ANCODICD  PIC X(07).
           02 ACSOCO    PIC X(03).
           02 AISO      PIC X(02).
           02 ACMARQO   PIC X(05).
           02 ALMARQO   PIC X(20).
           02 ACFAMO    PIC X(05).
           02 ALFAMO    PIC X(20).
           02 ACSOCD    PIC X(03).
           02 ACMARQD   PIC X(05).
           02 ALMARQD   PIC X(20).
           02 ACFAMD    PIC X(05).
           02 ALFAMD    PIC X(20).
        FD  MW-FILEB
        LABEL RECORD STANDARD
        DATA RECORD E-FILEB.
        01  E-FILEB.
           02 BNCODICK      PIC X(07).
           02 BCOPCO        PIC X(03).
           02 BCFAMD        PIC X(05).
           02 BCFAMS        PIC X(05).
           02 BLFAMS        PIC X(30).
           02 BCPROJ        PIC X(05).

        FD  MW-FILEC
        LABEL RECORD STANDARD
        DATA RECORD S-FILEC.
        01  S-FILEC.
            02 CNCODICK      PIC X(07).
            02 CNCODICO      PIC X(07).
            02 CCOPCO        PIC X(03).
            02 CLREFO        PIC X(20).
            02 CCFAMK        PIC X(05).
            02 CCFAMD        PIC X(05).
            02 CCFAMS        PIC X(05).
            02 CLFAMS        PIC X(30).
            02 CCFAMO        PIC X(05).
            02 CCMARQO       PIC X(05).
            02 CLMARQD       PIC X(20).
            02 CCPROJ        PIC X(05).
            02 CDCREA        PIC X(08).
            02 CDMAJ         PIC X(08).

       WORKING-STORAGE SECTION.
       01  EOF-FILEA PIC 9. 
       01  EOF-FILEB PIC 9. 
       01  IO-STATUS PIC XX.
       
       PROCEDURE DIVISION.
       START-PARA.
          OPEN INPUT MW-FILEA
          PERFORM VERIFY-OPEN.
          
          OPEN INPUT MW-FILEB
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-FILEC
          PERFORM VERIFY-OPEN.	

          READ MW-FILEA NEXT
               AT END MOVE 1 TO EOF-FILEA
               NOT AT END MOVE 0 TO EOF-FILEA
          END-READ.
          
          MOVE '0000000' TO BNCODICK
          PERFORM WITH TEST BEFORE UNTIL EOF-FILEA = 1
                  PERFORM WITH TEST BEFORE UNTIL ANCODICK > BNCODICK
                          OR EOF-FILEB = 1	  
                  READ MW-FILEB NEXT
                       AT END MOVE 1 TO EOF-FILEB
                       NOT AT END MOVE 0 TO EOF-FILEB
                  END-READ
                  END-PERFORM
          MOVE ANCODICK TO CNCODICK 
          MOVE ANCODICO TO CNCODICO 
          MOVE ACOPCO   TO CCOPCO   
          MOVE ALREFO   TO CLREFO   
          MOVE ACFAMK   TO CCFAMK   
          MOVE ACFAMD   TO CCFAMD   
          MOVE ACFAMO   TO CCFAMO   
          MOVE ACMARQO  TO CCMARQO  
          MOVE ALMARQD  TO CLMARQD  
          MOVE ''       TO CCFAMS   
          MOVE ''       TO CLFAMS   
          MOVE ''       TO CCPROJ   
          MOVE ADCREA   TO CDCREA   
          MOVE ADMAJ    TO CDMAJ    
          IF BNCODICK = ANCODICK
             MOVE BCFAMS TO CCFAMS
             MOVE BLFAMS TO CLFAMS
             MOVE BCPROJ TO CCPROJ
          END-IF
          WRITE S-FILEC
          READ MW-FILEA NEXT
               AT END MOVE 1 TO EOF-FILEA
               NOT AT END MOVE 0 TO EOF-FILEA
          END-READ
          END-PERFORM.
       
       FIN-REL.
          DISPLAY "PROGRAM TERMINATED OK".
          PERFORM END-COMMON-DISPLAY.
       
          EXIT PROGRAM.
          STOP RUN.
          
       VERIFY-OPEN.
          IF IO-STATUS NOT = "00"
             DISPLAY "ERROR: OPEN FILE " 
             DISPLAY "IO-STATUS =" IO-STATUS
             PERFORM FIN-ERREUR
          END-IF.
       
       FIN-ERREUR.
          PERFORM END-COMMON-DISPLAY.
       
          EXIT PROGRAM.
          STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
          CLOSE MW-FILEA
          CLOSE MW-FILEB
          CLOSE MW-FILEC.
          
