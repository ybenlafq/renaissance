       IDENTIFICATION DIVISION.
       PROGRAM-ID. GF090PAJ.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-OLDFILE
              ASSIGN TO "OLDFILE"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
      
       SELECT MW-NEWFILE
              ASSIGN TO "NEWFILE"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.

       SELECT MW-SORTIE
              ASSIGN TO "SORTIE"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
      
       DATA DIVISION.
       FILE SECTION.
       FD  MW-OLDFILE
           LABEL RECORD STANDARD
           DATA RECORD E-OLDFILE.
       01  E-OLDFILE.
            02 OLD-TYPE                   PIC X(1).
            02 OLD-NCDE                   PIC X(7).
            02 OLD-NCODIC                 PIC X(7).
            02 OLD-PRA                    PIC X(9).
            02 OLD-SOCDEPOT               PIC X(6).
            02 OLD-DATE                   PIC X(8).
            02 OLD-QTY                    PIC X(7).
            02 OLD-BKG                    PIC X(7).
           
      
       FD  MW-NEWFILE
           LABEL RECORD STANDARD
           DATA RECORD E-NEWFILE.
       01  E-NEWFILE.
           02 NEW-TYPE                  PIC X(1).
           02 NEW-NCDE                  PIC X(7).
           02 NEW-NCODIC                PIC X(7).
           02 NEW-PRA                   PIC X(9).
           02 NEW-SOCDEPOT              PIC X(6).
           02 NEW-DATE                  PIC X(8).
           02 NEW-QTY                   PIC X(7).
           02 NEW-BKG                   PIC X(7).
           
       FD  MW-SORTIE
           LABEL RECORD STANDARD
           DATA RECORD S-SORTIE.
       01  S-SORTIE.  
          02 OV-TYPE                   PIC X(1).
          02 OV-NCDE                   PIC X(7).
          02 OV-NCODIC                 PIC X(7).
          02 OV-PRA                    PIC X(9).
          02 OV-SOCDEPOT               PIC X(6).
          02 OV-DATE                   PIC X(8).
          02 OV-QTY                    PIC X(7).
          02 OV-BKG                    PIC X(7).
          02 NV-TYPE                   PIC X(1).
          02 NV-NCDE                   PIC X(7).
          02 NV-NCODIC                 PIC X(7).
          02 NV-PRA                    PIC X(9).
          02 NV-SOCDEPOT               PIC X(6).
          02 NV-DATE                   PIC X(8).
          02 NV-QTY                    PIC X(7).
          02 NV-BKG                    PIC X(7).

       WORKING-STORAGE SECTION.
       77 CPTOLD   PIC 9(7) VALUE 0.
       77 CPTNEW   PIC 9(7) VALUE 0.
       77 CPTOUT   PIC 9(7) VALUE 0.	
       77 EOF-OLD PIC 9.
       77 EOF-NEW PIC 9.

       01  IO-STATUS            PIC XX.
      
       PROCEDURE DIVISION.
       START-PARA.
           DISPLAY '***TRAITEMENT ECARTS**'
           DISPLAY '***VERSION 28/05/07***'
  
           OPEN INPUT MW-OLDFILE
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.
      
           OPEN INPUT MW-NEWFILE
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.
   
           OPEN OUTPUT MW-SORTIE
           IF IO-STATUS NOT = "00"
             DISPLAY "ERROR OPEN:"
             DISPLAY "IO-STATUS =" IO-STATUS
             GO TO FIN-ERREUR
           END-IF.

       PREM-OLDFILE.
             READ MW-OLDFILE NEXT
               AT END DISPLAY "FICHIER OLD VIDE"
               MOVE 1 TO EOF-OLD
               NOT AT END MOVE 0 TO EOF-OLD
             END-READ.

       PREM-NEWFILE.
             READ MW-NEWFILE NEXT
               AT END DISPLAY "FICHIER NEW VIDE"
               MOVE 1 TO EOF-NEW
               NOT AT END MOVE 0 TO EOF-NEW
             END-READ.

       READ-LOOP.
          PERFORM WITH TEST BEFORE UNTIL EOF-OLD=1 OR EOF-NEW=1
          IF OLD-NCDE < NEW-NCDE 
             PERFORM ALIM-OLD-VALUE
             COMPUTE CPTOUT = CPTOUT + 1
             WRITE S-SORTIE
             COMPUTE CPTOLD = CPTOLD + 1
             READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
             END-READ
          ELSE
          IF OLD-NCDE = NEW-NCDE 
             IF OLD-NCODIC < NEW-NCODIC
                PERFORM ALIM-OLD-VALUE
                COMPUTE CPTOUT = CPTOUT + 1
                WRITE S-SORTIE
                COMPUTE CPTOLD = CPTOLD + 1
                READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                END-READ
             ELSE
             IF OLD-NCODIC = NEW-NCODIC
                IF OLD-SOCDEPOT < NEW-SOCDEPOT
                PERFORM ALIM-OLD-VALUE
                COMPUTE CPTOUT = CPTOUT + 1
                WRITE S-SORTIE
                COMPUTE CPTOLD = CPTOLD + 1
                READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                END-READ
             ELSE
                IF OLD-SOCDEPOT = NEW-SOCDEPOT
                   IF OLD-DATE = NEW-DATE
                      IF OLD-QTY NOT = NEW-QTY
                      PERFORM ALIM-OLD-AND-NEW-VALUE
                      COMPUTE CPTOUT = CPTOUT + 1
                      WRITE S-SORTIE
                      COMPUTE CPTOLD = CPTOLD + 1
                      READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                      END-READ
                      COMPUTE CPTNEW = CPTNEW + 1
                      READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                      END-READ
                   ELSE
                   IF OLD-BKG NOT = NEW-BKG
                      PERFORM ALIM-OLD-AND-NEW-VALUE
                      COMPUTE CPTOUT = CPTOUT + 1
                      WRITE S-SORTIE
                      COMPUTE CPTOLD = CPTOLD + 1
                      READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                      END-READ
                      COMPUTE CPTNEW = CPTNEW + 1
                      READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                      END-READ
                   ELSE
                      IF OLD-PRA NOT =  NEW-PRA
                         PERFORM ALIM-OLD-AND-NEW-VALUE
                         COMPUTE CPTOUT = CPTOUT + 1
                         WRITE S-SORTIE
                         COMPUTE CPTOLD = CPTOLD + 1
                         READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                         END-READ
                         COMPUTE CPTNEW = CPTNEW + 1
                         READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                         END-READ
                      ELSE
                      IF OLD-TYPE NOT = NEW-TYPE
                         PERFORM ALIM-OLD-AND-NEW-VALUE
                         COMPUTE CPTOUT = CPTOUT + 1
                         WRITE S-SORTIE
                      END-IF
                      COMPUTE CPTOLD = CPTOLD + 1
                      READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                      END-READ
                      COMPUTE CPTNEW = CPTNEW + 1
                      READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                      END-READ
                      END-IF
                      END-IF
                      END-IF
                      ELSE
                        PERFORM ALIM-OLD-AND-NEW-VALUE
                        COMPUTE CPTOUT = CPTOUT + 1
                        WRITE S-SORTIE
                        COMPUTE CPTOLD = CPTOLD + 1
                        READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                        END-READ
                        COMPUTE CPTNEW = CPTNEW + 1
                        READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                        END-READ
                      END-IF
                      ELSE
                        PERFORM ALIM-NEW-VALUE
                        COMPUTE CPTOUT = CPTOUT + 1
                        WRITE S-SORTIE
                        COMPUTE CPTNEW = CPTNEW + 1
                        READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                        END-READ
                      END-IF
                      END-IF
                      ELSE
                        PERFORM ALIM-NEW-VALUE
                        COMPUTE CPTOUT = CPTOUT + 1
                        WRITE S-SORTIE
                        COMPUTE CPTNEW = CPTNEW + 1
                        READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                        END-READ
                      END-IF
                      END-IF
                      ELSE
                        PERFORM ALIM-NEW-VALUE
                        COMPUTE CPTOUT = CPTOUT + 1
                        WRITE S-SORTIE
                        COMPUTE CPTNEW = CPTNEW + 1
                        READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                        END-READ
                      END-IF
                      END-IF
              END-PERFORM

              PERFORM WITH TEST BEFORE UNTIL EOF-OLD=1
                  PERFORM ALIM-OLD-VALUE
                  COMPUTE CPTOUT = CPTOUT + 1
                  WRITE S-SORTIE
                  COMPUTE CPTOLD = CPTOLD + 1
                  READ MW-OLDFILE AT END MOVE 1 TO EOF-OLD
                  END-READ
              END-PERFORM
              PERFORM WITH TEST BEFORE UNTIL EOF-NEW=1
                  PERFORM ALIM-NEW-VALUE
                  COMPUTE CPTOUT = CPTOUT + 1
                  WRITE S-SORTIE
                  COMPUTE CPTNEW = CPTNEW + 1
                  READ MW-NEWFILE AT END MOVE 1 TO EOF-NEW
                  END-READ
              END-PERFORM.
              GO TO FIN-REL.

       ALIM-OLD-VALUE. 
            MOVE OLD-TYPE           TO OV-TYPE
            MOVE OLD-NCDE           TO OV-NCDE
            MOVE OLD-NCODIC         TO OV-NCODIC
            MOVE OLD-PRA            TO OV-PRA
            MOVE OLD-SOCDEPOT       TO OV-SOCDEPOT
            MOVE OLD-DATE           TO OV-DATE
            MOVE OLD-QTY            TO OV-QTY
            MOVE OLD-BKG            TO OV-BKG
            MOVE SPACES             TO NV-TYPE
            MOVE SPACES             TO NV-NCDE
            MOVE SPACES             TO NV-NCODIC
            MOVE SPACES             TO NV-PRA
            MOVE SPACES             TO NV-SOCDEPOT
            MOVE SPACES             TO NV-DATE
            MOVE SPACES             TO NV-QTY
            MOVE SPACES             TO NV-BKG.

       ALIM-NEW-VALUE.
            MOVE  SPACES             TO OV-TYPE
            MOVE  SPACES             TO OV-NCDE
            MOVE  SPACES             TO OV-NCODIC
            MOVE  SPACES             TO OV-PRA
            MOVE  SPACES             TO OV-SOCDEPOT
            MOVE  SPACES             TO OV-DATE
            MOVE  SPACES             TO OV-QTY
            MOVE  SPACES             TO OV-BKG
            MOVE  NEW-TYPE           TO NV-TYPE
            MOVE  NEW-NCDE           TO NV-NCDE
            MOVE  NEW-NCODIC         TO NV-NCODIC
            MOVE  NEW-PRA            TO NV-PRA
            MOVE  NEW-SOCDEPOT       TO NV-SOCDEPOT
            MOVE  NEW-DATE           TO NV-DATE
            MOVE  NEW-QTY            TO NV-QTY
            MOVE  NEW-BKG            TO NV-BKG.

       ALIM-OLD-AND-NEW-VALUE. 
            MOVE  OLD-TYPE           TO OV-TYPE
            MOVE  OLD-NCDE           TO OV-NCDE
            MOVE  OLD-NCODIC         TO OV-NCODIC
            MOVE  OLD-PRA            TO OV-PRA
            MOVE  OLD-SOCDEPOT       TO OV-SOCDEPOT
            MOVE  OLD-DATE           TO OV-DATE
            MOVE  OLD-QTY            TO OV-QTY
            MOVE  OLD-BKG            TO OV-BKG
            MOVE  NEW-TYPE           TO NV-TYPE
            MOVE  NEW-NCDE           TO NV-NCDE
            MOVE  NEW-NCODIC         TO NV-NCODIC
            MOVE  NEW-PRA            TO NV-PRA
            MOVE  NEW-SOCDEPOT       TO NV-SOCDEPOT
            MOVE  NEW-DATE           TO NV-DATE
            MOVE  NEW-QTY            TO NV-QTY
            MOVE  NEW-BKG            TO NV-BKG.
      
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK"
           PERFORM END-COMMON-DISPLAY.
      
           EXIT PROGRAM.
           STOP RUN.
      
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.

           EXIT PROGRAM.
           STOP RUN RETURNING 16.

       END-COMMON-DISPLAY.
            DISPLAY '**CPT OLD**' CPTOLD
            DISPLAY '**CPT NEW**' CPTNEW
            DISPLAY '**CPT OUT**' CPTOUT
            DISPLAY '**FIN TRAITEMENT ECARTS**'
      
            CLOSE MW-OLDFILE.
            CLOSE MW-NEWFILE.
            CLOSE MW-SORTIE.
