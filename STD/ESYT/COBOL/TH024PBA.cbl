       IDENTIFICATION DIVISION.
       PROGRAM-ID. TH024PBA.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
         ASSIGN TO "FILEA"
         ORGANIZATION LINE SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-FILEB
         ASSIGN TO "FILEB"
         ORGANIZATION LINE SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       SELECT MW-FILEC
         ASSIGN TO "FILEC"
         ORGANIZATION IS  LINE SEQUENTIAL
         ACCESS IS SEQUENTIAL
         FILE STATUS IS IO-STATUS.
         
       DATA DIVISION.
       FILE SECTION.
       FD  MW-FILEA
       LABEL RECORD STANDARD
       DATA RECORD E-FILEA.
       01  E-FILEA.
           02 FAENR.
              04 FA PIC X(7).
              04 FILLER  PIC X(14).
  
        FD  MW-FILEB
        LABEL RECORD STANDARD
        DATA RECORD E-FILEB.
        01  E-FILEB.
           02 FBENR.
              04 FB   PIC X(7).
              04 FILLER PIC X(85).

       FD  MW-FILEC
       LABEL RECORD STANDARD
       DATA RECORD S-FILEC.
       01  S-FILEC.
           02 FCENR     PIC X(92).
            
       WORKING-STORAGE SECTION.
       01  EOF-FILEA PIC 9. 
       01  EOF-FILEB PIC 9. 
       01  IO-STATUS PIC XX.
       
       PROCEDURE DIVISION.
       START-PARA.
          OPEN INPUT MW-FILEA
          PERFORM VERIFY-OPEN.
          
          OPEN INPUT MW-FILEB
          PERFORM VERIFY-OPEN.
          
          OPEN OUTPUT MW-FILEC
          PERFORM VERIFY-OPEN.	

          READ MW-FILEA NEXT
               AT END MOVE 1 TO EOF-FILEA
               NOT AT END MOVE 0 TO EOF-FILEA
          END-READ.
          READ MW-FILEB NEXT
               AT END MOVE 1 TO EOF-FILEB
               NOT AT END MOVE 0 TO EOF-FILEB
          END-READ.
          IF FB = 'NCODIC;'
             MOVE FBENR TO FCENR
             WRITE S-FILEC
             MOVE LOW-VALUE TO FB
          END-IF
          
          PERFORM WITH TEST BEFORE UNTIL EOF-FILEA = 1 AND EOF-FILEB = 1
                  PERFORM WITH TEST BEFORE UNTIL FA < FB
          	  IF FA = FB
                     WRITE S-FILEC FROM E-FILEB
                  END-IF
                  READ MW-FILEB NEXT
                       AT END MOVE 1 TO EOF-FILEB
                  END-READ
                  IF EOF-FILEB = 1
                     GO TO FIN-REL
                  END-IF
                  END-PERFORM
            READ MW-FILEA NEXT
                 AT END MOVE 1 TO EOF-FILEA EOF-FILEB
            END-READ

           END-PERFORM.	
       
       FIN-REL.
          DISPLAY "PROGRAM TERMINATED OK".
          PERFORM END-COMMON-DISPLAY.
       
          EXIT PROGRAM.
          STOP RUN.
          
       VERIFY-OPEN.
          IF IO-STATUS NOT = "00"
             DISPLAY "ERROR: OPEN FILE " 
             DISPLAY "IO-STATUS =" IO-STATUS
             PERFORM FIN-ERREUR
          END-IF.
       
       FIN-ERREUR.
          PERFORM END-COMMON-DISPLAY.
       
          EXIT PROGRAM.
          STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
          CLOSE MW-FILEA
          CLOSE MW-FILEB
          CLOSE MW-FILEC.
