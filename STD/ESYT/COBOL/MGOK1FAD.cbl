       IDENTIFICATION DIVISION.
       PROGRAM-ID. MGOK1FAD.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-RTGA10F
              ASSIGN TO "RTGA10F"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-RTGA10TF
              ASSIGN TO "RTGA10TF"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.

       SELECT WORK
              ASSIGN TO "WORK"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-MGOK1MEL
              ASSIGN TO "MGOK1MEL"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-MGOS
              ASSIGN TO "MGOS"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-MGOK040F
              ASSIGN TO "MGOK040F"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-MGOK907P
              ASSIGN TO "MGOK907P"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
        
       SELECT MW-MGOK908X
              ASSIGN TO "MGOK908X"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
        
       SELECT MW-MGOK916O
              ASSIGN TO "MGOK916O"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
        
       SELECT MW-MGOK961L
              ASSIGN TO "MGOK961L"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.	  
         
       SELECT MW-MGOK945Y
              ASSIGN TO "MGOK945Y"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.	 
       
       SELECT MW-MGOK989M
              ASSIGN TO "MGOK989M"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.	
       
       SELECT MW-MGOK991D
              ASSIGN TO "MGOK991D"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.	
       DATA DIVISION.
       FILE SECTION.
       FD  MW-RTGA10F
           LABEL RECORD STANDARD
           DATA RECORD E-RTGA10F.
       01  E-RTGA10F.
           02 TOUT.
              04 FILI       PIC X(3).
              04 MAG        PIC X(3).
              04 FILLER     PIC X(34).
              04 HEURE      PIC X(5).
          
       FD  MW-RTGA10TF
           LABEL RECORD STANDARD
           DATA RECORD E-RTGA10TF.
       01  E-RTGA10TF.
           02 TOUT.
              04 FILI       PIC X(3).
              04 MAG        PIC X(3).
              04 FILLER     PIC X(34).
              04 HEURE      PIC X(5).

       SD  WORK
           LABEL RECORD STANDARD
           DATA RECORD E-WORK.
       01  E-WORK.
           02 TOUT.
              04 FILI         PIC X(3).
              04 MAG          PIC X(3).
              04 FILLER       PIC X(34).
              04 HEURE-W      PIC X(5).

       FD  MW-MGOK1MEL
           LABEL RECORD STANDARD
           DATA RECORD E-MGOK1MEL.
       01  E-MGOK1MEL.
           02 LIGNE.
              04 FILLER pic X(34).
              04 COL30A39 PIC X(10).
              04 COL46A48 PIC X(3).
         
       FD MW-MGOS
       	LABEL RECORD STANDARD
       	DATA RECORD E-MGOK1MEL.
       01  E-MGOS.
           02 LIGNES.
              04 FILLER PIC X(41).
              04 CPW    PIC X(3).       
              04 CPI    PIC X(3).       
            	04 CPX    PIC X(3).       
            	04 CPO    PIC X(3).       
              04 CPL    PIC X(3).       
              04 CPM    PIC X(3).       
              04 CPY    PIC X(3).       
              04 CPD    PIC X(3).   
              04 FILLER PIC X(15).				

       FD MW-MGOK040F
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK040F.
          01 S-MGOK040F.
             02 TOUT          PIC X(45).
       FD MW-MGOK907P
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK907P.
          01 S-MGOK907P.
             02 TOUT          PIC X(45).
       FD MW-MGOK908X
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK908X.
          01 S-MGOK908X.
             02 TOUT          PIC X(45).
       FD MW-MGOK916O
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK916O.
          01 S-MGOK916O.
             02 TOUT          PIC X(45).
       FD MW-MGOK961L
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK961L.
          01 S-MGOK961L.
             02 TOUT          PIC X(45).
       FD MW-MGOK945Y
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK945Y.
          01 S-MGOK945Y.
             02 TOUT          PIC X(45).
       FD MW-MGOK989M
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK989M.
          01 S-MGOK989M.
             02 TOUT          PIC X(45).
       FD MW-MGOK991D
          LABEL RECORD STANDARD
          DATA RECORD S-MGOK991D.
          01 S-MGOK991D.
             02 TOUT          PIC X(45).
       WORKING-STORAGE SECTION.
       01	CPT040         PIC 9(03).
       01	CPT907         PIC 9(03).
       01	CPT908         PIC 9(03).
       01	CPT916         PIC 9(03).
       01	CPT961         PIC 9(03).
       01	CPT945         PIC 9(03).
       01	CPT989         PIC 9(03).
       01	CPT991         PIC 9(03).
       		
       01  IO-STATUS            PIC XX.
       
       PROCEDURE DIVISION.
       START-PARA.
      *    SORT RTGA10F TO RTGA10TF USING (HEURE)
       		
       		 OPEN INPUT MW-MGOK1MEL
           PERFORM VERIFY-OPEN.
       		   
           OPEN OUTPUT MW-MGOS
           PERFORM VERIFY-OPEN.
       		   
       		 OPEN OUTPUT MW-MGOK040F
           PERFORM VERIFY-OPEN.
       		   
       		 OPEN OUTPUT MW-MGOK907P
           PERFORM VERIFY-OPEN.
       		   
       		 OPEN OUTPUT MW-MGOK908X
           PERFORM VERIFY-OPEN.
       		   
       		 OPEN OUTPUT MW-MGOK916O
           PERFORM VERIFY-OPEN.
       		   
       		 OPEN OUTPUT MW-MGOK961L
           PERFORM VERIFY-OPEN.
       		   
       		 OPEN OUTPUT MW-MGOK989M
           PERFORM VERIFY-OPEN.
       		   
       		 OPEN OUTPUT MW-MGOK991D
           PERFORM VERIFY-OPEN.

       SORT-FILE.
           SORT WORK ON ASCENDING KEY HEURE-W 
           USING MW-RTGA10F GIVING MW-RTGA10TF.

           OPEN INPUT MW-RTGA10TF
           PERFORM VERIFY-OPEN.
       
       READ-LOOP.
           READ MW-RTGA10TF NEXT
                AT END GO TO FIN-REL
           END-READ.
       
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.
       
           GO TO READ-LOOP.
       
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN.
       		   
       VERIFY-OPEN.
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR: OPEN FILE " 
              DISPLAY "IO-STATUS =" IO-STATUS
              PERFORM FIN-ERREUR
           END-IF.
       		   
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
            CLOSE MW-RTGA10F
       			CLOSE MW-RTGA10TF
       			CLOSE MW-MGOK1MEL
       			CLOSE MW-MGOS
       			CLOSE MW-MGOK040F
       			CLOSE MW-MGOK907P
       			CLOSE MW-MGOK908X 
       			CLOSE MW-MGOK916O 
       			CLOSE MW-MGOK961L
       			CLOSE MW-MGOK945Y 
       			CLOSE MW-MGOK989M  
       			CLOSE MW-MGOK991D.
               
       
       MOVE-FIELD-TO-FIELD.
       			IF MAG OF MW-RTGA10TF  = '040'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK040F
          		 WRITE S-MGOK040F
       				 COMPUTE CPT040 = CPT040 + 1
       			END-IF
       			IF FILI OF MW-RTGA10TF = '961'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK961L 
          		 WRITE S-MGOK961L
       				 COMPUTE CPT961 = CPT961 + 1
       			END-IF
       			IF FILI OF MW-RTGA10TF = '989'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK989M 
       				 WRITE S-MGOK989M
       				 COMPUTE CPT989 = CPT989 + 1
       			END-IF
       			IF FILI OF MW-RTGA10TF = '916'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK916O 
           		 WRITE S-MGOK916O
       				 COMPUTE CPT916 = CPT916 + 1
       			END-IF
       			IF FILI OF MW-RTGA10TF = '907'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK907P 
           	   WRITE S-MGOK907P
       				 COMPUTE CPT907 = CPT907 + 1
       			END-IF
       			IF FILI OF MW-RTGA10TF = '908'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK908X 
       			   WRITE S-MGOK908X
       				 COMPUTE CPT908 = CPT908 + 1
       			END-IF
       			IF FILI OF MW-RTGA10TF = '945'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK945Y  
       			   WRITE S-MGOK945Y
       				 COMPUTE CPT945 = CPT945 + 1
       			END-IF
       			IF FILI OF MW-RTGA10TF = '991'  
               MOVE CORRESPONDING E-RTGA10TF TO S-MGOK991D 
           		 WRITE S-MGOK991D
       				 COMPUTE CPT991 = CPT991 + 1
       			END-IF
       			
       			IF COL30A39 = 'MAGOUVERTS'
       				MOVE LIGNE TO LIGNES
       			ELSE
       				MOVE LIGNE TO LIGNES
       				MOVE CPT040 TO CPW
       				MOVE CPT907 TO CPI 
       				MOVE CPT908 TO CPX 
       				MOVE CPT916 TO CPO
       				MOVE CPT961 TO CPL
       				MOVE CPT989 TO CPM 
       				MOVE CPT945 TO CPY 
       				MOVE CPT991 TO CPD 
       			END-IF
      * Ecriture du script pour courriel vers meteomag@darty.fr
       			WRITE E-MGOS
                  .
       E-MOVE-FIELD-TO-FIELD.
             EXIT.
