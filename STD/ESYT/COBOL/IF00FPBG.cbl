       IDENTIFICATION DIVISION.
       PROGRAM-ID. IF00FPBG.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FICHI
              ASSIGN TO "FICHI"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FICHO
              ASSIGN TO "FICHO"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FICHI
           LABEL RECORD STANDARD
           DATA RECORD E-FICHI.
       01 E-FICHI.                                                   
         02 CORGANISME      PIC X(3).                         
         02 DRECEPT         PIC X(8).                             
         02 HRECEPT         PIC X(4).                              
         02 CMODPAI         PIC X(3).                          
         02 NSOCCOMPT       PIC X(3).                         
         02 NETAB           PIC X(3).                            
         02 NSOCIETE        PIC X(3).                           
         02 NLIEU           PIC X(3).                           
         02 NIDENT          PIC X(20).                          
         02 DOPER           PIC X(8).                       
         02 DFINANCE        PIC X(8).                          
         02 DREMISE         PIC X(8).                           
         02 NREMISE         PIC X(6).                         
         02 NDOSSIER        PIC X(8).                         
         02 NOMCLIENT       PIC X(15).                      
         02 REFINTERNE      PIC X(9).                       
         02 NFACTURES       PIC S9(7)  COMP-3.                     
         02 NPORTEUR        PIC X(19).                            
         02 CTYPMVT         PIC X(1).                            
         02 MTBRUT          PIC S9(11) COMP-3.                       
         02 MTCOM           PIC S9(9)  COMP-3.                         
         02 MTNET           PIC S9(11) COMP-3.                          
         02 TYPANO          PIC X(1).                                   
         02 CODANO          PIC X(2).                                   
         02 CTYPCTA         PIC X(3).                                   
         02 CTYPMONT        PIC X(5).                                   
         02 CBANQUE         PIC X(2).                                   
         02 WNSEQ           PIC S9(5)  COMP-3.                          
         02 CDEVISE         PIC X(3).                                   
         02 FIN             PIC X(20).                                  
      *        
       FD  MW-FICHO
           LABEL RECORD STANDARD
           DATA RECORD S-FICHO.  
       	
       01 S-FICHO.                                               
         02 CORGANISME      PIC X(3).                                
         02 SEP01           PIC X(1).                                
         02 DRECEPT         PIC X(8).                              
         02 SEP02           PIC X(1).                             
         02 HRECEPT-O       PIC X(4).                           
         02 SEP03           PIC X(1).                         
         02 CMODPAI         PIC X(3).                            
         02 SEP04           PIC X(1).                            
         02 NSOCCOMPT       PIC X(3).                           
         02 SEP05           PIC X(1).                           
         02 NETAB           PIC X(3).                           
         02 SEP06           PIC X(1).                          
         02 NSOCIETE        PIC X(3).                           
         02 SEP07           PIC X(1).                          
      *                                   
         02 NLIEU           PIC X(3).                             
         02 SEP08           PIC X(1).                          
         02 NIDENT          PIC X(20).                              
         02 SEP09           PIC X(1).                              
         02 DOPER           PIC X(8).                                
         02 SEP10           PIC X(1).                          
      *                             
         02 DFINANCE        PIC X(8).                            
         02 SEP11           PIC X(1).                           
         02 DREMISE         PIC X(8).                           
         02 SEP12           PIC X(1).                            
         02 NREMISE         PIC X(6).                               
         02 SEP13           PIC X(1).                             
         02 NDOSSIER        PIC X(8).                            
         02 SEP14           PIC X(1).                               
         02 NOMCLIENT       PIC X(15).                             
         02 SEP15           PIC X(1).                                
         02 REFINTERNE      PIC X(9).                            
         02 SEP16           PIC X(1).                             
         02 NFACTURES-O     PIC X(7).                            
         02 SEP17           PIC X(1).                            
         02 NPORTEUR        PIC X(19).                     
         02 SEP18           PIC X(1).                              
         02 CTYPMVT         PIC X(1).                            
         02 SEP19           PIC X(1).                              
         02 SIGN-BRUT       PIC X(1).                          
         02 SEP31           PIC X(1).                               
         02 MTBRUT-O        PIC S9(11).                             
         02 SEP20           PIC X(1).                              
         02 SIGN-COM        PIC X(1).                                  
         02 SEP32           PIC X(1).                                
         02 MTCOM-O         PIC S9(9).                                
         02 SEP21           PIC X(1).                                   
         02 SIGN-NET        PIC X(1).                                   
         02 SEP33           PIC X(1).                                  
      *                                                              
         02 MTNET-O         PIC S9(11).                            
         02 SEP22           PIC X(1).                         
         02 TYPANO          PIC X(1).                            
         02 SEP23           PIC X(1).                         
         02 CODANO          PIC X(2).                           
         02 SEP24           PIC X(1).                          
         02 CTYPCTA         PIC X(3).                            
         02 SEP25           PIC X(1).                           
         02 CTYPMONT        PIC X(5).                          
         02 SEP26           PIC X(1).                            
         02 CBANQUE         PIC X(2).                          
         02 SEP27           PIC X(1).                          
         02 WNSEQ-O         PIC S9(5).                            
         02 SEP28           PIC X(1).                            
         02 CDEVISE         PIC X(3).                                
         02 SEP29           PIC X(1).                            
         02 FIN             PIC X(29).                             
         02 SEP30           PIC X(1).                                
         02 FIL1            PIC X(244).                             
      *          
       WORKING-STORAGE SECTION.
       77 NBREAD   PIC 9(5) VALUE 0.
       77 NBPUT    PIC 9(5) VALUE 0.
       77 NBBLANC  PIC 9(5) VALUE 0.
       	   
       01 WCORG    PIC X(3).
       
       01 WTITRE.
          02 ENREG1 PIC X(226).
          02 ENREG2 PIC X(274).
              
       01 WTITR1.
          02 FILLER PIC X(29) VALUE "CODE APPLICATION;DATE RECEPT;".
          02 FILLER PIC X(30) VALUE "HEURE RECEPT;MODE PAIEMENT;STE".
          02 FILLER PIC X(26) VALUE " CPTABLE;ETABLISSEMENT;STE".
          02 FILLER PIC X(28) VALUE " GESTION; LIEU GESTION;CODE ".
          02 FILLER PIC X(29) VALUE "COMMERCANT;DTE OPERATION;DTE ".
          02 FILLER PIC X(26) VALUE "FINANCEMENT;DTE REMISE;NO ".
          02 FILLER PIC X(29) VALUE "REMISE;NO DOSSIER;NOM CLIENT;".
          02 FILLER PIC X(29) VALUE "REF INTERNE;NBRE FACTURETTES;".
                       
       01 WTITR2.
          02 FILLER  PIC X(26) VALUE "NO CARTE PORTEUR;TYPE MVT;".
          02 FILLER  PIC X(23) VALUE "SIGNE;MT BRUT;SIGNE;MT ".
          02 FILLER  PIC X(29) VALUE "COMMISSIONS;SIGNE;MT NET;TYPE".
          02 FILLER  PIC X(30) VALUE " ANO;CODE ANO;TYPE CPTABILITE;".
          02 FILLER  PIC X(28) VALUE "TYPE MONTANT;CODE BANQUE;NO ".
       	  02 FILLER  PIC X(29) VALUE "SEQUEN;CODE DEVISE;FIN ENREG;".
       
       01  IO-STATUS            PIC XX.                         
                                                                       
      * WTITR2   W     200 A VALUE 'NO CARTE PORTEUR;TYPE MVT;MT BRUT;+
      * MT COMMISSIONS;MT NET;TYPE ANO;CODE ANO;TYPE CPTABILITE;+       
      * TYPE MONTANT;CODE BANQUE;NO SEQUEN;CODE DEVISE;FIN ENREG;'     
      *   
       PROCEDURE DIVISION.
       START-PARA.
           DISPLAY '********************************'
           DISPLAY '***** DEBUT EZFIF00 ************'
           OPEN INPUT MW-FICHI
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
             
           OPEN OUTPUT MW-FICHO
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.

       WRITE-TOP.                                            
           MOVE WTITR1 TO ENREG1                                  
           MOVE WTITR2 TO ENREG2                                    
           MOVE WTITRE TO S-FICHO                                 
           WRITE S-FICHO                                              
           ADD 1 TO NBPUT  
           MOVE ';' TO SEP01 SEP02 SEP03 SEP04 SEP05 SEP06
           SEP07 SEP08 SEP09 SEP10 SEP11 SEP12 SEP13 SEP14
           SEP15 SEP16 SEP17 SEP18 SEP19 SEP20 SEP21 SEP22
           SEP23 SEP24 SEP25 SEP26 SEP27 SEP28 SEP29 SEP30 
           SEP31 SEP32 SEP33.
        
       READ-LOOP.
          READ MW-FICHI NEXT
          AT END GO TO FIN-REL
          END-READ.
          ADD 1 TO NBREAD.		   
                  
          PERFORM MOVE-FIELD-TO-FIELD
          THRU E-MOVE-FIELD-TO-FIELD.
                  
          GO TO READ-LOOP.                                          
                                                                      
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
           DISPLAY 'NBRE LECTURES  FICHI : ' NBREAD            
           DISPLAY 'NBRE ECRITURES FICHO : ' NBPUT             
           DISPLAY 'NBRE ENREG A SPACES  : ' NBBLANC          
           DISPLAY '***** FIN   EZFIF00 ************'          
           DISPLAY '********************************'
                    
           CLOSE MW-FICHI.
           CLOSE MW-FICHO.
       
       MOVE-FIELD-TO-FIELD.
           MOVE CORGANISME OF MW-FICHI TO WCORG
           IF WCORG NOT = SPACES                                      
      *  DISPLAY 'MAR NBREAD : ' NBREAD                             
              MOVE SPACES TO FIL1                                   
              MOVE CORRESPONDING E-FICHI TO S-FICHO                  
              MOVE NFACTURES TO NFACTURES-O                        
              MOVE '+'    TO SIGN-BRUT                               
              MOVE '+'    TO SIGN-COM                              
              MOVE '+'    TO SIGN-NET                              
              MOVE MTBRUT TO MTBRUT-O                               
              MOVE MTCOM  TO MTCOM-O                                 
              MOVE MTNET  TO MTNET-O                                  
              MOVE WNSEQ  TO WNSEQ-O                                 
              IF MTBRUT < 0                                         
                 MOVE '-' TO SIGN-BRUT                             
              END-IF                                               
              IF MTCOM  < 0                                         
                 MOVE '-' TO SIGN-COM                               
              END-IF                                              
              IF MTNET  < 0                                       
                 MOVE '-' TO SIGN-NET                            
              END-IF                                             
              MOVE ' ' TO HRECEPT-O  
                                                           
              WRITE S-FICHO                                      
              ADD 1 TO NBPUT                                     
           ELSE                                                     
              ADD 1 TO NBBLANC                                  
           END-IF                                                
       		.
       E-MOVE-FIELD-TO-FIELD.
           EXIT.
