       IDENTIFICATION DIVISION.
       PROGRAM-ID. FTCREGAA.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-SORTIE
              ASSIGN TO "SORTIE"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
         02 CINTERFACE PIC X(5).      
         02 NPIECEA1   PIC X(2).     
         02 NPIECEA2   PIC X(8).     
         02 NSEQN      PIC S9(5).    
         02 FILLER1    PIC X(41).    
         02 TVAN       PIC S9(4).    
         02 FILLER2    PIC X(1).     
         02 MONTANTN   PIC S9(14).   
         02 FILLER3    PIC X(72).    
         02 DOPERATION PIC X(8).     
         02 FILLER4    PIC X(218).   
         02 REFDOC     PIC X(5).     
         02 FILLER5    PIC X(25).    
         
       FD  MW-SORTIE
           LABEL RECORD STANDARD
           DATA RECORD S-SORTIE.
       01 S-SORTIE.   
         02 CINTERFACE PIC X(5).   
         02 NPIECEA1   PIC X(2).  
         02 NPIECEA2   PIC X(8).  
         02 NSEQP      PIC S9(5) COMP-3.
         02 FILLER1    PIC X(41).   
         02 TVAP       PIC S9(4) COMP-3. 
         02 FILLER2    PIC X(1).  
         02 MONTANTP   PIC S9(14) COMP-3. 
         02 FILLER3    PIC X(72).
         02 DOPERATION PIC X(8). 
         02 FILLER4    PIC X(218). 
         02 REFDOC     PIC X(5).  
         02 FILLER5    PIC X(25). 
         
       WORKING-STORAGE SECTION.
       01  S-INDEX              PIC 9(5) VALUE 0.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
            
           OPEN INPUT MW-FILEA
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
           
           OPEN OUTPUT MW-SORTIE
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.  
       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.     
       	
       	GO TO READ-LOOP.
       
       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
             
          CLOSE MW-FILEA.
          CLOSE MW-SORTIE.
       
       MOVE-FIELD-TO-FIELD.
                                                                    
                                                                        
        IF CINTERFACE OF MW-FILEA = 'RGTNF'         
           COMPUTE S-INDEX    = S-INDEX + 1 
           MOVE S-INDEX      TO NSEQN    
           MOVE '00'       TO NPIECEA1 OF MW-FILEA
           MOVE DOPERATION OF MW-FILEA TO NPIECEA2 OF MW-FILEA
           MOVE 'VIRNF'    TO REFDOC OF MW-FILEA
        END-IF                       
        MOVE CORRESPONDING E-FILEA TO S-SORTIE 
        MOVE NSEQN    TO NSEQP    
        MOVE TVAN     TO TVAP     
        MOVE MONTANTN TO MONTANTP 
                                                           
        WRITE S-SORTIE                                          
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.      
