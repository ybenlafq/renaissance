       IDENTIFICATION DIVISION.
       PROGRAM-ID. GF057PAA.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-SORTIE
              ASSIGN TO "SORTIE"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD  MW-FILEA
           LABEL RECORD STANDARD
           DATA RECORD E-FILEA.
       01 E-FILEA.      
        02 AFIL1        PIC X(21). 
        02 AQSTOCK      PIC S9(5). 
        02 AQRES        PIC S9(5). 
        02 ADSYST       PIC S9(13).
              	    
       FD  MW-SORTIE
           LABEL RECORD STANDARD
           DATA RECORD S-SORTIE.
       01 S-SORTIE.   
          02 BFIL1       PIC X(21).        
          02 BQSTOCK     PIC S9(5) COMP-3. 
          02 BQRES       PIC S9(5) COMP-3. 
          02 BDSYST      PIC S9(13) COMP-3.
       	  
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.  
         
       PROCEDURE DIVISION.
       START-PARA.
            
           OPEN INPUT MW-FILEA
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
          
           OPEN OUTPUT MW-SORTIE
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.  

       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.                   

           GO TO READ-LOOP.

       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
        
             
        CLOSE MW-FILEA.
        CLOSE MW-SORTIE.
       
       MOVE-FIELD-TO-FIELD.
       
          MOVE AFIL1   TO  BFIL1                                    
          MOVE AQSTOCK TO  BQSTOCK                                  
          MOVE AQRES   TO  BQRES                              
          MOVE ADSYST  TO  BDSYST     
       
        WRITE S-SORTIE                                        
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                     
