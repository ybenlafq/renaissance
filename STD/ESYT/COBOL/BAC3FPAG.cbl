       IDENTIFICATION DIVISION.
       PROGRAM-ID. BAC3FPAG.
       ENVIRONMENT DIVISION.
       OBJECT-COMPUTER.
       PROGRAM COLLATING SEQUENCE IS MWEBCDIC.
       SPECIAL-NAMES.
        COPY MW-COLSEQ.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       SELECT MW-FILEA
              ASSIGN TO "FILEA"
              ORGANIZATION IS LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       SELECT MW-FILEB
              ASSIGN TO "SORTIE"
              ORGANIZATION IS  LINE SEQUENTIAL
              ACCESS IS SEQUENTIAL
              FILE STATUS IS IO-STATUS.
       
       DATA DIVISION.    
       FILE SECTION.
       FD MW-FILEA
          LABEL RECORD STANDARD
          DATA RECORD E-FILEA.
       01 E-FILEA.                                              
         02 FIL1        PIC X(17).                             
         02 Z1P         PIC S9(4) COMP-3. 
         02 Z2P         PIC S9(4) COMP-3.                           
         02 Z3P         PIC S9(4) COMP-3.                          
         02 Z4P         PIC S9(7) COMP-3.                      
         02 Z5P         PIC S9(7) COMP-3.                            
         02 Z6P         PIC S9(7) COMP-3.                 
         02 Z7P         PIC S9(4) COMP-3.                      
         02 Z8P         PIC S9(4) COMP-3.                            
         02 Z9P         PIC S9(4) COMP-3.                            
         02 ZAP         PIC S9(3) COMP-3.                            
         02 FIL2        PIC X(2). 
         
       FD MW-FILEB
          LABEL RECORD STANDARD
          DATA RECORD S-FILEB.
       01 S-FILEB.                                                     
         02 FIL1        PIC X(17).                     
         02 R1P         PIC S9(6).
         02 R2P         PIC S9(6).               
         02 R3P         PIC S9(6).    
         02 R4P         PIC S9(8).
         02 R5P         PIC S9(8).
         02 R6P         PIC S9(8).
         02 R7P         PIC S9(6).
         02 R8P         PIC S9(6).
         02 R9P         PIC S9(6). 
         02 RAP         PIC S9(4). 
         02 FIL2        PIC X(2).
       WORKING-STORAGE SECTION.
       01  IO-STATUS            PIC XX.

       PROCEDURE DIVISION.
       START-PARA.
            
           OPEN INPUT MW-FILEA
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.
           
           OPEN OUTPUT MW-FILEB
           IF IO-STATUS NOT = "00"
              DISPLAY "ERROR OPEN:"
              DISPLAY "IO-STATUS =" IO-STATUS
              GO TO FIN-ERREUR
           END-IF.  
       	   
       READ-LOOP.
           READ MW-FILEA NEXT
              AT END GO TO FIN-REL
           END-READ.
                  
           PERFORM MOVE-FIELD-TO-FIELD
           THRU E-MOVE-FIELD-TO-FIELD.

           GO TO READ-LOOP.

       FIN-REL.
           DISPLAY "PROGRAM TERMINATED OK".
           PERFORM END-COMMON-DISPLAY.
             
           EXIT PROGRAM.
           STOP RUN.
             
       FIN-ERREUR.
           PERFORM END-COMMON-DISPLAY.
       
           EXIT PROGRAM.
           STOP RUN RETURNING 16.
       
       END-COMMON-DISPLAY.
        
             
        CLOSE MW-FILEA.
        CLOSE MW-FILEB.
       
       MOVE-FIELD-TO-FIELD.
       	   
        MOVE CORRESPONDING E-FILEA TO S-FILEB                        
        MOVE Z1P TO R1P                                             
        MOVE Z2P TO R2P                                         
        MOVE Z3P TO R3P                                           
        MOVE Z4P TO R4P                                           
        MOVE Z5P TO R5P                                         
        MOVE Z6P TO R6P                                           
        MOVE Z7P TO R7P                                           
        MOVE Z8P TO R8P                                        
        MOVE Z9P TO R9P                                    
        MOVE ZAP TO RAP                                         
        WRITE S-FILEB                                      
       		.
       E-MOVE-FIELD-TO-FIELD.
         EXIT.                                         
