--MW CREATE  UNIQUE INDEX P945.RXIGSA0R ON P945.RTIGSA                               
--MW         ( DIGFA0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P945.RXIGSB0R ON P945.RTIGSB                               
--MW         ( DIGFA0 ASC                                                            
--MW         , NT_DIGSB ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P945.RXIGSC0R ON P945.RTIGSC                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFC0 ASC                                                            
--MW         , SM_DIGSC ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P945.RXIGSD0R ON P945.RTIGSD                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFD0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P945.RXIGSE0R ON P945.RTIGSE                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFE0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P945.RXIGSF0R ON P945.RTIGSF                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFF0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
CREATE  INDEX P945.RXAI241Y ON P945.RTAI24                                      
        ( FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXAI531Y ON P945.RTAI53                                      
        ( FSSKU ASC                                                             
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXAN000Y ON P945.RTAN00                                      
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXAN001Y ON P945.RTAN00                                      
        ( ENRID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA001Y ON P945.RTBA00                                      
        ( NFACTREM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA011Y ON P945.RTBA01                                      
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA012Y ON P945.RTBA01                                      
        ( NCHRONO ASC                                                           
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA013Y ON P945.RTBA01                                      
        ( DEMIS DESC                                                            
        , WANNUL ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA014Y ON P945.RTBA01                                      
        ( DEMIS ASC                                                             
        , WANNUL ASC                                                            
        , NBA DESC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA015Y ON P945.RTBA01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA101Y ON P945.RTBA10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXBA201Y ON P945.RTBA20                                      
        ( NTIERSCV ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCC031Y ON P945.RTCC03                                      
        ( MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , TYPTRANS ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCC041 ON P945.RTCC04                                       
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCD301Y ON P945.RTCD30                                      
        ( NCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCD302Y ON P945.RTCD30                                      
        ( NMAG ASC                                                              
        , NCODIC ASC                                                            
        , DREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCD303Y ON P945.RTCD30                                      
        ( NMAG ASC                                                              
        , NREC ASC                                                              
        , DVALIDATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCE101Y ON P945.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCE102Y ON P945.RTCE10                                      
        ( NZONPRIX ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCE103Y ON P945.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXCE104Y ON P945.RTCE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXDC001Y ON P945.RTDC00                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXDC101Y ON P945.RTDC10                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXDDDD1Y ON P945.RTDDDD                                      
        ( CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTYPENREG ASC                                                         
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXEG051Y ON P945.RTEG05                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMPC ASC                                                         
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXEG101Y ON P945.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXEG102Y ON P945.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXEG103Y ON P945.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND1 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXEG104Y ON P945.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND2 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXEG900Y ON P945.RTEG90                                      
        ( IDENTTS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW oop CREATE  INDEX P945.RXEXFA0Y ON P945.RTEXFA                                      
--MW oop         ( NTRI ASC                                                              
--MW oop         , LRUPTURE1 ASC                                                         
--MW oop         , LRUPTURE3 ASC                                                         
--MW oop           )                                                                     
--MW oop            PCTFREE 5                                                            
--MW oop            CLUSTER                                                              
--MW oop     ;                                                                           
CREATE  INDEX P945.RXEXFD0Y ON P945.RTEXFD                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX000Y ON P945.RTEX00                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX100Y ON P945.RTEX10                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX200Y ON P945.RTEX20                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX300Y ON P945.RTEX30                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX400Y ON P945.RTEX40                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX500Y ON P945.RTEX50                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX600Y ON P945.RTEX60                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX700Y ON P945.RTEX70                                      
        ( NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXEX900Y ON P945.RTEX90                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXFF001Y ON P945.RTFF00                                      
        ( NTIERS ASC                                                            
        , WANN ASC                                                              
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF002Y ON P945.RTFF00                                      
        ( DEVN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF051Y ON P945.RTFF05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF101Y ON P945.RTFF10                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF102Y ON P945.RTFF10                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF111Y ON P945.RTFF11                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF121Y ON P945.RTFF12                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF131Y ON P945.RTFF13                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF151Y ON P945.RTFF15                                      
        ( NPIECEFAC ASC                                                         
        , DEXCPTFAC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFF860Y ON P945.RTFF86                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXFF870Y ON P945.RTFF87                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW CREATE  INDEX P945.RXFF880Y ON P945.RTFF88                                      
--MW        ( PREFT1 ASC                                                            
--MW        , DEXCPT ASC                                                            
--MW        , NPIECE ASC                                                            
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P945.RXFF881Y ON P945.RTFF88                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXFI101Y ON P945.RTFI10                                      
        ( NMUTATION ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXFI102Y ON P945.RTFI10                                      
        ( DCOMPTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT121Y ON P945.RTFT12                                      
        ( NJRN ASC                                                              
        , NEXER ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT122Y ON P945.RTFT12                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT131Y ON P945.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT133Y ON P945.RTFT13                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT134Y ON P945.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT141Y ON P945.RTFT14                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT211Y ON P945.RTFT21                                      
        ( LETTRAGE DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT212Y ON P945.RTFT21                                      
        ( COMPTE ASC                                                            
        , NSOC ASC                                                              
        , LETTRAGE ASC                                                          
        , CDEVISE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT214Y ON P945.RTFT21                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT221Y ON P945.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , SECTION ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT222Y ON P945.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , RUBRIQUE ASC                                                          
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT261Y ON P945.RTFT26                                      
        ( BAN ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT271Y ON P945.RTFT27                                      
        ( NTBENEF ASC                                                           
        , NFOUR ASC                                                             
        , CTRESO ASC                                                            
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , NAUX ASC                                                              
        , NAUXR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT272Y ON P945.RTFT27                                      
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXFT310Y ON P945.RTFT31                                      
        ( SECTION ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXFT311 ON P945.RTFT31                                       
        ( RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT312Y ON P945.RTFT31                                      
        ( SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT313Y ON P945.RTFT31                                      
        ( RUBRIQUE ASC                                                          
        , SECTION ASC                                                           
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXFT760Y ON P945.RTFT76                                      
        ( DFACTURE ASC                                                          
        , NFACTURE ASC                                                          
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXFT761Y ON P945.RTFT76                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA001Y ON P945.RTGA00                                      
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA002Y ON P945.RTGA00                                      
        ( CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA003Y ON P945.RTGA00                                      
        ( CMARQ ASC                                                             
        , LEMBALLAGE ASC                                                        
        , NCODIC ASC                                                            
        , CASSORT ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA004Y ON P945.RTGA00                                      
        ( NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA005Y ON P945.RTGA00                                      
        ( NCODIC ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA006Y ON P945.RTGA00                                      
        ( CASSORT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA007Y ON P945.RTGA00                                      
        ( CFAM ASC                                                              
        , CASSORT ASC                                                           
        , CAPPRO ASC                                                            
        , NCODIC ASC                                                            
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA008Y ON P945.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA009Y ON P945.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CASSORT ASC                                                           
        , D1RECEPT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA011A ON P945.RTGA01YA                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA011J ON P945.RTGA01YJ                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA011Y ON P945.RTGA01                                      
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA031Y ON P945.RTGA03                                      
        ( NCODICK ASC                                                           
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA101Y ON P945.RTGA10                                      
        ( NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA102Y ON P945.RTGA10                                      
        ( CTYPLIEU ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA103Y ON P945.RTGA10                                      
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA120Y ON P945.RTGA12                                      
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA141Y ON P945.RTGA14                                      
        ( WSEQFAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA213Y ON P945.RTGA21                                      
        ( WRAYONFAM ASC                                                         
        , CRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA251Y ON P945.RTGA25                                      
        ( CMARKETING ASC                                                        
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA302Y ON P945.RTGA30                                      
        ( CPARAM ASC                                                            
        , LVPARAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA431Y ON P945.RTGA43                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXGA490Y ON P945.RTGA49                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGA561Y ON P945.RTGA56                                      
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA581Y ON P945.RTGA58                                      
        ( NCODICLIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA591Y ON P945.RTGA59                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA592Y ON P945.RTGA59                                      
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA621Y ON P945.RTGA62                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA651Y ON P945.RTGA65                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA652Y ON P945.RTGA65                                      
        ( CSTATUT ASC                                                           
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA661Y ON P945.RTGA66                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA681Y ON P945.RTGA68                                      
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA751Y ON P945.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGA752Y ON P945.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGB301Y ON P945.RTGB30                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGB801Y ON P945.RTGB80                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG151Y ON P945.RTGG15                                      
        ( NCODIC ASC                                                            
        , DDECISION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXGG170Y ON P945.RTGG17                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGG201Y ON P945.RTGG20                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG202Y ON P945.RTGG20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET DESC                                                           
        , PEXPTTC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG203Y ON P945.RTGG20                                      
        ( DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG204Y ON P945.RTGG20                                      
        ( NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG210Y ON P945.RTGG21                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG402Y ON P945.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG403Y ON P945.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
        , DFINEFFET ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG601Y ON P945.RTGG60                                      
        ( WTRAITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGG701Y ON P945.RTGG70                                      
        ( NCODIC ASC                                                            
        , DREC DESC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXGG702Y ON P945.RTGG70                                      
        ( DREC DESC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGP040Y ON P945.RTGP04                                      
        ( WTYPMVT ASC                                                           
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGQ014Y ON P945.RTGQ01                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGQ015Y ON P945.RTGQ01                                      
        ( CINSEE ASC                                                            
        , WCONTRA DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGQ031Y ON P945.RTGQ03                                      
        ( CZONLIV ASC                                                           
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGQ061Y ON P945.RTGQ06                                      
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGQ062Y ON P945.RTGQ06                                      
        ( CINSEE ASC                                                            
        , CTYPE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGQ063Y ON P945.RTGQ06                                      
        ( CPOSTAL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR301Y ON P945.RTGR30                                      
        ( DMVTREC ASC                                                           
        , WTLMELA ASC                                                           
        , CTYPMVT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR302Y ON P945.RTGR30                                      
        ( NENTCDE ASC                                                           
        , DJRECQUAI ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR303Y ON P945.RTGR30                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR304Y ON P945.RTGR30                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR351Y ON P945.RTGR35                                      
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR353Y ON P945.RTGR35                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR354Y ON P945.RTGR35                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGR355Y ON P945.RTGR35                                      
        ( NENTCDEPT ASC                                                         
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGS301Y ON P945.RTGS30                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGS311Y ON P945.RTGS31                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGS420Y ON P945.RTGS42                                      
        ( NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
        , NSSLIEUORIG ASC                                                       
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NSSLIEUDEST ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGS422Y ON P945.RTGS42                                      
        ( DMVT ASC                                                              
        , DHMVT ASC                                                             
        , DMMVT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGS601Y ON P945.RTGS60                                      
        ( NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGS602Y ON P945.RTGS60                                      
        ( NCODIC ASC                                                            
        , NSOCDEST ASC                                                          
        , NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , CSTATUTTRT ASC                                                        
        , LEMPLACT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGS700Y ON P945.RTGS70                                      
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGV02Z1 ON P945.RTGV02Z                                     
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV021Y ON P945.RTGV02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV022Y ON P945.RTGV02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV023Y ON P945.RTGV02                                      
        ( IDCLIENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV024Y ON P945.RTGV02                                      
        ( TELBUR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV050Y ON P945.RTGV05                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGV081Y ON P945.RTGV08                                      
        ( VALCTRL ASC                                                           
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV104Y ON P945.RTGV10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV105Y ON P945.RTGV10                                      
        ( NVENTO ASC                                                            
        , NLIEU ASC                                                             
        , TYPVTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV111Y ON P945.RTGV11                                      
        ( CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTYPENREG ASC                                                         
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV113Y ON P945.RTGV11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV114Y ON P945.RTGV11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV115Y ON P945.RTGV11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV116Y ON P945.RTGV11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV141Y ON P945.RTGV14                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NTRANS DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV142Y ON P945.RTGV14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV200Y ON P945.RTGV20                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGV231Y ON P945.RTGV23                                      
        ( DCREATION1 ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV271Y ON P945.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXGV272Y ON P945.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGV273Y ON P945.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXGV300Y ON P945.RTGV30                                      
        ( LNOM ASC                                                              
        , DVENTE ASC                                                            
        , DNAISSANCE ASC                                                        
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXGV35Z1 ON P945.RTGV35Z                                     
        ( NSOCIETE ASC                                                          
        , NDOCENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV352Y ON P945.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NDOCENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV353Y ON P945.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV355Y ON P945.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NDOCENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV356Y ON P945.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NBONENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXGV990Y ON P945.RTGV99                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE001Y ON P945.RTHE00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE002Y ON P945.RTHE00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE011Y ON P945.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE012Y ON P945.RTHE01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE013Y ON P945.RTHE01                                      
        ( NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE014Y ON P945.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE101Y ON P945.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE102Y ON P945.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE103Y ON P945.RTHE10                                      
        ( NCODIC ASC                                                            
        , NSERIE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE104Y ON P945.RTHE10                                      
        ( PDOSNASC ASC                                                          
        , DOSNASC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE105Y ON P945.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NENVOI ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE107Y ON P945.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WSOLDE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  INDEX P945.RXHE110Y ON P945.RTHE11                                      
--MW        ( NLIEUHED ASC                                                          
--MW        , NGHE ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P945.RXHE111Y ON P945.RTHE11                                      
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXHE201Y ON P945.RTHE20                                      
        ( CLIEUHET ASC                                                          
        , WFLAG ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE202Y ON P945.RTHE20                                      
        ( NUMPAL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHE221Y ON P945.RTHE22                                      
        ( CLIEUHET ASC                                                          
        , CTIERS ASC                                                            
        , DENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHN001Y ON P945.RTHN00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHN002Y ON P945.RTHN00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHN003Y ON P945.RTHN00                                      
        ( DTRAIT ASC                                                            
        , WSUPP ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHN011Y ON P945.RTHN01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHN012Y ON P945.RTHN01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHV021Y ON P945.RTHV02                                      
        ( DVENTECIALE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXHV071Y ON P945.RTHV07                                      
        ( CETAT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXHV081Y ON P945.RTHV08                                      
        ( DVENTECIALE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.RXHV600Y ON P945.RTHV60                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXHV960Y ON P945.RTHV96                                      
        ( DTSTAT ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
        , CVENDEUR ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXIA001Y ON P945.RTIA00                                      
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , WTLMELA ASC                                                           
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXIE001Y ON P945.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , CMODSTOCK ASC                                                         
        , CEMP13 ASC                                                            
        , CEMP23 ASC                                                            
        , CEMP33 ASC                                                            
        , CTYPEMPL3 ASC                                                         
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXIE003Y ON P945.RTIE00                                      
        ( NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXIE004Y ON P945.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXIE052Y ON P945.RTIE05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXIE301Y ON P945.RTIE30                                      
        ( "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXIN001Y ON P945.RTIN00                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXIN050Y ON P945.RTIN05                                      
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXIN901Y ON P945.RTIN90                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , LIEUTRT ASC                                                           
        , DINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXKIS01Y ON P945.RTKIS0                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXKIS11Y ON P945.RTKIS1                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXKIS21Y ON P945.RTKIS2                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXKIS31Y ON P945.RTKIS3                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXLG001 ON P945.RTLG00                                       
        ( CINSEE ASC                                                            
        , LNOMPVOIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXLG002 ON P945.RTLG00                                       
        ( CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXLM000Y ON P945.RTLM00                                      
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DATTRANS ASC                                                          
        , NCAISSE ASC                                                           
        , NTRANS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXPR001Y ON P945.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXPR002Y ON P945.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CASSORT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXPR011Y ON P945.RTPR01                                      
        ( CFAM ASC                                                              
        , WAFFICH ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXPR061Y ON P945.RTPR06                                      
        ( CSTATUT ASC                                                           
        , LSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXPR062Y ON P945.RTPR06                                      
        ( CSTATUT ASC                                                           
        , DEFFET ASC                                                            
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXPR063Y ON P945.RTPR06                                      
        ( CSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXPS001Y ON P945.RTPS00                                      
        ( NOMCLI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXQC011Y ON P945.RTQC01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXQC111Y ON P945.RTQC11                                      
        ( DTESSI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXQC112Y ON P945.RTQC11                                      
        ( NCLIENT ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXQC113Y ON P945.RTQC11                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.RXQC121Y ON P945.RTQC12                                      
        ( DENVOI_QV ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXRA001Y ON P945.RTRA00                                      
        ( CTYPDEMANDE ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXRA002Y ON P945.RTRA00                                      
        ( NAVOIRCLIENT ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXRD000Y ON P945.RTRD00                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXRE600Y ON P945.RTRE60                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXRE610Y ON P945.RTRE61                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXRX251Y ON P945.RTRX25                                      
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXRX300Y ON P945.RTRX30                                      
        ( NRELEVE ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXRX551Y ON P945.RTRX55                                      
        ( NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXSP101Y ON P945.RTSP10                                      
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , SRP ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXSP200Y ON P945.RTSP20                                      
        ( DTRAITEMENT ASC                                                       
        , CPROG ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXSP201Y ON P945.RTSP20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXTL021Y ON P945.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXTL022Y ON P945.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXTL040Y ON P945.RTTL04                                      
        ( NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXTL111Y ON P945.RTTL11                                      
        ( NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXVA000Y ON P945.RTVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXVA001Y ON P945.RTVA00                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXVA010Y ON P945.RTVA01                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RXVA011Y ON P945.RTVA01                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXVE021Y ON P945.RTVE02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE022Y ON P945.RTVE02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE102Y ON P945.RTVE10                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE103Y ON P945.RTVE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXVE111Y ON P945.RTVE11                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE113Y ON P945.RTVE11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE114Y ON P945.RTVE11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE115Y ON P945.RTVE11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE116Y ON P945.RTVE11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVE141Y ON P945.RTVE14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P945.RXVI010Y ON P945.RTVI01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.RYGV031Y ON P945.RTGV03                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.SXVA000Y ON P945.STVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.SXVA010Y ON P945.STVA01                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.ZXGA001Y ON P945.ZTGA00                                      
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA002Y ON P945.ZTGA00                                      
        ( CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA003Y ON P945.ZTGA00                                      
        ( CMARQ ASC                                                             
        , LEMBALLAGE ASC                                                        
        , NCODIC ASC                                                            
        , CASSORT ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA004Y ON P945.ZTGA00                                      
        ( NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA005Y ON P945.ZTGA00                                      
        ( NCODIC ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA006Y ON P945.ZTGA00                                      
        ( CASSORT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA007Y ON P945.ZTGA00                                      
        ( CFAM ASC                                                              
        , CASSORT ASC                                                           
        , CAPPRO ASC                                                            
        , NCODIC ASC                                                            
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA008Y ON P945.ZTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA009Y ON P945.ZTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CASSORT ASC                                                           
        , D1RECEPT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA101Y ON P945.ZTGA10                                      
        ( NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA102Y ON P945.ZTGA10                                      
        ( CTYPLIEU ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.ZXGA103Y ON P945.ZTGA10                                      
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.ZXVA000Y ON P945.ZTVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P945.ZXVA001Y ON P945.ZTVA00                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD060Y ON P945.RTAD06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD070Y ON P945.RTAD07                               
        ( CSOC ASC                                                              
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD071Y ON P945.RTAD07                               
        ( NENTCDEK ASC                                                          
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD100Y ON P945.RTAD10                               
        ( CSOC ASC                                                              
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD101Y ON P945.RTAD10                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD102Y ON P945.RTAD10                               
        ( NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD110Y ON P945.RTAD11                               
        ( CSOC ASC                                                              
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD111Y ON P945.RTAD11                               
        ( CFAMK ASC                                                             
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD140Y ON P945.RTAD14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD510Y ON P945.RTAD51                               
        ( CSOC ASC                                                              
        , CCOULEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD511Y ON P945.RTAD51                               
        ( CCOULEURK ASC                                                         
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD550Y ON P945.RTAD55                               
        ( CUSINE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD610Y ON P945.RTAD61                               
        ( CSOC ASC                                                              
        , CPAYS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAD611Y ON P945.RTAD61                               
        ( CPAYSK ASC                                                            
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAI240Y ON P945.RTAI24                               
        ( FDCLAS ASC                                                            
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAI270Y ON P945.RTAI27                               
        ( CDESCRIPTIF ASC                                                       
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAI530Y ON P945.RTAI53                               
        ( FSSKU ASC                                                             
        , FSCTL ASC                                                             
        , FSCLAS ASC                                                            
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAN010Y ON P945.RTAN01                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
        , CDEST ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAV010Y ON P945.RTAV01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAV020Y ON P945.RTAV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXAV030Y ON P945.RTAV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBA000Y ON P945.RTBA00                               
        ( NFACTBA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBA010Y ON P945.RTBA01                               
        ( NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBA100Y ON P945.RTBA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , CMODRGLT ASC                                                          
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBA200Y ON P945.RTBA20                               
        ( CTIERSBA ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBA210Y ON P945.RTBA21                               
        ( CTIERSBA ASC                                                          
        , DANNEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBA300Y ON P945.RTBA30                               
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBC310 ON P945.RTBC31                                
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBC320Y ON P945.RTBC32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBC330Y ON P945.RTBC33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBC340Y ON P945.RTBC34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXBC350Y ON P945.RTBC35                               
        ( CPROGRAMME ASC                                                        
        , DDEBUT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCCCCCY ON P945.RTCCCC                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCCCC1Y ON P945.RTCCCC                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCC000 ON P945.RTCC00                                
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCC010R ON P945.RTCC01                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCC020 ON P945.RTCC02                                
        ( DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCC030Y ON P945.RTCC03                               
        ( TYPTRANS ASC                                                          
        , MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NOPERATEUR ASC                                                        
        , HTRAIT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCC040 ON P945.RTCC04                                
        ( CPAICPT ASC                                                           
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCC050Y ON P945.RTCC05                               
        ( NSOC ASC                                                              
        , DJOUR ASC                                                             
        , NSEQ ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD000Y ON P945.RTCD00                               
        ( NCODIC ASC                                                            
        , NMAGGRP ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD050Y ON P945.RTCD05                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD100Y ON P945.RTCD10                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD150Y ON P945.RTCD15                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD200Y ON P945.RTCD20                               
        ( NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD250Y ON P945.RTCD25                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD300Y ON P945.RTCD30                               
        ( DCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD350Y ON P945.RTCD35                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCD500Y ON P945.RTCD50                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCE100Y ON P945.RTCE10                               
        ( NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCE200Y ON P945.RTCE20                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCT000Y ON P945.RTCT00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DDELIV ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXCX000Y ON P945.RTCX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , COMPTECG ASC                                                          
        , DATEFF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXDC000Y ON P945.RTDC00                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXDC100Y ON P945.RTDC10                               
        ( NDOSSIER ASC                                                          
        , NECHEANCE ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXDC200Y ON P945.RTDC20                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXDDDD0Y ON P945.RTDDDD                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXDS000Y ON P945.RTDS00                               
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CAPPRO ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXDS050Y ON P945.RTDS05                               
        ( NCODIC ASC                                                            
        , DSEMESTRE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXDS100Y ON P945.RTDS10                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEF100Y ON P945.RTEF10                               
        ( CTRAIT ASC                                                            
        , CTIERS ASC                                                            
        , NRENDU ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG000Y ON P945.RTEG00                               
        ( NOMETAT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG050Y ON P945.RTEG05                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG100Y ON P945.RTEG10                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG110Y ON P945.RTEG11                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG150Y ON P945.RTEG15                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG200Y ON P945.RTEG20                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG250Y ON P945.RTEG25                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , LIGNECALC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG300Y ON P945.RTEG30                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXEG400Y ON P945.RTEG40                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFA010Y ON P945.RTFA01                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFA020Y ON P945.RTFA02                               
        ( NSOCV ASC                                                             
        , NLIEUV ASC                                                            
        , NORDV ASC                                                             
        , NSOCF ASC                                                             
        , NLIEUF ASC                                                            
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF000Y ON P945.RTFF00                               
        ( NPIECE ASC                                                            
        , DEXCPT DESC                                                           
        , PREFT1 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF050Y ON P945.RTFF05                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF060Y ON P945.RTFF06                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF100Y ON P945.RTFF10                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF110Y ON P945.RTFF11                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF120Y ON P945.RTFF12                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF130Y ON P945.RTFF13                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF150Y ON P945.RTFF15                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF200Y ON P945.RTFF20                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNEFAC ASC                                                         
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF210Y ON P945.RTFF21                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF250Y ON P945.RTFF25                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , WTYPESEQ ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF300Y ON P945.RTFF30                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF350Y ON P945.RTFF35                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF360Y ON P945.RTFF36                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF370Y ON P945.RTFF37                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF400Y ON P945.RTFF40                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF410Y ON P945.RTFF41                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF420Y ON P945.RTFF42                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF430Y ON P945.RTFF43                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF440Y ON P945.RTFF44                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF450Y ON P945.RTFF45                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF500Y ON P945.RTFF50                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF510Y ON P945.RTFF51                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF600Y ON P945.RTFF60                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF650Y ON P945.RTFF65                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF660Y ON P945.RTFF66                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF670Y ON P945.RTFF67                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF700Y ON P945.RTFF70                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF750Y ON P945.RTFF75                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF800Y ON P945.RTFF80                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF810Y ON P945.RTFF81                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFF850Y ON P945.RTFF85                               
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFI000Y ON P945.RTFI00                               
        ( CPROF ASC                                                             
        , QTAUX ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFI050Y ON P945.RTFI05                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM010Y ON P945.RTFM01                               
        ( NENTITE ASC                                                           
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM020Y ON P945.RTFM02                               
        ( CDEVISE ASC                                                           
        , WSENS ASC                                                             
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM030Y ON P945.RTFM03                               
        ( NENTITE ASC                                                           
        , CMETHODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM040Y ON P945.RTFM04                               
        ( CDEVISE ASC                                                           
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM050Y ON P945.RTFM05                               
        ( CDEVORIG ASC                                                          
        , CDEVDEST ASC                                                          
        , DEFFET ASC                                                            
        , COPERAT ASC                                                           
        , PTAUX ASC                                                             
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM060Y ON P945.RTFM06                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM100Y ON P945.RTFM10                               
        ( NENTITE ASC                                                           
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM200 ON P945.RTFM20                                
        ( NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM230 ON P945.RTFM23                                
        ( NENTITE ASC                                                           
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM540Y ON P945.RTFM54                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM550Y ON P945.RTFM55                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM560Y ON P945.RTFM56                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM570Y ON P945.RTFM57                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM580Y ON P945.RTFM58                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM641 ON P945.RTFM64                                
        ( STEAPP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM691 ON P945.RTFM69                                
        ( COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM730Y ON P945.RTFM73                               
        ( CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM801Y ON P945.RTFM80                               
        ( CNATOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM811Y ON P945.RTFM81                               
        ( CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM821Y ON P945.RTFM82                               
        ( CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM831Y ON P945.RTFM83                               
        ( CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM841Y ON P945.RTFM84                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CTVA ASC                                                              
        , CGEO ASC                                                              
        , WGROUPE ASC                                                           
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM871Y ON P945.RTFM87                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM881Y ON P945.RTFM88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM891Y ON P945.RTFM89                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM901Y ON P945.RTFM90                               
        ( CINTERFACE ASC                                                        
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM910Y ON P945.RTFM91                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM921Y ON P945.RTFM92                               
        ( CZONE ASC                                                             
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFM930Y ON P945.RTFM93                               
        ( CRITLIEU1 ASC                                                         
        , CRITLIEU2 ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT010Y ON P945.RTFT01                               
        ( NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT020Y ON P945.RTFT02                               
        ( CACID ASC                                                             
        , NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT030Y ON P945.RTFT03                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT040Y ON P945.RTFT04                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT050Y ON P945.RTFT05                               
        ( CBANQUE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT060Y ON P945.RTFT06                               
        ( NSOC ASC                                                              
        , CPTR ASC                                                              
        , NAUX ASC                                                              
        , CREGTVA ASC                                                           
        , NATURE ASC                                                            
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT070Y ON P945.RTFT07                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT080Y ON P945.RTFT08                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT090Y ON P945.RTFT09                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT100Y ON P945.RTFT10                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT110Y ON P945.RTFT11                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , WTYPEADR ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT120Y ON P945.RTFT12                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT130Y ON P945.RTFT13                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT140Y ON P945.RTFT14                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NREG ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT150Y ON P945.RTFT15                               
        ( NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT160Y ON P945.RTFT16                               
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT170Y ON P945.RTFT17                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT180Y ON P945.RTFT18                               
        ( METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT190Y ON P945.RTFT19                               
        ( CTRAIT ASC                                                            
        , CACID ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT200Y ON P945.RTFT20                               
        ( NAUX ASC                                                              
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , CTRESO ASC                                                            
        , WACCES ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT210Y ON P945.RTFT21                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NLIGNE ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT220Y ON P945.RTFT22                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
        , SSCOMPTE ASC                                                          
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , STEAPP ASC                                                            
        , CDEVISE ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , WSENS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT230Y ON P945.RTFT23                               
        ( NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT250Y ON P945.RTFT25                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT260Y ON P945.RTFT26                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT270Y ON P945.RTFT27                               
        ( NLIGNE ASC                                                            
        , NPIECE ASC                                                            
        , DECH ASC                                                              
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NAUXR ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT280Y ON P945.RTFT28                               
        ( METHREG ASC                                                           
        , NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT290Y ON P945.RTFT29                               
        ( NCODE ASC                                                             
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT300Y ON P945.RTFT30                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT320Y ON P945.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT321Y ON P945.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT330Y ON P945.RTFT33                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
        , NVENTIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT340Y ON P945.RTFT34                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT350 ON P945.RTFT35                                
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , WTRT ASC                                                              
        , NEXER ASC                                                             
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT360 ON P945.RTFT36                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT370 ON P945.RTFT37                                
        ( NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT380 ON P945.RTFT38                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT390 ON P945.RTFT39                                
        ( NLIGNE ASC                                                            
        , REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT400 ON P945.RTFT40                                
        ( CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT410 ON P945.RTFT41                                
        ( NATCH ASC                                                             
        , CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT430Y ON P945.RTFT43                               
        ( NPIECE DESC                                                           
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , CDEVISE ASC                                                           
        , NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT440Y ON P945.RTFT44                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT450 ON P945.RTFT45                                
        ( CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT460 ON P945.RTFT46                                
        ( NPER ASC                                                              
        , CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT490 ON P945.RTFT49                                
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT500Y ON P945.RTFT50                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT510Y ON P945.RTFT51                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW oop CREATE  UNIQUE INDEX P945.RXFT520Y ON P945.RTFT52                               
--MW oop         ( CACID ASC                                                             
--MW oop           )                                                                     
--MW oop            PCTFREE 10                                                           
--MW oop            CLUSTER                                                              
--MW oop     ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT530Y ON P945.RTFT53                               
        ( CACID ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT540Y ON P945.RTFT54                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT570Y ON P945.RTFT57                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
        , WTYPEADR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT601 ON P945.RTFT60                                
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT611 ON P945.RTFT61                                
        ( SECTION ASC                                                           
        , NSOC ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT621 ON P945.RTFT62                                
        ( RUBRIQUE ASC                                                          
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT631 ON P945.RTFT63                                
        ( NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT651 ON P945.RTFT65                                
        ( CMASQUEC ASC                                                          
        , CMASQUES ASC                                                          
        , CMASQUER ASC                                                          
        , CMASQUEE ASC                                                          
        , CMASQUEA ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT660 ON P945.RTFT66                                
        ( COMPTE ASC                                                            
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT670 ON P945.RTFT67                                
        ( SECTION ASC                                                           
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT680 ON P945.RTFT68                                
        ( RUBRIQUE ASC                                                          
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT700Y ON P945.RTFT70                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT710Y ON P945.RTFT71                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT720Y ON P945.RTFT72                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT730Y ON P945.RTFT73                               
        ( NSOC ASC                                                              
        , CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT740Y ON P945.RTFT74                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , CNOMPROG ASC                                                          
        , DDATE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT750Y ON P945.RTFT75                               
        ( DDEMANDE DESC                                                         
        , NTIERS ASC                                                            
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT851Y ON P945.RTFT85                               
        ( CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT861Y ON P945.RTFT86                               
        ( DEFFET ASC                                                            
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT871Y ON P945.RTFT87                               
        ( SECTIONO ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT881Y ON P945.RTFT88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT891Y ON P945.RTFT89                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFT900Y ON P945.RTFT90                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX000Y ON P945.RTFX00                               
        ( NOECS DESC                                                            
        , DATEFF DESC                                                           
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX001Y ON P945.RTFX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
        , DATEFF DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX010Y ON P945.RTFX01                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX050Y ON P945.RTFX05                               
        ( NJRN ASC                                                              
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX100Y ON P945.RTFX10                               
        ( COMPTEGL ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX110Y ON P945.RTFX11                               
        ( CZONE ASC                                                             
        , ZONEGL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX150Y ON P945.RTFX15                               
        ( WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX200Y ON P945.RTFX20                               
        ( CTYPSTAT ASC                                                          
        , NDEPTLMELA ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX300Y ON P945.RTFX30                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX350Y ON P945.RTFX35                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX400Y ON P945.RTFX40                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX450Y ON P945.RTFX45                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXFX500Y ON P945.RTFX50                               
        ( DATTRANS ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CTYPTRANS ASC                                                         
        , CAPPRET ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA000Y ON P945.RTGA00                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA010A ON P945.RTGA01YA                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA010J ON P945.RTGA01YJ                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA010Y ON P945.RTGA01                               
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA030Y ON P945.RTGA03                               
        ( CSOC ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA040Y ON P945.RTGA04                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA060Y ON P945.RTGA06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA070Y ON P945.RTGA07                               
        ( CGRPFOURN ASC                                                         
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA080Y ON P945.RTGA08                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA090Y ON P945.RTGA09                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CMARKETIN3 ASC                                                        
        , CVMARKETIN3 ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA100Y ON P945.RTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA110Y ON P945.RTGA11                               
        ( CETAT ASC                                                             
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA130Y ON P945.RTGA13                               
        ( CFAM ASC                                                              
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA140Y ON P945.RTGA14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA150Y ON P945.RTGA15                               
        ( CFAM ASC                                                              
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA160Y ON P945.RTGA16                               
        ( CFAM ASC                                                              
        , CGARANTCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA170Y ON P945.RTGA17                               
        ( CFAM ASC                                                              
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA180Y ON P945.RTGA18                               
        ( CFAM ASC                                                              
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA200Y ON P945.RTGA20                               
        ( CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA210Y ON P945.RTGA21                               
        ( CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA211Y ON P945.RTGA21                               
        ( WRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA220Y ON P945.RTGA22                               
        ( CMARQ ASC                                                             
        , LMARQ ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA230Y ON P945.RTGA23                               
        ( CMARKETING ASC                                                        
        , LMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA240Y ON P945.RTGA24                               
        ( CDESCRIPTIF ASC                                                       
        , LDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA250Y ON P945.RTGA25                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA260Y ON P945.RTGA26                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA270Y ON P945.RTGA27                               
        ( CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA290Y ON P945.RTGA29                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  UNIQUE INDEX P945.RXGA300Y ON P945.RTGA30                               
--MW        ( CPARAM ASC                                                            
--MW        , CFAM ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA301Y ON P945.RTGA30                               
        ( CPARAM ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA310Y ON P945.RTGA31                               
        ( NCODIC ASC                                                            
        , NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA320Y ON P945.RTGA32                               
        ( NCODIC ASC                                                            
        , NSEQUENCE ASC                                                         
        , NEANCOMPO ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA330Y ON P945.RTGA33                               
        ( NCODIC ASC                                                            
        , CDEST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA390Y ON P945.RTGA39                               
        ( NENTSAP ASC                                                           
        , NENTANC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA400Y ON P945.RTGA40                               
        ( CGARANTIE ASC                                                         
        , CFAM ASC                                                              
        , CTARIF ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA410Y ON P945.RTGA41                               
        ( CGCPLT ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA420Y ON P945.RTGA42                               
        ( CGCPLT ASC                                                            
        , NMOIS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA430Y ON P945.RTGA43                               
        ( CSECTEUR ASC                                                          
        , CINSEE ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA440Y ON P945.RTGA44                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA450Y ON P945.RTGA45                               
        ( CFAM ASC                                                              
        , CGRP ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA460Y ON P945.RTGA46                               
        ( CGRP ASC                                                              
        , CSECT1 ASC                                                            
        , ZONE1 ASC                                                             
        , CSECT2 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA480Y ON P945.RTGA48                               
        ( NCODIC ASC                                                            
        , SPPRTP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA510Y ON P945.RTGA51                               
        ( NCODIC ASC                                                            
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA520Y ON P945.RTGA52                               
        ( NCODIC ASC                                                            
        , CGARANTCPLT ASC                                                       
        , CVGARANCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA530Y ON P945.RTGA53                               
        ( NCODIC ASC                                                            
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA550Y ON P945.RTGA55                               
        ( NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA560Y ON P945.RTGA56                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA580Y ON P945.RTGA58                               
        ( NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , CTYPLIEN ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA590Y ON P945.RTGA59                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA600Y ON P945.RTGA60                               
        ( NCODIC ASC                                                            
        , NMESSDEPOT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA620Y ON P945.RTGA62                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA630Y ON P945.RTGA63                               
        ( NCODIC ASC                                                            
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA640Y ON P945.RTGA64                               
        ( NCODIC ASC                                                            
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA650Y ON P945.RTGA65                               
        ( NCODIC ASC                                                            
        , CSTATUT ASC                                                           
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA660Y ON P945.RTGA66                               
        ( NCODIC ASC                                                            
        , CSENS ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA680Y ON P945.RTGA68                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA710Y ON P945.RTGA71                               
        ( CTABLE ASC                                                            
        , CSTABLE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA720Y ON P945.RTGA72                               
        ( CNUMCONT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA730Y ON P945.RTGA73                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA740Y ON P945.RTGA74                               
        ( NCODIC ASC                                                            
        , NTITRE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA750Y ON P945.RTGA75                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA760Y ON P945.RTGA76                               
        ( CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA770Y ON P945.RTGA77                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA790Y ON P945.RTGA79                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA810Y ON P945.RTGA81                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA830Y ON P945.RTGA83                               
        ( NCODIC ASC                                                            
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA910Y ON P945.RTGA91                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA920Y ON P945.RTGA92                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA930Y ON P945.RTGA93                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA940Y ON P945.RTGA94                               
        ( CPROG ASC                                                             
        , CCHAMP ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGA990Y ON P945.RTGA99                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGB300Y ON P945.RTGB30                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGB800Y ON P945.RTGB80                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGD990Y ON P945.RTGD99                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGF350Y ON P945.RTGF35                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGF400Y ON P945.RTGF40                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
        , WMARQFAM ASC                                                          
        , CMARQFAM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGF500Y ON P945.RTGF50                               
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGGDD0Y ON P945.RTGGDD                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGGXX0Y ON P945.RTGGXX                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG050Y ON P945.RTGG05                               
        ( NCONC ASC                                                             
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG100Y ON P945.RTGG10                               
        ( NDEMALI ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG150Y ON P945.RTGG15                               
        ( NDEMALI ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG200Y ON P945.RTGG20                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG250Y ON P945.RTGG25                               
        ( CHEFPROD ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG301Y ON P945.RTGG30                               
        ( DANNEE ASC                                                            
        , DMOIS ASC                                                             
        , CFAM ASC                                                              
        , CHEFPROD ASC                                                          
        , NCONC ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG400Y ON P945.RTGG40                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG500Y ON P945.RTGG50                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG510Y ON P945.RTGG51                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG550Y ON P945.RTGG55                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG600Y ON P945.RTGG60                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , DDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGG700Y ON P945.RTGG70                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI1000Y ON P945.RTGI10                              
        ( NSOCIETE ASC                                                          
        , CTABINT ASC                                                           
        , TTABINT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI1200Y ON P945.RTGI12                              
        ( NSOCIETE ASC                                                          
        , CTABINTMB ASC                                                         
        , MB ASC                                                                
        , TMB ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI1400Y ON P945.RTGI14                              
        ( NSOCIETE ASC                                                          
        , CTABINTCA ASC                                                         
        , CA ASC                                                                
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI1600Y ON P945.RTGI16                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI2000Y ON P945.RTGI20                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI2100Y ON P945.RTGI21                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI2200Y ON P945.RTGI22                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI2300Y ON P945.RTGI23                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGI2400Y ON P945.RTGI24                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGJ100Y ON P945.RTGJ10                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGJ400Y ON P945.RTGJ40                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGM010Y ON P945.RTGM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGM060Y ON P945.RTGM06                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , CZPRIX ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGM700Y ON P945.RTGM70                               
        ( NSOCIETE ASC                                                          
        , NZONE ASC                                                             
        , CHEFPROD ASC                                                          
        , WSEQFAM ASC                                                           
        , LVMARKET1 ASC                                                         
        , LVMARKET2 ASC                                                         
        , LVMARKET3 ASC                                                         
        , NCODIC2 ASC                                                           
        , NCODIC ASC                                                            
        , "SEQUENCE" ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGP000Y ON P945.RTGP00                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGP010Y ON P945.RTGP01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGP020Y ON P945.RTGP02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , DLIVRAISON ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGP030Y ON P945.RTGP03                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGP041Y ON P945.RTGP04                               
        ( NCODIC ASC                                                            
        , WTYPMVT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGP050Y ON P945.RTGP05                               
        ( CRAYON ASC                                                            
        , WRAYONFAM ASC                                                         
        , DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ000Y ON P945.RTGQ00                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ011Y ON P945.RTGQ01                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ020Y ON P945.RTGQ02                               
        ( CMODDEL ASC                                                           
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ030Y ON P945.RTGQ03                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
        , CZONLIV ASC                                                           
        , CPLAGE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ040Y ON P945.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , DEFFET ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ041Y ON P945.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ120Y ON P945.RTGQ12                               
        ( CINSEE ASC                                                            
        , CGRP ASC                                                              
        , CTYPGRP ASC                                                           
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGQ200Y ON P945.RTGQ20                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
        , CCADRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGR300Y ON P945.RTGR30                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGR305Y ON P945.RTGR30                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGR350Y ON P945.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGR352Y ON P945.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGS300Y ON P945.RTGS30                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGS310Y ON P945.RTGS31                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGS450Y ON P945.RTGS45                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGS500Y ON P945.RTGS50                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGS550Y ON P945.RTGS55                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGS600Y ON P945.RTGS60                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV02Z0 ON P945.RTGV02Z                              
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV020Y ON P945.RTGV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV040Y ON P945.RTGV04                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV060Y ON P945.RTGV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV08ZY ON P945.RTGV08Z                              
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV080Y ON P945.RTGV08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV100Y ON P945.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV101Y ON P945.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV102Y ON P945.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV110Y ON P945.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV119Y ON P945.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV130Y ON P945.RTGV13                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV140Y ON P945.RTGV14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV15ZY ON P945.RTGV15Z                              
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV150Y ON P945.RTGV15                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV230Y ON P945.RTGV23                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV250Y ON P945.RTGV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV260Y ON P945.RTGV26                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV270Y ON P945.RTGV27                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV310Y ON P945.RTGV31                               
        ( CVENDEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV35Z0 ON P945.RTGV35Z                              
        ( NSOCIETE ASC                                                          
        , NBONENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV350Y ON P945.RTGV35                               
        ( NSOCIETE ASC                                                          
        , NBONENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV400Y ON P945.RTGV40                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXGV410Y ON P945.RTGV41                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CTYPADR ASC                                                           
        , CMODDEL ASC                                                           
        , DDELIV ASC                                                            
        , CPLAGE ASC                                                            
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHA300Y ON P945.RTHA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHC000Y ON P945.RTHC00                               
        ( NCHS ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHC030Y ON P945.RTHC03                               
        ( NCHS ASC                                                              
        , NSEQ ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE000Y ON P945.RTHE00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE010Y ON P945.RTHE01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE100Y ON P945.RTHE10                               
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE150Y ON P945.RTHE15                               
        ( CLIEUHET ASC                                                          
        , NLIEUHED ASC                                                          
        , NGHE ASC                                                              
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE200Y ON P945.RTHE20                               
        ( NLIEUHED ASC                                                          
        , CLIEUHET ASC                                                          
        , NRETOUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE210Y ON P945.RTHE21                               
        ( CLIEUHET ASC                                                          
        , CLIEUHEI ASC                                                          
        , NLOTDIAG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE220Y ON P945.RTHE22                               
        ( CLIEUHET ASC                                                          
        , NENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHE230Y ON P945.RTHE23                               
        ( CLIEUHET ASC                                                          
        , NDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHL000Y ON P945.RTHL00                               
        ( NOMMAP ASC                                                            
        , POSIT ASC                                                             
        , ZECRAN ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHL050Y ON P945.RTHL05                               
        ( CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHL100Y ON P945.RTHL10                               
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
        , NREQUETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHL150 ON P945.RTHL15                                
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , NOCCUR ASC                                                            
        , NSEQCRIT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHN000Y ON P945.RTHN00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHN010Y ON P945.RTHN01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHN110Y ON P945.RTHN11                               
        ( CTIERS ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV000Y ON P945.RTHV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CMODDEL ASC                                                           
        , DOPER ASC                                                             
        , WSEQFAM ASC                                                           
        , CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV020Y ON P945.RTHV02                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV030 ON P945.RTHV03                                
        ( DVENTECIALE ASC                                                       
        , CPRESTATION ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV060Y ON P945.RTHV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV070Y ON P945.RTHV07                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV080Y ON P945.RTHV08                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV090Y ON P945.RTHV09                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV100Y ON P945.RTHV10                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV110Y ON P945.RTHV11                               
        ( NSOCIETE ASC                                                          
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV150Y ON P945.RTHV15                               
        ( CPROGRAMME ASC                                                        
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTECIALE ASC                                                       
        , CTERMID ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV160Y ON P945.RTHV16                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV170Y ON P945.RTHV17                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV180Y ON P945.RTHV18                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV200Y ON P945.RTHV20                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV250Y ON P945.RTHV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DGRPMOIS ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV260Y ON P945.RTHV26                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV270Y ON P945.RTHV27                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV280Y ON P945.RTHV28                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , WSEQPRO ASC                                                           
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV290Y ON P945.RTHV29                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV300Y ON P945.RTHV30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , NAGREGATED ASC                                                        
        , DSVENTECIALE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV320Y ON P945.RTHV32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV330Y ON P945.RTHV33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV340Y ON P945.RTHV34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV350Y ON P945.RTHV35                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV700Y ON P945.RTHV70                               
        ( NSOCIETE ASC                                                          
        , DMOIS ASC                                                             
        , NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXHV920Y ON P945.RTHV92                               
        ( NSOCIETE ASC                                                          
        , MAGASIN ASC                                                           
        , NCODIC ASC                                                            
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIA000Y ON P945.RTIA00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIA050Y ON P945.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIA051Y ON P945.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIA600Y ON P945.RTIA60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE000Y ON P945.RTIE00                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NFICHE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE050Y ON P945.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE051Y ON P945.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE100Y ON P945.RTIE10                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE200Y ON P945.RTIE20                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE250Y ON P945.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE251Y ON P945.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE300Y ON P945.RTIE30                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE350Y ON P945.RTIE35                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE360Y ON P945.RTIE36                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE450Y ON P945.RTIE45                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CZONE ASC                                                             
        , CSECTEUR ASC                                                          
        , NSOUSSECT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE600Y ON P945.RTIE60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE700Y ON P945.RTIE70                               
        ( NHS ASC                                                               
        , NLIEUHS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE800Y ON P945.RTIE80                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIE900Y ON P945.RTIE90                               
        ( NDEPOT ASC                                                            
        , NSOCDEPOT ASC                                                         
        , CREGROUP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIJ000Y ON P945.RTIJ00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIN000Y ON P945.RTIN00                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , WSTOCKMASQ ASC                                                        
        , CSTATUT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIN100Y ON P945.RTIN01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , SSLIEU ASC                                                            
        , CSTATUT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIN900Y ON P945.RTIN90                               
        ( DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIT000Y ON P945.RTIT00                               
        ( NINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIT050Y ON P945.RTIT05                               
        ( NINVENTAIRE ASC                                                       
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIT100Y ON P945.RTIT10                               
        ( NINVENTAIRE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIT150Y ON P945.RTIT15                               
        ( NINVENTAIRE ASC                                                       
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXIT900Y ON P945.RTIT90                               
        ( NINVENTAIRE ASC                                                       
        , NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXLG000 ON P945.RTLG00                                
        ( CINSEE ASC                                                            
        , CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXLG010Y ON P945.RTLG01                               
        ( CINSEE ASC                                                            
        , NVOIE ASC                                                             
        , CPARITE ASC                                                           
        , NDEBTR ASC                                                            
        , NFINTR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXLM100Y ON P945.RTLM10                               
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXLT000Y ON P945.RTLT00                               
        ( NLETTRE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXLT010Y ON P945.RTLT01                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXLT020Y ON P945.RTLT02                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
        , INDVAR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMG010Y ON P945.RTMG01                               
        ( CFAMMGI ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMO010Y ON P945.RTMO01                               
        ( TYPOFFRE ASC                                                          
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMQ150Y ON P945.RTMQ15                               
        ( CPROGRAMME ASC                                                        
        , CFONCTION ASC                                                         
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , DOPERATION ASC                                                        
        , "COMMENT" ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMQ160Y ON P945.RTMQ16                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU050Y ON P945.RTMU05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU210Y ON P945.RTMU21                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU250Y ON P945.RTMU25                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU260Y ON P945.RTMU26                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU300Y ON P945.RTMU30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU310Y ON P945.RTMU31                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU350Y ON P945.RTMU35                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXMU500Y ON P945.RTMU50                               
        ( CTYPCDE ASC                                                           
        , NSEQED ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPA700Y ON P945.RTPA70                               
        ( NSOCIETE ASC                                                          
        , CPTSAV ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPC080Y ON P945.RTPC08                               
        ( RANDOMNUM ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPC090Y ON P945.RTPC09                               
        ( RANDOMNUM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPM000Y ON P945.RTPM00                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPM060Y ON P945.RTPM06                               
        ( CMOPAI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPP000Y ON P945.RTPP00                               
        ( NLIEU ASC                                                             
        , DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR000 ON P945.RTPR00                                
        ( CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR010 ON P945.RTPR01                                
        ( CFAM ASC                                                              
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR020 ON P945.RTPR02                                
        ( NCODIC ASC                                                            
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR030 ON P945.RTPR03                                
        ( CPRESTATION ASC                                                       
        , CGESTION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR031Y ON P945.RTPR03                               
        ( CGESTION ASC                                                          
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR040 ON P945.RTPR04                                
        ( CPRESTATION ASC                                                       
        , CPRESTELT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR050 ON P945.RTPR05                                
        ( CPRESTATION ASC                                                       
        , CGRPETAT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR060 ON P945.RTPR06                                
        ( CPRESTATION ASC                                                       
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR100 ON P945.RTPR10                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR110 ON P945.RTPR11                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR120 ON P945.RTPR12                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR130 ON P945.RTPR13                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR140 ON P945.RTPR14                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR150 ON P945.RTPR15                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR160 ON P945.RTPR16                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR170 ON P945.RTPR17                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPR200 ON P945.RTPR20                                
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CAGREPRE ASC                                                          
        , WSEQED ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPS000Y ON P945.RTPS00                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPS010Y ON P945.RTPS01                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPS020Y ON P945.RTPS02                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPS030Y ON P945.RTPS03                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPS040Y ON P945.RTPS04                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , "TYPE" ASC                                                            
        , DINIT ASC                                                             
        , MONTTOT ASC                                                           
        , DUREE ASC                                                             
        , INFO1 ASC                                                             
        , INFO2 ASC                                                             
        , INFO3 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPT030Y ON P945.RTPT03                               
        ( CODLANG ASC                                                           
        , CNOMPGRM ASC                                                          
        , NSEQLIB ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXPV000Y ON P945.RTPV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC010Y ON P945.RTQC01                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC020Y ON P945.RTQC02                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC030 ON P945.RTQC03                                
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC050Y ON P945.RTQC05                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC060Y ON P945.RTQC06                               
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC110Y ON P945.RTQC11                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC120Y ON P945.RTQC12                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXQC130Y ON P945.RTQC13                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
        , CQUESTION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRA000Y ON P945.RTRA00                               
        ( NDEMANDE ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRA010Y ON P945.RTRA01                               
        ( NDEMANDE ASC                                                          
        , NSEQCODIC ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRA020Y ON P945.RTRA02                               
        ( CTYPREGL ASC                                                          
        , NREGL ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRC000Y ON P945.RTRC00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRC100Y ON P945.RTRC10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRE000Y ON P945.RTRE00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRE100Y ON P945.RTRE10                               
        ( NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WTLMELA ASC                                                           
        , NAGREGATED ASC                                                        
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX000Y ON P945.RTRX00                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX050Y ON P945.RTRX05                               
        ( NCONC ASC                                                             
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX100Y ON P945.RTRX10                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , NFREQUENCE ASC                                                        
        , NSEQUENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX150Y ON P945.RTRX15                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , DSUPPORT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX200Y ON P945.RTRX20                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX210Y ON P945.RTRX21                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX220Y ON P945.RTRX22                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX250Y ON P945.RTRX25                               
        ( NRELEVE ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX450Y ON P945.RTRX45                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX500Y ON P945.RTRX50                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXRX550Y ON P945.RTRX55                               
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSB110Y ON P945.RTSB11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSG020Y ON P945.RTSG02                               
        ( SECPAIE ASC                                                           
        , CSECTION ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSG030Y ON P945.RTSG03                               
        ( COMPTE ASC                                                            
        , NETABADM ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSL400Y ON P945.RTSL40                               
        ( NSOCIETE ASC                                                          
        , CLIEUINV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSL410Y ON P945.RTSL41                               
        ( CLIEUINV ASC                                                          
        , NREGL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSL500Y ON P945.RTSL50                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CLIEUINV ASC                                                          
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSL510Y ON P945.RTSL51                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CEMPL ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSM010Y ON P945.RTSM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSM020Y ON P945.RTSM02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSP000Y ON P945.RTSP00                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSP050Y ON P945.RTSP05                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSP100Y ON P945.RTSP10                               
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , NREC ASC                                                              
        , CMAJ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSP150Y ON P945.RTSP15                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSP250Y ON P945.RTSP25                               
        ( CMARQ ASC                                                             
        , NSOCDEST ASC                                                          
        , DPROV ASC                                                             
        , WTEBRB ASC                                                            
        , CTAUXTVA ASC                                                          
        , NLIEUDEST ASC                                                         
        , NLIEUORIG ASC                                                         
        , NSOCORIG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXSV010Y ON P945.RTSV01                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CTPSAV ASC                                                            
        , DEFFET ASC                                                            
        , NSOCSAV ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXTF300Y ON P945.RTTF30                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXTF350Y ON P945.RTTF35                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXTL020Y ON P945.RTTL02                               
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXTL060Y ON P945.RTTL06                               
        ( NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXTL080Y ON P945.RTTL08                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXTL090Y ON P945.RTTL09                               
        ( CLIVR ASC                                                             
        , DMMLIVR ASC                                                           
        , CRETOUR ASC                                                           
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXTL110Y ON P945.RTTL11                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA050Y ON P945.RTVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA051Y ON P945.RTVA05                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA060Y ON P945.RTVA06                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA061Y ON P945.RTVA06                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA100Y ON P945.RTVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA150Y ON P945.RTVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA160Y ON P945.RTVA16                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA200Y ON P945.RTVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA250Y ON P945.RTVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA300Y ON P945.RTVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA310Y ON P945.RTVA31                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVA500Y ON P945.RTVA50                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE020Y ON P945.RTVE02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE050Y ON P945.RTVE05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE060Y ON P945.RTVE06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE080Y ON P945.RTVE08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE100Y ON P945.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE101Y ON P945.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE110Y ON P945.RTVE11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RXVE140Y ON P945.RTVE14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.RYGV030Y ON P945.RTGV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA050Y ON P945.STVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA060Y ON P945.STVA06                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA100Y ON P945.STVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA150Y ON P945.STVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA160Y ON P945.STVA16                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA200Y ON P945.STVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA250Y ON P945.STVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA300Y ON P945.STVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.SXVA310Y ON P945.STVA31                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.ZXGA000Y ON P945.ZTGA00                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.ZXGA100Y ON P945.ZTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.ZXVA050Y ON P945.ZTVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P945.ZXVA051Y ON P945.ZTVA05                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.RXQTPSEPX ON P945.QTPSEPX                                    
        ( CFAM ASC                                                              
        , CGCPLT ASC                                                            
        , CTARIF ASC                                                            
        , WACTIF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.CCDIX_TTGV02 ON P945.TTGV02                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.CCDIX_TTGV11 ON P945.TTGV11                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.CCDIX_TTVE10 ON P945.TTVE10                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXGV020 ON P945.TTGV02                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXGV021 ON P945.TTGV02                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXGV022 ON P945.TTGV02                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXGV110 ON P945.TTGV11                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQNQ ASC                                                            
        , CTYPENREG ASC                                                         
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXGV111 ON P945.TTGV11                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXGV112 ON P945.TTGV11                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXVE101 ON P945.TTVE10                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P945.TXVE102 ON P945.TTVE10                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.CXGV020Y ON P945.CTGV02                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.CXGV110Y ON P945.CTGV11                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P945.CXVE100Y ON P945.CTVE10                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
