CREATE  UNIQUE INDEX P991.RXPS040M ON P991.RTPS04                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , "TYPE" ASC                                                            
        , DINIT ASC                                                             
        , MONTTOT ASC                                                           
        , DUREE ASC                                                             
        , INFO1 ASC                                                             
        , INFO2 ASC                                                             
        , INFO3 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW CREATE  UNIQUE INDEX P991.RXIGSA0R ON P991.RTIGSA                               
--MW         ( DIGFA0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P991.RXIGSB0R ON P991.RTIGSB                               
--MW         ( DIGFA0 ASC                                                            
--MW         , NT_DIGSB ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P991.RXIGSC0R ON P991.RTIGSC                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFC0 ASC                                                            
--MW         , SM_DIGSC ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P991.RXIGSD0R ON P991.RTIGSD                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFD0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P991.RXIGSE0R ON P991.RTIGSE                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFE0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P991.RXIGSF0R ON P991.RTIGSF                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFF0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
CREATE  INDEX P991.RXAI241D ON P991.RTAI24                                      
        ( FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXAI531D ON P991.RTAI53                                      
        ( FSSKU ASC                                                             
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXAN000D ON P991.RTAN00                                      
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXAN001D ON P991.RTAN00                                      
        ( ENRID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA001D ON P991.RTBA00                                      
        ( NFACTREM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA011D ON P991.RTBA01                                      
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA012D ON P991.RTBA01                                      
        ( NCHRONO ASC                                                           
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA013D ON P991.RTBA01                                      
        ( DEMIS DESC                                                            
        , WANNUL ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA014D ON P991.RTBA01                                      
        ( DEMIS ASC                                                             
        , WANNUL ASC                                                            
        , NBA DESC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA015D ON P991.RTBA01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA101D ON P991.RTBA10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXBA201D ON P991.RTBA20                                      
        ( NTIERSCV ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCC031D ON P991.RTCC03                                      
        ( MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , TYPTRANS ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCC041 ON P991.RTCC04                                       
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCD301D ON P991.RTCD30                                      
        ( NCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCD302D ON P991.RTCD30                                      
        ( NMAG ASC                                                              
        , NCODIC ASC                                                            
        , DREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCD303D ON P991.RTCD30                                      
        ( NMAG ASC                                                              
        , NREC ASC                                                              
        , DVALIDATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCE101D ON P991.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCE102D ON P991.RTCE10                                      
        ( NZONPRIX ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCE103D ON P991.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXCE104D ON P991.RTCE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXDC001D ON P991.RTDC00                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXDC101D ON P991.RTDC10                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXEG051D ON P991.RTEG05                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMPC ASC                                                         
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXEG101D ON P991.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXEG102D ON P991.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXEG103D ON P991.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND1 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXEG104D ON P991.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND2 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXEG900D ON P991.RTEG90                                      
        ( IDENTTS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEXFA0D ON P991.RTEXFA                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE3 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEXFD0D ON P991.RTEXFD                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX000D ON P991.RTEX00                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX100D ON P991.RTEX10                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX200D ON P991.RTEX20                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX300D ON P991.RTEX30                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX400D ON P991.RTEX40                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX500D ON P991.RTEX50                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX600D ON P991.RTEX60                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX700D ON P991.RTEX70                                      
        ( NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXEX900D ON P991.RTEX90                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFF001D ON P991.RTFF00                                      
        ( NTIERS ASC                                                            
        , WANN ASC                                                              
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF002D ON P991.RTFF00                                      
        ( DEVN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF051D ON P991.RTFF05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF101D ON P991.RTFF10                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF102D ON P991.RTFF10                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF111D ON P991.RTFF11                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF121D ON P991.RTFF12                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF131D ON P991.RTFF13                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF151D ON P991.RTFF15                                      
        ( NPIECEFAC ASC                                                         
        , DEXCPTFAC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFF860D ON P991.RTFF86                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFF870D ON P991.RTFF87                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW CREATE  INDEX P991.RXFF880D ON P991.RTFF88                                      
--MW        ( PREFT1 ASC                                                            
--MW        , DEXCPT ASC                                                            
--MW        , NPIECE ASC                                                            
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P991.RXFF881D ON P991.RTFF88                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFI101D ON P991.RTFI10                                      
        ( NMUTATION ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFI102D ON P991.RTFI10                                      
        ( DCOMPTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT121D ON P991.RTFT12                                      
        ( NJRN ASC                                                              
        , NEXER ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT122D ON P991.RTFT12                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT131D ON P991.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT133D ON P991.RTFT13                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT134D ON P991.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT141D ON P991.RTFT14                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFT211D ON P991.RTFT21                                      
        ( LETTRAGE DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT212D ON P991.RTFT21                                      
        ( COMPTE ASC                                                            
        , NSOC ASC                                                              
        , LETTRAGE ASC                                                          
        , CDEVISE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT214D ON P991.RTFT21                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT221D ON P991.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , SECTION ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT222D ON P991.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , RUBRIQUE ASC                                                          
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT261D ON P991.RTFT26                                      
        ( BAN ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT271D ON P991.RTFT27                                      
        ( NTBENEF ASC                                                           
        , NFOUR ASC                                                             
        , CTRESO ASC                                                            
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , NAUX ASC                                                              
        , NAUXR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT272D ON P991.RTFT27                                      
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFT310D ON P991.RTFT31                                      
        ( SECTION ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFT311 ON P991.RTFT31                                       
        ( RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT312D ON P991.RTFT31                                      
        ( SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT313D ON P991.RTFT31                                      
        ( RUBRIQUE ASC                                                          
        , SECTION ASC                                                           
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXFT760D ON P991.RTFT76                                      
        ( DFACTURE ASC                                                          
        , NFACTURE ASC                                                          
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXFT761D ON P991.RTFT76                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA001D ON P991.RTGA00                                      
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA002D ON P991.RTGA00                                      
        ( CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA003D ON P991.RTGA00                                      
        ( CMARQ ASC                                                             
        , LEMBALLAGE ASC                                                        
        , NCODIC ASC                                                            
        , CASSORT ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA004D ON P991.RTGA00                                      
        ( NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA005D ON P991.RTGA00                                      
        ( NCODIC ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA006D ON P991.RTGA00                                      
        ( CASSORT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA007D ON P991.RTGA00                                      
        ( CFAM ASC                                                              
        , CASSORT ASC                                                           
        , CAPPRO ASC                                                            
        , NCODIC ASC                                                            
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA008D ON P991.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA009D ON P991.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CASSORT ASC                                                           
        , D1RECEPT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA011A ON P991.RTGA01DA                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA011D ON P991.RTGA01                                      
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA011J ON P991.RTGA01DJ                                    
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA031D ON P991.RTGA03                                      
        ( NCODICK ASC                                                           
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA101D ON P991.RTGA10                                      
        ( NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA102D ON P991.RTGA10                                      
        ( CTYPLIEU ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA103D ON P991.RTGA10                                      
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA120D ON P991.RTGA12                                      
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA141D ON P991.RTGA14                                      
        ( WSEQFAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA213D ON P991.RTGA21                                      
        ( WRAYONFAM ASC                                                         
        , CRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA251D ON P991.RTGA25                                      
        ( CMARKETING ASC                                                        
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA302D ON P991.RTGA30                                      
        ( CPARAM ASC                                                            
        , LVPARAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA431D ON P991.RTGA43                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXGA490D ON P991.RTGA49                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGA561D ON P991.RTGA56                                      
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA581D ON P991.RTGA58                                      
        ( NCODICLIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA591D ON P991.RTGA59                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA592D ON P991.RTGA59                                      
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA621D ON P991.RTGA62                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA651D ON P991.RTGA65                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA652D ON P991.RTGA65                                      
        ( CSTATUT ASC                                                           
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA661D ON P991.RTGA66                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA681D ON P991.RTGA68                                      
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA751D ON P991.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGA752D ON P991.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGB301D ON P991.RTGB30                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGB801D ON P991.RTGB80                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG151D ON P991.RTGG15                                      
        ( NCODIC ASC                                                            
        , DDECISION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG170D ON P991.RTGG17                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGG201D ON P991.RTGG20                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG202D ON P991.RTGG20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET DESC                                                           
        , PEXPTTC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG203D ON P991.RTGG20                                      
        ( DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG204D ON P991.RTGG20                                      
        ( NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG210D ON P991.RTGG21                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG402D ON P991.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG403D ON P991.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
        , DFINEFFET ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGG601D ON P991.RTGG60                                      
        ( WTRAITE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXGG701D ON P991.RTGG70                                      
        ( NCODIC ASC                                                            
        , DREC DESC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXGG702D ON P991.RTGG70                                      
        ( DREC DESC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGM012D ON P991.RTGM01                                      
        ( CFAM ASC                                                              
        , NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGP040D ON P991.RTGP04                                      
        ( WTYPMVT ASC                                                           
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGQ014D ON P991.RTGQ01                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGQ015D ON P991.RTGQ01                                      
        ( CINSEE ASC                                                            
        , WCONTRA DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGQ031D ON P991.RTGQ03                                      
        ( CZONLIV ASC                                                           
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGQ061D ON P991.RTGQ06                                      
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGQ062D ON P991.RTGQ06                                      
        ( CINSEE ASC                                                            
        , CTYPE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGQ063D ON P991.RTGQ06                                      
        ( CPOSTAL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR301D ON P991.RTGR30                                      
        ( DMVTREC ASC                                                           
        , WTLMELA ASC                                                           
        , CTYPMVT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR302D ON P991.RTGR30                                      
        ( NENTCDE ASC                                                           
        , DJRECQUAI ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR303D ON P991.RTGR30                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR304D ON P991.RTGR30                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR351D ON P991.RTGR35                                      
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR353D ON P991.RTGR35                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR354D ON P991.RTGR35                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGR355D ON P991.RTGR35                                      
        ( NENTCDEPT ASC                                                         
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGS301D ON P991.RTGS30                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGS311D ON P991.RTGS31                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGS420D ON P991.RTGS42                                      
        ( NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
        , NSSLIEUORIG ASC                                                       
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NSSLIEUDEST ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGS422D ON P991.RTGS42                                      
        ( DMVT ASC                                                              
        , DHMVT ASC                                                             
        , DMMVT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGS601D ON P991.RTGS60                                      
        ( NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGS602D ON P991.RTGS60                                      
        ( NCODIC ASC                                                            
        , NSOCDEST ASC                                                          
        , NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , CSTATUTTRT ASC                                                        
        , LEMPLACT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGS603D ON P991.RTGS60                                      
        ( NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGS700D ON P991.RTGS70                                      
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGV021D ON P991.RTGV02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV022D ON P991.RTGV02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV023D ON P991.RTGV02                                      
        ( IDCLIENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV024D ON P991.RTGV02                                      
        ( TELBUR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV031D ON P991.RTGV03                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV050D ON P991.RTGV05                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGV081D ON P991.RTGV08                                      
        ( VALCTRL ASC                                                           
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV104D ON P991.RTGV10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV105D ON P991.RTGV10                                      
        ( NVENTO ASC                                                            
        , NLIEU ASC                                                             
        , TYPVTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV111D ON P991.RTGV11                                      
        ( CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTYPENREG ASC                                                         
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV112D ON P991.RTGV11                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV113D ON P991.RTGV11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV114D ON P991.RTGV11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV115D ON P991.RTGV11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV116D ON P991.RTGV11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV141D ON P991.RTGV14                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NTRANS DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV142D ON P991.RTGV14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV200D ON P991.RTGV20                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGV231D ON P991.RTGV23                                      
        ( DCREATION1 ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV271D ON P991.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXGV272D ON P991.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGV273D ON P991.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXGV300D ON P991.RTGV30                                      
        ( LNOM ASC                                                              
        , DVENTE ASC                                                            
        , DNAISSANCE ASC                                                        
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXGV352D ON P991.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NDOCENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV353D ON P991.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV355D ON P991.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NDOCENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV356D ON P991.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NBONENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXGV990D ON P991.RTGV99                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE001D ON P991.RTHE00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE002D ON P991.RTHE00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE011D ON P991.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE012D ON P991.RTHE01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE013D ON P991.RTHE01                                      
        ( NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE014D ON P991.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE101D ON P991.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE102D ON P991.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE103D ON P991.RTHE10                                      
        ( NCODIC ASC                                                            
        , NSERIE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE104D ON P991.RTHE10                                      
        ( PDOSNASC ASC                                                          
        , DOSNASC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE105D ON P991.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NENVOI ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE107D ON P991.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WSOLDE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE108D ON P991.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CREFUS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  INDEX P991.RXHE110D ON P991.RTHE11                                      
--MW        ( NLIEUHED ASC                                                          
--MW        , NGHE ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P991.RXHE111D ON P991.RTHE11                                      
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXHE202D ON P991.RTHE20                                      
        ( NUMPAL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHE221D ON P991.RTHE22                                      
        ( CLIEUHET ASC                                                          
        , CTIERS ASC                                                            
        , DENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHN001D ON P991.RTHN00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHN002D ON P991.RTHN00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHN003D ON P991.RTHN00                                      
        ( DTRAIT ASC                                                            
        , WSUPP ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHN011D ON P991.RTHN01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHN012D ON P991.RTHN01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHV011D ON P991.RTHV01                                      
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHV021D ON P991.RTHV02                                      
        ( DVENTECIALE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.RXHV071D ON P991.RTHV07                                      
        ( CETAT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHV081D ON P991.RTHV08                                      
        ( DVENTECIALE DESC                                                      
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXHV600D ON P991.RTHV60                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXHV960D ON P991.RTHV96                                      
        ( DTSTAT ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
        , CVENDEUR ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXIE001D ON P991.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , CMODSTOCK ASC                                                         
        , CEMP13 ASC                                                            
        , CEMP23 ASC                                                            
        , CEMP33 ASC                                                            
        , CTYPEMPL3 ASC                                                         
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  INDEX P991.RXIE002D ON P991.RTIE00                                      
--MW        ( NSOCDEPOT ASC                                                         
--MW        , NDEPOT ASC                                                            
--MW        , NCODIC3 ASC                                                           
--MW          )                                                                     
--MW           PCTFREE 5                                                            
--MW    ;                                                                           
CREATE  INDEX P991.RXIE003D ON P991.RTIE00                                      
        ( NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXIE004D ON P991.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXIE052D ON P991.RTIE05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXIE301D ON P991.RTIE30                                      
        ( "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXIN001D ON P991.RTIN00                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXIN050D ON P991.RTIN05                                      
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXIN901D ON P991.RTIN90                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , LIEUTRT ASC                                                           
        , DINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXKIS01D ON P991.RTKIS0                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXKIS11D ON P991.RTKIS1                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXKIS21D ON P991.RTKIS2                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXKIS31D ON P991.RTKIS3                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXLG001 ON P991.RTLG00                                       
        ( CINSEE ASC                                                            
        , LNOMPVOIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXLG002 ON P991.RTLG00                                       
        ( CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXLM000D ON P991.RTLM00                                      
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DATTRANS ASC                                                          
        , NCAISSE ASC                                                           
        , NTRANS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXPR001D ON P991.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXPR002D ON P991.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CASSORT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXPR011D ON P991.RTPR01                                      
        ( CFAM ASC                                                              
        , WAFFICH ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXPR061D ON P991.RTPR06                                      
        ( CSTATUT ASC                                                           
        , LSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXPR062D ON P991.RTPR06                                      
        ( CSTATUT ASC                                                           
        , DEFFET ASC                                                            
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXPR063D ON P991.RTPR06                                      
        ( CSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXPS001D ON P991.RTPS00                                      
        ( NOMCLI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXQC011D ON P991.RTQC01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXQC111D ON P991.RTQC11                                      
        ( DTESSI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXQC112D ON P991.RTQC11                                      
        ( NCLIENT ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.RXQC113D ON P991.RTQC11                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.RXQC121D ON P991.RTQC12                                      
        ( DENVOI_QV ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXRA001D ON P991.RTRA00                                      
        ( CTYPDEMANDE ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXRA002D ON P991.RTRA00                                      
        ( NAVOIRCLIENT ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXRD000D ON P991.RTRD00                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXRE600D ON P991.RTRE60                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXRE610D ON P991.RTRE61                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXRX251D ON P991.RTRX25                                      
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXRX300D ON P991.RTRX30                                      
        ( NRELEVE ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXRX551D ON P991.RTRX55                                      
        ( NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXSA02D ON P991.RTSA02                                       
        ( NINTERV ASC                                                           
        , CTIERS ASC                                                            
        , CTYPENRG ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXSA02XX ON P991.RTSA02X                                     
        ( NINTERV ASC                                                           
        , CTIERS ASC                                                            
        , CTYPENRG ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXSA03D ON P991.RTSA03                                       
        ( NINTERV ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXSA03XX ON P991.RTSA03X                                     
        ( NINTERV ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXSP101D ON P991.RTSP10                                      
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , SRP ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXSP200D ON P991.RTSP20                                      
        ( DTRAITEMENT ASC                                                       
        , CPROG ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXSP201D ON P991.RTSP20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXTL021D ON P991.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXTL022D ON P991.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXTL040D ON P991.RTTL04                                      
        ( NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXTL111D ON P991.RTTL11                                      
        ( NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXVA000D ON P991.RTVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXVA001D ON P991.RTVA00                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXVA700D ON P991.RTVA70                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , DOPER ASC                                                             
        , NORIGINE ASC                                                          
        , NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXVE021D ON P991.RTVE02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE022D ON P991.RTVE02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE102D ON P991.RTVE10                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE103D ON P991.RTVE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.RXVE111D ON P991.RTVE11                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE113D ON P991.RTVE11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE114D ON P991.RTVE11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE115D ON P991.RTVE11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE116D ON P991.RTVE11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVE141D ON P991.RTVE14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P991.RXVI010D ON P991.RTVI01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.SXVA000D ON P991.STVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE INDEX P991.XSTVA00 ON P991.STVA00
    ( NCODIC ASC
    , DOPER ASC
      )
       PCTFREE 10
;

CREATE  INDEX P991.ZXGA001D ON P991.ZTGA00                                      
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA002D ON P991.ZTGA00                                      
        ( CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA003D ON P991.ZTGA00                                      
        ( CMARQ ASC                                                             
        , LEMBALLAGE ASC                                                        
        , NCODIC ASC                                                            
        , CASSORT ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA004D ON P991.ZTGA00                                      
        ( NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA005D ON P991.ZTGA00                                      
        ( NCODIC ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA006D ON P991.ZTGA00                                      
        ( CASSORT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA007D ON P991.ZTGA00                                      
        ( CFAM ASC                                                              
        , CASSORT ASC                                                           
        , CAPPRO ASC                                                            
        , NCODIC ASC                                                            
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA008D ON P991.ZTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA009D ON P991.ZTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CASSORT ASC                                                           
        , D1RECEPT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA101D ON P991.ZTGA10                                      
        ( NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA102D ON P991.ZTGA10                                      
        ( CTYPLIEU ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA103D ON P991.ZTGA10                                      
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.ZXGA141D ON P991.ZTGA14                                      
        ( WSEQFAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD060D ON P991.RTAD06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD070D ON P991.RTAD07                               
        ( CSOC ASC                                                              
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD071D ON P991.RTAD07                               
        ( NENTCDEK ASC                                                          
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD100D ON P991.RTAD10                               
        ( CSOC ASC                                                              
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD101D ON P991.RTAD10                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD102D ON P991.RTAD10                               
        ( NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD110D ON P991.RTAD11                               
        ( CSOC ASC                                                              
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD111D ON P991.RTAD11                               
        ( CFAMK ASC                                                             
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD140D ON P991.RTAD14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD510D ON P991.RTAD51                               
        ( CSOC ASC                                                              
        , CCOULEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD511D ON P991.RTAD51                               
        ( CCOULEURK ASC                                                         
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD550D ON P991.RTAD55                               
        ( CUSINE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD610D ON P991.RTAD61                               
        ( CSOC ASC                                                              
        , CPAYS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAD611D ON P991.RTAD61                               
        ( CPAYSK ASC                                                            
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAI240D ON P991.RTAI24                               
        ( FDCLAS ASC                                                            
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAI270D ON P991.RTAI27                               
        ( CDESCRIPTIF ASC                                                       
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAI530D ON P991.RTAI53                               
        ( FSSKU ASC                                                             
        , FSCTL ASC                                                             
        , FSCLAS ASC                                                            
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAN010D ON P991.RTAN01                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
        , CDEST ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAV010D ON P991.RTAV01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAV020D ON P991.RTAV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXAV030D ON P991.RTAV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBA000D ON P991.RTBA00                               
        ( NFACTBA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBA010D ON P991.RTBA01                               
        ( NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBA100D ON P991.RTBA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , CMODRGLT ASC                                                          
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBA200D ON P991.RTBA20                               
        ( CTIERSBA ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBA210D ON P991.RTBA21                               
        ( CTIERSBA ASC                                                          
        , DANNEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBA300D ON P991.RTBA30                               
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBC310 ON P991.RTBC31                                
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBC320D ON P991.RTBC32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBC330D ON P991.RTBC33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBC340D ON P991.RTBC34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXBC350D ON P991.RTBC35                               
        ( CPROGRAMME ASC                                                        
        , DDEBUT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCC000 ON P991.RTCC00                                
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCC010D ON P991.RTCC01                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCC020 ON P991.RTCC02                                
        ( DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCC030D ON P991.RTCC03                               
        ( TYPTRANS ASC                                                          
        , MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NOPERATEUR ASC                                                        
        , HTRAIT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCC040 ON P991.RTCC04                                
        ( CPAICPT ASC                                                           
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCC050 ON P991.RTCC05                                
        ( NSOC ASC                                                              
        , DJOUR ASC                                                             
        , NSEQ ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD000D ON P991.RTCD00                               
        ( NCODIC ASC                                                            
        , NMAGGRP ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD050D ON P991.RTCD05                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD100D ON P991.RTCD10                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD150D ON P991.RTCD15                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD200D ON P991.RTCD20                               
        ( NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD250D ON P991.RTCD25                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD300D ON P991.RTCD30                               
        ( DCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD350D ON P991.RTCD35                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCD500D ON P991.RTCD50                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCE100D ON P991.RTCE10                               
        ( NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCE200D ON P991.RTCE20                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCT000D ON P991.RTCT00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DDELIV ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXCX000D ON P991.RTCX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , COMPTECG ASC                                                          
        , DATEFF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXDC000D ON P991.RTDC00                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXDC100D ON P991.RTDC10                               
        ( NDOSSIER ASC                                                          
        , NECHEANCE ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXDC200D ON P991.RTDC20                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXDS000D ON P991.RTDS00                               
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CAPPRO ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXDS050D ON P991.RTDS05                               
        ( NCODIC ASC                                                            
        , DSEMESTRE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXDS100D ON P991.RTDS10                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEF100D ON P991.RTEF10                               
        ( CTRAIT ASC                                                            
        , CTIERS ASC                                                            
        , NRENDU ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG000D ON P991.RTEG00                               
        ( NOMETAT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG050D ON P991.RTEG05                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG100D ON P991.RTEG10                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG110D ON P991.RTEG11                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG150D ON P991.RTEG15                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG200D ON P991.RTEG20                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG250D ON P991.RTEG25                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , LIGNECALC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG300D ON P991.RTEG30                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXEG400D ON P991.RTEG40                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFA010D ON P991.RTFA01                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFA020D ON P991.RTFA02                               
        ( NSOCV ASC                                                             
        , NLIEUV ASC                                                            
        , NORDV ASC                                                             
        , NSOCF ASC                                                             
        , NLIEUF ASC                                                            
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF000D ON P991.RTFF00                               
        ( NPIECE ASC                                                            
        , DEXCPT DESC                                                           
        , PREFT1 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF050D ON P991.RTFF05                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF060D ON P991.RTFF06                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF100D ON P991.RTFF10                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF110D ON P991.RTFF11                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF120D ON P991.RTFF12                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF130D ON P991.RTFF13                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF150D ON P991.RTFF15                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF200D ON P991.RTFF20                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNEFAC ASC                                                         
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF210D ON P991.RTFF21                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF250D ON P991.RTFF25                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , WTYPESEQ ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF300D ON P991.RTFF30                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF350D ON P991.RTFF35                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF360D ON P991.RTFF36                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF370D ON P991.RTFF37                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF400D ON P991.RTFF40                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF410D ON P991.RTFF41                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF420D ON P991.RTFF42                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF430D ON P991.RTFF43                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF440D ON P991.RTFF44                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF450D ON P991.RTFF45                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF500D ON P991.RTFF50                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF510D ON P991.RTFF51                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF600D ON P991.RTFF60                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF650D ON P991.RTFF65                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF660D ON P991.RTFF66                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF670D ON P991.RTFF67                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF700D ON P991.RTFF70                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF750D ON P991.RTFF75                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF800D ON P991.RTFF80                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF810D ON P991.RTFF81                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFF850D ON P991.RTFF85                               
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFI000D ON P991.RTFI00                               
        ( CPROF ASC                                                             
        , QTAUX ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFI050D ON P991.RTFI05                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM010D ON P991.RTFM01                               
        ( NENTITE ASC                                                           
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM020D ON P991.RTFM02                               
        ( CDEVISE ASC                                                           
        , WSENS ASC                                                             
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM030D ON P991.RTFM03                               
        ( NENTITE ASC                                                           
        , CMETHODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM040D ON P991.RTFM04                               
        ( CDEVISE ASC                                                           
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM050D ON P991.RTFM05                               
        ( CDEVORIG ASC                                                          
        , CDEVDEST ASC                                                          
        , DEFFET ASC                                                            
        , COPERAT ASC                                                           
        , PTAUX ASC                                                             
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM060D ON P991.RTFM06                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM100D ON P991.RTFM10                               
        ( NENTITE ASC                                                           
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM200D ON P991.RTFM20                               
        ( NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM230D ON P991.RTFM23                               
        ( NENTITE ASC                                                           
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM540D ON P991.RTFM54                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM550D ON P991.RTFM55                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM560D ON P991.RTFM56                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM570D ON P991.RTFM57                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM580D ON P991.RTFM58                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM590D ON P991.RTFM59                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM641D ON P991.RTFM64                               
        ( STEAPP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM691 ON P991.RTFM69                                
        ( COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM730D ON P991.RTFM73                               
        ( CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM801D ON P991.RTFM80                               
        ( CNATOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM811D ON P991.RTFM81                               
        ( CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM821D ON P991.RTFM82                               
        ( CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM831D ON P991.RTFM83                               
        ( CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM841D ON P991.RTFM84                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CTVA ASC                                                              
        , CGEO ASC                                                              
        , WGROUPE ASC                                                           
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM871D ON P991.RTFM87                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM881D ON P991.RTFM88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM891D ON P991.RTFM89                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM901D ON P991.RTFM90                               
        ( CINTERFACE ASC                                                        
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM910D ON P991.RTFM91                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM921D ON P991.RTFM92                               
        ( CZONE ASC                                                             
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFM930D ON P991.RTFM93                               
        ( CRITLIEU1 ASC                                                         
        , CRITLIEU2 ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT010D ON P991.RTFT01                               
        ( NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT020D ON P991.RTFT02                               
        ( CACID ASC                                                             
        , NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT030D ON P991.RTFT03                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT040D ON P991.RTFT04                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT050D ON P991.RTFT05                               
        ( CBANQUE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT060D ON P991.RTFT06                               
        ( NSOC ASC                                                              
        , CPTR ASC                                                              
        , NAUX ASC                                                              
        , CREGTVA ASC                                                           
        , NATURE ASC                                                            
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT070D ON P991.RTFT07                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT080D ON P991.RTFT08                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT090D ON P991.RTFT09                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT100D ON P991.RTFT10                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT110D ON P991.RTFT11                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , WTYPEADR ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT120D ON P991.RTFT12                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT130D ON P991.RTFT13                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT140D ON P991.RTFT14                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NREG ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT150D ON P991.RTFT15                               
        ( NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT160D ON P991.RTFT16                               
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT170D ON P991.RTFT17                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT180D ON P991.RTFT18                               
        ( METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT190D ON P991.RTFT19                               
        ( CTRAIT ASC                                                            
        , CACID ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT200D ON P991.RTFT20                               
        ( NAUX ASC                                                              
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , CTRESO ASC                                                            
        , WACCES ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT210D ON P991.RTFT21                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NLIGNE ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT220D ON P991.RTFT22                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
        , SSCOMPTE ASC                                                          
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , STEAPP ASC                                                            
        , CDEVISE ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , WSENS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT230D ON P991.RTFT23                               
        ( NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT250D ON P991.RTFT25                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT260D ON P991.RTFT26                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT270D ON P991.RTFT27                               
        ( NLIGNE ASC                                                            
        , NPIECE ASC                                                            
        , DECH ASC                                                              
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NAUXR ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT280D ON P991.RTFT28                               
        ( METHREG ASC                                                           
        , NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT290D ON P991.RTFT29                               
        ( NCODE ASC                                                             
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT300D ON P991.RTFT30                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT320D ON P991.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT321D ON P991.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT330D ON P991.RTFT33                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
        , NVENTIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT340D ON P991.RTFT34                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT350 ON P991.RTFT35                                
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , WTRT ASC                                                              
        , NEXER ASC                                                             
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT360 ON P991.RTFT36                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT370 ON P991.RTFT37                                
        ( NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT380 ON P991.RTFT38                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT390 ON P991.RTFT39                                
        ( NLIGNE ASC                                                            
        , REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT400 ON P991.RTFT40                                
        ( CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT410 ON P991.RTFT41                                
        ( NATCH ASC                                                             
        , CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT430D ON P991.RTFT43                               
        ( NPIECE DESC                                                           
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , CDEVISE ASC                                                           
        , NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT440D ON P991.RTFT44                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT450 ON P991.RTFT45                                
        ( CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT460 ON P991.RTFT46                                
        ( NPER ASC                                                              
        , CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT490D ON P991.RTFT49                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT500D ON P991.RTFT50                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT510D ON P991.RTFT51                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT520D ON P991.RTFT52                               
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT530D ON P991.RTFT53                               
        ( CACID ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT540D ON P991.RTFT54                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT570D ON P991.RTFT57                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
        , WTYPEADR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT601D ON P991.RTFT60                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT611D ON P991.RTFT61                               
        ( SECTION ASC                                                           
        , NSOC ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT621D ON P991.RTFT62                               
        ( RUBRIQUE ASC                                                          
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT631D ON P991.RTFT63                               
        ( NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT651D ON P991.RTFT65                               
        ( CMASQUEC ASC                                                          
        , CMASQUES ASC                                                          
        , CMASQUER ASC                                                          
        , CMASQUEE ASC                                                          
        , CMASQUEA ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT660D ON P991.RTFT66                               
        ( COMPTE ASC                                                            
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT670D ON P991.RTFT67                               
        ( SECTION ASC                                                           
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT680D ON P991.RTFT68                               
        ( RUBRIQUE ASC                                                          
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT700D ON P991.RTFT70                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT710D ON P991.RTFT71                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT720D ON P991.RTFT72                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT730D ON P991.RTFT73                               
        ( NSOC ASC                                                              
        , CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT740D ON P991.RTFT74                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , CNOMPROG ASC                                                          
        , DDATE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT750D ON P991.RTFT75                               
        ( DDEMANDE DESC                                                         
        , NTIERS ASC                                                            
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT851D ON P991.RTFT85                               
        ( CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT861D ON P991.RTFT86                               
        ( DEFFET ASC                                                            
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT871D ON P991.RTFT87                               
        ( SECTIONO ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT881D ON P991.RTFT88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT891D ON P991.RTFT89                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFT900D ON P991.RTFT90                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX000D ON P991.RTFX00                               
        ( NOECS DESC                                                            
        , DATEFF DESC                                                           
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX001D ON P991.RTFX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
        , DATEFF DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX010D ON P991.RTFX01                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX050D ON P991.RTFX05                               
        ( NJRN ASC                                                              
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX100D ON P991.RTFX10                               
        ( COMPTEGL ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX110D ON P991.RTFX11                               
        ( CZONE ASC                                                             
        , ZONEGL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX150D ON P991.RTFX15                               
        ( WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX200D ON P991.RTFX20                               
        ( CTYPSTAT ASC                                                          
        , NDEPTLMELA ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX300D ON P991.RTFX30                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX350D ON P991.RTFX35                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX400D ON P991.RTFX40                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX450D ON P991.RTFX45                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXFX500D ON P991.RTFX50                               
        ( DATTRANS ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CTYPTRANS ASC                                                         
        , CAPPRET ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA000D ON P991.RTGA00                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA010A ON P991.RTGA01DA                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA010D ON P991.RTGA01                               
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA010J ON P991.RTGA01DJ                             
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA030D ON P991.RTGA03                               
        ( CSOC ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA040D ON P991.RTGA04                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA060D ON P991.RTGA06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA070D ON P991.RTGA07                               
        ( CGRPFOURN ASC                                                         
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA080D ON P991.RTGA08                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA090D ON P991.RTGA09                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CMARKETIN3 ASC                                                        
        , CVMARKETIN3 ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA100D ON P991.RTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA110D ON P991.RTGA11                               
        ( CETAT ASC                                                             
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA130D ON P991.RTGA13                               
        ( CFAM ASC                                                              
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA140D ON P991.RTGA14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA150D ON P991.RTGA15                               
        ( CFAM ASC                                                              
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA160D ON P991.RTGA16                               
        ( CFAM ASC                                                              
        , CGARANTCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA170D ON P991.RTGA17                               
        ( CFAM ASC                                                              
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA180D ON P991.RTGA18                               
        ( CFAM ASC                                                              
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA200D ON P991.RTGA20                               
        ( CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA210D ON P991.RTGA21                               
        ( CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA211D ON P991.RTGA21                               
        ( WRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA220D ON P991.RTGA22                               
        ( CMARQ ASC                                                             
        , LMARQ ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA230D ON P991.RTGA23                               
        ( CMARKETING ASC                                                        
        , LMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA240D ON P991.RTGA24                               
        ( CDESCRIPTIF ASC                                                       
        , LDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA250D ON P991.RTGA25                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA260D ON P991.RTGA26                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA270D ON P991.RTGA27                               
        ( CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA290D ON P991.RTGA29                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  UNIQUE INDEX P991.RXGA300D ON P991.RTGA30                               
--MW        ( CPARAM ASC                                                            
--MW        , CFAM ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA301D ON P991.RTGA30                               
        ( CPARAM ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA310D ON P991.RTGA31                               
        ( NCODIC ASC                                                            
        , NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA320D ON P991.RTGA32                               
        ( NCODIC ASC                                                            
        , NSEQUENCE ASC                                                         
        , NEANCOMPO ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA330D ON P991.RTGA33                               
        ( NCODIC ASC                                                            
        , CDEST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA350D ON P991.RTGA35                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CPARAM ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA390D ON P991.RTGA39                               
        ( NENTSAP ASC                                                           
        , NENTANC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA400D ON P991.RTGA40                               
        ( CGARANTIE ASC                                                         
        , CFAM ASC                                                              
        , CTARIF ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA410D ON P991.RTGA41                               
        ( CGCPLT ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA420D ON P991.RTGA42                               
        ( CGCPLT ASC                                                            
        , NMOIS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA430D ON P991.RTGA43                               
        ( CSECTEUR ASC                                                          
        , CINSEE ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA440D ON P991.RTGA44                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA450D ON P991.RTGA45                               
        ( CFAM ASC                                                              
        , CGRP ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA460D ON P991.RTGA46                               
        ( CGRP ASC                                                              
        , CSECT1 ASC                                                            
        , ZONE1 ASC                                                             
        , CSECT2 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA480D ON P991.RTGA48                               
        ( NCODIC ASC                                                            
        , SPPRTP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA510D ON P991.RTGA51                               
        ( NCODIC ASC                                                            
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA520D ON P991.RTGA52                               
        ( NCODIC ASC                                                            
        , CGARANTCPLT ASC                                                       
        , CVGARANCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA530D ON P991.RTGA53                               
        ( NCODIC ASC                                                            
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA550D ON P991.RTGA55                               
        ( NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA560D ON P991.RTGA56                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA580D ON P991.RTGA58                               
        ( NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , CTYPLIEN ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA590D ON P991.RTGA59                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA600D ON P991.RTGA60                               
        ( NCODIC ASC                                                            
        , NMESSDEPOT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA620D ON P991.RTGA62                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA630D ON P991.RTGA63                               
        ( NCODIC ASC                                                            
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA640D ON P991.RTGA64                               
        ( NCODIC ASC                                                            
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA650D ON P991.RTGA65                               
        ( NCODIC ASC                                                            
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA660D ON P991.RTGA66                               
        ( NCODIC ASC                                                            
        , CSENS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA680D ON P991.RTGA68                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA710D ON P991.RTGA71                               
        ( CTABLE ASC                                                            
        , CSTABLE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA720D ON P991.RTGA72                               
        ( CNUMCONT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA730D ON P991.RTGA73                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA740D ON P991.RTGA74                               
        ( NCODIC ASC                                                            
        , NTITRE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA750D ON P991.RTGA75                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA760D ON P991.RTGA76                               
        ( CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA770D ON P991.RTGA77                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA790D ON P991.RTGA79                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA810D ON P991.RTGA81                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA830D ON P991.RTGA83                               
        ( NCODIC ASC                                                            
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA910D ON P991.RTGA91                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA920D ON P991.RTGA92                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA930D ON P991.RTGA93                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA940D ON P991.RTGA94                               
        ( CPROG ASC                                                             
        , CCHAMP ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGA990D ON P991.RTGA99                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGB300D ON P991.RTGB30                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGB800D ON P991.RTGB80                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGD990D ON P991.RTGD99                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGF350D ON P991.RTGF35                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGF400D ON P991.RTGF40                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
        , WMARQFAM ASC                                                          
        , CMARQFAM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGF500D ON P991.RTGF50                               
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGGDD0D ON P991.RTGGDD                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGGXX0D ON P991.RTGGXX                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG050D ON P991.RTGG05                               
        ( NCONC ASC                                                             
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG100D ON P991.RTGG10                               
        ( NDEMALI ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG150D ON P991.RTGG15                               
        ( NDEMALI ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG200D ON P991.RTGG20                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG250D ON P991.RTGG25                               
        ( CHEFPROD ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG301D ON P991.RTGG30                               
        ( DANNEE ASC                                                            
        , DMOIS ASC                                                             
        , CFAM ASC                                                              
        , CHEFPROD ASC                                                          
        , NCONC ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG400D ON P991.RTGG40                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG500D ON P991.RTGG50                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG550D ON P991.RTGG55                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG600D ON P991.RTGG60                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , DDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGG700D ON P991.RTGG70                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI1000D ON P991.RTGI10                              
        ( NSOCIETE ASC                                                          
        , CTABINT ASC                                                           
        , TTABINT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI1200D ON P991.RTGI12                              
        ( NSOCIETE ASC                                                          
        , CTABINTMB ASC                                                         
        , MB ASC                                                                
        , TMB ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI1400D ON P991.RTGI14                              
        ( NSOCIETE ASC                                                          
        , CTABINTCA ASC                                                         
        , CA ASC                                                                
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI1600D ON P991.RTGI16                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI2000D ON P991.RTGI20                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI2100D ON P991.RTGI21                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI2200D ON P991.RTGI22                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI2300D ON P991.RTGI23                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGI2400D ON P991.RTGI24                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGJ100D ON P991.RTGJ10                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGJ400D ON P991.RTGJ40                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGM010D ON P991.RTGM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGM060D ON P991.RTGM06                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , CZPRIX ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGM700D ON P991.RTGM70                               
        ( NSOCIETE ASC                                                          
        , NZONE ASC                                                             
        , CHEFPROD ASC                                                          
        , WSEQFAM ASC                                                           
        , LVMARKET1 ASC                                                         
        , LVMARKET2 ASC                                                         
        , LVMARKET3 ASC                                                         
        , NCODIC2 ASC                                                           
        , NCODIC ASC                                                            
        , "SEQUENCE" ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGP000D ON P991.RTGP00                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGP010D ON P991.RTGP01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGP020D ON P991.RTGP02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , DLIVRAISON ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGP030D ON P991.RTGP03                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGP041D ON P991.RTGP04                               
        ( NCODIC ASC                                                            
        , WTYPMVT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGP050D ON P991.RTGP05                               
        ( CRAYON ASC                                                            
        , WRAYONFAM ASC                                                         
        , DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ000D ON P991.RTGQ00                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ011D ON P991.RTGQ01                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ020D ON P991.RTGQ02                               
        ( CMODDEL ASC                                                           
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ030D ON P991.RTGQ03                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
        , CZONLIV ASC                                                           
        , CPLAGE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ040D ON P991.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , DEFFET ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ041D ON P991.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ120D ON P991.RTGQ12                               
        ( CINSEE ASC                                                            
        , CGRP ASC                                                              
        , CTYPGRP ASC                                                           
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGQ200D ON P991.RTGQ20                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
        , CCADRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGR300D ON P991.RTGR30                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGR350D ON P991.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGR352D ON P991.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGS300D ON P991.RTGS30                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGS310D ON P991.RTGS31                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGS450D ON P991.RTGS45                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGS500D ON P991.RTGS50                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGS550D ON P991.RTGS55                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGS600D ON P991.RTGS60                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV020D ON P991.RTGV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV030D ON P991.RTGV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV040D ON P991.RTGV04                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV060D ON P991.RTGV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV080D ON P991.RTGV08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV100D ON P991.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV101D ON P991.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV102D ON P991.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV110D ON P991.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV119D ON P991.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV130D ON P991.RTGV13                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV140D ON P991.RTGV14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV150D ON P991.RTGV15                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV230D ON P991.RTGV23                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV250D ON P991.RTGV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV260D ON P991.RTGV26                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV270D ON P991.RTGV27                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV310D ON P991.RTGV31                               
        ( CVENDEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV350D ON P991.RTGV35                               
        ( NSOCIETE ASC                                                          
        , NBONENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV400D ON P991.RTGV40                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXGV410D ON P991.RTGV41                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CTYPADR ASC                                                           
        , CMODDEL ASC                                                           
        , DDELIV ASC                                                            
        , CPLAGE ASC                                                            
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHA300D ON P991.RTHA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHC000D ON P991.RTHC00                               
        ( NCHS ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHC030D ON P991.RTHC03                               
        ( NCHS ASC                                                              
        , NSEQ ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE000D ON P991.RTHE00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE010D ON P991.RTHE01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE100D ON P991.RTHE10                               
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE150D ON P991.RTHE15                               
        ( CLIEUHET ASC                                                          
        , NLIEUHED ASC                                                          
        , NGHE ASC                                                              
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE200D ON P991.RTHE20                               
        ( NLIEUHED ASC                                                          
        , CLIEUHET ASC                                                          
        , NRETOUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE210D ON P991.RTHE21                               
        ( CLIEUHET ASC                                                          
        , CLIEUHEI ASC                                                          
        , NLOTDIAG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE220D ON P991.RTHE22                               
        ( CLIEUHET ASC                                                          
        , NENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHE230D ON P991.RTHE23                               
        ( CLIEUHET ASC                                                          
        , NDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHL000D ON P991.RTHL00                               
        ( NOMMAP ASC                                                            
        , POSIT ASC                                                             
        , ZECRAN ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHL050D ON P991.RTHL05                               
        ( CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHL100D ON P991.RTHL10                               
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
        , NREQUETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHL150 ON P991.RTHL15                                
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , NOCCUR ASC                                                            
        , NSEQCRIT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHN000D ON P991.RTHN00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHN010D ON P991.RTHN01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHN110D ON P991.RTHN11                               
        ( CTIERS ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV000D ON P991.RTHV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CMODDEL ASC                                                           
        , DOPER ASC                                                             
        , WSEQFAM ASC                                                           
        , CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV010D ON P991.RTHV01                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV012D ON P991.RTHV01                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV020D ON P991.RTHV02                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV030 ON P991.RTHV03                                
        ( DVENTECIALE ASC                                                       
        , CPRESTATION ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV060D ON P991.RTHV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV070D ON P991.RTHV07                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV080D ON P991.RTHV08                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV090D ON P991.RTHV09                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV100D ON P991.RTHV10                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV110D ON P991.RTHV11                               
        ( NSOCIETE ASC                                                          
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV150D ON P991.RTHV15                               
        ( CPROGRAMME ASC                                                        
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTECIALE ASC                                                       
        , CTERMID ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV160D ON P991.RTHV16                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV170D ON P991.RTHV17                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV180D ON P991.RTHV18                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV200D ON P991.RTHV20                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV250D ON P991.RTHV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DGRPMOIS ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV260D ON P991.RTHV26                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV270D ON P991.RTHV27                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV280D ON P991.RTHV28                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , WSEQPRO ASC                                                           
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV290D ON P991.RTHV29                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV300D ON P991.RTHV30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , NAGREGATED ASC                                                        
        , DSVENTECIALE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV320D ON P991.RTHV32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV330D ON P991.RTHV33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV340D ON P991.RTHV34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV350D ON P991.RTHV35                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV700D ON P991.RTHV70                               
        ( NSOCIETE ASC                                                          
        , DMOIS ASC                                                             
        , NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXHV920D ON P991.RTHV92                               
        ( NSOCIETE ASC                                                          
        , MAGASIN ASC                                                           
        , NCODIC ASC                                                            
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIA000D ON P991.RTIA00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIA050D ON P991.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIA051D ON P991.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIA600D ON P991.RTIA60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE000D ON P991.RTIE00                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NFICHE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE050D ON P991.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE051D ON P991.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE100D ON P991.RTIE10                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE200D ON P991.RTIE20                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE250D ON P991.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE251D ON P991.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE300D ON P991.RTIE30                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE350D ON P991.RTIE35                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE360D ON P991.RTIE36                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE450D ON P991.RTIE45                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CZONE ASC                                                             
        , CSECTEUR ASC                                                          
        , NSOUSSECT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE600D ON P991.RTIE60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE700D ON P991.RTIE70                               
        ( NHS ASC                                                               
        , NLIEUHS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE800D ON P991.RTIE80                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIE900D ON P991.RTIE90                               
        ( NDEPOT ASC                                                            
        , NSOCDEPOT ASC                                                         
        , CREGROUP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIJ000D ON P991.RTIJ00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIN000D ON P991.RTIN00                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , WSTOCKMASQ ASC                                                        
        , CSTATUT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIN100D ON P991.RTIN01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , SSLIEU ASC                                                            
        , CSTATUT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIN900D ON P991.RTIN90                               
        ( DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIT000D ON P991.RTIT00                               
        ( NINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIT050D ON P991.RTIT05                               
        ( NINVENTAIRE ASC                                                       
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIT100D ON P991.RTIT10                               
        ( NINVENTAIRE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIT150D ON P991.RTIT15                               
        ( NINVENTAIRE ASC                                                       
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXIT900D ON P991.RTIT90                               
        ( NINVENTAIRE ASC                                                       
        , NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXLG000 ON P991.RTLG00                                
        ( CINSEE ASC                                                            
        , CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXLG010D ON P991.RTLG01                               
        ( CINSEE ASC                                                            
        , NVOIE ASC                                                             
        , CPARITE ASC                                                           
        , NDEBTR ASC                                                            
        , NFINTR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXLM100D ON P991.RTLM10                               
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXLT000D ON P991.RTLT00                               
        ( NLETTRE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXLT010D ON P991.RTLT01                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXLT020D ON P991.RTLT02                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
        , INDVAR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMG010D ON P991.RTMG01                               
        ( CFAMMGI ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMO010D ON P991.RTMO01                               
        ( TYPOFFRE ASC                                                          
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMQ150D ON P991.RTMQ15                               
        ( CPROGRAMME ASC                                                        
        , CFONCTION ASC                                                         
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , DOPERATION ASC                                                        
        , "COMMENT" ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMQ160D ON P991.RTMQ16                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU050D ON P991.RTMU05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU210D ON P991.RTMU21                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU250D ON P991.RTMU25                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU260D ON P991.RTMU26                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU300D ON P991.RTMU30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU310D ON P991.RTMU31                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU350D ON P991.RTMU35                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXMU500D ON P991.RTMU50                               
        ( CTYPCDE ASC                                                           
        , NSEQED ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPA700D ON P991.RTPA70                               
        ( NSOCIETE ASC                                                          
        , CPTSAV ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPC080D ON P991.RTPC08                               
        ( RANDOMNUM ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPC090D ON P991.RTPC09                               
        ( RANDOMNUM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPM000D ON P991.RTPM00                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPM060D ON P991.RTPM06                               
        ( CMOPAI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPP000D ON P991.RTPP00                               
        ( NLIEU ASC                                                             
        , DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR000 ON P991.RTPR00                                
        ( CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR010 ON P991.RTPR01                                
        ( CFAM ASC                                                              
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR020 ON P991.RTPR02                                
        ( NCODIC ASC                                                            
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR030 ON P991.RTPR03                                
        ( CPRESTATION ASC                                                       
        , CGESTION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR031D ON P991.RTPR03                               
        ( CGESTION ASC                                                          
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR040 ON P991.RTPR04                                
        ( CPRESTATION ASC                                                       
        , CPRESTELT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR050 ON P991.RTPR05                                
        ( CPRESTATION ASC                                                       
        , CGRPETAT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR060 ON P991.RTPR06                                
        ( CPRESTATION ASC                                                       
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR100 ON P991.RTPR10                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR110 ON P991.RTPR11                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR120 ON P991.RTPR12                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR130 ON P991.RTPR13                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR140 ON P991.RTPR14                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR150 ON P991.RTPR15                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR160 ON P991.RTPR16                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR170 ON P991.RTPR17                                
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPR200 ON P991.RTPR20                                
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CAGREPRE ASC                                                          
        , WSEQED ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPS000D ON P991.RTPS00                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPS010D ON P991.RTPS01                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPS020D ON P991.RTPS02                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPS030D ON P991.RTPS03                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPT030D ON P991.RTPT03                               
        ( CODLANG ASC                                                           
        , CNOMPGRM ASC                                                          
        , NSEQLIB ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXPV000D ON P991.RTPV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC010D ON P991.RTQC01                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC020D ON P991.RTQC02                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC030 ON P991.RTQC03                                
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC050D ON P991.RTQC05                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC060D ON P991.RTQC06                               
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC100D ON P991.RTQC10                               
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CREGGAR ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC110D ON P991.RTQC11                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC120D ON P991.RTQC12                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXQC130D ON P991.RTQC13                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
        , CQUESTION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRA000D ON P991.RTRA00                               
        ( NDEMANDE ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRA010D ON P991.RTRA01                               
        ( NDEMANDE ASC                                                          
        , NSEQCODIC ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRA020D ON P991.RTRA02                               
        ( CTYPREGL ASC                                                          
        , NREGL ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRC000D ON P991.RTRC00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRC100D ON P991.RTRC10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRE000D ON P991.RTRE00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRE100D ON P991.RTRE10                               
        ( NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WTLMELA ASC                                                           
        , NAGREGATED ASC                                                        
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX000D ON P991.RTRX00                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX050D ON P991.RTRX05                               
        ( NCONC ASC                                                             
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX100D ON P991.RTRX10                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , NFREQUENCE ASC                                                        
        , NSEQUENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX150D ON P991.RTRX15                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , DSUPPORT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX200D ON P991.RTRX20                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX210D ON P991.RTRX21                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX220D ON P991.RTRX22                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX250D ON P991.RTRX25                               
        ( NRELEVE ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX450D ON P991.RTRX45                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX500D ON P991.RTRX50                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXRX550D ON P991.RTRX55                               
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSA01D ON P991.RTSA01                                
        ( NINTERV ASC                                                           
        , NCENTRE ASC                                                           
        , DDEPOT ASC                                                            
        , IDISPO ASC                                                            
        , HDISPO ASC                                                            
        , DDEVIS ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSA01XX ON P991.RTSA01X                              
        ( NINTERV ASC                                                           
        , NCENTRE ASC                                                           
        , DDEPOT ASC                                                            
        , IDISPO ASC                                                            
        , HDISPO ASC                                                            
        , DDEVIS ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSB110D ON P991.RTSB11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSG020D ON P991.RTSG02                               
        ( SECPAIE ASC                                                           
        , CSECTION ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSG030D ON P991.RTSG03                               
        ( COMPTE ASC                                                            
        , NETABADM ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSL400D ON P991.RTSL40                               
        ( NSOCIETE ASC                                                          
        , CLIEUINV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSL410D ON P991.RTSL41                               
        ( CLIEUINV ASC                                                          
        , NREGL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSL500D ON P991.RTSL50                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CLIEUINV ASC                                                          
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSL510D ON P991.RTSL51                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CEMPL ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSM010D ON P991.RTSM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSM020D ON P991.RTSM02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSP000D ON P991.RTSP00                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSP050D ON P991.RTSP05                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSP100D ON P991.RTSP10                               
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , NREC ASC                                                              
        , CMAJ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSP150D ON P991.RTSP15                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSP250D ON P991.RTSP25                               
        ( CMARQ ASC                                                             
        , NSOCDEST ASC                                                          
        , DPROV ASC                                                             
        , WTEBRB ASC                                                            
        , CTAUXTVA ASC                                                          
        , NLIEUDEST ASC                                                         
        , NLIEUORIG ASC                                                         
        , NSOCORIG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXSV010D ON P991.RTSV01                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CTPSAV ASC                                                            
        , DEFFET ASC                                                            
        , NSOCSAV ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXTF300D ON P991.RTTF30                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXTF350D ON P991.RTTF35                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXTL020D ON P991.RTTL02                               
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXTL060D ON P991.RTTL06                               
        ( NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXTL080D ON P991.RTTL08                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXTL090D ON P991.RTTL09                               
        ( CLIVR ASC                                                             
        , DMMLIVR ASC                                                           
        , CRETOUR ASC                                                           
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXTL110D ON P991.RTTL11                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA050D ON P991.RTVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA051D ON P991.RTVA05                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA100D ON P991.RTVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA150D ON P991.RTVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA200D ON P991.RTVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA250D ON P991.RTVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA300D ON P991.RTVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVA500D ON P991.RTVA50                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE020D ON P991.RTVE02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE050D ON P991.RTVE05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE060D ON P991.RTVE06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE080D ON P991.RTVE08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE100D ON P991.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE101D ON P991.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE110D ON P991.RTVE11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.RXVE140D ON P991.RTVE14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.SXVA050D ON P991.STVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.SXVA100D ON P991.STVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.SXVA150D ON P991.STVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.SXVA200D ON P991.STVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.SXVA250D ON P991.STVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.SXVA300D ON P991.STVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.ZXGA000D ON P991.ZTGA00                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.ZXGA100D ON P991.ZTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P991.ZXGA140D ON P991.ZTGA14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P991.RXQTPSEPX ON P991.QTPSEPX                                    
        ( CFAM ASC                                                              
        , CGCPLT ASC                                                            
        , CTARIF ASC                                                            
        , WACTIF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.CCDIX_TTGV02 ON P991.TTGV02                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.CCDIX_TTGV11 ON P991.TTGV11                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.CCDIX_TTVE10 ON P991.TTVE10                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXGV020 ON P991.TTGV02                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXGV021 ON P991.TTGV02                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXGV022 ON P991.TTGV02                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXGV110 ON P991.TTGV11                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQNQ ASC                                                            
        , CTYPENREG ASC                                                         
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXGV111 ON P991.TTGV11                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXGV112 ON P991.TTGV11                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXVE101 ON P991.TTVE10                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P991.TXVE102 ON P991.TTVE10                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.CXGV020D ON P991.CTGV02                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.CXGV110D ON P991.CTGV11                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P991.CXVE100D ON P991.CTVE10                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
