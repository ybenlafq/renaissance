CREATE  TRIGGER P991.RJGA59C                                                    
           AFTER INSERT                                                         
        ON P991.RTGA59                                                          
        REFERENCING NEW AS "NEW"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO         
        ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6        
        , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || NEW.NCODIC || '991' || 'XXX' ||                 
        NEW.NZONPRIX , 'M' , ' ' , '            ' , CURRENT TIMESTAMP )         
    ;                                                                           
CREATE  TRIGGER P991.RJGA59M                                                    
           AFTER UPDATE                                                         
        OF PSTDTTC                                                              
        ON P991.RTGA59                                                          
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( ( OLD.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) ,           
        ISO ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 6 , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 ,        
        2 ) )                                                                   
    AND ( OLD.NCODIC = NEW.NCODIC )                                             
    AND ( OLD.NZONPRIX = NEW.NZONPRIX )                                         
    AND ( OLD.PSTDTTC <> NEW.PSTDTTC ) )                                        
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || OLD.NCODIC || '991' || 'XXX' ||                 
        OLD.NZONPRIX , 'M' , ' ' , '            ' , CURRENT TIMESTAMP )         
    ;                                                                           
CREATE  TRIGGER P991.RJGA59S                                                    
           AFTER DELETE                                                         
        ON P991.RTGA59                                                          
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( OLD.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO         
        ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6        
        , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || OLD.NCODIC || '991' || 'XXX' ||                 
        OLD.NZONPRIX , 'M' , ' ' , '            ' , CURRENT TIMESTAMP )         
    ;                                                                           
CREATE  TRIGGER P991.RJGA75C                                                    
           AFTER INSERT                                                         
        ON P991.RTGA75                                                          
        REFERENCING NEW AS "NEW"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO         
        ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6        
        , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || NEW.NCODIC || '991' || 'XXX' ||                 
        NEW.NZONPRIX , 'M' , ' ' , '            ' , CURRENT TIMESTAMP )         
    ;                                                                           
CREATE  TRIGGER P991.RJGA75M                                                    
           AFTER UPDATE                                                         
        OF PCOMMART                                                             
        ON P991.RTGA75                                                          
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( ( OLD.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) ,           
        ISO ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 6 , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 ,        
        2 ) )                                                                   
    AND ( OLD.NCODIC = NEW.NCODIC )                                             
    AND ( OLD.NZONPRIX = NEW.NZONPRIX )                                         
    AND ( OLD.PCOMMART <> NEW.PCOMMART ) )                                      
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || OLD.NCODIC || '991' || 'XXX' ||                 
        OLD.NZONPRIX , 'M' , ' ' , '            ' , CURRENT TIMESTAMP )         
    ;                                                                           
CREATE  TRIGGER P991.RJGA75S                                                    
           AFTER DELETE                                                         
        ON P991.RTGA75                                                          
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( OLD.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO         
        ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6        
        , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || OLD.NCODIC || '991' || 'XXX' ||                 
        OLD.NZONPRIX , 'M' , ' ' , '            ' , CURRENT TIMESTAMP )         
    ;                                                                           
CREATE  TRIGGER P991.RJGG20C                                                    
           AFTER INSERT                                                         
        ON P991.RTGG20                                                          
        REFERENCING NEW AS "NEW"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO         
        ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6        
        , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.RJGG20M                                                    
           AFTER UPDATE                                                         
        OF PEXPTTC                                                              
        ON P991.RTGG20                                                          
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.PEXPTTC <> OLD.PEXPTTC                                       
    AND NEW.DEFFET = OLD.DEFFET                                                 
    AND ( OLD.DEFFET <= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) ,         
        1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 , 2        
        ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )          
    AND ( OLD.DFINEFFET >= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.RJGG20S                                                    
           AFTER DELETE                                                         
        ON P991.RTGG20                                                          
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( ( OLD.DEFFET <= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) ,          
        ISO ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 6 , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 ,        
        2 ) )                                                                   
    AND ( OLD.DFINEFFET >= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || OLD.NCODIC || OLD.NSOCIETE || OLD.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.RJGG20U                                                    
           AFTER UPDATE                                                         
        OF DFINEFFET                                                            
        ON P991.RTGG20                                                          
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.NCODIC = OLD.NCODIC                                          
    AND NEW.NSOCIETE = OLD.NSOCIETE                                             
    AND NEW.NLIEU = OLD.NLIEU                                                   
    AND NEW.DEFFET = OLD.DEFFET                                                 
    AND ( NEW.DFINEFFET < SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )         
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
    AND ( OLD.DFINEFFET >= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PX' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.RJGG40C                                                    
           AFTER INSERT                                                         
        ON P991.RTGG40                                                          
        REFERENCING NEW AS "NEW"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.DEFFET = SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO         
        ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6        
        , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.RJGG40M                                                    
           AFTER UPDATE                                                         
        OF PEXPCOMMART                                                          
        ON P991.RTGG40                                                          
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.PEXPCOMMART <> OLD.PEXPCOMMART                               
    AND NEW.DEFFET = OLD.DEFFET                                                 
    AND ( OLD.DEFFET <= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) ,         
        1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 , 2        
        ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )          
    AND ( OLD.DFINEFFET >= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.RJGG40S                                                    
           AFTER DELETE                                                         
        ON P991.RTGG40                                                          
        REFERENCING OLD AS "OLD"                                                
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( ( OLD.DEFFET <= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) ,          
        ISO ) , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 6 , 2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 ,        
        2 ) )                                                                   
    AND ( OLD.DFINEFFET > SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )         
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || OLD.NCODIC || OLD.NSOCIETE || OLD.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.RJGG40U                                                    
           AFTER UPDATE                                                         
        OF DFINEFFET                                                            
        ON P991.RTGG40                                                          
        REFERENCING OLD AS "OLD"                                                
        NEW AS "NEW"                                                            
           FOR EACH ROW MODE DB2SQL                                             
        WHEN ( NEW.NCODIC = OLD.NCODIC                                          
    AND NEW.NSOCIETE = OLD.NSOCIETE                                             
    AND NEW.NLIEU = OLD.NLIEU                                                   
    AND NEW.DEFFET = OLD.DEFFET                                                 
    AND ( NEW.DFINEFFET < SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )         
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
    AND ( OLD.DFINEFFET >= SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO )        
        , 1 , 4 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 6 ,        
        2 ) || SUBSTR ( CHAR ( DATE ( CURRENT DATE ) , ISO ) , 9 , 2 ) )        
        )                                                                       
INSERT                                                                          
   INTO PDARTY.RTNV00 ( DTD , CLE , EVENEMENT , STATUT , IDENTIFIANT ,          
        TIMESTAMP )                                                             
 VALUES ( 'prixPrime' , 'PM' || NEW.NCODIC || NEW.NSOCIETE || NEW.NLIEU         
        || 'XX' , 'M' , ' ' , ' ' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.GV11DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTGV11                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV11HD                                                         
 VALUES ( 'GV11D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.CTYPENREG ||        
        O.NCODICGRP || O.NCODIC || O.NSEQ || O.CMODDEL || O.DDELIV ||           
        O.NORDRE || O.CENREG || LEFT ( CHAR ( O.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( O.PVUNIT ) , 8 ) || RIGHT ( CHAR ( O.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( O.PVUNITF ) , 8 ) || RIGHT ( CHAR ( O.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( O.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        O.PVTOTAL ) , 2 ) || O.CEQUIPE || O.NLIGNE || LEFT ( CHAR (             
        O.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( O.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( O.QCONDT ) , 6 ) || LEFT ( CHAR ( O.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( O.PRMP ) , 2 ) || O.WEMPORTE || O.CPLAGE || O.CPROTOUR         
        || O.CADRTOUR || O.CPOSTAL || O.LCOMMENT || O.CVENDEUR ||               
        O.NSOCLIVR || O.NDEPOT || O.NAUTORM || O.WARTINEX || O.WEDITBL          
        || O.WACOMMUTER || O.WCQERESF || O.NMUTATION || O.CTOURNEE ||           
        O.WTOPELIVRE || O.DCOMPTA || O.DCREATION || O.HCREATION ||              
        O.DANNULATION || O.NLIEUORIG || O.WINTMAJ || LEFT ( CHAR (              
        O.DSYST ) , 14 ) || O.DSTAT || O.CPRESTGRP || O.NSOCMODIF ||            
        O.NLIEUMODIF || O.DATENC || O.CDEV || O.WUNITR || O.LRCMMT ||           
        O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 ) ||                
        O.NSEQFV || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        O.NSEQREF ) , 6 ) || LEFT ( CHAR ( O.NMODIF ) , 6 ) ||                  
        O.NSOCORIG || O.DTOPE || O.NSOCLIVR1 || O.NDEPOT1 || O.NSOCGEST         
        || O.NLIEUGEST || O.NSOCDEPLIV || O.NLIEUDEPLIV || O.TYPVTE ||          
        O.CTYPENT || LEFT ( CHAR ( O.NLIEN ) , 6 ) || LEFT ( CHAR (             
        O.NACTVTE ) , 6 ) || LEFT ( CHAR ( O.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( O.PVCODIG ) , 2 ) || LEFT ( CHAR ( O.QTCODIG ) , 6 ) ||          
        O.DPRLG || O.CALERTE || O.CREPRISE || LEFT ( CHAR ( O.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( O.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        O.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.GV11DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTGV11                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV11HD                                                         
 VALUES ( 'GV11I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.GV11DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTGV11                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV11HD                                                         
 VALUES ( 'GV11U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.VE11DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTVE11                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE11HD                                                         
 VALUES ( 'VE11D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.CTYPENREG ||        
        O.NCODICGRP || O.NCODIC || O.NSEQ || O.CMODDEL || O.DDELIV ||           
        O.NORDRE || O.CENREG || LEFT ( CHAR ( O.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( O.PVUNIT ) , 8 ) || RIGHT ( CHAR ( O.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( O.PVUNITF ) , 8 ) || RIGHT ( CHAR ( O.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( O.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        O.PVTOTAL ) , 2 ) || O.CEQUIPE || O.NLIGNE || LEFT ( CHAR (             
        O.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( O.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( O.QCONDT ) , 6 ) || LEFT ( CHAR ( O.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( O.PRMP ) , 2 ) || O.WEMPORTE || O.CPLAGE || O.CPROTOUR         
        || O.CADRTOUR || O.CPOSTAL || O.LCOMMENT || O.CVENDEUR ||               
        O.NSOCLIVR || O.NDEPOT || O.NAUTORM || O.WARTINEX || O.WEDITBL          
        || O.WACOMMUTER || O.WCQERESF || O.NMUTATION || O.CTOURNEE ||           
        O.WTOPELIVRE || O.DCOMPTA || O.DCREATION || O.HCREATION ||              
        O.DANNULATION || O.NLIEUORIG || O.WINTMAJ || LEFT ( CHAR (              
        O.DSYST ) , 14 ) || O.DSTAT || O.CPRESTGRP || O.NSOCMODIF ||            
        O.NLIEUMODIF || O.DATENC || O.CDEV || O.WUNITR || O.LRCMMT ||           
        O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 ) ||                
        O.NSEQFV || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        O.NSEQREF ) , 6 ) || LEFT ( CHAR ( O.NMODIF ) , 6 ) ||                  
        O.NSOCORIG || O.DTOPE || O.NSOCLIVR1 || O.NDEPOT1 || O.NSOCGEST         
        || O.NLIEUGEST || O.NSOCDEPLIV || O.NLIEUDEPLIV || O.TYPVTE ||          
        O.CTYPENT || LEFT ( CHAR ( O.NLIEN ) , 6 ) || LEFT ( CHAR (             
        O.NACTVTE ) , 6 ) || LEFT ( CHAR ( O.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( O.PVCODIG ) , 2 ) || LEFT ( CHAR ( O.QTCODIG ) , 6 ) ||          
        O.DPRLG || O.CALERTE || O.CREPRISE || LEFT ( CHAR ( O.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( O.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        O.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.VE11DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTVE11                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE11HD                                                         
 VALUES ( 'VE11I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.VE11DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTVE11                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE11HD                                                         
 VALUES ( 'VE11U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.CTYPENREG ||        
        N.NCODICGRP || N.NCODIC || N.NSEQ || N.CMODDEL || N.DDELIV ||           
        N.NORDRE || N.CENREG || LEFT ( CHAR ( N.QVENDUE ) , 6 ) || LEFT         
        ( CHAR ( N.PVUNIT ) , 8 ) || RIGHT ( CHAR ( N.PVUNIT ) , 2 ) ||         
        LEFT ( CHAR ( N.PVUNITF ) , 8 ) || RIGHT ( CHAR ( N.PVUNITF ) ,         
        2 ) || LEFT ( CHAR ( N.PVTOTAL ) , 8 ) || RIGHT ( CHAR (                
        N.PVTOTAL ) , 2 ) || N.CEQUIPE || N.NLIGNE || LEFT ( CHAR (             
        N.TAUXTVA ) , 4 ) || RIGHT ( CHAR ( N.TAUXTVA ) , 2 ) || LEFT (         
        CHAR ( N.QCONDT ) , 6 ) || LEFT ( CHAR ( N.PRMP ) , 8 ) || RIGHT        
        ( CHAR ( N.PRMP ) , 2 ) || N.WEMPORTE || N.CPLAGE || N.CPROTOUR         
        || N.CADRTOUR || N.CPOSTAL || N.LCOMMENT || N.CVENDEUR ||               
        N.NSOCLIVR || N.NDEPOT || N.NAUTORM || N.WARTINEX || N.WEDITBL          
        || N.WACOMMUTER || N.WCQERESF || N.NMUTATION || N.CTOURNEE ||           
        N.WTOPELIVRE || N.DCOMPTA || N.DCREATION || N.HCREATION ||              
        N.DANNULATION || N.NLIEUORIG || N.WINTMAJ || LEFT ( CHAR (              
        N.DSYST ) , 14 ) || N.DSTAT || N.CPRESTGRP || N.NSOCMODIF ||            
        N.NLIEUMODIF || N.DATENC || N.CDEV || N.WUNITR || N.LRCMMT ||           
        N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 ) ||                
        N.NSEQFV || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) || LEFT ( CHAR (             
        N.NSEQREF ) , 6 ) || LEFT ( CHAR ( N.NMODIF ) , 6 ) ||                  
        N.NSOCORIG || N.DTOPE || N.NSOCLIVR1 || N.NDEPOT1 || N.NSOCGEST         
        || N.NLIEUGEST || N.NSOCDEPLIV || N.NLIEUDEPLIV || N.TYPVTE ||          
        N.CTYPENT || LEFT ( CHAR ( N.NLIEN ) , 6 ) || LEFT ( CHAR (             
        N.NACTVTE ) , 6 ) || LEFT ( CHAR ( N.PVCODIG ) , 8 ) || RIGHT (         
        CHAR ( N.PVCODIG ) , 2 ) || LEFT ( CHAR ( N.QTCODIG ) , 6 ) ||          
        N.DPRLG || N.CALERTE || N.CREPRISE || LEFT ( CHAR ( N.NSEQENS )         
        , 6 ) || LEFT ( CHAR ( N.MPRIMECLI ) , 8 ) || RIGHT ( CHAR (            
        N.MPRIMECLI ) , 2 ) , 'N' , CURRENT TIMESTAMP )                         
    ;                                                                           
CREATE  TRIGGER P991.GV10DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTGV10                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV10HD                                                         
 VALUES ( 'GV10D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.NORDRE ||           
        O.NCLIENT || O.DVENTE || O.DHVENTE || O.DMVENTE || LEFT ( CHAR (        
        O.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( O.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( O.PVERSE ) , 8 ) || RIGHT ( CHAR ( O.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( O.PCOMPT ) , 8 ) || RIGHT ( CHAR ( O.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( O.PLIVR ) , 8 ) || RIGHT ( CHAR ( O.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( O.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        O.PDIFFERE ) , 2 ) || LEFT ( CHAR ( O.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( O.PRFACT ) , 2 ) || O.CREMVTE || LEFT ( CHAR ( O.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREMVTE ) , 2 ) || O.LCOMVTE1 ||            
        O.LCOMVTE2 || O.LCOMVTE3 || O.LCOMVTE4 || O.DMODIFVTE ||                
        O.WFACTURE || O.WEXPORT || O.WDETAXEC || O.WDETAXEHC ||                 
        O.CORGORED || O.CMODPAIMTI || O.LDESCRIPTIF1 || O.LDESCRIPTIF2          
        || O.DLIVRBL || O.NFOLIOBL || O.LAUTORM || O.NAUTORD || LEFT (          
        CHAR ( O.DSYST ) , 14 ) || O.DSTAT || O.DFACTURE || O.CACID ||          
        O.NSOCMODIF || O.NLIEUMODIF || O.NSOCP || O.NLIEUP || O.CDEV ||         
        O.CFCRED || O.NCREDI || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) ||               
        O.TYPVTE || O.VTEGPE || O.CTRMRQ || O.DATENC || O.WDGRAD ||             
        O.NAUTO || LEFT ( CHAR ( O.NSEQENS ) , 6 ) || O.NSOCO ||                
        O.NLIEUO || O.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER P991.GV10DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTGV10                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV10HD                                                         
 VALUES ( 'GV10I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER P991.GV10DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTGV10                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV10HD                                                         
 VALUES ( 'GV10U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER P991.VE10DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTVE10                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE10HD                                                         
 VALUES ( 'VE10D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.NORDRE ||           
        O.NCLIENT || O.DVENTE || O.DHVENTE || O.DMVENTE || LEFT ( CHAR (        
        O.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( O.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( O.PVERSE ) , 8 ) || RIGHT ( CHAR ( O.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( O.PCOMPT ) , 8 ) || RIGHT ( CHAR ( O.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( O.PLIVR ) , 8 ) || RIGHT ( CHAR ( O.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( O.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        O.PDIFFERE ) , 2 ) || LEFT ( CHAR ( O.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( O.PRFACT ) , 2 ) || O.CREMVTE || LEFT ( CHAR ( O.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREMVTE ) , 2 ) || O.LCOMVTE1 ||            
        O.LCOMVTE2 || O.LCOMVTE3 || O.LCOMVTE4 || O.DMODIFVTE ||                
        O.WFACTURE || O.WEXPORT || O.WDETAXEC || O.WDETAXEHC ||                 
        O.CORGORED || O.CMODPAIMTI || O.LDESCRIPTIF1 || O.LDESCRIPTIF2          
        || O.DLIVRBL || O.NFOLIOBL || O.LAUTORM || O.NAUTORD || LEFT (          
        CHAR ( O.DSYST ) , 14 ) || O.DSTAT || O.DFACTURE || O.CACID ||          
        O.NSOCMODIF || O.NLIEUMODIF || O.NSOCP || O.NLIEUP || O.CDEV ||         
        O.CFCRED || O.NCREDI || LEFT ( CHAR ( O.NSEQNQ ) , 6 ) ||               
        O.TYPVTE || O.VTEGPE || O.CTRMRQ || O.DATENC || O.WDGRAD ||             
        O.NAUTO || LEFT ( CHAR ( O.NSEQENS ) , 6 ) || O.NSOCO ||                
        O.NLIEUO || O.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER P991.VE10DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTVE10                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE10HD                                                         
 VALUES ( 'VE10I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER P991.VE10DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTVE10                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE10HD                                                         
 VALUES ( 'VE10U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.NORDRE ||           
        N.NCLIENT || N.DVENTE || N.DHVENTE || N.DMVENTE || LEFT ( CHAR (        
        N.PTTVENTE ) , 8 ) || RIGHT ( CHAR ( N.PTTVENTE ) , 2 ) || LEFT         
        ( CHAR ( N.PVERSE ) , 8 ) || RIGHT ( CHAR ( N.PVERSE ) , 2 ) ||         
        LEFT ( CHAR ( N.PCOMPT ) , 8 ) || RIGHT ( CHAR ( N.PCOMPT ) , 2         
        ) || LEFT ( CHAR ( N.PLIVR ) , 8 ) || RIGHT ( CHAR ( N.PLIVR ) ,        
        2 ) || LEFT ( CHAR ( N.PDIFFERE ) , 8 ) || RIGHT ( CHAR (               
        N.PDIFFERE ) , 2 ) || LEFT ( CHAR ( N.PRFACT ) , 8 ) || RIGHT (         
        CHAR ( N.PRFACT ) , 2 ) || N.CREMVTE || LEFT ( CHAR ( N.PREMVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREMVTE ) , 2 ) || N.LCOMVTE1 ||            
        N.LCOMVTE2 || N.LCOMVTE3 || N.LCOMVTE4 || N.DMODIFVTE ||                
        N.WFACTURE || N.WEXPORT || N.WDETAXEC || N.WDETAXEHC ||                 
        N.CORGORED || N.CMODPAIMTI || N.LDESCRIPTIF1 || N.LDESCRIPTIF2          
        || N.DLIVRBL || N.NFOLIOBL || N.LAUTORM || N.NAUTORD || LEFT (          
        CHAR ( N.DSYST ) , 14 ) || N.DSTAT || N.DFACTURE || N.CACID ||          
        N.NSOCMODIF || N.NLIEUMODIF || N.NSOCP || N.NLIEUP || N.CDEV ||         
        N.CFCRED || N.NCREDI || LEFT ( CHAR ( N.NSEQNQ ) , 6 ) ||               
        N.TYPVTE || N.VTEGPE || N.CTRMRQ || N.DATENC || N.WDGRAD ||             
        N.NAUTO || LEFT ( CHAR ( N.NSEQENS ) , 6 ) || N.NSOCO ||                
        N.NLIEUO || N.NVENTO , 'N' , CURRENT TIMESTAMP )                        
    ;                                                                           
CREATE  TRIGGER P991.GV02DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTGV02                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV02HD                                                         
 VALUES ( 'GV02D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.WTYPEADR || O.CTITRENOM || O.LNOM || O.LPRENOM || O.LBATIMENT         
        || O.LESCALIER || O.LETAGE || O.LPORTE || O.LCMPAD1 || O.LCMPAD2        
        || O.CVOIE || O.CTVOIE || O.LNOMVOIE || O.LCOMMUNE || O.CPOSTAL         
        || O.LBUREAU || O.TELDOM || O.TELBUR || O.LPOSTEBUR ||                  
        O.LCOMLIV1 || O.LCOMLIV2 || O.WETRANGER || O.CZONLIV || O.CINSEE        
        || O.WCONTRA || LEFT ( CHAR ( O.DSYST ) , 14 ) || O.CILOT ||            
        O.IDCLIENT || O.CPAYS || O.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER P991.GV02DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTGV02                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV02HD                                                         
 VALUES ( 'GV02U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER P991.GV02DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTGV02                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV02HD                                                         
 VALUES ( 'GV02I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER P991.GV03DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTGV03                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV03HD                                                         
 VALUES ( 'GV03D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.WTYPEADR || O.EMAIL || LEFT ( CHAR ( O.DSYST ) , 14 ) ||              
        O.CPORTE || O.WFLAG || O.WASC || O.WADPRP , 'N' , CURRENT               
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER P991.GV03DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTGV03                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV03HD                                                         
 VALUES ( 'GV03I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.EMAIL || LEFT ( CHAR ( N.DSYST ) , 14 ) ||              
        N.CPORTE || N.WFLAG || N.WASC || N.WADPRP , 'N' , CURRENT               
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER P991.GV03DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTGV03                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV03HD                                                         
 VALUES ( 'GV03U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.EMAIL || LEFT ( CHAR ( N.DSYST ) , 14 ) ||              
        N.CPORTE || N.WFLAG || N.WASC || N.WADPRP , 'N' , CURRENT               
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER P991.GV04DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTGV04                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV04HD                                                         
 VALUES ( 'GV04D' || O.NSOCIETE || O.NLIEU || O.NVENTE || O.DCREATION ||        
        O.DMAJ || O.WACCES || O.WREPRISE || LEFT ( CHAR ( O.NBPRDREP ) ,        
        3 ) || O.DATENAISS || O.CPNAISS || O.LIEUNAISS || O.WBTOB ||            
        O.WARF || LEFT ( CHAR ( O.DSYST ) , 14 ) || O.WASCUTIL ||               
        O.WMARCHES || O.WCOL || O.WSDEP || O.WFLAG , 'N' , CURRENT              
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER P991.GV04DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTGV04                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV04HD                                                         
 VALUES ( 'GV04I' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.DCREATION ||        
        N.DMAJ || N.WACCES || N.WREPRISE || LEFT ( CHAR ( N.NBPRDREP ) ,        
        3 ) || N.DATENAISS || N.CPNAISS || N.LIEUNAISS || N.WBTOB ||            
        N.WARF || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.WASCUTIL ||               
        N.WMARCHES || N.WCOL || N.WSDEP || N.WFLAG , 'N' , CURRENT              
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER P991.GV04DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTGV04                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV04HD                                                         
 VALUES ( 'GV04U' || N.NSOCIETE || N.NLIEU || N.NVENTE || N.DCREATION ||        
        N.DMAJ || N.WACCES || N.WREPRISE || LEFT ( CHAR ( N.NBPRDREP ) ,        
        3 ) || N.DATENAISS || N.CPNAISS || N.LIEUNAISS || N.WBTOB ||            
        N.WARF || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.WASCUTIL ||               
        N.WMARCHES || N.WCOL || N.WSDEP || N.WFLAG , 'N' , CURRENT              
        TIMESTAMP )                                                             
    ;                                                                           
CREATE  TRIGGER P991.GV14DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTGV14                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV14HD                                                         
 VALUES ( 'GV14D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.DSAISIE || O.NSEQ || O.CMODPAIMT || LEFT ( CHAR ( O.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREGLTVTE ) , 2 ) || O.NREGLTVTE ||         
        O.DREGLTVTE || O.WREGLTVTE || O.DCOMPTA || LEFT ( CHAR ( O.DSYST        
        ) , 14 ) || O.CPROTOUR || O.CTOURNEE || O.CDEVISE || LEFT ( CHAR        
        ( O.MDEVISE ) , 8 ) || RIGHT ( CHAR ( O.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( O.MECART ) , 2 ) || RIGHT ( CHAR ( O.MECART ) , 2 ) ||         
        O.CDEV || O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 )         
        || LEFT ( CHAR ( O.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        O.PMODPAIMT ) , 2 ) || O.CVENDEUR || O.CFCRED || O.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER P991.GV14DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTGV14                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV14HD                                                         
 VALUES ( 'GV14I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER P991.GV14DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTGV14                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV14HD                                                         
 VALUES ( 'GV14U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER P991.GV15DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTGV15                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV15HD                                                         
 VALUES ( 'GV15D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.NCODIC || O.NCODICGRP || O.NSEQ || O.CARACTSPE1 ||                    
        O.CVCARACTSPE1 || O.CARACTSPE2 || O.CVCARACTSPE2 || O.CARACTSPE3        
        || O.CVCARACTSPE3 || O.CARACTSPE4 || O.CVCARACTSPE4 ||                  
        O.CARACTSPE5 || O.CVCARACTSPE5 || O.WANNULCAR || LEFT ( CHAR (          
        O.DSYST ) , 14 ) , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER P991.GV15DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTGV15                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV15HD                                                         
 VALUES ( 'GV15I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.NCODIC || N.NCODICGRP || N.NSEQ || N.CARACTSPE1 ||                    
        N.CVCARACTSPE1 || N.CARACTSPE2 || N.CVCARACTSPE2 || N.CARACTSPE3        
        || N.CVCARACTSPE3 || N.CARACTSPE4 || N.CVCARACTSPE4 ||                  
        N.CARACTSPE5 || N.CVCARACTSPE5 || N.WANNULCAR || LEFT ( CHAR (          
        N.DSYST ) , 14 ) , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER P991.GV15DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTGV15                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTGV15HD                                                         
 VALUES ( 'GV15U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.NCODIC || N.NCODICGRP || N.NSEQ || N.CARACTSPE1 ||                    
        N.CVCARACTSPE1 || N.CARACTSPE2 || N.CVCARACTSPE2 || N.CARACTSPE3        
        || N.CVCARACTSPE3 || N.CARACTSPE4 || N.CVCARACTSPE4 ||                  
        N.CARACTSPE5 || N.CVCARACTSPE5 || N.WANNULCAR || LEFT ( CHAR (          
        N.DSYST ) , 14 ) , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER P991.TL04DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTTL04                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTTL04HD                                                         
 VALUES ( 'TL04D' || O.NSOC || O.NMAG || O.NVENTE || O.NCODICGRP ||             
        O.NCODIC || O.NSEQ || O.DDELIV || O.CPROTOUR || O.CADRTOUR ||           
        O.CTOURNEE || O.WRECYCL || O.CRETOUR || LEFT ( CHAR ( O.QTE ) ,         
        6 ) || LEFT ( CHAR ( O.QCDEE ) , 6 ) || LEFT ( CHAR ( O.DSYST )         
        , 14 ) || O.DMVT , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER P991.TL04DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTTL04                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTTL04HD                                                         
 VALUES ( 'TL04I' || N.NSOC || N.NMAG || N.NVENTE || N.NCODICGRP ||             
        N.NCODIC || N.NSEQ || N.DDELIV || N.CPROTOUR || N.CADRTOUR ||           
        N.CTOURNEE || N.WRECYCL || N.CRETOUR || LEFT ( CHAR ( N.QTE ) ,         
        6 ) || LEFT ( CHAR ( N.QCDEE ) , 6 ) || LEFT ( CHAR ( N.DSYST )         
        , 14 ) || N.DMVT , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER P991.TL04DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTTL04                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTTL04HD                                                         
 VALUES ( 'TL04U' || N.NSOC || N.NMAG || N.NVENTE || N.NCODICGRP ||             
        N.NCODIC || N.NSEQ || N.DDELIV || N.CPROTOUR || N.CADRTOUR ||           
        N.CTOURNEE || N.WRECYCL || N.CRETOUR || LEFT ( CHAR ( N.QTE ) ,         
        6 ) || LEFT ( CHAR ( N.QCDEE ) , 6 ) || LEFT ( CHAR ( N.DSYST )         
        , 14 ) || N.DMVT , 'N' , CURRENT TIMESTAMP )                            
    ;                                                                           
CREATE  TRIGGER P991.VE02DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTVE02                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE02HD                                                         
 VALUES ( 'VE02D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.WTYPEADR || O.CTITRENOM || O.LNOM || O.LPRENOM || O.LBATIMENT         
        || O.LESCALIER || O.LETAGE || O.LPORTE || O.LCMPAD1 || O.LCMPAD2        
        || O.CVOIE || O.CTVOIE || O.LNOMVOIE || O.LCOMMUNE || O.CPOSTAL         
        || O.LBUREAU || O.TELDOM || O.TELBUR || O.LPOSTEBUR ||                  
        O.LCOMLIV1 || O.LCOMLIV2 || O.WETRANGER || O.CZONLIV || O.CINSEE        
        || O.WCONTRA || LEFT ( CHAR ( O.DSYST ) , 14 ) || O.CILOT ||            
        O.IDCLIENT || O.CPAYS || O.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER P991.VE02DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTVE02                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE02HD                                                         
 VALUES ( 'VE02I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER P991.VE02DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTVE02                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE02HD                                                         
 VALUES ( 'VE02U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.WTYPEADR || N.CTITRENOM || N.LNOM || N.LPRENOM || N.LBATIMENT         
        || N.LESCALIER || N.LETAGE || N.LPORTE || N.LCMPAD1 || N.LCMPAD2        
        || N.CVOIE || N.CTVOIE || N.LNOMVOIE || N.LCOMMUNE || N.CPOSTAL         
        || N.LBUREAU || N.TELDOM || N.TELBUR || N.LPOSTEBUR ||                  
        N.LCOMLIV1 || N.LCOMLIV2 || N.WETRANGER || N.CZONLIV || N.CINSEE        
        || N.WCONTRA || LEFT ( CHAR ( N.DSYST ) , 14 ) || N.CILOT ||            
        N.IDCLIENT || N.CPAYS || N.NGSM , 'N' , CURRENT TIMESTAMP )             
    ;                                                                           
CREATE  TRIGGER P991.VE14DD_H                                                   
           AFTER DELETE                                                         
        ON P991.RTVE14                                                          
        REFERENCING OLD AS O                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE14HD                                                         
 VALUES ( 'VE14D' || O.NSOCIETE || O.NLIEU || O.NORDRE || O.NVENTE ||           
        O.DSAISIE || O.NSEQ || O.CMODPAIMT || LEFT ( CHAR ( O.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( O.PREGLTVTE ) , 2 ) || O.NREGLTVTE ||         
        O.DREGLTVTE || O.WREGLTVTE || O.DCOMPTA || LEFT ( CHAR ( O.DSYST        
        ) , 14 ) || O.CPROTOUR || O.CTOURNEE || O.CDEVISE || LEFT ( CHAR        
        ( O.MDEVISE ) , 8 ) || RIGHT ( CHAR ( O.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( O.MECART ) , 2 ) || RIGHT ( CHAR ( O.MECART ) , 2 ) ||         
        O.CDEV || O.NSOCP || O.NLIEUP || LEFT ( CHAR ( O.NTRANS ) , 9 )         
        || LEFT ( CHAR ( O.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        O.PMODPAIMT ) , 2 ) || O.CVENDEUR || O.CFCRED || O.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER P991.VE14DI_H                                                   
           AFTER INSERT                                                         
        ON P991.RTVE14                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE14HD                                                         
 VALUES ( 'VE14I' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
CREATE  TRIGGER P991.VE14DU_H                                                   
           AFTER UPDATE                                                         
        ON P991.RTVE14                                                          
        REFERENCING NEW AS N                                                    
           FOR EACH ROW MODE DB2SQL                                             
INSERT                                                                          
   INTO PDARTY.RTVE14HD                                                         
 VALUES ( 'VE14U' || N.NSOCIETE || N.NLIEU || N.NORDRE || N.NVENTE ||           
        N.DSAISIE || N.NSEQ || N.CMODPAIMT || LEFT ( CHAR ( N.PREGLTVTE         
        ) , 8 ) || RIGHT ( CHAR ( N.PREGLTVTE ) , 2 ) || N.NREGLTVTE ||         
        N.DREGLTVTE || N.WREGLTVTE || N.DCOMPTA || LEFT ( CHAR ( N.DSYST        
        ) , 14 ) || N.CPROTOUR || N.CTOURNEE || N.CDEVISE || LEFT ( CHAR        
        ( N.MDEVISE ) , 8 ) || RIGHT ( CHAR ( N.MDEVISE ) , 2 ) || LEFT         
        ( CHAR ( N.MECART ) , 2 ) || RIGHT ( CHAR ( N.MECART ) , 2 ) ||         
        N.CDEV || N.NSOCP || N.NLIEUP || LEFT ( CHAR ( N.NTRANS ) , 9 )         
        || LEFT ( CHAR ( N.PMODPAIMT ) , 8 ) || RIGHT ( CHAR (                  
        N.PMODPAIMT ) , 2 ) || N.CVENDEUR || N.CFCRED || N.NCREDI , 'N'         
        , CURRENT TIMESTAMP )                                                   
    ;                                                                           
