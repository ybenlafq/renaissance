--MW CREATE  UNIQUE INDEX P916.RXIGSA0R ON P916.RTIGSA                               
--MW         ( DIGFA0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P916.RXIGSB0R ON P916.RTIGSB                               
--MW         ( DIGFA0 ASC                                                            
--MW         , NT_DIGSB ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P916.RXIGSC0R ON P916.RTIGSC                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFC0 ASC                                                            
--MW         , SM_DIGSC ASC                                                          
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P916.RXIGSD0R ON P916.RTIGSD                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFD0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P916.RXIGSE0R ON P916.RTIGSE                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFE0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
--MW CREATE  UNIQUE INDEX P916.RXIGSF0R ON P916.RTIGSF                               
--MW         ( DIGFA0 ASC                                                            
--MW         , DIGFF0 ASC                                                            
--MW           )                                                                     
--MW            PCTFREE 5                                                            
--MW            CLUSTER                                                              
--MW     ;                                                                           
CREATE  INDEX P916.RXAI241O ON P916.RTAI24                                      
        ( FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXAI531O ON P916.RTAI53                                      
        ( FSSKU ASC                                                             
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXAN000O ON P916.RTAN00                                      
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXAN001O ON P916.RTAN00                                      
        ( ENRID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA001O ON P916.RTBA00                                      
        ( NFACTREM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA011O ON P916.RTBA01                                      
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA012O ON P916.RTBA01                                      
        ( NCHRONO ASC                                                           
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA013O ON P916.RTBA01                                      
        ( DEMIS DESC                                                            
        , WANNUL ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA014O ON P916.RTBA01                                      
        ( DEMIS ASC                                                             
        , WANNUL ASC                                                            
        , NBA DESC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA015O ON P916.RTBA01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA101O ON P916.RTBA10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXBA201O ON P916.RTBA20                                      
        ( NTIERSCV ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCC031O ON P916.RTCC03                                      
        ( MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , TYPTRANS ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCC041O ON P916.RTCC04                                      
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCD301O ON P916.RTCD30                                      
        ( NCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCD302O ON P916.RTCD30                                      
        ( NMAG ASC                                                              
        , NCODIC ASC                                                            
        , DREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCD303O ON P916.RTCD30                                      
        ( NMAG ASC                                                              
        , NREC ASC                                                              
        , DVALIDATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCE101O ON P916.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCE102O ON P916.RTCE10                                      
        ( NZONPRIX ASC                                                          
        , WETIQUETTE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCE103O ON P916.RTCE10                                      
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXCE104O ON P916.RTCE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXDC001O ON P916.RTDC00                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXDC101O ON P916.RTDC10                                      
        ( NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXEG051O ON P916.RTEG05                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMPC ASC                                                         
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXEG101O ON P916.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXEG102O ON P916.RTEG10                                      
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXEG103O ON P916.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND1 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXEG104O ON P916.RTEG10                                      
        ( NOMETAT ASC                                                           
        , CHCOND2 ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXEG900O ON P916.RTEG90                                      
        ( IDENTTS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEXFA0O ON P916.RTEXFA                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE3 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEXFD0O ON P916.RTEXFD                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX000O ON P916.RTEX00                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX100O ON P916.RTEX10                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX200O ON P916.RTEX20                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX300O ON P916.RTEX30                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX400O ON P916.RTEX40                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX500O ON P916.RTEX50                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , LRUPTURE2 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX600O ON P916.RTEX60                                      
        ( WSEQPRO ASC                                                           
        , NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX700O ON P916.RTEX70                                      
        ( NTRI ASC                                                              
        , LRUPTURE2 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXEX900O ON P916.RTEX90                                      
        ( NTRI ASC                                                              
        , LRUPTURE1 ASC                                                         
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXFF001O ON P916.RTFF00                                      
        ( NTIERS ASC                                                            
        , WANN ASC                                                              
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF002O ON P916.RTFF00                                      
        ( DEVN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF051O ON P916.RTFF05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF101O ON P916.RTFF10                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF102O ON P916.RTFF10                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF111O ON P916.RTFF11                                      
        ( NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF121O ON P916.RTFF12                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF131O ON P916.RTFF13                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF151O ON P916.RTFF15                                      
        ( NPIECEFAC ASC                                                         
        , DEXCPTFAC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFF860O ON P916.RTFF86                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXFF870O ON P916.RTFF87                                      
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
--MW CREATE  INDEX P916.RXFF880O ON P916.RTFF88                                      
--MW        ( PREFT1 ASC                                                            
--MW        , DEXCPT ASC                                                            
--MW        , NPIECE ASC                                                            
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P916.RXFF881O ON P916.RTFF88                                      
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXFI101O ON P916.RTFI10                                      
        ( NMUTATION ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXFI102O ON P916.RTFI10                                      
        ( DCOMPTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT121O ON P916.RTFT12                                      
        ( NJRN ASC                                                              
        , NEXER ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT122O ON P916.RTFT12                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT131O ON P916.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT133O ON P916.RTFT13                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT134O ON P916.RTFT13                                      
        ( NREG ASC                                                              
        , NORDRE ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT141O ON P916.RTFT14                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT211O ON P916.RTFT21                                      
        ( LETTRAGE DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT212O ON P916.RTFT21                                      
        ( COMPTE ASC                                                            
        , NSOC ASC                                                              
        , LETTRAGE ASC                                                          
        , CDEVISE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT214O ON P916.RTFT21                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT221O ON P916.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , SECTION ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT222O ON P916.RTFT22                                      
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , RUBRIQUE ASC                                                          
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
        , MONTANT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT261O ON P916.RTFT26                                      
        ( BAN ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT271O ON P916.RTFT27                                      
        ( NTBENEF ASC                                                           
        , NFOUR ASC                                                             
        , CTRESO ASC                                                            
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , NAUX ASC                                                              
        , NAUXR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT272O ON P916.RTFT27                                      
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXFT310 ON P916.RTFT31                                       
        ( SECTION ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXFT311 ON P916.RTFT31                                       
        ( RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT312 ON P916.RTFT31                                       
        ( SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT313O ON P916.RTFT31                                      
        ( RUBRIQUE ASC                                                          
        , SECTION ASC                                                           
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXFT760O ON P916.RTFT76                                      
        ( DFACTURE ASC                                                          
        , NFACTURE ASC                                                          
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXFT761O ON P916.RTFT76                                      
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA001O ON P916.RTGA00                                      
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA002O ON P916.RTGA00                                      
        ( CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA003O ON P916.RTGA00                                      
        ( CMARQ ASC                                                             
        , LEMBALLAGE ASC                                                        
        , NCODIC ASC                                                            
        , CASSORT ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA004O ON P916.RTGA00                                      
        ( NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA005O ON P916.RTGA00                                      
        ( NCODIC ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA006O ON P916.RTGA00                                      
        ( CASSORT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA007O ON P916.RTGA00                                      
        ( CFAM ASC                                                              
        , CASSORT ASC                                                           
        , CAPPRO ASC                                                            
        , NCODIC ASC                                                            
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA008O ON P916.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA009O ON P916.RTGA00                                      
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CASSORT ASC                                                           
        , D1RECEPT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA011O ON P916.RTGA01                                      
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
        , WTABLEG ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA031O ON P916.RTGA03                                      
        ( NCODICK ASC                                                           
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA101O ON P916.RTGA10                                      
        ( NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA102O ON P916.RTGA10                                      
        ( CTYPLIEU ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA103O ON P916.RTGA10                                      
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA120O ON P916.RTGA12                                      
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA141O ON P916.RTGA14                                      
        ( WSEQFAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA213O ON P916.RTGA21                                      
        ( WRAYONFAM ASC                                                         
        , CRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA251O ON P916.RTGA25                                      
        ( CMARKETING ASC                                                        
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA302O ON P916.RTGA30                                      
        ( CPARAM ASC                                                            
        , LVPARAM ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA431O ON P916.RTGA43                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXGA490O ON P916.RTGA49                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGA561O ON P916.RTGA56                                      
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA581O ON P916.RTGA58                                      
        ( NCODICLIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA591O ON P916.RTGA59                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA592O ON P916.RTGA59                                      
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA621O ON P916.RTGA62                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA651O ON P916.RTGA65                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA652O ON P916.RTGA65                                      
        ( CSTATUT ASC                                                           
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA661O ON P916.RTGA66                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA681O ON P916.RTGA68                                      
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA751O ON P916.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGA752O ON P916.RTGA75                                      
        ( DEFFET DESC                                                           
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGB301O ON P916.RTGB30                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGB801O ON P916.RTGB80                                      
        ( NCODIC ASC                                                            
        , NMUTATION ASC                                                         
        , NLIEUENT ASC                                                          
        , NHS ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG151O ON P916.RTGG15                                      
        ( NCODIC ASC                                                            
        , DDECISION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG170O ON P916.RTGG17                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGG201O ON P916.RTGG20                                      
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG202O ON P916.RTGG20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET DESC                                                           
        , PEXPTTC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG203O ON P916.RTGG20                                      
        ( DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG204O ON P916.RTGG20                                      
        ( NLIEU ASC                                                             
        , DFINEFFET ASC                                                         
        , DEFFET ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG210O ON P916.RTGG21                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG402O ON P916.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG403O ON P916.RTGG40                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
        , DFINEFFET ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGG601O ON P916.RTGG60                                      
        ( WTRAITE ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXGG701O ON P916.RTGG70                                      
        ( NCODIC ASC                                                            
        , DREC DESC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXGG702O ON P916.RTGG70                                      
        ( DREC DESC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGP040O ON P916.RTGP04                                      
        ( WTYPMVT ASC                                                           
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGQ014O ON P916.RTGQ01                                      
        ( CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGQ015O ON P916.RTGQ01                                      
        ( CINSEE ASC                                                            
        , WCONTRA DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGQ031O ON P916.RTGQ03                                      
        ( CZONLIV ASC                                                           
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGQ061O ON P916.RTGQ06                                      
        ( CTYPE ASC                                                             
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGQ062O ON P916.RTGQ06                                      
        ( CINSEE ASC                                                            
        , CTYPE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGQ063O ON P916.RTGQ06                                      
        ( CPOSTAL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR301O ON P916.RTGR30                                      
        ( DMVTREC ASC                                                           
        , WTLMELA ASC                                                           
        , CTYPMVT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR302O ON P916.RTGR30                                      
        ( NENTCDE ASC                                                           
        , DJRECQUAI ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR303O ON P916.RTGR30                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR304O ON P916.RTGR30                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR351O ON P916.RTGR35                                      
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR353O ON P916.RTGR35                                      
        ( NBL ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR354O ON P916.RTGR35                                      
        ( NREC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGR355O ON P916.RTGR35                                      
        ( NENTCDEPT ASC                                                         
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGS301O ON P916.RTGS30                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGS311O ON P916.RTGS31                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGS420O ON P916.RTGS42                                      
        ( NSOCORIG ASC                                                          
        , NLIEUORIG ASC                                                         
        , NSSLIEUORIG ASC                                                       
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , NSSLIEUDEST ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGS422O ON P916.RTGS42                                      
        ( DMVT ASC                                                              
        , DHMVT ASC                                                             
        , DMMVT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGS601O ON P916.RTGS60                                      
        ( NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGS602O ON P916.RTGS60                                      
        ( NCODIC ASC                                                            
        , NSOCDEST ASC                                                          
        , NSOCTRT ASC                                                           
        , NLIEUTRT ASC                                                          
        , NSSLIEUTRT ASC                                                        
        , CLIEUTRT ASC                                                          
        , CSTATUTTRT ASC                                                        
        , LEMPLACT ASC                                                          
        , NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGS700O ON P916.RTGS70                                      
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGV021O ON P916.RTGV02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV022O ON P916.RTGV02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV023O ON P916.RTGV02                                      
        ( IDCLIENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV024O ON P916.RTGV02                                      
        ( TELBUR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV031O ON P916.RTGV03                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV050O ON P916.RTGV05                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGV081O ON P916.RTGV08                                      
        ( VALCTRL ASC                                                           
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV104O ON P916.RTGV10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV105O ON P916.RTGV10                                      
        ( NVENTO ASC                                                            
        , NLIEU ASC                                                             
        , TYPVTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV111O ON P916.RTGV11                                      
        ( CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTYPENREG ASC                                                         
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV113O ON P916.RTGV11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV114O ON P916.RTGV11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV115O ON P916.RTGV11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV116O ON P916.RTGV11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV117O ON P916.RTGV11                                      
        ( NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV141O ON P916.RTGV14                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NTRANS DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV142O ON P916.RTGV14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV200O ON P916.RTGV20                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGV231O ON P916.RTGV23                                      
        ( DCREATION1 ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV271O ON P916.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXGV272O ON P916.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NORDRE ASC                                                            
        , CAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGV273O ON P916.RTGV27                                      
        ( NSOCIETE ASC                                                          
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV300O ON P916.RTGV30                                      
        ( LNOM ASC                                                              
        , DVENTE ASC                                                            
        , DNAISSANCE ASC                                                        
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXGV352O ON P916.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NDOCENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV353O ON P916.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV355O ON P916.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NDOCENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV356O ON P916.RTGV35                                      
        ( NSOCIETE ASC                                                          
        , NLIEUORIG ASC                                                         
        , NBONENLV DESC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXGV990O ON P916.RTGV99                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE001O ON P916.RTHE00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE002O ON P916.RTHE00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE011O ON P916.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE012O ON P916.RTHE01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE013O ON P916.RTHE01                                      
        ( NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE014O ON P916.RTHE01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE101O ON P916.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE102O ON P916.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE103O ON P916.RTHE10                                      
        ( NCODIC ASC                                                            
        , NSERIE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE104O ON P916.RTHE10                                      
        ( PDOSNASC ASC                                                          
        , DOSNASC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE105O ON P916.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NENVOI ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE107O ON P916.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WSOLDE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE108O ON P916.RTHE10                                      
        ( CLIEUHET ASC                                                          
        , NTYOPE ASC                                                            
        , CREFUS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  INDEX P916.RXHE110O ON P916.RTHE11                                      
--MW        ( NLIEUHED ASC                                                          
--MW        , NGHE ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  INDEX P916.RXHE111O ON P916.RTHE11                                      
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXHE202O ON P916.RTHE20                                      
        ( NUMPAL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHE221O ON P916.RTHE22                                      
        ( CLIEUHET ASC                                                          
        , CTIERS ASC                                                            
        , DENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHN001O ON P916.RTHN00                                      
        ( CTIERSELA ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHN002O ON P916.RTHN00                                      
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHN003O ON P916.RTHN00                                      
        ( DTRAIT ASC                                                            
        , WSUPP ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHN011O ON P916.RTHN01                                      
        ( CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHN012O ON P916.RTHN01                                      
        ( CTIERS ASC                                                            
        , NCODIC ASC                                                            
        , WTRI ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHV021O ON P916.RTHV02                                      
        ( DVENTECIALE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXHV071O ON P916.RTHV07                                      
        ( CETAT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXHV081O ON P916.RTHV08                                      
        ( DVENTECIALE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXHV600O ON P916.RTHV60                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXHV960O ON P916.RTHV96                                      
        ( DTSTAT ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
        , CVENDEUR ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXIE001O ON P916.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , CMODSTOCK ASC                                                         
        , CEMP13 ASC                                                            
        , CEMP23 ASC                                                            
        , CEMP33 ASC                                                            
        , CTYPEMPL3 ASC                                                         
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXIE003O ON P916.RTIE00                                      
        ( NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXIE004O ON P916.RTIE00                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC3 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXIE052O ON P916.RTIE05                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXIE301O ON P916.RTIE30                                      
        ( "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXIN001O ON P916.RTIN00                                      
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXIN050O ON P916.RTIN05                                      
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXIN901O ON P916.RTIN90                                      
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , LIEUTRT ASC                                                           
        , DINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXKIS01O ON P916.RTKIS0                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXKIS11O ON P916.RTKIS1                                      
        ( FILIALE ASC                                                           
        , NPLAN ASC                                                             
        , NCOMPTE ASC                                                           
        , NUMSS ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXKIS21O ON P916.RTKIS2                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXKIS31O ON P916.RTKIS3                                      
        ( CACIDOPER ASC                                                         
        , CODEOPER ASC                                                          
        , DATEOPER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXLG001 ON P916.RTLG00                                       
        ( CINSEE ASC                                                            
        , LNOMPVOIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXLG002 ON P916.RTLG00                                       
        ( CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXLM000O ON P916.RTLM00                                      
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DATTRANS ASC                                                          
        , NCAISSE ASC                                                           
        , NTRANS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXPR001O ON P916.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXPR002O ON P916.RTPR00                                      
        ( CTYPPREST ASC                                                         
        , NENTCDE ASC                                                           
        , CASSORT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXPR011O ON P916.RTPR01                                      
        ( CFAM ASC                                                              
        , WAFFICH ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXPR061O ON P916.RTPR06                                      
        ( CSTATUT ASC                                                           
        , LSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXPR062O ON P916.RTPR06                                      
        ( CSTATUT ASC                                                           
        , DEFFET ASC                                                            
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXPR063O ON P916.RTPR06                                      
        ( CSTATUT ASC                                                           
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXPS001O ON P916.RTPS00                                      
        ( NOMCLI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXQC011O ON P916.RTQC01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXQC111O ON P916.RTQC11                                      
        ( DTESSI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXQC112O ON P916.RTQC11                                      
        ( NCLIENT ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXQC113O ON P916.RTQC11                                      
        ( EMAIL ASC                                                             
          )                                                                     
           PCTFREE 30                                                           
    ;                                                                           
CREATE  INDEX P916.RXQC121O ON P916.RTQC12                                      
        ( DENVOI_QV ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXRA001O ON P916.RTRA00                                      
        ( CTYPDEMANDE ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXRA002O ON P916.RTRA00                                      
        ( NAVOIRCLIENT ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXRD000O ON P916.RTRD00                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXRE600O ON P916.RTRE60                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXRE610O ON P916.RTRE61                                      
        ( CTYPENREG ASC                                                         
        , WTYPE1 ASC                                                            
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
        , CPREST ASC                                                            
        , CENREG ASC                                                            
        , WTYPE2 ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXRX251O ON P916.RTRX25                                      
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXRX300O ON P916.RTRX30                                      
        ( NRELEVE ASC                                                           
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXRX551O ON P916.RTRX55                                      
        ( NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXSP101O ON P916.RTSP10                                      
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , SRP ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXSP200O ON P916.RTSP20                                      
        ( DTRAITEMENT ASC                                                       
        , CPROG ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXSP201O ON P916.RTSP20                                      
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXTL021O ON P916.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXTL022O ON P916.RTTL02                                      
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXTL040O ON P916.RTTL04                                      
        ( NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXTL111O ON P916.RTTL11                                      
        ( NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXVA000O ON P916.RTVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXVA001O ON P916.RTVA00                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXVA010O ON P916.RTVA01                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXVA011O ON P916.RTVA01                                      
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXVE021O ON P916.RTVE02                                      
        ( LNOM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVE022O ON P916.RTVE02                                      
        ( TELDOM ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVE102O ON P916.RTVE10                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVE103O ON P916.RTVE10                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.RXVE111O ON P916.RTVE11                                      
        ( NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVE113O ON P916.RTVE11                                      
        ( NCODIC ASC                                                            
        , WTOPELIVRE ASC                                                        
        , CTYPENREG ASC                                                         
        , CENREG ASC                                                            
        , WCQERESF ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVE114O ON P916.RTVE11                                      
        ( DCOMPTA ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVE115O ON P916.RTVE11                                      
        ( NSOCIETE ASC                                                          
        , DDELIV ASC                                                            
        , WTOPELIVRE ASC                                                        
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVE116O ON P916.RTVE11                                      
        ( DCREATION ASC                                                         
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  INDEX P916.RXVE141O ON P916.RTVE14                                      
        ( DSAISIE ASC                                                           
        , NSOCP ASC                                                             
        , NLIEUP ASC                                                            
        , NVENTE ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  INDEX P916.RXVI010O ON P916.RTVI01                                      
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.SXVA000O ON P916.STVA00                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.SXVA010O ON P916.STVA01                                      
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , CTYPMVTVALO ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD060O ON P916.RTAD06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD070O ON P916.RTAD07                               
        ( CSOC ASC                                                              
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD071O ON P916.RTAD07                               
        ( NENTCDEK ASC                                                          
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD100O ON P916.RTAD10                               
        ( CSOC ASC                                                              
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD101O ON P916.RTAD10                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD102O ON P916.RTAD10                               
        ( NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD110O ON P916.RTAD11                               
        ( CSOC ASC                                                              
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD111O ON P916.RTAD11                               
        ( CFAMK ASC                                                             
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD140O ON P916.RTAD14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD510O ON P916.RTAD51                               
        ( CSOC ASC                                                              
        , CCOULEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD511O ON P916.RTAD51                               
        ( CCOULEURK ASC                                                         
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD550O ON P916.RTAD55                               
        ( CUSINE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD610O ON P916.RTAD61                               
        ( CSOC ASC                                                              
        , CPAYS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAD611O ON P916.RTAD61                               
        ( CPAYSK ASC                                                            
        , CSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAI240O ON P916.RTAI24                               
        ( FDCLAS ASC                                                            
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAI270O ON P916.RTAI27                               
        ( CDESCRIPTIF ASC                                                       
        , FDFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAI530O ON P916.RTAI53                               
        ( FSSKU ASC                                                             
        , FSCTL ASC                                                             
        , FSCLAS ASC                                                            
        , FSFNUM ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAN010O ON P916.RTAN01                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
        , CDEST ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAV010O ON P916.RTAV01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAV020O ON P916.RTAV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
        , CTYPAVOIR ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXAV030O ON P916.RTAV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NAVOIR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBA000O ON P916.RTBA00                               
        ( NFACTBA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBA010O ON P916.RTBA01                               
        ( NBA ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBA100O ON P916.RTBA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DRECEP ASC                                                            
        , NECART ASC                                                            
        , CMODRGLT ASC                                                          
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBA200O ON P916.RTBA20                               
        ( CTIERSBA ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBA210O ON P916.RTBA21                               
        ( CTIERSBA ASC                                                          
        , DANNEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBA300O ON P916.RTBA30                               
        ( CTIERSBA ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBC310 ON P916.RTBC31                                
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBC320O ON P916.RTBC32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBC330O ON P916.RTBC33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBC340O ON P916.RTBC34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
        , DVERSION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXBC350O ON P916.RTBC35                               
        ( CPROGRAMME ASC                                                        
        , DDEBUT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCC000O ON P916.RTCC00                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCC010O ON P916.RTCC01                               
        ( DCAISSE ASC                                                           
        , NCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCC020O ON P916.RTCC02                               
        ( DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCC030O ON P916.RTCC03                               
        ( TYPTRANS ASC                                                          
        , MOPAI ASC                                                             
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NOPERATEUR ASC                                                        
        , HTRAIT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCC040O ON P916.RTCC04                               
        ( CPAICPT ASC                                                           
        , NCAISSE ASC                                                           
        , DCAISSE ASC                                                           
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCC050O ON P916.RTCC05                               
        ( NSOC ASC                                                              
        , DJOUR ASC                                                             
        , NSEQ ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD000O ON P916.RTCD00                               
        ( NCODIC ASC                                                            
        , NMAGGRP ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD050O ON P916.RTCD05                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD100O ON P916.RTCD10                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD150O ON P916.RTCD15                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD200O ON P916.RTCD20                               
        ( NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD250O ON P916.RTCD25                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD300O ON P916.RTCD30                               
        ( DCDE ASC                                                              
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD350O ON P916.RTCD35                               
        ( NCODIC ASC                                                            
        , NMAG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCD500O ON P916.RTCD50                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCE100O ON P916.RTCE10                               
        ( NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCE200O ON P916.RTCE20                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCT000O ON P916.RTCT00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DDELIV ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXCX000O ON P916.RTCX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , COMPTECG ASC                                                          
        , DATEFF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXDC000O ON P916.RTDC00                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXDC100O ON P916.RTDC10                               
        ( NDOSSIER ASC                                                          
        , NECHEANCE ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXDC200O ON P916.RTDC20                               
        ( NDOSSIER ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXDS000O ON P916.RTDS00                               
        ( CFAM ASC                                                              
        , CMARQ ASC                                                             
        , CAPPRO ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXDS050O ON P916.RTDS05                               
        ( NCODIC ASC                                                            
        , DSEMESTRE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXDS100O ON P916.RTDS10                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEF100O ON P916.RTEF10                               
        ( CTRAIT ASC                                                            
        , CTIERS ASC                                                            
        , NRENDU ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG000O ON P916.RTEG00                               
        ( NOMETAT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG050O ON P916.RTEG05                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG100O ON P916.RTEG10                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG110O ON P916.RTEG11                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG150O ON P916.RTEG15                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG200O ON P916.RTEG20                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
        , POSCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG250O ON P916.RTEG25                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
        , LIGNECALC ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG300O ON P916.RTEG30                               
        ( NOMETAT ASC                                                           
        , NOMCHAMP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXEG400O ON P916.RTEG40                               
        ( NOMETAT ASC                                                           
        , TYPLIGNE ASC                                                          
        , NOLIGNE ASC                                                           
        , CONTINUER ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFA010O ON P916.RTFA01                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFA020O ON P916.RTFA02                               
        ( NSOCV ASC                                                             
        , NLIEUV ASC                                                            
        , NORDV ASC                                                             
        , NSOCF ASC                                                             
        , NLIEUF ASC                                                            
        , NFACT ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF000O ON P916.RTFF00                               
        ( NPIECE ASC                                                            
        , DEXCPT DESC                                                           
        , PREFT1 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF050O ON P916.RTFF05                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF060O ON P916.RTFF06                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF100O ON P916.RTFF10                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF110O ON P916.RTFF11                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF120O ON P916.RTFF12                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF130O ON P916.RTFF13                               
        ( NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NRECCPT ASC                                                           
        , NCDE ASC                                                              
        , NCODIC ASC                                                            
        , PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF150O ON P916.RTFF15                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF200O ON P916.RTFF20                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNEFAC ASC                                                         
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF210O ON P916.RTFF21                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF250O ON P916.RTFF25                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , WTYPESEQ ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF300O ON P916.RTFF30                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF350O ON P916.RTFF35                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF360O ON P916.RTFF36                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF370O ON P916.RTFF37                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF400O ON P916.RTFF40                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF410O ON P916.RTFF41                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF420O ON P916.RTFF42                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF430O ON P916.RTFF43                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF440O ON P916.RTFF44                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF450O ON P916.RTFF45                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF500O ON P916.RTFF50                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
        , WQTEPRIX ASC                                                          
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF510O ON P916.RTFF51                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , DEXCPTLIT ASC                                                         
        , NPIECELIT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF600O ON P916.RTFF60                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF650O ON P916.RTFF65                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF660O ON P916.RTFF66                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF670O ON P916.RTFF67                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF700O ON P916.RTFF70                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF750O ON P916.RTFF75                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF800O ON P916.RTFF80                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF810O ON P916.RTFF81                               
        ( PREFT1 ASC                                                            
        , DEXCPT ASC                                                            
        , NPIECE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFF850O ON P916.RTFF85                               
        ( NSOC ASC                                                              
        , NAUX ASC                                                              
        , NFOUR ASC                                                             
        , CFAMCPT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFG000O ON P916.RTFG00                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFI000O ON P916.RTFI00                               
        ( CPROF ASC                                                             
        , QTAUX ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFI050O ON P916.RTFI05                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM010O ON P916.RTFM01                               
        ( NENTITE ASC                                                           
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM020O ON P916.RTFM02                               
        ( CDEVISE ASC                                                           
        , WSENS ASC                                                             
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM030O ON P916.RTFM03                               
        ( NENTITE ASC                                                           
        , CMETHODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM040O ON P916.RTFM04                               
        ( CDEVISE ASC                                                           
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM050O ON P916.RTFM05                               
        ( CDEVORIG ASC                                                          
        , CDEVDEST ASC                                                          
        , DEFFET ASC                                                            
        , COPERAT ASC                                                           
        , PTAUX ASC                                                             
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM060O ON P916.RTFM06                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM100O ON P916.RTFM10                               
        ( NENTITE ASC                                                           
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM200O ON P916.RTFM20                               
        ( NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM230O ON P916.RTFM23                               
        ( NENTITE ASC                                                           
        , NEXER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM540O ON P916.RTFM54                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM550O ON P916.RTFM55                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM560O ON P916.RTFM56                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM570O ON P916.RTFM57                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM580O ON P916.RTFM58                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM590O ON P916.RTFM59                               
        ( CODE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM641O ON P916.RTFM64                               
        ( STEAPP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM691 ON P916.RTFM69                                
        ( COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM730O ON P916.RTFM73                               
        ( CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM801O ON P916.RTFM80                               
        ( CNATOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM811O ON P916.RTFM81                               
        ( CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM821O ON P916.RTFM82                               
        ( CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM831O ON P916.RTFM83                               
        ( CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM841O ON P916.RTFM84                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CTVA ASC                                                              
        , CGEO ASC                                                              
        , WGROUPE ASC                                                           
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM871O ON P916.RTFM87                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM881O ON P916.RTFM88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM891O ON P916.RTFM89                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM901O ON P916.RTFM90                               
        ( CINTERFACE ASC                                                        
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM910O ON P916.RTFM91                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM921O ON P916.RTFM92                               
        ( CZONE ASC                                                             
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFM930O ON P916.RTFM93                               
        ( CRITLIEU1 ASC                                                         
        , CRITLIEU2 ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT010O ON P916.RTFT01                               
        ( NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT020O ON P916.RTFT02                               
        ( CACID ASC                                                             
        , NAUX ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT030O ON P916.RTFT03                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT040O ON P916.RTFT04                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT050O ON P916.RTFT05                               
        ( CBANQUE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT060O ON P916.RTFT06                               
        ( NSOC ASC                                                              
        , CPTR ASC                                                              
        , NAUX ASC                                                              
        , CREGTVA ASC                                                           
        , NATURE ASC                                                            
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT070O ON P916.RTFT07                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT080O ON P916.RTFT08                               
        ( CETAT ASC                                                             
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT090O ON P916.RTFT09                               
        ( CETAT ASC                                                             
        , CSERVICE ASC                                                          
        , NDEMANDE ASC                                                          
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT100O ON P916.RTFT10                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT110O ON P916.RTFT11                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , WTYPEADR ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT120O ON P916.RTFT12                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT130O ON P916.RTFT13                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT140O ON P916.RTFT14                               
        ( CBANQUE ASC                                                           
        , METHREG ASC                                                           
        , NREG ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT150O ON P916.RTFT15                               
        ( NORDRE ASC                                                            
        , NPROPO ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT160O ON P916.RTFT16                               
        ( NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT170O ON P916.RTFT17                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT180O ON P916.RTFT18                               
        ( METHREG ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT190O ON P916.RTFT19                               
        ( CTRAIT ASC                                                            
        , CACID ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT200O ON P916.RTFT20                               
        ( NAUX ASC                                                              
        , CDEVISE ASC                                                           
        , METHREG ASC                                                           
        , CTRESO ASC                                                            
        , WACCES ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT210O ON P916.RTFT21                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NLIGNE ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT220O ON P916.RTFT22                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , COMPTE ASC                                                            
        , SSCOMPTE ASC                                                          
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
        , STEAPP ASC                                                            
        , CDEVISE ASC                                                           
        , NEXER ASC                                                             
        , NPER ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , WSENS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT230O ON P916.RTFT23                               
        ( NEXER ASC                                                             
        , NPER ASC                                                              
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT250O ON P916.RTFT25                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT260O ON P916.RTFT26                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT270O ON P916.RTFT27                               
        ( NLIGNE ASC                                                            
        , NPIECE ASC                                                            
        , DECH ASC                                                              
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NAUXR ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT280O ON P916.RTFT28                               
        ( METHREG ASC                                                           
        , NPROPO ASC                                                            
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT290O ON P916.RTFT29                               
        ( NCODE ASC                                                             
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT300O ON P916.RTFT30                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT320O ON P916.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT321O ON P916.RTFT32                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
        , SECTION ASC                                                           
        , RUBRIQUE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT330K ON P916.RTFT33                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFERENCE ASC                                                         
        , NVENTIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT340O ON P916.RTFT34                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT350 ON P916.RTFT35                                
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , WTRT ASC                                                              
        , NEXER ASC                                                             
        , NPER ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT360 ON P916.RTFT36                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT370 ON P916.RTFT37                                
        ( NJRN ASC                                                              
        , REFDOC ASC                                                            
        , CDEVISE ASC                                                           
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT380 ON P916.RTFT38                                
        ( REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT390 ON P916.RTFT39                                
        ( NLIGNE ASC                                                            
        , REFDOC ASC                                                            
        , NJRN ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT400 ON P916.RTFT40                                
        ( CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT410 ON P916.RTFT41                                
        ( NATCH ASC                                                             
        , CATCH ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT430O ON P916.RTFT43                               
        ( NPIECE DESC                                                           
        , NJRN ASC                                                              
        , NEXER ASC                                                             
        , CDEVISE ASC                                                           
        , NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT440O ON P916.RTFT44                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT450 ON P916.RTFT45                                
        ( CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT460 ON P916.RTFT46                                
        ( NPER ASC                                                              
        , CPOND ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT490O ON P916.RTFT49                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NATURE ASC                                                            
        , NAUX ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT500O ON P916.RTFT50                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT510O ON P916.RTFT51                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NJRN ASC                                                              
        , COMPTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT520O ON P916.RTFT52                               
        ( CACID ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT530O ON P916.RTFT53                               
        ( CACID ASC                                                             
        , NSOC ASC                                                              
        , NETAB ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT540O ON P916.RTFT54                               
        ( NPIECE ASC                                                            
        , NJRN ASC                                                              
        , DECH ASC                                                              
        , NLIGNE ASC                                                            
        , NEXER ASC                                                             
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT570O ON P916.RTFT57                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , NAUX ASC                                                              
        , NTIERSCV ASC                                                          
        , WTYPEADR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT601O ON P916.RTFT60                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT611O ON P916.RTFT61                               
        ( SECTION ASC                                                           
        , NSOC ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT621O ON P916.RTFT62                               
        ( RUBRIQUE ASC                                                          
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT631O ON P916.RTFT63                               
        ( NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT651O ON P916.RTFT65                               
        ( CMASQUEC ASC                                                          
        , CMASQUES ASC                                                          
        , CMASQUER ASC                                                          
        , CMASQUEE ASC                                                          
        , CMASQUEA ASC                                                          
        , NENTITE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT660O ON P916.RTFT66                               
        ( COMPTE ASC                                                            
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT670O ON P916.RTFT67                               
        ( SECTION ASC                                                           
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDS ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT680O ON P916.RTFT68                               
        ( RUBRIQUE ASC                                                          
        , CCATEGORIE ASC                                                        
        , NSEQ ASC                                                              
        , CPDR ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT700O ON P916.RTFT70                               
        ( COMPTE ASC                                                            
        , CPCG ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT710O ON P916.RTFT71                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT720O ON P916.RTFT72                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , REFDOC ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT730O ON P916.RTFT73                               
        ( NSOC ASC                                                              
        , CNOMPROG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT740O ON P916.RTFT74                               
        ( NSOC ASC                                                              
        , NETAB ASC                                                             
        , CNOMPROG ASC                                                          
        , DDATE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT750O ON P916.RTFT75                               
        ( DDEMANDE DESC                                                         
        , NTIERS ASC                                                            
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT851O ON P916.RTFT85                               
        ( CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET ASC                                                            
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT861O ON P916.RTFT86                               
        ( DEFFET ASC                                                            
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
        , SOCREF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT871O ON P916.RTFT87                               
        ( SECTIONO ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT881O ON P916.RTFT88                               
        ( CINTERFACE ASC                                                        
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT891O ON P916.RTFT89                               
        ( CNATOPER ASC                                                          
        , CTYPOPER ASC                                                          
        , CINTERFACE ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFT900O ON P916.RTFT90                               
        ( NFOUR ASC                                                             
        , NAUX ASC                                                              
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX000O ON P916.RTFX00                               
        ( NOECS ASC                                                             
        , DATEFF DESC                                                           
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX001O ON P916.RTFX00                               
        ( NOECS ASC                                                             
        , CRITERE1 ASC                                                          
        , CRITERE2 ASC                                                          
        , CRITERE3 ASC                                                          
        , WFX00 ASC                                                             
        , DATEFF DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX010O ON P916.RTFX01                               
        ( NCOMPTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX050O ON P916.RTFX05                               
        ( NJRN ASC                                                              
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX100O ON P916.RTFX10                               
        ( COMPTEGL ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX110O ON P916.RTFX11                               
        ( CZONE ASC                                                             
        , ZONEGL ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX150O ON P916.RTFX15                               
        ( WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX200O ON P916.RTFX20                               
        ( CTYPSTAT ASC                                                          
        , NDEPTLMELA ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX300O ON P916.RTFX30                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX350O ON P916.RTFX35                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX400O ON P916.RTFX40                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NUMCAIS ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX450O ON P916.RTFX45                               
        ( CODEAPPLI ASC                                                         
        , NEXERCICE ASC                                                         
        , NPERIODE ASC                                                          
        , DCREAT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXFX500O ON P916.RTFX50                               
        ( DATTRANS ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , CTYPTRANS ASC                                                         
        , CAPPRET ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA000O ON P916.RTGA00                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA010O ON P916.RTGA01                               
        ( CTABLEG1 ASC                                                          
        , CTABLEG2 ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA030O ON P916.RTGA03                               
        ( CSOC ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA040O ON P916.RTGA04                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA060O ON P916.RTGA06                               
        ( NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA070O ON P916.RTGA07                               
        ( CGRPFOURN ASC                                                         
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA080O ON P916.RTGA08                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA090O ON P916.RTGA09                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CMARKETIN3 ASC                                                        
        , CVMARKETIN3 ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA100O ON P916.RTGA10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA110O ON P916.RTGA11                               
        ( CETAT ASC                                                             
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA130O ON P916.RTGA13                               
        ( CFAM ASC                                                              
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA140O ON P916.RTGA14                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA150O ON P916.RTGA15                               
        ( CFAM ASC                                                              
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA160O ON P916.RTGA16                               
        ( CFAM ASC                                                              
        , CGARANTCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA170O ON P916.RTGA17                               
        ( CFAM ASC                                                              
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA180O ON P916.RTGA18                               
        ( CFAM ASC                                                              
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA200O ON P916.RTGA20                               
        ( CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA210O ON P916.RTGA21                               
        ( CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA211O ON P916.RTGA21                               
        ( WRAYONFAM ASC                                                         
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA220O ON P916.RTGA22                               
        ( CMARQ ASC                                                             
        , LMARQ ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA230O ON P916.RTGA23                               
        ( CMARKETING ASC                                                        
        , LMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA240O ON P916.RTGA24                               
        ( CDESCRIPTIF ASC                                                       
        , LDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA250O ON P916.RTGA25                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
        , CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA260O ON P916.RTGA26                               
        ( CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA270O ON P916.RTGA27                               
        ( CDESCRIPTIF ASC                                                       
        , CVDESCRIPT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA290O ON P916.RTGA29                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , WSEQED ASC                                                            
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
--MW CREATE  UNIQUE INDEX P916.RXGA300O ON P916.RTGA30                               
--MW        ( CPARAM ASC                                                            
--MW        , CFAM ASC                                                              
--MW          )                                                                     
--MW           PCTFREE 10                                                           
--MW    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA301O ON P916.RTGA30                               
        ( CPARAM ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA310O ON P916.RTGA31                               
        ( NCODIC ASC                                                            
        , NEAN ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA320O ON P916.RTGA32                               
        ( NCODIC ASC                                                            
        , NSEQUENCE ASC                                                         
        , NEANCOMPO ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA330O ON P916.RTGA33                               
        ( NCODIC ASC                                                            
        , CDEST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA350O ON P916.RTGA35                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CPARAM ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA390O ON P916.RTGA39                               
        ( NENTSAP ASC                                                           
        , NENTANC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA400O ON P916.RTGA40                               
        ( CGARANTIE ASC                                                         
        , CFAM ASC                                                              
        , CTARIF ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA410O ON P916.RTGA41                               
        ( CGCPLT ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA420O ON P916.RTGA42                               
        ( CGCPLT ASC                                                            
        , NMOIS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA430O ON P916.RTGA43                               
        ( CSECTEUR ASC                                                          
        , CINSEE ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA440O ON P916.RTGA44                               
        ( CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA450O ON P916.RTGA45                               
        ( CFAM ASC                                                              
        , CGRP ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA460O ON P916.RTGA46                               
        ( CGRP ASC                                                              
        , CSECT1 ASC                                                            
        , ZONE1 ASC                                                             
        , CSECT2 ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA480O ON P916.RTGA48                               
        ( NCODIC ASC                                                            
        , SPPRTP ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA510O ON P916.RTGA51                               
        ( NCODIC ASC                                                            
        , CTYPDCL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA520O ON P916.RTGA52                               
        ( NCODIC ASC                                                            
        , CGARANTCPLT ASC                                                       
        , CVGARANCPLT ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA530O ON P916.RTGA53                               
        ( NCODIC ASC                                                            
        , CDESCRIPTIF ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA550O ON P916.RTGA55                               
        ( NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA560O ON P916.RTGA56                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA580O ON P916.RTGA58                               
        ( NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , CTYPLIEN ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA590O ON P916.RTGA59                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA600O ON P916.RTGA60                               
        ( NCODIC ASC                                                            
        , NMESSDEPOT ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA620O ON P916.RTGA62                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA630O ON P916.RTGA63                               
        ( NCODIC ASC                                                            
        , CARACTSPE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA640O ON P916.RTGA64                               
        ( NCODIC ASC                                                            
        , CMODDEL ASC                                                           
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA650O ON P916.RTGA65                               
        ( NCODIC ASC                                                            
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA660O ON P916.RTGA66                               
        ( NCODIC ASC                                                            
        , CSENS ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA680O ON P916.RTGA68                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA710O ON P916.RTGA71                               
        ( CTABLE ASC                                                            
        , CSTABLE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA720O ON P916.RTGA72                               
        ( CNUMCONT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA730O ON P916.RTGA73                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA740O ON P916.RTGA74                               
        ( NCODIC ASC                                                            
        , NTITRE ASC                                                            
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA750O ON P916.RTGA75                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA760O ON P916.RTGA76                               
        ( CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA770O ON P916.RTGA77                               
        ( NCODIC ASC                                                            
        , NZONPRIX ASC                                                          
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA790O ON P916.RTGA79                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA810O ON P916.RTGA81                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA830O ON P916.RTGA83                               
        ( NCODIC ASC                                                            
        , CMARKETING ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA910O ON P916.RTGA91                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA920O ON P916.RTGA92                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA930O ON P916.RTGA93                               
        ( CETAT ASC                                                             
        , WSEQPRO ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA940O ON P916.RTGA94                               
        ( CPROG ASC                                                             
        , CCHAMP ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGA990O ON P916.RTGA99                               
        ( CNOMPGRM ASC                                                          
        , NSEQERR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGB300O ON P916.RTGB30                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGB800O ON P916.RTGB80                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGD990O ON P916.RTGD99                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGF350O ON P916.RTGF35                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGF400O ON P916.RTGF40                               
        ( NENTCDE ASC                                                           
        , CINTERLOCUT ASC                                                       
        , WMARQFAM ASC                                                          
        , CMARQFAM ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGF500O ON P916.RTGF50                               
        ( NCDE ASC                                                              
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGGXX0O ON P916.RTGGXX                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG050O ON P916.RTGG05                               
        ( NCONC ASC                                                             
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG100O ON P916.RTGG10                               
        ( NDEMALI ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG150O ON P916.RTGG15                               
        ( NDEMALI ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG200O ON P916.RTGG20                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG250O ON P916.RTGG25                               
        ( CHEFPROD ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG301O ON P916.RTGG30                               
        ( DANNEE ASC                                                            
        , DMOIS ASC                                                             
        , CFAM ASC                                                              
        , CHEFPROD ASC                                                          
        , NCONC ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG400O ON P916.RTGG40                               
        ( NCODIC ASC                                                            
        , DEFFET ASC                                                            
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG500O ON P916.RTGG50                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG510O ON P916.RTGG51                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 15                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG550O ON P916.RTGG55                               
        ( NCODIC ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG600O ON P916.RTGG60                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , DDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGG700O ON P916.RTGG70                               
        ( NCODIC ASC                                                            
        , NREC ASC                                                              
        , NCDE ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI1000O ON P916.RTGI10                              
        ( NSOCIETE ASC                                                          
        , CTABINT ASC                                                           
        , TTABINT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI1200O ON P916.RTGI12                              
        ( NSOCIETE ASC                                                          
        , CTABINTMB ASC                                                         
        , MB ASC                                                                
        , TMB ASC                                                               
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI1400O ON P916.RTGI14                              
        ( NSOCIETE ASC                                                          
        , CTABINTCA ASC                                                         
        , CA ASC                                                                
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI1600O ON P916.RTGI16                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI2000O ON P916.RTGI20                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI2100O ON P916.RTGI21                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CFAM ASC                                                              
        , CMARKETIN1 ASC                                                        
        , CVMARKETIN1 ASC                                                       
        , CMARKETIN2 ASC                                                        
        , CVMARKETIN2 ASC                                                       
        , CTYPEAFF ASC                                                          
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI2200O ON P916.RTGI22                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI2300O ON P916.RTGI23                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGI2400O ON P916.RTGI24                              
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
        , CSIMU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGJ100O ON P916.RTGJ10                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGJ400O ON P916.RTGJ40                               
        ( NSOCIETE ASC                                                          
        , NZONPRIX ASC                                                          
        , CSIMU ASC                                                             
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGM010O ON P916.RTGM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGM060O ON P916.RTGM06                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , CZPRIX ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGM700O ON P916.RTGM70                               
        ( NSOCIETE ASC                                                          
        , NZONE ASC                                                             
        , CHEFPROD ASC                                                          
        , WSEQFAM ASC                                                           
        , LVMARKET1 ASC                                                         
        , LVMARKET2 ASC                                                         
        , LVMARKET3 ASC                                                         
        , NCODIC2 ASC                                                           
        , NCODIC ASC                                                            
        , "SEQUENCE" ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGP000O ON P916.RTGP00                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGP010O ON P916.RTGP01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGP020O ON P916.RTGP02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , DLIVRAISON ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGP030O ON P916.RTGP03                               
        ( DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGP041O ON P916.RTGP04                               
        ( NCODIC ASC                                                            
        , WTYPMVT ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGP050O ON P916.RTGP05                               
        ( CRAYON ASC                                                            
        , WRAYONFAM ASC                                                         
        , DANNEE ASC                                                            
        , DSEMAINE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ000O ON P916.RTGQ00                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ011O ON P916.RTGQ01                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ020O ON P916.RTGQ02                               
        ( CMODDEL ASC                                                           
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , DJOUR ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ030O ON P916.RTGQ03                               
        ( CMODDEL ASC                                                           
        , CEQUIP ASC                                                            
        , DJOUR ASC                                                             
        , CZONLIV ASC                                                           
        , CPLAGE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ040O ON P916.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , DEFFET ASC                                                            
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ041O ON P916.RTGQ04                               
        ( CMODDEL ASC                                                           
        , CPERIMETRE ASC                                                        
        , CFAM ASC                                                              
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ120O ON P916.RTGQ12                               
        ( CINSEE ASC                                                            
        , CGRP ASC                                                              
        , CTYPGRP ASC                                                           
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGQ200O ON P916.RTGQ20                               
        ( NSOC ASC                                                              
        , CINSEE ASC                                                            
        , CCADRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGR300O ON P916.RTGR30                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , WTLMELA ASC                                                           
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGR350O ON P916.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGR352O ON P916.RTGR35                               
        ( NRECCPT ASC                                                           
        , NREC ASC                                                              
        , NCDE ASC                                                              
        , WTLMELA ASC                                                           
        , NCODIC ASC                                                            
        , NAVENANT ASC                                                          
        , QREC ASC                                                              
        , QPRISE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGS300O ON P916.RTGS30                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGS310O ON P916.RTGS31                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , WSTOCKMASQ ASC                                                        
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGS450O ON P916.RTGS45                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGS500O ON P916.RTGS50                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGS550O ON P916.RTGS55                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGS600O ON P916.RTGS60                               
        ( NHS ASC                                                               
        , NLIEUENT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV020O ON P916.RTGV02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV030O ON P916.RTGV03                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV040O ON P916.RTGV04                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV060O ON P916.RTGV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV080O ON P916.RTGV08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV100O ON P916.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 20                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV101O ON P916.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 20                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV102O ON P916.RTGV10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV110O ON P916.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV119O ON P916.RTGV11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV130O ON P916.RTGV13                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV140O ON P916.RTGV14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV150O ON P916.RTGV15                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV230O ON P916.RTGV23                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV250O ON P916.RTGV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTATION ASC                                                       
        , NSEQ ASC                                                              
        , DCREATION ASC                                                         
        , HCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV260O ON P916.RTGV26                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV270O ON P916.RTGV27                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DAUTORD ASC                                                           
        , NAUTORD ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV310O ON P916.RTGV31                               
        ( CVENDEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV350O ON P916.RTGV35                               
        ( NSOCIETE ASC                                                          
        , NBONENLV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV400O ON P916.RTGV40                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV410O ON P916.RTGV41                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE ASC                                                            
        , CTYPADR ASC                                                           
        , CMODDEL ASC                                                           
        , DDELIV ASC                                                            
        , CPLAGE ASC                                                            
        , NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXGV810O ON P916.RTGV81                               
        ( CVENDEUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHA300O ON P916.RTHA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHC000O ON P916.RTHC00                               
        ( NCHS ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHC030O ON P916.RTHC03                               
        ( NCHS ASC                                                              
        , NSEQ ASC                                                              
        , NLIEUHC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE000O ON P916.RTHE00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE010O ON P916.RTHE01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE100O ON P916.RTHE10                               
        ( NLIEUHED ASC                                                          
        , NGHE ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE150O ON P916.RTHE15                               
        ( CLIEUHET ASC                                                          
        , NLIEUHED ASC                                                          
        , NGHE ASC                                                              
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE200O ON P916.RTHE20                               
        ( NLIEUHED ASC                                                          
        , CLIEUHET ASC                                                          
        , NRETOUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE210O ON P916.RTHE21                               
        ( CLIEUHET ASC                                                          
        , CLIEUHEI ASC                                                          
        , NLOTDIAG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE220O ON P916.RTHE22                               
        ( CLIEUHET ASC                                                          
        , NENVOI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHE230O ON P916.RTHE23                               
        ( CLIEUHET ASC                                                          
        , NDEMANDE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHL000O ON P916.RTHL00                               
        ( NOMMAP ASC                                                            
        , POSIT ASC                                                             
        , ZECRAN ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHL050O ON P916.RTHL05                               
        ( CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHL100O ON P916.RTHL10                               
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , CTABLE ASC                                                            
        , CCHAMPR ASC                                                           
        , CCHAMPL ASC                                                           
        , NREQUETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHL150 ON P916.RTHL15                                
        ( NOMMAP ASC                                                            
        , ZECRAN ASC                                                            
        , NOCCUR ASC                                                            
        , NSEQCRIT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHN000O ON P916.RTHN00                               
        ( CTIERS ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHN010O ON P916.RTHN01                               
        ( CTIERS ASC                                                            
        , CMARQ ASC                                                             
        , CFAM ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHN110O ON P916.RTHN11                               
        ( CTIERS ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV000O ON P916.RTHV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CMODDEL ASC                                                           
        , DOPER ASC                                                             
        , WSEQFAM ASC                                                           
        , CMARKETING ASC                                                        
        , CVMARKETING ASC                                                       
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV020O ON P916.RTHV02                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV030O ON P916.RTHV03                               
        ( DVENTECIALE ASC                                                       
        , CPRESTATION ASC                                                       
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV060O ON P916.RTHV06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV070O ON P916.RTHV07                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV080O ON P916.RTHV08                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV090O ON P916.RTHV09                               
        ( NSOCIETE ASC                                                          
        , CFAM ASC                                                              
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV100O ON P916.RTHV10                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV110O ON P916.RTHV11                               
        ( NSOCIETE ASC                                                          
        , CRAYONFAM ASC                                                         
        , WRAYONFAM ASC                                                         
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV150O ON P916.RTHV15                               
        ( CPROGRAMME ASC                                                        
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTECIALE ASC                                                       
        , CTERMID ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV160O ON P916.RTHV16                               
        ( NSOCIETE ASC                                                          
        , NCODIC ASC                                                            
        , NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV170O ON P916.RTHV17                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV180O ON P916.RTHV18                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DSVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV200O ON P916.RTHV20                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV250O ON P916.RTHV25                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CRAYON ASC                                                            
        , CINSEE ASC                                                            
        , DGRPMOIS ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV260O ON P916.RTHV26                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV270O ON P916.RTHV27                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV280O ON P916.RTHV28                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , WSEQPRO ASC                                                           
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV290O ON P916.RTHV29                               
        ( NSOCIETE ASC                                                          
        , WSEQPRO ASC                                                           
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV300O ON P916.RTHV30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , NAGREGATED ASC                                                        
        , DSVENTECIALE ASC                                                      
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV320O ON P916.RTHV32                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV330O ON P916.RTHV33                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV340O ON P916.RTHV34                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV350O ON P916.RTHV35                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NCODIC ASC                                                            
        , NCODICLIE ASC                                                         
        , DMVENTELIVREE ASC                                                     
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV700O ON P916.RTHV70                               
        ( NSOCIETE ASC                                                          
        , DMOIS ASC                                                             
        , NCODIC ASC                                                            
        , NENTCDE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV820O ON P916.RTHV82                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV830O ON P916.RTHV83                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CVENDEUR ASC                                                          
        , DMVENTE ASC                                                           
        , NAGREGATED ASC                                                        
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV860O ON P916.RTHV86                               
        ( NCODIC ASC                                                            
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CGARANTIE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXHV920O ON P916.RTHV92                               
        ( NSOCIETE ASC                                                          
        , MAGASIN ASC                                                           
        , NCODIC ASC                                                            
        , DVENTECIALE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIA000O ON P916.RTIA00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIA050O ON P916.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIA051O ON P916.RTIA05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIA600O ON P916.RTIA60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE000O ON P916.RTIE00                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NFICHE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE050O ON P916.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , WSEQFAM ASC                                                           
        , CMARQ ASC                                                             
        , LREFFOURN ASC                                                         
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE051O ON P916.RTIE05                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE100O ON P916.RTIE10                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE200O ON P916.RTIE20                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE250O ON P916.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE251O ON P916.RTIE25                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CAIRE ASC                                                             
        , CCOTE ASC                                                             
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE300O ON P916.RTIE30                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , "ZONE" ASC                                                            
        , SECTEUR ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE350O ON P916.RTIE35                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE360O ON P916.RTIE36                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CALLEE ASC                                                            
        , CNIVEAU ASC                                                           
        , NPOSITION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE450O ON P916.RTIE45                               
        ( NSOCLIVR ASC                                                          
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , CZONE ASC                                                             
        , CSECTEUR ASC                                                          
        , NSOUSSECT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE600O ON P916.RTIE60                               
        ( NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE700O ON P916.RTIE70                               
        ( NHS ASC                                                               
        , NLIEUHS ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE800O ON P916.RTIE80                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , CLIEUTRT ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIE900O ON P916.RTIE90                               
        ( NDEPOT ASC                                                            
        , NSOCDEPOT ASC                                                         
        , CREGROUP ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIJ000O ON P916.RTIJ00                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIN000O ON P916.RTIN00                               
        ( NSOCMAG ASC                                                           
        , NMAG ASC                                                              
        , DINVENTAIRE ASC                                                       
        , NCODIC ASC                                                            
        , NSSLIEU ASC                                                           
        , WSTOCKMASQ ASC                                                        
        , CSTATUT ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIN100O ON P916.RTIN01                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , SSLIEU ASC                                                            
        , CSTATUT ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIN900O ON P916.RTIN90                               
        ( DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIT000O ON P916.RTIT00                               
        ( NINVENTAIRE ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIT050O ON P916.RTIT05                               
        ( NINVENTAIRE ASC                                                       
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIT100O ON P916.RTIT10                               
        ( NINVENTAIRE ASC                                                       
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIT150O ON P916.RTIT15                               
        ( NINVENTAIRE ASC                                                       
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXIT900O ON P916.RTIT90                               
        ( NINVENTAIRE ASC                                                       
        , NSOCDEPOT ASC                                                         
        , NDEPOT ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXLG000 ON P916.RTLG00                                
        ( CINSEE ASC                                                            
        , CTVOIE ASC                                                            
        , LNOMVOIE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXLG010O ON P916.RTLG01                               
        ( CINSEE ASC                                                            
        , NVOIE ASC                                                             
        , CPARITE ASC                                                           
        , NDEBTR ASC                                                            
        , NFINTR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXLM100O ON P916.RTLM10                               
        ( NLISTE ASC                                                            
        , NAPPORT ASC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXLT000O ON P916.RTLT00                               
        ( NLETTRE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXLT010O ON P916.RTLT01                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXLT020O ON P916.RTLT02                               
        ( NLETTRE ASC                                                           
        , NPAGE ASC                                                             
        , NLIGNE ASC                                                            
        , INDVAR ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMG010O ON P916.RTMG01                               
        ( CFAMMGI ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMO010O ON P916.RTMO01                               
        ( TYPOFFRE ASC                                                          
        , NSOC ASC                                                              
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , CPRESTA ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMQ150O ON P916.RTMQ15                               
        ( CPROGRAMME ASC                                                        
        , CFONCTION ASC                                                         
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSOCDEST ASC                                                          
        , NLIEUDEST ASC                                                         
        , DOPERATION ASC                                                        
        , "COMMENT" ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMQ160O ON P916.RTMQ16                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU050O ON P916.RTMU05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NSSLIEU ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU210O ON P916.RTMU21                               
        ( DSEMAINE ASC                                                          
        , CRAYONFAM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU250O ON P916.RTMU25                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU260O ON P916.RTMU26                               
        ( NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU300O ON P916.RTMU30                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU310O ON P916.RTMU31                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NROTATION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU350O ON P916.RTMU35                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXMU500O ON P916.RTMU50                               
        ( CTYPCDE ASC                                                           
        , NSEQED ASC                                                            
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPA700O ON P916.RTPA70                               
        ( NSOCIETE ASC                                                          
        , CPTSAV ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPC080O ON P916.RTPC08                               
        ( RANDOMNUM ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPC090O ON P916.RTPC09                               
        ( RANDOMNUM ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPM000O ON P916.RTPM00                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPM060O ON P916.RTPM06                               
        ( CMOPAI ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPP000O ON P916.RTPP00                               
        ( NLIEU ASC                                                             
        , DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPP001O ON P916.RTPP01                               
        ( NLIEU ASC                                                             
        , DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NRUB ASC                                                              
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPP010O ON P916.RTPP10                               
        ( DMOISPAYE ASC                                                         
        , CPRIME ASC                                                            
        , NLIGNE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR000O ON P916.RTPR00                               
        ( CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR010O ON P916.RTPR01                               
        ( CFAM ASC                                                              
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR020O ON P916.RTPR02                               
        ( NCODIC ASC                                                            
        , CPRESTATION ASC                                                       
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR030O ON P916.RTPR03                               
        ( CPRESTATION ASC                                                       
        , CGESTION ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR031O ON P916.RTPR03                               
        ( CGESTION ASC                                                          
        , CPRESTATION ASC                                                       
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR040O ON P916.RTPR04                               
        ( CPRESTATION ASC                                                       
        , CPRESTELT ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR050O ON P916.RTPR05                               
        ( CPRESTATION ASC                                                       
        , CGRPETAT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR060O ON P916.RTPR06                               
        ( CPRESTATION ASC                                                       
        , CSTATUT ASC                                                           
        , DEFFET ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR100O ON P916.RTPR10                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR110O ON P916.RTPR11                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR120O ON P916.RTPR12                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR130O ON P916.RTPR13                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR140O ON P916.RTPR14                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR150O ON P916.RTPR15                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR160O ON P916.RTPR16                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR170O ON P916.RTPR17                               
        ( CPRESTATION ASC                                                       
        , DEFFET DESC                                                           
        , NZONPRIX ASC                                                          
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPR200O ON P916.RTPR20                               
        ( CETAT ASC                                                             
        , CFAM ASC                                                              
        , CAGREPRE ASC                                                          
        , WSEQED ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPS000O ON P916.RTPS00                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPS010O ON P916.RTPS01                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPS020O ON P916.RTPS02                               
        ( CGCPLT ASC                                                            
        , NOSAV ASC                                                             
        , CSECTEUR ASC                                                          
        , DEFFET ASC                                                            
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPS030O ON P916.RTPS03                               
        ( CGCPLT ASC                                                            
        , NGCPLT ASC                                                            
        , NVENTE ASC                                                            
        , NLIEU ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPS040O ON P916.RTPS04                               
        ( NSOC ASC                                                              
        , NLIEU ASC                                                             
        , "TYPE" ASC                                                            
        , DINIT ASC                                                             
        , MONTTOT ASC                                                           
        , DUREE ASC                                                             
        , INFO1 ASC                                                             
        , INFO2 ASC                                                             
        , INFO3 ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPT030O ON P916.RTPT03                               
        ( CODLANG ASC                                                           
        , CNOMPGRM ASC                                                          
        , NSEQLIB ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPV000O ON P916.RTPV00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXPV800O ON P916.RTPV80                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NSEQNQ ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC010O ON P916.RTQC01                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC020O ON P916.RTQC02                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC030O ON P916.RTQC03                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC050O ON P916.RTQC05                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC060O ON P916.RTQC06                               
        ( NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
        , NCISOC ASC                                                            
        , CTCARTE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC110O ON P916.RTQC11                               
        ( NCARTE ASC                                                            
        , NCISOC ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC120O ON P916.RTQC12                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXQC130O ON P916.RTQC13                               
        ( NCARTE ASC                                                            
        , CTQUEST ASC                                                           
        , CQUESTION ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRA000O ON P916.RTRA00                               
        ( NDEMANDE ASC                                                          
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRA010O ON P916.RTRA01                               
        ( NDEMANDE ASC                                                          
        , NSEQCODIC ASC                                                         
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRA020O ON P916.RTRA02                               
        ( CTYPREGL ASC                                                          
        , NREGL ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRC000O ON P916.RTRC00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRC100O ON P916.RTRC10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NOMFIC ASC                                                            
        , DATEFIC ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRE000O ON P916.RTRE00                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DMVENTE ASC                                                           
        , CVENDEUR ASC                                                          
        , CFAM ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRE100O ON P916.RTRE10                               
        ( NLIEU ASC                                                             
        , DVENTELIVREE ASC                                                      
        , CRAYON ASC                                                            
        , CRAYONFAM ASC                                                         
        , WTLMELA ASC                                                           
        , NAGREGATED ASC                                                        
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX000O ON P916.RTRX00                               
        ( NCONC ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX050O ON P916.RTRX05                               
        ( NCONC ASC                                                             
        , CRAYON ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX100O ON P916.RTRX10                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , NFREQUENCE ASC                                                        
        , NSEQUENCE ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX150O ON P916.RTRX15                               
        ( NCONC ASC                                                             
        , NMAG ASC                                                              
        , CRAYON ASC                                                            
        , DSUPPORT ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX200O ON P916.RTRX20                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX210O ON P916.RTRX21                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX220O ON P916.RTRX22                               
        ( NRELEVE ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX250O ON P916.RTRX25                               
        ( NRELEVE ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX450O ON P916.RTRX45                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX500O ON P916.RTRX50                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CPROFIL ASC                                                           
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXRX550O ON P916.RTRX55                               
        ( NCODIC ASC                                                            
        , NCONC ASC                                                             
        , DRELEVE DESC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSB110O ON P916.RTSB11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 30                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSG020O ON P916.RTSG02                               
        ( SECPAIE ASC                                                           
        , CSECTION ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSG030O ON P916.RTSG03                               
        ( COMPTE ASC                                                            
        , NETABADM ASC                                                          
        , NETAB ASC                                                             
        , NSOC ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSL400O ON P916.RTSL40                               
        ( NSOCIETE ASC                                                          
        , CLIEUINV ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSL410O ON P916.RTSL41                               
        ( CLIEUINV ASC                                                          
        , NREGL ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSL500O ON P916.RTSL50                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CLIEUINV ASC                                                          
        , CZONE ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSL510O ON P916.RTSL51                               
        ( DINV ASC                                                              
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , CEMPL ASC                                                             
        , NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSM010O ON P916.RTSM01                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSM020O ON P916.RTSM02                               
        ( NCODIC1 ASC                                                           
        , NCODIC2 ASC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSP000O ON P916.RTSP00                               
        ( NENTCDE ASC                                                           
        , CFAM ASC                                                              
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSP050O ON P916.RTSP05                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSP100O ON P916.RTSP10                               
        ( NCODIC ASC                                                            
        , DJOUR DESC                                                            
        , NREC ASC                                                              
        , CMAJ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSP150O ON P916.RTSP15                               
        ( NCODIC ASC                                                            
        , DEFFET DESC                                                           
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NZONPRIX ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSP250O ON P916.RTSP25                               
        ( CMARQ ASC                                                             
        , NSOCDEST ASC                                                          
        , DPROV ASC                                                             
        , WTEBRB ASC                                                            
        , CTAUXTVA ASC                                                          
        , NLIEUDEST ASC                                                         
        , NLIEUORIG ASC                                                         
        , NSOCORIG ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXSV010O ON P916.RTSV01                               
        ( NSOCIETE ASC                                                          
        , NMAG ASC                                                              
        , CTPSAV ASC                                                            
        , DEFFET ASC                                                            
        , NSOCSAV ASC                                                           
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXTF300O ON P916.RTTF30                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXTF350O ON P916.RTTF35                               
        ( LFICHIER ASC                                                          
        , CRITERE ASC                                                           
        , DCREATION ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXTL020O ON P916.RTTL02                               
        ( DDELIV ASC                                                            
        , NSOC ASC                                                              
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXTL060O ON P916.RTTL06                               
        ( NSOC ASC                                                              
        , CPROTOUR ASC                                                          
        , DDELIV ASC                                                            
        , CTOURNEE ASC                                                          
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXTL080O ON P916.RTTL08                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOC ASC                                                              
        , NMAG ASC                                                              
        , NVENTE ASC                                                            
        , CADRTOUR ASC                                                          
        , CTOURNEE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXTL090O ON P916.RTTL09                               
        ( CLIVR ASC                                                             
        , DMMLIVR ASC                                                           
        , CRETOUR ASC                                                           
        , NDEPOT ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXTL110O ON P916.RTTL11                               
        ( DDELIV ASC                                                            
        , CPROTOUR ASC                                                          
        , NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA050O ON P916.RTVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA051O ON P916.RTVA05                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA060O ON P916.RTVA06                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA061O ON P916.RTVA06                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA100O ON P916.RTVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA150O ON P916.RTVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA160O ON P916.RTVA16                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA200O ON P916.RTVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA250O ON P916.RTVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA300O ON P916.RTVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA310O ON P916.RTVA31                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVA500O ON P916.RTVA50                               
        ( NSOCCOMPT ASC                                                         
        , NCODIC ASC                                                            
        , DOPER ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE020O ON P916.RTVE02                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE050O ON P916.RTVE05                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE060O ON P916.RTVE06                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE080O ON P916.RTVE08                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NDOSSIER ASC                                                          
        , NSEQ ASC                                                              
        , CCTRL ASC                                                             
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE100O ON P916.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , DVENTE ASC                                                            
        , NORDRE DESC                                                           
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE101O ON P916.RTVE10                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
          )                                                                     
           PCTFREE 5                                                            
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE110O ON P916.RTVE11                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQ ASC                                                              
        , CTYPENREG ASC                                                         
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.RXVE140O ON P916.RTVE14                               
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , DSAISIE ASC                                                           
        , NSEQ ASC                                                              
          )                                                                     
           PCTFREE 5                                                            
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA050O ON P916.STVA05                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA060O ON P916.STVA06                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA100O ON P916.STVA10                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
        , DMOIS ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA150O ON P916.STVA15                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA160O ON P916.STVA16                               
        ( NCODIC ASC                                                            
        , NLIEUVALO ASC                                                         
        , NSOCVALO ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA200O ON P916.STVA20                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA250O ON P916.STVA25                               
        ( CFAM ASC                                                              
        , CSEQRAYON ASC                                                         
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA300O ON P916.STVA30                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  UNIQUE INDEX P916.SXVA310O ON P916.STVA31                               
        ( NCODIC ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
           CLUSTER                                                              
    ;                                                                           
CREATE  INDEX P916.RXQTPSEPX ON P916.QTPSEPX                                    
        ( CFAM ASC                                                              
        , CGCPLT ASC                                                            
        , CTARIF ASC                                                            
        , WACTIF ASC                                                            
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.CCDIX_TTGV02 ON P916.TTGV02                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.CCDIX_TTGV11 ON P916.TTGV11                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.CCDIX_TTVE10 ON P916.TTVE10                                  
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXGV020 ON P916.TTGV02                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , WTYPEADR ASC                                                          
        , NORDRE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXGV021 ON P916.TTGV02                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXGV022 ON P916.TTGV02                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXGV110 ON P916.TTGV11                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , NCODICGRP ASC                                                         
        , NCODIC ASC                                                            
        , NSEQNQ ASC                                                            
        , CTYPENREG ASC                                                         
        , IBMSNAP_LOGMARKER ASC                                                 
        , DSYST ASC                                                             
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXGV111 ON P916.TTGV11                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXGV112 ON P916.TTGV11                                       
        ( IBMSNAP_LOGMARKER ASC                                                 
        , NVENTE ASC                                                            
        , DSYSTUPD ASC                                                          
        , NLIEU ASC                                                             
        , NSOCIETE ASC                                                          
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXVE101 ON P916.TTVE10                                       
        ( DSYSTUPD ASC                                                          
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  INDEX P916.TXVE102 ON P916.TTVE10                                       
        ( NSOCIETE ASC                                                          
        , NLIEU ASC                                                             
        , NVENTE ASC                                                            
        , IBMSNAP_LOGMARKER ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.CXGV020O ON P916.CTGV02                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.CXGV110O ON P916.CTGV11                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
CREATE  UNIQUE INDEX P916.CXVE100O ON P916.CTVE10                               
        ( IBMSNAP_COMMITSEQ ASC                                                 
        , IBMSNAP_INTENTSEQ ASC                                                 
          )                                                                     
           PCTFREE 10                                                           
    ;                                                                           
