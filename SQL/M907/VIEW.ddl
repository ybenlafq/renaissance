CREATE  VIEW M907.RVABCNT                                                       
        ( CTABLEG2 , CFAM , NCODIC , WTABLEG , NSOCLIEU , INFOCONT ,            
        CONTROLE , LCOMMENT ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 4 ) , SUBSTR ( WTABLEG , 11 , 15 ) , SUBSTR ( WTABLEG , 26 ,        
        25 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ABCNT'                                                      
;                                                                               
CREATE  VIEW M907.RVABTYP                                                       
        ( CTABLEG2 , TYPPREST , WTABLEG , LTYPREST ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 60 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ABTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVAC0100                                                      
        AS                                                                      
SELECT  ALL DVENTE , NSOCIETE , NLIEU , QTLM , QELA , QDACEM , CATLM ,          
        CAELA , CADACEM , QTRDARTY , QTRDACEM , QENTRE , QVENDEUR ,             
        COUVERTN1 , COUVERTJ1 , DSYST                                           
    FROM  M907.RTAC01                                                           
;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVAC0150                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  NSOCGRP , DVENTE , A.NSOCIETE , A.NLIEU , QTLM , QELA , QDACEM ,        
--MW DZ-400         CATLM , CAELA , CADACEM , QTRDARTY , QTRDACEM , QENTRE ,                
--MW DZ-400         QVENDEUR , COUVERTN1 , COUVERTJ1                                        
--MW DZ-400     FROM  M907.RTAC01 A , M907.RTGA10FP B                                       
--MW DZ-400   WHERE A.NSOCIETE = B.NSOCIETE                                                 
--MW DZ-400     AND A.NLIEU = B.NLIEU                                                       
--MW DZ-400 ;                                                                               
CREATE  VIEW M907.RVAC0200                                                      
        AS                                                                      
SELECT  ALL NSOCIETE , NLIEU , DVENTE , HVENTE , QTLM , QELA , QDACEM ,         
        CATLM , CAELA , CADACEM , QTRDARTY , QENTRE , DSYST                     
    FROM  M907.RTAC02                                                           
;                                                                               
CREATE  VIEW M907.RVAC0300                                                      
        AS                                                                      
SELECT  ALL NSOCIETE , NLIEU , DVENTE , HVENTE , QENTRE , DSYST                 
    FROM  M907.RTAC03                                                           
;                                                                               
CREATE  VIEW M907.RVAC0301                                                      
        AS                                                                      
SELECT  ALL NSOCIETE , NLIEU , DVENTE , HVENTE , QENTRE , DSYST ,               
        QSTATUT , QBOUCHON , WPRESENCE , WMODIF                                 
    FROM  M907.RTAC03                                                           
;                                                                               
CREATE  VIEW M907.RVAC0302                                                      
        AS                                                                      
SELECT  ALL NSOCIETE , NLIEU , DVENTE , HVENTE , QENTRE , DSYST ,               
        QSTATUT , QBOUCHON , WPRESENCE , WMODIF , CATOTAL                       
    FROM  M907.RTAC03                                                           
;                                                                               
CREATE  VIEW M907.RVACCEZ                                                       
        ( CTABLEG2 , CACCES , WTABLEG , LACCES , WML , ZAA , STOP ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ACCEZ'                                                      
;                                                                               
CREATE  VIEW M907.RVACTYP                                                       
        ( CTABLEG2 , TYPCLIEN , ACID , WTABLEG , ACTIF , COMMENT ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 50 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ACTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVADMCH                                                       
        ( CTABLEG2 , CACID , WTABLEG , A , GRP , CHA , COD ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ADMCH'                                                      
;                                                                               
CREATE  VIEW M907.RVAIGUI                                                       
        ( CTABLEG2 , CSOC , CPROG , CFONC , NOPTION , WTABLEG , LCOMMENT        
        , WFLAG ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , SUBSTR ( CTABLEG2 , 12 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 ) , SUBSTR ( WTABLEG , 26         
        , 1 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AIGUI'                                                      
;                                                                               
CREATE  VIEW M907.RVAN0000                                                      
        AS                                                                      
SELECT  ALL CNOMPGRM , NSEQERR , LIBERR , DSYST                                 
    FROM  M907.RTAN00                                                           
;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVAP0000                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  MATRICULE , MONTANT , DSYST                                             
--MW DZ-400     FROM  M907.RTAP00                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVAP0500                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  MATRICULE , NPIECE , DSYST                                              
--MW DZ-400     FROM  M907.RTAP05                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW M907.RVARAUT                                                       
        ( CTABLEG2 , LIDENT , WTABLEG , LCOMMENT ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ARAUT'                                                      
;                                                                               
CREATE  VIEW M907.RVATRB2                                                       
        ( CTABLEG2 , IATRB2 , WTABLEG , LIATRB2 ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ATRB2'                                                      
;                                                                               
CREATE  VIEW M907.RVATRB3                                                       
        ( CTABLEG2 , IATRB3 , WTABLEG , LIATRB3 , WATRB3 ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ATRB3'                                                      
;                                                                               
CREATE  VIEW M907.RVATRB4                                                       
        ( CTABLEG2 , IATRB4 , WTABLEG , LIATRB4 , WIATRB4 ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ATRB4'                                                      
;                                                                               
CREATE  VIEW M907.RVAUTGV                                                       
        ( CTABLEG2 , CACID , WTABLEG , CODE ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 60 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTGV'                                                      
;                                                                               
CREATE  VIEW M907.RVAUTLI                                                       
        ( CTABLEG2 , CACID , NSOC , CPARAM , WTABLEG , WACTIF , LIBELLE         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        3 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTLI'                                                      
;                                                                               
CREATE  VIEW M907.RVAUTOP                                                       
        ( CTABLEG2 , CODTABLE , WTABLEG , TYPEAUTO ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTOP'                                                      
;                                                                               
CREATE  VIEW M907.RVAUTOR                                                       
        ( CTABLEG2 , CACID , CNOMPGRM , WTABLEG , NSOC , NLIEU , WFLAG ,        
        LCOMMENT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 3 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTOR'                                                      
;                                                                               
CREATE  VIEW M907.RVAUTRE                                                       
        ( CTABLEG2 , USER , WTABLEG , NATURE , DELAI , AUTOR ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTRE'                                                      
;                                                                               
CREATE  VIEW M907.RVBASCU                                                       
        ( CTABLEG2 , CPROG , NSOCIETE , NLIEU , WFLAG , WFLAG2 , WTABLEG        
        , DATE ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 3 ) , SUBSTR ( CTABLEG2 , 13 , 1         
        ) , SUBSTR ( CTABLEG2 , 14 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,         
        1 , 8 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BASCU'                                                      
;                                                                               
CREATE  VIEW M907.RVBATRS                                                       
        ( CTABLEG2 , ANCSOC , ANCBA , VALIDE , WTABLEG , NOUVBA ,               
        NOUVSOC ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , SUBSTR ( CTABLEG2 , 10 , 1 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 3 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BATRS'                                                      
;                                                                               
CREATE  VIEW M907.RVBBFAC                                                       
        ( CTABLEG2 , IDCLIENT , WTABLEG , WACTIF , PART , LIBELLE ,             
        DEFFET ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 20 ) , SUBSTR ( WTABLEG , 27 , 8 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BBFAC'                                                      
;                                                                               
CREATE  VIEW M907.RVBDB2B                                                       
        ( CTABLEG2 , SOC , LIEU , WTABLEG , ACTIF , VALDEF , DEBRANCH ,         
        COMMENT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 40        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BDB2B'                                                      
;                                                                               
CREATE  VIEW M907.RVBDCLI                                                       
        ( CTABLEG2 , SOC , LIEU , WTABLEG , ACTIF , CTNVE , CURSEUR ,           
        COD_PAY ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 , 17        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BDCLI'                                                      
;                                                                               
CREATE  VIEW M907.RVBDCSV                                                       
        ( CTABLEG2 , SOC , LIEU , WTABLEG , CTRMQ , CILOC , CILOM ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BDCSV'                                                      
;                                                                               
CREATE  VIEW M907.RVBRIDM                                                       
        ( CTABLEG2 , DEPOT , CSELART , JOUR , WTABLEG , QVOLUM , QPIECE         
        , WACTIF ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , SUBSTR ( CTABLEG2 , 12 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR ( WTABLEG , 7 ,         
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BRIDM'                                                      
;                                                                               
CREATE  VIEW M907.RVBS000                                                       
        ( CTABLEG2 , NENTITE , TYPE , CTYPCND , WTABLEG , LIBELLE ,             
        CTYPOPER , CNATOPER ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR ( WTABLEG , 26        
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BS000'                                                      
;                                                                               
CREATE  VIEW M907.RVBS001                                                       
        ( CTABLEG2 , NENTITE , TYPE , CTYPCND , WTABLEG , LIBELLE ,             
        TIERS ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 )                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BS001'                                                      
;                                                                               
CREATE  VIEW M907.RVBS002                                                       
        ( CTABLEG2 , CODE , TYPE , CODES , WTABLEG , LIBELLE , CTYPNAT ,        
        WTVA ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR ( WTABLEG , 31         
        , 1 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BS002'                                                      
;                                                                               
CREATE  VIEW M907.RVBS003                                                       
        ( CTABLEG2 , GROUPE , FAMILLE , WTABLEG , LIBELLE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BS003'                                                      
;                                                                               
CREATE  VIEW M907.RVBS004                                                       
        ( CTABLEG2 , TYPCND , WTABLEG , V1 , V2 , V3 , V4 ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BS004'                                                      
;                                                                               
CREATE  VIEW M907.RVBSACC                                                       
        ( CTABLEG2 , SOCMAG , WTABLEG , CMODEL , WDATA ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSACC'                                                      
;                                                                               
CREATE  VIEW M907.RVBSAFT                                                       
        ( CTABLEG2 , TENT , WTABLEG , LENT , WAUT , PLAGE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 10 ) , SUBSTR (            
        WTABLEG , 26 , 40 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSAFT'                                                      
;                                                                               
CREATE  VIEW M907.RVBSCAL                                                       
        ( CTABLEG2 , CCALCUL , WTABLEG , WACTIF , LIBELLE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSCAL'                                                      
;                                                                               
CREATE  VIEW M907.RVBSDEF                                                       
        ( CTABLEG2 , ORIG , USER , WTABLEG , MDEF , TVDEF , CLDF ,              
        RMDACC ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 2 ) , SUBSTR ( WTABLEG , 6 , 4 ) , SUBSTR ( WTABLEG , 10 , 1        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSDEF'                                                      
;                                                                               
CREATE  VIEW M907.RVBSENT                                                       
        ( CTABLEG2 , CTYPENT , WTABLEG , LTYPENT , CTABLE , WCFAM ,             
        NBLOC , CTRANS ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 3 ) , SUBSTR (             
        WTABLEG , 31 , 4 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSENT'                                                      
;                                                                               
CREATE  VIEW M907.RVBSGES                                                       
        ( CTABLEG2 , CGEST , WTABLEG , LIBELLE , LGCGEST ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSGES'                                                      
;                                                                               
CREATE  VIEW M907.RVBSLIE                                                       
        ( CTABLEG2 , CLIEN , WTABLEG , LLIEN , CTRANS , WAUTO ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR (             
        WTABLEG , 26 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSLIE'                                                      
;                                                                               
CREATE  VIEW M907.RVBSPRO                                                       
        ( CTABLEG2 , PROFLIEN , WTABLEG , WACTIF , LIBELLE , WDONNEES )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 16 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSPRO'                                                      
;                                                                               
CREATE  VIEW M907.RVBSSEN                                                       
        ( CTABLEG2 , CSENS , WTABLEG , LSENS , PLAGE ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 50 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BSSEN'                                                      
;                                                                               
CREATE  VIEW M907.RVBTROW                                                       
        AS                                                                      
SELECT  ALL NOMTABLE , SEM01 , SEM02 , SEM03 , SEM04 , SEM05 , SEM06 ,          
        SEM07 , SEM08 , SEM09 , SEM10 , SEM11 , SEM12 , SEM13 , SEM14 ,         
        SEM15 , SEM16 , SEM17 , SEM18 , SEM19 , SEM20 , SEM21 , SEM22 ,         
        SEM23 , SEM24 , SEM25 , SEM26 , SEM27 , SEM28 , SEM29 , SEM30 ,         
        SEM31 , SEM32 , SEM33 , SEM34 , SEM35 , SEM36 , SEM37 , SEM38 ,         
        SEM39 , SEM40 , SEM41 , SEM42 , SEM43 , SEM44 , SEM45 , SEM46 ,         
        SEM47 , SEM48 , SEM49 , SEM50 , SEM51 , SEM52 , SEM53 , SEM54 ,         
        SEM55                                                                   
    FROM  M907.RTBTROW                                                          
;                                                                               
CREATE  VIEW M907.RVBYPAS                                                       
        ( CTABLEG2 , NOMPGM , CMPT , WTABLEG , NDSP , CONTEXTE , INDBLOC        
        , SOCLIEUB ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG        
        , 11 , 10 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR ( WTABLEG ,          
        22 , 6 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BYPAS'                                                      
;                                                                               
CREATE  VIEW M907.RVCADRE                                                       
        ( CTABLEG2 , PRESTA , FAMILLE , WTABLEG , CADRE , EQUIPE ,              
        COMMENT1 , COMMENT2 ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 ) , SUBSTR ( WTABLEG , 11 , 20 ) , SUBSTR ( WTABLEG , 31 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CADRE'                                                      
;                                                                               
CREATE  VIEW M907.RVCALEN                                                       
        ( CTABLEG2 , SOCMAG , WTABLEG , WTOP ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 60 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CALEN'                                                      
;                                                                               
CREATE  VIEW M907.RVCALPX                                                       
        ( CTABLEG2 , CZONVENT , CSENVENT , DEFFET , WTABLEG , FLAGACT ,         
        CTYPREGL , VALCOMM ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR ( WTABLEG , 3 , 5         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CALPX'                                                      
;                                                                               
CREATE  VIEW M907.RVCAM                                                         
        AS                                                                      
SELECT  *                                                                       
    FROM  M907.RTRC20                                                           
  WHERE NOMFIC = 'CAMCOSYS'                                                     
    AND NSOCIETE = '996'                                                        
;                                                                               
CREATE  VIEW M907.RVCASIM                                                       
        ( CTABLEG2 , NCASE , WTABLEG , IMPR , TAILLE , ALLEE ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 1 ) , SUBSTR (             
        WTABLEG , 12 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CASIM'                                                      
;                                                                               
CREATE  VIEW M907.RVCAXIS                                                       
        ( CTABLEG2 , CMOPAI , WTABLEG , CARDT , COPERD , COPERC , CTRLC         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CAXIS'                                                      
;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVCB00                                                        
--MW DZ-400         ( ENRFIC1 , ENRFIC2 ) AS                                                
--MW DZ-400 SELECT  ENRFIC1 , ENRFIC2                                                       
--MW DZ-400     FROM  M907.RTRC20                                                           
--MW DZ-400   WHERE NOMFIC = 'CBCOSYS'                                                      
--MW DZ-400     AND DATEFIC = '19950811'                                                    
--MW DZ-400     AND NSOCIETE = '944'                                                        
--MW DZ-400 ;                                                                               
CREATE  VIEW M907.RVCBTAB                                                       
        ( CTABLEG2 , FONCTION , CPRINC , CSECOND , WTABLEG , WFLAG ,            
        CTYPE , CTABLE ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 ,         
        10 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CBTAB'                                                      
;                                                                               
CREATE  VIEW M907.RVCCAUT                                                       
        ( CTABLEG2 , CACID , CPAICPT , WTABLEG , WACTIF , WTRTECAR ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCAUT'                                                      
;                                                                               
CREATE  VIEW M907.RVCCCO2                                                       
        ( CTABLEG2 , CPAICPT , WTABLEG , WACTIF , LPAICPT , COMPTE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 8 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCCO2'                                                      
;                                                                               
CREATE  VIEW M907.RVCCCOD                                                       
        ( CTABLEG2 , CPAICPT , WTABLEG , WACTIF , LPAICPT , COMPTE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 8 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCCOD'                                                      
;                                                                               
CREATE  VIEW M907.RVCCDEL                                                       
        ( CTABLEG2 , NSOC , NLIEU , JOUR , WTABLEG , WACTIF , DELAI ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCDEL'                                                      
;                                                                               
CREATE  VIEW M907.RVCCLET                                                       
        ( CTABLEG2 , NOM , WTABLEG , LETTRAGE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCLET'                                                      
;                                                                               
CREATE  VIEW M907.RVCCPA2                                                       
        ( CTABLEG2 , TYPTRANS , MOPAI , WTABLEG , WACTIF , CPAICPT ,            
        FDCN ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCPA2'                                                      
;                                                                               
CREATE  VIEW M907.RVCCPAI                                                       
        ( CTABLEG2 , TYPTRANS , MOPAI , WTABLEG , WACTIF , CPAICPT ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCPAI'                                                      
;                                                                               
CREATE  VIEW M907.RVCCSTA                                                       
        ( CTABLEG2 , CSTATUT , TYPE , WTABLEG , LSTATUT ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCSTA'                                                      
;                                                                               
CREATE  VIEW M907.RVCEBDF                                                       
        ( CTABLEG2 , NSOC , WTABLEG , CPAYS ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CEBDF'                                                      
;                                                                               
CREATE  VIEW M907.RVCEDFO                                                       
        ( CTABLEG2 , CFORMAT , TOBJET , COBJET , WTABLEG , WACTIF , NSEQ        
        , WDATA ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR ( WTABLEG , 5 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CEDFO'                                                      
;                                                                               
CREATE  VIEW M907.RVCEDOL                                                       
        ( CTABLEG2 , COPCO400 , CLANG400 , WTABLEG , COPCOCAT , CLANGCAT        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 10 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CEDOL'                                                      
;                                                                               
CREATE  VIEW M907.RVCEFOR                                                       
        ( CTABLEG2 , CFORMAT , WTABLEG , WACTIF , LIBELLE , WDATA ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CEFOR'                                                      
;                                                                               
CREATE  VIEW M907.RVCEOBJ                                                       
        ( CTABLEG2 , TOBJET , COBJET , WTABLEG , LIBELLE , CBORNE ,             
        DEFAUT , WDATA ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 3 ) , SUBSTR ( WTABLEG , 24 , 3 ) , SUBSTR ( WTABLEG , 27 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CEOBJ'                                                      
;                                                                               
CREATE  VIEW M907.RVCEPRX                                                       
        ( CTABLEG2 , WPRIX , WTABLEG , WACTIF , LIBELLE , WDATA ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CEPRX'                                                      
;                                                                               
CREATE  VIEW M907.RVCGMGD                                                       
        ( CTABLEG2 , CGARMGD , WTABLEG , LGARMGD , CTYPGAR , NBMOIS ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR (             
        WTABLEG , 26 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CGMGD'                                                      
;                                                                               
CREATE  VIEW M907.RVCHGZP                                                       
        ( CTABLEG2 , NSOCIETE , NLIEU , ANCZP , NOUVZP , WTABLEG ,              
        DATEMAJ , COMMENT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 2 ) , SUBSTR ( CTABLEG2 , 9 , 2 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 ,         
        40 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CHGZP'                                                      
;                                                                               
CREATE  VIEW M907.RVCILOC                                                       
        ( CTABLEG2 , CINSEE , WTABLEG , FLAG , COMMUNE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 32 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CILOC'                                                      
;                                                                               
CREATE  VIEW M907.RVCILOM                                                       
        ( CTABLEG2 , SOC , MAG , CINSEE , WTABLEG , W , MQ ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CILOM'                                                      
;                                                                               
CREATE  VIEW M907.RVCLANG                                                       
        ( CTABLEG2 , CLANG , WTABLEG , LLANG ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CLANG'                                                      
;                                                                               
CREATE  VIEW M907.RVCLIEU                                                       
        ( CTABLEG2 , CLIEU , WTABLEG , LLIEU ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CLIEU'                                                      
;                                                                               
CREATE  VIEW M907.RVCLOGI                                                       
        ( CTABLEG2 , SOCLIEU , NSEQ , WTABLEG , CLOGI ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CLOGI'                                                      
;                                                                               
CREATE  VIEW M907.RVCMESU                                                       
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE , QUANTITE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 3 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CMESU'                                                      
;                                                                               
CREATE  VIEW M907.RVCODLM                                                       
        ( CTABLEG2 , NSOCDEP , NCODIC , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CODLM'                                                      
;                                                                               
CREATE  VIEW M907.RVCOLGR                                                       
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'COLGR'                                                      
;                                                                               
CREATE  VIEW M907.RVCONSI                                                       
        ( CTABLEG2 , CLOGIBAG , WTABLEG , CAS400 , CCHOIX , WFLAGS ,            
        DCOMENV , LOGIENV ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 15 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 10 ) , SUBSTR (            
        WTABLEG , 26 , 10 ) , SUBSTR ( WTABLEG , 36 , 1 ) , SUBSTR (            
        WTABLEG , 37 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CONSI'                                                      
;                                                                               
CREATE  VIEW M907.RVCPA2I                                                       
        ( CTABLEG2 , CPNASC , DEFFET , WTABLEG , CPVENTE , NVERSION ,           
        DUREEL , DUREEN ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 1 ) , SUBSTR ( WTABLEG , 7 , 40 ) , SUBSTR ( WTABLEG , 47 ,         
        2 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CPA2I'                                                      
;                                                                               
CREATE  VIEW M907.RVCPAYS                                                       
        ( CTABLEG2 , CPAYS , WTABLEG , LPAYS , LPAYS2 , CODE ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 ) , SUBSTR (            
        WTABLEG , 41 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CPAYS'                                                      
;                                                                               
CREATE  VIEW M907.RVCPFIL                                                       
        ( CTABLEG2 , CPOSTAL , WTABLEG , LFIL , NSOC ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CPFIL'                                                      
;                                                                               
CREATE  VIEW M907.RVCPREP                                                       
        ( CTABLEG2 , NSOC , NLIEU , CCASE , WTABLEG , LCASE ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 9 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CPREP'                                                      
;                                                                               
CREATE  VIEW M907.RVCRCTR                                                       
        ( CTABLEG2 , CCTRL , WTABLEG , WACTIF , LCTRL , CFORMAT , QLONG         
        , QLONGDEC ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 ) , SUBSTR (              
        WTABLEG , 52 , 2 ) , SUBSTR ( WTABLEG , 54 , 2 ) , SUBSTR (             
        WTABLEG , 56 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRCTR'                                                      
;                                                                               
CREATE  VIEW M907.RVCRDDS                                                       
        ( CTABLEG2 , CDOSSIER , NSEQ , WTABLEG , WACTIF , CCTRL , CLEINT        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 ) , SUBSTR ( WTABLEG , 7 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRDDS'                                                      
;                                                                               
CREATE  VIEW M907.RVCRDOS                                                       
        ( CTABLEG2 , CDOSSIER , WTABLEG , WACTIF , LDOSSIER , CMARQ ,           
        CTYPE , CAPPLI ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 5 ) , SUBSTR ( WTABLEG , 27 , 5 ) , SUBSTR (             
        WTABLEG , 32 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRDOS'                                                      
;                                                                               
CREATE  VIEW M907.RVCREAS                                                       
        ( CTABLEG2 , CREASON , WTABLEG , LREASON , WACTIF , WNSEQ ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 57 ) , SUBSTR ( WTABLEG , 58 , 1 ) , SUBSTR (             
        WTABLEG , 59 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CREAS'                                                      
;                                                                               
CREATE  VIEW M907.RVCREVT                                                       
        ( CTABLEG2 , ORIGINE , WTABLEG , PRESTATI , TYPVTE , TYPE ,             
        SOCLIEU , COMMENT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 ) , SUBSTR (               
        WTABLEG , 7 , 3 ) , SUBSTR ( WTABLEG , 10 , 6 ) , SUBSTR (              
        WTABLEG , 16 , 40 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CREVT'                                                      
;                                                                               
CREATE  VIEW M907.RVCRFIL                                                       
        ( CTABLEG2 , CNSOC , NLIEU , WTABLEG , SAVREF , PRFDOS ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 3 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRFIL'                                                      
;                                                                               
CREATE  VIEW M907.RVCRFUN                                                       
        ( CTABLEG2 , CCTRL , NSEQ , WTABLEG , CFUNC , PDEBUT , PLONG )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 2 ) , SUBSTR ( WTABLEG , 8 , 2 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRFUN'                                                      
;                                                                               
CREATE  VIEW M907.RVCRINO                                                       
        ( CTABLEG2 , CRCTR , WTABLEG , WBYPASS , WCONTEXT , WPRINT ,            
        VALID , FLAGS ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR (               
        WTABLEG , 5 , 50 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRINO'                                                      
;                                                                               
CREATE  VIEW M907.RVCRJUS                                                       
        ( CTABLEG2 , CJUSTIF , WTABLEG , LJUSTIF ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRJUS'                                                      
;                                                                               
CREATE  VIEW M907.RVCRSOF                                                       
        ( CTABLEG2 , CBAREME , WTABLEG , PSEUIL , WNMENS , PAGIOS ,             
        PTAEG ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 12 ) , SUBSTR ( WTABLEG , 13 , 3 ) , SUBSTR (             
        WTABLEG , 16 , 6 ) , SUBSTR ( WTABLEG , 22 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRSOF'                                                      
;                                                                               
CREATE  VIEW M907.RVCRVAL                                                       
        ( CTABLEG2 , CCTRL , NSEQ , WTABLEG , TYVAL , VAL1 , VAL2 ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 25 ) , SUBSTR ( WTABLEG , 28 , 25 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRVAL'                                                      
;                                                                               
CREATE  VIEW M907.RVCSELT                                                       
        ( CTABLEG2 , CSELA , WTABLEG , WFLAG , CORIG , LCOMMENT ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CSELT'                                                      
;                                                                               
CREATE  VIEW M907.RVCSELV                                                       
        ( CTABLEG2 , CSELART , WTABLEG , WFLAG , COMMENT ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CSELV'                                                      
;                                                                               
CREATE  VIEW M907.RVCSTSP                                                       
        ( CTABLEG2 , CSELA , WTABLEG , ENVNOMAD , CTYPRES , COPSL ,             
        AUTORRM ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 5 ) , SUBSTR ( WTABLEG , 9 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CSTSP'                                                      
;                                                                               
CREATE  VIEW M907.RVCTAXE                                                       
        ( CTABLEG2 , CTAXE , WTABLEG , LTAXE , WFRN , NSEQ , WACTIF ,           
        TRANSCO ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTAXE'                                                      
;                                                                               
CREATE  VIEW M907.RVCTCTA                                                       
        ( CTABLEG2 , TYPCPT , SENS , CGSG , WTABLEG , CTRANSAC , CCOMPTA        
        , LTYPCPT ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        1 ) , SUBSTR ( CTABLEG2 , 3 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 4 ) , SUBSTR ( WTABLEG , 5 , 2 ) , SUBSTR ( WTABLEG , 7 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTCTA'                                                      
;                                                                               
CREATE  VIEW M907.RVCTPCE                                                       
        ( CTABLEG2 , NJRN , NATURE , WTABLEG , WACTIF , TYPPCSAP ,              
        LIBPCSAP , TYPTVA ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 20 ) , SUBSTR ( WTABLEG , 24 ,         
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTPCE'                                                      
;                                                                               
CREATE  VIEW M907.RVCTRAT                                                       
        ( CTABLEG2 , CCONTRAT , WTABLEG , LCONTRAT , WACTIF ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTRAT'                                                      
;                                                                               
CREATE  VIEW M907.RVCTRDS                                                       
        ( CTABLEG2 , SYSTEM , WTABLEG , CTRL ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 15 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 50 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTRDS'                                                      
;                                                                               
CREATE  VIEW M907.RVCTSAP                                                       
        ( CTABLEG2 , CENT , TYPC , WTABLEG , CSAP , LCODE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG        
        , 21 , 30 )                                                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTSAP'                                                      
;                                                                               
CREATE  VIEW M907.RVCTSE1                                                       
        ( CTABLEG2 , SECTION , DEFFET , WTABLEG , WTRANSCO , SECTIONT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 7 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTSE1'                                                      
;                                                                               
CREATE  VIEW M907.RVCTSEC                                                       
        ( CTABLEG2 , SECTION , WTABLEG , WTRANSCO , SECTIONT ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 7 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTSEC'                                                      
;                                                                               
CREATE  VIEW M907.RVCTSOC                                                       
        ( CTABLEG2 , NSOC , NLIEU , DATEFFET , WTABLEG , WACTIF , SOCCPT        
        , ETACPT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR ( WTABLEG , 5 , 3         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTSOC'                                                      
;                                                                               
CREATE  VIEW M907.RVCTTVA                                                       
        ( CTABLEG2 , NATTVA , TXTVA , WTABLEG , WACTIF , CTVASAP ,              
        TYPTVA , CPTSAP ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 4 ) , SUBSTR ( WTABLEG , 8 , 8         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTTVA'                                                      
;                                                                               
CREATE  VIEW M907.RVDACMT                                                       
        ( CTABLEG2 , CMOTIF , WTABLEG , LMOTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DACMT'                                                      
;                                                                               
CREATE  VIEW M907.RVDARBX                                                       
        ( CTABLEG2 , NENTCDE , CFAM , WTABLEG , LIBELLE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DARBX'                                                      
;                                                                               
CREATE  VIEW M907.RVDB001                                                       
        ( CTABLEG2 , COFFRE , CENREG , WTABLEG , CTYPE , LIBELLE , FLAG         
        , FLAG2 ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 ) , SUBSTR ( WTABLEG , 22 , 20 ) , SUBSTR ( WTABLEG , 42         
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DB001'                                                      
;                                                                               
CREATE  VIEW M907.RVDB002                                                       
        ( CTABLEG2 , CHGT , CENREG , WTABLEG , LIBELLE , ACTION , WFLAG         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 ) , SUBSTR ( WTABLEG , 22 , 20 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DB002'                                                      
;                                                                               
CREATE  VIEW M907.RVDB003                                                       
        ( CTABLEG2 , NSOC , NLIEU , CHANG , WTABLEG , COFFRE1 , COFFRE2         
        , LIBELLE ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 ,          
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DB003'                                                      
;                                                                               
CREATE  VIEW M907.RVDB004                                                       
        ( CTABLEG2 , NSOCLIEU , COFFRE , CENREG , WTABLEG , LIBELLE ,           
        WAJOUT , WSUP ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        4 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR ( WTABLEG , 22        
        , 1 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DB004'                                                      
;                                                                               
CREATE  VIEW M907.RVDB005                                                       
        ( CTABLEG2 , CFAM , NCODIC , WTABLEG , TYPENREG , RELANCE ,             
        TEQUIP , EXTRACT ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 5 ) , SUBSTR ( WTABLEG , 8 , 1         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DB005'                                                      
;                                                                               
CREATE  VIEW M907.RVDEFAU                                                       
        ( CTABLEG2 , COPCO , CCHAMP , WTABLEG , CODE , WFLAG ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG         
        , 21 , 1 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DEFAU'                                                      
;                                                                               
CREATE  VIEW M907.RVDELIV                                                       
        ( CTABLEG2 , ORIGDEST , CMODDEL , NSEQ , WTABLEG , WFLAG , JJMM         
        , DELAI ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 4 ) , SUBSTR ( WTABLEG , 6 ,         
        3 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DELIV'                                                      
;                                                                               
CREATE  VIEW M907.RVDEPCL                                                       
        ( CTABLEG2 , NSOCIETE , NDEPOT , WTABLEG , CCOLOR , CBASE ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DEPCL'                                                      
;                                                                               
CREATE  VIEW M907.RVDISTM                                                       
        ( CTABLEG2 , CDISTM , WTABLEG , LDISTM ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DISTM'                                                      
;                                                                               
CREATE  VIEW M907.RVDMIGR                                                       
        ( CTABLEG2 , CLASS , SCLASS , WTABLEG , CFAM , WFLAG , OPTION )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 1 ) , SUBSTR ( WTABLEG , 7 , 5 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DMIGR'                                                      
;                                                                               
CREATE  VIEW M907.RVDSECU                                                       
        ( CTABLEG2 , CPARAM , WTABLEG , NBJ , LPARAM ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DSECU'                                                      
;                                                                               
CREATE  VIEW M907.RVDTENV                                                       
        ( CTABLEG2 , RACPRF , WTABLEG , NSOCIETE , QMAS400 , QMHOST ,           
        BIBFIC , BIBTAB ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 8 ) , SUBSTR (               
        WTABLEG , 12 , 4 ) , SUBSTR ( WTABLEG , 16 , 10 ) , SUBSTR (            
        WTABLEG , 26 , 10 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DTENV'                                                      
;                                                                               
CREATE  VIEW M907.RVDWTEN                                                       
        ( CTABLEG2 , CTYPENS , WTABLEG , WACTIF , LTYPENS ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DWTEN'                                                      
;                                                                               
CREATE  VIEW M907.RVEC001                                                       
        ( CTABLEG2 , IDSI , WTABLEG , IDXML , IDROOT , DTDNAME , WTRT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 16 ) , SUBSTR ( WTABLEG , 17 , 17 ) , SUBSTR (            
        WTABLEG , 34 , 20 ) , SUBSTR ( WTABLEG , 54 , 1 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC001'                                                      
;                                                                               
CREATE  VIEW M907.RVEC002                                                       
        ( CTABLEG2 , IDSI , NSEQ , WTABLEG , CMASQ , IDATTR ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 7 ) , SUBSTR ( WTABLEG         
        , 8 , 20 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC002'                                                      
;                                                                               
CREATE  VIEW M907.RVEC003                                                       
        ( CTABLEG2 , IDSI , WTABLEG , CREATION , MODIF , SUPPRESS ,             
        DIVERS ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 50 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC003'                                                      
;                                                                               
CREATE  VIEW M907.RVEC004                                                       
        ( CTABLEG2 , CAGR , WTABLEG , CFAMCOM , LFAMCOM ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 33 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC004'                                                      
;                                                                               
CREATE  VIEW M907.RVEC005                                                       
        ( CTABLEG2 , NSOC , CZONLIV , WTABLEG , WMALDES ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC005'                                                      
;                                                                               
CREATE  VIEW M907.RVEC006                                                       
        ( CTABLEG2 , NSOC , NLIEU , CZONLIV , CPLAGE , NJOUR , WTABLEG ,        
        QQUOTA ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , SUBSTR ( CTABLEG2 , 12 , 2 )        
        , SUBSTR ( CTABLEG2 , 14 , 1 ) , WTABLEG , SUBSTR ( WTABLEG , 1         
        , 3 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC006'                                                      
;                                                                               
CREATE  VIEW M907.RVEC007                                                       
        ( CTABLEG2 , NSOCPLT , NLIEUPLT , WTABLEG , NSOCMAG , NLIEUMAG )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 3 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC007'                                                      
;                                                                               
CREATE  VIEW M907.RVEC008                                                       
        ( CTABLEG2 , MOPAIDC , WTABLEG , MOPAISI , WENLIGNE ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC008'                                                      
;                                                                               
CREATE  VIEW M907.RVEC009                                                       
        ( CTABLEG2 , ECOM , WTABLEG , NCG ) AS                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 15 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC009'                                                      
;                                                                               
CREATE  VIEW M907.RVEC010                                                       
        ( CTABLEG2 , NSOCENTR , NDEPOT , NSOCIETE , NLIEU , WTABLEG ,           
        CSELART ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , SUBSTR ( CTABLEG2 , 10 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC010'                                                      
;                                                                               
CREATE  VIEW M907.RVEC100                                                       
        ( CTABLEG2 , CFAM , CARACT , WTABLEG , LIB ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 50 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EC100'                                                      
;                                                                               
CREATE  VIEW M907.RVEDTST                                                       
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , PARAM ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EDTST'                                                      
;                                                                               
CREATE  VIEW M907.RVEECOM                                                       
        ( CTABLEG2 , CETAT , WTABLEG , LETAT ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 25 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EECOM'                                                      
;                                                                               
CREATE  VIEW M907.RVEFEXL                                                       
        ( CTABLEG2 , CTIERS , WTABLEG , WFLAG , COMMENT ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFEXL'                                                      
;                                                                               
CREATE  VIEW M907.RVEFRDO                                                       
        ( CTABLEG2 , CRENDU , WTABLEG , APPLI , CTRAIT ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 5 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFRDO'                                                      
;                                                                               
CREATE  VIEW M907.RVEG0000                                                      
        AS                                                                      
SELECT  NOMETAT , RUPTCHTPAGE , RUPTRAZPAGE , LARGEUR , LONGUEUR ,              
        DSECTCREE , TRANSFERE , DSYST , LIBETAT                                 
    FROM  M907.RTEG00                                                           
;                                                                               
CREATE  VIEW M907.RVEG0500                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , SKIPBEFORE ,                 
        SKIPAFTER , RUPTIMPR , NOMCHAMPC , NOMCHAMPS , SOUSTABLE , DSYST        
    FROM  M907.RTEG05                                                           
;                                                                               
CREATE  VIEW M907.RVEG1000                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , NOMCHAMP ,        
        RUPTIMPR , CHCOND1 , CONDITION , CHCOND2 , DSYST , MASKCHAMP            
    FROM  M907.RTEG10                                                           
;                                                                               
CREATE  VIEW M907.RVEG1100                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , RUPTIMPR ,        
        SOUSTABLE , NOMCHAMPS , NOMCHAMPC , CHCOND1 , CONDITION ,               
        CHCOND2 , DSYST , LIBCHAMP                                              
    FROM  M907.RTEG11                                                           
;                                                                               
CREATE  VIEW M907.RVEG1500                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , NOMCHAMPT         
        , RUPTRAZ , DSYST                                                       
    FROM  M907.RTEG15                                                           
;                                                                               
CREATE  VIEW M907.RVEG2000                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , POSCHAMP , DSYST ,           
        LIBCHAMP                                                                
    FROM  M907.RTEG20                                                           
;                                                                               
CREATE  VIEW M907.RVEG2500                                                      
        AS                                                                      
SELECT  NOMETAT , NOMCHAMP , LIGNECALC , CHCALC1 , TYPCALCUL , CHCALC2 ,        
        DSYST                                                                   
    FROM  M907.RTEG25                                                           
;                                                                               
CREATE  VIEW M907.RVEG3000                                                      
        AS                                                                      
SELECT  NOMETAT , NOMCHAMP , TYPCHAMP , LGCHAMP , DECCHAMP , POSDSECT ,         
        OCCURENCES , DSYST                                                      
    FROM  M907.RTEG30                                                           
;                                                                               
CREATE  VIEW M907.RVEG4000                                                      
        AS                                                                      
SELECT  NOMETAT , TYPLIGNE , NOLIGNE , CONTINUER , NOMCHAMP , DSYST             
    FROM  M907.RTEG40                                                           
;                                                                               
CREATE  VIEW M907.RVEG9000                                                      
        AS                                                                      
SELECT  IDENTTS , NOMETAT , CHAMPTRI , RANGTS                                   
    FROM  M907.RTEG90                                                           
;                                                                               
CREATE  VIEW M907.RVEGPRT                                                       
        ( CTABLEG2 , CETAT , CTERM , WTABLEG , CIMPRIM , LIBELLE ,              
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG        
        , 11 , 30 ) , SUBSTR ( WTABLEG , 41 , 1 )                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EGPRT'                                                      
;                                                                               
CREATE  VIEW M907.RVEQ360                                                       
        ( CTABLEG2 , CLEF , WTABLEG , DIVERS ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 60 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQ360'                                                      
;                                                                               
CREATE  VIEW M907.RVEQPRE                                                       
        ( CTABLEG2 , PREST , WTABLEG , AREALISE , PL , CLIENT , DARTY ,         
        DUREE ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQPRE'                                                      
;                                                                               
CREATE  VIEW M907.RVEQUEX                                                       
        ( CTABLEG2 , NSOCLIEU , ENTITE , WTABLEG , TYPEQU ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQUEX'                                                      
;                                                                               
CREATE  VIEW M907.RVEQUFI                                                       
        ( CTABLEG2 , NSOCLIEU , WTABLEG , POIDS1 , POIDS2 , DIM , HTEUR         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 4 ) , SUBSTR (               
        WTABLEG , 9 , 4 ) , SUBSTR ( WTABLEG , 13 , 4 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQUFI'                                                      
;                                                                               
CREATE  VIEW M907.RVEQUMK                                                       
        ( CTABLEG2 , NSOCLIEU , CFAM , NSEQ , WTABLEG , CMARK , CVMARK ,        
        TYPEQU ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , SUBSTR ( CTABLEG2 , 12 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 ,        
        3 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQUMK'                                                      
;                                                                               
CREATE  VIEW M907.RVEQUPD                                                       
        ( CTABLEG2 , NSOCLIEU , WTABLEG , POIDS0 , TYPEQU ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQUPD'                                                      
;                                                                               
CREATE  VIEW M907.RVEQUPF                                                       
        ( CTABLEG2 , NSOCLIEU , WTABLEG , ELIGIB , REPRISE ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQUPF'                                                      
;                                                                               
CREATE  VIEW M907.RVEQUTY                                                       
        ( CTABLEG2 , CEQUI , WTABLEG , LEQUI , TLIVR ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EQUTY'                                                      
;                                                                               
CREATE  VIEW M907.RVFAMLM                                                       
        ( CTABLEG2 , CFAM , NSOCDEP , CREINIT , WTABLEG , WACTIF ,              
        DREINIT , CCTRL ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , SUBSTR ( CTABLEG2 , 12 , 1 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR ( WTABLEG , 10 ,        
        5 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FAMLM'                                                      
;                                                                               
CREATE  VIEW M907.RVFCCLI                                                       
        ( CTABLEG2 , NCLIB2B , WTABLEG , WACTIF , CFAC , PARAMF ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 7 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FCCLI'                                                      
;                                                                               
CREATE  VIEW M907.RVFERIE                                                       
        ( CTABLEG2 , CODE , NSEQ , DATEMMJJ , WTABLEG , TOP , LIB ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , SUBSTR ( CTABLEG2 , 8 , 4 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FERIE'                                                      
;                                                                               
CREATE  VIEW M907.RVFG0100                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSOCORIG ,                 
        NLIEUORIG , NSOCDEST , NLIEUDEST , CTYPFACT , NATFACT , CVENT ,         
        NUMORIG , DMOUV , MONTHT , CTAUXTVA , TXESCPT , CFRAIS , MPCF ,         
        MFRMG , QNBPIECES , DCREAT , DPIECE , DCOMPTA , DMVT , DANNUL ,         
        DECHEANC , DREGL , DSYST                                                
    FROM  M907.RTFG01                                                           
;                                                                               
CREATE  VIEW M907.RVFG0101                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSOCORIG ,                 
        NLIEUORIG , NSOCDEST , NLIEUDEST , CTYPFACT , NATFACT , CVENT ,         
        NUMORIG , DMOUV , MONTHT , CTAUXTVA , TXESCPT , CFRAIS , MPCF ,         
        MFRMG , QNBPIECES , DCREAT , DPIECE , DCOMPTA , DMVT , DANNUL ,         
        DECHEANC , DREGL , DSYST , SERVORIG , SERVDEST                          
    FROM  M907.RTFG01                                                           
;                                                                               
CREATE  VIEW M907.RVFG0102                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSOCORIG ,                 
        NLIEUORIG , NSOCDEST , NLIEUDEST , CTYPFACT , NATFACT , CVENT ,         
        NUMORIG , DMOUV , MONTHT , CTAUXTVA , TXESCPT , CFRAIS , MPCF ,         
        MFRMG , QNBPIECES , DCREAT , DPIECE , DCOMPTA , DMVT , DANNUL ,         
        DECHEANC , DREGL , DSYST , SERVORIG , SERVDEST , DVALEUR , CACID        
        , DEXCPT                                                                
    FROM  M907.RTFG01                                                           
;                                                                               
CREATE  VIEW M907.RVFG0103                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSOCORIG ,                 
        NLIEUORIG , NSOCDEST , NLIEUDEST , CTYPFACT , NATFACT , CVENT ,         
        NUMORIG , DMOUV , MONTHT , CTAUXTVA , TXESCPT , CFRAIS , MPCF ,         
        MFRMG , QNBPIECES , DCREAT , DPIECE , DCOMPTA , DMVT , DANNUL ,         
        DECHEANC , DREGL , DSYST , SERVORIG , SERVDEST , DVALEUR , CACID        
        , DEXCPT , MPRMP                                                        
    FROM  M907.RTFG01                                                           
;                                                                               
CREATE  VIEW M907.RVFG0104                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSOCORIG ,                 
        NLIEUORIG , NSOCDEST , NLIEUDEST , CTYPFACT , NATFACT , CVENT ,         
        NUMORIG , DMOUV , MONTHT , CTAUXTVA , TXESCPT , CFRAIS , MPCF ,         
        MFRMG , QNBPIECES , DCREAT , DPIECE , DCOMPTA , DMVT , DANNUL ,         
        DECHEANC , DREGL , DSYST , SERVORIG , SERVDEST , DVALEUR , CACID        
        , DEXCPT , MPRMP , SECORIG , SECDEST                                    
    FROM  M907.RTFG01                                                           
;                                                                               
CREATE  VIEW M907.RVFG0106                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSOCORIG ,                 
        NLIEUORIG , NSOCDEST , NLIEUDEST , CTYPFACT , NATFACT , CVENT ,         
        NUMORIG , DMOUV , MONTHT , CTAUXTVA , TXESCPT , CFRAIS , MPCF ,         
        MFRMG , QNBPIECES , DCREAT , DPIECE , DCOMPTA , DMVT , DANNUL ,         
        DECHEANC , DREGL , DSYST , SERVORIG , SERVDEST , DVALEUR , CACID        
        , DEXCPT , MPRMP , SECORIG , SECDEST , NTIERSE , NLETTRE ,              
        NTIERSR , NLETTRR , TYPECLI                                             
    FROM  M907.RTFG01                                                           
;                                                                               
CREATE  VIEW M907.RVFG0107                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSOCORIG ,                 
        NLIEUORIG , NSOCDEST , NLIEUDEST , CTYPFACT , NATFACT , CVENT ,         
        NUMORIG , DMOUV , MONTHT , CTAUXTVA , TXESCPT , CFRAIS , MPCF ,         
        MFRMG , QNBPIECES , DCREAT , DPIECE , DCOMPTA , DMVT , DANNUL ,         
        DECHEANC , DREGL , DSYST , SERVORIG , SERVDEST , DVALEUR , CACID        
        , DEXCPT , MPRMP , SECORIG , SECDEST , NTIERSE , NLETTRE ,              
        NTIERSR , NLETTRR , TYPECLI , MFRTP                                     
    FROM  M907.RTFG01                                                           
;                                                                               
CREATE  VIEW M907.RVFG0200                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , NJRN , NPIECE , NLIGNE , NEXERCICE , NPERIODE ,         
        NOECS , DCREAT , COMPTE , SSCOMPTE , SECTION , RUBRIQUE ,               
        TCOMPTE , LIBELLE , MONTANT , SENS , DCOMPTA , NETAB , NATFACT ,        
        CVENT , NUMFACT , CTYPPIECE , CTYPLIGNE , NLIGNEFACT , TSOCFACT         
        , NSOC , NLIEU , NSOCCOR , DATEFIC , WRECUP , DSYST                     
    FROM  M907.RTFG02                                                           
;                                                                               
CREATE  VIEW M907.RVFG0201                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , NJRN , NPIECE , NLIGNE , NEXERCICE , NPERIODE ,         
        NOECS , DCREAT , COMPTE , SSCOMPTE , SECTION , RUBRIQUE ,               
        TCOMPTE , LIBELLE , MONTANT , SENS , DCOMPTA , NETAB , NATFACT ,        
        CVENT , NUMFACT , CTYPPIECE , CTYPLIGNE , NLIGNEFACT , TSOCFACT         
        , NSOC , NLIEU , NSOCCOR , DATEFIC , WRECUP , DSYST , NTIERS            
    FROM  M907.RTFG02                                                           
;                                                                               
CREATE  VIEW M907.RVFG0202                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , NJRN , NPIECE , NLIGNE , NEXERCICE , NPERIODE ,         
        NOECS , DCREAT , COMPTE , SSCOMPTE , SECTION , RUBRIQUE ,               
        TCOMPTE , LIBELLE , MONTANT , SENS , DCOMPTA , NETAB , NATFACT ,        
        CVENT , NUMFACT , CTYPPIECE , CTYPLIGNE , NLIGNEFACT , TSOCFACT         
        , NSOC , NLIEU , NSOCCOR , DATEFIC , WRECUP , DSYST , NTIERS ,          
        DVALEUR , CCHRONO , DRECUPCC , TYPECPTE                                 
    FROM  M907.RTFG02                                                           
;                                                                               
CREATE  VIEW M907.RVFG0204                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , NJRN , NPIECE , NLIGNE , NEXERCICE , NPERIODE ,         
        NOECS , DCREAT , COMPTE , SSCOMPTE , SECTION , RUBRIQUE ,               
        TCOMPTE , LIBELLE , MONTANT , SENS , DCOMPTA , NETAB , NATFACT ,        
        CVENT , NUMFACT , CTYPPIECE , CTYPLIGNE , NLIGNEFACT , TSOCFACT         
        , NSOC , NLIEU , NSOCCOR , DATEFIC , WRECUP , DSYST , NTIERS ,          
        DVALEUR , CCHRONO , DRECUPCC , TYPECPTE , TIERSGCT , NLETTRAGE ,        
        TAUXTVA , TYPMT                                                         
    FROM  M907.RTFG02                                                           
;                                                                               
CREATE  VIEW M907.RVFG0300                                                      
        AS                                                                      
SELECT  ALL NSOCCRE , NSOCORIG , NLIEUORIG , NSOCDEST , NLIEUDEST ,             
        CTYPFACT , NATFACT , CVENT , DEFFET , MONTHT , CTAUXTVA ,               
        TXESCPT , DFIN , FREQ , DSYST                                           
    FROM  M907.RTFG03                                                           
;                                                                               
CREATE  VIEW M907.RVFG0301                                                      
        AS                                                                      
SELECT  ALL NSOCCRE , NSOCORIG , NLIEUORIG , NSOCDEST , NLIEUDEST ,             
        CTYPFACT , NATFACT , CVENT , DEFFET , MONTHT , CTAUXTVA ,               
        TXESCPT , DFIN , FREQ , DSYST , SECORIG , SECDEST                       
    FROM  M907.RTFG03                                                           
;                                                                               
CREATE  VIEW M907.RVFG0500                                                      
        AS                                                                      
SELECT  ALL NSOC , DSOLDE , MTSOLDE , DSYST                                     
    FROM  M907.RTFG05                                                           
;                                                                               
CREATE  VIEW M907.RVFG0600                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , NSEQ , DANNUL ,            
        DSYST , LCOMMENT                                                        
    FROM  M907.RTFG06                                                           
;                                                                               
CREATE  VIEW M907.RVFG0700                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CCHRONO , NUMFACT , NLIGNE , QTEFACT , PUFACT ,         
        DSYST , LCOMMENT                                                        
    FROM  M907.RTFG07                                                           
;                                                                               
CREATE  VIEW M907.RVFG1000                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , CFRAIS , NATFACT , TAUX , DSYST                         
    FROM  M907.RTFG10                                                           
;                                                                               
CREATE  VIEW M907.RVFG1100                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , EXERCICE , PERIODE , EXERSOC , PERSOC , DDMAJ ,         
        DSYST                                                                   
    FROM  M907.RTFG11                                                           
;                                                                               
CREATE  VIEW M907.RVFG1200                                                      
        AS                                                                      
SELECT  ALL NSOCCOMPT , NJRNG , NJRN , DSYST                                    
    FROM  M907.RTFG12                                                           
;                                                                               
CREATE  VIEW M907.RVFG1300                                                      
        AS                                                                      
SELECT  NSOCCOMPT , NSOC , NLIEU , COMPTECG , NATFACT , CVENT , NSOCCOR         
        , COMPTESOC , NAUXSOC , DATEFF , DSYST                                  
    FROM  M907.RTFG13                                                           
;                                                                               
CREATE  VIEW M907.RVFG1600                                                      
        AS                                                                      
SELECT  NSOC , NLIEU , SECORIG , SECTION , DSYST                                
    FROM  M907.RTFG16                                                           
;                                                                               
CREATE  VIEW M907.RVFIFAC                                                       
        ( CTABLEG2 , NCODIC , WTABLEG , WACTIF , CVENT ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FIFAC'                                                      
;                                                                               
CREATE  VIEW M907.RVFM9300                                                      
        AS                                                                      
SELECT  CRITLIEU1 , CRITLIEU2 , LIBLIEU , LIEUPCA , DMAJ , DSYST                
    FROM  M907.RTFM93                                                           
;                                                                               
--MW oop CREATE  VIEW M907.RVFP0100                                                      
--MW oop         AS                                                                      
--MW oop SELECT  ALL NSOCCOMPT , NSOC , NLIEU , CASSUR , NFACTURE , DFACTURE ,           
--MW oop         NSINISTRE , DSINISTRE , NOM , PRENOM , CPOSTAL , REFDOC ,               
--MW oop         CTYPINTER , CLIEUINTER , MTFRANCHISE , WFRANCHISE , TXREMISE ,          
--MW oop         MTLIVR , MTFORFREP , NDOSSAV , DANNUL , DSELECT , NPRESENT ,            
--MW oop         NLOT , DVALID , WREJET , DREJET , CREJET , LREJET , DCREAT ,            
--MW oop         DNETTING , ORIGINE , CDEVISE , DMAJ , DSYST                             
--MW oop     FROM  M907.RTFP01                                                           
--MW oop ;                                                                               
--MW oop CREATE  VIEW M907.RVFP0101                                                      
--MW oop         AS                                                                      
--MW oop SELECT  NSOCCOMPT , NSOC , NLIEU , CASSUR , NFACTURE , DFACTURE ,               
--MW oop         NSINISTRE , DSINISTRE , NOM , PRENOM , CPOSTAL , REFDOC ,               
--MW oop         CTYPINTER , CLIEUINTER , MTFRANCHISE , WFRANCHISE , TXREMISE ,          
--MW oop         MTLIVR , MTFORFREP , NDOSSAV , DANNUL , DSELECT , NPRESENT ,            
--MW oop         NLOT , DVALID , WREJET , DREJET , CREJET , LREJET , DCREAT ,            
--MW oop         DNETTING , ORIGINE , CDEVISE , DMAJ , DSYST , MTFORFEXP                 
--MW oop     FROM  M907.RTFP01                                                           
--MW oop ;                                                                               
--MW oop CREATE  VIEW M907.RVFP0102                                                      
--MW oop         AS                                                                      
--MW oop SELECT  NSOCCOMPT , NSOC , NLIEU , CASSUR , NFACTURE , DFACTURE ,               
--MW oop         NSINISTRE , DSINISTRE , NOM , PRENOM , CPOSTAL , REFDOC ,               
--MW oop         CTYPINTER , CLIEUINTER , MTFRANCHISE , WFRANCHISE , TXREMISE ,          
--MW oop         MTLIVR , MTFORFREP , NDOSSAV , DANNUL , DSELECT , NPRESENT ,            
--MW oop         NLOT , DVALID , WREJET , DREJET , CREJET , LREJET , DCREAT ,            
--MW oop         DNETTING , ORIGINE , CDEVISE , DMAJ , DSYST , MTFORFEXP ,               
--MW oop         WDERFACT                                                                
--MW oop     FROM  M907.RTFP01                                                           
--MW oop ;                                                                               
--MW oop CREATE  VIEW M907.RVFP0103                                                      
--MW oop         AS                                                                      
--MW oop SELECT  NSOCCOMPT , NSOC , NLIEU , CASSUR , NFACTURE , DFACTURE ,               
--MW oop         NSINISTRE , DSINISTRE , NOM , PRENOM , CPOSTAL , REFDOC ,               
--MW oop         CTYPINTER , CLIEUINTER , MTFRANCHISE , WFRANCHISE , TXREMISE ,          
--MW oop         MTLIVR , MTFORFREP , NDOSSAV , DANNUL , DSELECT , NPRESENT ,            
--MW oop         NLOT , DVALID , WREJET , DREJET , CREJET , LREJET , DCREAT ,            
--MW oop         DNETTING , ORIGINE , CDEVISE , DMAJ , DSYST , MTFORFEXP ,               
--MW oop         WDERFACT , WCARTE                                                       
--MW oop     FROM  M907.RTFP01                                                           
--MW oop ;                                                                               
--MW oop CREATE  VIEW M907.RVFP0200                                                      
--MW oop         AS                                                                      
--MW oop SELECT  ALL NSOCCOMPT , CASSUR , NFACTURE , NLIGNE , NATURE , MARQUE ,          
--MW oop         NCODIC , MTHT , CTAUXTVA , MTTTC , MTCLIENT , MTREMISE , MTPSE ,        
--MW oop         CDIAGNO , NCRI , DANNUL , DSYST                                         
--MW oop     FROM  M907.RTFP02                                                           
--MW oop ;                                                                               
--MW oop CREATE  VIEW M907.RVFP0300                                                      
--MW oop         AS                                                                      
--MW oop SELECT  NSINISTRE , CTYPFAC2 , DSYST                                            
--MW oop     FROM  M907.RTFP03                                                           
--MW oop ;                                                                               
CREATE  VIEW M907.RVFPFOR                                                       
        ( CTABLEG2 , CASSUR , CNATURE , DEFFET , WTABLEG , WACTIF ,             
        MTEXPERT , MFORFAIT ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR ( WTABLEG , 8 , 6         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPFOR'                                                      
;                                                                               
CREATE  VIEW M907.RVFPTIE                                                       
        ( CTABLEG2 , CUGS , WTABLEG , WACTIF , NTIERS ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPTIE'                                                      
;                                                                               
CREATE  VIEW M907.RVFPTYP                                                       
        ( CTABLEG2 , CTYPOC , WTABLEG , VALO , CTYPOL ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVFRCDD                                                       
        ( CTABLEG2 , CFAM , WTABLEG , CDESC1 , CDESC2 , COMMENT ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FRCDD'                                                      
;                                                                               
CREATE  VIEW M907.RVFRCPT                                                       
        ( CTABLEG2 , GV , CODE , WTABLEG , WFLAG , COMMENT ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 15 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FRCPT'                                                      
;                                                                               
CREATE  VIEW M907.RVFRTCO                                                       
        ( CTABLEG2 , NSOCIETE , CORIG , WTABLEG , CDEST ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FRTCO'                                                      
;                                                                               
CREATE  VIEW M907.RVFRVET                                                       
        ( CTABLEG2 , IDVETUST , CFAM , WTABLEG , PARAM , COMMENT ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG        
        , 11 , 10 )                                                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FRVET'                                                      
;                                                                               
CREATE  VIEW M907.RVFTCEG                                                       
        ( CTABLEG2 , CEGA , WTABLEG , LIBCEGA ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTCEG'                                                      
;                                                                               
CREATE  VIEW M907.RVFTCEN                                                       
        ( CTABLEG2 , NSOC , WTABLEG , WACTIF ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTCEN'                                                      
;                                                                               
CREATE  VIEW M907.RVFTDIV                                                       
        ( CTABLEG2 , ELTCTL , TYPCTL , WTABLEG , WACTIF , LIBELLE ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , SUBSTR ( CTABLEG2 , 13        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 50 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTDIV'                                                      
;                                                                               
CREATE  VIEW M907.RVFTEFG                                                       
        ( CTABLEG2 , CODPARAM , WTABLEG , VALPARAM , LIBPARAM ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 30 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTEFG'                                                      
;                                                                               
CREATE  VIEW M907.RVFTPCA                                                       
        ( CTABLEG2 , CODLIEU , WTABLEG , LIBLIEU ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTPCA'                                                      
;                                                                               
CREATE  VIEW M907.RVFTSAP                                                       
        ( CTABLEG2 , CAT , CC1 , CC2 , WTABLEG , WFLAG , VALEUR ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        8 ) , SUBSTR ( CTABLEG2 , 12 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 49 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTSAP'                                                      
;                                                                               
CREATE  VIEW M907.RVFTTYP                                                       
        ( CTABLEG2 , CTYPE , WTABLEG , WACTIF , LTYPE ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTTYP'                                                      
;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVFX0100                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NCOMPTE , CTYPCOMPTE , CAGREG , PMONTCA , CTAUXTVA , NOECS ,        
--MW DZ-400         NCOMPTETVA , DSYST                                                      
--MW DZ-400     FROM  M907.RTFX01                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVFX4500                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  CODEAPPLI , NEXERCICE , NPERIODE , DCREAT , PMONTANT , WNORMAL ,        
--MW DZ-400         DSYST                                                                   
--MW DZ-400     FROM  M907.RTFX45                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW M907.RVFX9001                                                      
        AS                                                                      
SELECT  NSOC , NOECS , CRITERE1 , CRITERE2 , CRITERE3 , COMPTECG ,              
        SECTION , RUBRIQUE , DATEFF , WSEQFAM , DSYST , WFX00                   
    FROM  M907.RTFX90                                                           
;                                                                               
CREATE  VIEW M907.RVGA0100                                                      
        AS                                                                      
SELECT  CTABLEG1 , CTABLEG2 , WTABLEG                                           
    FROM  M907.RTGA01                                                           
;                                                                               
CREATE  VIEW M907.RVGA01AB                                                      
        ( CTABLEG2 , NSOCDEPO , NDEPOT , CZONE , NSEQ , WTABLEG ,               
        CNIVEAU , CPOSIT ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 2 ) , SUBSTR ( CTABLEG2 , 9 , 2 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 ,         
        6 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AGRAL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01AX                                                      
        ( CTABLEG2 , CODE_CAR , WTABLEG , LIBELLE , CVDCARAC , WCARACTS         
        , FINNOV ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR (             
        WTABLEG , 26 , 1 ) , SUBSTR ( WTABLEG , 27 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CARSP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01B0                                                      
        ( CTABLEG2 , SOCENTRE , DEPOT , SOCLIEU , LIEU , NUMERO ,               
        WTABLEG , CSELART ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , SUBSTR ( CTABLEG2 , 10 , 3 )        
        , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1         
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CSELA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01BF                                                      
        ( CTABLEG2 , NSOCDEPO , NDEPOT , CSELART , WTABLEG , NSOCIETE ,         
        CDEPOT , CTYPE ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR ( WTABLEG , 7 , 1         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CDEPO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01BJ                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE , RANG , WACTIF ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 2 ) , SUBSTR (             
        WTABLEG , 23 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CEXPO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01BM                                                      
        ( CTABLEG2 , CCICS , WTABLEG , NSOC , LCICS , GCA ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 20 ) , SUBSTR (              
        WTABLEG , 24 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CISOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01BO                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , LMMJVSDA , MTWTFSSA ,         
        GROUPEE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 ) , SUBSTR ( WTABLEG , 17 , 1 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CMUTD'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01BU                                                      
        ( CTABLEG2 , VALEUR , WTABLEG , LIBELLE , TYPES , TYPE , WGROUPE        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'COPAR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01BX                                                      
        ( CTABLEG2 , CPARAM , CRAY , WTABLEG , WT , LIB_RAY , W , LLXTYP        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 30 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR ( WTABLEG , 34 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRAY1'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01BY                                                      
        ( CTABLEG2 , CPARAM , CRAY , WTABLEG , WT , LIB_RAY , W , LLXTYP        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 30 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR ( WTABLEG , 34 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CRAY2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01C6                                                      
        ( CTABLEG2 , CFORMAT , WTABLEG , WACTIF , NOMETAT , LFORMAT ,           
        ZSWITCH1 ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 30 ) , SUBSTR ( WTABLEG , 40 , 24 )                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FORMA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01CG                                                      
        ( CTABLEG2 , SOCIETE , NOMPROG , WTABLEG , DELAI , LIBRE ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 15 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DELAI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01CR                                                      
        ( CTABLEG2 , NSOCGL , WTABLEG , NENTITE ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ENTST'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01D7                                                      
        ( CTABLEG2 , TYPE , STATUT , WTABLEG , FLAG , COEFF , LIBELLE ,         
        CONTROLE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 ) , SUBSTR ( WTABLEG , 7 , 20 ) , SUBSTR ( WTABLEG , 27 ,         
        8 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INVST'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01DG                                                      
        ( CTABLEG2 , CTYPE , WTABLEG , LTYPE , SENS , NATURE ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 ) , SUBSTR (             
        WTABLEG , 32 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GCATP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01DI                                                      
        ( CTABLEG2 , CGCPLT , WTABLEG , LGCPLT , CTVA , QDC , QDS ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR (             
        WTABLEG , 26 , 3 ) , SUBSTR ( WTABLEG , 29 , 3 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GCPLT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01E5                                                      
        ( CTABLEG2 , CMODDEL , WTABLEG , LMODDEL , WPLASSO , QDECMINI ,         
        QDECMAXI , WDONNEES ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 2 ) , SUBSTR ( WTABLEG , 24 , 3 ) , SUBSTR (             
        WTABLEG , 27 , 33 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDLIV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01E6                                                      
        ( CTABLEG2 , SOCIETE , OPTION , WTABLEG , ETAT , FORMAT , TITRE         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 1 ) , SUBSTR ( WTABLEG , 10 , 47 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MEG36'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ED                                                      
        ( CTABLEG2 , CETAT , CAGREG , WTABLEG , LAGREG , COMMENT ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 30 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LAGRE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01EP                                                      
        ( CTABLEG2 , CLITRT , WTABLEG , LLITRT , ACTIF , ENTMAG ,               
        ASSOCIAT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 3 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LITRT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ET                                                      
        ( CTABLEG2 , CZONLIV , WTABLEG , LZONLIV , LIEUGEST , LDEPLIV )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 6 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVRG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01EU                                                      
        ( CTABLEG2 , CPLAGE , WTABLEG , LPLAGE , HEURE , CPLOGI , HDEBUT        
        , HFIN ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 4 ) , SUBSTR (               
        WTABLEG , 13 , 2 ) , SUBSTR ( WTABLEG , 15 , 4 ) , SUBSTR (             
        WTABLEG , 19 , 4 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LPLGE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01EV                                                      
        ( CTABLEG2 , CSELART , CFAM , WTABLEG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LSELA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01FA                                                      
        ( CTABLEG2 , CMODRGLT , WTABLEG , LMODRGLT , WMTVTE , C36SEQV ,         
        RENDU , IWERC ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 4 ) , SUBSTR ( WTABLEG , 35 , 5 ) , SUBSTR (             
        WTABLEG , 40 , 14 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MOPAI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01FB                                                      
        ( CTABLEG2 , CMODRGLT , WTABLEG , LMOD , WMTVTE , C36S , RENDU ,        
        IWERC ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 4 ) , SUBSTR ( WTABLEG , 35 , 5 ) , SUBSTR (             
        WTABLEG , 40 , 14 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MOPA2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01FI                                                      
        ( CTABLEG2 , CPROG , WTABLEG , WFLAG , QPVUNIT , COMMENT ,              
        WFLAG2 ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 15 ) , SUBSTR ( WTABLEG , 20 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NCGFC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01FK                                                      
        ( CTABLEG2 , NUMECS , WTABLEG , JC , JP , JF , SENS , LECS ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 10 ) , SUBSTR (            
        WTABLEG , 21 , 10 ) , SUBSTR ( WTABLEG , 31 , 1 ) , SUBSTR (            
        WTABLEG , 32 , 25 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NUECS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01G6                                                      
        ( CTABLEG2 , CAUTO , WTABLEG , LAUTO , WAUTO ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TAUTO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01GF                                                      
        ( CTABLEG2 , CODPRO , WTABLEG , FLAG , FAMCLT , SEPARTXT ,              
        DATEFFET , WCOUL ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 8 ) , SUBSTR (             
        WTABLEG , 41 , 7 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROGE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01GS                                                      
        ( CTABLEG2 , SECTEUR , CZONE , WTABLEG , LZONE , NSOCIETE ,             
        POUBELLE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 3 ) , SUBSTR ( WTABLEG , 24 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SAVIN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01H5                                                      
        ( CTABLEG2 , CVESPE , WTABLEG , LVESPE , WFLAG ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'VESPE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01H6                                                      
        ( CTABLEG2 , TYPE , WTABLEG , LIBEL ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'VTESP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01H8                                                      
        ( CTABLEG2 , CREMVTE , WTABLEG , LREMVTE , DEFFET , WTYPCOND ,          
        QPOURCEN , WREMVTE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 8 ) , SUBSTR (             
        WTABLEG , 29 , 3 ) , SUBSTR ( WTABLEG , 32 , 3 ) , SUBSTR (             
        WTABLEG , 35 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'VTREM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01HI                                                      
        ( CTABLEG2 , CLOGI , WTABLEG , WACTIF , CPROTOUR , WTRAITBA ,           
        WEDIT , LIEUGEST ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR (             
        WTABLEG , 34 , 24 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TLOGI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01HJ                                                      
        ( CTABLEG2 , TNOM , WTABLEG , LNOM , CAPPEL , WSOCIETE ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TNOM '                                                      
;                                                                               
CREATE  VIEW M907.RVGA01HM                                                      
        ( CTABLEG2 , NSOCIETE , TYPPRET , WTABLEG , LTYPPRET , ASSOCIAT         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 3 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TPRET'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01HN                                                      
        ( CTABLEG2 , CPROF , CMODDEL , CPLAGE , CEQUIP , WTABLEG ,              
        WACTIF , HEURE ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 2 ) , SUBSTR ( CTABLEG2 , 11 , 5 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        4 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TPROD'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01HO                                                      
        ( CTABLEG2 , PROFIL , WTABLEG , LIBELLE , FLAG , WECART , IMPRIM        
        , SATE ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 19 ) , SUBSTR (            
        WTABLEG , 42 , 4 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TPROG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01HX                                                      
        ( CTABLEG2 , TAUX , WTABLEG , LIBELLE , TVA , DATE , ANCTVA , CW        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 3 ) , SUBSTR (             
        WTABLEG , 24 , 8 ) , SUBSTR ( WTABLEG , 32 , 3 ) , SUBSTR (             
        WTABLEG , 35 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TXTVA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01IA                                                      
        ( CTABLEG2 , SOCIETE , ZONE , WTABLEG , LIBELLE , WTYP , WMGI ,         
        EDITDACE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 ) , SUBSTR ( WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ZPRIX'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01IG                                                      
        ( CTABLEG2 , CETAT , DEB_FIN , WTABLEG , NBLIGNES , COMMANDE )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 56 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EGIMP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01J0                                                      
        ( CTABLEG2 , TYPTRAN , CAPRET , WTABLEG , CTRAN , TYPAP ,               
        LIBTRAN , LIBCTRAN ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 30 ) , SUBSTR ( WTABLEG , 33 ,         
        25 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTINT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01J9                                                      
        ( CTABLEG2 , CMTMAXI , WTABLEG , PMTMAXI ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BAMAX'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01JV                                                      
        ( CTABLEG2 , NOMAG , CMOPAI , WTABLEG , CBANQ , LIBANQ ,                
        CTYPOPER , LIBEL ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 25 ) , SUBSTR ( WTABLEG , 29 , 5 ) , SUBSTR ( WTABLEG , 34 ,        
        10 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BQMAG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01K0                                                      
        ( CTABLEG2 , CFAM , CPANNE , WTABLEG , WACTIF ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEPAF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01K1                                                      
        ( CTABLEG2 , CREFUS , WTABLEG , WACTIF , LREFUS , WCHCODIC ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 35 ) , SUBSTR (              
        WTABLEG , 37 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEREF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01K5                                                      
        ( CTABLEG2 , CPROG , NSEQ , WTABLEG , WACTIF , WCLEDATA ,               
        ENVORIG , ENVDEST ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 16 ) , SUBSTR ( WTABLEG , 18 , 14 ) , SUBSTR ( WTABLEG , 32         
        , 14 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGSB'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01K9                                                      
        ( CTABLEG2 , NECART , WTABLEG , WACTIF , LECART , WMODRGLT ,            
        WANCBA , WECS ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BAERR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KA                                                      
        ( CTABLEG2 , NSIGNAT , WTABLEG , WACTIF , LSIGNAT ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 15 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BASIG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KC                                                      
        ( CTABLEG2 , CGARANTI , WTABLEG , WACTIF , LGARANTI , CMOTIF ,          
        WQTEMUL , WNHSMUL ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 10 ) , SUBSTR ( WTABLEG , 32 , 1 ) , SUBSTR (            
        WTABLEG , 33 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGAR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KF                                                      
        ( CTABLEG2 , CTYPTRAN , WTABLEG , WACTIF , LTYPTRAN ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HETRA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KH                                                      
        ( CTABLEG2 , NLIEUHED , WTABLEG , WACTIF , LLIEUHED ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HELID'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KI                                                      
        ( CTABLEG2 , CLIEUHET , WTABLEG , WACTIF , LLIEUHET , WSAISIES ,        
        CLIEUNAT , PARMNASC ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 5 ) , SUBSTR ( WTABLEG , 27 , 8 ) , SUBSTR (             
        WTABLEG , 35 , 9 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HELIT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KJ                                                      
        ( CTABLEG2 , CLIEUHEI , WTABLEG , WACTIF , LLIEUHEI ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HELII'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KK                                                      
        ( CTABLEG2 , NSOC , NLIEU , CTRAIT , WTABLEG , WACTIF , CTYPOPT         
        , LIBELLE ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFINT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KL                                                      
        ( CTABLEG2 , CTYPOPT , NOPTION , WTABLEG , WACTIF , COPTION ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6         
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 2 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFOPT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KM                                                      
        ( CTABLEG2 , COPTION , WTABLEG , WACTIF , LOPTION ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFCOP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KN                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , WACTIF , LTRAIT , TIERSHE ,             
        TOLERANC , FLAGS ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 16 ) , SUBSTR ( WTABLEG , 38 , 3 ) , SUBSTR (            
        WTABLEG , 41 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFTRT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KO                                                      
        ( CTABLEG2 , CACID , CLIEUHET , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEAUT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KP                                                      
        ( CTABLEG2 , CRENDU , WTABLEG , WACTIF , LRENDU , WRENDU ,              
        CTYPRDU , TYPREP ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFRDU'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KR                                                      
        ( CTABLEG2 , NTYOPE , WTABLEG , WACTIF , LTYOPE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HETYO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KU                                                      
        ( CTABLEG2 , NSOCDEPO , NDEPOT , CQUOTA , WTABLEG , LQUOTA ,            
        WACTIF , DATA ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR ( WTABLEG , 22 ,        
        30 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GRQUO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KW                                                      
        ( CTABLEG2 , CTRAIT , CRENDU , WTABLEG , WACTIF , CMOTIF ,              
        DESTRET , FLAGIR ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 ) , SUBSTR ( WTABLEG , 12 , 14 ) , SUBSTR ( WTABLEG , 26         
        , 1 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFRDF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KY                                                      
        ( CTABLEG2 , CTRAIT , CRENDU , WTABLEG , WACTIF , ENVORIG ,             
        ENVDEST ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 14 ) , SUBSTR ( WTABLEG , 16 , 14 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFENV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01KZ                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , WACTIF , NTEL , NFAX , NBCOP )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 11 ) , SUBSTR (              
        WTABLEG , 13 , 11 ) , SUBSTR ( WTABLEG , 24 , 1 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFBEF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L0                                                      
        ( CTABLEG2 , CTYPE , WTABLEG , WACTIF , LTYPE , WAVOIR , SENSDC         
        , FLAGS ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR (              
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 1 ) , SUBSTR (             
        WTABLEG , 29 , 12 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L1                                                      
        ( CTABLEG2 , NATURE , WTABLEG , WACTIF , LIBELLE , VENT ,               
        WAUTEMET , WAUTREC ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR (             
        WTABLEG , 34 , 10 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGNAT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L2                                                      
        ( CTABLEG2 , NATURE , WTABLEG , WACTIF , ALIM , FREQ , ECHEANCE         
        , JOUR ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 2 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 2 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGFRQ'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L3                                                      
        ( CTABLEG2 , NATURE , TYPDOC , TYPEER , NOECS , WTABLEG , WACTIF        
        , WGL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , SUBSTR ( CTABLEG2 , 8 , 1 ) , SUBSTR ( CTABLEG2 , 9 , 5 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGFAC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L6                                                      
        ( CTABLEG2 , NSOCGL , NAUX , WTABLEG , NETAB , WACTIF ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BAAUX'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L7                                                      
        ( CTABLEG2 , EXERCICE , PERIODE , WTABLEG , DDEBUT , DFIN ,             
        LPERIODE , DFSAISIE ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 ) , SUBSTR ( WTABLEG , 17 , 20 ) , SUBSTR ( WTABLEG , 37 ,        
        8 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGPER'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L8                                                      
        ( CTABLEG2 , CTRAIT , WTABLEG , WACTIF , LTRAIT , CMOTIF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 10 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HETRT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01L9                                                      
        ( CTABLEG2 , CTYPEHS , WTABLEG , WACTIF , LTYPEHS ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HETHS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01LL                                                      
        ( CTABLEG2 , CPSEMGI , WTABLEG , WACTIF , PPSEMGI , LPSEMGI ,           
        CTVAMGI , WDATAPSE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 32 ) , SUBSTR ( WTABLEG , 40 , 1 ) , SUBSTR (             
        WTABLEG , 41 , 15 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MGPSE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01LO                                                      
        ( CTABLEG2 , CTABLEG1 , WTABLEG , WACTIF , NSFICMGI , WFILIALE )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MGTRF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01LR                                                      
        ( CTABLEG2 , CFRAIS , NATURE , WTABLEG , WACTIF , BASEMT , CTVA         
        , TYPDOC ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 8         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGANN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01LV                                                      
        ( CTABLEG2 , NATURE , CTYPE , WTABLEG , WACTIF , TIERS ,                
        LETTRAGE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 2 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGINT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01LW                                                      
        ( CTABLEG2 , NATURE , CVENT , WTABLEG , WACTIF , LVENT , TYPEDC         
        , PRIMAIRE ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 30 ) , SUBSTR ( WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 ,        
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGVEN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01LX                                                      
        ( CTABLEG2 , COMPTE , WTABLEG , WACTIF , LIBELLE , TYPE , NAUX ,        
        TYPCPTE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR (             
        WTABLEG , 34 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGPCG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01M0                                                      
        ( CTABLEG2 , CTYPCPT , WTABLEG , WACTIF , LTYPCPT ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TYCPT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01M3                                                      
        ( CTABLEG2 , NCSAV , WTABLEG , WACTIF , CTRAIT ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFSAV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01M4                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WACTIF , CPRIX , CDATE ,          
        COEFF ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 4         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFMTP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01M5                                                      
        ( CTABLEG2 , CTRAIT , CGARANTI , WTABLEG , WACTIF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFGAR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01M9                                                      
        ( CTABLEG2 , CPROG , NSEQ , WTABLEG , WACTIF , FLAGAVAR , TYOPAV        
        , TYOPAR ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 8 ) , SUBSTR ( WTABLEG , 10 , 6 ) , SUBSTR ( WTABLEG , 16 ,         
        6 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGOP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MB                                                      
        ( CTABLEG2 , NSOCFISC , NSEQ , WTABLEG , WACTIF , LIB ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 60 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGLIB'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MC                                                      
        ( CTABLEG2 , NSOCC , WTABLEG , WACTIF , NSOCFISC , WFETAB ,             
        WALIM , LIBELLE ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 ) , SUBSTR (               
        WTABLEG , 7 , 25 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MD                                                      
        ( CTABLEG2 , NOECS , WTABLEG , WACTIF , MOT1 , MOT2 , MOT3 ,            
        TYPMT ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 8 ) , SUBSTR ( WTABLEG , 18 , 8 ) , SUBSTR (             
        WTABLEG , 26 , 6 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGECS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MF                                                      
        ( CTABLEG2 , NSOCC , NSOCDEP , WTABLEG , WACTIF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSGP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MG                                                      
        ( CTABLEG2 , NPOLPR , WTABLEG , WACTIF , NPOLPS , ABC , ORD ,           
        LAR ) AS                                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 4 ) , SUBSTR ( WTABLEG , 36 , 4 ) , SUBSTR (             
        WTABLEG , 40 , 4 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'POLPS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MH                                                      
        ( CTABLEG2 , CDOC , CCHRONO , WTABLEG , WACTIF ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MI                                                      
        ( CTABLEG2 , CACID , WTABLEG , WACTIF , NSOCLIEU , WGROUPE ,            
        SERVICE , AUTOR ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 1 ) , SUBSTR ( WTABLEG , 9 , 5 ) , SUBSTR (               
        WTABLEG , 14 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MJ                                                      
        ( CTABLEG2 , NATURE , NSOCC , WTABLEG , WACTIF , WSENS ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAUT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MK                                                      
        ( CTABLEG2 , CLIENT , WTABLEG , FLAG , LIEU , CVENT ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDAC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ML                                                      
        ( CTABLEG2 , NSOC , CETAB , WTABLEG , WACTIF , LIBELLE , WFACT ,        
        WETBPRIN ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 ) , SUBSTR ( WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 ,        
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGETB'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MM                                                      
        ( CTABLEG2 , NSOCFISC , WTABLEG , WACTIF , NSOCREG , NSOCGRP )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGFIK'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MN                                                      
        ( CTABLEG2 , MOTCLE , WTABLEG , WACTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGMOT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MP                                                      
        ( CTABLEG2 , NSOC , SECTION , WTABLEG , WACTIF , LIBELLE ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSEC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MQ                                                      
        ( CTABLEG2 , CTYPMT , WTABLEG , WACTIF , LIB , TYPLIG ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTMT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MR                                                      
        ( CTABLEG2 , NDEPOT , WTABLEG , WQJOUR , WGENGRO , GEMPLACT ,           
        WRESFOUR ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 1 ) , SUBSTR ( WTABLEG , 6 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GADEP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MX                                                      
        ( CTABLEG2 , ORGA , SOCIETE , WTABLEG , ACTIF , APPC ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ORFIN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01MY                                                      
        ( CTABLEG2 , LIEU , WTABLEG , PWD06 , PWD07 , PWD08 , PWD09 ,           
        PWD11 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 ) , SUBSTR (             
        WTABLEG , 21 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PWINV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01N5                                                      
        ( CTABLEG2 , CPANNE , WTABLEG , WACTIF , LPANNE , WGENER ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEPAN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01N6                                                      
        ( CTABLEG2 , NPOLPR , WTABLEG , P , SIZE , STYLE , BOLD , TYPO )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 4 ) , SUBSTR (               
        WTABLEG , 6 , 1 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR (               
        WTABLEG , 8 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'POLP5'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01N7                                                      
        ( CTABLEG2 , CFAM , WTABLEG , PT ) AS                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PAMPT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01N9                                                      
        ( CTABLEG2 , CACID , NLIEUHED , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NB                                                      
        ( CTABLEG2 , NSOC , NATURE , CVENT , WTABLEG , WACTIF , SERVDEST        
        , WTRANSPO ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 1         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAFR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NC                                                      
        ( CTABLEG2 , NSOC , NATURE , CVENT , WTABLEG , ACTIF , SERVORIG         
        , WTRANSPO ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 1         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAFE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ND                                                      
        ( CTABLEG2 , CAPPLI , WTABLEG , WACTIF , CCHRONO , CVENT , CFAC         
        , TVA ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 7 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAPP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NE                                                      
        ( CTABLEG2 , CAPPLI , CAVENT , WTABLEG , ACTIF , CVENT ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGAVE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NF                                                      
        ( CTABLEG2 , NSOCG , WTABLEG , WACTIF , NSOCS36 ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCOS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NG                                                      
        ( CTABLEG2 , NSOC , NJRN , WTABLEG , WACTIF , CAPPLI ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,        
        2 , 3 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDEV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NH                                                      
        ( CTABLEG2 , NSOC , SERVICE , WTABLEG , WACTIF , LIBELLE ,              
        EMETTEUR , ETAB ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 ) , SUBSTR ( WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 ,        
        3 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSER'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NI                                                      
        ( CTABLEG2 , NSOC , CCHRONO1 , SERVICE , WTABLEG , WACTIF ,             
        CCHRONO2 ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , SUBSTR ( CTABLEG2 , 6 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTCC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NJ                                                      
        ( CTABLEG2 , NSOC , NJRN1 , NSEQ , WTABLEG , WACTIF , SERVICE ,         
        NJRN2 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 ,         
        10 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTCJ'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NK                                                      
        ( CTABLEG2 , NSOC , NJRN1 , NSEQ , WTABLEG , WACTIF , NATFACT ,         
        NJRN2 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 ,         
        10 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTCN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NL                                                      
        ( CTABLEG2 , CTYPMVT , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 14 ) , WTABLEG                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEGSA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NN                                                      
        ( CTABLEG2 , CTAUXTVA , WTABLEG , WACTIF , PCOEFF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 4 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TVACO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NP                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , WACTIF , WCTRLNHS , NBEXHE15 ,        
        WPANNE , WSAVELA ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 2 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HESOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NT                                                      
        ( CTABLEG2 , CACID , WTABLEG , WACTIF , NSOCIETE , NLIEU ,              
        LIBELLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CAADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NW                                                      
        ( CTABLEG2 , CCOUL , CUTCOUL , WTABLEG , WCADRE , NCOUL , NSAT ,        
        LCOUL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 4 ) , SUBSTR ( WTABLEG , 6 , 3 ) , SUBSTR ( WTABLEG , 9 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROCO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NX                                                      
        ( CTABLEG2 , CCOUL , WTABLEG , WACTIF , LCOUL ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROTP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NY                                                      
        ( CTABLEG2 , CODPRO , WTABLEG , CCOUL1 , CCOUL2 , CCOUL3 ,              
        CCOUL4 , CCOUL5 ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 ) , SUBSTR (             
        WTABLEG , 21 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROPP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01NZ                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RACOD'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O0                                                      
        ( CTABLEG2 , NSOCTR , NETABTR , WTABLEG , NATREMUN , CTAUXTVA ,         
        NBJOURS , TYPEDOC ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 ) , SUBSTR ( WTABLEG , 11 , 2 ) , SUBSTR ( WTABLEG , 13 ,         
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGRMP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O1                                                      
        ( CTABLEG2 , NSOCTR , NETABTR , WTABLEG , JRNSOC , JRNTRESO ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG        
        , 31 , 30 )                                                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGRMJ'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O2                                                      
        ( CTABLEG2 , CETAT , WTABLEG , DDEBUT , DFIN ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AVETA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O3                                                      
        ( CTABLEG2 , ENTITE , WTABLEG , LIBELLE , DEVISE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 3 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTENT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O4                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE , NBPAGE ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 5 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTDOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O5                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTDAS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O6                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE , CODEGEO ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 1 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTPAY'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O7                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTGEO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O8                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE , NBMOIS ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 2 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTEPU'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01O9                                                      
        ( CTABLEG2 , ENTITE , CODE , WTABLEG , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTTRE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OB                                                      
        ( CTABLEG2 , LIEUHC , WTABLEG , LLIEUHC , LIEUST , WCMOTIF ,            
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 14 ) , SUBSTR (            
        WTABLEG , 35 , 10 ) , SUBSTR ( WTABLEG , 45 , 1 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HCTLI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OC                                                      
        ( CTABLEG2 , NSOCLIEU , NSSLIEU , CLIEUTRT , WTABLEG , WACTIF ,         
        CAPPLI , WCONTROL ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR ( WTABLEG , 5 ,         
        4 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HSAPP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OD                                                      
        ( CTABLEG2 , DTRAIT , WTABLEG , WACTIF , MOIS , ABONNE , FACTURE        
        , REMUNE ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 1 ) , SUBSTR ( WTABLEG , 9 , 1 ) , SUBSTR (               
        WTABLEG , 10 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCAL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OE                                                      
        ( CTABLEG2 , CTYPE , NATURE , NSEQ1 , NSEQ2 , WTABLEG , WACTIF ,        
        WCOMMENT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        5 ) , SUBSTR ( CTABLEG2 , 8 , 1 ) , SUBSTR ( CTABLEG2 , 9 , 2 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        38 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCOM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OF                                                      
        ( CTABLEG2 , CBATVA , WTABLEG , CTAUXTVA ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BATVA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OG                                                      
        ( CTABLEG2 , CACID , INTGEF , WTABLEG , WACTIF , CTA , CTYPOPT )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        11 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,        
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 5 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OH                                                      
        ( CTABLEG2 , CTRCTA , CTRAIT , WTABLEG , WACTIF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFCTA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OJ                                                      
        ( CTABLEG2 , MGI , WTABLEG , CICS , TOR ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 4 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CIMGI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OK                                                      
        ( CTABLEG2 , CCLASSE , WTABLEG , WACTIF , CCOUL , LCOMMENT ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 50 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROEN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OL                                                      
        ( CTABLEG2 , NSOC , WTABLEG , WFILIALE , COMMENT ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PCFFI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ON                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , NMAGFIC , ECTRAIT , TCTRAIT ,         
        CRENDU ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 5 ) , SUBSTR (               
        WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 5 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HSIN1'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OO                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , DEPTRAIT , MAGTRAIT , HSDEP ,         
        HSMAG ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HSIN2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OP                                                      
        ( CTABLEG2 , CPANGRF , CFAMGRF , WTABLEG , CPANGHS ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HSCOP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OQ                                                      
        ( CTABLEG2 , TIERSGRF , WTABLEG , TIERSGHS ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HSCOT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OR                                                      
        ( CTABLEG2 , NOMPROG , NSEQ , WTABLEG , CSTATUT , WACTIF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HCSTA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OT                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , CGESTION , NLIEUINI , DEPOTGRF        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HSGES'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OU                                                      
        ( CTABLEG2 , CFAM , NSEQ , WTABLEG , WACTIF , CMARKET1 ,                
        CMARKET2 , CFAM36 ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 ) , SUBSTR ( WTABLEG , 12 , 10 ) , SUBSTR ( WTABLEG , 22         
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FAM36'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OV                                                      
        ( CTABLEG2 , NSOCC , WTABLEG , WACTIF , NSOCIETE , NLIEU ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGLGP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OW                                                      
        ( CTABLEG2 , NLIEUHC , CSTATUT , WTABLEG , LSTATUT , WACTIF ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG        
        , 21 , 1 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HCCST'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OX                                                      
        ( CTABLEG2 , NSOCC , NETAB , SERVICE , SENS , WTABLEG , WACTIF ,        
        SERVCOR ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , SUBSTR ( CTABLEG2 , 12 , 1 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        5 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTSV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OY                                                      
        ( CTABLEG2 , CTAUX , DEFFET , NSOC , WTABLEG , TAUXPREC , TAUX )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        8 ) , SUBSTR ( CTABLEG2 , 10 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 6 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGTXI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01OZ                                                      
        ( CTABLEG2 , NSOCC , CETAB , COMPTE , CTIERS , WTABLEG , WACTIF         
        , CCOURANT ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , SUBSTR ( CTABLEG2 , 13 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        6 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGREG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01P0                                                      
        ( CTABLEG2 , CLIEUHET , CTRAIT , WTABLEG , WACTIF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEASS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01P3                                                      
        ( CTABLEG2 , NSOC , WTABLEG , WACTIF , LORIG , LDEST ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 9 ) , SUBSTR (               
        WTABLEG , 11 , 9 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HSREG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01P4                                                      
        ( CTABLEG2 , NUMCLI , WTABLEG , NSOC , NLIEU ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CCDA1'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PC                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , TRANCHED , TRANCHEF , WACTIF )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 7 ) , SUBSTR ( WTABLEG , 8 , 7 ) , SUBSTR (               
        WTABLEG , 15 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GATRA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PD                                                      
        ( CTABLEG2 , NCODIC , WTABLEG ) AS                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GAKAP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PJ                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , ACTIF , ZPRIX , EXPO , W )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 5 ) , SUBSTR ( WTABLEG , 9 , 1         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AVENV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PK                                                      
        ( CTABLEG2 , NSOC , CODE , WTABLEG , LIBELLE ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTETA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PL                                                      
        ( CTABLEG2 , NSOC , NOSAV , WTABLEG , WACTIF , CVENT ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 5 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSAV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PM                                                      
        ( CTABLEG2 , CACID , WTABLEG , WACTIF , LACID ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEFLU'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PN                                                      
        ( CTABLEG2 , NSOC , CODE , WTABLEG , LIBELLE , D_C ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 1 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTNAT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PO                                                      
        ( CTABLEG2 , LIEUHC , CTYPE , WTABLEG , WACTIF , LTYPE ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HCTPI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PP                                                      
        ( CTABLEG2 , NSOC , NATURE , AUX , WTABLEG , COMPTE ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 6 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTCPT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PQ                                                      
        ( CTABLEG2 , CACID , WTABLEG , NSOCLIEU ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CDADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PR                                                      
        ( CTABLEG2 , NSOCLIEU , NSEQ , WTABLEG , CCLIENT , TCRITERE ,           
        CRITERE , NSOCCONS ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 1 ) , SUBSTR ( WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 6        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CDDAC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01PY                                                      
        ( CTABLEG2 , CTCARTE , WTABLEG , WACTIF , LCARTE , WACTIFS ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QCTCA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Q0                                                      
        ( CTABLEG2 , NSOC , CMOTIF , DEFFET , WTABLEG , TAUX ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , SUBSTR ( CTABLEG2 , 6 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RATVA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Q2                                                      
        ( CTABLEG2 , NPOL , NSEQ , WTABLEG , WACTIF , DATAS ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 59 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'POLLG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Q3                                                      
        ( CTABLEG2 , CTRANS , WTABLEG , CTRAN , TYPAP , LIBTRAN ,               
        LIBCTRAN , WCOMPTA ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 30 ) , SUBSTR ( WTABLEG , 33 , 17 ) , SUBSTR (            
        WTABLEG , 50 , 10 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CTIN3'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Q4                                                      
        ( CTABLEG2 , CTLIEU , WTABLEG , WACTIF , LIBELLE , CTYPLIEU ,           
        CTYPSOC , WCONTROL ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 3 ) , SUBSTR (             
        WTABLEG , 26 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HCLIE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Q5                                                      
        ( CTABLEG2 , IDENT , WTABLEG , ACTIF , SA1 , SA2 , SA3 ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IDADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Q6                                                      
        ( CTABLEG2 , NSOCC , NETAB , WTABLEG , WACTIF , NSOCNCG , WFX00         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 6 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGSCT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Q9                                                      
        ( CTABLEG2 , CINSEE , WTABLEG , LREGION , CPAYS , WPARBUR ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 26 ) , SUBSTR ( WTABLEG , 27 , 5 ) , SUBSTR (             
        WTABLEG , 32 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVETR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QB                                                      
        ( CTABLEG2 , CTRQ , WTABLEG , LTRQ , WTRQ ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GRTRQ'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QC                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PPMAG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QD                                                      
        ( CTABLEG2 , NDEPOT , WTLMELA , WTABLEG , QNBREF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GS805'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QK                                                      
        ( CTABLEG2 , CLIENT , WTABLEG , ACTIF , SOC , LIEU , CVENT ,            
        TYPECLI ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 5 ) , SUBSTR (               
        WTABLEG , 13 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDA1'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QL                                                      
        ( CTABLEG2 , CPRIME , WTABLEG , WACTIF , LPRIME , CGRPRIME ,            
        LNOMZONE , CPRISIGA ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 5 ) , SUBSTR ( WTABLEG , 37 , 10 ) , SUBSTR (            
        WTABLEG , 47 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PPPRI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QM                                                      
        ( CTABLEG2 , CGRPRIME , WTABLEG , WACTIF , LGRPRIME ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PPGRP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QN                                                      
        ( CTABLEG2 , CTYPSIGA , WTABLEG , WACTIF , WCONST ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PPSIG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QO                                                      
        ( CTABLEG2 , NSOCLIEU , WTABLEG , WACTIF , QUOTA , CPERIM ,             
        GRVTE , QUOLIV ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 4 ) , SUBSTR (               
        WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 , 3 ) , SUBSTR (              
        WTABLEG , 14 , 4 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QCNBV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QP                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , DMOIS ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PPSTA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QQ                                                      
        ( CTABLEG2 , CACID , WTABLEG , CCLIENT , WACTIF , WPASS ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR (               
        WTABLEG , 8 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DACID'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QR                                                      
        ( CTABLEG2 , CCLIENT , WTABLEG , PASS , WACTIF , WPRIX ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DACMP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QT                                                      
        ( CTABLEG2 , CTYPE , TYPDOC , WTABLEG , LIBELLE , WPARAM ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 6 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GCATD'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QU                                                      
        ( CTABLEG2 , NSOCMGI , CFAMMGI , WTABLEG , RUBVEN , RUBPSE ,            
        RUBREM ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 ) , SUBSTR ( WTABLEG , 17 , 32 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MGFBI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QV                                                      
        ( CTABLEG2 , NJRN , NSOCC , NSEQ , WTABLEG , WACTIF , NETAB ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 ,          
        11 , 3 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGGCT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QW                                                      
        ( CTABLEG2 , CDFIL , CFAMMGI , CPSEMGI , WTABLEG , WACTIF ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        4 ) , SUBSTR ( CTABLEG2 , 8 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MGPSF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QX                                                      
        ( CTABLEG2 , NOMETAT , NOBJET , WTABLEG , WACTIF , DATAS ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 52 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROOG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01QZ                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WACTIF , WTYPE , LLIEU ,          
        SEC_SOC ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 25 ) , SUBSTR ( WTABLEG , 28 ,         
        9 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RALIE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01R0                                                      
        ( CTABLEG2 , CPROG , NSEQ , WTABLEG , CSELART ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MUEXC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01R2                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WTRI ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MUMAG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01R3                                                      
        ( CTABLEG2 , BGD025 , WTABLEG , DRAFALE , NRAFALE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 2 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GD025'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01R6                                                      
        ( CTABLEG2 , CPAYS , WTABLEG , LPAYS ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 26 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVET2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RA                                                      
        ( CTABLEG2 , CTYPLIG , WTABLEG , LTYPLIG , TLIEUSOC ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 24 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GBTLI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RC                                                      
        ( CTABLEG2 , NUMECS , WTABLEG , WACTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FXCTL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RD                                                      
        ( CTABLEG2 , CTPREST , WTABLEG , WACTIF , LTPREST , QMAJEUR ,           
        QOPTION ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 4 ) , SUBSTR ( WTABLEG , 26 , 4 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RG                                                      
        ( CTABLEG2 , CENERGIE , NSEQ , WTABLEG , WACTIF , NLGABC ,              
        LTEXTE , LCOMMENT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 6 ) , SUBSTR ( WTABLEG , 8 , 40 ) , SUBSTR ( WTABLEG , 48 ,         
        6 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROTE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RI                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , CGP , CFOUR , CQE ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MGCDE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RJ                                                      
        ( CTABLEG2 , CSTABLE , WTABLEG , WACTIF , LCOMMEN ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MINUS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RK                                                      
        ( CTABLEG2 , NSOC , WTABLEG , WAUTO , LCOMMENT ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SRPFI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RL                                                      
        ( CTABLEG2 , CSELART , CNATART , WTABLEG , AUTOMAJ , LCOMPENS )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 50 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GBQLI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RN                                                      
        ( CTABLEG2 , CMAJ , WTABLEG , LMAJ ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SPMAJ'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RO                                                      
        ( CTABLEG2 , CQCOEF , WTABLEG , QCOEFMIN , QCOEFMAX ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SPCOE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RP                                                      
        ( CTABLEG2 , SSF , FACTURE , WTABLEG , W , LIBELLE ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'BAFAC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RR                                                      
        ( CTABLEG2 , SSF , NSOCIETE , NLIEU , WTABLEG , WACTIF , WDEROG         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , SUBSTR ( CTABLEG2 , 6 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SRP34'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RX                                                      
        ( CTABLEG2 , ZPRIX , WTABLEG , WACTIF , LZPRIX , WPRINCIP ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRZPX'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RY                                                      
        ( CTABLEG2 , SOC , MARQUE , FAMILLE , WTABLEG , COEFF , FLAG )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , SUBSTR ( CTABLEG2 , 9 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 2 ) , SUBSTR ( WTABLEG , 3 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SRPK '                                                      
;                                                                               
CREATE  VIEW M907.RVGA01RZ                                                      
        ( CTABLEG2 , MARQUE , WTABLEG , WACTIF , JOUR , DELAI ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 2 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AVSRP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S0                                                      
        ( CTABLEG2 , DESTIN , WTABLEG , VOLUME , WCODBAR , WACTIF ,             
        WTYPE ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 9 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 ) , SUBSTR (               
        WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 , 8 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TL045'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S1                                                      
        ( CTABLEG2 , SSF , SAV , WTABLEG , W , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SAVF '                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S2                                                      
        ( CTABLEG2 , SSF , SAV , WTABLEG , W , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SAVT '                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S3                                                      
        ( CTABLEG2 , SSF , SAV , WTABLEG , W , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SAVE '                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S4                                                      
        ( CTABLEG2 , NCLE , WTABLEG , DEFFET , WACTASS , PTEG , PMENSUAL        
        , PCOMPTAN ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 2 ) , SUBSTR (               
        WTABLEG , 11 , 3 ) , SUBSTR ( WTABLEG , 14 , 3 ) , SUBSTR (             
        WTABLEG , 17 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CREXC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S5                                                      
        ( CTABLEG2 , ACID , WTABLEG , NZONPRIX ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GMZON'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S6                                                      
        ( CTABLEG2 , NZONE , WTABLEG , LZONE , NZONPRIX , NLIEUP ,              
        NLIEUS ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 2 ) , SUBSTR (             
        WTABLEG , 23 , 3 ) , SUBSTR ( WTABLEG , 26 , 3 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GM070'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01S7                                                      
        ( CTABLEG2 , CGRP , TGRP , WTABLEG , WACTIF , LGRP ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRGRP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SB                                                      
        ( CTABLEG2 , CDOMAPPL , CTABLE , WTABLEG , WACTIF , TYPEAUTO ,          
        LCOMMENT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 20 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTO1'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SC                                                      
        ( CTABLEG2 , CACID , CDOMAPPL , WTABLEG , WACTIF , LCOMMENT ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9         
        , 7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 20 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTO2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SD                                                      
        ( CTABLEG2 , CGRPETAT , WTABLEG , WACTIF , LGRPETAT , WFAM ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRGET'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SE                                                      
        ( CTABLEG2 , CETAT , WTABLEG , CGRPETAT , WACTIF , LETAT ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 ) , SUBSTR (               
        WTABLEG , 7 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRDET'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SF                                                      
        ( CTABLEG2 , CGESTION , WTABLEG , WACTIF , LGESTION , WENVOI ,          
        TYPE , TABLE ) AS                                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 ) , SUBSTR (              
        WTABLEG , 32 , 1 ) , SUBSTR ( WTABLEG , 33 , 1 ) , SUBSTR (             
        WTABLEG , 34 , 6 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRGES'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SH                                                      
        ( CTABLEG2 , CACID , WTABLEG , TCTPRT , LCOMMENT ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG , 5 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SPPRT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SI                                                      
        ( CTABLEG2 , CORGAN , WTABLEG , WACTIF , LORGAN , LGIDENT ,             
        SUFFIXE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 2 ) , SUBSTR ( WTABLEG , 24 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFORG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SJ                                                      
        ( CTABLEG2 , CTYPCTA , WTABLEG , WACTIF , LTYPCTA , APPC ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFTCT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SK                                                      
        ( CTABLEG2 , CTYPMONT , WTABLEG , WACTIF , LTYPMONT , WDETAIL ,         
        CMOTCLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFTMT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SL                                                      
        ( CTABLEG2 , CMODPAI , WTABLEG , WACTIF , LMODPAI ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFMPA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SM                                                      
        ( CTABLEG2 , CMOTCLE , WTABLEG , CONTENU ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFMOT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SN                                                      
        ( CTABLEG2 , CORGAN , CTYPMVT , WTABLEG , CMODPAI , NJRN ,              
        IMPDET ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 3 ) , SUBSTR ( WTABLEG , 7 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFOPA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SP                                                      
        ( CTABLEG2 , NDEPOT , NSEQ , WTABLEG , CSELART , HEURELIM ,             
        MINUTLIM , LMMJVSD ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 2 ) , SUBSTR ( WTABLEG , 8 , 2 ) , SUBSTR ( WTABLEG , 10 , 7        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EMDCS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SQ                                                      
        ( CTABLEG2 , NMAG , CSELART , WTABLEG , QNBP , QNBM3 ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 4 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EMDQU'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SR                                                      
        ( CTABLEG2 , NDEPOT , WTABLEG , QNBPG , QNBMG , QNBGM , QNBG ,          
        QNBPMIN ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 ) , SUBSTR (             
        WTABLEG , 21 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EMDG '                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SS                                                      
        ( CTABLEG2 , NDEPOT , WTABLEG , WTP ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EMDTP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ST                                                      
        ( CTABLEG2 , NSOCGROU , WTABLEG , WACTIF ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AUTOG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SU                                                      
        ( CTABLEG2 , CBANQUE , WTABLEG , WACTIF , LBANQUE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFBQE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SV                                                      
        ( CTABLEG2 , UTILIS , WTABLEG , SOCIETE , ETABL ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTSOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SW                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WACTIF , DFIN ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 8 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCLO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01SZ                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , LLIEU , NSOCCPT ,             
        NETAB ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 3 ) , SUBSTR ( WTABLEG , 24 , 3 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFSOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T0                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WACTIF ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGCTL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T2                                                      
        ( CTABLEG2 , CLE , WTABLEG , WACTIF , DEFFET , DFEFFET ,                
        AUTORISE , WZONES ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 8 ) , SUBSTR ( WTABLEG , 18 , 1 ) , SUBSTR (             
        WTABLEG , 19 , 30 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TFACT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T3                                                      
        ( CTABLEG2 , CLE , WTABLEG , WACTIF , DEFFET , DFEFFET , CCAISSE        
        , WZONES ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 8 ) , SUBSTR ( WTABLEG , 18 , 1 ) , SUBSTR (             
        WTABLEG , 19 , 30 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TPRIX'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T4                                                      
        ( CTABLEG2 , NSOCORIG , NLIEUORI , WTABLEG , WACTIF , LIBELLE )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFLID'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T5                                                      
        ( CTABLEG2 , CPARAM , CDEVISE , WTABLEG , WACTIF , HIT , ARRONDI        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 2 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TEURA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T6                                                      
        ( CTABLEG2 , INVDATE , WTABLEG , FLAGACT , FLAGSTK ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INVDF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T7                                                      
        ( CTABLEG2 , CFONCT , CODE , WTABLEG , LIBELLE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 60 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVCOM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T8                                                      
        ( CTABLEG2 , LIEUST , WTABLEG , NSOCIETE , NETAB ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LSTSO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01T9                                                      
        ( CTABLEG2 , CTYPUTIL , WTABLEG , LTYPUTIL ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AV000'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TB                                                      
        ( CTABLEG2 , NZONPRI , WTABLEG , LIEU ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GMPRI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TC                                                      
        ( CTABLEG2 , CSECTION , WTABLEG , WACTIF , LCODE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGSEC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TD                                                      
        ( CTABLEG2 , CACID , WTABLEG , WACTIF , NSOC , NETAB ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGSOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TE                                                      
        ( CTABLEG2 , CCOMPTA , WTABLEG , WACTIF , LCOMPTA , NOECS1 ,            
        NOECS2 , WLETTRA ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 5 ) , SUBSTR ( WTABLEG , 27 , 5 ) , SUBSTR (             
        WTABLEG , 32 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRECS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TF                                                      
        ( CTABLEG2 , CRAYON , CSAV , WTABLEG , WACTIF , CTIERS , WCUMUL         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 ) , SUBSTR ( WTABLEG , 4 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ELSAV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TG                                                      
        ( CTABLEG2 , COMPTE , WTABLEG , WACTIF , NJRN ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGJRN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TK                                                      
        ( CTABLEG2 , NSOC , WTABLEG , CBORNE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HVSTA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TL                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WACTIF , NTIERSCV ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 6 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFTIE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TM                                                      
        ( CTABLEG2 , NCODIC , NSEQ , WTABLEG , WACTIF , CPREST ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 53 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVPRE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TO                                                      
        ( CTABLEG2 , CSTRAIT , CATIN , CPREST , WTABLEG , LSTRAIT ,             
        CRITERE2 ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , SUBSTR ( CTABLEG2 , 10 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 5 )                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTSAV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TP                                                      
        ( CTABLEG2 , CTPREST , WTABLEG , WACTIF , DELMIN , DELMAX ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRTYL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TQ                                                      
        ( CTABLEG2 , NSOC , NLIEU , CFAM , WTABLEG , WACTIF , VALEUR )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FIFMG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TR                                                      
        ( CTABLEG2 , CDEVISE , WMONNAIE , WTABLEG , DDEBUT , DFIN ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EUROP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TS                                                      
        ( CTABLEG2 , CETAT , NSEQ , WTABLEG , DEFFET , CDEVISE ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG         
        , 9 , 3 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EURO2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TU                                                      
        ( CTABLEG2 , NTYPCONC , WTABLEG , LTYPCONC , CTYPENS ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RXTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TV                                                      
        ( CTABLEG2 , CLE , WTABLEG , DEBZONES , SUITZONE , NBDECIM ,            
        PSEURO , ARRONDI ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 16 ) , SUBSTR ( WTABLEG , 17 , 17 ) , SUBSTR (            
        WTABLEG , 34 , 1 ) , SUBSTR ( WTABLEG , 35 , 1 ) , SUBSTR (             
        WTABLEG , 36 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TFM04'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TW                                                      
        ( CTABLEG2 , CLE , WTABLEG , WACTIF , DEFFET , OPERATOR , TAUX ,        
        WZONES ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 1 ) , SUBSTR ( WTABLEG , 11 , 5 ) , SUBSTR (             
        WTABLEG , 16 , 25 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TFM05'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TX                                                      
        ( CTABLEG2 , CLE , WTABLEG , WACTIF , DEFFET , DFEFFET ,                
        WMONNAIE , WZONES ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 8 ) , SUBSTR ( WTABLEG , 18 , 1 ) , SUBSTR (             
        WTABLEG , 19 , 30 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TEURO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01TY                                                      
        ( CTABLEG2 , CLE , WTABLEG , WACTIF , DFEFFET , RENDMON ,               
        RMAXMON , ARRONDI ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 1 ) , SUBSTR ( WTABLEG , 11 , 3 ) , SUBSTR (             
        WTABLEG , 14 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MOPAE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01U1                                                      
        ( CTABLEG2 , CALERT , WTABLEG , LALERT ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD80'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01U3                                                      
        ( CTABLEG2 , CMPAIDEV , WTABLEG , DEFFFEFF , WRENDU , PREND1 ,          
        PREND2 , PARRON ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 16 ) , SUBSTR ( WTABLEG , 17 , 1 ) , SUBSTR (             
        WTABLEG , 18 , 5 ) , SUBSTR ( WTABLEG , 23 , 5 ) , SUBSTR (             
        WTABLEG , 28 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFM52'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01U4                                                      
        ( CTABLEG2 , CMOPAI , CMORMB , CDEV , WTABLEG , DEFFFEFF ,              
        SEUIL1 , SEUILM ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 16 ) , SUBSTR ( WTABLEG , 17 , 5 ) , SUBSTR ( WTABLEG , 22        
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EFM53'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01U5                                                      
        ( CTABLEG2 , COCRED , CFCRED , WTABLEG , WTPCRD , PCOMPT ,              
        LFRMLE ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 ) , SUBSTR ( WTABLEG , 11 , 30 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV18'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01U6                                                      
        ( CTABLEG2 , CREPRI , WTABLEG , LREPRI ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV19'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01U7                                                      
        ( CTABLEG2 , CNATUR , WTABLEG , LNATUR ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV86'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01U8                                                      
        ( CTABLEG2 , NTIERS , WTABLEG , LTIERS ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 9 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 25 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPA53'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UA                                                      
        ( CTABLEG2 , CTYPAV , WTABLEG , LTYPAV ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AV001'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UB                                                      
        ( CTABLEG2 , NSOCIETE , CMOTIFAV , WTABLEG , LMOTIFAV , NUECS ,         
        CTAUXTVA , WTECVENT ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 5 ) , SUBSTR ( WTABLEG , 26 , 5 ) , SUBSTR ( WTABLEG , 31 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AV002'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UD                                                      
        ( CTABLEG2 , CPARAM , NSOC , WTABLEG , WACTIF , TICKET , DECOMPT        
        , NCHEQUE ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TEURD'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UE                                                      
        ( CTABLEG2 , CABSENCE , WTABLEG , WACTIF , TYPE , PAYEE ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGABS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UF                                                      
        ( CTABLEG2 , TYPE , NATURE , HORAIRE , WTABLEG , WACTIF ,               
        CONTRAT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , SUBSTR ( CTABLEG2 , 5 , 4 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGCON'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UG                                                      
        ( CTABLEG2 , CLE , CODE , WTABLEG , FLAG , COMMENT ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'CSPRE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UH                                                      
        ( CTABLEG2 , SSGENRE , CODDESC , WTABLEG , FLAG , CRITERE ,             
        FLAGVAL , FAMILLE ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 8 ) , SUBSTR ( WTABLEG , 10 , 1 ) , SUBSTR ( WTABLEG , 11 ,         
        6 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'KPCRI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UI                                                      
        ( CTABLEG2 , FAMILLE , NSEQ , WTABLEG , FLAG , CMARKET ,                
        CDESCRIP , SSG8CRI8 ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 ) , SUBSTR ( WTABLEG , 12 , 10 ) , SUBSTR ( WTABLEG , 22         
        , 22 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'KPFAM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UJ                                                      
        ( CTABLEG2 , CMARQ , WTABLEG , WFLAG , KMARQ , FLAGCRE ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'KPMAR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UK                                                      
        ( CTABLEG2 , CFORMAT , WTABLEG , WACTIF , NOMETAT , LFORMAT ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 30 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FORMK'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UL                                                      
        ( CTABLEG2 , NSOCIETE , MODP , WTABLEG , LIBMODP ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AV004'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UN                                                      
        ( CTABLEG2 , CETAT , WTABLEG , DDEBUT , DFIN ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NEMET'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UT                                                      
        ( CTABLEG2 , TYPCON , NATCON , WTABLEG , WACTIF , TYPCTA ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 2 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGCTA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UU                                                      
        ( CTABLEG2 , CSTABLE , LETTRAGE , WTABLEG , WFLAG , COMMENT ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3         
        , 8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 20 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SAVNM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UV                                                      
        ( CTABLEG2 , CMOPAI , CDEVWTCO , WSEQAF , WTABLEG , PVALCP ,            
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        4 ) , SUBSTR ( CTABLEG2 , 10 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD01'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UW                                                      
        ( CTABLEG2 , CTCAIS , CMOPAI , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD02'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UX                                                      
        ( CTABLEG2 , CTCAIS , CMVTFC , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        2 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD03'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UY                                                      
        ( CTABLEG2 , CTTRAN , CTCAIS , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        1 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD06'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01UZ                                                      
        ( CTABLEG2 , CTCAIS , WTABLEG , LTCAIS , WTRNSF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD21'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V1                                                      
        ( CTABLEG2 , CAPPLI , CMVT , NENTITE , WTABLEG , LIBELLE ,              
        CTYPOPER , CNATOPER ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR ( WTABLEG , 26        
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM003'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V2                                                      
        ( CTABLEG2 , CAPPLI , COPERADM , WTABLEG , LIBELLE , CTYPOPER ,         
        CNATOPER ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 5 ) , SUBSTR ( WTABLEG , 26 , 5 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM004'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V3                                                      
        ( CTABLEG2 , NOMPROG , CFONC , WTABLEG , QALIAS , MSGID , DUREE         
        , LONGUEUR ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 18 ) , SUBSTR ( WTABLEG ,        
        19 , 24 ) , SUBSTR ( WTABLEG , 43 , 5 ) , SUBSTR ( WTABLEG , 48         
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MQGET'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V4                                                      
        ( CTABLEG2 , NOMPROG , CFONC , WTABLEG , QALIAS , MSGID , DUREE         
        , PERS ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 18 ) , SUBSTR ( WTABLEG ,        
        19 , 24 ) , SUBSTR ( WTABLEG , 43 , 5 ) , SUBSTR ( WTABLEG , 48         
        , 1 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MQPUT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V6                                                      
        ( CTABLEG2 , CAPPLI , CNATOPER , WTABLEG , CNATFACT , CVENT ,           
        CUMUL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 ) , SUBSTR ( WTABLEG , 11 , 1 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM005'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V7                                                      
        ( CTABLEG2 , NSOC , NLIEU , NCAISS , WTABLEG , CTCAIS ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EAD04'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V8                                                      
        ( CTABLEG2 , CREMIS , WTABLEG , CNATOPER ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM006'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01V9                                                      
        ( CTABLEG2 , CPREST , WTABLEG , CNATOPER ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM007'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VC                                                      
        ( CTABLEG2 , CTTRAN , WTABLEG , LBTRAN , CANNUL ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM04'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VD                                                      
        ( CTABLEG2 , CMVTFC , WTABLEG , LBMVTF , LMEDIT ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM07'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VE                                                      
        ( CTABLEG2 , LPAPPT , CVCODE , CACT , WTABLEG , CTCODE , LPAPPE         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 4 ) , SUBSTR ( CTABLEG2 , 15 , 1 ) , WTABLEG , SUBSTR (               
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 10 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM11'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VG                                                      
        ( CTABLEG2 , CCOMV , CTCOMV , WTABLEG , LCOMV ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM30'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VH                                                      
        ( CTABLEG2 , NSOCLIEU , CTYPE , WSEQLA , WTABLEG , NPROG ,              
        DATEDP , PERIOD ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , SUBSTR ( CTABLEG2 , 12 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 8 ) , SUBSTR ( WTABLEG , 19        
        , 1 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM36'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VI                                                      
        ( CTABLEG2 , CMOTIF , WTABLEG , LMOTIF , WPASS ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM50'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VJ                                                      
        ( CTABLEG2 , TPCALC , CDEV , WTABLEG , WARRON , QARRON ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EPM60'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VL                                                      
        ( CTABLEG2 , CSTABLE , WTABLEG , WACTIF ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NEMAT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VN                                                      
        ( CTABLEG2 , SOCLIEU , DATE , NSEQ , WTABLEG , INDICATE ,               
        LIEUGEST , DOMAINE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        8 ) , SUBSTR ( CTABLEG2 , 15 , 1 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 6 ) , SUBSTR ( WTABLEG , 12 ,        
        5 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NASL1'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VR                                                      
        ( CTABLEG2 , CACID , WTABLEG , ACTIF , SOCLIEU , COMPTA , GROUPE        
        , ENCNASL ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 ) , SUBSTR (               
        WTABLEG , 8 , 1 ) , SUBSTR ( WTABLEG , 9 , 1 ) , SUBSTR (               
        WTABLEG , 10 , 2 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VS                                                      
        ( CTABLEG2 , DOMAINE , COMPTEUR , WTABLEG , ACTIF , LIBELLE ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6         
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 20 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NASL2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VT                                                      
        ( CTABLEG2 , CMDRGT , WTABLEG , LMDRGL , NSEQED , CMDRGTH ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 2 ) , SUBSTR (             
        WTABLEG , 33 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV83'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VV                                                      
        ( CTABLEG2 , CODERX2 , WTABLEG , WACTIF , LIBRX2 , CTR1RX2 ,            
        CTR2RX2 , FILLER ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 6 ) , SUBSTR ( WTABLEG , 28 , 6 ) , SUBSTR (             
        WTABLEG , 34 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVRX2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VW                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , WACTIF , WDACEM , NBJ , COEFF )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 3 ) , SUBSTR ( WTABLEG , 6 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DACEM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VX                                                      
        ( CTABLEG2 , CAPPLI , MODPMT , WTABLEG , LIBELLE , CTYPOPER ,           
        CNATOPER ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,        
        21 , 5 ) , SUBSTR ( WTABLEG , 26 , 5 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM000'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VY                                                      
        ( CTABLEG2 , CAPPLI , NENTITE , TYPVENTE , WTABLEG , LIBELLE ,          
        CTYPOPER , CNATOPER ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 1 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR ( WTABLEG , 26        
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM001'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01VZ                                                      
        ( CTABLEG2 , CAPPLI , NENTITE , CODES , WTABLEG , LIBELLE ,             
        CTYPOPER , CNATOPER ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 ) , SUBSTR ( WTABLEG , 26        
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM002'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W0                                                      
        ( CTABLEG2 , CFLGMAG , NSEQ , WTABLEG , CGROUP ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FLCMG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W1                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , NSECTION , WTABLEG , LIBELLE )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 20 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AV005'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W2                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , CACID , WTABLEG , WFLAG ,               
        NSECTION ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 7 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 6 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'AV003'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W3                                                      
        ( CTABLEG2 , NSOCIETE , WTABLEG , LSOCIETE ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 4 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GP465'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W4                                                      
        ( CTABLEG2 , NSOCENTR , LDEPLIV , CMODDEL , WTABLEG , WACTIF ,          
        CPLAGES ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVCPL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W5                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WBMUTMUT , WBMUTDEM ,         
        WJMU242 , WJMU243 ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FLMUT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W6                                                      
        ( CTABLEG2 , NSOCENTR , LDEPLIV , CMODDEL , WTABLEG , WACTIF ,          
        QDELAI ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVDEL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W7                                                      
        ( CTABLEG2 , NSOCIETE , PROFIL , WTABLEG , ACTIF , NBJN , NBJM )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROST'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01W8                                                      
        ( CTABLEG2 , NSOCORIG , NSOCDEST , WTABLEG , SYSID ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SYSID'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WA                                                      
        ( CTABLEG2 , SOCIETE , LIEU , TYPE , WTABLEG , ACTIF , DATE ,           
        WIC00 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR ( WTABLEG , 10 , 1        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NASL0'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WB                                                      
        ( CTABLEG2 , CARACT , WTABLEG , TIERS ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM008'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WE                                                      
        ( CTABLEG2 , WVENTE , WTABLEG , WVVNTE ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'EGV07'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WK                                                      
        ( CTABLEG2 , TYPE33 , WTABLEG , SURPLUS , HISTM , HISTH ,               
        DINVENT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 2 ) , SUBSTR ( WTABLEG , 6 , 8 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'STOCK'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WL                                                      
        ( CTABLEG2 , PROG , SOCEXT , WTABLEG , ACTIF , PARAM , PARAM2 ,         
        PARAM3 ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 20 ) , SUBSTR ( WTABLEG , 22 , 20 ) , SUBSTR ( WTABLEG , 42         
        , 6 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARTE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WM                                                      
        ( CTABLEG2 , SOCDEP , SOCARR , WTABLEG , ACTIF , SOCREG , PLATEF        
        , SOCTRA ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 6 ) , SUBSTR ( WTABLEG , 8 , 6 ) , SUBSTR ( WTABLEG , 14 , 6        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DEPTR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WN                                                      
        ( CTABLEG2 , NSEQ , WTABLEG , CPROAFF , CMARQ , CFAM ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FLEXC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WO                                                      
        ( CTABLEG2 , CTYPTRT , WTABLEG , LTYPTRT , NPRIORIT ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FLTRT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WQ                                                      
        ( CTABLEG2 , NSOCLIEU , NSOCLIEM , WTABLEG , ACTIF , HEUREDES ,         
        CSATELIT , MSTOCK ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 4 ) , SUBSTR ( WTABLEG , 6 , 1 ) , SUBSTR ( WTABLEG , 7 , 40        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARTM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WR                                                      
        ( CTABLEG2 , CUSINE , WTABLEG , LUSINE , WACTIF , NENTCDE ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'USINE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WS                                                      
        ( CTABLEG2 , NSOCLIEU , TMVT , WTABLEG , ACTIF , NSSLIEUO ,             
        NSSLIEUD , COMMENT ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 9 ) , SUBSTR ( WTABLEG , 11 , 9 ) , SUBSTR ( WTABLEG , 20 ,         
        30 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARMV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WT                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , SLIEU , WTABLEG , ACTIF ,               
        SLIEUNCG , TYPTRT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR ( WTABLEG , 5 , 5         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARSS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WV                                                      
        ( CTABLEG2 , CFAM , CMARQ , WTABLEG , ACTIF , ENTREPOT ,                
        ENTREPO2 , ENTREPO3 ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 12 ) , SUBSTR ( WTABLEG , 14 , 12 ) , SUBSTR ( WTABLEG , 26         
        , 12 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FAMEX'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WW                                                      
        ( CTABLEG2 , CACID , WTABLEG , NSOCCICS , NSOCIETE , NLIEU ,            
        CFLGMAG ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 3 ) , SUBSTR ( WTABLEG , 10 , 5 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MFL10'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01WY                                                      
        ( CTABLEG2 , CFAM , WTABLEG , ACTIF , CNSERIE ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FASER'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01X0                                                      
        ( CTABLEG2 , CLIENT , WTABLEG , WACTIF , SOC , LIEU , CVENT ,           
        TYPECLI ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 3 ) , SUBSTR ( WTABLEG , 8 , 5 ) , SUBSTR (               
        WTABLEG , 13 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FGDA2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01X3                                                      
        ( CTABLEG2 , NSOCIETE , CFAM , WTABLEG , CMARKET1 , CMARKET2 )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INTCM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01X4                                                      
        ( CTABLEG2 , PREFT1 , WTABLEG , PBF , TOTLI , TOTFAC , TOTLIT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 4 ) , SUBSTR (               
        WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 5 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GCAEE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01X5                                                      
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , WMG , CMAGPR , WSL ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 6 ) , SUBSTR ( WTABLEG , 8 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDMAG'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01X7                                                      
        ( CTABLEG2 , NSAV , WTABLEG , FORMAT ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM10 '                                                      
;                                                                               
CREATE  VIEW M907.RVGA01X8                                                      
        ( CTABLEG2 , NSOC , NDEPOT , NSSLIEU , CLIEUTRT , WTABLEG ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , SUBSTR ( CTABLEG2 , 10 , 5 )        
        , WTABLEG                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INVHB'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01X9                                                      
        ( CTABLEG2 , CAPPLI , CMESS , WTABLEG , LIBEL , RESP , CGRA ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3         
        , 7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG        
        , 16 , 5 ) , SUBSTR ( WTABLEG , 21 , 2 )                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HELPD'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XA                                                      
        ( CTABLEG2 , CODMSG , WTABLEG , LMSG , CPGM ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDPAR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XB                                                      
        ( CTABLEG2 , CFAM , NAGREG , WTABLEG ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDAGH'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XC                                                      
        ( CTABLEG2 , NSOCP , NLIEUP , CFONC , NSOCR , NLIEUR , WTABLEG )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 3 )         
        , SUBSTR ( CTABLEG2 , 11 , 3 ) , WTABLEG                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDCNS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XE                                                      
        ( CTABLEG2 , CETAT , WTABLEG , LETAT , WEXH , WMULT , WNIVP ,           
        CONCAT ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 2 ) , SUBSTR (             
        WTABLEG , 25 , 10 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDETA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XF                                                      
        ( CTABLEG2 , CAPPRO , WTABLEG , WEP ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDWEP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XG                                                      
        ( CTABLEG2 , CSTAB , WTABLEG , LTAB , WLOC ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TG000'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XH                                                      
        ( CTABLEG2 , CSTG , NSEQ , WTABLEG , CZON , LZON , CONCAT ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG ,         
        7 , 15 ) , SUBSTR ( WTABLEG , 22 , 30 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TG001'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XI                                                      
        ( CTABLEG2 , NBRN , WTABLEG , BMIN , BMAX ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 15 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TG003'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XJ                                                      
        ( CTABLEG2 , NBRN , WTABLEG , BMIN , BMAX ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TG004'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XK                                                      
        ( CTABLEG2 , CSTG , CZON , NBRN , WTABLEG ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , SUBSTR ( CTABLEG2 , 12 , 4 ) , WTABLEG                            
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TG005'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XL                                                      
        ( CTABLEG2 , CGEST , WTABLEG , WTE , WBRBL , WOA , WEP , WDACEM         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR (               
        WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDGST'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XM                                                      
        ( CTABLEG2 , CFAM , WTABLEG , ACTIF , NBETIQ ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ITFAM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XN                                                      
        ( CTABLEG2 , NCODIC , WTABLEG , WACTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PAREP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XO                                                      
        ( CTABLEG2 , NSOCIETE , INVDATE , WTABLEG , FLAGACT , FLAGSTK )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INVDM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XP                                                      
        ( CTABLEG2 , NSOCLIEU , TYPMVT , WTABLEG , ACTIF , CUTILISA ,           
        DUTILISA , COMMENTA ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 10        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PAREX'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XQ                                                      
        ( CTABLEG2 , NSOCLIEU , CODIC , WTABLEG , ACTIF , PAF ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 12 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PAFMA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XR                                                      
        ( CTABLEG2 , SOCLIEU , CPROG , SSLLT , WTABLEG , ACTIF , CODEF ,        
        CODEM ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , SUBSTR ( CTABLEG2 , 12 , 4 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR ( WTABLEG , 4 ,         
        6 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARRE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XS                                                      
        ( CTABLEG2 , APPLI , WTABLEG , LIBELLE , NBMAJ , DELAI , PARAM1         
        , PARAM2 ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 6 ) , SUBSTR ( WTABLEG , 33 , 9 ) , SUBSTR (             
        WTABLEG , 42 , 9 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDCNA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XT                                                      
        ( CTABLEG2 , APPLI , TABLE , WTABLEG , LIBEL , CRTMSG , RCPMSG )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG         
        , 21 , 10 ) , SUBSTR ( WTABLEG , 31 , 10 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDCNM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XU                                                      
        ( CTABLEG2 , NOMPROG , WTABLEG , WACTIF , DEVISE , DEFFET ,             
        DEVANC ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 8 ) , SUBSTR ( WTABLEG , 13 , 3 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FEURO'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XV                                                      
        ( CTABLEG2 , CPGM , NSEQ , WTABLEG , WTYPD , DELAI , LIB ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG         
        , 2 , 5 ) , SUBSTR ( WTABLEG , 7 , 20 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'DELAS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XW                                                      
        ( CTABLEG2 , CPARAM , WTABLEG , WACTIF ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NMDPF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01XX                                                      
        ( CTABLEG2 , SOCLIEUE , SOCLIEUD , WTABLEG , ACTIF , LCOMMENT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 40 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARDI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y0                                                      
        ( CTABLEG2 , TYPAUX , WTABLEG , LTYPAX , WVND , WHS , WGHE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y1                                                      
        ( CTABLEG2 , CAUX , WTABLEG , LAUX , TYPAUX , CTYPL , NSOCLIEU )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 6 ) , SUBSTR (             
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 6 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXB'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y2                                                      
        ( CTABLEG2 , CAUX , WMAGPR , CMAGPR , WTABLEG , WAUTOR ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        1 ) , SUBSTR ( CTABLEG2 , 8 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXD'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y3                                                      
        ( CTABLEG2 , CTYPD , WTABLEG , LTYPD , BMIN , BMAX , CPGM ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 9 ) , SUBSTR (             
        WTABLEG , 30 , 9 ) , SUBSTR ( WTABLEG , 39 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLDOC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y4                                                      
        ( CTABLEG2 , CETAT , WTABLEG , LETAT , WVND , WTQ , WHS , WUT )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLETA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y5                                                      
        ( CTABLEG2 , COPAR , WOR , EVENT , NSEQ , WTABLEG , COPSL ,             
        WHOST ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 1 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 ,         
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPA'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y6                                                      
        ( CTABLEG2 , CAUX , COPAR , WTABLEG , COPSD , COPHS ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y7                                                      
        ( CTABLEG2 , COPAR , WTABLEG , LOPAR , WAUTR , WAUTA , WINT ,           
        DELAI ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 2 ) , SUBSTR (             
        WTABLEG , 23 , 2 ) , SUBSTR ( WTABLEG , 25 , 1 ) , SUBSTR (             
        WTABLEG , 26 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y8                                                      
        ( CTABLEG2 , COPSL , WMAGPR , CMAGPR , WTABLEG , WAUT , WARB )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01Y9                                                      
        ( CTABLEG2 , COPSL , CPRFID , WTABLEG , WAUT ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YA                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRRGL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YB                                                      
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRFAC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YD                                                      
        ( CTABLEG2 , NENTCDE , WABOREN , WTABLEG , NBL ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRNBL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YE                                                      
        ( CTABLEG2 , CPERIM , CZONLIV , WTABLEG ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVRL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YF                                                      
        ( CTABLEG2 , CPERIM , CTQUOTA , WTABLEG , CEQUIP , CMODDEL ,            
        WZONE ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 5 ) , SUBSTR ( WTABLEG , 11 , 1 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LIVRP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YG                                                      
        ( CTABLEG2 , CPRODEL , CPERIM , WTABLEG ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRDEP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YH                                                      
        ( CTABLEG2 , CPERIM , SMN , CTQUOTA , WTABLEG , CPRODEL , QQUOTA        
        , WILLIM ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 , 1        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRDEF'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YI                                                      
        ( CTABLEG2 , CAR , WTABLEG , LAR ) AS                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRARE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YJ                                                      
        ( CTABLEG2 , NSOCLIVR , NDEPOT , WTABLEG , WACTIF , WLIBELLE ,          
        QUOTA1 , QUOTA2 ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 18 ) , SUBSTR ( WTABLEG , 20 , 4 ) , SUBSTR ( WTABLEG , 24 ,        
        4 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QCNBL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YL                                                      
        ( CTABLEG2 , NSOCLIEU , TYPMVT , WTABLEG , ACTIF , NSSLIEUO ,           
        NSSLIEUD , COMMENT ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        9 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 9 ) , SUBSTR ( WTABLEG , 11 , 9 ) , SUBSTR ( WTABLEG , 20 ,         
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARMI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YM                                                      
        ( CTABLEG2 , NSOCIETE , NLIEU , CDINTT , WTABLEG , NGROUPE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QCTEC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YN                                                      
        ( CTABLEG2 , ENTCP , WTABLEG , ENTC1 , ENTC2 , ENTC3 , ENTCR )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PROPE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YQ                                                      
        ( CTABLEG2 , CTCARTE , CRAYON , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QCRAY'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YR                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , LIBSOC , TIERS , CPT ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NM009'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YS                                                      
        ( CTABLEG2 , CETAT , LIEU , WTABLEG , WFLAG ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                            
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MD400'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YT                                                      
        ( CTABLEG2 , COPER , WTABLEG , TOTALON ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MD401'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YU                                                      
        ( CTABLEG2 , SOCIETE , WTABLEG , TOPNUM , VGRNUM ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NMCPT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YV                                                      
        ( CTABLEG2 , CASSUR , WTABLEG , WACTIF , LASSUR , TXREMISE ,            
        DEFFET , ANCTXREM ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR (              
        WTABLEG , 27 , 4 ) , SUBSTR ( WTABLEG , 31 , 8 ) , SUBSTR (             
        WTABLEG , 39 , 4 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPASS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YW                                                      
        ( CTABLEG2 , CINTER , WTABLEG , WACTIF , LINTER , NATURE ,              
        WDERFACT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR (              
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPINT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YX                                                      
        ( CTABLEG2 , CASSUR , CINTER , WTABLEG , WACTIF ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPAUT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YY                                                      
        ( CTABLEG2 , CACID , NSOCCOMP , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPADR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01YZ                                                      
        ( CTABLEG2 , CFONC , WMAGPR , CMAGPR , WTABLEG , WREPONSE ,             
        WCOMPLEM ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 ) , SUBSTR ( WTABLEG , 11 , 40 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDPPM'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZA                                                      
        ( CTABLEG2 , COPAR , WTABLEG , CTYPDR , WSENS ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZB                                                      
        ( CTABLEG2 , COPSL , WOR , WMAGPR , CMAGPR , NSEQ , WTABLEG ,           
        CEMPL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 6 )         
        , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1         
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZC                                                      
        ( CTABLEG2 , CPGM , CTRT , WTABLEG , LTRT , COPSL ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG        
        , 21 , 5 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZD                                                      
        ( CTABLEG2 , CRSL , WTABLEG , LRSL , INDETAT , INDEMPL , CEMPL ,        
        NSEQ ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 4 ) , SUBSTR (             
        WTABLEG , 25 , 6 ) , SUBSTR ( WTABLEG , 31 , 5 ) , SUBSTR (             
        WTABLEG , 36 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLRSL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZE                                                      
        ( CTABLEG2 , CTYPRS , WTABLEG , LTYPRS , WVENTE , WATAFF ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLTRS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZF                                                      
        ( CTABLEG2 , TYPUTI , WTABLEG , LIBEL , COPSL ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 5 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLUTI'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZG                                                      
        ( CTABLEG2 , CTLIEU , WTABLEG , CAUX ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 6 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZH                                                      
        ( CTABLEG2 , CASSUR , CCTRT , WTABLEG , WACTIF , LCTRT , WTEX )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 25 ) , SUBSTR ( WTABLEG , 27 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPCTR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZI                                                      
        ( CTABLEG2 , CASSUR , CZONE , DEFFET , WTABLEG , WACTIF , LZONE         
        , MTLIVR ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR ( WTABLEG , 27 ,         
        6 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPZON'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZJ                                                      
        ( CTABLEG2 , CNATURE , WTABLEG , WACTIF , LNATURE , WTEX , WCA9         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR (              
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPNAT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZK                                                      
        ( CTABLEG2 , CASSUR , CNATURE , WTABLEG , WACTIF , WREMISE ,            
        MFORFAIT , DTFINVAL ) AS                                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 6 ) , SUBSTR ( WTABLEG , 9 , 8         
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPAUN'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZL                                                      
        ( CTABLEG2 , CDIAGNO , WTABLEG , WACTIF , LDIAGNO , WREPAR ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 25 ) , SUBSTR (              
        WTABLEG , 27 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPSAV'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZN                                                      
        ( CTABLEG2 , CPGM , NCOL , WTABLEG , CRSL , TQTE , MSGENT ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG         
        , 6 , 1 ) , SUBSTR ( WTABLEG , 7 , 7 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLCOL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZO                                                      
        ( CTABLEG2 , COPSL , WMAGPR , CMAGPR , WTABLEG , WCONSO ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLOPC'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZP                                                      
        ( CTABLEG2 , SOCZP , WTABLEG , CPARAM1 , CPARAM2 , CPARAM3 ,            
        CPARAM4 ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 10 ) , SUBSTR (            
        WTABLEG , 21 , 10 ) , SUBSTR ( WTABLEG , 31 , 20 )                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ZPFIL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZQ                                                      
        ( CTABLEG2 , SOCIETE , MONTANT , WTABLEG , LETTRE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'COPR2'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZR                                                      
        ( CTABLEG2 , NSOC , NLIEU , CAPPLI , WTABLEG , QMANAGER ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MQMGR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZU                                                      
        ( CTABLEG2 , SOCORIG , SOCDEST , WTABLEG , WACTIF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTFUS'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZV                                                      
        ( CTABLEG2 , NSOC , NETAB , COMPTE , WTABLEG , TRANSPO ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 6 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FTFUT'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZW                                                      
        ( CTABLEG2 , CCOLOR , WTABLEG , LCOLOR ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'COLOR'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZX                                                      
        ( CTABLEG2 , NDEPART , WTABLEG , WACTIF ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'FPDEP'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZY                                                      
        ( CTABLEG2 , CRSL , NSEQ , WTABLEG , CEMPL ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLRSE'                                                      
;                                                                               
CREATE  VIEW M907.RVGA01ZZ                                                      
        ( CTABLEG2 , SOCZP , TRANS , CTRL , WTABLEG , WFLAG , WPARAM ,          
        LIBELLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        6 ) , SUBSTR ( CTABLEG2 , 12 , 4 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 10 ) , SUBSTR ( WTABLEG , 12         
        , 30 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'XCTRL'                                                      
;                                                                               
CREATE  VIEW M907.RVGA1005                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA , CCUMCA           
    FROM  M907.RTGA10FP                                                         
;                                                                               
CREATE  VIEW M907.RVGA1006                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA , CCUMCA ,         
        WRETRO , NZPPREST                                                       
    FROM  M907.RTGA10FP                                                         
;                                                                               
CREATE  VIEW M907.RVGA1007                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA , CCUMCA ,         
        NZPPREST , WRETRO , TIERSGCT                                            
    FROM  M907.RTGA10FP                                                         
;                                                                               
CREATE  VIEW M907.RVGA1008                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA , CCUMCA ,         
        NZPPREST , WRETRO , TIERSGCT , CODEBDF                                  
    FROM  M907.RTGA10FP                                                         
;                                                                               
CREATE  VIEW M907.RVGA1060                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA , CCUMCA ,         
        NZPPREST , WRETRO                                                       
    FROM  M907.RTGA10FP                                                         
;                                                                               
CREATE  VIEW M907.RVGA1098                                                      
        AS                                                                      
SELECT  NSOCIETE , NLIEU , CTYPLIEU , LLIEU , DOUV , DFERM , NZONPRIX ,         
        CGRPMAG , CSTATEXPO , CACID , WDACEM , CTYPFACT , CTYPCONTR ,           
        QTAUXESCPT , LCONDRGLT , DSYST , LADRLIEU1 , LADRLIEU2 ,                
        LADRLIEU3 , CPOSTAL , LCOMMUNE , CTYPSOC , NSOCCOMPT ,                  
        NLIEUCOMPT , NSOCFACT , NLIEUFACT , CGRPMUT , NSOCVALO ,                
        NLIEUVALO , CTYPLIEUVALO , NSOCGRP , NORDRE , CACID1 , CACID2 ,         
        CACID3 , CACID4 , CACID5 , CACID6 , CFRAIS , SECTANA , CCUMCA ,         
        NZPPREST , WRETRO , TIERSGCT                                            
    FROM  M907.RTGA10FP                                                         
;                                                                               
CREATE  VIEW M907.RVGA9900                                                      
        AS                                                                      
SELECT  ALL CNOMPGRM , NSEQERR , CERRUT , LIBERR                                
    FROM  M907.RTGA99                                                           
;                                                                               
CREATE  VIEW M907.RVGA9901                                                      
        AS                                                                      
SELECT  ALL CNOMPGRM , NSEQERR , CERRUT , LIBERR , CLANG                        
    FROM  M907.RTGA99                                                           
;                                                                               
CREATE  VIEW M907.RVGAMGD                                                       
        ( CTABLEG2 , CTYPGAR , WTABLEG , LTYPGAR ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GAMGD'                                                      
;                                                                               
CREATE  VIEW M907.RVGCCND                                                       
        ( CTABLEG2 , CTYPCND , WTABLEG , LIBELLE , WINCCOND , CRGLT ,           
        CFACT , WMTVENTE ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 40 ) , SUBSTR ( WTABLEG , 41 , 1 ) , SUBSTR (             
        WTABLEG , 42 , 3 ) , SUBSTR ( WTABLEG , 45 , 3 ) , SUBSTR (             
        WTABLEG , 48 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GCCND'                                                      
;                                                                               
CREATE  VIEW M907.RVGCOPE                                                       
        ( CTABLEG2 , CMARQ , NENTCDE , WTABLEG , WENTCDE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GCOPE'                                                      
;                                                                               
CREATE  VIEW M907.RVGCTXP                                                       
        ( CTABLEG2 , CTYPTXP , WTABLEG , WACTIF , LIBELLE , WGESTION )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GCTXP'                                                      
;                                                                               
CREATE  VIEW M907.RVGLORI                                                       
        ( CTABLEG2 , CORIG , WTABLEG , WACTIF , LIBELLE , WDATA ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 30 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GLORI'                                                      
;                                                                               
CREATE  VIEW M907.RVGLQUA                                                       
        ( CTABLEG2 , CQUALIF , WTABLEG , WACTIF , LIBELLE , WDATA ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 30 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GLQUA'                                                      
;                                                                               
CREATE  VIEW M907.RVGLTYP                                                       
        ( CTABLEG2 , CTYPLIEN , WTABLEG , WACTIF , LIBELLE , WDATA ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 30 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GLTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVGPENU                                                       
        ( CTABLEG2 , CGROUP , WTABLEG , SOCREF , NSEQ ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 2 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GPENU'                                                      
;                                                                               
CREATE  VIEW M907.RVGQ0100                                                      
        AS                                                                      
SELECT  ALL CINSEE , NSOC , CURBAIN , CDOUBLE , QPRINCI , QSECOND ,             
        QVACANT , QEVOLUT , QRESACT , QCOEFF , WCONTRA , CELEMEN , DSYST        
    FROM  M907.RTGQ01                                                           
;                                                                               
CREATE  VIEW M907.RVGQ0601                                                      
        AS                                                                      
SELECT  ALL CTYPE , CINSEE , WPARCOM , LCOMUNE , CPOSTAL , WPARBUR ,            
        LBUREAU , DSYST                                                         
    FROM  M907.RTGQ06                                                           
;                                                                               
CREATE  VIEW M907.RVGQ9100                                                      
        AS                                                                      
SELECT  ALL CINSEE , NSOC , CURBAIN , CDOUBLE , QPRINCI , QSECOND ,             
        QVACANT , QEVOLUT , QRESACT , QCOEFF , WCONTRA , CELEMEN , DSYST        
    FROM  M907.RTGQ91                                                           
;                                                                               
CREATE  VIEW M907.RVGQ9600                                                      
        AS                                                                      
SELECT  ALL CTYPE , CINSEE , WPARCOM , LCOMUNE , CPOSTAL , WPARBUR ,            
        LBUREAU , DSYST                                                         
    FROM  M907.RTGQ96                                                           
;                                                                               
CREATE  VIEW M907.RVGRCRD                                                       
        ( CTABLEG2 , CBAREME , NSEQ , WTABLEG , DEFFET ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GRCRD'                                                      
;                                                                               
CREATE  VIEW M907.RVGSELA                                                       
        ( CTABLEG2 , SOCENTRE , DEPOT , CSELART , WTABLEG , LSELART ,           
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 4 ) , SUBSTR ( WTABLEG , 5 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GSELA'                                                      
;                                                                               
CREATE  VIEW M907.RVGVCAD                                                       
        ( CTABLEG2 , CADRE , WTABLEG , TCADRE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVCAD'                                                      
;                                                                               
CREATE  VIEW M907.RVGVLIV                                                       
        ( CTABLEG2 , MDEL , JSEM , DATE , WTABLEG , QDECMINI , QDECMAXI         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        2 ) , SUBSTR ( CTABLEG2 , 6 , 4 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 2 ) , SUBSTR ( WTABLEG , 3 , 3 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'GVLIV'                                                      
;                                                                               
CREATE  VIEW M907.RVHDMOB                                                       
        ( CTABLEG2 , CTYPOPI , CNATOPI , CRITANA , WTABLEG , LIBELLE ,          
        CTYPOPO , CNATOPO ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 5 ) , SUBSTR ( WTABLEG , 16        
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HDMOB'                                                      
;                                                                               
CREATE  VIEW M907.RVHEOPC                                                       
        ( CTABLEG2 , COPCO , WTABLEG , LIEUHED , CTRAIT , CTYPHS ,              
        CLIEUHT , WFLAG ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 5 ) , SUBSTR (               
        WTABLEG , 9 , 5 ) , SUBSTR ( WTABLEG , 14 , 5 ) , SUBSTR (              
        WTABLEG , 19 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HEOPC'                                                      
;                                                                               
CREATE  VIEW M907.RVHIERA                                                       
        ( CTABLEG2 , IDEPT , ISDEPT , ICLAS , ISCLAS , WTABLEG , LCHAMP         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 3 ) , SUBSTR ( CTABLEG2 , 10 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HIERA'                                                      
;                                                                               
CREATE  VIEW M907.RVHTRT1                                                       
        ( CTABLEG2 , CAPPLI , WTABLEG , LAPPLI ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HTRT1'                                                      
;                                                                               
CREATE  VIEW M907.RVHTRT2                                                       
        ( CTABLEG2 , CAPPLI , CTRANSF , WTABLEG , LTRANSF ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'HTRT2'                                                      
;                                                                               
CREATE  VIEW M907.RVICCOM                                                       
        ( CTABLEG2 , CODE , WANNEE , WMOIS , WTABLEG , CHAINE , COMPTA )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        4 ) , SUBSTR ( CTABLEG2 , 13 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ICCOM'                                                      
;                                                                               
CREATE  VIEW M907.RVIECAP                                                       
        ( CTABLEG2 , NSOCDEPO , NDEPOT , CTRACK , WTABLEG , LTRACK ,            
        QCAPCONT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 20 ) , SUBSTR ( WTABLEG , 21 , 3 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IECAP'                                                      
;                                                                               
CREATE  VIEW M907.RVIF0000                                                      
        AS                                                                      
SELECT  NSOCCOMPT , NETAB , CTYPCTA , CMODIMP , DSYST                           
    FROM  M907.RTIF00                                                           
;                                                                               
CREATE  VIEW M907.RVIF0100                                                      
        AS                                                                      
SELECT  NSOCCOMPT , NETAB , CORGANISME , WACTIF , DSYST                         
    FROM  M907.RTIF01                                                           
;                                                                               
CREATE  VIEW M907.RVIF0200                                                      
        AS                                                                      
SELECT  CORGANISME , NIDENT , NSOCIETE , NLIEU , CBANQUE , DSYST                
    FROM  m907.RTIF02                                                           
;                                                                               
CREATE  VIEW M907.RVIF0300                                                      
        AS                                                                      
SELECT  CMODPAI , CTYPCTA , CTYPMONT , CMODCTA , NAUX , WLETTRAGE ,             
        DSYST                                                                   
    FROM  m907.RTIF03                                                           
;                                                                               
CREATE  VIEW M907.RVIF0400                                                      
        AS                                                                      
SELECT  CTYPCTA , CTYPMONT , NOECS , CMOTCLE1 , CMOTCLE2 , CMOTCLE3 ,           
        DSYST                                                                   
    FROM  m907.RTIF04                                                           
;                                                                               
CREATE  VIEW M907.RVIF0500                                                      
        AS                                                                      
SELECT  CMODPAI , CTYPMONT , NORDRE , CLIBFIXE , CMOTCLE , CMASQUE ,            
        WSEPFIN , DSYST                                                         
    FROM  m907.RTIF05                                                           
;                                                                               
CREATE  VIEW M907.RVIF0600                                                      
        AS                                                                      
SELECT  NEXER , NPER , DDEBUT , DFIN , DLIMITE , DSYST                          
    FROM  m907.RTIF06                                                           
;                                                                               
CREATE  VIEW M907.RVIF0700                                                      
        AS                                                                      
SELECT  CMODPAI , CTYPCTA , CTYPMONT , NORDRE , CMOTCLE , CMASQUE ,             
        DSYST                                                                   
    FROM  m907.RTIF07                                                           
;                                                                               
CREATE  VIEW M907.RVIF0800                                                      
        AS                                                                      
SELECT  CMODPAI , CTYPCTA , CTYPMONT , NORDRE , CLIBFIXE , CMOTCLE ,            
        CMASQUE , DSYST                                                         
    FROM  m907.RTIF08                                                           
;                                                                               
CREATE  VIEW M907.RVIF1000                                                      
        AS                                                                      
SELECT  CORGANISME , DRECEPT , HRECEPT , DDTRAIT , NBRECUS , NBREJETS ,         
        NBANOS , DSYST                                                          
    FROM  m907.RTIF10                                                           
;                                                                               
CREATE  VIEW M907.RVIF1001                                                      
        AS                                                                      
SELECT  CORGANISME , DRECEPT , HRECEPT , DDTRAIT , NBRECUS , NBREJETS ,         
        NBANOS , DFICORG , NFICORG , DSYST                                      
    FROM  M907.RTIF10                                                           
;                                                                               
CREATE  VIEW M907.RVIF1100                                                      
        AS                                                                      
SELECT  corganisme , drecept , hrecept , nident , cmodpai , dfinance ,          
        doper , dremise , nremise , codano , mtbrut , mtcom , mtnet ,           
        DSYST                                                                   
    FROM  m907.RTIF11                                                           
;                                                                               
CREATE  VIEW M907.RVIF2000                                                      
        AS                                                                      
SELECT  corganisme , cmodpai , nident , dfinance , dtrait , dremise ,           
        nremise , nbmvts , njrn , DSYST                                         
    FROM  m907.RTIF20                                                           
;                                                                               
CREATE  VIEW M907.RVIFEXT                                                       
        ( CTABLEG2 , BIN , WTABLEG , FLAG , TCARTE ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 12 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 15 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IFEXT'                                                      
;                                                                               
CREATE  VIEW M907.RVIMPME                                                       
        ( CTABLEG2 , SOCIETE , LIEU , NOMLOG , WTABLEG , NOMPHY ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 9 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IMPME'                                                      
;                                                                               
CREATE  VIEW M907.RVIMPMF                                                       
        ( CTABLEG2 , NSOC , NLIEU , CFONC , NSEQ , WTABLEG , CIMP ,             
        CIMPH ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 7 ) , SUBSTR ( CTABLEG2 , 14 , 2 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11         
        , 4 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IMPMF'                                                      
;                                                                               
CREATE  VIEW M907.RVIMPMG                                                       
        ( CTABLEG2 , CFONC , WTABLEG , OUTQ ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IMPMG'                                                      
;                                                                               
CREATE  VIEW M907.RVINCRD                                                       
        ( CTABLEG2 , TYPE , VALEUR , POSIT , WTABLEG , DATA ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , SUBSTR ( CTABLEG2 , 12 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 50 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INCRD'                                                      
;                                                                               
CREATE  VIEW M907.RVINOTR                                                       
        ( CTABLEG2 , CGESTION , CTRTY , WTABLEG , VISIBLE , OBLIG ,             
        SAISIE , VALDFAUT ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 5 ) , SUBSTR ( WTABLEG , 8 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INOTR'                                                      
;                                                                               
CREATE  VIEW M907.RVINVLM                                                       
        ( CTABLEG2 , NSOCIETE , NDEPOT , DINVENV , WTABLEG , DDEBUT ,           
        DFIN ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'INVLM'                                                      
;                                                                               
CREATE  VIEW M907.RVIRPAR                                                       
        ( CTABLEG2 , CODE , WANNEE , WMOIS , WTABLEG , REEL ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        4 ) , SUBSTR ( CTABLEG2 , 11 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'IRPAR'                                                      
;                                                                               
CREATE  VIEW M907.RVKAPVE                                                       
        ( CTABLEG2 , RESERV , CLIENT , WTABLEG , CODIC1 , CODIC_2 ,             
        CODIC_3 , CODIC_4 ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 9 ) , SUBSTR ( WTABLEG ,         
        10 , 8 ) , SUBSTR ( WTABLEG , 18 , 8 ) , SUBSTR ( WTABLEG , 26 ,        
        8 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'KAPVE'                                                      
;                                                                               
CREATE  VIEW M907.RVLGTPM                                                       
        ( CTABLEG2 , CFAM , CMARKET , CVMARKET , WTABLEG , QTEMPS ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 3 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LGTPM'                                                      
;                                                                               
CREATE  VIEW M907.RVLI0000                                                      
        AS                                                                      
SELECT  ALL NSOCIETE , NLIEU , CPARAM , LVPARAM , DSYST                         
    FROM  M907.RTLI00                                                           
;                                                                               
CREATE  VIEW M907.RVLIBCO                                                       
        ( CTABLEG2 , CETAT , CEMP , NSEQ , WTABLEG , COMMENT1 , WACTIF )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        1 ) , SUBSTR ( CTABLEG2 , 9 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 55 ) , SUBSTR ( WTABLEG , 56 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LIBCO'                                                      
;                                                                               
CREATE  VIEW M907.RVLIBL                                                        
        ( CTABLEG2 , TYLBL , SOC , LIEU , CHRONO , WTABLEG , LIBELLE )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , SUBSTR ( CTABLEG2 , 12 , 4 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 51 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LIBL '                                                      
;                                                                               
CREATE  VIEW M907.RVLICRD                                                       
        ( CTABLEG2 , CBAREME , NSOC , NLIEU , NSEQ , WTABLEG , DEFFET )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        3 ) , SUBSTR ( CTABLEG2 , 11 , 3 ) , SUBSTR ( CTABLEG2 , 14 , 2         
        ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 )                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LICRD'                                                      
;                                                                               
CREATE  VIEW M907.RVLIGPR                                                       
        ( CTABLEG2 , CLIGPROD , WTABLEG , LLIGPROD ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LIGPR'                                                      
;                                                                               
CREATE  VIEW M907.RVLISAV                                                       
        ( CTABLEG2 , SAVTYP , WTABLEG , LIBSAVT , TRISEQ ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 17 ) , SUBSTR ( WTABLEG , 18 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LISAV'                                                      
;                                                                               
CREATE  VIEW M907.RVLITPM                                                       
        ( CTABLEG2 , CFAM , CMARKET , CVMARKET , WTABLEG , MONOQTPS ,           
        STDQTPS ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LITPM'                                                      
;                                                                               
CREATE  VIEW M907.RVLITPS                                                       
        ( CTABLEG2 , SOC , CTEMPS , WTABLEG , WACTIF , MONOQTPS ,               
        STDQTPS ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,        
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 3 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LITPS'                                                      
;                                                                               
CREATE  VIEW M907.RVLLDAP                                                       
        ( CTABLEG2 , NSOCHR , NLIEUHR , WTABLEG , WTRT , NSOC , NLIEU )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 3 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LLDAP'                                                      
;                                                                               
CREATE  VIEW M907.RVLPA2I                                                       
        ( CTABLEG2 , CPVENTE , NVERSION , CTYPE , NSEQ , WTABLEG ,              
        LCOMMENT ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 2 )         
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 55 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LPA2I'                                                      
;                                                                               
CREATE  VIEW M907.RVLSEXC                                                       
        ( CTABLEG2 , NSOCIETE , NLIEU , CFAM , WTABLEG , EXCLU ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'LSEXC'                                                      
;                                                                               
CREATE  VIEW M907.RVMAGAS                                                       
        ( CTABLEG2 , SYSN , NSOC , NLIEU , WTABLEG , WMAGP ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        3 ) , SUBSTR ( CTABLEG2 , 12 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGAS'                                                      
;                                                                               
CREATE  VIEW M907.RVMAGPX                                                       
        ( CTABLEG2 , NMAG , WTABLEG , V , J , G , E , LIBELLE ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 1 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR (               
        WTABLEG , 5 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MAGPX'                                                      
;                                                                               
CREATE  VIEW M907.RVMATSP                                                       
        ( CTABLEG2 , MATHR , WTABLEG , PARAM ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 60 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MATSP'                                                      
;                                                                               
CREATE  VIEW M907.RVMDACC                                                       
        ( CTABLEG2 , SOC , MAG , CFAM , WTABLEG , CMODEL , WACTIF ,             
        VALEUR ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDACC'                                                      
;                                                                               
CREATE  VIEW M907.RVMDICC                                                       
        ( CTABLEG2 , CTICS , CNATOP , WTABLEG , WDAC , WACC , WTLEL ,           
        WBRBL ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 3 ) , SUBSTR ( WTABLEG , 7 , 3 ) , SUBSTR ( WTABLEG , 10 , 3        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDICC'                                                      
;                                                                               
CREATE  VIEW M907.RVMDICN                                                       
        ( CTABLEG2 , CFONC , LVALPGM , WTABLEG , LVALUTI ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                            
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDICN'                                                      
;                                                                               
CREATE  VIEW M907.RVMDICT                                                       
        ( CTABLEG2 , CPROG , COPER , WTABLEG , CTICS , CTYPORIG ,               
        CTYPDEST ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,        
        6 , 5 ) , SUBSTR ( WTABLEG , 11 , 5 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDICT'                                                      
;                                                                               
CREATE  VIEW M907.RVMDTLI                                                       
        ( CTABLEG2 , CPROTOUR , NSOC , NLIEU , WTABLEG , WACTIF ,               
        WREPONSE , WLIBREP ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR ( WTABLEG , 3 , 20        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MDTLI'                                                      
;                                                                               
CREATE  VIEW M907.RVMGDEL                                                       
        ( CTABLEG2 , NSOCIETE , NLIEU , JOUR , WTABLEG , WACTIF , DELAI         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MGDEL'                                                      
;                                                                               
CREATE  VIEW M907.RVMGNMD                                                       
        ( CTABLEG2 , NSOCMG , DATE , CPROG , WTABLEG , SOCMAG , LCOMMENT        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , SUBSTR ( CTABLEG2 , 10 , 6 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 30 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MGNMD'                                                      
;                                                                               
CREATE  VIEW M907.RVMMCIV                                                       
        ( CTABLEG2 , TITRE , WTABLEG , WFLAG , COMMENT ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 10 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MMCIV'                                                      
;                                                                               
CREATE  VIEW M907.RVMMCP                                                        
        ( CTABLEG2 , NLIGNE , WTABLEG , CPDEB , CPFIN , WFLAG , COMMENT         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR (               
        WTABLEG , 11 , 1 ) , SUBSTR ( WTABLEG , 12 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MMCP '                                                      
;                                                                               
CREATE  VIEW M907.RVMMFAM                                                       
        ( CTABLEG2 , CFAM , CODE , WTABLEG , TYPEX , WFLAG , COMMENT )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 4 ) , SUBSTR ( WTABLEG ,         
        5 , 1 ) , SUBSTR ( WTABLEG , 6 , 10 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MMFAM'                                                      
;                                                                               
CREATE  VIEW M907.RVMMFAR                                                       
        ( CTABLEG2 , CFAM , CTARIF , WTABLEG , WFLAG , RATIO , COMMENT )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 10 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MMFAR'                                                      
;                                                                               
CREATE  VIEW M907.RVMMMAG                                                       
        ( CTABLEG2 , SOCIETE , LIEU , WTABLEG , WFLAG , COMMENT ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 10 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MMMAG'                                                      
;                                                                               
CREATE  VIEW M907.RVMMVEN                                                       
        ( CTABLEG2 , CVENDEUR , WTABLEG , WACTIF , COMMENT ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 10 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MMVEN'                                                      
;                                                                               
CREATE  VIEW M907.RVMQ110                                                       
        ( CTABLEG2 , TYPEMSG , WTABLEG , LIBELLE , CPGM ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MQ110'                                                      
;                                                                               
CREATE  VIEW M907.RVMUCEN                                                       
        ( CTABLEG2 , HDAR , TYPE , SELDAR , IND , WTABLEG , SELDAC ,            
        HDAC ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        5 ) , SUBSTR ( CTABLEG2 , 10 , 5 ) , SUBSTR ( CTABLEG2 , 15 , 1         
        ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6         
        , 4 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MUCEN'                                                      
;                                                                               
CREATE  VIEW M907.RVMUTLM                                                       
        ( CTABLEG2 , NSOCDEP , NSOCLIE , NSEQ , WTABLEG , CSELART ,             
        HCHARGT , WFLAG ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 ,        
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MUTLM'                                                      
;                                                                               
CREATE  VIEW M907.RVMUTNR                                                       
        ( CTABLEG2 , NSOCIETE , NLIEU , CSELART , NSEQ , WTABLEG ,              
        HEURELIM ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , SUBSTR ( CTABLEG2 , 12 , 2 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MUTNR'                                                      
;                                                                               
CREATE  VIEW M907.RVMUTST                                                       
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , ACTIF , NLIEUO , COPER        
        , COMMENT ) AS                                                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 10 ) , SUBSTR ( WTABLEG , 15 ,         
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MUTST'                                                      
;                                                                               
CREATE  VIEW M907.RVMUTTR                                                       
        ( CTABLEG2 , SOCDEPO , SOCDEPD , WTABLEG , ACTIF , TRANSIT ,            
        WTYPPRET ) AS                                                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 1 ) , SUBSTR ( WTABLEG , 3 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MUTTR'                                                      
;                                                                               
CREATE  VIEW M907.RVMVTLM                                                       
        ( CTABLEG2 , NSOCDEP , CNATMVT , WTABLEG , ACTIF , NSSLIEUO ,           
        NSSLIEUD , COMMENT ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        9 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 8 ) , SUBSTR ( WTABLEG , 10 , 8 ) , SUBSTR ( WTABLEG , 18 ,         
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'MVTLM'                                                      
;                                                                               
CREATE  VIEW M907.RVNCGPT                                                       
        ( CTABLEG2 , TRAN , WTABLEG , LTRAN ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 40 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NCGPT'                                                      
;                                                                               
CREATE  VIEW M907.RVNCGPU                                                       
        ( CTABLEG2 , ZUSER , NIV1 , NIV2 , NIV3 , WTABLEG , LIBELLE ,           
        TRAN ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        2 ) , SUBSTR ( CTABLEG2 , 10 , 2 ) , SUBSTR ( CTABLEG2 , 12 , 2         
        ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 40 ) , SUBSTR ( WTABLEG ,          
        41 , 4 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NCGPU'                                                      
;                                                                               
CREATE  VIEW M907.RVNMIMP                                                       
        ( CTABLEG2 , NLIEU , CIMP , WTABLEG , LIBELLE ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 )                            
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NMIMP'                                                      
;                                                                               
CREATE  VIEW M907.RVNRMPM                                                       
        ( CTABLEG2 , NSOCIETE , NLIEU , WTABLEG , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NRMPM'                                                      
;                                                                               
CREATE  VIEW M907.RVNV001                                                       
        ( CTABLEG2 , RACLEG , WASHOST , WTABLEG , CODEMQ , PGMTRT ,             
        ROUTEUR ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG         
        , 4 , 10 ) , SUBSTR ( WTABLEG , 14 , 40 )                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'NV001'                                                      
;                                                                               
CREATE  VIEW M907.RVOBSOL                                                       
        ( CTABLEG2 , CDESC , CVDESC , WTABLEG , FLAG , OPTION ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 10 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'OBSOL'                                                      
;                                                                               
CREATE  VIEW M907.RVOPGSM                                                       
        ( CTABLEG2 , NOMOPE , NUMGSM , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'OPGSM'                                                      
;                                                                               
CREATE  VIEW M907.RVPAINT                                                       
        ( CTABLEG2 , TYPVTE , CMOPAI , WTABLEG , DATEDEB , DATEFIN ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 8 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PAINT'                                                      
;                                                                               
CREATE  VIEW M907.RVPARA2                                                       
        ( CTABLEG2 , SOC , DEPOT , CPARAM , CFAM , WTABLEG , WFLAG ,            
        LVPARAM ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 4 ) , SUBSTR ( CTABLEG2 , 11 , 5 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARA2'                                                      
;                                                                               
CREATE  VIEW M907.RVPARAM                                                       
        ( CTABLEG2 , CSOC , CPARAM , WTABLEG , WFLAG , LCOMMENT , WFLAG2        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 15 ) , SUBSTR ( WTABLEG , 17 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARAM'                                                      
;                                                                               
CREATE  VIEW M907.RVPARME                                                       
        ( CTABLEG2 , CPARAM , NSOCLIEU , CFAM , WTABLEG , CETAT , WFLAG         
        , LCOMMENT ) AS                                                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        6 ) , SUBSTR ( CTABLEG2 , 10 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 6 ) , SUBSTR ( WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 ,         
        40 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PARME'                                                      
;                                                                               
CREATE  VIEW M907.RVPBREC                                                       
        ( CTABLEG2 , NLIEU , WTABLEG , EDQTE , SPWORD ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PBREC'                                                      
;                                                                               
CREATE  VIEW M907.RVPCCOP                                                       
        ( CTABLEG2 , COPER , WTABLEG , LOPER , WCRE , WSENS , WRECONCI ,        
        COPERA ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 25 ) , SUBSTR ( WTABLEG , 26 , 1 ) , SUBSTR (             
        WTABLEG , 27 , 1 ) , SUBSTR ( WTABLEG , 28 , 1 ) , SUBSTR (             
        WTABLEG , 29 , 6 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PCCOP'                                                      
;                                                                               
CREATE  VIEW M907.RVPCGCT                                                       
        ( CTABLEG2 , COMPTE , DDEB , NSEQ , WTABLEG , PRTYP , CPREST ,          
        LIBELLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PCGCT'                                                      
;                                                                               
CREATE  VIEW M907.RVPCSAP                                                       
        ( CTABLEG2 , COMPTE , DDEB , NSEQ , WTABLEG , PRTYP , CPREST ,          
        LIBELLE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        6 ) , SUBSTR ( CTABLEG2 , 15 , 1 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 ,        
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PCSAP'                                                      
;                                                                               
CREATE  VIEW M907.RVPCTRA                                                       
        ( CTABLEG2 , OPTYPE , TYPE , CTYPE , WTABLEG , WFLAG , NREGLE )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , SUBSTR ( CTABLEG2 , 5 ,        
        3 ) , SUBSTR ( CTABLEG2 , 8 , 7 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 ) , SUBSTR ( WTABLEG , 2 , 10 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PCTRA'                                                      
;                                                                               
CREATE  VIEW M907.RVPCTYP                                                       
        ( CTABLEG2 , CTYPE , WTABLEG , WACTIF , TYPE , LIBELLE , TXTVA )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 49 ) , SUBSTR ( WTABLEG , 54 , 4 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PCTYP'                                                      
;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVPE0100                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOCIETE , NRUBPAIE , CCONTRAT , CRUBRIQUE , WSIGNE , DSYST         
--MW DZ-400     FROM  M907.RTPE01                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVPE0200                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOCIETE , NRUBPAIE , NCPTGESTION , NCPTBILAN , WSSCOMPTE ,         
--MW DZ-400         WSIGNE , DSYST                                                          
--MW DZ-400     FROM  M907.RTPE02                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVPE0300                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOCIETE , NSECTPAIE , NSSCOMPTE , DSYST                            
--MW DZ-400     FROM  M907.RTPE03                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVPE0400                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOCIETE , NRUBPAIE , CCONTRAT , CHORAIRE , CRUBRIQUE ,             
--MW DZ-400         WSIGNE , DSYST , CABSENCE                                               
--MW DZ-400     FROM  M907.RTPE04                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVPE0500                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL CENREG , NSOCIETE , DMOIS , DANNEE , NSECTION , NSSSECTION ,        
--MW DZ-400         CHORAIRE , CCONTRAT , NRUBPAIE , CORGABS , PMONTANT , CANOMALIE         
--MW DZ-400         , DSYST                                                                 
--MW DZ-400     FROM  M907.RTPE05                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVPE0600                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOCIETE , TYPECPT , NCPT , CRUBRIQUE , NSECTION ,                  
--MW DZ-400         NSSSECTION , LIBCPTPE , WSOLDE , NJRN , DSYST                           
--MW DZ-400     FROM  M907.RTPE06                                                           
--MW DZ-400 ;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVPE0700                                                      
--MW DZ-400         AS                                                                      
--MW DZ-400 SELECT  ALL NSOCIETE , NSECTIONPAIE , NSSSECTIONPAIE , NSECTIONCONSO ,          
--MW DZ-400         NSSSECTIONCONSO , DSYST                                                 
--MW DZ-400     FROM  M907.RTPE07                                                           
--MW DZ-400 ;                                                                               
CREATE  VIEW M907.RVPETI2                                                       
        ( CTABLEG2 , CFAM , NSOC , NLIEU , WTABLEG , WACTIF ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , SUBSTR ( CTABLEG2 , 9 , 3 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PETI2'                                                      
;                                                                               
CREATE  VIEW M907.RVPETIQ                                                       
        ( CTABLEG2 , NSOCIETE , ZOPRIX , CPARAM , WTABLEG , VALPARM ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4         
        , 2 ) , SUBSTR ( CTABLEG2 , 6 , 5 ) , WTABLEG , SUBSTR ( WTABLEG        
        , 1 , 56 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PETIQ'                                                      
;                                                                               
CREATE  VIEW M907.RVPILOT                                                       
        ( CTABLEG2 , NSOC , NLIEU , TYPE , WTABLEG , WACTIF ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 9 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PILOT'                                                      
;                                                                               
CREATE  VIEW M907.RVPKLOA                                                       
        ( CTABLEG2 , CPREST , DEFFET , WTABLEG , ECL1 , ECL2 , ECL3 ,           
        ECL4 ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 12 ) , SUBSTR ( WTABLEG ,        
        13 , 12 ) , SUBSTR ( WTABLEG , 25 , 12 ) , SUBSTR ( WTABLEG , 37        
        , 12 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PKLOA'                                                      
;                                                                               
CREATE  VIEW M907.RVPLAGE                                                       
        ( CTABLEG2 , CPLAGE , WTABLEG , LPLAGE ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PLAGE'                                                      
;                                                                               
CREATE  VIEW M907.RVPLIEU                                                       
        ( CTABLEG2 , CPARAM , WTABLEG , LPARAM ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PLIEU'                                                      
;                                                                               
CREATE  VIEW M907.RVPMENA                                                       
        ( CTABLEG2 , IDENT , TYPE , WTABLEG , WACTIF , PARAM ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 40 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PMENA'                                                      
;                                                                               
CREATE  VIEW M907.RVPR930                                                       
        ( CTABLEG2 , NENTCDE , WTABLEG , NFICHIER , CPROGRAM ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 8 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PR930'                                                      
;                                                                               
CREATE  VIEW M907.RVPRACT                                                       
        ( CTABLEG2 , CACTIV , WTABLEG , WACTIF , PROTOC , CPRESTA ,             
        SUBTYPE , DOSSIER ) AS                                                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 15 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 10 ) , SUBSTR (              
        WTABLEG , 12 , 17 ) , SUBSTR ( WTABLEG , 29 , 10 ) , SUBSTR (           
        WTABLEG , 39 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRACT'                                                      
;                                                                               
CREATE  VIEW M907.RVPRALP                                                       
        ( CTABLEG2 , CPREST , WTABLEG , NEAN ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 13 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRALP'                                                      
;                                                                               
CREATE  VIEW M907.RVPREXP                                                       
        ( CTABLEG2 , CSELART , WTABLEG , WFLAG , LIBELLE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PREXP'                                                      
;                                                                               
CREATE  VIEW M907.RVPRFIL                                                       
        ( CTABLEG2 , CTPREST , CPARAM , CLEPARAM , WTABLEG , LVPARAM )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRFIL'                                                      
;                                                                               
CREATE  VIEW M907.RVPRGLM                                                       
        ( CTABLEG2 , CTYPMES , NCLETECH , WTABLEG , CPROG ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRGLM'                                                      
;                                                                               
CREATE  VIEW M907.RVPRGLV                                                       
        ( CTABLEG2 , CTYPMES , NCLETECH , WTABLEG , CPROG ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRGLV'                                                      
;                                                                               
CREATE  VIEW M907.RVPRIME                                                       
        ( CTABLEG2 , ROLE , WTABLEG , CODIC , PSE , OFFRE , PRESTA ,            
        CREDIT ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 1 ) , SUBSTR (               
        WTABLEG , 4 , 1 ) , SUBSTR ( WTABLEG , 5 , 1 ) , SUBSTR (               
        WTABLEG , 6 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRIME'                                                      
;                                                                               
CREATE  VIEW M907.RVPRLIB                                                       
        ( CTABLEG2 , CPRESTA , WTABLEG , WACTIF , TXTVA , LPRESTA , PRIX        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 30 ) , SUBSTR ( WTABLEG , 33 , 4 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRLIB'                                                      
;                                                                               
CREATE  VIEW M907.RVPRLIV                                                       
        ( CTABLEG2 , CPREST , WTABLEG , WACTIF , LPREST ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRLIV'                                                      
;                                                                               
CREATE  VIEW M907.RVPRPAR                                                       
        ( CTABLEG2 , CTPREST , CPARAM , CLEPARAM , WTABLEG , LVPARAM )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRPAR'                                                      
;                                                                               
CREATE  VIEW M907.RVPRPAV                                                       
        ( CTABLEG2 , TYPRES , ZONE , DEFFET , WTABLEG , CPRESTAT ,              
        VALEUR , TCALCUL ) AS                                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        2 ) , SUBSTR ( CTABLEG2 , 8 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 5 ) , SUBSTR ( WTABLEG , 6 , 6 ) , SUBSTR ( WTABLEG , 12 ,          
        30 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRPAV'                                                      
;                                                                               
CREATE  VIEW M907.RVPRPRE                                                       
        ( CTABLEG2 , CPRESTAT , WTABLEG , WACTIF , LPRESTAT ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 30 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PRPRE'                                                      
;                                                                               
CREATE  VIEW M907.RVPSEHY                                                       
        ( CTABLEG2 , CGARANT , CFAM , CTARIF , WTABLEG , CSERVHY ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PSEHY'                                                      
;                                                                               
CREATE  VIEW M907.RVPSEIN                                                       
        ( CTABLEG2 , CGCPLTNU , WTABLEG , CGCPLT , WACTIF , LIBCPLT ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 ) , SUBSTR (               
        WTABLEG , 7 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PSEIN'                                                      
;                                                                               
CREATE  VIEW M907.RVPSENC                                                       
        ( CTABLEG2 , NCODIC , WTABLEG , WACTIF , LCOMMENT ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PSENC'                                                      
;                                                                               
CREATE  VIEW M907.RVPSEPX                                                       
        ( CTABLEG2 , CFAM , CGCPLT , CTARIF , WTABLEG , WACTIF , PXMIN ,        
        PXMAX ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 ,         
        5 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PSEPX'                                                      
;                                                                               
CREATE  VIEW M907.RVPT9901                                                      
        AS                                                                      
SELECT  ALL CODLANG , CODPIC                                                    
    FROM  M907.RTPT01                                                           
;                                                                               
CREATE  VIEW M907.RVPUMUT                                                       
        ( CTABLEG2 , TYPMUT , WTABLEG , WACTIF , WPURGE , WDIVERS ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 1 ) , SUBSTR (               
        WTABLEG , 3 , 54 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PUMUT'                                                      
;                                                                               
CREATE  VIEW M907.RVPVACC                                                       
        ( CTABLEG2 , RAYFAM , WTABLEG , W , LIBELLE ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PVACC'                                                      
;                                                                               
CREATE  VIEW M907.RVPWCNC                                                       
        ( CTABLEG2 , NSOC , NLIEU , WTABLEG , PWCNC ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PWCNC'                                                      
;                                                                               
CREATE  VIEW M907.RVPWORD                                                       
        ( CTABLEG2 , NLIEU , CTRAIT , WTABLEG , PWORD ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 )                            
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PWORD'                                                      
;                                                                               
CREATE  VIEW M907.RVPXEXC                                                       
        ( CTABLEG2 , NSOCIETE , NLIEU , CFAM , WTABLEG , EXCLU ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 1 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PXEXC'                                                      
;                                                                               
CREATE  VIEW M907.RVPXOPS                                                       
        ( CTABLEG2 , COPS , DOPS , WTABLEG , OPATTR , LIEU , PXTYPE ,           
        PXATTR ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG ,        
        11 , 11 ) , SUBSTR ( WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23         
        , 10 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'PXOPS'                                                      
;                                                                               
CREATE  VIEW M907.RVQAUTO                                                       
        ( CTABLEG2 , SOCLIEU , CAUTO , WTABLEG , WACTIF , QUOTA ) AS            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 4 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QAUTO'                                                      
;                                                                               
CREATE  VIEW M907.RVQCPRE                                                       
        ( CTABLEG2 , CPRESTA , WTABLEG , WACTIF , CTYPSERV , TCARTE ,           
        LIEU , CFAM ) AS                                                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR (               
        WTABLEG , 7 , 1 ) , SUBSTR ( WTABLEG , 8 , 6 ) , SUBSTR (               
        WTABLEG , 14 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QCPRE'                                                      
;                                                                               
CREATE  VIEW M907.RVQCSOC                                                       
        ( CTABLEG2 , NSOCIETE , WTABLEG , WACTIF , LSOCIETE ) AS                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'QCSOC'                                                      
;                                                                               
CREATE  VIEW M907.RVRAINT                                                       
        ( CTABLEG2 , NSOC , CINT , CRIT1 , CRIT2 , WTABLEG , WACTIF ,           
        VALEUR ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        4 ) , SUBSTR ( CTABLEG2 , 8 , 5 ) , SUBSTR ( CTABLEG2 , 13 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 ,         
        20 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RAINT'                                                      
;                                                                               
CREATE  VIEW M907.RVRB050                                                       
        ( CTABLEG2 , NSEQ , WTABLEG , IDENTMSG , NOMDTD , LGCOPY ,              
        CPARAM ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 ) , SUBSTR (            
        WTABLEG , 41 , 5 ) , SUBSTR ( WTABLEG , 46 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RB050'                                                      
;                                                                               
CREATE  VIEW M907.RVRB130                                                       
        ( CTABLEG2 , TABLE , CLE , WTABLEG , LIBELLE ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 60 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RB130'                                                      
;                                                                               
CREATE  VIEW M907.RVRB600                                                       
        ( CTABLEG2 , NSOCIETE , WTABLEG , NSOCPLTF , NLIEUPTF , NSSLIEUP        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 3 ) , SUBSTR (               
        WTABLEG , 7 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RB600'                                                      
;                                                                               
CREATE  VIEW M907.RVRB700                                                       
        ( CTABLEG2 , XX , CODE , SCODE , WTABLEG , W , PRES2 , DIVERS )         
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        7 ) , SUBSTR ( CTABLEG2 , 10 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 ) , SUBSTR ( WTABLEG , 7 ,         
        40 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RB700'                                                      
;                                                                               
CREATE  VIEW M907.RVRB800                                                       
        ( CTABLEG2 , CFAM , CMARQ , CCTRL , WTABLEG , LIBELLE ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 20 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RB800'                                                      
;                                                                               
CREATE  VIEW M907.RVRBAUT                                                       
        ( CTABLEG2 , SOURCE , TSSID , WTABLEG , SOCIETE , DIVERS ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        7 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 40 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBAUT'                                                      
;                                                                               
CREATE  VIEW M907.RVRBCNS                                                       
        ( CTABLEG2 , CODIC , WTABLEG , CNS , LCNS ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBCNS'                                                      
;                                                                               
CREATE  VIEW M907.RVRBCTL                                                       
        ( CTABLEG2 , CVAL , NSEQ , WTABLEG , VALEUR ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 50 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBCTL'                                                      
;                                                                               
CREATE  VIEW M907.RVRBCTV                                                       
        ( CTABLEG2 , OFFRE , WTABLEG , LOFFRE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBCTV'                                                      
;                                                                               
CREATE  VIEW M907.RVRBECH                                                       
        ( CTABLEG2 , CODIC1 , CODIC2 , CLEF , WTABLEG , DEFFET , CODIC3         
        , FLAGS ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        7 ) , SUBSTR ( CTABLEG2 , 15 , 1 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 7 ) , SUBSTR ( WTABLEG , 16 ,        
        30 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBECH'                                                      
;                                                                               
CREATE  VIEW M907.RVRBELV                                                       
        ( CTABLEG2 , TENLV , WTABLEG , MODPAI , NA ) AS                         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBELV'                                                      
;                                                                               
CREATE  VIEW M907.RVRBEQP                                                       
        ( CTABLEG2 , TYEQP , WTABLEG , WCPLT , LEQPM , NEAN , IMMO ,            
        WDIVERS ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 13 ) , SUBSTR ( WTABLEG , 35 , 1 ) , SUBSTR (            
        WTABLEG , 36 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBEQP'                                                      
;                                                                               
CREATE  VIEW M907.RVRBERR                                                       
        ( CTABLEG2 , CODE , WTABLEG , CODERB , FILLER ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 50 ) , SUBSTR ( WTABLEG , 51 , 6 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBERR'                                                      
;                                                                               
CREATE  VIEW M907.RVRBFIL                                                       
        ( CTABLEG2 , CFILTRE , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBFIL'                                                      
;                                                                               
CREATE  VIEW M907.RVRBFRQ                                                       
        ( CTABLEG2 , FREQ , WTABLEG ) AS                                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBFRQ'                                                      
;                                                                               
CREATE  VIEW M907.RVRBGVL                                                       
        ( CTABLEG2 , CDECL , SEQ , WTABLEG , FLAG , AJOUT ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 54 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBGVL'                                                      
;                                                                               
CREATE  VIEW M907.RVRBIMM                                                       
        ( CTABLEG2 , TYEQP , WTABLEG , BINFERIE , BSUPERIE ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBIMM'                                                      
;                                                                               
CREATE  VIEW M907.RVRBLOG                                                       
        ( CTABLEG2 , CLOGMNT , WTABLEG , LCLOGMNT , CAGREG , AFFICH ,           
        INFO2 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 1 ) , SUBSTR ( WTABLEG , 32 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBLOG'                                                      
;                                                                               
CREATE  VIEW M907.RVRBMHS                                                       
        ( CTABLEG2 , NSOC , NLIEU , CFAM , WTABLEG , TYPAUX , CAUX ,            
        PARGHE ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 6 ) , SUBSTR ( WTABLEG , 7 , 6 ) , SUBSTR ( WTABLEG , 13 ,          
        10 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBMHS'                                                      
;                                                                               
CREATE  VIEW M907.RVRBMNT                                                       
        ( CTABLEG2 , SAV , PRESTA , SEQ , WTABLEG , MONTANT , DATEF ,           
        COMMENT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , SUBSTR ( CTABLEG2 , 12 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 9 ) , SUBSTR ( WTABLEG , 10 , 8 ) , SUBSTR ( WTABLEG , 18         
        , 30 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBMNT'                                                      
;                                                                               
CREATE  VIEW M907.RVRBNRE                                                       
        ( CTABLEG2 , NRESEAU , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBNRE'                                                      
;                                                                               
CREATE  VIEW M907.RVRBPNP                                                       
        ( CTABLEG2 , NLIEU , WTABLEG , CPROFIL ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBPNP'                                                      
;                                                                               
CREATE  VIEW M907.RVRBPPB                                                       
        ( CTABLEG2 , CPOSMNT , WTABLEG , LPOSMNT , CAGREG , AFFICH ,            
        INFO2 ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 1 ) , SUBSTR ( WTABLEG , 32 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBPPB'                                                      
;                                                                               
CREATE  VIEW M907.RVRBPRF                                                       
        ( CTABLEG2 , CPROFIL , WTABLEG , TYPPRF , LPROFIL ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBPRF'                                                      
;                                                                               
CREATE  VIEW M907.RVRBRAC                                                       
        ( CTABLEG2 , CRACC , WTABLEG , LCRACC , CAGREG , AFFICH , INFO2         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 ) , SUBSTR (            
        WTABLEG , 31 , 1 ) , SUBSTR ( WTABLEG , 32 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBRAC'                                                      
;                                                                               
CREATE  VIEW M907.RVRBSAV                                                       
        ( CTABLEG2 , CSAV , WTABLEG , CPROFIL ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBSAV'                                                      
;                                                                               
CREATE  VIEW M907.RVRBSST                                                       
        ( CTABLEG2 , CST , WTABLEG , LST ) AS                                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 50 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBSST'                                                      
;                                                                               
CREATE  VIEW M907.RVRBSTA                                                       
        ( CTABLEG2 , WCMPLT , WTABLEG , LWCMPLT ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBSTA'                                                      
;                                                                               
CREATE  VIEW M907.RVRBSTR                                                       
        ( CTABLEG2 , CSTR , WTABLEG , CPROFIL ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBSTR'                                                      
;                                                                               
CREATE  VIEW M907.RVRBTAP                                                       
        ( CTABLEG2 , CTAP , WTABLEG , LCTAP , CAGREG ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBTAP'                                                      
;                                                                               
CREATE  VIEW M907.RVRBTRE                                                       
        ( CTABLEG2 , CTYPRES , WTABLEG ) AS                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RBTRE'                                                      
;                                                                               
CREATE  VIEW M907.RVRC1000                                                      
        AS                                                                      
SELECT  ALL NSOCIETE , NLIEU , NOMFIC , DATEFIC , DSYST                         
    FROM  M907.RTRC10                                                           
;                                                                               
CREATE  VIEW M907.RVRC2000                                                      
        AS                                                                      
SELECT  ALL NSOCIETE , NLIEU , NOMFIC , DATEFIC , NSEQ , ENRFIC1 ,              
        ENRFIC2 , DSYST                                                         
    FROM  M907.RTRC20                                                           
;                                                                               
CREATE  VIEW M907.RVRENUM                                                       
        ( CTABLEG2 , CFONC , TABLE , WTABLEG , NPGM , WACTIF ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG         
        , 11 , 1 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RENUM'                                                      
;                                                                               
CREATE  VIEW M907.RVRESBS                                                       
        ( CTABLEG2 , LIEU , CPARAM , WTABLEG , DATE ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        10 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 )                            
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RESBS'                                                      
;                                                                               
CREATE  VIEW M907.RVRETTR                                                       
        ( CTABLEG2 , TYRT , WTABLEG , LTYRT , SEUIL , CORGT , WACC ,            
        COPAR ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 25 ) , SUBSTR ( WTABLEG , 26 , 6 ) , SUBSTR (             
        WTABLEG , 32 , 2 ) , SUBSTR ( WTABLEG , 34 , 1 ) , SUBSTR (             
        WTABLEG , 35 , 5 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RETTR'                                                      
;                                                                               
CREATE  VIEW M907.RVROUTR                                                       
        ( CTABLEG2 , TCODEM , SOCLDLF , WTABLEG , QUEUE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        9 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 50 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ROUTR'                                                      
;                                                                               
CREATE  VIEW M907.RVRPLCD                                                       
        ( CTABLEG2 , IRPLCD , WTABLEG , LIRPLCD , WIRPLCD ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'RPLCD'                                                      
;                                                                               
CREATE  VIEW M907.RVSAINT                                                       
        ( CTABLEG2 , SSTABLE , WTABLEG , WACTIF , LIBELLE ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SAINT'                                                      
;                                                                               
CREATE  VIEW M907.RVSAVPF                                                       
        ( CTABLEG2 , SOCIETE , SAV , WTABLEG , PFDOS , LIBRE ) AS               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 50 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SAVPF'                                                      
;                                                                               
CREATE  VIEW M907.RVSDA2I                                                       
        ( CTABLEG2 , NSOCIETE , DEFFET , WTABLEG , NSOCDA2I , LSOCDA2I )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG ,         
        4 , 50 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SDA2I'                                                      
;                                                                               
CREATE  VIEW M907.RVSEECL                                                       
        ( CTABLEG2 , TYPPREST , WTABLEG , WFLAG , CTABLE ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 5 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SEECL'                                                      
;                                                                               
CREATE  VIEW M907.RVSEQSA                                                       
        ( CTABLEG2 , CSELA , WTABLEG , NSEQ ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 3 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SEQSA'                                                      
;                                                                               
CREATE  VIEW M907.RVSGFDC                                                       
        ( CTABLEG2 , CODELIEU , WTABLEG , WACTIF ) AS                           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGFDC'                                                      
;                                                                               
CREATE  VIEW M907.RVSGPAR                                                       
        ( CTABLEG2 , CPROG , WTABLEG , WFLAG , QPVUNIT , COMMENT ,              
        WFLAG2 ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR (               
        WTABLEG , 5 , 15 ) , SUBSTR ( WTABLEG , 20 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGPAR'                                                      
;                                                                               
CREATE  VIEW M907.RVSGRUB                                                       
        ( CTABLEG2 , RUBPAIE , WTABLEG , TYPMNT , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG , 3 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SGRUB'                                                      
;                                                                               
CREATE  VIEW M907.RVSIBCL                                                       
        ( CTABLEG2 , CLIENT , WTABLEG , ACTIF , COMMENT ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 14 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SIBCL'                                                      
;                                                                               
CREATE  VIEW M907.RVSIBMQ                                                       
        ( CTABLEG2 , TYPVTE , WTABLEG , WACTIF , CFONC ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SIBMQ'                                                      
;                                                                               
CREATE  VIEW M907.RVSIBVE                                                       
        ( CTABLEG2 , VENTE , WTABLEG , ACTIF , RESTE ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 14 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 50 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SIBVE'                                                      
;                                                                               
CREATE  VIEW M907.RVSLARB                                                       
        ( CTABLEG2 , COPAR , WMAGPR , CMAGPR , NSEQ , WTABLEG , SOCLIEU         
        , RGARB ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 6 ) , SUBSTR ( CTABLEG2 , 13 , 3 )        
        , WTABLEG , SUBSTR ( WTABLEG , 1 , 6 ) , SUBSTR ( WTABLEG , 7 ,         
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLARB'                                                      
;                                                                               
CREATE  VIEW M907.RVSLAXE                                                       
        ( CTABLEG2 , CAUX , CFAM , WTABLEG ) AS                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLAXE'                                                      
;                                                                               
CREATE  VIEW M907.RVSLDAT                                                       
        ( CTABLEG2 , DATLIM , WTABLEG , WFLAG ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLDAT'                                                      
;                                                                               
CREATE  VIEW M907.RVSLDIM                                                       
        ( CTABLEG2 , NSOC , NLIEU , DINVDA , WTABLEG , DATDEB , DATINV ,        
        DHPHOTO ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 8 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 8 ) , SUBSTR ( WTABLEG , 9 , 8 ) , SUBSTR ( WTABLEG , 17 ,          
        13 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLDIM'                                                      
;                                                                               
CREATE  VIEW M907.RVSLPSL                                                       
        ( CTABLEG2 , CFONC , CEMPL , WTABLEG , WREPONS ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLPSL'                                                      
;                                                                               
CREATE  VIEW M907.RVSLRCO                                                       
        ( CTABLEG2 , COPSL , WTABLEG , IDRGPT ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLRCO'                                                      
;                                                                               
CREATE  VIEW M907.RVSLREC                                                       
        ( CTABLEG2 , COPAR , WTABLEG , WTYPE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLREC'                                                      
;                                                                               
CREATE  VIEW M907.RVSLRGP                                                       
        ( CTABLEG2 , IDRGPT , WTABLEG , LIBELLE , NSEQEDT , IMPR ,              
        TYPVALO ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 3 ) , SUBSTR (             
        WTABLEG , 34 , 1 ) , SUBSTR ( WTABLEG , 35 , 4 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLRGP'                                                      
;                                                                               
CREATE  VIEW M907.RVSLRST                                                       
        ( CTABLEG2 , CPGM , CRSL , WTABLEG , NSEQ ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 3 )                            
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLRST'                                                      
;                                                                               
CREATE  VIEW M907.RVSLTDA                                                       
        ( CTABLEG2 , NSOC , NLIEU , WMAGPR , CMAGPR , CTYPDOC , WTABLEG         
        , WAUT ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , SUBSTR ( CTABLEG2 , 7 , 1 ) , SUBSTR ( CTABLEG2 , 8 , 6 )         
        , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG , 1         
        , 1 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SLTDA'                                                      
;                                                                               
CREATE  VIEW M907.RVSM145                                                       
        ( CTABLEG2 , CHEFPROD , WTABLEG , LGDRAYON , LCOMMENT ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 20 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SM145'                                                      
;                                                                               
CREATE  VIEW M907.RVSMDEL                                                       
        ( CTABLEG2 , CSIEB , WTABLEG , CMODDEL , WEMPORTE , LIBELLE ) AS        
 SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (           
        WTABLEG , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 ) , SUBSTR (               
        WTABLEG , 5 , 30 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SMDEL'                                                      
;                                                                               
CREATE  VIEW M907.RVSOCRD                                                       
        ( CTABLEG2 , CBAREME , NSOC , NSEQ , WTABLEG , DEFFET ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        3 ) , SUBSTR ( CTABLEG2 , 11 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 8 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SOCRD'                                                      
;                                                                               
CREATE  VIEW M907.RVSOURC                                                       
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SOURC'                                                      
;                                                                               
CREATE  VIEW M907.RVSTACP                                                       
        ( CTABLEG2 , STATCOMP , WTABLEG , WACTIF , LIBELLE , CAPPRO ,           
        CEXPO , NSEQ ) AS                                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 20 ) , SUBSTR (              
        WTABLEG , 22 , 5 ) , SUBSTR ( WTABLEG , 27 , 5 ) , SUBSTR (             
        WTABLEG , 32 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'STACP'                                                      
;                                                                               
CREATE  VIEW M907.RVSTFIC                                                       
        ( CTABLEG2 , NFIC , CSTAT , WTABLEG , LSTAT ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 )                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'STFIC'                                                      
;                                                                               
CREATE  VIEW M907.RVSTLIB                                                       
        ( CTABLEG2 , CTYPE , WTABLEG , WACTIF , LIBELLE ) AS                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 10 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'STLIB'                                                      
;                                                                               
CREATE  VIEW M907.RVSTMAG                                                       
        ( CTABLEG2 , SOC , MAG , WTABLEG , WACTIF ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 )                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'STMAG'                                                      
;                                                                               
CREATE  VIEW M907.RVSTYPE                                                       
        ( CTABLEG2 , ISTYPE , WTABLEG , LISTYPE ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'STYPE'                                                      
;                                                                               
CREATE  VIEW M907.RVSUFIX                                                       
        ( CTABLEG2 , NSOC , WTABLEG , SUFIX ) AS                                
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SUFIX'                                                      
;                                                                               
CREATE  VIEW M907.RVSV010                                                       
        ( CTABLEG2 , NSEQ , WTABLEG , IDENTMSG , NOMDTD , LGCOPY ,              
        CPARAM ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 ) , SUBSTR (            
        WTABLEG , 41 , 5 ) , SUBSTR ( WTABLEG , 46 , 10 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SV010'                                                      
;                                                                               
CREATE  VIEW M907.RVSVAPP                                                       
        ( CTABLEG2 , APPLICAT , NSEC , WTABLEG , TAPPLI , DAPPLI ) AS           
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        2 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 3 )                                                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SVAPP'                                                      
;                                                                               
CREATE  VIEW M907.RVSVFAM                                                       
        ( CTABLEG2 , APPLICAT , CFAM , NSEQ , WTABLEG , CMARKET1 ,              
        CMARKET2 , CFAMCIB ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        5 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 15 ) , SUBSTR ( WTABLEG , 16 , 15 ) , SUBSTR ( WTABLEG ,          
        31 , 5 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SVFAM'                                                      
;                                                                               
CREATE  VIEW M907.RVSVMKG                                                       
        ( CTABLEG2 , MARQUE , FSAV , SEQ , WTABLEG , CDESC , CMKG ,             
        DEFFET ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 5 ) , SUBSTR ( WTABLEG , 11 ,        
        8 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SVMKG'                                                      
;                                                                               
CREATE  VIEW M907.RVSVRYC                                                       
        ( CTABLEG2 , APPLICAT , CRAYCIB , WTABLEG , LRAYCIB , CTRAYCIB ,        
        COMMENT , COMMENT1 ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG ,        
        31 , 1 ) , SUBSTR ( WTABLEG , 32 , 10 ) , SUBSTR ( WTABLEG , 42         
        , 5 )                                                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SVRYC'                                                      
;                                                                               
CREATE  VIEW M907.RVSVRYL                                                       
        ( CTABLEG2 , APPLICAT , CRAYCIB , NSEQED , WTABLEG , CRAYMGIF ,         
        COMMENT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 8 ) , SUBSTR ( CTABLEG2 , 9 ,        
        5 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 10 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'SVRYL'                                                      
;                                                                               
CREATE  VIEW M907.RVTD065                                                       
        ( CTABLEG2 , PARAM , WTABLEG , DATDEB , WNBJ , NBJ , WDATFIN ,          
        DATFIN ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG , 9 , 1 ) , SUBSTR (               
        WTABLEG , 10 , 3 ) , SUBSTR ( WTABLEG , 13 , 1 ) , SUBSTR (             
        WTABLEG , 14 , 8 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TD065'                                                      
;                                                                               
CREATE  VIEW M907.RVTDACT                                                       
        ( CTABLEG2 , CODANO , CODACT , WTABLEG , CODPRE , LIEUACT , NSEQ        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG ,         
        6 , 1 ) , SUBSTR ( WTABLEG , 7 , 1 )                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDACT'                                                      
;                                                                               
CREATE  VIEW M907.RVTDANO                                                       
        ( CTABLEG2 , CDOSSIER , CODANO , WTABLEG , NAFFICHE , LIBANO ,          
        WCTRL , PROV ) AS                                                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 2 ) , SUBSTR ( WTABLEG ,         
        3 , 36 ) , SUBSTR ( WTABLEG , 39 , 1 ) , SUBSTR ( WTABLEG , 40 ,        
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDANO'                                                      
;                                                                               
CREATE  VIEW M907.RVTDARI                                                       
        ( CTABLEG2 , TYPDOSS , WARI , WTABLEG , LARI ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDARI'                                                      
;                                                                               
CREATE  VIEW M907.RVTDARO                                                       
        ( CTABLEG2 , TYPDOSS , WARO , WTABLEG , LARO ) AS                       
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDARO'                                                      
;                                                                               
CREATE  VIEW M907.RVTDCTY                                                       
        ( CTABLEG2 , CTYPPRES , WTABLEG , CTYPE ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 )                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDCTY'                                                      
;                                                                               
CREATE  VIEW M907.RVTDDOS                                                       
        ( CTABLEG2 , TYPDOSS , WTABLEG , LIBDOSS , WMTD00 , DMTD00 ,            
        WTD00 , DTD00 ) AS                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 8 ) , SUBSTR ( WTABLEG , 30 , 1 ) , SUBSTR (             
        WTABLEG , 31 , 8 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDDOS'                                                      
;                                                                               
CREATE  VIEW M907.RVTDFAC                                                       
        ( CTABLEG2 , FAMACT , WTABLEG , LFAMACT ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDFAC'                                                      
;                                                                               
CREATE  VIEW M907.RVTDFCT                                                       
        ( CTABLEG2 , FAMCTR , WTABLEG , LFAMCTR , WACTIF ) AS                   
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDFCT'                                                      
;                                                                               
CREATE  VIEW M907.RVTDIMP                                                       
        ( CTABLEG2 , TYPDOSS , LIEUACT , NOPTION , WTABLEG , SELECTIO ,         
        LIBSEL , PARAM ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        1 ) , SUBSTR ( CTABLEG2 , 7 , 2 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 10 ) , SUBSTR ( WTABLEG , 11 , 20 ) , SUBSTR ( WTABLEG , 31         
        , 20 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDIMP'                                                      
;                                                                               
CREATE  VIEW M907.RVTDIRA                                                       
        ( CTABLEG2 , TYPDOSS , CDOSSIER , WTABLEG ) AS                          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDIRA'                                                      
;                                                                               
CREATE  VIEW M907.RVTDIRO                                                       
        ( CTABLEG2 , TYPDOSS , WARO1 , WARO2 , WTABLEG , WARO3 ) AS             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDIRO'                                                      
;                                                                               
CREATE  VIEW M907.RVTDLAC                                                       
        ( CTABLEG2 , FAMACT , CODACT , WTABLEG ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG                                                           
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDLAC'                                                      
;                                                                               
CREATE  VIEW M907.RVTDLBA                                                       
        ( CTABLEG2 , CODACT , WTABLEG , LIBACT ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 60 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDLBA'                                                      
;                                                                               
CREATE  VIEW M907.RVTDLCT                                                       
        ( CTABLEG2 , CCTRL , WTABLEG , FAMCTR , WACTIF ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 1 )                          
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDLCT'                                                      
;                                                                               
CREATE  VIEW M907.RVTDLGN                                                       
        ( CTABLEG2 , TYPDOSS , ECRAN , NOPTION , WTABLEG , LIBLGN ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 54 )                                                              
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDLGN'                                                      
;                                                                               
CREATE  VIEW M907.RVTDMAA                                                       
        ( CTABLEG2 , CDOSSIER , WARO , CODANO , WTABLEG , DELAI , ACTIF         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 5 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 3 ) , SUBSTR ( WTABLEG , 4 , 1 )                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDMAA'                                                      
;                                                                               
CREATE  VIEW M907.RVTDMRQ                                                       
        ( CTABLEG2 , TYPDOSS , CMARQ , WTABLEG , LMARQ ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDMRQ'                                                      
;                                                                               
CREATE  VIEW M907.RVTDOPT                                                       
        ( CTABLEG2 , TYPDOSS , ECRAN , TYPLIEU , NOPTION , WTABLEG ,            
        LOPTION , NAFFICHE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 1 ) , SUBSTR ( CTABLEG2 , 12 , 2         
        ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG ,          
        21 , 2 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDOPT'                                                      
;                                                                               
CREATE  VIEW M907.RVTDRCH                                                       
        ( CTABLEG2 , TYPDOSS , CODRECH , WTABLEG , LIBRECH ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDRCH'                                                      
;                                                                               
CREATE  VIEW M907.RVTDSTA                                                       
        ( CTABLEG2 , TYPDOSS , CSTAT , WTABLEG , LSTAT , NAFFICHE ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 34 ) , SUBSTR ( WTABLEG ,        
        35 , 2 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDSTA'                                                      
;                                                                               
CREATE  VIEW M907.RVTDTER                                                       
        ( CTABLEG2 , CAPPLI , CTERM , WTABLEG , CETAT ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        4 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDTER'                                                      
;                                                                               
CREATE  VIEW M907.RVTDTYP                                                       
        ( CTABLEG2 , TYPDOSS , CTYPE , WTABLEG , LTYPE ) AS                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TDTYP'                                                      
;                                                                               
CREATE  VIEW M907.RVTOPE2                                                       
        ( CTABLEG2 , FAMP , DEFFET , SEQ , WTABLEG , W , NBJ , PARAM )          
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        8 ) , SUBSTR ( CTABLEG2 , 14 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 3 ) , SUBSTR ( WTABLEG , 5 ,         
        40 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TOPE2'                                                      
;                                                                               
CREATE  VIEW M907.RVTOPEP                                                       
        ( CTABLEG2 , FAMP , WTABLEG , WACTION , DEFFET , CTOP ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 8 ) , SUBSTR (               
        WTABLEG , 10 , 20 )                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TOPEP'                                                      
;                                                                               
CREATE  VIEW M907.RVTOPLM                                                       
        ( CTABLEG2 , NSOCDEP , NSOCLIE , NSEQ , WTABLEG , CSELMU , TYPOP        
        , CURGE ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 7 ) , SUBSTR ( WTABLEG , 8 , 2 ) , SUBSTR ( WTABLEG , 10 ,        
        1 )                                                                     
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TOPLM'                                                      
;                                                                               
CREATE  VIEW M907.RVTORG                                                        
        ( CTABLEG2 , TORG , WTABLEG , LORG , CAPPEL , FLAG_TIT , Z , Q )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 1 ) , SUBSTR ( WTABLEG , 23 , 1 ) , SUBSTR (             
        WTABLEG , 24 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TORG '                                                      
;                                                                               
CREATE  VIEW M907.RVTRAC1                                                       
        ( CTABLEG2 , CODIC , FONCTION , TCODE , WTABLEG , IDREGLE , COMM        
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 7 ) , SUBSTR ( CTABLEG2 , 8 ,        
        3 ) , SUBSTR ( CTABLEG2 , 11 , 2 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 10 ) , SUBSTR ( WTABLEG , 11 , 20 )                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRAC1'                                                      
;                                                                               
CREATE  VIEW M907.RVTRAC2                                                       
        ( CTABLEG2 , MARQUE , FAMILLE , FONCTION , TCODE , WTABLEG ,            
        IDREGLE , COMM ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        5 ) , SUBSTR ( CTABLEG2 , 11 , 3 ) , SUBSTR ( CTABLEG2 , 14 , 2         
        ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 10 ) , SUBSTR ( WTABLEG ,          
        11 , 20 )                                                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRAC2'                                                      
;                                                                               
CREATE  VIEW M907.RVTRAC3                                                       
        ( CTABLEG2 , IDREGLE , WTABLEG , REGLE , COMM , PRINT , CONTEXT         
        ) AS                                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 30 ) , SUBSTR ( WTABLEG , 31 , 20 ) , SUBSTR (            
        WTABLEG , 51 , 1 ) , SUBSTR ( WTABLEG , 52 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRAC3'                                                      
;                                                                               
CREATE  VIEW M907.RVTRALM                                                       
        ( CTABLEG2 , NSOCDEP , NSOCLIE , NSEQ , WTABLEG , CSELART ,             
        TRANS ) AS                                                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , SUBSTR ( CTABLEG2 , 7 ,        
        6 ) , SUBSTR ( CTABLEG2 , 13 , 3 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 5 ) , SUBSTR ( WTABLEG , 6 , 44 )                                 
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRALM'                                                      
;                                                                               
CREATE  VIEW M907.RVTRALV                                                       
        ( CTABLEG2 , CTRANS , WTABLEG , LTRANS ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 13 ) , WTABLEG , SUBSTR (            
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRALV'                                                      
;                                                                               
CREATE  VIEW M907.RVTRLIV                                                       
        ( CTABLEG2 , CODE , NSEQ , WTABLEG , VALTRI ) AS                        
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 60 )                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRLIV'                                                      
;                                                                               
CREATE  VIEW M907.RVTRTD2                                                       
        ( CTABLEG2 , ID , CHRONO , WTABLEG , FOLDER , CODE , REGLE ) AS         
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 25 ) , SUBSTR ( WTABLEG        
        , 26 , 15 ) , SUBSTR ( WTABLEG , 41 , 9 )                               
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRTD2'                                                      
;                                                                               
CREATE  VIEW M907.RVTRTDA                                                       
        ( CTABLEG2 , CODEACT , CHRONO , WTABLEG , SYSTEM , ID ) AS              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 15 ) , SUBSTR ( WTABLEG        
        , 16 , 10 )                                                             
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TRTDA'                                                      
;                                                                               
CREATE  VIEW M907.RVTVAVE                                                       
        ( CTABLEG2 , CTAUXTVA , DDEBEFF , WTABLEG , DFINEFF , VALTVA ,          
        VALCOEFF , LIBELLE ) AS                                                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , SUBSTR ( CTABLEG2 , 6 ,        
        8 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 8 ) , SUBSTR ( WTABLEG ,         
        9 , 5 ) , SUBSTR ( WTABLEG , 14 , 8 ) , SUBSTR ( WTABLEG , 22 ,         
        10 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TVAVE'                                                      
;                                                                               
CREATE  VIEW M907.RVTVOI1                                                       
        ( CTABLEG2 , TVOIE , WTABLEG , LVOIE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 4 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TVOI1'                                                      
;                                                                               
CREATE  VIEW M907.RVTYCLI                                                       
        ( CTABLEG2 , TYPCLI , WTABLEG , LTYPCLI ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 6 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TYCLI'                                                      
;                                                                               
CREATE  VIEW M907.RVTYFRS                                                       
        ( CTABLEG2 , CCOMPTE , WTABLEG , LCOMPTE , WENVSAP ) AS                 
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 )                        
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TYFRS'                                                      
;                                                                               
CREATE  VIEW M907.RVTYPCL                                                       
        ( CTABLEG2 , TYPCLIEN , WTABLEG , ACTIF , LIBELLE , PROMOSTI ,          
        COMMENT ) AS                                                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 15 ) , SUBSTR (              
        WTABLEG , 17 , 1 ) , SUBSTR ( WTABLEG , 18 , 40 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPCL'                                                      
;                                                                               
CREATE  VIEW M907.RVTYPLM                                                       
        ( CTABLEG2 , TYPOP , WTABLEG , LTYPOP ) AS                              
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 40 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPLM'                                                      
;                                                                               
CREATE  VIEW M907.RVTYPTR                                                       
        ( CTABLEG2 , CODE , WTABLEG , LIBELLE , TYPE , APVF , DIVERS ,          
        WACTIF ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 1 ) , SUBSTR (             
        WTABLEG , 22 , 4 ) , SUBSTR ( WTABLEG , 26 , 20 ) , SUBSTR (            
        WTABLEG , 46 , 1 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TYPTR'                                                      
;                                                                               
CREATE  VIEW M907.RVTYREG                                                       
        ( CTABLEG2 , SOC , UNIVR , WTABLEG , W , SEUIL , LCOMMENT ) AS          
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 3 ) , SUBSTR ( CTABLEG2 , 4 ,        
        5 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG ,         
        2 , 3 ) , SUBSTR ( WTABLEG , 5 , 20 )                                   
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'TYREG'                                                      
;                                                                               
CREATE  VIEW M907.RVVALCT                                                       
        ( CTABLEG2 , TYPCTE , ACTION , STATUT , WTABLEG , ERREUR ,              
        CODEER , LIBLER ) AS                                                    
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , SUBSTR ( CTABLEG2 , 3 ,        
        3 ) , SUBSTR ( CTABLEG2 , 6 , 10 ) , WTABLEG , SUBSTR ( WTABLEG         
        , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 2 ) , SUBSTR ( WTABLEG , 4 ,         
        30 )                                                                    
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'VALCT'                                                      
;                                                                               
CREATE  VIEW M907.RVWARGC                                                       
        ( CTABLEG2 , IWARGC , WTABLEG , LIWARGC ) AS                            
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'WARGC'                                                      
;                                                                               
CREATE  VIEW M907.RVXML00                                                       
        ( CTABLEG2 , CARACT , WTABLEG , ENTITE ) AS                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 10 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'XML00'                                                      
;                                                                               
CREATE  VIEW M907.RVXMLAT                                                       
        ( CTABLEG2 , IDSI , NSEQBAL , WTABLEG , IDBALISE , IDATTR ,             
        CMASQ , WPARAM ) AS                                                     
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG        
        , 21 , 16 ) , SUBSTR ( WTABLEG , 37 , 7 ) , SUBSTR ( WTABLEG ,          
        44 , 6 )                                                                
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'XMLAT'                                                      
;                                                                               
CREATE  VIEW M907.RVXMLBA                                                       
        ( CTABLEG2 , IDSI , NSEQ , WTABLEG , CMASQ , IDBALISE , NIVEAU ,        
        WPARAM ) AS                                                             
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 10 ) , SUBSTR ( CTABLEG2 , 11        
        , 3 ) , WTABLEG , SUBSTR ( WTABLEG , 1 , 7 ) , SUBSTR ( WTABLEG         
        , 8 , 20 ) , SUBSTR ( WTABLEG , 28 , 2 ) , SUBSTR ( WTABLEG , 30        
        , 20 )                                                                  
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'XMLBA'                                                      
;                                                                               
CREATE  VIEW M907.RVXMLCS                                                       
        ( CTABLEG2 , CARACT , WTABLEG , VALNUM , VALALPHA ) AS                  
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 )                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'XMLCS'                                                      
;                                                                               
CREATE  VIEW M907.RVXMLDT                                                       
        ( CTABLEG2 , CDATE , WTABLEG , FDATE ) AS                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 50 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'XMLDT'                                                      
;                                                                               
CREATE  VIEW M907.RVXNV60                                                       
        ( CTABLEG2 , NSEQ , WTABLEG , NOMXSD , NOMDTD , CODE , NOMPGM ,         
        CMSG ) AS                                                               
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 5 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 20 ) , SUBSTR ( WTABLEG , 21 , 20 ) , SUBSTR (            
        WTABLEG , 41 , 3 ) , SUBSTR ( WTABLEG , 44 , 5 ) , SUBSTR (             
        WTABLEG , 49 , 3 )                                                      
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'XNV60'                                                      
;                                                                               
CREATE  VIEW M907.RVYFLIE                                                       
        ( CTABLEG2 , TYPL , AFL , ATRT , WTABLEG , NFL , NTRT , DACTIF )        
        AS                                                                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 1 ) , SUBSTR ( CTABLEG2 , 2 ,        
        7 ) , SUBSTR ( CTABLEG2 , 9 , 5 ) , WTABLEG , SUBSTR ( WTABLEG ,        
        1 , 7 ) , SUBSTR ( WTABLEG , 8 , 5 ) , SUBSTR ( WTABLEG , 13 , 8        
        )                                                                       
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'YFLIE'                                                      
;                                                                               
CREATE  VIEW M907.RVZSENS                                                       
        ( CTABLEG2 , ALLEE , WTABLEG , WFLAG , WPARAM ) AS                      
SELECT  CTABLEG2 , SUBSTR ( CTABLEG2 , 1 , 2 ) , WTABLEG , SUBSTR (             
        WTABLEG , 1 , 1 ) , SUBSTR ( WTABLEG , 2 , 10 )                         
    FROM  M907.RTGA01                                                           
  WHERE CTABLEG1 = 'ZSENS'                                                      
;                                                                               
CREATE  VIEW M907.RVFG0105                                                 
  AS SELECT                                                                     
          NSOCCOMPT                                                             
        , CCHRONO                                                               
        , NUMFACT                                                               
        , NLIGNE                                                                
        , NSOCORIG                                                              
        , NLIEUORIG                                                             
        , NSOCDEST                                                              
        , NLIEUDEST                                                             
        , CTYPFACT                                                              
        , NATFACT                                                               
        , CVENT                                                                 
        , NUMORIG                                                               
        , DMOUV                                                                 
        , MONTHT                                                                
        , CTAUXTVA                                                              
        , TXESCPT                                                               
        , CFRAIS                                                                
        , MPCF                                                                  
        , MFRMG                                                                 
        , QNBPIECES                                                             
        , DCREAT                                                                
        , DPIECE                                                                
        , DCOMPTA                                                               
        , DMVT                                                                  
        , DANNUL                                                                
        , DECHEANC                                                              
        , DREGL                                                                 
        , DSYST                                                                 
        , SERVORIG                                                              
        , SERVDEST                                                              
        , DVALEUR                                                               
        , CACID                                                                 
        , DEXCPT                                                                
        , MPRMP                                                                 
        , SECORIG                                                               
        , SECDEST                                                               
        , NTIERSE                                                               
        , NLETTRE                                                               
        , NTIERSR                                                               
        , NLETTRR                                                               
FROM M907.RTFG01                                                                
;                                                                               
CREATE  VIEW M907.RVFG0203                                                 
  AS SELECT                                                                     
          NSOCCOMPT                                                             
        , NJRN                                                                  
        , NPIECE                                                                
        , NLIGNE                                                                
        , NEXERCICE                                                             
        , NPERIODE                                                              
        , NOECS                                                                 
        , DCREAT                                                                
        , COMPTE                                                                
        , SSCOMPTE                                                              
        , SECTION                                                               
        , RUBRIQUE                                                              
        , TCOMPTE                                                               
        , LIBELLE                                                               
        , MONTANT                                                               
        , SENS                                                                  
        , DCOMPTA                                                               
        , NETAB                                                                 
        , NATFACT                                                               
        , CVENT                                                                 
        , NUMFACT                                                               
        , CTYPPIECE                                                             
        , CTYPLIGNE                                                             
        , NLIGNEFACT                                                            
        , TSOCFACT                                                              
        , NSOC                                                                  
        , NLIEU                                                                 
        , NSOCCOR                                                               
        , DATEFIC                                                               
        , WRECUP                                                                
        , DSYST                                                                 
        , NTIERS                                                                
        , DVALEUR                                                               
        , CCHRONO                                                               
        , DRECUPCC                                                              
        , TYPECPTE                                                              
        , TIERSGCT                                                              
        , NLETTRAGE                                                             
FROM M907.RTFG02                                                                
;                                                                               
--MW DZ-400 CREATE  VIEW M907.RVFX9000                                                 
--MW DZ-400   AS SELECT                                                                     
--MW DZ-400           NSOC                                                                  
--MW DZ-400         , NOECS                                                                 
--MW DZ-400         , CRITERE1                                                              
--MW DZ-400         , CRITERE2                                                              
--MW DZ-400         , CRITERE3                                                              
--MW DZ-400         , COMPTECG                                                              
--MW DZ-400         , SECTION                                                               
--MW DZ-400         , RUBRIQUE                                                              
--MW DZ-400         , DATEFF                                                                
--MW DZ-400         , WSEQFAM                                                               
--MW DZ-400         , DSYST                                                                 
--MW DZ-400 FROM M907.RTFX90                                                                
--MW DZ-400 ;                                                                               
CREATE VIEW M907.RVGA7100                                                       
  AS SELECT                                                                     
          CTABLE                                                                
        , CSTABLE                                                               
        , LTABLE                                                                
        , CRESP                                                                 
        , DDEBUT                                                                
        , DFIN                                                                  
        , DMAJ                                                                  
        , CHAMP1                                                                
        , LCHAMP1                                                               
        , QCHAMP1                                                               
        , WCHAMP1                                                               
        , QDEC1                                                                 
        , WCTLDIR1                                                              
        , WCTLIND1                                                              
        , CCTLIND11                                                             
        , CCTLIND12                                                             
        , CCTLIND13                                                             
        , CCTLIND14                                                             
        , CCTLIND15                                                             
        , CTABASS1                                                              
        , CSTABASS1                                                             
        , WPOSCLE1                                                              
        , CHAMP2                                                                
        , LCHAMP2                                                               
        , QCHAMP2                                                               
        , WCHAMP2                                                               
        , QDEC2                                                                 
        , WCTLDIR2                                                              
        , WCTLIND2                                                              
        , CCTLIND21                                                             
        , CCTLIND22                                                             
        , CCTLIND23                                                             
        , CCTLIND24                                                             
        , CCTLIND25                                                             
        , CTABASS2                                                              
        , CSTABASS2                                                             
        , WPOSCLE2                                                              
        , CHAMP3                                                                
        , LCHAMP3                                                               
        , QCHAMP3                                                               
        , WCHAMP3                                                               
        , QDEC3                                                                 
        , WCTLDIR3                                                              
        , WCTLIND3                                                              
        , CCTLIND31                                                             
        , CCTLIND32                                                             
        , CCTLIND33                                                             
        , CCTLIND34                                                             
        , CCTLIND35                                                             
        , CTABASS3                                                              
        , CSTABASS3                                                             
        , WPOSCLE3                                                              
        , CHAMP4                                                                
        , LCHAMP4                                                               
        , QCHAMP4                                                               
        , WCHAMP4                                                               
        , QDEC4                                                                 
        , WCTLDIR4                                                              
        , WCTLIND4                                                              
        , CCTLIND41                                                             
        , CCTLIND42                                                             
        , CCTLIND43                                                             
        , CCTLIND44                                                             
        , CCTLIND45                                                             
        , CTABASS4                                                              
        , CSTABASS4                                                             
        , WPOSCLE4                                                              
        , CHAMP5                                                                
        , LCHAMP5                                                               
        , QCHAMP5                                                               
        , WCHAMP5                                                               
        , QDEC5                                                                 
        , WCTLDIR5                                                              
        , WCTLIND5                                                              
        , CCTLIND51                                                             
        , CCTLIND52                                                             
        , CCTLIND53                                                             
        , CCTLIND54                                                             
        , CCTLIND55                                                             
        , CTABASS5                                                              
        , CSTABASS5                                                             
        , WPOSCLE5                                                              
        , CHAMP6                                                                
        , LCHAMP6                                                               
        , QCHAMP6                                                               
        , WCHAMP6                                                               
        , QDEC6                                                                 
        , WCTLDIR6                                                              
        , WCTLIND6                                                              
        , CCTLIND61                                                             
        , CCTLIND62                                                             
        , CCTLIND63                                                             
        , CCTLIND64                                                             
        , CCTLIND65                                                             
        , CTABASS6                                                              
        , CSTABASS6                                                             
        , WPOSCLE6                                                              
FROM M907.RTGA71                                                                
;                                                                               
